﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Serialization
Imports log4net
Imports System.Net.Mail
Imports System.Data.OleDb
Imports System
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.DirectoryServices
Imports System.Security
Imports System.Security.Principal
Imports System.Threading
Imports System.Globalization
Imports System.Windows.Forms
Imports System.Text.StringBuilder



Public Class Form1
    Private Shared ReadOnly Log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Dim dtUsers As DataTable
    Public Shared ADAdminUser As String = My.Settings.LDAPAdminUser
    ' = "administrator"; //Administrator username of DC
    Public Shared ADAdminPassword As String = My.Settings.LDAPAdminPassword
    ' = "password"; //Password of admin user on DC
    'This needs to have the domain name or IP address on this line
    Public Shared ADFullPath As String = My.Settings.LDAPRootpath
    Public Shared ADServer As String = My.Settings.LDAPServer
    Private con As New SqlConnection(My.Settings.ConnString)
    ' = "LDAP://10.3.12.250"; 
    'This must be the domain name of the domain controller (not the computer name, just the domain name)
    'Public Shared ADServer As String = "IFMM"
    ' = "sakura.com";
    '		private static string ADPath
    Sub LoadGroups()
        Try

            Dim dsGroups As DataSet
            dsGroups = GetAllGroups(txtGroupName.Text)

            lstGroups.DisplayMember = "GroupName"
            lstGroups.ValueMember = "GroupName"
            dsGroups.Tables(0).DefaultView.Sort = "GroupName"
            lstGroups.DataSource = dsGroups.Tables(0).DefaultView.ToTable("Groups", True)

        Catch ex As Exception

            MsgBox("There was an issue with the Active Direcotry Import Module: " & ex.Message)
        End Try

    End Sub
    Sub LoadUserList()
        Try
            dtUsers = New DataTable
            dtUsers.Columns.Add("UserName")
            dtUsers.Columns.Add("DisplayName")
            dtUsers.Columns.Add("EMailAddress")
            'Create default row
            Dim drUser As DataRow = dtUsers.NewRow()
            drUser("UserName") = "0"
            drUser("DisplayName") = "(Not Specified)"
            drUser("EMailAddress") = "(Not Specified)"
            dtUsers.Rows.Add(drUser)

            For Each s As DataRowView In lstGroups.SelectedItems
                Dim ds As DataSet
                ds = GetUsersinGroup(s.Item(0).ToString())
                dtUsers.Merge(ds.Tables(0))
            Next
            dtUsers.Rows.RemoveAt(0)
            dtUsers.DefaultView.Sort = "DisplayName"
            lstUsers.DisplayMember = "DisplayName"
            lstUsers.ValueMember = "UserName"
            lstUsers.DataSource = dtUsers.DefaultView.ToTable("Users", True)
        Catch ex As Exception

            MsgBox("There was an issue with the Active Direcotry Import Module: " & ex.Message)
        End Try

    End Sub
    Private Shared Function GetDirectoryObject() As DirectoryEntry
        Dim oDE As DirectoryEntry

        oDE = New DirectoryEntry(ADFullPath, ADAdminUser, ADAdminPassword, AuthenticationTypes.SecureSocketsLayer)

        Return oDE
    End Function

    Private Shared Function GetDirectoryObject(ByVal DomainReference As String) As DirectoryEntry
        Dim oDE As DirectoryEntry

        oDE = New DirectoryEntry(ADServer + DomainReference, ADAdminUser, ADAdminPassword, AuthenticationTypes.Secure)

        Return oDE
    End Function


    Public Shared Function GetAllGroups(GroupName As String) As DataSet
        Dim dsGroup As New DataSet()
        'Dim dirEntry As DirectoryEntry = GetDirectoryObject("DC=" & ADServer)
        Dim dirEntry As DirectoryEntry = GetDirectoryObject()

        'create instance fo the direcory searcher
        Dim dirSearch As New DirectorySearcher()

        'set the search filter
        dirSearch.SearchRoot = dirEntry
        dirSearch.SearchScope = SearchScope.Subtree
        'deSearch.PropertiesToLoad.Add("cn");
        'dirSearch.Filter = "(&(objectClass=group)(cn=CS_*))"
        dirSearch.Filter = "(&(objectClass=group)(cn=" + GroupName + "*))"

        'find the first instance
        Dim searchResults As SearchResultCollection = dirSearch.FindAll()

        'Create a new table object within the dataset
        Dim dtGroup As DataTable = dsGroup.Tables.Add("Groups")
        dtGroup.Columns.Add("GroupName")

        'if there are results (there should be some!!), then convert the results
        'into a dataset to be returned.
        If searchResults.Count > 0 Then

            'iterate through collection and populate the table with
            'the Group Name
            For Each Result As SearchResult In searchResults
                'set a new empty row
                Dim drGroup As DataRow = dtGroup.NewRow()

                'populate the column
                drGroup("GroupName") = Result.Properties("cn")(0)

                'append the row to the table of the dataset
                dtGroup.Rows.Add(drGroup)
            Next
        End If

        '---------------- UNIVERSAL GROUPS --------------------------

        'set the search filter
        dirSearch.SearchRoot = dirEntry
        dirSearch.SearchScope = SearchScope.Subtree
        'deSearch.PropertiesToLoad.Add("cn");
        'dirSearch.Filter = "(&(objectClass=group)(cn=CS_*))"
        dirSearch.Filter = "(&(objectClass=group)(groupType:1.2.840.113556.1.4.803:=8)(cn=" + GroupName + "*))"

        'find the first instance
        searchResults = dirSearch.FindAll()

        'Create a new table object within the dataset
        'Dim dtGroup As DataTable = dsGroup.Tables.Add("Groups")
        'dtGroup.Columns.Add("GroupName")

        'if there are results (there should be some!!), then convert the results
        'into a dataset to be returned.
        If searchResults.Count > 0 Then

            'iterate through collection and populate the table with
            'the Group Name
            For Each Result As SearchResult In searchResults
                'set a new empty row
                Dim drGroup As DataRow = dtGroup.NewRow()

                'populate the column
                drGroup("GroupName") = Result.Properties("cn")(0)

                'append the row to the table of the dataset
                dtGroup.Rows.Add(drGroup)
            Next
        End If


        Return dsGroup
    End Function

    Public Shared Function GetDirectoryEntry() As DirectoryEntry
        Dim dirEntry As DirectoryEntry = New DirectoryEntry()
        dirEntry.Path = My.Settings.LDAPRootpath
        dirEntry.Username = My.Settings.LDAPAdminUser
        dirEntry.Password = My.Settings.LDAPAdminPassword
        Return dirEntry
    End Function
    Public Shared Function GetDirectoryEntry(path As String) As DirectoryEntry
        Dim dirEntry As DirectoryEntry = New DirectoryEntry()
        dirEntry.Path = path
        dirEntry.Username = My.Settings.LDAPAdminUser
        dirEntry.Password = My.Settings.LDAPAdminPassword
        Return dirEntry
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        LoadGroups()

    End Sub
    Public Shared Function GetUsersinGroup(ByVal GroupName As String) As DataSet
        Dim dsUsers As New DataSet()
        Dim dirEntry As DirectoryEntry = GetDirectoryObject()

        'create instance fo the direcory searcher
        Dim dirSearch As New DirectorySearcher()

        'set the search filter
        dirSearch.SearchRoot = dirEntry
        dirSearch.SearchScope = SearchScope.Subtree
        'deSearch.PropertiesToLoad.Add("cn");
        dirSearch.Filter = "(&(objectClass=group)(cn=" + GroupName + "))"

        'get the group result
        Dim searchResults As SearchResult = dirSearch.FindOne()

        'Create a new table object within the dataset
        Dim dtUser As DataTable = dsUsers.Tables.Add("Users")
        dtUser.Columns.Add("UserName")
        dtUser.Columns.Add("DisplayName")
        dtUser.Columns.Add("EMailAddress")

        'Create default row
        Dim drUser As DataRow = dtUser.NewRow()
        drUser("UserName") = "0"
        drUser("DisplayName") = "(Not Specified)"
        drUser("EMailAddress") = "(Not Specified)"
        dtUser.Rows.Add(drUser)

        'if the group is valid, then continue, otherwise return a blank dataset
        If Not searchResults Is Nothing Then
            'create a link to the group object, so we can get the list of members
            'within the group
            Dim dirGroup As New DirectoryEntry(searchResults.Path, ADAdminUser, ADAdminPassword, AuthenticationTypes.Secure)
            'assign a property collection
            Dim propCollection As System.DirectoryServices.PropertyCollection = dirGroup.Properties
            Dim n As Integer = propCollection("member").Count
            For l As Integer = 0 To n - 1

                'if there are members fo the group, then get the details and assign to the table
                'create a link to the user object sot hat the FirstName, LastName and SUername can be gotten
                Dim deUser As New DirectoryEntry(ADServer + "/" + propCollection("member")(l).ToString(), ADAdminUser, ADAdminPassword, AuthenticationTypes.Secure)
                'Dim deUser As DirectoryEntry = GetDirectoryEntry(ADServer + "/" + propCollection("member")(l).ToString())

                'set a new empty row
                Dim rwUser As DataRow = dtUser.NewRow()

                'populate the column
                If GetProperty(deUser, "SamAccounttype") = "805306368" Then ' user record
                    rwUser("UserName") = GetProperty(deUser, "cn")
                    rwUser("DisplayName") = GetProperty(deUser, "displayName")
                    rwUser("EMailAddress") = GetProperty(deUser, "mail")
                    'append the row to the table of the dataset
                    dtUser.Rows.Add(rwUser)
                End If
                'close the directory entry object

                deUser.Close()
            Next
            dirGroup.Close()
        End If

        ' ----------------------------- UNIVERSAL GROUP USER SEARCH ----------


        'create instance fo the direcory searcher
        dirSearch = New DirectorySearcher()
        dirSearch.SearchScope = SearchScope.Subtree
        'set the search filter
        dirSearch.SearchRoot = dirEntry
        'deSearch.PropertiesToLoad.Add("cn");
        dirSearch.Filter = "(&(objectClass=group)(groupType:1.2.840.113556.1.4.803:=8)(cn=" + GroupName + "))"

        'get the group result
        searchResults = dirSearch.FindOne()

        'Create a new table object within the dataset
        'Dim dtUser As DataTable = dsUsers.Tables.Add("Users")
        'dtUser.Columns.Add("UserName")
        'dtUser.Columns.Add("DisplayName")
        'dtUser.Columns.Add("EMailAddress")

        ''Create default row
        'Dim drUser As DataRow = dtUser.NewRow()
        'drUser("UserName") = "0"
        'drUser("DisplayName") = "(Not Specified)"
        'drUser("EMailAddress") = "(Not Specified)"
        'dtUser.Rows.Add(drUser)

        'if the group is valid, then continue, otherwise return a blank dataset
        If Not searchResults Is Nothing Then
            'create a link to the group object, so we can get the list of members
            'within the group
            Dim dirGroup As New DirectoryEntry(searchResults.Path, ADAdminUser, ADAdminPassword, AuthenticationTypes.Secure)
            'assign a property collection
            Dim propCollection As System.DirectoryServices.PropertyCollection = dirGroup.Properties
            Dim n As Integer = propCollection("member").Count
            For l As Integer = 0 To n - 1

                'if there are members fo the group, then get the details and assign to the table
                'create a link to the user object sot hat the FirstName, LastName and SUername can be gotten
                Dim deUser As New DirectoryEntry(ADServer + "/" + propCollection("member")(l).ToString(), ADAdminUser, ADAdminPassword, AuthenticationTypes.Secure)
                'Dim deUser As DirectoryEntry = GetDirectoryEntry(ADServer + "/" + propCollection("member")(l).ToString())

                'set a new empty row
                Dim rwUser As DataRow = dtUser.NewRow()

                'populate the column
                If GetProperty(deUser, "SamAccounttype") = "805306368" Then ' user record
                    rwUser("UserName") = GetProperty(deUser, "cn")
                    rwUser("DisplayName") = GetProperty(deUser, "displayName")
                    rwUser("EMailAddress") = GetProperty(deUser, "mail")
                    'append the row to the table of the dataset
                    dtUser.Rows.Add(rwUser)
                End If
                'close the directory entry object

                deUser.Close()
            Next
            dirGroup.Close()
        End If


        dirEntry.Close()
        dtUser.Rows.RemoveAt(0)
        Return dsUsers
    End Function
    Public Shared Function GetProperty(ByVal oDE As DirectoryEntry, ByVal PropertyName As String) As String
        Try

            Return oDE.Properties(PropertyName)(0).ToString()
        Catch ex As Exception
            Return String.Empty
        End Try

    End Function

    ''' <summary>
    ''' This is an override that will allow a property to be extracted directly from
    ''' a searchresult object
    ''' </summary>
    ''' <param name="searchResult"></param>
    ''' <param name="PropertyName"></param>
    ''' <returns></returns>
    Public Shared Function GetProperty(ByVal searchResult As SearchResult, ByVal PropertyName As String) As String
        If searchResult.Properties.Contains(PropertyName) Then
            Return searchResult.Properties(PropertyName)(0).ToString()
        Else
            Return String.Empty
        End If
    End Function

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Me.Close()
        'Me.Dispose()

    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        LoadUserList()
    End Sub

    Public Shared Function GetUser(ByVal UserName As String) As DirectoryEntry
        'create an instance of the DirectoryEntry
        Dim dirEntry As DirectoryEntry = GetDirectoryObject()
        'Dim dirEntry As DirectoryEntry = GetDirectoryObject()

        'create instance fo the direcory searcher
        Dim dirSearch As New DirectorySearcher(dirEntry)

        dirSearch.SearchRoot = dirEntry
        'set the search filter
        dirSearch.Filter = "(&(objectCategory=user)(cn=" + UserName + "))"
        'deSearch.SearchScope = SearchScope.Subtree;

        'find the first instance
        Dim searchResults As SearchResult = dirSearch.FindOne()

        'if found then return, otherwise return Null
        If Not searchResults Is Nothing Then
            'de= new DirectoryEntry(results.Path,ADAdminUser,ADAdminPassword,AuthenticationTypes.Secure);
            'if so then return the DirectoryEntry object
            Return searchResults.GetDirectoryEntry()
        Else
            Return Nothing
        End If
    End Function


    Private Sub btnImport_Click(sender As System.Object, e As System.EventArgs) Handles btnImport.Click
        Dim row As DataRowView
        Dim importCoutner As Integer = 0
        For Each row In lstUsers.Items
            Dim deUser As DirectoryEntry
            deUser = GetUser(row.Item("UserName").ToString())
            Dim userid As String = GetProperty(deUser, "sAMAccountName")

            If userExistsInIdam(userid) Then
                txtMessages.Text = txtMessages.Text & "User " & userid & " already Exists in iDAM." & vbCrLf
            Else
                importUser(deUser)
                importCoutner = importCoutner + 1
            End If


        Next
        txtMessages.Text = txtMessages.Text & "Total Imported Users:  " & importCoutner & "." & vbCrLf
    End Sub
    Private Sub importUser(deUser As DirectoryEntry)
        Try

            Dim firstname As String = GetProperty(deUser, "givenname")
            Dim lastname As String = GetProperty(deUser, "sn")
            Dim userid As String = GetProperty(deUser, "sAMAccountName")
            Dim email As String = GetProperty(deUser, "mail")
            Dim username As String

            ' for hdr they configure username with sAMAccountName
            'username = "cn=" & GetProperty(deUser, "cn") & IIf(GetProperty(deUser, "ou") <> "", ",ou=" & GetProperty(deUser, "ou"), "") & IIf(GetProperty(deUser, "o") <> "", ",o=" & GetProperty(deUser, "o"), "")
            username = userid

            Dim maxCounter As String = GetMaxCounterID().ToString()
            con.Open()
            Dim cmd As New SqlCommand("insert into IPM_User (userid, securitylevel_id, firstname, lastname, email, regdate, regupdatedate, login, username, LDAP_Authentication, Active, update_date) " & _
                                      " values (@maxCounter , 1, @firstname , @lastname , @email , getDate(), getDate(), @Login, @username, 1, 'Y', getDate() )  ", con)
            cmd.Parameters.AddWithValue("@maxCounter", maxCounter)
            cmd.Parameters.AddWithValue("@firstname", firstname)
            cmd.Parameters.AddWithValue("@lastname", lastname)
            cmd.Parameters.AddWithValue("@email", email)
            cmd.Parameters.AddWithValue("@Login", userid)
            cmd.Parameters.AddWithValue("@username", username)
            cmd.ExecuteNonQuery()

            cmd = New SqlCommand("insert into IPM_User_role (userid, role_id) " & _
                                      " select @maxCounter, role_id from ipm_role where name = 'Default'  ", con)
            cmd.Parameters.AddWithValue("@maxCounter", maxCounter)
            cmd.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            MsgBox("There was an issue with the Active Direcotry Import Module: " & ex.Message)
            con.Close()
        End Try

    End Sub
    Private Function GetMaxCounterID() As Integer
        Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select counter from ipm_cat_counter", MyConnection)
        Dim DT1 As New DataTable("counter")
        MyCommand1.Fill(DT1)

        Dim MyCommand2 As New SqlCommand("update ipm_cat_counter set counter = counter + 1", MyConnection)
        MyCommand2.Connection.Open()
        MyCommand2.ExecuteNonQuery()
        MyCommand2.Connection.Close()

        Return DT1.Rows(0)("counter")
    End Function

    Private Function userExistsInIdam(userid) As Boolean
        Try

            con.Open()
            Dim cmd As New SqlCommand("select Login from IPM_User where login = @userid  ", con)
            cmd.Parameters.AddWithValue("@userid", userid)
            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable()
            da.Fill(dt)

            If dt.Rows.Count > 0 Then
                con.Close()
                Return True
            Else
                con.Close()
                Return False
            End If
        Catch ex As Exception
            MsgBox("There was an issue with the Active Direcotry Import Module: " & ex.Message)
            con.Close()
        End Try

    End Function

    Private Sub Label3_Click(sender As System.Object, e As System.EventArgs)

    End Sub

   
    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        Dim dt As DataTable
        Dim i As Integer = 0
        dt = dtUsers.Copy()

        For Each i In lstUsers.SelectedIndices
            Dim drv As DataRow() = dt.Select("DisplayName = '" & CType(lstUsers.Items(i), DataRowView).Item("DisplayName") & "'")
            dt.Rows.Remove(drv(0))
        Next
        dt.DefaultView.Sort = "DisplayName"
        lstUsers.DataSource = dt.DefaultView
                lstUsers.DisplayMember = "DisplayName"
                lstUsers.ValueMember = "UserName"
        dtUsers = dt.Copy()
        If lstUsers.Items.Count - 1 >= i Then
            lstUsers.SelectedIndex = i

        End If
    End Sub

 
End Class
