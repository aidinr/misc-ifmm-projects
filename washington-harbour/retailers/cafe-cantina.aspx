﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="cafe-cantina.aspx.vb" Inherits="retailers_cafe_cantina" %>
<%@ Import Namespace="System.IO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html class="no-js" ng-app="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>The Washington Harbour - Cafe Cantina &amp; Pizza Pino</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="http://fonts.googleapis.com/css?family=Michroma" rel="stylesheet" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="/assets/css/tooltipster.css">
        <link rel="stylesheet" href="/assets/css/superslides.css">

        <link href="/assets/css/jquery.bxslider.css" rel="stylesheet" />
        <link rel="stylesheet/less" type="text/css" href="/assets/css/style.less">

        <script src="/assets/js/jquery-1.11.1.min.js"></script>
        <script src="/assets/js/jquery.bxslider.min.js"></script>
        <script src="/assets/js/angular.min.js"></script>
        <link type="text/css" rel="stylesheet" href="/assets/css/style.css"/> 
        <!-- <script src="assets/js/jq.js" type="text/javascript"></script> -->
        <script src="/assets/js/custom.js" type="text/javascript"></script>  

    </head>
    <body class="template-b template-a-b" id="cafe-centrina">
        <header id="header" ng-include src="'/nav.html'"></header>

        <section id="template-a-head" class="text-sections view-directory">
            <div class="title-two">Cafe Cantina &amp; Pizza Pino</div>
            <div class="separator"></div>
            <div class="text-section">
                <p class="cms cmsType_TextMulti cmsName_cafe_cantina_Page_FirstParagraph"></p>
            </div>
        </section>

        <section class="temp-a-gallery">
            <div class="bx-wrapperer">
                <ul class="bxslider">
                    <li><img src="/assets/img/gallery/cafe_cantina/retail-storefronts-cafe-cantina.jpg"/></li>
                    <li><img src="/assets/img/gallery/starbucks/retail-storefronts-starbucks2.jpg"/></li>
                    <li><img src="/assets/img/gallery/starbucks/retail-storefronts-starbucks3.jpg"/></li>
                </ul>
            </div>
        </section>
 
        <section class="retailer-info">
            
            <div class="logo-container">
                <a href=""><img class="cms cmsType_Image cmsName_cafe_cantina_Intro_Image" src="../cms/data/Image/<%=CMSFunctions.Image(Server.MapPath("~") & "\cms\data\", "cafe_cantina_Intro_Image")%>"  />
		        </a>
            </div>
            <div class="middle-container info-containers">
                <p class="cms cmsType_Rich cmsName_cafe_cantina_Page_SecondParagraph"></p>
                
            </div>
            <div class="last-container info-containers">
                <p class="cms cmsType_Rich cmsName_cafe_cantina_Page_ThirdParagraph"></p>
            </div>            
        </section>  
        
        <section id="directory-start" class="text-sections interactivemap-head interactivemap-btn up">
            <div class="title-two">Directory</div>
            <div class="interactivebtn-wraper"><span class="interactivemap-btn"></span></div>
            <div class="text-section">
                <p></p>
            </div>
        </section>

        <section id="imagemap-wrapper" ng-include src="'/directory.html'"></section>

        <footer ng-include src="'/footer.html'"></footer>
        
        <script src="/assets/js/modernizr.2.8.3.js"></script>
        <script src="/vimeo/js/jquery.flexslider-min.js" type="text/javascript"></script>        
        <script src="/vimeo/js/magnific-popup.0.9.9.js" type="text/javascript"></script>
        <script src="/vimeo/js/waypoints.js" type="text/javascript"></script>
        <script src="/vimeo/js/victoria.js" type="text/javascript"></script>
        <script src="/assets/js/jquery.easing.1.3.js"></script>
        <script src="/assets/js/jquery.animate-enhanced.min.js"></script>
        <script src="/assets/js/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>

        <script src="/assets/js/owl.carousel.js"></script>
        <script src="/assets/js/jquery.rwdImageMaps.min.js"></script>
        <script src="/assets/js/jquery.tooltipster.js"></script>
        <script src="/assets/js/less.js" type="text/javascript"></script>
        <script src="/assets/js/main.js"></script>
        <script>
            // Google Analytics
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l; b[l] || (b[l] =
            function () { (b[l].q = b[l].q || []).push(arguments) }); b[l].l = +new Date;
                e = o.createElement(i); r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            } (window, document, 'script', 'ga'));
            ga('create', 'UA-57349170-1'); ga('send', 'pageview');
        </script>
    </body>
</html>