var homeSlider;
var bgLoaded;
var resizeTimer;
var optionSearchTimer = new Array();
var lastSearch;
var fontsReady=0;
var pageReady=0;
var appLoaded;
var transitioning=0;

if(window.self===window.top) {

history.navigationMode = 'compatible';
$('*').unbind();
$(window).unload(function () {
    $('*').unbind();
    $('body', 'html').val('');
});

//global vars
var current_hash = window.location.hash;

if(window.location.search || readCookie('SiteEdit')) {

}
else if(getPage()!='' && current_hash.length>2) {
 document.location='./'+current_hash;
}
else if(getPage()!='' && current_hash.length<2) {
 document.location='./#' + getPage();
}
else if(getPage()=='' && current_hash.length<2) {
 document.location='./#home';
}

WebFontConfig = {
  google: { families: [ 'Montserrat:400,700:latin', 'Arvo:400,400italic,700:latin', 'Open+Sans:400,300,300italic,400italic,600,600italic,700:latin' ] },
  active : function() {
   fontsReady=1;
   if(pageReady==1) $('body').css('visibility','visible');
  } 
};

(function() {
  var wf = document.createElement('script');
  wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
  wf.type = 'text/javascript';
  wf.async = 'true';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(wf, s);
})();


var css = document.createElement('link');
css.setAttribute("rel", "stylesheet");
css.setAttribute("type", "text/css");
css.setAttribute("href", 'assets/scroller/scroll.css');
$('head').append(css);

var script = document.createElement('script');
script.type = 'text/javascript';
script.src = 'assets/scroller/scroll.js';
$('head').append(script);

var script = document.createElement('script');
script.type = 'text/javascript';
script.src = 'assets/map/maphilight.js';
$('head').append(script);


// ON resize
$(window).resize(function(){
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout('resized()', 80);
}) 

// ON Hash Change
$(window).on('hashchange', function() {
    ajax_content(window.location.hash.replace('#',''));
    scroll(0,0);
});   

// Document ready
$(document).ready(function() {

// createCookie('SiteEdit','1');
    if(readCookie('SiteEdit')) $('body').addClass('cmsEditing');
    
    /* Add class to body so we are sure we have js enabled */
    $('body').removeClass('page').addClass('js-enabled');

    $('a[href="'+ getPage() +'"]').addClass('active');
    $('.menu ul li a[href="'+ getPage().split('\-')[0] +'"]').addClass('active');
    $('.menu ul li a[href="'+ getPage().replace('ty','ties').split('\-')[0] +'"]').addClass('active');

    // menu ajax loading

    if(!window.location.search && !readCookie('SiteEdit')) {
      $('.menu ul li a[href], .tabs-holder>li>a[href], .ajax-next-arrow[href], .result-list a[href], .link[href]').live('click', function(ev) {
        ev.preventDefault();
        if($(this).attr('href').length > 2 ) window.location.hash = $(this).attr('href');
        else window.location.hash = 'home';
      });
    }

    /* Mobile navigation (collapsing) */
    $('.mobile-navigation').click(function(){
      if($(this).hasClass('active')) $('.menu ul').stop(true, true).slideUp(380);
      else $('.menu ul').stop(true, true).slideDown(380);
      $(this).toggleClass('active'); 
    });
      

    $('.menu a[href]').click(function(){
      if($('.mobile-navigation').hasClass('active')) $('.menu ul').stop(true, true).slideUp(380);
      else $('.menu ul').stop(true, true).slideDown(380);
      $('.mobile-navigation').removeClass('active');
    });
      
    /* Input placeholders */
    $('.placeholder').each(function(){
        var title = $(this).attr('title');
        var content = $(this).val();
        if (content == '')
        {
            $(this).val(title);
        }
    }).bind('focus', function() {
        var content = $(this).val();
        var title = $(this).attr('title');
        if (content == title)
        {
            $(this).val('');
        }
    }).bind('blur', function() {
        var content = $(this).val();
        var title = $(this).attr('title');
        if (content == '')
        {
            $(this).val(title);
        }
    });   

    $('.resizable-background img').load(function() {
       $(this).addClass('loaded');
    });

    resized();

    if(appLoaded!=1) {
      ajax_content(window.location.hash.replace('#',''));
      scroll(0,0);
    }


}); // End Document Ready

$(window).load(function(){
    if(!$('body').hasClass('cmsEditing')  && !$('.load iframe').is('iframe')) {
     $('.load a[href$="' + getPage() + '"]').remove();
     cachePages();
    }
});

function resized() {
    clearTimeout(bgLoaded);
    if($('.resizable-background img').hasClass('loaded')) {
      var w = $(window).width()+20;
      var h = $(window).height()+20;
      var imgW = $('.resizable-background img').width();
      var imgH = $('.resizable-background img').height();
      var a = imgH/imgW;
      ww=w;
      hh=w*a;
      if(hh<h) {
      ww=w/a;
      hh=h;
      }
      $('.resizable-background img').css('width',ww+'px').css('height',hh+'px');
      $('.resizable-background').css('visibility','visible');
    }
    else {
       bgLoaded = setTimeout('resized()',200);
    }
}

function bind_scroller() {

    if($('.comapny-history-scroller').hasClass('comapny-history-scroller')) {
      var w = parseInt($('#scroller li').outerWidth());
      var c = $('#scroller li').length;
      if(readCookie('SiteEdit')) c=c+1;
      $('.comapny-history-scroller ul').css('width', (c*w) + 'px');
    }
    else if($('.investors-scroller').hasClass('investors-scroller')) {
       var w = 0;
       $('.investors-scroller ul li').each(function() {
         w += $(this).outerWidth();
       });
      if(readCookie('SiteEdit')) w=w+115;
      $('.investors-scroller ul').css('width', (w) + 'px');
    }


    $("#scroller").mCustomScrollbar({
        scrollInertia:550,
        horizontalScroll:true,
        mouseWheelPixels:200,
        scrollButtons:{
            enable:true,
            scrollType:"pixels",
            scrollAmount:200
        },
        callbacks:{
            onScroll:function(){snapScrollbar();}
        }
    });
    /* toggle buttons scroll type */
    var content=$("#scroller");
    $("a[rel='toggle-buttons-scroll-type']").html("<code>scrollType: \""+content.data("scrollButtons_scrollType")+"\"</code>");
    $("a[rel='toggle-buttons-scroll-type']").click(function(e){
        e.preventDefault();
        var scrollType;
        if(content.data("scrollButtons_scrollType")==="pixels"){
                scrollType="continuous";
        }else{
                scrollType="pixels";
        }
        content.data({"scrollButtons_scrollType":scrollType}).mCustomScrollbar("update");
        $(this).html("<code>scrollType: \""+content.data("scrollButtons_scrollType")+"\"</code>");
    });
    /* snap scrollbar fn */
    var snapTo=[];
    $("#scroller ul li").each(function(){
        var $this=$(this),thisX=$this.position().left;
        snapTo.push(thisX);
    });
    function snapScrollbar(){
        var posX=$("#scroller .mCSB_container").position().left,closestX=findClosest(Math.abs(posX),snapTo);
        $("#scroller").mCustomScrollbar("scrollTo",closestX,{scrollInertia:350,callbacks:false});
    }
    function findClosest(num,arr){
        var curr=arr[0];
        var diff=Math.abs(num-curr);
        for(var val=0; val<arr.length; val++){
        var newdiff=Math.abs(num-arr[val]);
            if(newdiff<diff){
            diff=newdiff;
                curr=arr[val];
        }
        }
        return curr;
    } 
}

function ajax_content(hash) {
    if(!window.location.hash) {
       pageInit();
       return;
    }

    if(hash=='home') hash='./';
    $.get(hash, function(data) {
        if (transitioning==0) {
         transitioning=1;
         $('.resizable-background img').show();
         if($('.resizable-background img').length<2) $('.resizable-background img').clone().appendTo('.resizable-background');
         $('img:eq(0)').attr('src', $(data).find('img').first().attr('src'));  
         $('img:eq(1)').fadeOut(700, function() {
          $('.resizable-background').append( $('.resizable-background img:eq(0)'));
          $('.resizable-background img').show();
          transitioning=0;
         });
        }

        $('#content').html($(data).find('#content').html());
        $('.menu ul li a.active, .tabs-holder a.active').removeClass('active');
        $('.tabs-holder a[href="'+hash.replace('#','')+'"]').addClass('active');
        $('.menu ul li a[href="'+ hash.replace('#','').split('\-')[0] +'"]').addClass('active');
        $('.menu ul li a[href="'+ hash.replace('#','').replace('ty','ties').split('\-')[0] +'"]').addClass('active');
        pageInit();
    });
    return false;    
}

}


function getPage() {
    var nPage = window.location.pathname;
    nPage = nPage.substring(nPage.lastIndexOf('/') + 1);
    return nPage;
}

function pageInit() {
    appLoaded=1;

    $('.next-arrow').unbind();
    $('.next-arrow span strong').css('opacity','0').css('visibility', 'visible').css('display', 'block');
    $('.next-arrow').on('hover', function(){

    if($('.next-arrow span strong').text().split(' ')[1]) $('.next-arrow span strong').css('lineHeight','auto');
    else $('.next-arrow span strong').css('lineHeight','38px');

        $('.next-arrow span strong').stop(true, true).animate({'opacity': 1}, 300);
    }).mouseleave(function(){
        $('.next-arrow span strong').stop(true, true).animate({'opacity': 0}, 300);
    });

    /* Custom horizontal scroller */
    if($('#scroller').attr('id') && !$('.mCustomScrollbar').hasClass('mCustomScrollbar') ) {
        bind_scroller();
    }

    if($(".accordion").hasClass("accordion")) accordion();
    if($(".content").hasClass("news")) showNews();
    if($(".content").hasClass("careers")) showCareers();
    if($(".content").hasClass("contact")) showContact();
    if($(".content").hasClass("propertySearch")) showPropertySearch();
    if($(".content").hasClass("search-results-page")) showPropertySearch();
    if($(".content").hasClass("serviceRequest")) showServiceRequest();

    clearInterval(homeSlider);

    if($('#homeBuckets').attr('id') && !$('body').hasClass('cmsEditing')) {
     $('.slide').hide();
     $('.slide:eq(0)').show();
     var delay = 1000 * Math.round($('#delay').attr('class').replace(/[^\d.]/g, ''));
     if(delay<2) delay = 2000;
     if(!$('body').hasClass('cmsEditing')) homeSlider = setInterval('homeSlide()', delay)
     $('.slide').unbind();
     $('.slide').hover(
      function () {
        $('#homeBuckets').addClass("hover");
      },
      function () {
        $('#homeBuckets').removeClass("hover");
      }
     );
    }

    if($('.placeholder').hasClass('placeholder')) {
     $('.placeholder').each(function(){
      if($(this).val()) {
       $(this).attr('title',$(this).val());
       $(this).focus(function () {
        if($(this).attr('title')==$(this).val()) $(this).val('');
       });
       $(this).blur(function () {
        if (!$(this).val()) $(this).val($(this).attr('title'));
       });
      }
     });
    }

    if($('#keyword').attr('id')) {
      $('.submit.search').click(function () {
          document.location='./'+'#properties-search-'+ escape($('#keyword').val());
      });
     /* $('#keyword').keyup(function () {
       if(String($('#keyword').val()).length>2) {
        autoKeywords();
       }
      });*/
    }
    if($('.search-results-page').hasClass('search-results-page') && current_hash) {
      lastSearch=window.location.hash;
    }
    if($('.sidebar').hasClass('backToSearch') && lastSearch) {
      $('.sidebar.backToSearch').append('<a href="'+lastSearch+'" class="next-arrow go-back-link"><ins><var>&nbsp;</var></ins></a>');
    }
    $('input').attr('autocomplete', 'off');

    if($('.contact-form').hasClass('contact-form')) {
      var error=0;
      $(':input.required').focus(function() {
        $(this).css('background','#000');
      });
      $('.contact-form').submit(function() {
        error=0;
        $(':input.required').each(function() {
          if(!$(this).val() || $(this).val()==$(this).attr('title') || ( $(this).hasClass('email') && /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test($(this).val()) == false)) {
           $(this).css('background','#453333');
           if(!$('#inputProperty').val() ) $('.selectOptions a').addClass('error');
           error=1;
          }
        });
        if(error==1) {
          alert('You must provide valid information for all required fields.');
        }
        else {

                $.post($(this).attr('action') + $(this).attr('name') , $(this).serialize(), function (response) {
                    if (response == '1') {
                        $('#thanks').show();
                        $('.contact-form').remove();
                        _gaq.push(['_setAccount', GoogleAnalyticsAccount]); 
                        _gaq.push(['_trackPageview', '/contact_submitted_' + window.location.hash.substr(1)]);

                    } else {
                        alert('There was an error.  Please try again later.');
                        $('.contact-form').remove();
                    }
                    scroll(0, 0);
                    $('#subForm input:text, #subForm textarea').val('');
                });

        }
      return false;
      });
    }
    if(fontsReady==1) $('body').css('visibility','visible');
    else {
      pageReady=1;
    }
}

function homeSlide() {
  if( $('#homeBuckets').hasClass("hover")) return false;
  else {
    var h = 270; 
    $(".slide").hide();
    $(".slide:eq(0)").show();
    $('.slide:eq(1)').css('top', h + 'px').show().css('opacity',0);
    $(".slide:eq(0)").animate({ 
        opacity: 0,
    }, 500, function() {
      $(".slide:eq(0)").hide();
    });
    $(".slide:eq(1)").animate({ 
        top:  "110px",
        opacity:  1,
    },  900, function() {
     $('#homeBuckets').append($('.slide:eq(0)'));
    });
    if($('.resizable-background img').length<2) $('.resizable-background img').clone().appendTo('.resizable-background');
    $('.resizable-background img:eq(0)').attr('src', $(".slide:eq(1) img").attr('src'));
    $('.resizable-background').append( $('.resizable-background img:eq(0)'));
    $('.resizable-background img:eq(1)').css('opacity', 0);
    $('.resizable-background img:eq(1)').animate({
      opacity: 1
    }, 700, function() {
      // Animation complete.
    });
  }
}

function accordion() {
        if($('body').hasClass('cmsEditing')) return false;
        if(!$('.content').hasClass('careers') && !$('.content').hasClass('property-inner-page')) {
          $('.billow:eq(0)').show();
          $('.accordion:eq(0) h3:eq(0)').addClass('on');
        }
        $('.accordion h3').click(function() {
            $('.accordion h3').removeClass('on');
            $('.billow').slideUp(500);
            if ($(this).next().is(':hidden') == true) {
                $(this).addClass('on');
                $(this).next().slideDown(450);
            }
        });
}

function showNews() {
        $('.year').hide();
        $('.tabs-holder.news').html('');
        $('.year:eq(0)').show();
        var year = $('.year:eq(0)').attr('id').substr(5);
        $('.year').each(function(){
            var separator = '';
            if($(this).index() !=  ($('.year').length-1)) separator = '</a> <ins> | </ins></li>';
            $('.tabs-holder.news').append('<li><a>'+ $(this).attr('id').substr(5) + separator);
        });
        $('.tabs-holder.news li a').unbind();
        $('.tabs-holder.news li a').click(function() {
           $('.year').hide();
           var y = $(this).index('.tabs-holder.news li a');
           $('.year:eq('+ y +')').show();
           $('.tabs-holder.news li a').removeClass('active');
           $('.tabs-holder.news li:eq('+ y +') a').addClass('active');
           if(!$('.year:eq('+ y +') .billow:visible').hasClass('billow')) {
              $('.year:eq('+ y +') h3:eq(0)').addClass('on');
              $('.year:eq('+ y +') .billow:eq(0)').show();
           }
        });
        $('.tabs-holder.news li:eq(0) a').addClass('active');
        $('.next-arrow').hover(
              function () {
                var n = $('.tabs-holder.news li a.active').parent().next().find('a').text();
                if(!n) n = $('.tabs-holder.news li:eq(0) a').text();
                $('.next-arrow strong').html(n);
                $('.next-arrow').unbind('click');
                $('.next-arrow').click(function () {
                   var n = $('.tabs-holder.news li a.active').parent().next().find('a').text();
                   if(n) $('.tabs-holder.news li a.active').parent().next().find('a').click();
                   else $('.tabs-holder.news li:eq(0) a').click();
                   var n = $('.tabs-holder.news li a.active').parent().next().find('a').text();
                   if(!n) n = $('.tabs-holder.news li:eq(0) a').text();            
                   $('.next-arrow strong').html(n);
                });
              });
}

function showContact() {
  $('.selectOptions a').hover(function(){
      $('.selectOptions:eq(0)').addClass('over');
      $('.optionList').slideDown(420);
    },
    function () {
      $('.selectOptions:eq(0)').removeClass('over');
      optionSearchTimer[0] = setTimeout('optionsSlideUp(0)',600);
    }
   );
  $('.optionList').hover(function(){
      $('.selectOptions:eq(0)').addClass('over');
    },
    function () {
      $('.selectOptions:eq(0)').removeClass('over');
      optionSearchTimer[0] = setTimeout('optionsSlideUp(0)',600);
    }
   );
}

function showServiceRequest() {
  $('.selectOptions a').hover(function(){
      $('.selectOptions:eq(0)').addClass('over');

      $(this).removeClass('error');

      $('.optionList').slideDown(420);
    },
    function () {
      $('.selectOptions:eq(0)').removeClass('over');
      optionSearchTimer[0] = setTimeout('optionsSlideUp(0)',600);
    }
   );
  $('.optionList').hover(function(){
      $('.selectOptions:eq(0)').addClass('over');
    },
    function () {
      $('.selectOptions:eq(0)').removeClass('over');
      optionSearchTimer[0] = setTimeout('optionsSlideUp(0)',600);
    }
   );
  $('.optionList a').click(function(){
      $('#inputProperty').val( $(this).text() );
      $('.selectOptions a:eq(0)').html( $(this).text() );
      $('.selectOptions:eq(0)').removeClass('over');
      optionsSlideUp(0);
  });

}

function showPropertySearch() {


  $('.map-img').maphilight();


  $('.selectOptions a').hover(function(){
      var e = $(this).parent().parent().find('.optionList').index('.optionList');
      $('.selectOptions:eq('+ e +')').addClass('over');
      $('.optionList:eq('+ e +')').slideDown(420);
    },
    function () {
      var e = $(this).parent().parent().find('.optionList').index('.optionList');
      $('.selectOptions:eq('+ e +')').removeClass('over');
      optionSearchTimer['+e+'] = setTimeout('optionsSlideUp('+e+')',600);
    }
   );
  $('.optionList').hover(function(){
      $(this).addClass('over');
    },
    function () {
      var e = $(this).index('.optionList');
      $(this).removeClass('over');
      optionSearchTimer[e] = setTimeout('optionsSlideUp('+e+')',600);
    }
   );
}

function showCareers() {
  $('.selectOptions a').hover(function(){
      $('.selectOptions').addClass('over');
      $('.optionList').slideDown(420);
    },
    function () {
      $('.selectOptions').removeClass('over');
      optionSearchTimer[0] = setTimeout('optionsSlideUp(0)',600);
    }
   );
  $('.optionList').hover(function(){
      $('.selectOptions').addClass('over');
    },
    function () {
      $('.selectOptions').removeClass('over');
      optionSearchTimer[0] = setTimeout('optionsSlideUp(0)',600);
    }
   );
   $('.careers-accordion>div').each(function(){
    var s = $(this).attr('class');
    if(!$('.optionList .'+s).hasClass(s)) $('.optionList').append('<a class="'+s+'">'+s.replace('_',' ')+'</a>');
   });
   $('.optionList a').sort(sortAlpha).appendTo('.optionList');
   $('.optionList').prepend('<a class="">All States</a>');
   $('.optionList a').click(function(){
     $('.careers-accordion .on').removeClass('on');
     $('.careers-accordion .billow').hide();
     $('.careers-accordion>div').hide();
     if($(this).attr('class')) $('.careers-accordion>div.' + $(this).attr('class') ).show();
     else $('.careers-accordion>div').show();
     $('.selectOptions a').html($(this).html());
     $('.optionList').hide();
    });
}

function optionsSlideUp(e) {
    clearTimeout(optionSearchTimer[e]);
    if(!$('.selectOptions:eq('+e+')').hasClass('over') && !$('.optionList:eq('+e+')').hasClass('over') ) $('.optionList:eq('+e+')').slideUp(330);
    else optionSearchTimer[e] = setTimeout('optionsSlideUp('+e+')',600);
}

function sortAlpha(a,b){  
    return a.innerHTML.toLowerCase() > b.innerHTML.toLowerCase() ? 1 : -1;  
}

var autoComplete;
function autoKeywords() {
 if (autoComplete) autoComplete.abort();
 autoComplete = $.get('autocomplete?q=' + escape($('#keyword').val()), function (data) {
  $('#propertySearch span').remove();
  if(data) {
   $('#propertySearch').append('<span>'+data+'</span>');
   $('#propertySearch').unbind();
   $('#propertySearch').hover(function () {
    //
   },function () {
    $('#propertySearch span').fadeOut(300, function () {
     $('#propertySearch span').remove();
    });
   });
  }
 });
}


function cachePages() {
     if($('.load a:eq(0)').attr('href')) {
       $('.load').append('<iframe src="'+ $('.load a:eq(0)').attr('href') + '" onload="cachePages()"></iframe>');
       $('.load a[href]:eq(0)').remove();
     }
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

$(document).ready(function() {
  setTimeout(function(){
    if ( readCookie('SiteEdit') || window.location.search.substr(0, 5) == '?edit') {
      if(readCookie('SiteEdit')) $('body').addClass('cmsEditing');
      var css = document.createElement('link');
      css.setAttribute("rel", "stylesheet");
      css.setAttribute("type", "text/css");
      css.setAttribute("href", '/cms/assets/cms.css');
      $('head').append(css);
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = '/cms/assets/cms.js';
      $('head').append(script);
    }
  },400);
});