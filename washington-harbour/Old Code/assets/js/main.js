$(document).ready(function() {

    // mobile site nav open/close
    $('#nav').click(function() {
        $(this).toggleClass('open');
    });
    
    // show more/less retailers            
    $('.more-retailers-btn').click(function() {
       var buttonName = $(this).text();
       if(!$(this).hasClass('up')) {
            $('.retailers-b').slideDown(.500);
            $(this).addClass('up');
            $(this).html($(this).html().replace(buttonName,'Show less'));
       } else {
            $('.retailers-b').slideUp('slow');
            $(this).removeClass('up');
            $(this).html($(this).html().replace(buttonName,'More retailers'));
            $('html,body').animate({
                scrollTop: $('.retailers-section').offset().top
            }, 2000);
        }
    });

    //  Image map
    $('img[usemap]').rwdImageMaps();
    
    $('area').mouseover(function() {
        $( '.map-box').removeClass('tooltipstered');
        $( '.map-box').addClass('tooltipstered');
        
      })
      .mouseout(function() {
        $( ".map-box").removeClass('tooltipstered');
      });

    // Carousel
    $("#owl-demo").owlCarousel({
        jsonPath : "/assets/js/json/eventsdata.json" 
    });

    // gallery
    function runGallery() {
        $('#slides').superslides({
            slide_easing: 'easeInOutCubic',
            slide_speed: 800,
            pagination: true
        });
    }
    
    runGallery();

    // change the map tooltip size for mobile device
    if ($(window).width() > 500) {
        $('area').each(function(tooltipster) {
            $(this).tooltipster({
                content: $('<img src="' + $(this).attr("rel") + '"><br>' + '<p><b>' + $(this).attr("title") + '</p>'),
                // setting a same value to minWidth and maxWidth will result in a fixed width
                minWidth: 190,
                maxWidth: 240,
                position: 'left' 
            });
        });
    }
    else {
        $('area').each(function(tooltipster) {
            $(this).tooltipster({
                content: $('<img src="' + $(this).attr("rel") + '"><br>' + '<p><b>' + $(this).attr("title") + '</p>'),
                minWidth: 130,
                maxWidth: 95,
                position: 'right'
            });   
        });
    }

    // Retailers list hover state
    $('area').each(function() {
        $(this).on('mouseenter', function() {
            var selectedRetailer = $(this).attr('class');
            // var retailerlistItem = $('.image-map-list li a').attr('class');
            var $retailerlistItem = $('.image-map-list li.' + selectedRetailer);

            if(!!selectedRetailer) {
                $('.image-map-list li a').removeClass('selected');
                $retailerlistItem.addClass('selected');  
            }
            // if(selectedRetailer = retailerlistItem) {
            // // if($('.image-map-list li a').hasClass(selectedRetailer)) {
            //         // console.log(selectedRetailer);
            //         $('.image-map-list li' + 'a.' + selectedRetailer).addClass('selected');
            // } else {
            //     $('.image-map-list li a').removeClass('selected');
            // }
        })
        .on('mouseleave', function() {
            $('.image-map-list li a').removeClass('selected');
        });
    });

    // Interactive map active tab
    $('.tabs-two').click(function() {
        $('.tabs-two').removeClass('active-tab');
        $(this).addClass('active-tab');
        $('#imagemap-wrapper .image-map').css('max-height','100%');
        
        if($('.active-tab').hasClass('up')) {
            $('.map-container').css('display','none');
            $('.upper-container').css('display','block');
        } else if($('.active-tab').hasClass('lp')) {
            $('.map-container').css('display','none');
            $('.lower-container').css('display','block');
            $('.lower-container').css('visibility','visible');
        }
    });
    
    // Interactive map active tab
    $('.tabs-two').click(function() {
        $('.tabs-two').removeClass('active-tab');
        $(this).addClass('active-tab');
    });


    // form section slide up/down
    $('.open-form').click(function() {
        var headerText;
        if($(this).hasClass('contact-iceskating')) {
            $('.formselect').attr('value','ice');
            $('.form-section').slideUp(1);
            $('.form-section').slideDown(1500);
            $('.contact-header').text('');
            headerText = 'Ice skating';

        } else if($(this).hasClass('contact-leasing')) {
            $('.formselect').attr('value','lease');
            $('.form-section').slideUp(1);
            $('.form-section').slideDown(1500);
            $('.contact-header').text('');
            headerText = 'Leasing';
        }
        $('.contact-header').delay(3500).text(headerText);
        $('html,body').animate({
            scrollTop: $('section.contact-section').offset().top
        }, 1500);
    });

});

$(window).bind("load", function() {

    // loader
    $('.home-gallery').delay(2500).css('visibility','visible');
    $('#header').delay(2500).css('visibility','visible');
    $('.form-section').delay(2500).css('visibility','visible');

    // retailer interactive map show/hide      
    $('#imagemap-wrapper').slideUp(50);
    $('.interactivemap-head').removeClass('up');

    // Scroll to sections
    $('.dir').click(function() {
        $('#imagemap-wrapper').slideDown(500);
        $('.interactivemap-head').addClass('up');
        $('.interactivemap-head .interactivebtn-wraper span').fadeOut(1000);
        $('html,body').animate({
            scrollTop: $('#directory-start').offset().top
            }, 2000);
    });

    $('.go-top').on('click',function(){
        $('html,body').animate({ scrollTop: 0 }, 1400, function () {
        });
    });

    $('.explore').on('click',function(){
        $('html,body').animate({
            scrollTop: $('#washington-harbour').offset().top
            }, 2000);
    });

    $('#events-lnk').on('click',function(){
        $('html,body').animate({
            scrollTop: $('.upcoming-events').offset().top
            }, 2000);
    });

    // scroll to events
    if (window.location.href.indexOf('?events') > -1) {
        $('html,body').animate({
            scrollTop: $('#events').offset().top
        }, 2000);
    }

    // scroll to thank you message --- show / hide thank you message
    if (window.location.href.indexOf('?thank-you') > -1) {
        $('.message-wrapper h3').css('max-height','100px');
        $('.message-wrapper h3').css('visibility','visible');
        $('.message-wrapper h3').slideDown('slow');
        $('html,body').animate({
            scrollTop: $('.thank-you-section').offset().top
        }, 2000);
    }
    
    setTimeout(function() {
        $('.thank-you-section').slideUp(1000);
    }, 10000);

    // Interactive map show/hide function
    function openMap () {
        $('.interactivemap-head').click(function() {
            if(!$(this).hasClass('up')) {
                $('#imagemap-wrapper').slideDown(900);
                $(this).addClass('up');
                $('.interactivemap-head .interactivebtn-wraper span').fadeOut(1000);
            } else {
                $('#imagemap-wrapper').slideUp(1200);
                $(this).removeClass('up');
                $('.interactivemap-head .interactivebtn-wraper span').fadeIn(1000);
            }
        });
    }

    openMap ();

    // bxslider
    var tempA = $('body').hasClass('template-a');
    var tempAB = $('body').hasClass('template-a-b');
    if(!!tempA || !! tempAB) {
        $('.bxslider').bxSlider();
    }

    // Events -- more/less 
    $('.items-wrapper').addClass('not-clicked');
    $('.items-wrapper p span.more-less').each(function () {
        $(this).click(function() {
            $(this).parents().eq(0).find('.more-less').css('display','none');
            $(this).parents().eq(0).find('.extended').css('display','block');
            $(this).parents().eq(0).find('.read-less').css('display','block');
            $('#events #owl-demo').css('min-height','560px');
            $(this).parents().eq(1).removeClass('clicked');
            $(this).parents().eq(1).removeClass('not-clicked');
            $(this).parents().eq(1).addClass('clicked');
        });
        $(this).parents().eq(0).find('.read-less').click(function() {
            $(this).parents().eq(0).find('.more-less').css('display','inline-block');
            $(this).parents().eq(0).find('.extended').css('display','none');
            $(this).parents().eq(0).find('.read-less').css('display','none');
        });
    });

});

$(document).scroll(function() {
    // Top nav fade in/out
    var y = $('body').scrollTop();
    var ffy = $('html, body').scrollTop();
    if (y > 108 || ffy > 108 ) {
        $('.homepage #header').addClass('header-bg');
        $('.homepage .header-wrapper').fadeIn(1100);
    } else {
        $('.homepage #header').removeClass('header-bg');
        $('.homepage .header-wrapper').fadeOut(1600);
    }
});