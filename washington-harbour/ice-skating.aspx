﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ice-skating.aspx.vb" Inherits="ice_skating" %>
<%@ Import Namespace="System.IO" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html class="no-js" ng-app="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>The Washington Harbour | Ice Skating Information</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="http://fonts.googleapis.com/css?family=Michroma" rel="stylesheet" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="/assets/css/tooltipster.css">
        <link rel="stylesheet" href="/assets/css/superslides.css">

        <link href="/assets/css/jquery.bxslider.css" rel="stylesheet" />
        <link rel="stylesheet/less" type="text/css" href="/assets/css/style.less">

        <script src="/assets/js/jquery-1.11.1.min.js"></script>
        <script src="/assets/js/jquery.bxslider.min.js"></script>
        <script src="/assets/js/angular.min.js"></script>
        <link type="text/css" rel="stylesheet" href="assets/css/style.css"/> 
       <!-- <script src="assets/js/jq.js" type="text/javascript"></script> -->
        <script src="assets/js/custom.js" type="text/javascript"></script>  
        
    </head>
    <body class="template-a template-a-b skating" id="skating">
        <header id="header" ng-include src="'/nav.html'"></header>

        <section id="template-a-head" class="text-sections view-directory">
            <div class="title-two">The Washington Harbour Ice Rink</div>
            <div class="separator"></div>
            <div class="text-section">
                <p class="cms cmsType_TextMulti cmsName_IceSkating_Page_FirstParagraph"></p>
            </div>
        </section>

        <section class="temp-a-gallery">
            <div class="bx-wrapperer">
                <ul class="bxslider">
                    <li><img src="/assets/img/gallery/ice_skating/iceskating_img_02.jpg"/></li>
                    <li><img src="/assets/img/gallery/ice_skating/iceskating_img_03.jpg"/></li>
                    <li><img src="/assets/img/gallery/ice_skating/iceskating_img_04.jpg"/></li>
                    <li><img src="/assets/img/gallery/ice_skating/iceskating_img_01.jpg"/></li>
                </ul>
            </div>
        </section>
 
        <section class="information ice">
            <div class="left-container info-containers">
                <h2>The Washington Harbour Ice Rink</h2>
                <p class="cms cmsType_Rich cmsName_IceSkating_Page_IceRink"></p>
            
                <h2>Learn to Skate</h2>
                <p class="cms cmsType_Rich cmsName_IceSkating_Page_LearnToSkate"></p>
                    
                <h2>PARTY ON ICE</h2>
                <p class="cms cmsType_Rich cmsName_IceSkating_Page_PartyOnIce"></p>     
                
            </div>
            
            <div class="right-container info-containers">
                
            <h2>Contact Us</h2>
            <p class="cms cmsType_Rich cmsName_IceSkating_Page_ContactUs"></p>

            <h2>Price</h2>
                <p class="cms cmsType_Rich cmsName_IceSkating_Page_Price"></p>
                
                
                <h2>Hours of Operation</h2>
                <p class="cms cmsType_Rich cmsName_IceSkating_Page_Operation"></p>
                   
            </div>            
        </section>   
        
        <section id="directory-start" class="text-sections interactivemap-head interactivemap-btn up">
            <div class="title-two">Directory</div>
            <div class="interactivebtn-wraper"><span class="interactivemap-btn"></span></div>
            <div class="text-section">
                <p></p>
            </div>
        </section>
        
        <section id="imagemap-wrapper" ng-include src="'/directory.html'"></section>

        <footer ng-include src="'/footer.html'"></footer>

        <script src="/assets/js/jquery.easing.1.3.js"></script>
        <script src="/assets/js/jquery.animate-enhanced.min.js"></script>
        <script src="/assets/js/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>

        <script src="/assets/js/owl.carousel.js"></script>
        <script src="/assets/js/jquery.rwdImageMaps.min.js"></script>
        <script src="/assets/js/jquery.tooltipster.js"></script>
        <script src="/assets/js/less.js" type="text/javascript"></script>
        <script src="/assets/js/main.js"></script>
        <script>
            // Google Analytics
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l; b[l] || (b[l] =
            function () { (b[l].q = b[l].q || []).push(arguments) }); b[l].l = +new Date;
                e = o.createElement(i); r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            } (window, document, 'script', 'ga'));
            ga('create', 'UA-57349170-1'); ga('send', 'pageview');
        </script>
    </body>
</html>