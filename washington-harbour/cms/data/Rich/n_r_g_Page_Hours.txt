<table>
<tbody>
<tr><th align="left">Hours of operation</th></tr>
<tr>
<td>
<table>
<tbody>
<tr>
<td align="left">Monday-Thursday</td>
<td align="right">11:30am - 1:30am</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<table>
<tbody>
<tr>
<td align="left">Friday-Saturday</td>
<td align="right">11:30am - 2:30am</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<table>
<tbody>
<tr>
<td align="left">Sunday</td>
<td align="right">11:30am - 1:30am</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>