var timeOriginal = new Date().getTime();

function tinyMCEinit () {

  var script = document.createElement( 'script' );
  script.type = 'text/javascript';
  script.src = 'tiny_mce/jquery.tinymce.js';
  $('head').append(script);

 $('textarea.cmsRich').tinymce({
     script_url: 'tiny_mce/tiny_mce.js',
     plugins: "safari,inlinepopups,contextmenu,noneditable,nonbreaking,xhtmlxtras,template,advlist",
     theme: "advanced",
     convert_urls: 0,
     content_css : "/assets/css/rich.css",
     theme_advanced_buttons1: "formatselect,fontsizeselect,forecolor,backcolor,|,removeformat",
     theme_advanced_buttons2: "justifyleft,justifycenter,justifyright,justifyfull",
     theme_advanced_buttons3: "bold,italic,underline,strikethrough,|,sub,sup",
     theme_advanced_buttons4: "bullist,numlist,|,charmap,|,link,unlink,|,code",
     paste_text_sticky : true,
     paste_text_sticky_default : true,
     theme_advanced_toolbar_align: "center",
     theme_advanced_statusbar_location: "none",
     theme_advanced_resizing: true,
     forced_root_block: false,
     force_p_newlines: false,
     force_br_newlines: true,
     language: "en",
     oninit : "fixPaste"
 });

}

var fixPasteTimer;
var fixPasteEvil = new Array('class=','id=','z-index','display:','face=','0in','Mso', 'ALIGN=', 'LsdEx','SemiHidden','o:Of','o:Allow','w:Word','w:View',':Zoom','gte m','webkit','web-kit','QFormat','<m:','</xml','[endif]','<styl');

function fixPaste () {
 if(fixPasteTimer) clearTimeout(fixPasteTimer);
 $('textarea.cmsRich').each(function() {
  if(!$(this).attr('data-tagsCount')) $(this).attr('data-tagsCount', 0);
  var t = '0';
  var tmp = $(this).val().replace(/<br[^>]*>/gi, '').match(/\>/g);
  if(tmp) t = Math.floor(String(tmp.length)/2);
  if ( (new Date().getTime() - timeOriginal) > 3000 && t> Math.round($(this).attr('data-tagsCount'))+10) $(this).val(stripTags($(this).val()));
  else if (numberGap(t, Math.round($(this).attr('data-tagsCount')),2) && inArray($(this).val(),fixPasteEvil)) $(this).val(stripTags($(this).val()));
  else if (numberGap(Math.round($(this).val().length), Math.round($(this).attr('data-chars')),70) && inArray($(this).val(),fixPasteEvil)) $(this).val(stripTags($(this).val()));
  else if (numberGap(contentCount($(this).val()), Math.round($(this).attr('data-plainChars')),30) && inArray($(this).val(),fixPasteEvil)) $(this).val(stripTags($(this).val()));
  $(this).attr('data-tagsCount', t)
  $(this).attr('data-chars', $(this).val().length);
  $(this).attr('data-plainChars', contentCount($(this).val()));

 });
 fixPasteTimer = setTimeout('fixPaste()', 130); 
}

function inArray(str,ar){
  for(var i=0,k=ar.length;i<k;i++) {
    if(str.indexOf(ar[i])!=-1) return true;
  }
  return false;
}

function stripTags (html){
 if(!html) return '';
 html = html.replace(/<br[^>]*>/gi, '\n');
 html = html.replace(/\<\/p\>/gi, '</p>\n\n');
 html = html.replace(/\<\/div\>/gi, '</div>\n');
 html = html.replace(/\>/g, '> ');
 var tmp = document.createElement('DIV');
 tmp.innerHTML = html;
 tmp = tmp.textContent || tmp.innerText;
 tmp = tmp.replace(/\n/g, '<br />');
 return tmp;
}

function contentCount (html){
 if(!html) return 0;
 var tmp = document.createElement('DIV');
 tmp.innerHTML = html;
 tmp = tmp.textContent || tmp.innerText;
 return tmp.length;
}

function numberGap (a,b,gap) {
 if(a && b && gap && (a-b) >=gap || (b-a) >=gap) return true;
 return false;
}