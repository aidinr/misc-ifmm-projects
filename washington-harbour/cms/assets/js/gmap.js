// home page google maps

function initialize() {

    var myStyles = [
        {
            featureType: 'poi', 
            elementType: 'all', 
            stylers: [ 
                { hue: '#68b897' }, 
                { saturation: -16 }, 
                { lightness: -28 }, 
                { visibility: 'off' } 
            ]
        },{
            featureType: 'road.highway',
            elementType: 'all',
            stylers: [
                { hue: '#ffffff' },
                { saturation: -100 },
                { lightness: 100 },
                { visibility: 'on' }
            ]
        },{
            featureType: 'water',
            elementType: 'all',
            stylers: [
                { hue: '#0091e5' },
                { saturation: 100 },
                { lightness: -41 },
                { visibility: 'on' }
            ]
        },{
            featureType: 'landscape',
            elementType: 'all',
            stylers: [
                { hue: '#e3e3e3' },
                { saturation: -100 },
                { lightness: 0 },
                { visibility: 'on' }
            ]
        },{
            featureType: 'landscape',
            elementType: 'all',
            stylers: [
                { hue: '#e3e3e3' },
                { saturation: -100 },
                { lightness: 0 },
                { visibility: 'off' }
            ]
        },{
            featureType: 'landscape.man_made',
            elementType: 'all',
            stylers: [
                { hue: '#e3e3e3' },
                { saturation: -100 },
                { lightness: 0 },
                { visibility: 'on' }
            ]
        },{
            featureType: 'road.local',
            elementType: 'all',
            stylers: [
                { hue: '#ffffff' },
                { saturation: -100 },
                { lightness: 100 },
                { visibility: 'on' }
            ]
        },
        
        {
            featureType: 'poi.park',
            elementType: 'all',
            stylers: [
                { hue: '#68b897' },
                { saturation: -16 },
                { lightness: -28 },
                { visibility: 'on' }
            ]
        }
    ];


    var mapOptions = {
        zoom: 16,
        panControl: false,
        zoomControl: false,
        scaleControl: false,
        scrollwheel: false,
        center: new google.maps.LatLng(38.9018, -77.0601),
        // mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: myStyles
    }
    var map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

    var image = '/assets/img/wh-maps-icon.png';
    var myLatLng = new google.maps.LatLng(38.9018, -77.0601);
    var beachMarker = new google.maps.Marker({
        position: myLatLng,
        
        map: map,
        icon: image,
        title:"THE WASHINGTON HARBOUR"
    });
}
google.maps.event.addDomListener(window, 'load', initialize);

