angular.module('myngApp', ['pickadate']).controller('myController', function($scope) {

    function getEventdata () {
        var url = 'http://www.thewashingtonharbour.com/idamfinder/geteventlist_JSONP.aspx';
        url = url + '?width=260&year=' + $scope.selectedYear;

        $.ajax({
            type: 'GET',
            url: url,
            async: false,
            jsonpCallback: 'jsonp_event_list_items',
            contentType: 'application/json',
            dataType: 'jsonp',
            success: function(result) {

                var
                    theList = [];
                theList = myFilter(result.rows, 'event_date', currentMonth);
                dayClicked();                
                loopData(theList, 'value');

                var r = $('.pickadate-today').text();
                if ( r > 1) {
                    checkTodayEvents ();
                }           

            },
            error: function(e) {
                console.log('error');
            }

        });

        function myFilter (items, attr, value) {
            var dataList = [];

            for (var item in items) {
                var 
                    dt = new Date(items[item].value[attr]),
                dtMonth = dt.getMonth(),
                check = checkRecurringEvent(items[item].value.UDFs),
                activeEvent = checkPullEvent(items[item].value);

                if(!!activeEvent) {
                    if (dtMonth + 1 === value || !!check) {    
                      dataList.push(items[item]); 
                    }
                }
            }
            return dataList;
        }

        function checkRecurringEvent (items) {
            
            for (var item in items) {
                if (items[item].name == 'Recurring Event') {
                    if (items[item].value == '1') {
                        return true;
                    }
                }
            }

            return false;
        }

        function checkPullEvent (item) {
            var 
                whatDay = $('.pickadate-today').text(),
            whatDate = $scope.getMonth + '/1/',
            whichYear = ($scope.getYear - 100) + 2000,
            whatDate = whatDate + '/' + whichYear,
            pullDate = new Date(item.pull_date).getTime(),
            actveDate = new Date(whatDate).getTime();
            if(actveDate < pullDate) {
                return true;
            }                  
            return false;
        }

        function checkTodayEvents() {
            if ($('.pickadate-enabled').hasClass('pickadate-today')) {
                $('ul.box li').each(function() {                        
                    var 
                        ourActiveDay = $('.pickadate-today').text(),
                    months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
                    getTheMonth = months[$scope.getMonth - 1],
                    whatDateActive = getTheMonth + ' ' + ourActiveDay + ' ' + $scope.selectedYear,
                    whatDayActive = new Date(whatDateActive).getDay(),
                    dayActiveTime = new Date(whatDateActive).getTime(),
                    weekdays = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"],
                    activeDayName = weekdays[whatDayActive],
                    activeList = [],
                    eventRuns = $(this).attr('data-day'),
                    eventRunsDay = new Date(eventRuns).getDay(),
                    pullOurData = $(this).attr('data-pull'),
                    pullTime = new Date(pullOurData).getTime();
                    
                    if (dayActiveTime > pullTime) {
                        activeList.push(whatDayActive);
                        console.log(eventRuns);
                        $(this).addClass('inactive');
                        $('ul.box li').each(function(){
                            if(activeDayName = eventRuns) {
                                $(this).addClass('inactive');

                            } else {
                                $(this).removeClass('inactive');
                                emptyMsgShow ();
                            } 
                        });
                    } else {
                        $(this).removeClass('inactive');
                        emptyMsgShow ();
                    }
                });
                
            } else {
                // emptyMsgShow ();
            }


            function emptyMsgShow () {
                $('ul.box li').each(function() { 
                    if ($(this).hasClass('inactive')) {
                        $('div.empty-msg').slideDown('fast');
                    } else {
                        $('div.empty-msg').slideUp('fast');
                    }
                });
            }
        }

        function checkWeekday (items) {
            for (var item in items) {
                var getDayVal = items[item].value;
                var getDayName = items[item].name;
                if (getDayName == '1. Monday') {
                    if (getVal.length > 0) {

                        return getDayName;
                    }
                }                 
            }

            return false;
        }

        function loopData(items, attr) {
            $.each(items, function () {
                $('.box').prepend(buildEvent(this[attr]));
            });
            
        }

        function buildEvent(obj) {
            var
                tmp = '',
                days = [],
                day = '',
                weekdays = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];

            for(var item in obj.UDFs) {
                if (obj.UDFs[item].value != ''){
                    day = obj.UDFs[item].name.replace(/[0-9]/g, '').replace('.', '').trim().toLowerCase();
                    if ($.inArray(day,weekdays) >= 0 ) {
                        days.push(day);
                        
                    }
                }
            }

            tmp = '<li data-day="' + days.join('-') + '"' + 'data-pull="' + obj.pull_date + '"' + 'data-active="">';

            tmp += '<div class="img-container">';

            if(!!obj.Assets) {
                if(!!obj.Assets[0].ViewURL) {
                    tmp += '<div class="obj-tag"><object data="' + obj.Assets[0].ViewURL +'"><img scr="' + obj.Assets[0].ViewURL +'"></object></div>';
                }
            } else {
                tmp += '<img src="/assets/img/events/images/event_default.jpg">';
            }

            tmp += '</div>';
            tmp += '<div class="details"><h2>' + obj.headline + '</h2>';
            tmp += '<h3>' + obj.event_date + '</h3>';

            $.each(obj.UDFs, function () {
                if(this.name.toLowerCase() ==='event time') {
                    tmp += '<p>' + this.value + '</p>';
                }

            });
            $.each(obj.UDFs, function () {
                if(this.name.toLowerCase() === 'event note') {
                    tmp += '<p><em>' + this.value + '</em></p>';
                }
            });

            tmp += '<p><span>' + obj.content + '</span></p>';
            tmp += '</div>';
            tmp += '</li>';
            return tmp;
        }
    }

    function clearEventdata () {
        $('.box').empty();
    }

    var currentMonth, currentYear;

    $scope.date = new Date();
    $scope.getMonth = $scope.date.getMonth()+1;
    $scope.getYear = $scope.date.getYear();
    $scope.selectedYear = ($scope.getYear - 100) + 2000;
    currentMonth = $scope.getMonth;

    function changeMonth() {
        $('.pickadate-next').click(function() {
            if ($scope.getMonth < 12) {
                $scope.getMonth = $scope.getMonth + 1;
                $scope.selectedYear = $scope.selectedYear;
            } else {
                $scope.getMonth = ($scope.getMonth - 12) + 1;
                $scope.selectedYear = $scope.selectedYear + 1;
            }

            currentMonth = $scope.getMonth;
            currentYear = $scope.selectedYear;
            clearEventdata();
            getEventdata();
        });

        $('.pickadate-prev').click(function() {
            if ($scope.getMonth < 12 && $scope.getMonth > 1) {
                $scope.getMonth = $scope.getMonth - 1;
                $scope.selectedYear = $scope.selectedYear;

            } else if ($scope.getMonth === 1) {
                $scope.getMonth = ($scope.getMonth + 12) - 1;
                $scope.selectedYear = $scope.selectedYear - 1;
            } else {
                $scope.getMonth = ($scope.getMonth) - 1;
                $scope.selectedYear = $scope.selectedYear;
            }

            currentMonth = $scope.getMonth;
            currentYear = $scope.selectedYear;
            clearEventdata();
            getEventdata();
        });
    }
    function removeExpired() {
        var 
            self,
        eventDay,
        activeList,
        months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
        getTheMonth = months[$scope.getMonth - 1],
        selectDay = $('.pickadate-active').text(),
        selectDate = getTheMonth + ' ' + selectDay + ' ' + $scope.selectedYear,
        weekdays = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"],
        dayName = '',
        fndDay = (new Date(selectDate)).getDay(),
        dayName = weekdays[fndDay],
        activeList = [],
        testArray = [],
        daysList =[];
        
        
        $('ul.box li').each (function() {
            self = $(this);
            eventDay = self.attr('data-day');
            expiryDate = self.attr('data-pull');
            selectedDateTime = new Date(selectDate).getTime();
            expiryDateTime = new Date (expiryDate).getTime();

            if ((eventDay === dayName) && (selectedDateTime < expiryDateTime)) {
                self.removeClass('inactive');
                activeList.push(dayName);
                testArray.push(eventDay);
            } else {
                self.addClass('inactive');
            }
        });

        if (activeList.length === 0) {
            $('div.empty-msg').slideDown('fast');
        } else {
            $('div.empty-msg').slideUp('fast');
        }
    }
    function emptyMsgShow () {
        
    }
    function dayClicked() {
        $('.pickadate-cell li').click(removeExpired);
    }
    clearEventdata();
    getEventdata();
    changeMonth();
});
