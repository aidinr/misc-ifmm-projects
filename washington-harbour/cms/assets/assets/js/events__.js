angular.module('myngApp', ['pickadate']).controller('myController', function($scope) {

    function getEventdata () {
        var url = 'http://www.thewashingtonharbour.com/idamfinder/geteventlist_JSONP.aspx';
        url = url + '?width=260&year=' + $scope.selectedYear;

        $.ajax({
            type: 'GET',
            url: url,
            async: false,
            jsonpCallback: 'jsonp_event_list_items',
            contentType: 'application/json',
            dataType: 'jsonp',
            success: function(result) {

            // console.dir(result.rows);

                var
                    theList = myFilter(result.rows, 'event_date', currentMonth);

                loopData(theList, 'value');

            },
            error: function(e) {
                console.log('error');
            }
        });

        function myFilter (items, attr, value) {
            var dataList = [];

            for (var item in items) {
                var d = new Date(items[item].value[attr]);
                var n = d.getMonth();
                var check = checkRecurringEvent(items[item].value.UDFs);
                var activeEvent = checkPullEvent(items[item].value);

                if(!!activeEvent) {
                    if (n + 1 === value || !!check) {    
                      dataList.push(items[item]); 
                    }
                }
            }
            return dataList;
        }

        function checkRecurringEvent (items) {
            
            for (var item in items) {
                if (items[item].name == 'Recurring Event') {
                    if (items[item].value == '1') {
                        return true;
                    }
                }
            }

            return false;
        }

        function checkPullEvent (item) {
            var pullDate = new Date(item.pull_date).getTime();
            var activeDate = $scope.date.getTime();
            // var activeDate = new Date().getTime();

            if(activeDate < pullDate) {
                return true;
            }                        
            return false;
        }

        function loopData(items, attr) {
            $.each(items, function () {
                $('.box').append(buildEvent(this[attr]));
            });
        }

        function buildEvent(obj) {
            var
                tmp = '<li>';

            tmp += '<div class="img-container">';
            if(!!obj.Assets) {
                if(!!obj.Assets[0].ViewURL) {
                    tmp += '<div class="obj-tag"><object data="' + obj.Assets[0].ViewURL +'"><img scr="' + obj.Assets[0].ViewURL +'"></object></div>';
                }
            } else {
                tmp += '<img src="/assets/img/events/images/event_default.jpg">';
            }

            tmp += '</div>';

            tmp += '<div class="details"><h2>' + obj.headline + '</h2>';
            tmp += '<h3>' + obj.event_date + '</h3>';

            $.each(obj.UDFs, function () {
                if(this.name.toLowerCase() ==='event time') {
                    tmp += '<p>' + this.value + '</p>';
                }

            });
            $.each(obj.UDFs, function () {
                if(this.name.toLowerCase() === 'event note') {
                    tmp += '<p><em>' + this.value + '</em></p>';
                }
            });

            tmp += '<p><span>' + obj.content + '</span></p>';

            tmp += '</div>';
            tmp += '</li>';
            return tmp;
        }
    }

    function clearEventdata () {
        $('.box').empty();
    }

    var currentMonth, currentYear;

    $scope.date = new Date();
    $scope.getMonth = $scope.date.getMonth()+1;
    $scope.getYear = $scope.date.getYear();
    $scope.selectedYear = ($scope.getYear - 100) + 2000;

    currentMonth = $scope.getMonth;
    clearEventdata ();
    getEventdata();

    $('.pickadate-next').click(function() {
    if ($scope.getMonth < 12) {
        $scope.getMonth = $scope.getMonth + 1;
        $scope.selectedYear = $scope.selectedYear;
    } else {
        $scope.getMonth = ($scope.getMonth - 12) + 1;
        $scope.selectedYear = $scope.selectedYear + 1;
    }

    currentMonth = $scope.getMonth;
    currentYear = $scope.selectedYear;
    clearEventdata ();
    getEventdata();
    });
    $('.pickadate-prev').click(function() {
    if ($scope.getMonth < 12 && $scope.getMonth > 1) {
        $scope.getMonth = $scope.getMonth - 1;
        $scope.selectedYear = $scope.selectedYear;

    } else if ($scope.getMonth === 1) {
        $scope.getMonth = ($scope.getMonth + 12) - 1;
        $scope.selectedYear = $scope.selectedYear - 1;
    }
     else {
        $scope.getMonth = ($scope.getMonth) - 1;
        $scope.selectedYear = $scope.selectedYear;
    }

    currentMonth = $scope.getMonth;
    currentYear = $scope.selectedYear;
    clearEventdata ();
    getEventdata();
    });
    
   
});


