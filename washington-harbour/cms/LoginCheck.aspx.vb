Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Security.Cryptography
Imports System.IO

Partial Class _logincheck
    Inherits System.Web.UI.Page

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim theFile As String = ""
        Dim CookieString As String
        Dim CookieDate As String
        Dim username As String = ""
        Dim password As String = ""

        If Not Request.Form("username") Is Nothing Then
            username = Request.Form("username")
        End If
        If Not Request.Form("password") Is Nothing Then
            password = Request.Form("password")
        End If

        Dim out As String = ""
        If username = "" Or password = "" Then
            out = 0
        Else
            Dim SqlLogIn As String = "select * from IPM_USER where Active = 'Y' and ltrim(rtrim(Login)) = '" & username & "' and ltrim(rtrim(Password)) = '" & password & "'"
            Dim DTUsers As New DataTable
            DTUsers = mmfunctions.GetDataTable(SqlLogIn)
            If DTUsers.Rows.Count > 0 Then

                Dim RandomKey As String = CMSFunctions.GetRandomKey()
                Dim SiteEditCookie As New HttpCookie("SiteEdit")
                SiteEditCookie.Value = username & "/" & RandomKey
                SiteEditCookie.Expires = DateTime.Now.AddDays(1)
                Response.Cookies.Add(SiteEditCookie)

                If Not Directory.Exists(Server.MapPath("~") & "cms\data\Users\" & username) Then
                    Try
                        Directory.CreateDirectory(Server.MapPath("~") & "cms\data\Users\" & username)
                    Catch ex As Exception
                    End Try
                End If

                Dim SessionFile As String = Server.MapPath("~") & "cms\data\Users\" & username & "\" & RandomKey & ".dat"
                Dim SessionFileStream As FileStream = Nothing
                If (Not File.Exists(SessionFile)) Then
                    SessionFileStream = File.Create(SessionFile)
                End If
                out = 1
            Else
                out = 0
            End If
        End If

        Response.Clear()
        Response.Write(out)
        Response.Flush()
        Response.End()

    End Sub

End Class