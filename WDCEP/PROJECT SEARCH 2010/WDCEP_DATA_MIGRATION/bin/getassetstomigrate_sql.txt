﻿
select a.asset_id,a.name,c.name projectname from IPM_ASSET a,ipm_project c, 
(
SELECT     21498078 asset_id,a.Item_ID,a.item_name,a.item_tag, ISNULL(b.Item_Value, a.Item_Default_Value) AS value
FROM         IPM_ASSET_FIELD_DESC AS a INNER JOIN
                      IPM_ASSET_FIELD_VALUE AS b ON a.Item_ID = b.Item_ID
WHERE     (b.ASSET_ID = 21498078)
UNION
select   21498078 asset_id,a.Item_ID,a.item_name,a.item_tag,a.Item_Default_Value as value from IPM_ASSET_FIELD_DESC a where Item_ID not in (
SELECT     a.Item_ID
FROM         IPM_ASSET_FIELD_DESC AS a INNER JOIN
                      IPM_ASSET_FIELD_VALUE AS b ON a.Item_ID = b.Item_ID
WHERE     (b.ASSET_ID = 21498078)) and a.Item_Default_Value <> '' and isnull(a.Item_Default_Value,'') <> ''
)
as b where a.asset_id = b.asset_id and b.Item_Tag = 'IDAM_FILE_SYSTEM_PUSH_REPLICATION' and b.value = 1 and a.proc_status = 0 
and a.Available = 'Y' and a.ProjectID = c.projectid