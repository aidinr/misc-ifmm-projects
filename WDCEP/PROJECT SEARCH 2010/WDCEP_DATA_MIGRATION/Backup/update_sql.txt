﻿UPDATE    IPM_ASSET WITH (ROWLOCK) 
set Search_Tags = replace(replace(replace(replace(replace(replace(replace(v.search_text_new,'(',' '),')',' '),'/',' '),'--',' '),'_',' '),'|','_'),'  ',' ') 
FROM         (SELECT     a.Asset_ID, 'ASSETID ' + CAST(a.Asset_ID AS varchar(50)) 
                                              + ' ASSETID ' + u.FirstName + ' ' + u.LastName + ' FILENAME ' + REPLACE(LTRIM(RTRIM(ISNULL(a.FileName, ''))), '_', '|') 
                                              + ' FILENAME FILENAME ' + LTRIM(RTRIM(REPLACE(LTRIM(RTRIM(ISNULL(a.FileName, ''))) + ' FILENAME NAME ' + ' ' + ISNULL(a.assetname, '') 
                                              + ' NAME' + ' ' + ISNULL(x.projectnumber, '') + ' ' + ISNULL(LTRIM(RTRIM(x.pn1)), '') + ' ' + ISNULL(LTRIM(RTRIM(x.pn2)), '') 
                                              + ' ' + ISNULL(LTRIM(RTRIM(x.pn3)), '') + ' ' + ISNULL(LTRIM(RTRIM(x.pn4)), '') + ' ' + ISNULL(LTRIM(RTRIM(x.pn5)), '') + ' ' + ISNULL(LTRIM(RTRIM(x.pn6)), 
                                              '') + ' ' + ISNULL(LTRIM(RTRIM(x.pn7)), '') + ' ' + ISNULL(LTRIM(RTRIM(x.pn8)), '') + ' ' + ISNULL(LTRIM(RTRIM(x.pn)), '') + ' ' + ISNULL(LTRIM(RTRIM(a.cn1)), 
                                              '') + ' ' + ISNULL(LTRIM(RTRIM(a.cn2)), '') + ' ' + ISNULL(LTRIM(RTRIM(a.cn3)), '') + ' ' + ISNULL(LTRIM(RTRIM(a.cn4)), '') + ' ' + ISNULL(LTRIM(RTRIM(a.cn5)),
                                               '') + ' ' + ISNULL(LTRIM(RTRIM(a.cn6)), '') + ' ' + ISNULL(LTRIM(RTRIM(a.cn7)), '') + ' ' + ISNULL(LTRIM(RTRIM(a.cn8)), ''), 'root', ''))) 
                                              + ' ' + y.search_keyword_new + ' ' + y.search_keyword_new + ' ' + y.search_keyword_new + ' ' + j.search_pkeyword_new + ' ' + t.search_tkeyword_new + ' ' + t.search_tkeyword_new + ' ' + t.search_tkeyword_new + ' This is the start the UDFs ' + b.search_ukeyword_new  AS search_text_new
                       FROM          (SELECT     a.Asset_ID, a.UserID, a.ProjectID, a.Name AS assetname, a.FileName, b.NAME AS cn1, c.NAME AS cn2, d.NAME AS cn3, e.NAME AS cn4, 
                                                                      f.NAME AS cn5, g.NAME AS cn6, h.NAME AS cn7, i.NAME AS cn8
                                               FROM          IPM_ASSET AS a WITH (NOLOCK) LEFT OUTER JOIN
                                                                      IPM_ASSET_CATEGORY AS b ON a.Category_ID = b.CATEGORY_ID LEFT OUTER JOIN
                                                                      IPM_ASSET_CATEGORY AS c ON b.PARENT_CAT_ID = c.CATEGORY_ID LEFT OUTER JOIN
                                                                      IPM_ASSET_CATEGORY AS d ON c.PARENT_CAT_ID = d.CATEGORY_ID LEFT OUTER JOIN
                                                                      IPM_ASSET_CATEGORY AS e ON d.PARENT_CAT_ID = e.CATEGORY_ID LEFT OUTER JOIN
                                                                      IPM_ASSET_CATEGORY AS f ON e.PARENT_CAT_ID = f.CATEGORY_ID LEFT OUTER JOIN
                                                                      IPM_ASSET_CATEGORY AS g ON f.PARENT_CAT_ID = g.CATEGORY_ID LEFT OUTER JOIN
                                                                      IPM_ASSET_CATEGORY AS h ON g.PARENT_CAT_ID = h.CATEGORY_ID LEFT OUTER JOIN
                                                                      IPM_ASSET_CATEGORY AS i ON h.PARENT_CAT_ID = i.CATEGORY_ID) AS a INNER JOIN
                                              IPM_USER AS u ON a.UserID = u.UserID INNER JOIN
                                                  (SELECT     a.ProjectID, ISNULL(a.ProjectNumber, '') AS projectnumber, a.Name AS pn, b.NAME AS pn1, c.NAME AS pn2, d.NAME AS pn3, e.NAME AS pn4, 
                                                                           f.NAME AS pn5, g.NAME AS pn6, h.NAME AS pn7, i.NAME AS pn8
                                                    FROM          IPM_PROJECT AS a WITH (NOLOCK) LEFT OUTER JOIN
                                                                           IPM_CATEGORY AS b ON a.Category_ID = b.CATEGORY_ID LEFT OUTER JOIN
                                                                           IPM_CATEGORY AS c ON b.PARENT_CAT_ID = c.CATEGORY_ID LEFT OUTER JOIN
                                                                           IPM_CATEGORY AS d ON c.PARENT_CAT_ID = d.CATEGORY_ID LEFT OUTER JOIN
                                                                           IPM_CATEGORY AS e ON d.PARENT_CAT_ID = e.CATEGORY_ID LEFT OUTER JOIN
                                                                           IPM_CATEGORY AS f ON e.PARENT_CAT_ID = f.CATEGORY_ID LEFT OUTER JOIN
                                                                           IPM_CATEGORY AS g ON f.PARENT_CAT_ID = g.CATEGORY_ID LEFT OUTER JOIN
                                                                           IPM_CATEGORY AS h ON g.PARENT_CAT_ID = h.CATEGORY_ID LEFT OUTER JOIN
                                                                           IPM_CATEGORY AS i ON h.PARENT_CAT_ID = i.CATEGORY_ID) AS x ON a.ProjectID = x.ProjectID INNER JOIN
                                                  (SELECT     Asset_ID, ' SERVICES ' + REPLACE(dbo.List_Services(Asset_ID), ',', ' SERVICES') 
                                                                           + ' SERVICES ' + ' ' + ' MEDIA ' + REPLACE(dbo.List_MediaType(Asset_ID), ',', ' MEDIA') 
                                                                           + ' MEDIA ' + ' ' + ' ILLUST ' + REPLACE(dbo.List_IllustType(Asset_ID), ',', ' ILLUST') + ' ILLUST ' AS search_keyword_new
                                                    FROM          IPM_ASSET WITH (NOLOCK)) AS y ON a.Asset_ID = y.Asset_ID INNER JOIN
                                                  (SELECT     Asset_ID, dbo.List_Discipline(ProjectID) + ' ' + dbo.List_Keyword(ProjectID) + ' ' + dbo.List_Office(ProjectID) AS search_pkeyword_new
                                                    FROM          IPM_ASSET WITH (NOLOCK)) AS j ON a.Asset_ID = j.Asset_ID INNER JOIN
                                                  (SELECT     Asset_ID, dbo.List_UDFS(Asset_ID) AS search_ukeyword_new
                                                    FROM          IPM_ASSET WITH (NOLOCK)) AS b ON a.Asset_ID = b.Asset_ID INNER JOIN
                                                   (SELECT     Asset_ID, ' TAGS ' + REPLACE(dbo.List_Tags(Asset_ID), ',', ' TAGS') + ' TAGS ' + ' TAGS ' + REPLACE(dbo.List_Tags(Asset_ID), ' ', ' TAGS ') + ' TAGS ' AS search_tkeyword_new
                                                    FROM          IPM_ASSET WITH (NOLOCK)) AS t ON a.Asset_ID = t.Asset_ID) AS v INNER JOIN
                      IPM_ASSET WITH (ROWLOCK) ON v.Asset_ID = IPM_ASSET.Asset_ID