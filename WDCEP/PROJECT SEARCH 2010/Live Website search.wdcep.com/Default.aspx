﻿<%@ page language="VB" autoeventwireup="false" inherits="_Default, App_Web_pbnym3yg" %>
<%@ Register assembly="ComponentArt.Web.UI" namespace="ComponentArt.Web.UI" tagprefix="ComponentArt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<asp:Literal id="htmltop" runat=server></asp:Literal>



<head runat="server">
<title>WDCEP Project Search - Home</title>
</head>

<style type="text/css">
@import url('css/style.css');
</style>
<link href="snapStyle.css" rel="stylesheet" type="text/css" />
<style>
.heading {

	padding: 4px;
	padding-left:10px;
	background-color: #f9f9f9;
	border-top:1px gray solid;
	position: relative;
	font-size:12px;
}
.heading_window {

	position: relative;
}
.content_window {
	padding: 10px;
	position: relative;
	background-color: #f9f9f9;	
}
.content_window_input {
	width:230px;
	
}

.test33{
	
	margin-top:10px;
	}
.auto-style1 {
	color: #0076C0;
}
.auto-style1 a {
	color: #0076C0;
}

.auto-style2 {
	font-size: 10px;
}
.auto-style3 {
	font-size: 12px;
	padding-bottom:5px;
	width:300px;
}
.auto-style5 {
	font-size: small;
	text-transform: uppercase;
		padding-bottom:5px;
	width:300px;
	padding-left:10px;

}
</style>
<body style="margin: 0">

    <form id="form1" runat="server" action="Result.aspx">

	<div id="projectsearchcont">
	</div>
	<div id="customquerycont">
	</div>
	<div >
		<table border="0" cellpadding="0" cellspacing="0" width="623"  style="width: 623px; background-color: #f8f3d3; border-top: 2px #0076C0 solid; border-bottom: 2px #0076C0 solid; padding: 10px;">
			<tr>
				<td class="maintext" style="width: 623px">
				<div class="auto-style3"><strong>FIND A PROJECT</strong><br />
				</div>
				<asp:TextBox ID="txtKeyword" runat="server" Width="309px"></asp:TextBox>
				   <div style="padding-top:10px; position:relative; float:right; left: -188px; top: -12px;"> <asp:ImageButton ID="ImageButton1" runat="server" 
                        ImageUrl="~/images/submit.jpg" /></div>
				<br />
				<!--<span class="auto-style2">search by location, developer, architect, use, etc. 
				</span> <br />-->
				</td>
			</tr>
		</table>
		
		<table border="0" cellpadding="0" cellspacing="0" width="623"  style="width: 623px; background-color:  #ececec;  padding: 10px;">
			<tr>
				<td class="maintext" style="width: 623px; height: 36px;">
				<span class="auto-style3"><strong>POPULAR SEARCHES</strong></span><br />
				<br />
				    <span class="auto-style1"><strong><asp:LinkButton ID=lbpsgreendevelopment OnClientClick="javascript:SetCookie('pstype','greendevelopment');" runat=server>Green Development</asp:LinkButton></strong></span> | 
				<span class="auto-style1"><strong><asp:LinkButton ID=LinkButton1 OnClientClick="javascript:SetCookie('pstype','retailprojects');" runat=server>Retail Projects</asp:LinkButton></strong></span> | 
				<span class="auto-style1"><strong><asp:LinkButton ID=LinkButton2 OnClientClick="javascript:SetCookie('pstype','residentialprojects');" runat=server>Residential Projects</asp:LinkButton></strong></span> | 
				<span class="auto-style1"><strong><asp:LinkButton ID=LinkButton3 OnClientClick="javascript:SetCookie('pstype','officeprojects');" runat=server>Office Projects</asp:LinkButton></strong></span></td>
			</tr>
		</table>

		
		
		
	</div>
	<img border="0" height="2" src="images/clear.gif"/><br />
	<br />

	<strong><div class="auto-style5">
	ADDITIONAL SEARCH OPTIONS
    <br />
    </div></strong>
	<div style="width:623px;padding:0px;">
			<div class="heading_window">
				<div class="heading">
					I. CONSTRUCTION TYPE</div>
				<div class="content_window">
					<asp:CheckBox id="newconstruction" runat="server" Text="New Construction" Width="200" CssClass="content_window_input" />
					<asp:CheckBox id="Renovation" runat="server" Text="Renovation"  Width="200" CssClass="content_window_input"/>
					<asp:CheckBox id="Infrastructure" runat="server" Text="Infrastructure" />
				</div>
			</div>
			<div class="heading_window">
				<div class="heading">
					II. PROJECT COMPONENTS</div>
				<div class="content_window">
					Search by use:
					<asp:DropDownList ID="searchbyuse" runat="server"></asp:DropDownList>
				</div>
			</div>
			<div class="heading_window">
				<div class="heading">
					III. CONSTRUCTION STATUS</div>
				<div class="content_window">
				<asp:CheckBox id="CheckBoxcompleted"  runat="server" /> Completed from <asp:DropDownList ID="DropDownListcompletedyearfrom" Enabled=false runat="server"></asp:DropDownList> to <asp:DropDownList ID="DropDownListcompletedyearto"  Enabled=false runat="server"></asp:DropDownList><br/>
				<asp:CheckBox id="CheckBoxunderconstruction" runat="server" Text=" Under Construction" CssClass="content_window_input" Width="200"/><br />
				<asp:CheckBox id="CheckBoxNearTerm" runat="server" Text=" Near Term" CssClass="content_window_input" Width="200"/> <asp:CheckBox id="CheckBoxplanned" runat="server" Text=" Medium Term" Width="200" CssClass="content_window_input"/> <asp:CheckBox id="CheckBoxProposed" runat="server" Text=" Long Term" CssClass="content_window_input" /> 
				
					
				</div>
			</div>
			<div class="heading_window">
				<div class="heading">
					IV. PROJECT LOCATION</div>
				<div class="content_window">
					Ward:
					<asp:DropDownList ID="ward" runat="server"></asp:DropDownList>

				</div>
			</div>
			<div class="heading_window">
				<div class="heading">
					V. LEED</div>
				<div class="content_window">
					LEED Level:
					<asp:DropDownList ID="leedlevel" runat="server"></asp:DropDownList>
				</div>
			</div>
			</div>
	<img border="0" height="2" src="images/clear.gif"/>
   

<p><asp:ImageButton ID="ImageButton2" runat="server" 
                        ImageUrl="~/images/submit.jpg" />&nbsp;&nbsp;&nbsp;
<a href="default.aspx"><img alt="" height="25" src="images/reset.jpg" width="93" /></a></p>
 </form>
 <script>

     function setCompletedDropdownstatus(bval) {
         if (bval) {
             var _DropDownListcompletedyearfrom = document.getElementById("DropDownListcompletedyearfrom")
             _DropDownListcompletedyearfrom.disabled = false;
             var _DropDownListcompletedyearto = document.getElementById("DropDownListcompletedyearto")
             _DropDownListcompletedyearto.disabled = false;             
         } else {
         var _DropDownListcompletedyearfrom = document.getElementById("DropDownListcompletedyearfrom")
         _DropDownListcompletedyearfrom.disabled = true;
         var _DropDownListcompletedyearto = document.getElementById("DropDownListcompletedyearto")
         _DropDownListcompletedyearto.disabled = true;        
         }

 
         
     }
 
     function GetCookie(name) {
         var nameEQ = name + "=";
         var ca = document.cookie.split(';');
         for (var i = 0; i < ca.length; i++) {
             var c = ca[i];
             while (c.charAt(0) == ' ') c = c.substring(1, c.length);
             if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
         }
         return null;
     }

     function SetCookie(name, value) {

         var days = 300
         var date = new Date();
         date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
         var expires = "; expires=" + date.toGMTString();
         document.cookie = name + "=" + escape(value) + expires + "; path=/";
         //document.cookie=name+ "=" +escape(value)+((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
     }

     function DeleteCookie(name) {

         var exp = new Date();
         exp.setTime(exp.getTime() - 1);
         var cval = GetCookie(name);
         document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
     }
     SetCookie('pstype', '');
 </script>
 
 <asp:Literal id="htmlbottom" runat=server></asp:Literal>



 <script type="text/javascript">
     var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
     document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-2739433-5");
        pageTracker._trackPageview();
    } catch (err) { }</script>




</body>

</html>



