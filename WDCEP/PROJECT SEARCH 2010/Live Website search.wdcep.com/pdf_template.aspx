﻿<%@ page language="VB" autoeventwireup="false" inherits="pdf_template, App_Web_pbnym3yg" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<head>
<meta content="en-us" http-equiv="Content-Language" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title><asp:Literal ID=ltrltitle runat=server ></asp:Literal></title>
<style type="text/css">
.auto-style1 {
	font-size: xx-large;
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style2 {
	font-size: medium;
	color: #999696;
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style3 {
	margin-left: -10px;
	margin-right: 0px;
}
.auto-style4 {
	font-family: Arial, Helvetica, sans-serif;padding-bottom:5px;
}
.auto-style5 {
	font-size: medium;
}
.auto-style6 {
	font-size: small;
}
.auto-style7 {
	text-align: justify;
}
.auto-style8 {
	font-size: xx-large;
	font-family: Verdana, Geneva, Tahoma, sans-serif;
}
.auto-style10 {
	padding-bottom: 5px;
}
.auto-style11 {
	font-size: small;
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style12 {
	font-size: xx-small;
}
.auto-style13 {
	font-size: x-large;
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style14 {
	font-size: x-large;
	font-family: Verdana, Geneva, Tahoma, sans-serif;
}
.auto-style15 {
	font-family: Arial, Helvetica, sans-serif;
}
</style>
</head>

<body style="margin: 0">

<img alt="" height="159" src="images/pdfheading.jpg" width="900" />
<div id="layer1" style="position: relative; width: 841px; height: 476px; z-index: 1; left: 34px; line-height: 1;">
	<div id="layer2" style="position: relative; width: 100px; height: 4px; z-index: 1; left: 236px; top: 73px;">
	<img height="393" src="<asp:Literal ID=ltrlprojectimage runat=server ></asp:Literal>" width="596" /></div>

	<span class="auto-style8"><asp:Literal ID=ltrlprojectname runat=server ></asp:Literal></span> <span class="auto-style2">(<asp:Literal ID=ltrlprojectaddress runat=server ></asp:Literal>)</span><img alt="" class="auto-style3" height="39" src="images/pdfheadingprojectspec.jpg" width="842" /><span class="auto-style4"><span class="auto-style5"><br />
	<br />
	</span></span><span class="auto-style10"><span class="auto-style11">Total Sq. Ft.: <strong>2,300,000</strong></span></span><span class="auto-style6"><br class="auto-style4" />
	<span class="auto-style4">Community Sq. F.t: <strong>3,000,000</strong></span><br class="auto-style4" />
	<span class="auto-style4">Education Sq. Ft.: <strong>3,000,000</strong></span><br class="auto-style4" />
	<span class="auto-style4">Hotel Rooms: <strong>99,000</strong></span><br class="auto-style4" />
	<span class="auto-style4">Industrial Sq. Ft.: <strong>3,000,000</strong></span><br class="auto-style4" />
	<span class="auto-style4">Medical Sq. Ft.: <strong>3,000,000</strong></span><br class="auto-style4" />
	<span class="auto-style4">Museum Sq. Ft.: <strong>3,000,000</strong></span><br class="auto-style4" />
	<span class="auto-style4">Office Sq. Ft.: <strong>3,000,000</strong></span><br class="auto-style4" />
	<span class="auto-style4">Residential Units: <strong>99,000</strong></span><br class="auto-style4" />
	<span class="auto-style4">Retail Sq. Ft.: <strong>3,000,000</strong></span><br class="auto-style4" />
	<span class="auto-style4">LEED: <strong>Platinum</strong></span><br class="auto-style4" />
	</span>
	<span class="auto-style4"><span class="auto-style6">Est. Cost: </span> <strong>
<span class="auto-style6">$2,500,000,000</span></strong></span>
	
	<p class="auto-style1">&nbsp;</p>
</div>
<div>


</div>


<p>
<div id="layer3" style="position: relative; width: 841px; height: 470px; z-index: 1; left: 34px; top: 0px;">
	<div class="auto-style7">
	<img alt="" class="auto-style3" height="39" src="images/pdfheadingprojectdetail.jpg" width="841" /><span class="auto-style4"><span class="auto-style5"><br />
		<br />
		</span><span class="auto-style6">Developer(s): <strong><asp:Literal ID=ltrlprojectdeveloper runat=server ></asp:Literal></strong><br />
	Architect(s): <strong><asp:Literal ID=ltrlprojectarchitects runat=server ></asp:Literal></strong><br />
	Contractor(s): <strong><asp:Literal ID=ltrlprojectcontractors runat=server ></asp:Literal></strong><br />
	Project Status: <strong><asp:Literal ID=ltrlprojectstatus runat=server ></asp:Literal></strong><br />
	Targeted Delivery: <strong><asp:Literal ID=ltrlprojecttargetdate runat=server ></asp:Literal><br />
		</strong></span><span class="auto-style5"><br />
		</span><span class="auto-style6">
		<asp:Literal ID=ltrlprojectdescription runat=server ></asp:Literal>
		<br />
		<br />
		</span>
		<span class="auto-style12">Disclaimer: The Washington, DC 
		Economic Partnership makes no warranty, guarantee as to the accuracy, 
		completeness or usefulness for any given purpose of the information 
		provided. This sheet uses information from various sources that may not 
		reflect the most current information. All information is approximate and 
		it is always recommended that you verify the data and outreach to each 
		sites’ contact(s) for the most up-to-date information. To update or add 
		a DC development project contact Chad Shuskey, 
		<a href="mailto:cshuskey@wdcep.com">cshuskey@wdcep.com</a>.<br />
		<br />
		<br />
		</span></span></div>
		<p class="auto-style1">&nbsp;</p>
</div>
</p>


<p>

<img alt="" height="159" src="images/pdfheading.jpg" width="900" /></p>
<div id="layer4" style="position: relative; width: 841px; height: 476px; z-index: 1; left: 30px; line-height: 1; top: -20px;">
	<div id="layer5" style="position: relative; width: 100px; height: 4px; z-index: 1; left: 236px; top: 73px;">
	<img height="393" src="http://idamimg.webarchives.com/IDAMCLIENT/(S(whwng0uhh3zkay55uyotqz55))/RetrieveAsset.aspx?id=2421316&amp;instance=IDAM_WDCEP&amp;type=asset&amp;size=1&amp;width=800&amp;height=600&amp;cache=1&amp;public=1&amp;guid=44bb3701-c096-4be1-b639-b4bbef9addf0" width="596" /></div>

	<span class="auto-style14">Southwest</span><span class="auto-style13"> 
	Waterfront</span><span class="auto-style1"> 
	</span> <span class="auto-style2">(cont.)</span><br />
	<span class="auto-style4"><span class="auto-style5">
	<img alt="" class="auto-style3" height="40" src="images/pdfheadinglocationspecsl.jpg" width="841" /><br />
	<br />
	</span><span class="auto-style6">Location: <strong>1st &amp; M Street, SE</strong><br />
	Ward.: <strong>6</strong><br />
	Square: <strong>5987</strong><br />
	Zip Code: <strong>20002</strong><br />
	Enterprise Zone: <strong>Primary EZ</strong></span></span></p>
	
	<p class="auto-style1">&nbsp;</p>
</div>


<div id="layer6" style="position: relative; width: 841px; height: 434px; z-index: 1; left: 34px; top: 0px;">
	<img height="284" src="http://idamimg.webarchives.com/IDAMCLIENT/(S(whwng0uhh3zkay55uyotqz55))/RetrieveAsset.aspx?id=2421316&amp;instance=IDAM_WDCEP&amp;type=asset&amp;size=1&amp;width=800&amp;height=600&amp;cache=1&amp;public=1&amp;guid=44bb3701-c096-4be1-b639-b4bbef9addf0" width="395" />&nbsp;&nbsp;&nbsp;
	<img height="285" src="http://idamimg.webarchives.com/IDAMCLIENT/(S(whwng0uhh3zkay55uyotqz55))/RetrieveAsset.aspx?id=2421316&amp;instance=IDAM_WDCEP&amp;type=asset&amp;size=1&amp;width=800&amp;height=600&amp;cache=1&amp;public=1&amp;guid=44bb3701-c096-4be1-b639-b4bbef9addf0" width="416" /><br />
	<br />
	<img alt="" height="42" src="images/pdfheadingmoreinformationl.jpg" width="830" /><br />
	<br />
	<span class="auto-style15">For more information about this project, please 
	visit: <a href="http://www.wdcep.com">www.wdcep.com</a><br />
	<br />
	<span class="auto-style4"><span class="auto-style12">Disclaimer: The Washington, DC 
		Economic Partnership makes no warranty, guarantee as to the accuracy, 
		completeness or usefulness for any given purpose of the information 
		provided. This sheet uses information from various sources that may not 
		reflect the most current information. All information is approximate and 
		it is always recommended that you verify the data and outreach to each 
		sites’ contact(s) for the most up-to-date information. To update or add 
		a DC development project contact Chad Shuskey, cshuskey@wdcep.com.</span></span></span></div>


</body>

</html>

