﻿<%@ page language="VB" autoeventwireup="false" inherits="AnalyzePDF, App_Web_pbnym3yg" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<!DOCTYPE html>

<html >
<head>

    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

    <script src="http://cdn.jquerytools.org/1.2.2/full/jquery.tools.min.js" type="text/javascript"></script>

    <style type="text/css">
 	@import url("css/style.css");
</style>
    <style type="text/css"> 
  html { height: 100% }
  body { height: 100%; }
  #map_canvas { height: 300px; width: 600px; padding:10px; }
  
  
  
  
  
#table_search 
{
    border-collapse: collapse;
    
    font-family:Verdana;
    font-size:12px;
}
#table_search th a 
{
    color: #ffffff;
}
#table_search td, #table_search th
{
    padding-top:5px;
    padding-bottom:5px;
}    
#table_search th
{
    background: #00224b;
    color: #ffffff;
    font-weight:bold;
}
#table_search td, #table_search th
{
    padding-left: 18px;
}
#table_search td a 
{
    font-weight:bold;
}
#table_search tr.odd td
{
    background: #ffffff;
    color:#333333;
}
#table_search tr.even td
{
    background: #feeac0;
    color:#333333;
}

  
  
.auto-style1 {
	border: 1px solid #000000;
	background-color: #00CCFF;
}
.auto-style2 {
	border: 1px solid #000000;
	background-color: #FEEAC0;
}
.auto-style3 {
	color: #000000;
	border: 1px solid #000000;
	background-color: #FEEAC0;
}
.auto-style4 {
	border: 1px solid #000000;
	 text-align:center;
}
.auto-style5 {
	border: 1px solid #000000;
	 text-align:right;
}
  
  
</style>
    <title>WDCEP Project Search - Analysis PDF</title>
</head>
<body style="margin-left: 0; margin-top: 0; margin-bottom: 0">
    <form id="form1" runat="server">
   
    </form>
    
    <div style="width: 623px; padding: 5px; color: black; font-family: Verdana; font-size: 12px;">

        <table cellpadding=4 cellspacing=2>
            <tr style="font-weight: bold;">
                <td width="200" class="auto-style1">
                    Search Results Analysis
                </td>
                <td width="200" class="auto-style1" align=center>
                    Number of Projects
                </td>
                <td width="200" class="auto-style1">
                    Totals
                </td>
            </tr>
            <tr style="background-color: White; color: Black; font-weight: bold;">
                <td class="auto-style2">
                    Summar<strong>y</strong>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <asp:Literal ID="literalAGrid" runat="server"></asp:Literal>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="background-color: White; color: Black; font-weight: bold;">
                <td class="auto-style3">
                    <strong>Construction Type</strong>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <asp:Literal ID="literalBGrid" runat="server"></asp:Literal>
            <tr style="background-color: White; color: Black; font-weight: bold;">
                <td class="auto-style2">
                    Project Components
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <asp:Literal ID="literalCGrid" runat="server"></asp:Literal>
            <tr style="background-color: White; color: Black; font-weight: bold;">
                <td class="auto-style2" style="height: 18px">
                    Construction Status
                </td>
                <td style="height: 18px">
                </td>
                <td style="height: 18px">
                </td>
            </tr>
            <asp:Literal ID="literalDGrid" runat="server"></asp:Literal>
            <tr style="background-color: White; color: Black; font-weight: bold;">
                <td class="auto-style2">
                    LEED Level
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <asp:Literal ID="literalEGrid" runat="server"></asp:Literal>
        </table>
        *Total may include other components of mixed-use projects.

    </div>
    

    
    
 <script type="text/javascript">
     var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
     document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-2739433-5");
        pageTracker._trackPageview();
    } catch (err) { }</script>
</body>
</html>
