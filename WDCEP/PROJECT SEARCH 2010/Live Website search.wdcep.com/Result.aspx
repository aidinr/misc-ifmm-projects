﻿<%@ page language="VB" autoeventwireup="false" inherits="Result, App_Web_pbnym3yg" enableviewstatemac="false" enableviewstate="false" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<!DOCTYPE html> 
<asp:Literal id="htmltop" runat=server></asp:Literal>
<html> 
<head> 
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" /> 
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/> 

<script src="http://cdn.jquerytools.org/1.2.2/full/jquery.tools.min.js" type="text/javascript"></script>
<style type="text/css">
 	@import url("css/style.css");
</style>
<style type="text/css"> 
  html { height: 100% }
  body { height: 100%;  }
  #map_canvas { height: 300px; width: 600px; padding:10px; }
  
  
  
  
  
  
.pageitem
{
	margin-right:5px;
  color:white; 
   background-color:orange; 
  font-family:tahoma; 
  font-size:11px; 
text-align:center;
  cursor:default; 
  padding-left:4px;padding-right:4px;padding-top:1px;padding-bottom:1px;
  width:12px;
  height:15px;
  position:relative;float:left;
}


.pageitem a
{
	
  color:white; 
  font-family:tahoma; 
  font-size:11px; 
}



.pageitemhighlight
{
  margin-right:5px;
  background-color:#0076C0; 
  color:white; 
  font-family:tahoma; 
  font-size:11px; 
  text-align:center;
  cursor:default; 
  padding-left:4px;padding-right:4px;padding-top:1px;padding-bottom:1px;
width:12px;
height:15px;
position:relative;float:left;
}
  
  
#table_search 
{
    border-collapse: collapse;
    
    font-family:Verdana;
    font-size:12px;
}
#table_search th a 
{
    color: #ffffff;
}
#table_search td, #table_search th
{
    padding-top:5px;
    padding-bottom:5px;
}    
#table_search th
{
    background: #0076C0;
    color: #ffffff;
    font-weight:bold;
}
#table_search td, #table_search th
{
    padding-left: 18px;
}
#table_search td a 
{
    font-weight:bold;
}
#table_search tr.odd td
{
    background: #ffffff;
    color:#333333;
    border-bottom:1px #0076C0 solid;
}
#table_search tr.even td
{
    background: #feeac0;
    color:#333333;
    border-bottom:1px #0076C0 solid;
}


#table_search tr.odd a
{

    color:#0076C0;

}
#table_search tr.even a
{

    color:#0076C0;

}



  
  
.auto-style1 {
	color: #666;
}

.auto-style1 a {
	color: #0076C0;
}


  
  
</style> 
<title>WDCEP Project Search - Results</title> 
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
<script type="text/javascript">


    var geocoder;
    var map;

    function initialize() {
        var myLatlng = new google.maps.LatLng(38.91971, -77.02931);
        var myOptions = {
            zoom: 12,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
       //var bounds = new google.maps.LatLngBounds(myLatlng,myLatlng);
       //map.fitBounds(bounds);
        // Create new geocoding object
        geocoder = new google.maps.Geocoder();

        // Retrieve location information, pass it to addToMap()
        //geocoder.getLocations(address, addToMap);

<asp:Literal ID=ltrladdresses runat=server></asp:Literal>


    }

    // This function adds the point to the map

    function addToMap(map,addressi) {
        geocoder.geocode({ 'address': addressi }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                           
                //alert("Geocode was not successful for the following reason: " + status + addressi);
            }
        });
    }


function closeallinfowindows()
{

}

function scrolltotopcust()
{
$('html, body').animate({scrollTop:0}, 'slow');
}
    
     function switchPage(i) {

        searchCallback.callback('paging',i);
    };
    function sortSearch(column) {

        searchCallback.callback('sort', column);
    };
    function filterSearch(filter) {

        searchCallback.callback('filter', filter);
    };
    function getcsv() {

        searchCallback.callback('getcsv');
    };    

    
</script> 
</head> 
<body onload="initialize()" style="margin-left: 0; margin-top: 0; margin-bottom: 0" > 
<form id="form1" runat="server">
 <table width="623" class="auto-style1"><tr><td align="left"> 
		Download <a href="Download.aspx?downloadtype=csv" >
		<strong>list.</strong></a>&nbsp;&nbsp;|&nbsp;&nbsp; <!--| 
		<a href="Download.aspx?downloadtype=excel" ><strong>excel</strong></a>.  -->
		<a href="Analyze.aspx" ><strong>View</strong></a> analysis of your results.</td>
		<td align="right"><a href="Default.aspx"><img src="images/newsearch.jpg"/></a></td></tr></table>
		
		

<br /><br />
</form>

  <div id="map_canvas"></div> 
<div style="width:610px; padding:5px;  color:#00224b; font-family:Verdana; font-size:12px; text-align:right; ">Results <asp:Literal ID="literalResults" runat="server"></asp:Literal></div>










<ComponentArt:CallBack runat="server" ID="searchCallback">
			                    <Content>
			                    <asp:PlaceHolder ID="slideshowPlaceholder" runat="server">
			                  
			                    
			                    <asp:Literal ID="literalPaging" runat="server"></asp:Literal>
			                    <asp:Repeater ID="RepeaterSearchResults" runat="server" OnItemDataBound="RepeaterSearchResultsItemDataBound">
						                <HeaderTemplate>
						                    <table cellpadding="0" cellspacing="0" width="620"  id="table_search">
						                        <thead>
						                                <tr>
						                                <th valign="top" width="16"></th>
						                                <th valign="top" width="111"></th>
						                                <th valign="top" width="197"><a href="javascript:sortSearch('pn')">Property Name</a></th>
						                                <th valign="top" width="127"><a href="javascript:sortSearch('mu')">Major Use</a></th>
						                                <th valign="top" width="95"><a href="javascript:sortSearch('sf')">Sq. Ft.</a></th>
						                                <th valign="top" width="127"><a href="javascript:sortSearch('ec')">Est Cost</a></th>
						                                
						                                </tr>
						                        </thead>
						                        <tbody>
						                </HeaderTemplate>
						          <ItemTemplate>
						                <tr class="odd">      <!---->  
						                <td valign="middle" align="center" width="16"><a style="cursor:pointer" onclick="javascript:<%#gotoMapLocation(Container.DataItem("x"),Container.DataItem("y"),Container.dataitem("projectid")) %>scrolltotopcust();" ><img src="images/mapicons/lightblue<%#getPagexFactor(Container.DataItem("projectid"))%>.gif" border=0 /></a></td>                                                                                                                                                                                     
						                               <td valign="top" align="center" width="111"><a href="property_detail.aspx?id=<%#Container.DataItem("projectid")%>"><img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("projectid")%>&type=project&crop=1&size=1&height=80&width=100" alt="<%#Container.DataItem("name").tostring.trim%>" /></a></td>
						                                <td valign="middle" width="197"><a href="property_detail.aspx?id=<%#Container.DataItem("projectid")%>"><%#Container.DataItem("name").trim%></a><asp:Literal ID="literlmanaged2" runat="server" ></asp:Literal></td>
						                                <td valign="middle" width="127"><%#Container.DataItem("majoruse").ToString.Trim%></td>
						                                <td valign="middle" width="95" align="right"><%#FormatNumber(Container.DataItem("totalsf"), 0).ToString.Trim%></td>
						                                <td valign="middle" width="127" align="right">$<%#FormatNumber(Container.DataItem("totalcost"), 0).ToString.Trim%></td>
						                               
						                </tr>
						          </ItemTemplate>
						          <AlternatingItemTemplate>
						                <tr class="even">
						                <td valign="middle" align="center" width="16"><a style="cursor:pointer" onclick="javascript:<%#gotoMapLocation(Container.DataItem("x"),Container.DataItem("y"),Container.dataitem("projectid")) %>scrolltotopcust();" target=_top><img src="images/mapicons/lightblue<%#getPagexFactor(Container.DataItem("projectid"))%>.gif" border=0/></a></td>                                                                                                                                                                                     
						                            <td valign="top" align="center" width="111"><a href="property_detail.aspx?id=<%#Container.DataItem("projectid")%>"><img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("projectid")%>&type=project&crop=1&size=1&height=80&width=100" alt="<%#Container.DataItem("name").tostring.trim%>" /></a></td>
						                                <td valign="middle" width="197"><a href="property_detail.aspx?id=<%#Container.DataItem("projectid")%>"><%#Container.DataItem("name").trim%></a><asp:Literal ID="literlmanaged2"  runat="server" ></asp:Literal></td>
						                                <td valign="middle" width="127"><%#Container.DataItem("majoruse").ToString.Trim%></td>
						                                <td valign="middle" width="95" align="right"><%#FormatNumber(Container.DataItem("totalsf"), 0).ToString.Trim%></td>
						                                <td valign="middle" width="127" align="right">$<%#FormatNumber(Container.DataItem("totalcost"), 0).ToString.Trim%></td>
						                                
						                </tr>
						          </AlternatingItemTemplate>
						          
						          <FooterTemplate>
						          </tbody>
						          </table>
						          </FooterTemplate>
						          </asp:Repeater>
			                    <br /><br />
			                    <asp:Literal ID="literalPagingBottom" runat="server"></asp:Literal>
			                    </asp:PlaceHolder>				
				                </Content>
				                <LoadingPanelClientTemplate>
                                                                    <table cellspacing="0" cellpadding="0" width="620" border="0">
                                                                        <tr>
                                                                            <td align="center">
                                                                                <table cellspacing="0" cellpadding="0" border="0">
                                                                                    <tr>
                                                                                        <td style="font-size: 12px">
                                                                                            Loading...
                                                                                        </td>
                                                                                        <td>
                                                                                            <img height="16" src="images/spinner.gif" width="16" border="0">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </LoadingPanelClientTemplate>
				               
                                                                				                </ComponentArt:CallBack>












 <asp:Literal id="htmlbottom" runat=server></asp:Literal>

 
 <script type="text/javascript">
     var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
     document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-2739433-5");
        pageTracker._trackPageview();
    } catch (err) { }</script>

</body> 
</html> 
