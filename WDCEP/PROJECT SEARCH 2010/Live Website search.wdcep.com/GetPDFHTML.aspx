﻿<%@ page language="VB" autoeventwireup="false" inherits="GetPDFHTML, App_Web_pbnym3yg" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta content="en-us" http-equiv="Content-Language" />
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <title>Get PDF HTML</title>
    <style type="text/css">
        .auto-style3
        {
            margin-left: -10px;
            margin-right: 0px;
        }
        .auto-style9997
        {
            font-size: 9pt;
            font-family: "Futura Medium";
            line-height: 1.9;
        }
        .auto-style7
        {
            line-height: 100%;
            text-align: justify;
        }
        .auto-style16
        {
            font-size: 18pt;
            font-family: "Futura Medium";
        }
        .auto-style17
        {
            font-size: 12pt;
            color: #3E444C;
            font-family: "Futura Medium";
            line-height: 1.3;
        }
        .auto-style20
        {
            font-family: "Futura Medium";
            font-size: 9pt;
        }
        .auto-style22
        {
            font-family: "Futura Medium";
            padding-bottom: 5px;
            font-size: 9pt;
            line-height: 1.9;
            color: #636C76;
        }
        .auto-style23
        {
            font-family: "Futura Medium";
            padding-bottom: 5px;
            font-size: 5pt;
            line-height: 8px;
        }
        .auto-style9999
        {
            text-align: center;
            font-family:"Futura Medium";
        }
        .auto-style10001
        {
            line-height: 1.3;
        }
        .auto-style10002
        {
            line-height: 100%;
        }
        .auto-style10003
        {
            color: #C6C8CA;
            font-family: "Futura Medium";
            font-size: 9pt;
            text-align: right;
        }
        .auto-style10004
        {
            color: #000000;
            font-family: "Futura Medium";
            font-size: 9pt;
            text-align: left;
        }
    </style>
</head>
<body style="margin: 0">
    <form id="form1" runat="server">
    <img alt="" height="159" src="images/pdfheading.jpg" width="900" />
    <div id="layer1" style="position: relative; width: 62px; height: 30px; z-index: 1;
        left: 802px; top: -128px;" class="auto-style10003">
        <asp:Literal ID="literalStatusDate" runat="server"></asp:Literal>
    </div>
    <div id="layer1" style="position: relative; width: 828px; height: 0px; z-index: 1;
        left: 33px; top: 905px;" class="auto-style10004">
        <div class="auto-style9999">
            <span class="auto-style20">For more information about this project, please visit:
            </span><strong><span class="auto-style20">www.wdcep.com</span></strong></div>
        <hr style="height: 1px" />
        <span class="auto-style23">Disclaimer: The Washington, DC Economic Partnership makes
            no warranty, guarantee as to the accuracy, completeness or usefulness for any given
            purpose of the information provided. This sheet uses information from various sources
            that may not reflect the most current information. All information is approximate
            and it is always recommended that you verify the data and outreach to each sites’
            contact(s) for the most up-to-date information. To update or add a DC development
            project contact Chad Shuskey, cshuskey@wdcep.com</span></div>
    <div id="layer1" style="position: relative; width: 841px; height: 476px; z-index: 1;
        left: 36px; top: -51px;" class="auto-style10002">
        <div id="layer2" style="position: relative; width: 100px; height: 4px; z-index: 1;
            left: 234px; top: 51px;">
             <asp:Image ID="projectImage1" runat="server" width="596" height="393" />
            <!--<img height="393" src="images\imgtemp.jpg" width="596" />--></div>
        <div class="auto-style10002">
            <span class="auto-style16"><strong>
                <asp:Literal ID="ltrlpn" runat=server></asp:Literal><asp:Literal ID="literalProjectName" runat="server"></asp:Literal></strong></span><br />
            <span class="auto-style17">
       
                <asp:Literal ID="literalProjectAddress" runat="server"></asp:Literal></span><img
                    alt="" class="auto-style3" height="39" src="images/pdfheadingprojectspec.jpg"
                    width="842" /><br />
            <span style="line-height: 1.9; font-size: 9pt; font-family: Futura Medium;">
                <asp:Literal ID="literalward" runat="server"></asp:Literal>
                
                
<asp:Literal ID="literaltotalsqft" runat="server"></asp:Literal>
<asp:Literal ID="literalcommunitytotalsqft" runat="server"></asp:Literal>
<asp:Literal ID="literaleducationtotalsqft" runat="server"></asp:Literal>
<asp:Literal ID="literalhotelrooms" runat="server"></asp:Literal>
<asp:Literal ID="literalMuseumSF" runat="server"></asp:Literal>


<asp:Literal ID="literalindustrialtotalsqft" runat="server"></asp:Literal>
<asp:Literal ID="literalmedicaltotalsqft" runat="server"></asp:Literal>



<asp:Literal ID="literalofficetotalsqft" runat="server"></asp:Literal>
<asp:Literal ID="literalresidentialunits" runat="server"></asp:Literal>
<asp:Literal ID="literalretailunits" runat="server"></asp:Literal>
<asp:Literal ID="literalstatus" runat="server"></asp:Literal>
<asp:Literal ID="literalleed" runat="server"></asp:Literal>
<asp:Literal ID="literalestimatedcost" runat="server"></asp:Literal>
                
                <!--
                Community Sq. F.t: <strong>3,000,000</strong><br class="auto-style10001" />
                Education Sq. Ft.:&nbsp; <strong>3,000,000</strong><br class="auto-style10001" />
                Hotel Rooms: <strong>99,000</strong><br class="auto-style10001" />
                Industrial Sq. Ft.: <strong>3,000,000</strong><br class="auto-style10001" />
                Medical Sq. Ft.: <strong>3,000,000</strong><br class="auto-style10001" />
                Museum Sq. Ft.: <strong>3,000,000</strong><br class="auto-style10001" />
                Office Sq. Ft.: <strong>3,000,000</strong><br class="auto-style10001" />
                Residential Units: <strong>99,000</strong><br class="auto-style10001" />
                Retail Sq. Ft.: <strong>3,000,000</strong><br class="auto-style10001" />
                LEED: <strong>Platinum</strong><br class="auto-style10001" />
                Est. Cost: <strong>$2,500,000,000 </strong>-->
                
             </span>
        </div>
    </div>
    <div>
    </div>
    <p>
    </p>
    <div id="layer3" style="position: relative; width: 826px; height: 470px; z-index: 1;
        left: 37px; top: -72px;">
        <div class="auto-style7">
            <img alt="" class="auto-style3" height="39" src="images/pdfheadingprojectdetail.jpg"
                width="841" />
            <span class="auto-style9997">Developer(s): <strong>
                <asp:Literal ID="ltrlprojectdeveloper" runat="server"></asp:Literal></strong><br />
                Architect(s): <strong>
                    <asp:Literal ID="ltrlprojectarchitects" runat="server"></asp:Literal></strong><br />
                Contractor(s): <strong>
                    <asp:Literal ID="ltrlprojectcontractors" runat="server"></asp:Literal></strong><br />
                Project Status: <strong>
                    <asp:Literal ID="ltrlprojectstatus" runat="server"></asp:Literal></strong><br />
              
                    <asp:Literal ID="ltrlprojecttargetdate" runat="server"></asp:Literal>
                <br />
            </span><span class="auto-style22">
                <asp:Literal ID="literaldescription" runat="server"></asp:Literal>

            </span>
        </div>
    </div>

    </form>
    
 <script type="text/javascript">
     var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
     document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-2739433-5");
        pageTracker._trackPageview();
    } catch (err) { }</script>
</body>
</html>
