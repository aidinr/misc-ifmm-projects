﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text

Imports WebSupergoo.ABCpdf7
Imports WebSupergoo.ABCpdf7.Objects
Imports WebSupergoo.ABCpdf7.Atoms
Imports WebSupergoo.ABCpdf7.Operations

Partial Class GetPDF
    Inherits System.Web.UI.Page

    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        GeneratePDF()


    End Sub

    Public Sub GeneratePDF()

        Dim SessionID As String = Session.SessionID
        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))

        Dim theDoc As Doc = New Doc()
        theDoc.HtmlOptions.Timeout = 120000
        theDoc.HtmlOptions.PageCacheClear()
        theDoc.HtmlOptions.PageCacheEnabled = False
        theDoc.HtmlOptions.PageCachePurge()
        theDoc.HtmlOptions.BrowserWidth = 900
        theDoc.HtmlOptions.ImageQuality = 93

        theDoc.HtmlOptions.FontEmbed = True
        theDoc.HtmlOptions.FontProtection = False
        theDoc.HtmlOptions.FontSubstitute = False


        'get windows dir
        Dim windir As String = Environment.GetEnvironmentVariable("windir") & "\Fonts\"


        Call theDoc.EmbedFont(windir & "AvenirLTStd-Light.otf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "AvenirLTStd-Black.otf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "AvenirLTStd-Roman.otf", "Latin", False, True, True)

        Call theDoc.EmbedFont(windir & "AvenirLTStd-BlackOblique.otf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "AvenirLTStd-Book.otf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "AvenirLTStd-BookOblique.otf", "Latin", False, True, True)

        Call theDoc.EmbedFont(windir & "AvenirLTStd-Heavy.otf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "AvenirLTStd-HeavyOblique.otf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "AvenirLTStd-LightOblique.otf", "Latin", False, True, True)

        Call theDoc.EmbedFont(windir & "AvenirLTStd-MediumOblique.otf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "AvenirLTStd-Oblique.otf", "Latin", False, True, True)


        Dim oriW As Integer = theDoc.Rect.Width
        Dim oriH As Integer = theDoc.Rect.Height

        Dim oriX As Integer = theDoc.Rect.Left
        Dim oriY As Integer = theDoc.Rect.Bottom

        Dim theUrl As String = ""

        Dim i As Integer = 1
        Dim pageCount As Integer = 1
        Dim theID As Integer

        'Dim logoFile As String = ConfigurationSettings.AppSettings("imagepath") & "JBG_Co_logo.jpg"
        Dim thePID As Integer = 0
        Try
            thePID = CInt(Request.QueryString("id"))
        Catch ex As Exception
            thePID = 0
            Response.Write("Error: invalid request.")
            Response.End()
        End Try

        theUrl = ConfigurationSettings.AppSettings("pdfurl") & "GetPDFHTML.aspx?id=" & thePID

        If (i > 1) Then


            theDoc.Rect.Resize(oriW, oriH)
            theDoc.Rect.Position(oriX, oriY)

            theDoc.Transform.Reset()
            theDoc.Page = theDoc.AddPage()
        End If

        theID = theDoc.AddImageUrl(theUrl)


        Do
            If Not theDoc.Chainable(theID) Then
                Exit Do
            End If

            'theDoc.Rect.Resize(102, 45)
            'theDoc.Rect.Move(55, 710)
            'theDoc.AddImageFile(logoFile, 1)

            theDoc.Rect.Resize(oriW, oriH)
            theDoc.Rect.Position(oriX, oriY)

            theDoc.Transform.Reset()
            theDoc.Page = theDoc.AddPage()
            theID = theDoc.AddImageToChain(theID)

            pageCount = pageCount + 1

        Loop

        If Not theDoc.Chainable(theID) Then
            'theDoc.Rect.Resize(102, 45)
            'theDoc.Rect.Move(55, 710)
            'theDoc.AddImageFile(logoFile, 1)

            'pageCount = pageCount + 1
        End If
        i = i + 1


        For i = 1 To theDoc.PageCount
            theDoc.PageNumber = i
            theDoc.Flatten()
        Next


        'Dim Sql As String = "select * from ipm_project a, ipm_project_discipline b, ipm_discipline c where a.projectid = @projectid and a.projectid = b.projectid and b.keyid = c.keyid and c.keyuse = 1 and available ='y' and publish = 1 and show = 1"

        'Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(Sql, MyConnection)

        'Dim projectID As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        'MyCommand1.SelectCommand.Parameters.Add(projectID)
        'MyCommand1.SelectCommand.Parameters("@projectid").Value = Request.QueryString("ProjectID")

        'Dim DT1 As New DataTable

        'MyCommand1.Fill(DT1)

        Dim theName = "Fact Sheet"

        'If (DT1.Rows.Count > 0) Then

        '    theName = Trim(DT1.Rows(0)("name")).Replace(" ", "_").Replace("&", "and")

        'End If

        Dim theDate As String = MonthName(DatePart("m", Date.Now)) & "-" & DatePart("d", Date.Now) ' & "_-_" & DatePart("h", Date.Now) & "-" & DatePart("n", Date.Now) & "-" & DatePart("s", Date.Now)
        Dim theProjectName As String = theName & "_-_" & theDate & ".pdf"

        theDoc.Save(ConfigurationSettings.AppSettings("pdfpath") & theProjectName)

        Dim stream As System.IO.FileStream

        stream = New FileStream(ConfigurationSettings.AppSettings("pdfpath") & theProjectName, FileMode.Open)
        Dim filesize As Integer = stream.Length

        Response.Clear()
        Response.ContentType = "application/pdf"
        Response.AddHeader("Content-Description", "File Transfer")
        Response.AddHeader("Content-Disposition", "attachment; filename=" & theProjectName)
        Response.AddHeader("Content-Length", filesize)

        Dim b(8192) As Byte

        Do While stream.Read(b, 0, b.Length) > 0
            Response.BinaryWrite(b)
            Response.Flush()
        Loop

        stream.Close()
        Response.End()

    End Sub


End Class
