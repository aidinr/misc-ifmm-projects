﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml

'Imports HtmlAgilityPack


Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        'Get Main HTML Template

        htmltop.Text = Functions.GetMainHTMLWrapper(Request.QueryString("htmlcache"))(0)
        htmlbottom.Text = Functions.GetMainHTMLWrapper(Request.QueryString("htmlcache"))(2)




        'fill dropdowns
        Session("ps") = ""

        'majoruse

        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select item_name from IPM_PROJECT_FIELD_DESC where Item_Tab = 'components' and Item_ID in (select Item_ID from IPM_PROJECT_FIELD_value where Item_Tab = 'components' and (Item_value = '0' or Item_Value = '1') )", MyConnection)
        'Dim DT1 As New DataTable("majoruse")
        'MyCommand1.Fill(DT1)
        'searchbyuse.DataSource = DT1
        'searchbyuse.DataTextField = "item_name"
        'searchbyuse.DataValueField = "item_name"
        'searchbyuse.DataBind()
        searchbyuse.Items.Insert(0, "All")
        searchbyuse.Items.Insert(1, "Community")
        searchbyuse.Items.Insert(2, "Education")
        searchbyuse.Items.Insert(3, "Hotel")
        searchbyuse.Items.Insert(4, "Industrial")
        searchbyuse.Items.Insert(5, "Medical")
        searchbyuse.Items.Insert(6, "Museum")
        searchbyuse.Items.Insert(7, "Entertainment")
        searchbyuse.Items.Insert(8, "Office")
        searchbyuse.Items.Insert(9, "Residential")
        searchbyuse.Items.Insert(10, "Retail")




        'MyCommand1 = New SqlDataAdapter("select item_value from ipm_project_field_value where item_id = 2400017 and item_value <> '4' group by item_value", MyConnection)
        'Dim DT2 As New DataTable("majoruse")
        'MyCommand1.Fill(DT2)
        'status.DataSource = DT2
        'status.DataTextField = "item_value"
        'status.DataValueField = "item_value"
        'status.DataBind()
        'status.Items.Insert(0, "All")


        MyCommand1 = New SqlDataAdapter("select datepart( yyyy,item_value) mindate from IPM_PROJECT_FIELD_VALUE where Item_ID = 2400089 group by datepart( yyyy,item_value) order by mindate asc", MyConnection)
        Dim DT2b As New DataTable("majoruse")
        MyCommand1.Fill(DT2b)
        DropDownListcompletedyearfrom.DataSource = DT2b
        DropDownListcompletedyearfrom.DataTextField = "mindate"
        DropDownListcompletedyearfrom.DataValueField = "mindate"
        DropDownListcompletedyearfrom.DataBind()


        MyCommand1 = New SqlDataAdapter("select datepart( yyyy,item_value) maxdate from IPM_PROJECT_FIELD_VALUE where Item_ID = 2400089 group by datepart( yyyy,item_value) order by maxdate desc", MyConnection)
        Dim DT2c As New DataTable("majoruse")
        MyCommand1.Fill(DT2c)
        DropDownListcompletedyearto.DataSource = DT2c
        DropDownListcompletedyearto.DataTextField = "maxdate"
        DropDownListcompletedyearto.DataValueField = "maxdate"
        DropDownListcompletedyearto.DataBind()




        'select min(datepart( yyyy,item_value)), max(datepart( yyyy,item_value)) from IPM_PROJECT_FIELD_VALUE where Item_ID = 2400089

        'select item_value from IPM_PROJECT_FIELD_VALUE where Item_ID = 2400017 and Item_Value like 'completed%'


        MyCommand1 = New SqlDataAdapter("select item_value from ipm_project_field_value where item_id = 2400009 group by item_value", MyConnection)
        Dim DT3 As New DataTable("majoruse")
        MyCommand1.Fill(DT3)
        ward.DataSource = DT3
        ward.DataTextField = "item_value"
        ward.DataValueField = "item_value"
        ward.DataBind()
        ward.Items.Insert(0, "All")


        'MyCommand1 = New SqlDataAdapter("select item_value from ipm_project_field_value where item_id = 2400008 and item_value <> '' group by item_value", MyConnection)
        'Dim DT4 As New DataTable("majoruse")
        'MyCommand1.Fill(DT4)
        'zipcode.DataSource = DT4
        'zipcode.DataTextField = "item_value"
        'zipcode.DataValueField = "item_value"
        'zipcode.DataBind()
        'zipcode.Items.Insert(0, "All")


        MyCommand1 = New SqlDataAdapter("select item_value from ipm_project_field_value where item_id = 2400099 and item_value <> '' and item_value <> '0' group by item_value order by case when Item_Value = 'Certified' then 1 when Item_Value = 'Silver' then 2 when Item_Value = 'Gold' then 3 when Item_Value = 'Platinum' then 4 end", MyConnection)
        Dim DT5 As New DataTable("majoruse")
        MyCommand1.Fill(DT5)
        leedlevel.DataSource = DT5
        leedlevel.DataTextField = "item_value"
        leedlevel.DataValueField = "item_value"
        leedlevel.DataBind()
        leedlevel.Items.Insert(0, "All")



        CheckBoxcompleted.Attributes.Add("onclick", "javascript:setCompletedDropdownstatus(CheckBoxcompleted.checked);")


    End Sub

   

    Protected Sub lbpsgreendevelopment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbpsgreendevelopment.Click
        Session("ps") = "greendevelopment"
    End Sub
End Class
