<?php
/**
web/content/wp_content/themes/wdcep/
 * Page Template
 *
 * This template is loaded when viewing a landing page.
 * @link http://codex.wordpress.org/Pages
 *
 * @package Hybrid
 * @subpackage Template
*/
/*
 Template Name: Development Search */

$action_page = 'index.php';
if(isset($_REQUEST['action'])) {
	$action_page = preg_replace('/[^a-z0-9]/', '', $_REQUEST['action']) . '.php';
}

get_header(); ?>

<?php hybrid_before_content(); // Before content hook ?>

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<?php //hybrid_before_entry(); // Before entry hook ?>
			<script  language="javascript" type="text/javascript" >
			
			</script>
			<div class="body-content">
				
				<div class="managed-content">
					<?php the_content() ?>
					<div class="clear"></div>
				</div>
				<!--/development-search/<?= $action_page ?> class="devsearch-panel"-->
				<iframe id="searchframe" height="1500" width="633" border="0" frameborder="0" scrolling="no"  src="http://wdcepapp.webarchives.com"></iframe>
   	  	  	</div>
			<?php hybrid_after_entry(); // After entry hook ?>
			
			<?php endwhile; ?>

		<?php else: ?>

			<p class="no-data">
				<?php _e( 'Sorry, no posts matched your criteria.', 'hybrid' ); ?>
			</p><!-- .no-data -->

		<?php endif; ?>

		<?php //hybrid_after_content(); // After content hook ?>

<?php get_footer(); ?>