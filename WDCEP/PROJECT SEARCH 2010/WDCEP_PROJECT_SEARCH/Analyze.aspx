﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Analyze.aspx.vb" Inherits="Analyze" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<!DOCTYPE html>

<html >
<asp:Literal id="htmltop" runat=server></asp:Literal>
<head>

    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

<script src="http://cdn.jquerytools.org/1.2.2/full/jquery.tools.min.js" type="text/javascript"></script>


<link rel="alternate" type="application/rss+xml" title="WDCEP &raquo; Feed" href="http://wdcep.com/?feed=rss2" />
<link rel="alternate" type="application/rss+xml" title="WDCEP &raquo; Comments Feed" href="http://wdcep.com/?feed=comments-rss2" />
<link rel="alternate" type="application/rss+xml" title="WDCEP &raquo; Home Comments Feed" href="http://wdcep.com/?feed=rss2&#038;page_id=10977" />
<link rel="stylesheet" id="flickr-gallery-css"  href='http://wdcep.com/wp-content/plugins/flickr-gallery/flickr-gallery.css' type='text/css' media='all' />
<link rel='stylesheet' id='fg-jquery-ui-css'  href='http://wdcep.com/wp-content/plugins/flickr-gallery/tab-theme/jquery-ui-1.7.3.css' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-flightbox-css'  href='http://wdcep.com/wp-content/plugins/flickr-gallery/flightbox/jquery.flightbox.css' type='text/css' media='all' />
<link rel='stylesheet' id='tubepress-css'  href='http://wdcep.com/wp-content/plugins/tubepress/src/main/web/css/tubepress.css' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='http://wdcep.com/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' />
<link rel='stylesheet' id='se-link-styles-css'  href='http://wdcep.com/wp-content/plugins/search-everything/static/css/se-styles.css' type='text/css' media='all' />
<link rel='stylesheet' id='wp-pagenavi-css'  href='http://wdcep.com/wp-content/plugins/wp-pagenavi/pagenavi-css.css' type='text/css' media='all' />
<link rel='stylesheet' id='parent-style-css'  href='http://wdcep.com/wp-content/themes/bones/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='http://wdcep.com/wp-content/themes/bones-child/library/css/font-awesome.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='framework-css'  href='http://wdcep.com/wp-content/themes/bones-child/library/css/gridle_framework.css' type='text/css' media='all' />
<link rel='stylesheet' id='googleFonts-css'  href='http://fonts.googleapis.com/css?family=Lato%3A400%2C700%2C400italic%2C700italic' type='text/css' media='all' />
<link rel='stylesheet' id='tablepress-default-css'  href='http://wdcep.com/wp-content/plugins/tablepress/css/default.min.css' type='text/css' media='all' />
<style id='tablepress-default-inline-css' type='text/css'>
    
.tablepress-id-1 th,.tablepress-id-1 .sorting{background-color:#effaea!important}.tablepress-id-1 .row-3 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-5 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-7 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-9 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-11 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-13 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-15 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-17 td{background-color:#f1f2f2!important}.tablepress-id-1 .column-3{text-align:right}.tablepress-id-1 .column-4{text-align:center}.tablepress-id-2 th,.tablepress-id-2 .sorting{background-color:#fc9!important}.tablepress-id-2 .row-2 td{background-color:#f1f2f2!important}.tablepress-id-2 .row-40 td{background-color:#f1f2f2!important}.tablepress-id-2 .row-43 td{background-color:#f1f2f2!important}.tablepress-id-2 .row-48 td{background-color:#f1f2f2!important}.tablepress-id-2 .row-53 td{background-color:#f1f2f2!important}.tablepress-id-2 .column-2{text-align:right}.tablepress-id-3 th,.tablepress-id-3 .sorting{background-color:#b2e599!important}.tablepress-id-3 .row-3 td{background-color:#f1f2f2!important}.tablepress-id-3 .row-5 td{background-color:#f1f2f2!important}.tablepress-id-3 .row-7 td{background-color:#f1f2f2!important}.tablepress-id-3 .row-9 td{background-color:#f1f2f2!important}.tablepress-id-3 .column-2{text-align:right}.tablepress-id-3 .column-3{text-align:right}.tablepress-id-4 th,.tablepress-id-4 .sorting{background-color:#efe5ea!important}.tablepress-id-4 .row-3 td{background-color:#f1f2f2!important}.tablepress-id-4 .row-5 td{background-color:#f1f2f2!important}.tablepress-id-4 .column-3{text-align:center}.tablepress-id-4 .column-4{text-align:center}.tablepress-id-5 th,.tablepress-id-5 .sorting{background-color:#d1d3d4!important}.tablepress-id-5 .row-3 td{background-color:#d1d3d4!important}.tablepress-id-5 .row-10 td{background-color:#d1d3d4!important}.tablepress-id-5,.tablepress-id-5 td,.tablepress-id-5 th{border:none!important;border-collapse:collapse!important;border-spacing:0!important}.tablepress-id-6 th,.tablepress-id-6 .sorting{background-color:#d1d3d4!important}.tablepress-id-6 .row-1 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-3 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-5 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-7 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-9 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-11 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-13 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-15 td{background-color:#d1d3d4!important}.tablepress-id-6 .column-3{text-align:right}.tablepress-id-6 .column-1{width:110px}.tablepress-id-6 .column-2{width:360px}.tablepress-id-6,.tablepress-id-6 td,.tablepress-id-6 th{border:none!important;border-collapse:collapse!important;border-spacing:0!important}.tablepress-id-7 th,.tablepress-id-7 .sorting{background-color:#fff!important}.tablepress-id-7 .column-1{width:110px}.tablepress-id-7 .column-2{width:200px}.tablepress-id-7 .column-3{width:10px}.tablepress-id-7 .column-4{width:110px}.tablepress-id-7 .column-5{width:200px}.tablepress-id-7,.tablepress-id-7 td,.tablepress-id-7 th{border:none!important;border-collapse:collapse!important;border-spacing:0!important}.tablepress-id-11 th,.tablepress-id-2 .sorting{background-color:#fc9!important}.tablepress-id-11 .column-2{text-align:left}.tablepress-id-11 .column-3{text-align:center}.tablepress-id-11 .column-4{text-align:center}
.button:hover, .button:focus {
    color: #80cec2;
}
.button:hover, .button:focus {
    color: #80cec2;
}
.button:hover, .button:focus {
    color: #80cec2;
}
</style>
</style>
    <title>WDCEP Project Search - Analysis</title>
</head>
<body style="margin-left: 0; margin-top: 0; margin-bottom: 0">
    <form id="form1" action="Result.aspx" runat="server">
   

    <div class="home-curated-content-feeds" style="width:950px; margin-left:200px; margin-top:0px; padding-top:0px">
        <div class="container" style="width:100%; margin-top:0px; padding-top:0px">
       
        
        <!-- left panel -->
        <div class="grid-8 parent grid-mobile-12" style="width:100%; margin-top:-50px">

        <br />
        <div class="breadcrumbs" style="z-index:100000; margin-top:0px" >
                <!-- Breadcrumb NavXT 5.2.2 -->
<span typeof="v:Breadcrumb"><a class="home" style="color: #bfbfbf;    font-weight: 700;     letter-spacing: 0.08em;    text-align: left;      text-transform: uppercase;    transition: all 0.4s ease 0s;" href="default.aspx" title="Go to Search." property="v:title" rel="v:url">Search</a></span> <span style="color: #bfbfbf;    font-weight: 700;     letter-spacing: 0.08em;    text-align: left;      text-transform: uppercase;    transition: all 0.4s ease 0s;"> &gt; </span> <span typeof="v:Breadcrumb"><a class="post post-page" style="color: #bfbfbf;    font-weight: 700;     letter-spacing: 0.08em;    text-align: left;      text-transform: uppercase;    transition: all 0.4s ease 0s;" href="result.aspx" title="Go to Results." property="v:title" rel="v:url">Results</a></span> <span style="color: #bfbfbf;    font-weight: 700;     letter-spacing: 0.08em;    text-align: left;      text-transform: uppercase;    transition: all 0.4s ease 0s;"> &gt; </span> <span typeof="v:Breadcrumb"></span> <span typeof="v:Breadcrumb"><span property="v:title" style="color: #bfbfbf;    font-weight: 700;     letter-spacing: 0.08em;    text-align: left;      text-transform: uppercase;    transition: all 0.4s ease 0s;">Analysis</span></span>            </div>
<br />
    <div style="width: 623px; padding: 5px; color: #4d4e4e;    font-size: 16px;    font-weight: 300;    letter-spacing: 0.024em;    line-height: 24px;    margin-bottom: 16px;">
  
        <table cellpadding=4 cellspacing=2 style=" color: #4d4e4e;    font-size: 16px;    font-weight: 300;    letter-spacing: 0.024em;    line-height: 24px;    margin-bottom: 16px;">
            <tr style="font-weight: bold;">
                <td width="223" class="auto-style1">
                    Search Results Analysis
                </td>
                <td width="200" class="auto-style1" align="left">
                    Number of Projects
                </td>
                <td width="200" class="auto-style1"  align="left">
                    Totals
                </td>
            </tr>
            <tr style="background-color: White; color: Black; font-weight: bold;">
                <td class="auto-style2">
                    Summar<strong>y</strong>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <asp:Literal ID="literalAGrid" runat="server"></asp:Literal>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="background-color: White; color: Black; font-weight: bold;">
                <td class="auto-style3">
                    <strong>Construction Type</strong>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <asp:Literal ID="literalBGrid" runat="server"></asp:Literal>
            <tr style="background-color: White; color: Black; font-weight: bold;">
                <td class="auto-style2">
                    Project Components
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <asp:Literal ID="literalCGrid" runat="server"></asp:Literal>
            <tr style="background-color: White; color: Black; font-weight: bold;">
                <td class="auto-style2" style="height: 25px">
                    Construction Status
                </td>
                <td style="height: 18px">
                </td>
                <td style="height: 18px">
                </td>
            </tr>
            <asp:Literal ID="literalDGrid" runat="server"></asp:Literal>
            <tr style="background-color: White; color: Black; font-weight: bold;">
                <td class="auto-style2">
                    LEED Level
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <asp:Literal ID="literalEGrid" runat="server"></asp:Literal>
        </table>
        *Total may include other components of mixed-use projects. <br /><br />
          <a class="button" style="background: white none repeat scroll 0 0;" href="Result.aspx">Back to Search Results </a> <a class="button" style="background: white none repeat scroll 0 0;" href="#"  >Export analysis </a>&nbsp;<a class="button" style="background: white none repeat scroll 0 0;" href="Default.aspx"> New Search </a>
   

    </div>
    

    


          </div> <!-- end of left panel -->


        </div>
    </div>

 
       </form>


   <asp:Literal id="htmlbottom" runat=server></asp:Literal>  
 <script type="text/javascript">
     var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
     document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));


     function setCompletedDropdownstatus(bval) {
         if (bval) {
             var _DropDownListcompletedyearfrom = document.getElementById("DropDownListcompletedyearfrom")
             _DropDownListcompletedyearfrom.disabled = false;
             var _DropDownListcompletedyearto = document.getElementById("DropDownListcompletedyearto")
             _DropDownListcompletedyearto.disabled = false;
         } else {
             var _DropDownListcompletedyearfrom = document.getElementById("DropDownListcompletedyearfrom")
             _DropDownListcompletedyearfrom.disabled = true;
             var _DropDownListcompletedyearto = document.getElementById("DropDownListcompletedyearto")
             _DropDownListcompletedyearto.disabled = true;
         }



     }

     function GetCookie(name) {
         var nameEQ = name + "=";
         var ca = document.cookie.split(';');
         for (var i = 0; i < ca.length; i++) {
             var c = ca[i];
             while (c.charAt(0) == ' ') c = c.substring(1, c.length);
             if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
         }
         return null;
     }

     function SetCookie(name, value) {

         var days = 300
         var date = new Date();
         date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
         var expires = "; expires=" + date.toGMTString();
         document.cookie = name + "=" + escape(value) + expires + "; path=/";
         //document.cookie=name+ "=" +escape(value)+((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
     }

     function DeleteCookie(name) {

         var exp = new Date();
         exp.setTime(exp.getTime() - 1);
         var cval = GetCookie(name);
         document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
     }


     function switchPage(i) {

         searchCallback.callback('paging', i);
     };
     function sortSearch(column) {

         searchCallback.callback('sort', column);
     };
     function filterSearch(filter) {

         searchCallback.callback('filter', filter);
     };
     function getcsv() {

         searchCallback.callback('getcsv');
     };    

    
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-2739433-5");
        pageTracker._trackPageview();
    } catch (err) { }</script>
</body>
</html>
