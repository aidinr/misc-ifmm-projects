﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.ComponentModel
Imports System.Xml

Partial Class Property_Detail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If InStr(Request.Browser.Type.ToLower, "firefox") > 0 Then
            'literalcssplusmod1.text = "margin-left: 436px;"
        End If
        'Get Main HTML Template

        htmltop.Text = Functions.GetMainHTMLWrapper(Request.QueryString("htmlcache"))(0)
        htmlbottom.Text = Functions.GetMainHTMLWrapper(Request.QueryString("htmlcache"))(2)

        Dim theProjectID = Request.QueryString("id")



        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select * from ipm_project where projectid = @projectid", MyConnection)
        Dim userid As New SqlParameter("@projectid", SqlDbType.Decimal)
        MyCommand1.SelectCommand.Parameters.Add(userid)
        MyCommand1.SelectCommand.Parameters("@projectid").Value = theProjectID
        Dim DT As New DataTable("Project")
        MyCommand1.Fill(DT)

        If (DT.Rows.Count > 0) Then
            literalProjectCrumbs.Text = DT.Rows(0)("name").ToString.Trim

            'Page.Header.Title = "WDCEP - Property Detail - " & DT.Rows(0)("name").ToString.Trim & " - " & DT.Rows(0)("city").ToString.Trim & ", " & DT.Rows(0)("state_id").ToString.Trim

            literalProjectName.Text = DT.Rows(0)("name").ToString.Trim
            'literalProjectDescription.Text = DT.Rows(0)("description").ToString.Trim.Replace("vblf", "<br />")
            'literalProjectLocation.Text = DT.Rows(0)("city").ToString.Trim & ", " & DT.Rows(0)("state_id").ToString.Trim & " " & DT.Rows(0)("zip").ToString.Trim
            literalProjectAddress.Text = getUDFbyName("WDCEP_Address", theProjectID, False, "") 'DT.Rows(0)("address") & "<br />" & DT.Rows(0)("city").ToString.Trim & ", " & DT.Rows(0)("state_id").ToString.Trim & " " & DT.Rows(0)("zip").ToString.Trim
            'hyperlinkProjectMap.NavigateUrl = "http://maps.google.com/maps?f=d&daddr=" & DT.Rows(0)("address") & " " & DT.Rows(0)("city").ToString.Trim & " " & DT.Rows(0)("state_id").ToString.Trim & " " & DT.Rows(0)("zip").ToString.Trim




            literaltotalsqft.Text = FormatNumber(getUDFbyName("WDCEP_TotalSF", theProjectID, False, "", True))
            If getUDFbyName("WDCEP_TotalSF", theProjectID, False, "", True) <> "" Then
                literaltotalsqft.Text = "<div class=""section_content"" ><table cellspacing=1 cellpadding=0><tr><td width=226 valign=top>Total SF: </td><td width=657 valign=top><b>" & Left(FormatNumber(getUDFbyName("WDCEP_TotalSF", theProjectID, False, "", True)), FormatNumber(getUDFbyName("WDCEP_TotalSF", theProjectID, False, "", True)).IndexOf(".")) & "</b></td></tr></table></div>"
            End If

            literalward.Text = getUDFbyName("WDCEP_Ward", theProjectID, True, "Ward")
            If getUDFbyName("WDCEP_Ward", theProjectID, False, "Ward") <> "" Then
                literalward.Text = "<div class=""section_content"" ><table cellspacing=1 cellpadding=0><tr><td width=226 valign=top>Ward: </td><td width=657 valign=top><b>" & getUDFbyName("WDCEP_Ward", theProjectID, False, "") & "</b></td></tr></table></div>"
            End If
        

            literalmajoruse.Text = getUDFbyName("WDCEP_majoruse", theProjectID, True, "Major Use")
            If getUDFbyName("WDCEP_majoruse", theProjectID, False, "Major Use") <> "" Then
                literalmajoruse.Text = "<div class=""section_content"" ><table cellspacing=1 cellpadding=0><tr><td width=226 valign=top>Major Use: </td><td width=657 valign=top><b>" & getUDFbyName("WDCEP_majoruse", theProjectID, False, "") & "</b></td></tr></table></div>"
            End If
            literalstatus.Text = Replace(Replace(getUDFbyName("WDCEP_statuslu", theProjectID, True, "Status"), "(over two years)", ""), "(past two years)", "")
            If getUDFbyName("WDCEP_statuslu", theProjectID, False, "Status") <> "" Then
                literalstatus.Text = "<div class=""section_content"" ><table cellspacing=1 cellpadding=0><tr><td width=226 valign=top>Status: </td><td width=657 valign=top><b>" & Replace(Replace(getUDFbyName("WDCEP_statuslu", theProjectID, False, ""), "(over two years)", ""), "(past two years)", "") & "</b></td></tr></table></div>"
            End If
            literalleed.Text = getUDFbyName("WDCEP_leedlu", theProjectID, True, "LEED")
            If getUDFbyName("WDCEP_leedlu", theProjectID, False, "LEED") <> "" Then
                literalleed.Text = "<div class=""section_content"" ><table cellspacing=1 cellpadding=0><tr><td width=226 valign=top>LEED: </td><td width=657 valign=top><b>" & getUDFbyName("WDCEP_leedlu", theProjectID, False, "") & "</b></td></tr></table></div>"
            End If
            literalestimatedcost.Text = getUDFbyName("WDCEP_TotalProjectCost", theProjectID, True, "Estimated Project Cost", True, "$")
            If getUDFbyName("WDCEP_TotalProjectCost", theProjectID, False, "Estimated Project Cost", True) <> "" Then
                literalestimatedcost.Text = "<div class=""section_content"" ><table cellspacing=1 cellpadding=0><tr><td width=226 valign=top>Estimated Project Cost: </td><td width=657 valign=top><b>$" & FormatNumber(getUDFbyName("WDCEP_TotalProjectCost", theProjectID, False, "Estimated Project Cost")) & "</b></td></tr></table></div>"
            End If
            literaldescription.Text = getUDFbyName("WDCEP_Description", theProjectID, False, "")

            literalURL.Text = "<br/><br/>" & getUDFbyName("WDCEP_URL", theProjectID, False, "")

            projectImage1.ImageUrl = Session("WSRetreiveAsset") & "qfactor=25&width=923&height=451&crop=2&size=2&type=project&cropcolor=lightgray&id=" & theProjectID
            projectImage1Thumb.ImageUrl = Session("WSRetreiveAsset") & "qfactor=25&width=84&height=84&crop=1&size=1&type=project&id=" & theProjectID
            hyperlinkImage1.NavigateUrl = (Session("WSRetreiveAsset") & "qfactor=25&size=1&width=800&height=800&type=project&id=" & theProjectID).ToString.Replace("(", "%28").Replace(")", "%29")
            hyperlinkImage1.Attributes.Add("rel", "lightbox")
            hyperlinkImage1.Attributes.Add("title", DT.Rows(0)("name").ToString.Trim)

            hyperlinkImage1Thumb.NavigateUrl = "javascript:switchImage('" & Session("WSRetreiveAsset") & "qfactor=25&width=923&height=451&crop=2&size=2&type=project&cropcolor=lightgray&id=" & theProjectID & "'," & theProjectID & ",'asset','" & DT.Rows(0)("name").ToString.Trim & "');"




            Dim MyConnection2 As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
            Dim MyCommand12 As SqlDataAdapter = New SqlDataAdapter("select name,asset_id from ipm_asset where projectid = @projectid and available = 'Y' and media_type in (select media_type from ipm_filetype_lookup where convertertype = 1) and Asset_ID in (select Asset_ID from IPM_ASSET_SERVICES where keyid = 2448933) order by name", MyConnection)
            Dim userid2 As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
            MyCommand12.SelectCommand.Parameters.Add(userid2)
            MyCommand12.SelectCommand.Parameters("@projectid").Value = theProjectID
            Dim dtAssets As New DataTable("Assets")
            MyCommand12.Fill(dtAssets)

            If dtAssets.Rows.Count = 0 Then

                Dim MyConnection5 As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
                Dim MyCommand15 As SqlDataAdapter = New SqlDataAdapter("select name, ipm_asset.asset_id, case when ltrim(rtrim(isnull(v.item_value,''))) = '' then 999999 else convert(int, v.item_value) end SortOrder from ipm_asset left outer join ipm_asset_field_value v on v.item_id = 2455763 and v.asset_id = ipm_asset.asset_id  where projectid = @projectid and available = 'Y' and media_type in (select media_type from ipm_filetype_lookup where convertertype = 1) order by case when ltrim(rtrim(isnull(v.item_value,''))) = '' then 999999 else convert(int, v.item_value) end asc ", MyConnection)
                Dim userid5 As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
                MyCommand15.SelectCommand.Parameters.Add(userid5)
                MyCommand15.SelectCommand.Parameters("@projectid").Value = theProjectID
                'Response.Write("theProjectID:" & theProjectID & " ---  select name, ipm_asset.asset_id, case when ltrim(rtrim(isnull(v.item_value,''))) = '' then 999999 else convert(int, v.item_value) end SortOrder from ipm_asset left outer join ipm_asset_field_value v where v.item_id = 2455763 and v.asset_id = ipm_asset.asset_id  where projectid = @projectid and available = 'Y' and media_type in (select media_type from ipm_filetype_lookup where convertertype = 1) order by case when ltrim(rtrim(isnull(v.item_value,''))) = '' then 999999 else convert(int, v.item_value) end asc ")
                MyCommand15.Fill(dtAssets)
            End If
            'select * from ipm_asset where projectid = @projectid and available = 'Y' and media_type in (select media_type from ipm_filetype_lookup where convertertype = 1) and Asset_ID in (select Asset_ID from IPM_ASSET_SERVICES where keyid = 2448933) order by name
            'select * from ipm_asset where projectid = @projectid and available = 'Y' and media_type in (select media_type from ipm_filetype_lookup where convertertype = 1) 

            'For Each x As DataColumn In dtAssets.Columns
            '    Console.WriteLine(x.ColumnName)
            'Next



            Dim dtGalleryAssets As DataTable = New DataTable
            dtGalleryAssets.Columns.Add("name")
            dtGalleryAssets.Columns.Add("asset_id")

            For Each x As DataRow In dtAssets.Rows
                If 1 = 1 Then '(x("catname") = "Property Images") Then
                    Dim r As DataRow = dtGalleryAssets.NewRow
                    r("name") = x("name")
                    r("asset_id") = x("asset_id")
                    dtGalleryAssets.Rows.Add(r)
                End If
            Next



            RepeaterGalleryThumbs.DataSource = dtGalleryAssets
            RepeaterGalleryThumbs.DataBind()


            Dim _projectarchitects As String = ""
            _projectarchitects = getUDFbyName("WDCEP_Architect1LU", theProjectID, False, "") & " " & getUDFbyName("WDCEP_Architect2LU", theProjectID, False, "", , "| ") & " " & getUDFbyName("WDCEP_Architect3LU", theProjectID, False, "", , "| ")
            If _projectarchitects.Trim <> "" Then
                ltrlprojectarchitects.Text = "<div class=""section_content"" ><table cellspacing=1 cellpadding=0><tr><td width=226 valign=top>Architect(s): </td><td width=657 valign=top><b>" & _projectarchitects & "</b></td></tr></table></div>"
            End If

            Dim _projectdeveloper As String = ""
            _projectdeveloper = getUDFbyName("WDCEP_Developer1LU", theProjectID, False, "") & " " & getUDFbyName("WDCEP_Developer2LU", theProjectID, False, "", , "| ") & " " & getUDFbyName("WDCEP_Developer3LU", theProjectID, False, "", , "| ") & " " & getUDFbyName("WDCEP_Developer4LU", theProjectID, False, "", , "| ")
            If _projectdeveloper.Trim <> "" Then
                ltrlprojectdeveloper.Text = "<div class=""section_content"" ><table cellspacing=1 cellpadding=0><tr><td width=226 valign=top>Developer(s): </td><td width=657 valign=top><b>" & _projectdeveloper & "</b></td></tr></table></div>"
            End If

            Dim _projectcontractors As String = ""
            _projectcontractors = getUDFbyName("WDCEP_Contractor1LU", theProjectID, False, "") & " " & getUDFbyName("WDCEP_Contractor2LU", theProjectID, False, "", , "| ")
            If _projectcontractors.Trim <> "" Then
                ltrlprojectcontractors.Text = "<div class=""section_content"" ><table cellspacing=1 cellpadding=0><tr><td width=226 valign=top>Contractor(s): </td><td width=657 valign=top><b>" & _projectcontractors & "</b></td></tr></table></div>"
            End If





            'get ordering
            Dim sorderedgroups As String() = System.Configuration.ConfigurationManager.AppSettings("projectdetailordering").Split(",")
            For Each Group As String In sorderedgroups

                literalProjectDetail.Text += getProjectDetails(theProjectID, Group)
                If Group = "TIMELINE" Then
                    'add status
                    'if group heading 
                    If InStr(literalProjectDetail.Text.ToUpper, ">TIMELINE</DIV>") > 0 Then
                        literalProjectDetail.Text += "<div class=""section_content"" ><table cellspacing=1 cellpadding=0><tr><td width=226 valign=top>Status: </td><td width=657 valign=top><b>" & Replace(getUDFbyName("WDCEP_statuslu", theProjectID, False, ""), "(over two years)", "") & "</b></td></tr></table></div>"
                    Else
                        literalProjectDetail.Text += "<div class=""heading_text"" >TIMELINE</div><div class=""section_content"" ><table cellspacing=1 cellpadding=0><tr><td width=226 valign=top>Status: </td><td width=657 valign=top><b>" & Replace(getUDFbyName("WDCEP_statuslu", theProjectID, False, ""), "(over two years)", "") & "</b></td></tr></table></div>"
                    End If

                End If

            Next

            literalStatusDate.Text = getUDFbyName("WDCEP_StatusDate", theProjectID, False, "")


        End If


    End Sub


    Function getUDFbyName(ByVal udfname As String, ByVal projectid As String, ByVal addhtml As Boolean, ByVal formattedtitle As String, Optional ByVal bformatnumber As Boolean = False, Optional ByVal valprefix As String = "") As String
        Dim rstring As String = ""
        Dim MyConnection2 As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand12 As SqlDataAdapter = New SqlDataAdapter("select Item_Name, Item_Tab, item_value, item_group from IPM_PROJECT_FIELD_DESC a, IPM_PROJECT_FIELD_VALUE b where a.item_tag = @itemname and a.Item_ID = b.Item_ID and b.ProjectID = @projectid and Item_Value <> '' and Item_Value <> 'false' and Item_Value <> 'true' and Item_Value <> '0' order by Item_Tab,Item_Group, item_name ", MyConnection2)
        Dim userid2 As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        MyCommand12.SelectCommand.Parameters.Add(userid2)
        MyCommand12.SelectCommand.Parameters("@projectid").Value = projectid

        Dim userid3 As New SqlParameter("@itemname", SqlDbType.VarChar, 255)
        MyCommand12.SelectCommand.Parameters.Add(userid3)
        MyCommand12.SelectCommand.Parameters("@itemname").Value = udfname


        Dim dtProjectd As New DataTable("ProjectInfo")
        MyCommand12.Fill(dtProjectd)
        If dtProjectd.Rows.Count > 0 Then
            If addhtml Then
                'Total Sq. Ft.: <span class="auto-style1"><strong><asp:Literal ID="literaltotalsqft" runat="server"></asp:Literal><br /></strong></span>
                Dim itemvalue As String = dtProjectd.Rows(0)("item_value")
                If bformatnumber Then
                    itemvalue = valprefix & FormatNumber(dtProjectd.Rows(0)("item_value"), 0).ToString.Trim
                End If
                Return formattedtitle & ":   <span class=""auto-style1""><strong>" & itemvalue & "</strong></span><br>"
            Else
                Return valprefix & dtProjectd.Rows(0)("item_value")
            End If

        End If
    End Function
    Function getProjectDetails(ByVal id As String, ByVal sgroup As String) As String
        Dim rstring As String = ""
        Dim MyConnection2 As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand12 As SqlDataAdapter = New SqlDataAdapter("select Item_Name, Item_Tab, item_value, item_group from IPM_PROJECT_FIELD_DESC a, IPM_PROJECT_FIELD_VALUE b where a.Item_ID = b.Item_ID and b.ProjectID = @projectid and Item_Value <> '' and Item_Value <> 'false' and  Item_Value <> '0' and viewable = 1 and item_group = '" & sgroup & "' order by Item_Tab,Item_Group, item_name ", MyConnection2)
        Dim userid2 As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        MyCommand12.SelectCommand.Parameters.Add(userid2)
        MyCommand12.SelectCommand.Parameters("@projectid").Value = id
        Dim dtProjectd As New DataTable("ProjectInfo")
        MyCommand12.Fill(dtProjectd)

        Dim currheading As String = ""
        For Each row In dtProjectd.Rows
            If currheading <> row("item_group") Then
                rstring += "<div class=""heading_text"" ><b>" & row("item_group") & "</b></div>"
                currheading = row("item_group")
            End If
            Dim sValue As String = ""

            ''If IsDate(row("item_value")) And (InStr(row("item_name"), "delivery", CompareMethod.Text) >= 1 Or InStr(row("item_name"), "delivery", CompareMethod.Text) >= 1) Then

            ''End If
            'trap "residential units"


            If row("item_name") <> "Residential Units" And row("item_name") <> "Apartments" And row("item_name") <> "Home Ownership" Then

                If IsNumeric(row("item_value")) And InStr(row("item_name"), "zip", CompareMethod.Text) = 0 Then
                    sValue = FormatNumber(row("item_value"), 0)
                    If InStr(row("item_name"), "cost", CompareMethod.Text) > 0 Or InStr(row("item_name"), "valuation", CompareMethod.Text) > 0 Then
                        sValue = "$" & sValue
                    Else
                        If InStr(row("item_name"), "Use Change", CompareMethod.Text) > 0 Or InStr(row("item_name"), "Green Community", CompareMethod.Text) > 0 Then
                            If sValue = 1 Then
                                sValue = "Yes"
                            End If
                        End If
                    End If
                Else
                    If IsDate(row("item_value")) And (InStr(row("item_name"), "delivery", CompareMethod.Text) > 0 Or InStr(row("item_name"), "Groundbreaking", CompareMethod.Text) > 0) Then
                        sValue = Year(row("item_value"))
                    Else
                        sValue = row("item_value")
                    End If
                End If
                rstring += "<div class=""section_content"" ><table cellspacing=1 cellpadding=0><tr><td width=226 valign=top>" & row("item_name").replace("LU", "") & ": </td><td width=657 valign=top><b>" & sValue & "</b></td></tr></table></div>"
            Else
                'sub in apartment items
                If row("item_name") = "Residential Units" Then

                    sValue = FormatNumber(row("item_value"), 0)
                    rstring += "<div class=""section_content"" ><table cellspacing=1 cellpadding=0><tr><td width=226 valign=top>" & row("item_name").replace("LU", "") & ": </td><td width=657 valign=top><b>" & sValue & "</b></td></tr></table></div>"

                    'get home ownership and apartments

                    Dim MyConnection3 As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
                    Dim MyCommand13 As SqlDataAdapter = New SqlDataAdapter("select Item_Name, Item_Tab, item_value, item_group from IPM_PROJECT_FIELD_DESC a, IPM_PROJECT_FIELD_VALUE b where a.Item_ID = b.Item_ID and b.ProjectID = @projectid and Item_Value <> '' and Item_Value <> 'false' and Item_Value <> 'true' and Item_Value <> '0' and viewable = 1 and item_group = '" & sgroup & "' order by Item_Tab,Item_Group, item_name ", MyConnection2)
                    Dim userid3 As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
                    MyCommand13.SelectCommand.Parameters.Add(userid3)
                    MyCommand13.SelectCommand.Parameters("@projectid").Value = id
                    Dim dtProjectdtmp As New DataTable("ProjectInfo")
                    MyCommand13.Fill(dtProjectdtmp)
                    For Each rowtmp As DataRow In dtProjectdtmp.Rows
                        If rowtmp("item_name") = "Home Ownership" Or rowtmp("item_name") = "Apartments" Then
                            sValue = FormatNumber(rowtmp("item_value"), 0)
                            rstring += "<div class=""section_content_sub"" ><table cellspacing=1 cellpadding=0><tr><td width=116 valign=top>" & rowtmp("item_name").replace("LU", "") & ": </td><td width=467 valign=top><b>" & sValue & "</b></td></tr></table></div>"
                        End If
                    Next
                End If
            End If




        Next
        Return rstring
    End Function
End Class
