﻿Imports System.Data.SqlClient
Imports System.Data

Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

Imports System.IO
Imports System.Text
Imports WebSupergoo.ABCpdf7
Imports WebSupergoo.ABCpdf7.Objects
Imports WebSupergoo.ABCpdf7.Atoms
Imports WebSupergoo.ABCpdf7.Operations



Partial Class Analyze
    Inherits System.Web.UI.Page



    Protected Sub Analyze_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load





        If Request.QueryString("export") = "pdf" Then
            GeneratePDF()
            Exit Sub
        End If


        'Get Main HTML Template

        htmltop.Text = Functions.GetMainHTMLWrapper(Request.QueryString("htmlcache"))(0)
        htmlbottom.Text = Functions.GetMainHTMLWrapper(Request.QueryString("htmlcache"))(2)


        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(GetSQLTotals(MakeSQL(False)), MyConnection)
        Dim userid As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Session("searchFilter").Replace("'", "''") & "%"
        Dim DT1 As New DataTable("Projects")
        MyCommand1.Fill(DT1)

        literalAGrid.Text += "<tr><td>Total Number of Projects</td><td class=""auto-style4"">" & FormatNumber(DT1.Rows(0)("total"), 0) & "</td><td class=""auto-style4""></td></tr>"
        literalAGrid.Text += "<tr><td>Total Sq. Ft.*</td><td class=""auto-style4""></td><td class=""auto-style5"">" & FormatNumber(DT1.Rows(0)("totalsf"), 0) & "</td></tr>"
        literalAGrid.Text += "<tr><td>Total Estimated Cost*</td><td class=""auto-style4""></td><td class=""auto-style5"">$" & FormatNumber(DT1.Rows(0)("totalcost"), 0) & "</td></tr>"


        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        MyCommand1 = New SqlDataAdapter(GetConstructionTypeTotals(MakeSQL(False)), MyConnection)
        Dim userid2 As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid2)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Session("searchFilter").Replace("'", "''") & "%"
        Dim DT2 As New DataTable("Projects")
        MyCommand1.Fill(DT2)

        For Each row In DT2.Rows
            literalBGrid.Text += "<tr><td>" & row("item_value") & "</td><td class=""auto-style4"">" & FormatNumber(row("totals"), 0) & "</td><td class=""auto-style4""></td></tr>"
        Next

        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        MyCommand1 = New SqlDataAdapter(MakeSQL(False), MyConnection)
        Dim userid3 As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid3)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Session("searchFilter").Replace("'", "''") & "%"
        Dim DT3 As New DataTable("Projects")
        MyCommand1.Fill(DT3)

        'get projectid list
        Dim _projectlist As String = ""
        For Each row As DataRow In DT3.Rows
            _projectlist += row("projectid") & ","
        Next
        'trim last ","
        _projectlist = _projectlist.Substring(0, _projectlist.Length - 1)

        Dim _community As DataTable = GetComponentTotals(2400022, 2400023, _projectlist)
        If _community.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Community</td><td class=""auto-style4"">" & FormatNumber(_community.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_community.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If
        Dim _education As DataTable = GetComponentTotals(2400025, 2400027, _projectlist)
        If _education.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Education</td><td class=""auto-style4"">" & FormatNumber(_education.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_education.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If
        Dim _entertainment As DataTable = GetComponentTotals(2400028, 2400029, _projectlist)
        If _entertainment.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Entertainment</td><td class=""auto-style4"">" & FormatNumber(_entertainment.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_entertainment.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If
        Dim _Hotel As DataTable = GetComponentTotals(2400031, 2400033, _projectlist)
        If _Hotel.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Hotel</td><td class=""auto-style4"">" & FormatNumber(_Hotel.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_Hotel.Rows(0)("value"), 0) & " rooms</td></tr>"
        End If
        Dim _industrial As DataTable = GetComponentTotals(2400034, 2400035, _projectlist)
        If _industrial.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Industrial</td><td class=""auto-style4"">" & FormatNumber(_industrial.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_industrial.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If
        Dim _Medical As DataTable = GetComponentTotals(2400036, 2400037, _projectlist)
        If _Medical.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Medical</td><td class=""auto-style4"">" & FormatNumber(_Medical.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_Medical.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If

        Dim _museum As DataTable = GetComponentTotals(2400038, 2400039, _projectlist)
        If _museum.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Museum</td><td class=""auto-style4"">" & FormatNumber(_museum.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_museum.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If

        Dim _office As DataTable = GetComponentTotals(2400040, 2400041, _projectlist)
        If _office.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Office</td><td class=""auto-style4"">" & FormatNumber(_office.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_office.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If
        Dim _residentialunits As DataTable = GetComponentTotals(2400042, 2400045, _projectlist)
        If _residentialunits.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Residential</td><td class=""auto-style4"">" & FormatNumber(_residentialunits.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_residentialunits.Rows(0)("value"), 0) & " units</td></tr>"
        End If
        Dim _retail As DataTable = GetComponentTotals(2400051, 2400052, _projectlist)
        If _retail.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Retail</td><td class=""auto-style4"">" & FormatNumber(_retail.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_retail.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If
        Dim _parking As DataTable = GetComponentTotals(2400053, 2400054, _projectlist)
        If _parking.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Parking</td><td class=""auto-style4"">" & FormatNumber(_parking.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_parking.Rows(0)("value"), 0) & " spaces</td></tr>"
        End If
        

        ''        Item_ID(Item_Tag)
        ''2400121: WDCEP_AffordableUnits()
        ''2400047: WDCEP_Apartments()
        ''2400026: WDCEP_CollegeUniversity()
        ''2400022: WDCEP_Community()
        ''2400023: WDCEP_CommunitySF()
        ''2400048: WDCEP_Condos()
        ''2400024: WDCEP_DistrictGovt()
        ''2400025: WDCEP_Education()
        ''2400027: WDCEP_EducationSF()
        ''2400028: WDCEP_Entertainment()
        ''2400029: WDCEP_EntertainmentSF()
        ''2400030: WDCEP_FederalGovt()
        ''2400049: WDCEP_HomeOwnership()
        ''2400031: WDCEP_Hotel()
        ''2400033: WDCEP_HotelRooms()
        ''2400032: WDCEP_HotelSF()
        ''2400034: WDCEP_Industrial()
        ''2400035: WDCEP_IndustrialSF()
        ''2400036: WDCEP_Medical()
        ''2400037: WDCEP_MedicalSF()
        ''2400038: WDCEP_Museum()
        ''2400039: WDCEP_MuseumSF()
        ''2400046: WDCEP_NewUnits()
        ''2400040: WDCEP_Office()
        ''2400041: WDCEP_OfficeSF()
        ''2400044: WDCEP_Ownership()
        ''2400053: WDCEP_Parking()
        ''2400097: WDCEP_ParkingSF()
        ''2400054: WDCEP_ParkingSpaces()
        ''2400043: WDCEP_Rental()
        ''2400042: WDCEP_Residential()
        ''2400050: WDCEP_ResidentialSF()
        ''2400045: WDCEP_ResidentialUnits()
        ''2400051: WDCEP_Retail()
        ''2400052: WDCEP_RetailSF()
        ''2400105: WDCEP_TheaterSeats()
        ''2400133: WDCEP_WorkforceUnits()





        'literalCGrid.Text += "<tr><td>" & row("majoruse") & "</td><td class=""auto-style4"">" & FormatNumber(row("totals"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(row("hotelrooms"), 0) & " rooms</td></tr>"





        ' ''For Each row In DT3.Rows
        ' ''    Select Case row("majoruse")
        ' ''        Case "Hospitality"
        ' ''            literalCGrid.Text += "<tr><td>" & row("majoruse") & "</td><td class=""auto-style4"">" & FormatNumber(row("totals"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(row("hotelrooms"), 0) & " rooms</td></tr>"
        ' ''        Case "Residential"
        ' ''            literalCGrid.Text += "<tr><td>" & row("majoruse") & "</td><td class=""auto-style4"">" & FormatNumber(row("totals"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(row("residentialunits"), 0) & " units</td></tr>"
        ' ''        Case Else
        ' ''            literalCGrid.Text += "<tr><td>" & row("majoruse") & "</td><td class=""auto-style4"">" & FormatNumber(row("totals"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(row("totalsf"), 0) & " sq. ft.</td></tr>"
        ' ''    End Select


        ' ''Next

        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        MyCommand1 = New SqlDataAdapter(GetConstructionStatusTotals(MakeSQL(False)), MyConnection)
        Dim userid4 As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid4)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Session("searchFilter").Replace("'", "''") & "%"
        Dim DT4 As New DataTable("Projects")
        MyCommand1.Fill(DT4)

        For Each row In DT4.Rows
            literalDGrid.Text += "<tr><td>" & row("item_value") & "</td><td class=""auto-style4"">" & FormatNumber(row("totals"), 0) & "</td><td class=""auto-style5"">$" & FormatNumber(row("totalcost"), 0) & " </td></tr>"
        Next


        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        MyCommand1 = New SqlDataAdapter(GetLeedLevelTotals(MakeSQL(False)), MyConnection)
        Dim userid5 As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid5)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Session("searchFilter").Replace("'", "''") & "%"
        Dim DT6 As New DataTable("Projects")
        MyCommand1.Fill(DT6)

        For Each row In DT6.Rows
            literalEGrid.Text += "<tr><td>" & row("item_value") & "</td><td class=""auto-style4"">" & FormatNumber(row("totals"), 0) & "</td><td class=""auto-style4""></td></tr>"
        Next




    End Sub

    Public Function MakeSQL(Optional ByVal blnUseLimiter As Boolean = True, Optional ByVal blnaddordering As Boolean = False) As String

        Return Functions.MakeSQL(Request.Cookies.Item("pstype").Value, Session("leedlevel"), Session("ward"), Session("undercontruction"), Session("completedfrom"), Session("completedto"), Session("proposed"), Session("planned"), Session("completed"), Session("newconstruction"), Session("rennovation"), Session("infrastructure"), Session("searchbyuse"), blnUseLimiter, blnaddordering, Session("nearterm"))

    End Function

    Function GetSQLTotals(ByVal sql) As String
        Return "select count(*) total, sum(cast (totalcost as decimal)) totalcost, sum(cast(totalsf as integer)) totalsf from (" & sql & ") a"
    End Function

    Function GetConstructionTypeTotals(ByVal sql) As String
        Return "select * from (select COUNT(*) totals, item_value from ipm_project a, IPM_PROJECT_FIELD_VALUE b where a.projectid = b.ProjectID and b.Item_ID = 2400016 and a.projectid in (select projectid from (" & sql & ") a) group by item_value) a where cast(Item_Value as varchar(50)) in ('New Construction','Renovation','Infrastructure')"
    End Function

    Function GetComponentTotals(ByVal _mainid As String, ByVal _mainidvalue As String, ByVal pids As String) As DataTable
        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select count(*) count, sum(CAST(case when rtrim(a.item_value) = '' then 0 when ltrim(a.item_value) = '' then 0 else isnull(a.item_value,0) end AS decimal)) value from ipm_project_field_value a, ipm_project_field_value b, IPM_PROJECT c where a.item_id = " & _mainidvalue & " and b.Item_ID =  " & _mainid & " and b.Item_value = '1' and a.ProjectID = c.ProjectID and b.ProjectID = c.projectid and c.ProjectID in (" & pids & ")", MyConnection)
        Dim DT1 As New DataTable("Projects")
        MyCommand1.Fill(DT1)
        Return DT1
    End Function


    Function GetConstructionStatusTotals(ByVal sql) As String
        Return "select COUNT(*) totals,sum (CAST(totalcost AS decimal)) totalcost,sum (CAST(totalsf AS decimal)) totalsf, replace(replace(item_value,'(over two years)',''),'(past two years)','') item_value from ipm_project a, IPM_PROJECT_FIELD_VALUE b,(" & sql & ") c where a.projectid = b.ProjectID and a.ProjectID = c.projectid and b.Item_ID = 2400017 group by replace(replace(item_value,'(over two years)',''),'(past two years)','') order by case when replace(replace(item_value,'(over two years)',''),'(past two years)','')  = 'Completed' then 1 when replace(replace(item_value,'(over two years)',''),'(past two years)','')  = 'Under Construction' then 2 when replace(replace(item_value,'(over two years)',''),'(past two years)','')  = 'Near Term' then 3 when replace(replace(item_value,'(over two years)',''),'(past two years)','')  = 'Medium Term' then 4 when replace(replace(item_value,'(over two years)',''),'(past two years)','')  = 'Long Term' then 5 end"
    End Function


    Function GetLeedLevelTotals(ByVal sql) As String
        Return "select * from (select COUNT(*) totals, item_value from ipm_project a, IPM_PROJECT_FIELD_VALUE b where a.projectid = b.ProjectID and b.Item_ID = 2400099 and a.projectid in (select projectid from (" & sql & ") a) group by item_value) a where item_value <> '' and item_value <> '0' order by case when Item_Value = 'Certified' then 1 when Item_Value = 'Silver' then 2 when Item_Value = 'Gold' then 3 when Item_Value = 'Platinum' then 4 end"
    End Function






    Public Sub GeneratePDF()

        Dim SessionID As String = Session.SessionID
        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))

        Dim theDoc As Doc = New Doc()
        theDoc.HtmlOptions.Timeout = 120000
        theDoc.HtmlOptions.PageCacheClear()
        theDoc.HtmlOptions.PageCacheEnabled = False
        theDoc.HtmlOptions.PageCachePurge()
        theDoc.HtmlOptions.BrowserWidth = 700
        theDoc.HtmlOptions.ImageQuality = 93

        theDoc.HtmlOptions.FontEmbed = True
        theDoc.HtmlOptions.FontProtection = False
        theDoc.HtmlOptions.FontSubstitute = False


        'get windows dir
        Dim windir As String = Environment.GetEnvironmentVariable("windir") & "\Fonts\"


        Call theDoc.EmbedFont(windir & "AvenirLTStd-Light.otf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "AvenirLTStd-Black.otf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "AvenirLTStd-Roman.otf", "Latin", False, True, True)

        Call theDoc.EmbedFont(windir & "AvenirLTStd-BlackOblique.otf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "AvenirLTStd-Book.otf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "AvenirLTStd-BookOblique.otf", "Latin", False, True, True)

        Call theDoc.EmbedFont(windir & "AvenirLTStd-Heavy.otf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "AvenirLTStd-HeavyOblique.otf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "AvenirLTStd-LightOblique.otf", "Latin", False, True, True)

        Call theDoc.EmbedFont(windir & "AvenirLTStd-MediumOblique.otf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "AvenirLTStd-Oblique.otf", "Latin", False, True, True)


        Dim oriW As Integer = theDoc.Rect.Width
        Dim oriH As Integer = theDoc.Rect.Height

        Dim oriX As Integer = theDoc.Rect.Left
        Dim oriY As Integer = theDoc.Rect.Bottom

        Dim theUrl As String = ""

        Dim i As Integer = 1
        Dim pageCount As Integer = 1
        Dim theID As Integer

        'Session.SessionID
        'http://wdcep.webarchives.com/(S(3qp2ayjey1pyyknpf51q1g45))/IDAM.aspx?Page=on("newconstruction") = "on" Or Session("Renovation") = "on" Or Session("Infrastructure") 
        theUrl = ConfigurationSettings.AppSettings("pdfurl") & "AnalyzePDF.aspx?searchFilter=" & Session("searchFilter") & "&pstype=" & Request.Cookies.Item("pstype").Value & "&newconstruction=" & Session("newconstruction") & "&Renovation=" & Session("Renovation") & "&Infrastructure=" & Session("Infrastructure") & "&searchbyuse=" & Session("searchbyuse") & "&completed=" & Session("completed") & "&proposed=" & Session("proposed") & "&planned=" & Session("planned") & "&undercontruction=" & Session("undercontruction") & "&ward=" & Session("ward") & "&leedlevel=" & Session("leedlevel") & "&completedfrom=" & Session("completedfrom") & "&completedto=" & Session("completedto") & "&nearterm=" & Session("nearterm")

        ''Response.Redirect(theUrl)
        ''Response.End()

        If (i > 1) Then


            theDoc.Rect.Resize(oriW, oriH)
            theDoc.Rect.Position(oriX, oriY)

            theDoc.Transform.Reset()
            theDoc.Page = theDoc.AddPage()
        End If

        theID = theDoc.AddImageUrl(theUrl)

        Do
            If Not theDoc.Chainable(theID) Then
                Exit Do
            End If

            'theDoc.Rect.Resize(102, 45)
            'theDoc.Rect.Move(55, 710)
            'theDoc.AddImageFile(logoFile, 1)

            theDoc.Rect.Resize(oriW, oriH)
            theDoc.Rect.Position(oriX, oriY)

            theDoc.Transform.Reset()
            theDoc.Page = theDoc.AddPage()
            theID = theDoc.AddImageToChain(theID)

            pageCount = pageCount + 1

        Loop

        If Not theDoc.Chainable(theID) Then
            'theDoc.Rect.Resize(102, 45)
            'theDoc.Rect.Move(55, 710)
            'theDoc.AddImageFile(logoFile, 1)

            'pageCount = pageCount + 1
        End If
        i = i + 1


        For i = 1 To theDoc.PageCount
            theDoc.PageNumber = i
            theDoc.Flatten()
        Next



        Dim theName = "Analyze"


        Dim theDate As String = MonthName(DatePart("m", Date.Now)) & "-" & DatePart("d", Date.Now)
        Dim theProjectName As String = theName & ".pdf"
        Dim tmpName As String = System.Guid.NewGuid.ToString & ".pdf"
        theDoc.Save(ConfigurationSettings.AppSettings("pdfpath") & tmpName)

        Dim stream As System.IO.FileStream

        stream = New FileStream(ConfigurationSettings.AppSettings("pdfpath") & tmpName, FileMode.Open)
        Dim filesize As Integer = stream.Length

        Response.Clear()
        Response.ContentType = "application/pdf"
        Response.AddHeader("Content-Description", "File Transfer")
        Response.AddHeader("Content-Disposition", "attachment; filename=" & theProjectName)
        Response.AddHeader("Content-Length", filesize)

        Dim b(8192) As Byte

        Do While stream.Read(b, 0, b.Length) > 0
            Response.BinaryWrite(b)
            Response.Flush()
        Loop

        stream.Close()
        Response.End()

    End Sub


    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload

    End Sub
End Class
