﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.ComponentModel
Imports System.Xml


Partial Class GetPDFHTML
    Inherits System.Web.UI.Page

    Protected Sub GetPDFHTML_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim theProjectID = Request.QueryString("id")



        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select * from ipm_project where projectid = @projectid", MyConnection)
        Dim userid As New SqlParameter("@projectid", SqlDbType.Decimal)
        MyCommand1.SelectCommand.Parameters.Add(userid)
        MyCommand1.SelectCommand.Parameters("@projectid").Value = theProjectID
        Dim DT As New DataTable("Project")
        MyCommand1.Fill(DT)

        If (DT.Rows.Count > 0) Then
            'literalProjectCrumbs.Text = DT.Rows(0)("name").ToString.Trim

            Page.Header.Title = "WDCEP - Property Detail - " & DT.Rows(0)("name").ToString.Trim & " - " & DT.Rows(0)("city").ToString.Trim & ", " & DT.Rows(0)("state_id").ToString.Trim

            literalProjectName.Text = DT.Rows(0)("name").ToString.Trim
            'literaldescription.Text = DT.Rows(0)("description").ToString.Trim.Replace("vblf", "<br />")
            'literalProjectLocation.Text = DT.Rows(0)("city").ToString.Trim & ", " & DT.Rows(0)("state_id").ToString.Trim & " " & DT.Rows(0)("zip").ToString.Trim
            literalProjectAddress.Text = getUDFbyName("WDCEP_Address", theProjectID, False, "") 'DT.Rows(0)("address") & "<br />" & DT.Rows(0)("city").ToString.Trim & ", " & DT.Rows(0)("state_id").ToString.Trim & " " & DT.Rows(0)("zip").ToString.Trim

            literalStatusDate.Text = getUDFbyName("WDCEP_StatusDate", theProjectID, False, "")

            literalcommunitytotalsqft.Text = getUDFbyName("WDCEP_CommunitySF", theProjectID, True, "Community Sq. Ft.", True)
            literaleducationtotalsqft.Text = getUDFbyName("WDCEP_EducationSF", theProjectID, True, "Education Sq. Ft.", True)

            literalhotelrooms.Text = getUDFbyName("WDCEP_hotelrooms", theProjectID, True, "Hotel Rooms", True)
            literalMuseumSF.Text = getUDFbyName("WDCEP_MuseumSF", theProjectID, True, "Museum Sq. Ft.", True)

            literalindustrialtotalsqft.Text = getUDFbyName("WDCEP_industrialSF", theProjectID, True, "Industrial Sq. Ft.", True)

            literalmedicaltotalsqft.Text = getUDFbyName("WDCEP_medicalSF", theProjectID, True, "Medical Sq. Ft.", True)

            literalofficetotalsqft.Text = getUDFbyName("WDCEP_officeSF", theProjectID, True, "Office Sq. Ft.", True)

            literalresidentialunits.Text = getUDFbyName("WDCEP_ResidentialUnits", theProjectID, True, "Residential Units", True)

            literalretailunits.Text = getUDFbyName("WDCEP_retailSF", theProjectID, True, "Retail Sq. Ft.", True)







            literaltotalsqft.Text = getUDFbyName("WDCEP_TotalSF", theProjectID, True, "Total Sq. Ft.", True)
            literalward.Text = getUDFbyName("WDCEP_Ward", theProjectID, True, "Ward")
            'literalmajoruse.Text = getUDFbyName("majoruse", theProjectID, True, "Major Use")
            'literalstatus.Text = Replace(getUDFbyName("WDCEP_statuslu", theProjectID, True, "Status"), "(over two years)", "")
            literalleed.Text = getUDFbyName("WDCEP_leedlu", theProjectID, True, "LEED")
            literalestimatedcost.Text = getUDFbyName("WDCEP_TotalProjectCost", theProjectID, True, "Estimated Project Cost", True, "$")
            literaldescription.Text = getUDFbyName("WDCEP_Description", theProjectID, False, "")

            ltrlprojectarchitects.Text = getUDFbyName("WDCEP_Architect1LU", theProjectID, False, "") & " " & getUDFbyName("WDCEP_Architect2LU", theProjectID, False, "", , "| ") & " " & getUDFbyName("WDCEP_Architect3LU", theProjectID, False, "", , "| ")
            ltrlprojectdeveloper.Text = getUDFbyName("WDCEP_Developer1LU", theProjectID, False, "") & " " & getUDFbyName("WDCEP_Developer2LU", theProjectID, False, "", , "| ") & " " & getUDFbyName("WDCEP_Developer3LU", theProjectID, False, "", , "| ") & " " & getUDFbyName("WDCEP_Developer4LU", theProjectID, False, "", , "| ")
            ltrlprojectcontractors.Text = getUDFbyName("WDCEP_Contractor1LU", theProjectID, False, "") & " " & getUDFbyName("WDCEP_Contractor2LU", theProjectID, False, "", , "| ")
            ltrlprojectstatus.Text = Replace(getUDFbyName("WDCEP_statuslu", theProjectID, False, ""), "(over two years)", "")


            Dim _TargetDate As String
            _TargetDate = Replace(getUDFbyName("WDCEP_Delivery", theProjectID, False, ""), "(over two years)", "")
            If IsDate(_TargetDate) Then
                ltrlprojecttargetdate.Text = "Targeted Delivery: <strong>" & Year(_TargetDate) & "</strong><br />"
            Else
                If _TargetDate <> "" Then
                    ltrlprojecttargetdate.Text = "Targeted Delivery: <strong>" & _TargetDate & "</strong><br />"
                End If

                End If
       



                projectImage1.ImageUrl = Session("WSRetreiveAsset") & "qfactor=25&width=596&height=393&crop=1&size=1&type=project&id=" & theProjectID
                ' ''projectImage1Thumb.ImageUrl = Session("WSRetreiveAsset") & "qfactor=25&width=84&height=84&crop=1&size=1&type=project&id=" & theProjectID







        End If


    End Sub





    Function getUDFbyName(ByVal udfname As String, ByVal projectid As String, ByVal addhtml As Boolean, ByVal formattedtitle As String, Optional ByVal bformatnumber As Boolean = False, Optional ByVal valprefix As String = "") As String
        Dim rstring As String = ""
        Dim MyConnection2 As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand12 As SqlDataAdapter = New SqlDataAdapter("select Item_Name, Item_Tab, item_value, item_group from IPM_PROJECT_FIELD_DESC a, IPM_PROJECT_FIELD_VALUE b where a.item_tag = @itemname and a.Item_ID = b.Item_ID and b.ProjectID = @projectid and Item_Value <> '' and Item_Value <> 'false' and Item_Value <> 'true' and Item_Value <> '0' order by Item_Tab,Item_Group, item_name ", MyConnection2)
        Dim userid2 As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        MyCommand12.SelectCommand.Parameters.Add(userid2)
        MyCommand12.SelectCommand.Parameters("@projectid").Value = projectid

        Dim userid3 As New SqlParameter("@itemname", SqlDbType.VarChar, 255)
        MyCommand12.SelectCommand.Parameters.Add(userid3)
        MyCommand12.SelectCommand.Parameters("@itemname").Value = udfname


        Dim dtProjectd As New DataTable("ProjectInfo")
        MyCommand12.Fill(dtProjectd)
        If dtProjectd.Rows.Count > 0 Then
            If addhtml Then
                'Total Sq. Ft.: <span class="auto-style1"><strong><asp:Literal ID="literaltotalsqft" runat="server"></asp:Literal><br /></strong></span>
                Dim itemvalue As String = valprefix & dtProjectd.Rows(0)("item_value")
                If bformatnumber Then
                    itemvalue = valprefix & FormatNumber(dtProjectd.Rows(0)("item_value"), 0).ToString.Trim
                End If
                Return formattedtitle & ":   <strong>" & itemvalue & "</strong><br class=""auto-style10001"" />"
            Else
                Return valprefix & dtProjectd.Rows(0)("item_value")
            End If

        End If
    End Function
    Function getProjectDetails(ByVal id As String) As String
        Dim rstring As String = ""
        Dim MyConnection2 As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand12 As SqlDataAdapter = New SqlDataAdapter("select Item_Name, Item_Tab, item_value, item_group from IPM_PROJECT_FIELD_DESC a, IPM_PROJECT_FIELD_VALUE b where a.Item_ID = b.Item_ID and b.ProjectID = @projectid and Item_Value <> '' and Item_Value <> 'false' and Item_Value <> 'true' and Item_Value <> '0' order by Item_Tab,Item_Group, item_name ", MyConnection2)
        Dim userid2 As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        MyCommand12.SelectCommand.Parameters.Add(userid2)
        MyCommand12.SelectCommand.Parameters("@projectid").Value = id
        Dim dtProjectd As New DataTable("ProjectInfo")
        MyCommand12.Fill(dtProjectd)

        Dim currheading As String = ""
        For Each row In dtProjectd.Rows
            If currheading <> row("item_group") Then
                rstring += "<div class=""heading_text"" >" & row("item_group") & "</div>"
                currheading = row("item_group")
            End If
            Dim sValue As String = ""
            If IsNumeric(row("item_value")) And InStr(row("item_name"), "zip", CompareMethod.Text) = 0 Then
                sValue = FormatNumber(row("item_value"), 0)
                If InStr(row("item_name"), "cost", CompareMethod.Text) > 0 Or InStr(row("item_name"), "valuation", CompareMethod.Text) > 0 Then
                    sValue = "$" & sValue
                End If
            Else
                sValue = row("item_value")
            End If
            rstring += "<div class=""section_content"" ><table cellspacing=1 cellpadding=0><tr><td width=123 valign=top>" & row("item_name").replace("LU", "") & ": </td><td width=500 valign=top><b>" & sValue & "</b></td></tr></table></div>"
        Next
        Return rstring
    End Function

End Class
