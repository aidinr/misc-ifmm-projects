﻿Imports Microsoft.VisualBasic
Imports HtmlAgilityPack
Imports System.Data
Imports System.IO
Imports System.Xml

Public Class Functions

    Public Shared Function ConvertDataSetToXMLDataSource(ByVal XMLURL As String, ByVal XMLFileName As String) As Boolean
        Dim _DSet As DataSet = New DataSet()
        Dim _DS As XmlDataSource = New XmlDataSource
        If File.GetLastWriteTime(System.AppDomain.CurrentDomain.BaseDirectory() & XMLFileName) < DateTime.Now.AddMinutes(-1000) Then

            Dim doc As XmlDocument = New XmlDocument()
            doc.Load(XMLURL)
            ' Load the XML data from a reader object.
            ' Ignore the white spaces.
            doc.PreserveWhitespace = False
            doc.Save(System.AppDomain.CurrentDomain.BaseDirectory() & XMLFileName)
            'old way
            '_DSet.ReadXml(XMLURL)
            '_DSet.WriteXml(ConfigurationSettings.AppSettings("XMLLocation") & XMLFileName)
        End If

        Return True
    End Function




    Public Shared Function MakeSQL(ByVal _pstype As String, ByVal _leedlevel As String, ByVal _ward As String, ByVal _undercontruction As String, ByVal _completedfrom As String, ByVal _completedto As String, ByVal _proposed As String, ByVal _planned As String, ByVal _completed As String, ByVal _newconstruction As String, ByVal _Renovation As String, ByVal _Infrastructure As String, ByVal _searchbyuse As String, Optional ByVal blnUseLimiter As Boolean = True, Optional ByVal blnAddOrdering As Boolean = True, Optional ByVal _nearterm As String = "") As String

        Dim sqllimiter As String = ""
        Dim sql As String = "select case when rtrim(h.item_value) = '' then 0 else isnull(h.item_value,0) end totalcost, case when rtrim(g.item_value) = '' then 0 else isnull(g.item_value,0) end  totalsf, f.item_value majoruse, d.item_value x, e.item_value y, a.projectid,a.name, b.item_value address, c.item_value zip from ipm_project a left outer join ipm_project_field_value b on b.ProjectID = a.ProjectID and b.Item_ID = 2400007 left outer join ipm_project_field_value c on c.ProjectID = a.ProjectID and c.Item_ID = 2400008 left outer join ipm_project_field_value d on d.ProjectID = a.ProjectID and d.Item_ID = 2400147 left outer join ipm_project_field_value e on e.ProjectID = a.ProjectID and e.Item_ID = 2400148 left outer join ipm_project_field_value f on f.ProjectID = a.ProjectID and f.Item_ID = 2400021 left outer join ipm_project_field_value g on g.ProjectID = a.ProjectID and g.Item_ID = 2400019 left outer join ipm_project_field_value h on h.ProjectID = a.ProjectID and h.Item_ID = 2400055 where isnull(a.search_tags,'') like @keyword and available = 'Y' and publish = 1"

        If _newconstruction = "on" Or _Renovation = "on" Or _Infrastructure = "on" Then
            Dim sqltmp As String = ""

            If _newconstruction = "on" Then
                sqltmp += "b.item_value = 'New Construction'"
            End If
            If _Renovation = "on" Then
                If sqltmp.Length > 0 Then
                    sqltmp += " or b.item_value = 'renovation'"
                Else
                    sqltmp += " b.item_value = 'renovation'"
                End If
            End If
            If _Infrastructure = "on" Then
                If sqltmp.Length > 0 Then
                    sqltmp += " or b.item_value = 'Infrastructure'"
                Else
                    sqltmp += " b.item_value = 'Infrastructure'"
                End If
            End If
            If sqltmp.Length > 0 Then
                sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (" & sqltmp & ") and b.item_id = 2400016"
            End If

        End If

        If _searchbyuse <> "All" Then
            sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where item_name = '" & _searchbyuse & "' and Item_Tab = 'components')"
        End If

        'status
        'Session("status") = Request.Form.Item("status") - 2400017
        ' '' '' ''If Session("status") <> "All" Then
        ' '' '' ''    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '" & Session("status") & "') and b.item_id = 2400017"
        ' '' '' ''End If


        'status completed
        If _completed = "on" Then
            sql = "select a.* from (" & sql & ") a, ipm_project_field_value b, IPM_PROJECT_FIELD_VALUE c, IPM_PROJECT_FIELD_VALUE d where a.ProjectID = b.projectid and a.ProjectID = c.projectid and a.ProjectID = d.projectid and (b.item_value like 'completed%') and b.item_id = 2400017 and (" & _completedfrom & " <= datepart( yyyy,c.item_value)) and c.Item_ID = 2400061 and (" & _completedto & " >= datepart( yyyy,d.item_value)) and d.Item_ID = 2400061"
        End If

        'status planned - near term
        Dim bconstatusval As Boolean = False
        Dim sconstatusvalue As String = ""
        If _nearterm = "on" Then
            sconstatusvalue = "b.item_value like 'Near Term'"
            bconstatusval = True
        End If

        'status planned - medium term
        If _planned = "on" Then
            If sconstatusvalue.Length > 0 Then
                sconstatusvalue = sconstatusvalue & " OR " & "b.item_value like 'Medium Term'"
            Else
                sconstatusvalue = "b.item_value like 'Medium Term'"
            End If
            bconstatusval = True
        End If

        'status proposed - long term
        If _proposed = "on" Then
            If sconstatusvalue.Length > 0 Then
                sconstatusvalue = sconstatusvalue & " OR " & "b.item_value like 'Long Term'"
            Else
                sconstatusvalue = "b.item_value like 'Long Term'"
            End If
            bconstatusval = True
        End If

        'status undercontruction
        If _undercontruction = "on" Then
            If sconstatusvalue.Length > 0 Then
                sconstatusvalue = sconstatusvalue & " OR " & "b.item_value like 'Under Construction'"
            Else
                sconstatusvalue = "b.item_value like 'Under Construction'"
            End If
            bconstatusval = True
        End If


        If bconstatusval Then
            sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (" & sconstatusvalue & ") and b.item_id = 2400017 "
        End If

        'Ward
        'Session("ward") = Request.Form.Item("ward") - 2400017
        If _ward <> "All" Then
            sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '" & _ward & "') and b.item_id = 2400009"
        End If


        'Zip
        '' '' '' '' ''Session("zipcode") = Request.Form.Item("zipcode") - 2400008
        ' '' '' '' ''If Session("zipcode") <> "All" Then
        ' '' '' '' ''    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '" & Session("zipcode") & "') and b.item_id = 2400008"
        ' '' '' '' ''End If

        'leedlevel
        'Session("leedlevel") = Request.Form.Item("leedlevel") - 2400099
        If _leedlevel <> "All" Then
            sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '" & _leedlevel & "') and b.item_id = 2400099"
        End If

        'add popular search 
        If _pstype <> "" Then
            Select Case _pstype
                Case "greendevelopment"
                    '2400098
                    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = 2400098"
                Case "retailprojects"
                    '2400051
                    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = 2400051"
                Case "residentialprojects"
                    '2400042
                    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = 2400042"
                Case "officeprojects"
                    '2400040
                    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = 2400040"
            End Select
        End If
        'add limiter
        If blnUseLimiter Then
            Dim sqltemp As String = sql
            sql = "select top " & System.Configuration.ConfigurationManager.AppSettings("MaxResults") & " * from (" & sqltemp & ") a"
        End If

        If blnAddOrdering Then
            'add ordering
            sql += " order by name asc"
        End If
        Return sql

    End Function




    Public Shared Function GetMainHTMLWrapper(Optional ByVal htmlcache As String = "0") As String()
        Dim _MainHTMLWrapper() As String
        'If File.GetLastWriteTime(AppDomain.CurrentDomain.BaseDirectory & "mainhtmlwrapper.html") < DateTime.Now.AddMinutes(-CInt(ConfigurationSettings.AppSettings("wrappertimeout"))) Or htmlcache = 1 Then
        '    Dim hw As HtmlAgilityPack.HtmlWeb = New HtmlWeb
        '    Dim doc As HtmlDocument = hw.Load(ConfigurationSettings.AppSettings("MAINHTMLLOCATION"))
        '    doc.Save(AppDomain.CurrentDomain.BaseDirectory & "mainhtmlwrapper.html")
        '    Dim str As String = doc.DocumentNode.OuterHtml
        '    str = Regex.Replace(str, "DC is", "Development Search")
        '    str = Regex.Replace(str, """Innovative"", ""Progressive"", ""Diverse"", ""Collaborative"", ""Dynamic""", """")
        '    _MainHTMLWrapper = Regex.Split(str, "<!--Featured News Item-->")
        '    'cleanup
        '    _MainHTMLWrapper(0) = _MainHTMLWrapper(0).Substring(0, _MainHTMLWrapper(0).Length - 1)
        '    _MainHTMLWrapper(2) = Regex.Split(str, "<!--END id=content-->")(1)
        'Else
        '    Dim doc As New HtmlDocument
        '    doc.Load(AppDomain.CurrentDomain.BaseDirectory & "mainhtmlwrapper.html")
        '    Dim str As String = doc.DocumentNode.OuterHtml
        '    str = Regex.Replace(str, "DC is", "Development Search")
        '    str = Regex.Replace(str, """Innovative"", ""Progressive"", ""Diverse"", ""Collaborative"", ""Dynamic""", """")
        '    _MainHTMLWrapper = Regex.Split(str, "<!--Featured News Item-->")
        '    'cleanup
        '    _MainHTMLWrapper(0) = _MainHTMLWrapper(0).Substring(0, _MainHTMLWrapper(0).Length - 1)
        '    _MainHTMLWrapper(2) = Regex.Split(str, "<!--END id=content-->")(1)
        'End If

        Dim doc As New HtmlDocument
        doc.Load(AppDomain.CurrentDomain.BaseDirectory & "mainhtmlwrapper.html")
        Dim str As String = doc.DocumentNode.OuterHtml
        str = Regex.Replace(str, "DC is", "Development Search")
        str = Regex.Replace(str, """Innovative"", ""Progressive"", ""Diverse"", ""Collaborative"", ""Dynamic""", """")
        _MainHTMLWrapper = Regex.Split(str, "<!--Featured News Item-->")
        'cleanup
        _MainHTMLWrapper(0) = _MainHTMLWrapper(0).Substring(0, _MainHTMLWrapper(0).Length - 1)
        _MainHTMLWrapper(2) = Regex.Split(str, "<!--END id=content-->")(1)
        Return _MainHTMLWrapper
    End Function




End Class
