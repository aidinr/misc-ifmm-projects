﻿<%@ Application Language="VB" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="WebArchives.iDAM.WebCommon.Security " %>
<%@ Import Namespace="System.Net" %>

<script runat="server">
    Dim sBaseIP As String = ConfigurationSettings.AppSettings("IDAMLocation")
    Dim sBaseInstance As String = ConfigurationSettings.AppSettings("IDAMInstance")
    Dim sVSIDAMClientDownload As String = ConfigurationSettings.AppSettings("IDAMLocationDownload")
    Dim sVSIDAMClient As String = ConfigurationSettings.AppSettings("IDAMLocationBrowse")
    
    
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
        
        Dim dsLats As New DataTable
        dsLats.TableName = "Projects"
        
        dsLats.Columns.Add("projectid")
        dsLats.Columns.Add("latlng")
        
        'get all projects
        
        
        
        
        If System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory() & "latlng.xml") Then
            Dim doc As XmlDocument = New XmlDocument()
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory() & "latlng.xml")
            Application("dsLats") = doc
        Else
            'get from service
            Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select case when rtrim(h.item_value) = '' then 0 else h.item_value end totalcost, case when rtrim(g.item_value) = '' then 0 else g.item_value end totalsf, f.item_value majoruse, d.item_value x, e.item_value y, a.projectid,a.name, b.item_value address, c.item_value zip from ipm_project a, ipm_project_field_value b, ipm_project_field_value c, ipm_project_field_value d, ipm_project_field_value e, ipm_project_field_value f, ipm_project_field_value g, ipm_project_field_value h where a.projectid = h.projectid and h.item_id = 2400055 and a.projectid = g.projectid and g.item_id = 2400019 and a.projectid = f.projectid and f.item_id = 2400021 and a.projectid = e.projectid and a.projectid = d.projectid and a.projectid = c.projectid and c.item_id = 2400008 and a.projectid = b.projectid and b.item_id = 2400007 and d.item_id = 2400147 and e.item_id = 2400148 and available = 'Y' order by name asc", MyConnection)
            Dim DT1 As New DataTable("Projects")
            MyCommand1.Fill(DT1)
            Dim citizenatlas As New gov.dc.citizenatlas.getUSNG
            For Each row In DT1.Rows
            
                Dim xy As String = row("x") & "," & row("y")
                Dim latlng As String = citizenatlas.MD_SPCStoLL(xy).Tables(0).Rows(0)("convstr")
                dsLats.Rows.Add(row("projectid"), latlng)
            Next
            Try
                dsLats.WriteXml(System.AppDomain.CurrentDomain.BaseDirectory() & "latlng.xml")
            Catch ex As Exception

            End Try
            
         
            ' Load the XML data from a reader object.
            ' Ignore the white spaces.
            Dim doc As XmlDocument = New XmlDocument()
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory() & "latlng.xml")
                          
            Application("dsLats") = doc
        
        End If
        
        
        
       
            
      
        
        
        
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
        
    End Sub
    
        Public Const LOGIN As String = "txtLogin"
    Public Const PASSWORD As String = "txtPassword"
    Public Const INSTANCEID As String = "txtInstanceID"
    Public Const DIRECT As String = "direct"
    Public Const ENCRYPTED As String = "encrypted"
    Public Const AUTHENTICATIONLEVEL As String = "level"
    Public Const SECUREMODE As String = "secure"
    Public Const LDAPDOMAIN As String = "ld"
    Public Const LDAPPATH As String = "ldp"
    
     Public Shared Function GetLoginRequestURL(ByVal pLocation As String, ByVal pLogin As String, ByVal pPassword As String, ByVal pInstanceID As String, ByVal pSecureMode As Boolean, Optional ByVal pEncrypted As Boolean = False, Optional ByVal pDirectMode As Boolean = False, Optional ByVal pAuthenticationLevel As Integer = 0, Optional ByVal pLDAPDomain As String = "", Optional ByVal pLDAPPath As String = "") As String
        If pLocation Is Nothing Then
            Throw New ArgumentException("location cannot be null")
        End If
        If pLogin Is Nothing Then
            Throw New ArgumentException("login cannot be null")
        End If
        If pInstanceID Is Nothing Then
            Throw New ArgumentException("pInstanceID cannot be null")
        End If
        If pPassword Is Nothing Then
            Throw New ArgumentException("pPassword cannot be null")
        End If

        Dim url As New Text.StringBuilder
        url.Append(pLocation)
        url.Append("?")
        url.Append(LOGIN).Append("=").Append(pLogin).Append("&")
        url.Append(PASSWORD).Append("=").Append(pPassword).Append("&")
        url.Append(INSTANCEID).Append("=").Append(pInstanceID).Append("&")
        url.Append(DIRECT).Append("=").Append(pDirectMode.ToString).Append("&")
        url.Append(ENCRYPTED).Append("=").Append(pEncrypted.ToString).Append("&")
        url.Append(AUTHENTICATIONLEVEL).Append("=").Append(pAuthenticationLevel.ToString).Append("&")
        url.Append(SECUREMODE).Append("=").Append(pSecureMode.ToString).Append("&")
        If pLDAPDomain <> "" Then
            url.Append(LDAPDOMAIN).Append("=").Append(pLDAPDomain.ToString).Append("&")
        End If
        If pLDAPPath <> "" Then
            url.Append(LDAPPATH).Append("=").Append(pLDAPPath.ToString)
        End If


        Return url.ToString

    End Function
    
    
     Private Function LoginToClient(ByVal pURL As String, ByVal pUsername As String, ByVal pPassword As String, ByVal pInstanceID As String) As Boolean
        Dim fullURL As String
        Try

            Dim wc As New Net.WebClient
            Dim result As String
            fullURL = GetLoginRequestURL(pURL, pUsername, pPassword, pInstanceID, False, False, False, 2)
            Dim data As IO.Stream = wc.OpenRead(fullURL)
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            Return (str.IndexOf("SUCCESS") >= 0)
        Catch ex As Exception
            'Log.Error("error logging in to :" + fullURL, ex)
            Return False
        End Try
    End Function

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
        ' Code that runs when a new session is started
        Dim assetRetrieve = sBaseIP & "/" & sVSIDAMClient
        Dim assetDownload = sBaseIP & "/" & sVSIDAMClientDownload
        
        Dim theLogin As String = ConfigurationSettings.AppSettings("IDAMLogin")
        Dim thePassword As String = ConfigurationSettings.AppSettings("IDAMPassword")
        'thePassword = WebArchives.iDAM.WebCommon.Security.Encrypt(thePassword)

        Dim loginStatus As Boolean = False

        Dim loginResult1 As String = ""

        Dim sessionID As String = "(S(" & GetSessionID("http://" & assetRetrieve & "/GetSessionID.aspx") & "))"
        Session("clientSessionID") = sessionID
        Dim iDAMBrowse As New browsesecure.BrowseService
        iDAMBrowse.Url = "http://" & assetRetrieve & "/" & sessionID & "/" & "BrowseService.asmx"
        'Response.Write(iDAMBrowse.Url)
        Try
        
        
        loginResult1 = LoginToClient( "http://" & assetRetrieve & "/" & sessionID & "/Login.aspx",theLogin, thePassword,sBaseInstance)
            'loginResult1 = iDAMBrowse.Login(theLogin, thePassword, sBaseInstance, False)
        Catch Ex As Exception
            Response.Write(Ex)
        End Try
        
        Dim sessionIDDownload As String = "(S(" & GetSessionID("http://" & assetDownload & "/GetSessionID.aspx") & "))"
        
        ' Dim iDAMDownload As New DownloadSecure.AssetDownloadService
        ' iDAMDownload.Url = "http://" & assetDownload & "/" & sessionIDDownload & "/" & "AssetDownloadService.asmx"
       
        ' Try
        'loginResult1 = iDAMDownload.Login(theLogin, thePassword, sBaseInstance, True)
        'Catch Ex As Exception
        ' 'Response.Write(Ex)
        'End Try
        
        Session("WSRetreiveAsset") = "http://" & assetRetrieve & "/" + sessionID + "/RetrieveAsset.aspx?instance=" & sBaseInstance & "&"
        'Session("WSDownloadAsset") = "http://" & assetDownload & "/" & sessionIDDownload + "/DownloadFile.aspx?dtype=assetdownload&instance=" & sBaseInstance & "&"
        
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
    
    Public Shared Function GetSessionID(ByVal URL As String) As String
        Try
            Dim clientreq As HttpWebRequest
            clientreq = HttpWebRequest.Create(URL)
            clientreq.Timeout = CType(10000, Integer)
            Dim clientres As HttpWebResponse = clientreq.GetResponse
            Dim data As IO.Stream = clientres.GetResponseStream
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            If str.IndexOf("SESSIONID") >= 0 Then
                Dim strtidx As Integer = str.IndexOf("SESSIONID=") + 10
                Return str.Substring(strtidx, str.IndexOf("</span>") - strtidx)
            Else
                Throw New Exception("No session ID found. check URL if it has the proper format" + URL)
            End If
        Catch ex As Exception
            Throw New Exception("WebService not available." + URL)
        End Try
    End Function
       
</script>