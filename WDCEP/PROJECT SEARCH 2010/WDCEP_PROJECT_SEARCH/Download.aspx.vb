﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.IO

Partial Class Download
    Inherits System.Web.UI.Page





    Public Function MakeSQL(Optional ByVal blnUseLimiter As Boolean = True) As String

        Dim sqllimiter As String = ""

        Dim sql As String = "select a.projectid,a.name, case when rtrim(h.item_value) = '' then 0 else h.item_value end totalcost, case when rtrim(g.item_value) = '' then 0 else g.item_value end totalsf, f.item_value majoruse, d.item_value x, e.item_value y, b.item_value address, c.item_value zip, dbo.List_UDFS_PROJECT_SEPERATOR(a.projectid) UDFITEMS from ipm_project a, ipm_project_field_value b, ipm_project_field_value c, ipm_project_field_value d, ipm_project_field_value e, ipm_project_field_value f, ipm_project_field_value g, ipm_project_field_value h where a.projectid = h.projectid and h.item_id = 2400055 and a.projectid = g.projectid and g.item_id = 2400019 and a.projectid = f.projectid and f.item_id = 2400021 and a.projectid = e.projectid and a.projectid = d.projectid and a.projectid = c.projectid and c.item_id = 2400008 and a.projectid = b.projectid and b.item_id = 2400007 and d.item_id = 2400147 and e.item_id = 2400148 and a.search_tags like @keyword and available = 'Y'"
        If Session("newconstruction") = "on" Or Session("Renovation") = "on" Or Session("Infrastructure") = "on" Then
            Dim sqltmp As String = ""

            If Session("newconstruction") = "on" Then
                sqltmp += "b.item_value = 'New Construction'"
            End If
            If Session("Renovation") = "on" Then
                If sqltmp.Length > 0 Then
                    sqltmp += " or b.item_value = 'renovation'"
                Else
                    sqltmp += " b.item_value = 'renovation'"
                End If
            End If
            If Session("Infrastructure") = "on" Then
                If sqltmp.Length > 0 Then
                    sqltmp += " or b.item_value = 'Infrastructure'"
                Else
                    sqltmp += " b.item_value = 'Infrastructure'"
                End If
            End If
            If sqltmp.Length > 0 Then
                sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (" & sqltmp & ") and b.item_id = 2400016"
            End If

        End If

        If Session("searchbyuse") <> "All" Then
            sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where item_name = '" & Session("searchbyuse") & "' and Item_Tab = 'components')"
        End If

        'status
        'Session("status") = Request.Form.Item("status") - 2400017
        ' '' '' ''If Session("status") <> "All" Then
        ' '' '' ''    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '" & Session("status") & "') and b.item_id = 2400017"
        ' '' '' ''End If


        'status completed
        If Session("completed") = "on" Then
            sql = "select a.* from (" & sql & ") a, ipm_project_field_value b, IPM_PROJECT_FIELD_VALUE c, IPM_PROJECT_FIELD_VALUE d where a.ProjectID = b.projectid and a.ProjectID = c.projectid and a.ProjectID = d.projectid and (b.item_value like 'completed%') and b.item_id = 2400017 and (" & Session("completedfrom") & " <= datepart( yyyy,c.item_value)) and c.Item_ID = 2400089 and (" & Session("completedto") & " >= datepart( yyyy,d.item_value)) and d.Item_ID = 2400089"
        End If






        'status planned - near term
        Dim bconstatusval As Boolean = False
        Dim sconstatusvalue As String = ""
        If Session("nearterm") = "on" Then
            sconstatusvalue = "b.item_value like 'Near Term'"
            bconstatusval = True
        End If

        'status planned - medium term
        If Session("planned") = "on" Then
            If sconstatusvalue.Length > 0 Then
                sconstatusvalue = sconstatusvalue & " OR " & "b.item_value like 'Medium Term'"
            Else
                sconstatusvalue = "b.item_value like 'Medium Term'"
            End If
            bconstatusval = True
        End If

        'status proposed - long term
        If Session("proposed") = "on" Then
            If sconstatusvalue.Length > 0 Then
                sconstatusvalue = sconstatusvalue & " OR " & "b.item_value like 'Long Term'"
            Else
                sconstatusvalue = "b.item_value like 'Long Term'"
            End If
            bconstatusval = True
        End If

        'status undercontruction
        If Session("undercontruction") = "on" Then
            If sconstatusvalue.Length > 0 Then
                sconstatusvalue = sconstatusvalue & " OR " & "b.item_value like 'Under Construction'"
            Else
                sconstatusvalue = "b.item_value like 'Under Construction'"
            End If
            bconstatusval = True
        End If


        If bconstatusval Then
            sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (" & sconstatusvalue & ") and b.item_id = 2400017 "
        End If


        'Ward
        'Session("ward") = Request.Form.Item("ward") - 2400017
        If Session("ward") <> "All" Then
            sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '" & Session("ward") & "') and b.item_id = 2400009"
        End If


        'Zip
        '' '' '' '' ''Session("zipcode") = Request.Form.Item("zipcode") - 2400008
        ' '' '' '' ''If Session("zipcode") <> "All" Then
        ' '' '' '' ''    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '" & Session("zipcode") & "') and b.item_id = 2400008"
        ' '' '' '' ''End If

        'leedlevel
        'Session("leedlevel") = Request.Form.Item("leedlevel") - 2400099
        If Session("leedlevel") <> "All" Then
            sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '" & Session("leedlevel") & "') and b.item_id = 2400099"
        End If

        'add popular search 
        If Request.Cookies.Item("pstype").Value <> "" Then
            Select Case Request.Cookies.Item("pstype").Value
                Case "greendevelopment"
                    '2400098
                    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = 2400098"
                Case "retailprojects"
                    '2400051
                    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = 2400051"
                Case "residentialprojects"
                    '2400042
                    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = 2400042"
                Case "officeprojects"
                    '2400040
                    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = 2400040"
            End Select
        End If
        'add limiter
        If blnUseLimiter Then
            Dim sqltemp As String = sql
            sql = "select top " & System.Configuration.ConfigurationManager.AppSettings("MaxResults") & " * from (" & sqltemp & ") a"
        End If

        'add ordering
        sql += " order by name asc"

        Return sql

    End Function
    Private Function AddColumns(ByVal sql As String) As String
        Return "select  a.name [Project], d.Item_Value [Location], x.item_value [Ward], b.Item_Value [Developer 1], c.Item_Value [Developer 2], e.Item_Value [Developer 3], f.Item_Value [Architect 1], g.Item_Value [Architect 2], h.Item_Value [Contractor 1], i.Item_Value [Total Sq Ft.], j.Item_Value [Construction Type], k.Item_Value [Major Use], l.Item_Value [Hotel Rooms], m.Item_Value [Office Sq Ft.], n.Item_Value [Residential Units], o.Item_Value [Retail Sq Ft.], p.Item_Value [LEED], replace(replace(q.Item_Value,'(over two years)',''),'(past two years)','') [Status], replace(cast(datepart(year,r.Item_Value) as varchar(4)),'1900','N/A') [Expected Groundbreaking], replace(cast(datepart(year,s.Item_Value) as varchar(4)),'1900','N/A') [Targeted Delivery], t.Item_Value [Estimated Project Cost], u.Item_Value [Edit Date] from IPM_PROJECT a left outer join IPM_PROJECT_FIELD_VALUE b on a.ProjectID = b.ProjectID and b.Item_ID = 2400010 /*developer1*/ left outer join IPM_PROJECT_FIELD_VALUE c on a.ProjectID = c.ProjectID and c.Item_ID = 2400011 /*developer2*/ left outer join IPM_PROJECT_FIELD_VALUE e on a.ProjectID = e.ProjectID and e.Item_ID = 2400012 /*developer3*/ left outer join IPM_PROJECT_FIELD_VALUE d on a.ProjectID = d.ProjectID and d.Item_ID = 2400004 /*address*/ left outer join IPM_PROJECT_FIELD_VALUE f on a.ProjectID = f.ProjectID and f.Item_ID = 2400013 /*WARD*/ left outer join IPM_PROJECT_FIELD_VALUE x on a.ProjectID = x.ProjectID and x.Item_ID = 2400009 /*architect 1*/ left outer join IPM_PROJECT_FIELD_VALUE g on a.ProjectID = g.ProjectID and g.Item_ID = 2400014 /*architect 2*/ left outer join IPM_PROJECT_FIELD_VALUE h on a.ProjectID = h.ProjectID and h.Item_ID = 2400092 /*WDCEP_Contractor1LU */ left outer join IPM_PROJECT_FIELD_VALUE i on a.ProjectID = i.ProjectID and i.Item_ID = 2400019 /*WDCEP_TotalSF */ left outer join IPM_PROJECT_FIELD_VALUE j on a.ProjectID = j.ProjectID and j.Item_ID = 2400016 /*WDCEP_ConstructionLU */ left outer join IPM_PROJECT_FIELD_VALUE k on a.ProjectID = k.ProjectID and k.Item_ID = 2400021 /*WDCEP_MajorUse */ left outer join IPM_PROJECT_FIELD_VALUE l on a.ProjectID = l.ProjectID and l.Item_ID = 2400033 /*WDCEP_HotelRooms */ left outer join IPM_PROJECT_FIELD_VALUE m on a.ProjectID = m.ProjectID and m.Item_ID = 2400041 /*WDCEP_OfficeSF */ left outer join IPM_PROJECT_FIELD_VALUE n on a.ProjectID = n.ProjectID and n.Item_ID = 2400045 /*WDCEP_ResidentialUnits */ left outer join IPM_PROJECT_FIELD_VALUE o on a.ProjectID = o.ProjectID and o.Item_ID = 2400052 /*WDCEP_RetailSF */ left outer join IPM_PROJECT_FIELD_VALUE p on a.ProjectID = p.ProjectID and p.Item_ID = 2400099 /*WDCEP_LEEDLU */ left outer join IPM_PROJECT_FIELD_VALUE q on a.ProjectID = q.ProjectID and q.Item_ID = 2400017 /*WDCEP_StatusLU */ left outer join IPM_PROJECT_FIELD_VALUE r on a.ProjectID = r.ProjectID and r.Item_ID = 2400060 /*WDCEP_Groundbreaking */ left outer join IPM_PROJECT_FIELD_VALUE s on a.ProjectID = s.ProjectID and s.Item_ID = 2400061 /*WDCEP_Delivery */ left outer join IPM_PROJECT_FIELD_VALUE t on a.ProjectID = t.ProjectID and t.Item_ID = 2400055 /*WDCEP_TotalProjectCost */ left outer join IPM_PROJECT_FIELD_VALUE u on a.ProjectID = u.ProjectID and u.Item_ID = 2400089 /*WDCEP_StatusDate */ where a.ProjectID in (select projectid From (" & sql & ") a ) order by name asc"


    End Function
    Private Sub getcsv()
        'Get the data from database into datatable

        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(AddColumns(Functions.MakeSQL(Request.Cookies.Item("pstype").Value, Session("leedlevel"), Session("ward"), Session("undercontruction"), Session("completedfrom"), Session("completedto"), Session("proposed"), Session("planned"), Session("completed"), Session("newconstruction"), Session("rennovation"), Session("infrastructure"), Session("searchbyuse"), True, True)), MyConnection)
        Dim userid As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Session("searchFilter").Replace("'", "''") & "%"
        Dim DT As New DataTable("Projects")
        MyCommand1.Fill(DT)
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=Results.csv")
        Response.Charset = ""
        Response.ContentType = "application/text"

        Dim sb As New StringBuilder()
        For k As Integer = 0 To DT.Columns.Count - 1
            'add separator
            If DT.Columns(k).ColumnName = "UDFITEMS" Then
                For Each item As String In Regex.Split(DT.Rows(0)("UDFITEMS"), "<items>")
                    sb.Append(Regex.Split(item, "<value>")(0) + ","c)
                Next
            Else
                sb.Append(DT.Columns(k).ColumnName + ","c)
            End If

        Next
        'append new line
        sb.Append(vbCr & vbLf)
        For i As Integer = 0 To DT.Rows.Count - 1
            For k As Integer = 0 To DT.Columns.Count - 1
                'add separator
                If DT.Columns(k).ColumnName = "UDFITEMS" Then
                    For Each item As String In Regex.Split(DT.Rows(i)("UDFITEMS"), "<items>")
                        If item <> "" Then
                            sb.Append(Regex.Split(item, "<value>")(1).Replace(",", ";") + ","c)
                        End If
                    Next

                Else
                    sb.Append(DT.Rows(i)(k).ToString().Replace(",", ";") + ","c)
                End If

            Next
            'append new line
            sb.Append(vbCr & vbLf)
        Next

        sb.Append(vbCr & vbLf)
        sb.Append(vbCr & vbLf)
        sb.Append(vbCr & vbLf)
        Dim _footer As String = "Data(Notes)" & vbCr & vbLf & _
 " WDCEP data is provided ""as is"" and WDCEP makes no warranty or guarantee to its accuracy or usefulness for any given purpose. All data are estimates." & vbCr & vbLf & _
 " projects include new construction & major renovations" & vbCr & vbLf & _
 " new construction does not necessarily mean ""net new""" & vbCr & vbLf & _
 " estimated delivery dates may reference a first phase delivery or final phase delivery" & vbCr & vbLf & _
 " pipeline data (Near Term & Long Term & Medium Term) is subject to change" & vbCr & vbLf & _
 " pipeline projects may still need DC Government (zoning, DC Council, historic preservation, etc.) approval" & vbCr & vbLf & _
 " LEED data is based on  project's certification goal and may not reflect final USGBC LEED certifiation. For a list of certified buildings visit www.usgbc.org." & vbCr & vbLf & _
 " Edit Date refers to last time information was updated" & vbCr & vbLf & _
 " for a more detailed methodology you can download our DC Development Report from our website" & vbCr & vbLf & _
 " if using WDCEP data please credit ""Washington DC Economic Partnership"" where applicable (please send any documents to WDCEP were WDCEP data is used)"
        sb.Append(_footer.Replace(",", ";"))


        Response.Output.Write(sb.ToString())
        Response.Flush()
        Response.End()
    End Sub


    Private Sub getexcel()
        'Get the data from database into datatable




        '' '' '' ''Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        '' '' '' ''Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(MakeSQL(True), MyConnection)
        '' '' '' ''Dim userid As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        '' '' '' ''MyCommand1.SelectCommand.Parameters.Add(userid)
        '' '' '' ''MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Session("searchFilter").Replace("'", "''") & "%"
        '' '' '' ''Dim DT As New DataTable("Projects")
        '' '' '' ''MyCommand1.Fill(DT)
        '' '' '' ''Response.Clear()
        '' '' '' ''Response.Buffer = True
        '' '' '' ''Response.AddHeader("content-disposition", "attachment;filename=Results.xls")
        '' '' '' ''Response.Charset = ""
        '' '' '' ''Response.ContentType = "application/vnd.ms-excel"

        '' '' '' ''Dim sb As New StringBuilder()
        '' '' '' ''For k As Integer = 0 To DT.Columns.Count - 1
        '' '' '' ''    'add separator
        '' '' '' ''    If DT.Columns(k).ColumnName = "UDFITEMS" Then
        '' '' '' ''        For Each item As String In Regex.Split(DT.Rows(0)("UDFITEMS"), "<items>")
        '' '' '' ''            sb.Append(Regex.Split(item, "<value>")(0) + ","c)
        '' '' '' ''        Next
        '' '' '' ''    Else
        '' '' '' ''        sb.Append(DT.Columns(k).ColumnName + ","c)
        '' '' '' ''    End If

        '' '' '' ''Next
        ' '' '' '' ''append new line
        '' '' '' ''sb.Append(vbCr & vbLf)
        '' '' '' ''For i As Integer = 0 To DT.Rows.Count - 1
        '' '' '' ''    For k As Integer = 0 To DT.Columns.Count - 1
        '' '' '' ''        'add separator
        '' '' '' ''        If DT.Columns(k).ColumnName = "UDFITEMS" Then
        '' '' '' ''            For Each item As String In Regex.Split(DT.Rows(i)("UDFITEMS"), "<items>")
        '' '' '' ''                sb.Append(Regex.Split(item, "<value>")(1).Replace(",", ";") + ","c)
        '' '' '' ''            Next

        '' '' '' ''        Else
        '' '' '' ''            sb.Append(DT.Rows(i)(k).ToString().Replace(",", ";") + ","c)
        '' '' '' ''        End If

        '' '' '' ''    Next
        '' '' '' ''    'append new line
        '' '' '' ''    sb.Append(vbCr & vbLf)
        '' '' '' ''Next
        '' '' '' ''Response.Output.Write(sb.ToString())
        '' '' '' ''Response.Flush()
        '' '' '' ''Response.End()


        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(MakeSQL(True), MyConnection)
        Dim userid As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Session("searchFilter").Replace("'", "''") & "%"
        Dim DT As New DataTable("Projects")
        MyCommand1.Fill(DT)

        'Create a dummy GridView
        Dim GridView1 As New GridView()
        GridView1.AllowPaging = False
        GridView1.DataSource = DT
        GridView1.DataBind()

        ' ''For k As Integer = 0 To DT.Columns.Count - 1
        ' ''    'add separator
        ' ''    If DT.Columns(k).ColumnName = "UDFITEMS" Then
        ' ''        For Each item As String In Regex.Split(DT.Rows(0)("UDFITEMS"), "<items>")
        ' ''            Dim x As System.Web.UI.WebControls.DataControlField

        ' ''            x.HeaderText = Regex.Split(item, "<value>")(0)
        ' ''            GridView1.Columns.Add(x)
        ' ''        Next
        ' ''    Else
        ' ''        'sb.Append(DT.Columns(k).ColumnName + ","c)
        ' ''    End If

        ' ''Next




        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", _
             "attachment;filename=Results.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        For i As Integer = 0 To GridView1.Rows.Count - 1
            'Apply text style to each Row
            GridView1.Rows(i).Attributes.Add("class", "textmode")
        Next
        GridView1.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()
    End Sub



    Protected Sub Download_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Select Case Request.QueryString("downloadtype")
            Case "csv"
                getcsv()
            Case "excel"
                getexcel()
            Case Else
                Response.Redirect("Result.aspx")
        End Select
    End Sub
End Class
