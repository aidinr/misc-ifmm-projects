﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Result.aspx.vb" Inherits="Result" EnableViewStateMac="false" enableEventValidation="false" EnableViewState=false %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<!DOCTYPE html> 
<asp:Literal id="htmltop" runat=server></asp:Literal>
<html> 
<head> 
<!-- <style type="text/css">
 	@import url("css/style.css");
</style> -->
<link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" />
<script src="http://cdn.jquerytools.org/1.2.3/full/jquery.tools.min.js" type="text/javascript"></script>

<!--<script src="js/jquery.perciformes.js" type="text/javascript"></script>
<script src="js/navi.js" type="text/javascript"></script>-->
<script type="text/javascript" src="js/slimbox2.js"></script>


<meta name="viewport" content="initial-scale=1.0, user-scalable=no" /> 
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/> 

<script src="http://cdn.jquerytools.org/1.2.2/full/jquery.tools.min.js" type="text/javascript"></script>


<link rel="alternate" type="application/rss+xml" title="WDCEP &raquo; Feed" href="http://wdcep.com/?feed=rss2" />
<link rel="alternate" type="application/rss+xml" title="WDCEP &raquo; Comments Feed" href="http://wdcep.com/?feed=comments-rss2" />
<link rel="alternate" type="application/rss+xml" title="WDCEP &raquo; Home Comments Feed" href="http://wdcep.com/?feed=rss2&#038;page_id=10977" />
<link rel="stylesheet" id="flickr-gallery-css"  href='http://wdcep.com/wp-content/plugins/flickr-gallery/flickr-gallery.css' type='text/css' media='all' />
<link rel='stylesheet' id='fg-jquery-ui-css'  href='http://wdcep.com/wp-content/plugins/flickr-gallery/tab-theme/jquery-ui-1.7.3.css' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-flightbox-css'  href='http://wdcep.com/wp-content/plugins/flickr-gallery/flightbox/jquery.flightbox.css' type='text/css' media='all' />
<link rel='stylesheet' id='tubepress-css'  href='http://wdcep.com/wp-content/plugins/tubepress/src/main/web/css/tubepress.css' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='http://wdcep.com/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' />
<link rel='stylesheet' id='se-link-styles-css'  href='http://wdcep.com/wp-content/plugins/search-everything/static/css/se-styles.css' type='text/css' media='all' />
<link rel='stylesheet' id='wp-pagenavi-css'  href='http://wdcep.com/wp-content/plugins/wp-pagenavi/pagenavi-css.css' type='text/css' media='all' />
<link rel='stylesheet' id='parent-style-css'  href='http://wdcep.com/wp-content/themes/bones/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='http://wdcep.com/wp-content/themes/bones-child/library/css/font-awesome.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='framework-css'  href='http://wdcep.com/wp-content/themes/bones-child/library/css/gridle_framework.css' type='text/css' media='all' />
<link rel='stylesheet' id='googleFonts-css'  href='http://fonts.googleapis.com/css?family=Lato%3A400%2C700%2C400italic%2C700italic' type='text/css' media='all' />
<link rel='stylesheet' id='tablepress-default-css'  href='http://wdcep.com/wp-content/plugins/tablepress/css/default.min.css' type='text/css' media='all' />
<style id='tablepress-default-inline-css' type='text/css'>
    
.tablepress-id-1 th,.tablepress-id-1 .sorting{background-color:#effaea!important}.tablepress-id-1 .row-3 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-5 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-7 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-9 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-11 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-13 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-15 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-17 td{background-color:#f1f2f2!important}.tablepress-id-1 .column-3{text-align:right}.tablepress-id-1 .column-4{text-align:center}.tablepress-id-2 th,.tablepress-id-2 .sorting{background-color:#fc9!important}.tablepress-id-2 .row-2 td{background-color:#f1f2f2!important}.tablepress-id-2 .row-40 td{background-color:#f1f2f2!important}.tablepress-id-2 .row-43 td{background-color:#f1f2f2!important}.tablepress-id-2 .row-48 td{background-color:#f1f2f2!important}.tablepress-id-2 .row-53 td{background-color:#f1f2f2!important}.tablepress-id-2 .column-2{text-align:right}.tablepress-id-3 th,.tablepress-id-3 .sorting{background-color:#b2e599!important}.tablepress-id-3 .row-3 td{background-color:#f1f2f2!important}.tablepress-id-3 .row-5 td{background-color:#f1f2f2!important}.tablepress-id-3 .row-7 td{background-color:#f1f2f2!important}.tablepress-id-3 .row-9 td{background-color:#f1f2f2!important}.tablepress-id-3 .column-2{text-align:right}.tablepress-id-3 .column-3{text-align:right}.tablepress-id-4 th,.tablepress-id-4 .sorting{background-color:#efe5ea!important}.tablepress-id-4 .row-3 td{background-color:#f1f2f2!important}.tablepress-id-4 .row-5 td{background-color:#f1f2f2!important}.tablepress-id-4 .column-3{text-align:center}.tablepress-id-4 .column-4{text-align:center}.tablepress-id-5 th,.tablepress-id-5 .sorting{background-color:#d1d3d4!important}.tablepress-id-5 .row-3 td{background-color:#d1d3d4!important}.tablepress-id-5 .row-10 td{background-color:#d1d3d4!important}.tablepress-id-5,.tablepress-id-5 td,.tablepress-id-5 th{border:none!important;border-collapse:collapse!important;border-spacing:0!important}.tablepress-id-6 th,.tablepress-id-6 .sorting{background-color:#d1d3d4!important}.tablepress-id-6 .row-1 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-3 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-5 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-7 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-9 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-11 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-13 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-15 td{background-color:#d1d3d4!important}.tablepress-id-6 .column-3{text-align:right}.tablepress-id-6 .column-1{width:110px}.tablepress-id-6 .column-2{width:360px}.tablepress-id-6,.tablepress-id-6 td,.tablepress-id-6 th{border:none!important;border-collapse:collapse!important;border-spacing:0!important}.tablepress-id-7 th,.tablepress-id-7 .sorting{background-color:#fff!important}.tablepress-id-7 .column-1{width:110px}.tablepress-id-7 .column-2{width:200px}.tablepress-id-7 .column-3{width:10px}.tablepress-id-7 .column-4{width:110px}.tablepress-id-7 .column-5{width:200px}.tablepress-id-7,.tablepress-id-7 td,.tablepress-id-7 th{border:none!important;border-collapse:collapse!important;border-spacing:0!important}.tablepress-id-11 th,.tablepress-id-2 .sorting{background-color:#fc9!important}.tablepress-id-11 .column-2{text-align:left}.tablepress-id-11 .column-3{text-align:center}.tablepress-id-11 .column-4{text-align:center}
.button:hover, .button:focus {
    color: #80cec2;
}
.button:hover, .button:focus {
    color: #80cec2;
}

  #map_canvas { height: 300px; width: 600px; padding:10px; }
  
</style>
<title>WDCEP Project Search - Results</title> 
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
<script type="text/javascript">


    var geocoder;
    var map;

    function initialize() {
        var myLatlng = new google.maps.LatLng(38.91971, -77.02931);
        var myOptions = {
            zoom: 12,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
       //var bounds = new google.maps.LatLngBounds(myLatlng,myLatlng);
       //map.fitBounds(bounds);
        // Create new geocoding object
        geocoder = new google.maps.Geocoder();

        // Retrieve location information, pass it to addToMap()
        //geocoder.getLocations(address, addToMap);

<asp:Literal ID=ltrladdresses runat=server></asp:Literal>


    }

    // This function adds the point to the map

    function addToMap(map,addressi) {
        geocoder.geocode({ 'address': addressi }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                           
                //alert("Geocode was not successful for the following reason: " + status + addressi);
            }
        });
    }


function closeallinfowindows()
{

}

function scrolltotopcust()
{
$('html, body').animate({scrollTop:0}, 'slow');
}
    
    

     function setCompletedDropdownstatus(bval) {
         if (bval) {
             var _DropDownListcompletedyearfrom = document.getElementById("DropDownListcompletedyearfrom")
             _DropDownListcompletedyearfrom.disabled = false;
             var _DropDownListcompletedyearto = document.getElementById("DropDownListcompletedyearto")
             _DropDownListcompletedyearto.disabled = false;             
         } else {
         var _DropDownListcompletedyearfrom = document.getElementById("DropDownListcompletedyearfrom")
         _DropDownListcompletedyearfrom.disabled = true;
         var _DropDownListcompletedyearto = document.getElementById("DropDownListcompletedyearto")
         _DropDownListcompletedyearto.disabled = true;        
         }

 
         
     }
 
     function GetCookie(name) {
         var nameEQ = name + "=";
         var ca = document.cookie.split(';');
         for (var i = 0; i < ca.length; i++) {
             var c = ca[i];
             while (c.charAt(0) == ' ') c = c.substring(1, c.length);
             if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
         }
         return null;
     }

     function SetCookie(name, value) {
      
         var days = 300
         var date = new Date();
         date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
         var expires = "; expires=" + date.toGMTString();
         document.cookie = name + "=" + escape(value) + expires + "; path=/";
         //document.cookie=name+ "=" +escape(value)+((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
     }

     function DeleteCookie(name) {

         var exp = new Date();
         exp.setTime(exp.getTime() - 1);
         var cval = GetCookie(name);
         document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
     }
     
 
     function switchPage(i) {

        searchCallback.callback('paging',i);
    };
    function sortSearch(column) {

        searchCallback.callback('sort', column);
    };
    function filterSearch(filter) {

        searchCallback.callback('filter', filter);
    };
    function getcsv() {

        searchCallback.callback('getcsv');
    };    

    
</script> 
</head> 
<body onload="initialize()" style="margin-left: 0; margin-top: 0; margin-bottom: 0" > 
<form id="form1" runat="server" action="Result.aspx">
 <div class="home-curated-content-feeds" style="width:950px; margin-left:200px;margin-top:0px">
        <div class="container" style="margin-top: 0px;">
        
        <!-- left panel -->
        <div class="grid-8 parent grid-mobile-12" style="width:100%;margin-top:-30px">
        <div class="breadcrumbs" style="z-index:100000; margin-top:0px" >
                <!-- Breadcrumb NavXT 5.2.2 -->
<span typeof="v:Breadcrumb"><a class="home" style="color: #bfbfbf;    font-weight: 700;     letter-spacing: 0.08em;    text-align: left;      text-transform: uppercase;    transition: all 0.4s ease 0s;" href="default.aspx" title="Go to Search." property="v:title" rel="v:url">Search</a></span> <span style="color: #bfbfbf;    font-weight: 700;     letter-spacing: 0.08em;    text-align: left;      text-transform: uppercase;    transition: all 0.4s ease 0s;"> &gt; </span> <span typeof="v:Breadcrumb"> <span typeof="v:Breadcrumb"></span> <span typeof="v:Breadcrumb"><span property="v:title" style="color: #bfbfbf;    font-weight: 700;     letter-spacing: 0.08em;    text-align: left;      text-transform: uppercase;    transition: all 0.4s ease 0s;">Results</span></span>            </div>


             <div id="map_canvas" style="width:100%;margin-top:0px; height:400px"></div> 
            <div style="width:100%; padding:5px;  color:#00224b; font-family:Verdana; font-size:12px; text-align:right; ">Results <asp:Literal ID="literalResults" runat="server"></asp:Literal></div>


 <ComponentArt:CallBack runat="server" ID="searchCallback">
			                    <Content>
			                    <asp:PlaceHolder ID="slideshowPlaceholder" runat="server">
			                  
			                    
			                    <asp:Literal ID="literalPaging" runat="server"></asp:Literal>
                               
			                    <asp:Repeater ID="RepeaterSearchResults" runat="server" OnItemDataBound="RepeaterSearchResultsItemDataBound">
						                <HeaderTemplate>
						                    <table cellpadding="5" cellspacing="5" width="100%"  id="table_search">
						                        <thead>
						                                <tr style="height:35px;">
						                                <th valign="top" style="vertical-align:middle;" width="16"></th>
						                                <!-- <th valign="top" width="111"></th> -->
						                                <th  align="left" style="vertical-align:middle;" width="227"><a style="color:White" href="javascript:sortSearch('pn')">Property Name</a></th>
						                                <th style="vertical-align:middle;" align="left" width="97"><a style="color:White" href="javascript:sortSearch('mu')">Major Use</a></th>
						                                <th style="vertical-align:middle;" align="right" width="95"><a style="color:White" href="javascript:sortSearch('sf')">Sq. Ft.</a></th>
						                                <th style="vertical-align:middle;" align="right" width="127"><a style="color:White" href="javascript:sortSearch('ec')">Est Cost&nbsp;</a></th>
						                                
						                                </tr>
						                        </thead>
						                        <tbody>
						                </HeaderTemplate>
						          <ItemTemplate>
						                <tr class="odd" style="height:35px;">      <!---->  
						                <td valign="middle" style="vertical-align:middle" align="left" width="16"><a style="cursor:pointer; vertical-align:bottom" onclick="javascript:<%#gotoMapLocation(Container.DataItem("x"),Container.DataItem("y"),Container.dataitem("projectid")) %>scrolltotopcust();" ><img style="vertical-align:bottom" src="images/mapicons/lightblue<%#getPagexFactor(Container.DataItem("projectid"))%>.gif" border=0 /></a></td>                                                                                                                                                                                     
						                         <!--      <td valign="top" align="center" width="111"><a href="property_detail.aspx?id=<%#Container.DataItem("projectid")%>"><img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("projectid")%>&type=project&crop=1&size=1&height=80&width=100" alt="<%#Container.DataItem("name").tostring.trim%>" /></a></td> -->
						                                <td style="vertical-align:middle" valign="middle" width="327"><a href="property_detail.aspx?id=<%#Container.DataItem("projectid")%>"><%#Container.DataItem("name").trim%></a><asp:Literal ID="literlmanaged2" runat="server" ></asp:Literal></td>
						                                <td style="vertical-align:middle" valign="middle" width="97"><%#Container.DataItem("majoruse").ToString.Trim%></td>
						                                <td style="vertical-align:middle" valign="middle" width="95" align="right"><%#FormatNumber(Container.DataItem("totalsf"), 0).ToString.Trim%></td>
						                                <td style="vertical-align:middle" valign="middle" width="127" align="right">$<%#FormatNumber(Container.DataItem("totalcost"), 0).ToString.Trim%></td>
						                               
						                </tr>
                                      
						          </ItemTemplate>
						          <AlternatingItemTemplate>
						                <tr class="even" style="height:35px;">
						                <td valign="middle" style="vertical-align:middle" align="left" width="16" ><a style="cursor:pointer; vertical-align:bottom" onclick="javascript:<%#gotoMapLocation(Container.DataItem("x"),Container.DataItem("y"),Container.dataitem("projectid")) %>scrolltotopcust();" target=_top><img style="vertical-align:bottom" src="images/mapicons/lightblue<%#getPagexFactor(Container.DataItem("projectid"))%>.gif" border=0/></a></td>                                                                                                                                                                                     
						                 <!--           <td valign="top" align="center" width="111"><a href="property_detail.aspx?id=<%#Container.DataItem("projectid")%>"><img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("projectid")%>&type=project&crop=1&size=1&height=80&width=100" alt="<%#Container.DataItem("name").tostring.trim%>" /></a></td>  -->
						                                <td style="vertical-align:middle" valign="middle" width="327"><a href="property_detail.aspx?id=<%#Container.DataItem("projectid")%>"><%#Container.DataItem("name").trim%></a><asp:Literal ID="literlmanaged2"  runat="server" ></asp:Literal></td>
						                                <td style="vertical-align:middle" valign="middle" width="97"><%#Container.DataItem("majoruse").ToString.Trim%></td>
						                                <td style="vertical-align:middle" valign="middle" width="95" align="right"><%#FormatNumber(Container.DataItem("totalsf"), 0).ToString.Trim%></td>
						                                <td style="vertical-align:middle" valign="middle" width="127" align="right">$<%#FormatNumber(Container.DataItem("totalcost"), 0).ToString.Trim%></td>
						                                
						                </tr>
						          </AlternatingItemTemplate>
						           
						          <FooterTemplate>
						          </tbody>
						          </table>
						          </FooterTemplate>
						          </asp:Repeater>
			                    <br /><br />
			                   <!-- <asp:Literal ID="literalPagingBottom" runat="server"></asp:Literal> -->
			                    </asp:PlaceHolder>				
				                </Content>
				                <LoadingPanelClientTemplate>
                                                                    <table cellspacing="0" cellpadding="0" width="620" border="0">
                                                                        <tr>
                                                                            <td align="center">
                                                                                <table cellspacing="0" cellpadding="0" border="0">
                                                                                    <tr>
                                                                                        <td style="font-size: 12px">
                                                                                            Loading...
                                                                                        </td>
                                                                                        <td>
                                                                                            <img height="16" src="images/spinner.gif" width="16" border="0">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </LoadingPanelClientTemplate>
				               
                                                                				                </ComponentArt:CallBack>

              <a class="button" style="background: white none repeat scroll 0 0;" href="Download.aspx?downloadtype=csv">Download List </a>
              <a class="button" style="background: white none repeat scroll 0 0;" href="Analyze.aspx"> View analysis of your results </a>
              <a class="button" style="background: white none repeat scroll 0 0;" href="Default.aspx"> New Search </a>


		
		

<br /><br />


          </div> <!-- end of left panel -->

          
        </div>
    </div>



























 <asp:Literal id="htmlbottom" runat=server></asp:Literal>
 </form>
 
 <script type="text/javascript">
     var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
     document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-2739433-5");
        pageTracker._trackPageview();
    } catch (err) { }</script>

</body> 
</html> 
