﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default"  %>
<%@ Register assembly="ComponentArt.Web.UI" namespace="ComponentArt.Web.UI" tagprefix="ComponentArt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<asp:Literal id="htmltop" runat=server></asp:Literal>



<head >
<title>WDCEP Project Search - Home</title>


<link rel="alternate" type="application/rss+xml" title="WDCEP &raquo; Feed" href="http://wdcep.com/?feed=rss2" />
<link rel="alternate" type="application/rss+xml" title="WDCEP &raquo; Comments Feed" href="http://wdcep.com/?feed=comments-rss2" />
<link rel="alternate" type="application/rss+xml" title="WDCEP &raquo; Home Comments Feed" href="http://wdcep.com/?feed=rss2&#038;page_id=10977" />
<link rel="stylesheet" id="flickr-gallery-css"  href='http://wdcep.com/wp-content/plugins/flickr-gallery/flickr-gallery.css' type='text/css' media='all' />
<link rel='stylesheet' id='fg-jquery-ui-css'  href='http://wdcep.com/wp-content/plugins/flickr-gallery/tab-theme/jquery-ui-1.7.3.css' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-flightbox-css'  href='http://wdcep.com/wp-content/plugins/flickr-gallery/flightbox/jquery.flightbox.css' type='text/css' media='all' />
<link rel='stylesheet' id='tubepress-css'  href='http://wdcep.com/wp-content/plugins/tubepress/src/main/web/css/tubepress.css' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='http://wdcep.com/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' />
<link rel='stylesheet' id='se-link-styles-css'  href='http://wdcep.com/wp-content/plugins/search-everything/static/css/se-styles.css' type='text/css' media='all' />
<link rel='stylesheet' id='wp-pagenavi-css'  href='http://wdcep.com/wp-content/plugins/wp-pagenavi/pagenavi-css.css' type='text/css' media='all' />
<link rel='stylesheet' id='parent-style-css'  href='http://wdcep.com/wp-content/themes/bones/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='http://wdcep.com/wp-content/themes/bones-child/library/css/font-awesome.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='framework-css'  href='http://wdcep.com/wp-content/themes/bones-child/library/css/gridle_framework.css' type='text/css' media='all' />
<link rel='stylesheet' id='googleFonts-css'  href='http://fonts.googleapis.com/css?family=Lato%3A400%2C700%2C400italic%2C700italic' type='text/css' media='all' />
<link rel='stylesheet' id='tablepress-default-css'  href='http://wdcep.com/wp-content/plugins/tablepress/css/default.min.css' type='text/css' media='all' />
<style id='tablepress-default-inline-css' type='text/css'>
    
.tablepress-id-1 th,.tablepress-id-1 .sorting{background-color:#effaea!important}.tablepress-id-1 .row-3 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-5 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-7 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-9 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-11 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-13 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-15 td{background-color:#f1f2f2!important}.tablepress-id-1 .row-17 td{background-color:#f1f2f2!important}.tablepress-id-1 .column-3{text-align:right}.tablepress-id-1 .column-4{text-align:center}.tablepress-id-2 th,.tablepress-id-2 .sorting{background-color:#fc9!important}.tablepress-id-2 .row-2 td{background-color:#f1f2f2!important}.tablepress-id-2 .row-40 td{background-color:#f1f2f2!important}.tablepress-id-2 .row-43 td{background-color:#f1f2f2!important}.tablepress-id-2 .row-48 td{background-color:#f1f2f2!important}.tablepress-id-2 .row-53 td{background-color:#f1f2f2!important}.tablepress-id-2 .column-2{text-align:right}.tablepress-id-3 th,.tablepress-id-3 .sorting{background-color:#b2e599!important}.tablepress-id-3 .row-3 td{background-color:#f1f2f2!important}.tablepress-id-3 .row-5 td{background-color:#f1f2f2!important}.tablepress-id-3 .row-7 td{background-color:#f1f2f2!important}.tablepress-id-3 .row-9 td{background-color:#f1f2f2!important}.tablepress-id-3 .column-2{text-align:right}.tablepress-id-3 .column-3{text-align:right}.tablepress-id-4 th,.tablepress-id-4 .sorting{background-color:#efe5ea!important}.tablepress-id-4 .row-3 td{background-color:#f1f2f2!important}.tablepress-id-4 .row-5 td{background-color:#f1f2f2!important}.tablepress-id-4 .column-3{text-align:center}.tablepress-id-4 .column-4{text-align:center}.tablepress-id-5 th,.tablepress-id-5 .sorting{background-color:#d1d3d4!important}.tablepress-id-5 .row-3 td{background-color:#d1d3d4!important}.tablepress-id-5 .row-10 td{background-color:#d1d3d4!important}.tablepress-id-5,.tablepress-id-5 td,.tablepress-id-5 th{border:none!important;border-collapse:collapse!important;border-spacing:0!important}.tablepress-id-6 th,.tablepress-id-6 .sorting{background-color:#d1d3d4!important}.tablepress-id-6 .row-1 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-3 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-5 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-7 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-9 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-11 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-13 td{background-color:#d1d3d4!important}.tablepress-id-6 .row-15 td{background-color:#d1d3d4!important}.tablepress-id-6 .column-3{text-align:right}.tablepress-id-6 .column-1{width:110px}.tablepress-id-6 .column-2{width:360px}.tablepress-id-6,.tablepress-id-6 td,.tablepress-id-6 th{border:none!important;border-collapse:collapse!important;border-spacing:0!important}.tablepress-id-7 th,.tablepress-id-7 .sorting{background-color:#fff!important}.tablepress-id-7 .column-1{width:110px}.tablepress-id-7 .column-2{width:200px}.tablepress-id-7 .column-3{width:10px}.tablepress-id-7 .column-4{width:110px}.tablepress-id-7 .column-5{width:200px}.tablepress-id-7,.tablepress-id-7 td,.tablepress-id-7 th{border:none!important;border-collapse:collapse!important;border-spacing:0!important}.tablepress-id-11 th,.tablepress-id-2 .sorting{background-color:#fc9!important}.tablepress-id-11 .column-2{text-align:left}.tablepress-id-11 .column-3{text-align:center}.tablepress-id-11 .column-4{text-align:center}
.button:hover, .button:focus {
    color: #80cec2;
}
</style>
</head>
<body style="margin: 0">

    <form id="form1" runat="server" action="Result.aspx">
    <div class="home-curated-content-feeds" style="width:950px; margin-left:200px; ">
     
        <div class="container" style="padding-top:0px">
         <h2 style="    color: #000000;   font-size: 27px;    font-weight: 900; vertical-align:top    ">Search over 1,200 development projects that have been completed since 2001, under construction or in the pipeline.</h2>
        <!-- left panel -->
        <div class="grid-8 parent grid-mobile-12">  

                 
          <div class="grid-12 news-list" style="margin-top:0px">
           
            <div class="home-newsletter" style="text-align: left;">
							<h4>
								Find a Project
							</h4>
							
                           
                            
                            
                                  <asp:TextBox ID="txtKeyword" runat="server" Width="400px" style="line-height: 10px;height:35px"  ></asp:TextBox>
                                   <!-- <input type="email" placeholder="Sign Me Up" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" size="40" value="" name="your-email"> -->
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a class="button" style="background: white none repeat scroll 0 0;line-height: 22px;height:35px; vertical-align:top" href="javascript: document.forms['form1'].submit();;"> Submit </a>
                                <!-- <input type="submit" class="wpcf7-form-control wpcf7-submit" value="Submit"><img class="ajax-loader" src="http://wdcep.com/wp-content/plugins/contact-form-7/images/ajax-loader.gif" alt="Sending ..." style="visibility: hidden;"> -->
                            
                           					
            </div>
           </div>
           <h4 >Additional Search Options</h4><br />
           <h3><b>I. Construction Type</b></h3><br />
           <asp:CheckBox id="newconstruction" runat="server" Text=" &nbsp;New Construction" Width="200" CssClass="content_window_input" />
					<asp:CheckBox id="Renovation" runat="server" Text=" &nbsp;Renovation"  Width="200" CssClass="content_window_input"/>
					<asp:CheckBox id="Infrastructure" runat="server" Text=" &nbsp;Infrastructure" /><br /><br /><br />
            <h3><b>II. Project Components</b></h3><br />
            Search by use:&nbsp;&nbsp;	<asp:DropDownList ID="searchbyuse" style="border:1px solid  #80cec2; width:150px;" runat="server"></asp:DropDownList><br /><br /><br />
            <h3><b>III. Construction Status</b></h3><br />
            <asp:CheckBox id="CheckBoxcompleted"  runat="server" /> &nbsp;Completed from &nbsp;&nbsp; <asp:DropDownList style=" border:1px solid  #80cec2; width:100px" ID="DropDownListcompletedyearfrom" Enabled=false runat="server"></asp:DropDownList> to <asp:DropDownList style=" border:1px solid  #80cec2; width:100px" ID="DropDownListcompletedyearto"  Enabled=false runat="server"></asp:DropDownList><br/>
				<asp:CheckBox id="CheckBoxunderconstruction" runat="server" Text=" &nbsp;Under Construction" CssClass="content_window_input" Width="200"/><br /><br />
				<asp:CheckBox id="CheckBoxNearTerm" runat="server" Text=" &nbsp;Near Term" CssClass="content_window_input" Width="200"/> <asp:CheckBox id="CheckBoxplanned" runat="server" Text=" &nbsp;Medium Term" Width="200" CssClass="content_window_input"/> <asp:CheckBox id="CheckBoxProposed" runat="server" Text=" &nbsp;Long Term" CssClass="content_window_input" /> 
				<br /><br /><br />
            <h3><b>IV. Project Location</b></h3><br />
            Ward:&nbsp;&nbsp;	<asp:DropDownList ID="ward" style=" border:1px solid  #80cec2; width:100px" runat="server"></asp:DropDownList>
            <br /><br /><br />
             <h3><b>V. Leed</b></h3>
             LEED Level:&nbsp;&nbsp;	<asp:DropDownList ID="leedlevel" style=" border:1px solid  #80cec2; width:100px" runat="server"></asp:DropDownList>
              <br /><br /><br />
              <a class="button" style="background: white none repeat scroll 0 0;" href="javascript: document.forms['form1'].submit();"> Submit </a>
              <a class="button" style="background: white none repeat scroll 0 0;" href="default.aspx"> Reset </a>
          </div> <!-- end of left panel -->

          <!-- right panel -->
          <div class="parent grid-4 grid-mobile-12 sidebar-events-list">
             
               <div class="home-events" style="margin-top:30px">
							<h3 class="section-title">
								Popular Searches
							</h3>

							<div class="clearfix">
															    
							    <ul style="vertical-align:middle; margin-top:0px; margin-bottom:0px; padding-top:0px; padding-bottom: 0px">
							        <li style="vertical-align:middle; margin-top:0px; margin-bottom:0px; padding-top:0px;">
                                    <br />
							        	<h4>
								        	<asp:LinkButton ID=lbpsgreendevelopment OnClientClick="javascript:SetCookie('pstype','greendevelopment');" runat=server>Green Development</asp:LinkButton>
								        </h4>
								        									
    							    </li>
							        <li style="vertical-align:middle; margin-top:0px; margin-bottom:0px; padding-top:0px;">
                                        <br />
							        	<h4>
								        	<asp:LinkButton ID=LinkButton1 OnClientClick="javascript:SetCookie('pstype','retailprojects');" runat=server>Retail Projects</asp:LinkButton>
								        </h4>
								        									
    							    </li>
							        <li style="vertical-align:middle; margin-top:0px; margin-bottom:0px; padding-top:0px;">
                                     <br />
							        	<h4>
								      		<asp:LinkButton ID=LinkButton2 OnClientClick="javascript:SetCookie('pstype','residentialprojects');" runat=server>Residential Projects</asp:LinkButton>
				                          </h4>
								        									
    							    </li>
							        <li style="vertical-align:middle; margin-top:0px; margin-bottom:0px; padding-top:0px;border-bottom-style: none">
                                     <br />
							        	<h4>
								        	<asp:LinkButton ID=LinkButton3 OnClientClick="javascript:SetCookie('pstype','officeprojects');" runat=server>Office Projects</asp:LinkButton>
		                                </h4>
								        									
    							    </li>

							    								</ul>
							
								
						      
							</div>
					    </div>
          </div>
          <!-- end of right panel -->
        </div>
    </div>






	<div id="projectsearchcont">
	</div>
	<div id="customquerycont">
	</div>
	
 </form>
 <script>

     function setCompletedDropdownstatus(bval) {
         if (bval) {
             var _DropDownListcompletedyearfrom = document.getElementById("DropDownListcompletedyearfrom")
             _DropDownListcompletedyearfrom.disabled = false;
             var _DropDownListcompletedyearto = document.getElementById("DropDownListcompletedyearto")
             _DropDownListcompletedyearto.disabled = false;             
         } else {
         var _DropDownListcompletedyearfrom = document.getElementById("DropDownListcompletedyearfrom")
         _DropDownListcompletedyearfrom.disabled = true;
         var _DropDownListcompletedyearto = document.getElementById("DropDownListcompletedyearto")
         _DropDownListcompletedyearto.disabled = true;        
         }

 
         
     }
 
     function GetCookie(name) {
         var nameEQ = name + "=";
         var ca = document.cookie.split(';');
         for (var i = 0; i < ca.length; i++) {
             var c = ca[i];
             while (c.charAt(0) == ' ') c = c.substring(1, c.length);
             if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
         }
         return null;
     }

     function SetCookie(name, value) {

         var days = 300
         var date = new Date();
         date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
         var expires = "; expires=" + date.toGMTString();
         document.cookie = name + "=" + escape(value) + expires + "; path=/";
         //document.cookie=name+ "=" +escape(value)+((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
     }

     function DeleteCookie(name) {

         var exp = new Date();
         exp.setTime(exp.getTime() - 1);
         var cval = GetCookie(name);
         document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
     }
     SetCookie('pstype', '');
 </script>
 
 <asp:Literal id="htmlbottom" runat=server></asp:Literal>



 <script type="text/javascript">
     var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
     document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-2739433-5");
        pageTracker._trackPageview();
    } catch (err) { }</script>




</body>

</html>



