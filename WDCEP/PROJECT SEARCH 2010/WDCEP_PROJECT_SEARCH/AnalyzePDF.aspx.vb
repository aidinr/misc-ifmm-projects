﻿Imports System.Data.SqlClient
Imports System.Data

Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

Imports System.IO
Imports System.Text




Partial Class AnalyzePDF
    Inherits System.Web.UI.Page



    Protected Sub Analyze_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(GetSQLTotals(MakeSQL(False)), MyConnection)
        Dim userid As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Request.QueryString("searchFilter").Replace("'", "'s'") & "%"
        Dim DT1 As New DataTable("Projects")
        MyCommand1.Fill(DT1)

        literalAGrid.Text += "<tr><td>Total Number of Projects</td><td class=""auto-style4"">" & FormatNumber(DT1.Rows(0)("total"), 0) & "</td><td class=""auto-style4""></td></tr>"
        Try

        
            literalAGrid.Text += "<tr><td>Total Sq. Ft.*</td><td class=""auto-style4""></td><td class=""auto-style5"">" & FormatNumber(DT1.Rows(0)("totalsf"), 0) & "</td></tr>"
        Catch ex As Exception

        End Try
        Try


            literalAGrid.Text += "<tr><td>Total Estimated Cost*</td><td class=""auto-style4""></td><td class=""auto-style5"">$" & FormatNumber(DT1.Rows(0)("totalcost"), 0) & "</td></tr>"

        Catch ex As Exception

        End Try
        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        MyCommand1 = New SqlDataAdapter(GetConstructionTypeTotals(MakeSQL(False)), MyConnection)
        Dim userid2 As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid2)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Request.QueryString("searchFilter").Replace("'", "''") & "%"
        Dim DT2 As New DataTable("Projects")
        MyCommand1.Fill(DT2)

        For Each row In DT2.Rows
            literalBGrid.Text += "<tr><td>" & row("item_value") & "</td><td class=""auto-style4"">" & FormatNumber(row("totals"), 0) & "</td><td class=""auto-style4""></td></tr>"
        Next

        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        MyCommand1 = New SqlDataAdapter(MakeSQL(False), MyConnection)
        Dim userid3 As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid3)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Request.QueryString("searchFilter").Replace("'", "''") & "%"
        Dim DT3 As New DataTable("Projects")
        MyCommand1.Fill(DT3)

        'get projectid list
        Dim _projectlist As String = ""
        For Each row As DataRow In DT3.Rows
            _projectlist += row("projectid") & ","
        Next
        'trim last ","
        _projectlist = _projectlist.Substring(0, _projectlist.Length - 1)

        Dim _community As DataTable = GetComponentTotals(2400022, 2400023, _projectlist)
        If _community.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Community</td><td class=""auto-style4"">" & FormatNumber(_community.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_community.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If
        Dim _education As DataTable = GetComponentTotals(2400025, 2400027, _projectlist)
        If _education.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Education</td><td class=""auto-style4"">" & FormatNumber(_education.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_education.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If
        Dim _entertainment As DataTable = GetComponentTotals(2400028, 2400029, _projectlist)
        If _entertainment.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Entertainment</td><td class=""auto-style4"">" & FormatNumber(_entertainment.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_entertainment.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If
        Dim _Hotel As DataTable = GetComponentTotals(2400031, 2400033, _projectlist)
        If _Hotel.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Hotel</td><td class=""auto-style4"">" & FormatNumber(_Hotel.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_Hotel.Rows(0)("value"), 0) & " rooms</td></tr>"
        End If
        Dim _industrial As DataTable = GetComponentTotals(2400034, 2400035, _projectlist)
        If _industrial.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Industrial</td><td class=""auto-style4"">" & FormatNumber(_industrial.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_industrial.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If
        Dim _Medical As DataTable = GetComponentTotals(2400036, 2400037, _projectlist)
        If _Medical.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Medical</td><td class=""auto-style4"">" & FormatNumber(_Medical.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_Medical.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If

        Dim _museum As DataTable = GetComponentTotals(2400038, 2400039, _projectlist)
        If _museum.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Museum</td><td class=""auto-style4"">" & FormatNumber(_museum.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_museum.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If

        Dim _office As DataTable = GetComponentTotals(2400040, 2400041, _projectlist)
        If _office.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Office</td><td class=""auto-style4"">" & FormatNumber(_office.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_office.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If
        Dim _residentialunits As DataTable = GetComponentTotals(2400042, 2400045, _projectlist)
        If _residentialunits.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Residential</td><td class=""auto-style4"">" & FormatNumber(_residentialunits.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_residentialunits.Rows(0)("value"), 0) & " units</td></tr>"
        End If
        Dim _retail As DataTable = GetComponentTotals(2400051, 2400052, _projectlist)
        If _retail.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Retail</td><td class=""auto-style4"">" & FormatNumber(_retail.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_retail.Rows(0)("value"), 0) & " sq. ft.</td></tr>"
        End If
        Dim _parking As DataTable = GetComponentTotals(2400053, 2400054, _projectlist)
        If _parking.Rows(0)("count") > 0 Then
            literalCGrid.Text += "<tr><td>Parking</td><td class=""auto-style4"">" & FormatNumber(_parking.Rows(0)("count"), 0) & "</td><td class=""auto-style5"">" & FormatNumber(_parking.Rows(0)("value"), 0) & " spaces</td></tr>"
        End If




        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        MyCommand1 = New SqlDataAdapter(GetConstructionStatusTotals(MakeSQL(False)), MyConnection)
        Dim userid4 As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid4)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Request.QueryString("searchFilter").Replace("'", "''") & "%"
        Dim DT4 As New DataTable("Projects")
        MyCommand1.Fill(DT4)

        For Each row In DT4.Rows
            literalDGrid.Text += "<tr><td>" & row("item_value") & "</td><td class=""auto-style4"">" & FormatNumber(row("totals"), 0) & "</td><td class=""auto-style5"">$" & FormatNumber(row("totalcost"), 0) & " </td></tr>"
        Next


        MyConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        MyCommand1 = New SqlDataAdapter(GetLeedLevelTotals(MakeSQL(False)), MyConnection)
        Dim userid5 As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid5)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Request.QueryString("searchFilter").Replace("'", "''") & "%"
        Dim DT6 As New DataTable("Projects")
        MyCommand1.Fill(DT6)

        For Each row In DT6.Rows
            literalEGrid.Text += "<tr><td>" & row("item_value") & "</td><td class=""auto-style4"">" & FormatNumber(row("totals"), 0) & "</td><td class=""auto-style4""></td></tr>"
        Next




    End Sub

    Public Function MakeSQL(Optional ByVal blnUseLimiter As Boolean = True, Optional ByVal blnaddordering As Boolean = False) As String

        Return Functions.MakeSQL(Request.QueryString("pstype"), Request.QueryString("leedlevel"), Request.QueryString("ward"), Request.QueryString("undercontruction"), Request.QueryString("completedfrom"), Request.QueryString("completedto"), Request.QueryString("proposed"), Request.QueryString("planned"), Request.QueryString("completed"), Request.QueryString("newconstruction"), Request.QueryString("rennovation"), Request.QueryString("infrastructure"), Request.QueryString("searchbyuse"), blnUseLimiter, blnaddordering, Request.QueryString("nearterm"))


    End Function

    Function GetSQLTotals(ByVal sql) As String
        Return "select count(*) total, sum(cast (totalcost as decimal)) totalcost, sum(cast(totalsf as integer)) totalsf from (" & sql & ") a"
    End Function

    Function GetConstructionTypeTotals(ByVal sql) As String
        Return "select * from (select COUNT(*) totals, item_value from ipm_project a, IPM_PROJECT_FIELD_VALUE b where a.projectid = b.ProjectID and b.Item_ID = 2400016 and a.projectid in (select projectid from (" & sql & ") a) group by item_value) a where cast(Item_Value as varchar(50)) in ('New Construction','Renovation','Infrastructure')"
    End Function

    Function GetComponentTotals(ByVal _mainid As String, ByVal _mainidvalue As String, ByVal pids As String) As DataTable
        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select count(*) count, sum(CAST(case when rtrim(a.item_value) = '' then 0 when ltrim(a.item_value) = '' then 0 else isnull(a.item_value,0) end AS decimal)) value from ipm_project_field_value a, ipm_project_field_value b, IPM_PROJECT c where a.item_id = " & _mainidvalue & " and b.Item_ID =  " & _mainid & " and b.Item_value = '1' and a.ProjectID = c.ProjectID and b.ProjectID = c.projectid and c.ProjectID in (" & pids & ")", MyConnection)
        Dim DT1 As New DataTable("Projects")
        MyCommand1.Fill(DT1)
        Return DT1
    End Function


    Function GetConstructionStatusTotals(ByVal sql) As String
        Return "select COUNT(*) totals,sum (CAST(totalcost AS decimal)) totalcost,sum (CAST(totalsf AS decimal)) totalsf, replace(replace(item_value,'(over two years)',''),'(past two years)','') item_value from ipm_project a, IPM_PROJECT_FIELD_VALUE b,(" & sql & ") c where a.projectid = b.ProjectID and a.ProjectID = c.projectid and b.Item_ID = 2400017 group by replace(replace(item_value,'(over two years)',''),'(past two years)','') order by case when replace(replace(item_value,'(over two years)',''),'(past two years)','')  = 'Completed' then 1 when replace(replace(item_value,'(over two years)',''),'(past two years)','')  = 'Under Construction' then 2 when replace(replace(item_value,'(over two years)',''),'(past two years)','')  = 'Near Term' then 3 when replace(replace(item_value,'(over two years)',''),'(past two years)','')  = 'Medium Term' then 4 when replace(replace(item_value,'(over two years)',''),'(past two years)','')  = 'Long Term' then 5 end"
    End Function


    Function GetLeedLevelTotals(ByVal sql) As String
        Return "select * from (select COUNT(*) totals, item_value from ipm_project a, IPM_PROJECT_FIELD_VALUE b where a.projectid = b.ProjectID and b.Item_ID = 2400099 and a.projectid in (select projectid from (" & sql & ") a) group by item_value) a where item_value <> '' and item_value <> '0' order by case when Item_Value = 'Certified' then 1 when Item_Value = 'Silver' then 2 when Item_Value = 'Gold' then 3 when Item_Value = 'Platinum' then 4 end"
    End Function








End Class
