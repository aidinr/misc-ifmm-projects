﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.IO

Partial Class Result
    Inherits System.Web.UI.Page

    Dim dtLatLng As XmlDocument
    Public repeaterCounter As Integer = 0
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        'Get Main HTML Template

        htmltop.Text = Functions.GetMainHTMLWrapper(Request.QueryString("htmlcache"))(0)
        htmlbottom.Text = Functions.GetMainHTMLWrapper(Request.QueryString("htmlcache"))(2)


        dtLatLng = CType(Application("dsLats"), XmlDocument)
        Dim callbackcheck As Boolean = False
        Try
            If (InStr(Request.Form.AllKeys(0).ToString, "Callback") > 0) Then
                callbackcheck = True
            End If
        Catch ex As Exception

        End Try


        If Not callbackcheck Then

            '    If Request.Form.Item("SnapWebUI$txtKeyword") <> "" Then

            If InStr(Request.UrlReferrer.AbsolutePath, "Property_Detail.aspx", CompareMethod.Text) = 0 And InStr(Request.UrlReferrer.AbsolutePath, "Result.aspx", CompareMethod.Text) = 0 And InStr(Request.UrlReferrer.AbsolutePath, "Analyze.aspx", CompareMethod.Text) = 0 Then
                Session("searchFilter") = Request.Form.Item("txtKeyword")
                If Session("searchFilter") Is Nothing Then
                    Session("searchFilter") = ""
                End If
                Session("newconstruction") = Request.Form.Item("newconstruction")
                Session("Rennovation") = Request.Form.Item("Renovation")
                Session("Infrastructure") = Request.Form.Item("Infrastructure")
                Session("searchbyuse") = Request.Form.Item("searchbyuse")
                Session("status") = Request.Form.Item("status")
                Session("ward") = Request.Form.Item("ward")
                'Session("zipcode") = Request.Form.Item("zipcode")
                Session("leedlevel") = Request.Form.Item("leedlevel")
                Session("completed") = Request.Form.Item("CheckBoxcompleted")
                Session("completedfrom") = Request.Form.Item("DropDownListcompletedyearfrom")
                Session("completedto") = Request.Form.Item("DropDownListcompletedyearto")
                Session("nearterm") = Request.Form.Item("CheckBoxNearTerm")
                Session("planned") = Request.Form.Item("CheckBoxplanned")
                Session("proposed") = Request.Form.Item("CheckBoxProposed")
                Session("undercontruction") = Request.Form.Item("CheckBoxunderconstruction")
                Session("nearterm") = Request.Form.Item("CheckBoxNearTerm")


            End If
            Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))




            'construct SQL

            Try
               
                'htmlbottom.Text = Functions.MakeSQL(Request.Cookies.Item("pstype").Value, Session("leedlevel"), Session("ward"), Session("undercontruction"), Session("completedfrom"), Session("completedto"), Session("proposed"), Session("planned"), Session("completed"), Session("newconstruction"), Session("rennovation"), Session("infrastructure"), Session("searchbyuse"), False, , Session("nearterm"))
                Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(Functions.MakeSQL(Request.Cookies.Item("pstype").Value, Session("leedlevel"), Session("ward"), Session("undercontruction"), Session("completedfrom"), Session("completedto"), Session("proposed"), Session("planned"), Session("completed"), Session("newconstruction"), Session("rennovation"), Session("infrastructure"), Session("searchbyuse"), False, , Session("nearterm")), MyConnection)
                Dim userid As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
                MyCommand1.SelectCommand.Parameters.Add(userid)
                MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Session("searchFilter").Replace("'", "''") & "%"
                Dim DT1 As New DataTable("Projects")


                Dim totalitems As Integer = 0
                Dim numitemsonpage As Integer = 0
                Dim limitedtotalitems As Integer = 0

                MyCommand1.Fill(DT1)
                totalitems = DT1.Rows.Count
                DT1 = New DataTable("Projects")
                MyCommand1.SelectCommand.CommandText = Functions.MakeSQL(Request.Cookies.Item("pstype").Value, Session("leedlevel"), Session("ward"), Session("undercontruction"), Session("completedfrom"), Session("completedto"), Session("proposed"), Session("planned"), Session("completed"), Session("newconstruction"), Session("rennovation"), Session("infrastructure"), Session("searchbyuse"), , , Session("nearterm"))
                MyCommand1.Fill(DT1)
                limitedtotalitems = DT1.Rows.Count

                Dim dvb As DataView = New DataView(DT1)

                If Session("searchSort") = "" Then
                    Session("searchSort") = "pn"
                End If

                Dim sSort As String
                Select Case Session("searchSort")
                    Case "pn"
                        sSort = "name"
                    Case "mu"
                        sSort = "majoruse"
                    Case "sf"
                        sSort = "totalsf"
                    Case "ec"
                        sSort = "totalcost"
                    Case Else
                        sSort = "name"
                End Select



                If Session("searchSortby") = "" Then
                    Session("searchSortby") = "asc"
                End If
                dvb.Sort = sSort & " " & Session("searchSortby")

                literalResults.Text = "( Showing " & limitedtotalitems & " of " & totalitems & " )"



                'Dim ii As Integer
                Dim upperbounds(2) As Decimal
                upperbounds(0) = 38.91971
                upperbounds(1) = -77.02931
                Dim lowerbounds(2) As Decimal
                lowerbounds(0) = 38.91971
                lowerbounds(1) = -77.02931

                Dim adjustbounds As Decimal = 0
                If DT1.Rows.Count = 1 Then
                    adjustbounds = 0.001

                End If
                Dim ii As Integer = 1



                'Dim dtLatLng As XmlDocument = CType(Application("dsLats"), XmlDocument)

                Dim GoogleHashTable As New Hashtable



                For Each row In dvb
                    Try
                        'get address
                        'var address = "821 Mohawk Street, Columbus OH";
                        'addToMap(map,address);
                        '"275-291 Bedford Ave, Brooklyn, NY 11211, USA"
                        'ltrladdresses.Text += "var address = """ & row("address") & ",DC " & row("zip") & ", USA"";addToMap(map,address);"
                        'var marker = new google.maps.Marker({position: myLatlng, map: map, title:"Hello World!"}); 
                        'get conversion to gps
                        ' '' '' '' '' ''Dim citizenatlas As New gov.dc.citizenatlas.getUSNG
                        ' '' '' '' '' ''Dim xy As String = row("x") & "," & row("y")
                        ' '' '' '' '' ''Dim latlng As String = citizenatlas.MD_SPCStoLL(xy).Tables(0).Rows(0)("convstr")


                        Dim latlng As String = ""
                        Dim latlngrow = dtLatLng.SelectNodes("descendant::Projects[projectid='" & row("projectid").ToString & "']")
                        'nodeList=root.SelectNodes("descendant::book[author/last-name='Austen']")


                        latlng = latlngrow.Item(0).ChildNodes(1).InnerText

                        If adjustbounds <> 0 Then
                            upperbounds(0) = latlng.Split(",")(0)
                            upperbounds(1) = latlng.Split(",")(1)
                            lowerbounds(0) = latlng.Split(",")(0)
                            lowerbounds(1) = latlng.Split(",")(1)
                        End If

                        If latlng.Split(",")(0) <= upperbounds(0) Then
                            upperbounds(0) = CType(latlng.Split(",")(0), Decimal) - adjustbounds
                        End If
                        If latlng.Split(",")(1) >= upperbounds(1) Then
                            upperbounds(1) = CType(latlng.Split(",")(1), Decimal) + adjustbounds
                        End If
                        'get lowerbounds
                        If latlng.Split(",")(0) >= lowerbounds(0) Then
                            lowerbounds(0) = CType(latlng.Split(",")(0), Decimal) + adjustbounds
                        End If
                        If latlng.Split(",")(1) <= lowerbounds(1) Then
                            lowerbounds(1) = CType(latlng.Split(",")(1), Decimal) - adjustbounds
                        End If

                        ltrladdresses.Text += "myLatLng" & " = new google.maps.LatLng(" & latlng & ");"
                        ltrladdresses.Text += "marker" & ii & " = new google.maps.Marker({position: myLatLng" & ", map: map,icon: 'images/mapicons/lightblue" & ii & ".gif', title:""" & row("name").ToString.Trim.Replace("'", " ").Replace("""", " ") & """});"


                        'info win
                        ltrladdresses.Text += "infowindow" & ii & " = new google.maps.InfoWindow({content:'<table style=""font-family:verdana;font-size:10px;""><tr><td><a href=""property_detail.aspx?id=" & row("projectid").ToString.Trim.Replace("'", "") & """><img src=""" & Session("WSRetreiveAsset") & "id=" & row("projectid") & "&type=project&crop=1&size=1&height=80&width=100" & """ alt=""" & row("name").ToString.Trim.Replace("'", " ").Replace("""", " ") & """ /></a></td><td valign=""top"" style=""vertical-align:top;padding-left:5px;""><a href=""property_detail.aspx?id=" & row("projectid").ToString.Trim.Replace("'", "") & """><b>" & row("name").ToString.Trim.Replace("'", " ").Replace("""", " ") & "</b></a><br>Total Cost: $" & FormatNumber(row("totalcost").ToString.Trim.Replace("'", ""), 0) & "<br>SF: " & FormatNumber(row("totalsf").ToString.Trim.Replace("'", ""), 0) & "<br>Major Use: " & row("majoruse").ToString.Trim.Replace("'", "") & "<br><br><a href=""property_detail.aspx?id=" & row("projectid").ToString.Trim.Replace("'", "") & """>view details</a></td></tr></table>'});"
                        ltrladdresses.Text += "google.maps.event.addListener(marker" & ii & ", 'click', function() {infowindow" & ii & ".open(map,marker" & ii & ");});"

                        GoogleHashTable.Add(row("projectid").ToString, ii)
                    Catch ex As Exception

                    End Try
                    ii += 1

                    'ltrladdresses.Text += "addToMap(map,""" & row("address") & ",DC " & row("zip") & ", USA"");"
                Next

                Session("GoogleHashTable") = GoogleHashTable

                'set bounds
                'up, down, down, up - 0.001

                'ltrladdresses.Text += "var myUpperLatLng = new google.maps.LatLng(38.9004944947735,-76.9358761102871);var myLowerLatLng = new google.maps.LatLng(38.8984944947735,-76.9388761102871);var bounds = new google.maps.LatLngBounds(myLowerLatLng,myUpperLatLng);map.fitBounds(bounds);"
                ltrladdresses.Text += "var myUpperLatLng = new google.maps.LatLng(" & upperbounds(0).ToString & "," & upperbounds(1).ToString & ");"
                ltrladdresses.Text += "var myLowerLatLng = new google.maps.LatLng(" & lowerbounds(0).ToString & "," & lowerbounds(1).ToString & ");"
                ltrladdresses.Text += "var bounds = new google.maps.LatLngBounds(myLowerLatLng,myUpperLatLng);map.fitBounds(bounds);"

                Dim dv As DataView = New DataView(DT1)


                If Session("searchSort") = "" Then
                    Session("searchSort") = "pn"
                End If

                Dim sSort2 As String
                Select Case Session("searchSort")
                    Case "pn"
                        sSort2 = "name"
                    Case "mu"
                        sSort2 = "majoruse"
                    Case "sf"
                        sSort2 = "totalsf"
                    Case "ec"
                        sSort2 = "totalcost"
                    Case Else
                        sSort2 = "name"
                End Select



                If Session("searchSortby") = "" Then
                    Session("searchSortby") = "asc"
                End If
                dv.Sort = sSort2 & " " & Session("searchSortby")

                Dim dTbl As DataTable = dv.Table


                Dim pgDataSc As PagedDataSource = New PagedDataSource
                pgDataSc.DataSource = dv
                pgDataSc.AllowPaging = True
                pgDataSc.PageSize = CInt(System.Configuration.ConfigurationManager.AppSettings("SearchPageSize"))
                pgDataSc.CurrentPageIndex = 0

                RepeaterSearchResults.DataSource = pgDataSc

                RepeaterSearchResults.DataBind()

                Dim pageCount As Integer = pgDataSc.PageCount

                Dim pagingString As String = ""
                If pageCount > 0 Then
                    For i As Integer = 0 To pageCount - 1
                        If (i = pgDataSc.CurrentPageIndex) Then
                            pagingString = pagingString & "<div style='display: table-cell;'>&nbsp;&nbsp;<b>" & (i + 1) & "</b>&nbsp;&nbsp; </div>"
                        Else
                            pagingString = pagingString & "<div style='display: table-cell;'>&nbsp;&nbsp;<a href=""javascript:switchPage(" & i & ");"">" & (i + 1) & "</a> &nbsp;&nbsp;</div> "
                        End If

                    Next
                    pagingString = pagingString & "<br />"
                End If

                literalPaging.Text = pagingString
                literalPagingBottom.Text = "<br/>"
                'Else

                'End If




                'For Each x As DataColumn In dt.Columns
                '    Console.WriteLine(x.ColumnName)
                'Next

                'RepeaterSearchResults.DataSource = dv
                'RepeaterSearchResults.DataBind()


                ' textboxFilter.Attributes.Add("onkeydown", "if(event.keyCode==13){filterSearch(this.value);return false;}")


                Session("searchPaging") = 0
                ' Session("searchFilter") = ""

            Catch ex As Exception
                Response.Redirect("Default.aspx")
            End Try


        End If

    End Sub

    Public Function MakeSQL(Optional ByVal blnUseLimiter As Boolean = True) As String


        Return Functions.MakeSQL(Request.Cookies.Item("pstype").Value, Session("leedlevel"), Session("ward"), Session("undercontruction"), Session("completedfrom"), Session("completedto"), Session("proposed"), Session("planned"), Session("completed"), Session("newconstruction"), Session("rennovation"), Session("infrastructure"), Session("searchbyuse"), blnUseLimiter, , Session("nearterm"))


        ' '' '' ''Dim sqllimiter As String = ""

        ' '' '' ''Dim sql As String = "select case when rtrim(h.item_value) = '' then 0 else h.item_value end totalcost, case when rtrim(g.item_value) = '' then 0 else g.item_value end totalsf, f.item_value majoruse, d.item_value x, e.item_value y, a.projectid,a.name, b.item_value address, c.item_value zip from ipm_project a, ipm_project_field_value b, ipm_project_field_value c, ipm_project_field_value d, ipm_project_field_value e, ipm_project_field_value f, ipm_project_field_value g, ipm_project_field_value h where a.projectid = h.projectid and h.item_id = 2400055 and a.projectid = g.projectid and g.item_id = 2400019 and a.projectid = f.projectid and f.item_id = 2400021 and a.projectid = e.projectid and a.projectid = d.projectid and a.projectid = c.projectid and c.item_id = 2400008 and a.projectid = b.projectid and b.item_id = 2400007 and d.item_id = 2400147 and e.item_id = 2400148 and a.search_tags like @keyword and available = 'Y'"
        ' '' '' ''If Session("newconstruction") = "on" Or Session("Renovation") = "on" Or Session("Infrastructure") = "on" Then
        ' '' '' ''    Dim sqltmp As String = ""

        ' '' '' ''    If Session("newconstruction") = "on" Then
        ' '' '' ''        sqltmp += "b.item_value = 'New Construction'"
        ' '' '' ''    End If
        ' '' '' ''    If Session("Renovation") = "on" Then
        ' '' '' ''        If sqltmp.Length > 0 Then
        ' '' '' ''            sqltmp += " or b.item_value = 'Renovation'"
        ' '' '' ''        Else
        ' '' '' ''            sqltmp += " b.item_value = 'Renovation'"
        ' '' '' ''        End If
        ' '' '' ''    End If
        ' '' '' ''    If Session("Infrastructure") = "on" Then
        ' '' '' ''        If sqltmp.Length > 0 Then
        ' '' '' ''            sqltmp += " or b.item_value = 'Infrastructure'"
        ' '' '' ''        Else
        ' '' '' ''            sqltmp += " b.item_value = 'Infrastructure'"
        ' '' '' ''        End If
        ' '' '' ''    End If
        ' '' '' ''    If sqltmp.Length > 0 Then
        ' '' '' ''        sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (" & sqltmp & ") and b.item_id = 2400016"
        ' '' '' ''    End If

        ' '' '' ''End If

        ' '' '' ''If Session("searchbyuse") <> "All" Then
        ' '' '' ''    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where item_name = '" & Session("searchbyuse") & "' and Item_Tab = 'components')"
        ' '' '' ''End If

        '' '' '' ''status
        '' '' '' ''Session("status") = Request.Form.Item("status") - 2400017
        '' '' '' '' '' '' ''If Session("status") <> "All" Then
        '' '' '' '' '' '' ''    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '" & Session("status") & "') and b.item_id = 2400017"
        '' '' '' '' '' '' ''End If


        '' '' '' ''status completed
        ' '' '' ''If Session("completed") = "on" Then
        ' '' '' ''    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b, IPM_PROJECT_FIELD_VALUE c, IPM_PROJECT_FIELD_VALUE d where a.ProjectID = b.projectid and a.ProjectID = c.projectid and a.ProjectID = d.projectid and (b.item_value like 'completed%') and b.item_id = 2400017 and (" & Session("completedfrom") & " <= datepart( yyyy,c.item_value)) and c.Item_ID = 2400089 and (" & Session("completedto") & " >= datepart( yyyy,d.item_value)) and d.Item_ID = 2400089"
        ' '' '' ''End If

        '' '' '' ''status planned
        ' '' '' ''If Session("planned") = "on" Then
        ' '' '' ''    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value like 'Long Term') and b.item_id = 2400017 "
        ' '' '' ''End If

        '' '' '' ''status proposed
        ' '' '' ''If Session("proposed") = "on" Then
        ' '' '' ''    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value like 'Medium Term') and b.item_id = 2400017 "
        ' '' '' ''End If

        '' '' '' ''status proposed
        ' '' '' ''If Session("nearterm") = "on" Then
        ' '' '' ''    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value like 'Near Term') and b.item_id = 2400017 "
        ' '' '' ''End If


        '' '' '' ''CheckBoxNearTerm

        '' '' '' ''status undercontruction
        ' '' '' ''If Session("undercontruction") = "on" Then
        ' '' '' ''    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value like 'Under Construction') and b.item_id = 2400017 "
        ' '' '' ''End If


        '' '' '' ''Ward
        '' '' '' ''Session("ward") = Request.Form.Item("ward") - 2400017
        ' '' '' ''If Session("ward") <> "All" Then
        ' '' '' ''    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '" & Session("ward") & "') and b.item_id = 2400009"
        ' '' '' ''End If


        '' '' '' ''Zip
        ' '' '' '' '' '' '' '' ''Session("zipcode") = Request.Form.Item("zipcode") - 2400008
        '' '' '' '' '' '' '' ''If Session("zipcode") <> "All" Then
        '' '' '' '' '' '' '' ''    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '" & Session("zipcode") & "') and b.item_id = 2400008"
        '' '' '' '' '' '' '' ''End If

        '' '' '' ''leedlevel
        '' '' '' ''Session("leedlevel") = Request.Form.Item("leedlevel") - 2400099
        ' '' '' ''If Session("leedlevel") <> "All" Then
        ' '' '' ''    sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '" & Session("leedlevel") & "') and b.item_id = 2400099"
        ' '' '' ''End If




        '' '' '' ''add popular search 
        ' '' '' ''If Request.Cookies.Item("pstype").Value <> "" Then
        ' '' '' ''    Select Case Request.Cookies.Item("pstype").Value
        ' '' '' ''        Case "greendevelopment"
        ' '' '' ''            '2400098
        ' '' '' ''            sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = 2400098"
        ' '' '' ''        Case "retailprojects"
        ' '' '' ''            '2400051
        ' '' '' ''            sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = 2400051"
        ' '' '' ''        Case "residentialprojects"
        ' '' '' ''            '2400042
        ' '' '' ''            sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = 2400042"
        ' '' '' ''        Case "officeprojects"
        ' '' '' ''            '2400040
        ' '' '' ''            sql = "select a.* from (" & sql & ") a, ipm_project_field_value b where a.ProjectID = b.projectid and (b.item_value = '1') and b.item_id = 2400040"
        ' '' '' ''    End Select
        ' '' '' ''End If





        '' '' '' ''add limiter
        ' '' '' ''If blnUseLimiter Then
        ' '' '' ''    Dim sqltemp As String = Sql
        ' '' '' ''    sql = "select top " & System.Configuration.ConfigurationManager.AppSettings("MaxResults") & " * from (" & sqltemp & ") a"
        ' '' '' ''End If

        '' '' '' ''add ordering
        ' '' '' ''sql += " order by name asc"

        ' '' '' ''Return sql

    End Function

    Public Function gotoMapLocation(ByVal x As Decimal, ByVal y As Decimal, ByVal projectid As String) As String


        ' '' '' ''Dim citizenatlas As New gov.dc.citizenatlas.getUSNG
        ' '' '' ''Dim xy As String = x & "," & y
        ' '' '' ''Dim latlng As String = citizenatlas.MD_SPCStoLL(xy).Tables(0).Rows(0)("convstr")


        Try

      



            Dim latlng As String = ""
            Dim latlngrow = dtLatLng.SelectNodes("descendant::Projects[projectid='" & projectid & "']")
            latlng = latlngrow.Item(0).ChildNodes(1).InnerText
            Return "map.setCenter(new google.maps.LatLng(" & latlng & "),19);map.setZoom(16);"
        Catch ex As Exception

        End Try
    End Function

    Public Function getPagexFactor(ByVal projectid As String) As Integer
        'Dim _PagedDataSource As PagedDataSource = RepeaterSearchResults.DataSource
        'Return _PagedDataSource.CurrentPageIndex * CInt(System.Configuration.ConfigurationManager.AppSettings("SearchPageSize"))
        Try
            Return CType(Session("GoogleHashTable"), Hashtable).Item(projectid).ToString
        Catch ex As Exception

        End Try

    End Function



    Public Sub RepeaterSearchResultsItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim literalAvailability As Literal = e.Item.FindControl("literalAvailability")
            Dim literalTenants As Literal = e.Item.FindControl("literalTenants")
            Dim theProjectID As String = e.Item.DataItem("projectID")

            Dim literalmanaged2 As Literal = e.Item.FindControl("literlmanaged2")




        End If


    End Sub

    Public Sub searchCallback_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles searchCallback.Callback
        If (e.Parameters(0) = "paging") Then

            executeCallback(CInt(e.Parameters(1)), Session("searchFilter"), Session("searchSort"), Session("searchSortby"))
            Session("searchPaging") = e.Parameters(1)
        ElseIf (e.Parameters(0) = "sort") Then
            Dim sSortBy As String
            If Session("searchSortby") = "asc" Then
                sSortBy = "desc"
            Else
                sSortBy = "asc"
            End If
            executeCallback(Session("searchPaging"), Session("searchFilter"), e.Parameters(1), sSortBy)
            Session("searchSort") = e.Parameters(1)
            Session("searchSortby") = sSortBy
        ElseIf (e.Parameters(0) = "filter") Then
            Session("searchPaging") = "0"
            executeCallback(Session("searchPaging"), e.Parameters(1), Session("searchSort"), Session("searchSortby"))
            Session("searchFilter") = e.Parameters(1)
        ElseIf (e.Parameters(0) = "getcsv") Then

            getcsv()
            Exit Sub
        End If


        literalPaging.RenderControl(e.Output)
        RepeaterSearchResults.RenderControl(e.Output)
        literalPagingBottom.RenderControl(e.Output)
    End Sub

    Private Sub executeCallback(ByVal paging As Integer, ByVal filter As String, ByVal sort As String, ByVal sortby As String)

        Dim searchMode = "search"

        Dim dt1 As DataTable = New DataTable





        If (searchMode = "search") Then
            'Dim searchState = Request.QueryString("state").Trim
            'Dim searchString = Request.QueryString("string").Trim

            'If (searchString = "Keyword Search") Then
            '    searchString = ""
            'End If

            'If (searchState = "All States") Then
            '    searchState = ""
            'End If

            Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(Functions.MakeSQL(Request.Cookies.Item("pstype").Value, Session("leedlevel"), Session("ward"), Session("undercontruction"), Session("completedfrom"), Session("completedto"), Session("proposed"), Session("planned"), Session("completed"), Session("newconstruction"), Session("rennovation"), Session("infrastructure"), Session("searchbyuse"), , , Session("nearterm")), MyConnection)
            Dim userid As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
            MyCommand1.SelectCommand.Parameters.Add(userid)
            MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Session("searchFilter").Replace("'", "''") & "%"


            MyCommand1.Fill(dt1)



        Else
            Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(Functions.MakeSQL(Request.Cookies.Item("pstype").Value, Session("leedlevel"), Session("ward"), Session("undercontruction"), Session("completedfrom"), Session("completedto"), Session("proposed"), Session("planned"), Session("completed"), Session("newconstruction"), Session("rennovation"), Session("infrastructure"), Session("searchbyuse"), , , Session("nearterm")), MyConnection)
            Dim userid As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
            MyCommand1.SelectCommand.Parameters.Add(userid)
            MyCommand1.SelectCommand.Parameters("@keyword").Value = ""


            MyCommand1.Fill(dt1)

        End If




        Dim dv As DataView = New DataView(dt1)
        'name, location, gla, availability
        Dim sSort As String
        Select Case sort
            Case "pn"
                sSort = "name"
            Case "mu"
                sSort = "majoruse"
            Case "sf"
                sSort = "totalsf"
            Case "ec"
                sSort = "totalcost"
            Case Else
                sSort = "name"
        End Select

        dv.Sort = sSort & " " & sortby
        '

        If (filter <> "") Then
            'dv.RowFilter = "name LIKE '%" & filter & "%'"

        End If




        Dim dTbl As DataTable = dv.Table

        Dim pgDataSc As PagedDataSource = New PagedDataSource
        pgDataSc.DataSource = dv
        pgDataSc.AllowPaging = True
        pgDataSc.PageSize = CInt(System.Configuration.ConfigurationManager.AppSettings("SearchPageSize"))
        pgDataSc.CurrentPageIndex = paging

        RepeaterSearchResults.DataSource = pgDataSc
        RepeaterSearchResults.DataBind()

        Dim pageCount As Integer = pgDataSc.PageCount

        Dim pagingString As String = ""
        If pageCount > 0 Then

            For i As Integer = 0 To pageCount - 1
                If (i = pgDataSc.CurrentPageIndex) Then


                    pagingString = pagingString & "<div style='display: table-cell;'>&nbsp;&nbsp;<b>" & (i + 1) & "</b>&nbsp;&nbsp; </div>"
                Else
                    pagingString = pagingString & "<div style='display: table-cell;'>&nbsp;&nbsp;<a href=""javascript:switchPage(" & i & ");"">" & (i + 1) & "</a>&nbsp;&nbsp;</div> "
                End If


                ''pagingString = pagingString & "<b>" & (i + 1) & "</b> "
                ''Else
                ''pagingString = pagingString & "<a href=""javascript:switchPage(" & i & ");"">" & (i + 1) & "</a> "
                ''End If

            Next
            pagingString = pagingString & "<br />"
        End If
        literalPaging.Text = pagingString
        literalPagingBottom.Text = "<br/>"

    End Sub





    Private Sub getcsv()
        'Get the data from database into datatable

        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(Functions.MakeSQL(Request.Cookies.Item("pstype").Value, Session("leedlevel"), Session("ward"), Session("undercontruction"), Session("completedfrom"), Session("completedto"), Session("proposed"), Session("planned"), Session("completed"), Session("newconstruction"), Session("rennovation"), Session("infrastructure"), Session("searchbyuse"), , , Session("nearterm")), MyConnection)
        Dim userid As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & Session("searchFilter").Replace("'", "''") & "%"
        Dim DT As New DataTable("Projects")
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", _
                "attachment;filename=Results.csv")
        Response.Charset = ""
        Response.ContentType = "application/text"

        Dim sb As New StringBuilder()
        For k As Integer = 0 To DT.Columns.Count - 1
            'add separator
            sb.Append(DT.Columns(k).ColumnName + ","c)
        Next
        'append new line
        sb.Append(vbCr & vbLf)
        For i As Integer = 0 To DT.Rows.Count - 1
            For k As Integer = 0 To DT.Columns.Count - 1
                'add separator
                sb.Append(DT.Rows(i)(k).ToString().Replace(",", ";") + ","c)
            Next
            'append new line
            sb.Append(vbCr & vbLf)
        Next
        Response.Output.Write(sb.ToString())
        Response.Flush()
        Response.End()
    End Sub

End Class
