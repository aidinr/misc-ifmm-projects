var jquerycssmenu={
fadesettings: {overduration: 200, outduration: 300}, //duration of fade in/ out animation, in milliseconds
 buildmenu:function(menuid){
	jQuery(document).ready(function(jmenu){
		var jmenumainmenu=jmenu("#"+menuid+">ul")
		var jmenuheaders=jmenumainmenu.find("ul").parent()
		jmenuheaders.each(function(i){
			var jmenucurobj=jmenu(this)
			var jmenusubul=jmenu(this).find('ul:eq(0)')
			this._dimensions={w:this.offsetWidth, h:this.offsetHeight, subulw:jmenusubul.outerWidth(), subulh:jmenusubul.outerHeight()}
			this.istopheader=jmenucurobj.parents("ul").length==1? true : false
			jmenusubul.css({top:this.istopheader? this._dimensions.h+"px" : 0});
			jmenucurobj.children("a:eq(0)").addClass("menuMore");
			jmenucurobj.hover(
				function(e){
					var jmenutargetul=jmenu(this).children("ul:eq(0)")
					this._offsets={left:jmenu(this).offset().left, top:jmenu(this).offset().top}
					var menuleft=this.istopheader? 0 : this._dimensions.w
					menuleft=(this._offsets.left+menuleft+this._dimensions.subulw>jmenu(window).width())? (this.istopheader? -this._dimensions.subulw+this._dimensions.w : -this._dimensions.w) : menuleft
					jmenutargetul.css({left:menuleft+"px"}).fadeIn(jquerycssmenu.fadesettings.overduration)
				},
				function(e){
					jmenu(this).children("ul:eq(0)").fadeOut(jquerycssmenu.fadesettings.outduration)
				}
			) //end hover
		}) //end jmenuheaders.each()
		jmenumainmenu.find("ul").css({display:'none', visibility:'visible'});
	}) ;
 }
}
