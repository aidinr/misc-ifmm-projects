$(document).ready(function () {

    $('#sliderBloc').append('<div id="slide_top_title"></div><p><a id="previous"></a><a id="next"></a></p>');
    $('#slider-list a:eq(0) img').attr('title', $('#slider-list a:eq(1) img').attr('alt'));
    $('#slide_top_title').html($('#slider-list a:eq(0) img').attr('alt'));
    $('#slider-list').css('width', (940 * Math.round($('#slider-list a').length) + 'px'));
    $('#next').click(function () {
        clearInterval(slide);
        slideNext();
    });
    $('#previous').click(function () {
        clearInterval(slide);
        $('#slider-list').prepend($('#slider-list a').last());
        $('#slider-list').css('marginLeft', '-940px');
        $('#slider-list a:eq(0) img').attr('title', $('#slider-list a:eq(0) img').attr('alt'));
        $('#slide_top_title').fadeOut(function () {}, function () {
            $('#slide_top_title').html($('#slider-list a:eq(0) img').attr('alt'));
            $(this).fadeIn();
        });
        $('#slider-list').animate({
            marginLeft: '0px'
        }, 500, function () {
            $('#slider-list').css('marginLeft', '0');
        });
    });

    $('#testimonials').append('<span id="prevNext"><a class="testLeft"></a><a class="testRight"></a></span>');

    $('.testRight').click(function () {
        $('.test_cont').first().animate({
            marginLeft: '-280px'
        }, 500, function () {
            $('.test_cont').first().css('marginLeft', '0');
            $('#test').append($('.test_cont').first());
        });
    });

    $('.testLeft').click(function () {
        $('#test').prepend($('.test_cont').last());
        $('.test_cont').first().css('marginLeft', '-280px');
        $('.test_cont').first().animate({
            marginLeft: '0'
        }, 500, function () {
            $('.test_cont').first().css('marginLeft', '0');

        });
    });

    var slide = setInterval('slideNext()', Math.round($('#slider-list').attr('class').replace('auto', '') * 1000));

    $('#slider-list').hover(

    function () {
        $(this).addClass('hover')
    }, function () {
        $(this).removeClass('hover')
    });

});

function slideNext() {

    if ($('#slider-list').hasClass('hover')) return false;

    $('#slider-list a:eq(1) img').attr('title', $('#slider-list a:eq(1) img').attr('alt'));
    $('#slide_top_title').fadeOut(function () {}, function () {
        $('#slide_top_title').html($('#slider-list a:eq(1) img').attr('alt'));
        $(this).fadeIn();
    });
    $('#slider-list').animate({
        marginLeft: '-940px'
    }, 500, function () {
        $('#slider-list').append($('#slider-list a').first());
        $('#slider-list').css('marginLeft', '0');
    });
}