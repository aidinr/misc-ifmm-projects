$(document).ready(function() {
    if ($('.accordion').html()) {
        $('html').css('overflow-y', 'scroll');
        $('.accordionButton').click(function() {
           if (IE) $('html').css('overflow-x', 'hidden');
            $('.accordionButton').removeClass('on');
            $('.accordionContent').slideUp('normal');
            if ($(this).next().is(':hidden') == true) {
                $(this).addClass('on');
                $(this).next().slideDown('normal');
            }
           if (IE) $('html').css('overflow-x', 'auto');
        });
        $('.accordionContent').hide();
        $(".accordionButton:first").addClass('on');
        $(".accordionContent:first").css('display', 'block');
    }
});
