var showPageIntval;
var IE = false /*@cc_on || @_jscript_version < 99 @*/;

if( location.href.indexOf("http") != 0 ) alert('For security reasons, this must be loaded from the web.');
else $.get("header.txt", null, function (data) {
	$('body').prepend(data);
	$.get("footer.txt", null, function (data) {
		$('body').append(data);
		showPageIntval = setInterval ('showPage ()', 20 );
		documentready();
	});
});

function showPage () {
 if( $('html').hasClass('wf-loading') ) return false;
 else {
  $('body').css('visibility','visible');
  clearInterval(showPageIntval);
 }
}

function documentready() {
 jquerycssmenu.buildmenu("menuTop");
 if( $('body').attr('id')=='home' ) $('.pageHome').addClass('selected');
 if( $('body').attr('id')=='about' ) $('.pageAbout').addClass('selected');
 if( $('body').attr('id')=='case' ) $('.pageCase').addClass('selected');
 if( $('body').attr('id')=='contact' ) $('.pageContact').addClass('selected');
 if( $('body').attr('id')=='news' ) $('.pageNews').addClass('selected');
 if( $('body').attr('id')=='solution' ) $('.pageSolution').addClass('selected');

 if($('#cForm').attr('id')=='cForm') {
 	$('#cForm_name').val('Your Name');
 	$('#cForm_email').val('Your Email');
 	$('#cForm_comments').val('');

	$('#cForm_name').focusin(function () {
		if( $('#cForm_name').val()=='Your Name')  $(this).val('');
	});
	$('#cForm_name').focusout(function () {
		if( $('#cForm_name').val()=='')  $(this).val('Your Name');
	});

	$('#cForm_email').focusin(function () {
		if( $('#cForm_email').val()=='Your Email')  $(this).val('');
	});
	$('#cForm_email').focusout(function () {
		if( $('#cForm_email').val()=='')  $(this).val('Your Email');
	});

 }


}
