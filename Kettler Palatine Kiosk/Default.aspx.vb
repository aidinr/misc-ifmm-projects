﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class _Default
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load




        If Request.Form.Keys.Count = 0 Then
            


            Dim WS As kettler_ws.Service = New kettler_ws.Service

            Dim ds As DataSet = New DataSet


            DS = WS.GetAllProjects()

            Application("DS") = ds
            PopulateHome(ds)
            PopulateLiveHere(ds)
            PopulateAmenities(ds)
            PopulateAbout(ds)
            PopulateGalleries(ds, "Exterior")
            PopulateContacts(ds)
        End If

        Dim webcontrol1 As Web.UI.Control

        webcontrol1 = Me.LoadControl(System.Configuration.ConfigurationManager.AppSettings("ascxFolder") & "Services.ascx")

        Dim webcontrol As Web.UI.Control
        webcontrol = Me.LoadControl(System.Configuration.ConfigurationManager.AppSettings("ascxFolder") & "Search.ascx")

        'damjan
        'PlaceholderServices.Controls.Add(webcontrol1)



        PlaceholderSearch.Controls.Add(webcontrol)
        

    End Sub
    Public Sub PopulateHome(ByVal theDataSet As DataSet)
        Dim iDAMPATHFull As String = Session("WSRetreiveAsset") & "size=0&id="
        Dim iDAMDownloadPathFull As String = ""
        
            For Each Row As DataRow In theDataSet.Tables("HomeAssets").Rows
                Select Case Row("name")
                Case "home_logo"
                    'damjan
                    'imgHomeLogo.ImageUrl = iDAMPATHFull & Row("asset_id")
                    'imgHomeLogo.AlternateText = Row("description")
                Case "home_image"
                    'damjan
                    'imgHomeImage.ImageUrl = iDAMPATHFull & Row("asset_id")
                    'imgHomeImage.AlternateText = Row("description")
            End Select
            Next

        'damjan    
        'imgHomeLogo.Visible = False

        

    End Sub
    Public Sub PopulateLiveHere(ByVal theDataSet As DataSet)
       
            Dim iDAMPATHFull As String = Session("WSRetreiveAsset") & "size=0&id="

        'Damjan
        'lblLiveHere.Text = theDataSet.Tables("LiveHereDescription").Rows(0)("descriptionmedium")
            For Each Row As DataRow In theDataSet.Tables("LiveHereAssets").Rows
                Select Case Row("name")
                Case "live_left"
                    'damjan
                    'imgLiveLeft.ImageUrl = iDAMPATHFull & Row("asset_id")
                    'imgLiveLeft.AlternateText = Row("description")
                    Case "live_right"
                        
                    Case "site_plan"
                    If (System.Configuration.ConfigurationManager.AppSettings("property") = "Allegro") Then
                        'damjan
                        'lblEnlarge.Visible = False
                    Else

                    End If

                End Select
            Next


        
            Dim featuredUnits() As DataRow
            Dim ds As DataSet
            ds = CType(Application("DS"), DataSet)

            featuredUnits = ds.Tables("Units").Select("Featured = 1")

        'damjan
        RepeaterFeaturedCarousel.DataSource = featuredUnits
        RepeaterFeaturedCarousel.DataBind()



    End Sub
    Public Sub PopulateAmenities(ByVal theDataSet As DataSet)

        Dim iDAMPATHFull As String = Session("WSRetreiveAsset") & "size=0&id="

        For Each Row As DataRow In theDataSet.Tables("AmenitiesAssets").Rows
            Select Case Row("name")
                Case "amenities_left"
                    'damjan
                    'imgAmenitiesLeft.ImageUrl = iDAMPATHFull & Row("asset_id")
                    'imgAmenitiesLeft.AlternateText = Row("description")
                Case "amenities_right"
                    'damjan
                    'imgAmenitiesRight.ImageUrl = iDAMPATHFull & Row("asset_id")
                    'imgAmenitiesRight.AlternateText = Row("description")
                Case "amenities_center"
                    'damjan
                    'imgAmenitiesCenter.ImageUrl = iDAMPATHFull & Row("asset_id")
                    'imgAmenitiesCenter.AlternateText = Row("description")
            End Select
        Next

        Dim AmenitiesArray() As String = theDataSet.Tables("AmenitiesDescription").Rows(0)("descriptionmedium").ToString.Split(vbCrLf)


        'damjan
        'repeaterAmenities.DataSource = AmenitiesArray
        'repeaterAmenities.DataBind()

    End Sub

    Public Sub PopulateAbout(ByVal theDataSet As DataSet)

        Dim iDAMPATHFull As String = Session("WSRetreiveAsset") & "size=0&id="

        For Each Row As DataRow In theDataSet.Tables("AboutKettlerAssets").Rows
            Select Case Row("name")
                Case "about_left"
                    'damjan
                    'imgAboutLeft.ImageUrl = iDAMPATHFull & Row("asset_id")
                    'imgAboutLeft.AlternateText = Row("description")
                Case "about_right"
                    'damjan
                    'imgAboutRight.ImageUrl = iDAMPATHFull & Row("asset_id")
                    'imgAboutRight.AlternateText = Row("description")
                Case "about_map"
                    'damjan
                    'lblAboutMap.Text = "<img onclick=""Milkbox.showThisImage('" & iDAMPATHFull & Row("asset_id") & "','" & Row("description") & "');"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "button_map.jpg"" alt=""View Site Map"" />"
                Case "about_logo"
                    'damjan
                    'imgLogo.ImageUrl = iDAMPATHFull & Row("asset_id")
                    'imgLogo.AlternateText = Row("description")
                Case "about_awards"
                    'damjan
                    'imgAwards.ImageUrl = iDAMPATHFull & Row("asset_id")
                    'imgAwards.AlternateText = Row("description")
            End Select
        Next

        'damjan
        'lblAboutKettler.Text = theDataSet.Tables("AboutKettlerDescription").Rows(0)("descriptionmedium")

    End Sub



    Public Sub PopulateGalleries(ByVal theDataSet As DataSet, ByVal strCategory As String)
        'Begin display galleries

        Dim iDAMPATHThumb As String = Replace(Session("WSRetreiveAsset") & "size=1&crop=1&width=179&height=179&qfactor=" & System.Configuration.ConfigurationManager.AppSettings("qfactor") & "&id=", "&", "&amp;")
        Dim iDAMPATHFull As String = Replace(Session("WSRetreiveAsset") & "size=1&crop=1&width=1106&height=572&qfactor=" & System.Configuration.ConfigurationManager.AppSettings("qfactor") & "&id=", "&", "&amp;")

       
        'End display galleries       
        rptGalleries.DataSource = theDataSet.Tables("Galleries").Select("Gallery_Category = '" & strCategory & "'")
        rptGalleries.DataBind()
    End Sub

    Public Sub PopulateContacts(ByVal theDataSet As DataSet)
        'Begin display galleries

        'End display galleries       
        RepeaterContacts.DataSource = theDataSet.Tables("Contacts")
        RepeaterContacts.DataBind()
    End Sub

    Public Sub CallBackReload_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallbackReload.Callback
        Session.Clear()
        Session.Abandon()
        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))

    End Sub

    Public Sub CallBackSearchCarousel_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBackSearchCarousel.Callback
        'searchCarouselRepeater.DataSource = getCarouselDataRows()
        'searchCarouselRepeater.DataBind()
        Dim drs As DataRow()
        drs = getCarouselDataRows()
        Dim dr As DataRow
        If Not drs Is Nothing Then
            dr = drs(Integer.Parse(e.Parameters(0)) - 1)
            imgCarouselItem.Attributes.Add("src", Session("WSRetreiveAsset") & "size=1&type=asset&width=65&height=75&crop=2&cache=1&id=" & dr.Item("Model_ASSETID"))
            lblCarouselCurrItem.Text = e.Parameter(0)
            lblCarouselCurrItem2.Text = e.Parameter(0)
            lblCarouselTotalItem.Text = drs.Length
            lblCarouselArea.Text = dr("Area") & " SQFT"
            lblCarouselBath.Text = dr("Bathroom") & " Bath"
            lblCarouselPrice.Text = dr("Price_String")
            lblCarouselType.Text = dr("Type").ToString.Replace("Bedroom", "Bed").Replace("Bedrooms", "Beds")
            lblCarouselType2.Text = dr("Type").ToString.Replace("Bedroom", "Bed").Replace("Bedrooms", "Beds")
            If e.Parameters(0) = 1 Then
                imgCarouselLeft.Attributes.Add("style", "display:none")
            Else
                imgCarouselLeft.Attributes.Add("onclick", "CallBackSearchCarousel.callback(""" & Integer.Parse(e.Parameters(0)) - 1 & """);")
            End If
            If e.Parameters(0) = drs.Length Then
                imgCarouselRight.Attributes.Add("style", "display:none")
            Else
                imgCarouselRight.Attributes.Add("onclick", "CallBackSearchCarousel.callback(""" & Integer.Parse(e.Parameters(0)) + 1 & """);")
            End If
            'myCart.Attributes.Add("onclick", "CallBackCartPopup.Callback('', '', '');")
        Else
            imgCarouselItem.Attributes.Add("src", "")
            lblCarouselCurrItem.Text = "0"
            lblCarouselCurrItem2.Text = "0"
            lblCarouselTotalItem.Text = "0"
            lblCarouselArea.Text = ""
            lblCarouselBath.Text = ""
            lblCarouselPrice.Text = ""
            lblCarouselType.Text = ""
            lblCarouselType2.Text = ""
            imgCarouselLeft.Attributes.Add("style", "display:none")
            imgCarouselRight.Attributes.Add("style", "display:none")

        End If


        PlaceholderSearchCarousel.RenderControl(e.Output)


    End Sub


    Public Sub deleteFromCarousel(ByVal assetID As String)
        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)

        Dim sessionArrayList = CType(Session("Carousel"), ArrayList)

        sessionArrayList.Remove(assetID)

        Session("Carousel") = sessionArrayList
    End Sub

    Public Sub CallBackCartPopup_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBackCartPopup.Callback
        Dim pageIndex As String = e.Parameters(0)

        If (pageIndex = "") Then
            pageIndex = "0"
        End If

        If (e.Parameters(1) = "delete") Then

            deleteFromCarousel(e.Parameters(2))


        End If

        Dim dr As DataRow() = getCarouselDataRows()

        Try
            If (dr.Length > 0) Then


                'RepeaterPrint.DataSource = dr
                'RepeaterPrint.DataBind()

                Dim pds As PagedDataSource = New PagedDataSource()
                pds.DataSource = dr
                pds.AllowPaging = True
                pds.PageSize = 6


                If (pageIndex > pds.PageCount - 1 And pageIndex <> 0) Then
                    pds.CurrentPageIndex = pageIndex - 1
                ElseIf (pageIndex <> 0) Then
                    pds.CurrentPageIndex = pageIndex
                Else
                    pds.CurrentPageIndex = 0
                End If


                divPaging.InnerText = pds.CurrentPageIndex + 1 & " of " & pds.PageCount

                If (pds.CurrentPageIndex = 0) Then
                    imagePrev.Attributes.Add("onclick", "CallBackCartPopup.Callback('" & pds.CurrentPageIndex & "', '', '')")
                    '    imagePrev.ImageUrl = "images/DelRay/cart_prev_off.jpg"
                    '    linkPrev.NavigateUrl = "#"
                Else
                    imagePrev.Attributes.Add("onclick", "CallBackCartPopup.Callback('" & pds.CurrentPageIndex - 1 & "', '', '')")
                    '    imagePrev.ImageUrl = "images/DelRay/cart_prev_on.jpg"
                    '    linkPrev.NavigateUrl = "CarouselCart.aspx?page=" & pds.CurrentPageIndex - 1
                End If

                If (pds.CurrentPageIndex = pds.PageCount - 1) Then
                    imageNext.Attributes.Add("onclick", "CallBackCartPopup.Callback('" & pds.CurrentPageIndex & "', '', '')")

                    '    imageNext.ImageUrl = "images/DelRay/cart_next_off.jpg"
                    '    linkNext.NavigateUrl = "#"
                Else
                    imageNext.Attributes.Add("onclick", "CallBackCartPopup.Callback('" & pds.CurrentPageIndex + 1 & "', '', '')")

                    '    imageNext.ImageUrl = "images/DelRay/cart_next_on.jpg"
                    '    linkNext.NavigateUrl = "CarouselCart.aspx?page=" & pds.CurrentPageIndex + 1
                End If

                repeaterCarousel.DataSource = pds
                repeaterCarousel.DataBind()
            End If
        Catch ex As Exception
            'imagePrev.Visible = False
            'imageNext.Visible = False
            'imagePrint.Visible = False
        End Try

        PlaceholderCartPopup.RenderControl(e.Output)
    End Sub


    Public Sub CallbackGalleries_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallbackGalleries.Callback
        PopulateGalleries(Application("DS"), e.Parameters(0))
        PlaceholderGalleries.RenderControl(e.Output)
    End Sub

    Public Function getCarouselDataRows() As DataRow()
        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)

        Dim assetID As String = ""
        Dim dr() As DataRow
        Dim sessionArrayList = CType(Session("Carousel"), ArrayList)
        If (sessionArrayList.Count > 0) Then
            For x = 0 To sessionArrayList.Count - 1
                assetID = assetID & sessionArrayList(x).ToString & ", "
            Next
            assetID = assetID.Substring(0, assetID.Length - 2)


            dr = ds.Tables("Units").Select("Asset_ID In (" & assetID & ")", "price")
        Else
            'literalSearchCarousel.Text = "<li><img src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "empty_carousel.jpg"" alt=""ADD UNIT"" /><p>ADD<br /><small>UNITS</small></p></li>"
            'literalDetailCarousel.Text = "<li><img src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "empty_carousel.jpg"" alt=""ADD UNIT"" /><p>ADD<br /><small>UNITS</small></p></li>"
        End If
        Return dr

    End Function

    Public Sub repeaterCarousel_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim literalFloor As Literal = e.Item.FindControl("literalFloor")
            Dim lastFloor As String = e.Item.DataItem("Floor").ToString.Substring(e.Item.DataItem("Floor").ToString.Length - 1, 1)
            literalFloor.Text = e.Item.DataItem("Floor")
            If (lastFloor = 1) Then
                literalFloor.Text = literalFloor.Text & "st"
            ElseIf (lastFloor = 2) Then
                literalFloor.Text = literalFloor.Text & "nd"
            ElseIf (lastFloor = 3) Then
                literalFloor.Text = literalFloor.Text & "rd"
            Else
                literalFloor.Text = literalFloor.Text & "th"
            End If

            Dim buttonDelete As HtmlImage = e.Item.FindControl("btnDelete")
            buttonDelete.Attributes.Add("onclick", "CallBackCartPopup.Callback('', 'delete', '" & e.Item.DataItem("Asset_Id").ToString & "');")



        End If
    End Sub
End Class