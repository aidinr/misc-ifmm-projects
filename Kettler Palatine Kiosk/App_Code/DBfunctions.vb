﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Data
Imports System.Data.SqlClient

Public Class mmfunctions

    Private Shared _searchSort As String
    Private Shared _searchSortby As String

    Private Shared Property searchSort(ByVal p1 As String) As String
        Get
            Return _searchSort
        End Get
        Set(ByVal value As String)
            _searchSort = value
        End Set
    End Property
    Private Shared Property searchSortby(ByVal p1 As String) As String
        Get
            Return _searchSortby
        End Get
        Set(ByVal value As String)
            _searchSortby = value
        End Set
    End Property

    Public Shared Function GetDataTableOLE(ByVal query As String) As DataTable
        Dim connection1 As OleDbConnection = New OleDbConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRINGOLE"))
        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)
        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()
        End Try
        GetDataTableOLE = table1
    End Function

    Public Shared Function GetDataTable(ByVal query As String) As DataTable
        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim sql As String = query
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("DTABLE")
        Try
            MyCommand1.Fill(DT1)
        Finally
            MyConnection.Close()
        End Try
        GetDataTable = DT1
    End Function


    Public Shared Function GetDataSet(ByVal query As String) As DataSet
        Dim connection1 As OleDbConnection = New OleDbConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRINGOLE"))
        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)
        Dim table1 As New DataSet
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()
        End Try
        GetDataSet = table1
    End Function


    Public Shared Function ResultsDataTable(ByVal _searchFilter As String, ByVal tid As String, ByVal sid As String, Optional ByVal _sort As String = Nothing) As DataTable

        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(MakeSQL(tid, sid, False, _sort), System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim userid As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & _searchFilter.Replace("'", "''") & "%"
        Dim DT1 As New DataTable("Projects")
        MyCommand1.Fill(DT1)
        Return DT1
    End Function

    Public Shared Function MakeSQL(ByVal tid As String, ByVal sid As String, Optional ByVal blnUseLimiter As Boolean = True, Optional ByVal _sort As String = Nothing) As String

        Dim sqllimiter As String = ""

        ' Dim sql As String = "select projectid,name,state_id,city,favorite from ipm_project where available = 'Y' and publish = 1 and search_tags like @keyword "

        'Dim sql As String = "select a.projectid,isnull(b.item_value,a.name) name,state_id,city,favorite, case when isnull(b.item_value,0) = '' then 0 else b.item_value end favorite2 from ipm_project a left outer join ipm_project_field_value b on a.projectid = b.projectid and b.item_id = 2400773 where a.available = 'Y' and publish = 1 and search_tags like @keyword "

        Dim sql As String = "select a.projectid,isnull(c.item_value,a.name) name,a.name shortname,state_id,city,favorite, case when isnull(b.item_value,0) = '' then 0 else b.item_value end favorite2 from ipm_project a left outer join ipm_project_field_value b on a.projectid = b.projectid and b.item_id = 2400773 left outer join ipm_project_field_value c on a.projectid = c.projectid and c.item_id = 2400075 where a.available = 'Y' and publish = 1 and search_tags like @keyword "


        If IsNumeric(tid) Then
            sql += " and a.projectid in (select projectid from ipm_project_office where officeid = " & tid & ") "
        End If
        If IsNumeric(sid) Then
            sql += " and a.projectid in (select projectid from ipm_project_discipline where keyid = " & sid & ") "
        End If
        'add limiter
        If blnUseLimiter Then
            Dim sqltemp As String = sql
            sql = "select top " & System.Configuration.ConfigurationManager.AppSettings("MaxResults") & " * from (" & sqltemp & ") a"
        End If
        'add ordering
        'sql += " order by name asc"

        'Add Sorting
        If _searchSort = "" Then
            _searchSort = "shortname"
        Else
            If _sort Is Nothing Then
                _searchSort = "shortname"
            Else
                _searchSort = _sort
            End If

        End If
        If _searchSortby = "" Then
            _searchSortby = "asc"
        Else
            If Not _sort Is Nothing Then
                If _searchSortby = "asc" Then
                    _searchSortby = "desc"
                Else
                    _searchSortby = "asc"
                End If
            End If

        End If
        If Not _sort Is Nothing Then
            sql += " order by " & _searchSort & " " & _searchSortby
        Else
            sql += " order by favorite2 desc," & _searchSort & " " & _searchSortby
        End If





        Return sql

    End Function



End Class
