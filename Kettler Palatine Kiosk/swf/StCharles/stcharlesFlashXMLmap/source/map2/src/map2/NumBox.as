﻿package map2 {
	import flash.display.*;
	import flash.text.*;
	
	import caurina.transitions.Tweener;
	import com.boontaran.Utility;
	
	public class NumBox extends Sprite 	{
		private var node:XML;
		public var id:int;
		public function NumBox(node:XML) 	{
			this.node = node;
			var pos:Array = node.xy.split(",");
			x = int(pos[0]);
			y = int(pos[1]);
			
			id = int(node.@id);
			
			hoverBg.x = normalBg.x = 1;
			hoverBg.y = normalBg.y = 1;
			
			
			
			Utility.setColor(normalBg, uint(Map2.xml.settings.key_background_color));
			Utility.setColor(hoverBg, uint(Map2.xml.settings.hover_color));
			
			hoverBg.alpha = 0;
			
						
			var fmt:TextFormat = new TextFormat();
			fmt.font = Map2.xml.settings.font;
			fmt.size = 11;
			fmt.color = uint(Map2.xml.settings.key_text_color);
			num_txt.text = node.@id;
			num_txt.setTextFormat(fmt);
			
			
			mouseChildren = false;
			buttonMode = true;
		}
		public function hover():void {
			Tweener.addTween(hoverBg, { alpha:1 , time:0.8 } );
		}
		public function normal():void {
			Tweener.addTween(hoverBg, { alpha:0 , time:0.8 } );
		}
	}

}