﻿Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Net
Imports System.IO

Partial Class FacebookFeed
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim webStream As Stream
        Dim webResponse = ""
        Dim req As HttpWebRequest
        Dim res As HttpWebResponse

        req = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings("FacebookFeed"))
        req.Method = "POST"
        ' req.Headers.Add("")
        req.UserAgent = "Mozilla/5.0"
        res = req.GetResponse()
        webStream = res.GetResponseStream()
        Dim webStreamReader As New StreamReader(webStream)

        While webStreamReader.Peek >= 0

            webResponse = webStreamReader.ReadToEnd()

        End While

        Response.Write(webResponse.ToString())

    End Sub


End Class
