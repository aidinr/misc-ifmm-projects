﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>
<% 
    Dim filepath As String = Server.MapPath("~") & "assets\cms\data\"
%>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagName="Search" TagPrefix="uc" Src="~/ascx/Gramercy/Search.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
     <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width">
        
        <meta name="viewport" content="initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width">
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <meta name="description" content="<% Response.Write(CMSFunctions.GetMetaDescription(filepath,"kiosk")) %>" />      
        <meta name="keywords" content="<% Response.Write(CMSFunctions.GetMetaKeywords(filepath,"kiosk")) %>" />
        <title><% Response.Write(CMSFunctions.GetMetaTitle(filepath,"kiosk")) %></title>
        <!-- CSS -->
        <link rel="stylesheet" type="text/css"  media="screen and (min-width: 1131px)"  href="css/kettler.css" />
        <link rel="stylesheet" type="text/css"  media="screen and (max-width: 1130px)"  href="css/tablet_wide.css" />
            
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'
        type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Sorts+Mill+Goudy' rel='stylesheet'
        type='text/css'>
        <link rel="stylesheet" type="text/css" href="skins/tango/skin.css" />
        <!--main gallery carousel-->
        <link rel="stylesheet" type="text/css" href="skins/ie7/skin.css" />
        <!--resident services carousel -->
        <link rel="stylesheet" type="text/css" href="skins/tango2/skin.css" />
        <!--live here carousel-->
        <link rel="stylesheet" type="text/css" href="skins/floorplate/skin.css"
        />
        <link rel="stylesheet" type="text/css" href="skins/team/skin.css"
        />
        <!--live here carousel-->
        <link rel="stylesheet" type="text/css" href="css/map.css"
        />
        <link rel="stylesheet" type="text/css" href="css/jqModal_tb.css"
        />
         <link rel="stylesheet" type="text/css" href="css/thickbox.css"
        />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="js/jquery.jcarousel.js"></script>
        <script type="text/javascript" src="js/jquery.jfeed.min.js"></script>
        <!--<script type="text/javascript" src="js/jqModal.js"></script>-->
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript" src="js/map.js"></script>
        <script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings("jsFolder")%>main.js"></script>
        <script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings("jsFolder")%>thickbox-compressed.js"></script>

    </head>
    
    <body class="cms">
        <div class="page ">
            <div class="menu_bar col">
                <div class="menu_container">
                    <div class="menu_logo">
                        <a href="javascript:scrollToHash('#home_page');"><img src="images/logo.png" alt="Kettler Logo" /></a>
                    </div>
                    
                    <div class="menu_item" id="menu_live_here_search_results">Live Here</div>
                    <div class="menu_item" id="menu_neighborhood">Neighborhood</div>
                    <div class="menu_item" id="menu_resident_services">Resident Services</div>
                    <div class="menu_item" id="menu_gallery" onclick="CallbackGalleries.Callback('Exterior');">Gallery</div>
                    <div class="menu_item" id="menu_about_kettler">About Kettler</div>

                    <div class="menu_video">
                        <a href="<% Response.Write(CMSFunctions.GetTextSimple(filepath, "Video_Link")) %>"><img src="<% Response.Write(CMSFunctions.Image(filepath, "Video_Thumnail")) %>" alt="View Kettler Video" class="cms cmsType_Image cmsWidth_174 cmsHeight_95 cmsName_Video_Thumnail" /></a>
                    </div>

                    <div class="cms cmsType_TextSingle cmsName_Video_Link"></div>

                    <div class="menu_favorite_cart"  onclick="CallBackCartPopup.Callback('','','');">
   						   <ComponentArt:CallBack id="CallBackSearchCarousel"  CacheContent="False" runat="server" >
                           <Content> 
                                     
                           <asp:PlaceHolder ID="PlaceholderSearchCarousel" runat="server">
                                <div class="menu_favorite_cart_header" >
                                    <div class="menu_favorite_cart_left">My Apartments</div>

                                    <div class="menu_favorite_cart_right"><asp:Label ID="lblCarouselCurrItem" runat="server"></asp:Label>
                                        <span>of</span> &nbsp; <asp:Label ID="lblCarouselTotalItem" runat="server"></asp:Label> </div>
                                    
                                    <div class="menu_favorite_cart_unit_container" id = "myCart" runat="server" onclick="javascript:tb_show('','#TB_inline?&inlineId=cart_dialog&amp;KeepThis=true&amp;height=668&amp;width=984',false);">    
                                    <div class="menu_favorite_cart_unit_container_title"><asp:Label ID="lblCarouselType" runat="server"></asp:Label> - <asp:Label ID="lblCarouselBath" runat="server"></asp:Label></div>
                                        <div class="menu_favorite_cart_unit_container_details_container">
                                            <div class="menu_favorite_cart_unit_container_details_container_image">
                                                <img style="width:65px" runat="server" id = "imgCarouselItem" alt="favorite unit" />
                                            </div>
                                            <div class="menu_favorite_cart_unit_container_details_detail">
                                                <b><asp:Label ID="lblCarouselPrice" runat="server"></asp:Label></b>
                                                <br /><asp:Label ID="lblCarouselType2" runat="server"></asp:Label>
                                                <br /><asp:Label ID="lblCarouselArea" runat="server"></asp:Label></div>
                                        </div>
                                    
                                    </div>
                                    <div class="menu_favorite_cart_button_container">
                                        <div class="menu_favorite_cart_button_container_left">
                                            <a href="#"><img src="images/arrow_left.png" runat="server" id="imgCarouselLeft" alt="left arrow" /></a>
                                        </div>
                                        <div class="menu_favorite_cart_button_container_center"><asp:Label ID="lblCarouselCurrItem2" runat="server"></asp:Label></div>
                                        <div class="menu_favorite_cart_button_container_right">
                                            <a href="#"><img src="images/arrow_right.png" runat="server" id="imgCarouselRight" alt="right arrow" /></a>
                                        </div>
                                    </div>
                                </div> 
                           		          
	                            
      					        <script type="text/javascript">
    
                                	            //$('#cart_dialog').jqm();
						        </script>
      

                        </asp:PlaceHolder>
                           
                           </Content>
                           <ClientEvents>
                            
                           </ClientEvents>
                           </ComponentArt:CallBack> 

                           
   						   
                          <div class="jqmWindow" id="cart_dialog">
                          <ComponentArt:CallBack id="CallBackCartPopup"  CacheContent="False" runat="server" >
                                  <Content> 
                                     
                                 <asp:PlaceHolder ID="PlaceholderCartPopup" runat="server">



                                  
					             <span class="content_title">
					             <b>The Grammercy</b> at Metropolitan Park
					             </span>
					             <!--<div class="unit_floorplan_details_button" onclick="CallBackSearchCarousel.callback(1);">Close</div>-->
                                 <div class="unit_floorplan_details_button" onclick="CallBackSearchCarousel.callback(1);self.parent.tb_remove();">Close</div>
                                

					             <div class="favorites_container">
                                  <asp:Repeater ID="repeaterCarousel" runat="server" OnItemDataBound="repeaterCarousel_ItemDataBound">
                                        <ItemTemplate>
                                            <div class='favorites_item_container<%# iif(((Container.ItemIndex + 1) Mod 3)=1, " left", "")%>'><div class="favorites_item_title">Unit <%#Container.DataItem("Unit")%></div><div class="button_delete"><img id="btnDelete" runat="server" src="images/button_delete.png" alt="delete this item" /></div><div class="favorites_item"><img src="<%=Session("WSRetreiveAsset")%>size=1&width=207&height=207&crop=1&id=<%#Container.DataItem("Model_ASSETID")%>" alt="small floorplan" /><div class="favorites_item_detail"><b><%#Container.DataItem("Price_String")%></b><br /><br />Unit <%#Container.DataItem("Unit")%><br /><%#Container.DataItem("Type")%><br /><%#Container.DataItem("Bathroom")%> bathrooms<br /><%#Container.DataItem("Area")%> SF<br /><asp:literal ID="literalFloor" runat="server"></asp:literal> Floor<br/><%#Container.DataItem("Available")%></div></div></div>
						                </ItemTemplate>
                                   </asp:Repeater>     
						            <div class="favorite_footer"><div class="favorite_footer_nav"><img id="imagePrev" runat="server" src="images/arrow_left.png" alt="left "/><div id="divPaging" runat="server" class="page_count">1 of 1</div><img src="images/arrow_right.png" id="imageNext" runat="server" alt="left "/></div><div class="unit_floorplan_details_button" onclick="window.print();">Print All</div></div>
					             </div>
                                       

                                 <script type="text/javascript">

                                     //$('#cart_dialog').jqm();
						        </script>
                          
                               
                                  </asp:PlaceHolder>
                           
                                   </Content>
                                   <ClientEvents>
                            
                                   </ClientEvents>
                               </ComponentArt:CallBack> 
					 </div> 
					  </div>
					      
                    <div class="menu_slogan">
                        <span class="yellow"></span>
                    </div>
                </div>
            </div>
            <div class="content col">
                <div class="content_page" id="home_page">
                    <span class="content_title">
                        <b>The Palatine</b> </span>
                </div>
                <div class="content_page" id="live_here" style="display:none">
                    <span class="content_title">
                        <b id="floorplate_selector_title">Select Floor Plan : 1st floor plan</b>
                    </span>
                    
                    <div class="unit_floorplan_details_button back_to_search_button small_button" >search all units</div>

                    <div class="placeholder">
                        <ul id="floorplate_selector" class="jcarousel-skin-floorplate">
                            <!--<li>
                                <img src="images/floorplates/main.png" alt="floor selector" />
                                <div class="find_button">Find My Apartment</div>
                                <div class="find_text">Start your search today or pick a floor to begin your search.</div>
                                <div
                                class="floor_title">Select a Floor</div>
                    <<div class="floor_selector" id="floor_9">
                        <div>Floor 9</div>
                </div>
                <div class="floor_selector" id="floor_7">
                    <div>Floors 7-8</div>
                </div>
                <div class="floor_selector" id="floor_6">
                    <div>Floor 6</div>
                </div>
                <div class="floor_selector" id="floor_5">
                    <div>Floor 5</div>
                </div>
                <div class="floor_selector" id="floor_4">
                    <div>Floor 4</div>
                </div>
                <div class="floor_selector" id="floor_3">
                    <div>Floor 3</div>
                </div>
                <div class="floor_selector" id="floor_2">
                    <div>Floor 2</div>
                </div>
                <div class="floor_selector" id="floor_1">
                    <div>Floor 1</div>
                </div>
                <div class="floor_selector" id="floor_10">
                    <div>Floor 10</div>
                </div>
                <div class="floor_selector" id="floor_11">
                    <div>Floors 11-13</div>
                </div>
                <div class="floor_selector" id="floor_14">
                    <div>Floor 14</div>
                </div>
                <div class="floor_selector 15" id="floor_15">
                    <div>Floors 15-16</div>
                </div>
                <div class="floor_selector 17" id="floor_17">
                    <div>Floor 17</div>
                </div>
                </li>-->
                <li>
                    <img src="images/floorplates/1.png" alt="1" />
                   <!-- <div class="floor_title">Select
                        <b>1st floor unit</b>
                    </div> -->
                    <div class="jcarousel-availability" id="unit_101"></div>
                    <div class="jcarousel-availability" id="unit_102"></div>
                    <div class="jcarousel-availability" id="unit_103"></div>
                    <div class="jcarousel-availability" id="unit_104"></div>
                    <div class="jcarousel-availability" id="unit_105"></div>
                    <div class="jcarousel-availability" id="unit_106"></div>
                    <div class="jcarousel-availability" id="unit_107"></div>
                    <div class="jcarousel-availability" id="unit_108"></div>
                    <div class="jcarousel-availability" id="unit_109"></div>
                    <div class="jcarousel-availability" id="unit_110"></div>
                    <div class="jcarousel-availability" id="unit_111"></div>
                    <div class="jcarousel-availability" id="unit_112"></div>
                    <div class="jcarousel-availability" id="unit_113"></div>
                    <div class="jcarousel-availability" id="unit_114"></div>
                    <div class="jcarousel-availability" id="unit_115"></div>
                    <div class="jcarousel-availability" id="unit_116"></div>
                    <div class="jcarousel-availability" id="unit_117"></div>
                    <div class="jcarousel-availability" id="unit_118"></div>
                    <div class="jcarousel-availability" id="unit_119"></div>
                    <div class="jcarousel-availability" id="unit_120"></div>
                    <div class="jcarousel-availability" id="unit_121"></div>
                    <div class="jcarousel-availability" id="unit_122"></div>
                    <div class="jcarousel-availability" id="unit_123"></div>
                    <div class="jcarousel-availability" id="unit_124"></div>
                    <div class="jcarousel-availability" id="unit_125"></div>
                </li>
                <li>
                    <img src="images/floorplates/2.png" alt="2" />
                    <!-- <div class="floor_title">Select
                        <b>2nd floor unit</b>
                    </div> -->
                    <div class="jcarousel-availability" id="unit_201"></div>
                    <div class="jcarousel-availability" id="unit_202"></div>
                    <div class="jcarousel-availability" id="unit_203"></div>
                    <div class="jcarousel-availability" id="unit_204"></div>
                    <div class="jcarousel-availability" id="unit_205"></div>
                    <div class="jcarousel-availability" id="unit_206"></div>
                    <div class="jcarousel-availability" id="unit_207"></div>
                    <div class="jcarousel-availability" id="unit_208"></div>
                    <div class="jcarousel-availability" id="unit_209"></div>
                    <div class="jcarousel-availability" id="unit_210"></div>
                    <div class="jcarousel-availability" id="unit_211"></div>
                    <div class="jcarousel-availability" id="unit_212"></div>
                    <div class="jcarousel-availability" id="unit_213"></div>
                    <div class="jcarousel-availability" id="unit_214"></div>
                    <div class="jcarousel-availability" id="unit_215"></div>
                    <div class="jcarousel-availability" id="unit_216"></div>
                    <div class="jcarousel-availability" id="unit_217"></div>
                    <div class="jcarousel-availability" id="unit_218"></div>
                    <div class="jcarousel-availability" id="unit_219"></div>
                    <div class="jcarousel-availability" id="unit_220"></div>
                    <div class="jcarousel-availability" id="unit_221"></div>
                    <div class="jcarousel-availability" id="unit_222"></div>
                    <div class="jcarousel-availability" id="unit_223"></div>
                    <div class="jcarousel-availability" id="unit_224"></div>
                    <div class="jcarousel-availability" id="unit_225"></div>
                    <div class="jcarousel-availability" id="unit_226"></div>
                    <div class="jcarousel-availability" id="unit_227"></div>
                    <div class="jcarousel-availability" id="unit_228"></div>
                    <div class="jcarousel-availability" id="unit_229"></div>
                    <div class="jcarousel-availability" id="unit_230"></div>
                    <div class="jcarousel-availability" id="unit_231"></div>
                    <div class="jcarousel-availability" id="unit_232"></div>
                    <div class="jcarousel-availability" id="unit_233"></div>
                    <div class="jcarousel-availability" id="unit_234"></div>
                    <div class="jcarousel-availability" id="unit_235"></div>
                    <div class="jcarousel-availability" id="unit_236"></div>
                    <div class="jcarousel-availability" id="unit_237"></div>
                    <div class="jcarousel-availability" id="unit_238"></div>
                    <div class="jcarousel-availability" id="unit_239"></div>
                    <div class="jcarousel-availability" id="unit_240"></div>
                </li>
                <li>
                    <img src="images/floorplates/3.png" alt="3" />
                    <!--<div class="floor_title">Select
                        <b>3rd floor unit</b>
                    </div>-->
                    <div class="jcarousel-availability" id="unit_302"></div>
                    <div class="jcarousel-availability" id="unit_303"></div>
                    <div class="jcarousel-availability" id="unit_304"></div>
                    <div class="jcarousel-availability" id="unit_305"></div>
                    <div class="jcarousel-availability" id="unit_306"></div>
                    <div class="jcarousel-availability" id="unit_307"></div>
                    <div class="jcarousel-availability" id="unit_308"></div>
                    <div class="jcarousel-availability" id="unit_309"></div>
                    <div class="jcarousel-availability" id="unit_310"></div>
                    <div class="jcarousel-availability" id="unit_311"></div>
                    <div class="jcarousel-availability" id="unit_312"></div>
                    <div class="jcarousel-availability" id="unit_313"></div>
                    <div class="jcarousel-availability" id="unit_314"></div>
                    <div class="jcarousel-availability" id="unit_315"></div>
                    <div class="jcarousel-availability" id="unit_316"></div>
                    <div class="jcarousel-availability" id="unit_317"></div>
                    <div class="jcarousel-availability" id="unit_318"></div>
                    <div class="jcarousel-availability" id="unit_319"></div>
                    <div class="jcarousel-availability" id="unit_320"></div>
                    <div class="jcarousel-availability" id="unit_321"></div>
                    <div class="jcarousel-availability" id="unit_323"></div>
                    <div class="jcarousel-availability" id="unit_324"></div>
                    <div class="jcarousel-availability" id="unit_325"></div>
                    <div class="jcarousel-availability" id="unit_326"></div>
                    <div class="jcarousel-availability" id="unit_327"></div>
                    <div class="jcarousel-availability" id="unit_328"></div>
                    <div class="jcarousel-availability" id="unit_329"></div>
                    <div class="jcarousel-availability" id="unit_330"></div>
                    <div class="jcarousel-availability" id="unit_331"></div>
                    <div class="jcarousel-availability" id="unit_332"></div>
                    <div class="jcarousel-availability" id="unit_333"></div>
                    <div class="jcarousel-availability" id="unit_334"></div>
                    <div class="jcarousel-availability" id="unit_335"></div>
                    <div class="jcarousel-availability" id="unit_336"></div>
                    <div class="jcarousel-availability" id="unit_337"></div>
                    <div class="jcarousel-availability" id="unit_338"></div>
                    <div class="jcarousel-availability" id="unit_339"></div>
                    <div class="jcarousel-availability" id="unit_340"></div>
                </li>
                <li>
                    <img src="images/floorplates/4.png" alt="4" />
                    <!--<div class="floor_title">Select
                        <b>4th floor unit</b>
                    </div>-->
                      <div class="jcarousel-availability" id="unit_402"></div>
                    <div class="jcarousel-availability" id="unit_403"></div>
                    <div class="jcarousel-availability" id="unit_404"></div>
                    <div class="jcarousel-availability" id="unit_405"></div>
                    <div class="jcarousel-availability" id="unit_406"></div>
                    <div class="jcarousel-availability" id="unit_407"></div>
                    <div class="jcarousel-availability" id="unit_408"></div>
                    <div class="jcarousel-availability" id="unit_409"></div>
                    <div class="jcarousel-availability" id="unit_410"></div>
                    <div class="jcarousel-availability" id="unit_411"></div>
                    <div class="jcarousel-availability" id="unit_412"></div>
                    <div class="jcarousel-availability" id="unit_413"></div>
                    <div class="jcarousel-availability" id="unit_414"></div>
                    <div class="jcarousel-availability" id="unit_415"></div>
                    <div class="jcarousel-availability" id="unit_416"></div>
                    <div class="jcarousel-availability" id="unit_417"></div>
                    <div class="jcarousel-availability" id="unit_418"></div>
                    <div class="jcarousel-availability" id="unit_419"></div>
                    <div class="jcarousel-availability" id="unit_420"></div>
                    <div class="jcarousel-availability" id="unit_421"></div>
                    <div class="jcarousel-availability" id="unit_423"></div>
                    <div class="jcarousel-availability" id="unit_424"></div>
                    <div class="jcarousel-availability" id="unit_425"></div>
                    <div class="jcarousel-availability" id="unit_426"></div>
                    <div class="jcarousel-availability" id="unit_427"></div>
                    <div class="jcarousel-availability" id="unit_428"></div>
                    <div class="jcarousel-availability" id="unit_429"></div>
                    <div class="jcarousel-availability" id="unit_430"></div>
                    <div class="jcarousel-availability" id="unit_431"></div>
                    <div class="jcarousel-availability" id="unit_432"></div>
                    <div class="jcarousel-availability" id="unit_433"></div>
                    <div class="jcarousel-availability" id="unit_434"></div>
                    <div class="jcarousel-availability" id="unit_435"></div>
                    <div class="jcarousel-availability" id="unit_436"></div>
                    <div class="jcarousel-availability" id="unit_437"></div>
                    <div class="jcarousel-availability" id="unit_438"></div>
                    <div class="jcarousel-availability" id="unit_439"></div>
                    <div class="jcarousel-availability" id="unit_440"></div>
                </li>
                <li>
                    <img src="images/floorplates/5.png" alt="5" />
                    <!--<div class="floor_title">Select
                        <b>5th floor unit</b>
                    </div>-->
                    <div class="jcarousel-availability" id="unit_502"></div>
                    <div class="jcarousel-availability" id="unit_503"></div>
                    <div class="jcarousel-availability" id="unit_504"></div>
                    <div class="jcarousel-availability" id="unit_505"></div>
                    <div class="jcarousel-availability" id="unit_506"></div>
                    <div class="jcarousel-availability" id="unit_507"></div>
                    <div class="jcarousel-availability" id="unit_508"></div>
                    <div class="jcarousel-availability" id="unit_509"></div>
                    <div class="jcarousel-availability" id="unit_510"></div>
                    <div class="jcarousel-availability" id="unit_511"></div>
                    <div class="jcarousel-availability" id="unit_512"></div>
                    <div class="jcarousel-availability" id="unit_513"></div>
                    <div class="jcarousel-availability" id="unit_514"></div>
                    <div class="jcarousel-availability" id="unit_515"></div>
                    <div class="jcarousel-availability" id="unit_516"></div>
                    <div class="jcarousel-availability" id="unit_517"></div>
                    <div class="jcarousel-availability" id="unit_518"></div>
                    <div class="jcarousel-availability" id="unit_519"></div>
                    <div class="jcarousel-availability" id="unit_520"></div>
                    <div class="jcarousel-availability" id="unit_521"></div>
                    <div class="jcarousel-availability" id="unit_523"></div>
                    <div class="jcarousel-availability" id="unit_524"></div>
                    <div class="jcarousel-availability" id="unit_525"></div>
                    <div class="jcarousel-availability" id="unit_526"></div>
                    <div class="jcarousel-availability" id="unit_527"></div>
                    <div class="jcarousel-availability" id="unit_528"></div>
                    <div class="jcarousel-availability" id="unit_529"></div>
                    <div class="jcarousel-availability" id="unit_530"></div>
                    <div class="jcarousel-availability" id="unit_531"></div>
                    <div class="jcarousel-availability" id="unit_532"></div>
                    <div class="jcarousel-availability" id="unit_533"></div>
                    <div class="jcarousel-availability" id="unit_534"></div>
                    <div class="jcarousel-availability" id="unit_535"></div>
                    <div class="jcarousel-availability" id="unit_536"></div>
                    <div class="jcarousel-availability" id="unit_537"></div>
                    <div class="jcarousel-availability" id="unit_538"></div>
                    <div class="jcarousel-availability" id="unit_539"></div>
                    <div class="jcarousel-availability" id="unit_540"></div>
                </li>
                <li>
                    <img src="images/floorplates/6.png" alt="6" />
                    <!--<div class="floor_title">Select
                        <b>6th floor unit</b>
                    </div>-->
                     <div class="jcarousel-availability" id="unit_602"></div>
                    <div class="jcarousel-availability" id="unit_603"></div>
                    <div class="jcarousel-availability" id="unit_604"></div>
                    <div class="jcarousel-availability" id="unit_605"></div>
                    <div class="jcarousel-availability" id="unit_606"></div>
                    <div class="jcarousel-availability" id="unit_607"></div>
                    <div class="jcarousel-availability" id="unit_608"></div>
                    <div class="jcarousel-availability" id="unit_609"></div>
                    <div class="jcarousel-availability" id="unit_610"></div>
                    <div class="jcarousel-availability" id="unit_611"></div>
                    <div class="jcarousel-availability" id="unit_612"></div>
                    <div class="jcarousel-availability" id="unit_613"></div>
                    <div class="jcarousel-availability" id="unit_614"></div>
                    <div class="jcarousel-availability" id="unit_615"></div>
                    <div class="jcarousel-availability" id="unit_616"></div>
                    <div class="jcarousel-availability" id="unit_617"></div>
                    <div class="jcarousel-availability" id="unit_618"></div>
                    <div class="jcarousel-availability" id="unit_619"></div>
                    <div class="jcarousel-availability" id="unit_620"></div>
                    <div class="jcarousel-availability" id="unit_621"></div>
                    <div class="jcarousel-availability" id="unit_623"></div>
                    <div class="jcarousel-availability" id="unit_624"></div>
                    <div class="jcarousel-availability" id="unit_625"></div>
                    <div class="jcarousel-availability" id="unit_626"></div>
                    <div class="jcarousel-availability" id="unit_627"></div>
                    <div class="jcarousel-availability" id="unit_628"></div>
                    <div class="jcarousel-availability" id="unit_629"></div>
                    <div class="jcarousel-availability" id="unit_630"></div>
                    <div class="jcarousel-availability" id="unit_631"></div>
                    <div class="jcarousel-availability" id="unit_632"></div>
                    <div class="jcarousel-availability" id="unit_633"></div>
                    <div class="jcarousel-availability" id="unit_635"></div>
                    <div class="jcarousel-availability" id="unit_636"></div>
                    <div class="jcarousel-availability" id="unit_639"></div>
                    <div class="jcarousel-availability" id="unit_640"></div>
                </li>
                <li>
                    <img src="images/floorplates/7-8.png" alt="7" />
                    <!--<div class="floor_title">Select
                        <b>7th floor unit</b>
                    </div>-->
                    <div class="jcarousel-availability" id="unit_702"></div>
                    <div class="jcarousel-availability" id="unit_703"></div>
                    <div class="jcarousel-availability" id="unit_704"></div>
                    <div class="jcarousel-availability" id="unit_705"></div>
                    <div class="jcarousel-availability" id="unit_706"></div>
                    <div class="jcarousel-availability" id="unit_707"></div>
                    <div class="jcarousel-availability" id="unit_708"></div>
                    <div class="jcarousel-availability" id="unit_709"></div>
                    <div class="jcarousel-availability" id="unit_710"></div>
                    <div class="jcarousel-availability" id="unit_711"></div>
                    <div class="jcarousel-availability" id="unit_712"></div>
                    <div class="jcarousel-availability" id="unit_713"></div>
                    <div class="jcarousel-availability" id="unit_714"></div>
                    <div class="jcarousel-availability" id="unit_715"></div>
                    <div class="jcarousel-availability" id="unit_716"></div>
                    <div class="jcarousel-availability" id="unit_717"></div>
                    <div class="jcarousel-availability" id="unit_718"></div>
                    <div class="jcarousel-availability" id="unit_719"></div>
                    <div class="jcarousel-availability" id="unit_720"></div>
                    <div class="jcarousel-availability" id="unit_721"></div>
                    <div class="jcarousel-availability" id="unit_723"></div>
                    <div class="jcarousel-availability" id="unit_724"></div>
                    <div class="jcarousel-availability" id="unit_725"></div>
                    <div class="jcarousel-availability" id="unit_726"></div>
                    <div class="jcarousel-availability" id="unit_727"></div>
                    <div class="jcarousel-availability" id="unit_728"></div>
                    <div class="jcarousel-availability" id="unit_729"></div>
                    <div class="jcarousel-availability" id="unit_730"></div>
                    <div class="jcarousel-availability" id="unit_731"></div>
                    <div class="jcarousel-availability" id="unit_732"></div>
                </li>
                <li>
                    <img src="images/floorplates/7-8.png" alt="8" />
                    <!--<div class="floor_title">Select
                        <b>8th floor unit</b>
                    </div>-->
                    <div class="jcarousel-availability" id="unit_802"></div>
                    <div class="jcarousel-availability" id="unit_803"></div>
                    <div class="jcarousel-availability" id="unit_804"></div>
                    <div class="jcarousel-availability" id="unit_805"></div>
                    <div class="jcarousel-availability" id="unit_806"></div>
                    <div class="jcarousel-availability" id="unit_807"></div>
                    <div class="jcarousel-availability" id="unit_808"></div>
                    <div class="jcarousel-availability" id="unit_809"></div>
                    <div class="jcarousel-availability" id="unit_810"></div>
                    <div class="jcarousel-availability" id="unit_811"></div>
                    <div class="jcarousel-availability" id="unit_812"></div>
                    <div class="jcarousel-availability" id="unit_813"></div>
                    <div class="jcarousel-availability" id="unit_814"></div>
                    <div class="jcarousel-availability" id="unit_815"></div>
                    <div class="jcarousel-availability" id="unit_816"></div>
                    <div class="jcarousel-availability" id="unit_817"></div>
                    <div class="jcarousel-availability" id="unit_818"></div>
                    <div class="jcarousel-availability" id="unit_819"></div>
                    <div class="jcarousel-availability" id="unit_820"></div>
                    <div class="jcarousel-availability" id="unit_821"></div>
                    <div class="jcarousel-availability" id="unit_823"></div>
                    <div class="jcarousel-availability" id="unit_824"></div>
                    <div class="jcarousel-availability" id="unit_825"></div>
                    <div class="jcarousel-availability" id="unit_826"></div>
                    <div class="jcarousel-availability" id="unit_827"></div>
                    <div class="jcarousel-availability" id="unit_828"></div>
                    <div class="jcarousel-availability" id="unit_829"></div>
                    <div class="jcarousel-availability" id="unit_830"></div>
                    <div class="jcarousel-availability" id="unit_831"></div>
                    <div class="jcarousel-availability" id="unit_832"></div>
                </li>
                <li>
                    <img src="images/floorplates/9.png" alt="9" />
                    <!--<div class="floor_title">Select
                        <b>9th floor unit</b>
                    </div>-->
                     <div class="jcarousel-availability" id="unit_902"></div>
                    <div class="jcarousel-availability" id="unit_903"></div>
                    <div class="jcarousel-availability" id="unit_906"></div>
                    <div class="jcarousel-availability" id="unit_907"></div>
                    <div class="jcarousel-availability" id="unit_909"></div>
                    <div class="jcarousel-availability" id="unit_910"></div>
                    <div class="jcarousel-availability" id="unit_911"></div>
                    <div class="jcarousel-availability" id="unit_912"></div>
                    <div class="jcarousel-availability" id="unit_913"></div>
                    <div class="jcarousel-availability" id="unit_914"></div>
                    <div class="jcarousel-availability" id="unit_915"></div>
                    <div class="jcarousel-availability" id="unit_916"></div>
                    <div class="jcarousel-availability" id="unit_917"></div>
                    <div class="jcarousel-availability" id="unit_918"></div>
                    <div class="jcarousel-availability" id="unit_919"></div>
                    <div class="jcarousel-availability" id="unit_920"></div>
                    <div class="jcarousel-availability" id="unit_921"></div>
                    <div class="jcarousel-availability" id="unit_923"></div>
                    <div class="jcarousel-availability" id="unit_924"></div>
                    <div class="jcarousel-availability" id="unit_927"></div>
                    <div class="jcarousel-availability" id="unit_928"></div>
                    <div class="jcarousel-availability" id="unit_929"></div>
                    <div class="jcarousel-availability" id="unit_930"></div>
                    <div class="jcarousel-availability" id="unit_931"></div>
                    <div class="jcarousel-availability" id="unit_932"></div>
                </li>
                <li>
                    <img src="images/floorplates/10.png" alt="10" />
                    <!--<div class="floor_title">Select
                        <b>10th floor unit</b>
                    </div>-->
                    <div class="jcarousel-availability" id="unit_1011"></div>
                    <div class="jcarousel-availability" id="unit_1012"></div>
                    <div class="jcarousel-availability" id="unit_1013"></div>
                    <div class="jcarousel-availability" id="unit_1014"></div>
                    <div class="jcarousel-availability" id="unit_1015"></div>
                    <div class="jcarousel-availability" id="unit_1016"></div>
                    <div class="jcarousel-availability" id="unit_1017"></div>
                    <div class="jcarousel-availability" id="unit_1018"></div>
                    <div class="jcarousel-availability" id="unit_1019"></div>
                    <div class="jcarousel-availability" id="unit_1020"></div>
                    <div class="jcarousel-availability" id="unit_1021"></div>
                    <div class="jcarousel-availability" id="unit_1031"></div>
                    <div class="jcarousel-availability" id="unit_1032"></div>
                </li>
                <li>
                    <img src="images/floorplates/11-13.png" alt="11" />
                    <!--<div class="floor_title">Select
                        <b>11th floor unit</b>
                    </div>-->
                    <div class="jcarousel-availability" id="unit_1111"></div>
                    <div class="jcarousel-availability" id="unit_1112"></div>
                    <div class="jcarousel-availability" id="unit_1113"></div>
                    <div class="jcarousel-availability" id="unit_1114"></div>
                    <div class="jcarousel-availability" id="unit_1115"></div>
                    <div class="jcarousel-availability" id="unit_1116"></div>
                    <div class="jcarousel-availability" id="unit_1117"></div>
                    <div class="jcarousel-availability" id="unit_1118"></div>
                    <div class="jcarousel-availability" id="unit_1119"></div>
                    <div class="jcarousel-availability" id="unit_1120"></div>
                    <div class="jcarousel-availability" id="unit_1121"></div>
                    <div class="jcarousel-availability" id="unit_1131"></div>
                    <div class="jcarousel-availability" id="unit_1132"></div>
                </li>
                <li>
                    <img src="images/floorplates/11-13.png" alt="12" />
                    <!--<div class="floor_title">Select
                        <b>12th floor unit</b>
                    </div>-->
                    <div class="jcarousel-availability" id="unit_1211"></div>
                    <div class="jcarousel-availability" id="unit_1212"></div>
                    <div class="jcarousel-availability" id="unit_1213"></div>
                    <div class="jcarousel-availability" id="unit_1214"></div>
                    <div class="jcarousel-availability" id="unit_1215"></div>
                    <div class="jcarousel-availability" id="unit_1216"></div>
                    <div class="jcarousel-availability" id="unit_1217"></div>
                    <div class="jcarousel-availability" id="unit_1218"></div>
                    <div class="jcarousel-availability" id="unit_1219"></div>
                    <div class="jcarousel-availability" id="unit_1220"></div>
                    <div class="jcarousel-availability" id="unit_1221"></div>
                    <div class="jcarousel-availability" id="unit_1231"></div>
                    <div class="jcarousel-availability" id="unit_1232"></div>
                </li>
                <li>
                    <img src="images/floorplates/11-13.png" alt="13" />
                    <!--<div class="floor_title">Select
                        <b>13th floor unit</b>
                    </div>-->
                    <div class="jcarousel-availability" id="unit_1311"></div>
                    <div class="jcarousel-availability" id="unit_1312"></div>
                    <div class="jcarousel-availability" id="unit_1313"></div>
                    <div class="jcarousel-availability" id="unit_1314"></div>
                    <div class="jcarousel-availability" id="unit_1315"></div>
                    <div class="jcarousel-availability" id="unit_1316"></div>
                    <div class="jcarousel-availability" id="unit_1317"></div>
                    <div class="jcarousel-availability" id="unit_1318"></div>
                    <div class="jcarousel-availability" id="unit_1319"></div>
                    <div class="jcarousel-availability" id="unit_1320"></div>
                    <div class="jcarousel-availability" id="unit_1321"></div>
                    <div class="jcarousel-availability" id="unit_1331"></div>
                    <div class="jcarousel-availability" id="unit_1332"></div>
                </li>
                <li>
                    <img src="images/floorplates/14.png" alt="14" />
                    <!--<div class="floor_title">Select
                        <b>14th floor unit</b>
                    </div>-->
                    <div class="jcarousel-availability" id="unit_1411"></div>
                    <div class="jcarousel-availability" id="unit_1412"></div>
                    <div class="jcarousel-availability" id="unit_1413"></div>
                    <div class="jcarousel-availability" id="unit_1414"></div>
                    <div class="jcarousel-availability" id="unit_1415"></div>
                    <div class="jcarousel-availability" id="unit_1416"></div>
                    <div class="jcarousel-availability" id="unit_1417"></div>
                    <div class="jcarousel-availability" id="unit_1418"></div>
                    <div class="jcarousel-availability" id="unit_1419"></div>
                    <div class="jcarousel-availability" id="unit_1420"></div>
                    <div class="jcarousel-availability" id="unit_1421"></div>
                    <div class="jcarousel-availability" id="unit_1431"></div>
                    <div class="jcarousel-availability" id="unit_1432"></div>
                </li>
                <li>
                    <img src="images/floorplates/15-16.png" alt="15" />
                    <!--<div class="floor_title">Select
                        <b>15th floor unit</b>
                    </div>-->
                    <div class="jcarousel-availability" id="unit_1511"></div>
                    <div class="jcarousel-availability" id="unit_1512"></div>
                    <div class="jcarousel-availability" id="unit_1513"></div>
                    <div class="jcarousel-availability" id="unit_1514"></div>
                    <div class="jcarousel-availability" id="unit_1515"></div>
                    <div class="jcarousel-availability" id="unit_1516"></div>
                    <div class="jcarousel-availability" id="unit_1517"></div>
                    <div class="jcarousel-availability" id="unit_1518"></div>
                    <div class="jcarousel-availability" id="unit_1519"></div>
                    <div class="jcarousel-availability" id="unit_1520"></div>
                    <div class="jcarousel-availability" id="unit_1521"></div>
                    <div class="jcarousel-availability" id="unit_1531"></div>
                    <div class="jcarousel-availability" id="unit_1532"></div>
                </li>
                <li>
                    <img src="images/floorplates/15-16.png" alt="16" />
                    <!--<div class="floor_title">Select
                        <b>16th floor unit</b>
                    </div>-->
                    <div class="jcarousel-availability" id="unit_1611"></div>
                    <div class="jcarousel-availability" id="unit_1612"></div>
                    <div class="jcarousel-availability" id="unit_1613"></div>
                    <div class="jcarousel-availability" id="unit_1614"></div>
                    <div class="jcarousel-availability" id="unit_1615"></div>
                    <div class="jcarousel-availability" id="unit_1616"></div>
                    <div class="jcarousel-availability" id="unit_1617"></div>
                    <div class="jcarousel-availability" id="unit_1618"></div>
                    <div class="jcarousel-availability" id="unit_1619"></div>
                    <div class="jcarousel-availability" id="unit_1620"></div>
                    <div class="jcarousel-availability" id="unit_1621"></div>
                    <div class="jcarousel-availability" id="unit_1631"></div>
                    <div class="jcarousel-availability" id="unit_1632"></div>
                </li>
                <li>
                    <img src="images/floorplates/17.png" alt="17" />
                    <!--<div class="floor_title">Select
                        <b>17th floor unit</b>
                    </div>-->
                     <div class="jcarousel-availability" id="unit_1711"></div>
                    <div class="jcarousel-availability" id="unit_1712"></div>
                    <div class="jcarousel-availability" id="unit_1713"></div>
                    <div class="jcarousel-availability" id="unit_1714"></div>
                    <div class="jcarousel-availability" id="unit_1715"></div>
                    <div class="jcarousel-availability" id="unit_1717"></div>
                    <div class="jcarousel-availability" id="unit_1720"></div>
                    <div class="jcarousel-availability" id="unit_1721"></div>
                    <div class="jcarousel-availability" id="unit_1731"></div>
                </li>
                </ul>
            </div>
            <div class="live_here_footer_container">
                <div class="live_here_footer_left_container">
                    <div class="live_here_footer_left">
                        <b>Everything is Met</b>
                        <br />
                        <br />
			<span class="spanText cms cmsType_TextMulti cmsName_Live_Here_Text">
<% Response.Write(CMSFunctions.GetTextFormat(filepath, "Live_Here_Text")) %>
                        </span>
			</div>
                    
                </div>
               
                <div class="live_here_footer_right_container">
                    <ul id="mycarousel3" class="jcarousel-skin-tango2">
                      <asp:Repeater ID="RepeaterFeaturedCarousel" runat="server">
                      <ItemTemplate>

                        <li>
                            <img src="images/live_here_image.jpg" onclick="loadApt('unit','<%#Container.DataItem("Unit")%>');scrollToHash('#unit_floorplan');" alt="live here image"  />
                            <div class="live_here_carousel_text_container" onclick="loadApt('unit','<%#Container.DataItem("Unit")%>');scrollToHash('#unit_floorplan');">
                                <div class="live_here_carousel_text">
                                    <span class="yellow">Featured</span>&nbsp;&nbsp; <%#Container.DataItem("Price_String")%> &nbsp;
                                    <span class="gray">/</span>&nbsp; <%#Container.DataItem("Type").ToString().Replace("Bedroom", "BR")%> &nbsp;
                                    <span class="gray">/</span>&nbsp; <%#Container.DataItem("Bathroom")%>BA</div>
                            </div>
                        </li>
                      </ItemTemplate>
                      </asp:Repeater>
                    </ul>
                </div>
            </div>
        </div>

                <!-- BEGIN SEARCH -->

                
                <asp:PlaceHolder ID="PlaceholderSearch" runat="server"></asp:PlaceHolder>
                <!--
                <div class="search_result" id="live_here_search_results">
                    <span class="content_title">
                        <b>Live Here</b>
                    </span>
                    <div class="search_result_header">
                        <div class="search_results_crumbs">RESULTS : &nbsp;
                            <span class="yellow">1 BED ROOM &nbsp; / &nbsp; LEVEL 5 &nbsp; / &nbsp; $1,500 - $1,800</span>
                        </div>
                        <div class="search_results_showing">SHOWING &nbsp; : &nbsp;
                            <span class="yellow">36 APARTMENTS</span>
                        </div>
                    </div>
                    <div class="search_result_container">
                    	<div class="search_result_scroll_bars">
                    		<a href="javascript:scrollToTop();"><img src="images/scroll_top.png" alt="scroll to the top" /></a>
                    		<a href="javascript:scrollUp();"><img src="images/scroll_up.png" alt="scroll up" /></a>
                    		<a href="javascript:scrollDown();"><img src="images/scroll_down.png" alt="scroll down" /></a>
                    		<a href="javascript:scrollToBottom();"><img src="images/scroll_bottom.png" alt="scroll to the bottom" /></a>
                    	</div>
                        <div class="search_result_sort">
                            <div class="search_menu_button_container">
                                <div class="search_menu_button" id="search_apartment_button">
                                    <div class="search_menu_text">Apartment</div>
                                    <div class="search_menu_sort_direction">
                                        <img src="images/arrow_up.png" alt="arrow" />
                                    </div>
                                </div>
                                <div id="search_apartment_dropdown" class="search_menu_dropdown">Show All Apt. Types
                                    <br />Studio
                                    <br />1 Bedroom
                                    <br />2 Bedroom
                                    <br />3 Bedroom
                                    <br />Penthouse</div>
                            </div>
                            <div class="search_menu_button_container">
                                <div class="search_menu_button_narrow" id="search_price_button">
                                    <div class="search_menu_text">Unit Price</div>
                                    <div class="search_menu_sort_direction">
                                        <img src="images/arrow_up.png" alt="arrow" />
                                    </div>
                                </div>
                                <div id="search_price_dropdown" class="search_menu_dropdown_narrow">Show All
                                    <br />&lt; $2000
                                    <br />$2000 - $2999
                                    <br />&gt; $3000</div>
                            </div>
                            <div class="search_menu_button_container">
                                <div class="search_menu_button_narrow" id="search_area_button">
                                    <div class="search_menu_text">Square Feet</div>
                                    <div class="search_menu_sort_direction">
                                        <img src="images/arrow_up.png" alt="arrow" />
                                    </div>
                                </div>
                                <div id="search_area_dropdown" class="search_menu_dropdown_narrow">Show All
                                    <br />&lt; 1000 sq ft
                                    <br />1000-1500 sq ft
                                    <br />&gt; 1500 sq ft</div>
                            </div>
                            <div class="search_menu_button_container">
                                <div class="search_menu_button_narrow" id="search_floor_button">
                                    <div class="search_menu_text">Floor Level</div>
                                    <div class="search_menu_sort_direction">
                                        <img src="images/arrow_up.png" alt="arrow" />
                                    </div>
                                </div>
                                <div id="search_floor_dropdown" class="search_menu_dropdown_narrow">Show All
                                    <br />1-5
                                    <br />6-10
                                    <br />11-15
                                    <br />Penthouse</div>
                            </div>
                            <div class="search_menu_button_container">
                                <div class="search_menu_button_narrow" id="search_available_button">
                                    <div class="search_menu_text">Available</div>
                                    <div class="search_menu_sort_direction">
                                        <img src="images/arrow_up.png" alt="arrow" />
                                    </div>
                                </div>
                                <div id="search_available_dropdown" class="search_menu_dropdown_narrow">Show All
                                    <br />Available Now
                                    <br />Available Soon
                                    <br />Occupied</div>
                            </div>
                            <div class="search_menu_apartment_text">My Apartments</div>
                        </div>
                        <div class="search_result_listing_container">
				<div class="search_result_row_container">
					<div class="search_result_row_result">
					 <div class="search_result_row_image"><img src="images/unit_image.jpg" alt="image unit" /></div>
					 <div class="search_result_row_type">1 Bedroom</div>
					 <div class="search_result_row_price">Price</div>
					 <div class="search_result_row_area">686 SQFT</div>
					 <div class="search_result_row_floor">5</div>
					 <div class="search_result_row_available">Vacant<br /><span>8/24/2012</span></div>
					</div>
				</div>
				<div class="search_result_row_result_add_button"><img src="images/add_to_carousel.png" alt="Add to Carousel" />
				</div>
				<div class="search_result_row_container">
					<div class="search_result_row_result">
					 <div class="search_result_row_image"><img src="images/unit_image.jpg" alt="image unit" /></div>
					 <div class="search_result_row_type">1 Bedroom</div>
					 <div class="search_result_row_price">Price</div>
					 <div class="search_result_row_area">686 SQFT</div>
					 <div class="search_result_row_floor">5</div>
					 <div class="search_result_row_available">Vacant<br /><span>8/24/2012</span></div>
					</div>
				</div>
				<div class="search_result_row_result_add_button"><img src="images/add_to_carousel.png" alt="Add to Carousel" />
				</div>
				<div class="search_result_row_container">
					<div class="search_result_row_result">
					 <div class="search_result_row_image"><img src="images/unit_image.jpg" alt="image unit" /></div>
					 <div class="search_result_row_type">1 Bedroom</div>
					 <div class="search_result_row_price">Price</div>
					 <div class="search_result_row_area">686 SQFT</div>
					 <div class="search_result_row_floor">5</div>
					 <div class="search_result_row_available">Vacant<br /><span>8/24/2012</span></div>
					</div>
				</div>
				<div class="search_result_row_result_add_button"><img src="images/add_to_carousel.png" alt="Add to Carousel" />
				</div>
				<div class="search_result_row_container">
					<div class="search_result_row_result">
					 <div class="search_result_row_image"><img src="images/unit_image.jpg" alt="image unit" /></div>
					 <div class="search_result_row_type">1 Bedroom</div>
					 <div class="search_result_row_price">Price</div>
					 <div class="search_result_row_area">686 SQFT</div>
					 <div class="search_result_row_floor">5</div>
					 <div class="search_result_row_available">Vacant<br /><span>8/24/2012</span></div>
					</div>
				</div>
				<div class="search_result_row_result_add_button"><img src="images/add_to_carousel.png" alt="Add to Carousel" />
				</div>
				<div class="search_result_row_container">
					<div class="search_result_row_result">
					 <div class="search_result_row_image"><img src="images/unit_image.jpg" alt="image unit" /></div>
					 <div class="search_result_row_type">1 Bedroom</div>
					 <div class="search_result_row_price">Price</div>
					 <div class="search_result_row_area">686 SQFT</div>
					 <div class="search_result_row_floor">5</div>
					 <div class="search_result_row_available">Vacant<br /><span>8/24/2012</span></div>
					</div>
				</div>
				<div class="search_result_row_result_add_button"><img src="images/add_to_carousel.png" alt="Add to Carousel" />
				</div>
				<div class="search_result_row_container">
					<div class="search_result_row_result">
					 <div class="search_result_row_image"><img src="images/unit_image.jpg" alt="image unit" /></div>
					 <div class="search_result_row_type">1 Bedroom</div>
					 <div class="search_result_row_price">Price</div>
					 <div class="search_result_row_area">686 SQFT</div>
					 <div class="search_result_row_floor">5</div>
					 <div class="search_result_row_available">Vacant<br /><span>8/24/2012</span></div>
					</div>
				</div>
				<div class="search_result_row_result_add_button"><img src="images/add_to_carousel.png" alt="Add to Carousel" />
				</div>
				<div class="search_result_row_container">
					<div class="search_result_row_result">
					 <div class="search_result_row_image"><img src="images/unit_image.jpg" alt="image unit" /></div>
					 <div class="search_result_row_type">1 Bedroom</div>
					 <div class="search_result_row_price">Price</div>
					 <div class="search_result_row_area">686 SQFT</div>
					 <div class="search_result_row_floor">5</div>
					 <div class="search_result_row_available">Vacant<br /><span>8/24/2012</span></div>
					</div>
				</div>
				<div class="search_result_row_result_add_button"><img src="images/add_to_carousel.png" alt="Add to Carousel" />
				</div>
				<div class="search_result_row_container">
					<div class="search_result_row_result">
					 <div class="search_result_row_image"><img src="images/unit_image.jpg" alt="image unit" /></div>
					 <div class="search_result_row_type">1 Bedroom</div>
					 <div class="search_result_row_price">Price</div>
					 <div class="search_result_row_area">686 SQFT</div>
					 <div class="search_result_row_floor">5</div>
					 <div class="search_result_row_available">Vacant<br /><span>8/24/2012</span></div>
					</div>
				</div>
				<div class="search_result_row_result_add_button"><img src="images/add_to_carousel.png" alt="Add to Carousel" />
				</div>
				<div class="search_result_row_container">
					<div class="search_result_row_result">
					 <div class="search_result_row_image"><img src="images/unit_image.jpg" alt="image unit" /></div>
					 <div class="search_result_row_type">1 Bedroom</div>
					 <div class="search_result_row_price">Price</div>
					 <div class="search_result_row_area">686 SQFT</div>
					 <div class="search_result_row_floor">5</div>
					 <div class="search_result_row_available">Vacant<br /><span>8/24/2012</span></div>
					</div>
				</div>
				<div class="search_result_row_result_add_button"><img src="images/add_to_carousel.png" alt="Add to Carousel" />
				</div>
				<div class="search_result_row_container">
					<div class="search_result_row_result">
					 <div class="search_result_row_image"><img src="images/unit_image.jpg" alt="image unit" /></div>
					 <div class="search_result_row_type">1 Bedroom</div>
					 <div class="search_result_row_price">Price</div>
					 <div class="search_result_row_area">686 SQFT</div>
					 <div class="search_result_row_floor">5</div>
					 <div class="search_result_row_available">Vacant<br /><span>8/24/2012</span></div>
					</div>
				</div>
				<div class="search_result_row_result_add_button"><img src="images/add_to_carousel.png" alt="Add to Carousel" />
				</div>
				<div class="search_result_row_container">
					<div class="search_result_row_result">
					 <div class="search_result_row_image"><img src="images/unit_image.jpg" alt="image unit" /></div>
					 <div class="search_result_row_type">1 Bedroom</div>
					 <div class="search_result_row_price">Price</div>
					 <div class="search_result_row_area">686 SQFT</div>
					 <div class="search_result_row_floor">5</div>
					 <div class="search_result_row_available">Vacant<br /><span>8/24/2012</span></div>
					</div>
				</div>
				<div class="search_result_row_result_add_button"><img src="images/add_to_carousel.png" alt="Add to Carousel" />
				</div>
				<div class="search_result_row_container">
					<div class="search_result_row_result">
					 <div class="search_result_row_image"><img src="images/unit_image.jpg" alt="image unit" /></div>
					 <div class="search_result_row_type">1 Bedroom</div>
					 <div class="search_result_row_price">Price</div>
					 <div class="search_result_row_area">686 SQFT</div>
					 <div class="search_result_row_floor">5</div>
					 <div class="search_result_row_available">Vacant<br /><span>8/24/2012</span></div>
					</div>
				</div>
				<div class="search_result_row_result_add_button"><img src="images/add_to_carousel.png" alt="Add to Carousel" />
				</div>
			</div>
			
                    </div>
                    </div>

                    -->
<!-- END SEARCH -->

        <div class="content_page" id="neighborhood">
            <span class="content_title">
                <b>Neighborhood</b>
            </span>
            <div class="content_container">
            <div id="map"></div>
            <div class="gallery_footer_container">
                    <div class="gallery_footer_double">
                        <div>
                            <b>Neighborhood</b>
                            <br />
                            <br />
			    <span class="spanText cms cmsType_TextMulti cmsName_Neighborhood_Text">
<% Response.Write(CMSFunctions.GetTextFormat(filepath, "Neighborhood_Text")) %>
			    </span>
			    </div>
                    </div>
                    <div class="gallery_footer_right" id="mapSelection">
                        <div class="gallery_button_header">
                            <b>Choose a Category</b>
                        </div>
                        <div id="mapConvenience" class="gallery_button gallery_button_odd gallery_button_active">Shopping</div>
                        <div id="mapDining" class="gallery_button">Dining</div>
                        <div id="mapServices" class="gallery_button gallery_button_odd">Active Life</div>
                        <div id="mapHistoric" class="gallery_button">Commute</div>
                    </div>
                </div>
                 
            </div>
            <!--
            <div id="mapSelection">
	    <a id="linkMapConvenience" href="#mapConvenience">Shopping</a>
	    <a id="linkMapDining" href="#mapDining">Dining</a>
	    <a id="linkMapServices" href="#mapServices">Active Life</a>
	    <a id="linkMapHistoric" href="#mapHistoric">Commute</a>
		</div>
		-->
        </div>
        <div class="content_page" id="resident_services">
            <span class="content_title">
                <b>Resident Services</b>
            </span>
            <div class="content_container_resident_services">
                <div class="resident_services_images_container">
                    <div class="resident_services_image" id="resident_services_image_1">
                        <img src="images/clear.gif" class="cms cmsType_Image cmsWidth_331 cmsHeight_407 cmsName_Resident_Services_Image_1" />
                        <img id="resident_services_image_button_1" src="images/arrow_right.png"
                        alt="open" />
                    </div>
                    <div class="resident_services_image" id="resident_services_image_2">
                        <img src="images/clear.gif" class="cms cmsType_Image cmsWidth_331 cmsHeight_407 cmsName_Resident_Services_Image_2" />
                        <img id="resident_services_image_button_2" src="images/arrow_right.png"
                        alt="open" />
                    </div>
                    <div class="resident_services_image" id="resident_services_image_3">
                        <img src="images/clear.gif" class="cms cmsType_Image cmsWidth_331 cmsHeight_407 cmsName_Resident_Services_Image_3" />
                        <img id="resident_services_image_button_3" src="images/arrow_right.png"
                        alt="open" />
                    </div>
                </div>
                <div class="resident_services_text_container">
                   <div id="resident_services_text_1" class="resident_services_text cms cmsType_TextMulti cmsName_Service_Highlights_1">
<% Response.Write(CMSFunctions.GetTextFormat(filepath, "Service_Highlights_1")) %>
                  </div>
                  <div id="resident_services_text_2" class="resident_services_text cms cmsType_TextMulti cmsName_Service_Highlights_2">
<% Response.Write(CMSFunctions.GetTextFormat(filepath, "Service_Highlights_2")) %>
                 </div>
                  <div id="resident_services_text_3" class="resident_services_text cms cmsType_TextMulti cmsName_Service_Highlights_3">
<% Response.Write(CMSFunctions.GetTextFormat(filepath, "Service_Highlights_3")) %>
                  </div>
                    </div>
                
            </div>
            <div class="gallery_footer_container">
                <div class="gallery_footer_left">
                    <div>
                        <b>Resident Services</b>
                        <br />
                        <br />
                        <span class="spanText cms cmsType_TextMulti cmsName_Resident_Services_Box">
<% Response.Write(CMSFunctions.GetTextFormat(filepath, "Resident_Services_Box")) %>
                        </span>
                        </div>
                </div>
                <div class="gallery_footer_center">
                    <ul id="mycarousel5" class="jcarousel-skin-team">
                        <asp:Repeater ID = "RepeaterContacts" runat="server">
                                    <ItemTemplate>
                                        <li><div><div class="team_title"><b>Meet Our Team</b></div><img src="<%=Session("WSRetreiveAsset") & "size=1&type=contact&crop=1&cache=1&width=150&height=200&qfactor=" & System.Configuration.ConfigurationManager.AppSettings("qfactor") & "&id="%><%#Container.DataItem("userid")%>" alt="portrait" /><div class="team_detail"><b><%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%></b><br /><span class="orange"><%#Container.DataItem("department")%></span></div></div></li>
                                    </ItemTemplate>
                         </asp:Repeater>
                      
                    </ul>
                </div>
                <div class="gallery_footer_right">
                    <ul id="mycarousel_2" class="jcarousel-skin-ie7">
                    <!--populated by jFeed from Facebook's RSS 2.0 feed-->
                    </ul>
                </div>
            </div>
        </div>
        <div class="content_page" id="gallery">
            <span class="content_title">
                <b>Gallery</b>
            </span>
            <div class="content_container">
            <ComponentArt:CallBack id="CallbackGalleries"  CacheContent="False" runat="server" >
                           <Content> 
                                     
                           <asp:PlaceHolder ID="PlaceholderGalleries" runat="server">
                            <ul id="mycarousel" class="jcarousel-skin-tango">
                
                               <asp:Repeater ID = "rptGalleries" runat="server">
                                <ItemTemplate>
                                    <li><img src='<%=Session("WSRetreiveAsset") & "size=1&crop=2&cropcolor=1d2123&cache=1&width=838&height=407&qfactor=" & System.Configuration.ConfigurationManager.AppSettings("qfactor") & "&id="%><%#Container.DataItem("Gallery_Image_ID")%>'  alt="" /></li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <script type="text/javascript">
                                        jQuery(document).ready(function () {
                                            jQuery('#mycarousel').jcarousel();

                                           
                                        });
                                    </script>
                                </FooterTemplate>
                               </asp:Repeater>
        
                            </ul>
                           </asp:PlaceHolder>
                           
                           </Content>
                           <ClientEvents>
                    </ClientEvents>
                </ComponentArt:CallBack> 


              </div>
                <div class="gallery_footer_container">
                    <div class="gallery_footer_double">
                        <div>
                            <b>Gallery</b>
                            <br />
                            <br />
			    <span class="spanText cms cmsType_TextMulti cmsName_Gallery_Text">
<% Response.Write(CMSFunctions.GetTextFormat(filepath, "Gallery_Text")) %>
			    </span>
			</div>
                        </div>
                        
                        <div class="gallery_footer_right" id="gallery_selection">
                        <div class="gallery_button_header">
                            <b>Choose a Gallery</b>
                        </div>
                        <div class="gallery_button gallery_button_odd" onclick="CallbackGalleries.Callback('Exterior');">Exterior</div>
                        <div class="gallery_button" onclick="CallbackGalleries.Callback('Amenities');">Amenities</div>
                        <div class="gallery_button gallery_button_odd" onclick="CallbackGalleries.Callback('Interior');">Interior</div>
                        <div class="gallery_button"  onclick="CallbackGalleries.Callback('Neighborhood');">Neighborhood</div>
                    </div>
                </div>
            
        </div>
        <div class="content_page" id="about_kettler">
            <span class="content_title">
                <b>About Kettler</b>
            </span>
            <div class="placeholder">
                <img src="images/about_kettler_image.png" alt="Kettler Map"
                />
            </div>
            <div class="gallery_footer_container">
                    <div class="gallery_footer_double">
                        <div class="about_us_blurb">
                            <b>About Kettler</b>
			    <span class="spanText cms cmsType_TextMulti cmsName_About_Kettler_Text">
<% Response.Write(CMSFunctions.GetTextFormat(filepath, "About_Kettler_Text")) %>
                            </span>
                            </div>
                    </div>
                    <div class="gallery_footer_right" id="gallery_selection">
                        <div class="gallery_button_header">
                            <b>Awards</b>
                            <br />
                            <img style="margin-top:15px;" src="images/award.png" alt="award" />
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
        </div>
       

        	<ComponentArt:CallBack ID="CallbackReload" runat="server">
	<ClientEvents >
	<CallbackComplete EventHandler="reloadKiosk" />
	</ClientEvents>
	</ComponentArt:CallBack>

    </body>

</html>
