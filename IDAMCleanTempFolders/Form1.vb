﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Net.Mail
Imports System.Net
Imports System
Imports System.IO
Imports System.Xml
Imports System.Xml.Serialization
Imports log4net

Public Class Form1
    Private Shared ReadOnly Log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        log4net.Config.XmlConfigurator.Configure()
        Dim ConfigXML As New XmlDocument
        Dim NodeList As XmlNodeList
        Try
            Log.Debug("Begin Execution")

            ConfigXML.Load("FolderConfig.xml")
            NodeList = ConfigXML.SelectNodes("/FolderList/Folder")
        Catch ex As Exception
            Log.Error("Error Init:" & ex.Message)
            Me.Close()
            Me.Dispose()
            Exit Sub
        End Try

        For Each n As XmlNode In NodeList
            Try
                Log.Debug("Begin Cleaning folder: " & n.Attributes("path").Value.ToString() & ", Number Days: " & n.Attributes("days_retention").Value.ToString())
                Dim di As DirectoryInfo = New DirectoryInfo(n.Attributes("path").Value.ToString())
                DeleteAllFilesAndFoldersFromRootFolder(di, Integer.Parse(n.Attributes("days_retention").Value.ToString()))
                Log.Debug("End Cleaning folder: " & n.Attributes("path").Value.ToString())
            Catch ex As Exception
                Log.Error("Error Processing:" & ex.Message)
            End Try
        Next

        Me.Close()
        Me.Dispose()

    End Sub


    Public Sub DeleteAllFilesAndFoldersFromRootFolder(ByVal RootFolder As DirectoryInfo, ByVal DeleteOlderThenXDays As Integer)
        Try
            DeleteFilesInThisFolder(RootFolder, DeleteOlderThenXDays)
            For Each SubFolder As DirectoryInfo In RootFolder.GetDirectories()
                DeleteAllFilesAndFoldersFromRootFolder(SubFolder, DeleteOlderThenXDays)
                If SubFolder.GetFiles.Length = 0 AndAlso SubFolder.GetDirectories.Length = 0 Then
                    If SubFolder.CreationTime < (DateAdd(DateInterval.Day, -DeleteOlderThenXDays, Today())) Then SubFolder.Delete()
                End If
            Next
        Catch ex As Exception
            Log.Error("Error Deleting from Above Folder: " & ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Deletes all Files in a Folder
    ''' </summary>
    ''' <param name="Folder">DirectoryInfo Object of Folder to delete files from.</param>
    ''' <param name="DeleteOlderThenXDays">Delete files that are older then this many number of days</param>
    ''' <remarks></remarks>
    Private Sub DeleteFilesInThisFolder(ByVal Folder As DirectoryInfo, ByVal DeleteOlderThenXDays As Integer)
        Try
            For Each ExistingFile As FileInfo In Folder.GetFiles()
                If ExistingFile.CreationTime < (DateAdd(DateInterval.Day, -DeleteOlderThenXDays, Today())) Then
                    Log.Debug("Begin Deleting File: " & ExistingFile.Name)
                    ExistingFile.Delete()
                    Log.Debug("Deleted File: " & ExistingFile.Name)
                End If
            Next
        Catch ex As Exception
            Log.Error("Error Deleting Above File: " & ex.Message)
        End Try
    End Sub
End Class
