﻿
<div id="map_canvas" class="LatLng39.0895_-77.52314_39.08933_-77.52339"></div>


<div id="map_cats">
    <div id="mapConvenience">
        <p id="LatLng39.1070_-77.5393">
            <img src="images/leesburgoutlet.jpg" alt="" />
            <span>
                <strong>Leesburg Outlet</strong>
                <br />
                <span>241 Fort Evans Road Northeast
                    <br />Leesburg, Virginia
                    <br />
                </span>
            </span>
        </p>
    </div>
    <div id="mapDining">
        <p id="LatLng39.1145_-77.5648">
            <img src="images/leesburg.jpg" alt="" />
            <span>
                <strong>Historic Downtown Leesburg</strong>
                <br />
                <span>
                    <br />
                    <br />
                </span>
            </span>
        </p>
	<p id="LatLng39.09053_-77.52575">
            <img src="images/pinkberry.jpg" alt="" />
            <span>
                <strong>Pinkberry</strong>
                <br />
                <span>1606 Village Market Blvd. SE
                    <br />703-771-3345
                </span>
            </span>
        </p>
	<p id="LatLng39.09053_-77.52575">
            <img src="images/finnegans.jpg" alt="" />
            <span>
                <strong>Finnegan's Sports Bar & Grill</strong>
                <br />
                <span>1608 Village Market Blvd. SE
                    <br />703-771-2333
                </span>
            </span>
        </p>
    </div>
   <div id="mapServices">
	<p id="LatLng39.09165_-77.52610">
            <img src="images/lafitness.gif" alt="" />
            <span>
                <strong>La Fitness</strong>
                <br />
                <span>1490 Classic Path Way, SE
                    <br />571-223-5694
                </span>
            </span>
        </p>
	<p id="LatLng39.09101_-77.52673">
            <img src="images/cobb.jpg" alt="" />
            <span>
                <strong>Cobb Village 12 Cinemas</strong>
                <br />
                <span>1602 Village Market Blvd, SE
                    <br />571-291-9462
                </span>
            </span>
        </p>
	<p id="LatLng39.09053_-77.52575">
            <img src="images/kingpinz.jpg" alt="" />
            <span>
                <strong>King Pinz</strong>
                <br />
                <span>1602 Village Market Blvd, SE
                    <br />703-443-8001
                </span>
            </span>
        </p>
    </div>
    <div id="mapHistoric">
        <p id="LatLng39.1105_-77.5618">
            <img src="images/leesburg.jpg" alt="" />
            <span>
                <strong>Downtown Leesburg</strong>
                <br />
                <span>
                <br />
                </span>
            </span>
        </p>
	<p id="LatLng39.08937_-77.51949">
            <img src="images/route7.jpg" alt="" />
            <span>
                <strong>Route 7</strong>
                <br />
                <span>
                    <br />
                    <br />
                </span>
            </span>
        </p>
	<p id="LatLng38.986_-77.462">
            <img src="images/dulles.jpg" alt="" />
            <span>
                <strong>Dulles International Airport</strong>
                <br />
                <span>
                    <br />
                </span>
            </span>
        </p>
	<p id="LatLng39.0693_-77.4828">
            <img src="images/greenway.jpg" alt="" />
            <span>
                <strong>Greenway Toll Road</strong>
                <br />
                <span>
                    <br />
                </span>
            </span>
        </p>
    </div>
</div>