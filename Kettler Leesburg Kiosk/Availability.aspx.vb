﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data

Partial Class Availability
    Inherits System.Web.UI.Page

    Protected Sub Availability_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.ContentType = "text/xml"
        Response.ContentEncoding = Encoding.UTF8

        Dim responseString As String = BuildXMLResponse(Request.QueryString("floor"))
        Response.Write(responseString)

    End Sub


    Public Function BuildXMLResponse(ByVal theFloor As String) As String
        Dim theString As String = "<Units>"

        Dim theUnits() As DataRow = getAptRows(theFloor)

        For Each dr As DataRow In theUnits
            theString = theString & "<Unit>"
            theString = theString & "<UnitNumber>" & dr("Unit") & "</UnitNumber>"
            theString = theString & "<Available>" & dr("Available") & "</Available>"
            theString = theString & "</Unit>"
        Next




        theString = theString & "</Units>"

        Return theString
    End Function

    Public Function getAptRows(ByVal theFloor As String) As DataRow()

        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)
        Dim dr() As DataRow

        dr = ds.Tables("Units").Select("floor = " & theFloor, "unit asc")

        Return dr

    End Function
End Class
