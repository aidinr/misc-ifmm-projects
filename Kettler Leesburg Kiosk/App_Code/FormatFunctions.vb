Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.VisualBasic

Public Class formatfunctions

    Public Shared Function AutoFormatText(ByVal input As String) As String

input = HttpUtility.HtmlEncode(input)


' Bullets
'input = RegularExpressions.Regex.Replace(input, "(^)- ", "&#8226; ")
'input = RegularExpressions.Regex.Replace(input, "(\n)- ", VbNewLine + "&#8226; ")

input = RegularExpressions.Regex.Replace(input, "^- (?=[A-Z])", "&#8226; ",RegexOptions.Multiline)

' Bold Heading
input = RegularExpressions.Regex.Replace(input, "(^|\n)(?=.{1,100}\n)([A-Z][A-Za-z]*(?:(?: [a-zA-Z0-9]+)* [A-Z][A-Za-z\?\!]*)?)(\r\n\r\n)", "$1<strong>$2</strong>$3")

' Links
input = RegularExpressions.Regex.Replace(input, "(https?://\S+[^\s@,.""']+)", "<a href=""$1"">$1</a>")
input = RegularExpressions.Regex.Replace(input, "(?<!\S)(\www\.\S+[^\s@,.""']+)", "<a href=""http://$1"">$1</a>")

' Email Addresses
input = RegularExpressions.Regex.Replace(input, "\w[\w\.]*\w?@[\w\.]+\w", "<a href=""mailto:$0"">$0</a>")

' Twitter
input = RegularExpressions.Regex.Replace(input, "(?<!\S)\((@)(\S+[^\s\)]+)\)", "(<a href=""http://twitter.com/$2"">$1$2</a>)")

' Introductory Statement
input = System.Text.RegularExpressions.Regex.Replace(input, "^([\w\d ,]*:\s)", "<strong>$1</strong>", System.Text.RegularExpressions.RegexOptions.Multiline)

' Spaces
input = RegularExpressions.Regex.Replace(input, "  ", " &nbsp;")

' Line Breaks
input = RegularExpressions.Regex.Replace(input, VbNewLine, VbNewLine +"<br />")

' Dash Symbol
input = RegularExpressions.Regex.Replace(input,"-- ", "&mdash; ")

' Copyright Symbol
input = RegularExpressions.Regex.Replace(input, "\([cC]\)", "&#169;")

' Trademark Symbol
input = RegularExpressions.Regex.Replace(input, "\([tT][mM]\)", "&#8482;")

' Registered Trademark Symbol
input = RegularExpressions.Regex.Replace(input, "\([rR]\)", "&#174;")

' Bullet Symbol
input = RegularExpressions.Regex.Replace(input, "\([\-]\)", "&#8226;")

'Special Bold Intro
'input = System.Text.RegularExpressions.Regex.Replace(input, "\(([^)]+)\)(?m:\r$|())", "<strong class=""introText"">$1</strong>")

'em span
input = System.Text.RegularExpressions.Regex.Replace(input, "\(([^)]+)\)(?m:\r$|())", "<em>($1)</em>")

input = RegularExpressions.Regex.Replace(input,"Ann Volz at 2", "<strong>Ann Volz</strong> at 2")

' Fix Double Entities
input = RegularExpressions.Regex.Replace(input, "(?<=&)amp;(?=#?\w+;)", String.Empty)

return input

    End Function

    Public Shared Function AutoFormatTextList(ByVal input As String) As String
        input = RegularExpressions.Regex.Replace(input, vbNewLine, vbNewLine + "")
        Return input
    End Function
End Class