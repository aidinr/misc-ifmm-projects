Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Web

Public Class CMSFunctions

    Public Shared Function Hello(theTest As String) As String

        Return "Hello " & theTest

    End Function

    Public Shared Function FormatSimple(theFile As String) As String
        Dim returnString As String = ""
        Try
            Using sr As New StreamReader(theFile)
                returnString = formatfunctionssingle.AutoFormatText(sr.ReadToEnd())
            End Using
        Catch ex As Exception
            returnString = ex.Message
        End Try

        Return returnString
    End Function

    Public Shared Function FormatList(theFile As String) As String
        Dim returnString As String = ""
        Try
            Using sr As New StreamReader(theFile)
                returnString = formatfunctionslist.AutoFormatText(sr.ReadToEnd())
            End Using
        Catch ex As Exception
            returnString = ex.Message
        End Try

        Return returnString
    End Function

    Public Shared Function Format(theFile As String) As String
        Dim returnString As String = ""
        Try
            Using sr As New StreamReader(theFile)
                returnString = formatfunctions.AutoFormatText(sr.ReadToEnd())
            End Using
        Catch ex As Exception
            returnString = ex.Message
        End Try

        Return returnString
    End Function

    Public Shared Function Image(thePath As String, theField As String) As String

        Dim returnString As String = ""
        For Each f As String In Directory.GetFiles(thePath & "Image\", "image_" & theField & "*")
            returnString = "/assets/cms/data/Image/" & Path.GetFileName(f) & "?i=" & DateTime.Now.Ticks.ToString.Trim
        Next

        Return returnString

    End Function

    Public Shared Function CheckTextFile(theFile As String, i As Integer) As Boolean
        Dim returnStatus As Boolean = True
        If Not File.Exists(theFile & "_xa" & i.ToString.Trim & ".txt") Then
            returnStatus = False
        End If
        If Not File.Exists(theFile & "_xb" & i.ToString.Trim & ".txt") Then
            returnStatus = False
        End If

        Return returnStatus
    End Function

    Public Shared Function GetMetaTitle(thePath As String, thePage As String) As String

        Return CMSFunctions.FormatSimple(thePath & "Metadata\metadataTitle_" & thePage & ".txt")

    End Function

    Public Shared Function GetMetaDescription(thePath As String, thePage As String) As String

        Return CMSFunctions.FormatSimple(thePath & "Metadata\metadataDescription_" & thePage & ".txt")

    End Function

    Public Shared Function GetMetaKeywords(thePath As String, thePage As String) As String

        Return CMSFunctions.FormatSimple(thePath & "Metadata\metadataKeywords_" & thePage & ".txt")

    End Function

    Public Shared Function GetTextSimple(thePath As String, theField As String) As String

        Return CMSFunctions.FormatSimple(thePath & "Text\Text_" & theField & ".txt")

    End Function

    Public Shared Function GetTextFormat(thePath As String, theField As String) As String

        Return CMSFunctions.Format(thePath & "Text\Text_" & theField & ".txt")

    End Function
    Public Shared Function GetTextFormatBulletList(thePath As String, theField As String) As String
        Return CMSFunctions.FormatBulletList(thePath & "Text\Text_" & theField & ".txt")
    End Function


    Public Shared Function FormatBulletList(theFile As String) As String
        Dim returnString As String = ""
        Try
            Using sr As New StreamReader(theFile)
                returnString = formatfunctions.AutoFormatTextList(sr.ReadToEnd())
            End Using
        Catch ex As Exception
            returnString = ex.Message
        End Try

        Return returnString
    End Function

End Class
