Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.VisualBasic

Public Class formatfunctionsurl

    Public Shared Function AutoFormatText(ByVal input As String) As String
        input = HttpUtility.HtmlEncode(input)
        input = RegularExpressions.Regex.Replace(input, "  ", " &nbsp;")
        input = RegularExpressions.Regex.Replace(input, VbNewLine, VbNewLine + "<br />")
        input = RegularExpressions.Regex.Replace(input, "-- ", "&mdash; ")
        input = RegularExpressions.Regex.Replace(input, "(?<=&)amp;(?=#?\w+;)", String.Empty)
        Return input
    End Function

End Class