﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Search.ascx.vb" Inherits="ascx_Gramercy_Search" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<script type="text/javascript">
/*
    jQuery(document).ready(function() {
        jQuery('#mycarousel').jcarousel({
            vertical: true,
            scroll: 2
        });
        jQuery('#mycarousel3').jcarousel({
            scroll: 3
        });
    });
*/
    function fireDetailsCarousel() {
        jQuery('#mycarousel2').jcarousel({
            vertical: true,
            scroll: 5
        });

    };
  /*  function fireSearchCarousel() {
        jQuery('#mycarousel').jcarousel({
            vertical: true,
            scroll: 2
        });
    };
    */
    var headerloaded = 0;
</script>
		
		         <div class="search_result" id="live_here_search_results">
                    <span class="content_title">
                        <b>Live Here</b>
                    </span>
                    <div class="search_result_header">
                        <div class="search_results_crumbs" style="width:470px">RESULTS : &nbsp;<span  class="yellow" style="width:450px" ID = "lblSearchCriteria"></span>
                        </div>

                        <ComponentArt:CallBack ID="CallBackSearchHeader"  runat="server">
							     <Content>
                                    <asp:PlaceHolder ID="PlaceHolderSearchHeader" runat="server">
                                     
                                    <div class="search_results_showing">SHOWING &nbsp; : &nbsp;
                                        

                                    <asp:Label CssClass="yellow" ID="lblSearchResults" runat="server"></asp:Label><span class="yellow">&nbsp;APARTMENTS</span>
                                    </div>
                                </asp:PlaceHolder>
								</Content>
						</ComponentArt:CallBack>
                    </div>
                    <div class="search_result_container">
                    	<div class="search_result_scroll_bars">
                    		<a href="javascript:scrollToTop();"><img src="images/scroll_top.png" alt="scroll to the top" /></a>
                    		<a href="javascript:scrollUp();"><img src="images/scroll_up.png" alt="scroll up" /></a>
                    		<a href="javascript:scrollDown();"><img src="images/scroll_down.png" alt="scroll down" /></a>
                    		<a href="javascript:scrollToBottom();"><img src="images/scroll_bottom.png" alt="scroll to the bottom" /></a>
                    	</div>
                        <div class="search_result_scroll_bars_short">
                            <a href="javascript:scrollToTop();"><img src="images/scroll_top.png" alt="scroll to the top" /></a>
                            <a href="javascript:scrollUp();"><img src="images/scroll_up_s.png" alt="scroll up" /></a>
                            <a href="javascript:scrollDown();"><img src="images/scroll_down_s.png" alt="scroll down" /></a>
                            <a href="javascript:scrollToBottom();"><img src="images/scroll_bottom.png" alt="scroll to the bottom" /></a>
                        </div>
                        <div class="search_result_sort">
                            <div class="search_menu_button_container">
                                <div class="search_menu_button" id="search_apartment_button">
                                    <div class="search_menu_text">Apartment</div>
                                    <div class="search_menu_sort_direction">
                                        <img src="images/arrow_up.png" alt="arrow" />
                                    </div>
                                </div>
                                <div id="search_apartment_dropdown" class="search_menu_dropdown">
                                <div onclick="javascript:typeResultsDisplay=''; doFilter('type','%');">Show All Apt. Types</div>
                                    <asp:Repeater id="filter_type" runat="server">
						                    <ItemTemplate>
						                    <div onclick="javascript: typeResultsDisplay=$(this).text(); doFilter('type','<%#Container.DataItem("Type")%>');"><%#Container.DataItem("Type")%></div>
						                    </ItemTemplate>
						                    </asp:Repeater>
                                </div>
                            </div>
                            <div class="search_menu_button_container">
                                <div class="search_menu_button_narrow" id="search_price_button">
                                    <div class="search_menu_text">Unit Price</div>
                                    <div class="search_menu_sort_direction">
                                        <img src="images/arrow_up.png" alt="arrow" />
                                    </div>
                                </div>
                                <div id="search_price_dropdown" class="search_menu_dropdown_narrow">
                                    <div onclick="javascript:priceResultsDisplay=''; doFilter('price','%');">Show All</div>
                                    <div id="lblFilterPrice1" runat="server"></div>
                                    <div id="lblFilterPrice2" runat="server"></div>
                                    <div id="lblFilterPrice3" runat="server"></div>
                                    <div id="lblFilterPrice4" runat="server"></div>
                                </div>
                            </div>
                            <div class="search_menu_button_container">
                                <div class="search_menu_button_narrow" id="search_area_button">
                                    <div class="search_menu_text">Square Feet</div>
                                    <div class="search_menu_sort_direction">
                                        <img src="images/arrow_up.png" alt="arrow" />
                                    </div>
                                </div>
                                <div id="search_area_dropdown" class="search_menu_dropdown_narrow">
                                    <div onclick="javascript:areaResultsDisplay=''; doFilter('area', '');">Show All</div>
                                    <div onclick="javascript:areaResultsDisplay=$(this).text(); doFilter('area', '0');">0 - 1000 sq ft</div>
                                    <div onclick="javascript:areaResultsDisplay=$(this).text(); doFilter('area', '1001');">1001 - 1500 sq ft</div>
                                    <div onclick="javascript:areaResultsDisplay=$(this).text(); doFilter('area', '1501');">1501+ sq ft</div>
                                    
                                    </div>
                            </div>
                            <div class="search_menu_button_container">
                                <div class="search_menu_button_narrow" id="search_floor_button">
                                    <div class="search_menu_text">Floor Level</div>
                                    <div class="search_menu_sort_direction">
                                        <img src="images/arrow_up.png" alt="arrow" />
                                    </div>
                                </div>
                                <div id="search_floor_dropdown" class="search_menu_dropdown_narrow">
                                    <div onclick="javascript:floorResultsDisplay= ''; doFilter('floor','all');">Show All</div>
                                    <div id="lblFilterFloor1_5" runat="server"></div>
                                    <div id="lblFilterFloor6_10" runat="server"></div>
                                    <div id="lblFilterFloor11_15" runat="server"></div>
                                    <div id="lblFilterFloor16_20" runat="server"></div>
                                    <div id="lblFilterFloor21_25" runat="server"></div>
                                    <div id="lblFilterFloor26_30" runat="server"></div>
                                    <div id="lblFilterFloor31_35" runat="server"></div>
                                    <div id="lblFilterFloor36_40" runat="server"></div>
                                    <div id="lblFilterFloor41_45" runat="server"></div>
                                    <div id="lblFilterFloor46_50" runat="server"></div>
                                </div>
                            </div>
                            <div class="search_menu_button_container">
                                <div class="search_menu_button_narrow" id="search_available_button">
                                    <div class="search_menu_text">Available</div>
                                    <div class="search_menu_sort_direction">
                                        <img src="images/arrow_up.png" alt="arrow" />
                                    </div>
                                </div>
                                <div id="search_available_dropdown" class="search_menu_dropdown_narrow">
                                    <div onclick="javascript:availableResultsDisplay=''; doFilter('available', '%');">Show All</div>
                                    <div onclick="javascript:availableResultsDisplay=$(this).text(); doFilter('available', 'Available Now');">Available Now</div>
                                    <div onclick="javascript:availableResultsDisplay=$(this).text(); doFilter('available', 'Available Soon');">Available Soon</div>
                                    <div onclick="javascript:availableResultsDisplay=$(this).text(); doFilter('available', 'Occupied');">Occupied</div>

                                </div>
                            </div>
                            <div class="search_menu_apartment_text">My Apartments</div>
                        </div>
                        <div class="search_result_listing_container">
        <ComponentArt:CallBack id="CallBack2" CacheContent="false" LoadingPanelFadeMaximumOpacity="20"  CssClass="CallBack" runat="server">
        <Content>
          <asp:PlaceHolder id="PlaceHolder2" runat="server">			
						<asp:Repeater id="search_results_rows" runat="server" OnItemDataBound="search_results_rows_ItemDataBound">
						<ItemTemplate>
				<div class="search_result_row_container" id="<%#Container.ItemIndex%>">
					<div class="search_result_row_result" onclick="loadApt('search','<%#Container.ItemIndex%>');scrollToHash('#unit_floorplan');">
					 <div class="search_result_row_image" ><img src="<%=Session("WSRetreiveAsset")%>size=1&type=asset&width=65&height=75&crop=2&cache=1&id=<%#Container.DataItem("Model_ASSETID")%>" alt="image unit" /></div>
					 <div class="search_result_row_type" ><%#Container.DataItem("Type")%></div>
					 <div class="search_result_row_price" ><%#Container.DataItem("Price_String")%></div>
					 <div class="search_result_row_area" ><%#Container.DataItem("Area")%></div>
					 <div class="search_result_row_floor" ><%#Container.DataItem("Floor")%></div>
					 <div class="search_result_row_available" ><%#Container.DataItem("Available")%><br /><span><%#Container.DataItem("Availability_date")%></span></div>
					</div>
				    <div class="search_result_row_result_add_button" onclick="addToCarousel(this.title);CallBackCartPopup.Callback('', '', '');" title="<%#Container.DataItem("Asset_ID")%>"><asp:Literal runat="server" ID = "LiteralSearchRowOptions" ></asp:Literal></div>
				</div>
                </ItemTemplate>
                <FooterTemplate>
						<script type="text/javascript">
						    //alert('hi');
						    //ctl00_CallBackSearchHeader.callback($(document.body).getElements('.search_row').length);
						    //ctl00_CallBackSearchHeader.callback();

						    //CallBackSearchHeader.callback($(document.body).getElements('.search_result_row_container').length);
						        CallBackSearchHeader.callback($('div.search_result_row_container').length);
						</script>
				</FooterTemplate>
                </asp:Repeater>
                <asp:Label ID="lblSearchResultsMore" runat="server"></asp:Label>
            </asp:PlaceHolder>
        </Content>
        <LoadingPanelClientTemplate >
		<table width="1024" height=100%>
		<tr><td align=center >
		<br /><Br /><br /><br /><br />Loading
		</td></tr>
		</table>
	
		
		</LoadingPanelClientTemplate>
		<ClientEvents>
		<CallbackComplete EventHandler="reFade" />
		</ClientEvents>
		</ComponentArt:CallBack>

			</div>
			
                    </div>
                    </div>
		<div class="content_page" id="unit_floorplan">
                <!-- <div id="main_details"> -->
				<ComponentArt:CallBack id="CallBack1"  CacheContent="False"  CssClass="CallBack" runat="server" >
                   <Content>
                       <asp:PlaceHolder id="PlaceHolder1" runat="server">
                       
        
            <span class="content_title">
                Apartment Floor Plan : &nbsp; <b><asp:Label ID="lblAptType" runat="server"></asp:Label></b>
            </span>
            <div class="unit_floorplan_details_button back_to_search_button small_button">back to search</div>
            <div class="unit_floorplan_carousel_container">
             <div id = "btnAptPrev" runat="server" class="unit_floorplan_carousel_left_button">
                
            </div>
            <div id = "btnAptNext" runat="server" class="unit_floorplan_carousel_right_button">
               
            </div>
                <div class="unit_floorplan_image">
                    <asp:Label ID="lblDetailGalleryMainImage1" runat="server"  alt="unit floorplan" ></asp:Label>
                    <asp:Label ID="lblDetailGalleryMainImage2" runat="server"  alt="unit floorplan"></asp:Label> 

                </div>
                <div class="unit_floorplan_details_container">
                    
                    <div class="unit_floorplan_details_title">Apartment Details</div>
                    <div class="unit_floorplan_details_row">
                        <table border="0">
                    	<tr><td>
                        <div class="unit_floorplan_details_subtitle">Apt Number</div>
                        </td><td>
                        <div class="unit_floorplan_details_detail"><asp:Label ID="lblAptNumber2" runat="server"></asp:Label></div>
                        </td>
                        </tr>
                        </table>
                    </div>
                    <div class="unit_floorplan_details_row">
                        <table border="0">
                        <tr><td>
                        <div class="unit_floorplan_details_subtitle">Floor Plan Type</div>
                        </td><td>
                        <div class="unit_floorplan_details_detail"><asp:Label ID="lblAptType2" runat="server"></asp:Label></div>
                        </td></tr>
                        </table>
                    </div>
                    <div class="unit_floorplan_details_row">
                        <table border="0">
                        <tr><td>
                        <div class="unit_floorplan_details_subtitle">Bathroom(s)</div>
                        </td><td>
                        <div class="unit_floorplan_details_detail"><asp:Label ID="lblAptBath" runat="server"></asp:Label></div>
                        </td></tr>
                        </table>
                    </div>
                    <div class="unit_floorplan_details_row">
                        <table border="0">
                        <tr>
                        <td>
                        <div class="unit_floorplan_details_subtitle">Square Feet</div>
                        </td>
                        <td><div class="unit_floorplan_details_detail"><asp:Label ID="lblAptArea" runat="server"></asp:Label></div>
                        </td>
                        </tr>
                        </table>
                    </div>
                    <div class="unit_floorplan_details_row">
                         <table border="0">
                        <tr>
                        <td>
                        <div class="unit_floorplan_details_subtitle">Floor Level</div>
                        </td><td>
                        <div class="unit_floorplan_details_detail"><asp:Label ID="lblAptFloor" runat="server"></asp:Label></div>
                         </td>
                        </tr>
                        </table>
                    </div>
                    <div class="unit_floorplan_details_row">
                         <table border="0">
                        <tr>
                        <td>
                        <div class="unit_floorplan_details_subtitle">Unit View(s)</div>
                        </td><td>
                        <div class="unit_floorplan_details_detail"><asp:Label ID="lblAptView" runat="server"></asp:Label></div>
                         </td>
                        </tr>
                        </table>
                    </div>
                    <div class="unit_floorplan_details_row">
                         <table border="0">
                        <tr>
                        <td>
                        <div class="unit_floorplan_details_subtitle">Price</div>
                        </td><td>
                        <div class="unit_floorplan_details_detail"><asp:Label ID="lblAptPrice" runat="server"></asp:Label></div>
                         </td>
                        </tr>
                        </table>
                    </div>
                    <div class="unit_floorplan_details_row">
                         <table border="0">
                        <tr>
                        <td>
                        <div class="unit_floorplan_details_subtitle">Availability</div>
                        </td><td>
                        <div class="unit_floorplan_details_detail"><asp:Label ID="lblAptAvailable" runat="server"></asp:Label></div>
                         </td>
                        </tr>
                        </table>
                    </div>
                    <div class="unit_floorplan_details_misc"></div>
                </div>
            </div>
            
        		<div class="unit_floorplan_details_buttons">
                <div class="unit_floorplan_details_button print_button small_button" onclick="window.print()">print</div>
                <div class="unit_floorplan_details_button small_button" runat="server" id="btnEmail" >email</div>
                <div class="unit_floorplan_details_add_button" id="FloorplanDetailsAdd" runat="server">
                  <div>my apartments</div> 
                </div>
                </div>
                	
				</asp:PlaceHolder>
                </Content>
                  <ClientEvents>
                    <CallbackComplete EventHandler="fireDetailCarouselCallback" />
                  </ClientEvents>
                  </ComponentArt:CallBack>
                
             </div>	          
                       
                       <ComponentArt:CallBack  id="CallbackDetailCarousel" runat="server">
              <Content>
               <asp:PlaceHolder ID="PlaceholderDetailCarousel" runat="server">
              <!--<div id="details_carousel">
                            <div style="margin-left:18px;margin-top:40px;">
				          <a href="javascript:tb_show('', 'CarouselCart.aspx?KeepThis=true&TB_iframe=true&height=750&width=1310', false);" ><img src="images/DelRay/search_carousel_title.jpg" alt="carousel title" /></a> 
                                     <ul id="mycarousel2" class="jcarousel jcarousel-skin-tango">
                                     <asp:Literal ID="literalDetailCarousel" runat="server"></asp:Literal>
                                     <asp:Repeater runat="server" ID="RepeaterDetailCarousel">
                                     <ItemTemplate>
                                        <li style="cursor:pointer;" onclick="loadApt('carousel',this.id);" id="<%#Container.ItemIndex%>"><img src="<%=Session("WSRetreiveAsset")%>size=1&width=73&height=74&crop=1&id=<%#Container.DataItem("Model_ASSETID")%>" alt="<%#Container.DataItem("Asset_ID")%>" width="73" height="74" /><p><%#Container.DataItem("Price_String")%><br /><small><%#Container.DataItem("Type").ToString.Replace("Bedroom", "BR")%>/<%#Container.DataItem("Bathroom").ToString%>BA</small></p></li>
                                     </ItemTemplate>
                                     </asp:Repeater>
                                     </ul>
				    </div>	 
				        </div>    -->
				        </asp:PlaceHolder>
              </Content>
              <ClientEvents>
                    <CallbackComplete EventHandler="fireDetailsCarousel" />
                  </ClientEvents>
              </ComponentArt:CallBack>
                       
           
              
               
            
			
			
			<ComponentArt:CallBack id="CallBackChangeCarousel"  CacheContent="False" runat="server" >
			<Content></Content>
			<ClientEvents>
			<CallbackComplete EventHandler="renderSearchCarousel" />
			</ClientEvents>
			</ComponentArt:CallBack> 
			
        				       
