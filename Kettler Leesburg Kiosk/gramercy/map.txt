﻿
<div id="map_canvas" class="LatLng38.861749_-77.053664_38.860722_-77.05552"></div>

<div id="map_cats">
    <div id="mapConvenience">
        <p id="LatLng38.864189_-77.063009">
            <img src="images/pentagon_row.jpg" alt="Pentagon Row" />
            <span>
                <strong>Pentagon Row</strong>
                <br />
                <span>1101 South Joyce Street
                    <br />Arlington, VA 22202
                    <br />(434) 977-0100
                </span>
            </span>
        </p>
        <p id="LatLng38.856143_-77.051454">
            <img src="images/crystal_city_shops.jpg" alt="" />
            <span>
                <strong>Crystal City Shops</strong>
                <br />
                <span>2100 Crystal Drive
                	 <br />Arlington, VA 22202
                	 <br />	(703) 413-4380
                </span>
            </span>
        </p>
        <p id="LatLng38.853829_-77.049544">
            <img src="images/crystal_drive.jpg" alt="" />
            <span>
                <strong>Crystal Drive</strong>
                <br />
                <span>Crystal Dr
                    <br />Arlington, VA
                    <br />(202) 889-5711
                </span>
            </span>
        </p>
        <p id="LatLng38.86484_-77.059736">
            <img src="images/fashion_center.jpg" alt="" />
            <span>
                <strong>Fashion Center</strong>
                <br />
                <span>1100 South Hayes Street 
                	<br />Arlington, VA 22202                    
                </span>
            </span>
        </p>
	<p id="LatLng38.85367_-77.05353">
            <img src="images/23rd_street.jpg" alt="" />
            <span>
                <strong>23rd Street South</strong>
                <br />
                <span>23rd Street South
                    <br />Arlington, VA
                </span>
            </span>
        </p>
       
    </div>
    <div id="mapDining">
        <p id="LatLng38.864189_-77.063009">
            <img src="images/pentagon_row.jpg" alt="Pentagon Row" />
            <span>
                <strong>Pentagon Row</strong>
                <br />
                <span>1101 South Joyce Street
                    <br />Arlington, VA 22202
                    <br />(434) 977-0100
                </span>
            </span>
        </p>
        <p id="LatLng38.856143_-77.051454">
            <img src="images/crystal_city_shops.jpg" alt="" />
            <span>
                <strong>Crystal City Shops</strong>
                <br />
                <span>2100 Crystal Drive
                	 <br />Arlington, VA 22202
                	 <br />	(703) 413-4380
                </span>
            </span>
        </p>
        <p id="LatLng38.853829_-77.049544">
            <img src="images/crystal_drive.jpg" alt="" />
            <span>
                <strong>Crystal Drive</strong>
                <br />
                <span>Crystal Dr
                    <br />Arlington, VA
                    <br />(202) 889-5711
                </span>
            </span>
        </p>
        <p id="LatLng38.85367_-77.05353">
            <img src="images/restaurant_row.jpg" alt="" />
            <span>
                <strong>Restaurant Row</strong>
                <br />
                <span>23rd Street South
                    <br />Arlington, VA
                </span>
            </span>
        </p>
    </div>
    <div id="mapServices">
                <p id="LatLng38.853829_-77.049544">
            <img src="images/crystal_drive.jpg" alt="" />
            <span>
                <strong>Crystal Drive</strong>
                <br />
                <span>Crystal Dr
                    <br />Arlington, VA
                    <br />(202) 889-5711
                </span>
            </span>
        </p>
	<p id="LatLng38.86738_-77.00411">
            <img src="images/long_bridge_park.jpg" alt="" />
            <span>
                <strong>Long Bridge Park</strong>
                <br />
                <span>475 South Old Jefferson Davis Highway
                    <br />Arlington, VA
                </span>
            </span>
        </p>
	<p id="LatLng38.859201_-77.061099">
            <img src="images/va_highlands_park.jpg" alt="" />
            <span>
                <strong>Virginia Highlands Park</strong>
                <br />
                <span>1600 South Hayes Street
                    <br />Arlington, VA
                </span>
            </span>
        </p>
	<p id="LatLng38.858404_-77.049165">
            <img src="images/water_park.jpg" alt="" />
            <span>
                <strong>Crystal City Water Park</strong>
                <br />
                <span>1750 Crystal Dr
                    <br />Arlington, VA
                </span>
            </span>
        </p>
	<p id="LatLng38.863027_-77.059253">
            <img src="images/capital_bikeshare.jpg" alt="" />
            <span>
                <strong>Capital Bike Share</strong>
                <br />
                <span>12th and South Hayes
                    <br />Arlington, VA
                </span>
            </span>
        </p>
	<p id="LatLng38.863027_-77.054275">
            <img src="images/capital_bikeshare.jpg" alt="" />
            <span>
                <strong>Capital Bike Share</strong>
                <br />
                <span>12th and South Eads
                    <br />Arlington, VA
                </span>
            </span>
        </p>
	<p id="LatLng38.860488_-77.059586">
            <img src="images/capital_bikeshare.jpg" alt="" />
            <span>
                <strong>Capital Bike Share</strong>
                <br />
                <span>15th and South Hayes
                    <br />Arlington, VA
                </span>
            </span>
        </p>
    </div>
    <div id="mapHistoric">
        <p id="LatLng38.853177_-77.043514">
            <img src="images/wmata.jpg" alt="" />
            <span>
                <strong>National Airport Metro</strong>
                <br />
                <span>Blue Line<br />
                Yellow Line
                </span>
            </span>
        </p>
        <p id="LatLng38.857923_-77.050552">
            <img src="images/wmata.jpg" alt="" />
            <span>
                <strong>Crystal City Metro</strong>
                <br />
                <span>Blue Line
                <br />Yellow line
                </span>
            </span>
        </p>
        <p id="LatLng38.862869_-77.059093">
            <img src="images/wmata.jpg" alt="" />
            <span>
                <strong>Pentagon City Metro</strong>
                <br />
                <span>Blue Line
                <br />Yellow Line
                </span>
            </span>
        </p>
         <p id="LatLng38.869585_-77.053728">
            <img src="images/wmata.jpg" alt="" />
            <span>
                <strong>Pentagon Metro</strong>
                <br />
                <span>Blue Line
                <br />Yellow Line
                </span>
            </span>
        </p>
        <p id="LatLng38.859289_-77.048444">
            <img src="images/vre.jpg" alt="" />
            <span>
                <strong>Virginia Railway Express</strong>
                <br />
                <span>1503 South Crystal Drive 
                <br />Arlington, Virginia 22202-4114
                </span>
            </span>
        </p>
    </div>
</div>
