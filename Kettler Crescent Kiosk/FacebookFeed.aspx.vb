﻿Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Net
Imports System.IO

Partial Class FacebookFeed
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim webStream As Stream
        Dim webResponse = ""
        Dim req As HttpWebRequest
        Dim res As HttpWebResponse

        Dim FacebookSql As String = ""
        Dim FacebookId As String = ""
        FacebookSql &= "select "
        FacebookSql &= "P3.Item_Value as FacebookID "
        FacebookSql &= "FROM IPM_PROJECT P "
        FacebookSql &= "left join IPM_PROJECT_FIELD_VALUE P3 on P.ProjectID = P3.ProjectID and P3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SOCIALFACEBOOK') "
        FacebookSql &= "where P.ProjectID = 6156620 "
        Dim FacebookDT As DataTable
        FacebookDT = New DataTable("FacebookDT")
        FacebookDT = mmfunctions.GetDataTable(FacebookSql)
        If FacebookDT.Rows.Count > 0 Then
            FacebookId = FacebookDT.Rows(0)("FacebookID").ToString
        End If

        'req = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings("FacebookFeed"))
        req = WebRequest.Create("http://www.facebook.com/feeds/page.php?format=atom10&id=" & FacebookId)
        req.Method = "POST"
        'req.Headers.Add("")
        req.UserAgent = "Mozilla/5.0"
        res = req.GetResponse()
        webStream = res.GetResponseStream()
        Dim webStreamReader As New StreamReader(webStream)

        While webStreamReader.Peek >= 0
            webResponse = webStreamReader.ReadToEnd()
        End While
        Response.Write(webResponse.ToString())
    End Sub


End Class
