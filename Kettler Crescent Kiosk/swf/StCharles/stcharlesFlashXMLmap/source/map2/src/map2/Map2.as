package map2 {
	import flash.display.*;
	import flash.events.MouseEvent;
	import flash.net.*;
	import com.boontaran.DataLoader;
	import com.boontaran.SWFLoader;
	import caurina.transitions.Tweener;
	
	public class Map2 extends MovieClip	{
		var paramObj:Object = loaderInfo.parameters;
		var xmlfile:String = paramObj.xmlfile;

		private const xmlFile:String = xmlfile;		
		internal static var xml:XML;
		
		private var map:Loader;
		private var keys:Array;
		private var keyCont:Sprite;
		
		private var box:Box;
		private var boxSpaceX:int = 25;
		private var boxSpaceY:int = 25;
		
		private var numBox:NumBox;
		private var numBoxList:Array;
		
		public function Map2() {
			DataLoader.load(xmlFile , xmlLoaded , xmlNotFound);
			
			stage.align     = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
		}
		private function xmlLoaded(data:String):void {
			xml = XML(data);
			
			//create keys
			createKeys();
			
			//load bg
			SWFLoader.load(xml.settings.map , mapLoaded,mapNotFound);
			
			
		}
		private function xmlNotFound():void {
			trace(xmlFile,' not found');
		}
		private function mapNotFound():void {
			trace(xml.settings.screen.map,' not found');
		}
		
		private function mapLoaded(ldr:Loader):void {
			map = ldr;
			addChild(map);
			
			//add keys
			addChild(keyCont);
			var posY:int = 0;
			for each(var key:Key in keys) {
				keyCont.addChild(key);
				key.y = posY;
				posY += key.height+1; // plus spacing
			}
			
			
			createNumBoxes();
		
			
			//create box
			box = new Box();
			box.alpha = 0;
			addChild(box);
			
			
			
			//look at mouse
			addEventListener(MouseEvent.MOUSE_OVER , mOver);
			addEventListener(MouseEvent.MOUSE_OUT , mOut);
			addEventListener(MouseEvent.MOUSE_DOWN , mDown);
		}
		private function createNumBoxes():void {
			var num:int = xml.item.length();
			var i:int;
			var nbox:NumBox;
			
			numBoxList = new Array();
			
			for (i = 0; i < num; i++) {
				nbox = new NumBox(xml.item[i]);
				addChild(nbox);
				
				numBoxList.push(nbox);
			}
		}
		private function mOver(e:MouseEvent):void {
			if (e.target is Key) {
				Key(e.target).mouseOver();
				showBox(Key(e.target));
			}
			if (e.target is NumBox) {
				var key:Key;
				var numBox:NumBox = NumBox(e.target);
				
				for each (key in keys) {
					if (key.id == numBox.id) {
						showBox(key);
						key.mouseOver();
					} 
				}
			}
			
		}
		private function mOut(e:MouseEvent):void {
			if (e.target is Key) {
				Key(e.target).mouseOut();
				hideBox();
			}
			if (e.target is NumBox) {
				hideBox();
				var key:Key;
				for each (key in keys) {
					key.mouseOut();
					//trace(key.mouseOut);
				}
			}
		}
		private function mDown(e:MouseEvent):void {
			if (e.target is Key) {
				navigateToURL(new URLRequest(Key(e.target).link),Key(e.target).linkTarget);
			}
		}
		private function showBox(key:Key):void {
			if (!key.description) return;
			
			Tweener.addTween(box, { alpha:1 , time:0.8 } );
			box.x = key.px + boxSpaceX;
			box.y = key.py + boxSpaceY;
			box.showDesc(key.description);
						
			if(box.y + box.height > stage.stageHeight) {
				box.y = key.py - box.height - 10;
			}
			if(box.x + box.width > stage.stageWidth) {
				box.x = key.px - box.width - 10;
			}
			
			//Tweener.addTween(smallBox, { alpha:1 , time:0.8 } );
			//smallBox.x = key.px;
			//smallBox.y = key.py;
			
			var nBox:NumBox;
			
			for each(nBox in numBoxList) {
				if (nBox.id == key.id) {
					nBox.hover();
				} else {
					nBox.normal();
				}
			}
			
			
		}
		private function hideBox():void {
			Tweener.addTween(box, { alpha:0 , time:0.8 } );
			
			for each(var nBox:NumBox in numBoxList) {
				
					nBox.normal();
				
			}
		}
		
		private function createKeys():void {
			keys = new Array();
			keyCont = new Sprite();
			
			
			var pos:Array = xml.settings.key_position.split(",");
			keyCont.x = pos[0];
			keyCont.y = pos[1];
						
			var key:Key;
			//title
			key = new Key();
			key.setTitle(xml.settings.title);
			keys.push(key);
			
			var i:int;
			var numKeys:int = xml.item.length();
			
			for (i = 0 ; i < numKeys ; i++) {
				key = new Key(xml.item[i]);
				keys.push(key);
			}
		}
		
	}

}