﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Services.ascx.vb" Inherits="ascx_Largo_Services" %>
<table cellpadding="0" cellspacing="0">
				        <tr>
				            <td class="servicesLeft"><asp:Image ID="imgServicesLeft" runat="server" /></td>
				            <td class="servicesCenter"><asp:Image ID="imgServicesCenter" runat="server" /></td>
				            <td class="servicesRight"><asp:Image ID="imgServicesRight" runat="server" /></td>
				        </tr>
				        <tr>
				            <td valign="top" colspan="3"><br />
				            <img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>services_title.jpg" alt="Amenities" />
				            <br /><br />
				            </td>
				        </tr>
				        <tr>
				            <td valign="top" style="line-height:20px;"><asp:Label ID="lblServices" runat="server"></asp:Label></td>
				            <td valign="top" colspan="2">
				            <asp:Repeater ID="repeaterServices" runat="server">
				            <ItemTemplate>
				            <div class="services_item"><%#Container.DataItem%></div>
				            </ItemTemplate>
				            </asp:Repeater>
				            </td>
				        </tr>
				</table>			