﻿Imports Microsoft.VisualBasic

Public Class ConvertFileSize

    Public Shared Function ConvertSize(ByVal fileSize As Long) As String

        Dim sizeOfKB As Long = 1024              ' Actual size in bytes of 1KB
        Dim sizeOfMB As Long = 1048576           ' 1MB
        Dim sizeOfGB As Long = 1073741824        ' 1GB
        Dim sizeOfTB As Long = 1099511627776     ' 1TB
        Dim sizeofPB As Long = 1125899906842624  ' 1PB

        Dim tempFileSize As Double
        Dim tempFileSizeString As String

        Dim myArr() As Char = {CChar("0"), CChar(".")}  'Characters to strip off the end of our string after formating

        If fileSize < sizeOfKB Then 'Filesize is in Bytes
            tempFileSize = ConvertBytes(fileSize, convTo.B)
            If tempFileSize = -1 Then Return Nothing 'Invalid conversion attempted so exit
            tempFileSizeString = Format(fileSize, "Standard").TrimEnd(myArr) ' Strip the 0's and 1's off the end of the string
            Return Math.Round(tempFileSize) & " bytes" 'Return our converted value

        ElseIf fileSize >= sizeOfKB And fileSize < sizeOfMB Then 'Filesize is in Kilobytes
            tempFileSize = ConvertBytes(fileSize, convTo.KB)
            If tempFileSize = -1 Then Return Nothing 'Invalid conversion attempted so exit
            tempFileSizeString = Format(fileSize, "Standard").TrimEnd(myArr)
            Return Math.Round(tempFileSize) & " KB"

        ElseIf fileSize >= sizeOfMB And fileSize < sizeOfGB Then ' Filesize is in Megabytes
            tempFileSize = ConvertBytes(fileSize, convTo.MB)
            If tempFileSize = -1 Then Return Nothing 'Invalid conversion attempted so exit
            tempFileSizeString = Format(fileSize, "Standard").TrimEnd(myArr)
            Return Math.Round(tempFileSize, 1) & " MB"

        ElseIf fileSize >= sizeOfGB And fileSize < sizeOfTB Then 'Filesize is in Gigabytes
            tempFileSize = ConvertBytes(fileSize, convTo.GB)
            If tempFileSize = -1 Then Return Nothing
            tempFileSizeString = Format(fileSize, "Standard").TrimEnd(myArr)
            Return Math.Round(tempFileSize, 1) & " GB"

        ElseIf fileSize >= sizeOfTB And fileSize < sizeofPB Then 'Filesize is in Terabytes
            tempFileSize = ConvertBytes(fileSize, convTo.TB)
            If tempFileSize = -1 Then Return Nothing
            tempFileSizeString = Format(fileSize, "Standard").TrimEnd(myArr)
            Return Math.Round(tempFileSize, 1) & " TB"

            'Anything bigger than that is silly ;)

        Else

            Return Nothing 'Invalid filesize so return Nothing

        End If

    End Function

    Public Shared Function ConvertBytes(ByVal bytes As Long, ByVal convertTo As convTo) As Double

        If convTo.IsDefined(GetType(convTo), convertTo) Then

            Return bytes / (1024 ^ convertTo)

        Else

            Return -1 'An invalid value was passed to this function so exit

        End If

    End Function

    Public Enum convTo
        B = 0
        KB = 1
        MB = 2
        GB = 3  'Enumerations for file size conversions
        TB = 4
        PB = 5
        EB = 6
        ZI = 7
        YI = 8
    End Enum


End Class
