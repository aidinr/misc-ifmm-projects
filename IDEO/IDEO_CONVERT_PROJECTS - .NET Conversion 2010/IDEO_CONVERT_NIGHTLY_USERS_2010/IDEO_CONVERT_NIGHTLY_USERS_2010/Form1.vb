﻿Imports System.Data.OleDb
Imports System.Configuration
Imports log4net
Imports log4net.Config

Public Class Form1
    Private Shared log As log4net.ILog
    Public ConnectionStringIDAM As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
    Public ConnectionStringUser As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringUsers")

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        log = log4net.LogManager.GetLogger("IDEO_USER_LOG")
        log4net.Config.XmlConfigurator.Configure()
        log.Debug("Startup")
        startIDEOEntSync()
        Me.Close()

    End Sub



    Public Shared Sub ExecuteTransaction(ByVal sqlstring As String)
        Dim ConnectionString As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
        Dim connection As New OleDbConnection(ConnectionString)
        Dim command As New OleDbCommand
        Dim transaction As OleDbTransaction

        ' Set the Connection to the new OleDbConnection.
        command.Connection = connection

        ' Open the connection and execute the transaction.
        Try
            connection.Open()

            ' Start a local transaction with ReadCommitted isolation level.
            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            ' Assign transaction object for a pending local transaction.
            command.Connection = connection
            command.Transaction = transaction

            ' Execute the commands.
            command.CommandText = sqlstring
            command.ExecuteNonQuery()

            ' Commit the transaction.
            transaction.Commit()
            Console.WriteLine("Executed.")

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Try to rollback the transaction
            Try
                transaction.Rollback()

            Catch
                ' Do nothing here; transaction is not active.
            End Try
        End Try
        ' The connection is automatically closed when the
        ' code exits the Using block.

    End Sub

    Public Shared Function GetDataTable(ByVal query As String, ByVal ConnectionString As String) As DataTable
        'Dim connection1 As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & System.Web.HttpContext.Current.Server.MapPath(dbFile))
        Dim connection1 As OleDbConnection = New OleDbConnection(ConnectionString)
        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)
        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()
        End Try
        Return table1
    End Function

    Function isullcheck(ByVal val)
        If val Is System.DBNull.Value Then
            isullcheck = ""
        Else
            isullcheck = val
        End If
    End Function


    Function isNull(ByVal val) As Boolean
        If val Is System.DBNull.Value Then
            isNull = True
        Else
            isNull = False
        End If
    End Function


    Dim mbChangedByCode As Boolean
    Dim mvBookMark As Object
    Dim mbEditFlag As Boolean
    Dim mbAddNewFlag As Boolean
    Dim mbDataChanged As Boolean
    Dim strcnn As String




    Public Function addPhoneNumber(sExternalUserID)
        Dim IDEOUsers As DataTable = GetDataTable("select a.empid, b.phonenumber from employee a, phones b where a.personid = b.personid and b.PhoneTypeID = 1 and a.empid = " & sExternalUserID, Me.ConnectionStringUser)

        If IDEOUsers.Rows.Count > 0 Then
            If GetDataTable("select * from ipm_user where externaluserid = " & sExternalUserID, Me.ConnectionStringIDAM).Rows.Count > 0 Then
                ExecuteTransaction("update ipm_user set phone = '" & Trim(IDEOUsers.Rows(0)("phonenumber")) & "'  where externaluserid = " & sExternalUserID)

            End If
        End If
       
        log.Debug("addPhoneNumber done")
    End Function



    Private Sub startIDEOEntSync()


        Dim SLOGIN As String
        Dim grouphandle As String
        Dim nickname As String
        Dim AreasOfExpertise, CurrentAndPastProjects, SoftwareExpertise, PersonalInfo, bio, extendedbio, sfirstname, slastname, sStatus As String


        'Normal update
        Dim IDEOUsers As DataTable = GetDataTable("select emain.nickname, * from employee a, person b, ideogroup c, ideo_office d, email e, employeemain emain Where a.empid = emain.employeeid and  a.personid = b.personid And a.groupid = c.groupid And a.homeoff = d.officeid And b.personid = e.personid and a.EmpStatus = 1 AND a.EmpType <> 4 order by personlastname", Me.ConnectionStringUser)
       
        'get max userid
        Dim i As Long
        i = GetDataTable("select max(cast(userid as decimal)) + 1 userid from ipm_user", Me.ConnectionStringIDAM).Rows(0)("userid")

        For Each row As DataRow In IDEOUsers.Rows
            Try

           
            SLOGIN = Replace(Trim(row("firstname")).Substring(0, 1) & Trim(row("lastname")), "'", "''", 1, -1, vbTextCompare)
            If GetDataTable("select * from ipm_user where externaluserid = " & row("empid"), Me.ConnectionStringIDAM).Rows.Count = 0 Then

                If row("lastname") <> "" Then



                    If Not isNull(row("grouphandle")) Then
                        grouphandle = Trim(Replace(row("grouphandle"), "'", "''"))
                    Else
                        grouphandle = ""
                    End If
                    If Not isNull(row("nickname")) Then
                        nickname = Trim(Replace(row("nickname"), "'", "''"))
                    Else
                        nickname = ""
                    End If
                    If Not isNull(row("AreasOfExpertise")) Then
                        AreasOfExpertise = Trim(Replace(row("AreasOfExpertise"), "'", "''"))
                    Else
                        AreasOfExpertise = ""
                    End If
                    If Not isNull(row("CurrentAndPastProjects")) Then
                        CurrentAndPastProjects = Trim(Replace(row("CurrentAndPastProjects"), "'", "''"))
                    Else
                        CurrentAndPastProjects = ""
                    End If
                    If Not isNull(row("SoftwareExpertise")) Then
                        SoftwareExpertise = Trim(Replace(row("SoftwareExpertise"), "'", "''"))
                    Else
                        SoftwareExpertise = ""
                    End If
                    If Not isNull(row("PersonalInfo")) Then
                        PersonalInfo = Trim(Replace(row("PersonalInfo"), "'", "''"))
                    Else
                        PersonalInfo = ""
                    End If
                    If Not isNull(row("Biography")) Then
                        bio = Trim(Replace(row("Biography"), "'", "''"))
                    Else
                        bio = ""
                    End If
                    If Not isNull(row("Biography")) Then
                        extendedbio = Trim(Replace(row("Biography"), "'", "''"))
                    Else
                        extendedbio = ""
                    End If

                    If nickname <> "" Then
                        sfirstname = nickname
                    Else
                        If Not isNull(row("firstname")) Then
                            sfirstname = Replace(row("firstname"), "'", "''", 1, -1, vbTextCompare)
                        Else
                            sfirstname = " "
                        End If
                    End If
                    slastname = Replace(row("lastname"), "'", "''", 1, -1, vbTextCompare)

                    Select Case row("empType")

                        Case 11
                            ' 'dbideo.Execute "insert into ipm_user  (nickname,Active,Agency,Bio,extendedbio,BirthDate,Cell,Contact,CreatedBy,department,Description,Education,email,ExpertiseArea,ExpertisePastWork,ExpertiseSoftware,ExternalUserID,Fax,firstname,Hint,HomeOffice,Introduction,lastname,Login,password,PersonalInfo,phone,RegDate,RegUpdateDate,Resume,SecurityLevel_Id,UserID,UserName,WorkAddress,WorkAddress2,WorkAddress3,WorkCity,WorkCountry,WorkState,WorkZip,Status) Values ('" & nickname & "','Y','IDEO','" & bio & "','" & extendedbio & "','01/01/2000','N/A','0','1','" & Trim(row("grouphandle) & "','','','" & row("EMAILADDRESS & " ','" & AreasOfExpertise & "','" & CurrentAndPastProjects & "','" & SoftwareExpertise & "'," & row("empid & ",'','" & Trim(row("firstname) & "','','" & Trim(row("HomeOff) & "','" & PersonalInfo & "','" & Trim(row("lastname) & "','" & SLOGIN & "','" & SLOGIN & "','" & PersonalInfo & "','','" & row("Begindate & "','" & row("DATEUPDATED & _
                            ' '' ''"',0,'1'," & i & ",'" & SLOGIN & "','','','','','','',''," & row("EmpStatus & ")"
                        Case 1

                            ExecuteTransaction("insert into ipm_user  (nickname,Active,Agency,Bio,extendedbio,BirthDate,Cell,Contact,CreatedBy,department,Description,Education,email,ExpertiseArea,ExpertisePastWork,ExpertiseSoftware,ExternalUserID,Fax,firstname,Hint,HomeOffice,Introduction,lastname,Login,password,PersonalInfo,phone,RegDate,RegUpdateDate,Resume,SecurityLevel_Id,UserID,UserName,WorkAddress,WorkAddress2,WorkAddress3,WorkCity,WorkCountry,WorkState,WorkZip,Status) Values ('" & nickname & "','Y','IDEO','" & bio & "','" & extendedbio & "','01/01/2000','N/A','0','1','" & Trim(row("grouphandle")) & "','','','" & row("EMAILADDRESS") & " ','" & AreasOfExpertise & "','" & CurrentAndPastProjects & "','" & SoftwareExpertise & "'," & row("empid") & ",'','" & Trim(sfirstname) & "','','" & Trim(row("HomeOff")) & "','" & PersonalInfo & "','" & Trim(slastname) & "','" & SLOGIN & "','" & SLOGIN & "','" & PersonalInfo & "','','" & row("Begindate") & "','" & row("DATEUPDATED") & "',0,'1'," & i & ",'" & SLOGIN & "','','','','','','',''," & row("EmpStatus") & ")")

                            addPhoneNumber(row("empid"))
                            'update Satus
                            log.Debug("startIDEOEntSync Adding " & ":" & row("PERSONLASTNAME") & "," & row("PERSONfirstNAME") & " - as user")




                        Case 3 And (InStr(1, Trim(row("emphandle")), "FESCO", vbTextCompare) > 0 Or InStr(1, Trim(row("emphandle")), "SAMSUNG", vbTextCompare) > 0)
                            ExecuteTransaction("insert into ipm_user  (nickname,Active,Agency,Bio,extendedbio,BirthDate,Cell,Contact,CreatedBy,department,Description,Education,email,ExpertiseArea,ExpertisePastWork,ExpertiseSoftware,ExternalUserID,Fax,firstname,Hint,HomeOffice,Introduction,lastname,Login,password,PersonalInfo,phone,RegDate,RegUpdateDate,Resume,SecurityLevel_Id,UserID,UserName,WorkAddress,WorkAddress2,WorkAddress3,WorkCity,WorkCountry,WorkState,WorkZip,Status) Values ('" & nickname & "','Y','IDEO','" & bio & "','" & extendedbio & "','01/01/2000','N/A','0','1','" & Trim(row("grouphandle")) & "','','','" & row("EMAILADDRESS") & " ','" & AreasOfExpertise & "','" & CurrentAndPastProjects & "','" & SoftwareExpertise & "'," & row("empid") & ",'','" & Trim(sfirstname) & "','','" & Trim(row("HomeOff")) & "','" & PersonalInfo & "','" & Trim(slastname) & "','" & SLOGIN & "','" & SLOGIN & "','" & PersonalInfo & "','','" & row("Begindate") & "','" & row("DATEUPDATED") & "',0,'1'," & i & ",'" & SLOGIN & "','','','','','','',''," & row("EmpStatus") & ")")
                            addPhoneNumber(row("empid"))
                            'update Satus

                            log.Debug("startIDEOEntSync Adding " & ":" & row("PERSONLASTNAME") & "," & row("PERSONfirstNAME") & " - as user")

                        Case 2

                            ExecuteTransaction("insert into ipm_user  (nickname,Active,Agency,Bio,extendedbio,BirthDate,Cell,Contact,CreatedBy,department,Description,Education,email,ExpertiseArea,ExpertisePastWork,ExpertiseSoftware,ExternalUserID,Fax,firstname,Hint,HomeOffice,Introduction,lastname,Login,password,PersonalInfo,phone,RegDate,RegUpdateDate,Resume,SecurityLevel_Id,UserID,UserName,WorkAddress,WorkAddress2,WorkAddress3,WorkCity,WorkCountry,WorkState,WorkZip,Status) Values ('" & nickname & "','Y','IDEO','" & bio & "','" & extendedbio & "','01/01/2000','N/A','1','1','" & Trim(row("grouphandle")) & "','','','" & Replace(row("EMAILADDRESS"), "'", "''") & " ','" & AreasOfExpertise & "','" & CurrentAndPastProjects & "','" & SoftwareExpertise & "'," & row("empid") & ",'','" & Trim(sfirstname) & "','','" & Trim(row("HomeOff")) & "','" & PersonalInfo & "','" & Trim(slastname) & "','" & SLOGIN & "','" & SLOGIN & "','" & PersonalInfo & "','','" & row("Begindate") & "','" & row("DATEUPDATED") & "',0,'1'," & i & ",'" & SLOGIN & "','','','','','','',''," & row("EmpStatus") & ")")
                            addPhoneNumber(row("empid"))
                            'update Satus

                            log.Debug("startIDEOEntSync Adding " & ":" & row("PERSONLASTNAME") & "," & row("PERSONfirstNAME") & " - as user")

                        Case 3
                            'dbideo.Execute "insert into ipm_vendor  (VendorID,Active,CreatedBy,email,ExternalVendorID,Name,UserID) Values (" & i & ",'1',1,'" & row("EMAILADDRESS & "'," & row("empid & ",'" & Trim(row("personlastname) & "',1)"
                    End Select
                    'MsgBox Err.Description

                End If


                'setvalues_users "IDEO\" & row("customerlogin, row("customerlogin, row("masterpassword, "3", "", clientprojectmanagerfirstname, clientprojectmanagerlastname, clientprojectmanageremail, clientprojectmanagercomp, clientprojectmanagerphone, clientprojectmanagercompanyaddress, clientprojectmanagercompanystate, clientprojectmanagercompanycity, clientprojectmanagercompanyzip, ""

                i = i + 1

            End If
            Catch ex As Exception

            End Try
        Next

        inActiveateUsers()
    End Sub

    Function getstatus(empid) As String
        Dim sStatus As String
        Dim dtUpdates As DataTable = GetDataTable("select typeofemployment from employeehire a, employee b Where a.employeeid = b.empid And b.empid = " & empid & " and hiredate in (select max(hiredate) from employeehire a, employee b where a.employeeid = b.empid and b.empid = " & empid & ")", ConnectionStringUser)
        If dtUpdates.Rows.Count > 0 Then
            Select Case dtUpdates.Rows(0)("typeofemployment")
                Case "202"
                    sStatus = "Contractor"
                Case "200"
                    sStatus = "Full Time"
                Case "203"
                    sStatus = "Intern"
                Case "201"
                    sStatus = "Part Time"
                Case "204"
                    sStatus = "Temp"
                Case Else
                    sStatus = "N/A"
            End Select
        End If
        Return sStatus
    End Function

    Public Function inActiveateUsers()

        Dim sUserID As String

        Dim dtIDEOUsers As DataTable = GetDataTable("select a.*,b.active from employee a,employeemain b where a.empid = b.EmployeeID", ConnectionStringUser)

        For Each row As DataRow In dtIDEOUsers.Rows
            Try

            
            sUserID = row("empid")
            '    adoPrimaryRS.Find "externaluserid = " & suserid, , adSearchForward, 1
           
            Dim sStatus As String = getstatus(row("empid"))
           
                'added addition of temp users to user list
            If sStatus = "Temp" Or sStatus = "Full Time" Or sStatus = "Intern" Or sStatus = "Part Time" Or sStatus = "" Or InStr(1, Trim(row("emphandle")), "FESCO", vbTextCompare) > 0 Or InStr(1, Trim(row("emphandle")), "SAMSUNG", vbTextCompare) > 0 Then
                'do nothing
                'send to user list
                If GetDataTable("select * from ipm_user where active = 'Y' and contact = 1 and externaluserid = " & sUserID, ConnectionStringIDAM).Rows.Count > 0 And row("active") = "Yes" Then
                    'check to see if already in contact list.
                    ExecuteTransaction("update ipm_user set contact = 0 where externaluserid = " & sUserID)
                    log.Debug("startIDEOEntSync Deleting " & ":" & sUserID & " - as user")
                End If
                If GetDataTable("select * from ipm_user where active = 'Y' and contact = 1 and externaluserid = " & sUserID, ConnectionStringIDAM).Rows.Count > 0 And row("active") = "No" Then
                    ExecuteTransaction("update ipm_user set contact = 1 where externaluserid = " & sUserID)
                    log.Debug("startIDEOEntSync Deleting " & ":" & sUserID & " - as user")
                End If
            Else
                'send to contact list
                If GetDataTable("select * from ipm_user where active = 'Y' and contact = 0 and externaluserid = " & sUserID, ConnectionStringIDAM).Rows.Count > 0 Then
                    ExecuteTransaction("update ipm_user set contact = 1 where externaluserid = " & sUserID)
                    log.Debug("startIDEOEntSync Deleting " & ":" & sUserID & " - as user")
                End If
            End If

            'final check for inactivated regardless of status and in user list.
             'send to contact list
            If GetDataTable("select * from ipm_user where active = 'Y' and contact = 0 and externaluserid = " & sUserID, ConnectionStringIDAM).Rows.Count > 0 And row("active") = "No" Then
                ExecuteTransaction("update ipm_user set contact = 1 where externaluserid = " & sUserID)
                log.Debug("startIDEOEntSync Deleting " & ":" & sUserID & " - as user")
            End If

            Catch ex As Exception
                log.Debug("startIDEOEntSync Error InactiveUsers " & ":" & row("empid").ToString)
            End Try

        Next

    End Function




End Class
