﻿Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Web.UI
Imports System.Web.UI.WebControls

Partial Class _AssetUDFPage
    Inherits System.Web.UI.Page

    Private con As New SqlConnection(ConfigurationManager.ConnectionStrings("dbconnection").ToString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGridData()
        End If
    End Sub
    Protected Sub BindGridData()
        con.Open()
        Dim cmd As New SqlCommand("SELECT [Item_ID], [Item_Tag], [Item_Name], [Item_Default_Value], d.[Active], [Viewable], [Editable], [Item_Tab], l.field_name as FIELD_TYPE FROM [IPM_ASSET_FIELD_DESC] d join IPM_FIELDTYPE_LOOKUP l on d.item_type = l.field_id where item_name like 'Archi%'", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable()
        da.Fill(dt)

        gvdetails.DataSource = dt

        gvdetails.DataBind()
    End Sub
    'Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdate.Click
    '    DirectCast(gvdetails.Rows(Integer.Parse(lblRow.Text)).FindControl("hdnDefaultValue"), HiddenField).Value = txtDefaultValue.Text

    'End Sub
    Protected Sub imgbtn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btndetails As LinkButton = TryCast(sender, LinkButton)
        Dim gvrow As GridViewRow = DirectCast(btndetails.NamingContainer, GridViewRow)
        lblID.Text = gvdetails.DataKeys(gvrow.RowIndex).Values("Item_Id").ToString()
        txtDefaultValue.Text = DirectCast(gvrow.FindControl("hdnDefaultValue"), HiddenField).Value.ToString()
        lblRow.Text = gvrow.RowIndex
        Dim s As String = "<script>	$(function() {		$( ""#dialog"" ).dialog();	});	</script>"
        Dim s2 As String = "<script type='text/javascript'>	$(document).ready(function() {  $(""#" & btnUpdate.ClientID & """).live('click', function() { document.getElementById('" & DirectCast(gvrow.FindControl("hdnDefaultValue"), HiddenField).ClientID & "').value = document.getElementById('" & txtDefaultValue.ClientID & "').value; $('#dialog').dialog('close'); }); $(""#" & btnCancel.ClientID & """).live('click', function() { $('#dialog').dialog('close'); }); } );	</script>"

        If (Not Page.ClientScript.IsStartupScriptRegistered("JSScript_Dialog")) Then

            Page.ClientScript.RegisterStartupScript(Me.GetType(), "JSScript_Dialog", s & " " & s2)

        End If

    End Sub
    Protected Sub lnkUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btndetails As LinkButton = TryCast(sender, LinkButton)
        Dim gvrow As GridViewRow = DirectCast(btndetails.NamingContainer, GridViewRow)

        con.Open()
        Dim cmd As New SqlCommand("update IPM_ASSET_FIELD_DESC set Item_Default_Value = @DefaultValue where item_id=@ItemId", con)
        Dim strDefaultValue As String
        strDefaultValue = DirectCast(gvdetails.Rows(gvrow.RowIndex).FindControl("hdnDefaultValue"), HiddenField).Value.ToString()
        Dim strItemId As String
        strItemId = gvdetails.DataKeys(gvrow.RowIndex).Values("Item_Id").ToString()
        cmd.Parameters.AddWithValue("@DefaultValue", strDefaultValue)
        cmd.Parameters.AddWithValue("@ItemId", strItemId)
        cmd.ExecuteNonQuery()
        con.Close()
        lblresult.Text = " Details Updated Successfully"
        lblresult.ForeColor = Color.Green
        'BindGridData()


    End Sub
   
End Class

