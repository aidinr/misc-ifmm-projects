﻿Imports Microsoft.VisualBasic
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class AutoCompleteControl
    Private con As New SqlConnection(ConfigurationManager.ConnectionStrings("dbconnection").ToString())


    Public Function Build_AC_Control(item_id As String, item_name As String, item_value As String, asset_id As String, item_default_value As String, Optional state As String = "full") As String
        '// populate unique ids
        Dim lblItemNameID As String = "lblItemName_" & item_id
        Dim txtAutoCompleteID As String = "txtAutoComplete_" & item_id
        Dim hdnAssetID As String = "hdnAssetID_" & item_id
        Dim hdnSelectedValuesID As String = "hdnSelectedValues_" & item_id
        Dim lnkAddID As String = "lnkAdd_" & item_id
        Dim SelectedListTableID As String = "SelectedListTable_" & item_id
        Dim labelHTML As String = ""
        Dim txtAutoCompleteHTML As String = ""
        Dim hdnAssetIDHTML As String = ""
        Dim hdnSelectedValuesHTML As String = ""
        Dim SelectedListTableHTML As String = ""
        Dim lnkAddHTML As String = ""
        '// end populate unique ids
        If state = "full" Then
            
            '// Adding javscript when gaining focus of textbox to assign autocomplete Control Functionality from JQueryUI
            txtAutoCompleteHTML = "<input name=""" & txtAutoCompleteID & """ type=""text"" id=""" & txtAutoCompleteID & """ onfocus='" & Build_OnFocus_Autocomplete_JQueryUIScript(txtAutoCompleteID, item_default_value) & "' />"
            '// end Addign Javascript focus

            '// Assign onclick attribute values for the Add linkbutton (javascript code)
            lnkAddHTML = "<a href=""#"" id=""" & lnkAddID & """ onclick=""Add_ACValue_ClickEvent(&#39;" & txtAutoCompleteID & "&#39;, &#39;" & SelectedListTableID & "&#39;, &#39;" & hdnSelectedValuesID & "&#39;);"">Add</a>"
            '// end assign onclick attribute ....
        End If

        '// populate field's label with the value from DB
        labelHTML = "<span id=""" & lblItemNameID & """>" & item_name & "&nbsp;&nbsp;&nbsp;</span>"
        '// end populate field's label


        '// end assign hidden fields values
        hdnAssetIDHTML = "<input type=""hidden"" name=""" & hdnAssetID & """ id=""" & hdnAssetID & """ value=""" & asset_id & """ />"
        hdnSelectedValuesHTML = "<input type=""hidden"" name=""" & hdnSelectedValuesID & """ id=""" & hdnSelectedValuesID & """ value=""" & item_value & """ />"
        '// end assign hidden ield values

        '// Populate the table with the values from db (this also contains the javascript code onclick when removing the items)
        SelectedListTableHTML = BuildHTMLTable(item_value, hdnSelectedValuesID, SelectedListTableID, state)
        '// end populate the table


        '// put it all together
        Return "<DIV class=""ui-widget"">" & SelectedListTableHTML & "<BR/>" & labelHTML & txtAutoCompleteHTML & lnkAddHTML & hdnAssetIDHTML & hdnSelectedValuesHTML & "<BR/>" & "<BR/>" & "<BR/></DIV>"

    End Function


    Private Function BuildHTMLTable(item_value As String, strHiddenFieldClientID As String, strTableClientId As String, state As String) As String
        Dim tr As HtmlTableRow
        Dim arr As New ArrayList(item_value.Split(";"))
        Dim htmlTable As String
        htmlTable = " <table id=""" & strTableClientId & """><tr><td></td><td></td></tr>"


        For Each item As String In arr
            If item.Trim() <> "" Then
                If state = "full" Then
                    htmlTable = htmlTable & "<TR><TD>" & "[<a  href='" & Chr(35) & "' onclick=""$(this).closest('tr').remove(); PopulateHiddenField('" & strHiddenFieldClientID & "', '" & strTableClientId & "');"" >X</a>]</TD><TD class=""content"">" & item & "</TD></TR>"
                Else
                    htmlTable = htmlTable & "<TR><TD></TD><TD class=""content"">" & item & "</TD></TR>"
                End If
            End If
        Next

        htmlTable = htmlTable & "</table>"
        Return htmlTable
    End Function




    Private Function Build_OnFocus_Autocomplete_JQueryUIScript(strAutoCompleteClientId As String, strItemDefaultValue As String) As String
        Dim sb As System.Text.StringBuilder = New System.Text.StringBuilder()

        strItemDefaultValue = strItemDefaultValue.Trim().Replace(vbCrLf, "")
        strItemDefaultValue = strItemDefaultValue.Trim().Replace(";", """,""")
        strItemDefaultValue = """" & strItemDefaultValue & """"
        sb.Append(" $(function () {")
        sb.Append("       var availableTags = [")
        sb.Append(" " & strItemDefaultValue & " ")

        sb.Append("		];")
        sb.Append("        $(""#" & strAutoCompleteClientId & """).autocomplete({")
        sb.Append("         source: availableTags")
        sb.Append("        });")
        sb.Append("        $(""#" & strAutoCompleteClientId & """).autocomplete(""option"", ""autoFocus"", true);")
        sb.Append(" });")

        Return sb.ToString()
    End Function


    Public Sub UpdateField(item_id As String, SelectedValues As String, asset_id As String)
        con.Open()
        Dim cmd As New SqlCommand("delete ipm_Asset_field_value where asset_id = @asset_id and item_id = @item_id", con)
        cmd.Parameters.AddWithValue("@asset_id", asset_id)
        cmd.Parameters.AddWithValue("@item_id", item_Id)
        cmd.Connection = con
        cmd.ExecuteNonQuery()

        cmd = New SqlCommand("insert into ipm_Asset_field_value (asset_id, Item_id, Item_value) values ( @asset_id, @item_id, @item_value)", con)
        cmd.Parameters.AddWithValue("@asset_id", asset_id)
        cmd.Parameters.AddWithValue("@item_id", item_Id)
        cmd.Parameters.AddWithValue("@item_value", SelectedValues)
        cmd.Connection = con
        cmd.ExecuteNonQuery()
        con.Close()

    End Sub

End Class
