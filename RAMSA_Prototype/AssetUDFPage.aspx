﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AssetUDFPage.aspx.vb" Title="Asset UDF Page" Inherits="_AssetUDFPage" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>Untitled Page</title>
<link rel="stylesheet" href="development-bundle/themes/base/jquery.ui.all.css">
	<script src="development-bundle/jquery-1.7.2.js"></script>
	<script src="development-bundle/external/jquery.bgiframe-2.1.2.js"></script>
	<script src="development-bundle/ui/jquery.ui.core.js"></script>
	<script src="development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="development-bundle/ui/jquery.ui.mouse.js"></script>
	<script src="development-bundle/ui/jquery.ui.draggable.js"></script>
	<script src="development-bundle/ui/jquery.ui.position.js"></script>
	<script src="development-bundle/ui/jquery.ui.resizable.js"></script>
	<script src="development-bundle/ui/jquery.ui.dialog.js"></script>
	<link rel="stylesheet" href="development-bundle/demos/demos.css">

</head>
<body>
<form id="form1" runat="server">

<div>
<asp:GridView EnableViewState="true" runat="server" ID="gvdetails"  DataKeyNames="Item_Id" AutoGenerateColumns="false">
<RowStyle BackColor="#EFF3FB" />
<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
<AlternatingRowStyle BackColor="White" />
<Columns>

<asp:BoundField DataField="Item_Tag" HeaderText="Item_Tag" />
<asp:BoundField DataField="Item_Name" HeaderText="Item_Name"  />

<asp:TemplateField HeaderText="Default_Value">
<ItemTemplate>
<!-- <a href="#" onclick="" ></a> -->
 <asp:LinkButton ID="imgbtn" Text="View/Edit" runat="server" Width="25" Height="25" OnClick="imgbtn_Click" /> 
<asp:HiddenField ID="hdnDefaultValue" Value='<%# DataBinder.Eval(Container, "DataItem.ITEM_DEFAULT_VALUE")%>' runat="server"   />
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="" HeaderStyle-Width="100">
<ItemTemplate>
<asp:LinkButton ID="lnkUpdate" Text="Update DB" runat="server" Width="100" Height="25" OnClick="lnkUpdate_Click" />
</ItemTemplate>
</asp:TemplateField>

<asp:BoundField DataField="Active" HeaderText="Active" />
<asp:BoundField DataField="Viewable" HeaderText="Viewable" />
<asp:BoundField DataField="Editable" HeaderText="Editable" />
<asp:BoundField DataField="Item_Tab" HeaderText="Item_Tab" />
<asp:BoundField DataField="FIELD_TYPE" HeaderText="FIELD_TYPE" />
</Columns>
</asp:GridView>
<asp:Label ID="lblresult" runat="server"/>

<div id="dialog" style="display:none" >
<table width="100%" style="border:Solid 3px #D55500; width:100%; height:100%" cellpadding="0" cellspacing="0">
<tr style="background-color:#D55500">
<td colspan="2" style=" height:10%; color:White; font-weight:bold; font-size:larger" align="center">Default Value Details</td>
</tr>
<tr>
<td align="right" style=" width:45%">
Item Id:
</td>
<td>
<asp:Label ID="lblID" runat="server"></asp:Label>
</td>
</tr>
<tr>
<td align="right" style=" width:45%">
Row:
</td>
<td>
<asp:Label ID="lblRow" runat="server"></asp:Label>
</td>
</tr>
<tr>
<td valign="top" align="right">
Default Value:
</td>
<td>
<asp:TextBox ID="txtDefaultValue" TextMode="MultiLine" Width="200" Height="400" runat="server"/>
</td>
</tr>
<tr>
<td>
</td>
<td>
<input type="button" ID="btnUpdate" runat="server" value="OK"/>
<input type="button" ID="btnCancel" runat="server" value="Cancel" />
</td>
</tr>
</table>
</div>
<asp:HyperLink runat="server" ID="h1" Text="GoTo Asset Detail Page" NavigateUrl="~/AssetDetails.aspx"></asp:HyperLink>

</div>
</form>
</body>
</html>