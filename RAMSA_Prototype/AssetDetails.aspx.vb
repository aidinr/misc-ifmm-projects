﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.DataRowView
Imports System.IO

Partial Class AssetDetails
    Inherits System.Web.UI.Page

    Private con As New SqlConnection(ConfigurationManager.ConnectionStrings("dbconnection").ToString())
    Dim assetID As String = "21600799"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        

        If Page.IsPostBack = False Then

            BindLiteral()
        End If
    End Sub

    Private Sub BindLiteral()
        Dim dr As DataRow
        Dim dt As DataTable
        hdnAutoCompleteMultiselectItemIDList.Value = ""
        literalContainer.Text = ""
        dt = GetUDFFields("2", "Asset Details", assetID)
        For Each dr In dt.Rows

            'Loading AutoCompleteUsercontrol (temporary)
            Dim objAC_Control As New AutoCompleteControl

            literalContainer.Text = literalContainer.Text & " " & objAC_Control.Build_AC_Control(dr.Item("Item_ID").ToString(), dr.Item("Item_Name").ToString(), dr.Item("Item_Value").ToString(), assetID, dr.Item("Item_Default_Value").ToString(), "readonly")

            hdnAutoCompleteMultiselectItemIDList.Value = hdnAutoCompleteMultiselectItemIDList.Value.ToString() & ";" & dr.Item("Item_ID").ToString()



        Next
    End Sub

    Private Function GetUDFFields(Field_type As String, field_tab As String, asset_id As String) As DataTable
        con.Open()
        Dim cmd As New SqlCommand("SELECT d.[Item_ID], d.[Item_Name], d.[Item_Default_Value], isnull(v.Item_Value,'') as Item_Value FROM [IPM_ASSET_FIELD_DESC] d join IPM_FIELDTYPE_LOOKUP l on d.item_type = l.field_id left outer join IPM_ASSET_FIELD_VALUE v on v.item_id = d.item_id and asset_id = @asset_id  where item_name like 'Archi%' and l.field_id = @field_type and ltrim(rtrim(item_tab)) = @item_tab  ", con)
        cmd.Parameters.AddWithValue("@field_type", Field_type)
        cmd.Parameters.AddWithValue("@item_tab", field_tab)
        cmd.Parameters.AddWithValue("@asset_id", asset_id)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable()
        da.Fill(dt)
        con.Close()
        Return dt
    End Function

    Protected Sub lnkUpdate_OnClick(sender As Object, e As System.EventArgs) Handles lnkUpdate.Click
        Dim arrItemIDList As New ArrayList(hdnAutoCompleteMultiselectItemIDList.Value.Split(";"))
        Dim item_id As String
        Dim objAC_Control As New AutoCompleteControl
        For Each item_id In arrItemIDList
            If item_id <> "" Then
                objAC_Control.UpdateField(item_id, Request("hdnSelectedValues_" & item_id), assetID)
            End If

        Next
        BindLiteral()
       
    End Sub
End Class
