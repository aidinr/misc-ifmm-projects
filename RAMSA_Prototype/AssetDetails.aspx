﻿<%@ Page EnableEventValidation="false" Language="VB" AutoEventWireup="false" CodeFile="AssetDetails.aspx.vb" Inherits="AssetDetails" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   	<meta charset="utf-8">
	<title>Asset Detail Page</title>
	<link rel="stylesheet" href="development-bundle/themes/base/jquery.ui.all.css"/>
	<script src="development-bundle/jquery-1.7.2.js"></script>
	<script src="development-bundle/ui/jquery.ui.core.js"></script>
	<script src="development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="development-bundle/ui/jquery.ui.position.js"></script>
	<script src="development-bundle/ui/jquery.ui.autocomplete.js"></script>
	<link rel="stylesheet" href="development-bundle/demos/demos.css">
 
    <script type="text/javascript">
      
      function Add_ACValue_ClickEvent(strAutoCompleteClientId, strTableClientId, strHiddenFieldClientID  ) { 
            var name = $('#' + strAutoCompleteClientId).val();
            if (name != '') { $("#" + strTableClientId + " tr:last").after("<tr><td>[<a  href='#' onclick=\"$(this).closest('tr').remove(); PopulateHiddenField('" + strHiddenFieldClientID + "', '" + strTableClientId + "');\" >X</a>]</td><td class='content'>" + name + "</td></tr>");}
            PopulateHiddenField( strHiddenFieldClientID,  strTableClientId );
            document.getElementById( strAutoCompleteClientId).value='';
       }
        

        function PopulateHiddenField(strHiddenField, strSelectedTableField) {
            
            document.getElementById(strHiddenField).value = "";
            $('#' + strSelectedTableField + ' td.content').each(function () {

                
                document.getElementById(strHiddenField).value = document.getElementById(strHiddenField).value + $(this).html() + ";";
            });
            
        }

    </script>


</head>
<body>
  
    <form id="form1" runat="server">
  
    
        <div class="ui-widget">
        <asp:Literal runat="server" ID = "literalContainer"></asp:Literal>

        <asp:HiddenField runat="server" ID = "hdnAutoCompleteMultiselectItemIDList" />
        
        
        <asp:Label ID="lblresult" runat="server"/>
        <asp:LinkButton runat="server" ID="lnkUpdate" Text="  UpdateDB" OnClick="lnkUpdate_OnClick"></asp:LinkButton>
        <br /><br />    
        <asp:HyperLink ID="h1" runat="server" Text="GoTo UDFPage" NavigateUrl="~/AssetUDFPage.aspx"></asp:HyperLink>
    </div>
    </form>
</body>
</html>
