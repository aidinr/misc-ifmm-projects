﻿Imports System.Data

Partial Class category
    Inherits System.Web.UI.Page

    Public CategoryId As Integer
    Public practiceName As String
    Public MainPracticeId As String
    Public MainPracticeName As String
    Public practiceAreaUDFID As String
    Public CategoryTitle As String
    Public TotalRecords As Integer
    Public RecordPR As Integer
    Public NewRow As Integer
    Public ProjectsDT As DataTable
    Public practiceFrontText As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim i = Request.QueryString("noproject")
        If (i <> "") Then
            DisplayNoProject()
        Else
            DisplayProjects()
        End If
    End Sub

    Sub DisplayProjects()
        CategoryId = Request.QueryString("cat")

        Select Case CategoryId
            Case "3413026"
                practiceName = "Broadcast and Media"
                practiceFrontText = "Broadcast and Media"
            Case "3414326"
                practiceName = "Courts and Public"
                practiceFrontText = "Courts and Public"
            Case "2400012"
                practiceName = "Corporate Interiors"
                practiceFrontText = "Corporate Interiors"
                practiceAreaUDFID = "3408922"
            Case "3409338"
                practiceName = "Education"
                practiceFrontText = "Education"
                practiceAreaUDFID = "3408922"
            Case "3409328"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "Campus Planning"
                practiceFrontText = "Campus Planning"
                practiceAreaUDFID = "3408924"
            Case "2400014"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "Higher Education"
                practiceFrontText = "Higher Education"
                practiceAreaUDFID = "3408924"
            Case "2400017"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "K-12 Education"
                practiceFrontText = "K-12 Education"
                practiceAreaUDFID = "3408924"
            Case "3409341"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "Town and Gown"
                practiceFrontText = "Town and Gown"
                practiceAreaUDFID = "3408924"
            Case "2400024"
                practiceName = "Healthcare"
                practiceFrontText = "Healthcare"
                practiceAreaUDFID = "3408922"
            Case "2400015"
                practiceName = "Hospitality"
                practiceFrontText = "Hospitality"
                practiceAreaUDFID = "3408922"
            Case "2400016"
                practiceName = "Housing"
                practiceFrontText = "Residential"
                practiceAreaUDFID = "3408922"
            Case "2400018"
                practiceName = "Office and Retail"
                practiceFrontText = "Office and Retail"
                practiceAreaUDFID = "3408922"
            Case "2400019"
                practiceName = "Cultural"
                practiceFrontText = "Cultural"
                practiceAreaUDFID = "3408922"
            Case "2400020"
                practiceName = "Science and Technology"
                practiceFrontText = "Science and Technology"
                practiceAreaUDFID = "3408922"
            Case "2400021"
                practiceName = "Senior Living"
                practiceFrontText = "Senior Living"
                practiceAreaUDFID = "3408922"
            Case "3409343"
                practiceName = "Urbanism"
                practiceFrontText = "Urbanism"
                practiceAreaUDFID = "3408922"
            Case "3409345"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Downtown Redevelopment"
                practiceFrontText = "Downtown Redevelopment"
                practiceAreaUDFID = "3408924"
            Case "3409347"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "New Town Planning"
                practiceFrontText = "New Town Planning"
                practiceAreaUDFID = "3408924"
            Case "3409349"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Transit and Development"
                practiceFrontText = "Transit and Development"
                practiceAreaUDFID = "3408924"
            Case "2400022"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Urban Design"
                practiceFrontText = "Urban Design"
                practiceAreaUDFID = "3408924"
            Case "3409351"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Waterfront"
                practiceFrontText = "Waterfront"
                practiceAreaUDFID = "3408924"
            Case "5699021"
                practiceName = "Sports and Exhibition"
                practiceFrontText = "Sports and Exhibition"

            Case "3421827"
                practiceName = "Planning and Urban Design"
                practiceFrontText = "Planning and Urban Design"
            Case "3421826"
                practiceName = "Transportation and Public Infrastructure"
                practiceFrontText = "Transportation and Public Infrastructure"
            Case "3421825"
                practiceName = "Large Scale Mixed Use"
                practiceFrontText = "Large Scale Mixed Use"
            Case Else
                DisplayNoProject()
                Exit Sub
        End Select

        category_name.Text = practiceFrontText
        CategoryTitle = practiceFrontText & " / Perkins Eastman"

        Dim out As String = ""
        Dim out2 As String = ""
        Dim sql As String = ""
        sql &= "select "
        sql &= "P.ProjectID, "
        sql &= "P.Name as ProjectName, "
        sql &= "P1.Item_Value AS Region, "
        sql &= "P2.Item_Value AS PracticeArea, "
        sql &= "P8.Item_Value as SubPracticeArea, "
        sql &= "case when isnull(P.State_id,'') <> '' or P.State_id is null then P.City + case when isnull(S.Name ,'') <> '' then ', ' + S.Name else '' end else p.City + case when isnull(P5.Item_Value,'') <> '' then ', ' + P5.Item_Value else '' end + case when isnull(P4.Item_Value,'') <> '' then ', ' + P4.Item_Value else '' end end as ProjectAddress, "
        sql &= "P6.Item_Value As WebDescription, "
        sql &= "P7.Item_Value as ShortName "
        sql &= "FROM IPM_PROJECT P "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P1 on P.ProjectID = P1.ProjectID and P1.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_REGIONS') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P2 on P.ProjectID = P2.ProjectID and P2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PRACTICEAREA') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P3 on P.ProjectID = P3.ProjectID and P3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_CITY') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P4 on P.ProjectID = P4.ProjectID and P4.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_COUNTRY') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P5 on P.ProjectID = P5.ProjectID and P5.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_STATE') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P6 on P.ProjectID = P6.ProjectID and P6.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_V_DESCRIPTION_WEB') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P7 on P.ProjectID = P7.ProjectID and P7.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_SHORT_NAME') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P8 on P.ProjectID = P8.ProjectID and P8.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUBPRACTICEAREA') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P9 on P.ProjectID = P9.ProjectID and P9.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAN_PROJECT_ORDERING') "
        sql &= "left join IPM_STATE S on S.State_id = P.State_id "
        sql &= "WHERE P.Available = 'Y' "
        sql &= "AND P.Show = 1 "
        If CategoryId = 3409328 Or CategoryId = 2400014 Or CategoryId = 2400017 Or CategoryId = 3409341 Or CategoryId = 3409345 Or CategoryId = 3409347 Or CategoryId = 3409349 Or CategoryId = 2400022 Or CategoryId = 3409351 Then
            sql &= "AND charindex('" & practiceName & "',LTRIM(RTRIM(P8.Item_Value)))!= 0 "
        Else
            sql &= "AND charindex('" & practiceName & "',LTRIM(RTRIM(P2.Item_Value)))!= 0 "
        End If
        sql &= "order by CAST(isnull(P9.Item_Value, 9999999999) as integer) desc"

        ProjectsDT = New DataTable("ProjectsDT")
        ProjectsDT = mmfunctions.GetDataTable(sql)

        RecordPR = 9
        TotalRecords = ProjectsDT.Rows.Count

        If TotalRecords > 0 Then
            For R As Integer = 0 To TotalRecords - 1
                NewRow = NewRow + 1
                If NewRow = RecordPR Then
                    out &= "<li class=""last_in_row"">"
                    NewRow = 0
                Else
                    out &= " <li> "
                End If
                Dim PrFriendlyLink As String = "project_" & ProjectsDT.Rows(R)("ProjectID").ToString.Trim & "_" & CMSFunctions.FormatFriendlyUrl(ProjectsDT.Rows(R)("ProjectName").ToString.Trim.ToLower, "_")
                out &= "<a href=""" & PrFriendlyLink & """ title="""" rel=""" & R + 1 & """>"
                out &= "<img src=""/dynamic/image/week/project/best/60x60/92/777777/South/" & ProjectsDT.Rows(R)("ProjectID").ToString.Trim & ".jpg"" alt=""""/>"
                out &= "</a>"
                out &= "</li>"
                out2 &= " <div class=""big_image_with_info_box"" id=""box_" & R + 1 & """>"
                out2 &= "<a class=""proj"" href=""" & PrFriendlyLink & """ > "
                out2 &= "<img src=""dynamic/image/week/project/best/700x467/92/ffffff/SouthWest/" & ProjectsDT.Rows(R)("ProjectID").ToString.Trim & ".jpg"" alt=""""/>"
                out2 &= "<span>"
                out2 &= "<em>" & formatfunctions.AutoFormatText(ProjectsDT.Rows(R)("ProjectName").ToString.Trim) & "</em>"
                out2 &= "<span>" & formatfunctions.AutoFormatText(ProjectsDT.Rows(R)("ProjectAddress").ToString.Trim) & "</span>"
                out2 &= "</span>"
                out2 &= "</a>"
                out2 &= "</div>"
            Next

            'Retaled Topics
            Dim sql4a As String = ""
            Dim sql4b As String = ""
            Dim sql4c As String = ""
            Dim sql4d As String = ""
            Dim sql4e As String = ""
            Dim sql4f As String = ""
            Dim sql4g As String = ""
            'Testi
            sql4a &= "select "
            sql4a &= "U.UserID, "
            sql4a &= "U.FirstName + ' ' + U.LastName FullName, "
            sql4a &= "U1.Item_Value As Testimonial, "
            sql4a &= "U2.Item_Value As Link, "
            sql4a &= "U3.Item_Value as RelId, "
            sql4a &= "U4.Item_Value as SubTestimonial "
            sql4a &= "from IPM_USER U "
            sql4a &= "left join IPM_USER_FIELD_VALUE U1 on U.UserID = U1.USER_ID and U1.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTIMONIAL') "
            sql4a &= "left join IPM_USER_FIELD_VALUE U2 on U.UserID = U2.USER_ID and U2.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USERLINK') "
            sql4a &= "left join IPM_USER_FIELD_VALUE U3 on U.UserID = U3.USER_ID and U3.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTIIDS') "
            sql4a &= "left join IPM_USER_FIELD_VALUE U4 on U.UserID = U4.USER_ID and U4.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTISUB') "
            sql4a &= "where Active = 'Y' "
            sql4a &= "and u.Contact = 1 "
            sql4a &= "and charindex('" & CategoryId & "',U3.Item_Value)!= 0 "
            Dim TestimonialDT As DataTable
            TestimonialDT = New DataTable("TestimonialDT")
            TestimonialDT = mmfunctions.GetDataTable(sql4a)
            'Award
            sql4c &= "select top 0 "
            sql4c &= "A.Awards_Id, "
            sql4c &= "A.Headline, "
            sql4c &= "A.PublicationTitle,"
            sql4c &= "YEAR(A.Post_Date) as AwardYear, "
            sql4c &= "P2.Item_Value As Practice, "
            sql4c &= "P8.Item_Value AS SubPractice "
            sql4c &= "FROM IPM_AWARDS A "
            sql4c &= "join IPM_AWARDS_RELATED_PROJECTS ARP on ARP.Awards_Id = A.Awards_Id "
            sql4c &= "join IPM_PROJECT P on P.ProjectID = ARP.ProjectID "
            sql4c &= "left join IPM_PROJECT_FIELD_VALUE P2 on P.ProjectID = P2.ProjectID and P2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PRACTICEAREA') "
            sql4c &= "left join IPM_PROJECT_FIELD_VALUE P8 on P.ProjectID = P8.ProjectID and P8.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUBPRACTICEAREA') "
            If CategoryId = 3409328 Or CategoryId = 2400014 Or CategoryId = 2400017 Or CategoryId = 3409341 Or CategoryId = 3409345 Or CategoryId = 3409347 Or CategoryId = 3409349 Or CategoryId = 2400022 Or CategoryId = 3409351 Then
                sql4c &= "where charindex('" & practiceName & "',LTRIM(RTRIM(P8.Item_Value)))!= 0 "
            Else
                sql4c &= "where charindex('" & practiceName & "',LTRIM(RTRIM(P2.Item_Value)))!= 0 "
            End If
            sql4c &= "AND A.Show = 1 "
            sql4c &= "and A.post_date < getdate() "
            sql4c &= "and A.pull_date > getdate() "
            sql4c &= "ORDER BY A.Post_Date desc "
            Dim AwardDT As DataTable
            AwardDT = New DataTable("AwardDT")
            AwardDT = mmfunctions.GetDataTable(sql4c)


            Dim PublicationsDT As New DataTable("PublicationsDT")
            PublicationsDT = CMSFunctions.GetNews(1, CategoryId, practiceName, 4)
            Dim PressDT As New DataTable("PressDT")
            PressDT = CMSFunctions.GetNews(8, CategoryId, practiceName, 4)
            Dim WhitePapersDT As New DataTable("WhitePapersDT")
            WhitePapersDT = CMSFunctions.GetNews(10, CategoryId, practiceName, 4)

            Dim SpeakingEngagementsql As String = "Select top 4 e.Headline, V2.Item_Value as EventTitle, V3.Item_Value as VideoLink, e.Content, DATENAME(mm,e.Event_Date) + ' ' + DATENAME(day,e.Event_Date) + ', ' + DATENAME(year, e.Event_Date) Event_Date, l.Item_Value Location, p.Item_Value Presenter, v.Item_Value Venue, V1.Item_Value as LinkU "
            SpeakingEngagementsql &= "From IPM_EVENTS e "
            SpeakingEngagementsql &= "Left join IPM_EVENTS_FIELD_VALUE l on (l.EVENT_ID = e.Event_Id and l.Item_ID = 3408650) "
            SpeakingEngagementsql &= "Left join IPM_EVENTS_FIELD_VALUE p on (p.EVENT_ID = e.Event_Id and p.Item_ID = 3408649) "
            SpeakingEngagementsql &= "Left join IPM_EVENTS_FIELD_VALUE v on (v.EVENT_ID = e.Event_Id and v.Item_ID = 3408651) "
            SpeakingEngagementsql &= "left join IPM_EVENTS_FIELD_VALUE V1 on e.Event_Id = V1.EVENT_ID and V1.Item_ID = (Select Item_ID From IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_LINK') "
            SpeakingEngagementsql &= "left join IPM_EVENTS_FIELD_VALUE V2 on e.Event_Id = V2.EVENT_ID and V2.Item_ID = (Select Item_ID From IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_EVENTTITLE') "
            SpeakingEngagementsql &= "left join IPM_EVENTS_FIELD_VALUE V3 on e.Event_Id = V3.EVENT_ID and V3.Item_ID = (Select Item_ID From IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_EVENTVIDEO') "
            SpeakingEngagementsql &= "left join IPM_EVENTS_FIELD_VALUE N1 on e.Event_Id = N1.EVENT_ID and N1.Item_ID = (Select Item_ID From IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_NEWSPROJCAT') "
            SpeakingEngagementsql &= "Where(e.Type = 5 And e.Post_Date < GETDATE() And e.Pull_Date > GETDATE() And e.Show = 1) and charindex('" & CategoryId & "',LTRIM(RTRIM(N1.Item_Value)))!= 0 "
            SpeakingEngagementsql &= "Order By e.Event_Date ASC "
            Dim SpeakingEngagementDT As New DataTable("SpeakingEngagementDT")
            SpeakingEngagementDT = mmfunctions.GetDataTable(SpeakingEngagementsql)

            Dim out4 As String = ""
            'Testimonials
            If TestimonialDT.Rows.Count > 0 Then
                out4 &= "<dl class=""contacts_list"">"
                out4 &= "<dt>Testimonial</dt>"
                Dim TestiFriendlyLink As String = ""
                TestiFriendlyLink = "testimonials" & "#testimonial_" & TestimonialDT.Rows(0)("UserID").ToString.Trim & "_" & formaturl.friendlyurl(TestimonialDT.Rows(0)("FullName").ToString.Trim.ToLower)
                out4 &= "<dd>"
                out4 &= "<p><strong>"
                out4 &= "<a href=""" & TestiFriendlyLink & """ title="""" >" & formatfunctionssimple.AutoFormatText(formaturl.getwords(TestimonialDT.Rows(0)("Testimonial").ToString.Trim, 35)) & "&#8230;" & "</a>"
                out4 &= "</strong></p>"
                out4 &= "</dd>"
                out4 &= "</dl> "
            End If
            'AWARDS
            If AwardDT.Rows.Count > 0 Then
                out4 &= "<dl class=""contacts_list"">"
                out4 &= "<dt>AWARDS</dt><div class=""projAwards"">"
                For aw As Integer = 0 To AwardDT.Rows.Count - 1
                    Dim AwardFriendlyLink As String = ""
                    AwardFriendlyLink = "honor_awards_" & AwardDT.Rows(aw)("Awards_Id").ToString.Trim & "_" & formaturl.friendlyurl(AwardDT.Rows(aw)("Headline").ToString.Trim)
                    out4 &= "<dd>"
                    out4 &= "<em class=""awardCat"">" & formatfunctionssimple.AutoFormatText(AwardDT.Rows(aw)("Headline").ToString.Trim) & ": " & "</em>"
                    out4 &= "<span class=""awardName"">" & formatfunctions.AutoFormatText(AwardDT.Rows(aw)("PublicationTitle").ToString.Trim) & " (" & AwardDT.Rows(aw)("AwardYear").ToString.Trim & ")" & "</span>"
                    out4 &= "</dd>"
                Next
                out4 &= "</div></dl> "
            End If


            If PressDT.Rows.Count > 0 Or PublicationsDT.Rows.Count > 0 Or WhitePapersDT.Rows.Count > 0 Or SpeakingEngagementDT.Rows.Count > 0 Then
                out4 &= "<dl class=""related_topics"">"
                out4 &= "<dt>Related Topics</dt>"

                If PressDT.Rows.Count > 0 Then
                    out4 &= "<p>Press Releases</p>"
                    For i As Integer = 0 To PressDT.Rows.Count - 1
                        out4 &= "<dd>"
                        out4 &= "<p><strong>"
                        If PressDT.Rows(i)("PDF").ToString.Trim = "1" Then
                            out4 &= "<a target=""_blank"" href=""/dynamic/document/week/news/download/" & PressDT.Rows(i)("News_ID").ToString.Trim & "/" & PressDT.Rows(i)("News_ID").ToString.Trim & ".pdf"">"
                        ElseIf PressDT.Rows(i)("PDF").ToString.Trim = "0" And PressDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                            out4 &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(PressDT.Rows(i)("WebUrl").ToString.Trim) & """ >"
                        End If
                        out4 &= formatfunctions.AutoFormatText(PressDT.Rows(i)("Headline").ToString.Trim)
                        If PressDT.Rows(i)("PDF").ToString.Trim = "1" Or PressDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                            out4 &= "</a>"
                        End If

                        If PressDT.Rows(i)("VideoLink").ToString.Trim <> "" Then
                            out4 &= "<a href=""" & formatfunctionssimple.AutoFormatText(PressDT.Rows(i)("VideoLink").ToString.Trim) & """ class=""video vimeo"">View video</a>"
                        End If

                        out4 &= "</strong></p>"
                        out4 &= "<p><strong></strong></p>"
                        out4 &= "</dd>"
                    Next
                End If

                If PublicationsDT.Rows.Count > 0 Then

                    out4 &= "<p>Publications</p>"
                    For i As Integer = 0 To PublicationsDT.Rows.Count - 1
                        out4 &= "<dd>"
                        out4 &= "<p><strong>"
                        If PublicationsDT.Rows(i)("PDF").ToString.Trim = "1" Then
                            out4 &= "<a target=""_blank"" href=""/dynamic/document/week/news/download/" & PublicationsDT.Rows(i)("News_ID").ToString.Trim & "/" & PublicationsDT.Rows(i)("News_ID").ToString.Trim & ".pdf"">"
                        ElseIf PublicationsDT.Rows(i)("PDF").ToString.Trim = "0" And PublicationsDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                            out4 &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(PublicationsDT.Rows(i)("WebUrl").ToString.Trim) & """ >"
                        End If
                        out4 &= formatfunctions.AutoFormatText(PublicationsDT.Rows(i)("Headline").ToString.Trim)
                        If PublicationsDT.Rows(i)("PDF").ToString.Trim = "1" Or PublicationsDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                            out4 &= "</a>"
                        End If

                        If PublicationsDT.Rows(i)("VideoLink").ToString.Trim <> "" Then
                            out4 &= "<a href=""" & formatfunctionssimple.AutoFormatText(PublicationsDT.Rows(i)("VideoLink").ToString.Trim) & """ class=""video vimeo"">View video</a>"
                        End If

                        out4 &= "</strong></p>"
                        out4 &= "<p><strong></strong></p>"
                        out4 &= "</dd>"
                    Next

                End If

                If SpeakingEngagementDT.Rows.Count > 0 Then
                    out4 &= "<p>Speaking Engagements</p>"
                    For i As Integer = 0 To SpeakingEngagementDT.Rows.Count - 1
                        out4 &= "<dd>"
                        out4 &= "<p><strong>"
                        If SpeakingEngagementDT.Rows(i)("LinkU").ToString.Trim <> "" Then
                            out4 &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(SpeakingEngagementDT.Rows(i)("LinkU").ToString.Trim) & """ title="""" >"
                        End If
                        out4 &= formatfunctions.AutoFormatText(SpeakingEngagementDT.Rows(i)("Headline").ToString.Trim)

                        If SpeakingEngagementDT.Rows(i)("LinkU").ToString.Trim <> "" Then
                            out4 &= "</a>"
                        End If

                        If SpeakingEngagementDT.Rows(i)("VideoLink").ToString.Trim <> "" Then
                            out4 &= "<a href=""" & formatfunctionssimple.AutoFormatText(SpeakingEngagementDT.Rows(i)("VideoLink").ToString.Trim) & """ class=""video vimeo"">View video</a>"
                        End If
                        out4 &= "</strong></p>"
                        out4 &= "<p><strong></strong></p>"
                        out4 &= "</dd>"
                    Next
                End If

                If WhitePapersDT.Rows.Count > 0 Then
                    out4 &= "<p>White Papers</p>"
                    For i As Integer = 0 To WhitePapersDT.Rows.Count - 1
                        out4 &= "<dd>"

                        out4 &= "<p><strong>"
                        If WhitePapersDT.Rows(i)("PDF").ToString.Trim = "1" Then
                            out4 &= "<a target=""_blank"" href=""/dynamic/document/week/news/download/" & WhitePapersDT.Rows(i)("News_ID").ToString.Trim & "/" & WhitePapersDT.Rows(i)("News_ID").ToString.Trim & ".pdf"">"
                        ElseIf WhitePapersDT.Rows(i)("PDF").ToString.Trim = "0" And WhitePapersDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                            out4 &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(WhitePapersDT.Rows(i)("WebUrl").ToString.Trim) & """ >"
                        End If
                        out4 &= formatfunctions.AutoFormatText(WhitePapersDT.Rows(i)("Headline").ToString.Trim)
                        If WhitePapersDT.Rows(i)("PDF").ToString.Trim = "1" Or WhitePapersDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                            out4 &= "</a>"
                        End If
                        If WhitePapersDT.Rows(i)("VideoLink").ToString.Trim <> "" Then
                            out4 &= "<a href=""" & formatfunctionssimple.AutoFormatText(WhitePapersDT.Rows(i)("VideoLink").ToString.Trim) & """ class=""video vimeo"">View video</a>"
                        End If
                        out4 &= "</strong></p>"
                        out4 &= "<p><strong></strong></p>"
                        out4 &= "</dd>"
                    Next
                End If
                out4 &= "</dl> "
            End If


            rel_pages_list1.Text = out4
            big_projects_list1.Text = out2
            small_projects_list1.Text = out

            Dim ClFriendlyLink As String = ""
            ClFriendlyLink = "clients_" & CategoryId & "_" & formaturl.friendlyurl(practiceFrontText)
            If Not practiceName = "Sports and Exhibition" Then
                clients_list_link.Text = "<a href=""" & ClFriendlyLink & """ title="""" class=""view_more padTop""><span class=""cmsMultiLanguage cms cmsType_TextSingle cmsName_Client_List"">" & CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Client_List") & "</span></a>"
            End If


            Dim sql_brochure As String = ""
            sql_brochure &= "select ipm_asset.asset_id from ipm_asset join ipm_asset_field_value u1 on u1.asset_id = ipm_asset.asset_id and u1.item_id = 2400401 and ltrim(rtrim(isnull(u1.item_value, ''))) = '1' join ipm_asset_field_value u2 on u2.asset_id = ipm_asset.asset_id and u2.item_id = 3425853 where projectid = 3409169 and category_id = 3425190 and active = 1 and available = 'Y' and u2.item_value like '%" & CategoryId & "%'"

            Dim BrochuresDT = New DataTable("BrochuresDT")
            BrochuresDT = mmfunctions.GetDataTable(sql_brochure)

            If BrochuresDT.Rows.Count > 0 Then
                brochures_link.Text = "<a href=""brochures"" title="""" class=""view_more padTop""><span class=""cmsMultiLanguage cms cmsType_TextSingle cmsName_Brochures_List"">" & CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Brochures_List") & "</span></a>"
            End If
        End If

    End Sub

    Private Sub DisplayNoProject()

        'literalName.Text = "No Matching Projects Found"
        'literalName2.Text = "No Matching Project Found"
        'featuredImage.Visible = False
        'repeaterProjects.Visible = False
        'viewProjectText.Visible = False

    End Sub


    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub

End Class
