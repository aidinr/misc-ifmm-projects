﻿Imports System.Data

Partial Class how_we_work
    Inherits System.Web.UI.Page
    Public RightTestimonialDT As DataTable


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim out2 As String = ""
        Dim sql3 As String = ""
        sql3 &= "select top 1 "
        sql3 &= "U.UserID, "
        sql3 &= "U1.Item_Value As Testimonial, "
        sql3 &= "U2.Item_Value As Link, "
        sql3 &= "U3.Item_Value as RelId, "
        sql3 &= "U4.Item_Value as SubTestimonial "
        sql3 &= "from IPM_USER U "
        sql3 &= "left join IPM_USER_FIELD_VALUE U1 on U.UserID = U1.USER_ID and U1.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTIMONIAL') "
        sql3 &= "left join IPM_USER_FIELD_VALUE U2 on U.UserID = U2.USER_ID and U2.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USERLINK') "
        sql3 &= "left join IPM_USER_FIELD_VALUE U3 on U.UserID = U3.USER_ID and U3.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTIIDS') "
        sql3 &= "left join IPM_USER_FIELD_VALUE U4 on U.UserID = U4.USER_ID and U4.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTISUB') "
        sql3 &= "left join IPM_USER_FIELD_VALUE U5 on u.UserID = U5.USER_ID and U5.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_AFFILIATECHECK') "
        sql3 &= "where Active = 'Y' "
        sql3 &= "and u.Contact = 1 and U5.Item_Value is null "
        'sql3 &= "and charindex('" & FuturedAwardsDT.Rows(0)("Awards_Id").ToString.Trim & "',U3.Item_Value)!= 0 "
        sql3 &= "ORDER BY NEWID() "
        RightTestimonialDT = New DataTable("RightTestimonialDT")
        RightTestimonialDT = mmfunctions.GetDataTable(sql3)

        out2 &= "<div class=""text"">"
        out2 &= "<p class=""red_text_special"">""" & RightTestimonialDT.Rows(0)("Testimonial").ToString.Trim & """</p>"
        out2 &= "</div>"
        out2 &= "<a href=""" & RightTestimonialDT.Rows(0)("Link").ToString.Trim & """ class=""featured_consult"" title="""">"
        out2 &= "<span>" & RightTestimonialDT.Rows(0)("SubTestimonial").ToString.Trim & "</span>"
        out2 &= "<b>Read More</b>"
        out2 &= "<img src=""/dynamic/image/week/contact/best/220x145/92/ffffff/Center/" & RightTestimonialDT.Rows(0)("UserID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
        out2 &= "</a>"


        right_testimonial.Text = out2

    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub

End Class
