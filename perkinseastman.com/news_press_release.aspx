﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="news_press_release.aspx.vb" Inherits="news_press_release" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first">NEWS</a><span> | </span></li>
                        <li><a href="<%=Master.UrlPage%>" class="active">Press Releases </a></li>
                    </ul>
                </div>

                <div class="two_columns">
                    <div class="left_smaller">
                        <h3 class="sub_title">News</h3>

                        <ul class="leadership_sorting_list">
                            <li><a href="news_press_release" title="" class="active">Press Releases</a></li>
                            <li><a href="news_current_press" title="">Current Press</a></li>
                            
                        </ul>

                    </div>
                    
                    <div class="right_bigger mt37">

                        <div class="headline">
                            <h2 class="smaller_font">PRESS RELEASES</h2>                       
                        </div>                 
                        
                        <ul class="white_papers_list" id="news_press_release_list">
                           <asp:Literal runat="server" ID="news_press_release_list1"/>                                          
                        </ul>    
                                                                     
                        <div class="testimonial_popups">
                         <asp:Literal runat="server" ID="news_press_release_popups1"/>                   
                        </div>

                    </div>
                </div>
</asp:Content>
