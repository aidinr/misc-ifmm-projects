﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="our_commitment.aspx.vb" Inherits="our_commitment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
<ul>

                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_Work"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Work")%></a><span> | </span></li>
                        <li><a class="cmsMultiLanguage cms cmsType_TextSingle cmsName_By_Region"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "By_Region")%></a><span> | </span></li>
                        <li><a href="region_China"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "China") %></a><span> | </span></li>
                        <li><a href="our_commitment" class="active"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "our_commitment") %></a></li>


</ul>
</div>
<h3 class="sub_title cmsMultiLanguage cms cmsType_TextSingle cmsName_our_commitment"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "our_commitment") %></h3>
<div class="left_bigger_right_smaller_layout">
<div class="left">
<img src="cms/data/Sized/liquid/690x/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "China_Image")%>" class="cms cmsType_Image cmsName_China_Image"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "China_Image")%>" />
<div class="text bigger_space_bettween_p">
<div class="cms cmsMultiLanguage cmsType_Rich cmsName_China_Intro">
<%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "China_Intro")%>
</div>
</div>
</div>
<div class="right no_border">
<div class="cms cmsMultiLanguage cmsType_Rich cmsName_China_Right">
<%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "China_Right")%>
</div>
<a href="/region_China" class="view_more padTop cmsMultiLanguage cms cmsType_TextSingle cmsName_View_Chinese_Projects"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "View_Chinese_Projects")%></a>



</div>
</div>

</asp:Content>

