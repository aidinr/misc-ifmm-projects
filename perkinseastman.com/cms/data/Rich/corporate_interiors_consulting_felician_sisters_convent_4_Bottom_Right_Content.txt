<ul>
<li>Over 95% of building occupants felt that the renovation supported the organization&rsquo;s mission.</li>
<li>80% of the residents felt their core values were influenced, with the renovation making them more eco-conscious.</li>
<li>Over 85% of residents reported a better sense of community due to the reconfiguration of the building.</li>
</ul>
<br /><br />