<ul>
<li>80% of residents leave campus multiple times per week to visit services/amenities in the neighborhood, even when similar resources are available on-site. Connections to the greater community are very important and redundant programming may be unnecessary.</li>
<li>Even though ecological sustainability is of interest to 85% of residents, only 44% would have paid a 5% higher entry fee for a green building.</li>
<li>Approximately 10% of shared common area is currently underused, primarily because of the spaces&rsquo; open configuration and lack of connection to the commons &ldquo;hub.&rdquo;</li>
<li>Kendal residents are very fond of private outdoor spaces (e.g. patios/balconies and enclosed three-season rooms). However, outdoor access is impeded by accessibility issues (e.g. heavy doors and high thresholds), a lack of visual privacy, and/or not enough sun shading.</li>
</ul>
<br /><br />