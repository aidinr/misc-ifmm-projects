{[English]}<strong>Perkins Eastman and its in-house affiliates offer the following services:</strong> <br />
<ul style="list-style-type: square;">
<li>Planning</li>
<li>Economic and Feasibility Analysis</li>
<li>Programming</li>
<li>Architecture</li>
<li>Interior Design</li>
<li>Landscape Architecture</li>
<li>Graphic Design</li>
<li>Urban Design</li>
<li>Supplemental services related to the firm&rsquo;s 15 practice area specialties</li>
</ul>{[/English]}{[Arabic]}<strong>تقوم Perkins Eastman والشركات التابعة لها بتقديم الخدمات التالية :<br /></strong><br />
<ul style="list-style-type: square;">
<li>التخطيط</li>
<li>التحليل الاقتصادي وتحليل الجدوى</li>
<li>البرمجة</li>
<li>العمارة</li>
<li>التصميم الداخلي</li>
<li>هندسة المناظر الطبيعية</li>
<li>التصميم الغرافيكي</li>
<li>التصميم الحضري</li>
<li>الخدمات التكميلية المرتبطة بالمجالات التخصصية الـ 15 للشركة</li>
</ul>{[/Arabic]}{[Mandarin]}<strong>Perkins Eastman附属公司可提供以下服务：</strong><br />
<ul style="list-style-type: square;">
<li>计划</li>
<li>经济和可行性分析</li>
<li>规划</li>
<li>建筑</li>
<li>室内设计</li>
<li>景观设计</li>
<li>平面设计</li>
<li>城市规划设计</li>
<li>与公司十五个业务领域相关的补充性服务</li>
</ul>{[/Mandarin]}