To reposition a former single user corporate headquarters to achieve the top in the market and service a financial-based multi-tenant occupancy, we first needed to address the building&rsquo;s existing exterior site plan and main entry that had several design issues:<br /><br />
<ul style="list-style-type: square;">
<li>Its orientated to the west away from the downtown retail and business activity that isolated the property</li>
<li>The dark bronze glass and window mullions at the main entrance that were almost residential in nature, and the building&rsquo;s overall lack of street presence</li>
<li>The small scale lobby would not well serve a multi tenancy and need for supporting amenity space</li>
<li>The buildings were visually disconnected at the main entrance and needed a way to leverage their curved shapes and architecturally &ldquo;join&rdquo; them</li>
</ul>