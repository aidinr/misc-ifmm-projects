<ul>
<li>The creation of a new Town Center provides a heart to the campus, creating a focal point for the community.</li>
<li>Creating a large wellness space in the Town Center has increased resident participation: Additional classes and extended programming are now offered to meet the demand. Residents are beginning to feel the benefits of a healthier lifestyle.</li>
<li>The renovated game room is an active and engaging place to be, enabling multiple generations to come together. Residents and their visitors converge for various activities, from playing shuffle board to computer and board games.</li>
<li>The fine dining room addition has become a &ldquo;joyful&rdquo; place, bringing the entire Laclede Groves community together to celebrate special events.</li>
<li>Upgrading, expanding, and repositioning the campus&rsquo; common spaces attracts the next generation of residents.</li>
</ul>
<br /><br />