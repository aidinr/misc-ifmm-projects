<ul>
<li>Overall employee satisfaction rates have reached 74%.</li>
<li>Satisfaction with the design and availability of conference space is up from 44% to 68%.</li>
<li>84% of staff agree or strongly agree that the informal and open collaborative meeting spaces are sufficient with regard to quantity, location, accessibility, and technology.</li>
<li>Since the renovation, there is 18% greater agreement that there is an advantage to sharing space with other co-workers.</li>
</ul>