Morocco

Al Maaden Golf Hotel**
Casa City Center
Hyatt Regency Port Lixus
Marrakech Retail/Mixed-Use Development
Meknes Master Plan
Oulad Yahia Master Plan
Park Hyatt Marrakech Al Maaden
Port Lixus Master Plan
Port Lixus Golf Club House
Rabat Dar Essalam Master Plan 
Taghazout Master Plan 
Tangers Beach Resort Development

Nigeria

American Nigerian Institutes of Health and Centres of Excellence for Medicine and Research
EKO Financial Center
Falomo Mixed-Use Development
Fairmont Hotel at Harbor Point
Lagos Island Mixed Use Development

Oman

Kalbooh Bay Mixed-Use Development

Qatar

Al Doha Grand Park
Al Jazeera Arabic Channel Extension
Khalifa Sports City Tower**
Kamal Mixed-Use Development
Lekhwiya Sport Stadium 

Saudi Arabia

Advanced Learning School, Riyadh
American International School, Riyadh
Aramco Abqaiq***
Aramco Ras Tanura***
Aramco Vdhiliyah***
Jeddah Beach Residence**
The World Academy

Senegal

International School of Dakar Master Plan, Dakar, Senegal

