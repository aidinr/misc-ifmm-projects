Carter’s, Inc.
Chevron Texaco
Consumers Union
Dannon Company, Inc.
Fort James Corporation
FUJIFILM, Inc.
General Motors Corporation
Health Market, Inc.
IBM
Malibu-Kahlua International
Pacific Partners Aviation
Pepperidge Farm, Inc.
Pepsi Bottling Group
Pernod Ricard USA
Pfizer
Victorinox/Swiss Army Brands
Unilever Home and Personal Care
Xerox Corporation

Media, Technology

ABC
A&E Television Networks
BBC
CBS
Charter Communications
IMG Worldwide, Inc.
NBC
Newscorp/Fox
Peppers & Rogers Group
Prodigy
Sun Microsystems
Televerse Studios
The Thomson Corporation
Time Inc.
Velocity Sports & Entertainment, LLC

Real Estate, Commercial

Albert B. Ashforth, Inc.
Antares
Apollo Real Estate Advisors
Benenson Capital Partners, LLC
Building & Land Technology
CB Richard Ellis
Cushman & Wakefield
Equitable Real Estate Investment Management
First New York Realty Co., Inc.
Forest City Ratner Companies
Fusco Corporation
Gilbane Building Company
Ginsburg Development LLC
Harbor Companies
Heyman Properties
Hines
Holmes Smith Developments
Jonathan Rose Companies
Jones Lang LaSalle