LaSalle Partners, Inc.
Malkin Properties
Midtown Equities
Muss Development Company
O’Neill Properties Group
Procida Realty Management Group
Reckson
RFR Realty, LLC
Rockefeller Group Development Corporation
Corporation
SL Green
Soffer Organization
Solow Management Corp.
The Spinnaker Companies
TDC Development & Construction Corporation
Thor Equities
Transwestern Investment Company
The Witkoff Group


Legal

Berwin Leighton Paisner, LLP
Cummings & Lockwood, LLC
Davidoff, Citron & Hutcher, LLP
Day Pitney, LLP
Delbello, Donnellan, Weingarten, Wise & Wiederkehr, LLP
Diserio Martin O’Connor & Castiglioni, LLP
Faust, Roy, Mangold & Fuchs, LLP
Fowler, White, Boggs
Goldschmidt & Genovese, LLP
Keane & Beane, PC
Lester Schwab Katz and Dwyer, LLP
Liddle & Robinson
Morgan, Lewis & Bockius, LLP
New York County District Attorney’s Office
Olshan Grundman Frome Rosenzweig & Wolosky, LLP
Pryor Cashman, LLP
Shipman & Goodwin, LLP
Sills, Cummis & Gross, PC
Weil, Gotshal & Manges, LLP

Retail

Italica
Juan Eljuri
Juicy Couture
Kate Spade
Liz Claiborne
Plaza Vendome
TKTS
