history.navigationMode = 'compatible';
$('*').unbind();

$(window).unload(function() {
 $('*').unbind();
 $('body','html').val('');}
);

var v = new Date().getTime();
var max = 0;
var na = 'Not Available';
$(document).ready(function() {

 $('#editForm').submit(function() {
  $('.wrapper').fadeOut(300);
  $('body .wrapper').css('background','#fff').fadeOut(50);
  $('body').css('background','#fff url(loading.gif) no-repeat fixed 50% 50%');
  return true;
 });

 $('h1.heading').html('Edit &ldquo;' + niceText(getParameterByName('name')) + '&rdquo;');
 if (getParameterByName('type')=='TextSingle') {
  if (getParameterByName('cmsMultiLanguage')) {
   $.get('/cms/data/Text/' + getParameterByName('name') + '.txt?v='+v, function(data) {
    $('#fields').html('<label>'+ niceText(getParameterByName('name')) +':</label>'
    + '<input class="master" type="text" name="Text_'+ getParameterByName('name') +'" value="'+ htmlEncode(data) +'" autocomplete="off" />'
    + '<h3>English</h3><input data-language="English" type="text" value="'+ htmlEncode(getStuff(data,'English')) +'" autocomplete="off" />'
    + '<h3>Arabic</h3><input data-language="Arabic" type="text" value="'+ htmlEncode(getStuff(data,'Arabic')) +'" autocomplete="off" />'
    + '<h3>Mandarin</h3><input data-language="Mandarin" type="text" value="'+ htmlEncode(getStuff(data,'Mandarin')) +'" autocomplete="off" />'
    + '<input type="hidden" name="cmsType" value="text" />');
    setTimeout("parent.cmsModalShow()",500);
    $('input[data-language]').change(function() {
     $('input.master').val('{[English]}'+ $('input[data-language="English"]').val() +'{[/English]}{[Arabic]}'+ $('input[data-language="Arabic"]').val() +'{[/Arabic]}{[Mandarin]}'+ $('input[data-language="Mandarin"]').val() +'{[/Mandarin]}');
    });
   }).error(function() {
    $('#fields').html('<label>'+ niceText(getParameterByName('name')) +':</label>'
    + '<input class="master" type="text" name="Text_'+ getParameterByName('name') +'" value="" autocomplete="off" />'
    + '<h3>English</h3><input data-language="English" type="text" value="" autocomplete="off" />'
    + '<h3>Arabic</h3><input data-language="Arabic" type="text" value="" autocomplete="off" />'
    + '<h3Mandarin</h3><input data-language="Mandarin" type="text" value="" autocomplete="off" />'
    + '<input type="hidden" name="cmsType" value="text" />');
    setTimeout("parent.cmsModalShow()",500);
    $('input[data-language]').change(function() {
     $('input.master').val('{[English]}'+ $('input[data-language="English"]').val() +'{[/English]}{[Arabic]}'+ $('input[data-language="Arabic"]').val() +'{[/Arabic]}{[Mandarin]}'+ $('input[data-language="Mandarin"]').val() +'{[/Mandarin]}');
    });
   });
  }
  else {
   $.get('/cms/data/Text/' + getParameterByName('name') + '.txt?v='+v, function(data) {
    $('#fields').html('<label>'+ niceText(getParameterByName('name')) +':</label><input type="text" name="Text_'+ getParameterByName('name') +'" value="'+ htmlEncode(data) +'" autocomplete="off" /><input type="hidden" name="cmsType" value="text" />');
    setTimeout("parent.cmsModalShow()",500);
   }).error(function() {
    $('#fields').html('<label>'+ niceText(getParameterByName('name')) +':</label><input type="text" name="Text_'+ getParameterByName('name') +'" value="'+ htmlEncode(na) +'" autocomplete="off" /><input type="hidden" name="cmsType" value="text" />');
    setTimeout("parent.cmsModalShow()",500);
   });
  }
 }
 else if (getParameterByName('type')=='TextMulti') {
  if(getParameterByName('cmsMultiLanguage')) {
  $.get('/cms/data/Text/' + getParameterByName('name') + '.txt?v='+v, function(data) {
   $('#fields').html('<label>'+ niceText(getParameterByName('name')) +':</label>'
    + '<textarea class="master" rows="99" cols="99" name="Text_'+ getParameterByName('name') +'">'+ htmlEncode(data) +'</textarea>' 
    + '<h3>English</h3><textarea rows="99" cols="99" data-language="English">'+ htmlEncode(getStuff(data,'English')) +'</textarea>'
    + '<h3>Arabic</h3><textarea rows="99" cols="99" data-language="Arabic">'+ htmlEncode(getStuff(data,'Arabic')) +'</textarea>'
    + '<h3>Mandarin</h3><textarea rows="99" cols="99" data-language="Mandarin">'+ htmlEncode(getStuff(data,'Mandarin')) +'</textarea>'
    + '<input type="hidden" name="cmsType" value="text" />');
   setTimeout("parent.cmsModalShow()",800);
   $('textarea[data-language]').change(function() {
    $('textarea.master').val('{[English]}'+ $('textarea[data-language="English"]').val() +'{[/English]}{[Arabic]}'+ $('textarea[data-language="Arabic"]').val() +'{[/Arabic]}{[Mandarin]}'+ $('textarea[data-language="Mandarin"]').val() +'{[/Mandarin]}');
   });
  }).error(function() {
   $('#fields').html('<label>'+ niceText(getParameterByName('name')) +':</label>'
    + '<textarea class="master" rows="99" cols="99" name="Text_'+ getParameterByName('name') +'">'+ htmlEncode(data) +'</textarea>' 
    + '<h3>English</h3><textarea rows="99" cols="99" data-language="English"></textarea>'
    + '<h3>Arabic</h3><textarea rows="99" cols="99" data-language="Arabic"></textarea>'
    + '<h3>Mandarin</h3><textarea rows="99" cols="99" data-language="Mandarin"></textarea>' 
    + '<input type="hidden" name="cmsType" value="text" />');
   setTimeout("parent.cmsModalShow()",800);
   $('textarea[data-language]').change(function() {
    $('textarea.master').val('{[English]}'+ $('textarea[data-language="English"]').val() +'{[/English]}{[Arabic]}'+ $('textarea[data-language="Arabic"]').val() +'{[/Arabic]}{[Mandarin]}'+ $('textarea[data-language="Mandarin"]').val() +'{[/Mandarin]}');
   });
  });
 }
 else {
  $.get('/cms/data/Text/' + getParameterByName('name') + '.txt?v='+v, function(data) {
   $('#fields').html('<label>'+ niceText(getParameterByName('name')) +':</label><textarea rows="99" cols="99" name="Text_'+ getParameterByName('name') +'">'+ htmlEncode(data)  +'</textarea><input type="hidden" name="cmsType" value="text" />');
   setTimeout("parent.cmsModalShow()",500);
  }).error(function() {
   $('#fields').html('<label>'+ niceText(getParameterByName('name')) +':</label><textarea rows="99" cols="99" name="Text_'+ getParameterByName('name') +'">'+ htmlEncode(na)  +'</textarea><input type="hidden" name="cmsType" value="text" />');
   setTimeout("parent.cmsModalShow()",500);
    });
 }
 }
 else if (getParameterByName('type')=='Rich') {
 if(getParameterByName('cmsMultiLanguage')) {
  $.get('/cms/data/Rich/' + getParameterByName('name') + '.txt?v='+v, function(data) {

   $('#fields').html('<label>'+ niceText(getParameterByName('name')) +':</label>'
    + '<textarea class="master" rows="99" cols="99" name="Rich_'+ getParameterByName('name') +'">'+ htmlEncode(data)  +'</textarea>'
    + '<h3>English</h3><textarea data-language="English" class="cmsRich" rows="99" cols="99">'+ htmlEncode(getStuff(data,'English'))  +'</textarea>'
    + '<h3>Arabic</h3><textarea data-language="Arabic" class="cmsRich" rows="99" cols="99">'+ htmlEncode(getStuff(data,'Arabic'))  +'</textarea>'
    + '<h3>Mandarin</h3><textarea data-language="Mandarin" class="cmsRich" rows="99" cols="99">'+ htmlEncode(getStuff(data,'Mandarin'))  +'</textarea>'
    + '<input type="hidden" name="cmsType" value="rich" />');
   var script = document.createElement('script');
   script.type = 'text/javascript';
   script.src = 'tiny_mce/init.js';
   $('head').append(script);
   tinyMCEinit();
   setTimeout("parent.cmsModalShow()",800);
   $('form').submit(function() {
     $('textarea.master').val('{[English]}'+ $('textarea[data-language="English"]').val() +'{[/English]}{[Arabic]}'+ $('textarea[data-language="Arabic"]').val() +'{[/Arabic]}{[Mandarin]}'+ $('textarea[data-language="Mandarin"]').val() +'{[/Mandarin]}');
   });
  }).error(function() {
   $('#fields').html('<label>'+ niceText(getParameterByName('name')) +':</label>'
    + '<textarea class="master" rows="99" cols="99" name="Rich_'+ getParameterByName('name') +'"></textarea>'
    + '<h3>English</h3><textarea data-language="English" class="cmsRich" rows="99" cols="99"></textarea>'
    + '<h3>Arabic</h3><textarea data-language="Arabic" class="cmsRich" rows="99" cols="99"></textarea>'
    + '<h3>Mandarin</h3><textarea data-language="Mandarin" class="cmsRich" rows="99" cols="99"></textarea>'
    + '<input type="hidden" name="cmsType" value="rich" />');
   var script = document.createElement('script');
   script.type = 'text/javascript';
   script.src = 'tiny_mce/init.js';
   $('head').append(script);
   tinyMCEinit();
   setTimeout("parent.cmsModalShow()",500);
   $('form').submit(function() {
     $('textarea.master').val('{[English]}'+ $('textarea[data-language="English"]').val() +'{[/English]}{[Arabic]}'+ $('textarea[data-language="Arabic"]').val() +'{[/Arabic]}{[Mandarin]}'+ $('textarea[data-language="Mandarin"]').val() +'{[/Mandarin]}');
   });
  });
 }
 else {
  $.get('/cms/data/Rich/' + getParameterByName('name') + '.txt?v='+v, function(data) {
   $('#fields').html('<label>'+ niceText(getParameterByName('name')) +':</label><textarea class="cmsRich" rows="99" cols="99" name="Rich_'+ getParameterByName('name') +'">'+ htmlEncode(data)  +'</textarea><input type="hidden" name="cmsType" value="rich" />');
   var script = document.createElement('script');
   script.type = 'text/javascript';
   script.src = 'tiny_mce/init.js';
   $('head').append(script);
   tinyMCEinit();
   setTimeout("parent.cmsModalShow()",500);
  }).error(function() {
   $('#fields').html('<label>'+ niceText(getParameterByName('name')) +':</label><textarea class="cmsRich"  rows="99" cols="99" name="Rich_'+ getParameterByName('name') +'">'+ htmlEncode(na)  +'</textarea><input type="hidden" name="cmsType" value="rich" />');
   var script = document.createElement('script');
   script.type = 'text/javascript';
   script.src = 'tiny_mce/init.js';
   $('head').append(script);
   tinyMCEinit();
   setTimeout("parent.cmsModalShow()",500);
   });
  }
 }
 else if (getParameterByName('type')=='Link') {
  $.get('/cms/data/Link/' + getParameterByName('name') + '.txt?v='+v, function(data) {
   var href = '';
   if(data.split('|-|')[0]) href = data.split('|-|')[0];
   var linktext = niceText(getParameterByName('name'));
   if(data.split('|-|')[1]) linktext = data.split('|-|')[1];
   $('#fields').html( '<label>Link Text:</label><span><input type="text" name="linktext" autocomplete="off" value="'+ linktext +'"/></span><label>Link URL:</label><span><input type="text" name="href" id="href" autocomplete="off" value="' + href + '"/></span><input type="hidden" name="cmsType" value="link" /><input type="hidden" name="linkitem" value="' + getParameterByName('name') + '"/>');
   setTimeout("parent.cmsModalShow()",500);
  }).error(function() {
   var href = '';
   var linktext = niceText(getParameterByName('name'));
   $('#fields').html( '<label>Link Text:</label><span><input type="text" name="linktext" autocomplete="off" value="'+ linktext +'"/></span><label>Link URL:</label><span><input type="text" name="href" id="href" autocomplete="off" value="' + href + '"/></span><input type="hidden" name="cmsType" value="link" /><input type="hidden" name="linkitem" value="' + getParameterByName('name') + '"/>');
   setTimeout("parent.cmsModalShow()",500);
  });
 } else if (getParameterByName('type')=='File') {
  $.get('/cms/data/File/' + getParameterByName('name') + '.txt?v='+v, function(data) {
   var filename = getParameterByName('name');
   if(data.split('|-|')[1]) filename = data.split('|-|')[1].substring(1+data.split('|-|')[1].lastIndexOf("/")).split('.')[0];
   $('#fields').html( '<label>Link Text:</label><span><input type="text" name="linktext" autocomplete="off" value="'+ niceText(data.split('|-|')[0]) +'"/></span><label>Filename: (Without Extension)</label><span><input type="text" name="filename" id="documentFilename" autocomplete="off" value="Download"/></span><label>'+ niceText(getParameterByName('name')) +':</label><span><input type="file" name="file"/></span><input type="hidden" name="cmsType" value="file" /><input type="hidden" name="fileitem" value="' + getParameterByName('name') + '"/><span class="checkboxSpan"><input type="checkbox" class="checkbox" value="1" name="delete"/>&emsp;<label>Delete File</label></span>');
   setTimeout("parent.cmsModalShow()",500);
  }).error(function() {
  $('#fields').html( '<label>Link Text:</label><span><input type="text" name="linktext" autocomplete="off" value="'+ niceText(getParameterByName('name')) +'"/></span><label>Filename: (Without Extension)</label><span><input type="text" name="filename" id="documentFilename" autocomplete="off" value="Download"/></span><label>'+ niceText(getParameterByName('name')) +':</label><span><input type="file" name="file"/></span><input type="hidden" name="cmsType" value="file" /><input type="hidden" name="fileitem" value="' + getParameterByName('name') + '"/><span class="checkboxSpan"><input value="1" type="checkbox" class="checkbox" name="delete"/>&emsp;<label>Delete File</label></span>');
   setTimeout("parent.cmsModalShow()",500);
  });
 }
 else if (getParameterByName('type')=='Image') {
  $.get('/cms/data/Image/' + getParameterByName('name') + '.txt?v='+v, function(data) {
   $('#fields').html( '<label>Image:</label><span><input type="file" name="file"/></span><label>Alternate Text:</label><span><input type="text" name="alt" value="'+ htmlEncode(data) +'" autocomplete="off" /></span><input type="hidden" name="width" value="'+ getParameterByName('width') +'" /><input type="hidden" name="height" value="' + getParameterByName('height') + '" /><input type="hidden" name="filename" value="image_' + getParameterByName('name') + '" /><input type="hidden" name="cmsType" value="image" /><span class="checkboxSpan"><input type="checkbox" class="checkbox" value="1" name="delete"/> &nbsp; <label>Delete Image</label></span>');
   setTimeout("parent.cmsModalShow()",500);
  }).error(function() {
   $('#fields').html( '<label>Image:</label><span><input type="file" name="file"/></span><label>Alternate Text:</label><span><input type="text" name="alt" autocomplete="off" value="" /></span><input type="hidden" name="width" value="'+ getParameterByName('width') +'" /><input type="hidden" name="height" value="' + getParameterByName('height') + '" /><input type="hidden" name="filename" value="image_' + getParameterByName('name') + '" /><input type="hidden" name="cmsType" value="image" /><span class="checkboxSpan"><input type="checkbox" class="checkbox" value="1" name="delete"/>&emsp;<label>Delete Image</label></span>');
   setTimeout("parent.cmsModalShow()",500);
    }); 
   setTimeout("parent.cmsModalShow()",500);
 }
 else if (getParameterByName('type')=='Metadata') {
  $('h1.heading').append(' Metadata');
  $('#fields').html('<div class="set"></div><input type="hidden" name="cmsType" value="metadata" /><input type="hidden" name="cmsPage" value="'+ getParameterByName('page') +'" />');
   $.get('/cms/data/Metadata/MetadataTitle_' + getParameterByName('page') + '.txt?v='+v, function(data) {
    $('#fields .set').append('<label>Page Title: </label><input type="text" name="metadataTitle" value="'+ htmlEncode(data) +'" autocomplete="off" />');
    $.get('/cms/data/Metadata/MetadataDescription_' + getParameterByName('page') + '.txt?v='+v, function(data) {
     $('#fields .set').append('<label>Page Description: </label><textarea rows="99" cols="99" name="metadataDescription">'+ htmlEncode(data)  +'</textarea>');
     $.get('/cms/data/Metadata/MetadataKeywords_' + getParameterByName('page') + '.txt?v='+v, function(data) {
      $('#fields .set').append('<label>Page Keywords: </label><textarea rows="99" cols="99" name="metadataKeywords">'+ htmlEncode(data)  +'</textarea>');
      setTimeout("parent.cmsModalShow()",500);
     });
    });
   }).error(function() {
    $('#fields .set').append('<label>Page Title: </label><input type="text" name="metadataTitle" value="'+ htmlEncode(na) +'" autocomplete="off" />');
    $('#fields .set').append('<label>Page Description: </label><textarea rows="99" cols="99" name="metadataDescription">'+ htmlEncode(na)  +'</textarea>');
    $('#fields .set').append('<label>Page Keywords: </label><textarea rows="99" cols="99" name="metadataKeywords">'+ htmlEncode(na)  +'</textarea>');
    setTimeout("parent.cmsModalShow()",500);
   });
 }

 else if (getParameterByName('type')=='Password') {
  $('h1.heading').html('Change Password');
  $('#fields').html('<div class="set"></div><input type="hidden" name="cmsType" value="password" />');
  $('#fields .set').append('<label>Username: </label><input type="input" name="username" id="username" value="" autocomplete="off" /><label>Current Password: </label><input type="password" name="passwordCurrent" value="" autocomplete="off" /><label>New Password: </label><input type="password" name="passwordNew" id="passwordNew" value="" autocomplete="off" /><label>New Password Again: </label><input type="password" id="passwordNew2" name="passwordNew2" value="" autocomplete="off" />');
  $('#editForm, #editForm *').unbind();
  $('#editForm').submit(function() {
   if($('#username').val()=='') {
    alert('You must provide your username.');
    return false;
   }
   if($('#passwordNew').val()=='') {
    alert('You must provide a new password.');
    return false;
   }
   else if($('#passwordNew').val()!=$('#passwordNew2').val()) {
    $('#passwordNew').val('');
    $('#passwordNew2').val('');
    alert('The new password fields must match.');
    return false;
   }
   else {
    $.post('/cms/update.aspx', $(this).serialize() , function(data) {
     alert(data);
     parent.cmsModalClose(1);
    });
    return false;
   }
  });
  setTimeout("parent.cmsModalShow()",500);
 }

 else if (getParameterByName('type')=='Textset') {
  $('p.actions').prepend('<input type="button" value="Add" id="add" /><input type="button" value="Alphabetize" id="alpha" /><input type="hidden" name="cmsType" value="textset" /><input type="hidden" name="cmsTextsetName" value="Textset_' + getParameterByName('name') + '" />');

  $.get('/cms/CountTextset.aspx?v=' + v + '&type=Textset&name=Text_' + getParameterByName('name'), function (data) {
   max = Math.ceil(data.replace(/[^\d.]/g, '')/2)-1;
   loadTextset_xa();
  });

  $('#add').click(function() {
  $('#fields').append('<p class="item"><a class="itemRemove"></a><a class="itemDown"></a><a class="itemUp"></a><input type="text" autocomplete="off" name="Text_'+ getParameterByName('name') +'_xa'+ $('body.editor form #fields .item').length +'" value="" /><textarea rows="99" cols="99" name="Text_'+ getParameterByName('name')+'_xb'+ $('body.editor form #fields .item').length +'"></textarea></p>');
   UpDownRemove ();
   return false;
  });
 }
 else if (getParameterByName('type')=='Linkset') {
  $('p.actions').prepend('<input type="button" value="Add" id="add" /><input type="hidden" name="cmsType" value="linkset" /><input type="hidden" name="cmsLinksetName" value="Linkset_' + getParameterByName('name') + '" />');
  $.get('/cms/CountTextset.aspx?v=' + v + '&type=Linkset&name=Link_' + getParameterByName('name'), function (data) {
   max = Math.ceil(data.replace(/[^\d.]/g, ''))-1;
   loadLinkset_xa();
  });

  $('#add').click(function() {
  $('#fields').append('<p class="item"><a class="itemRemove"></a><a class="itemDown"></a><a class="itemUp"></a><label>Link URL:</label><span><input type="text" autocomplete="off" name="Text_'+ getParameterByName('name') +'_xa'+ $('body.editor form #fields .item').length +'" value="" /></span><label>Link Text:</label><span><input type="text" autocomplete="off" name="Text_'+ getParameterByName('name') +'_xb'+ $('body.editor form #fields .item').length +'" value="" /></span><br><em style="float: none; clear: both; display: block;"></em><span class="checkboxSpan"> &nbsp; <input type="checkbox" class="checkbox" name="Text_'+ getParameterByName('name')+'_xc'+ $('body.editor form #fields .item').length +'" value="1" /><label>New Window?</label></span></p>');
   UpDownRemove ();
   return false;
  });
 }

 $('.boxClose, #cancel').click(function() {
  parent.cmsModalClose();
  return false;
 });

 $('input').attr('autocomplete','off');

 $('#documentFilename').keyup(function() {
  $(this).val( $(this).val().replace(/[^A-Za-z0-9_\-$-]/g, '').substr(0,180));

 });


});

function niceText (str) {
 str = str.replace(/_/g,' ');
 return str;
}

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function UpDownRemove () {

  $('#fields .item .itemUp').first().addClass('first');
  $('#fields .item .itemDown').last().addClass('last');
  $('#fields .item .itemDown.last').removeClass('last');
  $('#fields .item .itemDown').last().addClass('last');

  $('.itemUp').unbind();
  $('.itemUp').click(function() {
   var xi = $(this).index('.itemUp');
   var xa = $('body.editor form #fields .item:eq('+xi+')').find(':input:eq(0)').val();
   var xb = $('body.editor form #fields .item:eq('+xi+')').find(':input:eq(1)').val();
   var xc = $('body.editor form #fields .item:eq('+xi+')').find(':input:eq(2)').val();
   var xaAbove = $('body.editor form #fields .item:eq('+ (xi-1) + ')').find(':input:eq(0)').val();
   var xbAbove = $('body.editor form #fields .item:eq('+ (xi-1) + ')').find(':input:eq(1)').val();
   var xcAbove = $('body.editor form #fields .item:eq('+ (xi-1) + ')').find(':input:eq(2)').val();
   $('body.editor form #fields .item:eq('+xi+')').find(':input:eq(0)').val(xaAbove);
   $('body.editor form #fields .item:eq('+xi+')').find(':input:eq(1)').val(xbAbove);
   if(xcAbove) $('body.editor form #fields .item:eq('+xi+')').find(':input:eq(2)').val(xcAbove);
   $('body.editor form #fields .item:eq('+(xi-1)+')').find(':input:eq(0)').val(xa);
   $('body.editor form #fields .item:eq('+(xi-1)+')').find(':input:eq(1)').val(xb);
   if(xc) $('body.editor form #fields .item:eq('+(xi-1)+')').find(':input:eq(2)').val(xc);
   return false;
  });


  $('#alpha').unbind();
  $('#alpha').click(function() {
     $(".item").each(function() {
         sortDem($(this).prevAll(".item:eq(0)"), $(this))
     })
  });


  $('.itemDown').unbind();
  $('.itemDown').click(function() {
   var xi = $(this).index('.itemDown');
   var xa = $('body.editor form #fields .item:eq('+xi+')').find(':input:eq(0)').val();
   var xb = $('body.editor form #fields .item:eq('+xi+')').find(':input:eq(1)').val();
   var xc = $('body.editor form #fields .item:eq('+xi+')').find(':input:eq(2)').val();
   var xaBelow = $('body.editor form #fields .item:eq('+ (xi+1) + ')').find(':input:eq(0)').val();
   var xbBelow = $('body.editor form #fields .item:eq('+ (xi+1) + ')').find(':input:eq(1)').val();
   var xcBelow = $('body.editor form #fields .item:eq('+ (xi+1) + ')').find(':input:eq(2)').val();
   $('body.editor form #fields .item:eq('+xi+')').find(':input:eq(0)').val(xaBelow);
   $('body.editor form #fields .item:eq('+xi+')').find(':input:eq(1)').val(xbBelow);
   if(xcBelow) $('body.editor form #fields .item:eq('+xi+')').find(':input:eq(2)').val(xcBelow);
   $('body.editor form #fields .item:eq('+(xi+1)+')').find(':input:eq(0)').val(xa);
   $('body.editor form #fields .item:eq('+(xi+1)+')').find(':input:eq(1)').val(xb);
   if(xc) $('body.editor form #fields .item:eq('+(xi+1)+')').find(':input:eq(2)').val(xc);
   return false;
  });

  $('.itemRemove').unbind();
  $('.itemRemove').click(function() {
  $(this).parent().remove();

  var ii=0;
  $('body.editor form #fields .item').each(function() {
   $(this).find(':input').each(function() {
   $(this).attr('name', zapTrailingNumbers(String($(this).attr('name')).substr(0))+ii);
   });
   ii++;
  });

  UpDownRemove();
  return false;
 });
}

function loadTextset_xa (xi) {
 if(!xi) xi=0;
 $.get('/cms/data/Textset/' + getParameterByName('name') + '/'+ getParameterByName('name') +'_xa'+ xi +'.txt?v='+v, function(xa) {
  loadTextset_xb (xi, xa);
 }).error(function() {
  loadTextset_xb (xi, 'Not Available');
 });
}

function loadTextset_xb (xi, xa) {
 if(!xi) xi=0;
 $.get('/cms/data/Textset/' + getParameterByName('name') + '/'+ getParameterByName('name') +'_xb'+ xi +'.txt?v='+v, function(xb) {
  var d = '';
  if(xi>0)  d = '<a class="itemRemove"></a>';
  $('#fields').append('<p class="item">'+ d +'<a class="itemDown"></a><a class="itemUp"></a><input type="text" autocomplete="off" name="Text_'+ getParameterByName('name') +'_xa'+ $('body.editor form #fields .item').length +'" value="'+ htmlEncode(xa) +'" /><textarea rows="99" cols="99" name="Text_'+ getParameterByName('name')+'_xb'+ $('body.editor form #fields .item').length +'">'+ htmlEncode(xb) +'</textarea></p>');
  if(xi < max ) loadTextset_xa (xi+1);
  else {
   UpDownRemove();
   setTimeout("parent.cmsModalShow()",500);
  }
 }).error(function() {
  xb = 'Not Available';
  $('#fields').append('<p class="item"><a class="itemDown"></a><a class="itemUp"></a><input type="text" autocomplete="off" name="Text_'+ getParameterByName('name') +'_xa'+ $('body.editor form #fields .item').length +'" value="'+ htmlEncode(xa) +'" /><textarea rows="99" cols="99" name="Text_'+ getParameterByName('name')+'_xb'+ $('body.editor form #fields .item').length +'">'+ htmlEncode(xb) +'</textarea></p>');
  UpDownRemove();
  setTimeout("parent.cmsModalShow()",500);
 });
}


function loadLinkset_xa (xi) {
 if(!xi) xi=0;
 $.get('/cms/data/Linkset/' + getParameterByName('name') + '/'+ getParameterByName('name') +'_xa'+ xi +'.txt?v='+v, function(xa) {
  loadLinkset_xb (xi, xa);
 }).error(function() {
  loadLinkset_xb (xi, 'Not Available');
 });
}

function loadLinkset_xb (xi, xa) {
 if(!xi) xi=0;
 $.get('/cms/data/Linkset/' + getParameterByName('name') + '/'+ getParameterByName('name') +'_xb'+ xi +'.txt?v='+v, function(xb) {
  loadLinkset_xc (xi, xa, xb);
 }).error(function() {
  loadLinkset_xc (xi, 'Not Available', 'Not Available');
 });
}

function loadLinkset_xc (xi, xa, xb) {
 if(!xi) xi=0;
 $.get('/cms/data/Linkset/' + getParameterByName('name') + '/'+ getParameterByName('name') +'_xc'+ xi +'.txt?v='+v, function(xc) {
  var d = '';
  if(xi>0)  d = '<a class="itemRemove"></a>';
  var checked ='0';
  if($.trim((xc))=='1') checked=' checked="checked" ';
  $('#fields').append('<p class="item">'+ d +'<a class="itemDown"></a><a class="itemUp"></a><label>Link URL:</label><span><input type="text" autocomplete="off" name="Text_'+ getParameterByName('name') +'_xa'+ $('body.editor form #fields .item').length +'" value="'+ htmlEncode(xa) +'" /></span><label>Link Text:</label><span><input type="text" autocomplete="off" name="Text_'+ getParameterByName('name') +'_xb'+ $('body.editor form #fields .item').length +'" value="' +  htmlEncode(xb) + '" /></span><br><em style="float: none; clear: both; display: block;"></em><span class="checkboxSpan"> &nbsp; <input type="checkbox" class="checkbox" name="Text_'+ getParameterByName('name')+'_xc'+ $('body.editor form #fields .item').length +'" value="1" '+ checked +' /><label>New Window?</label></span></p>');
  if(xi < max ) loadLinkset_xa (xi+1);
  else {
   UpDownRemove();
   setTimeout("parent.cmsModalShow()",500);
  }
 }).error(function() {
  xc = '0';
  $('#fields').append('<p class="item"><label>Link URL:</label><span><input type="text" autocomplete="off" name="Text_'+ getParameterByName('name') +'_xa'+ $('body.editor form #fields .item').length +'" value="'+ htmlEncode(xa) +'" /></span><label>Link Text:</label><span><input type="text" autocomplete="off" name="Text_'+ getParameterByName('name') +'_xb'+ $('body.editor form #fields .item').length +'" value="' +  htmlEncode(xb) + '" /></span><br><em style="float: none; clear: both; display: block;"></em><span class="checkboxSpan"> &nbsp; <input type="checkbox" class="checkbox" name="Text_'+ getParameterByName('name')+'_xc'+ $('body.editor form #fields .item').length +'" value="'+ htmlEncode(xc) +'" /><label>New Window?</label></span></p>');
  UpDownRemove();
  setTimeout("parent.cmsModalShow()",500);
 });
}

function htmlEncode(str){
 if(str) return String(str).replace(/&/g, '&#38;').replace(/</g, '&#60;').replace(/>/g, '&#62;').replace(/"/g, '&#34;').replace(/'/g, '&#39;');
 else return '';
}

function zapTrailingNumbers(str) {
  return str.replace(/\d+$/,'');
}

function sortDem(prev, curr) {
    if(prev.length>0) {
        var pVal = $("textarea", prev).val();
        var cVal = $("textarea", curr).val();
        var pVal2 = $("input", prev).val();
        var cVal2 = $("input", curr).val();
        if(pVal.toLowerCase().replace('the ','').trim() > cVal.toLowerCase().replace('the ','').trim()) {

            $("textarea", curr).val( pVal );
            $("textarea", prev).val( cVal );
            $("input", curr).val( pVal2 );
            $("input", prev).val( cVal2 );

        }
    }
}

function getStuff(data, language) {
    if(data.indexOf('{[')<0) return data;
    var result = data.split('{['+language+']}')[1].split('{[/'+language+']}')[0];
    return result;
}
