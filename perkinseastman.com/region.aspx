﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="region.aspx.vb" Inherits="region" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_Work"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Work")%></a><span> | </span></li>
                        <li><a class="cmsMultiLanguage cms cmsType_TextSingle cmsName_By_Region"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "By_Region")%></a><span> | </span></li>
                        <li><a href="<%=Master.UrlPage%>" class="active"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", new Regex("[^a-zA-Z0-9]").Replace(regionName, "_")) %></a></li>
                    </ul>
                </div>

                <div class="two_columns">
                    <div class="left_smaller">
                        <h3 class="sub_title cmsMultiLanguage cms cmsType_TextSingle cmsName_<%= new Regex("[^a-zA-Z0-9]").Replace(regionName, "_") %>"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", new Regex("[^a-zA-Z0-9]").Replace(regionName, "_")) %></h3>
                         <%If regionName = "Africa and the Middle East" Then%>
                        <img src="assets/images/maps/ame.jpg" alt=""/>
                        <%ElseIf regionName = "The Americas" Then%>
                         <img src="assets/images/maps/americas.jpg" alt=""/>
                         <%ElseIf regionName = "Asia" Then%>
                          <img src="assets/images/maps/asia.jpg" alt=""/>
                          <%ElseIf regionName = "China" Then%>
                           <img src="assets/images/maps/china.jpg" alt=""/>
                           <%ElseIf regionName = "Europe" Then%>
                            <img src="assets/images/maps/europe.jpg" alt=""/>
                            <%End If%>
                        <br/>
                        <br/>
                        
                        <%
                            Dim CssClass As String = ""
                            Dim CssClass2 As String = ""
                            Dim PageName As String = ""
                            If regionName <> "" Then
                                PageName = regionName
                                PageName = Replace(PageName, " ", "_")
                                PageName = Replace(PageName, "-", "_")
                                PageName = "Region_" & PageName
                                CssClass = "cmsMultiLanguage cms learnIntro cmsType_Rich cmsName_" & PageName
                                CssClass2 = "cmsMultiLanguage cms learnMore cmsType_Rich cmsName_" & PageName & "_More"

                            Else
                                CssClass = "Text"
                            End If
                         %>
                        <div class="<%=CssClass%>">
                            <%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", PageName)%>
                        </div>
                        <div class="<%=CssClass2%>">
                            <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", PageName & "_More")%>
                        </div>
                        
                        
                            <%--<asp:Literal runat="server" ID="leadership_contacts_list1"/>  --%>            
                          
<dl class="contacts_list">
<dt>CONTACT INFORMATION</dt>
<dd>
<span></span>
<p class="cms cmsType_Rich cmsName_<%=CMSFunctions.FormatFriendlyUrl(regionName,"_")%>_Leadership">
<%--<%= CMSFunctions.Format(Server.MapPath("~") & "cms\data\Text\" & CMSFunctions.FormatFriendlyUrl(regionName, "_") & "_Leadership.txt")%>--%>
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", CMSFunctions.FormatFriendlyUrl(regionName, "_") & "_Leadership")%>
</p>
</dd>
</dl>                  
                        
                        
                        <div class="gray_box_wrap">
                            <% If regionName = "Asia" Then%>
                            <a href="region_China" title="" class="view_more cmsMultiLanguage cms cmsType_TextSingle cmsName_By_Region_our_commitment"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "By_Region_our_commitment")%></a>
                            <% else If regionName = "China" Then%>
                            <a href="our_commitment" title="" class="view_more cmsMultiLanguage cms cmsType_TextSingle cmsName_our_commitment"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "our_commitment")%></a>
                            <%End If%>

                     
                             <asp:Literal runat="server" ID="clients_list_link"/>  

                        </div>
                    </div>
                    
                    <div class="right_bigger">
                        
                        <!-- Every 4th elements needs to have class="last_in_Row" -->
                        <ul class="main_list smaller_list line_height_fix">
                         <asp:Literal runat="server" ID="projects_list1"/>  
                        </ul>                    
                    </div>
                </div>
</asp:Content>