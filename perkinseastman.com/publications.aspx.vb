﻿Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Partial Class publications
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sql As String = ""
        sql &= "select *, "
        sql &= "V1.Item_Value as WebUrl, "
        sql &= "cast(isnull(c.item_value,10000) as int) sort_value, "
        sql &= "n2.Item_Value as VideoLink "
        sql &= "from ipm_news a "
        sql &= "left join ipm_news_field_value b on a.news_id = b.news_id and b.item_id = 2400409 "
        sql &= "left join ipm_news_field_value c on a.news_id = c.news_id and c.item_id = 2401236 "
        sql &= "left join ipm_news_field_value V1 on a.news_id = V1.news_id and V1.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWS_URL') "
        sql &= "left join IPM_NEWS_FIELD_VALUE n2 on a.News_Id = n2.NEWS_ID and n2.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_VIDEOLINK') "
        sql &= "left join IPM_NEWS_FIELD_VALUE n3 on a.News_Id = n3.NEWS_ID and n3.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWS_FUTURED') "
        sql &= "where type = 1 "
        sql &= "and show = 1 "
        sql &= "and post_date < getdate() "
        sql &= "and pull_date > getdate() "
        sql &= "and n3.Item_Value = '1' "
        sql &= "order by cast(isnull(c.item_value,0) as int) desc "
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)

        Dim out As String = ""
        For i As Integer = 0 To DT1.Rows.Count - 1
            out &= " <li> "
            out &= "<div class=""photo_holder"">"


            If DT1.Rows(i)("PDF").ToString.Trim = "1" Then
                out &= "<a href=""/dynamic/document/week/news/download/" & DT1.Rows(i)("News_ID").ToString.Trim & "/" & DT1.Rows(i)("News_ID").ToString.Trim & ".pdf"">"
            ElseIf DT1.Rows(i)("PDF").ToString.Trim = "0" And DT1.Rows(i)("WebUrl").ToString.Trim <> "" Then
                out &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(DT1.Rows(i)("WebUrl").ToString.Trim) & """ >"
            Else
                out &= "<a>"
            End If
            out &= "<img src=""" & "/dynamic/image/week/news/best/140x169/92/ffffff/Center/" & DT1.Rows(i)("News_ID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
            out &= "</a>"
            out &= "</div>"
            out &= "<div class=""info_holder"">"

            out &= "<h1>"
            If DT1.Rows(i)("PDF").ToString.Trim = "1" Then
                out &= "<a href=""/dynamic/document/week/news/download/" & DT1.Rows(i)("News_ID").ToString.Trim & "/" & DT1.Rows(i)("News_ID").ToString.Trim & ".pdf"">"
            ElseIf DT1.Rows(i)("PDF").ToString.Trim = "0" And DT1.Rows(i)("WebUrl").ToString.Trim <> "" Then
                out &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(DT1.Rows(i)("WebUrl").ToString.Trim) & """>"
            Else
                out &= "<a>"
            End If
            out &= formatfunctions.AutoFormatText(DT1.Rows(i)("Headline").ToString.Trim) & "</a>"
            out &= "</h1>"

            out &= "<span>" & formatfunctions.AutoFormatText(DT1.Rows(i)("PublicationTitle").ToString.Trim) & "</span>"
            out &= "<em>" & formatfunctions.AutoFormatText(DT1.Rows(i)("Item_Value").ToString.Trim) & "</em>"
            out &= "<div class=""text"" > "
            out &= "<p>" & formatfunctions.AutoFormatText(DT1.Rows(i)("Content").ToString.Trim) & "</p>"
            out &= "<p><a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(DT1.Rows(i)("WebUrl").ToString.Trim) & """>" & formatfunctionssimple.AutoFormatText(DT1.Rows(i)("Contact").ToString.Trim) & "</a>"
            If DT1.Rows(i)("VideoLink").ToString.Trim <> "" Then
                out &= "<a href=""" & formatfunctionssimple.AutoFormatText(DT1.Rows(i)("VideoLink").ToString.Trim) & """ class=""video vimeo"">View video</a>"
            End If
            out &= "</p>"
            out &= "</div> "
            out &= "</div>"
            out &= "</li>"
            out &= vbNewLine
        Next
        publications_list1.Text = out
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub

End Class
