﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="honor_awards.aspx.vb" Inherits="honor_awards" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<%@ Import Namespace="System.IO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%
Dim pageURL As String = HttpContext.Current.Request.RawUrl
Dim PageName As String = Path.GetFileName(Request.PhysicalPath).ToLower
pageURL = Replace(pageURL, "/", "")
If InStr(pageURL, "?") > 0 Then
pageURL = pageURL.Substring(0, pageURL.IndexOf("?"))
pageURL = Replace(pageURL, "/", "")
End If
%>

<%  If AwardId <> "" Then%>
<%  
    If Not File.Exists(Server.MapPath("~") & "cms\data\Metadata\" & "metadataTitle_" & pageURL.ToUpper & ".txt") Then
%>
<% ="<title>" & AwardName & "</title>"%>
<%Else%>  
     <%If CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Metadata\" & "metadataTitle_" & pageURL.ToUpper & ".txt") <> "" Then%>
     <% ="<title>" & CMSFunctions.GetMetaTitle(Server.MapPath("~") & "cms\data\", pageURL.ToUpper) & "</title>"%>
     <%Else%>
     <%= "<title>" & AwardName & "</title>"%>
     <%End If%>
<%End If%>
<%Else%>
<%  
    If Not File.Exists(Server.MapPath("~") & "cms\data\Metadata\" & "metadataTitle_" & pageURL.ToUpper & ".txt") Then
%>
<%  = "<title>Perkins Eastman</title>"%>
<%Else%>  
     <%If CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Metadata\" & "metadataTitle_" & pageURL.ToUpper & ".txt") <> "" Then%>
     <% ="<title>" & CMSFunctions.GetMetaTitle(Server.MapPath("~") & "cms\data\", pageURL.ToUpper) & "</title>"%>
     <%Else%>
     <%  ="<title>Perkins Eastman</title>"%>
     <%End If%>
<%End If%>
<%End If%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <div class="breadcrumbs">
                    <ul>
                        <li><a class="first">about</a><span> | </span></li>
                        <li><a href="<% =Master.UrlPage%>" class="active">Awards and Honors</a></li>                        
                    </ul>
                </div>
                
                <h3 class="sub_title">Awards and Honors</h3>
                <div class="left_bigger_right_smaller_layout">

                    <div class="left">  
                        <div class="highlighted_project">
                                <asp:Literal runat="server" ID="highlighted_award"/>  
                        </div>
                        
                        <div class="award_history">
                            <asp:Literal runat="server" ID="awards_list1"/>                          
                        </div>
                    </div>

                    <div class="right rightText">


<p class="cms cmsType_TextMulti cmsName_Honor_Awards_Right_Text">
<%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Honor_Awards_Right_Text")%>
</p>

                    <!--
                    <asp:Literal runat="server" ID="award_testimonial"/> 
                    -->
                    </div>

                </div>

<p class="disclamer cms cmsType_TextMulti cmsName_Honor_Awards_Disclaimer">
<%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Honor_Awards_Disclaimer")%>
</p>



</asp:Content>

