﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="clients.aspx.vb" Inherits="clients" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
<ul>
<asp:Literal runat="server" ID="breadcrumbs_list"/>
</ul>
                </div>
                <h3 class="sub_title"><asp:Literal runat="server" ID="clients_list_title"/></h3>
                <div class="client_list clientsCol1">
                <ul class="cms cmsType_TextMulti cmsName_<%=FriendlyCMSName%>_Client_List_1">
               <%= CMSFunctions.FormatSpecialList(Server.MapPath("~") & "cms\data\Text\", FriendlyCMSName & "_Client_List_1", "li")%>
               </ul>
               <ul class="cms cmsType_TextMulti cmsName_<%=FriendlyCMSName%>_Client_List_2">
               <%= CMSFunctions.FormatSpecialList(Server.MapPath("~") & "cms\data\Text\", FriendlyCMSName & "_Client_List_2", "li")%>
               </ul>
               <ul class="cms cmsType_TextMulti cmsName_<%=FriendlyCMSName%>_Client_List_3">
               <%= CMSFunctions.FormatSpecialList(Server.MapPath("~") & "cms\data\Text\", FriendlyCMSName & "_Client_List_3", "li")%>
               </ul>
                <div class="featured_client">
                  <asp:Literal runat="server" ID="featured_client_lit"/>
                </div>
            </div>

<div class="csFullContent cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Footer">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Footer")%>
</div>

</asp:Content>


