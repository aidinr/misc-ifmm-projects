﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="client_list.aspx.vb" Inherits="client_list" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">

<ul>
<asp:Literal runat="server" ID="breadcrumbs_list"/>
</ul>

                </div>

                <h3 class="sub_title"><asp:Literal runat="server" ID="clients_list_title"/></h3>
                <div class="client_list clientsCol1">
                 <asp:Literal runat="server" ID="client_list1"/>   
                <div class="featured_client">
                </div>
            </div>

<div class="csFullContent cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Footer">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Footer")%>
</div>

</asp:Content>

