﻿Imports System.Data

Partial Class eek_projects
    Inherits System.Web.UI.Page
    Public EekProjectsDT As DataTable


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim out As String = ""
        Dim out1 As String = ""

        Dim sql As String = ""
        sql &= "select "
        sql &= "p.ProjectID, "
        sql &= "P.Name as ProjectName, "
        sql &= "case when isnull(P.State_id,'') <> '' or P.State_id is null then P.City + case when isnull(S.Name ,'') <> '' then ', ' + S.Name else '' end else p.City + case when isnull(P5.Item_Value,'') <> '' then ', ' + P5.Item_Value else '' end + case when isnull(P4.Item_Value,'') <> '' then ', ' + P4.Item_Value else '' end end as ProjectAddress, "
        sql &= "P3.Item_Value "
        sql &= "FROM IPM_PROJECT P "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P3 on P.ProjectID = P3.ProjectID and P3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_EEKPROJECT') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P4 on P.ProjectID = P4.ProjectID and P4.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_CITY') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P5 on P.ProjectID = P5.ProjectID and P5.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_STATE') "
        sql &= "left join IPM_STATE S on S.State_id = P.State_id "
        sql &= "where P3.Item_Value = 1 "
        sql &= "and P.Available = 'y' "
        sql &= "order by P.Name asc "
        EekProjectsDT = New DataTable("EekProjectsDT")
        EekProjectsDT = mmfunctions.GetDataTable(sql)

        If EekProjectsDT.Rows.Count > 0 Then

            For R As Integer = 0 To EekProjectsDT.Rows.Count - 1
                Dim PrFriendlyLink As String = ""
                PrFriendlyLink = "project_" & EekProjectsDT.Rows(R)("ProjectID").ToString.Trim & "_" & formaturl.friendlyurl(EekProjectsDT.Rows(R)("ProjectName").ToString.Trim.ToLower)

                out &= " <div class=""big_image_with_info_box"" id=""box_" & R + 1 & """>"
                out &= "<a href=""" & PrFriendlyLink & """  title="""">"
                out &= "<img src=""/dynamic/image/week/project/best/460x368/92/ffffff/Center/" & EekProjectsDT.Rows(R)("ProjectID").ToString.Trim & ".jpg"" alt=""""/>"
                out &= "<strong class=""info_pop inner_pop"">"
                out &= "<big>" & formatfunctions.AutoFormatText(EekProjectsDT.Rows(R)("ProjectName").ToString.Trim) & "</big>"
                out &= "<span class=""openSans"">" & formatfunctions.AutoFormatText(EekProjectsDT.Rows(R)("ProjectAddress").ToString.Trim) & "</span>"
                out &= "</strong> "
                out &= "</a>"
                out &= "</div>"

                out1 &= "<dd><a href=""#"" title="""" rel=""" & R + 1 & """>" & formatfunctionssimple.AutoFormatText(EekProjectsDT.Rows(R)("ProjectName").ToString.Trim) & "</a></dd>"
            Next


            eek_projects_list1.Text = out
            eek_projects_links.Text = out1



            Dim LastVisitedCookie As New HttpCookie("LastVisited")
            LastVisitedCookie.Values("parent") = "eekp," & ""
            LastVisitedCookie.Values("lastVisit") = DateTime.Now.ToString()
            LastVisitedCookie.Expires = DateTime.Now.AddDays(1)
            Response.Cookies.Add(LastVisitedCookie)

        End If


    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub

End Class
