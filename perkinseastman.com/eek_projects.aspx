﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="eek_projects.aspx.vb" Inherits="eek_projects" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first">about</a><span> | </span></li>
                        <li><a href="<%=Master.UrlPage%>" class="active">EE&amp;K a perkins eastman company</a></li>                        
                    </ul>
                </div>
                
                <h3 class="sub_title">
                    
                    <img 
                         src="cms/data/Sized/best/249x82/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "eek_projects_eek_logo")%>" 
                         alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "eek_projects_eek_logo")%>" 
                         title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "eek_projects_eek_logo")%>" 
                         class="<%="cms cmsType_Image cmsName_eek_projects_eek_logo"%>" />
                </h3>
                
                <div class="company_inner_layout">
                    <div class="left">
                        
                        <div class="inner_tabs"> 
                           <asp:Literal runat="server" ID="eek_projects_list1"/>                           
                        </div>                  
                    </div>
                    <div class="right mt-5">
                        <dl class="project_list">
                            <dt>Selected Projects</dt>
                           <asp:Literal runat="server" ID="eek_projects_links"/>     
                        </dl>
                        
                        <a href="eek_page" title="" class="view_more">Return to EE&amp;K Intro Page</a>
                    </div>
                </div>
</asp:Content>

