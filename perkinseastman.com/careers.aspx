﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="careers.aspx.vb" Inherits="careers" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a href="/" title="" class="first">Home</a><span> | </span></li>
                        <li><a href="<%=Master.UrlPage%>" title="" class="active">careers</a></li>                        
                    </ul>
                </div>                
                <h3 class="sub_title cms cmsType_TextMulti cmsName_Careers_Heading"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Careers_Heading")%></h3>
                <div class="left_bigger_right_smaller_layout">
                    <div class="left">
                        <img src="cms/data/Sized/liquid/690x/92/ffffff/NorthWest/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "careers_image_1")%>"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "careers_image_1")%>"  title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "careers_image_1")%>" class="<%="cms cmsType_Image cmsName_careers_image_1"%>" />
                          <div>
                        <div class="text cms cmsType_Textset cmsName_Careers">
                            <!-- item 1 -->
                             <% 
                                for i as integer = 0 to 500 
                                     If Not CMSFunctions.CheckTextFile(Server.MapPath("~") & "cms\data\Textset\Careers\Careers", i) Then
                                         Exit For
                                     End If
                            %>
                            
                                <h3><%= CMSFunctions.Format(Server.MapPath("~") & "cms\data\Textset\Careers\Careers_xa" & i.ToString.Trim & ".txt")%></h3>
                                    <p><%= CMSFunctions.Format(Server.MapPath("~") & "cms\data\Textset\Careers\Careers_xb" & i.ToString.Trim & ".txt")%></p>                                 
                         
                            <% next %>  
                            </div>
                        <img src="cms/data/Sized/liquid/690x/92/ffffff/NorthWest/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "careers_image_2")%>"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "careers_image_2")%>"  title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "careers_image_2")%>" class="<%="cms cmsType_Image cmsName_careers_image_2"%>" />
                        </div>                        
                    </div>
                    <div class="right no_border">
                        <div class="right_wrap">
                        <p class="careersRightIntro cms cmsType_TextMulti cmsName_Careers_Top_Right"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Careers_Top_Right")%></p>

                        </div>
                        <div class="gray_box_wrap">
                        <a href="<% =CMSFunctions.GetFile(Server.MapPath("~") & "cms\data\File\", "Current_Openings", "link") %>" class="<%="view_more cms cmsType_File cmsName_Current_Openings"%>"><% = CMSFunctions.GetFile(Server.MapPath("~") & "cms\data\File\", "Current_Openings", "text")%></a>
                        </div>

                        <div class="gray_box_wrap"> 
                            <a title="" href="<%= CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", "Recruiter_Email", "href")%>" class="view_more cms cmsType_Link cmsName_Recruiter_Email"><%= CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", "Recruiter_Email", "text")%></a>
                        </div>
                    </div>
                </div>
</asp:Content>
