﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="china.aspx.vb" Inherits="china" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
<ul>
<li><a href="<%=Master.UrlPage%>" class="first active">China</a></li>
<li><a href="china_en">View in English</a></li>
</ul>
</div>
<h3 class="sub_title">中国</h3>
<div class="left_bigger_right_smaller_layout">
<div class="left">
<img src="cms/data/Sized/liquid/690x/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "China_ZH_image_1")%>" class="cms cmsType_Image cmsName_China_ZH_image_1"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "China_ZH_image_1")%>" />
<div class="text bigger_space_bettween_p">
<div class="cms cmsType_Rich cmsName_China_ZH_Intro">
<%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "China_ZH_Intro")%>
</div>
</div>
</div>
<div class="right no_border">
<div class="cms cmsType_Rich cmsName_China_ZH_Right">
<%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "China_ZH_Right")%>
</div>
<a href="/region_China" class="view_more padTop">中国项目浏览</a>
</div>
</div>
</asp:Content>


