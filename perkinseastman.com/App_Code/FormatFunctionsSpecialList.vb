Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.VisualBasic

Public Class formatfunctionsspeciallist

    Public Shared Function AutoFormatText(ByVal input As String, ByVal tag As String) As String
        input = HttpUtility.HtmlEncode(input)

	' Bold Heading
	input = RegularExpressions.Regex.Replace(input,  "(^)(?=.{1,999}\r?\n)([\S\S]*(?:(?: [!\S!\S]+)*)?)(\r?\n\r?\n)", "<strong class=""top"">$2</strong>" + VbNewLine )
	input = RegularExpressions.Regex.Replace(input, "(\r?\n\r?\n)(?=.{1,999}\r?\n)([\S\S]*(?:(?: [!\S!\S]+)*)?)(\r?\n\r?\n)", "<strong>$2</strong>" + VbNewLine )

        ' Italic
        input = RegularExpressions.Regex.Replace(input, "///(.*?)///", "<em style=""font-style: italic; display: inline"">$1</em>")


        ' BB Links
        ' {[link=url]}text{[/link]}
        input = RegularExpressions.Regex.Replace(input, "\{\[link=([^\]]+)\]\}([^\]]+)\{\[\/link\]\}", "<a href=""$1"">$2</a>")
        
        ' Links
        input = RegularExpressions.Regex.Replace(input, "(?<!\S)(https?://\S+[^\s@,.""']+)", "<a href=""$1"">$1</a>")
        input = RegularExpressions.Regex.Replace(input, "(?<!\S)(\www\.\S+[^\s@,.""']+)", "<a href=""http://$1"">$1</a>")
        
        ' Email Addresses
        input = RegularExpressions.Regex.Replace(input, "(?<!\S)\w[\w\.]*\w?@[\w\.]+\w", "<a href=""mailto:$0"">$0</a>")

        ' Twitter
        input = RegularExpressions.Regex.Replace(input, "(?<!\S)\((@)(\S+[^\s\)]+)\)", "(<a href=""http://twitter.com/$2"">$1$2</a>)")
        ' Spaces
        input = RegularExpressions.Regex.Replace(input, "  ", " &nbsp;")
        ' Dash Symbol
        input = RegularExpressions.Regex.Replace(input, "-- ", "&mdash; ")
        ' Copyright Symbol
        input = RegularExpressions.Regex.Replace(input, "\([cC]\)", "&#169;")
        ' Trademark Symbol
        input = RegularExpressions.Regex.Replace(input, "\([tT][mM]\)", "&#8482;")
        ' Registered Trademark Symbol
        input = RegularExpressions.Regex.Replace(input, "\([rR]\)", "&#174;")
        ' Bullet Symbol
        input = RegularExpressions.Regex.Replace(input, "\([\-]\)", "&#8226;")
        ' Line Breaks
        input = RegularExpressions.Regex.Replace("<" & tag & ">" + input, vbNewLine, " </" & tag & ">" + vbNewLine + "<" & tag & ">")
        input = input & "</" & tag & ">"
        ' Fix Double Entities
        input = RegularExpressions.Regex.Replace(input, "(?<=&)amp;(?=#?\w+;)", String.Empty)
        ' Languages
        input = RegularExpressions.Regex.Replace(input, "(?s)\{\[([^\]]*)\]\}(.*?)\{\[/\]\}", "<ins class=""lang lng$1"">$2</ins>")

        Return input
    End Function



End Class


