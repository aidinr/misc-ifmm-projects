Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.VisualBasic

Public Class formatfunctions

    Public Shared Function AutoFormatText(ByVal input As String) As String

input = HttpUtility.HtmlEncode(input)

'input = RegularExpressions.Regex.Replace(input, "[^A-Za-z0-9]+", "_",RegexOptions.Multiline)
'return input

' HTML List
input =	 RegularExpressions.Regex.Replace(input, "^- (?=[A-Z])([^\n]*)", "<span class=""bulletList""> $1 </span>",RegexOptions.Multiline)
input =	 RegularExpressions.Regex.Replace(input, "^   � (?=[0-9A-Z])([^\n]*)", "<span class=""bulletList""> $1 </span>",RegexOptions.Multiline)

input = RegularExpressions.Regex.Replace(input, "///(.*?)///", "<em style=""font-style: italic; display: inline"">$1</em>")

' Bullets
'input = RegularExpressions.Regex.Replace(input, "(^)- ", "&#8226; ")
'input = RegularExpressions.Regex.Replace(input, "(\n)- ", VbNewLine + "&#8226; ")

input = RegularExpressions.Regex.Replace(input, "^- (?=[A-Z])", "&#8226; ",RegexOptions.Multiline)

' Bold Heading
input = RegularExpressions.Regex.Replace(input, "(^|\n)(?=.{1,100}\n)([A-Z][A-Za-z]*(?:(?: [a-zA-Z0-9]+)* [A-Z][A-Za-z\?\!]*)?)(\r\n\r\n)", "$1<strong>$2</strong>$3")

' BB Links
' {[link=url]}text{[/link]}
input = RegularExpressions.Regex.Replace(input, "\{\[link=([^\]]+)\]\}([^\]]+)\{\[\/link\]\}", "<a href=""$1"">$2</a>")

' Links
input = RegularExpressions.Regex.Replace(input, "(?<!\S)(https?://\S+[^\s@,.""']+)", "<a href=""$1"">$1</a>")
input = RegularExpressions.Regex.Replace(input, "(?<!\S)(\www\.\S+[^\s@,.""']+)", "<a href=""http://$1"">$1</a>")

' Email Addresses
input = RegularExpressions.Regex.Replace(input, "(?<!\S)\w[\w\.]*\w?@[\w\.]+\w", "<a href=""mailto:$0"">$0</a>")

' Twitter
input = RegularExpressions.Regex.Replace(input, "(?<!\S)\((@)(\S+[^\s\)]+)\)", "(<a href=""http://twitter.com/$2"">$1$2</a>)")

' Google Maps
input = RegularExpressions.Regex.Replace(input, "(?<!\S){([-+]?[0-9]*\.?[0-9]+), ?([-+]?[0-9]*\.?[0-9]+)}", "<a target=""_blank"" href=""https://maps.google.com/?q=$1,$2&#38;z=17&amp;type=map"">Map</a>")

' Introductory Statement
' input = System.Text.RegularExpressions.Regex.Replace(input, "^([\w\d ,]*:\s)", "<strong>$1</strong>", System.Text.RegularExpressions.RegexOptions.Multiline)

' Spaces
input = RegularExpressions.Regex.Replace(input, "  ", " &nbsp;")

' Line Breaks
input = RegularExpressions.Regex.Replace(input, VbNewLine, VbNewLine +"<br />")

' Dash Symbol
input = RegularExpressions.Regex.Replace(input,"-- ", "&mdash; ")

' Copyright Symbol
input = RegularExpressions.Regex.Replace(input, "\{[cC]\}", "&#169;")

' Trademark Symbol
input = RegularExpressions.Regex.Replace(input, "\{[tT][mM]\}", "&#8482;")

' Registered Trademark Symbol
input = RegularExpressions.Regex.Replace(input, "\{[rR]\}", "&#174;")

' Bullet Symbol
input = RegularExpressions.Regex.Replace(input, "\{[\-]\}", "&#8226;")

'Special Bold Intro
'input = System.Text.RegularExpressions.Regex.Replace(input, "\(([^)]+)\)(?m:\r$|())", "<strong class=""introText"">$1</strong>")

'em span
'input = System.Text.RegularExpressions.Regex.Replace(input, "\(([^)]+)\)(?m:\r$|())", "<em>($1)</em>")

' Fix Double Entities
input = RegularExpressions.Regex.Replace(input, "(?<=&)amp;(?=#?\w+;)", String.Empty)

' Languages
input = RegularExpressions.Regex.Replace(input, "(?s)\{RTL\[([^\]]*)\]\}(.*?)\{RTL\[\/([^\]]*)\]\}", "<ins class=""lang lng$1""><ins class=""rtl"">$2</ins></ins>")
input = RegularExpressions.Regex.Replace(input, "(?s)\{\[([^\]]*)\]\}(.*?)\{\[\/([^\]]*)\]\}", "<ins class=""lang lng$1"">$2</ins>")

return input

    End Function

    Public Shared Function AutoFormatTextList(ByVal input As String) As String
        input = RegularExpressions.Regex.Replace(input, vbNewLine, vbNewLine + "")
        Return input
    End Function
End Class