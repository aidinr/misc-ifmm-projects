﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="design_approach.aspx.vb" Inherits="design_approach" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a href="#" title="" class="first">APPROACH</a><span> | </span></li>
                        <li><a href="#" title="" class="active">Design Approach</a></li>                        
                    </ul>
                </div>
                
                <h3 class="sub_title">Design Approach</h3>
                
                <div class="left_bigger_right_smaller_layout">
                    <div class="left">
                        
                         <img 
                         src="cms/data/Sized/liquid/690x/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "design_approach_image_1")%>" 
                         alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "design_approach_image_1")%>" 
                         title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "design_approach_image_1")%>" 
                         class="<%="cms cmsType_Image cmsName_design_approach_image_1"%>" />



                        <div class="text bigger_space_bettween_p">
                        <div class="cms cmsType_Rich cmsName_Design_Approach_Intro">
                            <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Design_Approach_Intro")%>
                            </div>                         
                        </div>                        
                    </div>
                    <div class="right no_border">
                             <div class="cms cmsType_Rich cmsName_Design_Approach_Right">
                            <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Design_Approach_Right")%>
                            </div>      

                    </div>
                </div>
</asp:Content>


