﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="china_en.aspx.vb" Inherits="china_en" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
<ul>
<li><a href="china" class="first">China</a><span> | </span></li>
<li><a href="<%=Master.UrlPage%>" class="active">English</a></li>
</ul>
</div>
<h3 class="sub_title">China</h3>
<div class="left_bigger_right_smaller_layout">
<div class="left">
<img src="cms/data/Sized/liquid/690x/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "China_English_image_1")%>" class="cms cmsType_Image cmsName_China_English_image_1"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "China_English_image_1")%>" />
<div class="text bigger_space_bettween_p">
<div class="cms cmsType_Rich cmsName_China_English_Intro">
<%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "China_English_Intro")%>
</div>
</div>
</div>
<div class="right no_border">
<div class="cms cmsType_Rich cmsName_China_English_Right">
<%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "China_English_Right")%>
</div>
<a href="/region_China" class="view_more padTop">View our Chinese Projects</a>
</div>
</div>

</asp:Content>

