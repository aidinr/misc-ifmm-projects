﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="homepage_slideshow">
<div class="w"><asp:Literal runat="server" ID="carousel_images"/></div>
<p class="cms cmsType_TextSingle cmsName_Home_Page_Carousel_Delay" id="speed<%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Home_Page_Carousel_Delay")%>"></p>
<p class="cms cmsType_TextSingle cmsName_Home_Page_Carousel_Random"><!-- <%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Home_Page_Carousel_Random")%> --> </p>
</div>
</asp:Content>