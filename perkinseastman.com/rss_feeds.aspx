﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="rss_feeds.aspx.vb" Inherits="rss" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_Home" href="/"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Home")%></a><span> | </span></li>
                        <li><a class="active" href="<%=Master.UrlPage%>"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Rss_Feeds")%></a></li>
                    </ul>
                </div>
                <h3 class="sub_title cmsMultiLanguage cms cmsType_TextSingle cmsName_Rss_Feeds"><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Rss_Feeds")%></h3>
                <div class="left_bigger_right_smaller_layout">
                    <div class="left">
                        
                         <img 
                         src="cms/data/Sized/liquid/690x/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "rss_image")%>" 
                         alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "rss_image")%>" 
                         title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "rss_image")%>" 
                         class="<%="cms cmsType_Image cmsName_rss_image"%>" />



                        <div class="text bigger_space_bettween_p">
                        <div class="cms cmsType_Rich cmsName_rss_Intro">
                            <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "rss_Intro")%>
                            </div>                         
                        </div>
                        
                    </div>
                    <div class="right no_border">
                             <div class="cms cmsType_Rich cmsName_rss_Right">
                            <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "rss_Right")%>
                            </div>      

                    </div>
                </div>
</asp:Content>

