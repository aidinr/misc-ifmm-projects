﻿<%@ Page Title=""  Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="category.aspx.vb" Inherits="category"%>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


<%
Dim pageURL As String = HttpContext.Current.Request.RawUrl
Dim PageName As String = Path.GetFileName(Request.PhysicalPath).ToLower
pageURL = Replace(pageURL, "/", "")
If InStr(pageURL, "?") > 0 Then
pageURL = pageURL.Substring(0, pageURL.IndexOf("?"))
pageURL = Replace(pageURL, "/", "")
End If
%>

<%  
If Not File.Exists(Server.MapPath("~") & "cms\data\Metadata\" & "metadataTitle_" & pageURL.ToUpper & ".txt") Then
%>
<%= "<title>" & CategoryTitle & "</title>"%>
<%Else%>  
     <%If CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Metadata\" & "metadataTitle_" & pageURL.ToUpper & ".txt") <> "" Then%>
     <%= "<title>" & CMSFunctions.GetMetaTitle(Server.MapPath("~") & "cms\data\", pageURL.ToUpper) & "</title>"%>
     <%Else%>
     <%= "<title>" & CategoryTitle & "</title>"%>
     <%End If%>
<%End If%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_Work"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Work")%></a><span> | </span></li>
                        <li><a class="cmsMultiLanguage cms cmsType_TextSingle cmsName_By_Practice_Area"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "By_Practice_Area")%></a><span> | </span></li>
                        <%If MainPracticeName <> "" Then%>
                        <li><a href="practice_<%=MainPracticeId & "_" & formaturl.friendlyurl(MainPracticeName.ToLower)%>" title=""><%= MainPracticeName%></a><span> | </span></li>
                       <%End If%>
                        <li><a href="<%=Master.UrlPage%>" class="cmsMultiLanguage cms cmsType_TextSingle cmsName_<%= new Regex("[^a-zA-Z0-9]").Replace(practiceFrontText, "_") %>"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", new Regex("[^a-zA-Z0-9]").Replace(practiceFrontText, "_")) %></a><span> | </span></li>
                        <li><a href="<%=Master.UrlPage%>" class="active cmsMultiLanguage cms cmsType_TextSingle cmsName_Projects"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Projects")%></a></li>
                    </ul>
                </div>
                <div class="two_columns">
                    <div class="left_smaller">

                        <!-- <h3 class="sub_title"><asp:Literal runat="server" ID="category_name"/></h3> -->

                        <h3 class="sub_title cmsMultiLanguage cms cmsType_TextSingle cmsName_<%= new Regex("[^a-zA-Z0-9]").Replace(practiceFrontText, "_") %>"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", new Regex("[^a-zA-Z0-9]").Replace(practiceFrontText, "_")) %></h3>
                        <%
                            Dim CssClass As String = ""
                            Dim CssClass2 As String = ""
                            Dim PageName As String = ""
                            
                            If practiceName <> "" Then
                                PageName = practiceName
                                PageName = Replace(PageName, " ", "_")
                                PageName = Replace(PageName, "-", "_")
                                
                                CssClass = "cms cmsMultiLanguage learnIntro cmsType_Rich cmsName_" & PageName
                                CssClass2 = "cms cmsMultiLanguage learnMore cmsType_Rich cmsName_" & PageName & "_More"
                            Else
                                CssClass = "Text"
                            End If
                         %>
                     <div class="readMe">
                        <div class="<%=CssClass%>">
                            <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", PageName)%>
                        </div>
                        <div class="<%=CssClass2%>">
                            <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", PageName & "_More")%>
                        </div>
                     </div>
                        
                        
                            <%--<asp:Literal runat="server" ID="leadership_contacts_list1"/> --%>   


<dl class="contacts_list">
<dt class="cmsMultiLanguage cms cmsType_TextSingle cmsName_<%= new Regex("[^a-zA-Z0-9]").Replace(practiceFrontText, "_") %>_Leadership"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", new Regex("[^a-zA-Z0-9]").Replace(practiceFrontText, "_")&"_Leadership") %></dt>
<dd>
<span></span>
<p class="cms cmsType_Rich cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName,"_")%>_Leadership">

<%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", PageName & "_Leadership")%>

</p>
</dd>
</dl>

                                         
                        <asp:Literal runat="server" ID="rel_pages_list1"/>   


                        <asp:Literal runat="server" ID="clients_list_link"/>
                        <asp:Literal runat="server" ID="brochures_link"/>

                    </div>
                    
                    <div class="right_bigger">
                        <!-- BIG Image needs ID="BOX_number", and the SMALL_THUMBS Link needs REL="number" to open associate box -->
                        <div class="inner_tabs">                            
                         <asp:Literal runat="server" ID="big_projects_list1"/>
                                            
                            <h3 class="headline_underlined mt8">view select projects below</h3>
                            <!-- Every ninth LI should have class="last_in_row" -->
                            <ul class="main_list small_thumbs mt15">
                                  <asp:Literal runat="server" ID="small_projects_list1"/>           
                            </ul>      
                        </div>
                        
 <%
     'Dim HasName As String = ""
     'Dim HasSlides As String = ""
     'If Directory.GetFiles(Server.MapPath("~") & "cms\data\Text\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & "*").Length > 0 Then
     '    HasName = "1"
     'End If
     'If Directory.GetFiles(Server.MapPath("~") & "cms\data\Textset\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & "*").Length > 0 Then
     '    HasSlides = "1"
     'End If
     
     Dim ItemsCount As Integer = CMSFunctions.CountTextSingle(CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & "*" & "_Name")
     
%>

<%  If ItemsCount > 0 Then%>
 <div class="casestudy_list">
<h3 class="headline_additional">view select case studies below</h3>
<ul class="cmsGroup cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName, "_")%>_Case_Studies">
<!--
<li><a><span class="selectTemplate cms cmsType_TextSingle cmsName_Name">Case Study:</span><strong>Case Study Name</strong></a><a class="selectTemplate cms cmsType_Textset cmsName_List"></a></li>
-->
 <%
     '    Dim SlideName As String
     '    Dim SlideLayout As String
     'For i As Integer = 1 To 201 - 1
     ' If Directory.GetFiles(Server.MapPath("~") & "cms\data\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then

     Dim SlideName As String
     Dim SlideLayout As String
     For i As Integer = 1 To ItemsCount
         Dim SlidesCount As Integer = CMSFunctions.CountTextSet(CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List")

     
     %>

<%
    'For x As Integer = 0 To 500
    '    SlideName = ""
    '    SlideLayout = ""
    '    If CMSFunctions.CheckTextFile(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List", x) Then
    '        SlideName = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xa" & x & ".txt")
    '        SlideLayout = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xb" & x & ".txt")
    '        SlideName = CMSFunctions.FormatFriendlyUrl(SlideName, "_")
    '        Exit For
    '    End If
    'Next
    
    If SlidesCount > 0 Then
        SlideName = ""
        SlideLayout = ""
        SlideName = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xa" & 0 & ".txt")
        SlideLayout = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xb" & 0 & ".txt")
        SlideName = CMSFunctions.FormatFriendlyUrl(SlideName, "_")
    End If
    
    %>
<li class="cmsGroupItem">
<%  If SlideName <> "" Then%>
<a href="casestudy_<%=SlideLayout %>_<%=CategoryId %>_<%=SlideName %>">
<%End If%>
<span class="selectTemplate cms cmsType_TextSingle cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName,"_") %>_Case_Studies_Item_<%= i %>_Name">Case Study <%= i %>:</span>
<strong><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_Name")%></strong>
<%  If SlideName <> "" Then%>
</a>
<%End If%>
<a class="selectTemplate cms cmsType_Textset cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName,"_") %>_Case_Studies_Item_<%= i %>_List"></a>
</li>

<%Next%>  
</ul>
</div>         
<%Else%> 
<div class="noCase cmsGroup cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName, "_")%>_Case_Studies">
<!--
<div class="casestudy_list"><ul><li><a href="#"><span class="selectTemplate cms cmsType_TextSingle cmsName_Name">Case Study:</span><strong>Example Case Study</strong></a><a class="selectTemplate cms cmsType_Textset cmsName_List"></a></li></ul></div>
-->
</div>
<%End If%>                                   
         
    </div>
      </div>
</asp:Content>

