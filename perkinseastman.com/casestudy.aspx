﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="casestudy.aspx.vb" Inherits="casestudy" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
<ul>
<asp:Literal runat="server" ID="breadcrumbs_list"/>
</ul>
</div>
<div class="headline mt-10">
<div class="left">
<span>Case Study <%= CurrentCaseStrudyNo%>:</span>
<h2 class="cms cmsType_TextSingle cmsName_<%=FriendlyCMSName %>_Heading"><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Heading")%></h2>
<strong class="openSans cms cmsType_TextSingle cmsName_<%=FriendlyCMSName %>_Location"><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Location")%></strong>
</div>
<div class="project_right">
<ul class="project_nav">
<asp:Literal runat="server" ID="project_nav_list"/>
</ul>
<div class="slideNavSecondary">
<ul>
<asp:Literal runat="server" ID="slides_nav_list"/>
</ul>
</div>
</div>
</div>
<%  Select Case url_LayoutId%>
<%Case "1" %>
<div class="CaseStudy">
<div class="csFull">
<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image" & "*", SearchOption.AllDirectories).Length > 0 Then%>
<img src="cms/data/Sized/liquid/940x/92/33a535/NorthWest/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Image")%>" class="cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image")%>" />
<%Else%>
<span class="caseNoImg cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image"></span>
<%End If%>
<div class="csFullContent cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Content">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Content")%>
</div>
</div>
</div>
<%Case "2"%>
<div class="CaseStudy">
<div class="csFull">
<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image" & "*", SearchOption.AllDirectories).Length > 0 Then%>
<img src="cms/data/Sized/liquid/940x/92/33a535/NorthWest/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Image")%>" class="cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image")%>" />
<%Else%>
<span class="caseNoImg cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image"></span>
<%End If%>
<div class="csFullContent cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Content">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Content")%>
</div>
</div>
<div class="caseStudySecondary">
 <div class="gridc1">
  <h2 class="cms cmsType_TextSingle cmsName_<%=FriendlyCMSName %>_Bottom_Left_Heading"><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Bottom_Left_Heading")%></h2>
 </div>
 <div class="gridc3 cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Bottom_Right_Content">
  <%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Bottom_Right_Content")%>
 </div>
 <div class="c"></div>
</div>
</div>
<%Case "3"%>
<div class="CaseStudy">
<div class="csHalf1">


<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_1" & "*", SearchOption.AllDirectories).Length > 0 Then%>
<img src="cms/data/Sized/liquid/460x/92/33a535/NorthWest/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Image_1")%>" class="cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_1"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_1")%>" />
<%Else%>
<span class="caseNoImg cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_1"></span>
<%End If%>
<div class="csFullContent cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Content_1">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Content_1")%>
</div>
</div>
<div class="csHalf2">
<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_2" & "*", SearchOption.AllDirectories).Length > 0 Then%>
<img src="cms/data/Sized/liquid/460x/92/33a535/NorthWest/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Image_2")%>" class="cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_2"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_2")%>" />
<%Else%>
<span class="caseNoImg cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_2"></span>
<%End If%>
<div class="csFullContent cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Content_2">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Content_2")%>
</div>
</div>
<div class="c"></div>
</div>

<div class="caseStudySecondary">
 <div class="gridc1">
  <h2 class="cms cmsType_TextSingle cmsName_<%=FriendlyCMSName %>_Bottom_Left_Heading"><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Bottom_Left_Heading")%></h2>
 </div>
 <div class="gridc3 cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Bottom_Right_Content">
  <%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Bottom_Right_Content")%>
 </div>
 <div class="c"></div>
 </div>

<%Case "4"%>
<div class="CaseStudy">
<div class="csqa1">
<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_1" & "*", SearchOption.AllDirectories).Length > 0 Then%>
<img src="cms/data/Sized/liquid/220x/92/33a535/NorthWest/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Image_1")%>" class="cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_1"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_1")%>" />
<%Else%>
<span class="caseNoImg cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_1"></span>
<%End If%>
<div class="csFullContent cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Content_1">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Content_1")%>
</div>
</div>
<div class="csqa3">
<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_2" & "*", SearchOption.AllDirectories).Length > 0 Then%>
<img src="cms/data/Sized/liquid/700x/92/33a535/NorthWest/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Image_2")%>" class="cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_2"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_2")%>" />
<%Else%>
<span class="caseNoImg cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_2"></span>
<%End If%>
<div class="csFullContent cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Content_2">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Content_2")%>
</div>
</div>
<div class="c"></div>
</div>

<div class="caseStudySecondary">
 <div class="gridc1">
  <h2 class="cms cmsType_TextSingle cmsName_<%=FriendlyCMSName %>_Bottom_Left_Heading"><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Bottom_Left_Heading")%></h2>
 </div>
 <div class="gridc3 cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Bottom_Right_Content">
  <%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Bottom_Right_Content")%>
 </div>
 <div class="c"></div>
 </div>

<%Case "5"%>
<div class="CaseStudy">
<div class="csqb3">
<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_1" & "*", SearchOption.AllDirectories).Length > 0 Then%>
<img src="cms/data/Sized/liquid/700x/92/33a535/NorthWest/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Image_1")%>" class="cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_1"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_1")%>" />
<%Else%>
<span class="caseNoImg cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_1"></span>
<%End If%>
<div class="csFullContent cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Content_1">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Content_1")%>
</div>
</div>
<div class="csqb1">
<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_2" & "*", SearchOption.AllDirectories).Length > 0 Then%>
<img src="cms/data/Sized/liquid/220x/92/33a535/NorthWest/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Image_2")%>" class="cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_2"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_2")%>" />
<%Else%>
<span class="caseNoImg cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_2"></span>
<%End If%>
<div class="csFullContent cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Content_2">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Content_2")%>
</div>
</div>
<div class="c"></div>
</div>

<div class="caseStudySecondary">
 <div class="gridc1">
  <h2 class="cms cmsType_TextSingle cmsName_<%=FriendlyCMSName %>_Bottom_Left_Heading"><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Bottom_Left_Heading")%></h2>
 </div>
 <div class="gridc3 cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Bottom_Right_Content">
  <%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Bottom_Right_Content")%>
 </div>
 <div class="c"></div>
 </div>

<%Case "6"%>
<div class="CaseStudy">
<div class="csqb1a">
<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_1" & "*", SearchOption.AllDirectories).Length > 0 Then%>
<img src="cms/data/Sized/liquid/220x/92/33a535/NorthWest/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Image_1")%>" class="cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_1"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_1")%>" />
<%Else%>
<span class="caseNoImg cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_1"></span>
<%End If%>
<div class="csFullContent cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Content_1">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Content_1")%>
</div>
</div>
<div class="csqb1b">
<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_2" & "*", SearchOption.AllDirectories).Length > 0 Then%>
<img src="cms/data/Sized/liquid/220x/92/33a535/NorthWest/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Image_2")%>" class="cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_2"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_2")%>" />
<%Else%>
<span class="caseNoImg cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_2"></span>
<%End If%>
<div class="csFullContent cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Content_2">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Content_2")%>
</div>
</div>
<div class="csqb1c">
<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_3" & "*", SearchOption.AllDirectories).Length > 0 Then%>
<img src="cms/data/Sized/liquid/220x/92/33a535/NorthWest/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Image_3")%>" class="cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_3"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_3")%>" />
<%Else%>
<span class="caseNoImg cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_3"></span>
<%End If%>
<div class="csFullContent cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Content_3">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Content_3")%>
</div>
</div>
<div class="csqb1d">
<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_4" & "*", SearchOption.AllDirectories).Length > 0 Then%>
<img src="cms/data/Sized/liquid/220x/92/33a535/NorthWest/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Image_4")%>" class="cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_4"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", FriendlyCMSName & "_Image_4")%>" />
<%Else%>
<span class="caseNoImg cms cmsType_Image cmsName_<%=FriendlyCMSName%>_Image_4"></span>
<%End If%>
<div class="csFullContent cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Content_4">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Content_4")%>
</div>
</div>
<div class="c"></div>
</div>

<div class="caseStudySecondary">
 <div class="gridc1">
  <h2 class="cms cmsType_TextSingle cmsName_<%=FriendlyCMSName %>_Bottom_Left_Heading"><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Bottom_Left_Heading")%></h2>
 </div>
 <div class="gridc3 cms cmsType_Rich cmsName_<%=FriendlyCMSName %>_Bottom_Right_Content">
  <%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Bottom_Right_Content")%>
 </div>
 <div class="c"></div>
 </div>

<%End Select%>
<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\Text\", FriendlyCMSName & "_Project_ID" & "*", SearchOption.AllDirectories).Length > 0 Then%>
 <a class="caseProjectLink" href="<%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Project_ID")%>">View Project Page</a>
<%End If%>
<div class="caseProjectLink cms cmsType_TextSingle cmsName_<%=FriendlyCMSName %>_Project_ID"></div>
<div class="c"></div>

<asp:Literal runat="server" ID="Literal2"/>
<br />
<br />
<asp:Literal runat="server" ID="Literal1"/>
</asp:Content>