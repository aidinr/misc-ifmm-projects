﻿Imports System.Data

Partial Class region
    Inherits System.Web.UI.Page
    Public RecordPR As Integer
    Public NewRow As Integer
    Public regionName As String
    Public ProjectsDT As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim out As String = ""
        Dim sql As String = ""

        regionName = Request.QueryString("name")
        regionName = Replace(regionName, "_", " ")


        Dim LastVisitedCookie As New HttpCookie("LastVisited")
        LastVisitedCookie.Values("parent") = "region," & regionName
        LastVisitedCookie.Values("lastVisit") = DateTime.Now.ToString()
        LastVisitedCookie.Expires = DateTime.Now.AddDays(1)
        Response.Cookies.Add(LastVisitedCookie)


        sql &= "select "
        sql &= "a.ProjectID, "
        sql &= "a.name  "
        'sql &= "d.Item_Value disable "
        sql &= "from ipm_project a  "
        sql &= "join ipm_project_field_value c on c.projectid = a.projectid and c.item_id = 3408923 and c.item_value = '" & regionName & "' "
        sql &= "left join IPM_PROJECT_FIELD_VALUE b on b.ProjectID = a.ProjectID and b.Item_ID = 2400053 "
        sql &= "left join IPM_PROJECT_FIELD_VALUE d on a.ProjectID = d.ProjectID and d.Item_ID = 3409575 "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P9 on a.ProjectID = P9.ProjectID and P9.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAN_PROJECT_ORDERING') "
        sql &= "where available = 'Y' "
        sql &= "order by CAST(isnull(P9.Item_Value, 9999999999) as integer) desc"
        ProjectsDT = New DataTable("ProjectsDT")
        ProjectsDT = mmfunctions.GetDataTable(sql)
        RecordPR = 4
        For R As Integer = 0 To ProjectsDT.Rows.Count - 1
            Dim sqlImg As String = ""
            sqlImg &= "select top 9 * from "
            sqlImg &= " (select a.Asset_ID, a.projectid, 1 apply, c.Item_value from ipm_asset_category b, ipm_asset a "
            sqlImg &= "left join ipm_asset_field_value c on c.item_id = 2400079 and c.asset_id = a.asset_id "
            sqlImg &= "where a.projectid = '" & ProjectsDT.Rows(R)("ProjectID").ToString.Trim & "'"
            sqlImg &= "and a.category_id = b.category_id "
            sqlImg &= "and a.available = 'y' "
            sqlImg &= "and b.available = 'y' "
            sqlImg &= "and b.name = 'Public Website Images' "
            sqlImg &= "and a.HPixel >= 350 "
            sqlImg &= "UNION select a.Asset_ID, a.projectid, b.Item_Value apply, c.Item_value "
            sqlImg &= "from ipm_asset a "
            sqlImg &= "left join ipm_asset_field_value B on B.item_id = 2400401 and B.asset_id = a.asset_id "
            sqlImg &= "left join ipm_asset_field_value c on c.item_id = 2400079 and c.asset_id = a.asset_id "
            sqlImg &= "where a.projectid = '" & ProjectsDT.Rows(R)("ProjectID").ToString.Trim & "'"
            sqlImg &= "AND a.available = 'y' and B.Item_value = 1 and b.Item_Value is not null  and a.HPixel >= 350) "
            sqlImg &= "a order by a.item_value desc "
            Dim ProjectImageDT As DataTable
            ProjectImageDT = New DataTable("ProjectImageDT")
            ProjectImageDT = mmfunctions.GetDataTable(sqlImg)




            NewRow = NewRow + 1
            If NewRow = RecordPR Then
                out &= "<li class=""last_in_row"">"
                NewRow = 0
            Else
                out &= " <li> "
            End If
            Dim PrFriendlyLink As String = "project_" & ProjectsDT.Rows(R)("ProjectID").ToString.Trim & "_" & CMSFunctions.FormatFriendlyUrl(ProjectsDT.Rows(R)("name").ToString.Trim.ToLower, "_")
            If ProjectImageDT.Rows.Count > 0 Then
                out &= "<a href=""" & PrFriendlyLink & """  title="""">"
            Else
                out &= "<a title="""">"
            End If
            out &= "<img src=""/dynamic/image/week/project/best/160x160/92/ffffff/Center/" & ProjectsDT.Rows(R)("ProjectID").ToString.Trim & ".jpg"" alt=""""/>"
            out &= "<span>" & formatfunctions.AutoFormatText(ProjectsDT.Rows(R)("name").ToString.Trim) & "</span>"
            If ProjectImageDT.Rows.Count > 0 Then
                out &= "</a>"
            Else
                out &= "</a>"
            End If

            out &= "</li>"
        Next
        projects_list1.Text = out

        'Dim sql1 As String = ""
        'sql1 &= "select distinct "
        'sql1 &= "u.WorkCity "
        'sql1 &= "from IPM_USER u "
        'sql1 &= "left join IPM_USER_FIELD_VALUE vr on u.UserID = vr.USER_ID and vr.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_LEADERSHIP_REGION') "
        'sql1 &= "where Active = 'Y' "
        'sql1 &= "and vr.Item_Value is not null "
        'sql1 &= "and charindex('" & regionName & "',LTRIM(RTRIM(vr.Item_Value)))!= 0 "
        'sql1 &= "and u.Phone <> '' "
        'Dim CitiesDT As DataTable
        'CitiesDT = New DataTable("CitiesDT")
        'CitiesDT = mmfunctions.GetDataTable(sql1)
        'Dim out3 As String = ""
        'If CitiesDT.Rows.Count > 0 Then
        '    out3 &= "<dl class=""contacts_list"">"
        '    out3 &= "<dt>CONTACT INFORMATION</dt>"
        '    For Y As Integer = 0 To CitiesDT.Rows.Count - 1
        '        Dim sql2 As String = ""
        '        sql2 &= "select "
        '        sql2 &= "u.UserID, "
        '        sql2 &= "u.FirstName + ' ' + u.LastName as Name, "
        '        sql2 &= "u.WorkCity, "
        '        sql2 &= "u.Phone, "
        '        sql2 &= "vr.Item_Value as Region "
        '        sql2 &= "from IPM_USER u  "
        '        sql2 &= "left join IPM_USER_FIELD_VALUE vc on u.UserID = vc.USER_ID and vc.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TITLE') "
        '        sql2 &= "left join IPM_USER_FIELD_VALUE vd on u.UserID = vd.USER_ID and vd.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USER_DESIGNATIONS') "
        '        sql2 &= "left join IPM_USER_FIELD_VALUE vs on u.UserID = vs.USER_ID and vs.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USER_ORDERING') "
        '        sql2 &= "left join IPM_USER_FIELD_VALUE vr on u.UserID = vr.USER_ID and vr.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_LEADERSHIP_REGION') "
        '        sql2 &= "where Active = 'Y' "
        '        sql2 &= "and charindex('" & regionName & "',LTRIM(RTRIM(vr.Item_Value)))!= 0 "
        '        sql2 &= "and u.WorkCity = '" & CitiesDT.Rows(Y)("WorkCity").ToString.Trim & "'"
        '        sql2 &= "and u.Phone <> '' "

        '        Dim ContactsDT As DataTable
        '        ContactsDT = New DataTable("ContactsDT")
        '        ContactsDT = mmfunctions.GetDataTable(sql2)

        '        If ContactsDT.Rows.Count > 0 Then
        '            out3 &= "<dd>"
        '            out3 &= "<span>" & formatfunctions.AutoFormatText(CitiesDT.Rows(Y)("WorkCity").ToString.Trim) & "</span>"
        '            For A As Integer = 0 To ContactsDT.Rows.Count - 1
        '                out3 &= "<p><strong>" & formatfunctions.AutoFormatText(ContactsDT.Rows(A)("Name").ToString.Trim) & "</strong>" & " T. " & formatfunctions.AutoFormatText(ContactsDT.Rows(A)("Phone").ToString.Trim) & "</p>"
        '                out3 &= "<p><strong></strong></p>"
        '            Next
        '            out3 &= "</dd>"
        '        End If
        '    Next
        '    out3 &= "</dl>"
        'End If


        Dim ClFriendlyLink As String = ""
        ClFriendlyLink = "clients_" & formaturl.friendlyurl(regionName)
        'clients_list_link.Text &= "<a href=""" & ClFriendlyLink & """ title="""" class=""view_more"">View Client List</a>"

        clients_list_link.Text = "<a href=""" & ClFriendlyLink & """ title="""" class=""view_more""><span class=""cmsMultiLanguage cms cmsType_TextSingle cmsName_Client_List"">" & CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Client_List") & "</span></a>"

        'If regionName = "China" Then
        '    clients_list_link.Text &= "<a href=""china_clients_cn"" title="""" class=""view_more"">业主名单浏览</a>"
        'End If


        'leadership_contacts_list1.Text = out3


    End Sub



End Class
