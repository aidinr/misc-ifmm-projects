﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="leadership_executive_directors.aspx.vb" Inherits="leadership_executive_directors" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first">about</a><span> | </span></li>
                        <li><a href="leadership_executive_directors">Leadership</a> | </li>
                        <li><a href="<%=Master.UrlPage%>" class="active">Principal and Executive Directors</a></li>
                    </ul>
                </div>

                <div class="two_columns">
                    <div class="left_smaller">
                        <h3 class="sub_title">Leadership</h3>
                        <ul class="leadership_sorting_list">
                            <li><a href="leadership_executive_directors" title="" class="active">Principal and Executive Directors</a></li>
                            <li><a href="leadership_principals" title="">Principals</a></li>
                            <li><a href="leadership_associates" title="">Associate Principals</a></li>
                            <li><a href="<%=AgencyLink%>" title="">Affiliates</a></li>
                        </ul>
                    </div>
                    
                    <div class="right_bigger mt48">

                        <div class="headline leadership_headline">
                            <div class="left">
                                <h2 class="smaller_font">Principal and Executive Directors</h2>                       
                            </div>

                            <!--
                                SLides INFO:                    
                                id="slide_progression" DEFAULT VALUE = 1
                                id="max_slides" HERE you put SLIDES COUNT in my case 4
                                Different sldies are with ID="slide_1" ID="slide_2" e.t.c. :)
                            -->

                            <%  If TotalPages > 1 Then%>
                            <div class="slide_controls">
                                <ul>
                                    <li><a href="#" title="Previous" class="previous_slide">Previous</a></li>
                                    <li>Page <span id="slide_progression">1</span> of <span id="max_slides"><%=TotalPages%></span></li>
                                    <li><a href="#" title="Next" class="next_slide">Next</a></li>
                                </ul>
                            </div>
                            <%End If%>

                        </div>                 
                        
                        <div class="slides_holder">
                           <!-- Every 4th elements needs to have class="last_in_Row" -->
                              <asp:Literal runat="server" ID="leadership_principals_list1"/>       
                        </div>                        
 
                        <!--
                            Testimonial POPUPS INFO:
                            the popup div ID needs to be the value of the associate a HREF
                            ex: <div id="pop_10"> and <a href="#pop_10">
                        -->           

                        <div class="testimonial_popups">
                           <asp:Literal runat="server" ID="leadership_bio_list1"/>               
                        </div>

                    </div>
                </div>
</asp:Content>
