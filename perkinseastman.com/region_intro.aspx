﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="region_intro.aspx.vb" Inherits="region_intro" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_Work"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Work")%></a><span> | </span></li>
                        <li><a class="cmsMultiLanguage cms cmsType_TextSingle cmsName_By_Region"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "By_Region")%></a><span> | </span></li>
                        <li><a href="<% =Master.UrlPage%>" class="active"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Regions_Map")%></a></li>
                    </ul>
                </div>
                
                <div class="region_map_holder">
                    <div class="text_left">
                        <h3 class="sub_title cmsMultiLanguage cms cmsType_TextSingle cmsName_Regions_Map"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Regions_Map")%></h3> 
                        <div class="text">
                           
                          <div class="cmsMultiLanguage cms cmsType_Rich cmsName_Region_Intro_Left">
                            <div><%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Region_Intro_Left")%></div>
                         </div>

                            <div class="gray_box_wrap">
                                <a href="country_list" class="view_more cmsMultiLanguage cms cmsType_TextSingle cmsName_Regions_Country_List"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Regions_Country_List")%></a>
                                <a href="contact" class="view_more cmsMultiLanguage cms cmsType_TextSingle cmsName_Office_Locations"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Office_Locations")%></a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="map_holder">
                        <a href="region_The_Americas" class="north_america"></a>
                        <a href="region_The_Americas" class="south_america"></a>
                        <a href="region_Africa_and_the_Middle_East" class="africa"></a>
                        <a href="region_Europe" class="europe"></a>
                        <a href="region_Asia" class="pacific"></a>
                        <a href="region_Europe" class="russia"></a>
                    </div>
                </div>
</asp:Content>