﻿Imports System.Data

Partial Class honor_awards
    Inherits System.Web.UI.Page

    Public FuturedAwardsDT As DataTable
    Public YearsDT As DataTable
    Public AwardsDT As DataTable
    Public AwardsTestimonial As DataTable
    Public AwardId As String
    Public AwardName As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AwardId = ""
        AwardName = ""
        AwardId = Request.QueryString("id")


        Dim out As String = ""
        Dim out1 As String = ""

        Dim sql2 As String = ""
        If AwardId = "" Then
            sql2 &= "select top 1 "
        Else
            sql2 &= "select "
        End If
        sql2 &= "AW.Awards_Id, "
        sql2 &= "AW.Headline, "
        sql2 &= "AW.Content, "
        sql2 &= "AW.Contact, "
        sql2 &= "AW.PublicationTitle, "
        sql2 &= "AW.Creation_Date, "
        sql2 &= "AW1.Item_Value AS AwardTitle, "
        sql2 &= "AW2.Item_Value AS AwardLocation, "
        sql2 &= "AW3.Item_Value AS AwardFutured, "
        sql2 &= "AW4.ProjectID, "
        sql2 &= "AW5.Name, "
        sql2 &= "P6.Item_Value As WebDescription, "
        sql2 &= "case when isnull(AW5.State_id,'') <> '' or AW5.State_id is null then AW5.City + case when isnull(S.Name ,'') <> '' then ', ' + S.Name else '' end else AW5.City + case when isnull(P5.Item_Value,'') <> '' then ', ' + P5.Item_Value else '' end + case when isnull(P4.Item_Value,'') <> '' then ', ' + P4.Item_Value else '' end end as ProcjetAddress "
        sql2 &= "from IPM_AWARDS AW "
        sql2 &= "left join IPM_AWARDS_FIELD_VALUE AW1 on AW.AWARDS_ID = AW1.AWARDS_ID and AW1.Item_ID = (Select Item_ID From IPM_AWARDS_FIELD_DESC where Item_Tag = 'IDAM_AWARDTITLE') "
        sql2 &= "left join IPM_AWARDS_FIELD_VALUE AW2 on AW.AWARDS_ID = AW2.AWARDS_ID and AW2.Item_ID = (Select Item_ID From IPM_AWARDS_FIELD_DESC where Item_Tag = 'IDAM_AWARDLOCATION') "
        sql2 &= "left join IPM_AWARDS_FIELD_VALUE AW3 on AW.AWARDS_ID = AW3.AWARDS_ID and AW3.Item_ID = (Select Item_ID From IPM_AWARDS_FIELD_DESC where Item_Tag = 'IDAM_AWARDFEATURE') "
       
        sql2 &= "left join IPM_AWARDS_RELATED_PROJECTS AW4 on AW4.Awards_Id = AW.Awards_Id "
        sql2 &= "left join IPM_PROJECT AW5 on AW5.ProjectID = AW4.ProjectID "

        sql2 &= "left join IPM_PROJECT_FIELD_VALUE P3 on AW5.ProjectID = P3.ProjectID and P3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_CITY') "
        sql2 &= "left join IPM_PROJECT_FIELD_VALUE P4 on AW5.ProjectID = P4.ProjectID and P4.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_COUNTRY') "
        sql2 &= "left join IPM_PROJECT_FIELD_VALUE P5 on AW5.ProjectID = P5.ProjectID and P5.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_STATE') "
        sql2 &= "left join IPM_STATE S on S.State_id = AW5.State_id "

        sql2 &= "left join IPM_PROJECT_FIELD_VALUE P6 on AW5.ProjectID = P6.ProjectID and P6.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_DESCRIPTION_LONG') "
        sql2 &= "where 1=1 "
        If AwardId = "" Then
            sql2 &= "and AW3.Item_Value = '1' "
            sql2 &= "and (P6.Item_Value <> '' or P6.Item_Value is not null ) "
            sql2 &= "and AW.Picture = 1 "
            sql2 &= "ORDER BY NEWID() "
        Else
            sql2 &= "and AW.Awards_Id = '" & AwardId & "'"
        End If
        FuturedAwardsDT = New DataTable("FuturedAwardsDT")
        FuturedAwardsDT = mmfunctions.GetDataTable(sql2)

        If FuturedAwardsDT.Rows.Count > 0 Then
            Dim PrFriendlyLink As String = "project_" & FuturedAwardsDT.Rows(0)("ProjectID").ToString.Trim & "_" & formaturl.friendlyurl(FuturedAwardsDT.Rows(0)("Name").ToString.Trim)
            out1 &= "<div class=""project_info"">"
            out1 &= "<div class=""text"">" & formatfunctionssimple.AutoFormatText(FuturedAwardsDT.Rows(0)("Headline").ToString.Trim)
            out1 &= "<p>" & formatfunctions.AutoFormatText(FuturedAwardsDT.Rows(0)("Content").ToString.Trim) & "</p>"
            out1 &= "<span class=""red_text""><a href=""" & PrFriendlyLink & """ title=""View Project"">"&  formatfunctions.AutoFormatText(     FuturedAwardsDT.Rows(0)("Name").ToString.Trim    ) &"</a></span>"


            out1 &= "<p>" & formatfunctions.AutoFormatText(FuturedAwardsDT.Rows(0)("ProcjetAddress").ToString.Trim) & "</p>"
            out1 &= "<p>" & formatfunctions.AutoFormatText(FuturedAwardsDT.Rows(0)("Contact").ToString.Trim) & "</p>"

            out1 &= "<p class=""FirstLines"">" & CMSFunctions.GetChars(formatfunctions.AutoFormatText(FuturedAwardsDT.Rows(0)("WebDescription").ToString.Trim), 70) & "</p>"

            out1 &= "<a href=""" & PrFriendlyLink & """ title=""View Project"">View Project</a>"
            out1 &= "</div>"
            out1 &= "</div>"
            out1 &= "<img src=""/dynamic/image/week/awards/best/460x330/92/ffffff/Center/" & FuturedAwardsDT.Rows(0)("Awards_Id").ToString.Trim & ".jpg"" alt=""""/>"

            If Awardid <> "" Then
                AwardName = "Perkins Eastman: " & formatfunctionssimple.AutoFormatText(FuturedAwardsDT.Rows(0)("Headline").ToString.Trim)
            End If

        End If

        If AwardId = "" Then
            'Years
            Dim sql As String = ""
            sql &= "select distinct YEAR(Post_Date) AS Year from IPM_AWARDS AW "
            sql &= "join IPM_AWARDS_RELATED_PROJECTS AW4 on AW4.Awards_Id = AW.Awards_Id "
            sql &= "join IPM_PROJECT AW5 on AW5.ProjectID = AW4.ProjectID "
            sql &= "WHERE AW5.Available = 'Y' "
            sql &= "AND AW5.Show = 1 "
            sql &= "and AW.post_date < getdate() "
            sql &= "and AW.pull_date > getdate() "
            sql &= "and AW.Show = 1 "
            sql &= "and YEAR(Post_Date) > " & (Date.Today.Year - 5)
            sql &= "order by Year desc "

            YearsDT = New DataTable("YearsDT")
            YearsDT = mmfunctions.GetDataTable(sql)
            For Y As Integer = 0 To YearsDT.Rows.Count - 1
                'Awards Related Projects
                Dim AwardsRelatedProjectsSql As String = ""
                AwardsRelatedProjectsSql &= "select "
                AwardsRelatedProjectsSql &= "RP.ProjectID, "
                AwardsRelatedProjectsSql &= "P.Name as ProjectName, "
                AwardsRelatedProjectsSql &= "P1.Item_Value as Region, "
                AwardsRelatedProjectsSql &= "P2.Item_Value as Practice, "
                AwardsRelatedProjectsSql &= "P8.Item_Value as SubPractice "
                AwardsRelatedProjectsSql &= "FROM IPM_AWARDS_RELATED_PROJECTS RP "
                AwardsRelatedProjectsSql &= "JOIN IPM_PROJECT P ON P.ProjectID = RP.ProjectID "
                AwardsRelatedProjectsSql &= "JOIN IPM_AWARDS A ON A.Awards_Id = RP.Awards_Id "
                AwardsRelatedProjectsSql &= "left join IPM_PROJECT_FIELD_VALUE P1 on P.ProjectID = P1.ProjectID and P1.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_REGIONS') "
                AwardsRelatedProjectsSql &= "left join IPM_PROJECT_FIELD_VALUE P2 on P.ProjectID = P2.ProjectID and P2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PRACTICEAREA') "
                AwardsRelatedProjectsSql &= "left join IPM_PROJECT_FIELD_VALUE P8 on P.ProjectID = P8.ProjectID and P8.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUBPRACTICEAREA') "
                AwardsRelatedProjectsSql &= "where P.Available = 'Y' "
                AwardsRelatedProjectsSql &= "and P.Show = 1 "
                AwardsRelatedProjectsSql &= "and A.Show = 1 "
                AwardsRelatedProjectsSql &= "and A.post_date < getdate() "
                AwardsRelatedProjectsSql &= "and A.pull_date > getdate() "
                AwardsRelatedProjectsSql &= "and YEAR(A.Post_Date) =" & YearsDT.Rows(Y)("Year").ToString.Trim & " "
                AwardsRelatedProjectsSql &= "group by RP.ProjectID, P.Name, P1.Item_Value, P2.Item_Value, P8.Item_Value order by P.Name asc "
                Dim AwardsRelatedProjectsDT As DataTable
                AwardsRelatedProjectsDT = New DataTable("AwardsRelatedProjectsDT")
                AwardsRelatedProjectsDT = mmfunctions.GetDataTable(AwardsRelatedProjectsSql)
                out &= "<dl>"
                out &= "<dt>" & formatfunctions.AutoFormatText(YearsDT.Rows(Y)("Year").ToString.Trim) & "</dt>"
                For RP As Integer = 0 To AwardsRelatedProjectsDT.Rows.Count - 1
                    Dim PrFriendlyLink As String = ""
                    If AwardsRelatedProjectsDT.Rows(RP)("ProjectID").ToString.Trim <> "" And (AwardsRelatedProjectsDT.Rows(RP)("Practice").ToString.Trim <> "" Or AwardsRelatedProjectsDT.Rows(RP)("Region").ToString.Trim <> "" Or AwardsRelatedProjectsDT.Rows(RP)("SubPractice").ToString.Trim <> "") Then
                        PrFriendlyLink = "project_" & AwardsRelatedProjectsDT.Rows(RP)("ProjectID").ToString.Trim & "_" & CMSFunctions.FormatFriendlyUrl(AwardsRelatedProjectsDT.Rows(RP)("ProjectName").ToString.Trim, "_")
                    End If
                    out &= "<dd>"
                    out &= "<strong class=""awardProject"">"
                    If PrFriendlyLink <> "" Then
                        out &= "<a href=""" & PrFriendlyLink & """ title="""">"
                    End If
                    out &= formatfunctionssimple.AutoFormatText(AwardsRelatedProjectsDT.Rows(RP)("ProjectName").ToString.Trim)
                    If PrFriendlyLink <> "" Then
                        out &= "</a>"
                    End If
                    out &= "</strong>"

                    Dim AwardsNoCategory As String = ""
                    AwardsNoCategory &= "select "
                    AwardsNoCategory &= "AW.Awards_Id, "
                    AwardsNoCategory &= "AW.Headline, "
                    AwardsNoCategory &= "AW.Content, "
                    AwardsNoCategory &= "AW.PublicationTitle, "
                    AwardsNoCategory &= "AW.Creation_Date, "
                    AwardsNoCategory &= "AW1.Item_Value AS AwardTitle, "
                    AwardsNoCategory &= "AW2.Item_Value AS AwardLocation, "
                    AwardsNoCategory &= "AW3.Item_Value AS AwardFutured, "
                    AwardsNoCategory &= "AW4.ProjectID, "
                    AwardsNoCategory &= "AW5.Name, "
                    AwardsNoCategory &= "AW5.Available, "
                    AwardsNoCategory &= "AW5.Show "
                    AwardsNoCategory &= "from IPM_AWARDS AW "
                    AwardsNoCategory &= "left join IPM_AWARDS_FIELD_VALUE AW1 on AW.AWARDS_ID = AW1.AWARDS_ID and AW1.Item_ID = (Select Item_ID From IPM_AWARDS_FIELD_DESC where Item_Tag = 'IDAM_AWARDTITLE') "
                    AwardsNoCategory &= "left join IPM_AWARDS_FIELD_VALUE AWC on AW.AWARDS_ID = AWC.AWARDS_ID and AWC.Item_ID = (Select Item_ID From IPM_AWARDS_FIELD_DESC where Item_Tag = 'IDAM_AWARD_TYPE') "
                    AwardsNoCategory &= "left join IPM_AWARDS_FIELD_VALUE AW2 on AW.AWARDS_ID = AW2.AWARDS_ID and AW2.Item_ID = (Select Item_ID From IPM_AWARDS_FIELD_DESC where Item_Tag = 'IDAM_AWARDLOCATION') "
                    AwardsNoCategory &= "left join IPM_AWARDS_FIELD_VALUE AW3 on AW.AWARDS_ID = AW3.AWARDS_ID and AW3.Item_ID = (Select Item_ID From IPM_AWARDS_FIELD_DESC where Item_Tag = 'IDAM_AWARDFEATURE') "
                    AwardsNoCategory &= "join IPM_AWARDS_RELATED_PROJECTS AW4 on AW4.Awards_Id = AW.Awards_Id "
                    AwardsNoCategory &= "join IPM_PROJECT AW5 on AW5.ProjectID = AW4.ProjectID "
                    AwardsNoCategory &= "where AW5.ProjectID = " & AwardsRelatedProjectsDT.Rows(RP)("ProjectID").ToString.Trim & ""
                    AwardsNoCategory &= "and AW.Show = 1 "
                    AwardsNoCategory &= "and AW5.Available = 'Y' "
                    AwardsNoCategory &= "and AW5.Show = 1 "
                    AwardsNoCategory &= "and AW.post_date < getdate() "
                    AwardsNoCategory &= "and AW.pull_date > getdate() "
                    AwardsNoCategory &= "and (AWC.Item_Value = '' or AWC.Item_Value is null) "
                    AwardsNoCategory &= "and YEAR(AW.Post_Date) = " & YearsDT.Rows(Y)("Year").ToString.Trim & " "
                    AwardsNoCategory &= "ORDER BY Headline asc "
                    Dim AwardsNoCategoryDT As DataTable
                    AwardsNoCategoryDT = New DataTable("AwardsNoCategoryDT")
                    AwardsNoCategoryDT = mmfunctions.GetDataTable(AwardsNoCategory)
                    If AwardsNoCategoryDT.Rows.Count > 0 Then
                        'AWARDS
                        For A As Integer = 0 To AwardsNoCategoryDT.Rows.Count - 1
                            out &= "<em class=""awardCat"">" & formatfunctionssimple.AutoFormatText(AwardsNoCategoryDT.Rows(A)("Headline").ToString.Trim) & ", " & "</em>"
                            If A + 1 = AwardsNoCategoryDT.Rows.Count Then
                                out &= "<span class=""awardName"">" & formatfunctions.AutoFormatText(AwardsNoCategoryDT.Rows(A)("PublicationTitle").ToString.Trim) & "</span>"
                            Else
                                out &= "<span class=""awardName"">" & formatfunctions.AutoFormatText(AwardsNoCategoryDT.Rows(A)("PublicationTitle").ToString.Trim) & "; " & "</span>"
                            End If
                        Next
                    End If

                    Dim AwardCategorySql As String = ""
                    AwardCategorySql &= "select "
                    AwardCategorySql &= "AW1.Item_Value AS AwardCategory "
                    AwardCategorySql &= "from IPM_AWARDS AW "
                    AwardCategorySql &= "left join IPM_AWARDS_FIELD_VALUE AW1 on AW.AWARDS_ID = AW1.AWARDS_ID and AW1.Item_ID = (Select Item_ID From IPM_AWARDS_FIELD_DESC where Item_Tag = 'IDAM_AWARD_TYPE') "
                    AwardCategorySql &= "left join IPM_AWARDS_RELATED_PROJECTS AW4 on AW4.Awards_Id = AW.Awards_Id "
                    AwardCategorySql &= "left join IPM_PROJECT AW5 on AW5.ProjectID = AW4.ProjectID "
                    AwardCategorySql &= "WHERE AW.Show = 1 "
                    AwardCategorySql &= "AND AW5.Available = 'Y' "
                    AwardCategorySql &= "AND AW5.Show = 1 "
                    AwardCategorySql &= "and AW.post_date < getdate() "
                    AwardCategorySql &= "and AW.pull_date > getdate() "
                    AwardCategorySql &= "and AW1.Item_Value <> '' and AW1.Item_Value is not null "
                    AwardCategorySql &= "and AW5.ProjectID = " & AwardsRelatedProjectsDT.Rows(RP)("ProjectID").ToString.Trim & " "
                    AwardCategorySql &= "and YEAR(AW.Post_Date) =" & YearsDT.Rows(Y)("Year").ToString.Trim & " "
                    AwardCategorySql &= "Group BY AW1.Item_Value "
                    AwardCategorySql &= "ORDER BY AW1.Item_Value asc "
                    Dim AwardCategoryDT As DataTable
                    AwardCategoryDT = New DataTable("AwardCategoryDT")
                    AwardCategoryDT = mmfunctions.GetDataTable(AwardCategorySql)
                    If AwardCategoryDT.Rows.Count > 0 Then
                        For AC As Integer = 0 To AwardCategoryDT.Rows.Count - 1
                            Dim sql1 As String = ""
                            sql1 &= "select "
                            sql1 &= "AW.Awards_Id, "
                            sql1 &= "AW.Headline, "
                            sql1 &= "AW.Content, "
                            sql1 &= "AW.PublicationTitle, "
                            sql1 &= "AW.Creation_Date, "
                            sql1 &= "AW1.Item_Value AS AwardTitle, "
                            sql1 &= "AW2.Item_Value AS AwardLocation, "
                            sql1 &= "AW3.Item_Value AS AwardFutured, "
                            sql1 &= "AW4.ProjectID, "
                            sql1 &= "AW5.Name, "
                            sql1 &= "AW5.Available, "
                            sql1 &= "AW5.Show "
                            sql1 &= "from IPM_AWARDS AW "
                            sql1 &= "left join IPM_AWARDS_FIELD_VALUE AW1 on AW.AWARDS_ID = AW1.AWARDS_ID and AW1.Item_ID = (Select Item_ID From IPM_AWARDS_FIELD_DESC where Item_Tag = 'IDAM_AWARDTITLE') "
                            sql1 &= "left join IPM_AWARDS_FIELD_VALUE AWC on AW.AWARDS_ID = AWC.AWARDS_ID and AWC.Item_ID = (Select Item_ID From IPM_AWARDS_FIELD_DESC where Item_Tag = 'IDAM_AWARD_TYPE') "
                            sql1 &= "left join IPM_AWARDS_FIELD_VALUE AW2 on AW.AWARDS_ID = AW2.AWARDS_ID and AW2.Item_ID = (Select Item_ID From IPM_AWARDS_FIELD_DESC where Item_Tag = 'IDAM_AWARDLOCATION') "
                            sql1 &= "left join IPM_AWARDS_FIELD_VALUE AW3 on AW.AWARDS_ID = AW3.AWARDS_ID and AW3.Item_ID = (Select Item_ID From IPM_AWARDS_FIELD_DESC where Item_Tag = 'IDAM_AWARDFEATURE') "
                            sql1 &= "join IPM_AWARDS_RELATED_PROJECTS AW4 on AW4.Awards_Id = AW.Awards_Id "
                            sql1 &= "join IPM_PROJECT AW5 on AW5.ProjectID = AW4.ProjectID "
                            sql1 &= "where AW5.ProjectID = " & AwardsRelatedProjectsDT.Rows(RP)("ProjectID").ToString.Trim & " "
                            sql1 &= "and AW.Show = 1 "
                            sql1 &= "and AW5.Available = 'Y' "
                            sql1 &= "and AW5.Show = 1 "
                            sql1 &= "and AW.post_date < getdate() "
                            sql1 &= "and AW.pull_date > getdate() "
                            sql1 &= "and AWC.Item_Value = '" & AwardCategoryDT.Rows(AC)("AwardCategory").ToString.Trim & "' "
                            sql1 &= "and YEAR(AW.Post_Date) =" & YearsDT.Rows(Y)("Year").ToString.Trim & " "
                            sql1 &= "ORDER BY Headline asc "
                            AwardsDT = New DataTable("AwardsDT")
                            AwardsDT = mmfunctions.GetDataTable(sql1)
                            out &= "<strong class=""awardType"">" & AwardCategoryDT.Rows(AC)("AwardCategory").ToString.Trim & "</strong>"
                            For cataw As Integer = 0 To AwardsDT.Rows.Count - 1
                                out &= "<em class=""awardCat"">" & formatfunctionssimple.AutoFormatText(AwardsDT.Rows(cataw)("Headline").ToString.Trim) & ": " & "</em>"

                                If cataw + 1 = AwardsDT.Rows.Count Then
                                    out &= "<span class=""awardName"">" & formatfunctions.AutoFormatText(AwardsDT.Rows(cataw)("PublicationTitle").ToString.Trim) & "</span>"
                                Else
                                    out &= "<span class=""awardName"">" & formatfunctions.AutoFormatText(AwardsDT.Rows(cataw)("PublicationTitle").ToString.Trim) & "; " & "</span>"
                                End If
                            Next
                        Next
                    End If
                    out &= "</dd>"
                Next
                out &= "</dl>"
            Next
        End If





        Dim out2 As String = ""
        Dim sql3 As String = ""
        If FuturedAwardsDT.Rows.Count > 0 Then
            sql3 &= "select "
            sql3 &= "U.UserID, "
            sql3 &= "U1.Item_Value As Testimonial, "
            sql3 &= "U2.Item_Value As Link, "
            sql3 &= "U3.Item_Value as RelId, "
            sql3 &= "U4.Item_Value as SubTestimonial "
            sql3 &= "from IPM_USER U "
            sql3 &= "left join IPM_USER_FIELD_VALUE U1 on U.UserID = U1.USER_ID and U1.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTIMONIAL') "
            sql3 &= "left join IPM_USER_FIELD_VALUE U2 on U.UserID = U2.USER_ID and U2.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USERLINK') "
            sql3 &= "left join IPM_USER_FIELD_VALUE U3 on U.UserID = U3.USER_ID and U3.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTIIDS') "
            sql3 &= "left join IPM_USER_FIELD_VALUE U4 on U.UserID = U4.USER_ID and U4.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTISUB') "
            sql3 &= "left join IPM_USER_FIELD_VALUE U5 on u.UserID = U5.USER_ID and U5.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_AFFILIATECHECK') "
            sql3 &= "where Active = 'Y' "
            sql3 &= "and u.Contact = 1 and U5.Item_Value = '' "
            sql3 &= "and charindex('" & FuturedAwardsDT.Rows(0)("Awards_Id").ToString.Trim & "',U3.Item_Value)!= 0 "
        End If
        AwardsTestimonial = New DataTable("AwardsTestimonial")
        If FuturedAwardsDT.Rows.Count > 0 Then
            AwardsTestimonial = mmfunctions.GetDataTable(sql3)
        End If

        If AwardsTestimonial.Rows.Count > 0 Then
            out2 &= "<div class=""text"">"
            out2 &= "<p class=""red_text_special"">" & formatfunctions.AutoFormatText(AwardsTestimonial.Rows(0)("Testimonial").ToString.Trim) & "</p>"
            out2 &= "</div>"
            out2 &= "<a href=""" & AwardsTestimonial.Rows(0)("Link").ToString.Trim & """ class=""featured_consult"" title="""">"
            out2 &= "<span>" & formatfunctions.AutoFormatText(AwardsTestimonial.Rows(0)("SubTestimonial").ToString.Trim) & "</span>"
            out2 &= "<b>Read More</b>"
            out2 &= "<img src=""/dynamic/image/week/contact/best/220x145/92/ffffff/Center/" & AwardsTestimonial.Rows(0)("UserID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
            out2 &= "</a>"
        End If

        highlighted_award.Text = out1
        awards_list1.Text = out
        award_testimonial.Text = out2

        Dim LastVisitedCookie As New HttpCookie("LastVisited")
        LastVisitedCookie.Values("awards") = "clients," & ""
        LastVisitedCookie.Values("lastVisit") = DateTime.Now.ToString()
        LastVisitedCookie.Expires = DateTime.Now.AddDays(1)
        Response.Cookies.Add(LastVisitedCookie)

    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub

End Class
