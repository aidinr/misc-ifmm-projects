﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="404.aspx.vb" Inherits="err_404" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="assets/js/search.js" type="text/javascript"></script>
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="/" class="first">home</a><span> | </span></li>
                        <li><a class="active">Page Not Found</a></li>                        
                    </ul>
                </div>
                
                <h3 class="sub_title xtra_letter_spacing">Page Not Found</h3>
                <h2 class="search cms cmsType_TextMulti cmsName_Not_Found"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Not_Found")%></h2>
                
                <div class="left_bigger_right_smaller_layout">
                    <div class="left">
                        <form method="get" action="search" class="search_form" id="searchForm">
                            <input type="text" name="q" id="s" value="Search Term"/>
                            <button type="submit">GO</button>
                        </form>                        

<div class="text">
                     <h2 id="searchMsg"></h2>
                     <div id="resultsDiv"></div>  
</div>
                        
                    </div>
                </div>
</asp:Content>

