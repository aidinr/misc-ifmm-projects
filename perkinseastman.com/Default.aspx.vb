﻿Imports System.Data

Partial Class _Default
    Inherits System.Web.UI.Page
    Public CarouselDT As DataTable


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim out As String = ""
        Dim sql As String = ""
        Dim iii As Integer = 0

        sql &= "SELECT TOP 12 "
        sql &= "CI.Asset_ID, "
        sql &= "P.ProjectID, "
        sql &= "P.Name, "
        sql &= "P6.Item_Value AS ShortName, "
        sql &= "case when isnull(P.State_id,'') <> '' or P.State_id is null then P.City + case when isnull(S.Name ,'') <> '' then ', ' + S.Name else '' end else p.City + case when isnull(P5.Item_Value,'') <> '' then ', ' + P5.Item_Value else '' end + case when isnull(P4.Item_Value,'') <> '' then ', ' + P4.Item_Value else '' end end as ProjectAddress "
        sql &= "FROM IPM_CARROUSEL_ITEM CI "
        sql &= "JOIN IPM_ASSET PA ON PA.Asset_ID = CI.Asset_ID AND CI.Available = 'Y' "
        sql &= "JOIN IPM_PROJECT P ON P.ProjectID = PA.ProjectID "
        sql &= "left join IPM_CARROUSEL_SORT SR on SR.Carrousel_ID = CI.Carrousel_ID AND SR.Sort_ID = 24 "
        sql &= "left join IPM_CARROUSEL_ITEM_SORT SI on SI.Sort_ID = SR.Sort_ID and SI.Item_ID = PA.Asset_ID "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P3 on P.ProjectID = P3.ProjectID and P3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_CITY') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P4 on P.ProjectID = P4.ProjectID and P4.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_COUNTRY') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P5 on P.ProjectID = P5.ProjectID and P5.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_STATE') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P6 on P.ProjectID = P6.ProjectID and P6.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_SHORT_NAME') "
        sql &= "left join IPM_STATE S on S.State_id = P.State_id "
        sql &= "where CI.Carrousel_ID = 2402101 "
        sql &= "and CI.Available = 'Y' and CI.Available_Date <= getdate() "
        sql &= "Order By SI.Sort_Value "
        CarouselDT = New DataTable("FuturedProjectDT")
        CarouselDT = mmfunctions.GetDataTable(sql)
        If CarouselDT.Rows.Count > 0 Then
            For R As Integer = 0 To CarouselDT.Rows.Count - 1
                Dim PrFriendlyLink As String = "project_" & CarouselDT.Rows(R)("ProjectID").ToString.Trim & "_" & CMSFunctions.FormatFriendlyUrl(CarouselDT.Rows(R)("Name").ToString.Trim.ToLower, "_")
                out &= "<div class=""big_image_with_info_box homepage_transparency_fix"">"

                If iii < 1 Then
                    out &= "<img src=""/dynamic/image/week/asset/best/940x687/92/111111/South/" & CarouselDT.Rows(R)("Asset_ID").ToString.Trim & ".jpg"" alt=""""/>"
                Else
                    out &= "<img src=""assets/images/clear.png"" class=""loadMe"" longdesc=""/dynamic/image/week/asset/best/940x687/92/111111/South/" & CarouselDT.Rows(R)("Asset_ID").ToString.Trim & ".jpg"" alt=""""/>"
                End If
                iii = iii + 1

                If CarouselDT.Rows(R)("Name").ToString.Trim <> "" Then
                    out &= "<div class=""info_pop home_pop"">"
                    out &= " <h2>" & CarouselDT.Rows(R)("Name").ToString.Trim & "</h2>"
                    out &= "<span class=""openSans"">" & CarouselDT.Rows(R)("ProjectAddress").ToString.Trim & "</span>"
                    out &= "<a href=""" & PrFriendlyLink & """  title="""">Read More</a>"
                    out &= "</div>"
                End If
                out &= "</div>"



            Next

            carousel_images.Text = out


        End If

        Dim LastVisitedCookie As New HttpCookie("LastVisited")
        LastVisitedCookie.Values("parent") = "home," & ""
        LastVisitedCookie.Values("lastVisit") = DateTime.Now.ToString()
        LastVisitedCookie.Expires = DateTime.Now.AddDays(1)
        Response.Cookies.Add(LastVisitedCookie)


    End Sub


    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, "index")
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, "index")
        End If
    End Sub



End Class
