﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="eek_page.aspx.vb" Inherits="eek_page" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first">about</a><span> | </span></li>
                        <li><a href="<%=Master.UrlPage%>" class="active">EE&amp;K a perkins eastman company</a></li>                        
                    </ul>
                </div>
                
                <h3 class="sub_title">
                    
                    <img 
                         src="cms/data/Sized/best/249x82/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "eek_page_eek_logo")%>" 
                         alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "eek_page_eek_logo")%>" 
                         title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "eek_page_eek_logo")%>" 
                         class="<%="cms cmsType_Image cmsName_eek_page_eek_logo"%>" />
                </h3>
                
                <div class="company_inner_layout">
                    <div class="left">

                            <div class="big_image_with_info_box" id="box_5">
<asp:Literal runat="server" ID="futured_project"/>        
                            </div>  


                    </div>
                    <div class="right mt-5">

                            <div class="cms cmsType_Rich cmsName_Eek_Page_Intro">
                            <p><%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Eek_Page_Intro")%></p>
                            </div> 

                        <div class="gray_box_wrap">
                            <a href="eek_projects" title="" class="view_more">View Selected Projects</a>
                            <a href="http://www.eekarchitects.com" title="" class="view_more">Visit the EE&amp;K Website</a>
                        </div>

                        
                    </div>
                </div>
</asp:Content>