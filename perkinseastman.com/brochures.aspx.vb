﻿Imports System.Data
Imports System.IO

Partial Class brochures
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sql As String = ""
        sql &= "select ipm_asset.asset_id, u2.item_value Issuu from ipm_asset join ipm_asset_field_value u1 on u1.asset_id = ipm_asset.asset_id and u1.item_id = 2400401 and ltrim(rtrim(isnull(u1.item_value, ''))) = '1' left outer join ipm_asset_field_value u2 on u2.asset_id = ipm_asset.asset_id and u2.item_id = 3425766 left outer join ipm_asset_field_value u3 on u3.asset_id = ipm_asset.asset_id and u3.item_id = 2400079 where projectid = 3409169 and category_id = 3425190 and active = 1 and available = 'Y' order by convert(int, isnull(u3.item_value, '0')) asc"
        Dim BrochuresDT As DataTable
        BrochuresDT = New DataTable("BrochuresDT")
        BrochuresDT = mmfunctions.GetDataTable(sql)
        If BrochuresDT.Rows.Count > 0 Then
            rptBrochures.DataSource = BrochuresDT
            rptBrochures.DataBind()
        End If
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub


End Class
