﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="affiliates.aspx.vb" Inherits="affiliates" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_About"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "About")%></a><span> | </span></li>
                        <li><a class="cmsMultiLanguage cms cmsType_TextSingle cmsName_Affiliates" href="<%=Master.UrlPage%>" class="active"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Affiliates")%></a></li>
                    </ul>
                </div>
                

                <div class="leadership_headline">
                    <h3 class="sub_title hrule"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Affiliates")%></h3>
               
                
               <!--
                                SLides INFO:                    
                                id="slide_progression" DEFAULT VALUE = 1
                                id="max_slides" HERE you put SLIDES COUNT in my case 4
                                Different sldies are with ID="slide_1" ID="slide_2" e.t.c. :)
                            -->

                          <%--  <%  If TotalPages > 1 Then%>
                            <div class="slide_controls">
                                <ul>
                                    <li><a href="#" title="Previous" class="previous_slide">Previous</a></li>
                                    <li>Page <span id="slide_progression">1</span> of <span id="max_slides"><%Response.Write(TotalPages)%></span></li>
                                    <li><a href="#" title="Next" class="next_slide">Next</a></li>
                                </ul>
                            </div>
                            <%End If%>--%>

                         </div>                
                        
                        <%--<div class="slides_holder">--%>
                           <!-- Every 4th elements needs to have class="last_in_Row" -->
                              <asp:Literal runat="server" ID="leadership_principals_list1"/>       
                        <%--</div>   --%>                     
 
                        <!--
                            Testimonial POPUPS INFO:
                            the popup div ID needs to be the value of the associate a HREF
                            ex: <div id="pop_10"> and <a href="#pop_10">
                        -->           

                        <%--<div class="testimonial_popups">
                           <asp:Literal runat="server" ID="leadership_bio_list1"/>               
                        </div>--%>

</asp:Content>

