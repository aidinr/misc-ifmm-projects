﻿Imports System.Web
Imports System.Data.SqlClient
Imports System.Data

Partial Class sitemap
    Inherits System.Web.UI.Page
    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.AddHeader("Content-type", "application/xml")
        Dim out As String = "<?xml version=""1.0"" encoding=""UTF-8"" ?>" & vbNewLine
        out &= "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">" & vbNewLine
        Dim url() As String = { _
        "/", _
        "/practice_3409338_education", _
        "/practice_3409343_urbanism", _
        "/corporate_interiors", _
        "/corporate_interiors_strategies", _
        "/corporate_interiors_brand_development", _
        "/corporate_interiors_commercial_renovations", _
        "/category_2400012_corporate_interiors", _
        "/category_2400014_higher_education", _
        "/category_2400017_k12_education", _
        "/category_3409328_campus_planning", _
        "/category_3409341_town_and_gown", _
        "/category_2400024_healthcare", _
        "/category_2400015_hospitality", _
        "/category_2400016_housing", _
        "/category_2400018_office_retail", _
        "/category_2400019_cultural", _
        "/category_2400020_science_technology", _
        "/category_2400021_senior_living", _
        "/category_2400022_urban_design", _
        "/category_3409345_downtown_redevelopment", _
        "/category_3409347_new_town_planning", _
        "/category_3409349_transit_and_development", _
        "/category_3409351_waterfront", _
        "/clients_2400012_Corporate_Interiors", _
        "/clients_2400024_Healthcare", _
        "/clients_3409341_Town_and_Gown", _
        "/clients_3409328_Campus_Planning", _
        "/clients_2400017_K12_Education", _
        "/clients_2400014_Higher_Education", _
        "/clients_2400018_Office_and_Retail", _
        "/clients_2400016_Housing", _
        "/clients_2400015_Hospitality", _
        "/clients_3409351_Waterfront", _
        "/clients_3409349_Transit_and_Development", _
        "/clients_3409347_New_Town_Planning", _
        "/clients_3409345_Downtown_Redevelopment", _
        "/clients_2400022_Urban_Design", _
        "/clients_2400021_Senior_Living", _
        "/clients_2400020_Science_and_Technology", _
        "/clients_2400019_Public_and_Cultural", _
        "/region_intro", _
        "/region_Africa_and_the_Middle_East", _
        "/region_Asia", _
        "/region_China", _
        "/region_Europe", _
        "/region_The_Americas", _
        "/countrys", _
        "/contact", _
        "/design_approach", _
        "/sustainability", _
        "/our_process", _
        "/firm_profile", _
        "/eek_page", _
        "/affiliates", _
        "/leadership_executive_directors", _
        "/leadership_principals", _
        "/leadership_associates", _
        "/leadership_affiliates", _
        "/honor_awards", _
        "/white_papers", _
        "/speaking_engagement", _
        "/publications", _
        "/login", _
        "/careers", _
        "/news_press_release", _
        "/news_current_press" _
        }

        Dim yesterday As String = Date.Now.AddDays(-1).ToString("yyyy-MM-dd")
        For i As Integer = 0 To url.Length - 1
            out &= "<url><loc>http://www.perkinseastman.com" & url(i).Trim & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
        Next
        '----------------Affiliates----------------'
        Dim AffiliatesSql As String = ""
        AffiliatesSql &= "select "
        AffiliatesSql &= "u.UserID, "
        AffiliatesSql &= "u.agency As CompanyName "
        AffiliatesSql &= "from IPM_USER u "
        AffiliatesSql &= "left join IPM_USER_FIELD_VALUE vs on u.UserID = vs.USER_ID and vs.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USER_ORDERING') "
        AffiliatesSql &= "left join IPM_USER_FIELD_VALUE U1 on u.UserID = U1.USER_ID and U1.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_AFFILIATECHECK') "
        AffiliatesSql &= "left join IPM_USER_FIELD_VALUE U2 on u.UserID = U2.USER_ID and U2.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_AFFILIATEDESCRIPTION') "
        AffiliatesSql &= "left join IPM_USER_FIELD_VALUE U3 on u.UserID = U3.USER_ID and U3.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_AFFILIATEWEBSITE') "
        AffiliatesSql &= "where u.Active = 'Y' "
        AffiliatesSql &= "and u.Contact = '1' and U1.Item_Value = '1' "
        Dim AffiliatesDT As DataTable
        AffiliatesDT = New DataTable("AffiliatesDT")
        AffiliatesDT = mmfunctions.GetDataTable(AffiliatesSql)
        For R As Integer = 0 To AffiliatesDT.Rows.Count - 1
            Dim AffiliateFriendlyLink As String = ""
            AffiliateFriendlyLink = "affiliate_" & AffiliatesDT.Rows(R)("UserID").ToString.Trim & "_" & CMSFunctions.FormatFriendlyUrl(AffiliatesDT.Rows(R)("CompanyName").ToString.Trim.ToLower, "_")
            out &= "<url><loc>http://www.perkinseastman.com/" & AffiliateFriendlyLink & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>1.0</priority></url>" & vbNewLine
        Next
        ''----------------Case Study----------------'
        'Dim CaseStudySql As String = ""
        'CaseStudySql &= "select "
        'CaseStudySql &= "p.ProjectID, "
        'CaseStudySql &= "'Case Study ' + cast(ROW_NUMBER() OVER(order by cast(isnull(p11.Item_Value, 9999999999) as integer) desc)as varchar) + ':' as CaseStudyNo, "
        'CaseStudySql &= "p.Name as ProjectName, "
        'CaseStudySql &= "P7.Item_Value As ProjectShortName "
        'CaseStudySql &= "from IPM_PROJECT P "
        'CaseStudySql &= "left join IPM_PROJECT_FIELD_VALUE P7 on P.ProjectID = P7.ProjectID and P7.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_SHORT_NAME') "
        'CaseStudySql &= "left join IPM_PROJECT_FIELD_VALUE P9 on P.ProjectID = P9.ProjectID and P9.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_CASESTUDY_CHECK') "
        'CaseStudySql &= "left join IPM_PROJECT_FIELD_VALUE P10 on P.ProjectID = P10.ProjectID and P10.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_CASESTUDY_PAGE') "
        'CaseStudySql &= "left join IPM_PROJECT_FIELD_VALUE P11 on P.ProjectID = P11.ProjectID and P11.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_CASESTUDY_ORDER') "
        'CaseStudySql &= "where P9.Item_Value = 1 and P.Available = 'Y' and P.Show = 1 "
        'CaseStudySql &= "order by cast(isnull(p11.Item_Value, 9999999999) as integer) desc "
        'Dim CaseStudyDT As DataTable
        'CaseStudyDT = New DataTable("CaseStudyDT")
        'CaseStudyDT = mmfunctions.GetDataTable(CaseStudySql)
        'For R As Integer = 0 To CaseStudyDT.Rows.Count - 1
        '    Dim CaseStudyFriendlyLink As String = ""
        '    CaseStudyFriendlyLink = "corporate_interiors_casestudy_" & CaseStudyDT.Rows(R)("ProjectID").ToString.Trim & "_" & CMSFunctions.FormatFriendlyUrl(CaseStudyDT.Rows(R)("ProjectName").ToString.Trim.ToLower, "_")
        '    out &= "<url><loc>http://www.perkinseastman.com" & CaseStudyFriendlyLink & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>1.0</priority></url>" & vbNewLine
        'Next
        '----------------Projects----------------'
        Dim ProjectsSql As String = ""
        ProjectsSql &= "select "
        ProjectsSql &= "P.ProjectID, "
        ProjectsSql &= "P.Name as ProjectName "
        ProjectsSql &= "FROM IPM_PROJECT P "
        ProjectsSql &= "left join IPM_PROJECT_FIELD_VALUE P9 on P.ProjectID = P9.ProjectID and P9.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAN_PROJECT_ORDERING') "
        ProjectsSql &= "left join IPM_PROJECT_FIELD_VALUE P2 on P.ProjectID = P2.ProjectID and P2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PRACTICEAREA') "
        ProjectsSql &= "left join IPM_PROJECT_FIELD_VALUE P8 on P.ProjectID = P8.ProjectID and P8.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUBPRACTICEAREA') "
        ProjectsSql &= "WHERE P.Available = 'y' "
        ProjectsSql &= "and P.Show = 1 "
        ProjectsSql &= "and P2.Item_Value <> '' or P8.Item_Value <> '' "
        ProjectsSql &= "order by CAST(isnull(P9.Item_Value, 9999999999) as integer) desc"
        Dim ProjectsDT As DataTable
        ProjectsDT = New DataTable("ProjectsDT")
        ProjectsDT = mmfunctions.GetDataTable(ProjectsSql)
        For R As Integer = 0 To ProjectsDT.Rows.Count - 1
            Dim ProjectFriendlyLink As String = ""
            ProjectFriendlyLink = "project_" & ProjectsDT.Rows(R)("ProjectID").ToString.Trim & "_" & CMSFunctions.FormatFriendlyUrl(ProjectsDT.Rows(R)("ProjectName").ToString.Trim.ToLower, "_")
            out &= "<url><loc>http://www.perkinseastman.com/" & ProjectFriendlyLink & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>1.0</priority></url>" & vbNewLine
        Next
        '----------------News----------------'
        Dim NewsSql As String = ""
        NewsSql &= "Select "
        NewsSql &= "n.News_Id, "
        NewsSql &= "n.Headline, "
        NewsSql &= "n.Content, "
        NewsSql &= "n.Picture, "
        NewsSql &= "n.PDF, "
        NewsSql &= "DATENAME(mm,n.Post_Date) + ' ' + DATENAME(day,n.Post_Date) + ', ' + DATENAME(year, n.Post_Date) + COALESCE(' , ' + nullif(ltrim(v.Item_Value),''), '') DateLoc "
        NewsSql &= "From IPM_NEWS n "
        NewsSql &= "Left Join IPM_NEWS_FIELD_VALUE v ON (n.News_id = v.News_id AND v.Item_ID = 3409256) "
        NewsSql &= "Where(n.Show = 1 And n.Post_Date < GETDATE() And n.Pull_Date > GETDATE() And n.Type = 8) "
        NewsSql &= "Order By n.Post_Date DESC "
        Dim NewsDT As DataTable
        NewsDT = New DataTable("NewsDT")
        NewsDT = mmfunctions.GetDataTable(NewsSql)
        ''' For R As Integer = 0 To NewsDT.Rows.Count - 1
	For R As Integer = 0 To 0
            ''' If NewsDT.Rows(R)("PDF").ToString.Trim = "1" Then
	    If NewsDT.Rows(R)("PDF").ToString.Trim = "0" Then
                Dim NewsPdfLink As String = ""
                NewsPdfLink = "dynamic/document/week/asset/download/" & NewsDT.Rows(R)("News_ID").ToString.Trim & "/" & NewsDT.Rows(R)("News_ID").ToString.Trim & ".pdf"
                out &= "<url><loc>http://www.perkinseastman.com/" & NewsPdfLink & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>1.0</priority></url>" & vbNewLine
            End If
        Next



        out &= "</urlset>"
        Response.Write(out)

    End Sub

End Class
'
