﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="practice.aspx.vb" Inherits="practice" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_Work"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Work")%></a><span> | </span></li>
                        <li><a class="cmsMultiLanguage cms cmsType_TextSingle cmsName_By_Practice_Area"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "By_Practice_Area")%></a><span> | </span></li>
                        <li><a href="<% =Master.UrlPage%>" class="active cmsMultiLanguage cms cmsType_TextSingle cmsName_<%= new Regex("[^a-zA-Z0-9]").Replace(PracticeName, "_") %>"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", new Regex("[^a-zA-Z0-9]").Replace(PracticeName, "_")) %></a></li>
                    </ul>
                </div>
                <!-- <asp:Literal runat="server" ID="practice_name"/> -->
                <div class="headline">
                <h2 class="cmsMultiLanguage cms cmsType_TextSingle cmsName_<%= new Regex("[^a-zA-Z0-9]").Replace(PracticeName, "_") %>"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", new Regex("[^a-zA-Z0-9]").Replace(PracticeName, "_")) %></h2>
                </div>                
                <div class="wide5"><ul class="main_list futura_font practice">
         <asp:Literal runat="server" ID="subpractice_list1"/>
                </ul></div>
</asp:Content>