﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="corporate_interiors.aspx.vb" Inherits="corporate_interiors" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="breadcrumbs">
                    <ul>
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_Work"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Work")%></a><span> | </span></li>
                        <li><a class="cmsMultiLanguage cms cmsType_TextSingle cmsName_By_Practice_Area"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "By_Practice_Area")%></a><span> | </span></li>
                        <li><a href="<%=Master.UrlPage%>" class="active"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Corporate_Interiors")%></a></li>
                    </ul>
                </div>
<div class="two_columns"> 
<div class="left_smaller"> 
<h3 class="sub_title cmsMultiLanguage cms cmsType_TextSingle cmsName_Corporate_Interiors"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Corporate_Interiors")%></h3> 
<div class="cms cmsMultiLanguage learnIntro cmsType_Rich cmsName_Corporate_Interiors"> 
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Corporate_Interiors")%>
</div> 
<div class="cms cmsMultiLanguage learnMore cmsType_Rich cmsName_Corporate_Interiors_More"> 
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Corporate_Interiors_More")%>
</div> 

<dl class="contacts_list">
<dt class="cmsMultiLanguage cms cmsType_TextSingle cmsName_Corporate_Interiors_Leadership_Heading">
<%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Corporate_Interiors_Leadership_Heading")%>
</dt>
<dd>
<span></span>
<p class="cms cmsMultiLanguage cmsType_Rich cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName,"_")%>_Leadership">
<%--<%=CMSFunctions.Format(Server.MapPath("~") & "cms\data\Text\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Leadership.txt")%>--%>

<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Leadership")%>

</p>
</dd>
</dl>

<dl class="contacts_list">
<asp:Literal runat="server" ID="rel_pages_list1"/> 
</dl>
<a href="clients_2400012_Corporate_Interiors" title="" class="view_more padTop"><span class="cmsMultiLanguage cms cmsType_TextSingle cmsName_Client_List"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Client_List")%></span> </a> 
</div> 

<div class="right_bigger"> 
<div class="inner_tabs"> 
<asp:Literal runat="server" ID="big_projects_list1"/>
</div> 
</div> 

<ul class="main_list futura_font short landO"> 
                    <li>
                        <a href="category_2400012_corporate_interiors" title="">
                            <span>Projects</span>
                            <img src="cms/data/Sized/best/152x152/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "corporate_interiors_image_2")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "corporate_interiors_image_2")%>" title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "corporate_interiors_image_2")%>" class="<%="cms cmsType_Image cmsName_corporate_interiors_image_2"%>" />
                        </a>
                    </li>
                        <li>
                        <a href="corporate_interiors_strategies" title="">
                            <span>Strategies</span>
                        <img src="cms/data/Sized/best/152x152/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "corporate_interiors_image_1")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "corporate_interiors_image_1")%>" title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "corporate_interiors_image_1")%>" class="<%="cms cmsType_Image cmsName_corporate_interiors_image_1"%>" />
                        </a>
                    </li>

                    <li>
                        <a href="corporate_interiors_brand_development" title="">
                            <span>Brand Development</span>
                            <img src="cms/data/Sized/best/152x152/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "corporate_interiors_image_3")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "corporate_interiors_image_3")%>" title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "corporate_interiors_image_3")%>" class="<%="cms cmsType_Image cmsName_corporate_interiors_image_3"%>" />
                        </a>
                    </li>
                    <li class="last_in_row">
                        <a href="corporate_interiors_commercial_renovations" title="">
                            <span>Commercial Renovations</span>
                             <img src="cms/data/Sized/best/152x152/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "corporate_interiors_image_4")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "corporate_interiors_image_4")%>" title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "corporate_interiors_image_4")%>" class="<%="cms cmsType_Image cmsName_corporate_interiors_image_4"%>" />
                        </a>
                    </li>          
</ul> 



</div> 


<%
     Dim HasName As String = ""
     Dim HasSlides As String = ""
     If Directory.GetFiles(Server.MapPath("~") & "cms\data\Text\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & "*").Length > 0 Then
         HasName = "1"
     End If
     If Directory.GetFiles(Server.MapPath("~") & "cms\data\Textset\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & "*").Length > 0 Then
         HasSlides = "1"
     End If
%>

<%  If HasName = "1" Then%>
 <div class="casestudy_list">
<h3 class="headline_additional">view select case studies below</h3>
<ul class="cmsGroup cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName, "_")%>_Case_Studies">
<!--
<li><a><span class="selectTemplate cms cmsType_TextSingle cmsName_Name">Case Study:</span><strong>Case Study Name</strong></a><a class="selectTemplate cms cmsType_Textset cmsName_List"></a></li>
-->
 <%
     Dim SlideName As String
     Dim SlideLayout As String
 For i As Integer = 1 To 201 - 1
    If Directory.GetFiles(Server.MapPath("~") & "cms\data\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
%>

<%
    For x As Integer = 0 To 500
        SlideName = ""
        SlideLayout = ""
        If CMSFunctions.CheckTextFile(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List", x) Then
            SlideName = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xa" & x & ".txt")
            SlideLayout = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xb" & x & ".txt")
            SlideName = CMSFunctions.FormatFriendlyUrl(SlideName, "_")
            Exit For
        End If
    Next
    %>
<li class="cmsGroupItem">
<%  If SlideName <> "" Then%>
<a href="casestudy_<%=SlideLayout %>_<%=CategoryId %>_<%=SlideName %>">
<%End If%>
<span class="selectTemplate cms cmsType_TextSingle cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName,"_") %>_Case_Studies_Item_<%= i %>_Name">Case Study <%= i %>:</span>
<strong><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_Name")%></strong>
<%  If SlideName <> "" Then%>
</a>
<%End If%>
<a class="selectTemplate cms cmsType_Textset cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName,"_") %>_Case_Studies_Item_<%= i %>_List"></a>
</li>

<%End If %>
<%Next%>  
</ul>
</div>         
<%Else%> 
<div class="noCase cmsGroup cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName, "_")%>_Case_Studies">
<!--
<div class="casestudy_list"><ul><li><a href="#"><span class="selectTemplate cms cmsType_TextSingle cmsName_Name">Case Study:</span><strong>Example Case Study</strong></a><a class="selectTemplate cms cmsType_Textset cmsName_List"></a></li></ul></div>
-->
</div>
<%End If%>            

</asp:Content>
