﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="projectDetail.aspx.vb" Inherits="projectDetail" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%
Dim pageURL As String = HttpContext.Current.Request.RawUrl
Dim PageName As String = Path.GetFileName(Request.PhysicalPath).ToLower
pageURL = Replace(pageURL, "/", "")
If InStr(pageURL, "?") > 0 Then
pageURL = pageURL.Substring(0, pageURL.IndexOf("?"))
pageURL = Replace(pageURL, "/", "")
End If
%>
<%  
If Not File.Exists(Server.MapPath("~") & "cms\data\Metadata\" & "metadataTitle_" & pageURL.ToUpper & ".txt") Then
%>
<%= "<title>" & ProjectName & "</title>"%>
<%Else%>  
     <%If CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Metadata\" & "metadataTitle_" & pageURL.ToUpper & ".txt") <> "" Then%>
     <title><%=CMSFunctions.GetMetaTitle(Server.MapPath("~") & "cms\data\", pageURL.ToUpper)%> </title>
     <%Else%>
     <title> <%= ProjectName%>ProjectName </title>
     <%End If%>
<%End If%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <asp:Literal runat="server" ID="breadcrumbs_list"/>
                    </ul>
                </div>
                
                <div class="headline">
                    <div class="left">
                        <h2><asp:Literal runat="server" ID="project_title"/></h2>
                        <strong><asp:Literal runat="server" ID="project_location"/></strong>                        
                    </div>
                    <ul class="project_nav">
                        <asp:Literal runat="server" ID="project_nav_list"/>
                        <li><a target="_blank" href="<%=ProjectPrintLink%>">Print Page</a></li>
                    </ul>

                    <div id="EnlargeImg"><asp:Literal runat="server" ID="EnlargeImg"/></div>

                </div>

                <!-- BIG Image needs ID="BOX_number", and the SMALL_THUMBS Link needs REL="number" to open associate box -->
                <div class="inner_tabs" id="project_gallery">     
                
                     <asp:Literal runat="server" ID="big_project_image"/>                    

                    

                    <!-- Every ninth LI should have class="last_in_row" -->
                   
                            <asp:Literal runat="server" ID="small_images_list1"/>    
                                                  
                </div>   
                
                <div class="project text">
                     <asp:Literal runat="server" ID="project_description"/> 
                </div>
                <asp:Literal runat="server" ID="rel_pages_list1"/> 
<%--                <dl class="contacts_list">
                <asp:Literal runat="server" ID="publications_list1"/> 
                </dl>--%>
                <div class="c"></div>
</asp:Content>

