﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="affiliate_details.aspx.vb" Inherits="affiliate_details" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%
Dim pageURL As String = HttpContext.Current.Request.RawUrl
Dim PageName As String = Path.GetFileName(Request.PhysicalPath).ToLower
pageURL = Replace(pageURL, "/", "")
If InStr(pageURL, "?") > 0 Then
pageURL = pageURL.Substring(0, pageURL.IndexOf("?"))
pageURL = Replace(pageURL, "/", "")
End If
%>
<%  
    If Not File.Exists(Server.MapPath("~") & "cms\data\Metadata\" & "metadataTitle_" & pageURL.ToUpper & ".txt") Then
        %>
<%= "<title>" & "Perkins Eastman: Affiliates / " & System.Web.HttpUtility.HtmlEncode(CompanyName) & "</title>"%>
<%Else%>  
     <%If CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Metadata\" & "metadataTitle_" & pageURL.ToUpper & ".txt") <> "" Then%>
     <%= "<title>" & CMSFunctions.GetMetaTitle(Server.MapPath("~") & "cms\data\", pageURL.ToUpper) & "</title>"%>
     <%Else%>
     <% = "<title>" & "Perkins Eastman: Affiliates / " & formatfunctionssimple.AutoFormatText(CompanyName) & "</title>"%>
     <%End If%>
<%End If%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                         <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_About"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "About")%></a><span> | </span></li>
                        <li> | <a class="cmsMultiLanguage cms cmsType_TextSingle cmsName_Affiliates" href="affiliates" class="active"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Affiliates")%></a></li>
                        <li><a href="<%=Master.UrlPage%>" title="" class="active"><%=CompanyName%></a></li>                        
                    </ul>
                </div>
                    
                <h3 class="sub_title">
                <asp:Literal runat="server" ID="affiliate_logo"/>
                                </h3>
                <div class="company_inner_layout">
                    <div class="left">
                        <img src="cms/data/Sized/liquid/460x/65/F4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Image_" & Master.UrlPage & "")%>" alt="" title="" class="<%="cms cmsType_Image cmsName_Image_" & Master.UrlPage & ""%>" />        
                    </div>
                    <div class="right mt-5">
     <%--        <div class="cms cmsType_Rich cmsName_<%=Master.UrlPage%>_Intro">
                      <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", Master.UrlPage & "_Intro")%>
                     </div>--%>

<br /><br />
                   <asp:Literal runat="server" ID="affiliate_description"/>
                         <asp:Literal runat="server" ID="affiliate_link"/>
                         <%If CompanyId = "3414404" or CompanyId = "3421828" Then%>
                         <a href="affiliate_projects_<%=CompanyId%>_<%=CMSFunctions.FormatFriendlyUrl(CompanyName,"_")%>" title="" class="view_more cms cmsType_TextMulti cmsName_<%=Master.UrlPage%>_projects">View Selected Projects</a>
                    <%End If%>



<div class="affContact">
 <h4 class="cms cmsType_TextSingle cmsName_<%=Master.UrlPage%>_contact_heading">
 <%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", Master.UrlPage & "_contact_heading")%>
 </h4>

 <div class="cms cmsType_Rich cmsName_<%=Master.UrlPage%>_contact_content">
  <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", Master.UrlPage & "_contact_content")%>
 </div>

</div>



                    </div>
                </div>
</asp:Content>