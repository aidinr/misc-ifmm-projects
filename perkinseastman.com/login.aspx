﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="login.aspx.vb" Inherits="login" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a href="/" title="" class="first">home</a><span> | </span></li>
                        <li><a href="<%=Master.UrlPage%>" class="active">Log In</a></li>
                    </ul>
                </div>
                
                <h3 class="sub_title">Log In</h3>
                <div class="left_bigger_right_smaller_layout">
                    <div class="left">
                        
                        <div class="loginIntro cms cmsType_Rich cmsName_Login_Intro"><%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Login_Intro")%></div>
                        <div class="headline">
                            <h2 class="smaller_font">CLIENT LOG IN</h2>
                        </div>

                            <div class="cms cmsType_Rich cmsName_Client_Login">
                            <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Client_Login")%>
                            </div>      

                        <div class="headline">
                            <h2 class="smaller_font">STAFF LOG IN</h2>
                        </div>

                             <div class="cms cmsType_Rich cmsName_Staff_Login">
                            <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Staff_Login")%>
                            </div>      
                        

                    </div>
                    <div class="right no_border">
                        
                        <a target="_blank" href="<%=CMSFunctions.GetTextSimple(Server.MapPath("~") & "cms\data\", "Login_Help_Desk_Link")%>" class="view_more no_margin_top cms cmsType_TextMulti cmsName_Login_Help_Desk_Link" title="">Need help? Contact our<br/> <strong> Help Desk</strong>.</a>

                                            
                    </div>
                </div>
</asp:Content>
