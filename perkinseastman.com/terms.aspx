﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="terms.aspx.vb" Inherits="terms" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_About"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "About")%></a><span> | </span></li>
                        <li><a class="cmsMultiLanguage cms cmsType_TextSingle cmsName_Terms_And_Conditions active" href="<%=Master.UrlPage%>"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Terms_And_Conditions")%></a></li>
                    </ul>
                </div>
                <h3 class="sub_title"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Terms_And_Conditions")%></h3>
                <div class="left_bigger_right_smaller_layout">
                    <div class="left">
                        
                        <img 
                         src="cms/data/Sized/best/690x260/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Terms_Intro_Image")%>" 
                         alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "v")%>" 
                         title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Terms_Intro_Image")%>" 
                         class="<%="cms cmsType_Image cmsName_Terms_Intro_Image"%>" />

                        <div class="text bigger_space_bettween_p">
                        <div class="cms cmsType_Rich cmsName_Terms_Intro">
                            <p><%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Terms_Intro")%></p>
                            </div>                         
                        </div>                        
                    </div>
                    <div class="right no_border">
                             <div class="cms cmsType_Rich cmsName_Terms_Right">
                            <p><%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Terms_Right")%></p>
                            </div>      

                    </div>
                </div>
</asp:Content>


