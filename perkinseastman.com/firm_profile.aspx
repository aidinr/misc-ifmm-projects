﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="firm_profile.aspx.vb" Inherits="firm_profile" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first">about</a><span> | </span></li>
                        <li><a href="<%=Master.UrlPage %>" class="active">Firm Profile</a></li>                        
                    </ul>
                </div>
                
                <h3 class="sub_title">Firm Profile</h3>
                
                <div class="left_bigger_right_smaller_layout">
                    <div class="left">
                        
                        <img 
                         src="cms/data/Sized/best/690x260/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "firm_profile_image_1")%>" 
                         alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "firm_profile_image_1")%>" 
                         title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "firm_profile_image_1")%>" 
                         class="<%="cms cmsType_Image cmsName_firm_profile_image_1"%>" />

                        <div class="text bigger_space_bettween_p">
                        <div class="cms cmsType_Rich cmsName_Firm_Profile_Intro">
                            <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Firm_Profile_Intro")%>
                            </div>                         
                        </div>                        
                    </div>
                    <div class="right no_border">
                             <div class="cms cmsType_Rich cmsName_Firm_Profile_Right">
                            <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Firm_Profile_Right")%>
                            </div>      

                    </div>
                </div>
</asp:Content>