﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="how_we_work.aspx.vb" Inherits="how_we_work" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first">approach</a><span> | </span></li>
                        <li><a href="<% =Master.UrlPage%>" class="active">How We Work</a></li>                        
                    </ul>
                </div>
                
                <h3 class="sub_title">How We Work</h3>
                
                <div class="left_bigger_right_smaller_layout">
                    <div class="left">
                        
                        <img 
                         src="cms/data/Sized/best/690x260/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "how_we_work_image_1")%>" 
                         alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "how_we_work_image_1")%>" 
                         title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "how_we_work_image_1")%>" 
                         class="<%="cms cmsType_Image cmsName_how_we_work_image_1"%>" />

                        <div class="text bigger_space_bettween_p">
                        <div class="cms cmsType_Rich cmsName_How_We_Intro">
                            <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "How_We_Intro")%>
                            </div>                         
                        </div>                        
                    </div>
                    <div class="right less_padding">
                        <asp:Literal runat="server" ID="right_testimonial"/>
                    </div>
                </div>
</asp:Content>
