﻿Imports System.Data
Partial Class affiliate_details
    Inherits System.Web.UI.Page
    Public CompanyId As String
    Public CompanyName As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CompanyId = Request.QueryString("id")


        If CompanyId <> "" Then
            Dim out As String = ""
            Dim sql As String = ""
            sql &= "select "
            sql &= "u.UserID, "
            sql &= "u.agency As CompanyName, "
            sql &= "U.Description As CompanyDesc, "
            sql &= "U3.Item_Value As CompanyWebsite "
            sql &= "from IPM_USER u "
            sql &= "left join IPM_USER_FIELD_VALUE U2 on u.UserID = U2.USER_ID and U2.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_AFFILIATEDESCRIPTION') "
            sql &= "left join IPM_USER_FIELD_VALUE U3 on u.UserID = U3.USER_ID and U3.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_AFFILIATEWEBSITE') "
            sql &= "where u.UserID = " & CompanyId & ""
            Dim AffiliateDT As DataTable
            AffiliateDT = New DataTable("AffiliateDT")
            AffiliateDT = mmfunctions.GetDataTable(sql)
            If AffiliateDT.Rows.Count > 0 Then
                CompanyName = formatfunctionssimple.AutoFormatText(AffiliateDT.Rows(0)("CompanyName").ToString.Trim)

                out &= "<div class=""text"">"
                out &= formatfunctions.AutoFormatText(AffiliateDT.Rows(0)("CompanyDesc").ToString.Trim)
                out &= "</div>"

                affiliate_logo.Text = "<img src=""" & "/dynamic/image/week/contact/fit/330x85/100/ffffff/NorthWest/" & AffiliateDT.Rows(0)("UserID").ToString.Trim & ".png" & """" & " alt=""""/>"
                affiliate_description.Text = out
                If AffiliateDT.Rows(0)("CompanyWebsite").ToString.Trim.Length > 0 Then
                    affiliate_link.Text = "<a href=""" & formatfunctionssimple.AutoFormatText(AffiliateDT.Rows(0)("CompanyWebsite").ToString.Trim) & """ target=""_blank"" class=""view_more website"">Visit Website</a>"
                End If
            End If
        End If
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub

End Class
