﻿Imports System.Data

Partial Class testimonials
    Inherits System.Web.UI.Page

    Public RecordPR As Integer
    Public NewRow As Integer
    Public TestimonialsDT As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim out As String = ""
        Dim out2 As String = ""

        Dim sql As String = ""
        sql &= "select "
        sql &= "FirstName + ' ' + LastName FullName, "
        sql &= "u.Position, "
        sql &= "u.agency, "
        sql &= "u.Bio, "
        sql &= "vc.Item_Value, "
        sql &= "ISNULL(vs.Item_Value, '') usersort, "
        sql &= "u.UserID, LastName, "
        sql &= "FirstName, "
        sql &= "U1.Item_Value As Testimonial, "
        sql &= "U4.Item_Value as SubTestimonial "
        sql &= "from IPM_USER u "
        sql &= "left join IPM_USER_FIELD_VALUE vc on u.UserID = vc.USER_ID and vc.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USER_DESIGNATIONS') "
        sql &= "left join IPM_USER_FIELD_VALUE vs on u.UserID = vs.USER_ID and vs.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USER_ORDERING') "
        sql &= "left join IPM_USER_FIELD_VALUE U1 on U.UserID = U1.USER_ID and U1.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTIMONIAL') "
        sql &= "left join IPM_USER_FIELD_VALUE U4 on U.UserID = U4.USER_ID and U4.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTISUB') "
        sql &= "left join IPM_USER_FIELD_VALUE U5 on u.UserID = U5.USER_ID and U5.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_AFFILIATECHECK') "
        sql &= "left join IPM_USER_FIELD_VALUE U6 on u.UserID = U6.USER_ID and U6.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_CLIENTTESTIMONIAL') "
        sql &= "where Active = 'Y' "
        sql &= "and u.Contact = 1 "
        'sql &= "and U5.Item_Value is null "
        sql &= "and U6.Item_Value = '1'"
        'sql &= "order by vs.Item_Value DESC"
        sql &= "order by CAST(isnull(vs.Item_Value, 9999999999) as integer) desc"
        TestimonialsDT = New DataTable("TestimonialsDT")
        TestimonialsDT = mmfunctions.GetDataTable(sql)
        RecordPR = 4

        For R As Integer = 0 To TestimonialsDT.Rows.Count - 1
            NewRow = NewRow + 1

            If NewRow = RecordPR Then
                out &= "<li class=""last_in_row"">"
                NewRow = 0
            Else
                out &= " <li> "
            End If

            Dim TestiFriendlyLink As String = ""
            TestiFriendlyLink = "#testimonial_" & TestimonialsDT.Rows(R)("UserID").ToString.Trim & "_" & formaturl.friendlyurl(TestimonialsDT.Rows(R)("FullName").ToString.Trim.ToLower)

            out &= "<a href=""" & TestiFriendlyLink & """ title="""" >"
            out &= "<img src=""" & "/dynamic/image/week/contact/best/157x185/92/ffffff/North/" & TestimonialsDT.Rows(R)("UserID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
            out &= "<span>" & formatfunctions.AutoFormatText(TestimonialsDT.Rows(R)("FullName").ToString.Trim) & "</span>"
            out &= "<strong>" & formatfunctions.AutoFormatText(TestimonialsDT.Rows(R)("Position").ToString.Trim) & "</strong>"
            out &= "<em>" & formatfunctions.AutoFormatText(TestimonialsDT.Rows(R)("agency").ToString.Trim) & "</em>"
            out &= "</a>"
            out &= "</li>"

            Dim TestiPopFriendlyLink As String = ""
            TestiPopFriendlyLink = "testimonial_" & TestimonialsDT.Rows(R)("UserID").ToString.Trim & "_" & formaturl.friendlyurl(TestimonialsDT.Rows(R)("FullName").ToString.Trim.ToLower)

            Dim Position As String
	    Position = formatfunctions.AutoFormatText(TestimonialsDT.Rows(R)("Position").ToString.Trim)
            If Position = "" Then
                Position = ""
            Else
                Position = Position & ", "
            End If
            

            out2 &= "<div id=""" & TestiPopFriendlyLink & """ class=""popup_box"">"
            out2 &= "<h3 class=""sub_title"">" & formatfunctions.AutoFormatText(TestimonialsDT.Rows(R)("FullName").ToString.Trim) & "</h3>"
            out2 &= "<strong>" &  Position & formatfunctions.AutoFormatText(TestimonialsDT.Rows(R)("agency").ToString.Trim) & "</strong>"
            out2 &= "<div class=""popup_info"">"
            out2 &= "<img src=""" & "/dynamic/image/week/contact/best/220x293/92/777777/Center/" & TestimonialsDT.Rows(R)("UserID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
            out2 &= "<div class=""text"">"
            out2 &= "<p>" & formatfunctions.AutoFormatText(TestimonialsDT.Rows(R)("Testimonial").ToString.Trim) & "</p>"
            'out2 &= "<a href=""#"" title="""">View George Mason University Campus Plan</a>"
            out2 &= "</div>"
            out2 &= "</div>"
            out2 &= "</div>"
            out2 &= vbNewLine


        Next

        testimonial_list1.Text = out
        testimonial__popups_list1.Text = out2


    End Sub
    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub

End Class
