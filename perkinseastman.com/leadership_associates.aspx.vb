﻿Imports System.Data

Partial Class leadership_associates
    Inherits System.Web.UI.Page

    Public TotalRecords As Integer
    Public TotalPages As Integer
    Public RecordPP As Integer
    Public RecordPR As Integer
    Public NewRow As Integer
    Public NewPage As Integer
    Public CountPage As Integer
    Public PrincipalsDT As DataTable
    Public AgencyLink As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim sqlagency As String = "select distinct "
        sqlagency &= "u.agency "
        sqlagency &= "from IPM_USER u "
        sqlagency &= "left join IPM_USER_FIELD_VALUE vc on u.UserID = vc.USER_ID and vc.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TITLE') "
        sqlagency &= "left join IPM_USER_FIELD_VALUE vd on u.UserID = vd.USER_ID and vd.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USER_DESIGNATIONS') "
        sqlagency &= "left join IPM_USER_FIELD_VALUE vs on u.UserID = vs.USER_ID and vs.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USER_ORDERING') "
        sqlagency &= "where Active = 'Y' "
        sqlagency &= "and vc.Item_Value = 'Affiliates' "
        sqlagency &= "order by agency "
        Dim AgencyDT As DataTable
        AgencyDT = New DataTable("AgencyDT")
        AgencyDT = mmfunctions.GetDataTable(sqlagency)
        If AgencyDT.Rows.Count > 0 Then
            AgencyLink = "leadership_affiliates_" & formatfunctions.AutoFormatText(CMSFunctions.FormatFriendlyUrl1(AgencyDT.Rows(0)("agency").ToString.Trim, "_"))
        End If

        Dim out As String = ""
        Dim out2 As String = ""

        Dim sql As String = "select "
        sql &= "FirstName + ' ' + LastName FullName, "
        sql &= "u.Position, "
        sql &= "u.agency, "
        sql &= "u.Bio, "
        sql &= "vd.Item_Value as Designation, "
        sql &= "vc.Item_Value as Title, "
        sql &= "ISNULL(vs.Item_Value, '') usersort, "
        sql &= "u.UserID, LastName, "
        sql &= "FirstName, "
        sql &= "case when isnull(u.Position,'') <> '' then u.Position else '' end + case when isnull(u.Position ,'') <> '' and isnull(u.agency ,'') <> '' then ', ' else '' end + case when isnull(u.agency,'') <> '' then u.agency else '' end as TitleCompany "
        sql &= "from IPM_USER u "
        sql &= "left join IPM_USER_FIELD_VALUE vc on u.UserID = vc.USER_ID and vc.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TITLE') "
        sql &= "left join IPM_USER_FIELD_VALUE vd on u.UserID = vd.USER_ID and vd.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USER_DESIGNATIONS') "
        sql &= "left join IPM_USER_FIELD_VALUE vs on u.UserID = vs.USER_ID and vs.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USER_ORDERING') "
        sql &= "where Active = 'Y' "
        sql &= "and vc.Item_Value = 'Associate Principals' "
        sql &= "order by LastName "
        PrincipalsDT = New DataTable("FProjects")
        PrincipalsDT = mmfunctions.GetDataTable(sql)

        RecordPP = 12
        RecordPR = 4
        TotalRecords = PrincipalsDT.Rows.Count
        If TotalRecords > 0 Then
            If TotalRecords <= RecordPP Then
                TotalPages = 1
            Else
                TotalPages = (TotalRecords \ RecordPP) + 1
            End If
            For R As Integer = 0 To TotalRecords - 1

                'START White Papers SQL
                Dim WhitePapersSQL As String = ""
                WhitePapersSQL &= "Select "
                WhitePapersSQL &= "n.News_Id, "
                WhitePapersSQL &= "n.Headline, "
                WhitePapersSQL &= "n.Content, "
                WhitePapersSQL &= "n.Picture, "
                WhitePapersSQL &= "n2.Item_Value as VideoLink, "
                WhitePapersSQL &= "n.PDF, "
                WhitePapersSQL &= "COALESCE( 'by ' + nullif(ltrim(v.Item_Value),''),'') as Author , "
                WhitePapersSQL &= "DATENAME(mm,n.Post_Date) + ' ' + DATENAME(day,n.Post_Date) + ', ' + DATENAME(year, n.Post_Date) DateLoc, "
                WhitePapersSQL &= "v.Item_Value Employee, "
                WhitePapersSQL &= "v2.Item_Value Title, "
                WhitePapersSQL &= "v3.Item_Value video "
                WhitePapersSQL &= "From IPM_NEWS n "
                WhitePapersSQL &= "Left Join IPM_NEWS_FIELD_VALUE v ON (n.News_id = v.News_id AND v.Item_ID = 3409254) "
                WhitePapersSQL &= "Left Join  IPM_NEWS_FIELD_VALUE v2 on ( n.News_Id = v2.NEWS_ID and v2.Item_ID = 3409255 ) "
                WhitePapersSQL &= "Left Join IPM_NEWS_FIELD_VALUE v3 on (n.News_Id = v3.NEWS_ID and v3.Item_ID = 30052966)  "
                WhitePapersSQL &= "left join IPM_NEWS_FIELD_VALUE n2 on N.News_Id = n2.NEWS_ID and n2.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_VIDEOLINK') "
                WhitePapersSQL &= "left join IPM_NEWS_FIELD_VALUE n3 on N.News_Id = n3.NEWS_ID and n3.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_AUTHOR') "
                WhitePapersSQL &= "Where(n.Show = 1 And n.Post_Date < GETDATE() And n.Pull_Date > GETDATE() And n.Type = 10) "
                WhitePapersSQL &= "and charindex('" & PrincipalsDT.Rows(R)("UserID").ToString.Trim & "',LTRIM(RTRIM(n3.Item_Value)))!= 0 "
                WhitePapersSQL &= "Order By n.Post_Date DESC"
                Dim WhitePapersDT As New DataTable("WhitePapersDT")
                WhitePapersDT = mmfunctions.GetDataTable(WhitePapersSQL)
                'END White Papers SQL

                NewRow = NewRow + 1
                NewPage = NewPage + 1
                If R = 0 Then
                    CountPage = 1
                    out &= "<ul class=""main_list testimonial_list users"" id=""slide_" & CountPage & """> "
                End If
                If NewRow = RecordPR Then
                    out &= "<li class=""last_in_row"">"
                    NewRow = 0
                Else
                    out &= " <li> "
                End If
                out &= "<a href=""#testimonial_" & R + 1 & """ title="""" >"
                out &= "<img src=""" & "/dynamic/image/week/contact/best/157x195/92/777777/North/" & PrincipalsDT.Rows(R)("UserID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
                out &= "<span>" & formatfunctions.AutoFormatText(PrincipalsDT.Rows(R)("FullName").ToString.Trim) & "</span>"
                out &= "<strong>" & formatfunctions.AutoFormatText(PrincipalsDT.Rows(R)("Designation").ToString.Trim) & "</strong>"
                out &= "</a>"
                out &= "</li>"

                If NewPage = RecordPP Then
                    NewPage = 0
                    CountPage = CountPage + 1
                    out &= " </ul> "
                    out &= vbNewLine
                    out &= "<ul class=""main_list testimonial_list users"" id=""slide_" & CountPage & """> "
                End If

                If R + 1 = TotalRecords Then
                    out &= " </ul> "
                End If

                out2 &= "<div id=""testimonial_" & R + 1 & """ class=""popup_box"">"
                out2 &= "<h3 class=""sub_title"">" & formatfunctions.AutoFormatText(PrincipalsDT.Rows(R)("FullName").ToString.Trim) & " " & formatfunctions.AutoFormatText(PrincipalsDT.Rows(R)("Designation").ToString.Trim) & "</h3>"
                out2 &= "<div class=""popup_info"">"
                out2 &= "<img src=""" & "/dynamic/image/week/contact/best/220x293/92/f9F9F9/NorthWest/" & PrincipalsDT.Rows(R)("UserID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
                out2 &= "<div class=""text"">"
                out2 &= "<p>" & formatfunctions.AutoFormatText(PrincipalsDT.Rows(R)("Bio").ToString.Trim) & "</p>"

                'START White Papers
                If WhitePapersDT.Rows.Count > 0 Then
                    out2 &= "<p class=""whitepapers"">"
                    If WhitePapersDT.Rows.Count > 1 Then
                        out2 &= "<strong>White Papers</strong>"
                    Else
                        out2 &= "<strong>White Paper</strong>"
                    End If
                    For i As Integer = 0 To WhitePapersDT.Rows.Count - 1
                        If WhitePapersDT.Rows(i)("PDF").ToString.Trim = "1" Then
                            out2 &= "<a href=""/dynamic/document/week/asset/download/" & WhitePapersDT.Rows(i)("News_ID").ToString.Trim & "/" & WhitePapersDT.Rows(i)("News_ID").ToString.Trim & ".pdf"" >"
                        Else
                            out2 &= "<a href=""#"">"
                        End If
                        out2 &= formatfunctions.AutoFormatText(WhitePapersDT.Rows(i)("Headline").ToString.Trim)
                        out2 &= "</a>"
                    Next
                    out2 &= "</p>"
                End If
                'END White Papers

                out2 &= "</div>"
                out2 &= "</div>"
                out2 &= "</div>"
                out2 &= vbNewLine

            Next

        End If

        leadership_principals_list1.Text = out
        leadership_bio_list1.Text = out2

    End Sub
    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub


End Class
