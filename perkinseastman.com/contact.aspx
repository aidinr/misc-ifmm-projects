﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="contact.aspx.vb" Inherits="contact" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first">ABOUT</a> <span> | </span></li>
                        <li><a href="<%=Master.UrlPage%>" class="active">Contact</a></li> 
                    </ul>
                </div>
                
                <div class="left_bigger_right_smaller_layout">
                    <div class="left">
                        <div class="headline">
                            <h3 class="sub_title no_margin_bottom">Contact</h3>
                        </div>

                        <div class="office_locations_columned_layout">
                                <div class="column">
                                <ul class="locations cmsGroup cmsName_Locations">

<!--
<li>
<h2 class="cms cmsType_TextSingle cmsName_City_Name">City Name</h2>
<span class="cms cmsType_TextSingle cmsName_Content">
Line 1<br />
Line 2<br />
Line 3<br />
Line 4<br />
</span>
<a class="cms cmsType_Link cmsName_Link">Link</a><br />
<a class="cms cmsType_File cmsName_Document">Document</a><br />
</li>
-->
                  <%
                      Dim RowPosition As Integer
                      For i As Integer = 1 To 30 - 1
                          RowPosition = RowPosition + 1

If RowPosition = 4 Then
 RowPosition = 1
End If


If Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Locations_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
            %>
<li class="cmsGroupItem col<% =RowPosition %>">
<h2 class="cms cmsType_TextSingle cmsName_Locations_Item_<%=i %>_City_Name"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Locations_Item_" & i & "_City_Name")%></h2>
<span class="cms cmsType_TextMulti cmsName_Locations_Item_<%=i %>_Content">
<%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Locations_Item_" & i & "_Content")%>
</span>
<span class="lLink cms cmsType_Link cmsName_Locations_Item_<%=i %>_Link">
<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\Link\", "Locations_Item_" & i & "_Link" & "*", SearchOption.AllDirectories).Length > 0 Then%>
<a title="" href="<%= CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", "Locations_Item_" & i & "_Link", "href")%>" target="_blank"><%= CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", "Locations_Item_" & i & "_Link", "text")%></a>
<%End If%>
</span>
<span class="lDocument cms cmsType_File cmsName_Locations_Item_<%=i %>_Document">
<%  If Directory.GetFiles(Server.MapPath("~") & "cms\data\File\", "Locations_Item_" & i & "_Document" & "*", SearchOption.AllDirectories).Length > 0 Then%>
<a href="<% =CMSFunctions.GetFile(Server.MapPath("~") & "cms\data\File\", "Locations_Item_" & i & "_Document", "link") %>"><% =CMSFunctions.GetFile(Server.MapPath("~") & "cms\data\File\", "Locations_Item_" & i & "_Document", "text")%></a>
<%End If%>
</span>
</li>
                                 <%End If %>
                            <% next %>  
                                  </ul><div class="c"></div>
                            </div>        
                        </div>
                    </div>
                    <div class="right no_border mt37">
                        <div class="contactSidebar cms cmsType_Rich cmsName_Contact_Sidebar"><%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Contact_Sidebar")%></div>
                    </div>
                </div>
                
</asp:Content>

