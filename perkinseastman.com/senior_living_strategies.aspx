﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="senior_living_strategies.aspx.vb" Inherits="senior_living_strategies" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_Work"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Work")%></a><span> | </span></li>
                        <li><a class="cmsMultiLanguage cms cmsType_TextSingle cmsName_By_Practice_Area"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "By_Practice_Area")%></a><span> | </span></li>
                        <li><a class="cmsMultiLanguage cms cmsType_TextSingle cmsName_Senior_Living" href="senior_living"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Senior_Living")%></a><span> | </span></li>
                        <li><a class="active" href="<%=Master.UrlPage%>"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Strategies")%></a></li>
                    </ul>
                </div>
                
                <h3 class="sub_title cmsMultiLanguage cms cmsType_TextSingle cmsName_Strategies"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Strategies")%></h3>
                
                <div class="left_bigger_right_smaller_layout">
                    <div class="left">
                        <div class="cms cmsType_Rich cmsName_Senior_Living_Strategies_Intro">
                            <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Senior_Living_Strategies_Intro")%>
                            </div>                         
                    </div>
                    <div class="right less_padding">

                        <div class="text">
                        <p class="red_text_special cms cmsType_TextMulti cmsName_senior_living_strategies_testi_user_quote"><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "senior_living_strategies_testi_user_quote")%></p>
                        </div>

            <span class="featured_consult">
            <strong class="cms cmsType_TextSingle cmsName_senior_living_strategies_testi_user_name"><%= CMSFunctions.GetTextSimple(Server.MapPath("~") & "cms\data\", "senior_living_strategies_testi_user_name")%></strong>
            <span class="cms cmsType_TextSingle cmsName_senior_living_strategies_testi_user_practice"><%= CMSFunctions.GetTextSimple(Server.MapPath("~") & "cms\data\", "senior_living_strategies_testi_user_practice")%></span>
            <ins class="cms cmsType_TextSingle cmsName_senior_living_strategies_testi_user_title"><%=CMSFunctions.GetTextSimple(Server.MapPath("~") & "cms\data\", "senior_living_strategies_testi_user_title")%></ins>
            
            <img 
                         src="cms/data/Sized/best/220x145/92/ffffff/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "senior_living_strategies_testi_image")%>" 
                         alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "senior_living_strategies_testi_image")%>" 
                         title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "senior_living_strategies_testi_image")%>" 
                         class="cms cmsType_Image cmsName_senior_living_strategies_testi_image" />

            </span>
                    </div>
                </div>
                    
<%
    Dim ItemsCount As Integer = CMSFunctions.CountTextSingle(CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & "*" & "_Name")
%>

<% If ItemsCount > 0 Then%>
 <div class="casestudy_list">
<h3 class="headline_additional">view select case studies below</h3>
<ul class="cmsGroup cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName, "_")%>_Case_Studies">
<!--
<li><a><span class="selectTemplate cms cmsType_TextSingle cmsName_Name">Case Study:</span><strong>Case Study Name</strong></a><a class="selectTemplate cms cmsType_Textset cmsName_List"></a></li>
-->
 <%
     Dim SlideName As String
     Dim SlideLayout As String
     For i As Integer = 1 To ItemsCount
         Dim SlidesCount As Integer = CMSFunctions.CountTextSet(CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List")
%>

<%
    If SlidesCount > 0 Then
        SlideName = ""
        SlideLayout = ""
        SlideName = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xa" & 0 & ".txt")
        SlideLayout = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xb" & 0 & ".txt")
        SlideName = CMSFunctions.FormatFriendlyUrl(SlideName, "_")
    End If
    %>

<li class="cmsGroupItem">
<%  If SlideName <> "" Then%>
<a href="casestudy_<%=SlideLayout %>_<%=CategoryId %>_<%=SlideName %>">
<%End If%>
<span class="selectTemplate cms cmsType_TextSingle cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName,"_") %>_Case_Studies_Item_<%= i %>_Name">Case Study <%= i %>:</span>
<strong><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_Name")%></strong>
<%  If SlideName <> "" Then%>
</a>
<%End If%>
<a class="selectTemplate cms cmsType_Textset cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName,"_") %>_Case_Studies_Item_<%= i %>_List"></a>
</li>

<%Next%>  
</ul>
</div>         
<%Else%> 
<div class="noCase cmsGroup cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName, "_")%>_Case_Studies">
<!--
<div class="casestudy_list"><ul><li><a href="#"><span class="selectTemplate cms cmsType_TextSingle cmsName_Name">Case Study:</span><strong>Example Case Study</strong></a><a class="selectTemplate cms cmsType_Textset cmsName_List"></a></li></ul></div>
-->
</div>
<%End If%>     
           
</asp:Content>


