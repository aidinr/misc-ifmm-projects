﻿Imports System.Data

Partial Class affiliates
    Inherits System.Web.UI.Page
    Public TotalRecords As Integer
    Public TotalPages As Integer
    Public RecordPP As Integer
    Public RecordPR As Integer
    Public NewRow As Integer
    Public NewPage As Integer
    Public CountPage As Integer
    Public PrincipalsDT As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim out As String = ""
        Dim out2 As String = ""


        Dim sql As String = ""
        sql &= "select "
        sql &= "u.UserID, "
        sql &= "u.agency As CompanyName "
        sql &= "from IPM_USER u "
        sql &= "left join IPM_USER_FIELD_VALUE U1 on u.UserID = U1.USER_ID and U1.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_AFFILIATECHECK') "
        sql &= "left join IPM_USER_FIELD_VALUE U2 on u.UserID = U2.USER_ID and U2.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_AFFILIATEDESCRIPTION') "
        sql &= "left join IPM_USER_FIELD_VALUE U3 on u.UserID = U3.USER_ID and U3.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_AFFILIATEWEBSITE') "
        sql &= "left join IPM_USER_FIELD_VALUE vs on u.UserID = vs.USER_ID and vs.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USER_ORDERING') "
        sql &= "where u.Active = 'Y' "
        sql &= "and u.Contact = '1' and U1.Item_Value = '1' and u.agency <> '' "
        sql &= "order by CAST(isnull(vs.Item_Value, 9999999999) as integer) desc"
        PrincipalsDT = New DataTable("FProjects")
        PrincipalsDT = mmfunctions.GetDataTable(sql)

        RecordPP = 10
        RecordPR = 5
        TotalRecords = PrincipalsDT.Rows.Count

        If TotalRecords > 0 Then

            If TotalRecords <= RecordPP Then
                TotalPages = 1
            Else
                TotalPages = (TotalRecords \ RecordPP) + 1
            End If

            For R As Integer = 0 To TotalRecords - 1
                If PrincipalsDT.Rows(R)("CompanyName").ToString <> "" Then



                    NewRow = NewRow + 1
                    NewPage = NewPage + 1

                    If R = 0 Then
                        CountPage = 1
                        out &= "<ul class=""main_list futura_font more_spacing_fix users"" id=""slide_" & CountPage & """> "
                    End If

                    If NewRow = RecordPR Then
                        out &= "<li class=""last_in_row"">"
                        NewRow = 0
                    Else
                        out &= " <li> "
                    End If

                    Dim ComFriendlyLink As String = ""
                    ComFriendlyLink = "affiliate_" & PrincipalsDT.Rows(R)("UserID").ToString.Trim & "_" & CMSFunctions.FormatFriendlyUrl(PrincipalsDT.Rows(R)("CompanyName").ToString.Trim.ToLower, "_")

                    out &= "<a href=""" & ComFriendlyLink & """ title="""" >"
                    out &= "<span>" & formatfunctions.AutoFormatText(PrincipalsDT.Rows(R)("CompanyName").ToString.Trim) & "</span>"
                    'out &= "<img src=""" & "/dynamic/image/week/contact/padded/164x164/92/ffffff/Center/" & PrincipalsDT.Rows(R)("UserID").ToString.Trim & ".png" & """" & " alt=""""/>"
                    out &= "<img src=""" & "cms/data/Sized/best/164x164/92/f4F3ED/Center/" & CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Image_" & ComFriendlyLink & "") & """ alt="""" />"
                    out &= "</a>"
                    out &= "</li>"

                    'out &= "<a href=""#testimonial_" & R + 1 & """ title="""" >"
                    'out &= "<img src=""" & "/dynamic/image/week/contact/best/157x185/92/777777/Center/" & PrincipalsDT.Rows(R)("UserID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
                    'out &= "<span>" & formatfunctions.AutoFormatText(PrincipalsDT.Rows(R)("FullName").ToString.Trim) & "</span>"
                    'out &= "<strong>" & formatfunctions.AutoFormatText(PrincipalsDT.Rows(R)("Designation").ToString.Trim) & "</strong>"
                    ''out &= "<em>" & PrincipalsDT.Rows(R)("agency").ToString.Trim & "</em>"
                    'out &= "</a>"
                    'out &= "</li>"

                    'If NewPage = RecordPP Then
                    '    NewPage = 0
                    '    CountPage = CountPage + 1
                    '    out &= " </ul> "
                    '    out &= vbNewLine
                    '    out &= "<ul class=""main_list futura_font more_spacing_fix"" id=""slide_" & CountPage & """> "
                    'End If

                    If R + 1 = TotalRecords Then
                        out &= " </ul> "
                    End If

                    'out2 &= "<div id=""testimonial_" & R + 1 & """ class=""popup_box"">"
                    'out2 &= "<h3 class=""sub_title"">" & formatfunctions.AutoFormatText(PrincipalsDT.Rows(R)("FullName").ToString.Trim) & "</h3>"
                    'out2 &= "<strong>" & formatfunctions.AutoFormatText(PrincipalsDT.Rows(R)("Position").ToString.Trim) & "</strong>"
                    'out2 &= "<div class=""popup_info"">"
                    'out2 &= "<img src=""" & "/dynamic/image/week/contact/best/220x293/92/777777/Center/" & PrincipalsDT.Rows(R)("UserID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
                    'out2 &= "<div class=""text"">"
                    'out2 &= "<p>" & PrincipalsDT.Rows(R)("Bio").ToString.Trim & "</p>"
                    ''out2 &= "<a href=""#"" title="""">View George Mason University Campus Plan</a>"
                    'out2 &= "</div>"
                    'out2 &= "</div>"
                    'out2 &= "</div>"
                    'out2 &= vbNewLine

                End If
            Next

        End If

        leadership_principals_list1.Text = out
        'leadership_bio_list1.Text = out2
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub

End Class
