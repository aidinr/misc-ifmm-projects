﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="affiliate_projects.aspx.vb" Inherits="affiliate_projects" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first">about</a><span> | </span></li>
                        <li><a href="affiliates">affiliates</a><span> | </span></li>
                        <% If Request.QueryString("ID").ToString().Contains("3421828") Then %> <li><a href="<%=ComFriendlyLink%>"><%="S9"%></a><span> | </span></li>
                        <% Else  %>  <li><a href="<%=ComFriendlyLink%>"><%="EE&K"%></a><span> | </span></li>  
                        <% End If %>

                        <li><a href="<%=Master.UrlPage%>" class="active">selected projects</a></li>                        
                    </ul>
                </div>
                
                <h3 class="sub_title">
                    <asp:Literal runat="server" ID="affiliate_logo"/>
                </h3>
                
                <div class="company_inner_layout">
                    <div class="left">
                        
                        <div class="inner_tabs"> 
                           <asp:Literal runat="server" ID="eek_projects_list1"/>                           
                        </div>                  
                    </div>
                    <div class="right mt-5">
                        <dl class="project_list">
                            <dt>Selected Projects</dt>
                           <asp:Literal runat="server" ID="eek_projects_links"/>     
                        </dl>
                        <a href="<%=ComFriendlyLink%>" title="" class="view_more">Return to Intro Page</a>
                    </div>
                </div>
</asp:Content>
