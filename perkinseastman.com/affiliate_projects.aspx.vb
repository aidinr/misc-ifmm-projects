﻿Imports System.Data
Imports System.IO
Partial Class affiliate_projects
    Inherits System.Web.UI.Page
    Public EekProjectsDT As DataTable
    Public AffiliateId As String
    Public AffiliateName As String
    Public ComFriendlyLink As String
    Public ListOfProjects As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ListOfProjects = ""
        AffiliateId = Request.QueryString("ID")
        AffiliateName = Request.QueryString("name")
        ComFriendlyLink = ""

        AffiliateName = CMSFunctions.FormatNiceText(AffiliateName, 1)
        ComFriendlyLink = "affiliate_" & AffiliateId & "_" & CMSFunctions.FormatFriendlyUrl(AffiliateName, "_")

        If AffiliateName.contains("Rgr") Then
            AffiliateName = Replace(AffiliateName, "Rgr", "RGR")
        End If
        If AffiliateName.contains("Bfj") Then
            AffiliateName = Replace(AffiliateName, "Bfj", "BFJ")
        End If


        Dim Projects As String = ""
        Try
            Using sr As New StreamReader(Server.MapPath("~") & "cms\data\Text\affiliate_" & AffiliateId & "_" & CMSFunctions.FormatFriendlyUrl(AffiliateName, "_") & "_projects.txt")
                Projects = sr.ReadToEnd()
                Projects = Replace(Projects, vbnewline, ",")
                'Projects = Left(Projects, Len(Projects) - 1)
                If Projects.substring(Projects.length - 1) = "," Then
                    Projects = StrReverse(Replace(StrReverse(Projects), StrReverse(","), StrReverse(""), , 1))
                End If
                ListOfProjects &= Projects
            End Using
        Catch ex As Exception
            Projects = ex.Message
        End Try

        Dim out As String = ""
        Dim out1 As String = ""
        If ListOfProjects <> "" Then

            Dim sql As String = ""
            sql &= "select "
            sql &= "p.ProjectID, "
            sql &= "P.Name as ProjectName, "
            sql &= "case when isnull(P.State_id,'') <> '' or P.State_id is null then P.City + case when isnull(S.Name ,'') <> '' then ', ' + S.Name else '' end else p.City + case when isnull(P5.Item_Value,'') <> '' then ', ' + P5.Item_Value else '' end + case when isnull(P4.Item_Value,'') <> '' then ', ' + P4.Item_Value else '' end end as ProjectAddress, "
            sql &= "P2.Item_Value "
            sql &= "FROM IPM_PROJECT P "
            sql &= "left join IPM_PROJECT_FIELD_VALUE P2 on P.ProjectID = P2.ProjectID and P2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_EEKPROJECT') "
            sql &= "left join IPM_PROJECT_FIELD_VALUE P3 on P.ProjectID = P3.ProjectID and P3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_CITY') "
            sql &= "left join IPM_PROJECT_FIELD_VALUE P4 on P.ProjectID = P4.ProjectID and P4.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_COUNTRY') "
            sql &= "left join IPM_PROJECT_FIELD_VALUE P5 on P.ProjectID = P5.ProjectID and P5.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_STATE') "

            sql &= "left join IPM_STATE S on S.State_id = P.State_id "
            sql &= "WHERE p.ProjectID in ("
            sql &= ListOfProjects
            sql &= ")"
            'sql &= "and P3.Item_Value = 1 "
            sql &= "and P.Available = 'y' "
            sql &= "order by P.Name asc "
            EekProjectsDT = New DataTable("EekProjectsDT")
            EekProjectsDT = mmfunctions.GetDataTable(sql)


            If EekProjectsDT.Rows.Count > 0 Then
                For R As Integer = 0 To EekProjectsDT.Rows.Count - 1
                    Dim PrFriendlyLink As String = ""
                    PrFriendlyLink = "project_" & EekProjectsDT.Rows(R)("ProjectID").ToString.Trim & "_" & CMSFunctions.FormatFriendlyUrl(EekProjectsDT.Rows(R)("ProjectName").ToString.Trim.ToLower, "_")
                    out &= " <div class=""big_image_with_info_box"" id=""box_" & R + 1 & """>"

                    If (Request.QueryString("name") & "").ToString.Trim.ToUpper <> "S9" Then
                        out &= "<a href=""" & PrFriendlyLink & """  title="""">"
                    End If
                    out &= "<img src=""/dynamic/image/week/project/best/460x368/92/ffffff/Center/" & EekProjectsDT.Rows(R)("ProjectID").ToString.Trim & ".jpg"" alt=""""/>"
                    out &= "<strong class=""info_pop inner_pop"">"
                    out &= "<big>" & formatfunctions.AutoFormatText(EekProjectsDT.Rows(R)("ProjectName").ToString.Trim) & "</big>"
                    out &= "<span class=""openSans"">" & formatfunctions.AutoFormatText(EekProjectsDT.Rows(R)("ProjectAddress").ToString.Trim) & "</span>"
                    out &= "</strong> "
                    If (Request.QueryString("name") & "").ToString.Trim.ToUpper <> "S9" Then
                        out &= "</a>"
                    End If
                    out &= "</div>"
                    If (Request.QueryString("name") & "").ToString.Trim.ToUpper <> "S9" Then
                        out1 &= "<dd><a href=""" & PrFriendlyLink & """ rel=""" & R + 1 & """>" & formatfunctionssimple.AutoFormatText(EekProjectsDT.Rows(R)("ProjectName").ToString.Trim) & "</a></dd>"
                    Else
                        out1 &= "<dd><a href=""#"" rel=""" & R + 1 & """>" & formatfunctionssimple.AutoFormatText(EekProjectsDT.Rows(R)("ProjectName").ToString.Trim) & "</a></dd>"
                    End If
                Next
                eek_projects_list1.Text = out
                eek_projects_links.Text = out1
            End If
        End If
        affiliate_logo.Text = "<img src=""" & "/dynamic/image/week/contact/fit/330x85/100/ffffff/NorthWest/" & AffiliateId & ".png" & """" & " alt=""""/>"

        Dim LastVisitedCookie As New HttpCookie("LastVisited")
        LastVisitedCookie.Values("parent") = "eekp," & ""
        LastVisitedCookie.Values("lastVisit") = DateTime.Now.ToString()
        LastVisitedCookie.Expires = DateTime.Now.AddDays(1)
        Response.Cookies.Add(LastVisitedCookie)

    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub

End Class
