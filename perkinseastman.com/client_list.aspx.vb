﻿Imports System.Data
Imports System.IO
Partial Class client_list
    Inherits System.Web.UI.Page


    Public MainCategory As String
    Public practiceName As String
    Public MainPracticeId As String
    Public MainPracticeName As String
    Public practiceAreaUDFID As String
    Public breadcrumbsOut As String

    Public ClientsListDT As New DataTable
    Public FuturedClientDT As DataTable

    Public CategoryId As String

    Public FriendlyPracticeName As String
    Public FriendlyCMSName As String

    Public practiceFrontText As String = ""

    'Public ClientsDT As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        CategoryId = Request.QueryString("cat")
        'CategoryId = Replace(CategoryId, "_", " ")

        Select Case CategoryId
            Case "3413026"
                practiceName = "Broadcast and Media"
                practiceFrontText = "Broadcast and Media"
            Case "3414326"
                practiceName = "Courts and Public"
                practiceFrontText = "Courts and Public"
            Case "2400012"
                practiceName = "Corporate Interiors"
                practiceFrontText = "Corporate Interiors"
                practiceAreaUDFID = "3408922"
            Case "3409338"
                practiceName = "Education"
                practiceFrontText = "Education"
                practiceAreaUDFID = "3408922"
            Case "3409328"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "Campus Planning"
                practiceFrontText = "Campus Planning"
                practiceAreaUDFID = "3408924"
            Case "2400014"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "Higher Education"
                practiceFrontText = "Higher Education"
                practiceAreaUDFID = "3408924"
            Case "2400017"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "K-12 Education"
                practiceFrontText = "K-12 Education"
                practiceAreaUDFID = "3408924"
            Case "3409341"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "Town and Gown"
                practiceFrontText = "Town and Gown"
                practiceAreaUDFID = "3408924"
            Case "2400024"
                practiceName = "Healthcare"
                practiceFrontText = "Healthcare"
                practiceAreaUDFID = "3408922"
            Case "2400015"
                practiceName = "Hospitality"
                practiceFrontText = "Hospitality"
                practiceAreaUDFID = "3408922"
            Case "2400016"
                practiceName = "Housing"
                practiceFrontText = "Residential"
                practiceAreaUDFID = "3408922"
            Case "2400018"
                practiceName = "Office and Retail"
                practiceFrontText = "Office and Retail"
                practiceAreaUDFID = "3408922"
            Case "2400019"
                practiceName = "Cultural"
                practiceFrontText = "Cultural"
                practiceAreaUDFID = "3408922"
            Case "2400020"
                practiceName = "Science and Technology"
                practiceFrontText = "Science and Technology"
                practiceAreaUDFID = "3408922"
            Case "2400021"
                practiceName = "Senior Living"
                practiceFrontText = "Senior Living"
                practiceAreaUDFID = "3408922"
            Case "3409343"
                practiceName = "Urbanism"
                practiceFrontText = "Urbanism"
                practiceAreaUDFID = "3408922"
            Case "3409345"
                MainPracticeId = "3409343"
                MainPracticeName = "By PRACTICE"
                practiceName = "Science and Technology"
                practiceAreaUDFID = "3408922"
                practiceFrontText = "Science and Technology"
            Case "2400021"
                MainCategory = "By PRACTICE"
                practiceName = "Senior Living"
                practiceAreaUDFID = "3408922"
                practiceFrontText = "Senior Living"
            Case "3409343"
                practiceName = "Urbanism"
                practiceAreaUDFID = "3408922"
                practiceFrontText = "Urbanism"
            Case "3409345"
                MainCategory = "By PRACTICE"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Downtown Redevelopment"
                practiceAreaUDFID = "3408924"
                practiceFrontText = "Downtown Redevelopment"
            Case "3409347"
                MainCategory = "By PRACTICE"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "New Town Planning"
                practiceAreaUDFID = "3408924"
                practiceFrontText = "New Town Planning"
            Case "3409349"
                MainCategory = "By PRACTICE"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Transit and Development"
                practiceAreaUDFID = "3408924"
                practiceFrontText = "Transit and Development"
            Case "2400022"
                MainCategory = "By PRACTICE"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Urban Design"
                practiceAreaUDFID = "3408924"
                practiceFrontText = "Urban Design"
            Case "3409351"
                MainCategory = "By PRACTICE"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Waterfront"
                practiceAreaUDFID = "3408924"
                practiceFrontText = "Waterfront"
            Case "Asia"
                MainCategory = "By REGION"
                practiceName = "Asia"
                practiceFrontText = "Asia"
            Case "Africa and the Middle East"
                MainCategory = "By REGION"
                practiceName = "Africa and the Middle East"
                practiceFrontText = "Africa and the Middle East"
            Case "China"
                MainCategory = "By REGION"
                practiceName = "China"
                practiceFrontText = "China"
            Case "Europe"
                MainCategory = "By REGION"
                practiceName = "Europe"
                practiceFrontText = "Europe"
            Case "The Americas"
                MainCategory = "By REGION"
                practiceName = "The Americas"
                practiceFrontText = "The Americas"
            Case Else
        End Select



        FriendlyPracticeName = CMSFunctions.FormatFriendlyUrl(practiceName, "_")
        FriendlyPracticeName = Replace(FriendlyPracticeName, "-", "_")
        FriendlyCMSName = FriendlyPracticeName
        clients_list_title.Text = practiceName & " " & "Client List"


        If CategoryId <> "" Then
            breadcrumbsOut &= "<li><a title="""" class=""first"">Work</a><span> | </span></li>" & vbNewLine
            breadcrumbsOut &= "<li><a title="""">" & MainCategory & "</a><span> | </span></li>" & vbNewLine
            If MainPracticeName <> "" Then
                Dim MainCatFriendlyLink As String = ""
                MainCatFriendlyLink = "practice_" & MainPracticeId & "_" & formaturl.friendlyurl(MainPracticeName.ToLower)
                breadcrumbsOut &= "<li><a href=""" & MainCatFriendlyLink & """ title="""">" & MainPracticeName & "</a><span> | </span></li>" & vbNewLine
            End If
            If MainCategory = "By PRACTICE" Then
                Dim CatFriendlyLink As String = ""
                CatFriendlyLink = "category_" & CategoryId & "_" & formaturl.friendlyurl(practiceName.ToLower)
                breadcrumbsOut &= "<li><a href=""" & CatFriendlyLink & """ title="""">" & practiceName & "</a><span> | </span></li>" & vbNewLine
            Else
                Dim RegionFriendlyLink As String = ""
                RegionFriendlyLink = "region_" & formaturl.friendlyurl(practiceName)
                breadcrumbsOut &= "<li><a href=""" & RegionFriendlyLink & """ title="""">" & practiceName & "</a><span> | </span></li>" & vbNewLine
            End If
            breadcrumbsOut &= "<li><a title="""" class=""active"">" & practiceName & " " & "Client List" & "</a></li>" & vbNewLine
        End If

        'Dim HasClients As Integer = Directory.GetFiles(Server.MapPath("~") & "cms\data\Textset\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Client_List" & "*", SearchOption.AllDirectories).Length


        Dim out As String = ""
        Dim ClientName As String
        Dim ProjectId As String
        Dim ProjectLink As String
        Dim ProjectName As String
        Dim ProjectAddress As String
        Dim ProjectExist As Integer
        Dim TotalClients As Integer
        Dim TotalClientsPerColumn As Integer



        'Clients
        ClientsListDT.TableName = "ClientsListDT"
        Dim col_ClientNo As DataColumn = New DataColumn("ClientNo")
        col_ClientNo.DataType = System.Type.GetType("System.Int32")
        ClientsListDT.Columns.Add(col_ClientNo)
        Dim col_ClientName As DataColumn = New DataColumn("ClientName")
        col_ClientName.DataType = System.Type.GetType("System.String")
        ClientsListDT.Columns.Add(col_ClientName)
        Dim col_ProjectId As DataColumn = New DataColumn("ProjectId")
        col_ProjectId.DataType = System.Type.GetType("System.String")
        ClientsListDT.Columns.Add(col_ProjectId)

        For R As Integer = 0 To 500
            ProjectId = ""
            ProjectName = ""
            If CMSFunctions.CheckTextFile(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Client_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Client_List", R) Then
                ProjectId = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Client_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Client_List_xa" & R & ".txt")
                ClientName = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Client_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Client_List_xb" & R & ".txt")
                If ClientName <> "" Then
                    Dim ClientRow As DataRow = ClientsListDT.NewRow
                    ClientRow.Item("ClientNo") = R
                    ClientRow.Item("ClientName") = ClientName
                    ClientRow.Item("ProjectId") = ProjectId
                    ClientsListDT.Rows.Add(ClientRow)
                End If
            Else
                Exit For
            End If
        Next

        TotalClients = ClientsListDT.Rows.Count
        TotalClientsPerColumn = ClientsListDT.Rows.Count \ 3

        If ClientsListDT.Rows.Count <> (3 * TotalClientsPerColumn) Then
            TotalClientsPerColumn = TotalClientsPerColumn + 1
        End If

        Dim IndicativPeColoana As Integer
        Dim NumarColoana As Integer
        IndicativPeColoana = 0
        NumarColoana = 1

        'deschid tabel, coloana 1
        out &= "<ul class=""cms cmsType_Textset cmsName_" & FriendlyCMSName & "_Client_List"">"

        For i As Integer = 0 To ClientsListDT.Rows.Count - 1
            If ClientsListDT.Rows(i).Item("ClientName") <> "" Then
                If IndicativPeColoana < TotalClientsPerColumn Then
                    ClientName = ClientsListDT.Rows(i).Item("ClientName")
                    ProjectId = ClientsListDT.Rows(i).Item("ProjectId")
                    ' inserez element nou in coloana
                    If ProjectId <> "" AndAlso IsNumeric(ProjectId) = True Then
                        Dim Sql1 As String = ""
                        Sql1 &= "SELECT P.ProjectID, P.Name, "
                        Sql1 &= "case when isnull(P.State_id,'') <> '' or P.State_id is null then P.City + case when isnull(S.Name ,'') <> '' then ', ' + S.Name else '' end else p.City + case when isnull(P5.Item_Value,'') <> '' then ', ' + P5.Item_Value else '' end + case when isnull(P4.Item_Value,'') <> '' then ', ' + P4.Item_Value else '' end end as ProjectAddress "
                        Sql1 &= "FROM IPM_PROJECT P "
                        Sql1 &= "left join IPM_PROJECT_FIELD_VALUE P3 on P.ProjectID = P3.ProjectID and P3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_CITY') "
                        Sql1 &= "left join IPM_PROJECT_FIELD_VALUE P4 on P.ProjectID = P4.ProjectID and P4.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_COUNTRY') "
                        Sql1 &= "left join IPM_PROJECT_FIELD_VALUE P5 on P.ProjectID = P5.ProjectID and P5.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_STATE') "
                        Sql1 &= "left join IPM_PROJECT_FIELD_VALUE P7 on P.ProjectID = P7.ProjectID and P7.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_SHORT_NAME') "
                        Sql1 &= "left join IPM_STATE S on S.State_id = P.State_id "
                        Sql1 &= "where P.ProjectID = " & ProjectId & ""

                        ClientsListDT = New DataTable("ClientsListDT")
                        ClientsListDT = mmfunctions.GetDataTable(Sql1)

                        If ClientsListDT.Rows.Count > 0 Then
                            ProjectExist = 1
                            ProjectLink = "project_" & ClientsListDT.Rows(0)("ProjectID").ToString.Trim & "_" & CMSFunctions.FormatFriendlyUrl(ClientsListDT.Rows(0)("Name").ToString.Trim.ToLower, "_")
                            ProjectName = formatfunctionssimple.AutoFormatText(ClientsListDT.Rows(0)("Name").ToString.Trim)
                            ProjectAddress = formatfunctions.AutoFormatText(ClientsListDT.Rows(0)("ProjectAddress").ToString.Trim)
                        End If
                    End If

                    If ClientName <> "" Then
                        out &= "<li>"
                        If ProjectId <> "" AndAlso ProjectExist = 1 Then
                            out &= "<a href=""" & ProjectLink & """ class=""clientF"">" & ClientName & "</a>"
                        Else
                            out &= "<a class=""clientF"">" & ClientName & "</a>"
                        End If

                        If ProjectId <> "" AndAlso ProjectExist = 1 Then
                            out &= "<div class=""clientF"">"
                            out &= "<a href=""" & ProjectLink & """ class=""blocked"">"
                            out &= "<img src=""/dynamic/image/week/project/best/220x220/92/ffffff/Center/" & ProjectId & ".jpg"" alt=""""/>"
                            out &= "</a>"
                            out &= "<div class=""info_pop inner_pop"">"
                            out &= "<h2><a href=""" & ProjectLink & """>" & ProjectName & "</a></h2>"
                            out &= "<span>"
                            out &= "<a href=""" & ProjectLink & """>" & ProjectAddress & "</a>"
                            out &= "</span>"
                            out &= "</div>"
                            out &= "</div>"
                        End If
                        out &= "</li>"

                    End If

                    IndicativPeColoana = IndicativPeColoana + 1
                Else
                    out &= "</ul>"
                    IndicativPeColoana = 0
                    NumarColoana = NumarColoana + 1
                    ' inserez coloana noua
                    If i <> ClientsListDT.Rows.Count - 1 Then
                        out &= "<ul class=""client_list clientsCol" & NumarColoana & """>"
                    End If

                End If

            End If

        Next

        If IndicativPeColoana > 0 Then
            out &= "</ul>"
        End If
    
        'Futured
        Dim Sql2 As String = ""
        Sql2 &= "select top 1 "
        Sql2 &= "p.ProjectID, "
        Sql2 &= "P.Name as ProjectName, "
        Sql2 &= "p.ClientName, "
        Sql2 &= "case when isnull(P.State_id,'') <> '' or P.State_id is null then P.City + case when isnull(S.Name ,'') <> '' then ', ' + S.Name else '' end else p.City + case when isnull(P5.Item_Value,'') <> '' then ', ' + P5.Item_Value else '' end + case when isnull(P4.Item_Value,'') <> '' then ', ' + P4.Item_Value else '' end end as ProjectAddress "
        Sql2 &= "FROM IPM_PROJECT P "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P8 on P.ProjectID = P8.ProjectID and P8.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUBPRACTICEAREA') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P2 on P.ProjectID = P2.ProjectID and P2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PRACTICEAREA') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P3 on P.ProjectID = P3.ProjectID and P3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_FEATURED') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P4 on P.ProjectID = P4.ProjectID and P4.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_CITY') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P5 on P.ProjectID = P5.ProjectID and P5.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_STATE') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P1 on P.ProjectID = P1.ProjectID and P1.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_REGIONS') "
        Sql2 &= "left join IPM_STATE S on S.State_id = P.State_id "
        Sql2 &= "where p.ClientName is not null and p.ClientName <> '' and p.ClientName <> '<Unknown>' "
        Sql2 &= "and P3.Item_Value = 1 "
        Sql2 &= "and P.Available = 'y' "
        If IsNumeric(CategoryId) = True Then
            If CategoryId = "3409328" Or CategoryId = "2400014" Or CategoryId = "2400017" Or CategoryId = "3409341" Or CategoryId = "3409345" Or CategoryId = "3409347" Or CategoryId = "3409349" Or CategoryId = "2400022" Or CategoryId = "3409351" Then
                Sql2 &= "and charindex('" & practiceName & "',LTRIM(RTRIM(P8.Item_Value)))!= 0 "
            Else
                Sql2 &= "and charindex('" & practiceName & "',LTRIM(RTRIM(P2.Item_Value)))!= 0 "
            End If
        Else
            Sql2 &= "and charindex('" & practiceName & "',LTRIM(RTRIM(P1.Item_Value)))!= 0 "
        End If
        Sql2 &= "group by p.ClientName, p.ProjectID, P.Name, case when isnull(P.State_id,'') <> '' or P.State_id is null then P.City + case when isnull(S.Name ,'') <> '' then ', ' + S.Name else '' end else p.City + case when isnull(P5.Item_Value,'') <> '' then ', ' + P5.Item_Value else '' end + case when isnull(P4.Item_Value,'') <> '' then ', ' + P4.Item_Value else '' end end  "
        Sql2 &= "ORDER BY NEWID() "
        FuturedClientDT = New DataTable("FuturedClientDT")
        FuturedClientDT = mmfunctions.GetDataTable(Sql2)

        client_list1.Text = out

        breadcrumbs_list.Text = breadcrumbsOut

        Dim LastVisitedCookie As New HttpCookie("LastVisited")
        LastVisitedCookie.Values("parent") = "clients," & ""
        LastVisitedCookie.Values("lastVisit") = DateTime.Now.ToString()
        LastVisitedCookie.Expires = DateTime.Now.AddDays(1)
        Response.Cookies.Add(LastVisitedCookie)


    End Sub



    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub




End Class
