﻿Imports System.Data

Partial Class news_press_release
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sql As String = ""
        Dim out As String = ""
        Dim out2 As String = ""

        sql &= "Select "
        sql &= "n.News_Id, "
        sql &= "n.Headline, "
        sql &= "n.Content, "
        sql &= "n.Picture, "
        sql &= "n2.Item_Value as VideoLink, "
        sql &= "n.PDF, "
        sql &= "v1.Item_Value as Feature, "
        sql &= "DATENAME(mm,n.Post_Date) + ' ' + DATENAME(day,n.Post_Date) + ', ' + DATENAME(year, n.Post_Date) + COALESCE(' , ' + nullif(ltrim(v.Item_Value),''), '') DateLoc "
        sql &= "From IPM_NEWS n "
        sql &= "Left Join IPM_NEWS_FIELD_VALUE v ON (n.News_id = v.News_id AND v.Item_ID = 3409256) "
        sql &= "Left Join IPM_NEWS_FIELD_VALUE v1 ON (n.News_id = v1.News_id AND v1.Item_ID = 30052967) "
        sql &= "left join IPM_NEWS_FIELD_VALUE n2 on N.News_Id = n2.NEWS_ID and n2.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_VIDEOLINK') "
        sql &= "left join IPM_NEWS_FIELD_VALUE n3 on N.News_Id = n3.NEWS_ID and n3.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWS_FUTURED') "
        sql &= "Where(n.Show = 1 And YEAR(Post_Date) > " & (Date.Today.Year - 6) & " And n.Post_Date < GETDATE() And n.Pull_Date > GETDATE() And n.Type = 8 AND n3.Item_Value = '1' ) "

        sql &= "Order By n.Post_Date DESC "
        Dim DT1 As New DataTable("NewsDT")
        DT1 = mmfunctions.GetDataTable(sql)
        If DT1.Rows.Count > 0 Then
            For R As Integer = 0 To DT1.Rows.Count - 1
                out &= "<li>"
                out &= "<div class=""photo_holder"">"
                out &= "<img src=""" & "/dynamic/image/week/news/best/220x145/92/ffffff/West/" & DT1.Rows(R)("News_ID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
                out &= "</div>"
                out &= "<div class=""info_holder"">"

                If DT1.Rows(R)("PDF").ToString.Trim = "1" Then
                    out &= "<h1><a href=""/dynamic/document/week/asset/download/" & DT1.Rows(R)("News_ID").ToString.Trim & "/" & DT1.Rows(R)("News_ID").ToString.Trim & ".pdf"" title="""">" & formatfunctionssimple.AutoFormatText(DT1.Rows(R)("Headline").ToString.Trim) & "</a></h1>"
                Else
                    out &= "<h1><a title="""">" & formatfunctionssimple.AutoFormatText(DT1.Rows(R)("Headline").ToString.Trim) & "</a></h1>"
                End If
                out &= "<span>" & formatfunctions.AutoFormatText(DT1.Rows(R)("DateLoc").ToString.Trim) & "</span>"
                out &= "<div class=""text"">"
                out &= "<p>" & formatfunctions.AutoFormatText(DT1.Rows(R)("Content").ToString.Trim) & "</p>"
                out &= "<a href=""#testimonial_" & R + 1 & """ title="""" class=""view_full_release"">View full press release</a>"
                If DT1.Rows(R)("PDF").ToString.Trim = "1" Then
                    out &= "<a href=""/dynamic/document/week/asset/download/" & DT1.Rows(R)("News_ID").ToString.Trim & "/" & DT1.Rows(R)("News_ID").ToString.Trim & ".pdf"" title="""">PDF of press release</a>"
                Else
                    out &= "<a title="""">PDF of press release</a>"
                End If
                If DT1.Rows(R)("VideoLink").ToString.Trim <> "" Then
                    out &= "<a href=""" & formatfunctionssimple.AutoFormatText(DT1.Rows(R)("VideoLink").ToString.Trim) & """ class=""video vimeo"">View video</a>"
                End If
                out &= "</div>"
                out &= "</div>"
                out &= "</li>"
                out2 &= "<div id=""testimonial_" & R + 1 & """ class=""popup_box"" >"
                out2 &= "<h3 class=""sub_title"">" & formatfunctions.AutoFormatText(DT1.Rows(R)("Headline").ToString.Trim) & "</h3>"
                out2 &= "<strong>" & DT1.Rows(R)("DateLoc").ToString.Trim & "</strong>"
                out2 &= "<div class=""popup_info full_news_pop"">"
                out2 &= "<img src=""" & "/dynamic/image/week/news/best/220x145/92/ffffff/West/" & DT1.Rows(R)("News_ID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
                out2 &= "<div class=""text"">"
                out2 &= "<p>" & formatfunctions.AutoFormatText(DT1.Rows(R)("Feature").ToString.Trim) & "</p>"
                out2 &= "</div>"
                out2 &= "</div>"
                out2 &= "</div>"
            Next
            news_press_release_list1.Text = out
            news_press_release_popups1.Text = out2
        End If

    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub

End Class
