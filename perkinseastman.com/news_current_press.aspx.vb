﻿Imports System.Data

Partial Class news_current_press
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sql As String = ""
        Dim out As String = ""

        sql &= "select "
        sql &= "n.News_ID, "
        sql &= "n.Headline, "
        sql &= "n.Content, "
        sql &= "n2.Item_Value as VideoLink, "
        sql &= "v.Item_Value url, "
        sql &= "DATENAME(mm,n.Post_Date) + ' ' + DATENAME(day,n.Post_Date) + ', ' + DATENAME(year, n.Post_Date) PostDate "
        sql &= "From IPM_NEWS n left join IPM_NEWS_FIELD_VALUE v on n.News_Id = v.NEWS_ID and v.Item_ID = 3407671 "
        sql &= "left join IPM_NEWS_FIELD_VALUE n2 on N.News_Id = n2.NEWS_ID and n2.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_VIDEOLINK') "
        sql &= "left join IPM_NEWS_FIELD_VALUE n3 on N.News_Id = n3.NEWS_ID and n3.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWS_FUTURED') "
        sql &= "Where(n.Show = 1 And n.Post_Date < GETDATE() And YEAR(Post_Date) > " & (Date.Today.Year - 3) & " And  n.Pull_Date > GETDATE() And n.Type = 0 AND n3.Item_Value = '1' ) "
        sql &= "Order By n.Post_Date DESC "

        Dim DT1 As New DataTable("NewsDT")
        DT1 = mmfunctions.GetDataTable(sql)
        If DT1.Rows.Count > 0 Then

            For R As Integer = 0 To DT1.Rows.Count - 1
                out &= "<li>"
        If String.IsNullOrEmpty(DT1.Rows(R)("url").ToString) Then
                out &= "<a title="""">"
        Else
                out &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(DT1.Rows(R)("url").ToString.Trim) & """ title="""">"
        End If
                out &= "" & formatfunctionssimple.AutoFormatText(DT1.Rows(R)("Headline").ToString.Trim) & ""
                out &= "</a>"
                out &= "<em>&ldquo;" & formatfunctionssimple.AutoFormatText(DT1.Rows(R)("Content").ToString.Trim) & ",&rdquo; " & formatfunctions.AutoFormatText(DT1.Rows(R)("PostDate").ToString.Trim) & "</em>"
                If DT1.Rows(R)("VideoLink").ToString.Trim <> "" Then
                    out &= "<a href=""" & formatfunctionssimple.AutoFormatText(DT1.Rows(R)("VideoLink").ToString.Trim) & """ class=""video vimeo"">View video</a>"
                End If
                out &= "</li>"
            Next
            news_current_press_list1.Text = out
        End If
    End Sub


    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub


End Class
