﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="publications.aspx.vb" Inherits="publications" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_Insights"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Insights")%></a><span> | </span></li>
                        <li><a class="active" href="<%=Master.UrlPage%>"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Publications")%></a></li>
                    </ul>
                </div>
                <div class="headline">
                    <h2 class="no_text_transform cmsMultiLanguage cms cmsType_TextSingle cmsName_Publications"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Publications")%></h2>
                </div>
                
                <ul class="white_papers_list" id="publications_list">

                <asp:Literal runat="server" ID="publications_list1"/>                    
                                      
                </ul>
</asp:Content>