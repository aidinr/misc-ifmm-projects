﻿Imports System.Data
Imports System.IO
Partial Class china_clients_cn
    Inherits System.Web.UI.Page

    Public MainCategory As String
    Public practiceName As String
    Public MainPracticeId As String
    Public MainPracticeName As String
    Public practiceAreaUDFID As String
    Public breadcrumbsOut As String

    Public ClientsListDT As New DataTable
    Public FuturedClientDT As DataTable
    Public CategoryId As String

    Public FriendlyPracticeName As String
    Public FriendlyCMSName As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        CategoryId = "China"
        MainCategory = "By REGION"
        practiceName = "China"

        FriendlyPracticeName = "China_Cn"
        FriendlyCMSName = FriendlyPracticeName

        clients_list_title.Text = "作品"


        If CategoryId <> "" Then
            breadcrumbsOut &= "<li><a title="""" class=""first"">Work</a><span> | </span></li>" & vbNewLine
            breadcrumbsOut &= "<li><a title="""">" & MainCategory & "</a><span> | </span></li>" & vbNewLine
            If MainPracticeName <> "" Then
                Dim MainCatFriendlyLink As String = ""
                MainCatFriendlyLink = "practice_" & MainPracticeId & "_" & formaturl.friendlyurl(MainPracticeName.ToLower)
                breadcrumbsOut &= "<li><a href=""" & MainCatFriendlyLink & """ title="""">" & MainPracticeName & "</a><span> | </span></li>" & vbNewLine
            End If
            If MainCategory = "By PRACTICE" Then
                Dim CatFriendlyLink As String = ""
                If practiceName = "Corporate Interiors" Then
                    CatFriendlyLink = "corporate_interiors"
                Else
                    CatFriendlyLink = "category_" & CategoryId & "_" & formaturl.friendlyurl(practiceName.ToLower)
                End If

                breadcrumbsOut &= "<li><a href=""" & CatFriendlyLink & """ title="""">" & practiceName & "</a><span> | </span></li>" & vbNewLine
            Else
                Dim RegionFriendlyLink As String = ""
                RegionFriendlyLink = "region_" & formaturl.friendlyurl(practiceName)
                breadcrumbsOut &= "<li><a href=""" & RegionFriendlyLink & """ title="""">" & practiceName & "</a><span> | </span></li>" & vbNewLine
            End If
            breadcrumbsOut &= "<li><a title="""" class=""active"">业主名单浏览</a></li>" & vbNewLine
            breadcrumbsOut &= "<li><a href=""clients_China"" title="""">View In English</a><span> </span></li>" & vbNewLine
        End If


        'Futured
        Dim Sql2 As String = ""
        Sql2 &= "select top 1 "
        Sql2 &= "p.ProjectID, "
        Sql2 &= "P.Name as ProjectName, "
        Sql2 &= "P9.Item_Value as ProjectNameCn, "
        Sql2 &= "P10.Item_Value as ProjectAddressCn, "
        Sql2 &= "p.ClientName, "
        Sql2 &= "case when isnull(P.State_id,'') <> '' or P.State_id is null then P.City + case when isnull(S.Name ,'') <> '' then ', ' + S.Name else '' end else p.City + case when isnull(P5.Item_Value,'') <> '' then ', ' + P5.Item_Value else '' end + case when isnull(P4.Item_Value,'') <> '' then ', ' + P4.Item_Value else '' end end as ProjectAddress "
        Sql2 &= "FROM IPM_PROJECT P "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P8 on P.ProjectID = P8.ProjectID and P8.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUBPRACTICEAREA') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P2 on P.ProjectID = P2.ProjectID and P2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PRACTICEAREA') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P3 on P.ProjectID = P3.ProjectID and P3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_FEATURED') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P4 on P.ProjectID = P4.ProjectID and P4.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_CITY') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P5 on P.ProjectID = P5.ProjectID and P5.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_STATE') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P1 on P.ProjectID = P1.ProjectID and P1.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_REGIONS') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P9 on P.ProjectID = P9.ProjectID and P9.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_NAME_CHINA') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P10 on P.ProjectID = P10.ProjectID and P10.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_ADDRESS_CHINA') "

        Sql2 &= "left join IPM_STATE S on S.State_id = P.State_id "
        Sql2 &= "WHERE P.Available = 'Y' "
        Sql2 &= "AND P.Show = 1 "
        Sql2 &= "AND P9.Item_Value is not null AND P9.Item_Value <> '' "
        Sql2 &= "AND P10.Item_Value is not null AND P9.Item_Value <> '' "
        If IsNumeric(CategoryId) = True Then
            If CategoryId = "3409328" Or CategoryId = "2400014" Or CategoryId = "2400017" Or CategoryId = "3409341" Or CategoryId = "3409345" Or CategoryId = "3409347" Or CategoryId = "3409349" Or CategoryId = "2400022" Or CategoryId = "3409351" Then
                Sql2 &= "AND charindex('" & practiceName & "',LTRIM(RTRIM(P8.Item_Value)))!= 0 "
            Else
                Sql2 &= "AND charindex('" & practiceName & "',LTRIM(RTRIM(P2.Item_Value)))!= 0 "
            End If
        Else
            Sql2 &= "AND charindex('" & practiceName & "',LTRIM(RTRIM(P1.Item_Value)))!= 0 "
        End If

        Sql2 &= "ORDER BY NEWID() "
        FuturedClientDT = New DataTable("FuturedClientDT")
        FuturedClientDT = mmfunctions.GetDataTable(Sql2)
        Dim out1 As String = ""

        If FuturedClientDT.Rows.Count > 0 Then
            Dim sqlImg As String = ""
            sqlImg &= "select top 9 * from "
            sqlImg &= " (select a.Asset_ID, a.projectid, 1 apply, c.Item_value from ipm_asset_category b, ipm_asset a "
            sqlImg &= "left join ipm_asset_field_value c on c.item_id = 2400079 and c.asset_id = a.asset_id "
            sqlImg &= "where a.projectid = '" & FuturedClientDT.Rows(0)("ProjectID").ToString.Trim & "'"
            sqlImg &= "and a.category_id = b.category_id "
            sqlImg &= "and a.available = 'y' "
            sqlImg &= "and b.available = 'y' "
            sqlImg &= "and b.name = 'Public Website Images' "
            sqlImg &= "and a.HPixel >= 350 "
            sqlImg &= "UNION select a.Asset_ID, a.projectid, b.Item_Value apply, c.Item_value "
            sqlImg &= "from ipm_asset a "
            sqlImg &= "left join ipm_asset_field_value B on B.item_id = 2400401 and B.asset_id = a.asset_id "
            sqlImg &= "left join ipm_asset_field_value c on c.item_id = 2400079 and c.asset_id = a.asset_id "
            sqlImg &= "where a.projectid = '" & FuturedClientDT.Rows(0)("ProjectID").ToString.Trim & "'"
            sqlImg &= "AND a.available = 'y' and B.Item_value = 1 and b.Item_Value is not null  and a.HPixel >= 350) "
            sqlImg &= "a order by a.item_value desc "
            Dim ProjectImageDT As DataTable
            ProjectImageDT = New DataTable("ProjectImageDT")
            ProjectImageDT = mmfunctions.GetDataTable(sqlImg)
            Dim PrFriendlyLink As String = "project_" & FuturedClientDT.Rows(0)("ProjectID").ToString.Trim & "_" & formaturl.friendlyurl(FuturedClientDT.Rows(0)("ProjectName").ToString.Trim.ToLower)
            out1 &= "<div class=""big_image_with_info_box"">"
            If ProjectImageDT.Rows.Count > 0 Then
                out1 &= "<a href=""" & PrFriendlyLink & """>"
            Else
                out1 &= "<a title="""">"
            End If
            out1 &= "<img src=""/dynamic/image/week/project/best/220x230/92/ffffff/North/" & FuturedClientDT.Rows(0)("ProjectID").ToString.Trim & ".jpg"" alt=""""/>"
            out1 &= "<em class=""info_pop inner_pop"">"
            out1 &= "<em>" & FuturedClientDT.Rows(0)("ProjectNameCn").ToString.Trim & "</em>"
            out1 &= "<span>" & FuturedClientDT.Rows(0)("ProjectAddressCn").ToString.Trim & "</span>"
            out1 &= "</em></a>"
            out1 &= "</div>"
            If MainCategory = "By PRACTICE" Then
                Dim CatFriendlyLink As String = ""
                CatFriendlyLink = "category_" & CategoryId & "_" & formaturl.friendlyurl(practiceName.ToLower)
                If practiceName = "Corporate Interiors" Then
                    out1 &= "<a href=""corporate_interiors"" class=""view_more"">Back to Corporate Interiors</a>"
                Else
                    out1 &= "<a href=""" & CatFriendlyLink & """ class=""view_more"">Back to" & " " & practiceName & "</a>"
                End If
            Else
                Dim RegionFriendlyLink As String = ""
                RegionFriendlyLink = "region_" & formaturl.friendlyurl(practiceName)

                out1 &= "<div class=""cms cmsType_Rich cmsName_China_Clinets_Cn_Right"">"
                Dim TextCnRight As String = ""
                TextCnRight = CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "China_Clinets_Cn_Right")
                If TextCnRight <> "Not Available" Then
                    out1 &= TextCnRight
                End If
                out1 &= "</div>"

                out1 &= "<a href=""" & RegionFriendlyLink & """ class=""view_more gallery"">按区域中国</a>"
            End If

        End If


        featured_client_lit.Text = out1
        breadcrumbs_list.Text = breadcrumbsOut

        'Dim LastVisitedCookie As New HttpCookie("LastVisited")
        'LastVisitedCookie.Values("parent") = "clients," & ""
        'LastVisitedCookie.Values("lastVisit") = DateTime.Now.ToString()
        'LastVisitedCookie.Expires = DateTime.Now.AddDays(1)
        'Response.Cookies.Add(LastVisitedCookie)
    End Sub



    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub




End Class
