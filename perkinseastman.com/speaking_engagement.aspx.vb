﻿Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Partial Class speaking_engagement
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sql As String = "Select e.Headline, V2.Item_Value as EventTitle, V3.Item_Value as VideoLink, e.Content, DATENAME(mm,e.Event_Date) + ' ' + DATENAME(day,e.Event_Date) + ', ' + DATENAME(year, e.Event_Date) Event_Date, l.Item_Value Location, p.Item_Value Presenter, v.Item_Value Venue, V1.Item_Value as LinkU "
        sql &= "From IPM_EVENTS e "
        sql &= "Left join IPM_EVENTS_FIELD_VALUE l on (l.EVENT_ID = e.Event_Id and l.Item_ID = 3408650) "
        sql &= "Left join IPM_EVENTS_FIELD_VALUE p on (p.EVENT_ID = e.Event_Id and p.Item_ID = 3408649) "
        sql &= "Left join IPM_EVENTS_FIELD_VALUE v on (v.EVENT_ID = e.Event_Id and v.Item_ID = 3408651) "
        sql &= "left join IPM_EVENTS_FIELD_VALUE V1 on e.Event_Id = V1.EVENT_ID and V1.Item_ID = (Select Item_ID From IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_LINK') "
        sql &= "left join IPM_EVENTS_FIELD_VALUE V2 on e.Event_Id = V2.EVENT_ID and V2.Item_ID = (Select Item_ID From IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_EVENTTITLE') "
        sql &= "left join IPM_EVENTS_FIELD_VALUE V3 on e.Event_Id = V3.EVENT_ID and V3.Item_ID = (Select Item_ID From IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_EVENTVIDEO') "
        sql &= "Where(e.Type = 5 And e.Post_Date < GETDATE() And e.Pull_Date > GETDATE() And e.Show = 1) "
        sql &= "Order By e.Event_Date ASC"
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)

        Dim out As String = ""
        For i As Integer = 0 To DT1.Rows.Count - 1
            out &= " <li> "
            out &= "<div class=""info_holder"">"

            out &= "<h1>"
            If DT1.Rows(i)("LinkU").ToString.Trim <> "" Then
                out &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(DT1.Rows(i)("LinkU").ToString.Trim) & """ title="""" >"
            Else
                out &= "<a title="""" >"
            End If
            out &= formatfunctionssimple.AutoFormatText(DT1.Rows(i)("Headline").ToString.Trim) & "</a>"
            out &= "</h1>"
            out &= "<span>" & formatfunctions.AutoFormatText(DT1.Rows(i)("EventTitle").ToString.Trim) & "</span>"
            out &= "<span>" & formatfunctions.AutoFormatText(DT1.Rows(i)("Presenter").ToString.Trim) & " " & formatfunctions.AutoFormatText(DT1.Rows(i)("Venue").ToString.Trim) & "</span>"
            out &= "<em>" & formatfunctions.AutoFormatText(DT1.Rows(i)("Event_Date").ToString.Trim) & ", " & formatfunctions.AutoFormatText(DT1.Rows(i)("Location").ToString.Trim) & "</em>"
            out &= "<div class=""text"" > "
            out &= "<p>"
            out &= formatfunctions.AutoFormatText(DT1.Rows(i)("Content").ToString.Trim)

            out &= "<br />"
            If DT1.Rows(i)("VideoLink").ToString.Trim <> "" Then
                out &= "<a href=""" & formatfunctionssimple.AutoFormatText(DT1.Rows(i)("VideoLink").ToString.Trim) & """ class=""video vimeo"">View video</a>"
            End If

            out &= "</p>"
            out &= "</div> "
            out &= "</div>"
            out &= "</li>"
            out &= vbNewLine
        Next
        speaking_engagement_list1.Text = out
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub

End Class
