﻿Imports System.Data

Partial Class practice
    Inherits System.Web.UI.Page
    Public SubPracticeDT As DataTable
    Public PracticeName As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PracticeId As Integer = Request.QueryString("pr")

        'Dim listofprojects As String = "2400017,2400014,3409328,3409341"

        Dim out As String = ""
        Dim sql As String = ""
        sql &= "select "
        sql &= "P.ProjectID, "
        sql &= "PP.Name as PracticeName, "
        sql &= "P.Name AS SubPracticeName "
        sql &= "from IPM_PROJECT P "
        sql &= "JOIN IPM_PROJECT PP ON PP.ProjectID =" & PracticeId & ""
        If PracticeId = 3409338 Then
            sql &= "where P.ProjectID in (2400017,2400014,3409328,3409341)   "
            sql &= "order by Case when P.ProjectId = 2400017 then 1 "
            sql &= "when P.ProjectId = 2400014 then 2 "
            sql &= "when P.ProjectId = 3409328 then 3 "
            sql &= "when P.ProjectId = 3409341 then 4 end "
        ElseIf PracticeId = 3409343 Then
            sql &= "where P.ProjectID in (3409345,3409347,3409349,2400022,3409351) "
            sql &= "order by Case when P.ProjectId = 3409345 then 1 "
            sql &= "when P.ProjectId = 3409347 then 2 "
            sql &= "when P.ProjectId = 3409349 then 3 "
            sql &= "when P.ProjectId = 2400022 then 4 "
            sql &= "when P.ProjectId = 3409351 then 5 end "
        End If
        SubPracticeDT = New DataTable("SubPracticeDT")
        SubPracticeDT = mmfunctions.GetDataTable(sql)
        If SubPracticeDT.Rows.Count > 0 Then

            practice_name.Text = SubPracticeDT.Rows(0)("PracticeName").ToString.Trim
            PracticeName = SubPracticeDT.Rows(0)("PracticeName").ToString.Trim

            For R As Integer = 0 To SubPracticeDT.Rows.Count - 1

                Dim CatFriendlyLink As String = ""
                CatFriendlyLink = "category_" & SubPracticeDT.Rows(R)("ProjectID").ToString.Trim & "_" & formaturl.friendlyurl(SubPracticeDT.Rows(R)("SubPracticeName").ToString.Trim.ToLower)


                '                <li>
                ' <span class="cmsMultiLanguage cms cmsType_TextSingle cmsName_k_12_education"></span>
                ' <a href="category_2400017_k_12_education" title=""><span><ins class="lang lngEnglish">k 12 education</ins><ins class="lang lngArabic">ee</ins><ins class="lang lngMandarin">eqeqeq</ins></span><img src="/dynamic/image/week/project/best/164x164/92/777777/Center/2400017.jpg" alt=""/></a>
                '</li>

                out &= " <li>"
                out &= "<span class=""cmsMultiLanguage cms cmsType_TextSingle cmsName_" & formaturl.friendlyurl(SubPracticeDT.Rows(R)("SubPracticeName").ToString.Trim.ToLower) & """></span>"
                out &= "<a href=""" & CatFriendlyLink & """ title=""""><span class=""cmsMultiLanguage cms cmsType_TextSingle cmsName_" & formaturl.friendlyurl(SubPracticeDT.Rows(R)("SubPracticeName").ToString.Trim.ToLower) & """>" & CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", formaturl.friendlyurl(SubPracticeDT.Rows(R)("SubPracticeName").ToString.Trim.ToLower)) & "</span>"
                out &= "<img src=""/dynamic/image/week/project/best/164x164/92/777777/Center/" & SubPracticeDT.Rows(R)("ProjectID").ToString.Trim & ".jpg"" alt=""""/>"
                out &= "</a>"
                out &= "</li>"

            Next
            subpractice_list1.Text = out

        End If
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub


End Class
