document.write('<style id="hideBody" type="text/css">body{visibility:hidden}</style>');
function removeID(id){return (elem=document.getElementById(id)).parentNode.removeChild(elem);}
setTimeout("removeID('hideBody')",900)
      WebFontConfig = {
        google: { families: [ 'Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800:latin' ] }
      };
      (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
      })();

history.navigationMode = 'compatible';
$('*').unbind();

$(window).unload(function () {
    $('*').unbind();
    $('body', 'html').val('');
});

var moving = 0;
var printPage;
var homeAuto;
var startCar=0;
var scripts = document.getElementsByTagName('script');
var jsPath = './';
if (scripts) {
    if (scripts[0].src) jsPath = scripts[0].src.split("assets")[0];
}

$(document).ready(function() {
  ready();
  if(!readCookie('SiteEdit')) {
   $('body, div, img').attr('oncontextmenu','return false');
   $('img, h1, a').on('dragstart', function(event) { event.preventDefault(); });
   if(window.navigator.userAgent.indexOf('Trident')>0) {
    document.onselectstart = function() { return false; }
   }
  }
});

// Start Ready

function ready () {
    setTimeout("$('body').css('visibility','visible')",210) 
   
    if ($('#keywordSearch').attr('id')) {

        $('#keywordSearch').attr('autocomplete', 'off');
        $('#keywordSearch').val('Search Perkins Eastman');
        $('#keywordSearch').focus(function () {
            $(this).val('');
        });
        $('#keywordSearch').blur(function () {
            if (!$(this).val()) $(this).val('Search Perkins Eastman');
        });
    }

    if($('body').hasClass('categoryaspx') || $('body').hasClass('corporateinteriorsaspx') || $('body').hasClass('seniorlivingaspx') || $('body').hasClass('regionaspx') || $('body').hasClass('affiliateprojectsaspx')) {

      createCookie('bread', encode64($('.breadcrumbs ul').html()),2);
      var sisters='';
       $('ul.main_list li a').each(function () { 
       sisters +=$(this).attr('href')+'|';
      });
      createCookie('sisters', sisters.slice(0, -1),2);
    }
    else if($('body').hasClass('projectDetailaspx')) {
      var bread = readCookie('bread');
      if(bread) {
        bread=decode64(bread);
        $('.breadcrumbs ul li:eq(0)').remove();
        $('.breadcrumbs ul li:eq(0)').remove();
        $('.breadcrumbs ul li:eq(0)').prepend('<span> | </span> ');
        $('.breadcrumbs ul').prepend(bread);
        $('.breadcrumbs ul li:not(:last-child) a.active').removeClass('active');

        var sisters = readCookie('sisters');
        if(sisters && $('ul.project_nav').is('ul')) {
         var sissy = sisters.split('|');
         var si=Number(sissy.indexOf(window.location.pathname.replace('/','')));
         if (si<(sissy.length-1)) $('ul.project_nav').prepend('<li><a href="'+  sissy[(si+1)] +'">Next Project</a></li>');
         if (si>0) $('ul.project_nav').prepend('<li><a href="'+  sissy[(si-1)] +'">Previous Project</a></li>');
        }
       }
    }
    else {
     createCookie('bread', null, -1);
     createCookie('sisters', null, -1);
    }

    // Home big image with caption slide
    if($('.homepage_slideshow').length > 0) {
        if($('.cmsName_Home_Page_Carousel_Random').html().toLowerCase().replace(/[^a-z]/g,'').length>2) $('.homepage_slideshow .w > div').randomize();
        clearInterval(homeAuto);
        $('.homepage_slideshow').append('<a title="Previous" class="home_previous"></a><a title="Next" class="home_next"></a>');
        loadMe();
        // show infox box
        $('.homepage_slideshow a.home_next, .homepage_slideshow a.home_previous').hover(function(){
           $(this).addClass('hover'); 
        },function(){
           $(this).removeClass('hover'); 
        });
        $('.homepage_slideshow>div').hover(function(){
         //  $('.home_previous, .home_next').css('display','block');
           $('.home_previous, .home_next').fadeIn(170);
           $(this).addClass('hover'); 
           $(this).find('.info_pop').slideDown(); 
        },function(){
         $(this).removeClass('hover'); 
         setTimeout("hoverHomeOut()",300)
         });
        $('.homepage_slideshow a.home_previous').click(function(){
         clearInterval(homeAuto);
         if(moving==1) return false;
         moving = 1;
         $('.homepage_slideshow>div.w').prepend($('.homepage_slideshow>div.w>div').last());
         $('.homepage_slideshow>div.w').css('marginLeft','-940px');
         $('.homepage_slideshow div.w').animate({marginLeft: '0'}, 650, function() {
          moving = 0;
         });
        });
        $('.homepage_slideshow a.home_next').click(function(){
         clearInterval(homeAuto);
         homePageSlideNext();
        });
    }
    
    // Inner tabs 
    if($('.inner_tabs').length > 0)
    {
        $('.inner_tabs ul li a').first().addClass('active');
        $('.inner_tabs .big_image_with_info_box').first().show();
        
        $('.inner_tabs ul li a').on('hover', function(){
            
            var item = $(this).attr('rel');
            $('.inner_tabs ul li a').removeClass('active');
            $(this).addClass('active');
            $('.inner_tabs .big_image_with_info_box').hide();
            $('.inner_tabs #box_'+item).show();
            $('a.enlargeImage').attr('href',$(this).attr('href'));
        });
    }    
    
    // Switching between projects
    if($('.project_list').length > 0)
    {
        $('.project_list dd a').first().addClass('active');
        $('.inner_tabs .big_image_with_info_box').first().show();
        
        $('.project_list dd a').on('hover', function(){
            var item = $(this).attr('rel');
            $('.project_list dd a').removeClass('active');
            $(this).addClass('active');
            $('.inner_tabs .big_image_with_info_box').hide();
            $('.inner_tabs #box_'+item).show();
            
        });
    }        
 
    // Project gallery
    if($('#project_gallery').length > 0) {
     $('.main_list.small_thumbs li a').attr('fancyGroup','gal');
      $('a.enlargeImage').attr('href',$('#project_gallery.inner_tabs .main_list li a:eq(0)').attr('href'));
      $('.main_list.small_thumbs li a').fancybox({groupAttr: 'fancyGroup'});
      $('a.boxImg, a.enlargeImage').click(function(){
       $('.main_list.small_thumbs li a.active:eq(0)').click();
       return false;
      });
    } 
    
    // Testimonial popup boxes
    if($('.testimonial_list').length > 0) {
    	$('.testimonial_list li a').fancybox({'hideOnContentClick': true});
        if(location.hash) {
         $('a[href$="'+ location.hash +'"]').click();
        }
    }     
    
    // News pressrelease pops
    if($('#news_press_release_list').length > 0)
    {
    	$('.view_full_release').fancybox({'hideOnContentClick': true});
    }     
    
    // Leaderships associates popups    
    if($('.leadership_associates_list').length > 0)
    {
    	$('.leadership_associates_list li a').fancybox({'hideOnContentClick': true});
    }     
    // Casestudy slides
    if($('.slide_controls').length > 0)
    {
        var slide_progression = $('#slide_progression');
        var max_slides = $('#max_slides').html();
        var i = 1;

        $('.next_slide').click(function(){
            $('.previous_slide').css('visibility','visible');
            if(i < max_slides)
            {
                if((i+1)==max_slides) $('.next_slide').css('visibility','hidden');
                $('#slide_'+i).hide();
                i++;
                $('#slide_'+i).show();
                slide_progression.html(i);
                return false;                   
            }            
        });
        $('.previous_slide').click(function(){
            $('.next_slide').css('visibility','visible');
            if(i > 1) {
                if(i==2) $('.previous_slide').css('visibility','hidden');
                $('#slide_'+i).hide();
                i--;
                $('#slide_'+i).show();
                slide_progression.html(i);
                return false;                    
            }
        });   
    }

   // Start Menu


    $('.menu ul li a').hover(function() {     
        $('.mega_menu').slideDown();
        $('.mega_menu').addClass('menuOn');
     }, 
        function () {
        setTimeout("if(!$('.mega_menu.menuOn').hasClass('menuOn')) $('.mega_menu').slideUp();",300);
     }
    );

    $('.menu').hover(function() {
        $('.mega_menu').addClass('menuOn');
        }, 
        function () {
        $('.mega_menu').removeClass('menuOn');
        setTimeout("if(!$('.mega_menu.menuOn').hasClass('menuOn')) $('.mega_menu').slideUp();",300);
     }
    );

    $('.mega_menu').hover(function() {     
        $('.mega_menu').slideDown();
        $('.mega_menu').addClass('menuOn');
     }, 
        function () {
        $('.mega_menu').removeClass('menuOn');
        setTimeout("if(!$('.mega_menu.menuOn').hasClass('menuOn')) $('.mega_menu').slideUp();",300);
     }
    );
 
    // SUB menu
    $('.mega_menu ul li a').bind('click', function(){
       var next_elem = $(this).next();
       if(next_elem.hasClass('sub_menu')) {
            next_elem.slideToggle();
        }
    });

   // End Menu

   // Start Languages

 var language = readCookie('Language');
 if ($('ins.lang').hasClass('lang')) {
//  var languages = $('ins.lang').map(function(){return this.className;}).get();
//  languages.sort();
var languages = Array('lang lngEnglish','lang lngMandarin','lang lngArabic');


alert(JSON.stringify(languages));
return false;

  if(languages.indexOf('lang lngEnglish')>0) languages.unshift('lang lngEnglish');
  var uniqueLanguages = [];
  $.each(languages, function(i, el){
    if($.inArray(el, uniqueLanguages) === -1) uniqueLanguages.push(el);
  });
  languages = uniqueLanguages;
  var lanSelect = '';
  $.each(languages, function(i, v) { 
   var l = String(new RegExp('lng' + "[\\w]*").exec(v));
   var selected = '';
   if(readCookie('Language')==l) selected = ' selected="selected"'
   else selected = '';
   lanSelect += '<option value="'+ l +'" class="langSelect"'+ selected +'>'+ l.substr(3) +'</option>';
  });

  $('#header .navigation').prepend('<select class="langSelect">'+lanSelect+'</select>');
  $('.langSelect').change(function() {

   $('body').attr('class',$('body').attr('class').replace(/\slng[^\s]*|^lng[^\s]*\s+/g,''))
   $('body').addClass($(this).val());
   $('ins.lang').hide();
   $('ins.lang.' + $(this).val()).css('display','block');
   createCookie('Language', $(this).val(), 90);
  });

  if(!language) language = $('.langSelect option:selected').val();
  $('ins.lang').hide();
  $('ins.lang.' + language).css('display','block');
  $('body').addClass(language);

 }
   // End Languages

 $('.navigation').prepend('<em class="selectLang"><a class="lngEnglish"></a> <a class="lngMandarin"></a> <a class="lngArabic"></a></em>');
 $('.selectLang a.'+language).addClass('active');
 $('.selectLang a').click(function () {
  $('.selectLang a').removeClass('active');
  $('select.langSelect').val($(this).attr('class')).change();
  $(this).addClass('active');
  return false;
 });

   // Start Map

   if ($('.map_holder').hasClass('map_holder')) {


   $('.map_holder a.asia, .map_holder a.pacific').hover(
     function () {
       $(this).parent().addClass('pacific');
     },
     function () {
       $(this).parent().removeClass('pacific');
     }
    );

   $('.map_holder a.europe, .map_holder a.russia').hover(
     function () {
       $(this).parent().addClass('europe');
     },
     function () {
       $(this).parent().removeClass('europe');
     }
    );

   $('.map_holder a.africa').hover(
     function () {
       $(this).parent().addClass('africa');
     },
     function () {
       $(this).parent().removeClass('africa');
     }
    );

   $('.map_holder a.north_america, .map_holder a.south_america').hover(
     function () {
       $(this).parent().addClass('americas');
     },
     function () {
       $(this).parent().removeClass('americas');
     }
    );

   }

   $('.navigation ul a').each(function () {
      if ($(this).attr('href').replace(/\//g,'').split('_')[0] == getPage().split('_')[0]) $(this).addClass('active');
   });

   if($('div.clientF').hasClass('clientF')) {
    $('.featured_client').html('<div class="big_image_with_info_box">'+$('div.clientF:eq(0)').html()+'</div>');
    $('a.clientF:link').hover(function(){
     $('.featured_client').html('<div class="big_image_with_info_box">'+$(this).parent().find('div.clientF').html()+'</div>');
    });
   }


    if ($('.project.text').hasClass('text')) {


            var pHTotal = $('.project.text').outerHeight();
            $('.project.text').css('height','443px');
            var pH = $('.project.text').height();

            if (pHTotal > pH) {
             $('.project.text').after('<p id="readMore"><a>Read More</a></p>');

        $('#readMore a').click(function () {
            if ($(this).hasClass('opened')) {
                $(this).removeClass('opened');
                $(this).text('Read More');
                $('.project.text').animate({
                    height: [pH, 'linear']
                }, 250);
            } else {
                $(this).addClass('opened');
                $(this).text('Hide More');
                $('.project.text').animate({
                    height: [pHTotal, 'linear']
                }, 200, function() {
                  $('.project.text').css('height','auto');
                });
            }
        });
      }


    }


    if($('.firmprofileaspx .left div.text div.cms a').is('a')) {
     $('.firmprofileaspx .left div.text div.cms a').last().addClass('last');
    }

   if($('.learnMore').hasClass('learnMore') && $('.learnMore').text().length > 70) {
    $('.readMe').append('<p id="readMore"><a>Read More</a></p>');
        $('#readMore a').click(function () {
            if ($(this).hasClass('opened')) {
                $(this).removeClass('opened');
                $(this).text('Read More');
                $('.learnMore').slideUp(300);

            } else {
                $(this).addClass('opened');
                $(this).text('Hide More');
                $('.learnMore').slideDown(300);
            }
        });


   }

   Vimeo();

   // End Map
 setTimeout("ga();", 900);
}

// End Ready

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}


function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}


if (false/*@cc_on || @_jscript_version < 99@*/) {
    document.write('<meta http-equiv="X-UA-Compatible" content="IE=7" /><meta http-equiv="cleartype" content="off" />');
}

$(document).ready(function () {

    if (readCookie('SiteEdit') || window.location.search.substr(0, 5) == '?edit') {
       if (readCookie('SiteEdit')) $('body').addClass('cmsEditing');

        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = '/cms/assets/cms.js';
        $('head').append(script);
        var css = document.createElement('link');
        css.setAttribute("rel", "stylesheet");
        css.setAttribute("type", "text/css");
        css.setAttribute("href", '/cms/assets/cms.css');
        $('head').append(css);

    }

});


function getPage() {
    var nPage = window.location.pathname;
    nPage = nPage.substring(nPage.lastIndexOf('/') + 1);
    return nPage;
}


function modalShow(e) {
    var x = ($('body').width() / 2) - (e.width / 2);
    $('#modalIframe').css('left', x + 'px');
    $('#modalIframe').animate({ "opacity": 1 });
    $("#modalLoading").css("display", "none");
    $("#modalIframe").css("visibility", "visible");
    $("#modalIframe").css("opacity", "1");
}

function hoverHomeOut (t) {
 if($('.homepage_slideshow>div.hover').hasClass('hover')) return false;
 if($('.homepage_slideshow a.hover').hasClass('hover')) return false;
 $('.homepage_slideshow div').find('.info_pop').slideUp(); 
 $('.home_previous, .home_next').fadeOut(250);
}

function homePageSlideNext(a) {
 if(moving == 1) return false;
 if(a && $('a.hover').hasClass('hover')) return false;
 moving = 1;
 $('.homepage_slideshow div.w').animate({marginLeft: '-940px'}, 650, function() {
  $('.homepage_slideshow div.w').css('marginLeft','0');
  $('.homepage_slideshow div.w').append($('.homepage_slideshow div.w div:eq(0)'));
  moving = 0;
 });
}

function loadMe () {
 if ($('.loadMe').hasClass('loadMe')) {
  $('.loadMe:eq(0)').attr('src', $('.loadMe:eq(0)').attr('longdesc')).load(function () {
   $(this).removeClass('loadMe');
   loadMe();
  });
 }
 else if(startCar==0) {
  startCar = 1;
  $('.home_next,.home_previous').css('visibility','visible');
  if($('.cmsName_Home_Page_Carousel_Delay').hasClass('cmsName_Home_Page_Carousel_Delay')) {
   clearInterval(homeAuto);
   var speed = (0+Number($('.cmsName_Home_Page_Carousel_Delay').attr('id').replace(/[^\d.]/g, '')))*1000;
   if(speed<1000) speed=6000;
   homeAuto = setInterval("homePageSlideNext(1);",speed);
  }
 }
}

// Start Vimeo
function Vimeo () {

    if ($('.vimeo:eq(0)').attr('href')) {

        var modalCSS = '\
<style type="text/css">\
#modalIframe{\
position: fixed;\
margin: auto;\
top: 90px;\
left: 15%;\
border: 0;\
padding: 22px;\
display: none;\
z-index: 1112;\
opacity: 0;\
visibility: hidden\
}\
#modalIframe iframe {\
margin: 0;\
padding: 0;\
border: 4px solid #eee;\
}\
#modalIframe a {\
position: absolute;\
display: block;\
top: 0;\
right: 0;\
width: 30px;\
height: 30px;\
background: transparent url(assets/images/closebox.png) no-repeat scroll 100% 2px;\
cursor: pointer;\
}\
#modalIframe a:hover {\
background-position: 100% -28px;\
}\
#modalOverlay {\
position: fixed;\
margin: 0;\
top: 0;\
left: 0;\
width: 100%;\
height: 100%;\
padding: 0;\
display: none;\
background: #707070;\
z-index: 1111;\
}\
#modalLoading {\
left: 48%;\
position: fixed;\
z-index: 1114;\
top: 33%;\
width: 210px;\
height: 15px;\
padding: 20px;\
border: 5px solid #111;\
background: #fff;\
display: none;\
}\
</style>';

        $('head').append(modalCSS);
        $('body').append('<div id="modalIframe"></div><div id="modalOverlay"></div><div id="modalLoading"><img src="assets/images/loading.gif" alt="Loading" title="Loading" /></div>');
        $("a.vimeo").click(function () {
            $("#modalIframe").removeAttr('style');
            $("#modalIframe").css("display", "block");
            $("#modalLoading").css("opacity", "0");
            $("#modalLoading").css("display", "block");
            var src = 'http://vimeo.com/moogaloop.swf?clip_id=' + getID($(this).attr('href')) + '&autoplay=1';
            $("#modalIframe").html('<iframe width="730" height="540" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src="' + src + '" onload="modalShow(this)"></iframe><a></a>');
            $("#modalLoading").animate({
                "opacity": 1
            }, 1100);
            $("#modalOverlay").css("opacity", "0");
            $("#modalOverlay").css("display", "block");
            $("#modalOverlay").animate({
                "opacity": .8
            });

            return false;
        });
        $("#modalIframe").click(function () {
            $("#modalIframe").css("visibility", "hidden");
            $("#modalIframe").css("opacity", "0");
            $("#modalIframe").css("display", "none");
            $("#modalIframe").removeAttr('style');
            $("#modalIframe").html('');
            jQuery("#modalOverlay").css("display", "none");
            return false;
        });

    }

}

Array.prototype.getMax = function () {
    return Math.max.apply(Math, this);
}

function getID(str) {
    return str.split("?")[0].match(/\d+/g).getMax();
}

function modalShow(e) {
    var x = ($('body').width() / 2) - (e.width / 2);
    $('#modalIframe').css('left', x + 'px');
    $('#modalIframe').animate({
        "opacity": 1
    });
    $("#modalLoading").css("display", "none");
    $("#modalIframe").css("visibility", "visible");
    $("#modalIframe").css("opacity", "1");
}

// End Vimeo

function replaceIndex(string, at, repl) {
   return string.replace(/\S/g, function(match, i) {
        if( i === at ) return repl;

        return match;
    });
}

var gaRun = 0;
function ga() {
 if (gaRun == 1) return;
 gaRun = 1;
 if(String(window.location.hostname).replace('www.','')!='perkinseastman.com') return false;
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-40177322-1', 'perkinseastman.com');
  ga('send', 'pageview');
}

$(document).ready(function () {
  if(window.location.search.substr(0, 8) == '?refresh') {
    $('img').each(function () {
      if($(this).attr('src')) {
        var img = $(this).attr('src').split('?')[0];
        $(this).attr('src', img + '?refresh');
      }
      if($(this).attr('longdesc')) {
        var img = $(this).attr('longdesc').split('?')[0];
        $(this).attr('longdesc', img + '?refresh');
      }
    });
    $('*').each(function () {
      if($(this).css('background-image') && $(this).css('background-image') != 'none') {
        var img = $(this).css('background-image').split('?')[0].replace('url(', '').replace(/[\"\'\)]/g, '');
        $(this).css('backgroundImage', 'url("' + img + '?refresh' + '")');
      }
    });
  }
});

function htmlEncode(value){
  //create a in-memory div, set it's inner text(which jQuery automatically encodes)
  //then grab the encoded contents back out.  The div never exists on the page.
  return $('<div/>').text(value).html();
}

$.fn.randomize=function(a){(a?this.find(a):this).parent().each(function(){$(this).children(a).sort(function(){return Math.random()-0.5}).detach().appendTo(this)});return this};

var keyStr="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
function encode64(e){e=escape(e);var t="";var n,r,i="";var s,o,u,a="";var f=0;do{n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+keyStr.charAt(s)+keyStr.charAt(o)+keyStr.charAt(u)+keyStr.charAt(a);n=r=i="";s=o=u=a=""}while(f<e.length);return t}function decode64(e){var t="";var n,r,i="";var s,o,u,a="";var f=0;var l=/[^A-Za-z0-9\+\/\=]/g;if(l.exec(e)){alert("There were invalid base64 characters in the input text.\n"+"Valid base64 characters are A-Z, a-z, 0-9, '+', '/', and '='\n"+"Expect errors in decoding.")}e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");do{s=keyStr.indexOf(e.charAt(f++));o=keyStr.indexOf(e.charAt(f++));u=keyStr.indexOf(e.charAt(f++));a=keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}n=r=i="";s=o=u=a=""}while(f<e.length);return unescape(t)}
