﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="country_list.aspx.vb" Inherits="country_list" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first">Work</a><span> | </span></li>
                        <li><a href="region_intro" title="">By REGION</a><span> | </span></li>
                        <li><a href="<% =Master.UrlPage %>" class="active">Country List</a></li>
                    </ul>
                </div>

                <div class="two_columns">
                    <div class="left_smaller">
                        <h3 class="sub_title">Country List</h3>
                        
                            <div class="cms cmsType_Rich cmsName_Country_List_Left">
                            <p><%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Country_List_Left")%></p>
                            </div>      
                        
                        <div class="gray_box_wrap">
                            <a href="region_intro" title="" class="view_more">View Region Map</a>
                            <a href="contact" title="" class="view_more">Perkins Eastman Office Locations</a>                             
                        </div>

                    </div>
                    
                    <div class="right_bigger">
                        

   <div class="country_list">
 <%--<asp:Literal runat="server" ID="country_list1"/>       --%>
 

<dl class="dl1 cms cmsType_TextMulti cmsName_AFRICA_AND_THE_MIDDLE_EAST_country_list">
<dt>AFRICA AND THE MIDDLE EAST</dt>
                    
                 <%= CMSFunctions.FormatListDD(Server.MapPath("~") & "cms\data\Text\", "AFRICA_AND_THE_MIDDLE_EAST_country_list", "dd")%>

</dl>


<dl class="dl2 cms cmsType_TextMulti cmsName_ASIA_country_list">
<dt>ASIA</dt>
                    
                       <%= CMSFunctions.FormatListDD(Server.MapPath("~") & "cms\data\Text\", "ASIA_country_list", "dd")%>

</dl>
<dl class="dl3 cms cmsType_TextMulti cmsName_EUROPE_country_list">
<dt>EUROPE</dt>
                    
                       <%= CMSFunctions.FormatListDD(Server.MapPath("~") & "cms\data\Text\", "EUROPE_country_list", "dd")%>

</dl>

<dl class="dl4 cms cmsType_TextMulti cmsName_THE_AMERICAS_country_list">
<dt>THE AMERICAS</dt>
                    
                     <%= CMSFunctions.FormatListDD(Server.MapPath("~") & "cms\data\Text\", "THE_AMERICAS_country_list", "dd")%>

</dl>

 
                           
                        </div>
                        
                    </div>
                </div>
</asp:Content>

