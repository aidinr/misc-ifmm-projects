﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="speaking_engagement.aspx.vb" Inherits="speaking_engagement" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_Insights"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Insights")%></a><span> | </span></li>
                        <li><a class="cmsMultiLanguage cms cmsType_TextSingle cmsName_Speaking_Engagements active" href="<%=Master.UrlPage%>"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Speaking_Engagements")%></a></li>
                    </ul>
                </div>
                <div class="headline">
                    <h2 class="no_text_transform"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Speaking_Engagements")%></h2>
                </div>
                
                <ul class="white_papers_list speaking_engagement_list">
                    <asp:Literal runat="server" ID="speaking_engagement_list1"/>      
                </ul>

                <div class="csFullContent cms cmsType_Rich cmsName_speaking_engagement_Footer">
<%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "speaking_engagement_Footer")%>
</div>

</asp:Content>
