﻿Imports System.Data
Imports System.IO

Partial Class casestudy
    Inherits System.Web.UI.Page

    Public url_LayoutId As String
    Public CategoryId As Integer
    Public url_SlideName As String

    Public practiceName As String
    Public practiceName1 As String
    Public FriendlyPracticeName As String
    Public FriendlyCMSName As String
    Public MainPracticeId As String
    Public MainPracticeName As String
    Public practiceAreaUDFID As String

    Public CaseStudiesDT As New DataTable
    Public SliedsDT As New DataTable

    Dim NavLinksOut As String
    Dim NavSlidesOut As String
    Dim breadcrumbsOut As String

    Public CurrentCaseStrudyNo As Integer
    Public CurrentCaseStudyName As String
    Public CurentSlideName As String
    Public CurrentSlideNo As Integer

    Dim CatFriendlyLink As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        CategoryId = Request.QueryString("cat")
        url_LayoutId = Request.QueryString("layout")
        url_SlideName = Request.QueryString("name")

        Select Case CategoryId
            Case "3413026"
                practiceName = "Broadcast and Media"
                practiceName1 = "Broadcast and Media"
            Case "3414326"
                practiceName = "Courts and Public"
                practiceName1 = "Courts and Public"
            Case "2400012"
                practiceName = "Corporate Interiors"
                practiceName1 = "Corporate Interiors"
                practiceAreaUDFID = "3408922"
            Case "3409338"
                practiceName = "Education"
                practiceName1 = "Education"
                practiceAreaUDFID = "3408922"
            Case "3409328"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "Campus Planning"
                practiceName1 = "Campus Planning"
                practiceAreaUDFID = "3408924"
            Case "2400014"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "Higher Education"
                practiceName1 = "Higher Education"
                practiceAreaUDFID = "3408924"
            Case "2400017"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "K-12 Education"
                practiceName1 = "K-12 Education"
                practiceAreaUDFID = "3408924"
            Case "3409341"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "Town and Gown"
                practiceName1 = "Town and Gown"
                practiceAreaUDFID = "3408924"
            Case "2400024"
                practiceName = "Healthcare"
                practiceName1 = "Healthcare"
                practiceAreaUDFID = "3408922"
            Case "2400015"
                practiceName = "Hospitality"
                practiceName1 = "Hospitality"
                practiceAreaUDFID = "3408922"
            Case "2400016"
                practiceName = "Housing"
                practiceName1 = "Residential"
                practiceAreaUDFID = "3408922"
            Case "2400018"
                practiceName = "Office and Retail"
                practiceName1 = "Office and Retail"
                practiceAreaUDFID = "3408922"
            Case "2400019"
                practiceName = "Cultural"
                practiceName1 = "Cultural"
                practiceAreaUDFID = "3408922"
            Case "2400020"
                practiceName = "Science and Technology"
                practiceName1 = "Science and Technology"
                practiceAreaUDFID = "3408922"
            Case "2400021"
                practiceName = "Senior Living"
                practiceName1 = "Senior Living"
                practiceAreaUDFID = "3408922"
            Case "3409343"
                practiceName = "Urbanism"
                practiceName1 = "Urbanism"
                practiceAreaUDFID = "3408922"
            Case "3409345"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Downtown Redevelopment"
                practiceName1 = "Downtown Redevelopment"
                practiceAreaUDFID = "3408924"
            Case "3409347"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "New Town Planning"
                practiceName1 = "New Town Planning"
                practiceAreaUDFID = "3408924"
            Case "3409349"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Transit and Development"
                practiceName1 = "Transit and Development"
                practiceAreaUDFID = "3408924"
            Case "2400022"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Urban Design"
                practiceName1 = "Urban Design"
                practiceAreaUDFID = "3408924"
            Case "3409351"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Waterfront"
                practiceName1 = "Waterfront"
                practiceAreaUDFID = "3408924"
            Case "24000121"
                MainPracticeId = "2400012"
                MainPracticeName = "Corporate Interiors"
                practiceName = "Corporate Interiors Consulting"
                practiceName1 = "Corporate Interiors Strategies"
                practiceAreaUDFID = "34089221"
            Case "24000122"
                MainPracticeId = "2400012"
                MainPracticeName = "Corporate Interiors"
                practiceName = "Corporate Interiors Brand Development"
                practiceName1 = "Corporate Interiors Brand Development"
                practiceAreaUDFID = "34089222"
            Case "24000123"
                MainPracticeId = "2400012"
                MainPracticeName = "Corporate Interiors"
                practiceName = "Corporate Interiors Commercial Renovations"
                practiceName1 = "Corporate Interiors Commercial Renovations"
                practiceAreaUDFID = "34089223"
            Case "2410432"
                practiceName = "Strategies"
                practiceName1 = "Strategies"
            Case "9999999"
                practiceName = "Sustainability"
                practiceName1 = "Sustainability"
            Case "24000212"
                MainPracticeName = "Senior Living"
                practiceName = "Senior Living Ideas"
                practiceName1 = "Senior Living Ideas"
            Case "24000213"
                MainPracticeName = "Senior Living"
                practiceName = "Senior Living Strategies"
                practiceName1 = "Senior Living Strategies"
            Case Else
        End Select

        If CategoryId = 24000121 Or CategoryId = 24000122 Or CategoryId = 24000123 Or CategoryId = 24000212 Or CategoryId = 24000213 Or CategoryId = 2410432 Or CategoryId = 9999999 Then
            CatFriendlyLink = CMSFunctions.FormatFriendlyUrl(practiceName1.ToLower, "_")
        Else
            CatFriendlyLink = "category_" & CategoryId & "_" & CMSFunctions.FormatFriendlyUrl(practiceName1.ToLower, "_")
        End If

        FriendlyPracticeName = CMSFunctions.FormatFriendlyUrl(practiceName, "_")
        FriendlyCMSName = FriendlyPracticeName & "_" & url_SlideName

        'Case Studies
        CaseStudiesDT.TableName = "CaseStudiesDT"
        Dim col_CaseStudyNo As DataColumn = New DataColumn("CaseStudyNo")
        col_CaseStudyNo.DataType = System.Type.GetType("System.Int32")
        CaseStudiesDT.Columns.Add(col_CaseStudyNo)
        Dim col_CaseStudyName As DataColumn = New DataColumn("CaseStudyName")
        col_CaseStudyName.DataType = System.Type.GetType("System.String")
        CaseStudiesDT.Columns.Add(col_CaseStudyName)
        Dim col_CaseStudyLink As DataColumn = New DataColumn("CaseStudyLink")
        col_CaseStudyLink.DataType = System.Type.GetType("System.String")
        CaseStudiesDT.Columns.Add(col_CaseStudyLink)

        Dim CaseStudyName1 As String = ""
        Dim FirstSlideName As String = ""
        Dim FirstSlideLayout As String = ""
        Dim FirstSlideLink As String = ""


        For i As Integer = 1 To 20 - 1
            FirstSlideLink = ""
            If Directory.GetFiles(Server.MapPath("~") & "cms\data\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
                CaseStudyName1 = CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_Name")
                For x As Integer = 0 To 100
                    FirstSlideName = ""
                    FirstSlideLayout = ""
                    If CMSFunctions.CheckTextFile(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List", x) Then
                        FirstSlideName = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xa" & x & ".txt")
                        FirstSlideLayout = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xb" & x & ".txt")
                        Exit For
                    End If
                Next
                If FirstSlideName <> "" Then
                    FirstSlideLink = "casestudy_" & FirstSlideLayout & "_" & CategoryId & "_" & CMSFunctions.FormatFriendlyUrl(FirstSlideName, "_")
                    Dim CaseStudyLinkRow As DataRow = CaseStudiesDT.NewRow
                    CaseStudyLinkRow.Item("CaseStudyNo") = i
                    CaseStudyLinkRow.Item("CaseStudyName") = CaseStudyName1
                    CaseStudyLinkRow.Item("CaseStudyLink") = FirstSlideLink
                    CaseStudiesDT.Rows.Add(CaseStudyLinkRow)
                End If
            End If
        Next


        'Case Studies Slides
        SliedsDT.TableName = "SliedsDT"
        Dim col_SlideNo As DataColumn = New DataColumn("SlideNo")
        col_SlideNo.DataType = System.Type.GetType("System.Int32")
        SliedsDT.Columns.Add(col_SlideNo)
        Dim col_SlideName As DataColumn = New DataColumn("SlideName")
        col_SlideName.DataType = System.Type.GetType("System.String")
        SliedsDT.Columns.Add(col_SlideName)
        Dim col_SlideLayout As DataColumn = New DataColumn("SlideLayout")
        col_SlideLayout.DataType = System.Type.GetType("System.String")
        SliedsDT.Columns.Add(col_SlideLayout)
        Dim col_SlideLink As DataColumn = New DataColumn("SlideLink")
        col_SlideLink.DataType = System.Type.GetType("System.String")
        SliedsDT.Columns.Add(col_SlideLink)
        Dim col_SlideCaseStudyName As DataColumn = New DataColumn("SlideCaseStudyName")
        col_SlideCaseStudyName.DataType = System.Type.GetType("System.String")
        SliedsDT.Columns.Add(col_SlideCaseStudyName)

        Dim CaseStudyName2 As String = ""
        Dim SlideName As String = ""
        Dim SlideLayout As String = ""
        Dim SlideLink As String = ""


        For i As Integer = 1 To 50 - 1
            SlideLink = ""
            If Directory.GetFiles(Server.MapPath("~") & "cms\data\Text\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
                CaseStudyName2 = CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_Name")
                For x As Integer = 0 To 100
                    SlideName = ""
                    SlideLayout = ""
                    If CMSFunctions.CheckTextFile(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List", x) Then
                        SlideName = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xa" & x & ".txt")
                        SlideLayout = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xb" & x & ".txt")
                        SlideLink = "casestudy_" & SlideLayout & "_" & CategoryId & "_" & CMSFunctions.FormatFriendlyUrl(SlideName, "_")

                        Dim NewSlideRow As DataRow = SliedsDT.NewRow
                        NewSlideRow.Item("SlideNo") = x + 1
                        NewSlideRow.Item("SlideName") = SlideName
                        NewSlideRow.Item("SlideLayout") = SlideLayout
                        NewSlideRow.Item("SlideLink") = SlideLink
                        NewSlideRow.Item("SlideCaseStudyName") = CaseStudyName2
                        SliedsDT.Rows.Add(NewSlideRow)

                        If SlideLink = "casestudy_" & url_LayoutId & "_" & CategoryId & "_" & url_SlideName Then
                            CurrentCaseStrudyNo = i
                            CurrentCaseStudyName = CaseStudyName2
                            CurentSlideName = SlideName
                        End If
                    End If
                Next
            End If
        Next


        Dim v As DataView = SliedsDT.DefaultView
        v.RowFilter = "SlideCaseStudyName = '" & CurrentCaseStudyName & "'"
        SliedsDT = v.ToTable()

        'Nav Case Studies Slides
        If SliedsDT.Rows.Count > 0 Then
            'NavSlidesOut &= "<li>AAAA</li>"
            Dim i As Integer = 0
            For Each row As DataRow In SliedsDT.Rows
                If (row("SlideLink").ToString.Trim() = "casestudy_" & url_LayoutId & "_" & CategoryId & "_" & url_SlideName) Then
                    CurrentSlideNo = row("SlideNo")
                    Exit For
                End If
                i = i + 1
            Next

            If (i = 0) Then
            Else
                Dim PrFriendlyLink As String = SliedsDT.Rows(i - 1)("SlideLink")
                NavSlidesOut &= "<li><a href=""" & PrFriendlyLink & """ title=""Previous"">Previous</a></li>"
            End If
            NavSlidesOut &= "<li>Page " & CurrentSlideNo & " of " & SliedsDT.Rows.Count & "</li>"
            If (i < SliedsDT.Rows.Count - 1) Then
                Dim PrFriendlyLink As String = SliedsDT.Rows(i + 1)("SlideLink")
                NavSlidesOut &= "<li><a href=""" & PrFriendlyLink & """ title=""Next"">Next</a></li>"
            Else
            End If
        End If





        'Breadcrumbs

        If CategoryId = 2410432 Or CategoryId = 9999999 Then
            breadcrumbsOut &= "<li><a title="""" class=""first"">Approach</a><span> | </span></li>" & vbNewLine
        Else
            breadcrumbsOut &= "<li><a title="""" class=""first"">Work</a><span> | </span></li>" & vbNewLine
            breadcrumbsOut &= "<li><a title="""">By PRACTICE AREA</a><span> | </span></li>" & vbNewLine
        End If

        If MainPracticeName <> "" Then
            Dim MainCatFriendlyLink As String = ""
            Select Case MainPracticeName
                Case "Corporate Interiors"
                    MainCatFriendlyLink = "corporate_interiors"
                Case "Senior Living"
                    MainCatFriendlyLink = "senior_living"
                Case Else
                    MainCatFriendlyLink = "practice_" & MainPracticeId & "_" & CMSFunctions.FormatFriendlyUrl(MainPracticeName.ToLower, "_")
            End Select
            breadcrumbsOut &= "<li><a href=""" & MainCatFriendlyLink & """ title="""">" & MainPracticeName & "</a><span> | </span></li>" & vbNewLine
        End If

        If practiceName = "Corporate Interiors" Then
            breadcrumbsOut &= "<li><a href=""corporate_interiors"" title="""">" & practiceName1 & "</a><span> | </span></li>" & vbNewLine
        ElseIf practiceName = "Strategies" Then
            breadcrumbsOut &= "<li><a href=""strategies"" title="""">" & practiceName1 & "</a><span> | </span></li>" & vbNewLine
        ElseIf practiceName = "Senior Living" Then
            breadcrumbsOut &= "<li><a href=""senior_living"" title="""">" & practiceName1 & "</a><span> | </span></li>" & vbNewLine
        Else
            breadcrumbsOut &= "<li><a href=""" & CatFriendlyLink & """ title="""">" & practiceName1 & "</a><span> | </span></li>" & vbNewLine
        End If

        breadcrumbsOut &= "<li><a href=""" & CatFriendlyLink & """ title="""">CASE STUDIES</a><span> | </span></li>" & vbNewLine
        breadcrumbsOut &= "<li><a href=""" & Master.UrlPage & """ title="""" class=""active"">" & CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", FriendlyCMSName & "_Heading") & "</a></li>" & vbNewLine



        'Nav Case Studies
        If practiceName = "Corporate Interiors" Or practiceName = "Corporate Interiors Consulting" Or practiceName = "Corporate Interiors Brand Development" Or practiceName = "Corporate Interiors Commercial Renovations" Then
            NavLinksOut &= "<li><a href=""corporate_interiors"" title="""">Back to Corporate Interiors</a></li>"
        ElseIf practiceName = "Senior Living" Or practiceName = "Senior Living Strategies" Or practiceName = "Senior Living Ideas" Then
            NavLinksOut &= "<li><a href=""senior_living"" title="""">Back to Senior Living</a></li>"
        ElseIf practiceName = "Strategies" Then
            NavLinksOut &= "<li><a href=""strategies"" title="""">Back to Strategies</a></li>"
        Else
            NavLinksOut &= "<li><a href=""" & CatFriendlyLink & """ title="""">Back to " & practiceName & "</a></li>"
        End If

        If CaseStudiesDT.Rows.Count > 0 Then
            For i As Integer = 0 To CaseStudiesDT.Rows.Count - 1
                If CaseStudiesDT.Rows(i).Item("CaseStudyName").ToString = CurrentCaseStudyName Then
                    If i + 1 < CaseStudiesDT.Rows.Count Then
                        Dim PrFriendlyLink As String = CaseStudiesDT.Rows(i + 1)("CaseStudyLink")
                        NavLinksOut &= "<li><a href=""" & PrFriendlyLink & """ title="""">Next Case Study</a></li>"
                    End If
                    If i = 0 Then
                    Else
                        Dim PrFriendlyLink As String = CaseStudiesDT.Rows(i - 1)("CaseStudyLink")
                        NavLinksOut &= "<li><a href=""" & PrFriendlyLink & """ title="""">Previous Case Study</a></li>"
                    End If
                End If
            Next
        End If

        breadcrumbs_list.Text = breadcrumbsOut
        project_nav_list.Text = NavLinksOut
        slides_nav_list.Text = NavSlidesOut

    End Sub



    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub



End Class
