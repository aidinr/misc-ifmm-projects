﻿Imports System.Data

Partial Class corporate_interiors
    Inherits System.Web.UI.Page

    Public practiceName As String = "Corporate Interiors"
    Public CategoryId As Integer = 2400012


    Public TotalRecords As Integer
    Public RecordPR As Integer
    Public NewRow As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim FirstProjectString As String = ""
        Dim sql4 As String = ""
        sql4 &= "select top 1 "
        sql4 &= "P.ProjectID, "
        sql4 &= "P.Name as ProjectName, "
        sql4 &= "P1.Item_Value AS Region, "
        sql4 &= "P2.Item_Value AS PracticeArea, "
        sql4 &= "P8.Item_Value as SubPracticeArea, "
        sql4 &= "case when isnull(P.State_id,'') <> '' or P.State_id is null then P.City + case when isnull(S.Name ,'') <> '' then ', ' + S.Name else '' end else p.City + case when isnull(P5.Item_Value,'') <> '' then ', ' + P5.Item_Value else '' end + case when isnull(P4.Item_Value,'') <> '' then ', ' + P4.Item_Value else '' end end as ProjectAddress, "
        sql4 &= "P6.Item_Value As WebDescription, "
        sql4 &= "P7.Item_Value as ShortName "
        sql4 &= "FROM IPM_PROJECT P "
        sql4 &= "left join IPM_PROJECT_FIELD_VALUE P1 on P.ProjectID = P1.ProjectID and P1.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_REGIONS') "
        sql4 &= "left join IPM_PROJECT_FIELD_VALUE P2 on P.ProjectID = P2.ProjectID and P2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PRACTICEAREA') "
        sql4 &= "left join IPM_PROJECT_FIELD_VALUE P3 on P.ProjectID = P3.ProjectID and P3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_CITY') "
        sql4 &= "left join IPM_PROJECT_FIELD_VALUE P4 on P.ProjectID = P4.ProjectID and P4.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_COUNTRY') "
        sql4 &= "left join IPM_PROJECT_FIELD_VALUE P5 on P.ProjectID = P5.ProjectID and P5.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_STATE') "
        sql4 &= "left join IPM_PROJECT_FIELD_VALUE P6 on P.ProjectID = P6.ProjectID and P6.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_V_DESCRIPTION_WEB') "
        sql4 &= "left join IPM_PROJECT_FIELD_VALUE P7 on P.ProjectID = P7.ProjectID and P7.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_SHORT_NAME') "
        sql4 &= "left join IPM_PROJECT_FIELD_VALUE P8 on P.ProjectID = P8.ProjectID and P8.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUBPRACTICEAREA') "
        sql4 &= "left join IPM_PROJECT_FIELD_VALUE P9 on P.ProjectID = P9.ProjectID and P9.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAN_PROJECT_ORDERING') "
        sql4 &= "left join IPM_STATE S on S.State_id = P.State_id "
        If CategoryId = 3409328 Or CategoryId = 2400014 Or CategoryId = 2400017 Or CategoryId = 3409341 Or CategoryId = 3409345 Or CategoryId = 3409347 Or CategoryId = 3409349 Or CategoryId = 2400022 Or CategoryId = 3409351 Then
            sql4 &= "where charindex('" & practiceName & "',LTRIM(RTRIM(P8.Item_Value)))!= 0 "
        Else
            sql4 &= "where charindex('" & practiceName & "',LTRIM(RTRIM(P2.Item_Value)))!= 0 "
        End If
        sql4 &= "and P.Available = 'y' "
        sql4 &= "and P.Show = 1 "
        sql4 &= "order by CAST(isnull(P9.Item_Value, 9999999999) as integer) desc"
        Dim ProjectsDT As DataTable
        ProjectsDT = New DataTable("ProjectsDT")
        ProjectsDT = mmfunctions.GetDataTable(sql4)


        TotalRecords = ProjectsDT.Rows.Count

        Dim out2 As String = ""
        For R As Integer = 0 To TotalRecords - 1
            Dim PrFriendlyLink As String = "project_" & ProjectsDT.Rows(R)("ProjectID").ToString.Trim & "_" & formaturl.friendlyurl(ProjectsDT.Rows(R)("ProjectName").ToString.Trim.ToLower)
            out2 &= " <div class=""big_image_with_info_box"" id=""box_" & R + 1 & """>"
            out2 &= "<a class=""proj"" href=""" & PrFriendlyLink & """ > "
            out2 &= "<img src=""dynamic/image/week/project/liquid/700x/92/ffffff/NorthWest/" & ProjectsDT.Rows(R)("ProjectID").ToString.Trim & ".jpg"" alt=""""/>"
            out2 &= "<span>"
            out2 &= "<em>" & formatfunctions.AutoFormatText(ProjectsDT.Rows(R)("ProjectName").ToString.Trim) & "</em>"
            out2 &= "<span>" & formatfunctions.AutoFormatText(ProjectsDT.Rows(R)("ProjectAddress").ToString.Trim) & "</span>"
            out2 &= "</span>"
            out2 &= "</a>"
            out2 &= "</div>"
        Next

        big_projects_list1.Text = out2





        'Retaled Topics
        Dim sql4a As String = ""
        Dim sql4b As String = ""
        Dim sql4c As String = ""
        Dim sql4d As String = ""
        Dim sql4e As String = ""
        Dim sql4f As String = ""
        Dim sql4g As String = ""
        'Testi
        sql4a &= "select "
        sql4a &= "U.UserID, "
        sql4a &= "U.FirstName + ' ' + U.LastName FullName, "
        sql4a &= "U1.Item_Value As Testimonial, "
        sql4a &= "U2.Item_Value As Link, "
        sql4a &= "U3.Item_Value as RelId, "
        sql4a &= "U4.Item_Value as SubTestimonial "
        sql4a &= "from IPM_USER U "
        sql4a &= "left join IPM_USER_FIELD_VALUE U1 on U.UserID = U1.USER_ID and U1.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTIMONIAL') "
        sql4a &= "left join IPM_USER_FIELD_VALUE U2 on U.UserID = U2.USER_ID and U2.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USERLINK') "
        sql4a &= "left join IPM_USER_FIELD_VALUE U3 on U.UserID = U3.USER_ID and U3.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTIIDS') "
        sql4a &= "left join IPM_USER_FIELD_VALUE U4 on U.UserID = U4.USER_ID and U4.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTISUB') "
        sql4a &= "where Active = 'Y' "
        sql4a &= "and u.Contact = 1 "
        sql4a &= "and charindex('" & CategoryId & "',U3.Item_Value)!= 0 "
        Dim TestimonialDT As DataTable
        TestimonialDT = New DataTable("TestimonialDT")
        TestimonialDT = mmfunctions.GetDataTable(sql4a)
        'Award
        sql4c &= "select top 0 "
        sql4c &= "A.Awards_Id, "
        sql4c &= "A.Headline, "
        sql4c &= "A.PublicationTitle,"
        sql4c &= "YEAR(A.Post_Date) as AwardYear, "
        sql4c &= "P2.Item_Value As Practice, "
        sql4c &= "P8.Item_Value AS SubPractice "
        sql4c &= "FROM IPM_AWARDS A "
        sql4c &= "join IPM_AWARDS_RELATED_PROJECTS ARP on ARP.Awards_Id = A.Awards_Id "
        sql4c &= "join IPM_PROJECT P on P.ProjectID = ARP.ProjectID "
        sql4c &= "left join IPM_PROJECT_FIELD_VALUE P2 on P.ProjectID = P2.ProjectID and P2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PRACTICEAREA') "
        sql4c &= "left join IPM_PROJECT_FIELD_VALUE P8 on P.ProjectID = P8.ProjectID and P8.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUBPRACTICEAREA') "
        If CategoryId = 3409328 Or CategoryId = 2400014 Or CategoryId = 2400017 Or CategoryId = 3409341 Or CategoryId = 3409345 Or CategoryId = 3409347 Or CategoryId = 3409349 Or CategoryId = 2400022 Or CategoryId = 3409351 Then
            sql4c &= "where charindex('" & practiceName & "',LTRIM(RTRIM(P8.Item_Value)))!= 0 "
        Else
            sql4c &= "where charindex('" & practiceName & "',LTRIM(RTRIM(P2.Item_Value)))!= 0 "
        End If
        sql4c &= "AND A.Show = 1 "
        sql4c &= "and A.post_date < getdate() "
        sql4c &= "and A.pull_date > getdate() "
        sql4c &= "ORDER BY A.Post_Date desc "
        Dim AwardDT As DataTable
        AwardDT = New DataTable("AwardDT")
        AwardDT = mmfunctions.GetDataTable(sql4c)


        Dim PublicationsDT As New DataTable("PublicationsDT")
        PublicationsDT = CMSFunctions.GetNews(1, CategoryId, practiceName, 1)
        Dim PressDT As New DataTable("PressDT")
        PressDT = CMSFunctions.GetNews(8, CategoryId, practiceName, 1)
        Dim WhitePapersDT As New DataTable("WhitePapersDT")
        WhitePapersDT = CMSFunctions.GetNews(10, CategoryId, practiceName, 1)

        Dim SpeakingEngagementsql As String = "Select top 1 e.Headline, V2.Item_Value as EventTitle, V3.Item_Value as VideoLink, e.Content, DATENAME(mm,e.Event_Date) + ' ' + DATENAME(day,e.Event_Date) + ', ' + DATENAME(year, e.Event_Date) Event_Date, l.Item_Value Location, p.Item_Value Presenter, v.Item_Value Venue, V1.Item_Value as LinkU "
        SpeakingEngagementsql &= "From IPM_EVENTS e "
        SpeakingEngagementsql &= "Left join IPM_EVENTS_FIELD_VALUE l on (l.EVENT_ID = e.Event_Id and l.Item_ID = 3408650) "
        SpeakingEngagementsql &= "Left join IPM_EVENTS_FIELD_VALUE p on (p.EVENT_ID = e.Event_Id and p.Item_ID = 3408649) "
        SpeakingEngagementsql &= "Left join IPM_EVENTS_FIELD_VALUE v on (v.EVENT_ID = e.Event_Id and v.Item_ID = 3408651) "
        SpeakingEngagementsql &= "left join IPM_EVENTS_FIELD_VALUE V1 on e.Event_Id = V1.EVENT_ID and V1.Item_ID = (Select Item_ID From IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_LINK') "
        SpeakingEngagementsql &= "left join IPM_EVENTS_FIELD_VALUE V2 on e.Event_Id = V2.EVENT_ID and V2.Item_ID = (Select Item_ID From IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_EVENTTITLE') "
        SpeakingEngagementsql &= "left join IPM_EVENTS_FIELD_VALUE V3 on e.Event_Id = V3.EVENT_ID and V3.Item_ID = (Select Item_ID From IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_EVENTVIDEO') "
        SpeakingEngagementsql &= "left join IPM_EVENTS_FIELD_VALUE N1 on e.Event_Id = N1.EVENT_ID and N1.Item_ID = (Select Item_ID From IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_NEWSPROJCAT') "
        SpeakingEngagementsql &= "Where(e.Type = 5 And e.Post_Date < GETDATE() And e.Pull_Date > GETDATE() And e.Show = 1) and charindex('" & CategoryId & "',LTRIM(RTRIM(N1.Item_Value)))!= 0 "
        SpeakingEngagementsql &= "Order By e.Event_Date ASC"
        Dim SpeakingEngagementDT As New DataTable("SpeakingEngagementDT")
        SpeakingEngagementDT = mmfunctions.GetDataTable(SpeakingEngagementsql)





        Dim out4 As String = ""
        'Testimonials
        If TestimonialDT.Rows.Count > 0 Then
            out4 &= "<dl class=""contacts_list"">"
            out4 &= "<dt>Testimonial</dt>"
            Dim TestiFriendlyLink As String = ""
            TestiFriendlyLink = "testimonials" & "#testimonial_" & TestimonialDT.Rows(0)("UserID").ToString.Trim & "_" & formaturl.friendlyurl(TestimonialDT.Rows(0)("FullName").ToString.Trim.ToLower)
            out4 &= "<dd>"
            out4 &= "<p><strong>"
            out4 &= "<a href=""" & TestiFriendlyLink & """ title="""" >" & formatfunctionssimple.AutoFormatText(formaturl.getwords(TestimonialDT.Rows(0)("Testimonial").ToString.Trim, 35)) & "&#8230;" & "</a>"
            out4 &= "</strong></p>"
            out4 &= "</dd>"
            out4 &= "</dl> "
        End If
        'AWARDS
        If AwardDT.Rows.Count > 0 Then
            out4 &= "<dl class=""contacts_list"">"
            out4 &= "<dt>AWARDS</dt><div class=""projAwards"">"
            For aw As Integer = 0 To AwardDT.Rows.Count - 1
                Dim AwardFriendlyLink As String = ""
                AwardFriendlyLink = "honor_awards_" & AwardDT.Rows(aw)("Awards_Id").ToString.Trim & "_" & formaturl.friendlyurl(AwardDT.Rows(aw)("Headline").ToString.Trim)
                out4 &= "<dd>"
                out4 &= "<em class=""awardCat"">" & formatfunctionssimple.AutoFormatText(AwardDT.Rows(aw)("Headline").ToString.Trim) & ": " & "</em>"
                out4 &= "<span class=""awardName"">" & formatfunctions.AutoFormatText(AwardDT.Rows(aw)("PublicationTitle").ToString.Trim) & " (" & AwardDT.Rows(aw)("AwardYear").ToString.Trim & ")" & "</span>"
                out4 &= "</dd>"
            Next
            out4 &= "</div></dl> "
        End If


        If PressDT.Rows.Count > 0 Or PublicationsDT.Rows.Count > 0 Or WhitePapersDT.Rows.Count > 0 Or SpeakingEngagementDT.Rows.Count > 0 Then
            out4 &= "<dl class=""related_topics"">"
            out4 &= "<dt>Related Topics</dt>"

            If PressDT.Rows.Count > 0 Then
                For i As Integer = 0 To PressDT.Rows.Count - 1
                    out4 &= "<dd>"
                    out4 &= "<p>Press Releases</p>"
                    'out4 &= "<span>" & formatfunctions.AutoFormatText(PressDT.Rows(i)("PublicationTitle").ToString.Trim) & "</span>"
                    out4 &= "<p><strong>"
                    If PressDT.Rows(i)("PDF").ToString.Trim = "1" Then
                        out4 &= "<a target=""_blank"" href=""/dynamic/document/week/news/download/" & PressDT.Rows(i)("News_ID").ToString.Trim & "/" & PressDT.Rows(i)("News_ID").ToString.Trim & ".pdf"">"
                    ElseIf PressDT.Rows(i)("PDF").ToString.Trim = "0" And PressDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                        out4 &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(PressDT.Rows(i)("WebUrl").ToString.Trim) & """ >"
                        'Else
                        '    out4 &= "<a>"
                    End If
                    out4 &= formatfunctions.AutoFormatText(PressDT.Rows(i)("Headline").ToString.Trim)
                    If PressDT.Rows(i)("PDF").ToString.Trim = "1" Or PressDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                        out4 &= "</a>"
                    End If

                    If PressDT.Rows(i)("VideoLink").ToString.Trim <> "" Then
                        out4 &= "<a href=""" & formatfunctionssimple.AutoFormatText(PressDT.Rows(i)("VideoLink").ToString.Trim) & """ class=""video vimeo"">View video</a>"
                    End If

                    out4 &= "</strong></p>"
                    out4 &= "<p><strong></strong></p>"
                    out4 &= "</dd>"
                Next
            End If

            For i As Integer = 0 To PublicationsDT.Rows.Count - 1
                out4 &= "<dd>"
                out4 &= "<p>Publications</p>"
                'out4 &= "<span>" & formatfunctions.AutoFormatText(PublicationsDT.Rows(i)("PublicationTitle").ToString.Trim) & "</span>"
                out4 &= "<p><strong>"
                If PublicationsDT.Rows(i)("PDF").ToString.Trim = "1" Then
                    out4 &= "<a target=""_blank"" href=""/dynamic/document/week/news/download/" & PublicationsDT.Rows(i)("News_ID").ToString.Trim & "/" & PublicationsDT.Rows(i)("News_ID").ToString.Trim & ".pdf"">"
                ElseIf PublicationsDT.Rows(i)("PDF").ToString.Trim = "0" And PublicationsDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                    out4 &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(PublicationsDT.Rows(i)("WebUrl").ToString.Trim) & """ >"
                    'Else
                    '    out4 &= "<a>"
                End If
                out4 &= formatfunctions.AutoFormatText(PublicationsDT.Rows(i)("Headline").ToString.Trim)
                If PublicationsDT.Rows(i)("PDF").ToString.Trim = "1" Or PublicationsDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                    out4 &= "</a>"
                End If

                If PublicationsDT.Rows(i)("VideoLink").ToString.Trim <> "" Then
                    out4 &= "<a href=""" & formatfunctionssimple.AutoFormatText(PublicationsDT.Rows(i)("VideoLink").ToString.Trim) & """ class=""video vimeo"">View video</a>"
                End If

                out4 &= "</strong></p>"
                out4 &= "<p><strong></strong></p>"
                out4 &= "</dd>"
            Next


            If SpeakingEngagementDT.Rows.Count > 0 Then
                For i As Integer = 0 To SpeakingEngagementDT.Rows.Count - 1
                    out4 &= "<dd>"
                    out4 &= "<p>Speaking Engagements</p>"
                    out4 &= "<p><strong>"
                    If SpeakingEngagementDT.Rows(i)("LinkU").ToString.Trim <> "" Then
                        out4 &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(SpeakingEngagementDT.Rows(i)("LinkU").ToString.Trim) & """ title="""" >"
                        'Else
                        '    out4 &= "<a>"
                    End If
                    out4 &= formatfunctions.AutoFormatText(SpeakingEngagementDT.Rows(i)("Headline").ToString.Trim)

                    If SpeakingEngagementDT.Rows(i)("LinkU").ToString.Trim <> "" Then
                        out4 &= "</a>"
                    End If

                    If SpeakingEngagementDT.Rows(i)("VideoLink").ToString.Trim <> "" Then
                        out4 &= "<a href=""" & formatfunctionssimple.AutoFormatText(SpeakingEngagementDT.Rows(i)("VideoLink").ToString.Trim) & """ class=""video vimeo"">View video</a>"
                    End If
                    out4 &= "</strong></p>"
                    out4 &= "<p><strong></strong></p>"
                    out4 &= "</dd>"
                Next
            End If


            If WhitePapersDT.Rows.Count > 0 Then
                For i As Integer = 0 To WhitePapersDT.Rows.Count - 1
                    out4 &= "<dd>"
                    out4 &= "<p>White Papers</p>"
                    'out4 &= "<span>" & formatfunctions.AutoFormatText(WhitePapersDT.Rows(i)("PublicationTitle").ToString.Trim) & "</span>"
                    out4 &= "<p><strong>"
                    If WhitePapersDT.Rows(i)("PDF").ToString.Trim = "1" Then
                        out4 &= "<a target=""_blank"" href=""/dynamic/document/week/news/download/" & WhitePapersDT.Rows(i)("News_ID").ToString.Trim & "/" & WhitePapersDT.Rows(i)("News_ID").ToString.Trim & ".pdf"">"
                    ElseIf WhitePapersDT.Rows(i)("PDF").ToString.Trim = "0" And WhitePapersDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                        out4 &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(WhitePapersDT.Rows(i)("WebUrl").ToString.Trim) & """ >"
                        'Else
                        '    out4 &= "<a>"
                    End If
                    out4 &= formatfunctions.AutoFormatText(WhitePapersDT.Rows(i)("Headline").ToString.Trim)
                    If WhitePapersDT.Rows(i)("PDF").ToString.Trim = "1" Or WhitePapersDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                        out4 &= "</a>"
                    End If
                    If WhitePapersDT.Rows(i)("VideoLink").ToString.Trim <> "" Then
                        out4 &= "<a href=""" & formatfunctionssimple.AutoFormatText(WhitePapersDT.Rows(i)("VideoLink").ToString.Trim) & """ class=""video vimeo"">View video</a>"
                    End If
                    out4 &= "</strong></p>"
                    out4 &= "<p><strong></strong></p>"
                    out4 &= "</dd>"
                Next
            End If
            out4 &= "</dl> "
        End If


        rel_pages_list1.Text = out4

    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub

End Class
