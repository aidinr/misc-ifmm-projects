﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="news_current_press.aspx.vb" Inherits="news_current_press" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first">NEWS</a><span> | </span></li>
                        <li><a href="<%=Master.UrlPage%>" class="active">CURRENT PRESS</a></li>
                    </ul>
                </div>

                <div class="two_columns">
                    <div class="left_smaller">
                        <h3 class="sub_title">News</h3>

                        <ul class="leadership_sorting_list">
                            <li><a href="news_press_release" title="">Press Releases</a></li>
                            <li><a href="news_current_press" title="" class="active">Current Press</a></li>
                            
                        </ul>

                    </div>
                    
                    <div class="right_bigger mt37">

                        <div class="headline">
                            <h2>CURRENT PRESS</h2>                       
                        </div>                 
                        
                         <div class="cms cmsType_Rich cmsName_Current_Press_Intro">
                            <%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Current_Press_Intro")%>
                            </div>
                                                                     
                        <ul class="current_press">
                            <asp:Literal runat="server" ID="news_current_press_list1"/>          
                        </ul>
                        
                    </div>
                </div>
</asp:Content>

