﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="strategies.aspx.vb" Inherits="strategies" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_Approach"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Approach")%></a><span> | </span></li>
                        <li><a class="cmsMultiLanguage cms cmsType_TextSingle cmsName_Strategies active" href="<%=Master.UrlPage%>"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Strategies")%></a></li>
                    </ul>
                </div>
                <h3 class="sub_title"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Strategies")%></h3>
                <div class="left_bigger_right_smaller_layout">
                    <div class="left">
                         <img 
                         src="cms/data/Sized/liquid/690x/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "strategies_image_1")%>" 
                         alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "strategies_image_1")%>" 
                         title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "strategies_image_1")%>" 
                         class="<%="cms cmsType_Image cmsName_strategies_image_1"%>" />



                        <div class="text bigger_space_bettween_p">
                        <div class="cms cmsType_Rich cmsName_Strategies_Intro">
                            <%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Strategies_Intro")%>
                            </div>                         
                        </div>                        
                    </div>
                    <div class="right no_border">
                             <div class="cms cmsType_Rich cmsName_Strategies_Right">
                            <%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Strategies_Right")%>
                            </div>      

                    </div>
                </div>
<%
     Dim HasName As String = ""
     Dim HasSlides As String = ""
     If Directory.GetFiles(Server.MapPath("~") & "cms\data\Text\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & "*").Length > 0 Then
         HasName = "1"
     End If
     If Directory.GetFiles(Server.MapPath("~") & "cms\data\Textset\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & "*").Length > 0 Then
         HasSlides = "1"
     End If
%>

<%  If HasName = "1" Then%>
 <div class="casestudy_list">
<h3 class="headline_additional">view select case studies below</h3>
<ul class="cmsGroup cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName, "_")%>_Case_Studies">
<!--
<li><a><span class="selectTemplate cms cmsType_TextSingle cmsName_Name">Case Study:</span><strong>Case Study Name</strong></a><a class="selectTemplate cms cmsType_Textset cmsName_List"></a></li>
-->
 <%
     Dim SlideName As String
     Dim SlideLayout As String
     For i As Integer = 1 To 20 - 1
         If Directory.GetFiles(Server.MapPath("~") & "cms\data\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
%>

<%
    For x As Integer = 0 To 150
        SlideName = ""
        SlideLayout = ""
        If CMSFunctions.CheckTextFile(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List", x) Then
            SlideName = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xa" & x & ".txt")
            SlideLayout = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xb" & x & ".txt")
            SlideName = CMSFunctions.FormatFriendlyUrl(SlideName, "_")
            Exit For
        End If
    Next
    %>
<li class="cmsGroupItem">
<%  If SlideName <> "" Then%>
<a href="casestudy_<%=SlideLayout %>_<%=CategoryId %>_<%=SlideName %>">
<%End If%>
<span class="selectTemplate cms cmsType_TextSingle cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName,"_") %>_Case_Studies_Item_<%= i %>_Name">Case Study <%= i %>:</span>
<strong><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_Name")%></strong>
<%  If SlideName <> "" Then%>
</a>
<%End If%>
<a class="selectTemplate cms cmsType_Textset cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName,"_") %>_Case_Studies_Item_<%= i %>_List"></a>
</li>

<%End If %>
<%Next%>  
</ul>
</div>         
<%Else%> 
<div class="noCase cmsGroup cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName, "_")%>_Case_Studies">
<!--
<div class="casestudy_list"><ul><li><a href="#"><span class="selectTemplate cms cmsType_TextSingle cmsName_Name">Case Study:</span><strong>Example Case Study</strong></a><a class="selectTemplate cms cmsType_Textset cmsName_List"></a></li></ul></div>
-->
</div>
<%End If%>     
</asp:Content>

