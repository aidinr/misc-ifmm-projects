﻿Imports System.Data
Imports System.IO
Partial Class clients
    Inherits System.Web.UI.Page

    Public MainCategory As String
    Public practiceName As String
    Public MainPracticeId As String
    Public MainPracticeName As String
    Public practiceAreaUDFID As String
    Public breadcrumbsOut As String

    Public ClientsListDT As New DataTable
    Public FuturedClientDT As DataTable
    Public CategoryId As String

    Public FriendlyPracticeName As String
    Public FriendlyCMSName As String

    'Public ClientsDT As New DataTable
    Public practiceFrontText As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        CategoryId = Request.QueryString("cat")
        CategoryId = Replace(CategoryId, "_", " ")

        Select Case CategoryId
            Case "3413026"
                MainCategory = "By PRACTICE"
                practiceName = "Broadcast and Media"
                practiceFrontText = "Broadcast and Media"
            Case "3414326"
                MainCategory = "By PRACTICE"
                practiceName = "Courts and Public"
                practiceFrontText = "Courts and Public"
            Case "2400012"
                MainCategory = "By PRACTICE"
                practiceName = "Corporate Interiors"
                practiceFrontText = "Corporate Interiors"
                practiceAreaUDFID = "3408922"
            Case "3409338"
                MainCategory = "By PRACTICE"
                practiceName = "Education"
                practiceFrontText = "Education"
                practiceAreaUDFID = "3408922"
            Case "3409328"
                MainCategory = "By PRACTICE"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "Campus Planning"
                practiceFrontText = "Campus Planning"
                practiceAreaUDFID = "3408924"
            Case "2400014"
                MainCategory = "By PRACTICE"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "Higher Education"
                practiceFrontText = "Higher Education"
                practiceAreaUDFID = "3408924"
            Case "2400017"
                MainCategory = "By PRACTICE"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "K-12 Education"
                practiceFrontText = "K-12 Education"
                practiceAreaUDFID = "3408924"
            Case "3409341"
                MainCategory = "By PRACTICE"
                MainPracticeId = "3409338"
                MainPracticeName = "Education"
                practiceName = "Town and Gown"
                practiceFrontText = "Town and Gown"
                practiceAreaUDFID = "3408924"
            Case "2400024"
                MainCategory = "By PRACTICE"
                practiceName = "Healthcare"
                practiceFrontText = "Healthcare"
                practiceAreaUDFID = "3408922"
            Case "2400015"
                MainCategory = "By PRACTICE"
                practiceName = "Hospitality"
                practiceFrontText = "Hospitality"
                practiceAreaUDFID = "3408922"
            Case "2400016"
                MainCategory = "By PRACTICE"
                practiceName = "Housing"
                practiceFrontText = "Residential"
                practiceAreaUDFID = "3408922"
            Case "2400018"
                MainCategory = "By PRACTICE"
                practiceName = "Office and Retail"
                practiceFrontText = "Office and Retail"
                practiceAreaUDFID = "3408922"
            Case "2400019"
                MainCategory = "By PRACTICE"
                practiceName = "Cultural"
                practiceFrontText = "Cultural"
                practiceAreaUDFID = "3408922"
            Case "2400020"
                MainCategory = "By PRACTICE"
                practiceName = "Science and Technology"
                practiceFrontText = "Science and Technology"
                practiceAreaUDFID = "3408922"
            Case "2400021"
                MainCategory = "By PRACTICE"
                practiceName = "Senior Living"
                practiceFrontText = "Senior Living"
                practiceAreaUDFID = "3408922"
            Case "3409343"
                practiceName = "Urbanism"
                practiceFrontText = "Urbanism"
                practiceAreaUDFID = "3408922"
            Case "3409345"
                MainCategory = "By PRACTICE"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Downtown Redevelopment"
                practiceFrontText = "Downtown Redevelopment"
                practiceAreaUDFID = "3408924"
            Case "3409347"
                MainCategory = "By PRACTICE"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "New Town Planning"
                practiceFrontText = "New Town Planning"
                practiceAreaUDFID = "3408924"
            Case "3409349"
                MainCategory = "By PRACTICE"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Transit and Development"
                practiceFrontText = "Transit and Development"
                practiceAreaUDFID = "3408924"
            Case "2400022"
                MainCategory = "By PRACTICE"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Urban Design"
                practiceFrontText = "Urban Design"
                practiceAreaUDFID = "3408924"
            Case "3409351"
                MainCategory = "By PRACTICE"
                MainPracticeId = "3409343"
                MainPracticeName = "Urbanism"
                practiceName = "Waterfront"
                practiceFrontText = "Waterfront"
                practiceAreaUDFID = "3408924"

            Case "3421827"
                MainCategory = "By PRACTICE"
                practiceName = "Planning and Urban Design"
                practiceFrontText = "Planning and Urban Design"
            Case "3421826"
                MainCategory = "By PRACTICE"
                practiceName = "Transportation and Public Infrastructure"
                practiceFrontText = "Transportation and Public Infrastructure"
            Case "3421825"
                MainCategory = "By PRACTICE"
                practiceName = "Large Scale Mixed Use"
                practiceFrontText = "Large Scale Mixed Use"


            Case "Asia"
                MainCategory = "By REGION"
                practiceName = "Asia"
                practiceFrontText = "Asia"
            Case "Africa and the Middle East"
                MainCategory = "By REGION"
                practiceName = "Africa and the Middle East"
                practiceFrontText = "Africa and the Middle East"
            Case "China"
                MainCategory = "By REGION"
                practiceName = "China"
                practiceFrontText = "China"
            Case "Europe"
                MainCategory = "By REGION"
                practiceName = "Europe"
                practiceFrontText = "Europe"
            Case "The Americas"
                MainCategory = "By REGION"
                practiceName = "The Americas"
                practiceFrontText = "The Americas"
            Case "5699021"
                MainCategory = "By PRACTICE"
                practiceName = "Sports and Exhibition"
                practiceFrontText = "Sports and Exhibition"
            Case Else
                practiceName = "Invalid Practice"
                practiceFrontText = "Invalid Practice"
        End Select


        FriendlyPracticeName = CMSFunctions.FormatFriendlyUrl(practiceName, "_")
        FriendlyPracticeName = Replace(FriendlyPracticeName, "-", "_")
        FriendlyCMSName = FriendlyPracticeName

        clients_list_title.Text = practiceFrontText & " " & "Client List"


        If CategoryId <> "" Then
            breadcrumbsOut &= "<li><a title="""" class=""first"">Work</a><span> | </span></li>" & vbNewLine
            breadcrumbsOut &= "<li><a title="""">" & MainCategory & "</a><span> | </span></li>" & vbNewLine
            If MainPracticeName <> "" Then
                Dim MainCatFriendlyLink As String = ""
                MainCatFriendlyLink = "practice_" & MainPracticeId & "_" & formaturl.friendlyurl(MainPracticeName.ToLower)
                breadcrumbsOut &= "<li><a href=""" & MainCatFriendlyLink & """ title="""">" & MainPracticeName & "</a><span> | </span></li>" & vbNewLine
            End If
            If MainCategory = "By PRACTICE" Then
                Dim CatFriendlyLink As String = ""
                If practiceName = "Corporate Interiors" Then
                    CatFriendlyLink = "corporate_interiors"
                Else
                    CatFriendlyLink = "category_" & CategoryId & "_" & formaturl.friendlyurl(practiceFrontText.ToLower)
                End If

                breadcrumbsOut &= "<li><a href=""" & CatFriendlyLink & """ title="""">" & practiceFrontText & "</a><span> | </span></li>" & vbNewLine
            Else
                Dim RegionFriendlyLink As String = ""
                RegionFriendlyLink = "region_" & formaturl.friendlyurl(practiceFrontText)
                breadcrumbsOut &= "<li><a href=""" & RegionFriendlyLink & """ title="""">" & practiceFrontText & "</a><span> | </span></li>" & vbNewLine
            End If
            breadcrumbsOut &= "<li><a title="""" class=""active"">" & practiceFrontText & " " & "Client List" & "</a></li>" & vbNewLine
        End If


        'Futured
        Dim Sql2 As String = ""
        Sql2 &= "select top 1 "
        Sql2 &= "p.ProjectID, "
        Sql2 &= "P.Name as ProjectName, "
        Sql2 &= "p.ClientName, "
        Sql2 &= "case when isnull(P.State_id,'') <> '' or P.State_id is null then P.City + case when isnull(S.Name ,'') <> '' then ', ' + S.Name else '' end else p.City + case when isnull(P5.Item_Value,'') <> '' then ', ' + P5.Item_Value else '' end + case when isnull(P4.Item_Value,'') <> '' then ', ' + P4.Item_Value else '' end end as ProjectAddress "
        Sql2 &= "FROM IPM_PROJECT P "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P8 on P.ProjectID = P8.ProjectID and P8.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUBPRACTICEAREA') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P2 on P.ProjectID = P2.ProjectID and P2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PRACTICEAREA') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P3 on P.ProjectID = P3.ProjectID and P3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_FEATURED') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P4 on P.ProjectID = P4.ProjectID and P4.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_CITY') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P5 on P.ProjectID = P5.ProjectID and P5.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_STATE') "
        Sql2 &= "left join IPM_PROJECT_FIELD_VALUE P1 on P.ProjectID = P1.ProjectID and P1.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_REGIONS') "
        Sql2 &= "left join IPM_STATE S on S.State_id = P.State_id "
        Sql2 &= "WHERE P.Available = 'Y' "
        Sql2 &= "AND P.Show = 1 "
        If IsNumeric(CategoryId) = True Then
            If CategoryId = "3409328" Or CategoryId = "2400014" Or CategoryId = "2400017" Or CategoryId = "3409341" Or CategoryId = "3409345" Or CategoryId = "3409347" Or CategoryId = "3409349" Or CategoryId = "2400022" Or CategoryId = "3409351" Then
                Sql2 &= "AND charindex('" & practiceName & "',LTRIM(RTRIM(P8.Item_Value)))!= 0 "
            Else
                Sql2 &= "AND charindex('" & practiceName & "',LTRIM(RTRIM(P2.Item_Value)))!= 0 "
            End If
        Else
            Sql2 &= "AND charindex('" & practiceName & "',LTRIM(RTRIM(P1.Item_Value)))!= 0 "
        End If

        Sql2 &= "ORDER BY NEWID() "
        FuturedClientDT = New DataTable("FuturedClientDT")
        FuturedClientDT = mmfunctions.GetDataTable(Sql2)
        Dim out1 As String = ""
        If FuturedClientDT.Rows.Count > 0 Then
            Dim sqlImg As String = ""
            sqlImg &= "select top 9 * from "
            sqlImg &= " (select a.Asset_ID, a.projectid, 1 apply, c.Item_value from ipm_asset_category b, ipm_asset a "
            sqlImg &= "left join ipm_asset_field_value c on c.item_id = 2400079 and c.asset_id = a.asset_id "
            sqlImg &= "where a.projectid = '" & FuturedClientDT.Rows(0)("ProjectID").ToString.Trim & "'"
            sqlImg &= "and a.category_id = b.category_id "
            sqlImg &= "and a.available = 'y' "
            sqlImg &= "and b.available = 'y' "
            sqlImg &= "and b.name = 'Public Website Images' "
            sqlImg &= "and a.HPixel >= 350 "
            sqlImg &= "UNION select a.Asset_ID, a.projectid, b.Item_Value apply, c.Item_value "
            sqlImg &= "from ipm_asset a "
            sqlImg &= "left join ipm_asset_field_value B on B.item_id = 2400401 and B.asset_id = a.asset_id "
            sqlImg &= "left join ipm_asset_field_value c on c.item_id = 2400079 and c.asset_id = a.asset_id "
            sqlImg &= "where a.projectid = '" & FuturedClientDT.Rows(0)("ProjectID").ToString.Trim & "'"
            sqlImg &= "AND a.available = 'y' and B.Item_value = 1 and b.Item_Value is not null  and a.HPixel >= 350) "
            sqlImg &= "a order by a.item_value desc "
            Dim ProjectImageDT As DataTable
            ProjectImageDT = New DataTable("ProjectImageDT")
            ProjectImageDT = mmfunctions.GetDataTable(sqlImg)
            Dim PrFriendlyLink As String = "project_" & FuturedClientDT.Rows(0)("ProjectID").ToString.Trim & "_" & formaturl.friendlyurl(FuturedClientDT.Rows(0)("ProjectName").ToString.Trim.ToLower)
            out1 &= "<div class=""big_image_with_info_box"">"
            If ProjectImageDT.Rows.Count > 0 Then
                out1 &= "<a href=""" & PrFriendlyLink & """>"
            Else
                out1 &= "<a title="""">"
            End If
            out1 &= "<img src=""/dynamic/image/week/project/best/220x230/92/ffffff/North/" & FuturedClientDT.Rows(0)("ProjectID").ToString.Trim & ".jpg"" alt=""""/>"
            out1 &= "<em class=""info_pop inner_pop"">"
            out1 &= "<em>" & FuturedClientDT.Rows(0)("ProjectName").ToString.Trim & "</em>"
            out1 &= "<span>" & FuturedClientDT.Rows(0)("ProjectAddress").ToString.Trim & "</span>"
            out1 &= "</em></a>"
            out1 &= "</div>"
            If MainCategory = "By PRACTICE" Then
                Dim CatFriendlyLink As String = ""
                CatFriendlyLink = "category_" & CategoryId & "_" & formaturl.friendlyurl(practiceFrontText.ToLower)
                If practiceName = "Corporate Interiors" Then
                    out1 &= "<a href=""corporate_interiors"" class=""view_more"">Back to Corporate Interiors</a>"
                Else
                    out1 &= "<a href=""" & CatFriendlyLink & """ class=""view_more"">Back to" & " " & practiceFrontText & "</a>"
                End If
            Else
                Dim RegionFriendlyLink As String = ""
                RegionFriendlyLink = "region_" & formaturl.friendlyurl(practiceFrontText)
                out1 &= "<a href=""" & RegionFriendlyLink & """ class=""view_more gallery"">Back to" & " " & practiceFrontText & "</a>"
            End If

        End If


        featured_client_lit.Text = out1
        breadcrumbs_list.Text = breadcrumbsOut

        'Dim LastVisitedCookie As New HttpCookie("LastVisited")
        'LastVisitedCookie.Values("parent") = "clients," & ""
        'LastVisitedCookie.Values("lastVisit") = DateTime.Now.ToString()
        'LastVisitedCookie.Expires = DateTime.Now.AddDays(1)
        'Response.Cookies.Add(LastVisitedCookie)


    End Sub



    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub




End Class
