﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="our_process.aspx.vb" Inherits="our_process" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a class="first">ABOUT</a> <span> | </span></li>
                        <li><a href="<%=Master.UrlPage%>" class="active">Our Process</a></li> 
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_About"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "About")%></a><span> | </span></li>
                        <li><a class="active" href="<%=Master.UrlPage%>"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Our_Process")%></a></li>
                    </ul>
                </div>
                
                <h3 class="sub_title cmsMultiLanguage cms cmsType_TextSingle cmsName_Our_Process"><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Our_Process")%></h3>

                <h2 class="intro cms cmsType_TextMulti cmsName_Process_Intro"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Process_Intro")%></h2>
                <div class="cols4 cmsGroup cmsName_Process">
                <!--
                <div><img src="assets/images/black_ghost.png" alt="" class="cms cmsType_Image cmsName_Image"/><strong class="cms cmsType_TextSingle cmsName_Heading">Item XYZ</strong><p class="cms cmsType_TextMulti cmsName_Content">Content Here</p></div>
                -->
                  <%
                      Dim RowPosition As Integer
                      For i As Integer = 1 To 201 - 1
                          RowPosition = RowPosition + 1
                          If Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Process_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
            %>
<div class="cmsGroupItem">
<img src="cms/data/Sized/best/220x147/65/F4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Process_Item_" & i & "_Image")%>" class="left cms cmsType_Image cmsName_Process_Item_<%=i %>_Image"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Process_Item_" & i & "_Image")%>" />
<strong class="cms cmsType_TextSingle cmsName_Process_Item_<%=i %>_Heading"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Process_Item_" & i & "_Heading")%></strong>
<p class="cms cmsType_TextMulti cmsName_Process_Item_<%=i %>_Content">
<%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Process_Item_" & i & "_Content")%>
</p>
</div>

<%
    If RowPosition = 4 Then
        RowPosition = 0
        %>
        <div class="c"></div>
        <% End If%>
                                 <%End If %>
                            <%Next%>  
                
                </div>         
</asp:Content>

