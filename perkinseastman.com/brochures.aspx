﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="brochures.aspx.vb" Inherits="brochures" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="breadcrumbs">
    <ul>
        <li><a href="/" title="" class="first">Home</a><span> | </span></li>
        <li><a href="<%=Master.UrlPage%>" title="" class="active">Brochures</a></li>                        
    </ul>
</div>             
<h3 class="sub_title">Brochures</h3>
<div class="left_bigger_right_smaller_layout">
    <div class="brochures-container">
        <ul>
          <asp:repeater runat="server" id="rptBrochures">
           <ItemTemplate>
            <li style="background: url('<%=Session("WSRetreiveAsset")%>id=<%#Container.DataItem("asset_id")%>&width=211&height=300&qfactor=1') no-repeat;">
                <div>
                    <a class="download"  href="<%=Session("WSDownloadAsset")%>dtype=assetdownload&size=0&assetid=<%#Container.DataItem("asset_id")%>"><span>Download</span></a>
                    <%#IIf(String.IsNullOrEmpty(Container.DataItem("Issuu")), "", "<a class='issuu' target='_blank'  href='" & Container.DataItem("Issuu") & "'><span>View in Issuu</span></a>") %>
                </div>
            </li>
            </ItemTemplate>
            </asp:repeater>
        </ul>
    </div>
</div>
</asp:Content>