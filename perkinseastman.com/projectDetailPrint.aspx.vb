﻿Imports System.Data

Partial Class projectDetailPrint
    Inherits System.Web.UI.Page

    Public CategoryId As String
    Public practiceName As String
    Public MainPracticeId As String
    Public MainPracticeName As String
    Public practiceAreaUDFID As String

    Public TotalRecords As Integer
    Public RecordPR As Integer
    Public NewRow As Integer
    Public ProjectDT As DataTable
    Public ProjectImageDT As DataTable


    Public breadcrumbsOut As String
    Public NavLinksOut As String
    Public ParentString As String
    Public ParentPage As String
    Public ParentValue As String
    Public NextProjectDT As DataTable
    Public ProjectName As String
    Public ProjectPrintLink As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ParentPage = ""
        ParentValue = ""

        Dim pageURL As String = HttpContext.Current.Request.RawUrl
        pageURL = Replace(pageURL, "/", "")
        If InStr(pageURL, "?") > 0 Then
            pageURL = pageURL.Substring(0, pageURL.IndexOf("?"))
            pageURL = Replace(pageURL, "/", "")
        End If
        Dim UrlPage As String = pageURL



        ProjectPrintLink = "print_" & pageURL

        Dim projectID As String = Request.QueryString("p")

        If Not Request.Cookies("LastVisited") Is Nothing Then
            ParentString = Request.Cookies("LastVisited")("parent")
        End If

        If ParentString <> "" Then
            ParentPage = ParentString.Split(",")(0)
            ParentValue = ParentString.Split(",")(1)
        End If

        If ParentPage = "category" Then
            CategoryId = ParentValue
            If CategoryId <> "" Then
                Select Case CategoryId
                    Case "3413026"
                        practiceName = "Broadcast and Media"
                    Case "3414326"
                        practiceName = "Courts and Public"
                    Case "2400012"
                        practiceName = "Corporate Interiors"
                        practiceAreaUDFID = "3408922"
                    Case "3409338"
                        practiceName = "Education"
                        practiceAreaUDFID = "3408922"
                    Case "3409328"
                        MainPracticeId = "3409338"
                        MainPracticeName = "Education"
                        practiceName = "Campus Planning"
                        practiceAreaUDFID = "3408924"
                    Case "2400014"
                        MainPracticeId = "3409338"
                        MainPracticeName = "Education"
                        practiceName = "Higher Education"
                        practiceAreaUDFID = "3408924"
                    Case "2400017"
                        MainPracticeId = "3409338"
                        MainPracticeName = "Education"
                        practiceName = "K-12 Education"
                        practiceAreaUDFID = "3408924"
                    Case "3409341"
                        MainPracticeId = "3409338"
                        MainPracticeName = "Education"
                        practiceName = "Town and Gown"
                        practiceAreaUDFID = "3408924"
                    Case "2400024"
                        practiceName = "Healthcare"
                        practiceAreaUDFID = "3408922"
                    Case "2400015"
                        practiceName = "Hospitality"
                        practiceAreaUDFID = "3408922"
                    Case "2400016"
                        practiceName = "Housing"
                        practiceAreaUDFID = "3408922"
                    Case "2400018"
                        practiceName = "Office and Retail"
                        practiceAreaUDFID = "3408922"
                    Case "2400019"
                        practiceName = "Cultural"
                        practiceAreaUDFID = "3408922"
                    Case "2400020"
                        practiceName = "Science and Technology"
                        practiceAreaUDFID = "3408922"
                    Case "2400021"
                        practiceName = "Senior Living"
                        practiceAreaUDFID = "3408922"
                    Case "3409343"
                        practiceName = "Urbanism"
                        practiceAreaUDFID = "3408922"
                    Case "3409345"
                        MainPracticeId = "3409343"
                        MainPracticeName = "Urbanism"
                        practiceName = "Downtown Redevelopment"
                        practiceAreaUDFID = "3408924"
                    Case "3409347"
                        MainPracticeId = "3409343"
                        MainPracticeName = "Urbanism"
                        practiceName = "New Town Planning"
                        practiceAreaUDFID = "3408924"
                    Case "3409349"
                        MainPracticeId = "3409343"
                        MainPracticeName = "Urbanism"
                        practiceName = "Transit and Development"
                        practiceAreaUDFID = "3408924"
                    Case "2400022"
                        MainPracticeId = "3409343"
                        MainPracticeName = "Urbanism"
                        practiceName = "Urban Design"
                        practiceAreaUDFID = "3408924"
                    Case "3409351"
                        MainPracticeId = "3409343"
                        MainPracticeName = "Urbanism"
                        practiceName = "Waterfront"
                        practiceAreaUDFID = "3408924"
                    Case "5699021"
                        practiceName = "Sports and Exhibition"
                    Case Else

                End Select
            End If
        End If


        Dim sql As String = ""
        sql &= "select "
        sql &= "P.ProjectID, "
        sql &= "P.Name as ProjectName, "
        sql &= "P1.Item_Value AS Region, "
        sql &= "P2.Item_Value AS PracticeArea, "
        sql &= "P8.Item_Value as SubPracticeArea, "
        sql &= "case when isnull(P.State_id,'') <> '' or P.State_id is null then P.City + case when isnull(S.Name ,'') <> '' then ', ' + S.Name else '' end else p.City + case when isnull(P5.Item_Value,'') <> '' then ', ' + P5.Item_Value else '' end + case when isnull(P4.Item_Value,'') <> '' then ', ' + P4.Item_Value else '' end end as ProjectAddress, "
        sql &= "P6.Item_Value As WebDescription, "
        sql &= "P7.Item_Value as ShortName "
        sql &= "FROM IPM_PROJECT P "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P1 on P.ProjectID = P1.ProjectID and P1.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_REGIONS') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P2 on P.ProjectID = P2.ProjectID and P2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PRACTICEAREA') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P3 on P.ProjectID = P3.ProjectID and P3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_CITY') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P4 on P.ProjectID = P4.ProjectID and P4.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_COUNTRY') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P5 on P.ProjectID = P5.ProjectID and P5.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_STATE') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P6 on P.ProjectID = P6.ProjectID and P6.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_DESCRIPTION_LONG') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P7 on P.ProjectID = P7.ProjectID and P7.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_SHORT_NAME') "
        sql &= "left join IPM_PROJECT_FIELD_VALUE P8 on P.ProjectID = P8.ProjectID and P8.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUBPRACTICEAREA') "
        sql &= "left join IPM_STATE S on S.State_id = P.State_id "
        sql &= "where P.ProjectID = '" & projectID & "'"
        ProjectDT = New DataTable("ProjectDT")
        ProjectDT = mmfunctions.GetDataTable(sql)

        If ProjectDT.Rows.Count > 0 Then
            ProjectName = formatfunctionssimple.AutoFormatText(ProjectDT.Rows(0)("ProjectName").ToString.Trim) & " / Perkins Eastman"
            project_title.Text = formatfunctionssimple.AutoFormatText(ProjectDT.Rows(0)("ProjectName").ToString.Trim)
            project_location.Text = formatfunctions.AutoFormatText(ProjectDT.Rows(0)("ProjectAddress").ToString.Trim)
            project_description.Text = formatfunctions.AutoFormatText(ProjectDT.Rows(0)("WebDescription").ToString.Trim)
        End If


        If Not Request.UrlReferrer Is Nothing Then
            If ParentPage = "category" Then
                Dim sqlpr As String = ""
                sqlpr &= "select "
                sqlpr &= "P.ProjectID, "
                sqlpr &= "P.Name as ProjectName "
                sqlpr &= "FROM IPM_PROJECT P "
                sqlpr &= "left join IPM_PROJECT_FIELD_VALUE P1 on P.ProjectID = P1.ProjectID and P1.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_REGIONS') "
                sqlpr &= "left join IPM_PROJECT_FIELD_VALUE P2 on P.ProjectID = P2.ProjectID and P2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PRACTICEAREA') "
                sqlpr &= "left join IPM_PROJECT_FIELD_VALUE P8 on P.ProjectID = P8.ProjectID and P8.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUBPRACTICEAREA') "
                sqlpr &= "left join IPM_PROJECT_FIELD_VALUE P9 on P.ProjectID = P9.ProjectID and P9.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAN_PROJECT_ORDERING') "
                sqlpr &= "left join IPM_STATE S on S.State_id = P.State_id "
                If CategoryId = 3409328 Or CategoryId = 2400014 Or CategoryId = 2400017 Or CategoryId = 3409341 Or CategoryId = 3409345 Or CategoryId = 3409347 Or CategoryId = 3409349 Or CategoryId = 2400022 Or CategoryId = 3409351 Then
                    sqlpr &= "where charindex('" & practiceName & "',LTRIM(RTRIM(P8.Item_Value)))!= 0 "
                Else
                    sqlpr &= "where charindex('" & practiceName & "',LTRIM(RTRIM(P2.Item_Value)))!= 0 "
                End If
                sqlpr &= "and P.Available = 'y' "
                sqlpr &= "and P.Show = 1 "
                sqlpr &= "order by CAST(isnull(P9.Item_Value, 9999999999) as integer) desc "

                NextProjectDT = New DataTable("NextProjectDT")
                NextProjectDT = mmfunctions.GetDataTable(sqlpr)

                Dim CatFriendlyLink As String = ""
                CatFriendlyLink = "category_" & CategoryId & "_" & formaturl.friendlyurl(practiceName.ToLower)

                breadcrumbsOut &= "<li><a class=""nolink first"">WORK</a> <span> | </span></li>" & vbNewLine
                breadcrumbsOut &= "<li><a class=""nolink"">BY PRACTICE AREA</a> <span> | </span></li>" & vbNewLine

                If MainPracticeName <> "" Then
                    Dim MainCatFriendlyLink As String = ""
                    MainCatFriendlyLink = "practice_" & MainPracticeId & "_" & formaturl.friendlyurl(MainPracticeName.ToLower)
                    breadcrumbsOut &= "<li><a href=""" & MainCatFriendlyLink & """ title="""">" & MainPracticeName & "</a><span> | </span></li>" & vbNewLine
                End If

                If practiceName = "Corporate Interiors" Then
                    breadcrumbsOut &= "<li><a href=""corporate_interiors"" title="""">" & practiceName & "</a><span> | </span></li>" & vbNewLine
                Else
                    breadcrumbsOut &= "<li><a href=""" & CatFriendlyLink & """ title="""">" & practiceName & "</a><span> | </span></li>" & vbNewLine
                End If


                breadcrumbsOut &= "<li><a href=""" & CatFriendlyLink & """>PROJECTS</a><span> | </span></li>" & vbNewLine

                breadcrumbsOut &= "<li><a href=""" & UrlPage & """ title="""" class=""active"">" & formatfunctionssimple.AutoFormatText(ProjectDT.Rows(0)("ProjectName").ToString.Trim) & "</a></li>" & vbNewLine


                NavLinksOut &= "<li><a href=""" & CatFriendlyLink & """ title="""">Back to Project List</a></li>"
                If NextProjectDT.Rows.Count > 0 Then
                    Dim i As Integer = 0
                    For Each row As DataRow In NextProjectDT.Rows
                        If (row("ProjectID").ToString.Trim() = projectID.Trim()) Then
                            Exit For
                        End If
                        i = i + 1
                    Next
                    If (i < NextProjectDT.Rows.Count - 1) Then
                        Dim PrFriendlyLink As String = "project_" & NextProjectDT.Rows(i + 1)("ProjectID").ToString.Trim & "_" & formaturl.friendlyurl(NextProjectDT.Rows(i + 1)("ProjectName").ToString.Trim.ToLower)
                        NavLinksOut &= "<li><a href=""" & PrFriendlyLink & """ title="""">View Next Project</a></li>"
                    Else
                    End If
                    If (i = 0) Then
                    Else
                        Dim PrFriendlyLink As String = "project_" & NextProjectDT.Rows(i - 1)("ProjectID").ToString.Trim & "_" & formaturl.friendlyurl(NextProjectDT.Rows(i - 1)("ProjectName").ToString.Trim.ToLower)
                        NavLinksOut &= "<li><a href=""" & PrFriendlyLink & """ title="""">View Previous Project</a></li>"
                    End If
                End If

            ElseIf ParentPage = "region" Then

                Dim sqlpr As String = ""
                sqlpr &= "select "
                sqlpr &= "a.ProjectID, "
                sqlpr &= "COALESCE( b.Item_Value, a.name) ProjectName  "
                'sqlpr &= "d.Item_Value disable "
                sqlpr &= "from ipm_project a  "
                sqlpr &= "join ipm_project_field_value c on c.projectid = a.projectid and c.item_id = 3408923 and c.item_value = '" & ParentValue & "' "
                sqlpr &= "left join IPM_PROJECT_FIELD_VALUE b on b.ProjectID = a.ProjectID and b.Item_ID = 2400053 "
                sqlpr &= "left join IPM_PROJECT_FIELD_VALUE d on a.ProjectID = d.ProjectID and d.Item_ID = 3409575 "
                sqlpr &= "left join IPM_PROJECT_FIELD_VALUE P9 on a.ProjectID = P9.ProjectID and P9.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAN_PROJECT_ORDERING') "
                sqlpr &= "where available = 'Y' "
                sqlpr &= "order by CAST(isnull(P9.Item_Value, 9999999999) as integer) desc"
                NextProjectDT = New DataTable("NextProjectDT")
                NextProjectDT = mmfunctions.GetDataTable(sqlpr)

                Dim RegionFriendlyLink As String = ""
                RegionFriendlyLink = "region_" & formaturl.friendlyurl(ParentValue)

                breadcrumbsOut &= "<li><a title="""" class=""first"">Work</a><span> | </span></li>" & vbNewLine
                breadcrumbsOut &= "<li><a title="""">By REGION</a><span> | </span></li>"
                breadcrumbsOut &= "<li><a href=""" & RegionFriendlyLink & """ title="""">" & ParentValue & "</a><span> | </span></li>" & vbNewLine
                breadcrumbsOut &= "<li><a title="""" class=""active"">" & formatfunctionssimple.AutoFormatText(ProjectDT.Rows(0)("ProjectName").ToString.Trim) & "</a></li>" & vbNewLine

                NavLinksOut &= "<li><a href=""" & RegionFriendlyLink & """ title="""">Back to Project List</a></li>"
                If NextProjectDT.Rows.Count > 0 Then
                    Dim i As Integer = 0
                    For Each row As DataRow In NextProjectDT.Rows
                        If (row("ProjectID").ToString.Trim() = projectID.Trim()) Then
                            Exit For
                        End If
                        i = i + 1
                    Next
                    If (i < NextProjectDT.Rows.Count - 1) Then
                        Dim PrFriendlyLink As String = "project_" & NextProjectDT.Rows(i + 1)("ProjectID").ToString.Trim & "_" & formaturl.friendlyurl(NextProjectDT.Rows(i + 1)("ProjectName").ToString.Trim.ToLower)
                        NavLinksOut &= "<li><a href=""" & PrFriendlyLink & """ title="""">View Next Project</a></li>"
                    Else
                    End If
                    If (i = 0) Then
                    Else
                        Dim PrFriendlyLink As String = "project_" & NextProjectDT.Rows(i - 1)("ProjectID").ToString.Trim & "_" & formaturl.friendlyurl(NextProjectDT.Rows(i - 1)("ProjectName").ToString.Trim.ToLower)
                        NavLinksOut &= "<li><a href=""" & PrFriendlyLink & """ title="""">View Previous Project</a></li>"
                    End If
                End If

            ElseIf ParentPage = "eekp" Then

                Dim sqlpr As String = ""
                sqlpr &= "select "
                sqlpr &= "p.ProjectID, "
                sqlpr &= "P.Name as ProjectName, "
                sqlpr &= "P3.Item_Value "
                sqlpr &= "FROM IPM_PROJECT P "
                sqlpr &= "left join IPM_PROJECT_FIELD_VALUE P3 on P.ProjectID = P3.ProjectID and P3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_EEKPROJECT') "
                sqlpr &= "left join IPM_PROJECT_FIELD_VALUE P4 on P.ProjectID = P4.ProjectID and P4.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_CITY') "
                sqlpr &= "left join IPM_PROJECT_FIELD_VALUE P5 on P.ProjectID = P5.ProjectID and P5.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJECT_STATE') "
                sqlpr &= "left join IPM_STATE S on S.State_id = P.State_id "
                sqlpr &= "where P3.Item_Value = 1 "
                sqlpr &= "and P.Available = 'y' "
                sqlpr &= "order by P.Name asc "
                NextProjectDT = New DataTable("NextProjectDT")
                NextProjectDT = mmfunctions.GetDataTable(sqlpr)

                breadcrumbsOut &= "<li><a href=""eek_page"" title="""" class=""first"">EE&amp;K a perkins eastman company</a><span> | </span></li>" & vbNewLine
                breadcrumbsOut &= "<li><a href=""eek_projects"" title="""">Selected Projects</a><span> | </span></li>" & vbNewLine
                breadcrumbsOut &= "<li><a title="""" class=""active"">" & formatfunctionssimple.AutoFormatText(ProjectDT.Rows(0)("ProjectName").ToString.Trim) & "</a></li>" & vbNewLine

                NavLinksOut &= "<li><a href=""eek_projects"" title="""">Back to Project List</a></li>"
                If NextProjectDT.Rows.Count > 0 Then
                    Dim i As Integer = 0
                    For Each row As DataRow In NextProjectDT.Rows
                        If (row("ProjectID").ToString.Trim() = projectID.Trim()) Then
                            Exit For
                        End If
                        i = i + 1
                    Next
                    If (i < NextProjectDT.Rows.Count - 1) Then
                        Dim PrFriendlyLink As String = "project_" & NextProjectDT.Rows(i + 1)("ProjectID").ToString.Trim & "_" & formaturl.friendlyurl(NextProjectDT.Rows(i + 1)("ProjectName").ToString.Trim.ToLower)
                        NavLinksOut &= "<li><a href=""" & PrFriendlyLink & """ title="""">View Next Project</a></li>"
                    Else
                    End If
                    If (i = 0) Then
                    Else
                        Dim PrFriendlyLink As String = "project_" & NextProjectDT.Rows(i - 1)("ProjectID").ToString.Trim & "_" & formaturl.friendlyurl(NextProjectDT.Rows(i - 1)("ProjectName").ToString.Trim.ToLower)
                        NavLinksOut &= "<li><a href=""" & PrFriendlyLink & """ title="""">View Previous Project</a></li>"
                    End If
                End If


            Else
                breadcrumbsOut &= "<li><a title="""" class=""first"">Work</a><span> | </span></li>" & vbNewLine
                breadcrumbsOut &= "<li><a title="""">Projects</a><span> | </span></li>" & vbNewLine
                breadcrumbsOut &= "<li><a href=""" & UrlPage & """ title="""" class=""active"">" & formatfunctionssimple.AutoFormatText(ProjectDT.Rows(0)("ProjectName").ToString.Trim) & "</a></li>" & vbNewLine
            End If
        Else
            breadcrumbsOut &= "<li><a title="""" class=""first"">Work</a><span> | </span></li>" & vbNewLine
            breadcrumbsOut &= "<li><a title="""">Projects</a><span> | </span></li>" & vbNewLine
            breadcrumbsOut &= "<li><a href=""" & UrlPage & """ title="""" class=""active"">" & formatfunctionssimple.AutoFormatText(ProjectDT.Rows(0)("ProjectName").ToString.Trim) & "</a></li>" & vbNewLine
        End If


        Dim out As String = ""
        Dim out2 As String = ""

        Dim sql2 As String = ""
        sql2 &= "select top 9 * from "
        sql2 &= " (select a.Asset_ID, a.projectid, 1 apply, c.Item_value from ipm_asset_category b, ipm_asset a "
        sql2 &= "left join ipm_asset_field_value c on c.item_id = 2400079 and c.asset_id = a.asset_id "
        sql2 &= "where a.projectid = '" & projectID & "'"
        sql2 &= "and a.category_id = b.category_id "
        sql2 &= "and a.available = 'y' "
        sql2 &= "and b.available = 'y' "
        sql2 &= "and b.name = 'Public Website Images' "
        sql2 &= "and a.HPixel >= 350 "
        sql2 &= "UNION select a.Asset_ID, a.projectid, b.Item_Value apply, c.Item_value "
        sql2 &= "from ipm_asset a "
        sql2 &= "left join ipm_asset_field_value B on B.item_id = 2400401 and B.asset_id = a.asset_id "
        sql2 &= "left join ipm_asset_field_value c on c.item_id = 2400079 and c.asset_id = a.asset_id "
        sql2 &= "where a.projectid = '" & projectID & "'"
        sql2 &= "AND a.available = 'y' and B.Item_value = 1 and b.Item_Value is not null  and a.HPixel >= 350) "
        'sql2 &= "a order by a.item_value desc "
        sql2 &= "a order by CAST(isnull(a.item_value, 99999) as integer) desc"
        'sql2 &= "a order by CAST(isnull(dbo.NumericOnly(a.item_value), 99999) as integer) desc "

        ProjectImageDT = New DataTable("ProjectImageDT")
        ProjectImageDT = mmfunctions.GetDataTable(sql2)

        RecordPR = 9
        TotalRecords = ProjectImageDT.Rows.Count

        If TotalRecords > 0 Then
            out &= "<h3 class=""headline_additional"">Additional views of" & " " & formatfunctionssimple.AutoFormatText(ProjectDT.Rows(0)("ProjectName").ToString.Trim) & "</h3>"
            out &= "<ul class=""main_list small_thumbs"">"
            For R As Integer = 0 To TotalRecords - 1
                NewRow = NewRow + 1
                If NewRow = RecordPR Then
                    out &= "<li class=""last_in_row"">"
                    NewRow = 0
                Else
                    out &= " <li> "
                End If
                out &= "<a href=""/dynamic/image/week/asset/liquid/1500x/92/777777/Center/" & ProjectImageDT.Rows(R)("Asset_ID").ToString.Trim & ".jpg"" rel=""" & R + 1 & """>"
                out &= "<img src=""/dynamic/image/week/asset/best/60x60/92/777777/Center/" & ProjectImageDT.Rows(R)("Asset_ID").ToString.Trim & ".jpg"" alt=""""/>"
                out &= "</a>"
                out &= "</li>"

                out2 &= " <div class=""big_image_with_info_box"" id=""box_" & R + 1 & """>"
                'out2 &= "<a class=""boxImg"" href=""/dynamic/image/week/asset/liquid/1500x/92/777777/Center/" & ProjectImageDT.Rows(R)("Asset_ID").ToString.Trim & ".jpg""><img src=""/dynamic/image/week/asset/padded/700x467/92/ffffff/SouthWest/" & ProjectImageDT.Rows(R)("Asset_ID").ToString.Trim & ".jpg"" alt=""""/></a>"
                out2 &= "<a class=""boxImg"" href=""/dynamic/image/week/asset/liquid/1500x/92/777777/Center/" & ProjectImageDT.Rows(R)("Asset_ID").ToString.Trim & ".jpg""><img src=""/dynamic/image/week/asset/padded/700x467/92/ffffff/NorthWest/" & ProjectImageDT.Rows(R)("Asset_ID").ToString.Trim & ".jpg"" alt=""""/></a>"


                EnlargeImg.Text = "<a class=""enlargeImage"" rel=""group"">Enlarge Image</a>"

                out2 &= "</div>"
            Next
            out &= "</ul>"

            big_project_image.Text = out2
            small_images_list1.Text = out
        End If





        'Retaled Topics
        Dim sql4a As String = ""
        Dim sql4b As String = ""
        Dim sql4c As String = ""
        Dim sql4d As String = ""
        Dim sql4e As String = ""
        ''Testi
        'sql4a &= "select "
        'sql4a &= "U.UserID, "
        'sql4a &= "U.FirstName + ' ' + U.LastName FullName, "
        'sql4a &= "U1.Item_Value As Testimonial, "
        'sql4a &= "U2.Item_Value As Link, "
        'sql4a &= "U3.Item_Value as RelId, "
        'sql4a &= "U4.Item_Value as SubTestimonial "
        'sql4a &= "from IPM_USER U "
        'sql4a &= "left join IPM_USER_FIELD_VALUE U1 on U.UserID = U1.USER_ID and U1.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTIMONIAL') "
        'sql4a &= "left join IPM_USER_FIELD_VALUE U2 on U.UserID = U2.USER_ID and U2.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USERLINK') "
        'sql4a &= "left join IPM_USER_FIELD_VALUE U3 on U.UserID = U3.USER_ID and U3.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTIIDS') "
        'sql4a &= "left join IPM_USER_FIELD_VALUE U4 on U.UserID = U4.USER_ID and U4.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_TESTISUB') "
        'sql4a &= "where Active = 'Y' "
        'sql4a &= "and u.Contact = 1 "
        'sql4a &= "and charindex('" & CategoryId & "',U3.Item_Value)!= 0 "
        'Dim TestimonialDT As DataTable
        'TestimonialDT = New DataTable("TestimonialDT")
        'TestimonialDT = mmfunctions.GetDataTable(sql4a)
        'Video
        sql4b &= "select top 0 "
        sql4b &= "N.News_Id, "
        sql4b &= "N.Headline, "
        sql4b &= "N2.Item_Value as VideoLink "
        sql4b &= "FROM IPM_NEWS_RELATED_PROJECTS RP "
        sql4b &= "JOIN IPM_NEWS N on N.News_Id = RP.News_Id "
        sql4b &= "LEFT JOIN IPM_NEWS_FIELD_VALUE N2 on N.News_Id = N2.NEWS_ID and N2.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_VIDEOLINK') "
        sql4b &= "WHERE RP.ProjectID = " & projectID
        sql4b &= "AND N.Show = 1 And N.Post_Date < GETDATE() And N.Pull_Date > GETDATE() "
        sql4b &= "AND N2.Item_Value <> '' and  N2.Item_Value is not null "
        sql4b &= "ORDER BY NEWID() "
        Dim VideoDT As DataTable
        VideoDT = New DataTable("VideoDT")
        VideoDT = mmfunctions.GetDataTable(sql4b)
        'Awards
        sql4c &= "select top 12 "
        sql4c &= "AW.Awards_Id, "
        sql4c &= "AW.Headline, "
        sql4c &= "AW.PublicationTitle,"
        sql4c &= "YEAR(AW.Post_Date) as AwardYear "
        sql4c &= "from IPM_AWARDS AW "
        sql4c &= "join IPM_AWARDS_RELATED_PROJECTS AW4 on AW4.Awards_Id = AW.Awards_Id "
        sql4c &= "join IPM_PROJECT AW5 on AW5.ProjectID = AW4.ProjectID "
        sql4c &= "join IPM_AWARDS_FIELD_VALUE AW3 on AW.AWARDS_ID = AW3.AWARDS_ID and AW3.Item_ID = (Select Item_ID From IPM_AWARDS_FIELD_DESC where Item_Tag = 'IDAM_AWARDFEATURE') "
        sql4c &= "where AW5.ProjectID = " & projectID
        sql4c &= "and AW.Show = 1 "
        sql4c &= "and AW3.Item_Value = '1' "
        sql4c &= "and AW5.Available = 'Y' "
        sql4c &= "and AW5.Show = 1 "
        sql4c &= "and AW.post_date < getdate() "
        sql4c &= "and AW.pull_date > getdate() "
        sql4c &= "Group by YEAR(AW.Post_Date), AW.Awards_Id, AW.Headline, AW.PublicationTitle, AW3.Item_Value, AW.Post_Date "
        sql4c &= "ORDER BY CAST(isnull(AW3.Item_Value, 0) as integer) desc, AW.Post_Date desc "
        Dim AwardDT As DataTable
        AwardDT = New DataTable("AwardDT")
        AwardDT = mmfunctions.GetDataTable(sql4c)
        'Publications
        sql4d &= "select top 3 *, "
        sql4d &= "V1.Item_Value as WebUrl, "
        sql4d &= "cast(isnull(c.item_value,10000) as int) sort_value, "
        sql4d &= "left(DATENAME(mm,A.Post_Date),3) + ' ' + DATENAME(year, A.Post_Date) as PostYear "
        sql4d &= "from IPM_NEWS_RELATED_PROJECTS RP "
        sql4d &= "join ipm_news a on a.News_Id = RP.News_Id "
        sql4d &= "left join ipm_news_field_value b on a.news_id = b.news_id and b.item_id = 2400409 "
        sql4d &= "left join ipm_news_field_value c on a.news_id = c.news_id and c.item_id = 2401236 "
        sql4d &= "left join ipm_news_field_value V1 on a.news_id = V1.news_id and V1.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWS_URL') "
        sql4d &= "where type = 1 "
        sql4d &= "and RP.ProjectID = " & projectID
        sql4d &= "and show = 1 "
        sql4d &= "and post_date < getdate() "
        sql4d &= "and pull_date > getdate() "
        sql4d &= "order by sort_value asc, post_date desc"
        Dim PublicationsDT As New DataTable("PublicationsDT")
        PublicationsDT = mmfunctions.GetDataTable(sql4d)
        ''Press
        'sql4e &= "select top 0 *, "
        'sql4e &= "V1.Item_Value as WebUrl, "
        'sql4e &= "cast(isnull(c.item_value,10000) as int) sort_value, "
        'sql4e &= "left(DATENAME(mm,A.Post_Date),3) + ' ' + DATENAME(year, A.Post_Date) as PostYear "
        'sql4e &= "from IPM_NEWS_RELATED_PROJECTS RP "
        'sql4e &= "join ipm_news a on a.News_Id = RP.News_Id "
        'sql4e &= "left join ipm_news_field_value b on a.news_id = b.news_id and b.item_id = 2400409 "
        'sql4e &= "left join ipm_news_field_value c on a.news_id = c.news_id and c.item_id = 2401236 "
        'sql4e &= "left join ipm_news_field_value V1 on a.news_id = V1.news_id and V1.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWS_URL') "
        'sql4e &= "where type in (0,8) "
        'sql4e &= "and RP.ProjectID = " & projectID
        'sql4e &= "and show = 1 "
        'sql4e &= "and post_date < getdate() "
        'sql4e &= "and pull_date > getdate() "
        'sql4e &= "order by sort_value asc, post_date desc"



        sql4e &= "Select top 3 "
        sql4e &= "n.News_ID, "
        sql4e &= "n.Headline, "
        sql4e &= "n.Content, "
        sql4e &= "n.PDF, "
        sql4e &= "n2.Item_Value as VideoLink, "
        sql4e &= "v.Item_Value url, "
        sql4e &= "F1.Item_Value as WebUrl, "
        sql4e &= "F2.Item_Value as VideoLink, "
        sql4e &= "DATENAME(mm,n.Post_Date) + ' ' + DATENAME(day,n.Post_Date) + ', ' + DATENAME(year, n.Post_Date) PostDate "
        sql4e &= "From IPM_NEWS n left join IPM_NEWS_FIELD_VALUE v on n.News_Id = v.NEWS_ID and v.Item_ID = 3407671 "
        sql4e &= "join IPM_NEWS_RELATED_PROJECTS RP on N.News_Id = RP.News_Id "
        sql4e &= "left join IPM_NEWS_FIELD_VALUE n2 on N.News_Id = n2.NEWS_ID and n2.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_VIDEOLINK') "

        sql4e &= "left join IPM_NEWS_FIELD_VALUE n3 on N.News_Id = n3.NEWS_ID and n3.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWS_FUTURED') "

        sql4e &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F1 on N.News_Id = F1.NEWS_ID and F1.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWS_URL') "
        sql4e &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F2 on N.News_Id = F2.NEWS_ID and F2.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_VIDEOLINK') "

        sql4e &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F3 on N.News_Id = F3.NEWS_ID and F3.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWS_SORT') "

        sql4e &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F4 on N.News_Id = F4.NEWS_ID and F4.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWSPROJCAT') "

        sql4e &= "Where(n.Show = 1 And n.Post_Date < GETDATE() And YEAR(Post_Date) > " & (Date.Today.Year - 99) & " And  n.Pull_Date > GETDATE() And n.Type = 0 and RP.ProjectID = " & projectID & " ) "

        sql4e &= "Order By n.Post_Date DESC "
        Dim PressDT As New DataTable("PressDT")
        PressDT = mmfunctions.GetDataTable(sql4e)



        'Dim PressDT As New DataTable("PressDT")
        'PressDT = CMSFunctions.GetNews(0, 0, practiceName, 3)

        Dim out4 As String = ""
        'AWARDS
        If AwardDT.Rows.Count > 0 Then
            out4 &= "<dl class=""contacts_list"">"
            out4 &= "<dt>AWARDS</dt><div class=""projAwards"">"
            For aw As Integer = 0 To AwardDT.Rows.Count - 1
                Dim AwardFriendlyLink As String = ""
                AwardFriendlyLink = "honor_awards_" & AwardDT.Rows(aw)("Awards_Id").ToString.Trim & "_" & formaturl.friendlyurl(AwardDT.Rows(aw)("Headline").ToString.Trim)
                out4 &= "<dd>"
                'out4 &= "<p><strong>"
                ' out4 &= "<a href=""" & AwardFriendlyLink & """ target=""_blank"" >" & formatfunctionssimple.AutoFormatText(AwardDT.Rows(0)("Headline").ToString.Trim) & "</a>"
                out4 &= "<em class=""awardCat"">" & formatfunctionssimple.AutoFormatText(AwardDT.Rows(aw)("Headline").ToString.Trim) & ", " & "</em>"
                out4 &= "<span class=""awardName"">" & formatfunctions.AutoFormatText(AwardDT.Rows(aw)("PublicationTitle").ToString.Trim) & " (" & AwardDT.Rows(aw)("AwardYear").ToString.Trim & ")" & "</span>"

                out4 &= "<span class=""award_" & formatfunctions.AutoFormatText(AwardDT.Rows(aw)("Awards_Id").ToString.Trim) & """></span>"
                'out4 &= "</strong></p>"
                out4 &= "</dd>"

            Next
            out4 &= "</div></dl> "
        End If
        'PRESS

        If PressDT.Rows.Count > 0 Then
            out4 &= "<dl class=""contacts_list"">"
            out4 &= "<dt>Press</dt>"
            For i As Integer = 0 To PressDT.Rows.Count - 1
                out4 &= "<dd>"
                If PressDT.Rows(i)("PDF").ToString.Trim = "1" Then
                    out4 &= "<a target=""_blank"" href=""/dynamic/document/week/news/download/" & PressDT.Rows(i)("News_ID").ToString.Trim & "/" & PressDT.Rows(i)("News_ID").ToString.Trim & ".pdf"">"
                ElseIf PressDT.Rows(i)("PDF").ToString.Trim = "0" And PressDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                    out4 &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(PressDT.Rows(i)("WebUrl").ToString.Trim) & """ >"
                End If
                'out4 &= formatfunctions.AutoFormatText(PressDT.Rows(i)("Headline").ToString.Trim)


                out4 &= "<em class=""NewsLine1"">" & formatfunctions.AutoFormatText(PressDT.Rows(i)("Headline").ToString.Trim) & "</em>"
                out4 &= "<span class=""NewsLine2"">" & formatfunctions.AutoFormatText(PressDT.Rows(i)("Content").ToString.Trim) & "</span>"

                If PressDT.Rows(i)("PDF").ToString.Trim = "1" Or PressDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                    out4 &= "</a>"
                End If

                If PressDT.Rows(i)("VideoLink").ToString.Trim <> "" Then
                    out4 &= "<a href=""" & formatfunctionssimple.AutoFormatText(PressDT.Rows(i)("VideoLink").ToString.Trim) & """ class=""video vimeo"">View video</a>"
                End If


                out4 &= "</dd>"
            Next
            out4 &= "</dl>"
        End If

        'If PressDT.Rows.Count > 0 Then
        '    out4 &= "<dl class=""contacts_list"">"
        '    out4 &= "<dt>PRESS</dt>"
        '    For i As Integer = 0 To PressDT.Rows.Count - 1
        '        out4 &= "<dd>"
        '        out4 &= "<span>" & formatfunctions.AutoFormatText(PressDT.Rows(i)("PublicationTitle").ToString.Trim) & "</span>"
        '        out4 &= "<p><strong>"
        '        If PressDT.Rows(i)("PDF").ToString.Trim = "1" Then
        '            out4 &= "<a href=""/dynamic/document/week/news/download/" & PressDT.Rows(i)("News_ID").ToString.Trim & "/" & PressDT.Rows(i)("News_ID").ToString.Trim & ".pdf"">"
        '        ElseIf PressDT.Rows(i)("PDF").ToString.Trim = "0" And PressDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
        '            out4 &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(PressDT.Rows(i)("WebUrl").ToString.Trim) & """ >"
        '        Else
        '            out4 &= "<a>"
        '        End If
        '        out4 &= formatfunctions.AutoFormatText(PressDT.Rows(i)("Headline").ToString.Trim)
        '        If PressDT.Rows(i)("PDF").ToString.Trim = "1" Or PressDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
        '            out4 &= "</a>"
        '        End If

        '        out4 &= "<em>" & formatfunctionssimple.AutoFormatText(" (" & PressDT.Rows(i)("PostYear").ToString.Trim & ")") & "</em>"
        '        out4 &= "</strong></p>"
        '        out4 &= "<p><strong></strong></p>"
        '        out4 &= "</dd>"
        '    Next
        '    out4 &= "</dl>"
        'End If
        'PUBLICATIONS
        If PublicationsDT.Rows.Count > 0 Then
            out4 &= "<dl class=""contacts_list"">"
            out4 &= "<dt>PUBLICATIONS</dt>"
            For i As Integer = 0 To PublicationsDT.Rows.Count - 1
                out4 &= "<dd>"
                out4 &= "<span>" & formatfunctions.AutoFormatText(PublicationsDT.Rows(i)("PublicationTitle").ToString.Trim) & "</span>"
                out4 &= "<p><strong>"
                If PublicationsDT.Rows(i)("PDF").ToString.Trim = "1" Then
                    out4 &= "<a href=""/dynamic/document/week/news/download/" & PublicationsDT.Rows(i)("News_ID").ToString.Trim & "/" & PublicationsDT.Rows(i)("News_ID").ToString.Trim & ".pdf"">"
                ElseIf PublicationsDT.Rows(i)("PDF").ToString.Trim = "0" And PublicationsDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                    out4 &= "<a target=""_blank"" href=""" & formatfunctionssimple.AutoFormatText(PublicationsDT.Rows(i)("WebUrl").ToString.Trim) & """ >"
                Else
                    out4 &= "<a>"
                End If
                out4 &= formatfunctions.AutoFormatText(PublicationsDT.Rows(i)("Headline").ToString.Trim & " (" & PublicationsDT.Rows(i)("PostYear").ToString.Trim & ")")
                If PublicationsDT.Rows(i)("PDF").ToString.Trim = "1" Or PublicationsDT.Rows(i)("WebUrl").ToString.Trim <> "" Then
                    out4 &= "</a>"
                End If
                out4 &= "</strong></p>"
                out4 &= "<p><strong></strong></p>"
                out4 &= "</dd>"
            Next
            out4 &= "</dl>"
        End If
        'VIDEO
        If VideoDT.Rows.Count > 0 Then
            out4 &= "<dl class=""contacts_list"">"
            out4 &= "<dt>VIDEO</dt>"
            If VideoDT.Rows.Count > 0 Then
                out4 &= "<dd>"
                'out4 &= "<span>Video</span>"
                out4 &= "<p><strong>"
                If VideoDT.Rows(0)("VideoLink").ToString.Trim <> "" Then
                    out4 &= "<a href=""" & formatfunctionssimple.AutoFormatText(VideoDT.Rows(0)("VideoLink").ToString.Trim) & """ class=""vimeo"" >"
                Else
                    out4 &= "<a title="""" >"
                End If
                out4 &= "View</a>"
                'out4 &= formatfunctionssimple.AutoFormatText(VideoDT.Rows(0)("Headline").ToString.Trim) & "</a>"
                out4 &= "</strong></p>"
                out4 &= "<p><strong></strong></p>"
                out4 &= "</dd>"
            End If
            out4 &= "</dl> "
        End If


        'If TestimonialDT.Rows.Count > 0 Then
        '    out4 &= "<dl class=""contacts_list"">"
        '    out4 &= "<dt>related topics</dt>"
        '    If TestimonialDT.Rows.Count > 0 Then
        '        Dim TestiFriendlyLink As String = ""
        '        TestiFriendlyLink = "testimonials" & "#testimonial_" & TestimonialDT.Rows(0)("UserID").ToString.Trim & "_" & formaturl.friendlyurl(TestimonialDT.Rows(0)("FullName").ToString.Trim.ToLower)
        '        out4 &= "<dd>"
        '        out4 &= "<span>Testimonial</span>"
        '        out4 &= "<p><strong>"
        '        out4 &= "<a href=""" & TestiFriendlyLink & """ title="""" >" & formatfunctionssimple.AutoFormatText(formaturl.getwords(TestimonialDT.Rows(0)("Testimonial").ToString.Trim, 35)) & "&#8230;" & "</a>"
        '        out4 &= "</strong></p>"
        '        out4 &= "</dd>"
        '    End If
        '    out4 &= "</dl> "
        'End If


        rel_pages_list1.Text = out4
        breadcrumbs_list.Text = breadcrumbsOut
        project_nav_list.Text = NavLinksOut

    End Sub

End Class
