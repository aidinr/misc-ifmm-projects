﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="white_papers.aspx.vb" Inherits="white_papers" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                <div class="breadcrumbs">
                    <ul>
                        <li><a class="first">insights</a><span> | </span></li>
                        <li><a href="<%=Master.UrlPage%>" class="active">White Papers</a></li>                        
                    </ul>
                </div>
                
                <div class="headline">
                    <h2 class="no_text_transform">White Papers</h2>
                </div>
                
                <ul class="white_papers_list titles_not_links">
                    <asp:Literal runat="server" ID="white_papers_list1"/>                                      
                </ul>
</asp:Content>

