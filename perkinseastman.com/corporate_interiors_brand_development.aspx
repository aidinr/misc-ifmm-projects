﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="corporate_interiors_brand_development.aspx.vb" Inherits="corporate_interiors_brand_development" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="breadcrumbs">
                    <ul>
                        <li><a title="" class="first">Work</a><span> | </span></li>
                        <li><a title="">By PRACTICE AREA</a><span> | </span></li>
                        <li><a href="corporate_interiors" title="">corporate interiors</a><span> | </span></li>
                        <li><a href="<% =Master.UrlPage %>" class="active">Brand Development</a></li>                        
                    </ul>
                </div>
                
                <h3 class="sub_title">Brand Development</h3>
                
                <div class="left_bigger_right_smaller_layout">
                    <div class="left">
                      <div class="cms cmsType_Rich cmsName_Brand_Development_Intro">
                            <%=CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "Brand_Development_Intro")%>
                            </div>                           
                    </div>
                    <div class="right less_padding">
                  
                     <div class="text">
                        <p class="red_text_special cms cmsType_TextMulti cmsName_corporate_interiors_brand_development_testimonial_user_quote"><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "corporate_interiors_brand_development_testimonial_user_quote")%></p>
                        </div>

            <span class="featured_consult">
            <strong class="cms cmsType_TextSingle cmsName_corporate_interiors_brand_development_testimonial_user_name"><%= CMSFunctions.GetTextSimple(Server.MapPath("~") & "cms\data\", "corporate_interiors_brand_development_testimonial_user_name")%></strong>
            <span class="cms cmsType_TextSingle cmsName_corporate_interiors_brand_development_testimonial_user_practice"><%= CMSFunctions.GetTextSimple(Server.MapPath("~") & "cms\data\", "corporate_interiors_brand_development_testimonial_user_practice")%></span>
            <ins class="cms cmsType_TextSingle cmsName_corporate_interiors_brand_development_testimonial_user_title"><%= CMSFunctions.GetTextSimple(Server.MapPath("~") & "cms\data\", "corporate_interiors_brand_development_testimonial_user_title")%></ins>
            
            <img 
                         src="cms/data/Sized/best/220x145/92/ffffff/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "corporate_interiors_brand_development_testimonial_image")%>" 
                         alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "corporate_interiors_brand_development_testimonial_image")%>" 
                         title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "corporate_interiors_brand_development_testimonial_image")%>" 
                         class="cms cmsType_Image cmsName_corporate_interiors_brand_development_testimonial_image" />

            </span>

                    </div>
                </div>
                
                    
<%
    'Dim HasName As String = ""
     'Dim HasSlides As String = ""
    ' If Directory.GetFiles(Server.MapPath("~") & "cms\data\Text\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & "*").Length > 0 Then
    '     HasName = "1"
    ' End If
    ' If Directory.GetFiles(Server.MapPath("~") & "cms\data\Textset\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & "*").Length > 0 Then
    '     HasSlides = "1"
    'End If
    
    Dim ItemsCount As Integer = CMSFunctions.CountTextSingle(CMSFunctions.FormatFriendlyUrl(practiceName,"_") & "_Case_Studies_Item_" & "*" & "_Name")
    
    
%>

<%If ItemsCount > 0 Then%>
 <div class="casestudy_list">
<h3 class="headline_additional">view select case studies below</h3>
<ul class="cmsGroup cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName, "_")%>_Case_Studies">
<!--
<li><a><span class="selectTemplate cms cmsType_TextSingle cmsName_Name">Case Study:</span><strong>Case Study Name</strong></a><a class="selectTemplate cms cmsType_Textset cmsName_List"></a></li>
-->
 <%
     Dim SlideName As String
     Dim SlideLayout As String
     For i As Integer = 1 To ItemsCount
         Dim SlidesCount As Integer = CMSFunctions.CountTextSet(CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List")

         %>

<%
    If SlidesCount > 0 Then
        SlideName = ""
        SlideLayout = ""
        SlideName = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xa" & 0 & ".txt")
        SlideLayout = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Textset\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List" & "\" & CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_List_xb" & 0 & ".txt")
        SlideName = CMSFunctions.FormatFriendlyUrl(SlideName, "_")
    End If
    %>
<li class="cmsGroupItem">
<%  If SlideName <> "" Then%>
<a href="casestudy_<%=SlideLayout %>_<%=CategoryId %>_<%=SlideName %>">
<%End If%>
<span class="selectTemplate cms cmsType_TextSingle cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName,"_") %>_Case_Studies_Item_<%= i %>_Name">Case Study <%= i %>:</span>
<strong><%= CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", CMSFunctions.FormatFriendlyUrl(practiceName, "_") & "_Case_Studies_Item_" & i & "_Name")%></strong>
<%  If SlideName <> "" Then%>
</a>
<%End If%>
<a class="selectTemplate cms cmsType_Textset cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName,"_") %>_Case_Studies_Item_<%= i %>_List"></a>
</li>
<%Next%>
</ul>
</div>         
<%Else%> 
<div class="noCase cmsGroup cmsName_<%=CMSFunctions.FormatFriendlyUrl(practiceName, "_")%>_Case_Studies">
<!--
<div class="casestudy_list"><ul><li><a href="#"><span class="selectTemplate cms cmsType_TextSingle cmsName_Name">Case Study:</span><strong>Example Case Study</strong></a><a class="selectTemplate cms cmsType_Textset cmsName_List"></a></li></ul></div>
-->
</div>
<%End If%>

               
</asp:Content>
