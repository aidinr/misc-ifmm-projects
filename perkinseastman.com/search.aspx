﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="search.aspx.vb" Inherits="search" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="assets/js/search.js" type="text/javascript"></script>
                <div class="breadcrumbs">
                    <ul>
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_Home" href="/"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Home")%></a><span> | </span></li>
                        <li><a class="active" href="<%=Master.UrlPage%>"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Search_Results")%></a></li>
                    </ul>
                </div>
                <h3 class="sub_title cmsMultiLanguage cms cmsType_TextSingle cmsName_Search_Results"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Search_Results")%></h3>
                <div class="left_bigger_right_smaller_layout">
                    <div class="left">
                        
                        <img 
                         src="cms/data/Sized/best/690x260/92/f4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "search_image")%>" 
                         alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "search_image")%>" 
                         title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "search_image")%>" 
                         class="<%="cms cmsType_Image cmsName_search_image"%>" />

                
                <div class="left_bigger_right_smaller_layout">
                    <div class="left">
                        <form method="get" action="search" class="search_form" id="searchForm">
                            <input type="text" name="q" id="s" value="Search Term"/>
                            <button type="submit">GO</button>
                        </form>                        

<div class="text">
                     <h2 id="searchMsg"></h2>
                     <div id="resultsDiv"></div>  
</div>
                        
                    </div>
                </div>

                    </div>
                    <div class="right no_border">
                             <div>
                            
<p class="cms cmsType_TextMulti cmsName_Search_Right_Heading"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Search_Right_Heading")%></p>
<p class="cms cmsType_TextMulti cmsName_Search_Right_Content"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Search_Right_Content")%></p>


                            </div>      

                </div>
               </div>

</asp:Content>



