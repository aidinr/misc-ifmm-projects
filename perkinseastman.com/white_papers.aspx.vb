﻿Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Partial Class white_papers
    Inherits System.Web.UI.Page

    Protected Sub white_papers_list1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles white_papers_list1.Load
        Dim sql As String = ""
        sql &= "Select "
        sql &= "n.News_Id, "
        sql &= "n.Headline, "
        sql &= "n.Content, "
        sql &= "n.Picture, "
        sql &= "n2.Item_Value as VideoLink, "
        sql &= "n.PDF, "
        sql &= "COALESCE( 'by ' + nullif(ltrim(v.Item_Value),''),'') as Author , "
        sql &= "DATENAME(mm,n.Post_Date) + ' ' + DATENAME(day,n.Post_Date) + ', ' + DATENAME(year, n.Post_Date) DateLoc, "
        sql &= "v.Item_Value Employee, "
        sql &= "v2.Item_Value Title, "
        sql &= "v3.Item_Value video "
        sql &= "From IPM_NEWS n "
        sql &= "Left Join IPM_NEWS_FIELD_VALUE v ON (n.News_id = v.News_id AND v.Item_ID = 3409254) "
        sql &= "Left Join  IPM_NEWS_FIELD_VALUE v2 on ( n.News_Id = v2.NEWS_ID and v2.Item_ID = 3409255 ) "
        sql &= "Left Join IPM_NEWS_FIELD_VALUE v3 on (n.News_Id = v3.NEWS_ID and v3.Item_ID = 30052966)  "
        sql &= "left join IPM_NEWS_FIELD_VALUE n2 on N.News_Id = n2.NEWS_ID and n2.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_VIDEOLINK') "
        sql &= "left join IPM_NEWS_FIELD_VALUE n3 on N.News_Id = n3.NEWS_ID and n3.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWS_FUTURED') "
        sql &= "Where(n.Show = 1 And n.Post_Date < GETDATE() And n.Pull_Date > GETDATE() And n.Type = 10 AND n3.Item_Value = '1' ) "
        sql &= "Order By n.Post_Date DESC"
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)

        Dim out As String = ""
        For i As Integer = 0 To DT1.Rows.Count - 1
            out &= " <li> "
            out &= "<div class=""photo_holder"">"

            If DT1.Rows(i)("PDF").ToString.Trim = "1" Then
                out &= "<a href=""/dynamic/document/week/asset/download/" & DT1.Rows(i)("News_ID").ToString.Trim & "/" & DT1.Rows(i)("News_ID").ToString.Trim & ".pdf"" title="""">"
            Else
                out &= "<a title="""" >"
            End If
            out &= "<img src=""" & "/dynamic/image/week/news/best/220x145/92/ffffff/NorthWest/" & DT1.Rows(i)("News_ID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
            out &= "</a>"
            out &= "</div>"
            out &= "<div class=""info_holder"">"
            out &= "<h1 class=""whitepaperHeading"">" & formatfunctions.AutoFormatText(DT1.Rows(i)("Headline").ToString.Trim) & "</h1>"
            out &= "<span>" & formatfunctions.AutoFormatText(DT1.Rows(i)("Author").ToString.Trim) & "</span>"
            out &= "<em>" & formatfunctions.AutoFormatText(DT1.Rows(i)("DateLoc").ToString.Trim) & "</em>"
            out &= "<div class=""text"" > "
            out &= "<p>" & formatfunctions.AutoFormatText(DT1.Rows(i)("Content").ToString.Trim) & "</p>"
            If DT1.Rows(i)("PDF").ToString.Trim = "1" Then
                out &= "<a href=""/dynamic/document/week/asset/download/" & DT1.Rows(i)("News_ID").ToString.Trim & "/" & DT1.Rows(i)("News_ID").ToString.Trim & ".pdf"" title="""">Download complete White Paper here </a>"
            End If
            If DT1.Rows(i)("VideoLink").ToString.Trim <> "" Then
                out &= "<a href=""" & formatfunctionssimple.AutoFormatText(DT1.Rows(i)("VideoLink").ToString.Trim) & """ class=""video vimeo"">View video</a>"
            End If
            out &= "</div> "
            out &= "</div>"
            out &= "</li>"
            out &= vbNewLine
        Next
        white_papers_list1.Text = out
    End Sub
    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        writer.Flush()
        writer.Write(html)
        If Master.OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
        End If
    End Sub
End Class
