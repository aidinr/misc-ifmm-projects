﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mp1.master" AutoEventWireup="false" CodeFile="testimonials.aspx.vb" Inherits="testimonials" %>
<%@ MasterType  virtualPath="~/mp1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                <div class="breadcrumbs">
                    <ul>
                        <li><a class="first cmsMultiLanguage cms cmsType_TextSingle cmsName_Approach"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Approach")%></a><span> | </span></li>
                        <li><a class="cmsMultiLanguage cms cmsType_TextSingle cmsName_Testimonials active" href="<%=Master.UrlPage%>"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Testimonials")%></a></li>
                    </ul>
                </div>
            <div class="two_columns">
                    <div class="left_smaller">
                        <h3 class="sub_title"><%=CMSFunctions.GetTextFormat(Server.MapPath("~") & "cms\data\", "Testimonials")%></h3>
                        <div class="text mt27">
                        <div class="cmsMultiLanguage cms cmsType_Rich cmsName_testimonials_left">
                            <%= CMSFunctions.GetRichFormat(Server.MapPath("~") & "cms\data\", "testimonials_left")%>
                        </div>
                        </div>
                    </div>
                    
                    <div class="right_bigger">
                        
                        <!-- Every 4th elements needs to have class="last_in_Row" -->
                        <ul class="main_list testimonial_list extra_top_margin testimonials">
                        <asp:Literal runat="server" ID="testimonial_list1"/>  
                        </ul> 
                        
                        <!--
                            Testimonial POPUPS INFO:
                            the popup div ID needs to be the value of the associate a HREF
                            ex: <div id="pop_10"> and <a href="#pop_10">
                        -->
                        
                        <div class="testimonial_popups">
                            <asp:Literal runat="server" ID="testimonial__popups_list1"/>
                        </div>
                    </div>
                </div>

</asp:Content>