﻿Imports System.Net.Mail
Imports System.Data
Imports System.Data.SqlClient

Partial Class Email
    Inherits System.Web.UI.Page



    Sub btnSubmit_click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click


        Dim senderName As String = Request.Form("sender_name")
        Dim recipientEmail As String = Request.Form("recipient_email")
        Dim message As String = Request.Form("message")
        Dim pageID As String = Request.Form("id")

        Dim theMessage As String = ""

        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)

        Dim drArray() As DataRow
        Dim dr As DataRow
        drArray = ds.Tables("Units").Select("Unit = '" & pageID.Trim & "'")
        dr = drArray(0)
        Dim iDAMPATHPrint As String = Replace(Session("WSRetreiveAsset") & "size=0&width=750&height=450&qfactor=25&crop=2&id=", "&", "&amp;")

        theMessage = theMessage & "<style type=""text/css"">body {	padding:0;margin:0;background: white;font-family: arial;font-size: 14px;color: #666666;}" & vbCrLf
        theMessage = theMessage & "img {border:0;}" & vbCrLf
        theMessage = theMessage & ".print_floorplan { display:block; margin: 0; font-family: helvetica, arial; font-size: 14px;	line-height: 18px;	color: #000000;	width:800px;}" & vbCrLf
        theMessage = theMessage & ".print_details td {border-bottom: 1px solid #666666;padding-bottom: 6px;padding-top: 6px;padding-right:30px;}" & vbCrLf
        theMessage = theMessage & ".floorplan{ display: block;}" & vbCrLf
        theMessage = theMessage & ".print_floorplan table { display:block;}" & vbCrLf
        theMessage = theMessage & "</style>" & vbCrLf

        theMessage = theMessage & "Message from " & senderName & ": " & message & "<br /><br />"

        theMessage = theMessage & "<div class=""print_floorplan"">"
        '  theMessage = theMessage & "<img src=""http://hwcdn.net/e7i9s6j8/cds/kettler/print_header.jpg"" />"
        theMessage = theMessage & "<div class=""floorplan"">"
        theMessage = theMessage & "<img src=""" & iDAMPATHPrint.ToString() & dr("Model_AssetID").ToString() & """ alt=""" & dr("Unit").ToString & """/>"
        theMessage = theMessage & "</div>"
        theMessage = theMessage & "<table style=""margin-top:40px;margin-left:30px;width:350px;"" cellpadding=""0"" cellspacing=""0"" class=""print_container"">"
        theMessage = theMessage & "<tr>"
        theMessage = theMessage & "<td valign=""top"" width=""450"">"
        'theMessage = theMessage & "<table cellpadding=""0"" cellspacing=""0"" width=""100%"">"
        'theMessage = theMessage & "<tr>"
        'theMessage = theMessage & "<td valign=""top"" width=""220"" align=""left""><img src=""http://hwcdn.net/e7i9s6j8/cds/kettler/thumb_1.jpg"" /></td>"
        'theMessage = theMessage & "<td valign=""top"" align=""left""><img src=""http://hwcdn.net/e7i9s6j8/cds/kettler/thumb_2.jpg"" /></td>"
        'theMessage = theMessage & "</tr>"
        'theMessage = theMessage & "</table>"
        theMessage = theMessage & "</td>"
        theMessage = theMessage & "<td valign=""top"" width=""300"">"
        theMessage = theMessage & "<table cellpadding=""0"" cellspacing=""0"" class=""print_details"" width=""300"">"
        theMessage = theMessage & "<tr>"
        theMessage = theMessage & "<td colspan=""2"" style=""padding-top:0;"">"
        theMessage = theMessage & "<big><b>Apartment " & dr("Unit").ToString & "</b></big>"
        theMessage = theMessage & "</td>"
        theMessage = theMessage & "</tr>"
        theMessage = theMessage & "<tr>"
        theMessage = theMessage & "<td align=""left""  width=""130"">Type:</td>"
        theMessage = theMessage & "<td align=""left"">" & dr("Type").ToString & "</td>"
        theMessage = theMessage & "</tr>"
        theMessage = theMessage & "<tr>"
        theMessage = theMessage & "<td align=""left"" width=""130"">Bathroom(s):</td>"
        theMessage = theMessage & "<td align=""left"">" & dr("Bathroom").ToString & "</td>"
        theMessage = theMessage & "</tr>"
        theMessage = theMessage & "<tr>"
        theMessage = theMessage & "<td align=""left"" width=""130"">Square Feet:</td>"
        theMessage = theMessage & "<td align=""left"">" & dr("Area").ToString & "</td>"
        theMessage = theMessage & "</tr>"
        theMessage = theMessage & "<tr>"
        theMessage = theMessage & "<td align=""left"" width=""130"">Floor Level:</td>"
        theMessage = theMessage & "<td align=""left"">" & dr("Floor").ToString & "</td>"
        theMessage = theMessage & "</tr>"
        theMessage = theMessage & "<tr>"
        theMessage = theMessage & "<td align=""left"" width=""130"">View(s):</td>"
        theMessage = theMessage & "<td align=""left"">" & dr("Amenities").ToString & "</td>"
        theMessage = theMessage & "</tr>"
        theMessage = theMessage & "<tr>"
        theMessage = theMessage & "<td align=""left"" width=""130"">Price:</td>"
        theMessage = theMessage & "<td align=""left"">" & dr("Price_String").ToString & "</td>"
        theMessage = theMessage & "</tr>"
        theMessage = theMessage & "<tr>"
        theMessage = theMessage & "<td align=""left"" width=""130"">Availability:</td>"
        theMessage = theMessage & "<td align=""left"">" & dr("Available").ToString & "</td>"
        theMessage = theMessage & "</tr>"
        theMessage = theMessage & "</table>"
        theMessage = theMessage & "</td>"
        theMessage = theMessage & "</tr>"
        theMessage = theMessage & "<tr>"
        theMessage = theMessage & "<td colspan=""2"">"
        theMessage = theMessage & "<br />"
        'theMessage = theMessage & "1330 S. Fair Street, Arlington, VA &nbsp;  22204<br/>"
        'theMessage = theMessage & "<b>866.833.7158</b><br />"
        'theMessage = theMessage & "<b>www.metmillennium.com</b>"
        theMessage = theMessage & "</td>"
        theMessage = theMessage & "</tr>"
        theMessage = theMessage & "</table>"




        Dim mail As MailMessage = New MailMessage
        mail.From = New MailAddress("info@kettler.com")
        mail.Subject = senderName & " has sent you a Kettler floorplan"
        mail.Body = theMessage
        mail.IsBodyHtml = True
        Dim smtp As New SmtpClient()


        If (isEmail(Trim(recipientEmail)) = True) Then
            Try
                mail.To.Clear()
                mail.To.Add(Trim(recipientEmail))
                smtp.Send(mail)
            Catch ex As Exception
                labelError.Text = "A technical error occured. Please try again later."
            End Try

        Else
            labelError.Text = "Invalid e-mail address. Please try again."
        End If

        If labelError.Text = "" Then
            LabelSuccess.Text = "E-mail sent. Thank you for using the Kettler e-kiosk!"
        End If

    End Sub

    Function isEmail(ByVal inputEmail As String) As Boolean

        inputEmail = inputEmail.ToString()
        Dim strRegex As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
        Dim re As Regex = New Regex(strRegex)
        If (re.IsMatch(inputEmail)) Then
            Return True
        Else
            Return False
        End If

    End Function

    Protected Sub Email_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'divSubmit.Attributes.Add("onclick", String.Format("javascript:__doPostBack('{0}','')", btnSubmit.UniqueID))


    End Sub

End Class