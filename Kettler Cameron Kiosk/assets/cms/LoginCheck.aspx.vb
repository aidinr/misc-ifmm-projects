Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Security.Cryptography

Partial Class _logincheck
    Inherits System.Web.UI.Page

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim CookieString As String
        Dim CookieDate As String
        Dim username As String = ""
        Dim password As String = ""
        If Not Request.Form("username") Is Nothing Then
            username = Request.Form("username")
        End If
        If Not Request.Form("password") Is Nothing Then
            password = Request.Form("password")
        End If
        Dim out As String = "1"
        If username = "" Or password = "" Then
            out = 2
        Else
            Dim sql As String = "select * from IPM_USER where Active = 'Y' and Login = '" & username & "' and Password = '" & password & "'"
            Dim DT1 As New DataTable("FProjects")
            DT1 = mmfunctions.GetDataTable(sql)
            If DT1.Rows.Count > 0 Then
                'CookieDate = Format(DateAndTime.Now, "ddMMyyyy")
                'CookieString = username & CookieDate
                'CookieString = GenerateHash(CookieString)
                'Response.Cookies("SiteEdit").Value = username & ":" & CookieString
                'Response.Cookies("SiteEdit").Expires = Date.Now

                Session("LoggedUser") = username

                CookieDate = Format(DateAndTime.Now, "ddMMyyyy")
                CookieString = username & CookieDate
                CookieString = GenerateHash(CookieString)

                'Create a new cookie, passing the name into the constructor
                Dim cookie As New HttpCookie("SiteEdit")
                'Set the cookies value
                cookie.Value = username & ":" & CookieString
                'Set the cookie to expire in 1 minute
                Dim dtNow As DateTime = DateTime.Now
                Dim tsMinute As New TimeSpan(24, 0, 0, 0)
                cookie.Expires = dtNow.Add(tsMinute)
                'Add the cookie
                Response.Cookies.Add(cookie)


            Else
                out = 2
            End If
        End If

        Response.Clear()
        Response.Write(out)
        Response.Flush()
        Response.End()

    End Sub


    Private Function GenerateHash(ByVal SourceText As String) As String
        'Create an encoding object to ensure the encoding standard for the source text
        Dim Ue As New UnicodeEncoding()
        'Retrieve a byte array based on the source text
        Dim ByteSourceText() As Byte = Ue.GetBytes(SourceText)
        'Instantiate an MD5 Provider object
        Dim Md5 As New MD5CryptoServiceProvider()
        'Compute the hash value from the source
        Dim ByteHash() As Byte = Md5.ComputeHash(ByteSourceText)
        'And convert it to String format for return
        Return Convert.ToBase64String(ByteHash)
    End Function
End Class