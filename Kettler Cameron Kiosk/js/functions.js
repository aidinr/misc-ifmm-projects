var moving = 0;

/*bind triggers*/
jQuery(document).ready(function() {
	//clearTimeout(t);

$('div.search_menu_button_container div div:not([class])').click( function(){
		$("#search_apartment_dropdown, #search_price_dropdown, #search_area_dropdown, #search_floor_dropdown, #search_available_dropdown").hide()
});


$(document).on('click', '.menu_item, ', function(){
        if(moving==1) return false;
	var hash = this.id;
	hash = "#" + hash.replace("menu_","");
	scrollToHash(hash);
	$(".menu_item").removeClass('menu_item_active');
	$(this).addClass('menu_item_active');
	
	return false;
});

$(document).on('click', '.search_menu_button', function(){
	var theID = $(this).attr("id");
	
	if(theID == "search_apartment_button") {
		$("#search_apartment_dropdown").slideToggle(400);
		$("#search_price_dropdown, #search_area_dropdown, #search_floor_dropdown, #search_available_dropdown").fadeOut(300);

	}
	
	
		
	$(".search_menu_button").removeClass('search_menu_button_active');
	$(".search_menu_button_narrow").removeClass('search_menu_button_narrow_active');
	$(this).addClass('search_menu_button_active');
	return false;
		
});

$(document).on('click', '.search_menu_button_narrow', function(){
	var theID = $(this).attr("id");	

	if(theID == "search_price_button") {
		$("#search_price_dropdown").slideToggle(400);
		$("#search_apartment_dropdown, #search_area_dropdown, #search_floor_dropdown, #search_available_dropdown").fadeOut(300);
	}
	else if(theID == "search_area_button") {
		$("#search_area_dropdown").slideToggle(400);
		$("#search_apartment_dropdown, #search_price_dropdown, #search_floor_dropdown, #search_available_dropdown").fadeOut(300);
	}
	else if(theID == "search_floor_button") {
		$("#search_floor_dropdown").slideToggle(400);
		$("#search_apartment_dropdown, #search_area_dropdown, #search_price_dropdown, #search_available_dropdown").fadeOut(300);

	}
	else if(theID == "search_available_button") {
		$("#search_available_dropdown").slideToggle(400);
		$("#search_apartment_dropdown, #search_price_dropdown, #search_area_dropdown, #search_floor_dropdown").fadeOut(300);
	}

	
	$(".search_menu_button").removeClass('search_menu_button_active')
	$(".search_menu_button_narrow").removeClass('search_menu_button_narrow_active');
	$(this).addClass('search_menu_button_narrow_active');
	return false;
		
});





$(document).on('click','.search_result_row_container', function() {
//	scrollToHash("#unit_floorplan");	
		
});

$(document).on('click','.gallery_button', function() {
		if($(this).hasClass('gallery_button_active')) {
			$('.gallery_button').removeClass('gallery_button_active');	
		}
		else {
			$('.gallery_button').removeClass('gallery_button_active');
			$(this).addClass('gallery_button_active');
		}
			
});
$(document).on('click','.back_to_search_button', function() {
	scrollToHash("#live_here_search_results");	
		
});

/*floorplate selectors*/
$(document).on('click','.find_button', function() {
		scrollToHash('#live_here_search_results');
});

$(document).on('click','.floor_selector', function() {
		$('.floor_selector').removeClass('floor_selector_active');
		$(this).addClass('floor_selector_active');
});

$(document).on('click','.jcarousel-availability', function() {
		theID = $(this).attr("id").replace("unit_","");
		//alert("You clicked on unit " + theID);
        //doFilter("unit", "");
        if (theID.length == 3) {
           loadApt('unit','0' + theID);
        }
        else {
            loadApt('unit', theID);
        }
		scrollToHash("#unit_floorplan");
});
});

jQuery(document).ready(function() {


$('.search_result').addClass('content_page');


    jQuery('#floorplate_selector').jcarousel({

        initCallback:   mycarousel_initCallback,
        reloadCallback: mycarousel_reloadCallback,

        buttonNextCallback:   mycarousel_buttonNextCallback,
        buttonPrevCallback:   mycarousel_buttonPrevCallback,

        itemFirstInCallback:  mycarousel_itemFirstInCallback,
        itemFirstOutCallback: mycarousel_itemFirstOutCallback,
        itemLastInCallback:   mycarousel_itemLastInCallback,
        itemLastOutCallback:  mycarousel_itemLastOutCallback,
        itemVisibleInCallback: {
            onBeforeAnimation: mycarousel_itemVisibleInCallbackBeforeAnimation,
            onAfterAnimation:  mycarousel_itemVisibleInCallbackAfterAnimation
        },
        itemVisibleOutCallback: {
            onBeforeAnimation: mycarousel_itemVisibleOutCallbackBeforeAnimation,
            onAfterAnimation:  mycarousel_itemVisibleOutCallbackAfterAnimation
        }
    });
});

/**
 * This is the callback function which receives notification
 * about the state of the next button.
 */
function mycarousel_buttonNextCallback(carousel, button, enabled) {
    //display('Next button is now ' + (enabled ? 'enabled' : 'disabled'));
};

/**
 * This is the callback function which receives notification
 * about the state of the prev button.
 */
function mycarousel_buttonPrevCallback(carousel, button, enabled) {
    //display('Prev button is now ' + (enabled ? 'enabled' : 'disabled'));
};

/**
 * This is the callback function which receives notification
 * right after initialisation of the carousel
 */
function mycarousel_initCallback(carousel, state) {
    $('.floor_selector').bind('click', function() {
    	var theID = parseInt($(this).attr("id").replace("floor_","")) + 1;
    	
        carousel.scroll(theID,true);
        return false;
    });

};

/**
 * This is the callback function which receives notification
 * right after reloading of the carousel
 */
function mycarousel_reloadCallback(carousel) {
    //display('Carousel reloaded');
};

/**
 * This is the callback function which receives notification
 * when an item becomes the first one in the visible range.
 */
function mycarousel_itemFirstInCallback(carousel, item, idx, state) {
    //display('Item #' + idx + ' is now the first item');
};

/**
 * This is the callback function which receives notification
 * when an item is no longer the first one in the visible range.
 */
function mycarousel_itemFirstOutCallback(carousel, item, idx, state) {
    //display('Item #' + idx + ' is no longer the first item');
};

/**
 * This is the callback function which receives notification
 * when an item becomes the first one in the visible range.
 */
function mycarousel_itemLastInCallback(carousel, item, idx, state) {
    //display('Item #' + idx + ' is now the last item');
};

/**
 * This is the callback function which receives notification
 * when an item is no longer the first one in the visible range.
 */
function mycarousel_itemLastOutCallback(carousel, item, idx, state) {
    //display('Item #' + idx + ' is no longer the last item');
};

/**
 * This is the callback function which receives notification
 * when an item becomes the first one in the visible range.
 * Triggered before animation.
 */
function mycarousel_itemVisibleInCallbackBeforeAnimation(carousel, item, idx, state) {
    // No animation on first load of the carousel
    if (state == 'init')
        return;
    jQuery('img', item).fadeIn('slow');
};

/**
 * This is the callback function which receives notification
 * when an item becomes the first one in the visible range.
 * Triggered after animation.
 */
function mycarousel_itemVisibleInCallbackAfterAnimation(carousel, item, idx, state) {
    //display('Item #' + idx + ' is now visible');
    
    var theFloor = parseInt(idx);
    var theFloorString = "";
    if(theFloor == 1) {
    	theFloorString = theFloor + "st";    
    }
    else if(theFloor == 2) {
    	theFloorString = theFloor + "nd";    
    }
    else if(theFloor == 3) {
    	theFloorString = theFloor + "rd";
    }
    else {
    	theFloorString = theFloor + "th";
    }
    
    $('#floorplate_selector_title').text('Select Floor Plan : ' + theFloorString + ' floor');
    /*this feature has been removed
    if(theFloor == 0) {
    	    $('.jcarousel-skin-floorplate .jcarousel-next').css('display','none');
    }
    else if(theFloor != 1) {
    	        $('.jcarousel-skin-floorplate .jcarousel-prev').text('Floor ' + (theFloor - 1));
    	        $('.jcarousel-skin-floorplate .jcarousel-next').text('Floor ' + (theFloor + 1));
    	        
    	        $('.jcarousel-skin-floorplate .jcarousel-next').css('display','block');
    	        $('.jcarousel-skin-floorplate .jcarousel-prev').css('display','block');
    }
    else {
    	        $('.jcarousel-skin-floorplate .jcarousel-prev').text('Main Menu');
    	        $('.jcarousel-skin-floorplate .jcarousel-next').text('Floor ' + (theFloor + 1));
    	        
    	        $('.jcarousel-skin-floorplate .jcarousel-prev').css('display','block');
    	        $('.jcarousel-skin-floorplate .jcarousel-next').css('display','block');
    	        
    	        
    }
    */
    
    

    
};

/**
 * This is the callback function which receives notification
 * when an item is no longer the first one in the visible range.
 * Triggered before animation.
 */
function mycarousel_itemVisibleOutCallbackBeforeAnimation(carousel, item, idx, state) {
    jQuery('img', item).fadeOut('slow');
};

/**
 * This is the callback function which receives notification
 * when an item is no longer the first one in the visible range.
 * Triggered after animation.
 */
function mycarousel_itemVisibleOutCallbackAfterAnimation(carousel, item, idx, state) {
    //display('Item #' + idx + ' is no longer visible');
};

$(document).on('click','#resident_services_image_button_1', function() {
		expand_resident_services_image(1);
});
$(document).on('click','#resident_services_image_button_2', function() {
		expand_resident_services_image(2);
});
$(document).on('click','#resident_services_image_button_3', function() {
		expand_resident_services_image(3);
});


function expand_resident_services_image(i) {
	var duration = 800;
	var easing = 'easeOutQuad';
	
	$('.resident_services_image img').css("display","none");
	if(i == 1) {
		
		$('#resident_services_image_1').animate({'width': '331px'},duration,easing,function(){
				$('.resident_services_text').hide();
				$('#resident_services_text_1').show();

		});
		$('#resident_services_image_2').animate({'width': '72px'},duration,easing,function(){
				$('#resident_services_image_button_2').css("display","block");
		});
		$('#resident_services_image_3').animate({'width': '72px'},duration,easing,function(){
				$('#resident_services_image_button_3').css("display","block");
		});
	}
	else if (i == 2) {
		$('#resident_services_image_1').animate({'width': '72px'},duration,easing,function(){
				$('#resident_services_image_button_1').css("display","block");
		});
		$('#resident_services_image_2').animate({'width': '331px'},duration,easing,function(){
				$('.resident_services_text').hide();
				$('#resident_services_text_2').show();
		});
		$('#resident_services_image_3').animate({'width': '72px'},duration,easing,function(){
				$('#resident_services_image_button_3').css("display","block");
		});
	}
	else if (i == 3) {		
		$('#resident_services_image_1').animate({'width': '72px'},duration,easing,function(){
				$('#resident_services_image_button_1').css("display","block");
		});
		$('#resident_services_image_2').animate({'width': '72px'},duration,easing,function(){
				$('#resident_services_image_button_2').css("display","block");
		});
		$('#resident_services_image_3').animate({'width': '331px'},duration,easing,function(){
				$('.resident_services_text').hide();
				$('#resident_services_text_3').show();
		});
	}
}

var currentHash = "";

jQuery(document).ready(function() {
	var currentHash = window.location.hash;
	if(currentHash != "") {
		scrollToHash(currentHash);
	}
	else {
		scrollToHash("#home_page");
	}
});
/*
$(window).resize(function() {
	scrollToHash(currentHash);
});
*/
/*featured unit jcarousel hack since it does not auto resize height*/


function scrollToHash(hash) {
	var scrollOffset = 0;
	var scrollHomeOffset = 0;
	var width = parseInt($(window).width());
	if(width < 1131 ) {
	    scrollOffset = -13;
	    scrollHomeOffset = -30
	}
	else {
	    scrollOffset = -23;
	    scrollHomeOffset = -60
	}
		
	if(hash == "#home_page")
	
	{
        scrollOffset = scrollHomeOffset;
		$(".menu_item").removeClass('menu_item_active');
		$(".menu_video ").css("display","block");
		$(".menu_favorite_cart").css("display","none");
	}
	else if(hash == "#live_here_search_results" || hash == "#live_here" || hash == "#unit_floorplan")
	{
		//scrollOffset = -40;
		$(".menu_video ").css("display","none");
		$(".menu_favorite_cart").css("display","block");
	}
	else {
		$(".menu_video ").css("display","block");
		$(".menu_favorite_cart").css("display","none");	
	}
	

$('.content_page').css('z-index','1');
  if(moving==1) return false;
  moving = 1;

  $(hash).css('z-index','2').css('visibility','visible').css('opacity',0).css('top','100px');
  $('.content_page').not(hash).animate({
    opacity: 0
  }, 500, function() {
    $('.content_page').not(hash).css('visibility','hidden');
  });

  $(hash).animate({
    opacity: 1,
    top: '30px'
  }, 600, function() {
    moving = 0;

  $('.content').scrollTo(hash,600,{
	easing:'easeOutQuint',
	offset:{left:0,top:scrollOffset},
	/*onAfter: function(){ window.location.hash = hash; }*/
  });


});
















  currentHash = hash;

};

function scrollToTop() {
	$('.search_result_listing_container').scrollTo(0,500,{easing:'easeOutQuint'});
};
function scrollUp() {
	$('.search_result_listing_container').scrollTo('-=315',500,{easing:'easeOutQuint'});
};
function scrollDown() {
	$('.search_result_listing_container').scrollTo('+=315',500,{easing:'easeOutQuint'});
};
function scrollToBottom() {
	$('.search_result_listing_container').scrollTo('.search_result_row_container:last',500,{easing:'easeOutQuint'});
};

jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel();

    jQuery('#mycarousel3').jcarousel();
    jQuery('#mycarousel5').jcarousel();
});

/*deprecated
function expandCarousel() {
	$('.content').css('width','1366px');
	$('.content').css('margin-left','0');
	$('.menu_bar').css('display','none');
	
 $('.jcarousel-skin-floorplate .jcarousel-item').addClass('jcarousel-item-ipad');
 $('.jcarousel-skin-floorplate .jcarousel-item img').addClass('jcarousel-item-ipad');
 $('.jcarousel-skin-floorplate .jcarousel-clip-horizontal').addClass('jcarousel-clip-horizontal-ipad');
 $('.jcarousel-skin-floorplate .jcarousel-container-horizontal').addClass('jcarousel-container-horizontal-ipad');
 $('.jcarousel-skin-floorplate .jcarousel-container').addClass('jcarousel-container-ipad');
 
};
*/

/*read Facebook's RSS 2.0 feed to populate Community News*/
$(document).ready(function() {


jQuery(function() {
    jQuery.getFeed({
        url: 'FacebookFeed.aspx',
        dataType: 'xml',
        success: function(feed) {
            var theTitle = "Community News";
            var weekday=new Array(7);
		weekday[0]="Sun";
		weekday[1]="Mon";
		weekday[2]="Tue";
		weekday[3]="Wed";
		weekday[4]="Thu";
		weekday[5]="Fri";
		weekday[6]="Sat";
	    var month=new Array();
		month[0]="Jan";
		month[1]="Feb";
		month[2]="Mar";
		month[3]="Apr";
		month[4]="May";
		month[5]="Jun";
		month[6]="Jul";
		month[7]="Aug";
		month[8]="Sep";
		month[9]="Oct";
		month[10]="Nov";
		month[11]="Dec";	
	    var html = "";
            
            for(var i = 0; i < feed.items.length && i < 8 ; i++) {
            	   var theDay = "";
		   var theDate = "";
		   var theMonth = "";
		   var theYear = "";
		   var theHour = "";
		   var theMinute = "";
		   var theTime = "";
		   var theDateAndTime = "";
                   var item = feed.items[i];
                   
                   var d = new Date(Date.parse(item.updated));
                   theDay = weekday[d.getDay()];
                   theDate = d.getDate();
                   theYear = d.getFullYear();
                   theHour = parseInt(d.getHours());
                   theMinute = d.getMinutes();
                   theMonth = month[d.getMonth()];
                   
                   if(theMinute.length == 1) {
                   	theMinute = '0' + theMinute;	   
                   }
                   
                   if(theHour >= 13) {
                   	   theTime = theHour - 12 + ':' + theMinute+ ' PM';
                   }
                   else {
                   	   theTime = theHour + ':' + theMinute+ ' PM';
                   }
                   
                   theDateAndTime = theDay +', ' + theDate + ' ' + theMonth + ' ' + theYear + ' ' + theTime;
                
                
                
                html += '<li>'
                + '<div><b>' + theTitle + '</b><br />'
                + '<span class="orange">' + theDateAndTime+ '</span>'
                + '<br />'
                + item.title
                + '</div>'
                + '</li>';
            }
            jQuery('#mycarousel_2').append(html);
            jQuery('#mycarousel_2').jcarousel();
        }
    });
});
		
});
/*initialize favorite lightbox*/

$(document).ready(function() {
  //$('#cart_dialog').jqm();
  CallBackSearchCarousel.callback(1);
});

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

$(document).ready(function() {

 if (readCookie('SiteEdit') || window.location.search.substr(0,5)=='?edit') {
  var script = document.createElement( 'script' );
  script.type = 'text/javascript';
  script.src = '/assets/cms/cms.js';
  $('head').append(script);
  var css = document.createElement( 'link' );
  css.setAttribute("rel", "stylesheet");
  css.setAttribute("type", "text/css");
  css.setAttribute("href", '/assets/cms/cms.css');
  $('head').append(css);
 }
$('#resident_services_image_button_1.cms, #resident_services_image_button_2.cms, #resident_services_image_button_3.cms').removeClass('cms');
});


function printMy () {
  $('#myStuff').html(  $('.favorites_container').html()  );
  $('#myStuff').removeAttr('style');
  window.print();
}

function printGetReady() {
  $('#myStuff').html(  $('.favorites_container').html()  );
  $('#myStuff').removeAttr('style');
}