﻿
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class ascx_Leesburg_Services
    Inherits System.Web.UI.UserControl
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)

        PopulateServices(ds)
    End Sub
    Public Sub PopulateServices(ByVal theDataSet As DataSet)

        Dim iDAMPATHFull As String = Session("WSRetreiveAsset") & "size=0&id="

        For Each Row As DataRow In theDataSet.Tables("ServicesAssets").Rows
            Select Case Row("name")
                Case "services_left"
                    imgServicesLeft.ImageUrl = iDAMPATHFull & Row("asset_id")
                    imgServicesLeft.AlternateText = Row("description")
                Case "services_center"
                    imgServicesCenter.ImageUrl = iDAMPATHFull & Row("asset_id")
                    imgServicesCenter.AlternateText = Row("description")
                Case "services_right"
                    imgServicesRight.ImageUrl = iDAMPATHFull & Row("asset_id")
                    imgServicesRight.AlternateText = Row("description")
            End Select
        Next

        'lblLiveHere.Text = theDataSet.Tables("AmenitiesDescription").Rows(0)("descriptionmedium")
        Dim ServicesArray() As String = theDataSet.Tables("ServicesDescription").Rows(0)("descriptionmedium").ToString.Split(vbCrLf)
        repeaterServices.DataSource = ServicesArray
        repeaterServices.DataBind()
        lblServices.Text = theDataSet.Tables("ServicesDescription").Rows(0)("description")

    End Sub

End Class
