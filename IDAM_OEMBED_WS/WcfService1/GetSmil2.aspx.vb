Imports System.Web
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data
Imports WebArchives.iDAM.WebCommon.Security
Imports System.Net
Imports System.IO


Partial Class GetSmil2
    Inherits System.Web.UI.Page

    Public WSRetrieveAsset As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim sBaseIP As String = ConfigurationSettings.AppSettings("IDAMLocation")
    Dim sBaseInstance As String = ConfigurationSettings.AppSettings("IDAMInstance")
    Dim sVSIDAMClient As String = ConfigurationSettings.AppSettings("IDAMLocationBrowse")
    Dim ClientSessionID As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Session_Start()
        'Response.Write("<smil><head><layout></layout></head><body><video src=""" & WSRetrieveAsset & "id=" & Request.QueryString("ID") & """/></body></smil>")
        Return
    End Sub

    Private Sub Session_Start()
        ' Code that runs when a new session is started
        ' Code that runs when a new session is started
        Dim assetRetrieve = sBaseIP & "/" & sVSIDAMClient

        Dim theLogin As String = ConfigurationSettings.AppSettings("IDAMLogin")
        Dim thePassword As String = ConfigurationSettings.AppSettings("IDAMPassword")
        Dim loginStatus As Boolean = False
        Dim loginResult1 As String = ""
        Dim sessionID As String = "(S(" & GetSessionID("http://" & assetRetrieve & "/GetSessionID.aspx") & "))"
        ClientSessionID = sessionID
        Try

            loginResult1 = LoginToClient("http://" & assetRetrieve & "/" & sessionID & "/Login.aspx", theLogin, thePassword, sBaseInstance)

        Catch Ex As Exception
            Throw Ex
        End Try

        WSRetrieveAsset = "http://" & assetRetrieve & "/" + sessionID + "/RetrieveMovie.ashx?instance=" & sBaseInstance & "&"

        Dim loginResult2 As String = ""


    End Sub
    Private Function LoginToClient(ByVal pURL As String, ByVal pUsername As String, ByVal pPassword As String, ByVal pInstanceID As String) As Boolean
        Dim fullURL As String
        Try

            Dim wc As New Net.WebClient
            Dim result As String
            fullURL = GetLoginRequestURL(pURL, pUsername, pPassword, pInstanceID, False, False, False, 2)
            Dim data As IO.Stream = wc.OpenRead(fullURL)
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            Return (str.IndexOf("SUCCESS") >= 0)
        Catch ex As Exception
            'Log.Error("error logging in to :" + fullURL, ex)
            Return False
        End Try
    End Function
    Public Const LOGIN As String = "txtLogin"
    Public Const PASSWORD As String = "txtPassword"
    Public Const INSTANCEID As String = "txtInstanceID"
    Public Const DIRECT As String = "direct"
    Public Const ENCRYPTED As String = "encrypted"
    Public Const AUTHENTICATIONLEVEL As String = "level"
    Public Const SECUREMODE As String = "secure"
    Public Const LDAPDOMAIN As String = "ld"
    Public Const LDAPPATH As String = "ldp"


    Private Function GetLoginRequestURL(ByVal pLocation As String, ByVal pLogin As String, ByVal pPassword As String, ByVal pInstanceID As String, ByVal pSecureMode As Boolean, Optional ByVal pEncrypted As Boolean = False, Optional ByVal pDirectMode As Boolean = False, Optional ByVal pAuthenticationLevel As Integer = 0, Optional ByVal pLDAPDomain As String = "", Optional ByVal pLDAPPath As String = "") As String
        If pLocation Is Nothing Then
            Throw New ArgumentException("location cannot be null")
        End If
        If pLogin Is Nothing Then
            Throw New ArgumentException("login cannot be null")
        End If
        If pInstanceID Is Nothing Then
            Throw New ArgumentException("pInstanceID cannot be null")
        End If
        If pPassword Is Nothing Then
            Throw New ArgumentException("pPassword cannot be null")
        End If

        Dim url As New Text.StringBuilder
        url.Append(pLocation)
        url.Append("?")
        url.Append(LOGIN).Append("=").Append(pLogin).Append("&")
        url.Append(PASSWORD).Append("=").Append(pPassword).Append("&")
        url.Append(INSTANCEID).Append("=").Append(pInstanceID).Append("&")
        url.Append(DIRECT).Append("=").Append(pDirectMode.ToString).Append("&")
        url.Append(ENCRYPTED).Append("=").Append(pEncrypted.ToString).Append("&")
        url.Append(AUTHENTICATIONLEVEL).Append("=").Append(pAuthenticationLevel.ToString).Append("&")
        url.Append(SECUREMODE).Append("=").Append(pSecureMode.ToString).Append("&")
        If pLDAPDomain <> "" Then
            url.Append(LDAPDOMAIN).Append("=").Append(pLDAPDomain.ToString).Append("&")
        End If
        If pLDAPPath <> "" Then
            url.Append(LDAPPATH).Append("=").Append(pLDAPPath.ToString)
        End If


        Return url.ToString

    End Function
    Private Function GetSessionID(ByVal URL As String) As String
        Try
            Dim clientreq As HttpWebRequest
            clientreq = HttpWebRequest.Create(URL)
            clientreq.Timeout = CType(10000, Integer)
            Dim clientres As HttpWebResponse = clientreq.GetResponse
            Dim data As IO.Stream = clientres.GetResponseStream
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            If str.IndexOf("SESSIONID") >= 0 Then
                Dim strtidx As Integer = str.IndexOf("SESSIONID=") + 10
                Return str.Substring(strtidx, str.IndexOf("</span>") - strtidx)
            Else
                Throw New Exception("No session ID found. check URL if it has the proper format" + URL)
            End If
        Catch ex As Exception
            Throw New Exception("WebService not available." + URL)
        End Try
    End Function
End Class



