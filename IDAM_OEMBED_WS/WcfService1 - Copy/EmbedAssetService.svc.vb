﻿Imports System
Imports System.ServiceModel.Web
Imports System.Web
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data
Imports WebArchives.iDAM.WebCommon.Security
Imports System.Net
Imports System.IO

' NOTE: You can use the "Rename" command on the context menu to change the class name "Service1" in code, svc and config file together.
Public Class EmbedAssetService
    Implements IEmbedAssetService
    Dim sBaseIP As String = ConfigurationSettings.AppSettings("IDAMLocation")
    Dim sBaseInstance As String = ConfigurationSettings.AppSettings("IDAMInstance")
    Dim sVSIDAMClient As String = ConfigurationSettings.AppSettings("IDAMLocationBrowse")
    Dim sSMILUrl As String = ConfigurationSettings.AppSettings("SMILUrl")
    Dim ClientSessionID As String

    Dim WSRetreiveAsset As String

    Public Sub New()
        If WSRetreiveAsset = "" Then
            Session_Start()
        End If
    End Sub

    Public Function GetAssetJson(ByVal url As String, ByVal width As String) As AssetData Implements IEmbedAssetService.GetAssetJson
        Dim id As String = GetIdFromURL(url)
        Return GetAsset(id, width)
    End Function

    Public Function GetAssetXml(ByVal url As String, ByVal width As String) As AssetData Implements IEmbedAssetService.GetAssetXML
        Dim id As String = GetIdFromURL(url)
        Return GetAsset(id, width)
    End Function
    Private Function GetIdFromURL(url As String) As String

        GetIdFromURL = url.Substring(url.IndexOf("id=") + 3).Substring(0, url.Substring(url.IndexOf("id=") + 3).IndexOf("&") + 1)
        If GetIdFromURL = "" Then
            GetIdFromURL = url.Substring(url.IndexOf("id=") + 3)
        End If

    End Function
    Private Function GetAsset(asset_id As String, width As String) As AssetData
        Dim objAsset As AssetData
        Dim url As String
        Dim thumburl As String
        objAsset = AssignAssetDatafromDB(asset_id, width)
        url = WSRetreiveAsset & "id=" & asset_id & "&type=asset&size=1&width=" & objAsset.width & "&height=" & objAsset.height
        thumburl = WSRetreiveAsset & "id=" & asset_id & "&type=asset&size=1&width=190&height=190"
        objAsset.url = url
        objAsset.thumbnail_url = thumburl
        If objAsset.type = "video" Then
            objAsset.html = "    <object classid=""clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"" codebase=""http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0"" scale=""noscale"" SALIGN=""left"" height=""" & objAsset.height & """ width=""" & objAsset.width & """ id=""flvplayer9"" align=""left"" VIEWASTEXT> "
            objAsset.html = objAsset.html & "<param name=""allowScriptAccess"" value=""always"" />"
            objAsset.html = objAsset.html & "<param name=""allowFullScreen"" value=""true"" />"
            objAsset.html = objAsset.html & "<param name=""wmode"" value=""transparent"" />"
            objAsset.html = objAsset.html & "<param name=""movie"" value=""" & sSMILUrl & "Players/FLASH/flvplayer10.swf?asset_id=" & asset_id & "&idam=" & sSMILUrl & "getsmil2.aspx&skinswf=" & sSMILUrl & "PLayers/FLASH/SkinUnderPlayStopSeekFullVol.swf&timecode=&height=" & objAsset.height & "&width=" & objAsset.width & "&baseurl="" /><param name=""play"" value=""false"" /><param name=""loop"" value=""false"" /><param name=""menu"" value=""false"" /><param name=""quality"" value=""high"" /><param name=""bgcolor"" value=""#ffffff"" />	<embed src=""" & sSMILUrl & "Players/FLASH/flvplayer10.swf?asset_id=" & asset_id & "&idam=" & sSMILUrl & "getsmil2.aspx&skinswf=" & sSMILUrl & "Players/FLASH/SkinUnderPlayStopSeekFullVol.swf&timecode=1&height=" & objAsset.height & "&width=" & objAsset.width & "&baseurl="" play=""false"" loop=""false"" menu=""false"" quality=""high"" bgcolor=""#ffffff"" width=""" & objAsset.width & """ height=""" & objAsset.height & """ name=""flvplayer9"" align=""left"" allowScriptAccess=""always"" wmode=""transparent"" allowFullScreen=""true"" type=""application/x-shockwave-flash"" pluginspage=""http://www.macromedia.com/go/getflashplayer"" />"
            objAsset.html = objAsset.html & "    </object>"


        End If
        Return objAsset
    End Function

    Private Function AssignAssetDatafromDB(asset_id As String, width As String) As AssetData
        Dim objAssetData As New AssetData
        Dim DT As New DataTable

        DT = GetAssetDataDT(asset_id)

        If DT.Rows.Count >= 1 Then
            Select Case DT.Rows(0).Item("extension") & ""
                Case "JPG", "BMP", "GIF", "PNG", "PSD", "TIF", "JPEG", "TIFF", "PCT"
                    objAssetData.type = "photo"
                Case "FLV", "MOV"
                    objAssetData.type = "video"
                Case Else
                    objAssetData.type = "link"
            End Select

            objAssetData.version = "1"
            objAssetData.title = DT.Rows(0).Item("name") & ""
            objAssetData.author_name = DT.Rows(0).Item("author_name") & ""
            objAssetData.thumbnail_width = "190"
            objAssetData.thumbnail_height = "190"
            objAssetData.width = DT.Rows(0).Item("WPixel") & ""
            objAssetData.height = DT.Rows(0).Item("HPixel") & ""
            If objAssetData.type <> "video" Then
                If width <> "" Then
                    If objAssetData.width <> "" Then
                        Dim i As Decimal
                        i = Decimal.Parse(width) / Decimal.Parse(objAssetData.width)
                        objAssetData.width = width
                        i = Decimal.Parse(objAssetData.height) * i
                        objAssetData.height = Decimal.ToInt16(i).ToString()
                    End If
                End If
            Else   ' This means it is a Video forcing 640x480, unless otherwise specified
                If width <> "" Then
                    Dim i As Decimal
                    i = Decimal.Parse(width) / Decimal.Parse("640")
                    objAssetData.width = width
                    i = Decimal.Parse("480") * i
                    objAssetData.height = Decimal.ToInt16(i).ToString()
                Else
                    objAssetData.width = "640"
                    objAssetData.height = "480"
                End If

            End If



        End If
        Return objAssetData
    End Function
    Private Function GetAssetDataDT(asset_id As String) As DataTable
        Dim myConnString = ConfigurationSettings.AppSettings("IDAMDBConnString") '"Server=" & ConfigurationSettings.AppSettings("IDAMDBServer") & ";Database=" & sBaseInstance & ";Uid=" & ConfigurationSettings.AppSettings("IDAMLogin") & ";Pwd=" & ConfigurationSettings.AppSettings("IDAMPassword") & ";"
        Dim MyConnection As SqlConnection = New SqlConnection(myConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("  select a.name, a.WPixel, a.HPixel, u.firstname + ' ' + u.lastname as author_name, ltrim(rtrim(upper(f.Extension))) as extension  from ipm_asset a    join ipm_user u on a.userid = u.userid     join ipm_filetype_lookup f on a.media_type = f.media_type where asset_id = @assetid", MyConnection)
        MyCommand1.SelectCommand.Parameters.Add("@assetid", SqlDbType.NVarChar, 255)
        MyCommand1.SelectCommand.Parameters("@assetid").Value = asset_id

        Dim DT1 As New DataTable("AssetData")
        MyCommand1.Fill(DT1)
        Return DT1
    End Function

    Sub Session_Start()
        ' Code that runs when a new session is started
        ' Code that runs when a new session is started
        Dim assetRetrieve = sBaseIP & "/" & sVSIDAMClient

        Dim theLogin As String = ConfigurationSettings.AppSettings("IDAMLogin")
        Dim thePassword As String = ConfigurationSettings.AppSettings("IDAMPassword")
        Dim loginStatus As Boolean = False
        Dim loginResult1 As String = ""
        Dim sessionID As String = "(S(" & GetSessionID("http://" & assetRetrieve & "/GetSessionID.aspx") & "))"
        ClientSessionID = sessionID
        Try

            loginResult1 = LoginToClient("http://" & assetRetrieve & "/" & sessionID & "/Login.aspx", theLogin, thePassword, sBaseInstance)

        Catch Ex As Exception
            Throw Ex
        End Try

        WSRetreiveAsset = "http://" & assetRetrieve & "/" + sessionID + "/RetrieveAsset.aspx?instance=" & sBaseInstance & "&"

        Dim loginResult2 As String = ""


    End Sub




    Private Function LoginToClient(ByVal pURL As String, ByVal pUsername As String, ByVal pPassword As String, ByVal pInstanceID As String) As Boolean
        Dim fullURL As String
        Try

            Dim wc As New Net.WebClient
            Dim result As String
            fullURL = GetLoginRequestURL(pURL, pUsername, pPassword, pInstanceID, False, False, False, 2)
            Dim data As IO.Stream = wc.OpenRead(fullURL)
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            Return (str.IndexOf("SUCCESS") >= 0)
        Catch ex As Exception
            'Log.Error("error logging in to :" + fullURL, ex)
            Return False
        End Try
    End Function


    Public Shared Function GetSessionID(ByVal URL As String) As String
        Try
            Dim clientreq As HttpWebRequest
            clientreq = HttpWebRequest.Create(URL)
            clientreq.Timeout = CType(10000, Integer)
            Dim clientres As HttpWebResponse = clientreq.GetResponse
            Dim data As IO.Stream = clientres.GetResponseStream
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            If str.IndexOf("SESSIONID") >= 0 Then
                Dim strtidx As Integer = str.IndexOf("SESSIONID=") + 10
                Return str.Substring(strtidx, str.IndexOf("</span>") - strtidx)
            Else
                Throw New Exception("No session ID found. check URL if it has the proper format" + URL)
            End If
        Catch ex As Exception
            Throw New Exception("WebService not available." + URL)
        End Try
    End Function

    Public Const LOGIN As String = "txtLogin"
    Public Const PASSWORD As String = "txtPassword"
    Public Const INSTANCEID As String = "txtInstanceID"
    Public Const DIRECT As String = "direct"
    Public Const ENCRYPTED As String = "encrypted"
    Public Const AUTHENTICATIONLEVEL As String = "level"
    Public Const SECUREMODE As String = "secure"
    Public Const LDAPDOMAIN As String = "ld"
    Public Const LDAPPATH As String = "ldp"

    Public Shared Function GetLoginRequestURL(ByVal pLocation As String, ByVal pLogin As String, ByVal pPassword As String, ByVal pInstanceID As String, ByVal pSecureMode As Boolean, Optional ByVal pEncrypted As Boolean = False, Optional ByVal pDirectMode As Boolean = False, Optional ByVal pAuthenticationLevel As Integer = 0, Optional ByVal pLDAPDomain As String = "", Optional ByVal pLDAPPath As String = "") As String
        If pLocation Is Nothing Then
            Throw New ArgumentException("location cannot be null")
        End If
        If pLogin Is Nothing Then
            Throw New ArgumentException("login cannot be null")
        End If
        If pInstanceID Is Nothing Then
            Throw New ArgumentException("pInstanceID cannot be null")
        End If
        If pPassword Is Nothing Then
            Throw New ArgumentException("pPassword cannot be null")
        End If

        Dim url As New Text.StringBuilder
        url.Append(pLocation)
        url.Append("?")
        url.Append(LOGIN).Append("=").Append(pLogin).Append("&")
        url.Append(PASSWORD).Append("=").Append(pPassword).Append("&")
        url.Append(INSTANCEID).Append("=").Append(pInstanceID).Append("&")
        url.Append(DIRECT).Append("=").Append(pDirectMode.ToString).Append("&")
        url.Append(ENCRYPTED).Append("=").Append(pEncrypted.ToString).Append("&")
        url.Append(AUTHENTICATIONLEVEL).Append("=").Append(pAuthenticationLevel.ToString).Append("&")
        url.Append(SECUREMODE).Append("=").Append(pSecureMode.ToString).Append("&")
        If pLDAPDomain <> "" Then
            url.Append(LDAPDOMAIN).Append("=").Append(pLDAPDomain.ToString).Append("&")
        End If
        If pLDAPPath <> "" Then
            url.Append(LDAPPATH).Append("=").Append(pLDAPPath.ToString)
        End If


        Return url.ToString

    End Function

End Class

Public Class AssetData
    Public Property type As String
    Public Property version As String
    Public Property title As String
    Public Property author_name As String
    Public Property thumbnail_url As String
    Public Property thumbnail_width As String
    Public Property thumbnail_height As String
    Public Property url As String
    Public Property width As String
    Public Property height As String
    Public Property html As String
End Class
