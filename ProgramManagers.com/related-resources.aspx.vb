Imports System.Web
Imports System.Data.SqlClient
Imports System.Data


Partial Class related_resources
    Inherits System.Web.UI.Page

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim parm As String = ""
        Dim submarket As String = ""
        If Not (Request.QueryString("keyword") Is Nothing) Then
            parm = Request.QueryString("keyword")
            overviewlink.HRef = "submarket_" & parm
            resourceslink.HRef = "related-resources_" & parm
            allprojectslink.HRef = "project-archive#submarket_" & parm
            submarket = parm.Replace("__", " | ").Replace("-amp-", "&").Replace("-slash-", "/").Replace("-semi-", ";").Replace("-dash-", "-").Replace("_", " ")
            Dim words() As String = submarket.Split("|")
            h1.InnerHtml = formatfunctions.AutoFormatText(words(words.Length - 1).Trim.Replace("Developer", "Business &amp; Industry"))
            htmltitle.InnerHtml = formatfunctions.AutoFormatText(words(words.Length - 1).Trim) & " Resources: Brailsford &amp; Dunlavey"
            'select Headline, Content, DateName(MONTH,Post_Date) + ' ' + DATENAME(dd,post_date) +', ' + DATENAME(yyyy,post_date) Post_Date, PDFLinkTitle, PDF, n.News_Id from IPM_NEWS n left join IPM_NEWS_FIELD_VALUE v on n.News_Id = v.NEWS_ID and v.Item_ID = ( select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWSURL' ) join IPM_NEWS_FIELD_VALUE v2 on n.News_Id = v2.News_Id and v2.Item_ID = ( select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_SUBMARKET' ) and v2.Item_Value like '%" & submarket & "%' where Post_Date <= GETDATE() and Pull_Date >= GETDATE() and Show = 1 and Type = 1 Order By Post_Date Desc

            Dim sql As String = "select n.News_ID, Headline, Content, DateName(MONTH,Post_Date) + ' ' + DATENAME(dd,post_date) +', ' + DATENAME(yyyy,post_date) Post_DateText, PDFLinkTitle, PDF, n.News_Id from IPM_NEWS n left join IPM_NEWS_FIELD_VALUE v on n.News_Id = v.NEWS_ID and v.Item_ID = ( select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWSURL' ) join IPM_NEWS_FIELD_VALUE v2 on n.News_Id = v2.News_Id and v2.Item_ID = ( select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_SUBMARKET' ) and v2.Item_Value like '%" & submarket & "%' where Post_Date <= GETDATE() and Pull_Date >= GETDATE() and Show = 1 and Type = 1 Order By Post_Date Desc"
            'debug.InnerText = sql
            Dim DT1 As New DataTable("FProjects")
            DT1 = mmfunctions.GetDataTable(sql)

            Dim out As String = ""
            For i As Integer = 0 To DT1.Rows.Count - 1
                out &= "<h4>" & DT1.Rows(i)("Post_DateText").ToString.Trim & "<br /><strong>" & formatfunctions.AutoFormatText(DT1.Rows(i)("Headline").ToString.Trim) & "</strong><span>&nbsp;</span></h4>"
                out &= "<div>" & formatfunctions.AutoFormatText(DT1.Rows(i)("Content").ToString.Trim)
                If DT1.Rows(i)("PDF").ToString.Trim = "1" Then
                    Dim temp2 As String = DT1.Rows(i)("PDFLinkTitle").ToString.Trim
                    If temp2 = "" Then
                        temp2 = "Download"
                    End If
                    out &= "<a href=""dynamic/document/fresh/newspdf/download/" & DT1.Rows(i)("News_ID").ToString.Trim & "/" & DT1.Rows(i)("News_ID").ToString.Trim & ".pdf""><strong>" & formatfunctions.AutoFormatText(temp2) & "</strong></a>"
                End If
                out &= "</div>"
            Next

            news_accordion_data.Text = out
















            Dim sql555 As String = "select n.News_ID, Headline, Content, DateName(MONTH,Post_Date) + ' ' + DATENAME(dd,post_date) +', ' + DATENAME(yyyy,post_date) Post_DateText, PDFLinkTitle, PDF, n.News_Id from IPM_NEWS n left join IPM_NEWS_FIELD_VALUE v on n.News_Id = v.NEWS_ID and v.Item_ID = ( select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWSURL' ) join IPM_NEWS_FIELD_VALUE v2 on n.News_Id = v2.News_Id and v2.Item_ID = ( select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_SUBMARKET' ) and v2.Item_Value like '%" & submarket & "%' where Post_Date <= GETDATE() and Pull_Date >= GETDATE() and Show = 1 and Type = 0 Order By Post_Date Desc"
            Dim DT1555 As New DataTable("FProjects")
            Dim out555 As String = ""


        If 1=1 Then
            DT1555 = mmfunctions.GetDataTable(sql555)
            out=""
            For i As Integer = 0 To DT1555.Rows.Count - 1
                out &= "<h4>" & DT1555.Rows(i)("Post_DateText").ToString.Trim & "<br /><strong>" & formatfunctions.AutoFormatText(DT1555.Rows(i)("Headline").ToString.Trim) & "</strong><span>&nbsp;</span></h4>"
                out &= "<div>" & formatfunctions.AutoFormatText(DT1555.Rows(i)("Content").ToString.Trim)
                If DT1555.Rows(i)("PDF").ToString.Trim = "1" Then
                    Dim temp2 As String = DT1555.Rows(i)("PDFLinkTitle").ToString.Trim
                    If temp2 = "" Then
                        temp2 = "Download"
                    End If
                    out &= "<a href=""dynamic/document/fresh/newspdf/download/" & DT1555.Rows(i)("News_ID").ToString.Trim & "/" & DT1555.Rows(i)("News_ID").ToString.Trim & ".pdf""><strong>" & formatfunctions.AutoFormatText(temp2) & "</strong></a>"
                End If
                out &= "</div>"
            Next


if DT1555.Rows.Count > 0
            stories_accordion_data.Text = "<h2>News</h2><div id=""accordion3"" class=""news_accordion"">" & out & "</div>"
end if


        End If








            Dim sql2 As String = "select e.Event_ID, Headline, Content, DateName(MONTH,Event_Date) + ' ' + DATENAME(dd, Event_Date) +', ' + DATENAME(yyyy,Event_Date) EventDateText, ISNULL(v2.Item_Value, '') linktitle  from IPM_EVENTS e join IPM_EVENTS_FIELD_VALUE v on e.Event_Id = v.EVENT_ID and v.Item_ID = (select item_id from IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_SUBMARKET') and v.Item_Value like '%" & submarket & "%'  left join IPM_EVENTS_FIELD_VALUE v2 on e.Event_Id = v2.EVENT_ID and v2.Item_ID = (select Item_ID from IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_EVENTTITLE')  Where pull_date >=  getdate() and Show = 1 and Type = 0 Order By Event_Date Desc"
            Dim DT2 As New DataTable("FProjects")
            DT2 = mmfunctions.GetDataTable(sql2)

            out = ""

            For i As Integer = 0 To DT2.Rows.Count - 1

                out &= "<h4>" & DT2.Rows(i)("EventDateText").ToString.Trim & "<br /><strong>" & formatfunctions.AutoFormatText(DT2.Rows(i)("Headline").ToString.Trim) & "</strong><span>&nbsp;</span></h4><div><p>"
                out &= formatfunctions.AutoFormatText(DT2(i)("Content").ToString.Trim) & "</p>"
                Dim temp2 As String = DT2.Rows(i)("linktitle").ToString.Trim
                If temp2 = "" Then
                    temp2 = "Download"
                End If
                Dim sql3 As String = "Select a.Asset_ID from IPM_ASSET a where a.Category_ID = " & DT2.Rows(i)("Event_ID").ToString.Trim & " and a.Active = 1 and a.Available = 'Y'"
                Dim DT3 As New DataTable("FProjects")
                DT3 = mmfunctions.GetDataTable(sql3)
                If DT3.Rows.Count > 0 Then
                    out &= "<ul>"
                    For j As Integer = 0 To DT3.Rows.Count - 1
                        out &= "<li><a href=""dynamic/document/fresh/asset/download/" & DT3.Rows(j)("Asset_ID").ToString.Trim & "/" & DT3.Rows(j)("Asset_ID").ToString.Trim & ".pdf"">" & temp2 & "</a></li>"
                    Next
                    out &= "</ul>"
                End If
                out &= "</div>"
            Next

            events_accordion_data.Text = out

        End If

    End Sub

End Class
