Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.VisualBasic

Public Class formaturl
    Public Shared Function friendlyurl(ByVal input As String) As String

        ' remove entities
        input = Regex.Replace(input, "&\w+;", "")
        ' remove anything that is not letters, numbers, dash, or space
        input = Regex.Replace(input, "[^A-Za-z0-9\-\s]", "")
        ' remove any leading or trailing spaces left over
        input = input.Trim()
        ' replace spaces with single dash
        input = Regex.Replace(input, "\s+", "-")
        ' if we end up with multiple dashes, collapse to single dash            
        input = Regex.Replace(input, "\-{2,}", "-")
        ' make it all lower case
        input = input.ToLower()
        ' if it's too long, clip it
        If input.Length > 80 Then
            input = input.Substring(0, 79)
        End If
        ' remove trailing dash, if there is one
        If input.EndsWith("-") Then
            input = input.Substring(0, input.Length - 1)
        End If

        Return input
    End Function

    Public Shared Function getwords(ByVal input As String, ByVal WordNo As Integer) As String

        Dim s As String = input
        Dim output As String = ""
        Dim sp() As Char = {" "}
        Dim noOfWordsRequired As Integer = WordNo
        Dim words As String() = s.Split(sp)

        If noOfWordsRequired < words.Length Then
            For i As Integer = 0 To noOfWordsRequired - 1
                output &= words(i) & " "
            Next
        Else
            output = s
        End If

        input = output
        Return input
    End Function

End Class