Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.VisualBasic

Public Class formatfunctionssingle

    Public Shared Function AutoFormatText(ByVal input As String) As String

        input = HttpUtility.HtmlEncode(input)

        ' Dash Symbol
        input = RegularExpressions.Regex.Replace(input, "-- ", "&mdash; ")

        ' Bullet Symbol
        input = RegularExpressions.Regex.Replace(input, "\{[\-]\}", "&#8226;")

        ' Line Breaks
        input = RegularExpressions.Regex.Replace(input, vbNewLine, " ")

        ' Fix Double Entities
        input = RegularExpressions.Regex.Replace(input, "(?<=&)amp;(?=#?\w+;)", String.Empty)

        Return input
    End Function

End Class