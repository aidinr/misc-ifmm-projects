﻿Imports System
Imports System.Web
Imports System.Text
Imports System.Web.UI
Imports System.IO
Imports System.Text.RegularExpressions
Imports HtmlAgilityPack
Imports System.Data

Public Class CMSBasePage
Inherits Page

    Dim urlPage As String
    Dim originalUrlPage As String
    Dim pageUrl As String
    Dim pageName As String
    Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreInit
        pageUrl = HttpContext.Current.Request.RawUrl
        OriginalUrlPage = HttpContext.Current.Request.RawUrl
        pageName = Path.GetFileName(Request.PhysicalPath)
        pageURL = Replace(pageURL, "/", "")
        If InStr(pageURL, "?") > 0 Then
            pageURL = pageURL.Substring(0, pageURL.IndexOf("?"))
            pageURL = Replace(pageURL, "/", "")
        End If
        If pageURL = "Default.aspx" Then
            pageURL = ""
        End If
        UrlPage = pageURL
    End Sub
    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        Dim doc As New HtmlAgilityPack.HtmlDocument
        'If HtmlNode.ElementsFlags.ContainsKey("br") Then
        '    HtmlNode.ElementsFlags("br") = HtmlElementFlag.Closed
        'Else
        '    HtmlNode.ElementsFlags.Add("br", HtmlElementFlag.Closed)
        'End If
        If HtmlNode.ElementsFlags.ContainsKey("img") Then
            HtmlNode.ElementsFlags("img") = HtmlElementFlag.Closed
        Else
            HtmlNode.ElementsFlags.Add("img", HtmlElementFlag.Closed)
        End If
        HtmlNode.ElementsFlags.Remove("form")
        HtmlNode.ElementsFlags.Remove("option")
        doc.OptionWriteEmptyNodes = True
        doc.OptionAutoCloseOnEnd = False
        doc.OptionCheckSyntax = False
        doc.OptionFixNestedTags = False
        doc.LoadHtml(html)
        html = ""
        For Each CmsNode As HtmlNode In doc.DocumentNode.SelectNodes("//@*")
            If CmsNode.Attributes.Contains("class") = True Then
                If CmsNode.Attributes("class").Value.Contains("cms") = True Then
                    Dim FullCmsClassName As String = ""
                    Dim CmsType As String = ""
                    Dim CmsName As String = ""
                    Dim CmsFill As String = ""
                    FullCmsClassName = CmsNode.Attributes("class").Value
                    CmsType = CMSFunctions.GetCmsType(FullCmsClassName)
                    CmsName = CMSFunctions.GetCmsName(FullCmsClassName)
                    CmsFill = CMSFunctions.GetCmsFillType(FullCmsClassName)

                    If CmsFill <> "Static" Then
                        Select Case CmsType
                            Case "TextSingle"
                                Dim elementText As String = CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", CmsName)
                                doc.DocumentNode.SelectSingleNode(CmsNode.XPath).InnerHtml = ElementText
                            Case "TextMulti"
                                Dim elementText As String = CMSFunctions.getContentMulti(Server.MapPath("~") & "cms\data\", CmsName)
                                doc.DocumentNode.SelectSingleNode(CmsNode.XPath).InnerHtml = ElementText
                            Case "Rich"
                                Dim elementText As String = CMSFunctions.GetContentRich(Server.MapPath("~") & "cms\data\", CmsName)
                                doc.DocumentNode.SelectSingleNode(CmsNode.XPath).InnerHtml = ElementText
                            Case "Image"
                        End Select
                    End If

                End If
            End If
        Next
        html = doc.DocumentNode.OuterHtml
        html = Replace(html, "<br>", "<br/>")
        writer.Flush()
        writer.Write(html)

        If OriginalUrlPage.Contains("?") = False Then
            If UrlPage = "" Then
                CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, "index")
            Else
                CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, UrlPage)
            End If
        Else
            If UrlPage = "" Then
                CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, "index")
            Else
                CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, UrlPage)
            End If
        End If

    End Sub

    Private Function Get_HTMLTag(ByVal TagName As String, ByVal HTML As String) As List(Of String)
        Dim lMatch As New List(Of String)
        Dim Tag As New Regex("<\s*" & TagName & "[^>]*>(.*?)<\s*/\s*" & TagName & ">", RegexOptions.IgnoreCase Or RegexOptions.Singleline)
        For Each rMatch As Match In Tag.Matches(HTML)
            lMatch.Add(rMatch.Value)
        Next
        Return lMatch
    End Function

End Class
