﻿Imports System.Web
Imports System.Data.SqlClient
Imports System.Data

Partial Class submarket
    Inherits System.Web.UI.Page

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim out As String = ""
        Dim temp As String = ""
        Dim number As String = "6"
        Dim keyword As String = ""
        Dim parm As String = ""
        Dim name() As String
        If Not (Request.QueryString("keyword") Is Nothing) Then
            parm = Request.QueryString("keyword").ToString.Trim
            keyword = parm.Replace("__", " | ").Replace("-amp-", "&amp;").Replace("-slash-", "/").Replace("-semi-", ";").Replace("-dash-", "-").Replace("_", " ")
            name = keyword.Split("|")
            h1.InnerHtml = name(name.Length - 1).Trim.Replace("Developer", "Business &amp; Industry")
            keyword_data.Text = name(name.Length - 1).Trim
        End If

        If parm <> "" Then
            htmltitle.InnerHtml = name(name.Length - 1) & " Projects: Brailsford &amp; Dunlavey"
            overviewlink.HRef = "submarket_" & parm
            resourceslink.HRef = "related-resources_" & parm
            allprojectslink.HRef = "project-archive#submarket_" & parm
            allProjects.HRef = "project-archive#submarket_" & parm
        End If
        Dim sql As String = "select top " & number & " p.projectid, p.Name, ISNULL(cn.Item_Value, '') ClientName, isnull(v2.item_value,'') Project_Name from IPM_PROJECT p join IPM_PROJECT_FIELD_VALUE v on p.ProjectID = v.ProjectID and v.Item_ID = (Select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_FEATURE') and v.Item_Value = '1'  left join  ipm_project_field_value cn on p.ProjectID = cn.ProjectID and  cn.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_CLIENTNAME') join ipm_project_field_value f on p.ProjectID = f.ProjectID and f.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_FEATUREDSUBMARKET') and f.Item_Value = '1' "
        If keyword <> "" Then
            sql &= " left outer join ipm_project_field_Value v2 on v2.item_id = 2400039 and v2.projectid = p.projectid  join IPM_PROJECT_KEYWORD k on p.ProjectID = k.ProjectID and k.KeyID = (select KeyID from IPM_KEYWORD where keyName ='" & keyword & "')"
        End If
        sql &= " where Available = 'Y' and Show = '1' order by NEWID() "

        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)


        '<ul class="framed_list">
        '    <li>
        '        <a href="portfolio-casestudy" title="">
        '            <img src="assets/images/sample.png" alt=""/>
        '            <span>Northern Kentuky University</span>                                   
        '        </a>
        '    </li>
        '    <li>
        '        <a href="portfolio-casestudy" title="">
        '            <img src="assets/images/sample.png" alt=""/>
        '            <span>Northern Kentuky University</span>                                   
        '        </a>
        '    </li>
        '    <li>
        '        <a href="portfolio-casestudy" title="">
        '            <img src="assets/images/sample.png" alt=""/>
        '            <span>Northern Kentuky University</span>                                   
        '        </a>
        '    </li>                            
        '</ul>
        If DT1.Rows.Count > 0 Then

            out &= "<ul class=""framed_list floating_elements"">"
            For i As Integer = 0 To DT1.Rows.Count - 1
                out &= "<li><a href=""/" & Regex.Replace(DT1.Rows(i)("Project_Name").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-casestudy-" & DT1.Rows(i)("ProjectID") & """ title=""""><img src="""
                temp = "/dynamic/image/forever/project/best/222x140/58/777777/Center/" & DT1.Rows(i)("ProjectID").ToString.Trim & ".jpg"""
                out &= temp.Replace("&", "&amp;")
                out &= " alt=""""/><span>" & formatfunctions.AutoFormatText(DT1.Rows(i)("ClientName").ToString.Trim) & "</span></a></li>"
            Next
            out &= "</ul>"
        End If
        casestudy_data.Text = out

        'Dim sql2 As String = "select top 1 a.Asset_ID,  a.Description, a.Filesize from IPM_ASSET a join IPM_ASSET_CATEGORY c on a.Category_ID = c.CATEGORY_ID and c.NAME = '" & name(name.Length - 1).Trim.Replace(" / ", "/").Replace("&amp;", "&") & "' where a.ProjectID = (select ProjectID from IPM_PROJECT where Name ='Web Site Content') and  a.Active = 1 and a.Available = 'Y'  and a.Media_Type = 2404  Order By a.Creation_Date desc"
        Dim sql2 As String = "select Top 1 a.Asset_ID, a.Description, a.Filesize from IPM_ASSET a join IPM_ASSET_CATEGORY c on a.Category_ID = c.CATEGORY_ID and c.NAME = '" & name(1).Trim.Replace(" / ", "/").Replace("&amp;", "&") & "' and a.ProjectID = (select ProjectID from IPM_PROJECT where Name ='Web Site Content') and c.AVAILABLE = 'Y' join IPM_ASSET_CATEGORY ch on c.PARENT_CAT_ID = ch.CATEGORY_ID and ch.NAME = '" & name(0).Trim.Replace(" / ", "/").Replace("&amp;", "&") & "' and a.ProjectID = (select ProjectID from IPM_PROJECT where Name ='Web Site Content') and ch.AVAILABLE = 'Y' where a.Active = 1 and a.Available = 'Y' and a.Media_Type = 2404 Order By a.Update_Date desc"
        'Response.Write(sql2)
        'Response.End()
        'debug.InnerText = sql2
        Dim DT2 As New DataTable("FProjects")
        DT2 = mmfunctions.GetDataTable(sql2)
        'debug.InnerText = sql & vbCrLf & sql2

        If DT2.Rows.Count > 0 Then
            temp = formatfunctions.AutoFormatText(DT2(0)("Description").ToString.Trim)
            Dim temp2() As String = Split(temp, "<br />", 2, CompareMethod.Text)
            header_data.Text = temp2(0)
            If temp2.Length > 1 Then
                subheader_data.Text = temp2(1)
            End If

            brochurelink.HRef = "dynamic/document/fresh/asset/download/" & DT2.Rows(0)("Asset_ID") & "/" & DT2.Rows(0)("Asset_ID") & ".pdf"
            downloadlink.HRef = "dynamic/document/fresh/asset/download/" & DT2.Rows(0)("Asset_ID") & "/" & DT2.Rows(0)("Asset_ID") & ".pdf"

            filesize.Text = ConvertFileSize.ConvertSize(DT2.Rows(0)("Filesize").ToString.Trim)
            'test.InnerText = temp
        End If
        'casestudy_data.Text = sql
    End Sub

End Class
