<%@ Page Language="VB" AutoEventWireup="false" CodeFile="content_contact.aspx.vb" Inherits="content_contact" Debug="true"  %>
<%
    Dim T1970 As New DateTime(1970, 1, 1)
    Dim Timestamp As Integer = (DateTime.Now.ToUniversalTime - T1970).TotalSeconds
    Dim firstDayThisWeek As Date = Today.AddDays(Today.DayOfWeek)
    Dim Life304 As New Date(firstDayThisWeek.Year, firstDayThisWeek.Month, firstDayThisWeek.Day, 0, 0, 1)
    Dim Life304Offset As Integer = (DateTime.Now.ToUniversalTime - Life304).TotalSeconds
    Dim TimestampWeek As Integer =  Timestamp - Life304Offset - 518400
	If Not Request.Headers("If-Modified-Since") Is Nothing Then
	 Dim tSince As String = Request.Headers("If-Modified-Since")
	 Dim t As Integer = System.Text.RegularExpressions.Regex.Replace(tSince, "[^0-9+]", "")
	 If Timestamp< (t + 604800) Then
	  Response.StatusCode = 304
	  Response.StatusDescription = "Not Modified"
	  return
         End If
        End If
	 Response.Cache.SetCacheability(HttpCacheability.Public)
	 Response.AppendHeader("Pragma", "public")
	 Response.AppendHeader("Connection", "close")
	 Response.AppendHeader("Last-Modified", TimestampWeek & " GMT")
	 Response.AppendHeader("ETag", "Dyno")
%>
         



<div class="content" id="content_contact">
    <div>
        <h2><asp:Literal runat="server" ID="Name"/></h2>
        <p><asp:Literal runat="server" ID="Description"/></p>
    </div>
</div>
