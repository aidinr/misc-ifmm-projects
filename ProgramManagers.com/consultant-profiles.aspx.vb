Imports System.Web
Imports System.Data.SqlClient
Imports System.Data


Partial Class people_profiles
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage
    Protected Function GetIndex(ByRef sorts() As String, value As String) As Integer
        For i As Integer = 0 To sorts.Length - 1
            'Response.Write(value)
            'Response.Write(sorts(i))
            If sorts(i) = value Then
                Return i
            End If
        Next
        Return 999
    End Function
    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sql2 As String = "select Item_Default_Value from IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_STAFFCATEGORY'"
        Dim DT2 As New DataTable("FProjects")
        DT2 = mmfunctions.GetDataTable(sql2)
        Dim sorts() As String = Split(DT2.Rows(0)("Item_Default_Value").ToString.Trim, ";")
 
        Dim sql As String = ""
        sql &= "select FirstName + ' ' + LastName FullName, u.Position, vc.Item_Value, ISNULL(vs.Item_Value, '') usersort, u.UserID, LastName, FirstName "
        sql &= "from IPM_USER u "
        sql &= "left join IPM_USER_FIELD_VALUE vc on u.UserID = vc.USER_ID and vc.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_STAFFCATEGORY') "
        sql &= "left join IPM_USER_FIELD_VALUE vs on u.UserID = vs.USER_ID and vs.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_USERSORT') "
        sql &= "where Active = 'Y' "
        sql &= "and Contact = '0' "
        sql &= "and vc.Item_Value = 'Consultants' "
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)
        DT1.Columns.Add("Sort1", GetType(Integer))
        DT1.Columns.Add("Sort2", GetType(Integer))
        For i As Integer = 0 To DT1.Rows.Count - 1
            DT1.Rows(i)("Sort1") = GetIndex(sorts, DT1.Rows(i)("Item_Value").ToString.Trim)
            Select Case DT1.Rows(i)("usersort").ToString.Trim
                Case "0"
                    DT1.Rows(i)("Sort2") = Integer.MaxValue
                Case ""
                    DT1.Rows(i)("Sort2") = 0
                Case Else
                    Dim temp As Integer = 0
                    Integer.TryParse(DT1.Rows(i)("usersort").ToString.Trim, temp)
                    If temp < 0 Then
                        temp = 0
                    End If
                    DT1.Rows(i)("Sort2") = temp
            End Select
        Next

        DT1.DefaultView.Sort = "Sort1, Sort2 DESC, LastName, FirstName"


        Dim cat As String = ""
        Dim out As String = ""
        For i As Integer = 0 To DT1.DefaultView.Count - 1
            If DT1.DefaultView(i)("Item_Value").ToString.Trim <> cat Then
                cat = DT1.DefaultView(i)("Item_Value").ToString.Trim
                If out <> "" Then
                    out &= "</ul></div>"
                End If
                out &= "<h4><strong>" & formatfunctions.AutoFormatText(cat) & "</strong></h4><div><ul>"
            End If
            out &= "<li><a href=""" & Regex.Replace(DT1.DefaultView(i)("FullName").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-consultant-" & DT1.DefaultView(i)("UserID").ToString.Trim & """><strong>" & formatfunctions.AutoFormatText(DT1.DefaultView(i)("FullName").ToString.Trim) & "</strong></a>" & vbNewLine & "</li>"
        Next
        out &= "</ul></div>"
        accordion_data.Text = out

    End Sub

End Class
