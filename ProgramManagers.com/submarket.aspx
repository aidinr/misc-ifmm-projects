﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="submarket.aspx.vb" Inherits="submarket" Debug="true" %>
<% 

    Dim T1970 As New DateTime(1970, 1, 1)
    Dim Timestamp As Integer = (DateTime.Now.ToUniversalTime - T1970).TotalSeconds
	If Not Request.Headers("If-Modified-Since") Is Nothing Then
	 Dim tSince As String = Request.Headers("If-Modified-Since")
	 Dim t As Integer = System.Text.RegularExpressions.Regex.Replace(tSince, "[^0-9+]", "")
	 If Timestamp < (t + 190) Then ' ########### CACHE FOR 3 MINUTES ###########
	  Response.StatusCode = 304
	  Response.StatusDescription = "Not Modified"
	  return
         End If
        End If
	 Response.Cache.SetCacheability(HttpCacheability.Public)
	 Response.AppendHeader("Pragma", "public")
	 Response.AppendHeader("Connection", "close")
	 Response.AppendHeader("Last-Modified", Timestamp & " GMT")
	 Response.AppendHeader("ETag", "Dyno")

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- submarket.aspx -->
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        <link type="text/css" rel="stylesheet" href="assets/css/reset.css"/>    
        <link type="text/css" rel="stylesheet" href="assets/css/style.css"/>    
        <script src="assets/js/jquery.js" type="text/javascript"></script>  
        <script src="assets/js/js.js" type="text/javascript"></script>         
        <title id="htmltitle" runat="server">B&amp;D</title>       
    </head>
    <body class="submarket">  
        
        <div id="headsUp"></div>
        
        <div class="wrapper">
            <div id="headLeft"></div>
            <div id="headRight"></div>
            <div id="head">
                <div id="header">
                    <div class="content">
                        <h1 title="B&amp;D Inspire. Empower. Advance.">
                            <a href="./" title="B&amp;D Inspire. Empower. Advance.">&nbsp;</a>
                        </h1>
<p>
<a href="offices" title="Contact Us" class="locations">Contact Us</a>
</p>
                            
                    </div>
                </div>
                <div id="menu">
                    <div class="content">
                        <ul>
                            <li><a href="about-us" title="">ABOUT US</a></li>
                            <li><a href="people-overview" title="">PEOPLE</a></li>
                            <li><a href="services" title="">SERVICES</a></li>
                            <li><a href="portfolio" title="" class="selected">PORTFOLIO</a></li>
                            <li><a href="resources" title="">RESOURCES</a></li>
                            <li class="last"><a href="careers" title="">CAREERS</a></li>
                        </ul>
                        <form method="get" id="siteSearch" action="search">
                            <p>
                                <input type="text" name="q" id="siteSearchQ" value="" />
                                <input  id="searchSubmit" type="submit" name="search" value="Search"  />
                            </p>
                        </form>
                    </div>
                </div>                
            </div> 

            <div id="banner">
                <div class="content">
                    <!-- <h6>HIGHER EDUCATION</h6>  -->
                    <h1 id="h1" runat="server">campus edge</h1>
                    <ul> 
                        <li><a id="overviewlink" runat="server" href="portfolio-campus" title="">Overview</a></li>
                        <li><a id="resourceslink" runat="server" href="related-resources" title="">RESOURCES</a></li>
                        <li><a id="allprojectslink" runat="server" href="project-archive" title="">ALL PROJECTS</a></li>
                    </ul>                    	                                     
                </div>
            </div>  
            
            <div id="inner_content">
                <div class="two_columns">
                    <div class="left">
                        <code id="debug" runat="server"></code>
                        <h2>
                            <asp:Literal ID="header_data" runat="server"></asp:Literal>
                        </h2>
                        <h3 class="section_title"><span class="crawlme"><asp:Literal ID="keyword_data" runat="server"></asp:Literal> </span>CASE STUDIES</h3>

                        <div class="text"> 
                        <p><asp:Literal ID="subheader_data" runat="server"></asp:Literal></p>                                                   
                        </div>

                                                
                        <asp:Literal ID="casestudy_data" runat="server"></asp:Literal>

                        &nbsp;<br /><a id="allProjects" runat="server" href="project-archive" title="" class="see_all">SEE ALL PROJECTS</a>
                    </div>
                    <div class="right">
                        
                        <a href="portfolio" title="" class="go_back push_in_sidebar">back to portfolio</a>
                        <div id="rightSOQ">
                        <h3 class="section_title">Statement of Qualifications</h3>
                        <span><a id="downloadlink" runat="server" title="">Download (<asp:Literal ID="filesize" runat="server"></asp:Literal>)</a></span>
                        <pre id="test" runat="server"></pre>                        
                        <br/><br/>
                        <a id="brochurelink" runat="server" href="javascript:;" title="">
                            <img src="assets/images/brochure.png" alt="" style="margin-left:0px;"/>
                        </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="push"></div>
        </div>
   
        <div id="footer">
            <div id="footLeft"></div>
            <div id="footRight"></div>
            <div id="foot">
                <div id="footUp"></div>
                <div class="content">
                    <a class="UpDown" title="Expand"></a><a class="site_map" title="Site Map">SITE MAP</a>
                    <p>
                        <a href="http://venues.programmanagers.com/" class="venues">B&amp;D VENUES</a>
                        <a class="visit_bd selected">B&amp;D</a><a href="http://www.centersusa.com/" class="visit_centers">CENTERS</a>
                    </p>
                    <address>Copyright Brailsford  &amp; Dunlavey, Inc. <span>&copy;</span></address>
                </div>                
            </div>
        </div>
    </body>
</html>
