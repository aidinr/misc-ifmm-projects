﻿<%@ Page Language="VB"   AutoEventWireup="false" CodeFile="offices.aspx.vb" Inherits="offices" %>
<%@ Import Namespace="System.IO" %>
<%
    Dim thePage As String = "OFFICES"
    Dim filepath As String = Server.MapPath("~") & "cms\data\"
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <link type="text/css" rel="stylesheet" href="assets/css/reset.css" />
    <link type="text/css" rel="stylesheet" href="assets/css/style.css" />
    <script src="assets/js/jquery.js" type="text/javascript"></script>
    <script src="assets/js/js.js" type="text/javascript"></script>
    <title><%=CMSFunctions.GetMetaTitle(filepath, thePage)%>: Brailsford        &amp; Dunlavey</title>
    <meta name="description" content="<%=CMSFunctions.GetMetaDescription(filepath, thePage)%>" />
    <meta name="keywords" content="<%=CMSFunctions.GetMetaKeywords(filepath, thePage)%>" />
</head>
<body class="cms">

    <div id="headsUp"></div>

    <div class="wrapper">
        <div id="headLeft"></div>
        <div id="headRight"></div>
        <div id="head">
            <div id="header">
                <div class="content">
                    <h1 title="B&amp;D Inspire. Empower. Advance.">
                        <a href="./" title="B&amp;D Inspire. Empower. Advance.">&nbsp;</a>
                    </h1>
<p>
<a href="offices" title="Contact Us" class="locations">Contact Us</a>
</p>
                            
                </div>
            </div>
            <div id="menu">
                <div class="content">
                    <ul>
                        <li><a href="about-us" title="">ABOUT US</a></li>
                        <li><a href="people-overview" title="">PEOPLE</a></li>
                        <li><a href="services" title="">SERVICES</a></li>
                        <li><a href="portfolio" title="">PORTFOLIO</a></li>
                        <li><a href="resources" title="">RESOURCES</a></li>
                        <li class="last"><a href="careers" title="">CAREERS</a></li>
                    </ul>
                    <form method="get" id="siteSearch" action="search">
                    <p>
                        <input type="text" name="q" id="siteSearchQ" value="" />
                        <input id="searchSubmit" type="submit" name="search" value="Search" />
                    </p>
                    </form>
                </div>
            </div>
        </div>

               <div id="banner" class="<%="cms cmsType_Image cmsName_About_Banner_Image"%>" style="background-image: url(cms/data/Sized/best/1920x314/65/F4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "About_Banner_Image")%>)">

            <div class="content">
                <h1>ABOUT US</h1>
                <ul>
                    <li><a href="about-us" title="">Overview</a></li>
                    <li><a href="mission" title="">mission</a></li>
                    <li><a href="history" title="">history</a></li>
                    <li><a href="associations" title="">associations</a></li>
                    <li><a href="community" title="">community</a></li>
                    <li><a href="news" title="">news</a></li>
                    <li><a href="sustainability" title="">sustainability</a></li>
                    <li><a href="offices" title="">offices</a></li>
                </ul>
            </div>
        </div>

        <div id="inner_content">
            <div class="two_columns">
                <div class="left">
                    <h2 class="cms cmsType_TextMulti cmsName_Locations_Page_Heading">
                       <%-- <% Response.Write(CMSFunctions.GetTextSimple(filepath, thePage, "Page_Heading"))%>--%>
                    </h2>
                    <div class="text">

                    <div class="cms cmsType_Rich cmsName_Locations_Page_Intro">
                       <%-- <% Response.Write(CMSFunctions.GetTextSimple(filepath, thePage, "Page_Intro"))%>--%>
                    </div>

<div class="lCol1 cmsGroup cmsName_Locations_Column_1">
<!--
<p class="locItem cmsGroupItem">
<strong class="cms cmsType_TextSingle cmsName_City">City Name</strong><br />
<span class="cms cmsType_TextMulti cmsName_Address">
Address Line 1
Address Line 2
Address Line 3
City, ZIP
Phone: 222-555-1212
</span>
<em><a class="cms cmsType_Link cmsName_Link" href="#" target="_blank">Map Office</a></em>
</p>
-->

<%
  For i As Integer = 1 To 20 - 1
 If IO.Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Locations_Column_1_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
%>

<p class="locItem cmsGroupItem">
<strong class="cms cmsType_TextSingle cmsName_Locations_Column_1_Item_<%=i%>_City"></strong><br />
<span class="cms cmsType_TextMulti cmsName_Locations_Column_1_Item_<%=i%>_Address">
</span>
<em><a class="cms cmsType_Link cmsName_Locations_Column_1_Item_<%=i%>_Link" href="<%=CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", "Locations_Column_1_Item_" & i & "_Link", "href")%>" target="_blank"><%=CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", "Locations_Column_1_Item_" & i & "_Link", "text")%></a></em>
</p>

 <%End If %>
<%Next%>  


</div>



<div class="lCol2 cmsGroup cmsName_Locations_Column_2">
<!--
<p class="locItem cmsGroupItem">
<strong class="cms cmsType_TextSingle cmsName_City">City Name</strong><br />
<span class="cms cmsType_TextMulti cmsName_Address">
Address Line 1
Address Line 2
Address Line 3
City, ZIP
Phone: 222-555-1212
</span>
<em><a class="cms cmsType_Link cmsName_Link" href="#" target="_blank">Map Office</a></em>
</p>
-->

<%
  For i As Integer = 1 To 20 - 1
        If IO.Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Locations_Column_2_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
%>

<p class="locItem cmsGroupItem">
<strong class="cms cmsType_TextSingle cmsName_Locations_Column_2_Item_<%=i%>_City"></strong><br />
<span class="cms cmsType_TextMulti cmsName_Locations_Column_2_Item_<%=i%>_Address">
</span>
<em><a class="cms cmsType_Link cmsName_Locations_Column_2_Item_<%=i%>_Link" href="<%=CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", "Locations_Column_2_Item_" & i & "_Link", "href")%>" target="_blank">Map Office</a></em>
</p>

 <%End If %>
<%Next%>  

</div>



<div class="lCol3 cmsGroup cmsName_Locations_Column_3">
<!--
<p class="locItem cmsGroupItem">
<strong class="cms cmsType_TextSingle cmsName_City">City Name</strong><br />
<span class="cms cmsType_TextMulti cmsName_Address">
Address Line 1
Address Line 2
Address Line 3
City, ZIP
Phone: 222-555-1212
</span>
<em><a class="cms cmsType_Link cmsName_Link" href="#" target="_blank">Map Office</a></em>
</p>
-->

<%
  For i As Integer = 1 To 20 - 1
        If IO.Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Locations_Column_3_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
%>

<p class="locItem cmsGroupItem">
<strong class="cms cmsType_TextSingle cmsName_Locations_Column_3_Item_<%=i%>_City"></strong><br />
<span class="cms cmsType_TextMulti cmsName_Locations_Column_3_Item_<%=i%>_Address">
</span>
<em><a class="cms cmsType_Link cmsName_Locations_Column_3_Item_<%=i%>_Link" href="<%=CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", "Locations_Column_3_Item_" & i & "_Link", "href")%>" target="_blank">Map Office</a></em>
</p>
 <%End If %>
<%Next%>  
<div class="c"></div>
</div>


                    </div>
                </div>
                <div class="right">
                        <ol class="strong_links cms cmsFill_Static cmsType_TextMulti cmsName_Side_Highlights">
                                <% Dim s As String = CMSFunctions.GetTextFormatBulletList(filepath, "Side_Highlights")%>
                                <% Dim words As String() = Split(s, vbNewLine)
                                    Dim word As String
                                    For Each word In words%>
                                    <li><a><%=word%></a></li>
                                <% Next word%>
                        </ol>
                </div>
            </div>
        </div>

        <div class="push"></div>
    </div>
        <div id="footer">
            <div id="footLeft"></div>
            <div id="footRight"></div>
            <div id="foot">
                <div id="footUp"></div>
                <div class="content">
                    <a class="UpDown" title="Expand"></a><a class="site_map" title="Site Map">SITE MAP</a>
                    <p>
                        <a href="http://venues.programmanagers.com/" class="venues">B&amp;D VENUES</a>
                        <a class="visit_bd selected">B&amp;D</a><a href="http://www.centersusa.com/" class="visit_centers">CENTERS</a>
                    </p>
                    <address>Copyright Brailsford  &amp; Dunlavey, Inc. <span>&copy;</span></address>
                </div>                
            </div>
        </div>        
      
    </body>
</html>
