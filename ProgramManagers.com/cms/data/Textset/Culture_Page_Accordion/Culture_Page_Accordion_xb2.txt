B&D places strong emphasis on the importance of diversity in all we do. Our recruiting efforts are focused on hiring the best and brightest unique individuals from various cultural, experiential, and educational backgrounds. We believe that diversity is the key to collaboration and ultimately to providing world-class solutions to our diverse clients.

