Brailsford & Dunlavey does not rent, sell, or share personal information about you with other people or nonaffiliated companies, except to provide services that are specifically requested or under the following circumstances:

B&D may provide your information to third parties who work on behalf of or with B&D under confidentiality agreements. These companies may use your personal information to help B&D communicate with you about its services. However, these companies do not have any independent right to share this information;
B&D also may use and/or disclose Personally Identifiable Information to any third party if it believes that it is required to do so for any or all of the following reasons:
(-) by law;
(-) to respond to requests from governmental or public authorities;
(-) to prevent, investigate, detect, or prosecute criminal offenses or attacks on the technical
    integrity of the Site or B&D’s network; and/or
(-) to protect the rights, privacy, property, business, or safety of Brailsford & Dunlavey and 
    its affiliates, their partners and employees, the users of the Site, or the public.

By using the Site, you acknowledge and consent to the fact that Personally Identifiable Information provided by you or collected by B&D through your use of the Site may be stored and processed by B&D in the United States, which may provide a different level of data protection than that provided by your country of residence. You also acknowledge and consent to the fact that B&D may, at its discretion, store and process such data in other countries and will endeavor to protect the Personally Identifiable Information under its control wherever it is processed or stored.
