Imports System
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Web.Configuration
Imports System.Web.UI

Partial Public Class sized
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim imgxDimensions As String = ""
        If Not Request.QueryString("imgxDimensions") Is Nothing Then
            imgxDimensions = Request.QueryString("imgxDimensions")
        End If
        Dim widthheight() As String = imgxDimensions.Split("x")

        Dim imgxWidth As String = widthheight(0)
        Dim imgxHeight As String = ""
        If widthheight.Length > 1 Then
            imgxHeight = widthheight(1)
        End If
        If imgxheight = "" Then
            imgxheight = imgxwidth
            If imgxheight = "" Then
                imgxheight = "1"
            End If
        ElseIf imgxwidth = "" Then
            imgxwidth = imgxheight
            If imgxwidth = "" Then
                imgxwidth = "1"
            End If
        End If
        Dim imgxQuality As String = ""
        If Not Request.QueryString("imgxQuality") Is Nothing Then
            imgxQuality = Request.QueryString("imgxQuality")
        End If
        Dim imgxCrop As String = ""
        If Not Request.QueryString("imgxCrop") Is Nothing Then
            imgxCrop = Request.QueryString("imgxCrop")
        End If
        Dim imgxColor As String = ""
        If Not Request.QueryString("imgxColor") Is Nothing Then
            imgxColor = Request.QueryString("imgxColor")
        End If
        Dim imgxAlign As String = ""
        If Not Request.QueryString("imgxAlign") Is Nothing Then
            imgxAlign = Request.QueryString("imgxAlign")
        End If
        Dim imgxId As String = ""
        If Not Request.QueryString("imgxId") Is Nothing Then
            imgxId = Request.QueryString("imgxId")
        End If
        Dim url As String = Request.Url.ToString()
        Dim imgxExtention As String = Request.QueryString("imgxExtension")

        Dim srcpath As String = Server.MapPath("~") & "cms\data\Image\"
        Dim srcfile As String = imgxId

        Dim filepath As String = Server.MapPath("~") & "cms\data\Sized\" & imgxCrop & "\" & imgxDimensions & "\" & imgxQuality & "\" & imgxColor & "\" & imgxAlign & "\"
        Dim fullfilename As String = filepath & imgxId & "." & imgxExtention


        If Not Directory.Exists(filepath) Then
            Try
                Directory.CreateDirectory(filepath)
            Catch ex As Exception
            End Try
        End If
        If Not File.Exists(srcpath & srcfile) Then
            If Not Directory.Exists(srcpath) Then
                Try
                    Directory.CreateDirectory(srcpath)
                Catch ex As exception
                End Try
            End If

        End If


        For Each f As String In Directory.GetFiles(srcpath, srcfile & ".*")
            If Path.GetExtension(f) <> ".txt" Then
                Dim ext As String = Path.GetExtension(f)

                If Not File.Exists(fullfilename) Then

                    Dim fileargs As String = ""
                    Select Case imgxCrop
                        Case "spot"
                            fileargs = " -size " & imgxDimensions & " xc:#" & imgxColor & " null: (  " & srcpath & srcfile & ext & " -gravity " & imgxAlign & " -crop " & imgxDimensions & "+0+0 +repage ) -gravity " & imgxAlign & " -layers Composite -format " & imgxExtention & " -unsharp 0x.5 -strip -quality " & imgxQuality & "  " & fullfilename.Replace("\", "\\")
                        Case "padded"
                            fileargs = " -size " & imgxDimensions & " xc:#" & imgxColor & " null: (  " & srcpath & srcfile & ext & " -gravity " & imgxAlign & " -resize """ & imgxDimensions & ">"" -crop " & imgxDimensions & "+0+0 +repage ) -gravity " & imgxAlign & " -layers Composite -format " & imgxExtention & " -unsharp 0x.5 -strip -quality " & imgxQuality & "  " & fullfilename.Replace("\", "\\")
                        Case "best"
                            fileargs = " -size " & imgxDimensions & " xc:#""" & imgxColor & """ null: (  " & srcpath & srcfile & ext & " -resize """ & imgxDimensions & "^>"" ) -gravity " & imgxAlign & " -layers Composite -format " & imgxExtention & " -unsharp 0x.5 -strip -quality " & imgxQuality & " " & fullfilename
                        Case "edge"
                            fileargs = " -size " & imgxDimensions & " xc:""#" & imgxColor & "00"" null: ( " & srcpath & srcfile & ext & " -thumbnail """ & imgxDimensions & """ +repage ( +clone -shave 10x10 -fill gray50 -colorize 100% -mattecolor gray50 -frame 10x10+3+4 -blur 0x2 ) -compose HardLight -composite ) -gravity " & imgxAlign & " -layers Composite -format " & imgxExtention & " -unsharp 0x.5 -strip -quality " & imgxQuality & " " & fullfilename.Replace("\", "\\")
                        Case "fit"
                            fileargs = srcpath & srcfile & ext & " -resize """ & imgxDimensions & ">"" -format " & imgxExtention & " -unsharp 0x.5 -strip -quality " & imgxQuality & "  " & fullfilename.Replace("\", "\\")
                        Case "liquid"
                            fileargs = " " & srcpath & srcfile & ext & " -resize " & imgxDimensions & " -unsharp 0x.5 -quality " & imgxQuality & " " & fullfilename.Replace("\", "\\")
                        Case "painted"
                            fileargs = " -size " & imgxDimensions & " xc:#" & imgxColor & " null: (  " & srcpath & srcfile & ext & " -gravity " & imgxAlign & " -resize """ & imgxDimensions & ">"" -crop " & imgxDimensions & "+0+0 +repage ) -gravity " & imgxAlign & " -layers Composite   -fuzz 4% -fill ""#" & imgxColor & """  -opaque ""#ffffff""        -format " & imgxExtention & " -unsharp 0x.5 -strip -quality " & imgxQuality & "  " & fullfilename.Replace("\", "\\")
                        Case Else
                            fileargs = " -size " & imgxHeight & "x" & imgxWidth & " xc:'" & imgxColor & "' " & fullfilename
                    End Select
                    Try
                        'WebEventLog.WriteToEventLog("fileargs - " & fileargs)
                        Dim proc As New Diagnostics.Process()
                        proc.StartInfo.Arguments = fileargs
                        proc.StartInfo.FileName = "C:\Program Files\ImageMagick-6.7.8-Q16\convert.exe"
                        proc.StartInfo.UseShellExecute = False
                        proc.StartInfo.CreateNoWindow = True
                        proc.StartInfo.RedirectStandardOutput = True
                        proc.StartInfo.RedirectStandardError = True
                        proc.Start()
                        proc.BeginOutputReadLine()
                        Dim output As String = proc.StandardError.ReadToEnd()
                        proc.WaitForExit()
                        proc.Close()
                    Catch ex As Exception
                        Response.Redirect("/dynamic/clear.png", True)
                    End Try

                End If

                Response.Clear()
                Response.ClearHeaders()

                If ext <> ".txt" Then
                    Dim theImg As System.Drawing.Image
                    theImg = System.Drawing.Image.FromFile(fullfilename)

                    Select Case imgxExtention
                        Case "jpg"
                            Response.ContentType = "image/jpeg"
                            Response.WriteFile(fullfilename)
                        Case "gif"
                            Response.ContentType = "image/gif"
                            Response.WriteFile(fullfilename)
                        Case "png"
                            Response.ContentType = "image/png"
                            Response.WriteFile(fullfilename)
                    End Select

                    theImg.Dispose()

                End If
            End If
        Next

    End Sub

    Private Sub errorImg(ByVal filepath As String, ByVal height As String, ByVal width As String, ByVal color As String)

        'convert -size 100x100 xc:'#aaffaa' xyz.jpg

        Dim fileargs As String = "-size " & height & "x" & width & " xc:'" & color & "' " & filepath
        Dim proc As New Diagnostics.Process()
        proc.StartInfo.Arguments = fileargs
        proc.StartInfo.FileName = "C:\Program Files\ImageMagick-6.7.8-Q16\convert.exe"
        proc.StartInfo.UseShellExecute = False
        proc.StartInfo.CreateNoWindow = True
        proc.StartInfo.RedirectStandardOutput = False
        proc.Start()
        proc.WaitForExit()

    End Sub
End Class