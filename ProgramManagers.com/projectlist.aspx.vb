﻿Imports System.Web
Imports System.Data.SqlClient
Imports System.Data


Partial Class projectlist
    Inherits System.Web.UI.Page
    Protected Function FormatTag(TagType As String, Tag As String) As String

        Dim formatedTag As String = " "
        Dim temp As String = Tag.Trim.Replace("&amp;", "&")
        temp = temp.Replace(" ", "_")
        temp = temp.Replace("-", "-dash-")
        temp = temp.Replace("&", "-amp-")
        temp = temp.Replace("/", "-slash-")
        temp = temp.Replace("|", "")

        Dim matchpattern As String = "[^\w-_]"
        Dim replace As String = ""
        temp = Regex.Replace(temp, matchpattern, replace)

        formatedTag = TagType & temp & " "
        Return formatedTag

    End Function

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim UserID As String = ""
        If Not (Request.QueryString("UserID") Is Nothing) Then
            UserID = Request.QueryString("UserID").ToString.Trim
        End If

        Dim number As String = ""
        If Not (Request.QueryString("number") Is Nothing) Then
            number = Request.QueryString("number").ToString.Trim
        End If

        Dim out As String = "<dl><dt><em>Client / project name</em><span>Location</span></dt>"
        Dim sql As String = "select "
        If number <> "" Then
            sql &= " top " & number
        End If
        sql &= " p.ProjectID,  ISNULL(cn.Item_Value, '') ClientName,  DATEDIFF(SECOND, '1/1/1970', ProjectDate) UnixDate, City, State_id, ISNULL(v.Item_Value, '0') featured, isnull(v2.item_value, '') as Project_Name from IPM_PROJECT p left join IPM_PROJECT_FIELD_VALUE v on p.ProjectID = v.ProjectID and v.Item_ID = ( select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_FEATURE')  left join  ipm_project_field_value cn on p.ProjectID = cn.ProjectID and  cn.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_CLIENTNAME') join ipm_project_field_value wa on p.ProjectID = wa.ProjectID and  wa.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_WEBACTIVE') and wa.Item_Value = '1' left outer join ipm_project_field_Value v2 on v2.item_id = 2400039 and v2.projectid = p.projectid "
        If UserID <> "" Then
            sql &= " join IPM_PROJECT_CONTACT c on c.ProjectID = p.ProjectID and c.UserID = " & UserID
        End If
        sql &= " where Available = 'Y' "
        sql &= " Order By DATEDIFF(SECOND, '1/1/1970', ProjectDate) DESC "

        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)

        Dim sql2 As String = "Select v2.ProjectID, d2.Item_Tag, v2.Item_Value from IPM_PROJECT_FIELD_VALUE v2 join IPM_PROJECT_FIELD_DESC d2 on v2.Item_ID = d2.Item_ID and d2.Item_Tag in ('IDAM_Address','IDAM_COUNTRY2', 'IDAM_PROJNAME', 'IDAM_PROJECTTAG') "
        If UserID <> "" Then
            sql2 &= " where v2.ProjectID in (select p.ProjectID from IPM_PROJECT p join IPM_PROJECT_CONTACT c on c.ProjectID = p.ProjectID and c.UserID = " & UserID & ")"
        End If

        Dim DT2 As New DataTable("FProjects")
        DT2 = mmfunctions.GetDataTable(sql2)

        Dim sql3 As String = "select ProjectID, KeyName from IPM_PROJECT_DISCIPLINE pd join IPM_DISCIPLINE d on pd.KeyID = d.KeyID and d.KeyUse = 1 "
        If UserID <> "" Then
            sql3 &= " where ProjectID in (select p.ProjectID from IPM_PROJECT p join IPM_PROJECT_CONTACT c on c.ProjectID = p.ProjectID and c.UserID = " & UserID & ")"
        End If

        Dim DT3 As New DataTable("FProjects")
        DT3 = mmfunctions.GetDataTable(sql3)

        Dim sql4 As String = "select ProjectID, KeyName from IPM_PROJECT_KEYWORD pk join IPM_KEYWORD k on pk.KeyID = k.KeyID and k.KeyUse = 1 "
        If UserID <> "" Then
            sql4 &= " where ProjectID in (select p.ProjectID from IPM_PROJECT p join IPM_PROJECT_CONTACT c on c.ProjectID = p.ProjectID and c.UserID = " & UserID & ")"
        End If
        Dim DT4 As New DataTable("FProjects")
        DT4 = mmfunctions.GetDataTable(sql4)

        Dim sql5 As String = "select ProjectID, KeyName from IPM_PROJECT_OFFICE po join IPM_OFFICE o on po.OfficeID = o.KeyID and o.KeyUse = 1  "
        If UserID <> "" Then
            sql5 &= " where ProjectID in (select p.ProjectID from IPM_PROJECT p join IPM_PROJECT_CONTACT c on c.ProjectID = p.ProjectID and c.UserID = " & UserID & ")"
        End If
        Dim DT5 As New DataTable("FProjects")
        DT5 = mmfunctions.GetDataTable(sql5)

        If DT1.Rows.Count > 0 Then
            For i As Integer = 0 To DT1.Rows.Count - 1
                Dim ProjectID As String = DT1.Rows(i)("ProjectID").ToString.Trim
                Dim ClientName As String = DT1.Rows(i)("ClientName").ToString.Trim
                Dim ProjectName As String = DT1.Rows(i)("Project_Name").ToString.Trim

                Dim drow() As DataRow = DT2.Select("ProjectID='" & ProjectID & "' AND Item_Tag = 'IDAM_PROJNAME'")

                If drow.Length = 0 Then
                    Continue For
                Else
                    If drow(0)("Item_Value").ToString.Trim = "" Then
                        Continue For
                    End If
                End If
                If ClientName.Trim.Length = 0 Then
                    Continue For
                End If
                out &= "<dd class=""filter "
                If DT1.Rows(i)("featured").ToString.Trim = "1" Then
                    out &= "featured "
                End If

                drow = DT2.Select("ProjectID='" & ProjectID & "' AND ITEM_TAG = 'IDAM_SERVICE'")
                If drow.Length > 0 Then
                    For j As Integer = 0 To drow.Length - 1
                        If drow(j)("Item_Value").ToString.Trim <> "" Then
                            out &= FormatTag("sustainable_", drow(j)("Item_Value").ToString.Trim)
                        End If
                    Next
                End If

                drow = DT2.Select("ProjectID='" & ProjectID & "' AND ITEM_TAG = 'IDAM_BUILT'")
                If drow.Length > 0 Then
                    For j As Integer = 0 To drow.Length - 1
                        If drow(j)("Item_Value").ToString.Trim <> "" Then
                            out &= FormatTag("built_", drow(j)("Item_Value").ToString.Trim)
                        End If
                    Next
                End If

                drow = DT2.Select("ProjectID='" & ProjectID & "' AND ITEM_TAG = 'IDAM_PROJECTTAG'")
                If drow.Length > 0 Then
                    For j As Integer = 0 To drow.Length - 1
                        If drow(j)("Item_Value").ToString.Trim <> "" Then
                            Dim values() As String = Split(drow(j)("Item_Value").ToString.Trim, ",", -1, CompareMethod.Text)
                            For k As Integer = 0 To values.Length - 1
                                out &= FormatTag("tag_", values(k).Trim)
                            Next
                        End If
                    Next
                End If


                drow = DT3.Select("ProjectID='" & ProjectID & "'")
                If drow.Length > 0 Then
                    For j As Integer = 0 To drow.Length - 1
                        out &= FormatTag("market_", drow(j)("KeyName").ToString.Trim)
                    Next
                End If

                drow = DT2.Select("ProjectID='" & ProjectID & "' AND Item_Tag = 'IDAM_Address'")
                If drow.Length > 0 Then
                    dim state as String = drow(0)("Item_Value").ToString.Trim
                    state = Regex.Match(state, "[^,]+$").Groups(0).Value.Trim
                    out &= " state_" & state & " "
                End If

                drow = DT4.Select("ProjectID='" & ProjectID & "'")
                If drow.Length > 0 Then
                    For j As Integer = 0 To drow.Length - 1
                        out &= FormatTag("submarket_", drow(j)("KeyName").ToString.Trim)
                    Next
                End If
                drow = DT5.Select("ProjectID='" & ProjectID & "'")

                If drow.Length > 0 Then
                    For j As Integer = 0 To drow.Length - 1
                        out &= FormatTag("service_", drow(j)("KeyName").ToString.Trim)

                    Next
                End If
                out &= """><a href=""" & Regex.Replace(ProjectName.ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-casestudy-" & ProjectID & """>"
                'drow = DT2.Select("ProjectID='" & ProjectID & "' AND Item_Tag = 'IDAM_PROJNAME'")
                out &= "<strong>"
                'If drow.Length > 0 Then
                out &= formatfunctions.AutoFormatText(ClientName)
                'End If
                out &= "</strong>"
                'out &= "<tt>" & DT1(i)("UnixDate").ToString.Trim & "</tt>"
                out &= "<tt>" & DT1(i)("UnixDate").ToString.Trim.PadLeft(11, "0"c) & "</tt>"
                out &= "<span>"
                out &= "<span>"
                drow = DT2.Select("ProjectID='" & ProjectID & "' AND Item_Tag = 'IDAM_PROJNAME'")
                If drow.Length > 0 Then
                    out &= formatfunctions.AutoFormatText(drow(0)("Item_Value").ToString.Trim)
                End If
                out &= "</span>"

                out &= "<em>"
                drow = DT2.Select("ProjectID='" & ProjectID & "' AND Item_Tag = 'IDAM_Address'")
                If drow.Length > 0 Then
                    out &= formatfunctions.AutoFormatText(drow(0)("Item_Value").ToString.Trim)
                End If
                out &= "</em>"
                out &= "</span></a></dd>" & vbCrLf
            Next
        End If
        out &= "</dl>"
        Response.Clear()
        Response.Write(out)

    End Sub

End Class
