Imports System.Web
Imports System.Data.SqlClient
Imports System.Data


Partial Class portfolio_casestudy
    Inherits System.Web.UI.Page
    Protected Function IsRelated(ProjectID As String, Projects As String) As Boolean
        Dim check() As String = Projects.Split(",")
        For i As Integer = 0 To check.Length - 1
            If ProjectID = check(i).Trim Then
                Return True
            End If
        Next

        Return False

    End Function
    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim ProjectID As String = ""

        If Not (Request.QueryString("ProjectID") Is Nothing) Then
            ProjectID = Request.QueryString("ProjectID")

            Dim sql3 As String = "select COUNT(*) from IPM_PROJECT_ASSET where Project_ID = " & ProjectID & " and Available = 'Y'"

            'ProjectText.Text = ProjectID & " ~ " & sql3
            'Exit Sub
            Dim DT3 As New DataTable("FProjects")
            DT3 = mmfunctions.GetDataTable(sql3)

            If DT3.Rows(0)(0).ToString.Trim <> "0" Then
                '<img id="case_img" runat="server" alt=" " src="assets/images/case_img.png" />
                case_img.Text = "<img alt="" "" src=""/dynamic/image/forever/project/best/220x145/60/777777/Center/" & ProjectID & ".jpg"" />"
            End If


            'Dim sql4 As String = "select ISNULL(ClientName, '') ClientName from IPM_PROJECT where ProjectID = " & ProjectID & " and Available = 'Y'"
            'Dim DT4 As New DataTable("FProjects")
            'DT4 = mmfunctions.GetDataTable(sql4)

            Dim sql As String = "select d.Item_Tag, v.Item_Value from IPM_PROJECT_FIELD_VALUE v join IPM_PROJECT_FIELD_DESC d on v.Item_ID = d.Item_ID where ProjectID = " & ProjectID
            Dim DT1 As New DataTable("FProjects")
            DT1 = mmfunctions.GetDataTable(sql)
            Dim Project_Name As String = ""
            If DT1.Rows.Count > 0 Then
                Dim drows() As DataRow = DT1.Select("Item_Tag='IDAM_CLIENTNAME'")
                If drows.Length > 0 Then
                    ClientName.Text = formatfunctions.AutoFormatText(drows(0)("Item_Value").ToString.Trim)
                    htmltitle.InnerHtml = formatfunctions.AutoFormatText(drows(0)("Item_Value").ToString.Trim) & " (Brailsford &amp; Dunlavey)"
                End If

                drows = DT1.Select("Item_Tag='IDAM_Address'")
                If drows.Length > 0 Then
                    Address.Text = formatfunctions.AutoFormatText(drows(0)("Item_Value").ToString.Trim)
                End If

                drows = DT1.Select("Item_Tag='IDAM_PROJNAME'")
                If drows.Length > 0 Then
                    ProjectName.Text = formatfunctions.AutoFormatText(drows(0)("Item_Value").ToString.Trim)
                    Project_Name = drows(0)("Item_Value").ToString.Trim
                End If
                'ProjectName.Text = formatfunctions.AutoFormatText(DT4(0)("ClientName").ToString.Trim)

                drows = DT1.Select("Item_Tag='IDAM_WEBDESCRIPTION'")
                If drows.Length > 0 Then
                    ProjectText.Text = formatfunctions.AutoFormatText(drows(0)("Item_Value").ToString.Trim)
                End If

                drows = DT1.Select("Item_Tag='IDAM_VIDEO'")
                If drows.Length > 0 Then
                    If drows(0)("Item_Value").ToString.Trim <> "" Then
                        Dim temp As String = "<a href=""http://vimeo.com/"
                        'temp &= drows(0)("Item_Value").ToString.Trim
                        temp &= Regex.Replace(drows(0)("Item_Value").ToString.Trim, "[^\d]", "")
                        temp &= """ class=""play_video vidThumb vimeo"" rel=""" & Regex.Replace(drows(0)("Item_Value").ToString.Trim, "[^\d]", "") & """>PLAY VIDEO</a>"
                        videolink.Text = temp
                    End If
                End If

'                drows = DT1.Select("Item_Tag='IDAM_PROJVID'")
'                If drows.Length > 0 Then
'                    If drows(0)("Item_Value").ToString.Trim <> "" Then
'                        Dim temp As String = "<div id=""vimeoVid"">"
'                        temp &= drows(0)("Item_Value").ToString.Trim
'                        temp &= "</div>"
'                        projectvideolink.Text = temp
'                    End If
'                End If

            End If

            Dim sql2 As String = "Select KeyName, tsort from (select KeyName, 1  tsort from IPM_PROJECT_KEYWORD pk join IPM_KEYWORD k on pk.KeyID = k.KeyID and k.KeyUse = 1 where ProjectID = " & ProjectID
            sql2 &= " union all select KeyName, 0  tsort from IPM_PROJECT_DISCIPLINE pd join IPM_DISCIPLINE d on pd.KeyID = d.KeyID  and d.KeyUse = 1 where ProjectID = " & ProjectID
            'sql2 &= " union select KeyName from IPM_PROJECT_OFFICE po join IPM_OFFICE o on po.OfficeID = o.KeyID and o.KeyUse = 1 where ProjectID = " & ProjectID
            sql2 &= " ) t   Order By tsort, KeyName"

            Dim DT2 As New DataTable("FProjects")
            DT2 = mmfunctions.GetDataTable(sql2)

            '<li><a href="javascript:;" title="">Casestudies</a> / </li>
            '<li><a href="javascript:;" title="">Higher Education </a> / </li>
            '<li><a href="javascript:;" title="">Campus Planning</a> / </li>
            '<li><a href="javascript:;" title="">University of Nevada</a></li>

            Dim out As String = "<li><a href=""project-archive"" title="""">Case Studies</a> / </li>"
            If DT2.Rows.Count > 0 Then
                For i As Integer = 0 To DT2.Rows.Count - 1
                    Dim names() As String = DT2.Rows(i)("KeyName").ToString.Trim.Split("|")
                    If DT2.Rows(i)("tsort").ToString.Trim = "0" Then
                        out &= "<li><a href=""project-archive#market_" & DT2.Rows(i)("KeyName").ToString.Trim.Replace("-", "-dash-").Replace("/", "-slash-").Replace(" ", "_").Replace("|", "").Replace("&amp;", "-amp-") & """ title="""">" & formatfunctions.AutoFormatText(names(names.Length - 1).Trim) & "</a> / </li>"
                    Else
                        out &= "<li><a href=""project-archive#submarket_" & DT2.Rows(i)("KeyName").ToString.Trim.Replace("-", "-dash-").Replace("/", "-slash-").Replace(" ", "_").Replace("|", "").Replace("&amp;", "-amp-") & """ title="""">" & formatfunctions.AutoFormatText(names(names.Length - 1).Trim) & "</a> / </li>"
                    End If
                Next
            End If
            Links.Text = out & "<li><a href=""" & Regex.Replace(Project_Name.ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-casestudy-" & ProjectID & """ title="""">" & ProjectName.Text & "</a></li>"


            out = ""
            Dim sql4 As String = "Select distinct t.Headline, t.Content, t.Post_Date from (select a.Headline, cast(a.Content as varchar(max)) Content, a.Post_Date from IPM_AWARDS_RELATED_PROJECTS rp join IPM_AWARDS a on rp.Awards_Id = a.Awards_Id where a.Show = 1 and a.Pull_Date >= GETDATE() and  rp.ProjectID = " & ProjectID & " union all Select a.Headline, cast(a.Content as varchar(max)) Content, a.Post_Date from IPM_AWARDS a join IPM_AWARDS_FIELD_VALUE v on a.Awards_Id = v.AWARDS_ID and v.Item_ID = ( select Item_ID from IPM_AWARDS_FIELD_DESC where Item_Tag = 'IDAM_AWARDPROJECTID') and v.Item_Value = '" & ProjectID & "') t Order By t.Post_Date desc"
            Dim DT4 As New DataTable("FProjects")
            DT4 = mmfunctions.GetDataTable(sql4)
            If DT4.Rows.Count > 0 Then
                out &= "<h4><strong>Awards</strong><span>&nbsp;</span></h4><div>"
                For i As Integer = 0 To DT4.Rows.Count - 1
                    out &= "<p><strong>" & DT4.Rows(i)("Content").ToString.Trim & "</strong><span>" & DT4.Rows(i)("Headline").ToString.Trim & "</span></p>"
                Next
                out &= "</div>"
            End If


            Dim sql5 As String = "select e.Event_ID, e.Headline, e.Content, v.Item_Value, ISNULL(v2.Item_Value, '') linktitle from IPM_EVENTS e join IPM_EVENTS_FIELD_VALUE v on e.Event_Id = v.EVENT_ID and v.Item_ID = ( select fd.Item_ID from IPM_EVENTS_FIELD_DESC fd where fd.Item_Tag = 'IDAM_EVENTRELATED' )  left join IPM_EVENTS_FIELD_VALUE v2 on e.Event_Id = v2.EVENT_ID and v2.Item_ID = (select Item_ID from IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_EVENTTITLE')  Where e.Show = 1 and e.Pull_Date >= GETDATE() Order by e.Post_Date desc "

            Dim related As Boolean = False
            Dim DT5 As New DataTable("FProjects")
            DT5 = mmfunctions.GetDataTable(sql5)
            If DT5.Rows.Count > 0 Then
                For i As Integer = 0 To DT5.Rows.Count - 1
                    If IsRelated(ProjectID, DT5.Rows(i)("Item_Value").ToString.Trim) Then
                        related = True
                    End If
                Next
                If related = True Then

                    out &= "<h4><strong>Presentations</strong><span>&nbsp;</span></h4><div>"
                    For i As Integer = 0 To DT5.Rows.Count - 1
                        If IsRelated(ProjectID, DT5.Rows(i)("Item_Value").ToString.Trim) Then
                            out &= "<p><strong>" & formatfunctions.AutoFormatText(DT5.Rows(i)("Headline").ToString.Trim) & "</strong><span>" & formatfunctions.AutoFormatText(DT5(i)("Content").ToString.Trim) & "</span></p>"
                            Dim temp2 As String = DT5.Rows(i)("linktitle").ToString.Trim
                            If temp2 = "" Then
                                temp2 = "Download"
                            End If
                            Dim sql5a As String = "Select a.Asset_ID from IPM_ASSET a where a.Category_ID = " & DT5.Rows(i)("Event_ID").ToString.Trim & " and a.Active = 1 and a.Available = 'Y'"
                            Dim DT5a As New DataTable("FProjects")
                            DT5a = mmfunctions.GetDataTable(sql5a)
                            If DT5a.Rows.Count > 0 Then
                                out &= "<ul>"
                                For j As Integer = 0 To DT5a.Rows.Count - 1
                                    out &= "<li><a href=""dynamic/document/fresh/asset/download/" & DT5a.Rows(j)("Asset_ID").ToString.Trim & "/" & DT5a.Rows(j)("Asset_ID").ToString.Trim & ".pdf"">" & temp2 & "</a></li>"
                                Next
                                out &= "</ul>"
                            End If
                        End If
                    Next
                    out &= "</div>"
                End If

            End If



            Dim sql6a As String = "Select * from ( Select n.News_ID, n.Headline, n.Content, PDFLinkTitle, PDF, Post_Date, LTrim(RTrim(CAST(ProjectID as varchar(255)))) Projects  from IPM_NEWS_RELATED_PROJECTS rp join IPM_NEWS n on rp.News_Id = n.News_Id where n.Show = 1 and n.Pull_Date >= GETDATE() and n.Type = 1 and rp.ProjectID = " & ProjectID & "Union Select n.News_ID, n.Headline, n.Content, PDFLinkTitle, PDF, Post_Date, v.Item_Value from IPM_NEWS n join IPM_NEWS_FIELD_VALUE v on n.News_Id = v.NEWS_ID and v.Item_ID = (select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWSRELATEDPROJ') and v.Item_Value <> '' where n.Show = 1 and n.Pull_Date >= GETDATE() and n.Type = 1 ) t Order By Post_Date desc"

            Dim DT6a As New DataTable("FProjects")
            DT6a = mmfunctions.GetDataTable(sql6a)
            If DT6a.Rows.Count > 0 Then
                Dim outnews As String = ""
                Dim outnewsheader As String = "<h4><strong>Publications</strong><span>&nbsp;</span></h4><div>"
                For i As Integer = 0 To DT6a.Rows.Count - 1
                    If IsRelated(ProjectID, DT6a.Rows(i)("Projects").ToString.Trim) Then
                        outnews &= "<p><strong>" & formatfunctions.AutoFormatText(DT6a.Rows(i)("Headline").ToString.Trim) & "</strong><span>" & formatfunctions.AutoFormatText(DT6a.Rows(i)("Content").ToString.Trim) & "</span></p>"
                        If DT6a.Rows(i)("PDF").ToString.Trim = "1" Then
                            Dim temp2 As String = DT6a.Rows(i)("PDFLinkTitle").ToString.Trim
                            If temp2 = "" Then
                                temp2 = "Download"
                            End If
                            outnews &= "<a href=""dynamic/document/fresh/newspdf/download/" & DT6a.Rows(i)("News_ID").ToString.Trim & "/" & DT6a.Rows(i)("News_ID").ToString.Trim & ".pdf""><strong>" & formatfunctions.AutoFormatText(temp2) & "</strong></a>"
                        End If
                    End If
                Next
                Dim outnewsfooter As String = "</div>"
                If outnews <> "" Then
                    out &= outnewsheader & outnews & outnewsfooter
                End If
            End If





            Dim sql6 As String = "Select * from ( Select n.News_ID, n.Headline, n.Content, PDFLinkTitle, PDF, Post_Date, LTrim(RTrim(CAST(ProjectID as varchar(255)))) Projects  from IPM_NEWS_RELATED_PROJECTS rp join IPM_NEWS n on rp.News_Id = n.News_Id where n.Show = 1 and n.Pull_Date >= GETDATE() and n.Type = 0 and rp.ProjectID = " & ProjectID & "Union Select n.News_ID, n.Headline, n.Content, PDFLinkTitle, PDF, Post_Date, v.Item_Value from IPM_NEWS n join IPM_NEWS_FIELD_VALUE v on n.News_Id = v.NEWS_ID and v.Item_ID = (select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWSRELATEDPROJ') and v.Item_Value <> '' where n.Show = 1 and n.Pull_Date >= GETDATE() and n.Type = 0 ) t Order By Post_Date desc"

            Dim DT6 As New DataTable("FProjects")
            DT6 = mmfunctions.GetDataTable(sql6)
            If DT6.Rows.Count > 0 Then
                Dim outnews As String = ""
                Dim outnewsheader As String = "<h4><strong>In the News</strong><span>&nbsp;</span></h4><div>"
                For i As Integer = 0 To DT6.Rows.Count - 1
                    If IsRelated(ProjectID, DT6.Rows(i)("Projects").ToString.Trim) Then
                        outnews &= "<p><strong>" & formatfunctions.AutoFormatText(DT6.Rows(i)("Headline").ToString.Trim) & "</strong><span>" & formatfunctions.AutoFormatText(DT6.Rows(i)("Content").ToString.Trim) & "</span></p>"
                        If DT6.Rows(i)("PDF").ToString.Trim = "1" Then
                            Dim temp2 As String = DT6.Rows(i)("PDFLinkTitle").ToString.Trim
                            If temp2 = "" Then
                                temp2 = "Download"
                            End If
                            outnews &= "<a href=""dynamic/document/fresh/newspdf/download/" & DT6.Rows(i)("News_ID").ToString.Trim & "/" & DT6.Rows(i)("News_ID").ToString.Trim & ".pdf""><strong>" & formatfunctions.AutoFormatText(temp2) & "</strong></a>"
                        End If
                    End If
                Next
                Dim outnewsfooter As String = "</div>"
                If outnews <> "" Then
                    out &= outnewsheader & outnews & outnewsfooter
                End If
            End If

















            accordion_data.Text = out

        End If

    End Sub

End Class
