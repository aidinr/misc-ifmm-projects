Imports System.Web
Imports System.Data.SqlClient
Imports System.Data

Partial Class _projectSearch_Featured
    Inherits System.Web.UI.Page
    Protected Function FormatTag(TagType As String, Tag As String) As String

        Dim formatedTag As String = " "
        Dim temp As String = Tag.Trim.Replace("&amp;", "&")
        temp = temp.Replace(" ", "_")
        temp = temp.Replace("-", "-dash-")
        temp = temp.Replace("&", "-amp-")
        temp = temp.Replace("/", "-slash-")
        temp = temp.Replace("|", "")

        Dim matchpattern As String = "[^\w-_]"
        Dim replace As String = ""
        temp = Regex.Replace(temp, matchpattern, replace)

        formatedTag = TagType & temp & " "
        Return formatedTag

    End Function
    Protected Function DecodeTag(TagType As String, Tag As String) As String

        Dim temp As String = Tag.Trim.Replace(TagType, "")
        temp = temp.Replace("__", " | ")
        temp = temp.Replace("-slash-", "/")
        temp = temp.Replace("-amp-", "&")
        temp = temp.Replace("-dash-", "-")
        temp = temp.Replace("_", " ")
        temp = temp.Replace("&", "&amp;")
        Return temp

    End Function
    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '<ul class="framed_list_results floating_elements less_margin">
        '    <li>
        '        <a href="casestudy-2400198"><img src="/dynamic/image/forever/project/best/201x125/50/777777/Center/2400198.jpg" alt=""><span>Young Harris College</span></a>
        '    </li>
        '    <li>
        '        <a href="casestudy-2402732"><img src="/dynamic/image/forever/project/best/201x125/50/777777/Center/2402732.jpg" alt=""><span>Roosevelt University</span></a>
        '    </li>
        '    <li>
        '        <a href="casestudy-2405591"><img src="/dynamic/image/forever/project/best/201x125/50/777777/Center/2405591.jpg" alt=""><span>Capstone Development Corp.</span></a>
        '    </li>
        '</ul>


        Dim market As String = ""
        Dim decodedMarket As String = ""
        If Not Request.QueryString("market") Is Nothing Then
            market = Request.QueryString("market")
            decodedMarket = DecodeTag("market_", market)
        End If

        Dim submarket As String = ""
        Dim decodedSubMarket As String = ""
        If Not Request.QueryString("submarket") Is Nothing Then
            submarket = Request.QueryString("submarket")
            decodedSubMarket = DecodeTag("submarket_", submarket)
        End If

        Dim service As New System.Collections.Generic.List(Of String)
        If Not Request.QueryString("Service[]") Is Nothing Then
            service.AddRange(Request.QueryString("Service[]").Split(","))
        End If

        Dim build As New System.Collections.Generic.List(Of String)
        If Not Request.QueryString("Built[]") Is Nothing Then
            build.AddRange(Request.QueryString("Built[]").Split(","))
            'For i As Integer = 0 To build.Count - 1
            '    Response.Write(build(i).Trim & "<br />")
            'Next
        End If

        Dim sustainable As New System.Collections.Generic.List(Of String)
        If Not Request.QueryString("Sustainable[]") Is Nothing Then
            sustainable.AddRange(Request.QueryString("Sustainable[]").Split(","))
        End If

        Dim filterState As String = ""
        If Not Request.QueryString("state") Is Nothing Then
            filterState = Request.QueryString("state")
        End If

        Dim sql As String = "Select top 12 t.ProjectID, ClientName, isnull(v2.item_value, '') as Project_Name from ( Select distinct p.ProjectID, ClientName from IPM_PROJECT p join ipm_project_field_value wa on p.ProjectID = wa.ProjectID and  wa.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_WEBACTIVE') and wa.Item_Value = '1' join IPM_PROJECT_ASSET pa on p.ProjectID = pa.Project_ID and pa.Available = 'Y' join IPM_PROJECT_FIELD_VALUE v on p.ProjectID = v.ProjectID and v.Item_ID = (select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_FEATURE'  ) and v.Item_Value = '1'  "
        If market <> "" Then
            sql &= " join IPM_PROJECT_DISCIPLINE pd on p.ProjectID = pd.ProjectID and pd.KeyID = (Select KeyID from IPM_DISCIPLINE where KeyName = '" & decodedMarket & "' and KeyUse = 1 ) "
        End If
        If submarket <> "" Then
            sql &= " join IPM_PROJECT_KEYWORD pk on p.ProjectID = pk.ProjectID and pk.KeyID = (Select KeyID from IPM_KEYWORD where KeyUse = 1 and  KeyName = '" & decodedSubMarket & "' ) "
        End If
        If service.Count > 0 Then
            sql &= " join IPM_PROJECT_OFFICE po on p.ProjectID = po.ProjectID and po.OfficeID "
            If service.Count = 1 Then
                sql &= "  = (Select KeyID from IPM_OFFICE where KeyName = '" & DecodeTag("service_", service(0).Trim) & "' and KeyUse = 1 ) "
            Else
                sql &= " in ( "
                For i As Integer = 0 To service.Count - 1
                    If i > 0 Then
                        sql &= " , "
                    End If
                    sql &= "  (Select KeyID from IPM_OFFICE where KeyName = '" & DecodeTag("service_", service(i).Trim) & "' and KeyUse = 1 ) "
                Next
                sql &= " ) "
            End If
        End If
        If build.Count > 0 Then
            sql &= " join IPM_PROJECT_FIELD_VALUE pb on p.ProjectID = pb.ProjectID and pb.Item_ID = (Select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_BUILT') and pb.Item_Value "
            If build.Count = 1 Then
                sql &= " = '" & build(0).Trim & "'"
            Else
                sql &= " in ( "
                For i As Integer = 0 To build.Count - 1
                    If i > 0 Then
                        sql &= " , "
                    End If
                    sql &= " '" & build(0).Trim & "' "
                Next
                sql &= " ) "
            End If
        End If
        If sustainable.Count > 0 Then
            sql &= " join IPM_PROJECT_FIELD_VALUE ps on p.ProjectID = ps.ProjectID and ps.Item_ID = (Select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUSTAINABLE') and ps.Item_Value "
            If build.Count = 1 Then
                sql &= " = '" & sustainable(0).Trim & "'"
            Else
                sql &= " in ( "
                For i As Integer = 0 To sustainable.Count - 1
                    If i > 0 Then
                        sql &= " , "
                    End If
                    sql &= " '" & sustainable(0).Trim & "' "
                Next
                sql &= " ) "
            End If
        End If
        sql &= " where p.Available = 'Y' "
        If filterState <> "" Then
            sql &= " and p.State_id  = '" & filterState & "' "
        End If
        sql &= " ) t left outer join ipm_project_field_Value v2 on v2.item_id = 2400039 and v2.projectid = t.projectid Order by NEWID()"

        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)

        Dim out As String = ""
        If DT1.Rows.Count > 0 Then
            out = "<ul class=""framed_list_results floating_elements less_margin"">"
            For i As Integer = 0 To DT1.Rows.Count - 1
                out &= "<li><a href=""" & Regex.Replace(DT1.Rows(i)("Project_Name").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-casestudy-" & DT1.Rows(i)("ProjectID").ToString.Trim & """><img src=""/dynamic/image/forever/project/best/201x125/50/777777/Center/" & DT1.Rows(i)("ProjectID").ToString.Trim & ".jpg"" alt="""" /><span>" & DT1.Rows(i)("ClientName").ToString.Trim & "</span></a></li>"
            Next
            out &= "</ul>"
        End If
        Response.Write(out)
    End Sub

End Class
