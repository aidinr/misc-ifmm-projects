document.write('<style type="text/css">body {visibility: hidden;opacity: 0} .wf-oswald-i7-active body {visibility: visible;opacity: 1}</style>');
history.navigationMode = 'compatible';
$('*').unbind();
$(window).unload(function () {
    $('*').unbind();
    $('body', 'html').val('');
});

      WebFontConfig = {
        google: { families: [ 'Open Sans:bold,bolditalic,italic,regular', 'Oswald:bold,bolditalic,italic,regular', 'Sorts Mill Goudy:bold,bolditalic,italic,regular' ] }
      };
      (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
      })();

if (String(document.location).split('.').pop() == 'html' || String(document.location).split('.').pop() == 'aspx') {
    document.location = String(document.location).substr(0, String(document.location).lastIndexOf('.'));
}

var IEVersion = 0 /*@cc_on+ScriptEngineMajorVersion()@*/;

var startWorkCompleted = 0;
var perPageWorkCompleted = 1000;

var startArchive = 0;
var perPageArchive = 10;
var carGo;
var loadedTime = new Date().getTime();
var projectSearchList;
var projectSearchFilters;
var projectSearchFeatured;
var workCompletedList;
var resizeTimer;

var ff = setInterval('fontsFix ()', 90);

$(document).ready(function () {

    document.body.style.filter = '';
    $('#headsUp, .wrapper, .content *, #banner, #bannerProjectArchive').removeAttr('filter');

    // Start submarket carousel
    if ($('body').hasClass('submarket')) {

        $('body').prepend('<div id="CarSlidesWrapper"><div id="CarSlides"></div><div id="slideBottom"></div><div id="slideTop"><div class="carDetails"><p></p></div></div></div><div class="carousel_navWrapper"><div class="carousel_navWrapper2"><div class="carousel_nav"></div></div></div>');
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'carousel.aspx?height=400&carMarket=submarket_' + getPage().replace('related-resources_', '').replace('submarket_', '');
        $('head').append(script);
    }
    // End submarket carousel

    // Home Page
    if ($('#home').attr('id')) {

        $('#home').prepend('<div id="CarSlidesWrapper"><div id="CarSlides"></div><div id="slideBottom"></div><div id="slideTop"><div class="carDetails"><p></p></div></div></div>');

        var h = Math.round($(window).height()) - 464;
        if ((Math.round($(window).height()) - 464) < 200) {
            $('#main').css('height', '140px');
            $('body').addClass('short');
            $('.main_heading').css('marginTop', '0');
            $('.CarSlide, #slideTop, #slideBottom').css('backgroundPosition', '50% 100px').css('minHeight', '450px');
            $('html').css('overflow-y', 'hidden');
            h = h + 5;
        }
        if (h < 550) h = 550;
        else if (h < 600) h = 600;
        else if (h < 650) h = 650;
        else if (h < 700) h = 700;
        else h = 750;
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'carousel.aspx?height=' + h;
        $('head').append(script);


     $('body, .wrapper, #main, #slideTop, #slideBottom').css('minWidth', '997px');
if(!IEVersion) {
     setInterval(function(){
        $(window).resize(function() {
          clearTimeout(resizeTimer);
          resizeTimer = setTimeout("sizeHome()", 50);
        });
     },500);
}
        // End Home Page
    }

    // Sub Pages
    if ($('#banner').attr('id') || $('#bannerProjectArchive').attr('id')) {
        $('#banner, #bannerProjectArchive').prepend('<div class="mask"></div>');
if(!IEVersion) {
        $(window).resize(function () {
            sizeBanner();
        });
}
        $('#menu a').each(function () {
            if ($(this).attr('href') == getPage()) $(this).addClass('selected');
        });
        $('#banner .content li a').each(function () {
            if ($(this).attr('href') == getPage()) $(this).addClass('active');
            if (String($(this).html()).toLowerCase() == 'overview') {
                $('#menu a[href="' + $(this).attr('href') + '"] ').addClass('selected');
            }
        });

        if ($('.project_filters').hasClass('project_filters')) {
            projectSearch_Init();
            $('#state').change(function () {
             startArchive = 0;
             projectSearch_Init();
            });
        }

        $('#projectSearch').submit(function () {

            if ($('#project_keywords').val() == 'Search Projects') return false;
            projectSearchKeywords_Init();
            return false;
        })

        $('.moreFeaturedPrev').hide();

        // End Sub Page
    }

    // Accordion
    if ($('#accordion').attr('id')) {

        var perPageAccordion = 10;
        if ($('#accordion').hasClass('paginate') && $('#accordion h4').length > perPageAccordion) {
            var startAcordion = 0;
            $('#accordion').after('<p class="pagination"><a class="previous">&lt;&lt; Previous</a><a class="next">Next &gt;&gt;</a></p>');
            $('#accordion h4').slice(perPageAccordion).hide();
            $('.pagination .next').click(function (index) {
                $('.pagination .previous').show();
                var iii = 0;
                $('#accordion h4').removeClass('active');
                $('#accordion>div').slideUp(160, function () {
                    if (iii++ < 1) {
                        $('#accordion h4:lt(' + (startAcordion + perPageAccordion) + ')').slideUp(250);
                        $('#accordion h4:gt(' + (startAcordion + perPageAccordion - 1) + '):lt(' + perPageAccordion + ')').slideDown(300);
                        $('html, body').animate({
                            scrollTop: ($(this).parent().offset().top - 110) + 'px'
                        }, 400);
                        startAcordion = startAcordion + perPageAccordion;
                        if (startAcordion + perPageAccordion >= $('#accordion h4').length) $('.pagination .next').hide();
                    }
                });
            });
            $('.pagination .previous').click(function (index) {

                $('.paginate:eq(0)').css('height', $('.paginate:eq(0)').height() + 'px').css('overflow', 'hidden');

                $('html, body').animate({
                    scrollTop: ($(this).parent().prev().offset().top - 110) + 'px'
                }, 400);

                var ii = 0;
                var iii = 0;
                startAcordion = startAcordion - perPageAccordion;
                if (startAcordion < 1) $('.pagination .previous').hide();
                $('.pagination .next').show();
                $('#accordion h4').removeClass('active');
                $('#accordion>div').slideUp(160, function () {
                    if (iii++ < 1) {
                        if (startAcordion > 2) $('#accordion h4:gt(' + (startAcordion - 1) + ')').slideDown(250);

                        else $('#accordion h4').slideDown(250);
                        $('#accordion h4:gt(' + (startAcordion + perPageAccordion - 1) + ')').slideUp(300, function () {
                            if (ii++ < 1) {
                                setTimeout("$('.paginate:eq(0)').css('height','auto');", 180);
                            }
                        });
                    }
                });
            });
        }

        if ($('#accordion2').hasClass('paginate') && $('#accordion2 h4').length > perPageAccordion) {
            var startAcordion2 = 0;
            $('#accordion2').after('<p class="pagination"><a class="previous2">&lt;&lt; Previous</a><a class="next2">Next &gt;&gt;</a></p>');
            $('#accordion2 h4').slice(perPageAccordion).hide();
            $('.pagination .next2').click(function (index) {
                $('.pagination .previous2').show();
                var iii = 0;
                $('#accordion2 h4').removeClass('active');
                $('#accordion2>div').slideUp(160, function () {
                    if (iii++ < 1) {
                        $('#accordion2 h4:lt(' + (startAcordion2 + perPageAccordion) + ')').slideUp(250);
                        $('#accordion2 h4:gt(' + (startAcordion2 + perPageAccordion - 1) + '):lt(' + perPageAccordion + ')').slideDown(300);
                        $('html, body').animate({
                            scrollTop: ($(this).parent().offset().top - 110) + 'px'
                        }, 400);
                        startAcordion2 = startAcordion2 + perPageAccordion;
                        if (startAcordion2 + perPageAccordion >= $('#accordion2 h4').length) $('.pagination .next2').hide();
                    }
                });
            });
            $('.pagination .previous2').click(function (index) {

                $('.paginate:eq(1)').css('height', $('.paginate:eq(1)').height() + 'px').css('overflow', 'hidden');

                $('html, body').animate({
                    scrollTop: ($(this).parent().prev().offset().top - 110) + 'px'
                }, 400);

                var ii = 0;
                var iii = 0;
                startAcordion2 = startAcordion2 - perPageAccordion;
                if (startAcordion2 < 1) $('.pagination .previous2').hide();
                $('.pagination .next2').show();
                $('#accordion2 h4').removeClass('active');
                $('#accordion2>div').slideUp(160, function () {
                    if (iii++ < 1) {
                        if (startAcordion2 > 2) $('#accordion2 h4:gt(' + (startAcordion2 - 1) + ')').slideDown(250);

                        else $('#accordion2 h4').slideDown(250);
                        $('#accordion2 h4:gt(' + (startAcordion2 + perPageAccordion - 1) + ')').slideUp(300, function () {
                            if (ii++ < 1) {
                                setTimeout("$('.paginate:eq(1)').css('height','auto');", 180);
                            }
                        });
                    }
                });
            });
        }

        $('#accordion, #accordion2').css('visibility', 'hidden');
        $('#accordion h4, #accordion2 h4').bind('click', function () {
            if ($(this).hasClass('active')) {
                $(this).parent().find('h4').removeClass('active');
                $(this).next('div').slideUp(300);
                eraseCookie(String($(this).parent().attr('id') + getPage()).replace(/[^a-zA-Z0-9]+/g, ''));
            } else {
                $(this).parent().find('h4').removeClass('active');
                $(this).parent().find('div').slideUp(300);
                $(this).addClass('active');
                $(this).next('div').slideDown(300);
                if ($(this).parent().attr('id') == 'accordion') createCookie(String('accordion' + getPage()).replace(/[^a-zA-Z0-9]+/g, ''), 'A' + String($(this).index('#accordion h4')));
                if ($(this).parent().attr('id') == 'accordion2') createCookie(String('accordion2' + getPage()).replace(/[^a-zA-Z0-9]+/g, ''), 'A' + String($(this).index('#accordion2 h4')));
            }
        });

        var a = readCookie(String('accordion' + getPage()).replace(/[^a-zA-Z0-9]+/g, ''));
        if (a) {
            a = Math.round(a.substr(1));
            $('#accordion h4').removeClass();
            $('#accordion>div:eq(' + a + ')').slideUp(300);
            $('#accordion h4:eq(' + a + ')').addClass('active');
            $('#accordion h4:eq(' + a + ')').next('div').show();
        } else {
            if ($('#accordion.people_list_accordion').hasClass('people_list_accordion')) {
                $('#accordion h4').removeClass();
                $('#accordion>div:eq(0)').slideUp(300);
                $('#accordion h4:eq(0)').addClass('active');
                $('#accordion h4:eq(0)').next('div').show();
            }
        }
        var a = readCookie(String('accordion2' + getPage()).replace(/[^a-zA-Z0-9]+/g, ''));
        if (a) {
            a = Math.round(a.substr(1));
            $('#accordion2 h4').removeClass();
            $('#accordion2 div:eq(' + a + ')').slideUp(300);
            $('#accordion2 h4:eq(' + a + ')').addClass('active');
            $('#accordion2 h4:eq(' + a + ')').next('div').show();
        }

        if ($('#accordion h4').is('h4')) $('#accordion').css('visibility', 'visible');
        else $('#accordion').prev('h2').css('visibility', 'hidden');

        if ($('#accordion2 h4').is('h4')) $('#accordion2').css('visibility', 'visible');
        else $('#accordion2').prev('h2').css('visibility', 'hidden');
    }
    // End Accordion

    // Start Search Projects Auto

    if ($('#project_keywords').attr('id')) {
        $('#project_keywords').attr('autocomplete', 'off');

        if (window.location.hash && String(window.location.hash).substr(1, 8) == 'keywords') {
          $('#project_keywords').val(window.location.hash.substr(10).replace(/\+/g, ' ').split('&')[0]);
        }

        else $('#project_keywords').val('Search Project');
        $('#project_keywords').focus(function () {
            $(this).val('');
        });
        $('#project_keywords').blur(function () {
            if (!$(this).val()) $(this).val('Search Projects');
        });
    }
    // End Search Projects Auto

    // Start Breadcrumbs
    if ($('#sub_menu .content').hasClass('content')) {
        $('#sub_menu .content>ul>li>a').each(function () {
            $(this).after('<strong>' + $(this).html() + '</strong>');
        });
    }
    // End Breadcrumbs


    // Start Portfolio
    if ($('.project_list').hasClass('project_list')) {
        $('.portPlus a').each(function () {
            $(this).html($(this).parent().next('.portDetails').find('p a em span').html());
        });

        $('#banner.cmsName_Portfolio_Banner_Image div.content>ul>li>a').click(function () {
            if ($(this).attr('href')) {
                if ($(this).hasClass('active')) return false;
                $('.project_list>li .img').hide();
                $('#inner_content>ul.project_list>li').show();
                $('#banner>div.content>ul>li>a.active').removeClass('active');
                $('#banner>div.content>ul>li>a:eq(0)').addClass('active');
                $('.portPlus').hide();
                return false;
            } else {
                var i = $(this).index('#banner div.content>ul>li>a') - 1;
                $('#inner_content>ul.project_list>li').hide();
                $('#inner_content>ul.project_list>li:eq(' + i + ')').show();
                $('.project_list>li .img').hide();
                $('.project_list>li .img:eq(' + i + ')').show();
                $('#banner>div.content>ul>li>a.active').removeClass('active');
                $('#banner>div.content>ul>li>a:eq(' + $(this).index('#banner div.content>ul>li>a') + ')').addClass('active');
                $('.portPlus').show();
            }
        });

        //project list img sliding
        $('.project_list>li').hover(function () {
            $(this).siblings().find('.img').slideUp(300);
            if (!$(this).find('.img').is(':visible')) $(this).find('.img').slideDown(300, function () {
                $('.portPlus').fadeIn(200);
            });
        }).mouseleave(function () {
            //
        });

        $('.portPlus').hover(function () {
            $(this).fadeOut(200);
            $(this).next('.portDetails').fadeIn(300);
        });

        $('.portDetails').mouseleave(function () {
            $(this).fadeOut(200);
            $('.portPlus').fadeIn(200);
        });
        // End Portfolio
    }


    $.get('assets/common/content_sitemap.txt', function (data) {
        $('#footUp').append(data);
                $.get('assets/common/content_clientLogin.txt', function (data) {
                    $('#headsUp').append(data);
                        $.get('content_contact.aspx', function (data) {
                            $('#headsUp').append(data);

                            // START AJAX DONE
twit();
                            $('.sitemap_content a').each(function () {
                                if ($(this).attr('href') == getPage()) $(this).addClass('selected');
                            });

                            if ($('#sitemapEmail').attr('id')) {
                                $('#sitemapEmail').attr('autocomplete', 'off');
                                $('#sitemapEmail').val('Your Email Address').css('color', '#ccc');
                                $('#sitemapEmail').focus(function () {
                                    $('#sitemapEmail').css('background', '#2B2B2B');
                                    $(this).val('').css('color', '#fff');
                                });
                                $('#sitemapEmail').blur(function () {
                                    if (!$(this).val()) $(this).val('Your Email Address').css('color', '#ccc');
                                });
                            }
                            $('#ConstantContact').submit(function () {
                                var email = $('#sitemapEmail').val();
                                if (email) {
                                    if (/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(email) == false) {
                                        $('#sitemapEmail').css('background', '#9A4444');
                                        alert('You must provide a valid email address.');
                                        return false;
                                    } else {
                                        $('#sitemapEmail').css('background', '#2B2B2B');
                                    }
                                }
                                return true;
                            });

                            $('.UpDown').click(function () {
                                if ($('#footUp').is(':visible')) {
                                    $('#footUp').slideUp(400);
                                    $(this).attr('title', 'Expand');
                                    $('#foot .selected').removeClass('selected');
                                } else {
                                    $('#footUp').slideDown(400);
                                    $(this).attr('title', 'Minimize');
                                    $(this).addClass('selected');
                                    $('.site_map').addClass('selected');
                                }
                            });

                            $('.site_map').click(function () {
                                if ($('#sitemap_content').is(':visible')) {
                                    $('#footUp').slideUp(400);
                                    $(this).removeClass('selected');
                                    $('.UpDown').removeClass('selected');
                                    $('.UpDown').attr('title', 'Expand');
                                } else {
                                    $(this).addClass('selected');
                                    $('.UpDown').addClass('selected');
                                    $('.UpDown').attr('title', 'Minimize');
                                    $('#footUp .content').hide();
                                    $('#sitemap_content').show();
                                    $('#footUp').slideDown(400);
                                    return false;
                                }
                            });

                            $('.clientLogin').click(function () {
                                $('.contact').removeClass('selected');
                                if ($('#content_clientLogin').is(':visible')) {
                                    $(this).removeClass('selected');
                                    $('#headsUp').slideUp(400);
                                    if ($('#slideTop').attr('id')) $('#slideBottom, #slideTop, .CarSlide').animate({
                                        top: '0'
                                    }, 400);
                                } else {
                                if($('body').attr('id')=='home') $('html,body').css('overflow-y','hidden');
                                    $(this).addClass('selected');
                                    $('#headsUp .content').hide();
                                    $('#content_clientLogin').show();
                                    $('#headsUp').slideDown(400);
                                    if ($('#slideTop').attr('id')) $('#slideBottom, #slideTop, .CarSlide').animate({
                                        top: '170px'
                                    }, 400);
                                    return false;
                                }
                            });

                            $('.contact').click(function () {
                                $('.clientLogin').removeClass('selected');
                                if ($('#content_contact').is(':visible')) {
                                    $(this).removeClass('selected');
                                    $('#headsUp').slideUp(400);
                                    if ($('#slideTop').attr('id')) $('#slideBottom, #slideTop, .CarSlide').animate({
                                        top: '0'
                                    }, 400);
                                } else {
                                if($('body').attr('id')=='home') $('html,body').css('overflow-y','hidden');
                                    $(this).addClass('selected');
                                    $('#headsUp .content').hide();
                                    $('#content_contact').show();
                                    $('#headsUp').slideDown(400);
                                    if ($('#slideTop').attr('id')) $('#slideBottom, #slideTop, .CarSlide').animate({
                                        top: '170px'
                                    }, 400);
                                    return false;
                                }
                            });

                            sizeBanner();
                            $('body').addClass('d');

                            // End AJAX DONE

                        });
                });
    });

    if ($('#siteSearchQ').attr('id')) {

        $('#siteSearchQ').attr('autocomplete', 'off');
        $('#siteSearchQ').val('Search Site');
        $('#siteSearchQ').focus(function () {
            $(this).val('');
        });
        $('#siteSearchQ').blur(function () {
            if (!$(this).val()) $(this).val('Search Site');
        });

        $('#siteSearch').submit(function () {
            if (!$('#siteSearchQ').val() || $('#siteSearchQ').val() == 'Search Site') {
                alert('You must provide a search term.');
                return false;
            }
        });
    }

    if ($('.casestudy_accordion').attr('class')) {
        if (!$('.casestudy_accordion:eq(0)').text().trim()) $('.casestudy_accordion:eq(0)').hide();
    }

    if ($('#caseStudies').attr('class')) {

        var n = Math.round($('#caseStudies').attr('class').replace(/[^0-9]/g, ''));
        if (n < 1) n = 3;
        var v = '';
        var f = '';
        if ($('#caseStudies').attr('class').substr(0, 13) == 'Presentations') f = '&limitPresentations=1';
        else if ($('#caseStudies').attr('class').substr(0, 12) == 'Publications') f = '&limitPublications=1';
        else if ($('#caseStudies').attr('class').substr(0, 8) == 'Services') f = '&limitServices=1';
        $.get('case-studies.aspx?image=1' + f + '&t=' + new Date().getTime() + '&number=' + n + v, function (data) {
            $('#caseStudies').html(data);
        });

    }

    if ($('body.opportunities').hasClass('opportunities')) $('html').css('background', '#C6C6C7');
    if ($('body.welcome').hasClass('welcome')) $('html').css('background', '#C6C6C7');

    $('#inner_content .two_columns div.right address').after('<div class="quoteClose"/>');

    $('a[href*=pdf]').each(function (index) {
        $(this).attr('target', '_blank');
    });

    if (IEVersion > 1) {
        $('body').addClass('IE' + IEVersion);
    }

    if ($('.pmg').hasClass('pmg')) $('.pmg img:eq(0)').attr('src', $('.pmg img:eq(0)').attr('longdesc'));

    if( $('.vidThumb').attr('rel') ) {
     vidID = $('.vidThumb').attr('rel').replace(/\D/g,'');
     $.getJSON('http://www.vimeo.com/api/v2/video/' + vidID + '.json?callback=?', {format: "json"}, function(data) {
      $('.vidThumb').css('background','transparent url('+ data[0].thumbnail_medium + ') no-repeat scroll 50% 0');
     })
    }

    if( $('.resourcesFeaturedProjects').hasClass('resourcesFeaturedProjects') ) {
     $('.resourcesFeaturedProjects').load('/case-studies.aspx?image=1&number=5&keyword=' + escape(getName(getPage()).replace(/  /g,' | ')));
    }

    if($('.cmsName_Home_Page_Text').hasClass('cmsName_Home_Page_Text')) {
      var r = $('.cmsName_Home_Page_Red_Text:eq(0)').html();
      $('.cmsName_Home_Page_Text').html($('.cmsName_Home_Page_Text').html().replace(r,'<span class="red_text">'+r+'</span>'));
    }

    // End Document Ready

});

// Start Vimeo
$(document).ready(function () {

    if ($('.vimeo:eq(0)').attr('href')) {

        var modalCSS = '\
<style type="text/css">\
#modalIframe{\
position: fixed;\
margin: auto;\
top: 90px;\
left: 15%;\
border: 0;\
padding: 22px;\
display: none;\
z-index: 1112;\
opacity: 0;\
visibility: hidden\
}\
#modalIframe iframe {\
margin: 0;\
padding: 0;\
border: 4px solid #eee;\
}\
#modalIframe a {\
position: absolute;\
display: block;\
top: 0;\
right: 0;\
width: 30px;\
height: 30px;\
background: transparent url(assets/images/closebox.png) no-repeat scroll 100% 2px;\
cursor: pointer;\
}\
#modalIframe a:hover {\
background-position: 100% -28px;\
}\
#modalOverlay {\
position: fixed;\
margin: 0;\
top: 0;\
left: 0;\
width: 100%;\
height: 100%;\
padding: 0;\
display: none;\
background: #707070;\
z-index: 1111;\
}\
#modalLoading {\
left: 48%;\
position: fixed;\
z-index: 1114;\
top: 33%;\
width: 32px;\
height: 32px;\
padding: 20px;\
border: 5px solid #111;\
background: #fff;\
display: none;\
}\
</style>';

        $('head').append(modalCSS);
        $('body').append('<div id="modalIframe"></div><div id="modalOverlay"></div><div id="modalLoading"><img src="assets/images/loading.gif" alt="Loading" title="Loading" /></div>');
        $("a.vimeo").click(function () {
            $("#modalIframe").removeAttr('style');
            $("#modalIframe").css("display", "block");
            $("#modalLoading").css("opacity", "0");
            $("#modalLoading").css("display", "block");
            var src = 'http://vimeo.com/moogaloop.swf?clip_id=' + getID($(this).attr('href')) + '&autoplay=1';
            $("#modalIframe").html('<iframe width="960" height="540" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src="' + src + '" onload="modalShow(this)"></iframe><a></a>');
            $("#modalLoading").animate({
                "opacity": 1
            }, 1100);
            $("#modalOverlay").css("opacity", "0");
            $("#modalOverlay").css("display", "block");
            $("#modalOverlay").animate({
                "opacity": .8
            });

            return false;
        });
        $("#modalIframe").click(function () {
            $("#modalIframe").css("visibility", "hidden");
            $("#modalIframe").css("opacity", "0");
            $("#modalIframe").css("display", "none");
            $("#modalIframe").removeAttr('style');
            $("#modalIframe").html('');
            jQuery("#modalOverlay").css("display", "none");
            return false;
        });

    }

});

Array.prototype.getMax = function () {
    return Math.max.apply(Math, this);
}

function getID(str) {
    return str.split("?")[0].match(/\d+/g).getMax();
}

function modalShow(e) {
    var x = ($('body').width() / 2) - (e.width / 2);
    $('#modalIframe').css('left', x + 'px');
    $('#modalIframe').animate({
        "opacity": 1
    });
    $("#modalLoading").css("display", "none");
    $("#modalIframe").css("visibility", "visible");
    $("#modalIframe").css("opacity", "1");
}

// End Vimeo

function sizeBanner() {

    if ($('body').hasClass('submarket')) {
        if ($(window).height() > 900) $('body').removeAttr('class').addClass('submarket').addClass('height1');
        else if ($(window).height() > 850) $('body').removeAttr('class').addClass('submarket').addClass('height2');
        else if ($(window).height() > 800) $('body').removeAttr('class').addClass('submarket').addClass('height3');
        else if ($(window).height() > 750) $('body').removeAttr('class').addClass('submarket').addClass('height4');
        else if ($(window).height() > 700) $('body').removeAttr('class').addClass('submarket').addClass('height5');
        else if ($(window).height() > 670) $('body').removeAttr('class').addClass('submarket').addClass('height6');
        else $('body').removeAttr('class').addClass('submarket').addClass('height7');
    } else {
        if ($(window).height() > 900) $('#banner').css('height', '314px');
        else if ($(window).height() > 870) $('#banner').css('height', '300px');
        else if ($(window).height() > 820) $('#banner').css('height', '280px');
        else if ($(window).height() > 760) $('#banner').css('height', '260px');
        else if ($(window).height() > 710) $('#banner').css('height', '245px');
        else if ($(window).height() > 670) $('#banner').css('height', '230px');
        else $('#banner').css('height', '220px');
    }
    setTimeout("$('.wrapper, #footer, #CarSlidesWrapper').css('opacity', '1')",120);
}
    setTimeout("$('.wrapper, #footer, #CarSlidesWrapper').css('opacity', '1')",850);

var ttt=new Date().getTime();
function sizeHome () {



if(new Date().getTime()<(ttt+150)) return false;
     ttt = new Date().getTime();
     document.location=document.location;
}


function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", - 1);
}

function closeFancy(t) {
    if ($('.selectFancy:eq(' + t + ')').find('em').hasClass('selected')) return false;
    $('.selectFancy:eq(' + t + ')').find('span').slideUp('300');
}

function order_project_list(by) {
    if (by == 'a' && $('.project_sort_a').hasClass('selected')) return false;
    else if (by == 't' && $('.project_sort_t').hasClass('selected')) return false;
    else if (by == 'v' && $('.project_sort_v').hasClass('selected')) return false;

    startWorkCompleted = 0;
    $('.countTotal').html($('.project_list dd').length);
    if ($('.project_list dd').length) $('.countStart').html(1 + startWorkCompleted);
    else $('.countStart').html(0);
    $('.countThrough').html(startWorkCompleted + perPageWorkCompleted);
    $('.project_list dd').show();
    if ($('.project_list dd').length) $('.countStart').html(1 + startWorkCompleted);
    else $('.countStart').html(0);
    var countThrough = startWorkCompleted + perPageWorkCompleted;
    if (countThrough > ($('.project_list dd').length)) countThrough = $('.project_list dd').length;
    $('.countThrough').html(countThrough);
    $('.pagination .previous, .pagination .next').hide();
    if ($('.project_list dd').length > perPageWorkCompleted) $('.pagination .next').show();

    if (!by) {
        by = readCookie('sort');
        if (!by) by = 't';
    } else if (by == 'v') {
        $('.project_sort_v').addClass('selected');
        $('.project_sort_t, .project_sort_a').removeClass('selected');
        var f = 'tt:eq(1)';
    } else if (by == 'a') {
        $('.project_sort_a').addClass('selected');
        $('.project_sort_t, .project_sort_v').removeClass('selected');
        createCookie('sort', 'a');
        var f = 'strong:eq(0)';
    } else {
        $('.project_sort_a, .project_sort_v').removeClass('selected');
        $('.project_sort_t').addClass('selected');
        createCookie('sort', 't');
        var f = 'tt:eq(0)';
    }

    var projects = $('.project_list:eq(0) dd');
    projects.sort(function (a, b) {

        var val1 = stripAccents($(a).find(f).text().toUpperCase());
        var val2 = stripAccents($(b).find(f).text().toUpperCase());

        if (by == 'a') return (val1 < val2) ? -1 : (val1 > val2) ? 1 : 0;
        else return (val1 > val2) ? -1 : (val1 < val2) ? 1 : 0;
    });
    $.each(projects, function (index, row) {
        $('.project_list:eq(0) dl').append(row);
    });

    $('.project_list dd').slice(perPageWorkCompleted).hide();
}

var filters_propgated;

function workCompleted() {

    $('.project_list, .project_list').unbind();
    $('.project_list').html(workCompletedList);
    var filter_markets = new Array();
    var filter_services = new Array();
    var filter_states = new Array();

    $('.project_list dl dd.filter').each(function () {
        var classList = $(this).attr('class').split(/\s+/);
        $(classList).each(function (index, value) {
            if (classList[index] && classList[index].indexOf('market_') === 0) filter_markets[value] = value;
            else if (classList[index] && classList[index].indexOf('service_') === 0) filter_services[value] = value;
            else if (classList[index] && classList[index].indexOf('state_') === 0) filter_states[value] = value;
        })
    });

    if (filters_propgated) return;

    if ($('.project_list').hasClass('project_list')) {
        $('.project_sort_a').each(function () {
            $(this).after('<strong class="project_sort_a">' + $(this).html() + '</strong>');
        });
        $('.project_sort_t').each(function () {
            $(this).after('<strong class="project_sort_t">' + $(this).html() + '</strong>');
        });
        $('.project_sort_a').click(function () {
            order_project_list('a');
        });
        $('.project_sort_t').click(function () {
            order_project_list('t');
        });
        $('.project_list').after('<p class="pagination"><a class="previous">&lt;&lt; Previous</a><a class="next"> Next &gt;&gt;</a></p>');
    }

    filters_propgated = 1;

    filter_markets = sortObj(filter_markets);
    for (var val in filter_markets) {
        if (val && val.indexOf('market_') === 0) {

            $('#selectFancy_Markets').append($('<input type="checkbox" />').attr('id', val).val(val).change(function () {
                workCompletedFilter();
            }));


            $('#selectFancy_Markets').append($('<label />').attr('for', val).html(getName(val)));
            var filter_submarkets = new Array();
            $('.project_list dl dd.filter.' + val).each(function () {
                var classList = $(this).attr('class').split(/\s+/);
                $(classList).each(function (index, value) {
                    if (classList[index] && classList[index].indexOf('submarket_') === 0) {
                        filter_submarkets[value] = value;
                    }
                });
            });
            $('#selectFancy_Markets').append($('<strong />'));
            var marketval = val;
            filter_submarkets = sortObj(filter_submarkets);
            for (var val in filter_submarkets) {
                if (val && val.indexOf('submarket_') === 0 && val.indexOf(marketval) > 0) {
                    $('#selectFancy_Markets strong').last().append($('<input type="checkbox" />').attr('id', val).val(val).change(function () {
                        if ($(this).is(':not(:checked)')) $(this).parent().prev().prev().prop("checked", false);
                        workCompletedFilter();
                    }));
                    $('#selectFancy_Markets strong').last().append($('<label />').attr('for', val).html(getName(getSubmarketName(val))));
                }
            }
        }
    }

    filter_services = sortObj(filter_services);
    for (var val in filter_services) {
        if (val && val.indexOf('service_') === 0) {
            $('#selectFancy_Services').append($('<input type="checkbox" />').attr('id', val).val(val).change(function () {
                workCompletedFilter();
            }));
            $('#selectFancy_Services').append($('<label />').attr('for', val).html(getName(val)));
        }
    }

    filter_states = sortObj(filter_states);
    for (var val in filter_states) {
        if (val && val.indexOf('state_') === 0) {
            $('#selectFancy_States').append($('<input type="checkbox" />').attr('id', val).val(val).change(function () {
                workCompletedFilter();
            }));
            $('#selectFancy_States').append($('<label />').attr('for', val).html(getState(val)));
        }
    }

    // Start Select Fancy
    if ($('.selectFancy').hasClass('selectFancy')) {

        $('.selectFancy strong').filter(function () {
            return $.trim($(this).text()) == ''
        }).remove();
        $('.selectFancy strong').filter(function () {
            return $.trim($(this).text()) != ''
        }).prev().addClass('hasChildren');
        $('.selectFancy').last().addClass('last');
//slide        $('.selectFancy span>strong').hide();
        $('.selectFancy').hover(

        function () {
            $(this).find('em').addClass('selected');
            $(this).find('span').slideDown();
        },

        function () {
            $(this).find('em').removeClass('selected');
            setTimeout('closeFancy(' + $(this).index() + ')', 900);
        });
        $('.selectFancy span>label').hover(

        function () {
            var i = $(this).index('.selectFancy span>label.hasChildren');
            var t = $(this).parent().find('strong:eq(' + i + ')');
//slide            $(this).parent().find('strong').not(t).slideUp();
//slide            t.slideDown(400);
        },

        function () {
            //
        });
        // End Select Fancy
    }
    project_list_init();
}

function workCompletedFilter(x) {
    if (!x) workCompleted();
    var el = '';
    $('.selectFancy span input:checked').each(function () {
        el = el + '.' + $(this).val();
    });
    if(el) $('.project_list dd').not(el).remove();
    project_list_init();
}

function project_list_init() {
    $('.pagination *').unbind();
    order_project_list();
    if ($('.project_list').hasClass('paginate')) {
        $('.pagination .next').click(function (index) {
            $('.pagination .previous').show();
            $('.project_list dd:lt(' + (startWorkCompleted + perPageWorkCompleted) + ')').hide();
            $('.project_list dd:gt(' + (startWorkCompleted + perPageWorkCompleted - 1) + '):lt(' + perPageWorkCompleted + ')').show()
            startWorkCompleted = startWorkCompleted + perPageWorkCompleted;
            if (startWorkCompleted + perPageWorkCompleted >= $('.project_list dd').length) $('.pagination .next').hide();
            if ($('.project_list dd').length) $('.countStart').html(1 + startWorkCompleted);
            else $('.countStart').html(0);
            var countThrough = startWorkCompleted + perPageWorkCompleted;
            if (countThrough > ($('.project_list dd').length)) countThrough = $('.project_list dd').length;
            $('.countThrough').html(countThrough);
        });
        $('.pagination .previous').click(function (index) {
            startWorkCompleted = startWorkCompleted - perPageWorkCompleted;
            if (startWorkCompleted < 1) $('.pagination .previous').hide();
            $('.pagination .next').show();
            if (startWorkCompleted > 2) $('.project_list dd:gt(' + (startWorkCompleted - 1) + ')').show();
            else $('.project_list dd').show();
            $('.project_list dd:gt(' + (startWorkCompleted + perPageWorkCompleted - 1) + ')').hide();
            if ($('.project_list dd').length) $('.countStart').html(1 + startWorkCompleted);
            else $('.countStart').html(0);
            $('.countThrough').html(startWorkCompleted + perPageWorkCompleted);
        });
    }

    $('.selectFancy').each(function () {
        if ($(this).find('span input').length > 0) $(this).show();
        $(this).show(); 
    });

    if ($('.project_list dd').length > 0) $('.person_work').slideDown();
}

function homeCar() {

    $('#home .wrapper').css('backgroundImage', 'none');
    var cnt = $('.CarSlide').length;

    $('#CarSlides').children().each(function (i, li) {
        $('#CarSlides').prepend(li)
    })

    if (cnt > 1) {
        $('.CarSlide').each(function () {
            $('.carousel_nav').append('<a class="CarSlideSelector"></a>');
        });
        $('a.CarSlideSelector:eq(0)').addClass('active');
        setTimeout("carGo = setInterval('carNext()', 5800);", 500);
    }

    $('#slideTop').css('backgroundImage', $('.CarSlide:eq(0)').css('backgroundImage'));
    $('#slideBottom').css('backgroundImage', $('.CarSlide:eq(0)').css('backgroundImage'));

    $('#slideTop').html($('<div />').addClass('carSlideDetails').html($('.carDetails:eq(0)').html()));
    $('#slideBottom').html($('<div />').addClass('carSlideDetails').html($('.carDetails:eq(0)').html() + '<div class="carClient"><p><span>' + $('.carDetails:eq(0) .carClientName').text() + '</span></p></div>'));

    $('#slideTop').hover(function () {
        $('#slideBottom .carClient').fadeOut(250);
        $('#CarSlidesWrapper').addClass('over');
        $('.carSlideDetails p a').addClass('over');
        $('.carSlideDetails p a em').hide().fadeIn(250, function () {
            $('.carSlideDetails p a').addClass('overDone');
        })
    },

    function () {
        $('.carSlideDetails p a em').fadeOut(250, function () {
            $('#CarSlidesWrapper').removeClass('over');
            $('.carSlideDetails p a').removeClass('over').removeClass('overDone');
            $('#slideBottom .carClient').fadeIn(250);
        });
    });

    $('a.CarSlideSelector').click(function () {
        clearInterval(carGo);
        var eq = $(this).index('a.CarSlideSelector');
        $('a.CarSlideSelector').removeClass('active');
        $('a.CarSlideSelector:eq(' + eq + ')').addClass('active');
        $('#slideTop').css('backgroundImage', $('.CarSlide:eq(' + eq + ')').css('backgroundImage'));
        $('#slideTop').html($('<div />').addClass('carSlideDetails').html($('.carDetails:eq(' + eq + ')').html()));
        $('#slideBottom .carClient').fadeOut(250);
        $('#slideTop').hide().fadeIn(400, function () {
            $('#slideBottom .carClient').html('<div class=""><p><span>' + $('.carDetails:eq(' + eq + ') .carClientName').text() + '</span></p></div>').fadeIn(250);
            $('#slideBottom').css('backgroundImage', $('#slideTop').css('backgroundImage'));
        });
    });

    $('a.CarSlideSelector').hover(function () {
        $('#CarSlidesWrapper').addClass('over');
    },

    function () {
        $('#CarSlidesWrapper').removeClass('over');
    });

}

function carNext() {

    if ($('#CarSlidesWrapper').hasClass('over')) return false;
    var eq = Math.round($('a.CarSlideSelector.active').index('a.CarSlideSelector')) + 1;
    if (eq >= $('a.CarSlideSelector').length) eq = 0;
    $('a.CarSlideSelector').removeClass('active');
    $('a.CarSlideSelector:eq(' + eq + ')').addClass('active');
    $('#slideTop').css('backgroundImage', $('.CarSlide:eq(' + eq + ')').css('backgroundImage'));
    $('#slideBottom .carClient').fadeOut(250);
    $('#slideTop').hide().fadeIn(500, function () {
        $('#slideTop').html($('<div />').addClass('carSlideDetails').html($('.carDetails:eq(' + eq + ')').html()));
        $('#slideBottom .carClient').html('<div class=""><p><span>' + $('.carDetails:eq(' + eq + ') .carClientName').text() + '</span></p></div>').fadeIn(250);
        $('#slideBottom').css('backgroundImage', $('#slideTop').css('backgroundImage'));
    });
}

function changeHeight(q, nwh) {
    return q.replace(/height=\d*/, "height=" + nwh);
}

function sortObj(arr) {
    var temp = [],
        i = -1;
    for (var k in arr) {
        if (arr.hasOwnProperty(k)) {
            temp[++i] = {
                "key": (k + ''),
                "value": (arr[k] + ''),
                "numeric": !isNaN(parseInt(k, 10))
            };
        }
    }

    temp.sort(function (a, b) {
        var x = String(a.key).toUpperCase();
        var y = String(b.key).toUpperCase();
        if (a.numeric && b.numeric) {
            x = 1 * x;
            y = 1 * y;
        }
        return (x == y ? 0 : (x > y ? 1 : -1));
    });
    var i = -1,
        limit = temp.length,
        arr = [];
    while (++i < limit) {
        arr[temp[i].key] = temp[i].value;
    }
    return arr;
}

function getState(str) {
 str = str.replace('state_','');
 var s=Array();
 s['AL']='Alabama';
 s['AK']='Alaska';
 s['AS']='America Samoa';
 s['AZ']='Arizona';
 s['AR']='Arkansas';
 s['CA']='California';
 s['CO']='Colorado';
 s['CT']='Connecticut';
 s['DE']='Delaware';
 s['DC']='District of Columbia';
 s['FM']='Micronesia1';
 s['FL']='Florida';
 s['GA']='Georgia';
 s['GU']='Guam';
 s['HI']='Hawaii';
 s['ID']='Idaho';
 s['IL']='Illinois';
 s['IN']='Indiana';
 s['IA']='Iowa';
 s['KS']='Kansas';
 s['KY']='Kentucky';
 s['LA']='Louisiana';
 s['ME']='Maine';
 s['MH']='Islands1';
 s['MD']='Maryland';
 s['MA']='Massachusetts';
 s['MI']='Michigan';
 s['MN']='Minnesota';
 s['MS']='Mississippi';
 s['MO']='Missouri';
 s['MT']='Montana';
 s['NE']='Nebraska';
 s['NV']='Nevada';
 s['NH']='New Hampshire';
 s['NJ']='New Jersey';
 s['NM']='New Mexico';
 s['NY']='New York';
 s['NC']='North Carolina';
 s['ND']='North Dakota';
 s['OH']='Ohio';
 s['OK']='Oklahoma';
 s['OR']='Oregon';
 s['PW']='Palau';
 s['PA']='Pennsylvania';
 s['PR']='Puerto Rico';
 s['RI']='Rhode Island';
 s['SC']='South Carolina';
 s['SD']='South Dakota';
 s['TN']='Tennessee';
 s['TX']='Texas';
 s['UT']='Utah';
 s['VT']='Vermont';
 s['VI']='Virgin Island';
 s['VA']='Virginia';
 s['WA']='Washington';
 s['WV']='West Virginia';
 s['WI']='Wisconsin';
 s['WY']='Wyoming'
 if(s[str]) return s[str];
 return str;
}

function getName(str) {
    // -amp- &
    // -slash- /
    // -dash- -
    return str.replace(/^[^_]+(?=_)/, "").substr(1).replace(/_/g, ' ').replace(/-amp-/g, '&amp;').replace(/-slash-/g, '/').replace(/-dash-/g, '-');
}


function getMarket(key) {
    if (!key || key.length < 5) return '';
    key = key.substr(3);
    key = key.substr(0, key.indexOf('__'));
    return key;
}

function getSubmarketName(key) {
    if (!key || key.length < 5) return '';
    key = key.substr(key.indexOf('__'));
    return key;
}

function getMarketName(key) {
    if (!key || key.length < 5) return '';
    key = key.substr(3, (key.indexOf('__') - 3));
    return key;
}

function fontsFix(x) {
    if ($('#rightSOQ').is('div')) {
        $('#rightSOQ').css('top', $('.left h3.section_title').offset().top + 'px');
    } else if ($('.two_columns:eq(0) .right:eq(0)').hasClass('right') && $('.two_columns:eq(0) .left:eq(0) h2').is('h2')) {
        $('.two_columns:eq(0) .right:eq(0)').css('marginTop', ($('.two_columns:eq(0) .left:eq(0) h2:eq(0)').outerHeight() + 4) + 'px');
    }
    if (x) clearInterval(ff);
}

function projectSearchKeywords_Init(goDeeper) {

    if (!goDeeper) {
        goDeeper = 0;
        startArchive = 0;
        $('.pagination a, .project_sort_a, .project_sort_t, .project_sort_r, .moreFeatured, .moreFeaturedPrev').unbind();
    } else {
        $('#projectResultList .pagination a').unbind();
    }

    $('.project_archive, #projectsFeatures').hide();
    $('#inner_content').css('background','transparent url(/assets/images/loadingBar.gif) no-repeat scroll 270px 200px');
    var q = '&keywords=' + escape($('#project_keywords').val());
    if ($('.project_sort_t.selected').hasClass('selected')) q = q + '&sort=t';
    else if ($('.project_sort_a.selected').hasClass('selected')) q = q + '&sort=a';
    else q = q + '&sort=r';
    q = q + '&start=' + startArchive;
    if (projectSearchList) projectSearchList.abort();

    projectSearchList = $.ajax({
        url: '_projectSearch_list.aspx?' + q,
        success: function (data) {
            $('#inner_content').css('background','none');
            $('#projectResultList').html(data);
            $('.project_archive').show();
            $('.project_results').show();

             $('#projectResultList .pagination a.previous').click(function (index) {
                startArchive = startArchive - 10;
                if (startArchive < 0) startArchive = 0;
                projectSearchKeywords_Init(1);
            });
            $('#projectResultList .pagination a.next').click(function (index) {
                startArchive = startArchive + 10;
                projectSearchKeywords_Init(1);
            });
            $('.project_sort_a, .project_sort_t, .project_sort_r').click(function () {
                $(this).parent().parent().find('a').removeClass('selected');
                $(this).addClass('selected');
                startArchive = 0;
                projectSearchKeywords_Init(1);
            });
        }
    });

    if (goDeeper != 1) {

        $('.search_breadcrumbs ul').html('<li>Search Results:</li><li><a rel="#' + q.replace(/%20/g, "+").replace('&start=0', '').replace(/^\&+/, "") + '">' + $('#project_keywords').val() + '</a><strong>' + $('#project_keywords').val() + '</strong></li>');

        if($('#projectSearch_Filters dd>a.selected').hasClass('selected') || !$('#projectSearch_Filters dd a').is('a')) {
         if (projectSearchFilters) projectSearchFilters.abort();
         projectSearchFilters = $.ajax({
            url: '_projectSearch_Filters.aspx',
            success: function (data) {
                $('#projectSearch_Filters').html(data).css('visibility', 'visible');
                $('.filter_Markets dd a, .filter_Submarkets dd a').click(function () {
                    $(this).siblings().parent().parent().find('a').removeClass('selected');
                    $(this).addClass('selected');
                    startArchive = 0;
                    projectSearch_Init();
                });
                $('.filter_Services dd a, .filter_BuiltStatus dd a, .filter_SustainableStatus dd a').click(function () {
                    if ($(this).hasClass('selected')) $(this).removeClass('selected');
                    else $(this).addClass('selected');
                    startArchive = 0;
                    projectSearch_Init();
                });
            }
         });
        }
    }
}

function projectSearch_Init(goDeeper) {

    if (window.location.hash && new Date().getTime() < (loadedTime + 500)) {
        if (String(window.location.hash).substr(1, 8) == 'keywords') {
            $('#project_keywords').val(window.location.hash.substr(10).replace(/\+/g, ' ').split('&')[0]);
            projectSearchKeywords_Init();
            return;
        }
    }

    $('#project_keywords').val('Search Projects');


    $('.search_breadcrumbs ul').html('<li>Search Results:</li>');
    $('#projectSearch_Filters .filter_Markets dd a.selected').each(function () {
        $('.search_breadcrumbs ul').append('<li><a  rel="#' + getF($(this).attr('class')) + '">' + $(this).html() + '</a><strong>' + $(this).html() + '</strong></li><li>/</li>');
    });
    $('#projectSearch_Filters .filter_Submarkets dd a.selected').each(function () {
        $('.search_breadcrumbs ul').append('<li><a  rel="#' + getF($(this).attr('class')) + '">' + $(this).html() + '</a><strong>' + $(this).html() + '</strong></li><li>/</li>');
    });
    $('#projectSearch_Filters .filter_Services dd a.selected').each(function () {
        $('.search_breadcrumbs ul').append('<li><a  rel="#' + getF($(this).attr('class')) + '">' + $(this).html() + '</a><strong>' + $(this).html() + '</strong></li><li>/</li>');
    });
    $('#projectSearch_Filters .filter_BuiltStatus dd a.selected').each(function () {
        $('.search_breadcrumbs ul').append('<li><a  rel="#' + getF($(this).attr('class')) + '">' + $(this).html() + '</a><strong>' + $(this).html() + '</strong></li><li>/</li>');
    });
    $('#projectSearch_Filters .filter_SustainableStatus dd a.selected').each(function () {
        $('.search_breadcrumbs ul').append('<li><a rel="#' + getF($(this).attr('class')) + '">' + $(this).html() + '</a><strong>' + $(this).html() + '</strong></li><li>/</li>');
    });
    $('.search_breadcrumbs ul li').last().remove();

    $('.clear_filters').unbind();
    $('.clear_filters').click(function (index) {

        startArchive = 0;
        $('.filter_Markets dd a.selected, .filter_Submarkets dd a.selected, .filter_Services dd a.selected, .filter_BuiltStatus dd a.selected, .filter_SustainableStatus dd a.selected').removeClass('selected');
        $('#state').val('');
        projectSearch_Init();
    });

    if (!goDeeper) goDeeper = 0;

    var q = '';

    if (window.location.hash && new Date().getTime() < (loadedTime + 500)) {
        if (window.location.hash.substr(1, 6) == 'market') q = 'market=' + String(window.location.hash).substr(1);
        else if (window.location.hash.substr(1, 9) == 'submarket') q = 'market=' + String(window.location.hash).substr(11).split('__')[0] + '&' + 'submarket=' + String(window.location.hash).substr(1);
    }

    if ($('.filter_Markets dd a.selected').hasClass('selected') && !($('.filter_Markets dd a.selected').hasClass('filter'))) q = q + '&market=' + getF($('.filter_Markets dd a.selected:eq(0)').attr('class'));
    if ($('.filter_Submarkets dd a.selected').hasClass('selected') && !($('.filter_Submarkets dd a.selected').hasClass('filter'))) q = q + '&submarket=' + getF($('.filter_Submarkets dd a.selected:eq(0)').attr('class'));

    if ($('#state').val()) {
        q = q + '&state=' + $('#state').val();
    }

    var t = '';
    if ($('.filter_Services dd a.selected').hasClass('selected')) {
        $('.filter_Services dd a.selected').each(function () {
            t = t + '&Service[]=' + getF($(this).attr('class'));
        });
        q = q + t;
    }
    var t = '';
    if ($('.filter_BuiltStatus dd a.selected').hasClass('selected')) {
        $('.filter_BuiltStatus dd a.selected').each(function () {
            t = t + '&Built[]=' + getF($(this).attr('class'));
        });
        q = q + t;
    }
    var t = '';
    if ($('.filter_SustainableStatus dd a.selected').hasClass('selected')) {
        $('.filter_SustainableStatus dd a.selected').each(function () {
            t = t + '&Sustainable[]=' + getF($(this).attr('class'));
        });
        q = q + t;
    }

    if ($('.project_sort_t.selected').hasClass('selected')) q = q + '&sort=t';
    else if ($('.project_sort_a.selected').hasClass('selected')) q = q + '&sort=a';

    q = q + '&start=' + startArchive;

    if (goDeeper == 0) {

        if (projectSearchFilters) projectSearchFilters.abort();
        projectSearchFilters = $.ajax({
            url: '_projectSearch_Filters.aspx?' + q,
            success: function (data) {

                $('#projectSearch_Filters dl dd a').unbind();

                $('#projectSearch_Filters').html(data).css('visibility', 'visible');
                $('.filter_Markets dd a, .filter_Submarkets dd a').click(function () {
                    $('.filter_Services dd a, .filter_BuiltStatus dd a').removeClass('selected');
                    $(this).siblings().parent().parent().find('a').removeClass('selected');
                    $(this).addClass('selected');
                    startArchive = 0;
                    projectSearch_Init();
                });
                $('.filter_Services dd a').click(function () {
                    if ($(this).hasClass('selected')) $(this).removeClass('selected');
                    else $(this).addClass('selected');
                    startArchive = 0;
                    projectSearch_Init();
                });
                $('.filter_BuiltStatus dd a').click(function () {
                    $('.filter_BuiltStatus dd a.selected').not(this).removeClass('selected');
                    if ($(this).hasClass('selected')) $(this).removeClass('selected');
                    else $(this).addClass('selected');
                    startArchive = 0;
                    projectSearch_Init();
                });
                $('.filter_SustainableStatus dd a').click(function () {
                    $('.filter_SustainableStatus dd a.selected').not(this).removeClass('selected');
                    if ($(this).hasClass('selected')) $(this).removeClass('selected');
                    else $(this).addClass('selected');
                    startArchive = 0;
                    projectSearch_Init();
                });
            }
        });

        if (projectSearchFeatured) projectSearchFeatured.abort();

      if(new Date().getTime() < (loadedTime + 2000)) {
       $('#projectsFeatures .featured_projects ul.framed_list_results li').css('visibility','hidden');
       $('#inner_content').css('background','transparent url(/assets/images/loadingBar.gif) no-repeat scroll 270px 200px');
       setTimeout(function() {
        featuredResults(q);
       }, 800);
      }
      else featuredResults(q);;
    }

    if (projectSearchList) projectSearchList.abort();
    $('#projectResultList').css('visibility', 'hidden');
    projectSearchList = $.ajax({
        url: '_projectSearch_list.aspx?' + q,
        success: function (data) {
            $('.pagination a, .project_sort_a, .project_sort_t, .project_sort_r').unbind();
            $('#projectResultList').html(data).css('visibility', 'visible');
            $('.project_results').show();
            $('.project_archive').show();

            $('#projectResultList .pagination a.previous').click(function (index) {
                startArchive = startArchive - 10;
                if (startArchive < 0) startArchive = 0;
                projectSearch_Init(1);
            });
            $('#projectResultList .pagination a.next').click(function (index) {
                startArchive = startArchive + 10;
                projectSearch_Init(1);
            });
            $('.project_sort_a, .project_sort_t, .project_sort_r').click(function (index) {
                $(this).parent().parent().find('a').removeClass('selected');
                $(this).addClass('selected');
                startArchive = 0;
                projectSearch_Init(1);
            });
        }
    });
}

function getF(str) {
    return str.match(/\w+\_[0-9a-zA-Z\-_]+/)
}

$(document).ready(function () {
    if (window.location.search.substr(0, 9) == '?projects') {
        createCookie('peopleProjects', '1');
    }
    if ($('.person_work').hasClass('person_work')) {
        if (readCookie('peopleProjects') == '1') {
            if ($('#pp').attr('class')) {
                $.get('projectlist.aspx?UserID=' + $('#pp').attr('class').substr(1), function (data) {
                    workCompletedList = data;
                    workCompleted();
                });
                $('#banner .content ul').append('<li><a id="HideProjects">Hide Projects</a></li>');
                $('#HideProjects').click(function () {
                    eraseCookie('peopleProjects');
                    document.location = String(document.location.href.replace(/\?.*/, ''));
                })
            }
        }
    }
});


function featuredResults (q) {
        projectSearchFeatured = $.ajax({
            url: '_projectSearch_featured.aspx?' + q,
            success: function (data) {
                $('#inner_content').css('background','transparent');
                $('.moreFeatured, .moreFeaturedPrev').unbind();

                $('.featured_projects:eq(0)').html(data).css('visibility', 'visible');
                if ($('.featured_projects:eq(0)>ul>li').length < 1) {
                    $('#projectsFeatures').slideUp();

                } else {
                    var w = 711;
                    if ($('.featured_projects>ul.framed_list_results>li').length > 6) w = Math.ceil(($('.featured_projects>ul.framed_list_results>li').length + 1) * 237 / 2);
                    if ($('.featured_projects>ul.framed_list_results>li').length < 4) $('#projectsFeatures .featured_projects').css('height', '161px');
                    else $('#projectsFeatures .featured_projects').css('height', '322px');

                    $('#projectsFeatures').slideDown();

                    if ($('.featured_projects:eq(0)>ul>li').length > 6) $('.moreFeatured').css('visibility', 'visible');
                    else $('.moreFeatured').css('visibility', 'hidden');
                    $('.framed_list_results').width(w + 'px');
                    $('.framed_list_results').css('marginLeft', '0');
                    $('.moreFeaturedPrev').hide();

                    var moving = 0;

                    $('.moreFeatured, .moreFeaturedPrev').unbind();
                    $('.moreFeatured').click(function (index) {
                        $('.moreFeaturedPrev').show().css('visibility', 'visible');

                        if (moving == 1) return false;
                        if ((Math.abs($('.framed_list_results').css('marginLeft').replace('px', '')) + 711) >= (Math.round(w) - 119)) return false;
                        moving = 1;
                        $('.moreFeaturedPrev').show()
                        $('.framed_list_results').animate({
                            marginLeft: '-=711'
                        }, 500, function () {
                            if ((Math.abs($('.framed_list_results').css('marginLeft').replace('px', '')) + 711) >= (Math.round(w) - 119)) $('.moreFeatured').css('visibility', 'hidden');
                            moving = 0;
                        });
                        $('.moreFeaturedPrev').click(function (index) {
                            if (moving == 1) return false;
                            if ($('.framed_list_results').css('marginLeft').replace('px', '') > -1) return false;
                            moving = 1;
                            $('.moreFeatured').css('visibility', 'visible');
                            $('.framed_list_results').animate({
                                marginLeft: '+=711'
                            }, 500, function () {
                                if ($('.framed_list_results').css('marginLeft').replace('px', '') > -1) $('.moreFeaturedPrev').hide();
                                moving = 0;
                            });
                        });
                    });

                    $('.featured_projects:eq(0)>ul>li img').each(function () {
                     $(this).load(function () {
                      if($(this).closest('ul').find('.loaded').hasClass('loaded')) $(this).closest('ul').find('.loaded').last().after($(this).closest('li'));
                      else $(this).closest('ul').prepend($(this).closest('li'));
                      $(this).closest('li').addClass('loaded');
                     });
                    });
                }
            }
        });
}

function stripAccents (str) {
      var in_chrs   = '�������������������������',
      out_chrs      = 'AAAAACEEEEIIIINOOOOOUUUUY', 
      chars_rgx = new RegExp('[' + in_chrs + ']', 'g'),
      transl    = {}, i,
      lookup    = function (m) { return transl[m] || m; };

  for (i=0; i<in_chrs.length; i++) {
    transl[ in_chrs[i] ] = out_chrs[i];
  }
  return str.replace(chars_rgx, lookup);
}


  var _gaq = _gaq || [];
  setTimeout(function(){
    if( String(document.location).indexOf('dsenseid')>0) return false;
    _gaq.push(['_setAccount', 'UA-33350011-1']);
    _gaq.push(['_trackPageview']);
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  },3000);

function getPage() {
     var nPage = window.location.pathname;
     nPage = nPage.substring(nPage.lastIndexOf('/') + 1);
     return nPage;
}



















$(document).ready(function () {
    if (readCookie('SiteEdit') || window.location.search.substr(0, 5) == '?edit') {
        if (readCookie('SiteEdit')) $('body').addClass('cmsEditing');
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = '/cms/assets/cms.js';
        $('head').append(script);
        var css = document.createElement('link');
        css.setAttribute("rel", "stylesheet");
        css.setAttribute("type", "text/css");
        css.setAttribute("href", '/cms/assets/cms.css');
        $('head').append(css);
    }
});

function twit() {
    if($('#twitterfeed').is('script')) $('#twitterfeed').remove();
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.id = 'twitterfeed';
    script.src = 'twitterfeed?results=3&start=0';
    $('head').append(script);
}

function twitLinks(str) {
   var exp = /(\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
   str = str.replace(exp,"<a href='$1' target='_blank'>$1</a>");
   exp = /(^|\s)#(\w+)/g;
   str = str.replace(exp, "$1<a href='https://twitter.com/search?q=%23$2' target='_blank'>#$2</a>");
   exp = /(^|\s)@(\w+)/g;
   str = str.replace(exp, "$1<a href='https://twitter.com/$2' target='_blank'>@$2</a>");
   return str;
}

function twitterFeed(data) {
   var datetime = data.datetime;
   var t = '';
   $.each( data.tweets, function( i, item ) {
     t += '<p>'
     t += '<strong class="twitDateContent">'+ twitLinks(item.content) +'</strong>';
     t += '<em class="twitDateTime">'+ formatDateTime(item.datetime, datetime) +'</em>';
     t += '</p>'
   });
   $('.tweet').html(t);
}

function resetTime(d) {
    d = new Date(d);
    d.setHours(0);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    return d;
}

function formatDateTime(datePast,dateCurrent) {
    var dPast = resetTime(datePast);
    var dCurrent = resetTime(dateCurrent);
    if ( (dCurrent.getDate() + " | " + dCurrent.getMonth() + ' | '+ dCurrent.getFullYear()) ==  (dPast.getDate() + " | " + dPast.getMonth() + ' | '+ dPast.getFullYear()) ) {
     tCurrent = new Date(dateCurrent);
     tPast = new Date(datePast);
     var m = Math.floor((Math.abs(tPast - tCurrent)/1000)/60);
     if (m<5) return 'Just Posted';
     else if (m<60) return m + ' Minutes Ago';
     else if (m<120) return 'One Hour Ago';
     else return Math.ceil(m/60) + ' Hours Ago';
    }
    var dt = (dCurrent-dPast)/(24*3600*1000);
    var arr = ["Today", "Yesterday", "Two Days Ago", "Three Days Ago"];
    return (dt>=0&&dt<4)?arr[dt]:["January","Febuary","March","April","May","June","July","August","September","October","November","December"][dPast.getMonth(dPast)] + " " + dPast.getDate() + ", " + dPast.getFullYear();
}