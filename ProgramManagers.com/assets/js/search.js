$(document).ready(function(){

  var time = new Date().getTime();

  $('#s').attr('autocomplete','off');

  $('#s').val(getQ('q'));
  $('#s').val(getQ('q'));
  $('#s').focusin(function() {
    if( (new Date().getTime() - time) > 500) $(this).val('');
  });

	var config = {
		siteURL		: 'www.programmanagers.com',	// Change this to your site
		searchSite	: true,
		type		: 'web',
		append		: false,
		perPage		: 8,			// A maximum of 8 is allowed by Google
		page		: 0				// The start page
	}
	
	// The small arrow that marks the active search icon:
	var arrow = $('<span>',{className:'arrow'}).appendTo('ul.icons');
	
	$('ul.icons li').click(function(){
		var el = $(this);
		
		if(el.hasClass('active')){
			// The icon is already active, exit
			return false;
		}
		
		el.siblings().removeClass('active');
		el.addClass('active');
		
		// Move the arrow below this icon
		arrow.stop().animate({
			left		: el.position().left,
			marginLeft	: (el.width()/2)-4
		});
		
		// Set the search type
		config.type = el.attr('data-searchType');
		$('#more').fadeOut();
	});
	
	// Adding the site domain as a label for the first radio button:
	$('#siteNameLabel').append(' '+config.siteURL);
	
	// Marking the web search icon as active:
	$('li.web').click();
	
	// Focusing the input text box:
	//$('#s').focus();

	$('#searchForm').submit(function(){
		googleSearch();
		return false;
	});

	$('#searchPages,#searchPDF').change(function(){
		config.searchDocuments = this.id == 'searchPDF';
		googleSearch();
		return false;
	});

	if($('#searchPDF').is(':checked')) config.searchDocuments = this.id == 'searchPDF';
	
	function googleSearch(settings){

		settings = $.extend({},config,settings);
		settings.term = settings.term || $('#s').val().replace(/ /g,'+');
		settings.term = 'site:'+settings.siteURL+' '+settings.term + ' ' + '-casestudy ';

		if(settings.page<1) {
		$('#resultsDiv, #resultsDivProjects, #searchMsg').hide();
		$('#inner_content').css('background','transparent url(/assets/images/loadingBar.gif) no-repeat scroll 170px 200px');


	        if(!$('#searchPDF').is(':checked')) $.get('/_projectSearch_quick.aspx?keywords='+ escape($('#s').val().replace(/ /g,'+')) +'&sort=r&start=0', function(data) {
		 $('#resultsDivProjects').html(data);
		 $('#moreSearchProjects').attr('href','/project-archive#keywords='+ escape($('#s').val().replace(/ /g,'+')) +'&sort=r');
		 $('#inner_content').css('background','none');
		 $('#resultsDiv, #resultsDivProjects, #searchMsg').fadeIn(90);
		});

		 else {
		 $('#resultsDivProjects').html('');
		 $('#inner_content').css('background','none');
		 $('#resultsDiv, #resultsDivProjects, #searchMsg').fadeIn(90);
		 }
		}
		else {
		 $('#inner_content').css('background','none');
		 $('#resultsDivProjects').hide();
		 $('#resultsDiv, #searchMsg').show();
		}
		if($('#searchPDF').is(':checked')) $('#searchMsg').html('Document Search Results');
		else $('#searchMsg').html('General Search Results');
		scroll(0,0);
		$('#resultsDiv').css('visibility','hidden')
			
		if(settings.searchDocuments) settings.term =  'filetype:pdf ' +' ' + settings.term;
		else settings.term = '-filetype:pdf ' +' ' + settings.term;
	
		// URL of Google's AJAX search API
		var apiURL = 'http://ajax.googleapis.com/ajax/services/search/'+settings.type+'?v=1.0&callback=?';
		var resultsDiv = $('#resultsDiv');
		
		$.getJSON(apiURL,{q:settings.term,rsz:settings.perPage,start:settings.page*settings.perPage},function(r){
			
			var results = r.responseData.results;
			$('#more').remove();
			
			if(results.length){
				
				// If results were returned, add them to a pageContainer div,
				// after which append them to the #resultsDiv:
				
				var pageContainer = $('<div>',{className:'pageContainer'});
				
				for(var i=0;i<results.length;i++){
					// Creating a new result object and firing its toString method:
					pageContainer.append(new result(results[i]) + '');
				}
				
				if(!settings.append){
					// This is executed when running a new search, 
					// instead of clicking on the More button:
					resultsDiv.empty();
				}
				
				pageContainer.append('<div class="clear"></div>')
							 .hide().appendTo(resultsDiv)
							 .fadeIn('slow');
				
				var cursor = r.responseData.cursor;
				
				// Checking if there are more pages with results, 
				// and deciding whether to show the More button:
				
				if( +cursor.estimatedResultCount > (settings.page+1)*settings.perPage){
					$('<div class="more">',{id:'more'}).appendTo(resultsDiv).click(function(){


				if(!config.append){
					// This is executed when running a new search, 
					// instead of clicking on the More button:
					resultsDiv.empty();
				}


						googleSearch({append:true,page:settings.page+1});
						$(this).fadeOut();
					});
				}
			}
			else {
				
				// No results were found for this search.
				
				resultsDiv.empty();
				$('<h2/>',{class:'notFound',html:'No Results Found.'}).hide().appendTo(resultsDiv).fadeIn();
			}
                $('#resultsDiv').css('visibility','visible')
		});
	}
	
	function result(r){
		
		// This is class definition. Object of this class are created for
		// each result. The markup is generated by the .toString() method.
		
		var arr = [];
		
		// GsearchResultClass is passed by the google API

		if(config.searchDocuments) var view = 'Download Document';
		else var view = 'View Page';

		var title = r.title.replace('File:','').split('?')[0].split(':')[0].split('(')[0];
			arr = [
			'<div class="webResult">',
			'<h2><a href="',r.unescapedUrl,'">', title ,'</a></h2>',
			'<p>',r.content.replace(/<b>\.\.\.<\/b>/g,'&#8230;') ,'</p>',
			'<a href="',r.unescapedUrl,'">', view ,'</a>',
			'</div>'
			];
		
		// The toString method.
		this.toString = function(){
			return arr.join('');
		}
	}
	
if($('#s').val()) googleSearch();

setTimeout("$('.searchOptions').css('display','block');",300);
	
});

function getQ (name) {
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}