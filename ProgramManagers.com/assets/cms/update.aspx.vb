Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO

Partial Class _update
    Inherits System.Web.UI.Page

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim returnText As String = "<html><head><title></title><script type=""text/javascript"">setTimeout(""parent.parent.cmsModalUpdated()"",110)</script></head><body></body></html>"

        Response.Clear()

        Dim value As String = ""
        Dim filepath As String = Server.MapPath("~") & "assets\cms\data\"

        Dim cmsType As String = ""
        If Not Request.Form("cmsType") Is Nothing Then
            cmsType = Request.Form("cmsType").ToUpper
        End If

        Select Case cmsType
            Case "METADATA"
                filepath &= "Metadata\"
                Dim cmsPage As String = ""
                If Not Request.Form("cmsPage") Is Nothing Then
                    cmsPage = Request.Form("cmsPage")
                End If
                For i As Integer = 0 To Request.Form.Count - 1
                    If Request.Form.Keys(i).Length > 8 Then
                        Select Case Request.Form.Keys(i).Substring(0, 8).ToUpper
                            Case "METADATA"
                                If Not Directory.Exists(filepath) Then
                                    Try
                                        Directory.CreateDirectory(filepath)
                                    Catch ex As Exception
                                    End Try
                                End If

                                Dim thefile As String = filepath & Request.Form.Keys(i) & "_" & cmsPage & ".txt"
                                Dim fileinfo As FileInfo = New FileInfo(thefile)
                                If fileinfo.Exists Then
                                    fileinfo.Delete()
                                End If
                                Dim fs As FileStream = Nothing
                                If (Not File.Exists(thefile)) Then
                                    fs = File.Create(thefile)
                                    Using fs
                                    End Using
                                    If File.Exists(thefile) Then
                                        Using sw As StreamWriter = New StreamWriter(thefile)
                                            sw.Write(Request.Form.Item(i))
                                        End Using
                                    End If
                                End If
                        End Select
                    End If
                Next

            Case "TEXT"
                filepath &= "Text\"
                For i As Integer = 0 To Request.Form.Count - 1
                    If Request.Form.Keys(i).IndexOf("_") > -1 Then
                        Select Case Request.Form.Keys(i).Split("_")(0)
                            Case "Text"
                                If Not Directory.Exists(filepath) Then
                                    Try
                                        Directory.CreateDirectory(filepath)
                                    Catch ex As Exception
                                    End Try
                                End If

                                Dim thefile As String = filepath & Request.Form.Keys(i) & ".txt"
                                Dim fileinfo As FileInfo = New FileInfo(thefile)
                                If fileinfo.Exists Then
                                    fileinfo.Delete()
                                End If
                                Dim fs As FileStream = Nothing
                                If (Not File.Exists(thefile)) Then
                                    fs = File.Create(thefile)
                                    Using fs
                                    End Using
                                    If File.Exists(thefile) Then
                                        Using sw As StreamWriter = New StreamWriter(thefile)
                                            sw.Write(Request.Form.Item(i))
                                        End Using
                                    End If
                                End If
                        End Select
                    End If
                Next

            Case "TEXTSET"
                filepath &= "Textset\"
                Dim cmsTextsetName As String = ""
                If Not Request.Form("cmsTextsetName") Is Nothing Then
                    cmsTextsetName = Request.Form("cmsTextsetName")
                End If
                For Each files As String In Directory.GetFiles(filepath & cmsTextsetName & "\")
                    File.Delete(files)
                Next
                For i As Integer = 0 To Request.Form.Count - 1
                    If Request.Form.Keys(i).IndexOf("_") > -1 Then
                        Dim keyName() As String = Request.Form.Keys(i).Split("_")
                        Dim setFolder As String = "Textset"
                        For j As Integer = 1 To keyName.Length - 2
                            setFolder &= "_" & keyName(j)
                        Next
                        setFolder &= "\"
                        Select Case keyName(0)
                            Case "Text"
                                If Not Directory.Exists(filepath & setFolder) Then
                                    Try
                                        Directory.CreateDirectory(filepath & setFolder)
                                    Catch ex As Exception
                                    End Try
                                End If

                                Dim thefile As String = filepath & setFolder & Request.Form.Keys(i) & ".txt"
                                Dim fileinfo As FileInfo = New FileInfo(thefile)
                                If fileinfo.Exists Then
                                    fileinfo.Delete()
                                End If
                                Dim fs As FileStream = Nothing
                                If (Not File.Exists(thefile)) Then
                                    fs = File.Create(thefile)
                                    Using fs
                                    End Using
                                    If File.Exists(thefile) Then
                                        Using sw As StreamWriter = New StreamWriter(thefile)
                                            sw.Write(Request.Form.Item(i))
                                        End Using
                                    End If
                                End If
                        End Select
                    End If
                Next

            Case "IMAGE"
                Dim width As String = ""
                Dim height As String = ""
                Dim filename As String = ""
                Dim fileExt As String = ""
                If Not Request.Form("width") Is Nothing Then
                    width = Request.Form("width")
                End If

                If Not Request.Form("height") Is Nothing Then
                    height = Request.Form("height")
                End If
                If Not Request.Form("filename") Is Nothing Then
                    filename = Request.Form("filename")
                End If
                If Request.Files.Count > 0 Then
                    Select Case Request.Files(0).ContentType
                        Case "image/gif"
                            fileExt = ".gif"
                        Case "image/png"
                            fileExt = ".png"
                        Case "image/jpeg"
                            fileExt = ".jpg"
                        Case Else
                            returnText = "<html><head><title></title><script type=""text/javascript"">setTimeout(""alert('You must upload a valid jpg or png or gif image').parent.parent.cmsModalUpdated()"",110)</script></head><body></body></html>"
                    End Select
                    If fileExt <> "" Then
                        filepath &= "Image\"
                        If Not Directory.Exists(filepath) Then
                            Try
                                Directory.CreateDirectory(filepath)
                            Catch ex As Exception
                            End Try
                        End If

                        Dim thefile As String = filepath & filename & ".png"
                        Dim fileinfo As FileInfo = New FileInfo(thefile)
                        If fileinfo.Exists Then
                            fileinfo.Delete()
                        End If
                        thefile = filepath & filename & ".jpg"
                        fileinfo = New FileInfo(thefile)
                        If fileinfo.Exists Then
                            fileinfo.Delete()
                        End If
                        thefile = filepath & filename & ".gif"
                        fileinfo = New FileInfo(thefile)
                        If fileinfo.Exists Then
                            fileinfo.Delete()
                        End If
                        Dim tempfile As String = Guid.NewGuid.ToString()
                        Request.Files(0).SaveAs(filepath & tempfile & fileExt)
                        Dim fileargs As String = filepath & tempfile & fileExt & " -resize " & width & "x" & height & " -unsharp 0x.5 -strip -quality 90 " & filepath & filename & fileExt
                        Dim proc As New Diagnostics.Process()
                        proc.StartInfo.Arguments = fileargs
                        proc.StartInfo.FileName = "C:\Program Files\ImageMagick-6.7.8-Q16\convert.exe"
                        proc.StartInfo.UseShellExecute = False
                        proc.StartInfo.CreateNoWindow = True
                        proc.StartInfo.RedirectStandardOutput = True
                        proc.StartInfo.RedirectStandardError = True
                        proc.Start()
                        proc.BeginOutputReadLine()
                        Dim output As String = proc.StandardError.ReadToEnd()
                        proc.WaitForExit()
                        thefile = filepath & tempfile & fileExt
                        fileinfo = New FileInfo(thefile)
                        If fileinfo.Exists Then
                            fileinfo.Delete()
                        End If

                    End If
                End If

        End Select

        Response.Write(returnText)
        Response.Flush()
        Response.End()

    End Sub

End Class