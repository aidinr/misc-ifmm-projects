history.navigationMode = 'compatible';
$('*').unbind();

$(window).unload(function() {
 $('*').unbind();
 $('body','html').val('');}
);

var v = new Date().getTime();
var max = 0;

$(document).ready(function() {

 $('#editForm').submit(function() {
  $('.wrapper').fadeOut(300);
  $('body .wrapper').css('background','#fff').fadeOut(50);
  $('body').css('background','#fff url(loading.gif) no-repeat fixed 50% 50%');
  return true;
 });

 $('h1.heading').html('Edit &ldquo;' + niceText(getParameterByName('name')) + '&rdquo;');

 if (getParameterByName('type')=='TextSingle') {
  $.get('/assets/cms/data/Text/Text_' + getParameterByName('name') + '.txt?v='+v, function(data) {
   $('#fields').html('<label>'+ niceText(getParameterByName('name')) +':</label><input type="text" name="Text_'+ getParameterByName('name') +'" value="'+ htmlEncode(data) +'" autocomplete="off" /><input type="hidden" name="cmsType" value="text" />');
   setTimeout("parent.cmsModalShow()",500);
  });
 }
 else if (getParameterByName('type')=='TextMulti') {
  $.get('/assets/cms/data/Text/Text_' + getParameterByName('name') + '.txt?v='+v, function(data) {
   $('#fields').html('<label>'+ niceText(getParameterByName('name')) +':</label><textarea rows="99" cols="99" name="Text_'+ getParameterByName('name') +'">'+ htmlEncode(data)  +'</textarea><input type="hidden" name="cmsType" value="text" />');
   setTimeout("parent.cmsModalShow()",500);
  });
 }
 else if (getParameterByName('type')=='Image') {
  $('#fields').html( '<label>'+ niceText(getParameterByName('name')) +':</label><span><input type="file" name="Image_'+ getParameterByName('name') +'"/></span><input type="hidden" name="width" value="'+ getParameterByName('width') +'" /><input type="hidden" name="height" value="' + getParameterByName('height') + '" /><input type="hidden" name="filename" value="image_' + getParameterByName('name') + '" /><input type="hidden" name="cmsType" value="image" />');
   setTimeout("parent.cmsModalShow()",500);
 }
 else if (getParameterByName('type')=='Metadata') {
  $('h1.heading').append(' Metadata');
  $('#fields').html('<div class="set"></div><input type="hidden" name="cmsType" value="metadata" /><input type="hidden" name="cmsPage" value="'+ getParameterByName('page') +'" />');
   $.get('/assets/cms/data/Metadata/MetadataTitle_' + getParameterByName('page') + '.txt?v='+v, function(data) {
    $('#fields .set').append('<label>Page Title: </label><input type="text" name="metadataTitle" value="'+ htmlEncode(data) +'" autocomplete="off" />');
    $.get('/assets/cms/data/Metadata/MetadataDescription_' + getParameterByName('page') + '.txt?v='+v, function(data) {
     $('#fields .set').append('<label>Page Description: </label><textarea rows="99" cols="99" name="metadataDescription">'+ htmlEncode(data)  +'</textarea>');
     $.get('/assets/cms/data/Metadata/MetadataKeywords_' + getParameterByName('page') + '.txt?v='+v, function(data) {
      $('#fields .set').append('<label>Page Keywords: </label><textarea rows="99" cols="99" name="metadataKeywords">'+ htmlEncode(data)  +'</textarea>');
      setTimeout("parent.cmsModalShow()",500);
     });
    });
   });
 }
 else if (getParameterByName('type')=='Textset') {
  $('p.actions').prepend('<input type="button" value="Add" id="add" /><input type="hidden" name="cmsType" value="textset" /><input type="hidden" name="cmsTextsetName" value="Textset_' + getParameterByName('name') + '" />');

  $.get('/assets/cms/CountTextset.aspx?v='+v+'&name=Text_' + getParameterByName('name'), function(data) {
   max = Math.floor(data.replace(/[^\d.]/g, '')/2)-1;
   loadTextset_xa();
  });

  $('#add').click(function() {
  $('#fields').append('<p class="item"><a class="itemDown"></a><a class="itemUp"></a><input type="text" autocomplete="off" name="Text_'+ getParameterByName('name') +'_xa'+ $('body.editor form #fields .item').length +'" value="" /><textarea rows="99" cols="99" name="Text_'+ getParameterByName('name')+'_xb'+ $('body.editor form #fields .item').length +'"></textarea></p>');
   UpDownRemove ();
   return false;
  });
 }

 $('.boxClose, #cancel').click(function() {
  parent.cmsModalClose();
  return false;
 });

 $('input').attr('autocomplete','off');

});

function niceText (str) {
 str = str.replace(/_/g,' ');
 return str;
}

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function UpDownRemove () {

  $('#fields .item .itemUp').first().addClass('first');
  $('#fields .item .itemDown').last().addClass('last');
  $('#fields .item .itemDown.last').removeClass('last');
  $('#fields .item .itemDown').last().addClass('last');

  $('.itemUp').unbind();
  $('.itemUp').click(function() {
   var xi = $(this).index('.itemUp');
   var xa = $('body.editor form #fields .item input:eq('+xi+')').val();
   var xb = $('body.editor form #fields .item textarea:eq('+xi+')').val();
   var xaAbove = $('body.editor form #fields .item input:eq('+(xi-1)+')').val();
   var xbAbove = $('body.editor form #fields .item textarea:eq('+(xi-1)+')').val();
   $('body.editor form #fields .item input:eq('+xi+')').val(xaAbove);
   $('body.editor form #fields .item textarea:eq('+xi+')').val(xbAbove);
   $('body.editor form #fields .item input:eq('+(xi-1)+')').val(xa);
   $('body.editor form #fields .item textarea:eq('+(xi-1)+')').val(xb);
   return false;
  });

  $('.itemDown').unbind();
  $('.itemDown').click(function() {
   var xi = $(this).index('.itemDown');
   var xa = $('body.editor form #fields .item input:eq('+xi+')').val();
   var xb = $('body.editor form #fields .item textarea:eq('+xi+')').val();
   var xaBelow = $('body.editor form #fields .item input:eq('+(xi+1)+')').val();
   var xbBelow = $('body.editor form #fields .item textarea:eq('+(xi+1)+')').val();
   $('body.editor form #fields .item input:eq('+xi+')').val(xaBelow);
   $('body.editor form #fields .item textarea:eq('+xi+')').val(xbBelow);
   $('body.editor form #fields .item input:eq('+(xi+1)+')').val(xa);
   $('body.editor form #fields .item textarea:eq('+(xi+1)+')').val(xb);
   return false;
  });

  $('.itemRemove').remove();
  if($('body.editor form #fields .item').length>1) {
   $('.itemUp').last().before('<a class="itemRemove"></a>');
   $('.itemRemove').click(function() {
    $('body.editor form #fields .item').last().remove();
    UpDownRemove ();
    return false;
   });
  }
}

function loadTextset_xa (xi) {
 if(!xi) xi=0;
 $.get('/assets/cms/data/Textset/Textset_' + getParameterByName('name') + '/Text_'+ getParameterByName('name') +'_xa'+ xi +'.txt?v='+v, function(xa) {
  loadTextset_xb (xi, xa);
 });
}

function loadTextset_xb (xi, xa) {
 if(!xi) xi=0;
 $.get('/assets/cms/data/Textset/Textset_' + getParameterByName('name') + '/Text_'+ getParameterByName('name') +'_xb'+ xi +'.txt?v='+v, function(xb) {
  $('#fields').append('<p class="item"><a class="itemDown"></a><a class="itemUp"></a><input type="text" autocomplete="off" name="Text_'+ getParameterByName('name') +'_xa'+ $('body.editor form #fields .item').length +'" value="'+ htmlEncode(xa) +'" /><textarea rows="99" cols="99" name="Text_'+ getParameterByName('name')+'_xb'+ $('body.editor form #fields .item').length +'">'+ htmlEncode(xb) +'</textarea></p>');
  if(xi < max ) loadTextset_xa (xi+1);
  else {
   UpDownRemove();
   setTimeout("parent.cmsModalShow()",500);
  }
 });
}

function htmlEncode(str){
 if(str) return String(str).replace(/&/g, '&#38;').replace(/</g, '&#60;').replace(/>/g, '&#62;').replace(/"/g, '&#34;').replace(/'/g, '&#39;');
 else return '';
}