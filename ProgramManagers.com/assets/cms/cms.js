// Start Modal //
$('body').append('<a class="cmsLogout">LOGOUT</a>');
$('body .cms').prepend('<a class="cmsEdit">EDIT</a>');
$('body.cms').prepend('<a class="cmsEditMetadata">METADATA</a>');
$('img.cms').before('<a class="cmsEdit">EDIT</a>');

$('body').append('<div id="cmsModalIframe"><iframe width="100" height="100" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" src="/assets/cms/loading.gif"></iframe></div><div id="cmsModalOverlay"></div><div id="cmsModalLoading"></div>');

function cmsModalShow() {
 $('#cmsModalLoading').animate({opacity: 0}, 400, function() {$('#cmsModalLoading').hide()});

 var x = ($('body').width() / 2 ) - ($('#cmsModalIframe').outerWidth()/2);
 $('#cmsModalIframe').css('left',x+'px').css('visibility', 'visible').css('display','block').css('opacity','0').animate({'opacity': 1},800);
}

function cmsModalUpdated() {
 $('#cmsModalIframe').fadeOut(400);
 $('#cmsModalOverlay').animate({opacity: 1}, 400, function() {document.location=String(document.location)+'?&t=' + new Date().getTime()});
 return true;
}

function cmsModalClose(x) {
 if(x) {
  $('#cmsModalIframe').fadeOut(400);
  $('#cmsModalOverlay').animate({opacity: 0}, 400, function() {$('#cmsModalOverlay').hide()});
  return false;
 }
 var answer = confirm('Any changes you made will be lost unless you save them.')
 if (answer) {
  $('#cmsModalIframe').fadeOut(400);
  $('#cmsModalOverlay').animate({opacity: 0}, 400, function() {$('#cmsModalOverlay').hide()});
 }
 return false;
}

$(document).keyup(function(e) {
 if (e.keyCode == 27 && $('#cmsModalIframe').is(':visible')) {
  cmsModalClose();
  return false;
 }
});

$('a.cmsLogout').click(function () {
 $.get('/assets/cms/logout.aspx?v=' + new Date().getTime(), function(data) {
  document.location=document.location;
 });
 return false;
});

$('a.cmsEdit, a.cmsEditMetadata').click(function () {
 if($(this).parent().hasClass('cms')) var obj = $(this).parent();
 else if($(this).next().hasClass('cms')) var obj = $(this).next();

 var WidthHeight = '';
 if( getVal('cmsWidth_', obj.attr('class')) || getVal('cmsHeight_', obj.attr('class'))) {
  WidthHeight = '&width=' + getVal('cmsWidth_', obj.attr('class')) + '&height=' + getVal('cmsHeight_', obj.attr('class'));
 }

 var h = 400;
 h= $(window).height() - 170;

 if ($(this).hasClass('cmsEditMetadata')) h=406;
 else if (getVal('cmsType_', obj.attr('class'))=='TextSingle') h=180;
 else if (getVal('cmsType_', obj.attr('class'))=='Image') h=210;
 else if (getVal('cmsType_', obj.attr('class'))=='TextMulti') h=400;

 var t = '';
 var n = '';
 var p = '';

 if ($(this).hasClass('cmsEditMetadata')) {
  var t = 'Metadata';
  var p = getPage(document.location); 
  if(p) var n = p.capitalize();
  else var n = String(window.location.hostname);

 }
 else {
  var t = getVal('cmsType_', String(obj.attr('class')));
  var n = getVal('cmsName_', String(obj.attr('class')));
 }

 $('#cmsModalLoading').css('opacity','.4').show().animate({'opacity': 1},300);
 $('#cmsModalOverlay').css('opacity', '0').show().animate({'opacity': .8},300);
 $('#cmsModalIframe').css('display', 'block').css('visibility','visible').css('opacity','0');
 $('#cmsModalIframe iframe:eq(0)').attr('width','800').attr('height',h).attr('src','/assets/cms/cms.html?name='+ encodeURIComponent(n) + WidthHeight + '&type=' + encodeURIComponent(t) + '&page=' + encodeURIComponent(p) );
 return false;
});

function getVal (prefix, str) {
 return String(new RegExp(prefix + "[^\w+) ]*").exec(str)).substr(prefix.length);
}

// End Modal //

// Start Login //

if( window.location.search.substr(0,5)=='?edit' && readCookie('SiteEdit')!='1') {
 $('.cmsEdit, .cmsEditMetadata, .cmsLogout').remove();
 $('.cms').removeClass('cms');
 setTimeout("cmsLogin()",800);
}

else if( window.location.search.substr(0,5)=='?edit') {
 document.location=String(document.location).split('?')[0].split('#')[0];
}

function cmsLogin () {
 $('#cmsModalLoading').css('opacity','.3').show().animate({'opacity': 1},300);
 $('#cmsModalOverlay').css('opacity', '0').show().animate({'opacity': .8},300);
 $('#cmsModalIframe').css('display', 'block').css('visibility','visible').css('opacity','0');
 $('#cmsModalIframe iframe:eq(0)').attr('width','800').attr('height','240').attr('src','/assets/cms/login.html');
 return false;
}

// End Login //

function getPage () {
 var nPage = window.location.pathname;
 nPage = nPage.substring(nPage.lastIndexOf('/') + 1);
 //nPage = nPage.substring(0, nPage.indexOf('.'));
 return nPage;
}

String.prototype.capitalize = function(){
  return this.replace(/(^|[_\-])([a-z])/g , function(m,p1,p2){ return p1+p2.toUpperCase(); } );
};