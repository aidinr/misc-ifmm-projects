100% company-paid PPO medical, prescription, and dental insurance premiums
Voluntary vision insurance
Company-paid disability and life insurance
Flexible spending accounts for medical and dependent care expenses
Company-paid contributions to traditional and Roth 401(k) retirement accounts
Employee assistance program
Vacation, sick and holiday pay
Commuter benefits
Tuition reimbursement
Reimbursement for professional memberships/certifications
In-house training
Performance-based bonuses
Flextime and telecommuting