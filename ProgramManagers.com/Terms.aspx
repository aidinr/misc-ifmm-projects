﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Terms.aspx.vb" Inherits="Terms" %>
<%@ Import Namespace="System.IO" %>
<% 

    Dim thePage As String = "Terms"
    Dim filepath As String = Server.MapPath("~") & "cms\data\"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        <link type="text/css" rel="stylesheet" href="assets/css/reset.css"/>    
        <link type="text/css" rel="stylesheet" href="assets/css/style.css"/>    
        <script src="assets/js/jquery.js" type="text/javascript"></script>  
        <script src="assets/js/js.js" type="text/javascript"></script>         
        <title><%=CMSFunctions.GetMetaTitle(filepath, thePage)%>: Brailsford &amp; Dunlavey</title>
        <meta name="description" content="<%=CMSFunctions.GetMetaDescription(filepath, thePage)%>" />      
        <meta name="keywords" content="<%=CMSFunctions.GetMetaKeywords(filepath, thePage)%>" />         
    </head>
    <body class="cms">  
        
        <div id="headsUp"></div>
        
        <div class="wrapper">
            <div id="headLeft"></div>
            <div id="headRight"></div>
            <div id="head">
                <div id="header">
                    <div class="content">
                        <h1 title="B&amp;D Inspire. Empower. Advance.">
                            <a href="./" title="B&amp;D Inspire. Empower. Advance.">&nbsp;</a>
                        </h1>
<p>
<a href="offices" title="Contact Us" class="locations">Contact Us</a>
</p>
                            

                    </div>
                </div>
                <div id="menu">
                    <div class="content">
                        <ul>
                            <li><a href="about-us" title="">ABOUT US</a></li>
                            <li><a href="people-overview" title="">PEOPLE</a></li>
                            <li><a href="services" title="">SERVICES</a></li>
                            <li><a href="portfolio" title="">PORTFOLIO</a></li>
                            <li><a href="resources" title="">RESOURCES</a></li>
                            <li class="last"><a href="careers" title="">CAREERS</a></li>
                        </ul>
                        <form method="get" id="siteSearch" action="search">
                            <p>
                                <input type="text" name="q" id="siteSearchQ" value="" />
                                <input  id="searchSubmit" type="submit" name="search" value="Search"  />
                            </p>
                        </form>
                    </div>
                </div>                
            </div> 

            <div id="banner" class="<%="cms cmsType_Image cmsName_Terms_Banner_Image"%>" style="background-image: url(cms/data/Sized/best/1920x314/65/F4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Terms_Banner_Image")%>)">

                <div class="content">
                    <h1>Terms of Use</h1>
                    <ul>
                        <li><a href="terms" title="">Terms</a></li>
                        <li><a href="privacy" title="">Privacy Statement</a></li>
                    </ul>                    	                                     
                </div>
            </div>  
            
            <div id="inner_content">
                <div class="two_columns">
                    <div class="left">
                        <h2>Terms of Use</h2>
                        <div class="text">                                                    
                            <p class="cms cmsType_TextMulti cmsName_Terms_Page_Introduction_Text">
                                
                            </p>
                            <ul class="cms cmsFill_Static cmsType_TextMulti cmsName_Terms_Page_Bulleted_Items">
                                <% Dim s As String = CMSFunctions.GetTextFormatBulletList(filepath, "Terms_Page_Bulleted_Items")%>
                                <% Dim words As String() = Split(s, vbNewLine)
                                    Dim word As String
                                    For Each word In words%>
                                    <li><%=word%></li>
                                <% Next word%>
                            </ul>
                        </div>
                    </div>
                    <div class="right">
                        <h3 class="section_title">Featured Case Studies</h3>
                        <div id="caseStudies" class="show3"></div>
                    </div>
                </div>
            </div>

            <div class="push"></div>
        </div>
   
        <div id="footer">
            <div id="footLeft"></div>
            <div id="footRight"></div>
            <div id="foot">
                <div id="footUp"></div>
                <div class="content">
                    <a class="UpDown" title="Expand"></a><a class="UpDown" title="Expand"></a><a class="site_map" title="Site Map">SITE MAP</a>
                    <p>
                        <a href="http://venues.programmanagers.com/" class="venues">B&amp;D VENUES</a>
                        <a class="visit_bd selected">B&amp;D</a><a href="http://www.centersusa.com/" class="visit_centers">CENTERS</a>
                    </p>
                    <address>Copyright Brailsford  &amp; Dunlavey, Inc. <span>&copy;</span></address>
                </div>                
            </div>
        </div>        
      
    </body>
</html>
