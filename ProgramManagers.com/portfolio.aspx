﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="portfolio.aspx.vb" Inherits="portfolio"   %>
<%@ Import Namespace="System.IO" %>
<% 

   

    Dim thePage As String = "Portfolio"
    Dim filepath As String = Server.MapPath("~") & "cms\data\"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        <link type="text/css" rel="stylesheet" href="assets/css/reset.css"/>    
        <link type="text/css" rel="stylesheet" href="assets/css/style.css"/>    
        <script src="assets/js/jquery.js" type="text/javascript"></script>  
        <script src="assets/js/js.js" type="text/javascript"></script>         
        <title><%=CMSFunctions.GetMetaTitle(filepath, thePage)%>: Brailsford &amp; Dunlavey</title>
        <meta name="description" content="<%=CMSFunctions.GetMetaDescription(filepath, thePage)%>" />      
        <meta name="keywords" content="<%=CMSFunctions.GetMetaKeywords(filepath, thePage)%>" />
    </head>
    <body class="cms">  

        <div id="headsUp"></div>

        <div class="wrapper">
            <div id="headLeft"></div>
            <div id="headRight"></div>
            <div id="head">
                <div id="header">
                    <div class="content">
                        <h1 title="B&amp;D Inspire. Empower. Advance.">
                            <a href="./" title="B&amp;D Inspire. Empower. Advance.">&nbsp;</a>
                        </h1>
<p>
<a href="offices" title="Contact Us" class="locations">Contact Us</a>
</p>
                            
                    </div>
                </div>
                <div id="menu">
                    <div class="content">
                        <ul>
                            <li><a href="about-us" title="">ABOUT US</a></li>
                            <li><a href="people-overview" title="">PEOPLE</a></li>
                            <li><a href="services" title="">SERVICES</a></li>
                            <li><a href="portfolio" title="">PORTFOLIO</a></li>
                            <li><a href="resources" title="">RESOURCES</a></li>
                            <li class="last"><a href="careers" title="">CAREERS</a></li>
                        </ul>
                        <form method="get" id="siteSearch" action="search">
                            <p>
                                <input type="text" name="q" id="siteSearchQ" value="" />
                                <input  id="searchSubmit" type="submit" name="search" value="Search"  />
                            </p>
                        </form>
                    </div>
                </div>                
            </div> 
        
        <div id="banner" class="<%="cms cmsType_Image cmsName_Portfolio_Banner_Image"%>" style="background-image: url(cms/data/Sized/best/1920x314/65/F4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Portfolio_Banner_Image")%>)">
                <div class="content">
                    <h1>PORTFOLIO</h1>
                    <ul> 
                        <li><a href="portfolio" title="">Overview</a></li>
                        <li><a title="">HIGHER EDUCATION</a></li>
                        <li><a title="">GOVERNMENT / MUNICIPAL</a></li>
                        <li><a title="">PRE K - 12</a></li>
                        <li><a title="">PRIVATE</a></li>
                        <li><a title="">B&amp;D VENUES</a></li>
                    </ul>                    	                                     
                </div>
            </div>  

            <div id="inner_content">

                <h2 class="red_heading cms cmsType_TextSingle cmsName_Portfolio_Page_Heading">
                   
                </h2>
                <div class="text">                                                    
                <p class="cms cmsType_TextMulti cmsName_Portfolio_Page_Content">
                   
                </p>                                            
                </div>
                
                <ul class="project_list">
                    <li>
                        <div class="project_title">
                            <h1>HIGHER EDUCATION</h1>
                            <a href="#" title="" id="HESoQLink" runat="server" >Statement of Qualifications</a>
                            <p class="allProjects"><a href="project-archive#market_Higher_Education">See all projects</a></p>
                        </div>
                        <div class="project_description cms cmsType_TextMulti cmsName_Portfolio_Page_Higher_Education_Content">
                            
                        </div>
                        <div class="projects_menu">
                            <ol>
                                <li><a href="submarket_Higher_Education__Academic_-slash-_Administrative">Academic / Administrative</a></li>
                                <li><a href="submarket_Higher_Education__Athletics">Athletics</a></li>
                                <li><a href="submarket_Higher_Education__Campus_Edge">Campus Edge</a></li>
                                <li><a href="submarket_Higher_Education__Foodservice_-slash-_Dining">Foodservice / Dining</a></li>
                                <li><a href="submarket_Higher_Education__Recreation_-amp-_Wellness">Recreation &amp; Wellness</a></li>
                                <li><a href="submarket_Higher_Education__Retail">Retail</a></li>
                                <li><a href="submarket_Higher_Education__Student_Housing">Student Housing</a></li>
                                <li><a href="submarket_Higher_Education__Student_Unions">Student Unions</a></li>
                                <li><a href="submarket_Higher_Education__Venues">Venues</a></li>
                                <li><a href="submarket_Higher_Education__Workforce_Housing">Workforce Housing</a></li>
                            </ol>
                        </div>
                        <div class="clear_and_margin">&nbsp;</div>
                        <div class="img"><img id="HEImage" runat="server" alt=" " src="assets/images/portfolio_HigherEducation.jpg" />
                           <div class="portPlus"><a></a></div>
                           <div class="portDetails">
                            <p>
                              <asp:Literal ID="HEImageText" runat="server"></asp:Literal>
                            </p>
                           </div>
                         </div>
                    </li>                    
                    <li>
                        <div class="project_title">
                            <h1>GOVERNMENT / MUNICIPAL</h1>
                            <a href="#" title=""  id="MunSoQLink" runat="server" >Statement of Qualifications</a>
                            <p class="allProjects"><a href="project-archive#market_Government_-slash-_Municipal">See all projects</a></p>

                        </div>
                        <div class="project_description cms cmsType_TextMulti cmsName_Portfolio_Page_Government_Municipal">
                            
                        </div>
                        <div class="projects_menu">
                            <ol>
                                    <li><a href="submarket_Government_-slash-_Municipal__Parks_-amp-_Recreation">Parks &amp; Recreation</a></li>
                                    <li><a href="submarket_Government_-slash-_Municipal__Venues">Venues</a></li>	
                            </ol>                            
                        </div>
                        <div class="clear_and_margin">&nbsp;</div>
                        <div class="img"><img  id="GMImage" runat="server" src="assets/images/portfolio_GovernmentMunicipal.jpg" alt=" "/>
                           <div class="portPlus"><a></a></div>
                           <div class="portDetails">
                            <p>
                            <asp:Literal ID="GMImageText" runat="server"></asp:Literal>
                            </p>
                           </div>
                        </div>
                    </li>
                    <li>
                        <div class="project_title">
                            <h1>PRE K - 12</h1>
                            <a href="#" title=""  id="PreKSoQLink" runat="server" >Statement of Qualifications</a>
                            <p class="allProjects"><a href="project-archive#market_PRE-dash-K_-dash-_12">See all projects</a></p>
                        </div>
                        <div class="project_description cms cmsType_TextMulti cmsName_Portfolio_Page_PRE_K_12_Content">
                          
                        </div>
                        <div class="projects_menu">
                            <ol>
                                    <li><a href="submarket_PRE-dash-K_-dash-_12__Charter">Charter</a></li>
                                    <li><a href="submarket_PRE-dash-K_-dash-_12__Private">Private</a></li>
                                    <li><a href="submarket_PRE-dash-K_-dash-_12__Public">Public</a></li>
                            </ol>
                        </div>
                        <div class="clear_and_margin">&nbsp;</div>
                        <div class="img"><img id="PREKImage" runat="server" src="assets/images/portfolio_PreK12.jpg" alt=" "/>
                           <div class="portPlus"><a></a></div>
                           <div class="portDetails">
                            <p>
                            <asp:Literal ID="PREKImageText" runat="server"></asp:Literal>
                            </p>
                           </div>
                        </div>
                    </li>                    
                    <li>
                        <div class="project_title">
                            <h1>PRIVATE</h1>
                            <a href="#" title=""  id="PriSoQLink" runat="server" >Statement of Qualifications</a>
                            <p class="allProjects"><a href="project-archive#market_Private">See all projects</a></p>
                        </div>
                        <div class="project_description cms cmsType_TextMulti cmsName_Portfolio_Page_Private_Content">
                            
                        </div>
                        <div class="projects_menu">
                            <ol>
                                    <li><a href="submarket_Private__Developer">Business &amp; Industry</a></li>
                            </ol>
                        </div>
                        <div class="clear_and_margin">&nbsp;</div>
                        <div class="img"><img id="PRIVATEImage" runat="server" src="assets/images/portfolio_Private.jpg" alt=" "/>
                           <div class="portPlus"><a></a></div>
                           <div class="portDetails">
                            <p>
                            <asp:Literal ID="PRIAVATEIamgeText" runat="server"></asp:Literal>
                            </p>
                           </div>
                        </div>
                    </li>                    
                    <li>
                        <div class="project_title">
                            <h1>B&amp;D VENUES</h1>
                            <a href="#" title="" id="VenuesSoQLink" runat="server" >Statement of Qualifications</a>
                            <p class="allProjects"><a href="project-archive#market_B-amp-D_Venues">See all projects</a></p>
                        </div>
                        <div class="project_description cms cmsType_TextMulti cmsName_Portfolio_Page_Venues_Content">
                           
                        </div>
                        <div class="projects_menu">
                            <ol>
                                    <li><a href="submarket_B-amp-D_Venues__Arenas_-slash-_Convocation_Centers">Arenas / Convocation Centers </a></li>
                                    <li><a href="submarket_B-amp-D_Venues__Ballparks">Ballparks</a></li>
                                    <li><a href="submarket_B-amp-D_Venues__Conference_-slash-_Convention_Centers">Convention / Conference Centers</a></li>
                                    <li><a href="submarket_B-amp-D_Venues__Performing_Arts">Performing Arts</a></li>
                                    <li><a href="submarket_B-amp-D_Venues__Stadiums">Stadiums</a></li>
                            </ol>                            
                        </div>
                        <div class="clear_and_margin">&nbsp;</div>
                        <div class="img"><img id="BDVenuseImage" runat="server" src="assets/images/portfolio_Venues.jpg" alt=" "/>
                           <div class="portPlus"><a></a></div>
                           <div class="portDetails">
                            <p>
                            <asp:Literal ID="BDVenuesImageText" runat="server"></asp:Literal>
                            </p>
                           </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="push"></div>
        </div>

        <div id="footer">
            <div id="footLeft"></div>
            <div id="footRight"></div>
            <div id="foot">
                <div id="footUp"></div>
                <div class="content">
                    <a class="UpDown" title="Expand"></a><a class="site_map" title="Site Map">SITE MAP</a>
                    <p>
                        <a href="http://venues.programmanagers.com/" class="venues">B&amp;D VENUES</a>
                        <a class="visit_bd selected">B&amp;D</a><a href="http://www.centersusa.com/" class="visit_centers">CENTERS</a>
                    </p>
                    <address>Copyright Brailsford  &amp; Dunlavey, Inc. <span>&copy;</span></address>
                </div>                
            </div>
        </div>        

    </body>
</html>
