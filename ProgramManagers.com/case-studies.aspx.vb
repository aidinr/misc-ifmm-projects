﻿Imports System.Web
Imports System.Data.SqlClient
Imports System.Data

Partial Class case_studies
    Inherits System.Web.UI.Page

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim out As String = ""
        Dim temp As String = ""
        Dim number As String = "4"
        If Not (Request.QueryString("number") Is Nothing) Then
            number = Request.QueryString("number")
        End If
        Dim limitServices As String = ""
        If Not (Request.QueryString("limitServices") Is Nothing) Then
            limitServices = Request.QueryString("limitServices")
        End If
        Dim limitPublications As String = ""
        If Not (Request.QueryString("limitPublications") Is Nothing) Then
            limitPublications = Request.QueryString("limitPublications")
        End If
        Dim limitPresentations As String = ""
        If Not (Request.QueryString("limitPresentations") Is Nothing) Then
            limitPresentations = Request.QueryString("limitPresentations")
        End If
        Dim discipline As String = ""
        If Not (Request.QueryString("discipline") Is Nothing) Then
            discipline = Request.QueryString("discipline")
        End If
        Dim keyword As String = ""
        If Not (Request.QueryString("keyword") Is Nothing) Then
            keyword = Request.QueryString("keyword").ToString.Trim
        End If
        Dim image As String = ""
        If Not (Request.QueryString("image") Is Nothing) Then
            image = Request.QueryString("image").ToString.Trim
        End If
        Dim sql As String = "select top " & number & " p.projectid, ISNULL(cn.Item_Value, '') ClientName,  p.Name, isnull(v2.item_value,'') Project_Name from IPM_PROJECT p join IPM_PROJECT_FIELD_VALUE v on p.ProjectID = v.ProjectID and v.Item_ID = (Select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_FEATURE') and v.Item_Value = '1'  left join  ipm_project_field_value cn on p.ProjectID = cn.ProjectID and  cn.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_CLIENTNAME') left outer join ipm_project_field_Value v2 on v2.item_id = 2400039 and v2.projectid = p.projectid "
        If discipline <> "" Then
            sql &= " join IPM_PROJECT_DISCIPLINE d on p.ProjectID = d.ProjectID and d.KeyID = (select KeyID from IPM_DISCIPLINE where KeyName = '" & discipline & "') "
        End If
        If keyword <> "" Then
            sql &= " join IPM_PROJECT_KEYWORD k on p.ProjectID = k.ProjectID and k.KeyID = (select KeyID from IPM_KEYWORD where keyName ='" & keyword & "')"
        End If
        If image = "1" Then
            sql &= " join IPM_PROJECT_ASSET pa on p.ProjectID = pa.Project_ID and pa.Available= 'Y' "
        End If
        If limitServices = "1" Then
            sql &= " join ipm_project_field_value f on p.ProjectID = f.ProjectID and f.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_FEATUREDSERVICES') and f.Item_Value = '1' "
        End If
        If limitPublications = "1" Then
            sql &= " join ipm_project_field_value f on p.ProjectID = f.ProjectID and f.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_FEATUREDPUBLICATIONS') and f.Item_Value = '1' "
        End If
        If limitPresentations = "1" Then
            sql &= " join ipm_project_field_value f on p.ProjectID = f.ProjectID and f.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_FEATUREDPRESENTATIONS') and f.Item_Value = '1' "
        End If

        sql &= " where p.Available = 'Y' and Show = '1' order by NEWID() "


        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)


        '<ul class="framed_list">
        '    <li>
        '        <a href="portfolio-casestudy" title="">
        '            <img src="assets/images/sample.png" alt=""/>
        '            <span>Northern Kentuky University</span>                                   
        '        </a>
        '    </li>
        '    <li>
        '        <a href="portfolio-casestudy" title="">
        '            <img src="assets/images/sample.png" alt=""/>
        '            <span>Northern Kentuky University</span>                                   
        '        </a>
        '    </li>
        '    <li>
        '        <a href="portfolio-casestudy" title="">
        '            <img src="assets/images/sample.png" alt=""/>
        '            <span>Northern Kentuky University</span>                                   
        '        </a>
        '    </li>                            
        '</ul>

        out = "<ul class=""framed_list"">" & VbNewLine
        For i As Integer = 0 To DT1.Rows.Count - 1
            out &= "<li><a href=""/" & Regex.Replace(DT1(i)("Project_Name").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-casestudy-" & DT1(i)("ProjectID") & """ title="""">"
            out &= "<img src=""/dynamic/image/forever/project/best/222x140/68/777777/Center/" & DT1(i)("ProjectID").ToString.Trim & ".jpg"" alt="" ""/><span>" & formatfunctions.AutoFormatText(DT1(i)("ClientName").ToString.Trim) & "</span></a></li>" & VbNewLine
        Next
        out &= "</ul>"
        Response.Clear()
        Response.Write(out)

    End Sub

End Class
