Imports System.Web
Imports System.Data.SqlClient
Imports System.Data


Partial Class news
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sql As String = "select Headline, Content, DateName(MONTH,Post_Date) + ' ' + DATENAME(dd,post_date) +', ' + DATENAME(yyyy,post_date) PostDate, PDFLinkTitle, PDF, n.News_Id, Isnull(v.Item_Value,'') URL,  isnull(v2.Item_Value, 'VIEW') URLTITLE from IPM_NEWS n left join IPM_NEWS_FIELD_VALUE v on n.News_Id = v.NEWS_ID and v.Item_ID = ( select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWSURL' ) left join IPM_NEWS_FIELD_VALUE v2 on n.News_Id = v2.NEWS_ID and v2.Item_ID = ( select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_URLTITLE' )where Post_Date <= GETDATE() and Pull_Date >= GETDATE() and Show = 1 and Type = 0 Order By Post_Date Desc"
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)

        '<h4>April 27, 2012<br/>
        '<a href="#">4 Baseball Development Team Aims to Ease Funding Burden, Reap Rewards</a>
        '<span>&nbsp;</span>
        '</h4>
        '<div>
        '    <p>As opposition strengthened against a proposed taxpayer-funded baseball stadium, Mandalay Baseball's Rich Neumann made a last-minute decision earlier this month to shift to a privately led development effort.</p>
        '    <p>He first talked to the Atlanta Braves, which will bring a minor league team to Wilmington if a stadium is built, and then called some of the biggest players in the industry � including some local power brokers � and said, "Hey, I've got an idea. What do you think of this?"</p>
        '    <p>Neumann even pitched his idea to individual Wilmington City Council members, and a news conference was held within a week to announce that a development team � stacked with reputable contractors, project managers, architects and developers � would seek private financing for the $40 million stadium in a public-private partnership with the city.</p>
        '    <ul>
        '     <li><a href="http://www.starnewsonline.com/">Star News Online</a><br/><strong>Link to PDF</strong><span></span></li>
        '    </ul>         
        '</div>


        Dim out As String = ""
        For i As Integer = 0 To DT1.Rows.Count - 1
            out &= "<h4>" & DT1(i)("PostDate").tostring.trim & "<br /><strong>" & formatfunctions.AutoFormatText(DT1(i)("Headline").tostring.trim) & "</strong><span>&nbsp;</span></h4>"
            out &= "<div>" & formatfunctions.AutoFormatText(DT1(i)("Content"))
            If DT1(i)("PDF").tostring.trim = "1" Or DT1(i)("URL").tostring.trim <> "" Then
                out &= "<ul><li>"
                If DT1(i)("URL").tostring.trim <> "" Then
                    out &= "<a href=""" & DT1(i)("URL").tostring.trim & """ >" & DT1(i)("URLTITLE").tostring.trim & "</a>"
                End If
                If DT1(i)("PDF").tostring.trim = "1" And DT1(i)("URL").tostring.trim <> "" Then
                    out &= "<br />"
                End If
                If DT1(i)("PDF").tostring.trim = "1" Then
                    out &= "<a href=""dynamic/document/fresh/newspdf/download/" & DT1(i)("News_ID").ToString.Trim & "/" & DT1(i)("News_ID").ToString.Trim & ".pdf""><strong>" & formatfunctions.AutoFormatText(DT1(i)("PDFLinkTitle")) & "</strong></a>"
                End If
                out &= "</li></ul>"
            End If
            out &= "</div>"
        Next

        accordion_data.Text = out

    End Sub

End Class
