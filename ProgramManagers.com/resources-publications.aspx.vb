Imports System.Web
Imports System.Data.SqlClient
Imports System.Data


Partial Class resources_publications
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sql As String = "select Headline, Content, DateName(MONTH,Post_Date) + ' ' + DATENAME(dd,post_date) +', ' + DATENAME(yyyy,post_date) PostDate, PDFLinkTitle, PDF, n.News_Id, Isnull(v.Item_Value,'') url, Isnull(v.Item_Value,'') urltitle  from IPM_NEWS n left join IPM_NEWS_FIELD_VALUE v on n.News_Id = v.NEWS_ID and v.Item_ID = ( select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWSURL' ) left join IPM_NEWS_FIELD_VALUE t on n.News_Id = t.NEWS_ID and t.Item_ID = ( select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_URLTITLE' ) where Post_Date <= GETDATE() and Pull_Date >= GETDATE() and Show = 1 and Type = 1 Order By Post_Date Desc"
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(Sql)

        Dim out As String = ""
        For i As Integer = 0 To DT1.Rows.Count - 1
            out &= "<h4>" & DT1.Rows(i)("PostDate").ToString.Trim & "<br /><strong>" & formatfunctions.AutoFormatText(DT1.Rows(i)("Headline").ToString.Trim) & "</strong><span>&nbsp;</span></h4>"
            out &= "<div>" & formatfunctions.AutoFormatText(DT1.Rows(i)("Content").ToString.Trim)
            If DT1.Rows(i)("PDF").ToString.Trim = "1" Then
                Dim temp2 As String = DT1.Rows(i)("PDFLinkTitle").ToString.Trim
                If temp2 = "" Then
                    temp2 = "Download"
                End If
                out &= "<a href=""dynamic/document/fresh/newspdf/download/" & DT1.Rows(i)("News_ID").ToString.Trim & "/" & DT1.Rows(i)("News_ID").ToString.Trim & ".pdf""><strong>" & formatfunctions.AutoFormatText(temp2) & "</strong></a>"
            End If
            If DT1.Rows(i)("url").ToString.Trim <> "" Then
                Dim temp As String = DT1.Rows(i)("url").ToString.Trim
                Dim temp2 As String = DT1.Rows(i)("urltitle").ToString.Trim
                If temp2 = "" Then
                    temp2 = "View"
                End If
                out &= "<a href=""" & temp & """><strong>" & formatfunctions.AutoFormatText(temp2) & "</strong></a>"
            End If
            out &= "</div>"
        Next

        accordion_data.Text = out


    End Sub

End Class
