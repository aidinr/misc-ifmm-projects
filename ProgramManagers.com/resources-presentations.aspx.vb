Imports System.Web
Imports System.Data.SqlClient
Imports System.Data


Partial Class resources_lectures
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sql As String = "select e.Event_ID, Headline, Content,DateName(MONTH,Event_Date) + ' ' + DATENAME(dd, Event_Date) +', ' + DATENAME(yyyy,Event_Date) EventDate, ISNULL(v.Item_Value, '') linktitle from IPM_EVENTS e left join IPM_EVENTS_FIELD_VALUE v on e.Event_Id = v.EVENT_ID and v.Item_ID = (select Item_ID from IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_EVENTTITLE') Where Post_Date <= Getdate() and pull_date >=  getdate() and Show = 1 and Type = 0 Order By Event_Date Desc "
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)

        Dim out As String = ""

        '<h4>April 27, 2012<br/>
        '<strong>1 Baseball Development Team Aims to Ease Funding Burden, Reap Rewards</strong>
        '<span>&nbsp;</span>
        '</h4>
        '<div>
        '    <p>As opposition strengthened against a proposed taxpayer-funded baseball stadium, Mandalay Baseball's Rich Neumann made a last-minute decision earlier this month to shift to a privately led development effort.</p>
        '    <p>He first talked to the Atlanta Braves, which will bring a minor league team to Wilmington if a stadium is built, and then called some of the biggest players in the industry � including some local power brokers � and said, "Hey, I've got an idea. What do you think of this?"</p>
        '    <p>Neumann even pitched his idea to individual Wilmington City Council members, and a news conference was held within a week to announce that a development team � stacked with reputable contractors, project managers, architects and developers � would seek private financing for the $40 million stadium in a public-private partnership with the city.</p>
        '    <ul>
        '     <li><a href="#" target="_blank">Visit</a><span></span></li>
        '    </ul>         
        '</div>   

        For i As Integer = 0 To DT1.Rows.Count - 1

            out &= "<h4>" & DT1(i)("EventDate").ToString.Trim & "<br /><strong>" & formatfunctions.AutoFormatText(DT1(i)("Headline").ToString.Trim) & "</strong><span>&nbsp;</span></h4><div><p>"
            out &= formatfunctions.AutoFormatText(DT1(i)("Content").ToString.Trim) & "</p>"
            Dim temp2 As String = DT1(i)("linktitle").ToString.Trim
            If temp2 = "" Then
                temp2 = "Download"
            End If
            Dim sql2 As String = "Select a.Asset_ID from IPM_ASSET a where a.Category_ID = " & DT1(i)("Event_ID").ToString.Trim & " and a.Active = 1 and a.Available = 'Y'"
            Dim DT2 As New DataTable("FProjects")
            DT2 = mmfunctions.GetDataTable(sql2)
            If DT2.Rows.Count > 0 Then
                out &= "<ul>"
                For j As Integer = 0 To DT2.Rows.Count - 1
                    out &= "<li><a href=""dynamic/document/fresh/asset/download/" & DT2(j)("Asset_ID").ToString.Trim & "/" & DT2(j)("Asset_ID").ToString.Trim & ".pdf"">" & temp2 & "</a></li>"
                Next
                out &= "</ul>"
            End If

            out &= "</div>"

        Next

        accordion_data.Text = out

    End Sub

End Class
