Imports System.Web
Imports System.Data.SqlClient
Imports System.Data

Partial Class content_contact
    Inherits System.Web.UI.Page

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sql As String = "select Name, DESCRIPTION from IPM_asset_CATEGORY where NAME = 'Contact Us' and PROJECTID = 2407137 and AVAILABLE = 'Y'"
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)
        If DT1.Rows.Count > 0 Then
            Name.Text = formatfunctions.AutoFormatText(DT1(0)("Name").ToString.Trim)
            Description.Text = formatfunctions.AutoFormatText(DT1(0)("DESCRIPTION").ToString.Trim)
        End If
    End Sub

End Class