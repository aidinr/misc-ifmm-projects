Imports System.Web
Imports System.Data.SqlClient
Imports System.Data

Partial Class carousel
    Inherits System.Web.UI.Page

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    Dim T1970 As New DateTime(1970, 1, 1)
    Dim Timestamp As Integer = (DateTime.Now.ToUniversalTime - T1970).TotalSeconds
	If Not Request.Headers("If-Modified-Since") Is Nothing Then
	 Dim tSince As String = Request.Headers("If-Modified-Since")
	 Dim t As Integer = System.Text.RegularExpressions.Regex.Replace(tSince, "[^0-9+]", "")
	 If Timestamp < (t + 180000) Then ' ########### CACHE FOR 180000 Seconds ###########
	  Response.StatusCode = 304
	  Response.StatusDescription = "Not Modified"
	  return
         End If
        End If
	 Response.Cache.SetCacheability(HttpCacheability.Public)
	 Response.AppendHeader("Pragma", "public")
	 Response.AppendHeader("Connection", "close")
	 Response.AppendHeader("Last-Modified", Timestamp & " GMT")
	 Response.AppendHeader("ETag", "Dyno")

        Dim carMarket As String = "Homepage Carousel"

        If Not Request.QueryString("carMarket") Is Nothing Then
            carMarket = Request.QueryString("carMarket")
        End If

        Dim h As String
        h = Request.QueryString("height")
        If h = Nothing Then
            h = "500"
        End If
        Dim hi As Integer = 500
        Try
            hi = CInt(h)
        Catch ex As Exception

        End Try
        If hi > 1200 Then
            h = "1200"
        ElseIf hi < 200 Then
            h = "200"
        End If
        h = hi.ToString()
        Dim sql As String = "select  a.Asset_id, WPixel, p.ProjectID, p.Name, ISNULL(cn.Item_Value, '') ClientName, ISNULL(pa.Item_Value, '') ProjectArchitect, ISNULL(wm.Item_Value, '') WaterMark, isnull(v2.item_value, '') as Project_Name  from IPM_CARROUSEL c join IPM_CARROUSEL_ITEM i on c.Carrousel_ID = i.Carrousel_ID and c.Available = 'Y' and c.Active = 1 and i.Available = 'Y' and i.Active = 1 and Available_Date < getdate() join IPM_ASSET a on i.Asset_ID = a.Asset_ID and a.Available = 'Y' and a.Active = 1 and WPixel > 0 and HPixel > 0 join IPM_PROJECT p on a.ProjectID = p.ProjectID left join IPM_PROJECT_FIELD_VALUE cn on p.ProjectID = cn.ProjectID and cn.Item_ID = (select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_CLIENTNAME') left join IPM_PROJECT_FIELD_VALUE pa on p.ProjectID = pa.ProjectID and pa.Item_ID = (select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJARCH') left join IPM_ASSET_FIELD_VALUE wm on a.Asset_ID = wm.ASSET_ID and wm.Item_ID = (Select Item_ID from IPM_ASSET_FIELD_DESC where Item_Tag = 'IDAM_WATERMARK_VALUE')  left outer join ipm_project_field_Value v2 on v2.item_id = 2400039 and v2.projectid = p.projectid  where c.name = '" & carMarket & "' ORDER BY newid() "
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)

        Dim js As String = "$(document).ready(function () {$('body').append('<img width=""2"" height=""2"" src="""
            js &= "/dynamic/image/always/asset/liquid/x" & h & "/58/777777/Center/" & DT1.Rows(0)("Asset_ID").ToString.Trim & ".jpg"
            js &= """  alt="""" />');setTimeout(""carsouselDeep();"", 500);"
            js &= "$('#slideTop').css('backgroundImage', 'url(" & "/dynamic/image/always/asset/liquid/x" & h & "/58/777777/Center/" & DT1.Rows(0)("Asset_ID").ToString.Trim & ".jpg" & ")');"
            js &= "}); function carsouselDeep () { $('#home .wrapper').css('backgroundImage','none'); "

        For i As Integer = 0 To DT1.Rows.Count - 1

            js &= "$('#CarSlides').prepend($('<div><div class=""carDetails""><p><a href=""" & Regex.Replace(DT1(i)("Project_Name").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-casestudy-" & DT1(i)("ProjectID").ToString.Trim & """><em>"
            If DT1(i)("ClientName").ToString.Trim <> "" Then
                js &= "<span class=""carClientName"">" & DT1.Rows(i)("ClientName").ToString.Trim & "</span>"
            End If
            js &= "<span class=""carProjectName"">" & DT1.Rows(i)("Name").ToString.Trim & "</span>"
            If DT1(i)("ProjectArchitect").ToString.Trim <> "" Then
                js &= "<span class=""carArch"">Architect: " & DT1.Rows(i)("ProjectArchitect").ToString.Trim & "</span>"
            End If
            js &= "<span class=""carMore"">Learn More</span>"
            If DT1(i)("WaterMark").ToString.Trim <> "" Then
                js &= "<span class=""carCredits"">Photograph &copy; Copyright " & DT1.Rows(i)("WaterMark").ToString.Trim & "</span>"
            End If
            js &= "</em></a></p></div></div>').addClass('CarSlide').css('backgroundImage','url(\'"
            js &= "/dynamic/image/always/asset/liquid/x" & h & "/58/777777/Center/" & DT1.Rows(i)("Asset_ID").ToString.Trim & ".jpg"
            js &= "\')'));"

        Next

        js &= "slidesIII = 0; $('.CarSlide').each(function (){var img = new Image; img.src = $(this).css( 'background-image' ).match(/url\(['"" ]*([^\'\""]+)['"" ]*\)/)[1];  img.onload = function(){ if(++slidesIII == $('.CarSlide').length) homeCar(); }; });}"


        If js.Length() < 300 Then
            js = "$(document).ready(function() { $('#banner').addClass('portfolio_banner').css('backgroundImage','url(assets/images/banners/portfolio_banner.jpg)') });"
        End If

        Response.Clear()
        Response.Write(js)

    End Sub

End Class
