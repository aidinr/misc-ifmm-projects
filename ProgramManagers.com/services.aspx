﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Sustainability.aspx.vb" Inherits="Sustainability" %>
<%@ Import Namespace="System.IO" %>
<% 

   
        Dim thePage as string = "services"
        Dim filepath As String = Server.MapPath("~") & "cms\data\" 
        %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        <link type="text/css" rel="stylesheet" href="assets/css/reset.css"/>    
        <link type="text/css" rel="stylesheet" href="assets/css/style.css"/>    
        <script src="assets/js/jquery.js" type="text/javascript"></script>  
        <script src="assets/js/js.js" type="text/javascript"></script>         
        <title><%=CMSFunctions.GetMetaTitle(filepath, thePage)%>: Brailsford &amp; Dunlavey</title>
        <meta name="description" content="<%=CMSFunctions.GetMetaDescription(filepath, thePage)%>" />      
        <meta name="keywords" content="<%=CMSFunctions.GetMetaKeywords(filepath, thePage)%>" />
    </head>
    <body class="cms">  
        <div id="headsUp"></div>
        
        <div class="wrapper">
            <div id="headLeft"></div>
            <div id="headRight"></div>
            <div id="head">
                <div id="header">
                    <div class="content">
                        <h1 title="B&amp;D Inspire. Empower. Advance.">
                            <a href="./" title="B&amp;D Inspire. Empower. Advance.">&nbsp;</a>
                        </h1>
<p>
<a href="offices" title="Contact Us" class="locations">Contact Us</a>
</p>
                            
                    </div>
                </div>
                <div id="menu">
                    <div class="content">
                        <ul>
                            <li><a href="about-us" title="">ABOUT US</a></li>
                            <li><a href="people-overview" title="">PEOPLE</a></li>
                            <li><a href="services" title="">SERVICES</a></li>
                            <li><a href="portfolio" title="">PORTFOLIO</a></li>
                            <li><a href="resources" title="">RESOURCES</a></li>
                            <li class="last"><a href="careers" title="">CAREERS</a></li>
                        </ul>
                        <form method="get" id="siteSearch" action="search">
                            <p>
                                <input type="text" name="q" id="siteSearchQ" value="" />
                                <input  id="searchSubmit" type="submit" name="search" value="Search"  />
                            </p>
                        </form>
                    </div>
                </div>                
            </div> 

            
           <div id="banner" class="<%="cms cmsType_Image cmsName_Services_Banner_Image"%>" style="background-image: url(cms/data/Sized/best/1920x314/65/F4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Services_Banner_Image")%>)">
           
                <div class="content">
                    <h1>Services</h1>
                    <ul> 
                        <li><a href="services" title="">Overview</a></li>
                    </ul>                    	                                     
                </div>
            </div>  
            
            <div id="inner_content">
                <div class="two_columns">
                    <div class="left">
                        <h2 class="cms cmsType_TextSingle cmsName_Services_Page_Heading">
                          
                            </h2>
                        <div class="text"> 
                                                       
                            <img src="cms/data/Image/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Services_Page_Banner_Image")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Services_Page_Banner_Image")%>" class="<%="cms cmsType_Image cmsName_Services_Page_Banner_Image"%>" />

                            <p class="cms cmsType_TextMulti cmsName_Services_Page_Introduction_Text">
                            
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="two_columns">
                    <div class="left">

                        <h3 class="section_title cms cmsType_TextSingle cmsName_Services_Page_Section_1_Heading"></h3>
                       
                        <div id="accordion" class="cms cmsFill_Static cmsType_Textset cmsName_Services_Page_Services">
                            <% 
                                for i as integer = 0 to 200 
                                    If Not CMSFunctions.CheckTextFile(filepath & "Textset\Services_Page_Services\Services_Page_Services", i) Then
                                        Exit For
                                    End If
                            %>
                                <h4><strong><%=CMSFunctions.FormatSimple(filepath & "Textset\Services_Page_Services\Services_Page_Services_xa" & i.ToString.Trim & ".txt")%></strong><span>&nbsp;</span></h4>
                                <div>
                                    <%=CMSFunctions.Format(filepath & "Textset\Services_Page_Services\Services_Page_Services_xb" & i.tostring.trim & ".txt") %>    
                                </div>
                            <% next %>
                        </div>

<div class="c"></div>

                        <h3 class="section_title cms cmsType_TextSingle cmsName_Services_Page_Section_2_Heading"></h3>
                       
                        <div id="accordion2" class="cms cmsFill_Static cmsType_Textset cmsName_Services_Page_Services_2">
                            <% 
                                for i as integer = 0 to 200 
                                    If Not CMSFunctions.CheckTextFile(filepath & "Textset\Services_Page_Services_2\Services_Page_Services_2", i) Then
                                        Exit For
                                    End If
                            %>
                                <h4><strong><%=CMSFunctions.FormatSimple(filepath & "Textset\Services_Page_Services_2\Services_Page_Services_2_xa" & i.ToString.Trim & ".txt")%></strong><span>&nbsp;</span></h4>
                                <div>
                                    <%=CMSFunctions.Format(filepath & "Textset\Services_Page_Services_2\Services_Page_Services_2_xb" & i.tostring.trim & ".txt") %>    
                                </div>
                            <% next %>
                        </div>

<div class="c"></div>

                    </div>
                    <div class="right">
                      <h3 class="section_title">Case Studies</h3>
                     <div id="caseStudies" class="Services3"></div>
                    </div>
                </div>
            </div>

            <div class="push"></div>
        </div>
   
        <div id="footer">
            <div id="footLeft"></div>
            <div id="footRight"></div>
            <div id="foot">
                <div id="footUp"></div>
                <div class="content">
                    <a class="UpDown" title="Expand"></a><a class="site_map" title="Site Map">SITE MAP</a>
                    <p>
                        <a href="http://venues.programmanagers.com/" class="venues">B&amp;D VENUES</a>
                        <a class="visit_bd selected">B&amp;D</a><a href="http://www.centersusa.com/" class="visit_centers">CENTERS</a>
                    </p>
                    <address>Copyright Brailsford  &amp; Dunlavey, Inc. <span>&copy;</span></address>
                </div>                
            </div>
        </div>        
      
    </body>
</html>
