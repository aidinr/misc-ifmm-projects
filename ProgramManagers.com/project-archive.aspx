﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="project-archive.aspx.vb" Inherits="project_archive" %>
<%@ Import Namespace="System.IO" %>
<% 
    
    Dim T1970 As New DateTime(1970, 1, 1)
    Dim Timestamp As Integer = (DateTime.Now.ToUniversalTime - T1970).TotalSeconds
    If Not Request.Headers("If-Modified-Since") Is Nothing Then
        Dim tSince As String = Request.Headers("If-Modified-Since")
        Dim t As Integer = System.Text.RegularExpressions.Regex.Replace(tSince, "[^0-9+]", "")
        If Timestamp < (t + 190) Then ' ########### CACHE FOR 3 MINUTES ###########
            Response.StatusCode = 304
            Response.StatusDescription = "Not Modified"
            Return
        End If
    End If
    Response.Cache.SetCacheability(HttpCacheability.Public)
    Response.AppendHeader("Pragma", "public")
    Response.AppendHeader("Connection", "close")
    Response.AppendHeader("Last-Modified", Timestamp & " GMT")
    Response.AppendHeader("ETag", "Dyno")
    
    Dim filepath As String = Server.MapPath("~") & "cms\data\"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        <link type="text/css" rel="stylesheet" href="assets/css/reset.css"/>    
        <link type="text/css" rel="stylesheet" href="assets/css/style.css"/>    
        <script src="assets/js/jquery.js" type="text/javascript"></script>  
        <script src="assets/js/jq.highlight.js" type="text/javascript"></script>  
        <script src="assets/js/js.js" type="text/javascript"></script>         
        <title>Project Search: Brailsford &amp; Dunlavey</title>       
    </head>
    <body>  
        
        <div id="headsUp"></div>
        
        <div class="wrapper">
            <div id="headLeft"></div>
            <div id="headRight"></div>
            <div id="head">
                <div id="header">
                    <div class="content">
                        <h1 title="B&amp;D Inspire. Empower. Advance.">
                            <a href="./" title="B&amp;D Inspire. Empower. Advance.">&nbsp;</a>
                        </h1>
<p>
<a href="offices" title="Contact Us" class="locations">Contact Us</a>
</p>
                            
                    </div>
                </div>
                <div id="menu">
                    <div class="content">
                        <ul>
                            <li><a href="about-us" title="">ABOUT US</a></li>
                            <li><a href="people-overview" title="">PEOPLE</a></li>
                            <li><a href="services" title="">SERVICES</a></li>
                            <li><a href="portfolio" title="" class="selected">PORTFOLIO</a></li>
                            <li><a href="resources" title="">RESOURCES</a></li>
                            <li class="last"><a href="careers" title="">CAREERS</a></li>
                        </ul>
                        <form method="get" id="siteSearch" action="search">
                            <p>
                                <input type="text" name="q" id="siteSearchQ" value="" />
                                <input  id="searchSubmit" type="submit" name="search" value="Search"  />
                            </p>
                        </form>
                    </div>
                </div>                
            </div> 

            
                 <div id="bannerProjectArchive" class="project-archive_banner" style="background-image: url(cms/data/Sized/best/1920x314/65/F4F3ED/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Portfolio_Banner_Image")%>)">
                <div class="content">
                    <h1>PROJECTS</h1>
                    <ul> 
                        <li>
                            <form method="post" id="projectSearch" action="./">
                                <p>
                                    <input type="text" name="project_keywords" id="project_keywords" value="" />
                                    <input  id="projectSearchSubmit" type="submit" name="search" value="Search"  />
                                </p>
                            </form>
                        </li>
                    </ul>                    	                                     
                </div>
            </div>  
            
            <div id="inner_content">
                <div class="person_layout">
                    <div class="search_breadcrumbs">
                        <ul>
                            <li></li>
                        </ul>
                    </div>
                    
                    <div class="person_content project_archive">
                     <div id="projectsFeatures">
                        <a class="moreFeatured">MORE FEATURED</a>
                        <a class="moreFeaturedPrev"></a>

                        <div class="person_title no_bottom_margin">                            
                            <h1 class="section_title">FEATURED PROJECTS</h1>                            
                        </div>
                        <div style="clear: both;" ></div>
                        <div class="featured_projects"><ul class="framed_list_results floating_elements less_margin"><li></li></ul></div>
                     </div>

                        <div class="c"></div>
                        <div class="project_results">
                            <h2 class="section_title">PROJECTS</h2>
                            <div id="projectResultList"></div>
                        </div>
                    </div>
                    <div class="project_filters">
                        <a class="clear_filters">Clear Filters</a>
                        <em>FILTER PROJECTS BY:</em>
                        <div id="projectSearch_FilterState">
<h2>State</h2>
<select name="state" id="state"> 
<option value="" selected="selected">All</option> 
<option value="AL">Alabama</option> 
<option value="AK">Alaska</option> 
<option value="AZ">Arizona</option> 
<option value="AR">Arkansas</option> 
<option value="CA">California</option> 
<option value="CO">Colorado</option> 
<option value="CT">Connecticut</option> 
<option value="DE">Delaware</option> 
<option value="DC">District of Columbia</option> 
<option value="FL">Florida</option> 
<option value="GA">Georgia</option> 
<option value="HI">Hawaii</option> 
<option value="ID">Idaho</option> 
<option value="IL">Illinois</option> 
<option value="IN">Indiana</option> 
<option value="IA">Iowa</option> 
<option value="KA">Kansas</option> 
<option value="KY">Kentucky</option> 
<option value="LA">Louisiana</option> 
<option value="ME">Maine</option> 
<option value="MD">Maryland</option> 
<option value="MA">Massachusetts</option> 
<option value="MI">Michigan</option> 
<option value="MN">Minnesota</option> 
<option value="MS">Mississippi</option> 
<option value="MO">Missouri</option> 
<option value="MT">Montana</option> 
<option value="NE">Nebraska</option> 
<option value="NV">Nevada</option> 
<option value="NH">New Hampshire</option> 
<option value="NJ">New Jersey</option> 
<option value="NM">New Mexico</option> 
<option value="NY">New York</option> 
<option value="NC">North Carolina</option> 
<option value="ND">North Dakota</option> 
<option value="OH">Ohio</option> 
<option value="OK">Oklahoma</option> 
<option value="OR">Oregon</option> 
<option value="PA">Pennsylvania</option> 
<option value="RI">Rhode Island</option> 
<option value="SC">South Carolina</option> 
<option value="SD">South Dakota</option> 
<option value="TN">Tennessee</option> 
<option value="TX">Texas</option> 
<option value="UT">Utah</option> 
<option value="VT">Vermont</option> 
<option value="VA">Virginia</option> 
<option value="WA">Washington</option>
<option value="WV">West Virginia</option> 
<option value="WI">Wisconsin</option> 
<option value="WY">Wyoming</option>

</select>

                        </div>
                        <div id="projectSearch_Filters"></div>
                    </div>
                </div>
            </div>

            <div class="push"></div>
        </div>
   
        <div id="footer">
            <div id="footLeft"></div>
            <div id="footRight"></div>
            <div id="foot">
                <div id="footUp"></div>
                <div class="content">
                    <a class="UpDown" title="Expand"></a><a class="site_map" title="Site Map">SITE MAP</a>
                    <p>
                        <a href="http://venues.programmanagers.com/" class="venues">B&amp;D VENUES</a>
                        <a class="visit_bd selected">B&amp;D</a><a href="http://www.centersusa.com/" class="visit_centers">CENTERS</a>
                    </p>
                    <address>Copyright Brailsford  &amp; Dunlavey, Inc. <span>&copy;</span></address>
                </div>                
            </div>
        </div>        
      
    </body>
</html>
