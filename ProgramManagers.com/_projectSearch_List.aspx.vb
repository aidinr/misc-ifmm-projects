Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections.Generic

Partial Class _projectSearch_List
    Inherits System.Web.UI.Page
    Protected Function RemoveExtraSpaces(input_text As String) As String

        Dim rsRegEx As System.Text.RegularExpressions.Regex
        rsRegEx = New System.Text.RegularExpressions.Regex("\s+")

        Return rsRegEx.Replace(input_text, " ").Trim()

    End Function

    Protected Function FormatTag(TagType As String, Tag As String) As String

        Dim formatedTag As String = " "
        Dim temp As String = Tag.Trim.Replace("&amp;", "&")
        temp = temp.Replace(" ", "_")
        temp = temp.Replace("-", "-dash-")
        temp = temp.Replace("&", "-amp-")
        temp = temp.Replace("/", "-slash-")
        temp = temp.Replace("|", "")

        Dim matchpattern As String = "[^\w-_]"
        Dim replace As String = ""
        temp = Regex.Replace(temp, matchpattern, replace)

        formatedTag = TagType & temp & " "
        Return formatedTag

    End Function
    Protected Function DecodeTag(TagType As String, Tag As String) As String

        Dim temp As String = Tag.Trim.Replace(TagType, "")
        temp = temp.Replace("__", " | ")
        temp = temp.Replace("-slash-", "/")
        temp = temp.Replace("-amp-", "&")
        temp = temp.Replace("-dash-", "-")
        temp = temp.Replace("_", " ")
        temp = temp.Replace("&", "&amp;")
        Return temp

    End Function
    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Response.Write("Page load time - " & Now.ToString & "<br />")
        Dim sort As String = ""
        If Not Request.QueryString("sort") Is Nothing Then
            sort = Request.QueryString("sort")
        End If

        Dim startindex As String = "0"
        If Not Request.QueryString("start") Is Nothing Then
            startindex = Request.QueryString("start")
        End If
        Dim startdisplay As String = (CInt(startindex + 1)).ToString.Trim

        Dim market As String = ""
        Dim decodedMarket As String = ""
        If Not Request.QueryString("market") Is Nothing Then
            market = Request.QueryString("market")
            decodedMarket = DecodeTag("market_", market)
        End If

        Dim submarket As String = ""
        Dim decodedSubMarket As String = ""
        If Not Request.QueryString("submarket") Is Nothing Then
            submarket = Request.QueryString("submarket")
            decodedSubMarket = DecodeTag("submarket_", submarket)
        End If

        Dim service As New System.Collections.Generic.List(Of String)
        If Not Request.QueryString("Service[]") Is Nothing Then
            service.AddRange(Request.QueryString("Service[]").Split(","))
        End If

        Dim build As New System.Collections.Generic.List(Of String)
        If Not Request.QueryString("Built[]") Is Nothing Then
            build.AddRange(Request.QueryString("Built[]").Split(","))
            'For i As Integer = 0 To build.Count - 1
            '    Response.Write(build(i).Trim & "<br />")
            'Next
        End If

        Dim Venues As Integer
        Venues = 0
        If Not Request.QueryString("venues") Is Nothing Then
            Venues = Request.QueryString("venues")
        End If


        Dim sustainable As New System.Collections.Generic.List(Of String)
        If Not Request.QueryString("Sustainable[]") Is Nothing Then
            sustainable.AddRange(Request.QueryString("Sustainable[]").Split(","))
        End If

        Dim filterState As String = ""
        If Not Request.QueryString("state") Is Nothing Then
            filterState = Request.QueryString("state")
        End If

        Dim searchkeywords As New System.Collections.Generic.List(Of String)
        Dim fullkeyword As String = ""
        If Not Request.QueryString("keywords") Is Nothing Then
            fullkeyword = Request.QueryString("keywords")
            searchkeywords.AddRange(RemoveExtraSpaces(fullkeyword).Split(" "))
        End If

        '--------------------------------SQL1 START--------------------------------------'
        '--------------------------------SQL1 START--------------------------------------'
        '--------------------------------SQL1 START--------------------------------------'

        'Response.Write("sql1 start - " & Now.ToString & "<br />")

        Dim sql As String = "With WebProjects( ProjectID ) as ( Select distinct p.ProjectID from IPM_PROJECT p join ipm_project_field_value wa on p.ProjectID = wa.ProjectID and  wa.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_WEBACTIVE') and wa.Item_Value = '1' join IPM_PROJECT_FIELD_VALUE pn on p.ProjectID = pn.ProjectID and pn.Item_ID = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJNAME') and pn.Item_Value <> '' "
        If market <> "" Then
            sql &= " join IPM_PROJECT_DISCIPLINE pd on p.ProjectID = pd.ProjectID and pd.KeyID = (Select KeyID from IPM_DISCIPLINE  where KeyUse = '1' and KeyName = '" & decodedMarket & "' ) "
        End If
        If submarket <> "" Then
            sql &= " join IPM_PROJECT_KEYWORD pk on p.ProjectID = pk.ProjectID and pk.KeyID = (Select KeyID from IPM_KEYWORD where KeyUse = 1 and  KeyName = '" & decodedSubMarket & "' ) "
        End If
        If service.Count > 0 Then
            If service.Count = 1 Then
                sql &= " join IPM_PROJECT_OFFICE po on p.ProjectID = po.ProjectID and po.OfficeID = (Select KeyID from IPM_OFFICE where KeyUse = 1 and KeyName = '" & DecodeTag("service_", service(0).Trim) & "' ) "
            Else
                sql &= " join IPM_PROJECT_OFFICE po on p.ProjectID = po.ProjectID and po.OfficeID in (Select KeyID from IPM_OFFICE where KeyUse = 1 and KeyName in ( "
                For i As Integer = 0 To service.Count - 1
                    If i > 0 Then
                        sql &= " , "
                    End If
                    sql &= "'" & DecodeTag("service_", service(i).Trim) & "'"
                Next
                sql &= " ) ) "
            End If
        End If
        If build.Count > 0 Then
            If build.Count = 1 Then
                'Response.Write(build(0).Trim)
                'Response.Write(DecodeTag("built_", build(0).Trim))
                sql &= " join IPM_PROJECT_FIELD_VALUE pb on p.ProjectID = pb.ProjectID and pb.Item_ID = (Select Item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_BUILT') and pb.Item_Value = '" & DecodeTag("built_", build(0).Trim) & "'"
            Else
                sql &= " join IPM_PROJECT_FIELD_VALUE pb on p.ProjectID = pb.ProjectID and pb.Item_ID in (Select Item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_BUILT') and pb.Item_Value in ( "
                For i As Integer = 0 To build.Count - 1
                    If i > 0 Then
                        sql &= " , "
                    End If
                    sql &= "'" & DecodeTag("built_", build(i).Trim) & "'"
                Next
                sql &= " ) "
            End If
        End If
        If sustainable.Count > 0 Then
            If sustainable.Count = 1 Then
                sql &= " join IPM_PROJECT_FIELD_VALUE ps on p.ProjectID = ps.ProjectID and ps.Item_ID = (Select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUSTAINABLE') and ps.Item_Value = '" & DecodeTag("sustainable_", sustainable(0).Trim) & "'"
            Else
                sql &= " join IPM_PROJECT_FIELD_VALUE ps on p.ProjectID = ps.ProjectID and ps.Item_ID = (Select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUSTAINABLE') and ps.Item_Value in ( "
                For i As Integer = 0 To sustainable.Count - 1
                    If i > 0 Then
                        sql &= " , "
                    End If
                    sql &= "'" & DecodeTag("sustainable_", sustainable(i).Trim) & "'"
                Next
                sql &= " ) "
            End If
        End If
        sql &= " where p.Available = 'Y' and p.ClientName <> '' "
        If filterState <> "" Then
            sql &= " and p.State_id  = '" & filterState & "' "
        End If
        sql &= " )select COUNT(*) kount from WebProjects "

        If searchkeywords.Count > 0 Then
            sql = "select projectid, Item_ID, Item_Value into #WebAddresses from IPM_PROJECT_FIELD_VALUE where "
            For i As Integer = 0 To searchkeywords.Count - 1
                If i > 0 Then
                    sql &= " or "
                End If
                sql &= " Item_Value like '%" & searchkeywords(i).Trim & "%' "
            Next
            sql &= "; "
            sql &= "With WebProjects( ProjectID, ProjectName ) as ( Select distinct p.ProjectID, pn.Item_Value ProjectName from IPM_PROJECT p join ipm_project_field_value wa on p.ProjectID = wa.ProjectID and  wa.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_WEBACTIVE') and wa.Item_Value = '1' join IPM_PROJECT_FIELD_VALUE pn on p.ProjectID = pn.ProjectID and pn.Item_ID = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJNAME') and pn.Item_Value <> '' where p.Available = 'Y' and p.ClientName <> '' "
            If filterState <> "" Then
                sql &= " and p.State_id  = '" & filterState & "' "
            End If
            sql &= " ) Select COUNT(*) kount from ( select p.projectid, p.ClientName, p.Creation_Date, wp.ProjectName,  pl.Item_Value location "
            sql &= " from IPM_PROJECT p  join WebProjects wp on p.ProjectID = wp.ProjectID left join #WebAddresses pl on p.ProjectID = pl.ProjectID and pl.Item_ID = (Select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_Address') "
            sql &= " left join #WebAddresses p2 on p.ProjectID = p2.ProjectID and p2.Item_ID = (Select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_WEBDESCRIPTION')  where "
            For i As Integer = 0 To searchkeywords.Count - 1
                If i > 0 Then
                    sql &= " or "
                End If
                sql &= " p.ClientName like '%" & searchkeywords(i).Trim & "%' or wp.ProjectName like '%" & searchkeywords(i).Trim & "%'" 'or pl.Item_Value like '%" & searchkeywords(i).Trim & "%' or p2.Item_Value like '%" & searchkeywords(i).Trim & "%' "
            Next
            sql &= " or ISNULL( pl.ProjectID , p2.ProjectID ) is not null"
            sql &= ")t "
        End If

        'Response.Write(sql)
        'Response.Write("<br />")
        'Return
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)
        'Response.Write("sql1 end - " & Now.ToString & " venues= " & venues & "<br />")

        '--------------------------------SQL1 END--------------------------------------'
        '--------------------------------SQL1 END--------------------------------------'
        '--------------------------------SQL1 END--------------------------------------'


        '--------------------------------SQL2 START--------------------------------------'
        '--------------------------------SQL2 START--------------------------------------'
        '--------------------------------SQL2 START--------------------------------------'

        If DT1.Rows(0)("kount").ToString.Trim = "0" Then
            Response.Write("<div class=""project_results404""><h2 class=""section_title"">NO MATCHING PROJECTS FOUND</h2></div>")
            Response.Flush()
            Return
        End If

        Dim sql2 As String = "With WebProjects( ProjectID, ProjectName  ) as ( Select distinct p.ProjectID, pn.Item_Value ProjectName from IPM_PROJECT p join ipm_project_field_value wa on p.ProjectID = wa.ProjectID and  wa.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_WEBACTIVE') and wa.Item_Value = '1' join IPM_PROJECT_FIELD_VALUE pn on p.ProjectID = pn.ProjectID and pn.Item_ID = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJNAME') and pn.Item_Value <> '' "
        If market <> "" Then
            sql2 &= " join IPM_PROJECT_DISCIPLINE pd on p.ProjectID = pd.ProjectID and pd.KeyID = (Select KeyID from IPM_DISCIPLINE  where KeyUse = '1' and KeyName = '" & decodedMarket & "' ) "
        End If
        If submarket <> "" Then
            sql2 &= " join IPM_PROJECT_KEYWORD pk on p.ProjectID = pk.ProjectID and pk.KeyID = (Select KeyID from IPM_KEYWORD where KeyUse = 1 and  KeyName = '" & decodedSubMarket & "' ) "
        End If
        If service.Count > 0 Then
            If service.Count = 1 Then
                sql2 &= " join IPM_PROJECT_OFFICE po on p.ProjectID = po.ProjectID and po.OfficeID = (Select KeyID from IPM_OFFICE where KeyUse = 1 and KeyName = '" & DecodeTag("service_", service(0).Trim) & "' ) "
            Else
                sql2 &= " join IPM_PROJECT_OFFICE po on p.ProjectID = po.ProjectID and po.OfficeID in (Select KeyID from IPM_OFFICE where KeyUse = 1 and KeyName in ( "
                For i As Integer = 0 To service.Count - 1
                    If i > 0 Then
                        sql2 &= " , "
                    End If
                    sql2 &= "'" & DecodeTag("service_", service(i).Trim) & "'"
                Next
                sql2 &= " ) ) "
            End If
        End If
        If build.Count > 0 Then
            If build.Count = 1 Then
                sql2 &= " join IPM_PROJECT_FIELD_VALUE pb on p.ProjectID = pb.ProjectID and pb.Item_ID = (Select Item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_BUILT') and pb.Item_Value = '" & DecodeTag("built_", build(0).Trim) & "' "
            Else
                sql2 &= " join IPM_PROJECT_FIELD_VALUE pb on p.ProjectID = pb.ProjectID and pb.Item_ID in (Select Item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_BUILT') and pb.Item_Value in ( "
                For i As Integer = 0 To build.Count - 1
                    If i > 0 Then
                        sql2 &= " , "
                    End If
                    sql2 &= "'" & DecodeTag("built_", build(i).Trim) & "'"
                Next
                sql2 &= " ) "
            End If
        End If
        If sustainable.Count > 0 Then
            If sustainable.Count = 1 Then
                sql2 &= " join IPM_PROJECT_FIELD_VALUE ps on p.ProjectID = ps.ProjectID and ps.Item_ID = (Select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUSTAINABLE') and ps.Item_Value = '" & DecodeTag("sustainable_", sustainable(0).Trim) & "' "
            Else
                sql2 &= " join IPM_PROJECT_FIELD_VALUE ps on p.ProjectID = ps.ProjectID and ps.Item_ID = (Select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUSTAINABLE') and ps.Item_Value in ( "
                For i As Integer = 0 To sustainable.Count - 1
                    sql2 &= "'" & DecodeTag("sustainable_", sustainable(i).Trim) & "'"
                Next
                sql2 &= " ) "
            End If
        End If
        sql2 &= " where p.Available = 'Y' and p.ClientName <> '' "
        If filterState <> "" Then
            sql2 &= " and p.State_id  = '" & filterState & "' "
        End If
        sql2 &= " ) Select * from (select p.projectid, p.ClientName, p.Creation_Date, wp.ProjectName,  pl.Item_Value location, ROW_NUMBER() over (order by "
        If sort = "" Or sort = "t" Then
            sql2 &= " p.ProjectDate Desc "
        ElseIf sort = "a" Then
            sql2 &= " p.ClientName "
        End If
        sql2 &= " ) RowID from IPM_PROJECT p join WebProjects wp on p.ProjectID = wp.ProjectID join IPM_PROJECT_FIELD_VALUE pl on p.ProjectID = pl.ProjectID and pl.Item_ID = (Select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_Address')) t where RowID >= "
        sql2 &= startdisplay & " and RowID <=  " & (CInt(startdisplay) + 9).ToString.Trim & " Order by RowID "

        If searchkeywords.Count > 0 Then

            sql2 = "select projectid, Item_ID, Item_Value into #WebAddresses from IPM_PROJECT_FIELD_VALUE where projectid in (select distinct projectid from IPM_PROJECT_FIELD_VALUE where "
            For i As Integer = 0 To searchkeywords.Count - 1
                If i > 0 Then
                    sql2 &= " or "
                End If
                sql2 &= " Item_Value like '%" & searchkeywords(i).Trim & "%' "
            Next
            sql2 &= "); "

            sql2 &= " With WebProjects( ProjectID, ProjectName ) as ( Select distinct p.ProjectID, pn.Item_Value ProjectName from IPM_PROJECT p join ipm_project_field_value wa on p.ProjectID = wa.ProjectID and  wa.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_WEBACTIVE') and wa.Item_Value = '1' join IPM_PROJECT_FIELD_VALUE pn on p.ProjectID = pn.ProjectID and pn.Item_ID = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJNAME') and pn.Item_Value <> '' where p.Available = 'Y' and p.ClientName <> '' "
            If filterState <> "" Then
                sql2 &= " and p.State_id  = '" & filterState & "' "
            End If
            sql2 &= " ) Select *  from ( select p.projectid, p.ClientName, p.Creation_Date, wp.ProjectName,  pl.Item_Value location, ROW_NUMBER() over (order by "
            If sort = "a" Then
                sql2 &= " p.ClientName "
            ElseIf sort = "t" Or sort = "" Then
                sql2 &= " p.Creation_Date Desc "
            ElseIf sort = "r" Then
                If Venues = 1 Then
                    sql2 &= " case when pd1.projectid is not null then 4000 else 0 end + "
                    sql2 &= " case when pd1.projectid is null and pd2.projectid is not null then 3000 else 0 end + "
                    sql2 &= " case when pd1.projectid is null and pd2.projectid is null and pd3.projectid is not null then 2000 else 0 end + "
                End If
                sql2 &= " case "
                sql2 &= " when charindex(' " & fullkeyword & " ', pn.Item_Value + ' ')+ charindex(' " & fullkeyword & " ', p.ClientName + ' ') "
                sql2 &= " + charindex(' " & fullkeyword & " ', pl.Item_Value + ' ')+ charindex(' " & fullkeyword & " ', p2.Item_Value + ' ') "
                sql2 &= "> 0 then 1100 + "
                sql2 &= " (((LEN(pn.Item_Value) - LEN(REPLACE(pn.Item_Value,'" & fullkeyword & "',''))) / Len('" & fullkeyword & "') )"
                sql2 &= "+ ((LEN(p.ClientName) - LEN(REPLACE(p.ClientName,'" & fullkeyword & "',''))) / Len('" & fullkeyword & "') ) "
                sql2 &= ") * 2 "
                sql2 &= " when charindex(' " & fullkeyword & ",', pn.Item_Value)+ charindex(' " & fullkeyword & ",', p.ClientName) "
                sql2 &= " + charindex(' " & fullkeyword & ",', pl.Item_Value)+ charindex(' " & fullkeyword & ",', p2.Item_Value) "
                sql2 &= "> 0 then 1100 + "
                sql2 &= " (((LEN(pn.Item_Value) - LEN(REPLACE(pn.Item_Value,'" & fullkeyword & "',''))) / Len('" & fullkeyword & "') )"
                sql2 &= "+ ((LEN(p.ClientName) - LEN(REPLACE(p.ClientName,'" & fullkeyword & "',''))) / Len('" & fullkeyword & "') ) "
                sql2 &= ") * 2 "
                sql2 &= " when charindex(' " & fullkeyword & ".', pn.Item_Value)+ charindex(' " & fullkeyword & ".', p.ClientName) "
                sql2 &= " + charindex(' " & fullkeyword & ".', pl.Item_Value)+ charindex(' " & fullkeyword & ".', p2.Item_Value) "
                sql2 &= "> 0 then 1100 + "
                sql2 &= " (((LEN(pn.Item_Value) - LEN(REPLACE(pn.Item_Value,'" & fullkeyword & "',''))) / Len('" & fullkeyword & "') )"
                sql2 &= "+ ((LEN(p.ClientName) - LEN(REPLACE(p.ClientName,'" & fullkeyword & "',''))) / Len('" & fullkeyword & "') ) "
                sql2 &= ") * 2 "

                sql2 &= " when charindex('" & fullkeyword & "', pn.Item_Value)+ charindex('" & fullkeyword & "', p.ClientName) "
                sql2 &= " + charindex('" & fullkeyword & "', pl.Item_Value)+ charindex('" & fullkeyword & "', p2.Item_Value) "
                sql2 &= "> 0 then 1000 + "
                sql2 &= " (((LEN(pn.Item_Value) - LEN(REPLACE(pn.Item_Value,'" & fullkeyword & "',''))) / Len('" & fullkeyword & "') )"
                sql2 &= "+ ((LEN(p.ClientName) - LEN(REPLACE(p.ClientName,'" & fullkeyword & "',''))) / Len('" & fullkeyword & "') ) "
                'sql2 &= "+ ((LEN(pl.Item_Value) - LEN(REPLACE(pl.Item_Value,'" & fullkeyword & "',''))) / Len('" & fullkeyword & "') ) "
                'sql2 &= "+ ((LEN(p2.Item_Value) - LEN(REPLACE(p2.Item_Value,'" & fullkeyword & "',''))) / Len('" & fullkeyword & "') ) "
                sql2 &= ") * 2 "
                'sql2 &= " ( "

                For i As Integer = 0 To searchkeywords.Count - 1
                    'If i > 0 Then
                    '    sql2 &= " + ("
                    'End If
                    sql2 &= " when charindex(' " & searchkeywords(i).Trim & " ', pn.Item_Value + ' ')+ charindex(' " & searchkeywords(i).Trim & " ', p.ClientName + ' ') "
                    sql2 &= " + charindex(' " & searchkeywords(i).Trim & " ', pl.Item_Value + ' ')+ charindex(' " & searchkeywords(i).Trim & " ', p2.Item_Value + ' ') "
                    sql2 &= "> 0 then 900 - 100*" & i & " + "
                    sql2 &= " ( ((LEN(pn.Item_Value) - LEN(REPLACE(pn.Item_Value,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) * 7 "
                    sql2 &= "+ ((LEN(p.ClientName) - LEN(REPLACE(p.ClientName,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) * 3 "
                    sql2 &= "+ ((LEN(pl.Item_Value) - LEN(REPLACE(pl.Item_Value,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) "
                    sql2 &= "+ ((LEN(p2.Item_Value) - LEN(REPLACE(p2.Item_Value,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) "
                    sql2 &= " ) * 1 "

                    sql2 &= " when charindex(' " & searchkeywords(i).Trim & ",', pn.Item_Value)+ charindex(' " & searchkeywords(i).Trim & ",', p.ClientName) "
                    sql2 &= " + charindex(' " & searchkeywords(i).Trim & ",', pl.Item_Value)+ charindex(' " & searchkeywords(i).Trim & ",', p2.Item_Value) "
                    sql2 &= "> 0 then 900 - 100*" & i & " + "
                    sql2 &= " ( ((LEN(pn.Item_Value) - LEN(REPLACE(pn.Item_Value,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) * 7 "
                    sql2 &= "+ ((LEN(p.ClientName) - LEN(REPLACE(p.ClientName,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) * 3 "
                    sql2 &= "+ ((LEN(pl.Item_Value) - LEN(REPLACE(pl.Item_Value,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) "
                    sql2 &= "+ ((LEN(p2.Item_Value) - LEN(REPLACE(p2.Item_Value,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) "
                    sql2 &= " ) * 1 "

                    sql2 &= " when charindex(' " & searchkeywords(i).Trim & ".', pn.Item_Value)+ charindex(' " & searchkeywords(i).Trim & ".', p.ClientName) "
                    sql2 &= " + charindex(' " & searchkeywords(i).Trim & ".', pl.Item_Value)+ charindex(' " & searchkeywords(i).Trim & ".', p2.Item_Value) "

                    sql2 &= "> 0 then 900 - 100*" & i & " + "
                    sql2 &= " ( ((LEN(pn.Item_Value) - LEN(REPLACE(pn.Item_Value,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) * 7 "
                    sql2 &= "+ ((LEN(p.ClientName) - LEN(REPLACE(p.ClientName,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) * 3 "
                    sql2 &= "+ ((LEN(pl.Item_Value) - LEN(REPLACE(pl.Item_Value,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) "
                    sql2 &= "+ ((LEN(p2.Item_Value) - LEN(REPLACE(p2.Item_Value,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) "
                    sql2 &= " ) * 1 "

                    sql2 &= " when charindex('" & searchkeywords(i).Trim & "', pn.Item_Value)+ charindex('" & searchkeywords(i).Trim & "', p.ClientName) "
                    sql2 &= " + charindex('" & searchkeywords(i).Trim & "', pl.Item_Value)+ charindex('" & searchkeywords(i).Trim & "', p2.Item_Value) "
                    sql2 &= "> 0 then 850 - 100*" & i & " + "
                    sql2 &= " ( ((LEN(pn.Item_Value) - LEN(REPLACE(pn.Item_Value,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) * 7 "
                    sql2 &= "+ ((LEN(p.ClientName) - LEN(REPLACE(p.ClientName,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) * 3 "
                    sql2 &= "+ ((LEN(pl.Item_Value) - LEN(REPLACE(pl.Item_Value,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) "
                    sql2 &= "+ ((LEN(p2.Item_Value) - LEN(REPLACE(p2.Item_Value,'" & searchkeywords(i).Trim & "',''))) / Len('" & searchkeywords(i).Trim & "') ) "
                    sql2 &= " ) * 1 "
                    'sql2 &= " ) * (10-" & i & ") "
                Next
                'sql2 &= " ) * 10 desc"
                sql2 &= " end  desc"

            End If
            sql2 &= " ) RowID  "
            sql2 &= " from IPM_PROJECT p  join WebProjects wp on p.ProjectID = wp.ProjectID left join #WebAddresses pl on p.ProjectID = pl.ProjectID and pl.Item_ID = (Select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_Address') "
            sql2 &= " left join #WebAddresses p2 on p.ProjectID = p2.ProjectID and p2.Item_ID = (Select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_WEBDESCRIPTION')  "
            sql2 &= "join IPM_PROJECT_FIELD_VALUE pn on p.ProjectID = pn.ProjectID and pn.Item_ID = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJNAME') left outer join ipm_project_field_value v on v.item_id = 2400039 and v.projectid = p.Projectid "
            If Venues = 1 Then
                sql2 &= "left join IPM_PROJECT_DISCIPLINE pd1 on p.ProjectID = pd1.ProjectID and pd1.KeyID = (Select KeyID from IPM_DISCIPLINE where KeyUse = '1' and KeyName = 'B&amp;D Venues' ) "
                sql2 &= "left join IPM_PROJECT_DISCIPLINE pd2 on p.ProjectID = pd2.ProjectID and pd2.KeyID = (Select KeyID from IPM_DISCIPLINE where KeyUse = '1' and KeyName = 'Government / Municipal' ) "
                sql2 &= "left join IPM_PROJECT_DISCIPLINE pd3 on p.ProjectID = pd3.ProjectID and pd3.KeyID = (Select KeyID from IPM_DISCIPLINE where KeyUse = '1' and KeyName = 'Higher Education' ) "
            End If

            sql2 &= " where "
            For i As Integer = 0 To searchkeywords.Count - 1
                If i > 0 Then
                    sql2 &= " or "
                End If
                sql2 &= " p.ClientName like '%" & searchkeywords(i).Trim & "%' or wp.ProjectName like '%" & searchkeywords(i).Trim & "%' "  'or pl.Item_Value like '%" & searchkeywords(i).Trim & "%' or p2.Item_Value like '%" & searchkeywords(i).Trim & "%' "
            Next
            sql2 &= " or ISNULL( pl.ProjectID , p2.ProjectID ) is not null "
            sql2 &= ")t where RowID >= " & startdisplay & " and RowID <=  " & (CInt(startdisplay) + 9).ToString.Trim & " Order by RowID"
        End If

        Dim DT2 As New DataTable("FProjects")
        'Response.Write(sql2)
        'Response.Write("<br />")
        'Return
        DT2 = mmfunctions.GetDataTable(sql2)
        'Response.Write("Sql2 end - " & Now.ToString & "<br />")


        '--------------------------------SQL2 END--------------------------------------'
        '--------------------------------SQL2 END--------------------------------------'
        '--------------------------------SQL2 END--------------------------------------'



        Dim out As String = "<div class=""project_sort"">"
        out &= "<span class=""sb"">Sort By</span>"
        out &= "<a title=""Sort By Most Recent"" class=""project_sort_t "
        If sort = "t" Then
            out &= " selected "
        End If

        If sort = "" Then
            out &= " selected "
        End If

        out &= """>MOST RECENT</a>"
        out &= "<em class=""orderSeparator"">|</em>"
        out &= "<a title=""Sort Alphabetically"" class=""project_sort_a"
        If sort = "a" Then
            out &= " selected "
        End If
        out &= """>A - Z</a>"
        If searchkeywords.Count > 0 Then
            out &= "<em class=""relevance"">&nbsp;|"
            out &= "<a title=""Sort by Relevance"" class=""project_sort_r"
            If sort = "" Or sort = "r" Then
                out &= " selected "
            End If
            out &= """>Relevance</a>"
            out &= "</em>"
        End If

        out &= "<em>Showing "
        out &= "<span class=""countStart"">" & startdisplay & "</span> - "
        Dim endindex As String = (CInt(startindex) + CInt(DT2.Rows.Count.ToString.Trim)).ToString.Trim
        out &= "<span class=""countThrough"">" & endindex & "</span> of "
        out &= "<span class=""countTotal"">" & DT1.Rows(0)("kount").ToString.Trim & "</span>"
        out &= "</em>"
        out &= "</div>"

        If DT2.Rows.Count > 0 Then
            out &= "<div class=""project_list paginate""><dl><dt><em>Client / project name</em><span>Location</span></dt>"
            For i As Integer = 0 To DT2.Rows.Count - 1
                Dim clientname As String = DT2.Rows(i)("ClientName").ToString.Trim
                Dim projectname As String = DT2.Rows(i)("ProjectName").ToString.Trim
                Dim location As String = DT2.Rows(i)("location").ToString.Trim
                If searchkeywords.Count = 0 Then
                    out &= "<dd><a href=""" & Regex.Replace(projectname.ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-casestudy-" & DT2.Rows(i)("ProjectID").ToString.Trim & """><strong>" & DT2.Rows(i)("ClientName").ToString.Trim & "</strong><span><span>" & DT2.Rows(i)("ProjectName").ToString.Trim & "</span><em>" & DT2.Rows(i)("location").ToString.Trim & "</em></span></a></dd>"
                Else
                    For j As Integer = 0 To searchkeywords.Count - 1
                        clientname = Regex.Replace(clientname, "(" & searchkeywords(j) & ")", "<span class=""highight"">$1</span>", RegexOptions.IgnoreCase)
                        projectname = Regex.Replace(projectname, "(" & searchkeywords(j) & ")", "<span class=""highight"">$1</span>", RegexOptions.IgnoreCase)
                        location = Regex.Replace(location, "(" & searchkeywords(j) & ")", "<span class=""highight"">$1</span>", RegexOptions.IgnoreCase)
                        'Response.Write(searchkeywords(j) & " - " & Regex.Escape(searchkeywords(j)) & "<br />")
                        'Response.Write(Regex.Replace("ME Me me", "(me)", "*$1*", RegexOptions.IgnoreCase))
                        'Response.Write(location)
                    Next
                    out &= "<dd><a href=""" & Regex.Replace(projectname.ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-casestudy-" & DT2.Rows(i)("ProjectID").ToString.Trim & """><strong>" & clientname & "</strong><span><span>" & projectname & "</span><em>" & location & "</em></span></a></dd>"
                End If
            Next
            out &= "</dl></div>"
        End If

        out &= "<p class=""pagination"">"
        If startindex <> "0" Then
            out &= "<a class=""previous"">&lt;&lt; Previous</a>"
        End If
        If CInt(endindex) < CInt(DT1.Rows(0)("kount").ToString.Trim) Then
            out &= "<a class=""next"">Next &gt;&gt;</a>"
        End If
        out &= "</p>"
        'Response.Write("Page out time - " & Now.ToString & "<br />")

        Response.Write(out)
        'Response.Write("Page done time - " & Now.ToString & "<br />")

    End Sub

End Class
'<div class="project_sort">
'    <span class="sb">Sort By</span>
'    <a title="Sort By Most Recent" class="project_sort_t selected">MOST RECENT</a>
'    <em class="orderSeparator">|</em>
'    <a title="Sort Alphabetically" class="project_sort_a">A - Z</a>
'    <em class="relevance">&nbsp;|
'        <a title="Sort by Relevance" class="project_sort_r">Relevance</a>
'    </em>
'    <em>Showing
'        <span class="countStart">1</span> -
'        <span class="countThrough">10</span> of
'        <span class="countTotal">729</span>
'    </em>
'</div>
'<div class="project_list paginate">
'    <dl>
'        <dt>
'            <em>Client / project name</em>
'            <span>Location</span>
'        </dt>
'        <dd>
'            <a href="casestudy-2408857"><strong>Jackson, MS, City of</strong><span><span>Performing Arts Facility Market and Financial Analysis</span><em>Jackson, MS</em></span></a>
'        </dd>
'        <dd>
'            <a href="casestudy-2408791"><strong>Morven Park</strong><span><span>Outdoor Field Business Planning</span><em>Leesburg, VA</em></span></a>
'        </dd>
'        <dd>
'            <a href="casestudy-2408233"><strong>Montevallo, University of</strong><span><span>Capital Projects Master Plan</span><em>Montevallo, AL</em></span></a>
'        </dd>
'        <dd>
'            <a href="casestudy-2408697"><strong>Jackson, MS, City of</strong><span><span>Arena Feasibility Study</span><em>Jackson, MS</em></span></a>
'        </dd>
'        <dd>
'            <a href="casestudy-2408751"><strong>Maricopa, AZ, City of</strong><span><span>Community Recreation Center Market Study</span><em>Maricopa, AZ</em></span></a>
'        </dd>
'        <dd>
'            <a href="casestudy-2408502"><strong>Dauphin County Redevelopment Authority</strong><span><span>Sports Complex Feasibility Study</span><em>Harrisburg, PA</em></span></a>
'        </dd>
'        <dd>
'            <a href="casestudy-2408248"><strong>2 M Street Redevelopment, LLC</strong><span><span>Private Housing Construction Draw Inspections Services</span><em>Washington, DC</em></span></a>
'        </dd>
'        <dd>
'            <a href="casestudy-2408256"><strong>Alexandria City Public Schools</strong><span><span>New School Program Management - &nbsp;Jefferson-Houston School</span><em>Alexandria, VA</em></span></a>
'        </dd>
'        <dd>
'            <a href="casestudy-2408909"><strong>Sustain Sports Denver, LLC</strong><span><span>Sports Complex Feasibility Study</span><em>Douglas County, CO</em></span></a>
'        </dd>
'        <dd>
'            <a href="casestudy-2408354"><strong>Beauvoir, The National Cathedral Elementary School</strong><span><span>Outdoor Recreation Program Management</span><em>Washington, DC</em></span></a>
'        </dd>
'    </dl>
'</div>
'<p class="pagination">
'    <a class="previous">&lt;&lt; Previous</a>
'    <a class="next">Next &gt;&gt;</a>
'</p>