Imports System
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Web.Configuration
Imports System.Web.UI

Partial Public Class image
    Inherits System.Web.UI.Page

    ' imgxCache=fresh
    ' imgxType=project
    ' imgximgxDimensions=500x200
    ' imgxWidth=500
    ' imgxHeight=200
    ' imgxQuality=8
    ' imgxCrop=1
    ' imgxColor=ff0000
    ' imgxAlign=NorthEast
    ' imgxId=2403059
    ' imgxExtention=jpg

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim imgxCache As String = ""
        If Not Request.QueryString("imgxCache") Is Nothing Then
            imgxCache = Request.QueryString("imgxCache")
        End If
        Dim imgxType As String = ""
        If Not Request.QueryString("imgxType") Is Nothing Then
            imgxType = Request.QueryString("imgxType")
        End If
        Dim imgxDimensions As String = ""
        If Not Request.QueryString("imgxDimensions") Is Nothing Then
            imgxDimensions = Request.QueryString("imgxDimensions")
        End If
        Dim widthheight() As String = imgxDimensions.Split("x")

        Dim imgxWidth As String = widthheight(0)
        Dim imgxHeight As String = ""
        If widthheight.Length > 1 Then
            imgxHeight = widthheight(1)
        End If
        If imgxheight = "" Then
            imgxheight = imgxwidth
            If imgxheight = "" Then
                imgxheight = "1"
            End If
        ElseIf imgxwidth = "" Then
            imgxwidth = imgxheight
            If imgxwidth = "" Then
                imgxwidth = "1"
            End If
        End If
        Dim imgxQuality As String = ""
        If Not Request.QueryString("imgxQuality") Is Nothing Then
            imgxQuality = Request.QueryString("imgxQuality")
        End If
        Dim imgxCrop As String = ""
        If Not Request.QueryString("imgxCrop") Is Nothing Then
            imgxCrop = Request.QueryString("imgxCrop")
        End If
        Dim imgxColor As String = ""
        If Not Request.QueryString("imgxColor") Is Nothing Then
            imgxColor = Request.QueryString("imgxColor")
        End If
        Dim imgxAlign As String = ""
        If Not Request.QueryString("imgxAlign") Is Nothing Then
            imgxAlign = Request.QueryString("imgxAlign")
        End If
        Dim imgxId As String = ""
        If Not Request.QueryString("imgxId") Then
            imgxId = Request.QueryString("imgxId")
        End If
        Dim url As String = Request.Url.ToString()
        Dim imgxExtention As String = Request.QueryString("imgxExtension")

        'get the file from iDAM
        Dim temp As String = Session("WSRetreiveAsset").ToString.Trim
        temp &= "id=" & imgxId & "&type=" & imgxType & "&width=9999&height=9999&crop=0&cache=1&qfactor=0"
        Dim filepath As String = Server.MapPath("~") & "dynamic\image\cache\" & imgxType & "\" & imgxCrop & "\" & imgxDimensions & "\" & imgxQuality & "\" & imgxColor & "\" & imgxAlign & "\"
        Dim srcpath As String = Server.MapPath("~") & "dynamic\image\idam\" & imgxType & "\"
        Dim srcfile As String = imgxId
        Dim fullfilename As String = filepath & imgxId & "." & imgxExtention

        If Not Directory.Exists(filepath) Then
            Try
                Directory.CreateDirectory(filepath)
            Catch ex As Exception
            End Try
        End If
        If Not File.Exists(srcpath & srcfile) Then
            If Not Directory.Exists(srcpath) Then
                Try
                    Directory.CreateDirectory(srcpath)
                Catch ex As exception
                End Try
            End If
            Try
                Dim request As WebRequest = WebRequest.Create(temp)
                request.Credentials = CredentialCache.DefaultCredentials
                Dim response As HttpWebResponse = CType(request.GetResponse(), HttpWebResponse)
                Dim img As System.Drawing.Image = System.Drawing.Image.FromStream(response.GetResponseStream())
                img.Save(srcpath & srcfile)
                Try
                    response.Close()
                Catch ex1 As Exception
                End Try
            Catch ex As Exception
                Response.Redirect("/dynamic/clear.png", True)
            End Try
        End If

        If Not File.Exists(fullfilename) Then
            'spot
            'convert -size 200x200 xc:#aaffaa null: (  C:\\WebSites\\dynamic.ifmmdev.com\\dynamic\\image\\demo\\2403059 -gravity SouthWest -crop 200x200+0+0 +repage ) -gravity SouthWest -layers Composite -format jpg -unsharp 0x.5 -strip -quality 80  C:\\WebSites\\dynamic.ifmmdev.com\\dynamic\\image\\demo\\spot.jpg

            'padded
            'convert -size 200x200 xc:#aaffaa null: (  C:\\WebSites\\dynamic.ifmmdev.com\\dynamic\\image\\demo\\2403059 -gravity SouthWest -resize "200x200>" -crop 200x200+0+0 +repage ) -gravity SouthWest -layers Composite -format jpg -unsharp 0x.5 -strip -quality 80  C:\\WebSites\\dynamic.ifmmdev.com\\dynamic\\image\\demo\\padded.jpg

            'best
            'convert -size 400x100 xc:#aaffaa null: (  C:\\WebSites\\dynamic.ifmmdev.com\\dynamic\\image\\demo\\2403059 -resize "400x100^>" ) -gravity Center -layers Composite -format jpg -unsharp 0x.5 -strip -quality 80 C:\\WebSites\\dynamic.ifmmdev.com\\dynamic\\image\\demo\\best.jpg

            'edge
            'convert -size 200x200 xc:"#11ccaa00" null: ( C:\\WebSites\\dynamic.ifmmdev.com\\dynamic\\image\\demo\\2403059 -thumbnail "200x200" +repage ( +clone -shave 10x10 -fill gray50 -colorize 100% -mattecolor gray50 -frame 10x10+3+4 -blur 0x2 ) -compose HardLight -composite ) -gravity Center -layers Composite -format jpg -unsharp 0x.5 -strip -quality 90 C:\\WebSites\\dynamic.ifmmdev.com\\dynamic\\image\\demo\\edge.jpg

            'liquid
            'convert tree.png -resize x200 -unsharp 0x.5 -strip -quality 80 zz4.jpg

            Dim fileargs As String = ""
            Select Case imgxCrop
                        Case "spot"
                            fileargs = " -size " & imgxDimensions & " xc:#" & imgxColor & " null: (  " & srcpath.Replace("\", "\\") & srcfile & " -gravity " & imgxAlign & " -crop " & imgxDimensions & "+0+0 +repage ) -gravity " & imgxAlign & " -layers Composite -format " & imgxExtention & " -unsharp 0x.5 -strip -quality " & imgxQuality & "  " & fullfilename.Replace("\", "\\")
                        Case "padded"
                            fileargs = " -size " & imgxDimensions & " xc:#" & imgxColor & " null: (  " & srcpath.Replace("\", "\\") & srcfile & " -gravity " & imgxAlign & " -resize """ & imgxDimensions & ">"" -crop " & imgxDimensions & "+0+0 +repage ) -gravity " & imgxAlign & " -layers Composite -format " & imgxExtention & " -unsharp 0x.5 -strip -quality " & imgxQuality & "  " & fullfilename.Replace("\", "\\")
                        Case "grey"
                            fileargs = " -size " & imgxDimensions & " xc:#" & imgxColor & " null: (  " & srcpath.Replace("\", "\\") & srcfile & " -gravity " & imgxAlign & " -resize """ & imgxDimensions & ">"" -crop " & imgxDimensions & "+0+0 +repage ) -gravity " & imgxAlign & " -layers Composite -channel RGBA -matte -colorspace gray -brightness-contrast 1x15 -format " & imgxExtention & " -unsharp 0x.15 -strip -quality " & imgxQuality & "  " & fullfilename.Replace("\", "\\")
                        Case "best"
                            fileargs = " -size " & imgxDimensions & " xc:#""" & imgxColor & """ null: (  " & srcpath.Replace("\", "\\") & srcfile & " -resize """ & imgxDimensions & "^>"" ) -gravity " & imgxAlign & " -layers Composite -format " & imgxExtention & " -unsharp 0x.5 -strip -quality " & imgxQuality & " " & fullfilename.Replace("\", "\\")
                        Case "edge"
                            fileargs = " -size " & imgxDimensions & " xc:""#" & imgxColor & "00"" null: ( " & srcpath.Replace("\", "\\") & srcfile & " -thumbnail """ & imgxDimensions & """ +repage ( +clone -shave 10x10 -fill gray50 -colorize 100% -mattecolor gray50 -frame 10x10+3+4 -blur 0x2 ) -compose HardLight -composite ) -gravity " & imgxAlign & " -layers Composite -format " & imgxExtention & " -unsharp 0x.5 -strip -quality " & imgxQuality & " " & fullfilename.Replace("\", "\\")
                        Case "liquid"
                            fileargs = " " & srcpath.Replace("\", "\\") & srcfile & " -resize " & imgxDimensions & " -unsharp 0x.5 -quality " & imgxQuality & " " & fullfilename.Replace("\", "\\")
                        Case "fit"
                            fileargs = " " & srcpath.Replace("\", "\\") & srcfile & " -resize " & imgxDimensions & "> -unsharp 0x.5 -quality " & imgxQuality & " " & fullfilename.Replace("\", "\\")
                        Case "painted"
                            fileargs = " -size " & imgxDimensions & " xc:#" & imgxColor & " null: (  " & srcpath.Replace("\", "\\") & srcfile & " -gravity " & imgxAlign & " -resize """ & imgxDimensions & ">"" -crop " & imgxDimensions & "+0+0 +repage ) -gravity " & imgxAlign & " -layers Composite   -fuzz 4% -fill ""#" & imgxColor & """  -opaque ""#ffffff""        -format " & imgxExtention & " -unsharp 0x.5 -strip -quality " & imgxQuality & "  " & fullfilename.Replace("\", "\\")
                        Case Else
                            fileargs = " -size " & imgxHeight & "x" & imgxWidth & " xc:'" & imgxColor & "' " & fullfilename
            End Select
            Try

                'WebEventLog.WriteToEventLog("fileargs - " & fileargs)
                Dim proc As New Diagnostics.Process()
                proc.StartInfo.Arguments = fileargs
                proc.StartInfo.FileName = "C:\Program Files\ImageMagick-6.7.8-Q16\convert.exe"
                proc.StartInfo.UseShellExecute = False
                proc.StartInfo.CreateNoWindow = True
                proc.StartInfo.RedirectStandardOutput = True
                proc.StartInfo.RedirectStandardError = True
                proc.Start()
                proc.BeginOutputReadLine()
                Dim output As String = proc.StandardError.ReadToEnd()
                Try
                    proc.Kill()
                    proc.WaitForExit()
                Catch ex As Exception
                End Try
                'WebEventLog.WriteToEventLog("results - " & output)
            Catch ex As Exception
                Response.Redirect("/dynamic/clear.png", True)
            End Try

        End If

        Dim T1970 As New DateTime(1970, 1, 1)
        Dim Timestamp As Integer = (DateTime.Now.ToUniversalTime - T1970).TotalSeconds

        Response.Clear()
        Response.ClearHeaders()

        Select Case imgxCache.ToUpper
            Case "FRESH"

              Response.Cache.SetCacheability(HttpCacheability.NoCache)
              Response.Cache.SetValidUntilExpires(false)
              Response.Cache.SetNoStore()
              Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches)
              Response.Expires = -1

            Case "DAY"
                If Not Request.Headers("If-None-Match") Is Nothing Then
                    Dim tExpire As String = Request.Headers("If-None-Match")
                    If Timestamp < (tExpire) Then
                        Response.StatusCode = 304
                        Response.StatusDescription = "Not Modified"
                        Return
                    End If
                End If
                Response.Cache.SetCacheability(HttpCacheability.Public)
                Response.Cache.SetValidUntilExpires(true)
                Response.Cache.SetETag(Timestamp + 86400 )
                Response.Cache.SetMaxAge(TimeSpan.FromDays(1))
                Response.Cache.SetLastModified( DateTime.Now)
                Response.AppendHeader("Connection", "close")

            Case "WEEK"
                If Not Request.Headers("If-None-Match") Is Nothing Then
                    Dim tExpire As String = Request.Headers("If-None-Match")
                    If Timestamp < (tExpire) Then
                        Response.StatusCode = 304
                        Response.StatusDescription = "Not Modified"
                        Return
                    End If
                End If
                Response.Cache.SetCacheability(HttpCacheability.Public)
                Response.Cache.SetValidUntilExpires(true)
                Response.Cache.SetETag(Timestamp + 604800 )
                Response.Cache.SetMaxAge(TimeSpan.FromDays(30))
                Response.Cache.SetLastModified( DateTime.Now)
                Response.AppendHeader("Connection", "close")

            Case "MONTH"
                If Not Request.Headers("If-None-Match") Is Nothing Then
                    Dim tExpire As String = Request.Headers("If-None-Match")
                    If Timestamp < (tExpire) Then
                        Response.StatusCode = 304
                        Response.StatusDescription = "Not Modified"
                        Return
                    End If
                End If
                Response.Cache.SetCacheability(HttpCacheability.Public)
                Response.Cache.SetValidUntilExpires(true)
                Response.Cache.SetETag(Timestamp + 2592000 )
                Response.Cache.SetMaxAge(TimeSpan.FromDays(30))
                Response.Cache.SetLastModified( DateTime.Now)
                Response.AppendHeader("Connection", "close")

            Case "ALWAYS"
                Response.Cache.SetCacheability(HttpCacheability.Public)
                Response.Cache.SetValidUntilExpires(true)
                Response.Cache.SetExpires(DateTime.Now.AddYears(5))

            Case "FOREVER"
                If Not Request.Headers("If-Modified-Since") Is Nothing Then
                    Response.StatusCode = 304
                    Response.StatusDescription = "Not Modified"
                    Return
                Else
                    Response.Cache.SetCacheability(HttpCacheability.Public)
                    Response.AppendHeader("Pragma", "public")
                    Response.AppendHeader("Connection", "close")
                    Response.AppendHeader("Last-Modified", "1325376000 GMT")
                    Response.AppendHeader("Expires", "1767139201 GMT")
                    Response.AppendHeader("ETag", "Dyno")
                End If
            Case Else
                Response.Redirect("/dynamic/clear.png", True)
        End Select

        Dim theImg As System.Drawing.Image
        theImg = System.Drawing.Image.FromFile(fullfilename)

        Select Case imgxExtention
            Case "jpg"
                Response.ContentType = "image/jpeg"
                theImg.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg)
            Case "gif"
                Response.ContentType = "image/gif"
                theImg.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif)
            Case "png"
                Response.ContentType = "image/png"
                theImg.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Png)
        End Select


    End Sub

End Class