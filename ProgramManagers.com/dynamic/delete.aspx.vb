Imports System
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Web.Configuration
Imports System.Web.UI

Partial Public Class delete_image
    Inherits System.Web.UI.Page
    Public SuccessDelMsg As String = ""
    Public ErrorDelMsg As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.CacheControl = "no-cache"
        Response.AddHeader("Pragma", "no-cache")
        Response.AddHeader("Pragma", "no-store")
        Response.AddHeader("cache-control", "no-cache")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetNoServerCaching()


        Dim imgxId As String = ""
        If Not Request.QueryString("imgxId") Is Nothing Then
            imgxId = Request.QueryString("imgxId")
        End If

        Dim filepath As String = Server.MapPath("~") & "dynamic\image\cache\"
        Dim srcpath As String = Server.MapPath("~") & "dynamic\image\idam\"
        DirDeleteFiles(filepath, imgxId & ".*")
        DirDeleteFiles(srcpath, imgxId & ".*")

        Dim logfile As String = Server.MapPath("~") & "dynamic\delete.log.txt"
        Dim LogString As String = ""

        If SuccessDelMsg <> "" Then
            LogString &= SuccessDelMsg & vbNewLine
        End If
        If ErrorDelMsg <> "" Then
            LogString &= ErrorDelMsg & vbNewLine
        End If


        Dim fs As FileStream = Nothing
        If File.Exists(logfile) Then

            Dim CurrentLogString As String()
            Using sr As New StreamReader(logfile)
                CurrentLogString = sr.ReadToEnd().Split(vbNewLine)
                sr.Close()
            End Using

            Dim byteData() As Byte
            byteData = Encoding.ASCII.GetBytes(LogString)

            fs = New FileStream(logfile, FileMode.Append)

            fs.Write(byteData, 0, byteData.Length)
            fs.Close()
            If LogString <> "" Then
                Response.Write(LogString)
            End If

        End If

        Response.Status = "307 Temporary Redirect"
        Response.AddHeader("Location", Request.Url.GetLeftPart(UriPartial.Authority) & HttpContext.Current.Request.RawUrl & "ed&" & CMSFunctions.GetRandomKey())
        Response.Write(Request.Url.GetLeftPart(UriPartial.Authority) & HttpContext.Current.Request.RawUrl & "ed&" & CMSFunctions.GetRandomKey())



    End Sub



    Sub DirDeleteFiles(ByVal sDir As String, ByVal thefile As String)
        Try
            For Each d In Directory.GetDirectories(sDir)
                For Each f In Directory.GetFiles(d, thefile, SearchOption.AllDirectories = True)
                    File.Delete(f)
                    SuccessDelMsg &= f & vbNewLine
                Next
                DirDeleteFiles(d, thefile)
            Next
        Catch excpt As System.Exception
            ErrorDelMsg &= excpt.Message & vbNewLine
        End Try
    End Sub



End Class