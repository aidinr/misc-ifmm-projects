Imports System.Web
Imports System.Data.SqlClient
Imports System.Data

Partial Class sitemap
    Inherits System.Web.UI.Page
    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim out As String = "<?xml version=""1.0"" encoding=""UTF-8"" ?>"  & vbNewLine
        out &= "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">" & vbNewLine
        Dim url() As String = { _
"http://www.programmanagers.com/", _
"http://www.programmanagers.com/about-us", _
"http://www.programmanagers.com/services", _
"http://www.programmanagers.com/portfolio", _
"http://www.programmanagers.com/resources", _
"http://www.programmanagers.com/careers", _
"http://www.programmanagers.com/mission", _
"http://www.programmanagers.com/history", _
"http://www.programmanagers.com/associations", _
"http://www.programmanagers.com/community", _
"http://www.programmanagers.com/news", _
"http://www.programmanagers.com/sustainability", _
"http://www.programmanagers.com/people-overview", _
"http://www.programmanagers.com/people-profiles", _
"http://www.programmanagers.com/resources-publications", _
"http://www.programmanagers.com/resources-presentations", _
"http://www.programmanagers.com/opportunities", _
"http://www.programmanagers.com/culture", _
"http://www.programmanagers.com/terms", _
"http://www.programmanagers.com/privacy", _
"http://www.programmanagers.com/submarket_Higher_Education__Academic_-slash-_Administrative", _
"http://www.programmanagers.com/related-resources_Education__Academic_-slash-_Administrative", _
"http://www.programmanagers.com/related-resources_Higher_Education__Athletics", _
"http://www.programmanagers.com/related-resources_Higher_Education__Campus_Edge", _
"http://www.programmanagers.com/related-resources_Higher_Education__Food_Service_-slash-_Retail", _
"http://www.programmanagers.com/related-resources_Higher_Education__Recreation_-amp-_Wellness", _
"http://www.programmanagers.com/related-resources_Higher_Education__Student_Housing", _
"http://www.programmanagers.com/related-resources_Higher_Education__Student_Unions", _
"http://www.programmanagers.com/related-resources_Higher_Education__Venues", _
"http://www.programmanagers.com/related-resources_Higher_Education__Workforce_Housing", _
"http://www.programmanagers.com/related-resources_Government_-slash-_Municipal__Parks_-amp-_Recreation", _
"http://www.programmanagers.com/related-resources_Government_-slash-_Municipal__Venues", _
"http://www.programmanagers.com/related-resources_PRE-dash-K_-dash-_12__Charter", _
"http://www.programmanagers.com/related-resources_PRE-dash-K_-dash-_12__Private", _
"http://www.programmanagers.com/related-resources_PRE-dash-K_-dash-_12__Public", _
"http://www.programmanagers.com/related-resources_Private__Developer", _
"http://www.programmanagers.com/related-resources_B-amp-D_Venues__Arenas_-slash-_Convocation_Centers", _
"http://www.programmanagers.com/related-resources_B-amp-D_Venues__Ballparks", _
"http://www.programmanagers.com/related-resources_B-amp-D_Venues__Conference_-slash-_Convention_Centers", _
"http://www.programmanagers.com/related-resources_B-amp-D_Venues__Stadiums", _
"http://www.programmanagers.com/related-resources_B-amp-D_Venues__Performing_Arts", _
"http://www.programmanagers.com/submarket_Higher_Education__Athletics", _
"http://www.programmanagers.com/submarket_Higher_Education__Campus_Edge", _
"http://www.programmanagers.com/submarket_Higher_Education__Food_Service_-slash-_Retail", _
"http://www.programmanagers.com/submarket_Higher_Education__Recreation_-amp-_Wellness", _
"http://www.programmanagers.com/submarket_Higher_Education__Student_Housing", _
"http://www.programmanagers.com/submarket_Higher_Education__Student_Unions", _
"http://www.programmanagers.com/submarket_Higher_Education__Venues", _
"http://www.programmanagers.com/submarket_Higher_Education__Workforce_Housing", _
"http://www.programmanagers.com/submarket_Government_-slash-_Municipal__Parks_-amp-_Recreation", _
"http://www.programmanagers.com/submarket_Government_-slash-_Municipal__Venues", _
"http://www.programmanagers.com/submarket_PRE-dash-K_-dash-_12__Charter", _
"http://www.programmanagers.com/submarket_PRE-dash-K_-dash-_12__Private", _
"http://www.programmanagers.com/submarket_PRE-dash-K_-dash-_12__Public", _
"http://www.programmanagers.com/submarket_Private__Developer", _
"http://www.programmanagers.com/submarket_B-amp-D_Venues__Arenas_-slash-_Convocation_Centers", _
"http://www.programmanagers.com/submarket_B-amp-D_Venues__Ballparks", _
"http://www.programmanagers.com/submarket_B-amp-D_Venues__Conference_-slash-_Convention_Centers", _
"http://www.programmanagers.com/submarket_B-amp-D_Venues__Stadiums", _
"http://www.programmanagers.com/submarket_B-amp-D_Venues__Performing_Arts" _
}

        Dim yesterday As String = Date.Now.AddDays(-1).ToString("yyyy-MM-dd")
        For i As Integer = 0 To url.Length - 1
            out &= "<url><loc>" & url(i).Trim & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
        Next

        Dim sql As String = "Select FirstName + ' ' + LastName FullName, u.UserID, CONVERT(char(10), u.Update_Date,126) update_date from IPM_USER u  join IPM_USER_FIELD_VALUE vc on u.UserID = vc.USER_ID and vc.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where Item_Tag = 'IDAM_STAFFCATEGORY') and vc.Item_value <> ''  where Active = 'Y' and Contact = '0'"
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)

        For i As Integer = 0 To DT1.Rows.Count - 1
            'http://www.programmanagers.com/paul_brailsford-profile-2406835
            'Regex.Replace(DT1.DefaultView(i)("FullName").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-profile-" & DT1.DefaultView(i)("UserID").ToString.Trim
            'out &= "<url><loc>http://www.programmanagers.com/" & DT1.Rows(i)("FullName").ToString.Trim.Replace(" ", "_") & "-profile-" & DT1.Rows(i)("UserID").ToString.Trim & "</loc><lastmod>" & DT1.Rows(i)("update_date").ToString.Trim & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
            out &= "<url><loc>http://www.programmanagers.com/" & Regex.Replace(DT1.Rows(i)("FullName").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-profile-" & DT1.Rows(i)("UserID").ToString.Trim & "</loc><lastmod>" & DT1.Rows(i)("update_date").ToString.Trim & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
        Next

        Dim sql2 As String = "select  p.projectid, CONVERT(char(10), p.Update_Date,126) Update_Date, isnull(v2.item_value,'') as Project_Name from IPM_PROJECT p join IPM_PROJECT_FIELD_VALUE v on p.ProjectID = v.ProjectID and v.Item_ID = (Select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_FEATURE') and v.Item_Value = '1' left outer join ipm_project_field_Value v2 on v2.item_id = 2400039 and v2.projectid = p.projectid   where Available = 'Y' "
        Dim DT2 As New DataTable("FProjects")
        DT2 = mmfunctions.GetDataTable(sql2)

        For i As Integer = 0 To DT2.Rows.Count - 1
            out &= "<url><loc>http://www.programmanagers.com/" & Regex.Replace(DT2.Rows(i)("Project_Name").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-casestudy-" & DT2.Rows(i)("ProjectID").ToString.Trim & "</loc><lastmod>" & DT2.Rows(i)("update_date").ToString.Trim & "</lastmod><changefreq>weekly</changefreq><priority>1.0</priority></url>" & vbNewLine
        Next
        out &= "</urlset>"

        Response.Write(out)

    End Sub

End Class
'
