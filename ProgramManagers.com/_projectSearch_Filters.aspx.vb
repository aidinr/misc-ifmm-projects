Imports System.Web
Imports System.Data.SqlClient
Imports System.Data


Partial Class _projectSearch_Filters
    Inherits System.Web.UI.Page
    Protected Function FormatTag(TagType As String, Tag As String) As String

        Dim formatedTag As String = " "
        Dim temp As String = Tag.Trim.Replace("&amp;", "&")
        temp = temp.Replace(" ", "_")
        temp = temp.Replace("-", "-dash-")
        temp = temp.Replace("&", "-amp-")
        temp = temp.Replace("/", "-slash-")
        temp = temp.Replace("|", "")

        Dim matchpattern As String = "[^\w-_]"
        Dim replace As String = ""
        temp = Regex.Replace(temp, matchpattern, replace)

        formatedTag = TagType & temp & " "
        Return formatedTag

    End Function
    Protected Function DecodeTag(TagType As String, Tag As String) As String

        Dim temp As String = Tag.Trim.Replace(TagType, "")
        temp = temp.Replace("-slash-", "/")
        temp = temp.Replace("-amp-", "&")
        temp = temp.Replace("-dash-", "-")
        temp = temp.Replace("_", " ")
        temp = temp.Replace("&", "&amp;")
        Return temp

    End Function
    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Response.Write("Page load time" & Now.ToString & "<br />")
        Dim market As String = ""
        Dim decodedMarket As String = ""
        If Not Request.QueryString("market") Is Nothing Then
            market = Request.QueryString("market")
            decodedMarket = DecodeTag("market_", market)
        End If

        Dim submarket As String = ""
        Dim decodedSubMarket As String = ""
        If Not Request.QueryString("submarket") Is Nothing Then
            submarket = Request.QueryString("submarket")
            decodedSubMarket = DecodeTag("submarket_", submarket)
        End If

        Dim service As New System.Collections.Generic.List(Of String)
        If Not Request.QueryString("Service[]") Is Nothing Then
            service.AddRange(Request.QueryString("Service[]").Split(","))
        End If

        Dim build As New System.Collections.Generic.List(Of String)
        If Not Request.QueryString("Built[]") Is Nothing Then
            build.AddRange(Request.QueryString("Built[]").Split(","))
            'For i As Integer = 0 To build.Count - 1
            '    Response.Write(build(i).Trim & "<br />")
            'Next
        End If

        Dim sustainable As New System.Collections.Generic.List(Of String)
        If Not Request.QueryString("Sustainable[]") Is Nothing Then
            sustainable.AddRange(Request.QueryString("Sustainable[]").Split(","))
        End If

        Dim filterState As String = ""
        If Not Request.QueryString("state") Is Nothing Then
            filterState = Request.QueryString("state")
        End If


        'Response.Write("Done parms -" & Now.ToString & "<br />")

        Dim out As String = ""

        'Response.Write("start sql " & Now.ToString & "<br />")

        Dim sql As String = "With WebProjects( ProjectID ) as (Select p.ProjectID from IPM_PROJECT p join ipm_project_field_value wa on p.ProjectID = wa.ProjectID and  wa.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_WEBACTIVE') and wa.Item_Value = '1' join IPM_PROJECT_FIELD_VALUE pn on p.ProjectID = pn.ProjectID and pn.Item_ID = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJNAME') and pn.Item_Value <> '' where p.Available = 'Y' and p.ClientName <> '' "
        If filterState <> "" Then
            sql &= " and p.State_id  = '" & filterState & "' "
        End If
        sql &= " )Select KeyName, count(*) kount from IPM_DISCIPLINE d join IPM_PROJECT_DISCIPLINE pd on d.KeyID = pd.KeyID and d.KeyUse = 1 join WebProjects wp on pd.ProjectID = wp.ProjectID group by KeyName"
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)
        If DT1.Rows.Count > 0 Then
            out = "<dl class=""filter_Markets""><dt>Markets</dt>"
            For i As Integer = 0 To DT1.Rows.Count - 1
                out &= "<dd><a class=""" & FormatTag("market_", DT1.Rows(i)("KeyName").ToString.Trim)
                If decodedMarket = DT1.Rows(i)("KeyName").ToString.Trim Then
                    out &= " selected "
                End If
                out &= """>" & DT1.Rows(i)("KeyName").ToString.Trim & "</a> <span>(" & DT1.Rows(i)("kount").ToString.Trim & ")</span></dd>"
            Next
            out &= "</dl>"
        End If
        'Response.Write("end sql " & Now.ToString & "<br />")

        If market <> "" Then
            'Response.Write("start sql2code" & Now.ToString & "<br />")

            Dim sql2 As String = "With WebProjects( ProjectID ) as ( Select p.ProjectID from IPM_PROJECT p join ipm_project_field_value wa on p.ProjectID = wa.ProjectID and  wa.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_WEBACTIVE') and wa.Item_Value = '1' join IPM_PROJECT_FIELD_VALUE pn on p.ProjectID = pn.ProjectID and pn.Item_ID = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJNAME') and pn.Item_Value <> '' "
            sql2 &= "join IPM_PROJECT_DISCIPLINE pd on p.ProjectID = pd.ProjectID join IPM_DISCIPLINE d on pd.KeyID = d.KeyID and d.KeyUse = '1' and d.KeyName = '" & decodedMarket & "'"
            sql2 &= " where p.Available = 'Y' and p.ClientName <> '' "
            If filterState <> "" Then
                sql2 &= " and p.State_id  = '" & filterState & "' "
            End If
            sql2 &= " ) Select KeyName, count(*) kount from WebProjects wp join IPM_PROJECT_KEYWORD pk on wp.ProjectID = pk.ProjectID join IPM_KEYWORD k on pk.KeyID = k.KeyID  and k.KeyName like '" & decodedMarket & "%' group by KeyName "
            Dim DT2 As New DataTable("FProjects")
            'Response.Write(sql2 & "<br />")
            'Response.Write("end sql2code" & Now.ToString & "<br />")
            DT2 = mmfunctions.GetDataTable(sql2)
            'Response.Write("end sql2 run" & Now.ToString & "<br />")

            If DT2.Rows.Count > 0 Then
                out &= "<dl class=""filter_Submarkets""><dt>Expertise</dt>"
                For i As Integer = 0 To DT2.Rows.Count - 1
                    Dim codedSubMarket As String = FormatTag("submarket_", DT2.Rows(i)("KeyName").ToString.Trim).Trim
                    out &= "<dd class=""filter " & submarket & """><a class=""" & codedSubMarket
                    If submarket = codedSubMarket Then
                        out &= " selected "
                    End If
                    out &= """>" & DT2.Rows(i)("KeyName").ToString.Trim.Split("|")(1).Replace("Developer","Business &amp; Industry") & "</a> <span>(" & DT2.Rows(i)("kount").ToString.Trim & ")</span></dd>"
                Next
                out &= "</dl>"
            End If
            'Response.Write("end sql2 out" & Now.ToString & "<br />")

        End If
        'Response.Write("end sql2 " & Now.ToString & "<br />")

        Dim sql3 As String = "With WebProjects( ProjectID ) as ( Select p.ProjectID from IPM_PROJECT p join ipm_project_field_value wa on p.ProjectID = wa.ProjectID and  wa.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_WEBACTIVE') and wa.Item_Value = '1' join IPM_PROJECT_FIELD_VALUE pn on p.ProjectID = pn.ProjectID and pn.Item_ID = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJNAME') and pn.Item_Value <> '' "
        If decodedMarket <> "" Then
            sql3 &= "join IPM_PROJECT_DISCIPLINE pd on p.ProjectID = pd.ProjectID join IPM_DISCIPLINE d on pd.KeyID = d.KeyID and d.KeyUse = '1' and d.KeyName = '" & decodedMarket & "'"
        End If
        sql3 &= " where p.Available = 'Y' and p.ClientName <> '' "
        If filterState <> "" Then
            sql3 &= " and p.State_id  = '" & filterState & "' "
        End If
        sql3 &= " ) "
        'sql3 &= " Select KeyName, count(*) kount from IPM_OFFICE o join IPM_PROJECT_OFFICE po on o.KeyID = po.OfficeID and o.KeyUse = 1 join WebProjects wp on po.ProjectID = wp.ProjectID group by KeyName "
        sql3 &= " Select KeyName from IPM_OFFICE o join IPM_PROJECT_OFFICE po on o.KeyID = po.OfficeID and o.KeyUse = 1 join WebProjects wp on po.ProjectID = wp.ProjectID group by KeyName "
        Dim DT3 As New DataTable("FProjects")
        'Response.Write(sql2)
        DT3 = mmfunctions.GetDataTable(sql3)
        If DT3.Rows.Count > 0 Then
            out &= "<dl class=""filter_Services""><dt>Services</dt>"
            For i As Integer = 0 To DT3.Rows.Count - 1
                Dim codedService As String = FormatTag("service_", DT3.Rows(i)("KeyName").ToString.Trim).Trim
                out &= "<dd><a class=""" & codedService
                If service.Contains(codedService) Then
                    out &= " selected "
                End If
                'out &= """>" & DT3.Rows(i)("KeyName").ToString.Trim & "</a> <span>(" & DT3.Rows(i)("kount").ToString.Trim & ")</span></dd>"
                out &= """>" & DT3.Rows(i)("KeyName").ToString.Trim & "</a></dd>"
            Next
            out &= "</dl>"
        End If
        'Response.Write("end sql3 " & Now.ToString & "<br />")

        Dim sql4 As String = "With WebProjects( ProjectID ) as ( Select p.ProjectID from IPM_PROJECT p join ipm_project_field_value wa on p.ProjectID = wa.ProjectID and  wa.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_WEBACTIVE') and wa.Item_Value = '1' join IPM_PROJECT_FIELD_VALUE pn on p.ProjectID = pn.ProjectID and pn.Item_ID = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJNAME') and pn.Item_Value <> '' "
        If decodedMarket <> "" Then
            sql4 &= "join IPM_PROJECT_DISCIPLINE pd on p.ProjectID = pd.ProjectID join IPM_DISCIPLINE d on pd.KeyID = d.KeyID and d.KeyUse = '1' and d.KeyName = '" & decodedMarket & "'"
        End If
        sql4 &= " where p.Available = 'Y' and p.ClientName <> '' "
        If filterState <> "" Then
            sql4 &= " and p.State_id  = '" & filterState & "' "
        End If
        sql4 &= " ) "
        'sql4 &= "Select v.Item_Value, COUNT(*) kount from IPM_PROJECT_FIELD_VALUE v join WebProjects wp on v.ProjectID = wp.ProjectID and v.Item_ID = (select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_BUILT' ) and v.Item_Value <> '' Group By v.Item_Value "
        sql4 &= "Select v.Item_Value from IPM_PROJECT_FIELD_VALUE v join WebProjects wp on v.ProjectID = wp.ProjectID and v.Item_ID = (select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_BUILT' ) and v.Item_Value <> '' Group By v.Item_Value "
        Dim DT4 As New DataTable("FProjects")
        'Response.Write(sql2)
        DT4 = mmfunctions.GetDataTable(sql4)
        If DT4.Rows.Count > 0 Then
            out &= "<dl class=""filter_BuiltStatus""><dt>Built Status</dt>"
            For i As Integer = 0 To DT4.Rows.Count - 1
                Dim codedBuiltStatus As String = FormatTag("built_", DT4.Rows(i)("Item_Value").ToString.Trim).Trim
                'Response.Write("Current Code - ")
                'Response.Write(codedBuiltStatus)
                'Response.Write(build.Contains(codedBuiltStatus).ToString)
                'Response.Write("<br />--------<br />")
                out &= "<dd><a class=""" & codedBuiltStatus
                If build.Contains(codedBuiltStatus) Then
                    out &= " selected "
                End If
                'out &= """>" & DT4.Rows(i)("Item_Value").ToString.Trim & "</a> <span>(" & DT4.Rows(i)("kount").ToString.Trim & ")</span></dd>"
                out &= """>" & DT4.Rows(i)("Item_Value").ToString.Trim & "</a></dd>"
            Next
            out &= "</dl>"
        End If
        'Response.Write("end sql4 " & Now.ToString & "<br />")

        Dim sql5 As String = "With WebProjects( ProjectID ) as ( Select p.ProjectID from IPM_PROJECT p join ipm_project_field_value wa on p.ProjectID = wa.ProjectID and  wa.item_id = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_WEBACTIVE') and wa.Item_Value = '1' join IPM_PROJECT_FIELD_VALUE pn on p.ProjectID = pn.ProjectID and pn.Item_ID = (select item_id from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJNAME') and pn.Item_Value <> '' "
        If decodedMarket <> "" Then
            sql5 &= "join IPM_PROJECT_DISCIPLINE pd on p.ProjectID = pd.ProjectID join IPM_DISCIPLINE d on pd.KeyID = d.KeyID and d.KeyUse = '1' and d.KeyName = '" & decodedMarket & "'"
        End If
        sql5 &= " where p.Available = 'Y' and p.ClientName <> '' "
        If filterState <> "" Then
            sql5 &= " and p.State_id  = '" & filterState & "' "
        End If
        sql5 &= " ) "
        'sql5 &= " Select v.Item_Value, COUNT(*) kount from IPM_PROJECT_FIELD_VALUE v join WebProjects wp on v.ProjectID = wp.ProjectID and v.Item_ID = (select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUSTAINABLE' ) and v.Item_Value <> '' Group By v.Item_Value "
        sql5 &= " Select v.Item_Value from IPM_PROJECT_FIELD_VALUE v join WebProjects wp on v.ProjectID = wp.ProjectID and v.Item_ID = (select Item_ID from IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SUSTAINABLE' ) and v.Item_Value <> '' Group By v.Item_Value "
        Dim DT5 As New DataTable("FProjects")
        'Response.Write(sql2)
        DT5 = mmfunctions.GetDataTable(sql5)
        If DT5.Rows.Count > 0 Then
            out &= "<dl class=""filter_SustainableStatus""><dt>Sustainable Status</dt>"
            For i As Integer = 0 To DT5.Rows.Count - 1
                Dim codedSustainableStatus As String = FormatTag("sustainable_", DT5.Rows(i)("Item_Value").ToString.Trim).Trim
                out &= "<dd><a class=""" & codedSustainableStatus
                If sustainable.Contains(codedSustainableStatus) Then
                    out &= " selected "
                End If
                'out &= """>" & DT5.Rows(i)("Item_Value").ToString.Trim & "</a> <span>(" & DT5.Rows(i)("kount").ToString.Trim & ")</span></dd>"
                out &= """>" & DT5.Rows(i)("Item_Value").ToString.Trim & "</a></dd>"
            Next
            out &= "</dl>"
        End If
        Response.Write(out)

    End Sub

End Class

