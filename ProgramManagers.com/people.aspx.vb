Imports System.Web
Imports System.Data.SqlClient
Imports System.Data


Partial Class people
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Protected Function altAutoFormatText(ByVal input As String) As String
        input = Server.HtmlEncode(input)
        input = RegularExpressions.Regex.Replace(input, "(https?://\S+[^\s@,.""']+)", "<a href=""$1"">$1</a>")
        input = RegularExpressions.Regex.Replace(input, "\w[\w\.]*\w?@[\w\.]+\w", "<a href=""mailto:$0"">$0</a>")
        input = RegularExpressions.Regex.Replace(input, "(?<!\S)(\www\.\S+[^\s@,.""']+)", "<a href=""http://$1"">$1</a>")
        input = RegularExpressions.Regex.Replace(input, "  ", " &nbsp;")
        input = RegularExpressions.Regex.Replace(input, "-- ", "&mdash; ")
        input = RegularExpressions.Regex.Replace(input, "(?<!\S)\((@)(\S+[^\s\)]+)\)", "(<a href=""http://twitter.com/$2"">$1$2</a>)")
        input = RegularExpressions.Regex.Replace(input, "(?<=&)amp;(?=#?\w+;)", String.Empty)
        input = System.Text.RegularExpressions.Regex.Replace(input, "(.+(?<!\r))", "<p>$1</p>")

        Return input

    End Function
    Protected Function IsRelated(ProjectID As String, Projects As String) As Boolean
        Dim check() As String = Projects.Split(",")
        For i As Integer = 0 To check.Length - 1
            If ProjectID = check(i).Trim Then
                Return True
            End If
        Next

        Return False

    End Function
    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim UserID As String = ""
        If Not (Request.QueryString("id") Is Nothing) Then
            UserID = Request.QueryString("id")
        End If

        If UserID <> "" Then

            UserID_data.text = UserID 

            Dim sql As String = "select TOP 1 Active, u.UserID, Email, FirstName + ' ' + LastName as FullName, Position, Bio, ITEM_VALUE from IPM_USER u Left Join IPM_USER_FIELD_VALUE v on u.UserID = v.User_ID and v.Item_ID = (Select Item_ID From IPM_USER_FIELD_DESC where ITEM_TAG = 'IDAM_HIDEUSERIMAGE') where UserID = " & UserID & " and Active = 'Y' and Contact= '0'"
            Dim DT1 As New DataTable("FProjects")
            DT1 = mmfunctions.GetDataTable(sql)

            Dim out As String = ""
            Dim temp As String = ""

            If DT1.Rows.Count < 1

        Response.Status = ("404 Not Found")
        Response.StatusCode = ("404")

            Return

            Else



                If Request.QueryString("con")=1
                catlink_data.Text  = "<li><a href=""consultant-profiles"">Consultants</a></li>"
                backlink_data.Text = "<a href=""consultant-profiles"" class=""go_back"">BACK TO CONSULTANTS</a>"
                Else
                catlink_data.Text  = "<li><a href=""people-profiles"">Employees</a></li>"
                backlink_data.Text = "<a href=""people-profiles"" class=""go_back"">BACK TO LEADERSHIP</a>"
                End If


                '<img src="assets/images/person.jpg" alt=""/>
                '<a href="mailto:test@example.com" title="" class="contact_mail">Contact email</a>

                htmltitle.InnerHtml = formatfunctions.AutoFormatText(DT1(0)("FullName").ToString.Trim) & ": Brailsford &amp; Dunlavey"

                temp = "/dynamic/image/forever/contact/best/220x293/85/777777/Center/" & DT1.Rows(0)("UserID").ToString.Trim & ".jpg"" alt=""""/>"
                out &= "<div class=""pmg"">"
                If DT1.Rows(0)("ITEM_VALUE").ToString.Trim <> "1" Then
                    out &= "<img src=""assets/images/clear.png"" longdesc=""" & temp.Replace("&", "&amp;")
                End If
                out &= "</div>"

if Not String.IsNullOrEmpty(DT1.Rows(0)("Email").ToString()) Then
                out &= "<a href=""mailto:" & DT1.Rows(0)("Email") & """ title="""" class=""contact_mail"">Contact email</a>"
End if
                person_layout_data.Text = out

                '<h1 class="section_title">PAUL A. BRAILSFORD</h1>
                '<h2>CEO</h2>

                out = "<h1 class=""section_title"">" & DT1.Rows(0)("FullName") & "</h1>"
                out &= "<h2>" & formatfunctions.AutoFormatText(DT1.Rows(0)("Position")) & "</h2>"
                person_title_data.Text = out

                bioStandard_data.Text = formatfunctions.AutoFormatText(DT1.Rows(0)("Bio"))

                Dim sql2 As String = "select Item_Tag, ISNULL(Item_Value,'') Item_value from IPM_USER_FIELD_VALUE v join IPM_USER_FIELD_DESC d on d.Item_ID = v.Item_ID where USER_ID = " & UserID
                Dim DT2 As New DataTable("FProjects")
                DT2 = mmfunctions.GetDataTable(sql2)
                Dim trow() As DataRow = DT2.Select("Item_Tag = 'IDAM_BIO'")

                out = ""



' ### START EDUCATION

                trow = DT2.Select("Item_Tag = 'IDAM_EDUCATION'")
                If trow.Length > 0 And trow(0)("Item_Value").ToString.Trim <> "" Then
                    out &= "<h4><strong>Education</strong><span>&nbsp;</span></h4><div><dl>"
                    out &= "<dd>" & formatfunctions.AutoFormatText(trow(0)("Item_Value").ToString.Trim) & "</dd>"
                    out &= "</dl></div>"
                End If
' ### END EDUCATION

' ### START CERTIFICATIONS

                trow = DT2.Select("Item_Tag = 'IDAM_CERTIFICATIONS'")
                If trow.Length > 0 And trow(0)("Item_Value").ToString.Trim <> "" Then
                    out &= "<h4><strong>CERTIFICATIONS / associationS</strong><span>&nbsp;</span></h4><div><dl>"
                    out &= "<dd class=""cert"">" & altAutoFormatText(trow(0)("Item_Value").ToString.Trim) & "</dd>"
                    out &= "</dl></div>"
                End If

' ### END CERTIFICATIONS

' ### START PUBLICATIONS AND PRESENTATIONS

                Dim sql3 As String = "Select YEAR, Headline, Content, ID, pdf, TYPE, linktitle, url, urltitle from (Select  YEAR(Event_Date) Year, Headline, '' as Content,  e.Event_Id as  id, 0 as pdf, event_date, 0 as type, ISNULL(v2.Item_Value, '') linktitle, '' as url, '' as urltitle from IPM_EVENTS_FIELD_VALUE v join IPM_EVENTS e on v.EVENT_ID = e.Event_Id  left join IPM_EVENTS_FIELD_VALUE v2 on e.Event_Id = v2.EVENT_ID and v2.Item_ID = (select Item_ID from IPM_EVENTS_FIELD_DESC where Item_Tag = 'IDAM_EVENTTITLE') Where e.Show = '1' and e.Pull_Date >= GETDATE() and v.Item_Value like '%" & UserID & "%' union all select year(n.post_date) year, n.Headline, n.Content, n.News_Id id, n.pdf, n.Post_Date, 1 as type, PDFLinkTitle as linktitle, Isnull(u.Item_Value,'') url, Isnull(t.Item_Value,'') urltitle from IPM_NEWS n left join IPM_NEWS_FIELD_VALUE u on n.News_Id = u.NEWS_ID and u.Item_ID = ( select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWSURL' ) left join IPM_NEWS_FIELD_VALUE t on n.News_Id = t.NEWS_ID and t.Item_ID = ( select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_URLTITLE' )  join IPM_NEWS_FIELD_VALUE v on n.News_Id = v.NEWS_ID and v.Item_ID = (select item_id from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_EVENTSTAFF') and v.Item_Value like '%" & UserID & "%' where n.Type = ( select Type_Id from IPM_NEWS_TYPE where Type_Value = 'Publication' and Show = 1 ) and n.Pull_Date >= GETDATE() and n.Show = 1) t Order By Event_Date desc"
                Dim DT3 As New DataTable("FProjects")
                DT3 = mmfunctions.GetDataTable(sql3)

                If DT3.Rows.Count > 0 Then
                    out &= "<h4><strong>PUBLICATIONS &amp; PRESENTATIONs</strong><span>&nbsp;</span></h4><div><dl>"
                    For i As Integer = 0 To DT3.Rows.Count - 1
                        out &= "<dt>" & DT3.Rows(i)("Year").ToString.Trim & "</dt>"
                        out &= "<dd><span>" & formatfunctions.AutoFormatText(DT3.Rows(i)("Headline").ToString.Trim) & "</span>"
                        out &= "<span class=""description"">" & formatfunctions.AutoFormatText(DT3.Rows(i)("Content").ToString.Trim) & "</span>"
                        Dim pdflink As String = "Download"
                        If DT3.Rows(i)("linktitle").ToString.Trim <> "" Then
                            pdflink = DT3.Rows(i)("linktitle").ToString.Trim
                        End If
                        Dim url As String = ""
                        If DT3.Rows(i)("url").ToString.Trim <> "" Then
                            url = DT3.Rows(i)("url").ToString.Trim
                        End If
                        Dim urltitle As String = ""
                        If DT3.Rows(i)("urltitle").ToString.Trim <> "" Then
                            urltitle = DT3.Rows(i)("urltitle").ToString.Trim
                        End If
                        If DT3.Rows(i)("type").ToString.Trim = "0" Then 'event

                            If url <> "" Then
                                out &= "<em><a href=""" & url & """>" & urltitle & "</a></em>"
                            End If
                            Dim sql4 As String = "Select a.Asset_ID from IPM_ASSET a where a.Category_ID = " & DT3.Rows(i)("ID").ToString.Trim & " and a.Active = 1 and a.Available = 'Y'"
                            Dim DT4 As New DataTable("FProjects")
                            DT4 = mmfunctions.GetDataTable(sql4)
                            If DT4.Rows.Count > 0 Then
                                For j As Integer = 0 To DT4.Rows.Count - 1
                                    out &= "<em><a href=""/dynamic/document/fresh/asset/download/" & DT4.Rows(j)("Asset_ID").ToString.Trim & "/" & DT4.Rows(j)("Asset_ID").ToString.Trim & ".pdf"">" & pdflink & "</a></em>"
                                Next
                            End If

                        Else 'News
                            If url <> "" Then
                                out &= "<em><a href=""" & url & """>" & urltitle & "</a></em>"
                            End If
                            If DT3.Rows(i)("PDF") <> "0" Then
                                out &= "<em><a href=""/dynamic/document/fresh/newspdf/download/" & DT3.Rows(i)("ID").ToString.Trim & "/" & DT3.Rows(i)("ID").ToString.Trim & ".pdf"">" & pdflink & "</a></em>"
                            End If

                        End If

                        out &= "</dd>"
                    Next
                    out &= "</dl></div>"
                End If

' ### END PUBLICATIONS AND PRESENTATIONS

' ### START NEWS

                Dim sql6 As String = "Select n.News_ID, n.Headline, n.Content, PDFLinkTitle, PDF, Post_Date, v.Item_Value UserIds from IPM_NEWS n join IPM_NEWS_FIELD_VALUE v on n.News_Id = v.NEWS_ID and v.Item_ID = (select Item_ID from IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_EVENTSTAFF') and v.Item_Value <> '' where n.Type = ( select Type_Id from IPM_NEWS_TYPE where Type_Value = 'General News' and Show = 1 ) and n.Pull_Date >= GETDATE() and ((n.Type = 0) or (n.Type = 1))  Order By Post_Date desc"

                Dim DT6 As New DataTable("FProjects")
                DT6 = mmfunctions.GetDataTable(sql6)
                If DT6.Rows.Count > 0 Then
                    Dim outnews As String = ""
                    Dim outnewsheader As String = "<h4><strong>In the News</strong><span>&nbsp;</span></h4><div class=""peopleNews"">"
                    For i As Integer = 0 To DT6.Rows.Count - 1
                        If IsRelated(UserID, DT6.Rows(i)("UserIds").ToString.Trim) Then
                            outnews &= "<p><strong>" & formatfunctions.AutoFormatText(DT6.Rows(i)("Headline").ToString.Trim) & "</strong><span>" & formatfunctions.AutoFormatText(DT6.Rows(i)("Content").ToString.Trim) & "</span></p>"
                            If DT6.Rows(i)("PDF").ToString.Trim = "1" Then
                                Dim temp2 As String = DT6.Rows(i)("PDFLinkTitle").ToString.Trim
                                If temp2 = "" Then
                                    temp2 = "Download"
                                End If
                                outnews &= "<a href=""dynamic/document/fresh/newspdf/download/" & DT6.Rows(i)("News_ID").ToString.Trim & "/" & DT6.Rows(i)("News_ID").ToString.Trim & ".pdf""><strong>" & formatfunctions.AutoFormatText(temp2) & "</strong></a>"
                            Else
                                outnews &= "<span style=""display: block;margin: 14px 0 0 0""></span>"
                            End If
                        End If
                    Next
                    Dim outnewsfooter As String = "</div>"
                    If outnews <> "" Then
                        out &= outnewsheader & outnews & outnewsfooter
                    End If
                End If
                accordion_data.Text = out
                PersonLink.InnerText = DT1.Rows(0)("FullName").ToString.Trim

                If Request.QueryString("con")=1
                PersonLink.HRef = Regex.Replace(DT1.Rows(0)("FullName").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-consultant-" & UserID
                Else
                PersonLink.HRef = Regex.Replace(DT1.Rows(0)("FullName").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-profile-" & UserID
                End If

            End If

        End If

' ### END NEWS


    End Sub

End Class
