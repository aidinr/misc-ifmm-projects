Imports System.Web
Imports System.Data.SqlClient
Imports System.Data


Partial Class portfolio
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sql As String = "Select c.NAME, a.Asset_ID from IPM_ASSET a join IPM_Asset_CATEGORY c on a.Category_ID = c.CATEGORY_ID and c.ACTIVE = 1 and c.AVAILABLE = 'Y' and c.NAME in ('Campus Edge', 'B&D Venues', 'Government/Municipal', 'PRE-K - 12', 'Higher Education', 'Private') where a.Active = 1 and a.Available = 'Y'"
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)

        Dim temp As String
        Dim drow() As DataRow = DT1.Select("Name='Higher Education'")
        If drow.Length > 0 Then
            temp = "dynamic/document/fresh/asset/download/" & drow(0)("Asset_ID").ToString.Trim & "/HigherEducation.pdf"
            HESoQLink.href = temp.Replace("&", "&amp;")
        End If
        drow = DT1.Select("Name='Private'")
        If drow.Length > 0 Then
            temp = "dynamic/document/fresh/asset/download/" & drow(0)("Asset_ID").ToString.Trim & "/Private.pdf"
            PriSoQLink.href = temp.Replace("&", "&amp;")
        End If
        drow = DT1.Select("Name='PRE-K - 12'")
        If drow.Length > 0 Then
            temp = "dynamic/document/fresh/asset/download/" & drow(0)("Asset_ID").ToString.Trim & "/PreK12.pdf"
            PreKSoQLink.href = temp.Replace("&", "&amp;")
        End If
        drow = DT1.Select("Name='Government/Municipal'")
        If drow.Length > 0 Then
            temp = "dynamic/document/fresh/asset/download/" & drow(0)("Asset_ID").ToString.Trim & "/Municipal.pdf"
            MunSoQLink.href = temp.Replace("&", "&amp;")
        End If
        drow = DT1.Select("Name='B&D Venues'")
        If drow.Length > 0 Then
            temp = "dynamic/document/fresh/asset/download/" & drow(0)("Asset_ID").ToString.Trim & "/Venues.pdf"
            VenuesSoQLink.href = temp.Replace("&", "&amp;")
        End If

        Dim sql2 As String = "select a.Asset_ID, a.Name, a.Description from IPM_ASSET a  where Name in ('portfolioHigherEducation.jpg', 'portfolioGovernmentMunicipal.jpg', 'portfolioPREK12.jpg','portfolioPrivate.jpg','portfolioBDVenues.jpg') and Available = 'Y' "
        Dim DT2 As New DataTable("FProjects")
        DT2 = mmfunctions.GetDataTable(sql2)

        Dim out As String = ""
        Dim src As String = ""
        Dim linkstart As String = ""
        Dim link As String = ""
        Dim linkend As String = ""
        Dim sql3 As String = ""
        drow = DT2.Select("Name='portfolioHigherEducation.jpg'")
        If drow.Length > 0 Then
            sql3 = "select d.Item_Id, d.Item_tag, ISNULL(v.Item_Value, '') value, isnull(v2.item_value, '') as Project_Name from IPM_ASSET_FIELD_DESC d left join IPM_ASSET_FIELD_VALUE v on d.Item_ID = v.Item_ID and v.ASSET_ID = " & drow(0)("Asset_ID").ToString.Trim & " left outer join ipm_project_field_Value v2 on v2.item_id = 2400039 and convert(varchar(50), v2.projectid) = ltrim(rtrim(v.item_value)) where d.Item_Tag in ('IDAM_WATERMARK_VALUE','IDAM_ASSETARCHITECT','IDAM_ASSETPROJNAME','IDAM_ASSETCLIENT','IDAM_CASESTUDY')"
            Dim DT3 As New DataTable("FProjects")
            DT3 = mmfunctions.GetDataTable(sql3)
            Dim drow2() As DataRow = DT3.Select("Item_tag='IDAM_CASESTUDY'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    linkstart = "<a href=""" & Regex.Replace(drow2(0)("Project_Name").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-casestudy-" & drow2(0)("value").ToString.Trim & """><em>"
                    link = "<span class=""carMore"">Learn More</span>"
                    linkend = "</em></a>"
                Else
                    linkstart = "<em>"
                    link = ""
                    linkend = "</em>"
                End If
            End If
            out = linkstart
            drow2 = DT3.Select("Item_tag='IDAM_ASSETCLIENT'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carClientName"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            drow2 = DT3.Select("Item_tag='IDAM_ASSETPROJNAME'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carProjectName"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            drow2 = DT3.Select("Item_tag='IDAM_ASSETARCHITECT'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carArch"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            out &= link
            drow2 = DT3.Select("Item_tag='IDAM_WATERMARK_VALUE'")
            If drow2(0)("value").ToString.Trim <> "" Then
                out &= "<span class=""carCredits"">Photograph &copy; Copyright " & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
            End If
            out &= linkend
            HEImageText.Text = out
            HEImage.Src = "/dynamic/image/forever/asset/best/940x224/58/ffffff/North/" & drow(0)("Asset_ID").ToString.Trim & ".jpg"

        End If

        drow = DT2.Select("Name='portfolioGovernmentMunicipal.jpg'")
        If drow.Length > 0 Then
            sql3 = "select d.Item_Id, d.Item_tag, ISNULL(v.Item_Value, '') value, isnull(v2.item_value, '') as Project_Name from IPM_ASSET_FIELD_DESC d left join IPM_ASSET_FIELD_VALUE v on d.Item_ID = v.Item_ID and v.ASSET_ID = " & drow(0)("Asset_ID").ToString.Trim & " left outer join ipm_project_field_Value v2 on v2.item_id = 2400039 and convert(varchar(50), v2.projectid) = ltrim(rtrim(v.item_value)) where d.Item_Tag in ('IDAM_WATERMARK_VALUE','IDAM_ASSETARCHITECT','IDAM_ASSETPROJNAME','IDAM_ASSETCLIENT','IDAM_CASESTUDY')"
            Dim DT3 As New DataTable("FProjects")
            DT3 = mmfunctions.GetDataTable(sql3)
            Dim drow2() As DataRow = DT3.Select("Item_tag='IDAM_CASESTUDY'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    linkstart = "<a href=""" & Regex.Replace(drow2(0)("Project_Name").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-casestudy-" & drow2(0)("value").ToString.Trim & """><em>"
                    link = "<span class=""carMore"">Learn More</span>"
                    linkend = "</em></a>"
                Else
                    linkstart = "<em>"
                    link = ""
                    linkend = "</em>"
                End If
            End If
            out = linkstart
            drow2 = DT3.Select("Item_tag='IDAM_ASSETCLIENT'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carClientName"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            drow2 = DT3.Select("Item_tag='IDAM_ASSETPROJNAME'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carProjectName"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            drow2 = DT3.Select("Item_tag='IDAM_ASSETARCHITECT'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carArch"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            out &= link
            drow2 = DT3.Select("Item_tag='IDAM_WATERMARK_VALUE'")
            If drow2(0)("value").ToString.Trim <> "" Then
                out &= "<span class=""carCredits"">Photograph &copy; Copyright " & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
            End If
            out &= linkend
            GMImageText.Text = out
            GMImage.Src = "/dynamic/image/forever/asset/best/940x224/58/ffffff/North/" & drow(0)("Asset_ID").ToString.Trim & ".jpg"

        End If

        drow = DT2.Select("Name='portfolioPREK12.jpg'")
        If drow.Length > 0 Then
            sql3 = "select d.Item_Id, d.Item_tag, ISNULL(v.Item_Value, '') value, isnull(v2.item_value, '') as Project_Name from IPM_ASSET_FIELD_DESC d left join IPM_ASSET_FIELD_VALUE v on d.Item_ID = v.Item_ID and v.ASSET_ID = " & drow(0)("Asset_ID").ToString.Trim & " left outer join ipm_project_field_Value v2 on v2.item_id = 2400039 and convert(varchar(50), v2.projectid) = ltrim(rtrim(v.item_value)) where d.Item_Tag in ('IDAM_WATERMARK_VALUE','IDAM_ASSETARCHITECT','IDAM_ASSETPROJNAME','IDAM_ASSETCLIENT','IDAM_CASESTUDY')"
            Dim DT3 As New DataTable("FProjects")
            DT3 = mmfunctions.GetDataTable(sql3)
            Dim drow2() As DataRow = DT3.Select("Item_tag='IDAM_CASESTUDY'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    linkstart = "<a href=""" & Regex.Replace(drow2(0)("Project_Name").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-casestudy-" & drow2(0)("value").ToString.Trim & """><em>"
                    link = "<span class=""carMore"">Learn More</span>"
                    linkend = "</em></a>"
                Else
                    linkstart = "<em>"
                    link = ""
                    linkend = "</em>"
                End If
            End If
            out = linkstart
            drow2 = DT3.Select("Item_tag='IDAM_ASSETCLIENT'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carClientName"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            drow2 = DT3.Select("Item_tag='IDAM_ASSETPROJNAME'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carProjectName"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            drow2 = DT3.Select("Item_tag='IDAM_ASSETARCHITECT'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carArch"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            out &= link
            drow2 = DT3.Select("Item_tag='IDAM_WATERMARK_VALUE'")
            If drow2(0)("value").ToString.Trim <> "" Then
                out &= "<span class=""carCredits"">Photograph &copy; Copyright " & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
            End If
            out &= linkend
            PREKImageText.Text = out
            PREKImage.Src = "/dynamic/image/forever/asset/best/940x224/58/ffffff/North/" & drow(0)("Asset_ID").ToString.Trim & ".jpg"

        End If

        drow = DT2.Select("Name='portfolioPrivate.jpg'")
        If drow.Length > 0 Then
            sql3 = "select d.Item_Id, d.Item_tag, ISNULL(v.Item_Value, '') value, isnull(v2.item_value, '') as Project_Name from IPM_ASSET_FIELD_DESC d left join IPM_ASSET_FIELD_VALUE v on d.Item_ID = v.Item_ID and v.ASSET_ID = " & drow(0)("Asset_ID").ToString.Trim & " left outer join ipm_project_field_Value v2 on v2.item_id = 2400039 and convert(varchar(50), v2.projectid) = ltrim(rtrim(v.item_value)) where d.Item_Tag in ('IDAM_WATERMARK_VALUE','IDAM_ASSETARCHITECT','IDAM_ASSETPROJNAME','IDAM_ASSETCLIENT','IDAM_CASESTUDY')"
            Dim DT3 As New DataTable("FProjects")
            DT3 = mmfunctions.GetDataTable(sql3)
            Dim drow2() As DataRow = DT3.Select("Item_tag='IDAM_CASESTUDY'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    linkstart = "<a href=""" & Regex.Replace(drow2(0)("Project_Name").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-casestudy-" & drow2(0)("value").ToString.Trim & """ ><em >"
                    link = "<span class=""carMore"">Learn More</span>"
                    linkend = "</em></a>"
                Else
                    linkstart = "<em>"
                    link = ""
                    linkend = "</em>"
                End If
            End If
            out = linkstart
            drow2 = DT3.Select("Item_tag='IDAM_ASSETCLIENT'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carClientName"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            drow2 = DT3.Select("Item_tag='IDAM_ASSETPROJNAME'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carProjectName"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            drow2 = DT3.Select("Item_tag='IDAM_ASSETARCHITECT'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carArch"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            out &= link
            drow2 = DT3.Select("Item_tag='IDAM_WATERMARK_VALUE'")
            If drow2(0)("value").ToString.Trim <> "" Then
                out &= "<span class=""carCredits"">Photograph &copy; Copyright " & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
            End If
            out &= linkend
            PRIAVATEIamgeText.Text = out
            PRIVATEImage.Src = "/dynamic/image/forever/asset/best/940x224/58/ffffff/North/" & drow(0)("Asset_ID").ToString.Trim & ".jpg"

        End If

        drow = DT2.Select("Name='portfolioBDVenues.jpg'")
        If drow.Length > 0 Then
            sql3 = "select d.Item_Id, d.Item_tag, ISNULL(v.Item_Value, '') value, isnull(v2.item_value, '') as Project_Name from IPM_ASSET_FIELD_DESC d left join IPM_ASSET_FIELD_VALUE v on d.Item_ID = v.Item_ID and v.ASSET_ID = " & drow(0)("Asset_ID").ToString.Trim & " left outer join ipm_project_field_Value v2 on v2.item_id = 2400039 and convert(varchar(50), v2.projectid) = ltrim(rtrim(v.item_value)) where d.Item_Tag in ('IDAM_WATERMARK_VALUE','IDAM_ASSETARCHITECT','IDAM_ASSETPROJNAME','IDAM_ASSETCLIENT','IDAM_CASESTUDY')"
            Dim DT3 As New DataTable("FProjects")
            DT3 = mmfunctions.GetDataTable(sql3)
            Dim drow2() As DataRow = DT3.Select("Item_tag='IDAM_CASESTUDY'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    linkstart = "<a href=""" & Regex.Replace(drow2(0)("Project_Name").ToString.Split(","c)(0).ToLower().Replace(" ", "_").Trim("_"), "[^A-z_]", "", RegexOptions.Multiline) & "-casestudy-" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & """><em>"
                    link = "<span class=""carMore"">Learn More</span>"
                    linkend = "</em></a>"
                Else
                    linkstart = "<em>"
                    link = ""
                    linkend = "</em>"
                End If
            End If
            out = linkstart
            drow2 = DT3.Select("Item_tag='IDAM_ASSETCLIENT'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carClientName"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            drow2 = DT3.Select("Item_tag='IDAM_ASSETPROJNAME'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carProjectName"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            drow2 = DT3.Select("Item_tag='IDAM_ASSETARCHITECT'")
            If drow2.Length > 0 Then
                If drow2(0)("value").ToString.Trim <> "" Then
                    out &= "<span class=""carArch"">" & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
                End If
            End If
            out &= link
            drow2 = DT3.Select("Item_tag='IDAM_WATERMARK_VALUE'")
            If drow2(0)("value").ToString.Trim <> "" Then
                out &= "<span class=""carCredits"">Photograph &copy; Copyright " & formatfunctionssimple.AutoFormatText(drow2(0)("value").ToString.Trim) & "</span>"
            End If
            out &= linkend
            BDVenuesImageText.Text = out
            BDVenuseImage.Src = "/dynamic/image/forever/asset/best/940x224/58/ffffff/North/" & drow(0)("Asset_ID").ToString.Trim & ".jpg"

        End If

    End Sub

End Class
