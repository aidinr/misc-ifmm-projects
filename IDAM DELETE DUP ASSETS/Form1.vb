﻿Imports System.IO
Imports System.Security.Cryptography
Imports System
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Collections.Generic
Imports System.Text
Imports System.Net.Mail
Imports System.Net
Imports System.Xml
Imports System.Xml.Serialization


Public Class Form1
    Public Shared ConnectionStringIDAM As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
    Public Shared ConnectionStringIDAMIDAMINSTANCE As String = ConfigurationSettings.AppSettings("IDAM.IDAMINSTANCE")
    Public Shared ConnectionStringIDAMENGINE As String = ConfigurationSettings.AppSettings("IDAM.IDAMENGINEID")
    Public Shared bState As Boolean = True
    Public Shared AppIDUnique As String = System.Guid.NewGuid.ToString
    Private Delegate Sub ExecuteChecksum()
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If CheckBox1.Checked Then
            Dim del As ExecuteChecksum
            del = New ExecuteChecksum(AddressOf ExecuteCS)
            del.Invoke()
        End If
    End Sub


    Private Sub ExecuteCS()

        'get all assets
        Dim allassets As DataTable = GetDataTable("select location_id,asset_id,extension from ipm_asset a, IPM_FILETYPE_LOOKUP b where a.Media_Type  = b.Media_Type and a.asset_id in (select asset_id from ipm_asset where asset_ref_id is not null and asset_ref_id <> asset_id) and available = 'Y'", ConnectionStringIDAM)
        TextBox1.Text = "Total assets deleted: 0 of " & allassets.Rows.Count.ToString
        ProgressBar1.Maximum = allassets.Rows.Count
        Dim sDestRoot As String = ConfigurationSettings.AppSettings("IDAM.IDAMREPOROOT")
        For Each row As DataRow In allassets.Rows
            'find original asset
            System.Threading.Thread.CurrentThread.Sleep(10)
            System.Windows.Forms.Application.DoEvents()
            Do While Not bState
                'paused
                System.Threading.Thread.CurrentThread.Sleep(1000)
                System.Windows.Forms.Application.DoEvents()
            Loop
            Dim sSourceFile, sSourceFileThumb As String
            sSourceFile = sDestRoot + row("location_id").ToString.Trim + "\resource\" & row("asset_id") & "." & row("extension")
            ' delete
            Dim ExistingFile As FileInfo
            ExistingFile = New FileInfo(sSourceFile)
            Try
                ExistingFile.Delete()
                ProgressBar1.Value += 1

            Catch ex As Exception
                TextBox1.Text += "Failed to Delete asset: " & row("asset_id") & " | "
            End Try


        Next
        TextBox1.Text += "Total assets Deleted: " & ProgressBar1.Value & " of " & allassets.Rows.Count.ToString
        MsgBox("Completed")
    End Sub


    Public Shared Function SendError(ByVal sError, Optional ByVal object_type = "", Optional ByVal object_id = "")
        
        Dim sql As String
        sError = sError.ToString.Replace("'", "''")
        
        Try
            sql = "insert into ipm_error (date,error_level,machine_id,instance_id,app_id,output,object_type,object_id) values (getdate(),'High','Localhost','" & ConnectionStringIDAMIDAMINSTANCE & "','" & AppIDUnique & "','" & sError & "','" & object_type & "','" & object_id & "')"
            ExecuteTransaction(sql)

        Catch ex As Exception

        End Try



    End Function



    Public Shared Sub ExecuteTransaction(ByVal sqlstring As String, Optional ByVal connectionstring As String = "")
        If connectionstring = "" Then
            connectionstring = ConnectionStringIDAM
        End If

        Dim connection As New OleDbConnection(connectionstring)
        Dim command As New OleDbCommand
        Dim transaction As OleDbTransaction

        ' Set the Connection to the new OleDbConnection.
        command.Connection = connection

        ' Open the connection and execute the transaction.
        Try
            connection.Open()

            ' Start a local transaction with ReadCommitted isolation level.
            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            ' Assign transaction object for a pending local transaction.
            command.Connection = connection
            command.Transaction = transaction

            ' Execute the commands.
            command.CommandText = sqlstring
            command.ExecuteNonQuery()

            ' Commit the transaction.
            transaction.Commit()
            Console.WriteLine("Executed.")

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Try to rollback the transaction
            Try
                transaction.Rollback()

            Catch
                ' Do nothing here; transaction is not active.
            End Try
        End Try
        ' The connection is automatically closed when the
        ' code exits the Using block.

    End Sub

    Public Shared Function GetDataTable(ByVal query As String, ByVal ConnectionString As String) As DataTable
        'Dim connection1 As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & System.Web.HttpContext.Current.Server.MapPath(dbFile))
        Dim connection1 As OleDbConnection = New OleDbConnection(ConnectionString)
        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)
        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()

        End Try
        Return table1
    End Function
    Function isullcheck(ByVal v)
        If v Is System.DBNull.Value Then
            isullcheck = ""
        Else
            isullcheck = v
        End If
    End Function



    Private Function addUDFtoasset(ByVal asset_id As String, ByVal UDFid As String, ByVal UDFValue As String) As String
        'check to see if exists
        Try
            'add
            'assume we have a tagid now
            If GetDataTable("select * from ipm_asset_field_value where asset_id = " & asset_id & " and item_id = " & UDFid, ConnectionStringIDAM).Rows.Count = 0 Then
                ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & asset_id & "," & UDFid & ",'" & UDFValue.Replace("'", "''") & "')")
            End If

        Catch ex As Exception

        End Try
        Return UDFid
    End Function

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Button2.Text = "Pause" Then
            bState = False
            Button2.Text = "Resume"
        Else
            bState = True
            Button2.Text = "Pause"
        End If


    End Sub
End Class
