﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Serialization
Imports log4net
Imports System.Net.Mail
Imports System.Data.OleDb



Public Class Form1
    Private Shared ReadOnly Log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '' ''For Each row In GetDataTable("select a.Item_Value modelid, b.Item_Value modelname from IPM_ASSET_FIELD_value a, IPM_ASSET_FIELD_VALUE b where a.Item_ID = 21550857 and b.item_id = 229806 and a.ASSET_ID = b.ASSET_ID and a.ASSET_ID in (select ASSET_ID from IPM_ASSET where ProjectID in ((SELECT     ProjectID  FROM          IPM_PROJECT WHERE      (Category_ID = 452624) AND (ProjectID = 452692 OR ProjectID = 452691 OR ProjectID = 452690 OR ProjectID = 452689 OR ProjectID = 452688 OR ProjectID = 452687 OR ProjectID = 452686 OR ProjectID = 452685 OR ProjectID = 452684 OR ProjectID = 452683 OR ProjectID = 452685))))group by a.Item_Value , b.item_value", My.Settings.ConnString).Rows
        '' ''    checkcreatemodel(row("modelid"), row("modelname"), 453505)
        '' ''Next

        '' ''Exit Sub

        ExecuteUpdate()
        Me.Close()
        Me.Dispose()
    End Sub

    Private Function InsertSalesplaceData(dt As DataTable) As Boolean
        Dim connection As New SqlConnection(My.Settings.ConnString)
        Dim querystring1 As String
        Dim MyCommand1 As New SqlCommand("", connection)
        Dim i As Integer
        For i = 0 To dt.Rows.Count - 1
            MyCommand1 = New SqlCommand("", connection)


            MyCommand1.Parameters.Add("@SalesPlaceId", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@SalesPlaceId").Value = dt.Rows(i)(0).ToString
            MyCommand1.Parameters.Add("@SquareFoot", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@SquareFoot").Value = dt.Rows(i)(1).ToString
            MyCommand1.Parameters.Add("@AccountNumber", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@AccountNumber").Value = dt.Rows(i)(2).ToString
            MyCommand1.Parameters.Add("@AccountName", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@AccountName").Value = dt.Rows(i)(3).ToString
            MyCommand1.Parameters.Add("@Architect", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@Architect").Value = dt.Rows(i)(4).ToString
            MyCommand1.Parameters.Add("@City", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@City").Value = dt.Rows(i)(5).ToString
            MyCommand1.Parameters.Add("@ClarkProjectType", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@ClarkProjectType").Value = dt.Rows(i)(6).ToString
            MyCommand1.Parameters.Add("@ClarkRegion", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@ClarkRegion").Value = dt.Rows(i)(7).ToString
            MyCommand1.Parameters.Add("@CompanyName", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@CompanyName").Value = dt.Rows(i)(8).ToString
            MyCommand1.Parameters.Add("@ContractingType", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@ContractingType").Value = dt.Rows(i)(9).ToString
            MyCommand1.Parameters.Add("@County", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@County").Value = dt.Rows(i)(10).ToString
            MyCommand1.Parameters.Add("@DatasheetDescription", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@DatasheetDescription").Value = dt.Rows(i)(11).ToString
            MyCommand1.Parameters.Add("@DeveloperCompany", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@DeveloperCompany").Value = dt.Rows(i)(12).ToString
            MyCommand1.Parameters.Add("@ElecEngCompany", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@ElecEngCompany").Value = dt.Rows(i)(13).ToString
            MyCommand1.Parameters.Add("@LEEDCertification", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@LEEDCertification").Value = dt.Rows(i)(14).ToString
            MyCommand1.Parameters.Add("@MechEngCompany", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@MechEngCompany").Value = dt.Rows(i)(15).ToString
            MyCommand1.Parameters.Add("@OtherCompany", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@OtherCompany").Value = dt.Rows(i)(16).ToString
            MyCommand1.Parameters.Add("@OwnerCompany", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@OwnerCompany").Value = dt.Rows(i)(17).ToString
            MyCommand1.Parameters.Add("@OwnerProjectNumber", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@OwnerProjectNumber").Value = dt.Rows(i)(18).ToString
            MyCommand1.Parameters.Add("@OwnerType", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@OwnerType").Value = dt.Rows(i)(19).ToString
            MyCommand1.Parameters.Add("@ProjectAddress", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@ProjectAddress").Value = dt.Rows(i)(20).ToString
            MyCommand1.Parameters.Add("@ProjectAlternateName", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@ProjectAlternateName").Value = dt.Rows(i)(21).ToString
            MyCommand1.Parameters.Add("@ProjectVentureName", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@ProjectVentureName").Value = dt.Rows(i)(22).ToString
            MyCommand1.Parameters.Add("@ResumeDescription", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@ResumeDescription").Value = dt.Rows(i)(23).ToString
            MyCommand1.Parameters.Add("@ServiceType", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@ServiceType").Value = dt.Rows(i)(24).ToString
            MyCommand1.Parameters.Add("@State", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@State").Value = dt.Rows(i)(25).ToString
            MyCommand1.Parameters.Add("@StoriesAboveGrade", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@StoriesAboveGrade").Value = dt.Rows(i)(26).ToString
            MyCommand1.Parameters.Add("@StoriesBelowGrade", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@StoriesBelowGrade").Value = dt.Rows(i)(27).ToString
            MyCommand1.Parameters.Add("@StructEngineer", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@StructEngineer").Value = dt.Rows(i)(28).ToString
            MyCommand1.Parameters.Add("@StructureType", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@StructureType").Value = dt.Rows(i)(29).ToString
            MyCommand1.Parameters.Add("@JointVenture", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@JointVenture").Value = dt.Rows(i)(30).ToString
            MyCommand1.Parameters.Add("@ZipCode", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@ZipCode").Value = dt.Rows(i)(31).ToString
            MyCommand1.Parameters.Add("@Column32", SqlDbType.NVarChar, 4000)
            MyCommand1.Parameters("@Column32").Value = dt.Rows(i)(32).ToString


            Try
                MyCommand1.Connection.Open()
                querystring1 = "insert into EXPORT_MKT_DATA ([SalesPlaceId], [Square Foot],[AccountNumber],[AccountName],[Architect],[City],[Clark Project Type],[Clark Region],[Company Name],[Contracting Type],[County],[Datasheet Description],[Developer Company],[Elec Eng Company],[LEED Certification],[Mech Eng Company],[Other Company],[Owner Company],[Owner Project Number],[Owner Type],[Project Address],[Project Alternate Name],[Project Venture Name],[Resume Description],[Service Type],[State],[Stories Above Grade],[Stories Below Grade],[Struct Engineer],[Structure Type],[Joint Venture],[ZipCode],[Column 32]) values (@SalesPlaceId, @SquareFoot, @AccountNumber, @AccountName, @Architect, @City, @ClarkProjectType, @ClarkRegion, @CompanyName, @ContractingType, @County, @DatasheetDescription, @DeveloperCompany,  @ElecEngCompany, @LEEDCertification, @MechEngCompany, @OtherCompany, @OwnerCompany, @OwnerProjectNumber, @OwnerType, @ProjectAddress, @ProjectAlternateName, @ProjectVentureName, @ResumeDescription, @ServiceType, @State, @StoriesAboveGrade, @StoriesBelowGrade, @StructEngineer, @StructureType, @JointVenture, @ZipCode, @Column32 )"
                MyCommand1.CommandText = querystring1
                'Console.WriteLine(MyCommand1.SelectCommand.CommandText)
                MyCommand1.ExecuteNonQuery()
                MyCommand1.Connection.Close()
            Catch ex As Exception
                Log.Error("Error Init:" & ex.Message)
                '                SendError(ex.Message)
            End Try

        Next



        Return True

    End Function


    Private Function InsertJDEData(dt As DataTable) As Boolean
        Dim connection As New SqlConnection(My.Settings.ConnString)
        Dim querystring1 As String
        Dim MyCommand1 As New SqlCommand("", connection)
        Dim i As Integer
        For i = 0 To dt.Rows.Count - 1
            MyCommand1 = New SqlCommand("", connection)


            MyCommand1.Parameters.Add("@ProjectNumber", SqlDbType.VarChar, 2000)
            MyCommand1.Parameters("@ProjectNumber").Value = dt.Rows(i)(0).ToString
            MyCommand1.Parameters.Add("@ProjectName", SqlDbType.VarChar, 2000)
            MyCommand1.Parameters("@ProjectName").Value = dt.Rows(i)(1).ToString
            MyCommand1.Parameters.Add("@OriginalContract", SqlDbType.VarChar, 2000)
            MyCommand1.Parameters("@OriginalContract").Value = dt.Rows(i)(2).ToString
            MyCommand1.Parameters.Add("@OwnerChangeOrderQuantity", SqlDbType.VarChar, 2000)
            MyCommand1.Parameters("@OwnerChangeOrderQuantity").Value = dt.Rows(i)(3).ToString
            MyCommand1.Parameters.Add("@OwnerChangeOrderAmount", SqlDbType.VarChar, 2000)
            MyCommand1.Parameters("@OwnerChangeOrderAmount").Value = dt.Rows(i)(4).ToString
            MyCommand1.Parameters.Add("@QBCS05", SqlDbType.VarChar, 2000)
            MyCommand1.Parameters("@QBCS05").Value = dt.Rows(i)(5).ToString
            MyCommand1.Parameters.Add("@FinalContractValue", SqlDbType.VarChar, 2000)
            MyCommand1.Parameters("@FinalContractValue").Value = dt.Rows(i)(6).ToString
            MyCommand1.Parameters.Add("@ClarkLaborValue", SqlDbType.VarChar, 2000)
            MyCommand1.Parameters("@ClarkLaborValue").Value = dt.Rows(i)(7).ToString
            MyCommand1.Parameters.Add("@NoticeToProceed", SqlDbType.VarChar, 2000)
            MyCommand1.Parameters("@NoticeToProceed").Value = dt.Rows(i)(8).ToString
            MyCommand1.Parameters.Add("@ActualStartDate", SqlDbType.VarChar, 2000)
            MyCommand1.Parameters("@ActualStartDate").Value = dt.Rows(i)(9).ToString
            MyCommand1.Parameters.Add("@ScheduledSubstantialCompletionDate", SqlDbType.VarChar, 2000)
            MyCommand1.Parameters("@ScheduledSubstantialCompletionDate").Value = dt.Rows(i)(10).ToString
            MyCommand1.Parameters.Add("@ActualCompletionDate", SqlDbType.VarChar, 2000)
            MyCommand1.Parameters("@ActualCompletionDate").Value = dt.Rows(i)(11).ToString


            Try
                MyCommand1.Connection.Open()
                querystring1 = "insert into [Project_Summary_Imports] ([Project Number], [Project Name],[Original Contract Value],[Owner Change Order Quantity],[Owner Change Order Amount],[QBCS05],[Final Contract Value],[Clark Labor Value],[Notice to Proceed],[Actual Start Date],[Scheduled Substantial Completion Date],[Actual Completion Date]) values (@ProjectNumber, @ProjectName,  @OriginalContract,  @OwnerChangeOrderQuantity, @OwnerChangeOrderAmount, @QBCS05, @FinalContractValue, @ClarkLaborValue,  @NoticeToProceed, @ActualStartDate, @ScheduledSubstantialCompletionDate, @ActualCompletionDate)"


                MyCommand1.CommandText = querystring1
                'Console.WriteLine(MyCommand1.SelectCommand.CommandText)
                MyCommand1.ExecuteNonQuery()
                MyCommand1.Connection.Close()
            Catch ex As Exception
                Log.Error("Error Init:" & ex.Message)
                '                SendError(ex.Message)
            End Try

        Next



        Return True

    End Function


    Sub ExecuteUpdate()
        log4net.Config.XmlConfigurator.Configure()
        Dim ConfigXML As New XmlDocument
        Dim emailString As String = ""
        Dim folder = My.Settings.SalesplaceFolder
        Dim CnStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & folder & ";Extended Properties=""text;HDR=Yes;FMT=Delimited"";"
        Dim dt As New DataTable
        '        Using Adp As New OleDbDataAdapter("select * from [" + My.Settings.SalesplaceFilename + "]", CnStr)
        ' Adp.Fill(dt)
        ' End Using

        Dim flgInsertSales As Boolean
        'flgInsertSales = InsertSalesplaceData(dt)

        folder = My.Settings.JDEFolder
        CnStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & folder & ";Extended Properties=""text;HDR=Yes;FMT=Delimited"";"
        dt = New DataTable
        Using Adp As New OleDbDataAdapter("select * from [" + My.Settings.JDEFilename + "]", CnStr)
            Adp.Fill(dt)
        End Using

        Dim flgInsertJDE As Boolean

        flgInsertJDE = InsertJDEData(dt)
        emailString = emailString & vbCrLf & vbCrLf & "Script execution has ended. Daily update successful."
        Log.Debug("End Execution")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click


        ExecuteUpdate()

    End Sub

    Private Sub PrintRows(ByVal ds As DataSet, ByVal label As String) 'Debug sub
        Console.WriteLine(ControlChars.Cr + label)
        Dim t As DataTable
        For Each t In ds.Tables
            Console.WriteLine("TableName: " + t.TableName)
            Dim c As DataColumn
            For Each c In t.Columns
                Console.Write(ControlChars.Tab + " " + c.ColumnName)
            Next c
            Console.WriteLine()
            Dim r As DataRow
            For Each r In t.Rows

                For Each c In t.Columns
                    Console.Write(ControlChars.Tab + " " + r(c).ToString())
                Next c
                Console.WriteLine()
            Next r
        Next t
    End Sub

    Private Function GetDataTable(ByVal ds As DataSet, ByVal TableName As String) As DataTable
        Return ds.Tables(TableName)
    End Function




End Class
