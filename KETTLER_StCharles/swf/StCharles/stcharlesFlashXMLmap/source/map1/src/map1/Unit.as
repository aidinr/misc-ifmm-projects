package map1 {
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import caurina.transitions.Tweener;
	import flash.net.*;
	
	public class Unit extends Sprite {
		private static const TIME:Number = 0.8;
		private var node:XML;
		
		public function Unit(main:Map1, node:XML) {
			this.node = node;
			var points:Array;
			
			if (node.rectangle != undefined) {
				
				points = node.rectangle.split(",");
				var w:Number = Math.abs(points[2] - points[0]);
				var h:Number = Math.abs(points[3] - points[1]);
				
				graphics.beginFill(uint(node.hover_color), Number(node.hover_opacity) / 100);
				graphics.drawRect(points[0], points[1], w, h);
				graphics.endFill();
				
			} else if (node.circle != undefined) {
				points = node.circle.split(",");
				
				graphics.beginFill(uint(node.hover_color), Number(node.hover_opacity) / 100);
				graphics.drawCircle(points[0], points[1], points[2]);
				graphics.endFill();
			}
			
			alpha = 0;
			
			addEventListener(MouseEvent.MOUSE_OVER , mOver);
			addEventListener(MouseEvent.MOUSE_OUT , mOut);
			addEventListener(MouseEvent.MOUSE_DOWN , mDown);
			
			buttonMode = true;
		}
		private function mOver(e:MouseEvent):void {
			Tweener.addTween(this, { alpha:1 , time:TIME} );
		}
		private function mOut(e:MouseEvent):void {
			Tweener.addTween(this, { alpha:0 , time:TIME} );
		}
		private function mDown(e:MouseEvent):void {
			var target:String = node.link.@target;
			if (!target) target = "_self";
			
			
			navigateToURL(new URLRequest(node.link),target);
		}
	}

}