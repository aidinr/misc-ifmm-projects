﻿package map1 {
	
	import flash.display.*;
	import com.boontaran.DataLoader;
	import com.boontaran.SWFLoader;

	public class Map1 extends MovieClip {
		var paramObj:Object = loaderInfo.parameters;
		var xmlfile:String = paramObj.xmlfile;

		private const xmlFile:String = xmlfile;

		private static var xml:XML;
		
		private var map:Loader;
		private var units:Array;
		
		public function Map1() {
			DataLoader.load(xmlFile , xmlLoaded , xmlNotFound);
			
			stage.align     = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
		}
		private function xmlLoaded(data:String):void {
			xml = XML(data);
			
			//create units
			createUnits();
			
			//load bg
			SWFLoader.load(xml.settings.screen.map , mapLoaded,mapNotFound);
			
			
		}
		private function xmlNotFound():void {
			trace(xmlFile,' not found');
		}
		private function mapNotFound():void {
			trace(xml.settings.screen.map,' not found');
		}
		
		private function mapLoaded(ldr:Loader):void {
			map = ldr;
			addChild(map);
			
			//add units
			for each (var unit:Unit in units) {
				addChild(unit);
			}
		}
		private function createUnits():void {
			units = new Array();
			
			var unit:Unit;
			var numUnits:int = xml.unit.length();
			var i:int;
			
			for (i = 0 ; i < numUnits ; i++) {
				unit = new Unit(this , xml.unit[i]);
				units.push(unit);
			}
		}
	}
}