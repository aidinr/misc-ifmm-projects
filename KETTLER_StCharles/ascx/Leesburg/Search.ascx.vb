﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient

Partial Class ascx_Leesburg_Search
    Inherits System.Web.UI.UserControl
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Request.Form.Keys.Count = 0 Then


            Dim ds As DataSet
            ds = CType(Application("DS"), DataSet)
            PopulateFilters("")
            PopulateSearch(ds, "", "Type ASC")
            PopulateAptinfo(0, 1, "", "")
        End If
    End Sub


    Public Sub PopulateFilters(ByVal theColumn As String, ByVal theFilter As String)

        Select Case theColumn
            Case "building"
                filter_building.DataSource = getDistinctRows("Units", "Building", theFilter)
                filter_building.DataBind()
            Case "type"
                filter_type.DataSource = getDistinctRows("Units", "Type", theFilter)
                filter_type.DataBind()
            Case "floors"
                filter_floor.DataSource = getDistinctRows("Units", "Floor", theFilter)
                filter_floor.DataBind()
            Case "price"
                Dim DistinctPriceTable As DataTable
                Dim FilterFlag1 As Boolean = False
                Dim FilterFlag2 As Boolean = False
                Dim FilterFlag3 As Boolean = False
                Dim FilterFlag4 As Boolean = False

                DistinctPriceTable = getDistinctRows("Units", "Price", theFilter)
                For Each Row As DataRow In DistinctPriceTable.Rows
                    If (Row("Price") < 1000) Then
                        FilterFlag1 = True
                    ElseIf ((Row("Price") < 2000)) Then
                        FilterFlag2 = True
                    ElseIf ((Row("Price") < 3000)) Then
                        FilterFlag3 = True
                    ElseIf ((Row("Price") >= 3000)) Then
                        FilterFlag4 = True
                    End If
                Next

                If (FilterFlag1) Then
                    lblFilterPrice1.Text = "<hr /><a href=""javascript:doFilter('price','0');"">$0 - $999</a>"
                Else
                    lblFilterPrice1.Text = ""
                End If
                If (FilterFlag2) Then
                    lblFilterPrice2.Text = "<hr /><a href=""javascript:doFilter('price','1000');"">$1,000 - $1,999</a>"
                Else
                    lblFilterPrice2.Text = ""
                End If
                If (FilterFlag3) Then
                    lblFilterPrice3.Text = "<hr /><a href=""javascript:doFilter('price','2000');"">$2,000 - $2,999</a>"
                Else
                    lblFilterPrice3.Text = ""
                End If
                If (FilterFlag4) Then
                    lblFilterPrice4.Text = "<hr /><a href=""javascript:doFilter('price','3000');"">$3,000+</a>"
                Else
                    lblFilterPrice4.Text = ""
                End If

        End Select
    End Sub
    Public Sub PopulateFilters(ByVal theFilter As String)

        filter_building.DataSource = getDistinctRows("Units", "Building", theFilter)

        filter_building.DataBind()

        filter_floor.DataSource = getDistinctRows("Units", "Floor", theFilter)
        filter_floor.DataBind()


        filter_type.DataSource = getDistinctRows("Units", "Type", theFilter)

        filter_type.DataBind()

        Dim DistinctPriceTable As DataTable
        Dim FilterFlag1 As Boolean = False
        Dim FilterFlag2 As Boolean = False
        Dim FilterFlag3 As Boolean = False
        Dim FilterFlag4 As Boolean = False

        DistinctPriceTable = getDistinctRows("Units", "Price", theFilter)
        For Each Row As DataRow In DistinctPriceTable.Rows
            If (Row("Price") < 1000) Then
                FilterFlag1 = True
            ElseIf ((Row("Price") < 2000)) Then
                FilterFlag2 = True
            ElseIf ((Row("Price") < 3000)) Then
                FilterFlag3 = True
            ElseIf ((Row("Price") >= 3000)) Then
                FilterFlag4 = True
            End If
        Next

        If (FilterFlag1) Then
            lblFilterPrice1.Text = "<hr /><a href=""javascript:doFilter('price','0');"">$0 - $999</a>"
        Else
            lblFilterPrice1.Text = ""
        End If
        If (FilterFlag2) Then
            lblFilterPrice2.Text = "<hr /><a href=""javascript:doFilter('price','1000');"">$1,000 - $1,999</a>"
        Else
            lblFilterPrice2.Text = ""
        End If
        If (FilterFlag3) Then
            lblFilterPrice3.Text = "<hr /><a href=""javascript:doFilter('price','2000');"">$2,000 - $2,999</a>"
        Else
            lblFilterPrice3.Text = ""
        End If
        If (FilterFlag4) Then
            lblFilterPrice4.Text = "<hr /><a href=""javascript:doFilter('price','3000');"">$3,000+</a>"
        Else
            lblFilterPrice4.Text = ""
        End If

    End Sub
    Public Sub PopulateSearch(ByVal theDataSet As DataSet, ByVal theFilter As String, ByVal theSort As String)

        Dim SearchArray() As DataRow

        'Dim UnitArea As Integer
        'Dim UnitPrice As Integer

        Dim resizeLength As Integer = 50

        SearchArray = theDataSet.Tables("Units").Select(theFilter, theSort)
        Session("SearchArrayLength") = SearchArray.Length
        If SearchArray.Length >= resizeLength Then
            Array.Resize(SearchArray, resizeLength)
            lblSearchResultsMore.Text = "<div class=""search_row""><div style=""font-size:16px;font-style:italic;font-weight:bold;margin-top:0px;margin-left:8px;"">Please refine your search to reduce the number of units displayed.</div></div>"
        Else
            Array.Resize(SearchArray, SearchArray.Length)
        End If



        'For Each Row As DataRow In SearchArray
        'UnitArea = Row("Area")
        'UnitPrice = Row("Price")

        'Row("Area") = String.Format("{0:n}", UnitArea)
        'Row("Price") = String.Format("{0:c}", UnitPrice)

        'Next

        search_results_rows.DataSource = SearchArray
        search_results_rows.DataBind()


    End Sub

    Public Sub PopulateAptinfo(ByVal RowIndex As String, ByVal MaxLength As Integer, ByVal theFilter As String, ByVal theSort As String)
        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)
        Dim iDAMPATHThumb As String = Replace(Session("WSRetreiveAsset") & "size=1&crop=1&width=80&height=80&id=", "&", "&amp;")
        Dim iDAMPATHFull As String = Replace(Session("WSRetreiveAsset") & "size=1&crop=2&width=850&height=510&qfactor=25&id=", "&", "&amp;")

        Dim dr As DataRow
        Dim PrevIndex As Integer
        Dim NextIndex As Integer

        Dim AptPrice As String
        Dim AptArea As Integer

        dr = getAptDataRow(RowIndex, theFilter, theSort)
        If Not dr Is Nothing Then

            If (RowIndex = (MaxLength - 1)) Then
                NextIndex = 0
            Else
                NextIndex = RowIndex + 1
            End If

            If (RowIndex = 0) Then
                PrevIndex = MaxLength - 1
            Else
                PrevIndex = RowIndex - 1
            End If

            AptArea = CInt(dr("Area"))

            AptPrice = dr("Price_String")


            lblModelNumber.Text = dr("Model_Name")
            'lblAptArea.Text = String.Format("{0:n}", AptArea)
            lblAptArea.Text = AptArea
            lblAptAvailable.Text = dr("Available")
            lblAptBath.Text = dr("Bathroom")
            lblAptBuilding.Text = dr("Building")
            lblAptFloor.Text = dr("Floor")
            lblAptNumber.Text = dr("Unit")
            lblAptNumber2.Text = dr("Unit")
            lblAptPrice.Text = AptPrice
            lblAptType.Text = dr("Type")
            lblAptType2.Text = dr("Type")
            'lblAptView.Text = dr("Views")

            If (MaxLength > 1) Then
                lblAptPrev.Text = "<a class=""lblAptPrev"" href=""javascript:loadApt(" & PrevIndex & ");""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_button_prev.jpg"" alt=""navi"" /></a>"
                lblAptNext.Text = "<a class=""lblAptNext"" href=""javascript:loadApt(" & NextIndex & ");""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_button_next.jpg"" alt=""navi"" /></a>"
            Else
                lblAptPrev.Text = ""
                lblAptNext.Text = ""
            End If


            'gallery
            lblDetailGalleryMainImage1.Text = "<img src=""" & iDAMPATHFull & dr("Model_AssetID") & """ alt=""" & dr("Unit") & """/>"
            lblDetailGalleryMainImage2.Text = "<img src="""" alt=""" & dr("Unit") & """/>"
            lblDetailGalleryFirstImage.Text = "<a href=""javascript:loadDetailGallery('" & iDAMPATHFull & dr("Model_AssetID") & "');""><img src=""" & iDAMPATHThumb & dr("Model_AssetID") & """ alt=""" & dr("Unit") & """ class=""active_thumb detail_thumb"" /></a>"
            gallery_detail_thumb_images_additional_image.DataSource = ds.Tables("Images").Select("Model_ID = " & dr("Model_ID"))

            If (ds.Tables("Images").Select("Model_ID = " & dr("Model_ID")).Length > 2) Then
                lblDetailGalleryThumbPrev.Text = "<img id=""detail_gallery_thumb_prev"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_thumb_prev.jpg"" alt=""thumbnails"" />"
                lblDetailGalleryThumbNext.Text = "<img id=""detail_gallery_thumb_next"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_thumb_next.jpg"" alt=""thumbnails"" />"
            Else
                lblDetailGalleryThumbPrev.Text = "<img style=""visibility:hidden;"" id=""detail_gallery_thumb_prev"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_thumb_prev.jpg"" alt=""thumbnails"" />"
                lblDetailGalleryThumbNext.Text = "<img style=""visibility:hidden;"" id=""detail_gallery_thumb_next"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_thumb_next.jpg"" alt=""thumbnails"" />"
            End If


            gallery_detail_thumb_images_additional_image.DataBind()

        End If
    End Sub


    Public Function getAptDataRow(ByVal RowIndex As String, ByVal theFilter As String, ByVal theSort As String) As DataRow
        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)
        Dim dr() As DataRow

        dr = ds.Tables("Units").Select(theFilter, theSort)

        Return dr(RowIndex)


    End Function

    Public Function getDistinctRows(ByVal theTableName As String, ByVal theColumnName As String, ByVal theFilter As String) As DataTable
        Dim DV As New DataView
        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)
        DV.Table = ds.Tables(theTableName)
        If (theFilter <> "") Then
            'DV.RowFilter = theColumnName & " <> NULL and " & theFilter
            DV.RowFilter = theFilter
        Else
            'DV.RowFilter = theColumnName & " <> NULL"
        End If

        DV.Sort = theColumnName & " asc"
        Return DV.ToTable(theColumnName, True, theColumnName)

    End Function

    Public Sub CallBack1_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBack1.Callback
        PopulateAptinfo(e.Parameters(0), e.Parameters(1), e.Parameters(2), e.Parameters(3))
        PlaceHolder1.RenderControl(e.Output)
    End Sub

    Public Sub CallBack2_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBack2.Callback
        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)
        PopulateSearch(ds, e.Parameters(0), e.Parameters(1))
        PlaceHolder2.RenderControl(e.Output)
    End Sub
    Public Sub CallbackFilterBuilding_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBackFilterBuilding.Callback
        PopulateFilters("building", e.Parameter)
        PlaceholderFilterBuilding.RenderControl(e.Output)
    End Sub
    Public Sub CallBackFilterType_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBackFilterType.Callback
        PopulateFilters("type", e.Parameter)
        PlaceholderFilterType.RenderControl(e.Output)
    End Sub
    Public Sub CallBackFilterFloors_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBackFilterFloors.Callback
        PopulateFilters("floors", e.Parameter)
        PlaceholderFilterFloors.RenderControl(e.Output)
    End Sub
    Public Sub CallBackFilterPrice_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBackFilterPrice.Callback
        PopulateFilters("price", e.Parameter)
        PlaceholderFilterPrice.RenderControl(e.Output)
    End Sub
    Public Sub CallBackSearchHeader_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBackSearchHeader.Callback
        Dim SearchArrayLength As String
        SearchArrayLength = Session("SearchArrayLength")
        lblSearchResults.Text = SearchArrayLength
        PlaceHolderSearchHeader.RenderControl(e.Output)
    End Sub

End Class
