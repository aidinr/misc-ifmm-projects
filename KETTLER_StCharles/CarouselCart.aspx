﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CarouselCart.aspx.vb" Inherits="CarouselCart" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Carousel Shopping Cart</title>
    <style type="text/css" >
        @media screen {
        body 
        {
            background: url('images/stcharles/cart_bg.jpg');
            font-family: Helvetica,Arial;
            font-size: 14px;
            line-height: 18px;   
             width:1275px;
             height:750px;
             overflow:hidden;
        }
        big 
        {
            font-weight: 600;
            font-size: 24px;
        }
        img 
        {
            border: 0;
        }
        #close 
        {
            position: absolute;
            right: 38px;   
            top: 15px;
        }
        #main 
        {
            margin-left:32px;
            margin-right: 30px;
            margin-bottom: 0px;
            margin-top:65px;
            width:1275px;
            float: left;
        }
        #main .item 
        {
            background: #9f9f9f;
            width: 398px;
            height: 278px;
            float: left;
            margin-left: 33px;
            margin-bottom: 18px;   
        }
        #main #item0, #main #item3 
        {
            margin-left: 0;
        }
        #main .item table 
        {
            margin-left: 20px;
            margin-top: 18px;
        }
        #main .item table div
        {
            margin-top: 14px;
        }
        #main .item table .remove
        {
            margin-left: 8px;
        }
        #main .item table .print
        {
            margin-left: 8px;
        }
        #paging 
        {
            width:398px;
            height: 46px;
            margin-left:;
            position: absolute;
            left: 471px;
            top: 667px;
        }
        #print 
        {
            
            position: absolute;
            right: 38px;   
            top: 677px;
            
        }
        .print_floorplan 
        {
            display:none;
            visibility: hidden;
        }
        }
        
@media print 
{
body {
	padding:0;
	margin:0;
	background: white;
	font-family: arial;
	font-size: 14px;
	color: #666666;
}
img {
	border: 0;
}
.print_floorplan 
{
    display:block;
    margin: 0;
	font-family: helvetica, arial;
	font-size: 14px;
	line-height: 18px;
	color: #000000;
	width:800px;
	page-break-after:always;
}
	.print_details td 
	{
		border-bottom: 1px solid #666666;
		padding-bottom: 6px;
		padding-top: 6px;
		padding-right:30px;
	}
	.floorplan 
	{
	    display: block;
	}
	.print_floorplan table 
	{
	    display:block;
	}
    #main,#close,#paging,#print 
    {
        display:none;
        visibility:hidden;
    }
}



    </style>
    <script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings("jsFolder")%>jquery-1.2.3.pack.js"></script>
    <script type="text/javascript">
        function printApt(apt) {
            $('.print_floorplan').css("display", "none");
            $('#print_' + apt).css("display", "block");
            window.print();
        }
        function printAll() {

            $('.print_floorplan').css("display", "block");
        window.print();
        }
    </script>
</head>
<body onclick="self.parent.resetSS();">
    <form id="form1" runat="server">
    <div id="main">
        <asp:Repeater ID="repeaterCarousel" runat="server" OnItemDataBound="repeaterCarousel_ItemDataBound">
            <ItemTemplate>
            <div class="item" id="item<%#Container.ItemIndex%>">
            <table width="353" cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign="top" width="230" align="left"><img src="<%=Session("WSRetreiveAsset")%>size=1&width=207&height=207&crop=2&id=<%#Container.DataItem("Model_ASSETID")%>" alt="Thumb" /></td>
                        <td valign="top">                        <big><%#Container.DataItem("Price_String")%></big><br /><br />                        Apartment <%#Container.DataItem("Unit")%><br />                        <%#Container.DataItem("type")%><br />                        <%#Container.DataItem("Bathroom")%> Bathroom<br />                        <%#Container.DataItem("Area")%> SF<br />                        <asp:literal ID="literalFloor" runat="server"></asp:literal> Floor<br />                        Vacant                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div>
                            <a href="CarouselCart.aspx?page=<%=request.querystring("page") %>&action=delete&deleteid=<%#Container.DataItem("ASSET_ID")%>"><img src="images/DelRay/cart_remove.jpg" alt="Remove" class="print" /></a>
                            <img onclick="printApt(<%#Container.ItemIndex %>);" src="images/DelRay/cart_print.jpg" alt="Print" class="print" />
                            </div>
                        </td>
                    </tr>
            </table>
        </div>
            </ItemTemplate>
        
        </asp:Repeater>
        
        
    </div>
    <div id="close">
        <a href="javascript:self.parent.tb_remove();"><img src="images/DelRay/cart_close.jpg" alt="Close" /></a>
    </div>
    <div id="paging">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="right"><asp:HyperLink runat="server" ID="linkPrev"><asp:Image ID="imagePrev" runat="server" ImageUrl="images/DelRay/cart_prev_off.jpg" /></asp:HyperLink></td>
                <td align="center"><asp:Literal ID="literalPaging" runat="server"></asp:Literal></td>
                <td align="left"><asp:HyperLink runat="server" ID="linkNext"><asp:Image ID="imageNext" runat="server" ImageUrl="images/DelRay/cart_next_off.jpg" /></asp:HyperLink></td>
            </tr>
        </table>
    
    </div>
        <div id="print" onclick="printAll();">
        <asp:Image ID="imagePrint" runat="server" ImageUrl="images/DelRay/cart_print_all.jpg" />
        </div>
        <script type="text/javascript">
            self.parent.fireDetailCarouselCallback();
            self.parent.CallBackSearchCarousel.Callback('');
        </script>
        
        <asp:Repeater ID="RepeaterPrint" runat="server" OnItemDataBound="repeaterPrint_ItemDataBound">
        <ItemTemplate>

        <div class="print_floorplan" id="print_<%#Container.ItemIndex %>">
				<img src="images/print_header.jpg" />
				<div class="floorplan">
	                <asp:Label ID="printImage" runat="server"></asp:Label>
                </div>
                <table style="margin-top:40px;margin-left:30px;width:750px;" cellpadding="0" cellspacing="0" class="print_container">
		                <tr>
			                <td valign="top" width="450">
				                <table cellpadding="0" cellspacing="0" width="100%">
					                <tr>
						                <td valign="top" width="220" align="left"><asp:Label ID="printImage1" runat="server"></asp:Label></td>
						                <td valign="top" align="left"><asp:Label ID="printImage2" runat="server"></asp:Label></td>
					                </tr>
				                </table>
			                </td>
			                <td valign="top" width="300">
					                <table cellpadding="0" cellspacing="0" class="print_details" width="300">
						                            <tr>
						                                 <td colspan="2" style="padding-top:0;">
						                                        <big><b>Apartment <asp:Label ID="printNumber" runat="server"></asp:Label></b></big>
						                                 </td>
						                            </tr>
						                            <tr>
							                            <td align="left"  width="130">Type:</td>
							                            <td align="left"><asp:Label ID="printType" runat="server"></asp:Label></td>
						                            </tr>
						                            <tr>
							                            <td align="left" width="130">Bathroom(s):</td>
							                            <td align="left"><asp:Label ID="printBath" runat="server"></asp:Label></td>
						                            </tr>
						                            <tr>
							                            <td align="left" width="130">Square Feet:</td>
							                            <td align="left"><asp:Label ID="printSQF" runat="server"></asp:Label></td>
						                            </tr>
						                            <tr>
							                            <td align="left" width="130">Floor Level:</td>
							                            <td align="left"><asp:Label ID="printFloor" runat="server"></asp:Label></td>
						                            </tr>
						                            <tr>
							                            <td align="left" width="130">View(s):</td>
							                            <td align="left"><asp:Label ID="printView" runat="server"></asp:Label></td>
						                            </tr>
						                            <tr>

							                            <td align="left" width="130">Price:</td>
							                            <td align="left"><asp:Label ID="printPrice" runat="server"></asp:Label></td>
						                            </tr>
						                            <tr>
							                            <td align="left" width="130">Availability:</td>
							                            <td align="left"><asp:Label ID="printAvailability" runat="server"></asp:Label></td>
						                            </tr>
					                            </table>
			                </td>
		                </tr>
		                <tr>
			                <td colspan="2">
				                <br />
				                1330 S. Fair Street, Arlington, VA &nbsp;  22204<br/>
				                <b>866.833.7158</b><br />
				                <b>www.metDelRay.com</b>
			                </td>
		                </tr>
                </table>
				</div>
            </ItemTemplate>
            </asp:Repeater> 
     </form>
</body>
</html>
