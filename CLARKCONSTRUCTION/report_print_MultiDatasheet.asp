<!--#include file="includes\config.asp" -->
<%
'''NOTE: THE DISCIPLINE AND FUNCTIONAL MARKET (OFFICE) TABLES ARE INVERTED DUE TO PAST MISLABELLING
if request.form("submit") = "1" then
%>
<!--#include file="includes\report_main.asp" -->
<%if (report_output = "html") then %>
<html>
<head>
<title>
Report
</title>
<style type="text/css">
td,th {
border: 1px solid gray ;
padding: 3px;
text-align: left;
vertical-align: top;
font-family: Myriad Pro;
font-size: 12px;
line-height: 16px;
color: #4d4d4d
}
table {
border:1px solid gray;
border-collapse: collapse;
padding:10px;
}
th {
background: #0076be;
}
</style>
</head>
<body>
<table style="width:100%; border: 1px solid transparent; "  >
<tr>
<td align="left" style="width:50%; text-align: left; border: 1px solid transparent;">
<font style="font-size:16px" color='#004b8d' ><%=UCASE(report_title)%> </font>
</td>
<td align="right" style="width:50%; text-align: right; border: 1px solid transparent;" >
<% If Request("search_display_logo") = "Clark" then%>
<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Atkinson" then %>
<img src="images/atkinson_logo.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Edgemoor" then %>
<img src="images/edgemoor_logo.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Shirley" then %>
<img src="images/shirley_logo.jpg" alt="Clark Logo" />
<%end if %></td>
</tr>
</table>
<br/>
<table width="100%">
<!--begin table heading-->
<tr>
<th > <b><font  color="white"> PROJECT NAME <br/> & LOCATION</font></b></th>
<th><b><font color="white">PROJECT DESCRIPTION</font></b></th>
<th><b><font color="white">SERVICE <br/>TYPE</font></b></th>
<th><b><font color="white">SQUARE<br/>FOOTAGE</font></b></th>
<th><b><font color="white">CONTRACT <br/>VALUE</font></b></th>
<th><b><font color="white">COMPLETION<br/>DATE</font></b></th>
</tr>
<!--end table heading-->
<!--Begin results listing-->
<%
 countii = 0
do while not rsProjects.eof%>
<%  if countii<>0 then %>
    <tr>
    <th colspan="6" style="background: #e3e9f5; "><b>&nbsp</b></th>
    </tr>
    <% 
    End If
    countii = countii+1
 %>
<tr>
<td align="left"  valign="top"><table style=" width:100%; border: 1px solid transparent;"><tr><td style="width: 152px; border: 1px solid transparent;"><img width="150"  src="<%=sAssetPath%><%=rsProjects("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=150&height=118&qfactor=25" alt="<%=rsProjects("Name")%>" /></td><td style="text-align: left; border: 1px solid transparent;" align="left" valign="top"><font color='#004b8d' ><%=UCase(rsProjects("Name"))%></font><br/><%=rsProjects("city") & ", " & rsProjects("state_id") %></td></tr></table></td>
<%
'''Begin Standard
'''End Standard
''''Begin UDFs
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_RESUME'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "<td style=""text-align: justify;  text-justify: newspaper; text-align-last: Left"">" & rsLineUDF("item_value") & "</td>"
else
response.write "<td>&nbsp;</td>"
end if
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_SERVICE'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "<td>" & rsLineUDF("item_value") & "</td>"
else
response.write "<td>&nbsp;</td>"
end if
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_GLA'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
        Response.Write("<td>" & FormatNumber(rsLineUDF("item_value"), 0) & "</td>")
else
response.write "<td>&nbsp;</td>"
end if
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_JDE_FinalContractValue'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "<td>" & rsLineUDF("item_value") & "</td>"
else
response.write "<td>&nbsp;</td>"
end if
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select case when ltrim(rtrim(isnull(item_value,''))) = '' then '' else convert(varchar(10),  convert(datetime, item_value), 101) + ''  end item_value from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_JDE_ActualCompletionDate'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
response.write "<td>&nbsp;</td>"
else
response.write "<td>" & rsLineUDF("item_value") & "&nbsp;</td>"
end if
else
response.write "<td>&nbsp;</td>"
end if
''''End Keywords
''''Begin Case Studies
   '''End Case Studies
   Response.Write("</tr>")
   rsProjects.moveNext()
Loop
%>
<!--End results listing-->
</table>
</body>
</html>
<%elseif (report_output = "csv") then
newLine = chr(13) & chr(10)
dateToday = Now
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=" & replace(report_title," ","-") & "_-_" & DatePart( "yyyy", dateToday )  & "-" & DatePart( "m", dateToday ) & "-" & DatePart( "d", dateToday ) & ".csv"
Response.AddHeader "Content-Description", "File Transfer"
response.write report_title & newLine
response.write "Project,"
'begin header
if(display_number = 1) then
response.write "Number,"
end if
if (display_client = 1) then
response.write "Owner,"
end if
if (display_address = 1) then
response.write "Address,"
end if
if (display_city_state = 1) then
response.write "Location,"
end if
if (display_zip = 1) then
response.write "ZIP,"
end if    
if (display_desc = 1) then
response.write "Summary,"
end if
if (display_mdesc = 1) then
response.write "Description,"
end if
if (display_url = 1) then
response.write "URL,"
end if    
do while not rsUDF.eof
itemTag = trim(rsUDF("item_tag"))
if(Request.Form(itemTag) = "1") then
response.write """" & replace(rsUDF("item_name"),"""","""""") & """" & ","
end if
rsUDF.moveNext
loop
if(displayDisc = 1) then
response.write "Discipline,"
end if
if(displayKeywords = 1) then
response.write "Keywords,"
end if
if(displayFunctional = 1) then
response.write "Functional Markets,"
end if    
if (displayChallenge = 1) then
response.write "Challenge,"
end if
if (displayApproach = 1) then
response.write "Approach,"
end if    
if (displaySolution = 1) then
response.write "Solution,"
end if    
response.write newLine
'end header
'begin listing
do while not rsProjects.eof
response.write trim(rsProjects("Name")) &","
if(display_number = 1) then
response.write """" & replace(rsProjects("projectNumber"),"""","""""") & """" & ","
end if
if (display_client = 1) then
response.write """" & replace(rsProjects("clientName"),"""","""""") & """" & ","
end if
if (display_address = 1) then
response.write """" & replace(replace(rsProjects("Address"),"""",""""""),newLine," ") & """" & ","
end if
if (display_city_state = 1) then
response.write """" & replace(rsProjects("city"),"""","""""") & " , " & replace(rsProjects("state_id"),"""","""""") & """" & ","
end if
if (display_zip = 1) then
response.write """" & replace(replace(rsProjects("Zip"),"""",""""""),newLine," ") & """" & ","
end if    
if (display_desc = 1) then
response.write """" & replace(replace(rsProjects("Description"),"""",""""""),newLine," ") & """" & ","
end if
if (display_mdesc = 1) then
response.write """" & replace(replace(rsProjects("DescriptionMedium"),"""",""""""),newLine," ") & """" & ","
end if
if (display_url = 1) then
response.write """" & replace(replace(rsProjects("URL"),"""",""""""),newLine," ") & """" & ","
end if    
rsUDF.moveFirst
do while not rsUDF.eof
itemTag = trim(rsUDF("item_tag"))
if(Request.Form(itemTag) = "1") then
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = '" & rsUDF("item_tag") & "'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
response.write """" & replace(replace(rsLineUDF("item_value"),"""",""""""),newLine," ") & """" & ","
else
response.write ","
end if
end if
rsUDF.moveNext
loop
''''Begin Keywords
if(displayDisc = 1) then
if(rsLineKeywords.State = 1) then
rsLineKeywords.close
end if
sql = "select * from ipm_project_office a,ipm_office b where a.officeid = b.keyid and a.projectid = " & rsProjects("projectID") & " order by keyname"
rsLineKeywords.Open sql, Conn, 1, 4
if(rsLineKeywords.recordCount > 0) then
lineKeywords = trim(rsLineKeywords("keyName"))
rsLineKeywords.movenext
do while not rsLineKeywords.eof
lineKeywords = lineKeywords & ", " & trim(rsLineKeywords("keyName"))
rsLineKeywords.movenext
loop
else
lineKeywords = ""
end if
response.write """" & replace(replace(lineKeywords,"""",""""""),newLine," ") & """" & ","
end if
if(displayKeywords = 1) then
if(rsLineKeywords.State = 1) then
rsLineKeywords.close
end if
sql = "select * from ipm_project_keyword a,ipm_keyword b where a.keyid = b.keyid and a.projectid = " & rsProjects("projectID") & " order by keyname"
rsLineKeywords.Open sql, Conn, 1, 4
if(rsLineKeywords.recordCount > 0) then
lineKeywords = trim(rsLineKeywords("keyName"))
rsLineKeywords.movenext
do while not rsLineKeywords.eof
lineKeywords = lineKeywords & ", " & trim(rsLineKeywords("keyName"))
rsLineKeywords.movenext
loop
else
lineKeywords = ""
end if
response.write """" & replace(replace(lineKeywords,"""",""""""),newLine," ") & """" & ","    
end if
if(displayFunctional = 1) then
if(rsLineKeywords.State = 1) then
rsLineKeywords.close
end if    
sql = "select * from ipm_project_discipline a,ipm_discipline b where a.keyid = b.keyid and a.projectid = " & rsProjects("projectID") & " order by keyname"
rsLineKeywords.Open sql, Conn, 1, 4
if(rsLineKeywords.recordCount > 0) then
lineKeywords = trim(rsLineKeywords("keyName"))
rsLineKeywords.movenext
do while not rsLineKeywords.eof
lineKeywords = lineKeywords & ", " & trim(rsLineKeywords("keyName"))
rsLineKeywords.movenext
loop
else
lineKeywords = ""
end if
response.write """" & replace(replace(lineKeywords,"""",""""""),newLine," ") & """" & ","    
end if
''''End Keywords
''''Begin Case Studies
'''End Case Studies    
rsProjects.moveNext
response.write newLine
loop
'end listing
elseif (report_output = "pdf") then
Set theDoc = Server.CreateObject("ABCpdf8.Doc")
theDoc.HtmlOptions.Timeout = 120000
' apply a rotation transform
w = theDoc.MediaBox.Width
h = theDoc.MediaBox.Height
l = theDoc.MediaBox.Left
b = theDoc.MediaBox.Bottom 
theDoc.Transform.Rotate 90, l, b
theDoc.Transform.Translate w, 0
' rotate our rectangle
theDoc.Rect.Width = h
theDoc.Rect.Height = w
theDoc.Rect.Inset 36, 18
theDoc.Page = theDoc.AddPage()
'theURL = "http://idam.clarkrealty.com:8081/report_print_get.asp?" & Request.Form
theURL = "http://ccgdc4idam004.int.clarkus.com:8081/report_print_get_MultiDatasheet.asp?" & Request.Form
'damjan
'Response.Redirect(theURL)
theID = theDoc.AddImageUrl(theURL)
theDate = MonthName(DatePart("m",Date)) & " " & DatePart("d",Date) & ", "& DatePart("yyyy",Date)
Do
  If Not theDoc.Chainable(theID) Then Exit Do
  theDoc.Pos.X = 670
theDoc.Pos.Y = 7
            'theDoc.addHTML  theDate
  
theDoc.Pos.X = 400
theDoc.Pos.Y = 7
            'theDoc.addHTML "Page " & theDoc.PageNumber
theDoc.Pos.X = 50
theDoc.Pos.Y = 7
            'theDoc.addHTML report_title
'Reset before adding page
'theDoc.Rect.Resize oriW,oriH
'theDoc.Rect.Position oriX,oriY
  
  theDoc.Page = theDoc.AddPage()
  theID = theDoc.AddImageToChain(theID)
Loop
  If Not theDoc.Chainable(theID) Then
theDoc.Pos.X = 670
theDoc.Pos.Y = 7
theDoc.addHTML  theDate
  
  
  theDoc.Pos.X = 400
theDoc.Pos.Y = 7
theDoc.addHTML "Page " & theDoc.PageNumber
theDoc.Pos.X = 50
theDoc.Pos.Y = 7
theDoc.addHTML report_title
  End if
For i = 1 To theDoc.PageCount
  theDoc.PageNumber = i
  theDoc.Flatten
Next
theDate = DatePart("m",Date) & "-" & DatePart("d",Date) & "-" & DatePart("yyyy",Date)
theProjectName = replace(report_title," ","_") & "_-_" & theDate &".pdf"
theDoc.Save "C:\TEMP\Project Reporting Generated PDFS\" & theProjectName
FileExt = Mid(theProjectName, InStrRev(theProjectName, ".") + 1)
set stream = server.CreateObject("ADODB.Stream")
stream.type = "1"
stream.open
stream.LoadFromFile "C:\TEMP\Project Reporting Generated PDFS\" & theProjectName
fileSize = stream.size
'response.write fileSize
response.clear
response.ContentType = "application/pdf"
Response.AddHeader "Content-Description", "File Transfer"
response.addheader "Content-Disposition", "attachment; filename=" & theProjectName
Response.AddHeader "Content-Length", fileSize
if(FileExt ="pdf") then
do while not(stream.eos)
response.BinaryWrite Stream.Read(8192)
response.Flush
loop
stream.Close
response.End
end if
'Response.Redirect "http://idam.clarkrealty.com/PUBLIC/cbg/pdf/" & theProjectName & ".pdf"
elseif (report_output = "word") then
' apply a rotation transform
'theURL = "http://ccgdc4idam004.int.clarkus.com:8081/report_print_get.asp?" & Replace(Request.Form, "pdf_page_break=", "pdf_page_break=100000")
theURL = "http://ccgdc4idam004.int.clarkus.com:8081/report_print_get_MultiDatasheet.asp?" & Request.Form
'damjan
'Response.Redirect(theURL)
theDate = DatePart("m",Date) & "-" & DatePart("d",Date) & "-" & DatePart("yyyy",Date)
theProjectName = replace(report_title," ","_") & "_-_" & theDate &".doc"
'damjan added
	Dim objSvrHTTP
	Set objSvrHTTP = Server.CreateObject("Msxml2.XMLHTTP")
	objSvrHTTP.open "GET", theURL, false
	objSvrHTTP.send 
	result =  objSvrHTTP.responseText 
'damjan writign file to folder
Response.Write result
dim fs,f
set fs=Server.CreateObject("Scripting.FileSystemObject")
set f=fs.CreateTextFile("C:\TEMP\Project Reporting Generated PDFS\" & theProjectName,true, true)
f.write(result)
f.close
set f=nothing
set fs=nothing
FileExt = Mid(theProjectName, InStrRev(theProjectName, ".") + 1)
set stream = server.CreateObject("ADODB.Stream")
stream.type = "1"
stream.open
stream.LoadFromFile "C:\TEMP\Project Reporting Generated PDFS\" & theProjectName
fileSize = stream.size
'response.write fileSize
response.clear
'damjan
'response.ContentType = "application/pdf"
'Response.AddHeader "Content-Description", "File Transfer"
'response.addheader "Content-Disposition", "attachment; filename=" & theProjectName 
'damjan
Response.ContentType = "application/vnd.ms-word"
Response.AddHeader "content-disposition", "inline; filename=" & theProjectName 
response.AddHeader "Content-Length", fileSize
if(FileExt ="doc") then
do while not(stream.eos)
response.BinaryWrite Stream.Read(8192)
response.Flush
loop
stream.Close
response.End
end if
'Response.Redirect "http://idam.clarkrealty.com/PUBLIC/cbg/pdf/" & theProjectName & ".pdf"
end if
%>