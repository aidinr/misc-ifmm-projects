<!--#include file="includes\config.asp" -->
<%
'''NOTE: THE DISCIPLINE AND FUNCTIONAL MARKET (OFFICE) TABLES ARE INVERTED DUE TO PAST MISLABELLING
if Request.Querystring("submit") = "1" then
%>
<!--#include file="includes\report_main_pdf_get.asp" -->
<% 'if (report_output = "html") then %>
<html>
<head>
<title>
Report
</title>
<style type="text/css">
td,th {
border: 1px solid #777777;
padding: 3px;
text-align: center;
vertical-align: top;
font-family: verdana;
font-size: 12px;
}
table {
border:1px solid #777777;
border-collapse: collapse;
padding:3px;
}
th {
background: #DDDDDD;
}
</style>
</head>
<body>
<table style="width:100%; border: 1px solid transparent; "  >
<tr>
<td align="left" style="width:50%; text-align: left; border: 1px solid transparent;">
<font style="font-size:16px" color='#004b8d' ><%=UCase(report_title)%> </font>
</td>
<td align="right" style="width:50%; text-align: right; border: 1px solid transparent;">
<% If Request("search_display_logo") = "Clark" then%>
<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Atkinson" then %>
<img src="images/atkinson_logo.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Edgemoor" then %>
<img src="images/edgemoor_logo.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Shirley" then %>
<img src="images/shirley_logo.jpg" alt="Clark Logo" />
<%end if %>
</td>
</tr>
</table>
<table width="100%">
<!--begin table heading-->
<tr>
<th><b>Project</b></td>
<%
if(display_number = 1) then
response.write "<th><b>Number</b></th>"
end if
if (display_client = 1) then
response.write "<th><b>Owner</b></th>"
end if
if (display_address = 1) then
response.write "<th><b>Address</b></th>"
end if
if (display_city_state = 1) then
response.write "<th><b>Location</b></th>"
end if
if (display_zip = 1) then
response.write "<th><b>ZIP</b></th>"
end if    
if (display_desc = 1) then
response.write "<th><b>Summary</b></th>"
end if
if (display_mdesc = 1) then
response.write "<th><b>Description</b></th>"
end if
if (display_url = 1) then
response.write "<th><b>URL</b></th>"
end if    
do while not rsUDF.eof
itemTag = trim(rsUDF("item_tag"))
if(Request.Querystring(itemTag) = "1") then
response.write "<th><b>" & rsUDF("item_name") & "</b></th>"
end if
rsUDF.moveNext
loop
if(displayDisc = 1) then
response.write "<th><b>Discipline</b></th>"
end if
if(displayKeywords = 1) then
response.write "<th><b>Keywords</b></th>"
end if
if(displayFunctional = 1) then
response.write "<th><b>Functional Markets</b></th>"
end if    
if (displayChallenge = 1) then
response.write "<th><b>Challenge</b></th>"
end if
if (displayApproach = 1) then
response.write "<th><b>Approach</b></th>"
end if    
if (displaySolution = 1) then
response.write "<th><b>Solution</b></th>"
end if    
i=0
%>
</tr>
<!--end table heading-->
<!--Begin results listing-->
<%do while not rsProjects.eof%>
<tr>
<td align="center" valign="top"><b><%=rsProjects("Name")%></b><%if (report_display_image = "1") then %><br /><img width="150"  src="<%=sAssetPath%><%=rsProjects("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=150&height=118&qfactor=25" alt="<%=rsProjects("Name")%>" /><%end if%></td>
<%
'''Begin Standard
if(display_number = 1) then
response.write "<td>"& rsProjects("projectNumber")  &"&nbsp;</td>"
end if
if (display_client = 1) then
response.write "<td>"& rsProjects("clientName")  &"&nbsp;</td>"
end if
if (display_address = 1) then
response.write "<td>"& rsProjects("Address")  &"&nbsp;</td>"
end if
if (display_city_state = 1) then
if(rsProjects("city") <> "") then
rsProjects("city") = rsProjects("city") & ", "
end if
response.write "<td>"& rsProjects("city") & rsProjects("state_id")  &"&nbsp;</td>"
end if
if (display_zip = 1) then
response.write "<td>"& rsProjects("zip")  &"&nbsp;</td>"
end if    
if (display_desc = 1) then
response.write "<td>"& rsProjects("Description")  &"&nbsp;</td>"
end if
if (display_mdesc = 1) then
response.write "<td>"& rsProjects("DescriptionMedium")  &"&nbsp;</td>"
end if
if (display_url = 1) then
response.write "<td>"& rsProjects("url")  &"&nbsp;</td>"
end if
'''End Standard
''''Begin UDFs
rsUDF.moveFirst
do while not rsUDF.eof
itemTag = trim(rsUDF("item_tag"))
if(Request.Querystring(itemTag) = "1") then
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = '" & rsUDF("item_tag") & "'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "<td>" & rsLineUDF("item_value") & "</td>"
else
response.write "<td>&nbsp;</td>"
end if
end if
rsUDF.moveNext
loop
''''End UDFs
''''Begin Keywords
if(displayDisc = 1) then
if(rsLineKeywords.State = 1) then
rsLineKeywords.close
end if
sql = "select * from ipm_project_office a,ipm_office b where a.officeid = b.keyid and a.projectid = " & rsProjects("projectID")
rsLineKeywords.Open sql, Conn, 1, 4
if(rsLineKeywords.recordCount > 0) then
lineKeywords = trim(rsLineKeywords("keyName"))
rsLineKeywords.movenext
do while not rsLineKeywords.eof
lineKeywords = lineKeywords & ", " & trim(rsLineKeywords("keyName"))
rsLineKeywords.movenext
loop
else
lineKeywords = "&nbsp;"
end if
response.write "<td>" & lineKeywords & "</td>"
end if
if(displayKeywords = 1) then
if(rsLineKeywords.State = 1) then
rsLineKeywords.close
end if
sql = "select * from ipm_project_keyword a,ipm_keyword b where a.keyid = b.keyid and a.projectid = " & rsProjects("projectID")
rsLineKeywords.Open sql, Conn, 1, 4
if(rsLineKeywords.recordCount > 0) then
lineKeywords = trim(rsLineKeywords("keyName"))
rsLineKeywords.movenext
do while not rsLineKeywords.eof
lineKeywords = lineKeywords & ", " & trim(rsLineKeywords("keyName"))
rsLineKeywords.movenext
loop
else
lineKeywords = "&nbsp;"
end if
response.write "<td>" & lineKeywords & "</td>"    
end if
if(displayFunctional = 1) then
if(rsLineKeywords.State = 1) then
rsLineKeywords.close
end if    
sql = "select * from ipm_project_discipline a,ipm_discipline b where a.keyid = b.keyid and a.projectid = " & rsProjects("projectID")
rsLineKeywords.Open sql, Conn, 1, 4
if(rsLineKeywords.recordCount > 0) then
lineKeywords = trim(rsLineKeywords("keyName"))
rsLineKeywords.movenext
do while not rsLineKeywords.eof
lineKeywords = lineKeywords & ", " & trim(rsLineKeywords("keyName"))
rsLineKeywords.movenext
loop
else
lineKeywords = "&nbsp;"
end if
response.write "<td>" & lineKeywords & "</td>"    
end if
''''End Keywords
''''Begin Case Studies
   '''End Case Studies
   Response.Write("</tr>")
   rsProjects.moveNext()
            i = i + 1
if(i mod report_page_break = 0 and not rsProjects.eof) then %>
</table>
<div style="page-break-before:always">&nbsp;</div> 
<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
<h4><%=report_title%></h4>
<table width="100%">
<!--begin table heading-->
<tr>
<th><b>Project</b></th>
<%
if(display_number = 1) then
response.write "<th><b>Number</b></th>"
end if
if (display_client = 1) then
response.write "<th><b>Owner</b></th>"
end if
if (display_address = 1) then
response.write "<th><b>Address</b></th>"
end if
if (display_city_state = 1) then
response.write "<th><b>Location</b></th>"
end if
if (display_zip = 1) then
response.write "<th><b>ZIP</b></th>"
end if    
if (display_desc = 1) then
response.write "<th><b>Summary</b></th>"
end if
if (display_mdesc = 1) then
response.write "<th><b>Description</b></th>"
end if
if (display_url = 1) then
response.write "<th><b>URL</b></th>"
end if    
rsUDF.moveFirst
do while not rsUDF.eof
itemTag = trim(rsUDF("item_tag"))
if(Request.querystring(itemTag) = "1") then
response.write "<th><b>" & rsUDF("item_name") & "</b></th>"
end if
rsUDF.moveNext
loop
if(displayDisc = 1) then
response.write "<th><b>Discipline</b></th>"
end if
if(displayKeywords = 1) then
response.write "<th><b>Keywords</b></th>"
end if
if(displayFunctional = 1) then
response.write "<th><b>Functional Markets</b></th>"
end if    
if (displayChallenge = 1) then
response.write "<th><b>Challenge</b></th>"
end if
if (displayApproach = 1) then
response.write "<th><b>Approach</b></th>"
end if    
if (displaySolution = 1) then
response.write "<th><b>Solution</b></th>"
end if    
%>
</tr>
<!--end table heading-->    
<%
end if    
Loop
%>
<!--End results listing-->
</table>
</body>
</html>