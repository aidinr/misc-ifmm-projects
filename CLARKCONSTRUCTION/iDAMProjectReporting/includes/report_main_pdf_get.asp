<%        'Pre-set parameters
report_title = Request.Querystring("report_name")
if(report_title = "") then
report_title = "Report"
end if
report_name = prep_sql(Request.Querystring("search_name"))
        
        report_STATE = prep_sql(Request.Querystring("ITEM_15_-2[]"))
        report_DELIVERY = prep_sql(Request.Querystring("ITEM_15_-1[]"))
        report_STRUCTURAL_TYPE = prep_sql(Request.Querystring("ITEM_15_2417370[]"))
        report_OWNER_TYPE = prep_sql(Request.Querystring("ITEM_15_2417362[]"))
        report_LEED = prep_sql(Request.Querystring("ITEM_15_2417353[]"))
        report_PROTYPE = prep_sql(Request.Querystring("ITEM_15_2417355[]"))
        report_STRUCTURAL_ENGINEER = prep_sql(Request.Querystring("hdnSelectedValues_2417369"))
        report_MEP = prep_sql(Request.Querystring("hdnSelectedValues_2417359"))
        report_ELE = prep_sql(Request.Querystring("hdnSelectedValues_2417358"))
        report_DEV = prep_sql(Request.Querystring("hdnSelectedValues_2417357"))
        report_CLIENTOWNER = prep_sql(Request.Querystring("hdnSelectedValues_2417351"))
        report_ARCH = prep_sql(Request.Querystring("hdnSelectedValues_2417354"))
        report_SF_From = prep_sql(Request.Querystring("SF_From"))
        report_SF_TO = prep_sql(Request.Querystring("SF_To"))
        report_Cost_From = prep_sql(Request.Querystring("Cost_From"))
        report_Cost_TO = prep_sql(Request.Querystring("Cost_To"))
        report_Compl_From = prep_sql(Request.Querystring("Completion_From"))
        report_Compl_To = prep_sql(Request.Querystring("Completion_To"))
        report_output = Request.Querystring("search_output")
   report_page_break = int(Request.Querystring("pdf_page_break"))
report_display_image = Request.Querystring("search_display_image")
if(Request.Querystring("search_favorites") = "0") then
report_favorite = " AND c.favorite = 0"
elseif(Request.Querystring("search_favorites") = "1") then
report_favorite = " AND c.favorite = 1"
else
report_favorite = ""
end if
if(Request.Querystring("search_published") = "0") then
report_published = " AND c.publish = 0"
elseif(Request.Querystring("search_published") = "1") then
report_published = " AND c.publish = 1"
else
report_published = ""
end if
display_number = Request.Querystring("number")
display_client = Request.Querystring("client")
display_address = Request.Querystring("address")
display_city_state = Request.Querystring("city_state")
display_zip = Request.Querystring("zip")
display_desc = Request.Querystring("desc")
display_mdesc = Request.Querystring("mdesc")
display_url = Request.Querystring("purl")
displayChallenge = Request.Querystring("challenge")
displayApproach = Request.Querystring("approach")
displaySolution = Request.Querystring("solution")
displayDisc = Request.Querystring("disciplines")
displayKeywords = Request.Querystring("keywords")
displayFunctional = Request.Querystring("functional")    
'    if (report_output <> "pdf") then
'Big search filter
set rsProjects = server.createobject("adodb.recordset")
'sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description from ipm_project_field_value a, ipm_project_field_desc b, ipm_project c, ipm_project_discipline d, ipm_discipline e WHERE a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_REGION' AND a.projectid = c.projectid AND c.publish = 1AND item_value LIKE '%" & report_region & "%' AND d.projectid = c.projectid AND d.keyid = e.keyid AND c.name LIKE '%" & report_name & "%' AND isnull(c.clientname, '') LIKE '%" & report_client & "%'AND c.state_id LIKE '%" & report_state & "%' AND c.projectID IN (select ipm_project.projectid from ipm_project, ipm_project_discipline where ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND keyid LIKE '%" & report_disc & "%') ORDER BY c.name"
            'sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description from ipm_project_field_value a, ipm_project_field_desc b, ipm_project c WHERE a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_REGION' AND a.projectid = c.projectid AND c.publish = 1 " & report_favorite & "  AND item_value LIKE '%" & report_region & "%' AND c.name LIKE '%" & report_name & "%' AND isnull(c.clientname, '') LIKE '%" & report_client & "%'AND c.state_id LIKE '%" & report_state & "%' ORDER BY c.name"
    ' squarefoot filters and joins
    strSFMinFilter = ""
    strSFMaxFilter = ""
    strSFtableJOIN = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_tag = 'IDAM_GLA') ) sf on sf.ProjectID = c.projectid "
    If report_SF_From <> "No Minimum" then
        strSFMinFilter= " AND isnull(replace(sf.item_value,',', ''),0) >= " & report_SF_From
    End If
    If report_SF_TO <> "No Maximum" then
        strSFMaxFilter= " AND isnull(replace(replace(sf.item_value,',', ''), '$', ''),0) <= " & report_SF_TO
    End If
    If Trim(report_SF_TO) = "No Maximum" and Trim(report_SF_FROM) = "No Minimum"  then
                strSFtableJOIN=""       
    end if
    ' end square foot
    ' Cost filters and joins
    strCOSTMinFilter = ""
            strCOSTMaxFilter = ""
            
            strCOSTtableJOIN = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_tag = 'IDAM_JDE_FinalContractValue') ) cv on cv.ProjectID = c.projectid "
            strCOSTtableJOIN_mcv = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_id = 2460084) ) mcv on mcv.ProjectID = c.projectid "
            If report_COST_From <> "" Then
                strCOSTMinFilter = " AND case when isnull(replace(replace(mcv.item_value,',', ''), '$', ''),0) = 0 then isnull(replace(replace(cv.item_value,',', ''), '$', ''),0) else isnull(replace(replace(mcv.item_value,',', ''), '$', ''),0) end >= " & report_COST_From
            End If
 
            If report_COST_TO <> "" Then
                strCOSTMaxFilter = " AND case when isnull(replace(replace(mcv.item_value,',', ''), '$', ''),0) = 0 then isnull(replace(replace(cv.item_value,',', ''), '$', ''),0) else isnull(replace(replace(mcv.item_value,',', ''), '$', ''),0) end <= " & report_COST_TO
            End If
   
            If Trim(report_COST_To) = "" And Trim(report_COST_From) = "" Then
                strCOSTtableJOIN = ""
                strCOSTtableJOIN_mcv = ""
            End If
    ' end Cost
    ' Compl filters and joins
    strCOMPLMinFilter = ""
    strCOMPLMaxFilter = ""
            strCOMPLtableJOIN = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_tag = 'IDAM_JDE_ActualCompletionDate') ) cd on cd.ProjectID = c.projectid "
            strCOMPLtableJOIN_mcd = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_id=2460085) ) mcd on mcd.ProjectID = c.projectid "
            strCOMPLtableJOIN_scd = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_id=2420815) ) scd on scd.ProjectID = c.projectid "
            If report_COMPL_From <> "" Then
                strCOMPLMinFilter = " AND  case when    convert(varchar(10), convert(datetime, case when isnull(replace(  case when ltrim(rtrim(isnull(mcd.item_value,''))) ='' then  cd.item_value else mcd.item_value end   ,',', '') ,'1/1/1900') = '' then '1/1/1900' else isnull(replace( case when ltrim(rtrim(isnull(mcd.item_value,''))) ='' then  cd.item_value else mcd.item_value end ,',', '') ,'1/1/1900') end), 101)    = '01/01/1900' then case when ltrim(rtrim(isnull(scd.item_value, '1/1/1900'))) = '' then '1/1/1900' else convert(varchar(10), convert(datetime, scd.item_value), 101)  end     else   convert(varchar(10), convert(datetime, case when isnull(replace(  case when ltrim(rtrim(isnull(mcd.item_value,''))) ='' then  cd.item_value else mcd.item_value end   ,',', '') ,'1/1/1900') = '' then '1/1/1900' else isnull(replace( case when ltrim(rtrim(isnull(mcd.item_value,''))) ='' then  cd.item_value else mcd.item_value end ,',', '') ,'1/1/1900') end), 101)     end       >= convert(datetime, '" & report_COMPL_From & "')"
            End If
    
            If report_COMPL_To <> "" Then
                strCOMPLMaxFilter = " AND case when    convert(varchar(10), convert(datetime, case when isnull(replace(  case when ltrim(rtrim(isnull(mcd.item_value,''))) ='' then  cd.item_value else mcd.item_value end   ,',', '') ,'1/1/1900') = '' then '1/1/1900' else isnull(replace( case when ltrim(rtrim(isnull(mcd.item_value,''))) ='' then  cd.item_value else mcd.item_value end ,',', '') ,'1/1/1900') end), 101)    = '01/01/1900' then case when ltrim(rtrim(isnull(scd.item_value, '1/1/1900'))) = '' then '1/1/1900' else convert(varchar(10), convert(datetime, scd.item_value), 101)  end     else   convert(varchar(10), convert(datetime, case when isnull(replace(  case when ltrim(rtrim(isnull(mcd.item_value,''))) ='' then  cd.item_value else mcd.item_value end   ,',', '') ,'1/1/1900') = '' then '1/1/1900' else isnull(replace( case when ltrim(rtrim(isnull(mcd.item_value,''))) ='' then  cd.item_value else mcd.item_value end ,',', '') ,'1/1/1900') end), 101)     end        <=  convert(datetime, '" & report_COMPL_To & "')"
            End If
    
            If Trim(report_COMPL_To) = "" And Trim(report_COMPL_From) = "" Then
                strCOMPLtableJOIN = ""
                strCOMPLtableJOIN_mcd = ""
                strCOMPLtableJOIN_scd = ""
            End If
    ' end Compl
    ' STATE filters 
    strStateFilter = ""
    If report_STATE <> "" and report_STATE <> "0" then
            arr=Split(report_STATE, ",")
            For Each item in arr
                if Trim(item) <> "" then
                    strStateFilter = strStateFilter & " OR ltrim(rtrim(c.state_id)) = '" & Trim(item) & "' "
                end if
            Next
    End If
    If Trim(strStateFilter) <>  "" then
        strStateFilter = " AND (" &  Mid(strStateFilter, 4) & " ) "
    End If
    ' end STATE
    ' DELIVERY filters 
    strDELIVERYFilter = ""
    If report_DELIVERY <> "" and report_DELIVERY <> "0" then
            arr=Split(report_DELIVERY, ",")
            For Each item in arr
                if Trim(item) <> "" then
                    strDELIVERYFilter = strDELIVERYFilter & " OR ltrim(rtrim(keyname)) = '" & Trim(item) & "' "
                end if
            Next
    End If
    If Trim(strDELIVERYFilter) <>  "" then
       
        strDELIVERYFilter = " AND (select count(*) from ipm_office office join ipm_project_office po on office.keyid = po.officeid and po.projectid = c.projectid  where " &  Mid(strDELIVERYFilter, 4) & " )>0 "
    End If
    ' end DELIVERY
    ' STRUCTURAL_TYPE filters 
    strSTRUCTURAL_TYPEFilter = ""
    If report_STRUCTURAL_TYPE <> "" and report_STRUCTURAL_TYPE <> "0" then
            arr=Split(report_STRUCTURAL_TYPE, ",")
            For Each item in arr
                if Trim(item) <> "" then
                    strSTRUCTURAL_TYPEFilter = strSTRUCTURAL_TYPEFilter & " OR ltrim(rtrim(st.item_value)) like '%" & Trim(item) & "%' "
                end if
            Next
    End If
    If Trim(strSTRUCTURAL_TYPEFilter) <>  "" then
        strSTRUCTURAL_TYPEtableJOIN = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_tag = 'IDAM_STRUCTURAL') ) st on st.ProjectID = c.projectid "        
       
        strSTRUCTURAL_TYPEFilter =  " AND (" &  Mid(strSTRUCTURAL_TYPEFilter, 4) & " ) "
    End If
    ' end STRUCTURAL_TYPE
    ' OWNER_TYPE filters 
    strOWNER_TYPEFilter = ""
    If report_OWNER_TYPE <> "" and report_OWNER_TYPE <> "0" then
            arr=Split(report_OWNER_TYPE, ",")
            For Each item in arr
                if Trim(item) <> "" then
                    strOWNER_TYPEFilter = strOWNER_TYPEFilter & " OR ltrim(rtrim(ot.item_value)) like '%" & Trim(item) & "%' "
                end if
            Next
    End If
    If Trim(strOWNER_TYPEFilter) <>  "" then
        strOWNER_TYPEtableJOIN = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_tag = 'IDAM_OWNERTYPE') ) ot on ot.ProjectID = c.projectid "        
       
        strOWNER_TYPEFilter =  " AND (" &  Mid(strOWNER_TYPEFilter, 4) & " ) "
    End If
    ' end OWNER_TYPE
    ' LEED filters 
    strLEEDFilter = ""
    If report_LEED <> "" and report_LEED <> "0" then
            arr=Split(report_LEED, ",")
            For Each item in arr
                if Trim(item) <> "" then
                    strLEEDFilter = strLEEDFilter & " OR ltrim(rtrim(LEED.item_value)) like '%" & Trim(item) & "%' "
                end if
            Next
    End If
    If Trim(strLEEDFilter) <>  "" then
        strLEEDtableJOIN = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_tag = 'IDAM_LEED') ) LEED on LEED.ProjectID = c.projectid "        
       
        strLEEDFilter =  " AND (" &  Mid(strLEEDFilter, 4) & " ) "
    End If
    ' end LEED
    ' PROTYPE filters 
    strPROTYPEFilter = ""
    If report_PROTYPE <> "" and report_PROTYPE <> "0" then
            arr=Split(report_PROTYPE, ",")
            For Each item in arr
                if Trim(item) <> "" then
                    strPROTYPEFilter = strPROTYPEFilter & " OR ltrim(rtrim(PROTYPE.item_value)) like '%" & Trim(item) & "%' "
                end if
            Next
    End If
    If Trim(strPROTYPEFilter) <>  "" then
        strPROTYPEtableJOIN = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_tag = 'IDAM_PROTYPE') ) PROTYPE on PROTYPE.ProjectID = c.projectid "        
       
        strPROTYPEFilter =  " AND (" &  Mid(strPROTYPEFilter, 4) & " ) "
    End If
    ' end PROTYPE
    ' STRUCTURAL_ENGINEER filters 
    strSTRUCTURAL_ENGINEERFilter = ""
    If report_STRUCTURAL_ENGINEER <> "" and report_STRUCTURAL_ENGINEER <> "0" then
            arr=Split(report_STRUCTURAL_ENGINEER, ";")
            For Each item in arr
                if Trim(item) <> "" then
                    strSTRUCTURAL_ENGINEERFilter = strSTRUCTURAL_ENGINEERFilter & " OR ltrim(rtrim(se.item_value)) like '%" & Trim(item) & "%' "
                end if
            Next
    End If
    If Trim(strSTRUCTURAL_ENGINEERFilter) <>  "" then
        strSTRUCTURAL_ENGINEERtableJOIN = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_tag = 'IDAM_STRUCT') ) se on se.ProjectID = c.projectid "        
       
        strSTRUCTURAL_ENGINEERFilter =  " AND (" &  Mid(strSTRUCTURAL_ENGINEERFilter, 4) & " ) "
    End If
    ' end STRUCTURAL_ENGINEER
' MECHANICAL_ENGINEER filters 
    strMEPFilter = ""
    If report_MEP <> "" and report_MEP <> "0" then
            arr=Split(report_MEP, ";")
            For Each item in arr
                if Trim(item) <> "" then
                    strMEPFilter = strMEPFilter & " OR ltrim(rtrim(MEP.item_value)) like '%" & Trim(item) & "%' "
                end if
            Next
    End If
    If Trim(strMEPFilter) <>  "" then
        strMEPtableJOIN = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_tag = 'IDAM_MEP') ) MEP on MEP.ProjectID = c.projectid "        
       
        strMEPFilter =  " AND (" &  Mid(strMEPFilter, 4) & " ) "
    End If
    ' end MECHANICAL_ENGINEER
' ELECTRICAL_ENGINEER filters 
    strELEFilter = ""
    If report_ELE <> "" and report_ELE <> "0" then
            arr=Split(report_ELE, ";")
            For Each item in arr
                if Trim(item) <> "" then
                    strELEFilter = strELEFilter & " OR ltrim(rtrim(ELE.item_value)) like '%" & Trim(item) & "%' "
                end if
            Next
    End If
    If Trim(strELEFilter) <>  "" then
        strELEtableJOIN = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_tag = 'IDAM_ELE') ) ELE on ELE.ProjectID = c.projectid "        
       
        strELEFilter =  " AND (" &  Mid(strELEFilter, 4) & " ) "
    End If
    ' end ELECTRICAL_ENGINEER
' DEVELOPER filters 
    strDEVFilter = ""
    If report_DEV <> "" and report_DEV <> "0" then
            arr=Split(report_DEV, ";")
            For Each item in arr
                if Trim(item) <> "" then
                    strDEVFilter = strDEVFilter & " OR ltrim(rtrim(DEV.item_value)) like '%" & Trim(item) & "%' "
                end if
            Next
    End If
    If Trim(strDEVFilter) <>  "" then
        strDEVtableJOIN = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_tag = 'IDAM_DEV') ) DEV on DEV.ProjectID = c.projectid "        
       
        strDEVFilter =  " AND (" &  Mid(strDEVFilter, 4) & " ) "
    End If
    ' end DEVELOPER
    ' CLIENTOWNER filters 
    strCLIENTOWNERFilter = ""
    If report_CLIENTOWNER <> "" and report_CLIENTOWNER <> "0" then
            arr=Split(report_CLIENTOWNER, ";")
            For Each item in arr
                if Trim(item) <> "" then
                    strCLIENTOWNERFilter = strCLIENTOWNERFilter & " OR ltrim(rtrim(fv.item_value)) like '%" & Trim(item) & "%' "
                end if
            Next
    End If
    If Trim(strCLIENTOWNERFilter) <>  "" then
       
        strCLIENTOWNERFilter = " AND (select count(*) from ipm_project_field_Value fv where item_id in (2417351 , 2417361) and fv.projectid = c.projectid and (" &  Mid(strCLIENTOWNERFilter, 4) & ") )>0 "
    End If
    ' end CLIENTOWNER
' ARCHITECT filters 
    strARCHFilter = ""
    If report_ARCH <> "" and report_ARCH <> "0" then
            arr=Split(report_ARCH, ";")
            For Each item in arr
                if Trim(item) <> "" then
                    strARCHFilter = strARCHFilter & " OR ltrim(rtrim(ARCH.item_value)) like '%" & Trim(item) & "%' "
                end if
            Next
    End If
    If Trim(strARCHFilter) <>  "" then
        strARCHtableJOIN = " left outer join (select item_value, projectid from ipm_project_field_value a where  a.Item_ID in (select Item_ID from ipm_project_field_desc sf1 where sf1.item_tag = 'IDAM_ARCH') ) ARCH on ARCH.ProjectID = c.projectid "        
       
        strARCHFilter =  " AND (" &  Mid(strARCHFilter, 4) & " ) "
    End If
    ' end ARCHITECT
            ' here we are only limiting to: lark Mid-Atlantic (and all subcategory folders), Clark Western, Clark Northern, Clark Southern, Shirley, Atkinson, Clark Civil, and Edgemoor
            selectedProjects = " JOIN ( select ProjectID from IPM_PROJECT where Category_ID in (select CATEGORY_ID from IPM_CATEGORY where CATEGORY_ID in (select CATEGORY_ID from IPM_CATEGORY where (PARENT_CAT_ID = 2400016) or CATEGORY_ID in (2400020, 2400018, 2400019, 2400021, 2400022, 2400023, 2474814)) and AVAILABLE = 'Y') and Available = 'Y' ) selectedProjects on c.projectid = selectedProjects.projectid "
            sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description,c.descriptionmedium,url from  ipm_project c " & selectedProjects & strSFtableJOIN & strCOSTtableJOIN & strCOSTtableJOIN_mcv & strCOMPLtableJOIN & strCOMPLtableJOIN_mcd & strCOMPLtableJOIN_scd & strSTRUCTURAL_TYPEtableJOIN & strOWNER_TYPEtableJOIN & strLEEDtableJOIN & strPROTYPEtableJOIN & strSTRUCTURAL_ENGINEERtableJOIN & strMEPtableJOIN & strELEtableJOIN & strDEVtableJOIN & strARCHtableJOIN & " WHERE 1=1  " & strSFMinFilter & strSFMaxFilter & strCOSTMinFilter & strCOSTMaxFilter & strCOMPLMinFilter & strCOMPLMaxFilter & strStateFilter & strDELIVERYFilter & strSTRUCTURAL_TYPEFilter & strOWNER_TYPEFilter & strLEEDFilter & strPROTYPEFilter & strSTRUCTURAL_ENGINEERFilter & strMEPFilter & strELEFilter & strDEVFilter & strCLIENTOWNERFilter & strARCHFilter & " AND  ltrim(rtrim(c.name)) LIKE '%" & report_name & "%'  ORDER BY c.name"
           '   response.write(sql)  
        'DAMJAN response
            'response.write(getSessionID("http://ccgdc4idam001.int.clarkus.com/IDAMBROWSESERVICE/getsessionid.aspx"))
            
            rsProjects.Open sql, Conn, 1, 4
            'Query for additional UDFs
            set rsUDF = Server.CreateObject("adodb.recordset")
            sql = "select * from ipm_project_field_desc where active = 1 order by item_name"
            rsUDF.Open sql, Conn, 1, 4
            'loop through and match checked inputs
            set rsLineUDF = Server.CreateObject("adodb.recordset")
            '  rsLineCaseStudies = Server.CreateObject("adodb.recordset")
            set rsLineKeywords = Server.CreateObject("adodb.recordset")
        End If
    'End If
 %>