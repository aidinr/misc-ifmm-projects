<form style="padding:0;margin:0;" method="get" action="portfolio_search.asp">
<div id="menu">

			<div id="nav">
				<div>
				<a href="overview.asp"><img src="images/menu/menu_overview.jpg" /></a>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" class="navigation_left"></td>
						<td align="left" class="navigation">			    
							 
							 <a href="overview_history.asp">HISTORY</a>
							 <a href="overview_clients.asp">CLIENTS</a>
							 <a href="overview_people.asp">PEOPLE</a>
							 <a href="overview_form.asp">JOB APPLICATION FORM</a>
							 <a href="overview_subcontracting.asp">SUBCONTRACTING</a>
							 <a href="overview_project_types.asp">PROJECT TYPES</a>
							 
						</td>
						<td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
					</tr>
					<tr>
						<td width="10" height="10" class="navigation_bottom_left"></td>
						<td height="10" class="navigation_bottom"></td>
						<td width="8" height="10" class="navigation_bottom_right"></td>
					</tr>
				</table>
				</div>
				
				<div>
				<a href="capabilities.asp"><img src="images/menu/menu_capabilities.jpg" /></a>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" class="navigation_left"></td>
						<td align="left" class="navigation">			    
							 
							 <a href="capabilities_preconstruction.asp">PRECONSTRUCTION SERVICES</a>
							 <a href="capabilities_general_contracting.asp">GENERAL CONTRACTING</a>
							 <a href="capabilities_sustainable.asp">SUSTAINABLE BUILDING</a>
							 <a href="capabilities_public_private.asp">PUBLIC PRIVATE PARTNERSHIP</a>
							 
						</td>
						<td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
					</tr>
					<tr>
						<td width="10" height="10" class="navigation_bottom_left"></td>
						<td height="10" class="navigation_bottom"></td>
						<td width="8" height="10" class="navigation_bottom_right"></td>
					</tr>
				</table>
				</div>
				
				<div>
				<a href="portfolio.asp"><img src="images/menu/menu_portfolio.jpg" /></a>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" class="navigation_left"></td>
						<td align="left" class="navigation">			    
							 
							 <a href="portfolio_cat.asp?cid=<%=DISC_MIX_USE%>">MIXED-USE DEVELOPMENTS</a>
							 <a href="portfolio_cat.asp?cid=<%=DISC_LUX_APT%>">LUXURY APARTMENTS</a>
							 <a href="portfolio_cat.asp?cid=<%=DISC_HIG_DEN%>">HIGH-DENSITY APARTMENTS</a>
							 <a href="portfolio_cat.asp?cid=<%=DISC_MIL_COM%>">MILITARY HOUSING</a>
							 <a href="portfolio_cat.asp?cid=<%=DISC_AFF_HOU%>">AFFORDABLE HOUSING</a>
							 <a href="portfolio_cat.asp?cid=<%=DISC_SEN_USE%>">SENIOR LIVING</a>
							 <a href="portfolio_cat.asp?cid=<%=DISC_MSC_ROH%>">RETAIL/OFFICE/HOSPITALITY</a>
							 
						</td>
						<td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
					</tr>
					<tr>
						<td width="10" height="10" class="navigation_bottom_left"></td>
						<td height="10" class="navigation_bottom"></td>
						<td width="8" height="10" class="navigation_bottom_right"></td>
					</tr>
				</table>
				</div>
				
				<div>
				<a href="news.asp"><img src="images/menu/menu_news.jpg" /></a>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" class="navigation_left"></td>
						<td align="left" class="navigation">			    
							 
							 <a href="news_archives.asp">PRESS RELEASES</a>
							 <a href="awards.asp">AWARDS</a>
							 <a href="rankings.asp">RANKINGS</a>							 
						</td>
						<td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
					</tr>
					<tr>
						<td width="10" height="10" class="navigation_bottom_left"></td>
						<td height="10" class="navigation_bottom"></td>
						<td width="8" height="10" class="navigation_bottom_right"></td>
					</tr>
				</table>
				</div>	
				
				<div>
				<a href="regions.asp"><img src="images/menu/menu_region.jpg" /></a>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" class="navigation_left"></td>
						<td align="left" class="navigation">			    
							 
							 <a href="region.asp?region=ne">NORTHEAST</a>
							 <a href="region.asp?region=nw">NORTHWEST</a>
							 <a href="region.asp?region=se">SOUTHEAST</a>
							 <a href="region.asp?region=sw">SOUTHWEST</a>
							 
						</td>
						<td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
					</tr>
					<tr>
						<td width="10" height="10" class="navigation_bottom_left"></td>
						<td height="10" class="navigation_bottom"></td>
						<td width="8" height="10" class="navigation_bottom_right"></td>
					</tr>
				</table>
				</div>

				
			</div>
			
			<div class="menu_item_search"><form method="get" action="portfolio_search.asp"><input type="text" id="search_projects" name="search" value="SEARCH PROJECTS" onfocus="this.select();this.value=''"/></div>
			
			</div>
</form>
