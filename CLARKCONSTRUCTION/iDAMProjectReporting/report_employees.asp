<!--#include file="includes\config.asp" -->
<%

	'''NOTE: THE DISCIPLINE AND FUNCTIONAL MARKET (OFFICE) TABLES ARE INVERTED DUE TO PAST MISLABELLING

	set rsUDF=server.createobject("adodb.recordset")
	sql = "select * from ipm_user_field_desc where active = 1 order by item_name"
	rsUDF.Open sql, Conn, 1, 4
	
	set rsEmployee = server.createobject("adodb.recordset")
	sql = "select userid, firstname, lastname from ipm_user where active = 'y' and contact = 0 and userid <> 1 and userid <> 21763403 order by firstname,lastname"
	rsEmployee.Open sql, Conn, 1, 4
	
	set rsCompany = server.createobject("adodb.recordset")
	sql = "select distinct agency from ipm_user where active = 'y' and contact = 0 and agency <> '' order by agency"
	rsCompany.Open sql, Conn, 1, 4


%>

<html>
<head>
<title><%=sPageTitle%> Reports Tool</title>
	<!--#include file="includes\header.asp" -->
<script type="text/javascript">
	function toggleFields(fieldsID,indicatorID) {
		theFields = document.getElementById(fieldsID);
		theFieldsIndicator = document.getElementById(indicatorID);
			if(theFields.style.display != "none") {
				theFields.style.display = "none";
				theFieldsIndicator.innerHTML = "-";
			}
			else {
				theFields.style.display = "block";
				theFieldsIndicator.innerHTML = "+";			

			}
	}
	function showEmployee() {
		$("#selectEmployee").css("display","block");
		$("#selectCompany").css("display","none");
	}
	function showCompany() {
		$("#selectEmployee").css("display","none");
		$("#selectCompany").css("display","block");
	}	
	function initSelections() {
	if(document.getElementById('pdf_radio').checked == true) { document.getElementById('pdf_options').style.visibility='visible'; }
	if(document.getElementById('employeeRadio').checked == true) { showEmployee(); }
	if(document.getElementById('companyRadio').checked == true) { showCompany(); }
	if(document.getElementById('radioBasic').checked == true) { $("#detailFields").css("display","none"); }
	if(document.getElementById('radioDetailed').checked == true) { $("#detailFields").css("display","block"); }
	$("#employeeRadio").click(function() {	
		showEmployee();
	});
	$("#companyRadio").click(function() {
		showCompany();
	});
	$("#radioBasic").click(function() {
		$("#detailFields").css("display","none");
	});
	$("#radioDetailed").click(function() {
		$("#detailFields").css("display","block");
	});		
	}
</script>
</head>
<body onload="initSelections();">
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			
			<div id="content">

			<br />
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview" style="width:100%;">
					<div id="content_text">
					Marketing Tools | Generate Employee Report
					<br /><br />
					<!--<img src="images/title_project_reporting.jpg" alt="Project Reporting" />-->
					<h2 style="font-family:Times New Roman;color:#996d50;">Employee Reports</h2>
					<h4>Step 1 - Select Employees</h4>
					<form method="post" action="report_employees_print.asp">

				<table>
					<tr>
						<td>Select by:</td>
						<td><input name="search_filter" type="radio" value="employee" checked="checked" id="employeeRadio" />Employee Name <input name="search_filter" type="radio" value="company" id="companyRadio" /> Company</td>
					</tr>
					<tr>
						<td colspan="2">
						<select style="display:none;" id="selectEmployee" name="search_employee" size="12" multiple="multiple" >
							<%
							do while not rsEmployee.eof
							employeeName = trim(rsEmployee("firstName")) & " " & trim(rsEmployee("lastName"))
							%>
							
							<option value="<%=trim(rsEmployee("userid"))%>"><%=employeeName%></option>
							
							<%rsEmployee.moveNext
							loop							
							
							%>
						</select>						
						<select style="display:none;" id="selectCompany" name="search_company" size="4" multiple="multiple">
							<%
							do while not rsCompany.eof
							companyName = replace(trim(rsCompany("agency")),","," ")
							%>
							
							<option value="<%=companyName%>"><%=companyName%></option>
							
							<%rsCompany.moveNext
							loop							
							
							%>
						</select>						
						</td>
					</tr>					
				</table>


<h4>Step 2 - Select Formatting Options</h4>

<table cellpadding="3" cellspacing="3">
		<tr>
			<td valign="top">Report Name: </td>
			<td><input type="text" name="report_name" /></td>
		</tr>	
					<tr>
						<td valign="top">Output:</td>
						<td> <input name="report_output" type="radio" value="html" checked="checked" onclick="document.getElementById('pdf_options').style.visibility='hidden';" />HTML  <input id="pdf_radio" name="report_output" type="radio" value="pdf" onclick="document.getElementById('pdf_options').style.visibility='visible';" />PDF  <input name="report_output" type="radio" value="csv" onclick="document.getElementById('pdf_options').style.visibility='hidden';" />CSV
						     <div id="pdf_options" style="visibility:hidden;">Page break interval: <input type="text" size="2" name="pdf_page_break" id="pdf_page_break" value="3" /></div>
						</td>
					</tr>
					<tr>
						<td valign="top">Display Type:</td>
						<td><input name="search_display" type="radio" value="basic" checked="checked" id="radioBasic" />Basic <input name="search_display" type="radio" value="detailed" id="radioDetailed" /> Detailed 
						</td>
					</tr>			
		<tr>
			<td colspan="2">
			<table cellpadding="0" id="detailFields" cellspacing="0">
			<tr>
			<td valign="top">Fields to display: </td>
			<td></td>
		</tr>
		<tr>
			<td valign="top">Standard Fields</td>

			<td>
			<input type="checkbox" class="udf" name="image" value="1" checked="checked" /> Image<br />
			<input type="checkbox" class="udf" name="firstname" value="1" checked="checked" /> First Name<br />
			<input type="checkbox" class="udf" name="lastname" value="1" checked="checked" /> Last Name<br />
			<input type="checkbox" class="udf" name="title" value="1" checked="checked" /> Title<br />
			<input type="checkbox" class="udf" name="company" value="1" checked="checked" /> Company<br />
			<input type="checkbox" class="udf" name="bio" value="1" checked="checked" /> Biography<br />
			<input type="checkbox" class="udf" name="education" value="1" checked="checked" /> Education<br />
			</td>
		</tr>
		<tr>
			<td valign="top">Other Fields</td>

			<td>
			<input type="checkbox" class="udf" name="security" value="1" checked="checked" /> Security Level<br />
			<input type="checkbox" class="udf" name="phone" value="1" checked="checked" /> Phone<br />
			<input type="checkbox" class="udf" name="cellphone" value="1" checked="checked" /> Cell Phone<br />
			<input type="checkbox" class="udf" name="email" value="1" checked="checked" /> Email<br />
			</td>
		</tr>		
		<tr>
			<td valign="top"> <big><b><a id="udfFieldsIndicator" href="javascript:toggleFields('udfFields','udfFieldsIndicator');">-</a></b></big> <a href="javascript:toggleFields('udfFields','udfFieldsIndicator');">User-Defined Fields</a></td>
			<td>
			<div id ="udfFields">
			<%
			do while not rsUDF.eof
			itemTag = trim(rsUDF("item_tag"))
			%><input type="checkbox" class="udf" name="<%=itemTag%>" value="1" /><%=trim(rsUDF("item_name"))%><br /><%rsUDF.moveNext
			loop
			
			%>
			</div>
			</td>
		</tr>
			</table>
			
			</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" value="Generate Report" /></td>
		</tr>
<input type="hidden" value="1" name="submit" />
</form>
					
					
					
					
					
					</div>
					
				</td>
				


			</tr>
	
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
