<!--#include file="includes\config.asp" -->
<%
	set rsAwardsTypes=server.createobject("adodb.recordset")
	sql = "select * from ipm_awards_type order by type_id"
	rsAwardsTypes.Open sql, Conn, 1, 4
	
	set rsOrganizationName=server.createobject("adodb.recordset")
	sql = "select distinct item_value from ipm_awards_field_desc a,ipm_awards_field_value b where a.item_tag = 'IDAM_AWARDS_ORGANIZATION_NAME' AND a.item_id = b.item_id and active = 1 and viewable = 1 and item_value <> '' order by item_value"
	rsOrganizationName.Open sql, Conn, 1, 4

	set rsRelatedProjects=server.createobject("adodb.recordset")
	sql = "select distinct ipm_project.projectid,name from ipm_awards_related_projects,ipm_project where ipm_awards_related_projects.projectid = ipm_project.projectid and available = 'y' order by name"
	rsRelatedProjects.Open sql, Conn, 1, 4

	set rsAwardsYears=server.createobject("adodb.recordset")
	sql = "select distinct year(post_date) awardsyears from ipm_awards order by awardsyears desc"
	rsAwardsYears.Open sql, Conn, 1, 4		


%>

<html>
<head>
<title><%=sPageTitle%> Reports Tool</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body onload="if(document.getElementById('pdf_radio').checked == true) { document.getElementById('pdf_options').style.display='block'; }">
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			
			<div id="content">

			<br />
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview" style="width:100%;">
					<div id="content_text">
					Marketing Tools | Generate Awards Report
					<br /><br />
					<img src="images/awards_reporting.jpg" alt="Project Reporting" />
					<h4>Step 1 - Select News</h4>
					<form method="post" action="awards_report_print.asp">
				<table>
					<tr>
						<td>Keywords:</td>
						<td><input type="text" name="keywords" /></td>
					</tr>
					<tr>
						<td>Type:</td>
						<td>	<select name="awards_type">
							<option value="">All</option>
							<%
							do while not rsAwardsTypes.eof
							AwardsType = trim(rsAwardsTypes("type_value"))
							%>
							
							<option value="<%=rsAwardsTypes("type_id")%>"><%=AwardsType%></option>
							
							<%rsAwardsTypes.moveNext
							loop							
							
							%>
							</select>
						</td>
					</tr>					
					<tr>
						<td>Organization Name:</td>
						<td><select name="org_name">
						<option value="">All</option>
							<%
							do while not rsOrganizationName.eof
							organizationName = trim(rsOrganizationName("item_value"))
							%>
							<option value="<%=organizationName%>"><%=organizationName%></option>
							<%rsOrganizationName.moveNext
							loop							
							%>
							</select></td>
					</tr>
<tr>
						<td>Project:</td>
						<td>
						<select name="related_project">
						<option value="">All</option>
							<%
							do while not rsRelatedProjects.eof
							projectName = trim(rsRelatedProjects("name"))
							%>
							<option value="<%=rsRelatedProjects("projectID")%>"><%=projectName%></option>
							<%rsRelatedProjects.moveNext
							loop							
							
							%>
						</select>
</td>
					</tr>
					<tr>
						<td>Year:</td>
						<td>
						<select name="awards_years">
						<option value="">All</option>
							<%
							do while not rsAwardsYears.eof
							%>
							<option value="<%=rsAwardsYears("awardsyears")%>"><%=rsAwardsYears("awardsyears")%></option>
							<%rsAwardsYears.moveNext
							loop							
							
							%>
						</select>
</td>
					</tr>						
				</table>


<h4>Step 2 - Select Formatting Options</h4>

<table cellpadding="3" cellspacing="3">
		<tr>
			<td valign="top">Report Name: </td>
			<td><input type="text" name="report_name" /></td>
		</tr>	
					<tr>
						<td valign="top">Output:</td>
						<td> <input name="search_output" type="radio" value="html" checked="checked" onclick="document.getElementById('pdf_options').style.display='none';" />HTML  <input id="pdf_radio" name="search_output" type="radio" value="pdf" onclick="document.getElementById('pdf_options').style.display='block';" />PDF  <input name="search_output" type="radio" value="csv" onclick="document.getElementById('pdf_options').style.display='none';" />CSV
						     <div id="pdf_options" style="display:none;">Page break interval: <input type="text" size="2" name="pdf_page_break" id="pdf_page_break" value="3" /></div>
						</td>
					</tr>		
		<tr>
			<td valign="top">Display Type </td>
			<td><input name="search_display" type="radio" value="basic" checked="checked"" />Basic  <input id="pdf_radio" name="search_display" type="radio" value="detailed" />Detailed</td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Generate Report" /></td>
		</tr>
<input type="hidden" value="1" name="submit" />
</form>
					
					
					
					
					
					</div>
					
				</td>
				


			</tr>
	
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
