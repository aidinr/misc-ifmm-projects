<!--#include file="includes\config.asp" -->
<%
'''NOTE: THE DISCIPLINE AND FUNCTIONAL MARKET (OFFICE) TABLES ARE INVERTED DUE TO PAST MISLABELLING
if request.Querystring("submit") = "1" then
%>
<!--#include file="includes\report_main_pdf_get.asp" -->
<% 'if (report_output = "html") then %>
<html>
<head>
<title>
Report
</title>
<style type="text/css">
td,th {
border: 1px solid transparent;
padding: 20px;
text-align: left;
vertical-align: top;
font-family: Myriad Pro;
font-size: 13px;
line-height: 19px;
color: #4d4d4d
}
table {
border:1px solid transparent;
border-collapse: collapse;
padding:10px;
}
th {
background: #0076be;
}
</style>
</head>
<body style="margin: 75px">
<!--begin table heading-->
<!--end table heading-->
<!--Begin results listing-->
<%
do while not rsProjects.eof%>
<table width="650px">
<tr>
<td colspan="2" style="text-align: left;  " align="left" valign="top"><font style="font-size:19px" color='#004b8d' ><%=UCase(rsProjects("Name"))%></font>
<br/><%=rsProjects("city") & ", " & rsProjects("state_id") %>
<br/>
<br/>
<img width="650" height="235" src="<%=sAssetPath%><%=rsProjects("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=800&height=300&qfactor=25" alt="<%=rsProjects("Name")%>" /></td>
</td>
</tr>
<tr> <td style="width:200px; text-align: left;  " align="left" valign="top">
<font color="#004b8d">Square Footage</font> &nbsp;&nbsp;
<br/>
<%
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_GLA'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
        Response.Write("" & FormatNumber(rsLineUDF("item_value"), 0) & "")
else
response.write "&nbsp;"
end if
%>
<br/>
<br/>
<font color="#004b8d">Contract Value</font> &nbsp;&nbsp;
<br/>
<%
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_JDE_FinalContractValue'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "" & rsLineUDF("item_value") & ""
else
response.write "&nbsp;"
end if
%>
<br/>
<br/>
<font color="#004b8d">Contract Start Date</font> &nbsp;&nbsp;
<%
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select case when ltrim(rtrim(isnull(item_value, '')))  <> '' then YEAR(convert(datetime, item_value)) + '' else '' end item_value from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_JDE_ActualStartDate'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "" & rsLineUDF("item_value") & ""
else
response.write "&nbsp;"
end if
%>
<br/>
<br />
<font color="#004b8d">Contract Finish Date</font> &nbsp;&nbsp;
<%
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select case when ltrim(rtrim(isnull(item_value, '')))  <> '' then YEAR(convert(datetime, item_value)) + '' else '' end item_value from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_JDE_ActualCompletionDate'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "" & rsLineUDF("item_value") & ""
else
response.write "&nbsp;"
end if
%>
<br/>
<br/>
<font color="#004b8d">Owner</font> &nbsp;&nbsp;
<br/>
<%
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_OWNER'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "" & rsLineUDF("item_value") & ""
else
response.write "&nbsp;"
end if
%>
<br/>
<br/>
<font color="#004b8d">Architect</font> &nbsp;&nbsp;
<br/>
<%
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_ARCH'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "" & rsLineUDF("item_value") & ""
else
response.write "&nbsp;"
end if
%>
<br/>
<br/>
<font color="#004b8d">Role of Firm</font> &nbsp;&nbsp;
<br/>
<%
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
    sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID") & " AND a.item_id = b.item_id AND item_tag = 'IDAM_SERVICE'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "" & rsLineUDF("item_value") & ""
else
response.write "&nbsp;"
end if
%>
<br/>
<br/>
</td>
<td style="text-align: justify;  text-justify: newspaper; text-align-last: Left; width:600px; " align="left" valign="top">
<%
'''Begin Standard
'''End Standard
''''Begin UDFs
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_DESC'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "" & rsLineUDF("item_value") & ""
else
response.write "&nbsp;"
end if
   Response.Write("</td></tr>")
    rsProjects.moveNext()
%>
    </table>
<div style="page-break-before:always">&nbsp;</div> 
<br />
<br />
<%
Loop
%>
<!--End results listing-->
</body>
</html>