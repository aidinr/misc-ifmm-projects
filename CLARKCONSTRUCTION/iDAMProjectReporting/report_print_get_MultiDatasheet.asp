<!--#include file="includes\config.asp" -->
<%
'''NOTE: THE DISCIPLINE AND FUNCTIONAL MARKET (OFFICE) TABLES ARE INVERTED DUE TO PAST MISLABELLING
if request.QueryString("submit") = "1" then
%>
<!--#include file="includes\report_main_pdf_get.asp" -->
<%'if (report_output = "html") then %>
<html>
<head>
<title>
Report
</title>
<style type="text/css">
td,th {
border: 1px solid gray ;
padding: 3px;
text-align: left;
vertical-align: top;
font-family: Myriad Pro;
font-size: 12px;
line-height: 16px;
color: #4d4d4d
}
table {
border:1px solid gray;
border-collapse: collapse;
padding:10px;
}
th {
background: #0076be;
}
</style>
</head>
<body>
<table style="width:100%; border: 1px solid transparent; "  >
<tr>
<td align="left" style="width:50%; text-align: left; border: 1px solid transparent;">
<font style="font-size:16px" color='#004b8d' ><%=UCASE(report_title)%> </font>
</td>
<td align="right" style="width:50%; text-align: right; border: 1px solid transparent;" >
<% If Request("search_display_logo") = "Clark" then%>
<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Atkinson" then %>
<img src="images/atkinson_logo.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Edgemoor" then %>
<img src="images/edgemoor_logo.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Shirley" then %>
<img src="images/shirley_logo.jpg" alt="Clark Logo" />
<%end if %></td>
</tr>
</table>
<br/>
<table width="100%">
<!--begin table heading-->
<tr>
<th > <b><font  color="white"> PROJECT NAME <br/> & LOCATION</font></b></th>
<th><b><font color="white">PROJECT DESCRIPTION</font></b></th>
<th><b><font color="white">SERVICE <br/>TYPE</font></b></th>
<th><b><font color="white">SQUARE<br/>FOOTAGE</font></b></th>
<th><b><font color="white">CONTRACT <br/>VALUE</font></b></th>
<th><b><font color="white">COMPLETION<br/>DATE</font></b></th>
</tr>
<!--end table heading-->
<!--Begin results listing-->
<%
 countii = 0
do while not rsProjects.eof%>
<%  if countii<>0 then %>
    <tr>
    <th colspan="6" style="background: #e3e9f5; "><b>&nbsp</b></th>
    </tr>
    <% 
    End If
    countii = countii+1
 %>
<tr>
<td align="left"  valign="top"><table style=" width:100%; border: 1px solid transparent;"><tr><td style="width: 152px; border: 1px solid transparent;"><img width="150"  src="<%=sAssetPath%><%=rsProjects("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=150&height=118&qfactor=25" alt="<%=rsProjects("Name")%>" /></td><td style="text-align: left; border: 1px solid transparent;" align="left" valign="top"><font color='#004b8d' ><%=UCase(rsProjects("Name"))%></font><br/><%=rsProjects("city") & ", " & rsProjects("state_id") %></td></tr></table></td>
<%
'''Begin Standard
'''End Standard
''''Begin UDFs
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_RESUME'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "<td style=""text-align: justify;  text-justify: newspaper; text-align-last: Left"">" & rsLineUDF("item_value") & "</td>"
else
response.write "<td>&nbsp;</td>"
end if
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_SERVICE'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "<td>" & rsLineUDF("item_value") & "</td>"
else
response.write "<td>&nbsp;</td>"
end if
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_GLA'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
        Response.Write("<td>" & FormatNumber(rsLineUDF("item_value"), 0) & "</td>")
else
response.write "<td>&nbsp;</td>"
end if
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_JDE_FinalContractValue'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "<td>" & rsLineUDF("item_value") & "</td>"
else
response.write "<td>&nbsp;</td>"
end if
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select case when ltrim(rtrim(isnull(item_value,''))) = '' then '' else convert(varchar(10),  convert(datetime, item_value), 101) + ''  end item_value from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_JDE_ActualCompletionDate'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
response.write "<td>&nbsp;</td>"
else
response.write "<td>" & rsLineUDF("item_value") & "</td>"
end if
else
response.write "<td>&nbsp;</td>"
end if
''''End Keywords
''''Begin Case Studies
   '''End Case Studies
   Response.Write("</tr>")
   rsProjects.moveNext()
i = i +1
if(i mod report_page_break = 0 and not rsProjects.eof) then %>
    <tr>
    <th colspan="6" style="background: #e3e9f5; "><b>&nbsp</b></th>
    </tr>
</table>
<div style="page-break-before:always">&nbsp;</div> 
<table style="width:100%; border: 1px solid transparent; "  >
<tr>
<td align="left" style="width:50%; text-align: left; border: 1px solid transparent;">
<font style="font-size:16px" color='#004b8d' ><%=UCASE(report_title)%> </font>
</td>
<td align="right" style="width:50%; text-align: right; border: 1px solid transparent;" >
<% If Request("search_display_logo") = "Clark" then%>
<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Atkinson" then %>
<img src="images/atkinson_logo.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Edgemoor" then %>
<img src="images/edgemoor_logo.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Shirley" then %>
<img src="images/shirley_logo.jpg" alt="Clark Logo" />
<%end if %></td>
</tr>
</table>
<br/>
<table width="100%">
<!--begin table heading-->
<tr>
<th > <b><font  color="white"> PROJECT NAME <br/> & LOCATION</font></b></th>
<th><b><font color="white">PROJECT DESCRIPTION</font></b></th>
<th><b><font color="white">SERVICE <br/>TYPE</font></b></th>
<th><b><font color="white">SQUARE<br/>FOOTAGE</font></b></th>
<th><b><font color="white">CONTRACT <br/>VALUE</font></b></th>
<th><b><font color="white">COMPLETION<br/>DATE</font></b></th>
</tr>
<!--end table heading-->
<!--Begin results listing-->
<%
 countii = 0
end if
Loop
%>
<!--End results listing-->
</table>
</body>
</html>