<!--#include file="includes\config.asp" -->
<%
if Request.Querystring("submit") = "1" then
	
	'Pre-set parameters
	report_title = Request.Querystring("report_name")
	
	if(report_title = "") then
		report_title = "Report"
	end if
	
	theEmployees = prep_sql(Request.Querystring("search_employee"))
	theCompanies = "'" & replace(prep_sql(Request.Querystring("search_company")),",","','") & "'"

	
	if(Request.Querystring("search_filter") = "employee") then 'search by companies
		sql = "select * from ipm_user where userID in (" & theEmployees & ") and active = 'y' order by lastname"
	elseif(Request.Querystring("search_filter") = "company") then 'search by employees
		sql = "select * from ipm_user where agency in ('') and active = 'y' and userid <> 1  order by lastname"
	end if
	set rsEmployees = server.createobject("adodb.recordset")
	rsEmployees.Open sql, Conn, 1, 4
	
	
	'Query for additional UDFs
	
	
	set rsUDF=server.createobject("adodb.recordset")
	sql = "select * from ipm_user_field_desc where active = 1 order by item_name"
	rsUDF.Open sql, Conn, 1, 4
	'loop through and match checked inputs

	if(Request.Querystring("search_display")="basic") then 'basic
	display_firstname = 1
	display_lastname = 1
	display_image = 1
	display_title = 1
	display_company = 1
	display_bio = 1
	display_education = 1
	elseif(Request.Querystring("search_display")="detailed") then 'detailed
	display_firstname = Request.Querystring("firstname")
	display_lastname = Request.Querystring("lastname")
	display_image = Request.Querystring("image")
	display_title = Request.Querystring("title")
	display_company = Request.Querystring("company")
	display_bio = Request.Querystring("bio")
	display_education = Request.Querystring("education")
	
	display_security = Request.Querystring("security")
	display_phone = Request.Querystring("phone")
	display_cellphone = Request.Querystring("cellphone")
	display_email = Request.Querystring("email")
	end if
	
	report_output = Request.Querystring("report_output")
	
	end if
%>
<html>
<head>
<title>
Report
</title>
<style type="text/css">
body {
	font-size:10px;
}
td,th {
	
	border: 1px solid #777777;
	padding: 3px;
	text-align: center;
	vertical-align: top;
	font-size:10px;
}
table {
	border:1px solid #777777;
	border-collapse: collapse;
	padding:3px;
}
</style>
</head>
<html>
<head>
<title>
Report
</title>
<style type="text/css">
td,th {
	
	border: 1px solid #777777;
	padding: 3px;
	text-align: center;
	vertical-align: top;
	font-family: verdana;
	font-size: 12px;
}
table {
	border:1px solid #777777;
	border-collapse: collapse;
	padding:3px;
}
th {
	background: #DDDDDD;
}
</style>
</head>
<body>
	<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
	<h2><%=report_title%></h2>
	<table width="100%">
		<!--begin table heading-->
		<tr>
			<%
			if(display_image = 1) then
			response.write "<th><b>Image</b></th>"
			end if
			if(display_firstname = 1) then
			response.write "<th><b>First Name</b></th>"
			end if
			if (display_lastname = 1) then
			response.write "<th><b>Last Name</b></th>"
			end if
			if (display_title = 1) then
			response.write "<th><b>Title</b></th>"
			end if
			if (display_company = 1) then
			response.write "<th><b>Company</b></th>"
			end if
			if (display_bio = 1) then
			response.write "<th><b>Biography</b></th>"
			end if				
			if (display_education = 1) then
			response.write "<th><b>Education</b></th>"
			end if
			if (display_security = 1) then
			response.write "<th><b>Security Level</b></th>"
			end if
			if (display_phone = 1) then
			response.write "<th><b>Phone</b></th>"
			end if
			if (display_cellphone = 1) then
			response.write "<th><b>Cellphone</b></th>"
			end if
			if (display_email = 1) then
			response.write "<th><b>E-mail</b></th>"
			end if			
			
			do while not rsUDF.eof
			itemTag = trim(rsUDF("item_tag"))
			if(request.querystring(itemTag) = "1") then
			response.write "<th><b>" & rsUDF("item_name") & "</b></th>"
			end if
			
			rsUDF.moveNext
			loop
			
			set rsLineUDF=server.createobject("adodb.recordset")
			%>
		</tr>
		<!--end table heading-->
		<!--Begin results listing-->
		
			
			<%do while not rsEmployees.eof%>
			<tr>
			<%
			'''Begin Standard
			if(display_image = 1) then%>
			 <td align="center" valign="top"><img width="150" height="118" src="<%=sAssetPath%><%=rsEmployees("userID")%>&Instance=<%=siDAMInstance%>&type=contact&size=1&width=300&height=236&crop=1" alt="<%=rsEmployees("lastname")%>" /></td>
			<%
			end if
			if(display_firstname = 1) then
			response.write "<td>" & rsEmployees("firstname") & "</td>"
			end if
			if (display_lastname = 1) then
			response.write "<td>" & rsEmployees("lastname") & "</td>"
			end if
			if (display_title = 1) then
			response.write "<td>" & rsEmployees("position") & "</td>"
			end if
			if (display_company = 1) then
			response.write "<td>" & rsEmployees("agency") & "</td>"
			end if
			if (display_bio = 1) then
			response.write "<td>" & rsEmployees("bio") & "</td>"
			end if				
			if (display_education = 1) then
			response.write "<td>" & rsEmployees("education") & "</td>"
			end if
			if (display_security = 1) then
			if(rsEmployees("securitylevel_id") = "0") then
			employeeSecurity = "Administrator"
			elseif(rsEmployees("securitylevel_id") = "1") then
			employeeSecurity = "Internal Use"
			elseif(rsEmployees("securitylevel_id") = "2") then
			employeeSecurity = "Client Use"
			elseif(rsEmployees("securitylevel_id") = "3") then
			employeeSecurity = "Public"
			else
			employeeSecurity = "N/A"
			end if
			response.write "<td>" & employeeSecurity & "</td>"
			end if
			if (display_phone = 1) then
			response.write "<td>" & rsEmployees("phone") & "</td>"
			end if
			if (display_cellphone = 1) then
			response.write "<td>" & rsEmployees("fax") & "</td>"
			end if
			if (display_email = 1) then
			response.write "<td>" & rsEmployees("email") & "</td>"
			end if			
			'''End Standard
			
			''''Begin UDFs
			
			rsUDF.moveFirst
			
			do while not rsUDF.eof
			itemTag = trim(rsUDF("item_tag"))
			
			if(request.querystring(itemTag) = "1") then
				
				if(rsLineUDF.State = 1) then
				rsLineUDF.close
				end if
				sql = "select * from ipm_user_field_value a, ipm_user_field_desc b WHERE user_id = " & rsEmployees("userID")  & " AND a.item_id = b.item_id AND item_tag = '" & rsUDF("item_tag") & "'"
				rsLineUDF.Open sql, Conn, 1, 4
			
			if(rsLineUDF.recordCount > 0) then
			if(rsLineUDF("item_value") = "") then
			rsLineUDF("item_value") = "&nbsp;"
			end if
			response.write "<td>" & rsLineUDF("item_value") & "</td>"
			else
			response.write "<td>&nbsp;</td>"
			end if
			
			end if
			
			rsUDF.moveNext
			loop
			''''End UDFs
			response.write "</tr>"
			rsEmployees.moveNext
			loop
			%>
		<!--End results listing-->
	</table>
</body>
</html>
