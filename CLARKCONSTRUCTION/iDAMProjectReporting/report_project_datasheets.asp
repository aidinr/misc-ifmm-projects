<!--#include file="includes\config.asp" -->
<%
	set rsClient = server.createobject("adodb.recordset")
	sql = "select * from ipm_client where active = 1 order by name"
	rsClient.Open sql, Conn, 1, 4
	
	set rsDisc = server.createobject("adodb.recordset")
	sql = "select * from ipm_discipline where keyuse = 1 order by keyname"
	rsDisc.Open sql, Conn, 1, 4

%>

<html>
<head>
<title><%=sPageTitle%> Reports Tool</title>
	<!--#include file="includes\header.asp" -->
	
<script type="text/javascript">
<!--
	function selectAll(theID) {
	
		theObject = document.getElementById(theID);
		
		for (i=theObject.length-1; i>=0; i--) {
		theObject.options[i].selected = true;
		} 
	
	}
	function deselectAll(theID) {
	
		theObject = document.getElementById(theID);
		
		for (i=theObject.length-1; i>=0; i--) {
		theObject.options[i].selected = false;
		} 
	
	}

//-->
</script>
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			
			<div id="content">

			<br />
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview" style="width:100%;">
					<div id="content_text">
					Marketing Tools | Project Datasheet
					<br /><br />
					<img src="images/title_project_datasheet.jpg" alt="Project Reporting" />
					<h4>Step 1 - Select Projects</h4>
					<form method="post" action="datasheet_print_html.asp">
	<table cellpadding="3" cellspacing="3">
			<tr>
				<td>Conditions: </td>
				<td></td>
			</tr>
			<tr>
			<td valign="top"></td>
			<td valign="top">
				<table>
					<tr>
						<td valign="top">Project Name Contains:</td>
						<td><input type="text" name="n" /></td>
					</tr>
					<tr>
						<td valign="top">Project Client: <!--<span style="color:red">*</span>--></td>
						<td>	
						<a href="javascript:selectAll('client');">[Select All Clients]</a> &nbsp;  <a href="javascript:deselectAll('client');">[Deselect All Clients]</a>
						<br />
						<select id="client" name="c" MULTIPLE size="4">
							<%
							do while not rsClient.eof
							clientName = trim(rsClient("Name"))
							%>
							
							<option value="<%=rsClient("ClientID")%>"><%=clientName%></option>
							
							<%rsClient.moveNext
							loop							
							
							%>
							</select>
							<br /><br />
							
						</td>
					</tr>					
					<tr>
						<td valign="top">Project Type: <!--<span style="color:red">*</span>--></td>
						<td>
						<a href="javascript:selectAll('type');">[Select All Project Types]</a> &nbsp;  <a href="javascript:deselectAll('type');">[Deselect All Project Types]</a>
						<br />
						<select id="type" name="d" MULTIPLE size="4">
							<%
							do while not rsDisc.eof
							keyName = trim(rsDisc("keyName"))
							%>
							
							<option value="<%=rsDisc("KeyID")%>"><%=keyName%></option>
							
							<%rsDisc.moveNext
							loop							
							
							%>
							</select>
							<br /><br />
							</td>
					</tr>
<tr>
						<td valign="top">Favorite Projects:</td>
						<td>
						<input name="fav" type="radio" checked="checked" value="" /> All  <input type="radio" name="fav" value="1" /> Favorites  <input type="radio" name="fav" value="0" />Non-Favorites
</td>
					</tr>
					<tr>
						<td valign="top">Published Projects:</td>
						<td>
						<input type="radio" name="pub" checked="checked" value=""/> All  <input type="radio" name="pub" value="1" /> Published  <input type="radio" name="pub" value="0" />Non-Published
</td>
					</tr>						
					<tr>
						<td valign="top">Project Region: <!--<span style="color:red">*</span>--></td>
						<td>
						 <a href="javascript:selectAll('region');">[Select All Regions]</a> &nbsp; <a href="javascript:deselectAll('region');">[Deselect All Regions]</a>
						 <br />
						<select id="region" name="r" MULTIPLE size="4">
							<option value="Northeast">Northeast</option>
							<option value="Northwest">Northwest</option>
							<option value="Southeast">Southeast</option>
							<option value="Southwest">Southwest</option>
						    </select>
						    <br /><br />
						   
						</td>
					</tr>
					<tr>
						<td valign="top">Project State: <!--<span style="color:red">*</span>--></td>
						<td>
						<a href="javascript:selectAll('state');">[Select All States]</a> &nbsp; <a href="javascript:deselectAll('state');">[Deselect All States]</a>
						<br />
						<select id="state" name="s" MULTIPLE size="4">
								
								<option value="AK">AK</option>
								<option value="AL">AL</option>
								<option value="AR">AR</option>
								<option value="AZ">AZ</option>

								<option value="CA">CA</option>
								<option value="CO">CO</option>
								<option value="CT">CT</option>
								<option value="DC">DC</option>
								<option value="DE">DE</option>
								<option value="FL">FL</option>

								<option value="GA">GA</option>
								<option value="HI">HI</option>
								<option value="IA">IA</option>
								<option value="ID">ID</option>
								<option value="IL">IL</option>
								<option value="IN">IN</option>

								<option value="KS">KS</option>
								<option value="KY">KY</option>
								<option value="LA">LA</option>
								<option value="MA">MA</option>
								<option value="MD">MD</option>
								<option value="ME">ME</option>

								<option value="MI">MI</option>
								<option value="MN">MN</option>
								<option value="MO">MO</option>
								<option value="MS">MS</option>
								<option value="MT">MT</option>
								<option value="NC">NC</option>

								<option value="ND">ND</option>
								<option value="NE">NE</option>
								<option value="NH">NH</option>
								<option value="NJ">NJ</option>
								<option value="NM">NM</option>
								<option value="NV">NV</option>

								<option value="NY">NY</option>
								<option value="OH">OH</option>
								<option value="OK">OK</option>
								<option value="OR">OR</option>
								<option value="PA">PA</option>
								<option value="RI">RI</option>

								<option value="SC">SC</option>
								<option value="SD">SD</option>
								<option value="TN">TN</option>
								<option value="TX">TX</option>
								<option value="UT">UT</option>
								<option value="VA">VA</option>

								<option value="VT">VT</option>
								<option value="WA">WA</option>
								<option value="WI">WI</option>
								<option value="WV">WV</option>
								<option value="WY">WY</option>
							</select>
							</td>
					</tr>
	
				
					
				</table>
			</td>
		</tr>
</table>

<h4>Step 2 - Select Formatting Options</h4>

<table cellpadding="3" cellspacing="3">
		<tr>
			<td valign="top"><input type="checkbox" name="head" value="1" /> </td>
			<td>Show Letterhead</td>
		</tr>
		<tr>
				<td valign="top"><input type="checkbox" name="custom" value="1" /></td>
				<td>Use Custom Logo &nbsp; &nbsp; 
					<select name="report_logo">
					<option>Clark Builders Group</option>
					<option>Clark Realty</option>
					<option>Clark Realty Capital</option>
					<option>Clark Realty Management</option>
					<option>Duxbury Financial LLC</option>
					</select>
				</td>
		</tr>	
		
					<tr>
						<td valign="top"><input type="checkbox" name="title" value="1" /></td>
						<td>  Use Custom Report Title &nbsp; &nbsp;  <input type="text" name="report_name" /> 
						</td>
					</tr>
<tr>
						<td valign="top"><input type="checkbox" name="showconstructionvalue" value="1" /></td>
						<td>  Show Construction Contract Value &nbsp; &nbsp; 
						</td>
					</tr>
					
		<tr>
			<td valign="top">	<select name="head_x">
					<option value="121">-51</option>	
					<option value="118">-48</option>
					<option value="115">-45</option>
					<option value="112">-42</option>
					<option value="109">-39</option>
					<option value="106">-36</option>
					<option value="103">-33</option>
					<option value="100">-30</option>
					<option value="97">-27</option>
					<option value="94">-24</option>
					<option value="91">-21</option>
					<option value="88">-18</option>
					<option value="85">-15</option>
					<option value="82">-12</option>
					<option value="79">-9</option>
					<option value="76">-6</option>
					<option value="73">-3</option>
					<option selected="selected" value="70">0</option>
					<option value="67">+3</option>
					<option value="64">+9</option>
					<option value="61">+12</option>
					<option value="58">+15</option>
					<option value="55">+18</option>
					<option value="52">+21</option>
					<option value="49">+24</option>
					<option value="46">+27</option>
					<option value="43">+30</option>
				</select></td>
			<td>Adjust Title's X Position</td>
		</tr>	
		<tr>
			<td valign="top">	<select name="head_y">
					<option value="-15">-51</option>	
					<option value="-12">-48</option>
					<option value="-9">-45</option>
					<option value="-6">-42</option>
					<option value="-3">-39</option>
					<option value="0">-36</option>
					<option value="3">-33</option>
					<option value="6">-30</option>
					<option value="9">-27</option>
					<option value="12">-24</option>
					<option value="15">-21</option>
					<option value="18">-18</option>
					<option value="21">-15</option>
					<option value="24">-12</option>
					<option value="27">-9</option>
					<option value="30">-6</option>
					<option value="33">-3</option>
					<option selected="selected" value="36">0</option>
					<option value="39">+3</option>
					<option value="42">+9</option>
					<option value="45">+12</option>
					<option value="48">+15</option>
					<option value="51">+18</option>
					<option value="54">+21</option>
					<option value="57">+24</option>
					<option value="60">+27</option>
					<option value="63">+30</option>
				</select></td>
			<td>Adjust Title's Y Position</td>
		</tr>		
		<!--<tr>
			<td><span style="color:red">*</span></td>
			<td>Required field</td>
		</tr>
		-->		
		<tr>
			<td></td>
			<td><br /><br /><input type="submit" value="Generate Report" /></td>
		</tr>		
		<input type="hidden" value="1" name="submit" />
		</table>
		</form>
					
					
					
					
					
					</div>
					
				</td>
				


			</tr>
	
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
