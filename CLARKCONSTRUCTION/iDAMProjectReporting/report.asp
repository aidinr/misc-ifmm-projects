<!--#include file="includes\config.asp" -->
<%
    '''NOTE: THE DISCIPLINE AND FUNCTIONAL MARKET (OFFICE) TABLES ARE INVERTED DUE TO PAST MISLABELLING
    set rsUDF=server.createobject("adodb.recordset")
    sql = "select * from ipm_project_field_desc where active = 1 order by item_name"
    rsUDF.Open sql, Conn, 1, 4
    set rsArchitect=server.createobject("adodb.recordset")
    sql = "select stuff( (select '; ' + replace(item_value, '&amp;', '&')    from (        select distinct ltrim(rtrim(isnull(item_value,'')))  item_value            from IPM_PROJECT_FIELD_VALUE         where Item_ID =d.item_id    ) t    order by ltrim(rtrim(item_value))    FOR XML PATH('')    ),    1,1,'') Default_values, Item_id, Item_Name from IPM_PROJECT_FIELD_DESC d where d.Item_ID= 2417354"
	
rsArchitect.Open sql, Conn, 1, 4
    set rsClientOwnerCompanies=server.createobject("adodb.recordset")
    sql = "select stuff( (select '; ' + replace(item_value, '&amp;', '&')    from (        select distinct ltrim(rtrim(isnull(item_value,'')))  item_value            from IPM_PROJECT_FIELD_VALUE         where Item_ID in (2417351 , 2417361)    ) t    order by ltrim(rtrim(item_value))    FOR XML PATH('')    ),    1,1,'') Default_values, Item_id, 'Client/Owner Companies' as Item_Name from IPM_PROJECT_FIELD_DESC d where d.Item_ID = 2417351"
    rsClientOwnerCompanies.Open sql, Conn, 1, 4
    set rsDeveloper=server.createobject("adodb.recordset")
    sql = "select stuff( (select '; ' + replace(item_value, '&amp;', '&')    from (        select distinct ltrim(rtrim(isnull(item_value,'')))  item_value            from IPM_PROJECT_FIELD_VALUE         where Item_ID = d.item_id    ) t    order by ltrim(rtrim(item_value))    FOR XML PATH('')    ),    1,1,'') Default_values, Item_id,  Item_Name from IPM_PROJECT_FIELD_DESC d where d.Item_ID = 2417357"
    rsDeveloper.Open sql, Conn, 1, 4
    set rsElectrical=server.createobject("adodb.recordset")
    sql = "select stuff( (select '; ' + replace(item_value, '&amp;', '&')    from (        select distinct ltrim(rtrim(isnull(item_value,'')))  item_value            from IPM_PROJECT_FIELD_VALUE         where Item_ID = d.item_id    ) t    order by ltrim(rtrim(item_value))    FOR XML PATH('')    ),    1,1,'') Default_values, Item_id,  Item_Name from IPM_PROJECT_FIELD_DESC d where d.Item_ID = 2417358"
    rsElectrical.Open sql, Conn, 1, 4
    set rsMechanical=server.createobject("adodb.recordset")
    sql = "select stuff( (select '; ' + replace(item_value, '&amp;', '&')    from (        select distinct ltrim(rtrim(isnull(item_value,'')))  item_value            from IPM_PROJECT_FIELD_VALUE         where Item_ID = d.item_id    ) t    order by ltrim(rtrim(item_value))    FOR XML PATH('')    ),    1,1,'') Default_values, Item_id,  Item_Name from IPM_PROJECT_FIELD_DESC d where d.Item_ID = 2417359"
    rsMechanical.Open sql, Conn, 1, 4
    
    set rsStructural=server.createobject("adodb.recordset")
    sql = "select stuff( (select '; ' + replace(item_value, '&amp;', '&')    from (        select distinct ltrim(rtrim(isnull(item_value,'')))  item_value            from IPM_PROJECT_FIELD_VALUE         where Item_ID = d.item_id    ) t    order by ltrim(rtrim(item_value))    FOR XML PATH('')    ),    1,1,'') Default_values, Item_id,  Item_Name from IPM_PROJECT_FIELD_DESC d where d.Item_ID = 2417369"
    rsStructural.Open sql, Conn, 1, 4
    
    set rsLEED=server.createobject("adodb.recordset")
    sql = "select stuff( (select '; ' + replace(item_value, '&amp;', '&')    from (        select distinct ltrim(rtrim(isnull(item_value,'')))  item_value            from IPM_PROJECT_FIELD_VALUE         where Item_ID =d.item_id    ) t    order by ltrim(item_value)    FOR XML PATH('')    ),    1,1,'') Default_values, Item_id, Item_Name from IPM_PROJECT_FIELD_DESC d where d.Item_ID= 2417353"
    rsLEED.Open sql, Conn, 1, 4
    set rsOwnerType=server.createobject("adodb.recordset")
    sql = "select stuff( (select '; ' + replace(item_value, '&amp;', '&')    from (        select distinct ltrim(rtrim(isnull(item_value,'')))  item_value            from IPM_PROJECT_FIELD_VALUE         where Item_ID =d.item_id    ) t    order by ltrim(item_value)    FOR XML PATH('')    ),    1,1,'') Default_values, Item_id, Item_Name from IPM_PROJECT_FIELD_DESC d where d.Item_ID= 2417362"
    rsOwnerType.Open sql, Conn, 1, 4
    set rsStructuralType=server.createobject("adodb.recordset")
    sql = "select stuff( (select '; ' + replace(item_value, '&amp;', '&')    from (        select distinct ltrim(rtrim(isnull(item_value,'')))  item_value            from IPM_PROJECT_FIELD_VALUE         where Item_ID =d.item_id    ) t    order by ltrim(item_value)    FOR XML PATH('')    ),    1,1,'') Default_values, Item_id, Item_Name from IPM_PROJECT_FIELD_DESC d where d.Item_ID= 2417370"
    rsStructuralType.Open sql, Conn, 1, 4
    set rsProjectType=server.createobject("adodb.recordset")
    sql = "select stuff( (select '; ' + replace(item_value, '&amp;', '&')    from (        select distinct ltrim(rtrim(isnull(item_value,'')))  item_value            from IPM_PROJECT_FIELD_VALUE         where Item_ID =d.item_id    ) t    order by ltrim(item_value)    FOR XML PATH('')    ),    1,1,'') Default_values, Item_id, Item_Name from IPM_PROJECT_FIELD_DESC d where d.Item_ID= 2417355"
    rsProjectType.Open sql, Conn, 1, 4
    set rsProjectDeliveryMethod=server.createobject("adodb.recordset")
    sql = "select stuff( (select '; ' + replace(KeyName, '&amp;', '&')    from (        select distinct ltrim(rtrim(isnull(KeyName,'')))  keyname            from IPM_OFFICE where keyUse=1    ) t    order by ltrim(rtrim(keyname))    FOR XML PATH('')    ),    1,1,'') Default_values, '-1' as Item_id, 'Project Delivery Method' as Item_Name "
    rsProjectDeliveryMethod.Open sql, Conn, 1, 4
    
    
    set rsClient = server.createobject("adodb.recordset")
    sql = "select * from ipm_client where active = 1 order by name"
    rsClient.Open sql, Conn, 1, 4
    
    set rsFunctional = server.createobject("adodb.recordset")
    sql = "select * from ipm_discipline where keyuse = 1 order by keyname"
    rsFunctional.Open sql, Conn, 1, 4
    
    set rsDiscipline = server.createobject("adodb.recordset")
    sql = "select * from ipm_office where keyuse = 1 order by keyname"
    rsDiscipline.Open sql, Conn, 1, 4
    
    set rsProjectsStates=server.createobject("adodb.recordset")
    sql = "select stuff( (select '; ' + replace(state_id, '&amp;', '&')    from (        select distinct ltrim(rtrim(isnull(state_id,'')))  state_id            from IPM_PROJECT where available = 'y' and state_id <> ''    ) t    order by ltrim(rtrim(State_id))    FOR XML PATH('')    ),    1,1,'') Default_values, '-2' as Item_id, 'Project State' as Item_Name "
    rsProjectsStates.Open sql, Conn, 1, 4
    Function Build_AC_Control(item_id, item_name, item_value, asset_id, item_default_value, state) 
    '// populate unique ids
    Dim lblItemNameID
    lblItemNameID = "lblItemName_" & item_id
    Dim txtAutoCompleteID
    txtAutoCompleteID = "txtAutoComplete_" & item_id
    Dim hdnAssetID
    hdnAssetID = "hdnAssetID_" & item_id
    Dim hdnSelectedValuesID
    hdnSelectedValuesID = "hdnSelectedValues_" & item_id
    Dim lnkAddID
    lnkAddID = "lnkAdd_" & item_id
    Dim SelectedListTableID
    SelectedListTableID = "SelectedListTable_" & item_id
    Dim labelHTML
    labelHTML = ""
    Dim txtAutoCompleteHTML
    txtAutoCompleteHTML = ""
    Dim hdnAssetIDHTML
    hdnAssetIDHTML = ""
    Dim hdnSelectedValuesHTML
    hdnSelectedValuesHTML = ""
    Dim SelectedListTableHTML
    SelectedListTableHTML = ""
    Dim lnkAddHTML
    lnkAddHTML = ""
    '// end populate unique ids
    If state = "full" Then
        '// Adding javscript when gaining focus of textbox to assign autocomplete Control Functionality from JQueryUI
        txtAutoCompleteHTML = "&nbsp;&nbsp;<input style=""width:275px"" title=""" & item_name & """  name=""" & txtAutoCompleteID & """ type=""text"" id=""" & txtAutoCompleteID & """ onfocus='" & Build_OnFocus_Autocomplete_JQueryUIScript(txtAutoCompleteID, item_default_value) & "' />"
        '// end Addign Javascript focus
        '// Assign onclick attribute values for the Add linkbutton (javascript code)
        lnkAddHTML = "<a href=""javascript:;"" id=""" & lnkAddID & """ onclick=""Add_ACValue_ClickEvent(&#39;" & txtAutoCompleteID & "&#39;, &#39;" & SelectedListTableID & "&#39;, &#39;" & hdnSelectedValuesID & "&#39;);""><img src='images/add.png' height='20px' width='40px'></img></a>"
        '// end assign onclick attribute ....
    End If
    '// populate field's label with the value from DB
    labelHTML = "<span id=""" & lblItemNameID & """>" & item_name & "&nbsp;&nbsp;&nbsp;</span>"
    '// end populate field's label
    '// end assign hidden fields values
    hdnAssetIDHTML = "<input type=""hidden"" name=""" & hdnAssetID & """ id=""" & hdnAssetID & """ value=""" & asset_id & """ />"
    hdnSelectedValuesHTML = "<input type=""hidden"" name=""" & hdnSelectedValuesID & """ id=""" & hdnSelectedValuesID & """ value=""" & item_value & """ />"
    '// end assign hidden ield values
    '// Populate the table with the values from db (this also contains the javascript code onclick when removing the items)
    SelectedListTableHTML = BuildHTMLTable(item_value, hdnSelectedValuesID, SelectedListTableID, state)
    '// end populate the table
    '// put it all together
    'Damjan comment out
    'Return "<DIV class=""ui-widget"">" & SelectedListTableHTML & "<BR/><div class=""UDF_Title"" style=""width:150px"">" & labelHTML & "</div>" & "<div class=""UDF_Title"" style=""width:250px"">" & txtAutoCompleteHTML & lnkAddHTML & "</div>" & hdnAssetIDHTML & hdnSelectedValuesHTML & "<BR/>" & "<BR/>" & "<BR/></DIV>"
    Build_AC_Control = "<DIV class=""ui-widget"">" & SelectedListTableHTML & "<BR/>" & "<div class=""UDF_Title"" style=""width:350px"">" & txtAutoCompleteHTML & "&nbsp;&nbsp;" & lnkAddHTML & "</div>" & hdnAssetIDHTML & hdnSelectedValuesHTML & "<BR/>" & "" & "</DIV>"
End Function
    Function BuildHTMLTable(item_value, strHiddenFieldClientID, strTableClientId, state)
        
        Dim arr 
        arr = Split(item_value, ";")
        Dim htmlTable 
        htmlTable = " <table id=""" & strTableClientId & """><tr><td></td><td></td></tr>"
        dim item 
        item = ""
        For Each item In arr
            If Trim(item) <> "" Then
                If state = "full" Then
                    htmlTable = htmlTable & "<TR><TD><div class=""UDF_Title"" style=""width:20px"">" & "<a  href='javascript:;' onclick=""$(this).closest('tr').remove(); PopulateHiddenField('" & strHiddenFieldClientID & "', '" & strTableClientId & "');"" ><img src='images/delete.png'></img></a></div></TD><TD class=""content"" style=""font-family:Verdana;font-size:10px;font-weight: normal;width:325px;color:#EB6601"">" & Server.HTMLEncode(item) & "</TD></TR>"
                Else
                    htmlTable = htmlTable & "<TR><TD></TD><TD class=""content"" style=""font-family:Verdana;font-size:10px;font-weight: normal;width:150px"">" & item & "</TD></TR>"
                End If
            End If
        Next
        htmlTable = htmlTable & "</table>"
        BuildHTMLTable = htmlTable
    End Function
    Function Build_OnFocus_Autocomplete_JQueryUIScript(strAutoCompleteClientId, strItemDefaultValue) 
        Dim sb 
        sb = ""
        strItemDefaultValue = Replace(Replace(replace(strItemDefaultValue, "&amp;", "&"), vbCrLf, ""), ";", """,""")
        strItemDefaultValue = """" & strItemDefaultValue & """"
        sb = sb & " $(function () { "
        sb = sb & "       var availableTags = [ "
        sb = sb & " " & strItemDefaultValue & " " 
        sb = sb & "        ]; "
        sb = sb & "        $(""#" & strAutoCompleteClientId & """).autocomplete({ "
        sb = sb & "         source: availableTags "
        sb = sb & "        }); "
        sb = sb & "        $(""#" & strAutoCompleteClientId & """).autocomplete(""option"", ""autoFocus"", true); "
        sb = sb & " }); "
        Build_OnFocus_Autocomplete_JQueryUIScript = sb
    End Function
    Function BuildMultiSelectDropDown(item_id, sKeywords)
            Dim strMSDD
            dim sBlankText
            sBlankText = "Select a value..."
            dim item
            item = ""
            dim arr
            arr=Split(sKeywords, ";")
            strMSDD = "<select  multiple=""multiple"" size=""15"" id=""ITEM_15_" & item_id & """ name=""ITEM_15_" & item_id & "[]"">"
            strMSDD = strMSDD & "<option value="""">" & sBlankText & "</option>"
            For Each item in arr
                if Trim(item) <> "" then
                    strMSDD = strMSDD & "<option style=""font-family:Verdana;font-size:10px;font-weight: normal;"" value=""" & Server.HTMLEncode(item) & """ ><span style=""font-family:Verdana;font-size:10px;font-weight: normal;"">" & Server.HTMLEncode(item) & "</span></option>"
                end if
            Next
            strMSDD = strMSDD & "</select><input style=""font-family:Verdana;font-size:10px;font-weight: normal;"" class=""ITEM_15_" & item_id & """ id=""ITEM_15_" & item_id & """ type=""hidden"" name=""ITEM_15_" & item_id & """ value=""" & sBlankText & """/>"
            strMSDD = strMSDD & "<SCRIPT>$(document).ready( function() {$(""#ITEM_15_" & item_id & """).multiSelect({ oneOrMoreSelected: '*' }); $(""#ITEM_15_" & item_id & """).uncheckAllText=""Unselect All""; });</script>"
            
            BuildMultiSelectDropDown = strMSDD
    End Function
%>
<html>
<head>
<title><%=sPageTitle%> Reports Tool</title>
    <!--#include file="includes\header.asp" -->
<link href="css/custom/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css"  media="all"/>
<link href="css/snapStyle.css" type="text/css" rel="stylesheet" />
    
<style type="text/css">BODY { MARGIN: 0px }
            
            .paneliconoff
            {
                background-position: bottom left;
            }
            .paneliconon
            {
                background-image: url(images/i_openclose.gif);background-repeat:no-repeat;
                cursor:pointer;
            }            
                .ui-autocomplete {
                    max-height: 250px;
                    overflow-y: auto;
                    /* prevent horizontal scrollbar */
                    overflow-x: hidden;
                    width: 350px;
                    text-align:left;
                }
                * html .ui-autocomplete { text-align:left;       height: 250px; width:350px;   }
    </style>
<script type="text/javascript" src="js/jquery.1.6.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.1.8.13.min.js"></script>
<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
     <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
  <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
  <script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
-->
<script type="text/javascript" src="http://cdn.jquerytools.org/1.2.3/all/jquery.tools.min.js"></script>
<script src="js/jquery.multiSelect.js" type="text/javascript"></script>
  
 <script type="text/javascript">
     function Add_ACValue_ClickEvent(strAutoCompleteClientId, strTableClientId, strHiddenFieldClientID) {
         var name = $('#' + strAutoCompleteClientId).val();
         if (name != '') { $("#" + strTableClientId + " tr:last").after("<tr><td><div class='UDF_Title' style='width:20px'><a  href='#' onclick=\"$(this).closest('tr').remove(); PopulateHiddenField('" + strHiddenFieldClientID + "', '" + strTableClientId + "');\" ><img src='images/delete.png'></img></a></div></td><td class='content' style='font-family:Verdana;font-size:10px;font-weight: normal;width:325px;color:#EB6601'>" + name + "</td></tr>"); }
         PopulateHiddenField(strHiddenFieldClientID, strTableClientId);
         document.getElementById(strAutoCompleteClientId).value = '';
     }
     function PopulateHiddenField(strHiddenField, strSelectedTableField) {
         document.getElementById(strHiddenField).value = "";
         $('#' + strSelectedTableField + ' td.content').each(function () {
             document.getElementById(strHiddenField).value = document.getElementById(strHiddenField).value + $(this).html() + ";";
         });
     }
    </script>
    
<script type="text/javascript">
    function toggleFields(fieldsID, indicatorID) {
        theFields = document.getElementById(fieldsID);
        theFieldsIndicator = document.getElementById(indicatorID + "_img");
        if (theFields.style.display != "none") {
            theFields.style.display = "none";
            theFieldsIndicator.src = "../images/plus.png";
        }
        else {
            theFields.style.display = "block";
            theFieldsIndicator.src = "../images/minus.png";
        }
    }
    function validate() {
        if ((!isNumeric(document.getElementById("Cost_From").value)) && (document.getElementById("Cost_From").value != "")) {
            alert("Project Cost must be Numeric Value");
            return false;
        }
        if ((!isNumeric(document.getElementById("Cost_To").value)) && (document.getElementById("Cost_To").value != "")) {
            alert("Project Cost must be Numeric Value");
            return false;
        }
        return true;
    }
    function isNumeric(n) {
        return !isNaN(n);
    }
    //var calendarObj = new CalendarPopup("testdiv1");
    //calendarObj.showNavigationDropdowns();
    $(function () {
        $("#Completion_From").datepicker();
    });
    $(function () {
        $("#Completion_To").datepicker();
    });
</script>
</head>
<body onload="if(document.getElementById('pdf_radio').checked == true) { document.getElementById('pdf_options').style.visibility='visible'; }">
    <div id="drop_shadow">
        <div id="container">
            
            <!--#include file="includes\page_header.asp" -->
            
            
            <div id="content">
            <br />
            <table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
            <tr>
                <td class="left_col_overview" style="width:100%;">
                    <div id="content_text">
                    Marketing Tools | Generate Project Report
                    <br /><br />
                    <!-- <img src="images/title_project_reporting.jpg" alt="Project Reporting" /> -->
                    <h4>Step 1 - Select Projects</h4>
                    <form id="myform" method="post" action="report_print.asp" onsubmit="javascript: document.getElementById('lnkAdd_2417359').click(); document.getElementById('lnkAdd_2417354').click(); document.getElementById('lnkAdd_2417351').click();document.getElementById('lnkAdd_2417357').click(); document.getElementById('lnkAdd_2417358').click(); document.getElementById('lnkAdd_2417369').click(); if (validate() == false) {return false;}  ">
                <table>
                    <tr>
                        <td style="background-color:#EFEFEF;padding-left:6px">Project Name Contains:</td>
                        <td >&nbsp;&nbsp;<input type="text" name="search_name" /></td>
                    </tr>
                    <tr>
                        <td style="background-color:#EFEFEF;padding-left:6px" valign="top"><BR /><%=rsArchitect("Item_Name")%>:</td>
                        <td >    
                            <%
                            response.write(Build_AC_Control(rsArchitect("Item_Id"), rsArchitect("Item_Name"), "", "0", replace(replace(rsArchitect("Default_Values"), "'", ""), "&amp;", "&"), "full"))
                            
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="background-color:#EFEFEF;padding-left:6px"><BR /><%=rsClientOwnerCompanies("Item_Name")%>:</td>
                        <td >    
                            <%
                            response.write(Build_AC_Control(rsClientOwnerCompanies("Item_Id"), rsClientOwnerCompanies("Item_Name"), "", "0", replace(replace(rsClientOwnerCompanies("Default_Values"), "'", ""), "&amp;", "&"), "full"))
                            
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="background-color:#EFEFEF;padding-left:6px"><BR /><%=rsDeveloper("Item_Name")%>:</td>
                        <td>    
                            <%
                            response.write(Build_AC_Control(rsDeveloper("Item_Id"), rsDeveloper("Item_Name"), "", "0", replace(replace(rsDeveloper("Default_Values"), "'", ""), "&amp;", "&"), "full"))
                            
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="background-color:#EFEFEF;padding-left:6px"><BR /><%=rsElectrical("Item_Name")%>:</td>
                        <td>    
                            <%
                            response.write(Build_AC_Control(rsElectrical("Item_Id"),rsElectrical("Item_Name"), "", "0", replace(replace(rsElectrical("Default_Values"), "'", ""), "&amp;", "&"), "full"))
                            
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="background-color:#EFEFEF;padding-left:6px"><BR /><%=rsMechanical("Item_Name")%>:</td>
                        <td>    
                            <%
                            response.write(Build_AC_Control(rsMechanical("Item_Id"),rsMechanical("Item_Name"), "", "0", replace(replace(rsMechanical("Default_Values"), "'", ""), "&amp;", "&"), "full"))
                            
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="background-color:#EFEFEF;padding-left:6px"><BR /><%=rsStructural("Item_Name")%>:</td>
                        <td>    
                            <%
                            response.write(Build_AC_Control(rsStructural("Item_Id"),rsStructural("Item_Name"), "", "0", replace(replace(rsStructural("Default_Values"), "'", ""), "&amp;", "&"), "full"))
                            
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="background-color:#EFEFEF;padding-left:6px"><%=rsProjectType("Item_Name")%>:</td>
                        <td>    &nbsp;
                            <%
                            response.write(BuildMultiSelectDropDown(rsProjectType("Item_Id"), replace(replace(rsProjectType("Default_Values"), "'", ""), "&amp;", "&")))
                            'response.write(Build_AC_Control(rsProjectType("Item_Id"),rsProjectType("Item_Name"), "", "0", replace(replace(rsProjectType("Default_Values"), "'", ""), "&amp;", "&"), "full"))
                            
                            %>
                        </td>
                    </tr>
                                        
                    <tr>
                        <td valign="top" style="background-color:#EFEFEF;padding-left:6px"><%=rsLEED("Item_Name")%>:</td>
                        <td>&nbsp;    
                            <%
                            response.write(BuildMultiSelectDropDown(rsLEED("Item_Id"), replace(replace(rsLEED("Default_Values"), "'", ""), "&amp;", "&")))
                            
                            %>
                        </td>
                    </tr>                    
                    <tr>
                        <td valign="top" style="background-color:#EFEFEF;padding-left:6px"><%=rsOwnerType("Item_Name")%>:</td>
                        <td>    &nbsp;
                            <%
                            response.write(BuildMultiSelectDropDown(rsOwnerType("Item_Id"), replace(replace(rsOwnerType("Default_Values"), "'", ""), "&amp;", "&")))
                            
                            %>
                        </td>
                    </tr>                    
                    <tr>
                        <td valign="top" style="background-color:#EFEFEF;padding-left:6px"><%=rsStructuralType("Item_Name")%>:</td>
                        <td>    &nbsp;
                            <%
                            response.write(BuildMultiSelectDropDown(rsStructuralType("Item_Id"), replace(replace(rsStructuralType("Default_Values"), "'", ""), "&amp;", "&")))
                            
                            %>
                        </td>
                    </tr>                    
                    <tr>
                        <td valign="top" style="background-color:#EFEFEF;padding-left:6px"><%=rsProjectDeliveryMethod("Item_Name")%>:</td>
                        <td>    &nbsp;
                            <%
                            response.write(BuildMultiSelectDropDown(rsProjectDeliveryMethod("Item_Id"), replace(replace(rsProjectDeliveryMethod("Default_Values"), "'", ""), "&amp;", "&")))
                            
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="background-color:#EFEFEF;padding-left:6px"><%=rsProjectsStates("Item_Name")%>:</td>
                        <td>    &nbsp;
                            <%
                            response.write(BuildMultiSelectDropDown(rsProjectsStates("Item_Id"), replace(replace(rsProjectsStates("Default_Values"), "'", ""), "&amp;", "&")))
                            
                            %>
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                    <tr>
                    <td style="background-color:#EFEFEF;padding-left:6px">Total Gross SF:</td>
                    <td>&nbsp;&nbsp;From: <select name="SF_From" id="SF_From"> <option value="No Minimum">No Minimum</option>    <option value="0">0</option> <option value="25000">25,000</option><option value="50000">50,000</option><option value="75000">75,000</option><option value="100000">100,000</option><option value="125000">125,000</option><option value="150000">150,000</option><option value="175000">175,000</option><option value="200000">200,000</option><option value="225000">225,000</option><option value="250000">250,000</option><option value="275000">275,000</option><option value="300000">300,000</option><option value="325000">325,000</option><option value="350000">350,000</option><option value="375000">375,000</option><option value="400000">400,000</option><option value="425000">425,000</option><option value="450000">450,000</option><option value="475000">475,000</option><option value="500000">500,000</option><option value="525000">525,000</option><option value="550000">550,000</option><option value="575000">575,000</option><option value="600000">600,000</option><option value="625000">625,000</option><option value="650000">650,000</option><option value="675000">675,000</option><option value="700000">700,000</option><option value="725000">725,000</option><option value="750000">750,000</option><option value="775000">775,000</option><option value="800000">800,000</option><option value="825000">825,000</option><option value="850000">850,000</option><option value="875000">875,000</option><option value="900000">900,000</option><option value="925000">925,000</option><option value="950000">950,000</option><option value="975000">975,000</option><option value="1000000">1,000,000</option><option value="1025000">1,025,000</option><option value="1050000">1,050,000</option><option value="1075000">1,075,000</option><option value="1100000">1,100,000</option><option value="1125000">1,125,000</option><option value="1150000">1,150,000</option><option value="1175000">1,175,000</option><option value="1200000">1,200,000</option><option value="1225000">1,225,000</option><option value="1250000">1,250,000</option><option value="1275000">1,275,000</option><option value="1300000">1,300,000</option><option value="1325000">1,325,000</option><option value="1350000">1,350,000</option><option value="1375000">1,375,000</option><option value="1400000">1,400,000</option><option value="1425000">1,425,000</option><option value="1450000">1,450,000</option><option value="1475000">1,475,000</option><option value="1500000">1,500,000</option><option value="1525000">1,525,000</option><option value="1550000">1,550,000</option><option value="1575000">1,575,000</option><option value="1600000">1,600,000</option><option value="1625000">1,625,000</option><option value="1650000">1,650,000</option><option value="1675000">1,675,000</option><option value="1700000">1,700,000</option><option value="1725000">1,725,000</option><option value="1750000">1,750,000</option><option value="1775000">1,775,000</option><option value="1800000">1,800,000</option><option value="1825000">1,825,000</option><option value="1850000">1,850,000</option><option value="1875000">1,875,000</option><option value="1900000">1,900,000</option><option value="1925000">1,925,000</option><option value="1950000">1,950,000</option><option value="1975000">1,975,000</option><option value="2000000">2,000,000</option></select> &nbsp; &nbsp;To: <select name="SF_To" id="SF_To"> <option value="No Maximum">No Maximum</option>    <option value="0">0</option> <option value="25000">25,000</option><option value="50000">50,000</option><option value="75000">75,000</option><option value="100000">100,000</option><option value="125000">125,000</option><option value="150000">150,000</option><option value="175000">175,000</option><option value="200000">200,000</option><option value="225000">225,000</option><option value="250000">250,000</option><option value="275000">275,000</option><option value="300000">300,000</option><option value="325000">325,000</option><option value="350000">350,000</option><option value="375000">375,000</option><option value="400000">400,000</option><option value="425000">425,000</option><option value="450000">450,000</option><option value="475000">475,000</option><option value="500000">500,000</option><option value="525000">525,000</option><option value="550000">550,000</option><option value="575000">575,000</option><option value="600000">600,000</option><option value="625000">625,000</option><option value="650000">650,000</option><option value="675000">675,000</option><option value="700000">700,000</option><option value="725000">725,000</option><option value="750000">750,000</option><option value="775000">775,000</option><option value="800000">800,000</option><option value="825000">825,000</option><option value="850000">850,000</option><option value="875000">875,000</option><option value="900000">900,000</option><option value="925000">925,000</option><option value="950000">950,000</option><option value="975000">975,000</option><option value="1000000">1,000,000</option><option value="1025000">1,025,000</option><option value="1050000">1,050,000</option><option value="1075000">1,075,000</option><option value="1100000">1,100,000</option><option value="1125000">1,125,000</option><option value="1150000">1,150,000</option><option value="1175000">1,175,000</option><option value="1200000">1,200,000</option><option value="1225000">1,225,000</option><option value="1250000">1,250,000</option><option value="1275000">1,275,000</option><option value="1300000">1,300,000</option><option value="1325000">1,325,000</option><option value="1350000">1,350,000</option><option value="1375000">1,375,000</option><option value="1400000">1,400,000</option><option value="1425000">1,425,000</option><option value="1450000">1,450,000</option><option value="1475000">1,475,000</option><option value="1500000">1,500,000</option><option value="1525000">1,525,000</option><option value="1550000">1,550,000</option><option value="1575000">1,575,000</option><option value="1600000">1,600,000</option><option value="1625000">1,625,000</option><option value="1650000">1,650,000</option><option value="1675000">1,675,000</option><option value="1700000">1,700,000</option><option value="1725000">1,725,000</option><option value="1750000">1,750,000</option><option value="1775000">1,775,000</option><option value="1800000">1,800,000</option><option value="1825000">1,825,000</option><option value="1850000">1,850,000</option><option value="1875000">1,875,000</option><option value="1900000">1,900,000</option><option value="1925000">1,925,000</option><option value="1950000">1,950,000</option><option value="1975000">1,975,000</option><option value="2000000">2,000,000</option></select></td>
                    </tr>
                    <tr>
                    <td style="background-color:#EFEFEF;padding-left:6px">Project Cost:</td>
                    <td>&nbsp;&nbsp;From: <input type="text" name ="Cost_From" id ="Cost_From" /> &nbsp; &nbsp;To: <input type="text" name ="Cost_To" id ="Cost_To" /></td>
                    </tr>
                    <tr>
                    <td style="background-color:#EFEFEF;padding-left:6px">Project Completion Date:</td>
                    <td>&nbsp;&nbsp;From: <input type="text"  name ="Completion_From"  id ="Completion_From" /> &nbsp; &nbsp;To: <input type="text" name ="Completion_To" id ="Completion_To" /></td>
                    </tr>
    
                
                    
                </table>
<h4>Step 2 - Select Formatting Options</h4>
<table cellpadding="3" cellspacing="3">
        <tr>
            <td valign="top" style="background-color:#EFEFEF;padding-left:6px">Report Name: </td>
            <td><input type="text" name="report_name" /></td>
        </tr>    
                    <tr>
                        <td style="background-color:#EFEFEF;padding-left:6px" valign="top">Output:</td>
                        <td> <input name="search_output" type="radio" value="html" checked="checked" onclick="document.getElementById('pdf_options').style.visibility='hidden';" />HTML  <input id="pdf_radio" name="search_output" type="radio" value="pdf" onclick="document.getElementById('pdf_options').style.visibility='visible';" />PDF  <input id="csv_output" name="search_output" type="radio" value="csv" onclick="document.getElementById('pdf_options').style.visibility='hidden';" />CSV
                             <div id="pdf_options" style="visibility:hidden;">Page break interval: <input type="text" size="2" name="pdf_page_break" id="pdf_page_break" value="3" /></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color:#EFEFEF;padding-left:6px" valign="top">Display Image:</td>
                        <td> <input name="search_display_image" type="checkbox" checked="checked" value="1" />
                        </td>
                    </tr>            
                    <tr>
                        <td style="background-color:#EFEFEF;padding-left:6px" valign="top">Display Logo:</td>
                        <td> <select name="search_display_logo">
                                <option selected value="Clark">Clark</option>
                                <option value="Atkinson">Atkinson</option>
                                <option value="Edgemoor">Edgemoor</option>
                                <option value="Shirley">Shirley</option>
                            </select>
                        </td>
                    </tr>            
        <tr>
            <td valign="top" ><h4><br>Fields to display: </h4></td>
            <td></td>
        </tr>
        <tr>
            <td valign="top" style="background-color:#EFEFEF;padding-left:6px">Standard Fields</td>
            <td>
            <input type="checkbox" class="udf" name="number" value="1" checked="checked" /> Project Number<br />
            <input type="checkbox" class="udf" name="client" value="1" checked="checked" /> Client<br />
            <input type="checkbox" class="udf" name="address" value="1" checked="checked" /> Address<br />
            <input type="checkbox" class="udf" name="city_state" value="1" checked="checked" /> Location (City, State)<br />
            </td>
        </tr>
        <tr>
            <td valign="top" style="background-color:#EFEFEF;"><table><td valign="middle"> <big><b><a id="udfFieldsIndicator" href="javascript:toggleFields('udfFields','udfFieldsIndicator');"><img id="udfFieldsIndicator_img" src="images\plus.png"></img></a></b></big></td> <td valign="middle"> <a href="javascript:toggleFields('udfFields','udfFieldsIndicator');">User-Defined Fields</a></td></table></td>
            <td>
            <div id ="udfFields" style="display:none;">
            <%
            do while not rsUDF.eof
            itemTag = trim(rsUDF("item_tag"))
            %><input type="checkbox" class="udf" name="<%=itemTag%>" value="1" /><%=trim(rsUDF("item_name"))%><br /><%rsUDF.moveNext
            loop
            
            %>
            </div>
            </td>
        </tr>
        <tr>
            <td valign="top" style="background-color:#EFEFEF;"><table><td valign="middle">  <big><b><a id="keywordsIndicator" href="javascript:toggleFields('keywordsFields','keywordsIndicator');"><img id = "keywordsIndicator_img" src="images\plus.png"></img></a></b></big> </td> <td valign="middle">  <a href="javascript:toggleFields('keywordsFields','keywordsIndicator');">Keywords</a></td></table></td>
            <td>
            <div id ="keywordsFields" style="display:none;">
            <input type="checkbox" class="udf" name="disciplines" value="1" /> Project Features <br />
            <input type="checkbox" class="udf" name="keywords" value="1" /> Building Types <br />
            <input type="checkbox" class="udf" name="office" value="1" /> Project Keywords <br />
            </div>
            </td>
        </tr>
        
        <tr>
            <td></td>
            <td>
		<a href = "javascript: document.forms[0].action='report_print.asp'; document.getElementById('btnSubmit').click();" > <img src="images/GenerateReport.png"  width="129px" height="30px"  ></img> </a>
		<div style="display: none"> <input type="submit" id="btnSubmit" value="Generate Report" /> </div>  
		<a href = "javascript:  document.forms[0].action='report_print_MultiDatasheet.asp'; document.getElementById('btnSubmit').click();" > <img src="images/GenerateReport_multi.png"  width="129px" height="30px" onclick=" if (getElementById('csv_output').checked == true) { alert('CSV is not available for this report.'); return false;}"  ></img> </a> 
        <br/>
        <a href = "javascript: document.forms[0].action='report_print_3Datasheet.asp'; document.getElementById('btnSubmit').click();" > <img src="images/GenerateReport_three.png"  width="129px" height="30px" onclick=" if (getElementById('csv_output').checked == true) { alert('CSV is not available for this report.'); return false;}" ></img> </a>
        <a href = "javascript: document.forms[0].action='report_print_ProjectDatasheet.asp'; document.getElementById('btnSubmit').click();" > <img src="images/GenerateReport_single.png"  width="129px" height="30px" onclick=" if (getElementById('csv_output').checked == true) { alert('CSV is not available for this report.'); return false;}" ></img> </a> 
        </td>
        </tr>
<input type="hidden" value="1" name="submit" />
</form>
                    
                    
                    
                    
                    
                    </div>
                    
                </td>
                
            
    
            </table>
            </div>
            
        
        </div>
    </div>
</body>
</html>