<!--#include file="includes\config.asp" -->
<%
'''NOTE: THE DISCIPLINE AND FUNCTIONAL MARKET (OFFICE) TABLES ARE INVERTED DUE TO PAST MISLABELLING
 if request.QueryString("submit") = "1" then
%>
<!--#include file="includes\report_main_pdf_get.asp" -->
<% 'if (report_output = "html") then %>
<html>
<head>
<title>
Report
</title>
<style type="text/css">
td,th {
border: 1px solid transparent;
padding: 3px;
text-align: left;
vertical-align: top;
font-family: Myriad Pro;
font-size: 13px;
line-height: 19px;
color: #4d4d4d
}
table {
border:1px solid transparent;
border-collapse: collapse;
padding:10px;
}
th {
background: #0076be;
}
</style>
</head>
<body style="margin-left:100px; margin-top:50px; margin-right:50px; margin-bottom:50px; ">
<table style="width:100%; border: 1px solid transparent; "  >
<tr>
<td align="left" style="width:50%; text-align: left; border: 1px solid transparent;">
<font style="font-size:16px" color='#004b8d' ><%=UCase(report_title)%> </font>
</td>
<td align="right" style="width:50%; text-align: right; border: 1px solid transparent;" >
<% If Request("search_display_logo") = "Clark" then%>
<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Atkinson" then %>
<img src="images/atkinson_logo.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Edgemoor" then %>
<img src="images/edgemoor_logo.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Shirley" then %>
<img src="images/shirley_logo.jpg" alt="Clark Logo" />
<%end if %>
</td>
</tr>
</table>
<br/>
<table width="100%">
<!--begin table heading-->
<tr>
</tr>
<!--end table heading-->
<!--Begin results listing-->
<%
i=0
do while not rsProjects.eof%>
<tr>
<td align="left" valign="top" style="width: 198px;"> <img width="196"  src="<%=sAssetPath%><%=rsProjects("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=196&height=196&qfactor=25" alt="<%=rsProjects("Name")%>" /></td>
<td style="text-align: left; " align="left" valign="top"><font style="font-size:16px" color='#004b8d' ><%=UCase(rsProjects("Name"))%></font>
<br/><%=rsProjects("city") & ", " & rsProjects("state_id") %>
<br/>
<br/>
<table style="width: 100%; padding:0px;"><tr> <td style="width:50%; text-align: left; padding: 0px; " align="left" valign="top"><b><font color="#004b8d">Contract Value:</font></b> &nbsp;&nbsp;
<%
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_JDE_FinalContractValue'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "" & rsLineUDF("item_value") & "</td>"
else
response.write "&nbsp;</td>"
end if
%>
<td style="width:50%; text-align: left; padding: 0px; " align="left" valign="top"><b><font color="#004b8d">Square Footage:</font></b> &nbsp;&nbsp;
<%
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_GLA'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
        Response.Write("" & FormatNumber(rsLineUDF("item_value"), 0) & "</td>")
else
response.write "&nbsp;</td>"
end if
%>
</tr>
</table>
<br/>
<div style="text-align: justify;  text-justify: newspaper; text-align-last: left">
<%
'''Begin Standard
'''End Standard
''''Begin UDFs
if(rsLineUDF.State = 1) then
rsLineUDF.close
end if
sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_RESUME'"
rsLineUDF.Open sql, Conn, 1, 4
if(rsLineUDF.recordCount > 0) then
if(rsLineUDF("item_value") = "") then
rsLineUDF("item_value") = "&nbsp;"
end if
response.write "" & rsLineUDF("item_value") & ""
else
response.write "&nbsp;"
end if
   Response.Write("</div></td></tr><tr><td>&nbsp</td></tr><tr><td>&nbsp</td></tr>")
   rsProjects.moveNext()
i = i + 1
if(i mod 3 = 0 and not rsProjects.eof) then %>
</table>
<div style="page-break-before:always">&nbsp;</div> 
<table style="width:100%; border: 1px solid transparent; "  >
<tr>
<td align="left" style="width:50%; text-align: left; border: 1px solid transparent;">
<font style="font-size:16px" color='#004b8d' ><%=UCase(report_title)%> </font>
</td>
<td align="right" style="width:50%; text-align: right; border: 1px solid transparent;" >
<% If Request("search_display_logo") = "Clark" then%>
<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Atkinson" then %>
<img src="images/atkinson_logo.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Edgemoor" then %>
<img src="images/edgemoor_logo.jpg" alt="Clark Logo" />
<% ElseIf Request("search_display_logo") = "Shirley" then %>
<img src="images/shirley_logo.jpg" alt="Clark Logo" />
<%end if %>
</td>
</tr>
</table>
<br/>
<table width="100%">
<!--begin table heading-->
<tr>
</tr>
<%
End If
Loop
%>
<!--End results listing-->
</table>
</body>
</html>