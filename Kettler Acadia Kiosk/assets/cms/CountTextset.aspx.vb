Imports System.IO

Partial Class _counttextset
    Inherits System.Web.UI.Page

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim out As String = "0"

        Dim filepath As String = Server.MapPath("~") & "assets\cms\data\"
        Dim setDir() As String = Request.QueryString("name").Split("_")
        Dim theDir As String = "Textset"
        For i As Integer = 1 To setDir.Length - 1
            theDir &= "_" & setDir(i)
        Next
        theDir &= "\"
        filepath &= "Textset\" & theDir

        out = System.IO.Directory.GetFiles(filepath, "*.txt").Length.ToString
        Response.Clear()
        Response.Write(out)
        Response.Flush()
        Response.End()

    End Sub
End Class
