﻿package map2 {
	import flash.display.*;
	import flash.events.MouseEvent;
	import flash.text.*;
	import caurina.transitions.Tweener;
	import com.boontaran.Utility;
	
	public class Key extends Sprite {
		private var titleTxt:TextField;
		public var px:Number;
		public var py:Number;
		public var description:String;
		public var link:String;
		public var linkTarget:String;
		public var id:int;
		
		private var numBox:NumBox;
		
		public function Key(node:XML=null) {
			if (node) {
				titleTxt = createTitle(node.title);
				link = node.link;
				linkTarget = node.link.@target;
				id = int(node.@id);
				
				if (!linkTarget) linkTarget = "_self";
				
				addChild(titleTxt);
				bg.height = bgHover.height = light.height = titleTxt.height + 4;
				
				titleTxt.x = 28;
				titleTxt.y = 2;
				
				var pos:Array = node.xy.split(",");
				px = pos[0];
				py = pos[1];
				description = node.description;
				
				buttonMode = true;
				
				numBox = new NumBox(node);
				addChild(numBox);
				
				numBox.x = 4;
				numBox.y = (bg.height - numBox.height)/2;
								
			} else {
				mouseEnabled = false;
			}
			mouseChildren = false;
			
			bg.x = bgHover.x = light.x = 0;
			bg.y = bgHover.y = light.y = 0;
									
			Utility.setColor(bgHover, uint(Map2.xml.settings.hover_color));
			Utility.setColor(bg, uint(Map2.xml.settings.key_background_color));
			bgHover.alpha = 0;
			
			
						
		}
		public function setTitle(title:String):void {
			titleTxt = createTitle(title);
			addChild(titleTxt);
			
			titleTxt.x = 6;
			
			
			bg.height = bgHover.height = light.height = titleTxt.height+4;
			
			titleTxt.y = (bg.height - titleTxt.height) / 2;
			
		}
		private function createTitle(title:String):TextField {
			var txt:TextField = new TextField();
			
			txt = new TextField();
			txt.selectable = false;
			txt.multiline = false;
			txt.autoSize = TextFieldAutoSize.LEFT;
			txt.condenseWhite = true;
		
			txt.htmlText = title;
			
			
			var fmt:TextFormat = new TextFormat();
			fmt.font = Map2.xml.settings.font;
			fmt.size = Map2.xml.settings.size;
			fmt.color = uint(Map2.xml.settings.key_text_color);
			
			
			txt.setTextFormat(fmt);
			
			
			
			return txt;
		}
		internal function mouseOver():void {
			Tweener.addTween(bgHover, { alpha:1 , time:0.8 } );
			if(numBox) numBox.hover();
		}
		internal function mouseOut():void {
			Tweener.addTween(bgHover, { alpha:0 , time:0.8 } );
			if(numBox) numBox.normal();
			
		}
	}

}