﻿/*
3 oct 2010		: - add a canceling method
*/

package com.boontaran {
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.Dictionary;
	
	public class DataLoader {
		private static var calls:Dictionary = new Dictionary(true);;
				
		public static function load(file:String,onComplete:Function,onNotFound:Function=null,vars:Array=null):URLLoader {
			var ldr:URLLoader = new URLLoader();
			var req:URLRequest = new URLRequest(file);
			
			if(vars) {
				var variables:URLVariables = new URLVariables();
				
				for(var key:String in vars) {
					variables[key] = vars[key];
					
				}
				
				req.data = variables;
				req.method = URLRequestMethod.POST;
				
				
			}
			ldr.addEventListener(Event.COMPLETE , completed);
			ldr.load(req);
			
			var call:Array = new Array();
			call['onComplete'] = onComplete;
			
			if(onNotFound != null) {
				ldr.addEventListener(IOErrorEvent.IO_ERROR,  ioError);
				call['notFound'] = onNotFound;
			} 
			
			calls[ldr]=call;
			
			return ldr;
		}
		// !!!
		public static function cancel(ldr:URLLoader):void {
			ldr.close();
			delete calls[ldr];
		}
		private static function completed(e:Event):void {
			var ldr:URLLoader = URLLoader(e.target);
			
			removeListeners(ldr);
			
			var callBack:Function = calls[ldr]['onComplete'];
			
			delete calls[ldr];
			
			callBack(ldr.data);
			
		}
		private static function ioError(e:Event):void {
			var ldr:URLLoader = URLLoader(e.target);
			removeListeners(ldr);
			
			var callBack:Function = calls[ldr]['notFound'] ;
			delete calls[ldr];
			callBack();
		}
		private static function removeListeners(ldr:URLLoader):void {
			if(calls[ldr]['onComplete']) {
				ldr.removeEventListener(Event.COMPLETE , completed);
			} 
			if(calls[ldr]['onNotFound']) {
				ldr.removeEventListener(IOErrorEvent.IO_ERROR,  ioError);
			} 
			
		}
	}	
}