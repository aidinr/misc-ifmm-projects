﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class ascx_Gramercy_Search
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.Form.Keys.Count = 0 Then


            Dim ds As DataSet
            ds = CType(Application("DS"), DataSet)
            PopulateFilters("")
            PopulateSearch(ds, "", "Type ASC")
            Try
                PopulateAptinfo(0, 1, "", "")
            Catch ex As Exception

            End Try

            'damjan
            'searchCarouselRepeater.DataSource = getCarouselDataRows()
            'searchCarouselRepeater.DataBind()

        End If
    End Sub

    Public Sub PopulateFilters(ByVal theColumn As String, ByVal theFilter As String)

        Select Case theColumn


            Case "building"
                'filter_building.DataSource = getDistinctRows("Units", "Building", theFilter)
                'filter_building.DataBind()
            Case "penthouse"
                If (theFilter = "") Then
                    'filter_penthouse.DataSource = getDistinctRows("Units", "Type", "type like '%penthouse%'")

                Else
                    'filter_penthouse.DataSource = getDistinctRows("Units", "Type", theFilter)
                End If
                'filter_penthouse.DataBind()
            Case "type"
                filter_type.DataSource = getDistinctRows("Units", "Type", theFilter)
                filter_type.DataBind()
            Case "floors"
                'filter_floor.DataSource = getDistinctRows("Units", "Floor", theFilter)
                'filter_floor.DataBind()
            Case "price"
                Dim DistinctPriceTable As DataTable
                Dim FilterFlag1 As Boolean = False
                Dim FilterFlag2 As Boolean = False
                Dim FilterFlag3 As Boolean = False
                Dim FilterFlag4 As Boolean = False

                DistinctPriceTable = getDistinctRows("Units", "Price", theFilter)
                For Each Row As DataRow In DistinctPriceTable.Rows
                    If (Row("Price") < 1000) Then
                        FilterFlag1 = True
                    ElseIf ((Row("Price") < 2000)) Then
                        FilterFlag2 = True
                    ElseIf ((Row("Price") < 3000)) Then
                        FilterFlag3 = True
                    ElseIf ((Row("Price") >= 3000)) Then
                        FilterFlag4 = True
                    End If
                Next

                If (FilterFlag1) Then
                    'lblFilterPrice1.Text = "<hr /><a href=""javascript:doFilter('price','1');"">Up to $999</a>"
                Else
                    'lblFilterPrice1.Text = ""
                End If
                If (FilterFlag2) Then
                    'lblFilterPrice2.Text = "<hr /><a href=""javascript:doFilter('price','1000');"">$1,000 - $1,999</a>"
                Else
                    'lblFilterPrice2.Text = ""
                End If
                If (FilterFlag3) Then
                    'lblFilterPrice3.Text = "<hr /><a href=""javascript:doFilter('price','2000');"">$2,000 - $2,999</a>"
                Else
                    'lblFilterPrice3.Text = ""
                End If
                If (FilterFlag4) Then
                    'lblFilterPrice4.Text = "<hr /><a href=""javascript:doFilter('price','3000');"">$3,000+</a>"
                Else
                    'lblFilterPrice4.Text = ""
                End If

        End Select
    End Sub
    Public Sub PopulateFilters(ByVal theFilter As String)

        If (theFilter = "") Then
            'filter_penthouse.DataSource = getDistinctRows("Units", "Type", "type like '%penthouse%'")

        Else
            'filter_penthouse.DataSource = getDistinctRows("Units", "Type", theFilter)
        End If



        'filter_penthouse.DataBind()



        'filter_building.DataSource = getDistinctRows("Units", "Building", theFilter)

        'filter_building.DataBind()



        'filter_floor.DataSource = getDistinctRows("Units", "Floor", theFilter)
        'filter_floor.DataBind()

        If (theFilter = "") Then
            filter_type.DataSource = getDistinctRows("Units", "Type", "")

        Else
            filter_type.DataSource = getDistinctRows("Units", "Type", theFilter)
        End If

        filter_type.DataBind()

        Dim DistinctPriceTable As DataTable
        Dim FilterFlag1 As Boolean = False
        Dim FilterFlag2 As Boolean = False
        Dim FilterFlag3 As Boolean = False
        Dim FilterFlag4 As Boolean = False
        Dim FilterFlag5 As Boolean = False
        Dim FilterFlag6 As Boolean = False

        DistinctPriceTable = getDistinctRows("Units", "Price", theFilter)
        For Each Row As DataRow In DistinctPriceTable.Rows
            If (Row("Price") < 2000) Then
                FilterFlag1 = True
            ElseIf ((Row("Price") < 3000)) Then
                FilterFlag2 = True
            ElseIf ((Row("Price") < 4000)) Then
                FilterFlag3 = True
            ElseIf ((Row("Price") < 5000)) Then
                FilterFlag4 = True
            ElseIf ((Row("Price") >= 5000)) Then
                FilterFlag5 = True
            End If
        Next

        If (FilterFlag1) Then
            lblFilterPrice1.InnerText = "Up to $1,999"
            lblFilterPrice1.Attributes.Add("onclick", "javascript:priceResultsDisplay=$(this).text(); doFilter('price','1000');")

            'lblFilterPrice1.Text = "<hr /><a href=""javascript:doFilter('price','0');"">Up to $999</a>"
        Else
            lblFilterPrice1.InnerText = ""
        End If
        If (FilterFlag2) Then
            lblFilterPrice2.InnerText = "$2,000 - $2,999"
            lblFilterPrice2.Attributes.Add("onclick", "javascript:priceResultsDisplay=$(this).text(); doFilter('price','2000');")
            'lblFilterPrice2.Text = "<hr /><a href=""javascript:doFilter('price','1000');"">$1,000 - $1,999</a>"
        Else
            lblFilterPrice2.InnerText = ""
        End If
        If (FilterFlag3) Then
            lblFilterPrice3.InnerText = "$3,000 - $3,999"
            lblFilterPrice3.Attributes.Add("onclick", "javascript:priceResultsDisplay=$(this).text(); doFilter('price','3000');")
            'lblFilterPrice3.Text = "<hr /><a href=""javascript:doFilter('price','2000');"">$2,000 - $2,999</a>"
        Else
            lblFilterPrice3.InnerText = ""
        End If
        If (FilterFlag4) Then
            lblFilterPrice4.InnerText = "$4,000 - $4,999"
            lblFilterPrice4.Attributes.Add("onclick", "javascript:priceResultsDisplay=$(this).text(); doFilter('price','4000');")
            'lblFilterPrice4.Text = "<hr /><a href=""javascript:doFilter('price','3000');"">$3,000+</a>"
        Else
            lblFilterPrice4.InnerText = ""
        End If
        If (FilterFlag5) Then
            lblFilterPrice5.InnerText = "$5,000+"
            lblFilterPrice5.Attributes.Add("onclick", "javascript:priceResultsDisplay=$(this).text(); doFilter('price','5000');")
            'lblFilterPrice4.Text = "<hr /><a href=""javascript:doFilter('price','3000');"">$3,000+</a>"
        Else
            lblFilterPrice5.InnerText = ""
        End If

        '---------------------  FLOOR FILTER LOAD
        Dim FilterFloorFlag_1_5 As Boolean = False
        Dim FilterFloorFlag_6_10 As Boolean = False
        Dim FilterFloorFlag_11_15 As Boolean = False
        Dim FilterFloorFlag_16_20 As Boolean = False
        Dim FilterFloorFlag_21_25 As Boolean = False
        Dim FilterFloorFlag_26_30 As Boolean = False
        Dim FilterFloorFlag_31_35 As Boolean = False
        Dim FilterFloorFlag_36_40 As Boolean = False
        Dim FilterFloorFlag_41_45 As Boolean = False
        Dim FilterFloorFlag_46_50 As Boolean = False

        DistinctPriceTable = getDistinctRows("Units", "Floor", theFilter)
        For Each Row As DataRow In DistinctPriceTable.Rows
            If (Row("Floor") <= 5) Then
                FilterFloorFlag_1_5 = True
            ElseIf ((Row("Floor") <= 10)) Then
                FilterFloorFlag_6_10 = True
            ElseIf ((Row("Floor") <= 15)) Then
                FilterFloorFlag_11_15 = True
            ElseIf ((Row("Floor") <= 20)) Then
                FilterFloorFlag_16_20 = True
            ElseIf ((Row("Floor") <= 25)) Then
                FilterFloorFlag_21_25 = True
            ElseIf ((Row("Floor") <= 30)) Then
                FilterFloorFlag_26_30 = True
            ElseIf ((Row("Floor") <= 35)) Then
                FilterFloorFlag_31_35 = True
            ElseIf ((Row("Floor") <= 40)) Then
                FilterFloorFlag_36_40 = True
            ElseIf ((Row("Floor") <= 45)) Then
                FilterFloorFlag_41_45 = True
            ElseIf ((Row("Floor") <= 50)) Then
                FilterFloorFlag_46_50 = True
            End If
        Next

        lblFilterFloor1_5.InnerText = "1st - 5th"
        lblFilterFloor1_5.Attributes.Add("onclick", "javascript:floorResultsDisplay= $(this).text(); doFilter('floor','1');")
        lblFilterFloor6_10.InnerText = "6th - 10th"
        lblFilterFloor6_10.Attributes.Add("onclick", "javascript:floorResultsDisplay= $(this).text(); doFilter('floor','2');")
        lblFilterFloor11_15.InnerText = "11th - 15th"
        lblFilterFloor11_15.Attributes.Add("onclick", "javascript:floorResultsDisplay= $(this).text(); doFilter('floor','3');")
        lblFilterFloor16_20.InnerText = "16th - 18th"
        lblFilterFloor16_20.Attributes.Add("onclick", "javascript:floorResultsDisplay= $(this).text(); doFilter('floor','4');")
        


        ' CRESCENT NINTH ONLY HAS 5 FLOORS , so we don't have to do this.
        'If (FilterFloorFlag_1_5) Then
        '    lblFilterFloor1_5.InnerText = "1 - 5"
        '    lblFilterFloor1_5.Attributes.Add("onclick", "javascript:floorResultsDisplay= $(this).text(); doFilter('floor','1');")
        'Else
        '    lblFilterFloor1_5.InnerText = ""
        'End If

        'If (FilterFloorFlag_6_10) Then
        '    lblFilterFloor6_10.InnerText = "6 - 10"
        '    lblFilterFloor6_10.Attributes.Add("onclick", "javascript:floorResultsDisplay= $(this).text(); doFilter('floor','6');")
        'Else
        '    lblFilterFloor6_10.InnerText = ""
        'End If

        'If (FilterFloorFlag_11_15) Then
        '    lblFilterFloor11_15.InnerText = "11 - 15"
        '    lblFilterFloor11_15.Attributes.Add("onclick", "javascript:floorResultsDisplay= $(this).text(); doFilter('floor','11');")
        'Else
        '    lblFilterFloor11_15.InnerText = ""
        'End If

        'If (FilterFloorFlag_16_20) Then
        '    lblFilterFloor16_20.InnerText = "16 - 20"
        '    lblFilterFloor16_20.Attributes.Add("onclick", "javascript:floorResultsDisplay= $(this).text(); doFilter('floor','16');")
        'Else
        '    lblFilterFloor16_20.InnerText = ""
        'End If

        'If (FilterFloorFlag_21_25) Then
        '    lblFilterFloor21_25.InnerText = "21 - 25"
        '    lblFilterFloor21_25.Attributes.Add("onclick", "javascript:floorResultsDisplay= $(this).text(); doFilter('floor','21');")
        'Else
        '    lblFilterFloor21_25.InnerText = ""
        'End If

        'If (FilterFloorFlag_26_30) Then
        '    lblFilterFloor26_30.InnerText = "26 - 30"
        '    lblFilterFloor26_30.Attributes.Add("onclick", "javascript:floorResultsDisplay= $(this).text(); doFilter('floor','26');")
        'Else
        '    lblFilterFloor26_30.InnerText = ""
        'End If

        'If (FilterFloorFlag_31_35) Then
        '    lblFilterFloor31_35.InnerText = "31 - 35"
        '    lblFilterFloor31_35.Attributes.Add("onclick", "javascript:floorResultsDisplay= $(this).text(); doFilter('floor','31');")
        'Else
        '    lblFilterFloor31_35.InnerText = ""
        'End If

        'If (FilterFloorFlag_36_40) Then
        '    lblFilterFloor36_40.InnerText = "36 - 40"
        '    lblFilterFloor36_40.Attributes.Add("onclick", "javascript:floorResultsDisplay= $(this).text(); doFilter('floor','36');")
        'Else
        '    lblFilterFloor36_40.InnerText = ""
        'End If

        'If (FilterFloorFlag_41_45) Then
        '    lblFilterFloor41_45.InnerText = "41 - 45"
        '    lblFilterFloor41_45.Attributes.Add("onclick", "javascript:floorResultsDisplay= $(this).text(); doFilter('floor','41');")
        'Else
        '    lblFilterFloor41_45.InnerText = ""
        'End If

        'If (FilterFloorFlag_46_50) Then
        '    lblFilterFloor46_50.InnerText = "46 - 50"
        '    lblFilterFloor46_50.Attributes.Add("onclick", "javascript:floorResultsDisplay= $(this).text(); doFilter('floor','46');")
        'Else
        '    lblFilterFloor46_50.InnerText = ""
        'End If

    End Sub
    Public Sub PopulateSearch(ByVal theDataSet As DataSet, ByVal theFilter As String, ByVal theSort As String)

        Dim SearchArray() As DataRow

        'Dim UnitArea As Integer
        'Dim UnitPrice As Integer

        Dim resizeLength As Integer = 50

        SearchArray = theDataSet.Tables("Units").Select(theFilter, theSort)
        Session("SearchArrayLength") = SearchArray.Length
        If SearchArray.Length >= resizeLength Then
            Array.Resize(SearchArray, resizeLength)
            lblSearchResultsMore.Text = "<div class=""search_result_row_container""><div style=""font-size:16px;font-style:italic;font-weight:bold;margin-top:0px;margin:8px 0 0 1px;color: #fff"">Please refine your search to reduce the number of units displayed.</div></div>"
            'lblSearchResults.Text = Session("SearchArrayLength")
        Else
            Array.Resize(SearchArray, SearchArray.Length)
        End If

        Session("SearchCriteria") = theFilter


        
        search_results_rows.DataSource = SearchArray
        search_results_rows.DataBind()


    End Sub
    'search mode
    Public Sub PopulateAptinfo(ByVal RowIndex As String, ByVal MaxLength As Integer, ByVal theFilter As String, ByVal theSort As String)
        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)
        Dim iDAMPATHThumb As String = Replace(Session("WSRetreiveAsset") & "size=1&crop=1&width=80&height=80&id=", "&", "&amp;")
        Dim iDAMPATHFull As String = Replace(Session("WSRetreiveAsset") & "size=0&width=650&height=531&qfactor=25&crop=2&cache=1&id=", "&", "&amp;")
        Dim iDAMPATHPrint As String = Replace(Session("WSRetreiveAsset") & "size=0&width=750&height=450&qfactor=25&crop=2&id=", "&", "&amp;")
        Dim iDAMPATHPrintThumb As String = Replace(Session("WSRetreiveAsset") & "size=1&crop=1&width=200&height=200&id=", "&", "&amp;")
        Dim dr As DataRow
        Dim PrevIndex As Integer
        Dim NextIndex As Integer

        Dim AptPrice As String
        Dim AptArea As Integer

        'LiteralButtonBack.Text = "<a onclick=""naviScroll.start(2866,0);$('main_details').fade('out');"" href=""#"" class=""navi_search""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_button_back.jpg"" alt=""navi"" /></a>"
        If MaxLength = 99999 Then
            MaxLength = ds.Tables("Units").Rows.Count
        End If
        dr = getAptDataRow(RowIndex, theFilter, theSort)
        If Not dr Is Nothing Then

            If (RowIndex = (MaxLength - 1)) Then
                NextIndex = 0
            Else
                NextIndex = RowIndex + 1
            End If

            If (RowIndex = 0) Then
                PrevIndex = MaxLength - 1
            Else
                PrevIndex = RowIndex - 1
            End If

            AptArea = CInt(dr("Area"))

            AptPrice = dr("Price_String")


            'lblModelNumber.Text = dr("Model_Name")

            ' printNumber.Text = dr("Unit")
            'printAvailability.Text = dr("Available")
            'printBath.Text = dr("Bathroom")
            'printFloor.Text = dr("Floor")
            'printType.Text = dr("Type")
            'printPrice.Text = AptPrice
            'printSQF.Text = AptArea
            'printImage.Text = "<img src=""" & iDAMPATHPrint & dr("Model_AssetID") & """ alt=""" & dr("Unit") & """/>"
            'printImage1.Text = "<img src=""images/thumb_1.jpg"" />"
            'printImage2.Text = "<img src=""images/thumb_2.jpg"" />"
            'printView.Text = dr("Amenities")

            'buttonEmail.Text = "<img onclick=""javascript:tb_show('', 'Email.aspx?id=" & dr("Unit") & "&KeepThis=true&TB_iframe=true&height=498&width=481', false);"" id=""button_email"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_button_email.jpg"" alt=""button"" />"

            btnEmail.Attributes.Add("onclick", "javascript:tb_show('', 'Email.aspx?id=" & dr("Unit") & "&KeepThis=true&TB_iframe=true&height=498&width=481', false);")


            'lblAptArea.Text = String.Format("{0:n}", AptArea)
            lblAptArea.Text = AptArea
            If dr("Available") = "Available Soon" Then
                lblAptAvailable.Text = dr("Available").ToString.Replace("Soon", "") & dr("Availability_date")
            Else
                lblAptAvailable.Text = dr("Available")
            End If
            lblAptBath.Text = dr("Bathroom")
            ' lblAptBuilding.Text = dr("Building")
            lblAptFloor.Text = dr("Floor")
            'lblAptNumber.Text = dr("Unit")
            lblAptNumber2.Text = dr("Unit")
            lblAptPrice.Text = AptPrice
            lblAptType.Text = dr("Type") & ", " & dr("Bathroom") & " BATH"
            lblAptType2.Text = dr("Type")

            Dim arrList As New ArrayList(dr("Amenities").ToString().Split(","))
            Dim strViews As String = ""

            For Each item As String In arrList
                If (item.ToUpper().Contains("VIEW")) Then
                    strViews = strViews & item.Replace("View", "").Replace("view", "") & ", "
                End If
            Next

            If strViews.Length > 2 Then
                strViews = strViews.Substring(0, strViews.Length - 2)
            End If
            lblAptView.Text = strViews

            If (MaxLength > 1) Then
                If MaxLength > 50 Then
                    btnAptPrev.Attributes.Add("onclick", "javascript:loadApt('unit'," & PrevIndex & ");")
                    btnAptNext.Attributes.Add("onclick", "javascript:loadApt('unit'," & NextIndex & ");")

                Else
                    btnAptPrev.Attributes.Add("onclick", "javascript:loadApt('search'," & PrevIndex & ");")
                    btnAptNext.Attributes.Add("onclick", "javascript:loadApt('search'," & NextIndex & ");")

                End If
            Else
                '  lblAptPrev.Text = ""
                '  lblAptNext.Text = ""
            End If


            'gallery

            lblDetailGalleryMainImage1.Text = "<img src=""" & iDAMPATHFull & dr("Model_AssetID") & """ alt=""" & dr("Unit") & """/>"




            'lblDetailGalleryMainImage2.Text = "<img src="""" alt=""" & dr("Unit") & """/>"

            'lblDetailGalleryFirstImage.Text = "<a href=""javascript:loadDetailGallery('" & iDAMPATHFull & dr("Model_AssetID") & "');""><img src=""" & iDAMPATHThumb & dr("Model_AssetID") & """ alt=""" & dr("Unit") & """ class=""active_thumb detail_thumb"" /></a>"
            'gallery_detail_thumb_images_additional_image.DataSource = ds.Tables("Images").Select("Model_ID = " & dr("Model_ID"))

            'If (ds.Tables("Images").Select("Model_ID = " & dr("Model_ID")).Length > 2) Then
            '    lblDetailGalleryThumbPrev.Text = "<img id=""detail_gallery_thumb_prev"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_thumb_prev.jpg"" alt=""thumbnails"" />"
            '    lblDetailGalleryThumbNext.Text = "<img id=""detail_gallery_thumb_next"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_thumb_next.jpg"" alt=""thumbnails"" />"
            'Else
            '    lblDetailGalleryThumbPrev.Text = "<img style=""visibility:hidden;"" id=""detail_gallery_thumb_prev"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_thumb_prev.jpg"" alt=""thumbnails"" />"
            '    lblDetailGalleryThumbNext.Text = "<img style=""visibility:hidden;"" id=""detail_gallery_thumb_next"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_thumb_next.jpg"" alt=""thumbnails"" />"
            'End If


            'gallery_detail_thumb_images_additional_image.DataBind()

            If (dr("Available") = "Available Now" Or dr("Available") = "Available Soon") Then
                FloorplanDetailsAdd.Attributes.Add("onclick", "addToCarousel('" & dr("Asset_ID") & "');CallBackCartPopup.Callback('', '', '');")
                ' literalDetailAddCarousel.Text = "<img src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_button_add.jpg"" onclick=""addToCarousel(" & dr("Asset_ID") & ");fireDetailCarouselCallback();"" />"
            Else
                FloorplanDetailsAdd.Attributes.Add("style", "display:none")
                '  literalDetailAddCarousel.Text = ""
            End If


        End If
    End Sub
    'carousel mode
    Public Sub PopulateAptinfo(ByVal theMethod As String, ByVal RowIndex As String)
        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)
        Dim iDAMPATHThumb As String = Replace(Session("WSRetreiveAsset") & "type=asset&size=1&crop=1&width=80&height=80&id=", "&", "&amp;")
        Dim iDAMPATHFull As String = Replace(Session("WSRetreiveAsset") & "type=asset&size=0&width=650&height=531&qfactor=25&crop=2&id=", "&", "&amp;")
        Dim iDAMPATHPrint As String = Replace(Session("WSRetreiveAsset") & "type=asset&size=0&width=750&height=450&qfactor=25&crop=2&id=", "&", "&amp;")
        Dim iDAMPATHPrintThumb As String = Replace(Session("WSRetreiveAsset") & "type=asset&size=1&crop=1&width=200&height=200&id=", "&", "&amp;")
        Dim dr As DataRow
        Dim PrevIndex As Integer
        Dim NextIndex As Integer

        Dim AptPrice As String
        Dim AptArea As Integer

        Dim maxlength As Integer

        If (theMethod = "carousel") Then
            Dim sessionArrayList = CType(Session("Carousel"), ArrayList)
            maxlength = sessionArrayList.Count

            dr = getCarouselDataRows()(RowIndex)

            ' LiteralButtonBack.Text = "<a onclick=""naviScroll.start(2866,0);$('main_details').fade('out');"" href=""#"" class=""navi_search""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_button_back.jpg"" alt=""navi"" /></a>"

        ElseIf (theMethod = "unit") Then
            maxlength = CType(ds.Tables("Units").Select("", " unit desc "), DataRow())(0).Item("Unit")

            dr = getAptDataRow(RowIndex)


            'LiteralButtonBack.Text = "<a onclick=""naviScroll.start(1433,0);$('main_details').fade('out');$('main_search').fade('out');"" href=""#"" class=""navi_search""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_button_back.jpg"" alt=""navi"" /></a>"
        End If





        If Not dr Is Nothing Then
            Try

                If theMethod = "unit" Then
                    If (RowIndex = maxlength) Then
                        NextIndex = 0
                    Else
                        NextIndex = CType(ds.Tables("Units").Select(" unit > " & RowIndex, " unit asc "), DataRow())(0).Item("Unit")
                    End If

                    If (RowIndex = CType(ds.Tables("Units").Select("", " unit asc "), DataRow())(0).Item("Unit")) Then
                        PrevIndex = maxlength
                    Else
                        PrevIndex = CType(ds.Tables("Units").Select("unit < " & RowIndex, " unit desc "), DataRow())(0).Item("Unit")
                    End If

                Else
                    If (RowIndex = (maxlength - 1)) Then
                        NextIndex = 0
                    Else
                        NextIndex = RowIndex + 1
                    End If

                    If (RowIndex = 0) Then
                        PrevIndex = maxlength - 1
                    Else
                        PrevIndex = RowIndex - 1
                    End If
                End If
            Catch ex As Exception

            End Try

            AptArea = CInt(dr("Area"))

            AptPrice = dr("Price_String")


            'lblModelNumber.Text = dr("Model_Name")
            'lblAptArea.Text = String.Format("{0:n}", AptArea)
            lblAptArea.Text = AptArea
            If dr("Available") = "Available Soon" Then
                lblAptAvailable.Text = dr("Available").ToString.Replace("Soon", "") & dr("Availability_date")
            Else
                lblAptAvailable.Text = dr("Available")
            End If
            lblAptBath.Text = dr("Bathroom")
            'lblAptBuilding.Text = dr("Building")
            lblAptFloor.Text = dr("Floor")
            'lblAptNumber.Text = dr("Unit")
            lblAptNumber2.Text = dr("Unit")
            lblAptPrice.Text = AptPrice
            lblAptType.Text = dr("Type") & ", " & dr("Bathroom") & " BATH"
            lblAptType2.Text = dr("Type")


            Dim arrList As New ArrayList(dr("Amenities").ToString().Split(","))
            Dim strViews As String = ""

            For Each item As String In arrList
                If (item.ToUpper().Contains("VIEW")) Then
                    strViews = strViews & item.Replace(" View", "").Replace(" view", "") & ", "
                End If
            Next

            If strViews.Length > 2 Then
                strViews = strViews.Substring(0, strViews.Length - 2)
            End If
            lblAptView.Text = strViews

            btnEmail.Attributes.Add("onclick", "javascript:tb_show('', 'Email.aspx?id=" & dr("Unit") & "&KeepThis=true&TB_iframe=true&height=498&width=481', false);")

            'buttonEmail.Text = "<img onclick=""javascript:tb_show('', 'Email.aspx?id=" & dr("Unit") & "&KeepThis=true&TB_iframe=true&height=498&width=481', false);"" id=""button_email"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_button_email.jpg"" alt=""button"" />"

            'printNumber.Text = dr("Unit")
            'printAvailability.Text = dr("Available")
            'printBath.Text = dr("Bathroom")
            'printFloor.Text = dr("Floor")
            'printType.Text = dr("Type")
            'printPrice.Text = AptPrice
            'printSQF.Text = AptArea
            'printImage.Text = "<img src=""" & iDAMPATHPrint & dr("Model_AssetID") & """ alt=""" & dr("Unit") & """/>"
            'printImage1.Text = "<img src=""images/thumb_1.jpg"" />"
            'printImage2.Text = "<img src=""images/thumb_2.jpg"" />"
            'printView.Text = dr("Amenities")
            If (maxlength > 1) Then
                Dim prevIndexString As String = ""
                If PrevIndex < 1000 Then
                    prevIndexString = "0" & PrevIndex.ToString()
                Else
                    prevIndexString = PrevIndex.ToString()
                End If
                Dim nextIndexString As String = ""
                If NextIndex < 1000 Then
                    nextIndexString = "0" & NextIndex.ToString()
                Else
                    nextIndexString = NextIndex.ToString()
                End If
                If maxlength > 50 Then
                    If PrevIndex <> 0 Then
                        btnAptPrev.Attributes.Add("onclick", "javascript:loadApt('unit','" & prevIndexString & "');")
                    Else
                        btnAptPrev.Attributes.Add("style", "display:none")
                    End If
                    If NextIndex <> 0 Then
                        btnAptNext.Attributes.Add("onclick", "javascript:loadApt('unit','" & nextIndexString & "');")
                    Else
                        btnAptNext.Attributes.Add("style", "display:none")
                    End If
                Else
                    btnAptPrev.Attributes.Add("onclick", "javascript:loadApt('search','" & prevIndexString & "');")
                    btnAptNext.Attributes.Add("onclick", "javascript:loadApt('search','" & nextIndexString & "');")

                End If
                End If
            'gallery
            lblDetailGalleryMainImage1.Text = "<img src=""" & iDAMPATHFull & dr("Model_AssetID") & """ alt=""" & dr("Unit") & """/>"
            'lblDetailGalleryMainImage2.Text = "<img src="""" alt=""" & dr("Unit") & """/>"
            'lblDetailGalleryFirstImage.Text = "<a href=""javascript:loadDetailGallery('" & iDAMPATHFull & dr("Model_AssetID") & "');""><img src=""" & iDAMPATHThumb & dr("Model_AssetID") & """ alt=""" & dr("Unit") & """ class=""active_thumb detail_thumb"" /></a>"
            'gallery_detail_thumb_images_additional_image.DataSource = ds.Tables("Images").Select("Model_ID = " & dr("Model_ID"))

            'If (ds.Tables("Images").Select("Model_ID = " & dr("Model_ID")).Length > 2) Then
            '    lblDetailGalleryThumbPrev.Text = "<img id=""detail_gallery_thumb_prev"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_thumb_prev.jpg"" alt=""thumbnails"" />"
            '    lblDetailGalleryThumbNext.Text = "<img id=""detail_gallery_thumb_next"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_thumb_next.jpg"" alt=""thumbnails"" />"
            'Else
            '    lblDetailGalleryThumbPrev.Text = "<img style=""visibility:hidden;"" id=""detail_gallery_thumb_prev"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_thumb_prev.jpg"" alt=""thumbnails"" />"
            '    lblDetailGalleryThumbNext.Text = "<img style=""visibility:hidden;"" id=""detail_gallery_thumb_next"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_thumb_next.jpg"" alt=""thumbnails"" />"
            'End If


            'gallery_detail_thumb_images_additional_image.DataBind()

            If (dr("Available") = "Available Now" Or dr("Available") = "Available Soon") Then
                FloorplanDetailsAdd.Attributes.Add("onclick", "addToCarousel('" & dr("Asset_ID") & "');CallBackCartPopup.Callback('', '', '');")
                ' literalDetailAddCarousel.Text = "<img src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "gallery_detail_button_add.jpg"" onclick=""addToCarousel(" & dr("Asset_ID") & ");fireDetailCarouselCallback();"" />"
            Else
                FloorplanDetailsAdd.Attributes.Add("style", "display:none")
                '  literalDetailAddCarousel.Text = ""
            End If


        End If
    End Sub


    Public Function getAptDataRow(ByVal RowIndex As String, ByVal theFilter As String, ByVal theSort As String) As DataRow
        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)
        Dim dr() As DataRow

        dr = ds.Tables("Units").Select(theFilter, theSort)

        Return dr(RowIndex)


    End Function

    Public Function getAptDataRow(ByVal UnitNumber As String) As DataRow


        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)
        Dim dr() As DataRow

        dr = ds.Tables("Units").Select("Unit = '" & UnitNumber.Trim & "'")


        Return dr(0)


    End Function
    Public Function getCarouselDataRows() As DataRow()
        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)

        Dim assetID As String = ""
        Dim dr() As DataRow
        Dim sessionArrayList = CType(Session("Carousel"), ArrayList)
        If (sessionArrayList.Count > 0) Then
            For x = 0 To sessionArrayList.Count - 1
                assetID = assetID & sessionArrayList(x).ToString & ", "
            Next
            assetID = assetID.Substring(0, assetID.Length - 2)


            dr = ds.Tables("Units").Select("Asset_ID In (" & assetID & ")", "price")
        Else
            'literalSearchCarousel.Text = "<li><img src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "empty_carousel.jpg"" alt=""ADD UNIT"" /><p>ADD<br /><small>UNITS</small></p></li>"
            'literalDetailCarousel.Text = "<li><img src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "empty_carousel.jpg"" alt=""ADD UNIT"" /><p>ADD<br /><small>UNITS</small></p></li>"
        End If
        Return dr

    End Function

    Public Function getDistinctRows(ByVal theTableName As String, ByVal theColumnName As String, ByVal theFilter As String) As DataTable
        Dim DV As New DataView
        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)
        DV.Table = ds.Tables(theTableName)
        If (theFilter <> "") Then
            'DV.RowFilter = theColumnName & " <> NULL and " & theFilter
            DV.RowFilter = theFilter
        Else
            'DV.RowFilter = theColumnName & " <> NULL"
        End If

        DV.Sort = theColumnName & " asc"
        Return DV.ToTable(theColumnName, True, theColumnName)

    End Function

    Public Sub CallBack1_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBack1.Callback
        If (e.Parameters(0) = "search") Then
            PopulateAptinfo(e.Parameters(1), e.Parameters(2), e.Parameters(3), e.Parameters(4))
        ElseIf (e.Parameters(0) = "carousel" Or e.Parameters(0) = "scroll" Or e.Parameters(0) = "unit") Then
            PopulateAptinfo(e.Parameters(0), e.Parameters(1))
        End If


        PlaceHolder1.RenderControl(e.Output)

    End Sub

    Public Sub CallBack2_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBack2.Callback
        Dim WS As Kettler_WS.Service = New Kettler_WS.Service

        Dim ds As DataSet = New DataSet


        ds = WS.GetAllProjects()
        'ds = CType(Application("DS"), DataSet)
        PopulateSearch(ds, e.Parameters(0), e.Parameters(1))
        PlaceHolder2.RenderControl(e.Output)
    End Sub
    Public Sub CallBackFilterPenthouse_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) ' Handles CallBackFilterPenthouse.Callback
        PopulateFilters("penthouse", "") 'This is a hack to always show the full range of penthouse filters
        'PlaceholderFilterPenthouse.RenderControl(e.Output)
    End Sub
    Public Sub CallBackFilterType_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) ' Handles CallBackFilterType.Callback
        PopulateFilters("type", e.Parameter)
        'PlaceholderFilterType.RenderControl(e.Output)
    End Sub
    Public Sub CallBackFilterFloors_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) ' Handles CallBackFilterFloors.Callback
        PopulateFilters("floors", e.Parameter)
        'PlaceholderFilterFloors.RenderControl(e.Output)
    End Sub
    Public Sub CallBackFilterPrice_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) 'Handles CallBackFilterPrice.Callback
        PopulateFilters("price", e.Parameter)
        'PlaceholderFilterPrice.RenderControl(e.Output)
    End Sub
    Public Sub CallBackSearchHeader_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBackSearchHeader.Callback
        Dim SearchArrayLength As String
        SearchArrayLength = Session("SearchArrayLength")
        'lblSearchCriteria.Text = Session("SearchCriteria") & ""
        lblSearchresults.Text = SearchArrayLength
        PlaceHolderSearchHeader.RenderControl(e.Output)
    End Sub
    Public Sub CallBackSearchCarousel_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) ' Handles CallBackSearchCarousel.Callback
        'searchCarouselRepeater.DataSource = getCarouselDataRows()
        'searchCarouselRepeater.DataBind()
        ' PlaceholderSearchCarousel.RenderControl(e.Output)
    End Sub

    Public Sub CallbackFilterBuilding_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) ' Handles CallBackFilterBuilding.Callback
        PopulateFilters("building", e.Parameter)
        ' PlaceholderFilterBuilding.RenderControl(e.Output)
    End Sub
    Public Sub CallBackChangeCarousel_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBackChangeCarousel.Callback
        If (e.Parameters(0) = "add") Then
            Dim theAssetID As String = e.Parameters(1)
            Dim sessionArrayList = CType(Session("Carousel"), ArrayList)

            If Not (sessionArrayList.Contains(theAssetID)) Then
                sessionArrayList.Add(e.Parameters(1).ToString)
            End If

            Session("Carousel") = sessionArrayList

        End If
    End Sub
    Public Sub CallBackDetailCarousel_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallbackDetailCarousel.Callback

        RepeaterDetailCarousel.DataSource = getCarouselDataRows()
        RepeaterDetailCarousel.DataBind()

        PlaceholderDetailCarousel.RenderControl(e.Output)


    End Sub



    Public Sub search_results_rows_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim literalSearchRowOptions As Literal = e.Item.FindControl("LiteralSearchRowOptions")

            If (e.Item.DataItem("Availability_date") & "" <> "") Then

                literalSearchRowOptions.Text = "<img src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "add_to_carousel.png"" alt=""Add to Carousel"" />"
            End If


        End If
    End Sub

End Class

