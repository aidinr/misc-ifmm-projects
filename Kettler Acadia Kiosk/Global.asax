﻿<%@ Application Language="VB" %>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="WebArchives.iDAM.WebCommon.Security " %>
<%@ Import Namespace="System.Net" %>
<script runat="server">
    

    '    Const sBaseIP As String = "idam.ifmm.com:8667"
    Const sBaseIP As String = "idam2.ifmm.com"
    Const sBaseInternalIP As String = "208.255.178.48"
    Const SBaseInstance As String = "IDAM_KETTLER"
    Const sVSIDAMClientUpload As String = "idamClientUpload"
    Const sVSIDAMClientDownload As String = "IDAM"
    Const sVSIDAMClient As String = "IDAMBrowseService"
    Const sVSIDAMClient110Upload As String = "IDAMFileUpload"
    
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
        Dim DS As New DataSet()
        
        
        Dim WS As kettler_ws.Service = New kettler_ws.Service
        Try
            DS = WS.GetAllProjects()
            DS.WriteXml("Kettler.xml")
        Catch ex As Exception
            'DS.ReadXml("Kettler.xml")
        Finally
        End Try
            
        Application("DS") = DS
        
        'Dim downloadSessionID As String = getSessionID("http://" & sBaseIP & "/" & sVSIDAMClientDownload & "/GetSessionID.aspx")
        'Dim downloadLogin As String = "http://" & sBaseInternalIP & "/" & sVSIDAMClientDownload & "/(" + downloadSessionID + ")/Login.aspx"
        'LoginToWebserviceClient(downloadLogin, "KettlerPublic", "kiosk2008", "IDAM_KETTLER", "false")
        'Dim downloadURL As String = "http://" & sBaseIP & "/" & sVSIDAMClientDownload & "/(" + downloadSessionID + ")/DownloadFile.aspx?dtype=assetdownload&instance=IDAM_KETTLER&"
        
        
        
        
        'Dim flvpath As String = System.AppDomain.CurrentDomain.BaseDirectory() + System.Configuration.ConfigurationManager.AppSettings("swfFolder")
        'check to make sure dir exists
        'If Not Directory.Exists(flvpath) Then           
        'Directory.CreateDirectory(flvpath)
        'End If
        'For Each Row As DataRow In DS.Tables("HomeAssets").Rows
        'Select Case Row("name")
        '  Case "home_video"
        'Try
        'SaveUrlToPath(flvpath, downloadURL & "size=0&assetid=" & Row("asset_id"))
        'Catch ex As Exception

        'Finally
        'End Try
        'End Select
        'Next
        
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
        
        Dim assetRetrieve = sBaseIP & "/" & sVSIDAMClient
        Dim assetDownload = sBaseIP & "/" & sVSIDAMClientDownload
        
        Dim theLogin As String = "KettlerPublic"
        Dim thePassword As String = "kiosk2008"
        thePassword = WebArchives.iDAM.WebCommon.Security.Encrypt(thePassword)

        Dim loginStatus As Boolean = False

        Dim loginResult1 As String = ""

        Dim sessionID As String = "(S(" & GetSessionID("http://" & assetRetrieve & "/GetSessionID.aspx") & "))"
        Dim iDAMBrowse As New browsesecure.BrowseService
        iDAMBrowse.Url = "http://" & assetRetrieve & "/" & sessionID & "/" & "BrowseService.asmx"
        'Response.Write(iDAMBrowse.Url)
        Try
            loginResult1 = iDAMBrowse.Login(theLogin, thePassword, sBaseInstance, True)
        Catch Ex As Exception
            Response.Write(Ex)
        End Try
        
        Dim sessionIDDownload As String = "(S(" & GetSessionID("http://" & assetDownload & "/GetSessionID.aspx") & "))"
        
        Dim iDAMDownload As New DownloadSecure.AssetDownloadService
        iDAMDownload.Url = "http://" & assetDownload & "/" & sessionIDDownload & "/" & "AssetDownloadService.asmx"
       
        Try
            loginResult1 = iDAMDownload.Login(theLogin, thePassword, sBaseInstance, True)
        Catch Ex As Exception
            Response.Write(Ex)
        End Try
        
        Session("WSRetreiveAsset") = "http://" & assetRetrieve & "/" + sessionID + "/RetrieveAsset.aspx?instance=" & sBaseInstance & "&"
        Session("WSDownloadAsset") = "http://" & assetDownload & "/" & sessionIDDownload + "/DownloadFile.aspx?dtype=assetdownload&instance=" & sBaseInstance & "&"
 
        
        
        'Carousel Session for MET Millennium only!
        Dim initArray As ArrayList = New ArrayList


        Session("Carousel") = initArray
        
        
        
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
    
    
 




 
    Public Shared Function GetSessionID(ByVal URL As String) As String
        Try
            Dim clientreq As HttpWebRequest
            clientreq = HttpWebRequest.Create(URL)
            clientreq.Timeout = CType(10000, Integer)
            Dim clientres As HttpWebResponse = clientreq.GetResponse
            Dim data As IO.Stream = clientres.GetResponseStream
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            If str.IndexOf("SESSIONID") >= 0 Then
                Dim strtidx As Integer = str.IndexOf("SESSIONID=") + 10
                Return str.Substring(strtidx, str.IndexOf("</span>") - strtidx)
            Else
                Throw New Exception("No session ID found. check URL if it has the proper format" + URL)
            End If
        Catch ex As Exception
            Throw New Exception("WebService not available." + URL)
        End Try
    End Function
       
    
    Sub CopyStream(ByRef source As Stream, ByRef target As Stream)
        Dim buffer As Byte() = New Byte(65535) {}
        If source.CanSeek Then
            source.Seek(0, SeekOrigin.Begin)
        End If
        Dim bytes As Integer = source.Read(buffer, 0, buffer.Length)
        Try
            While (bytes > 0)

                target.Write(buffer, 0, bytes)
                bytes = source.Read(buffer, 0, buffer.Length)
            End While
        Finally
            ' Or target.Close(); if you're done here already.
            target.Flush()
        End Try
    End Sub
    
    
    
    Sub SaveUrlToPath(ByVal sPath As String, ByVal sURL As String)
        Dim wc As Net.WebClient
        Dim stream As System.IO.Stream
        Try
               
            Dim t As Date = Now
            wc = New Net.WebClient
            'ping first to prevent a major bug on downloadfile call. If there's a 404, sPath will be deleted
            stream = wc.OpenRead(sURL)
            Dim tempPath As String = sPath & "home_video.flv"
            SaveStreamToFilePath(tempPath, stream)
            'System.IO.File.Copy(tempPath, "home_video.flv", True)
            'stream.Close()
            'wc.DownloadFile(sURL, sPath)
          
        Catch ex2 As Net.WebException
         
        Catch ex As Exception
            
        Finally
            If Not wc Is Nothing Then
                If Not stream Is Nothing Then
                    stream.Close()
                End If
                wc.Dispose()
            End If
        End Try
    End Sub

    
    Sub SaveStreamToFilePath(ByVal sFilePath As String, ByRef istream As Stream)
        Dim outputStream As New FileStream(sFilePath, FileMode.Create)
        'Dim bw As New BinaryWriter(outputStream)
        'Dim br As New BinaryReader(istream)

        'bw.Write(br.ReadBytes(istream.Length))
        'bw.Flush()
        ''            Dim i As Integer
        ''           For i = 0 To istream.Length
        ''bw.Write(br.Read())
        ''Next i
        'bw.Close()
        'br.Close()
        Try
            CopyStream(istream, outputStream)
        Finally
            outputStream.Close()
            istream.Close()
        End Try
    End Sub
    
    
    
    
   
</script>
