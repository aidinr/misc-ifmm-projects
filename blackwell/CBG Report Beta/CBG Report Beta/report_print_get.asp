<!--#include file="includes\config.asp" -->
<%

	'''NOTE: THE DISCIPLINE AND FUNCTIONAL MARKET (OFFICE) TABLES ARE INVERTED DUE TO PAST MISLABELLING

	if request.querystring("submit") = "1" then
	
	'Pre-set parameters
	report_title = Request.Querystring("report_name")
	
	report_name = prep_sql(Request.Querystring("search_name"))
	report_client = prep_sql(Request.Querystring("search_client"))
	report_region = prep_sql(Request.Querystring("search_region"))
	report_state = prep_sql(Request.Querystring("search_state"))
	report_functional = prep_sql(Request.Querystring("search_functional"))
	report_discipline = prep_sql(Request.Querystring("search_discipline"))
	report_output = Request.Querystring("search_output")
	
	report_display_image = Request.Querystring("search_display_image")
	report_page_break = int(Request.Querystring("pdf_page_break"))
	
	if(Request.Querystring("search_favorites") = "0") then
	report_favorite = " AND c.favorite = 0"
	elseif(Request.Querystring("search_favorites") = "1") then
	report_favorite = " AND c.favorite = 1"
	else
	report_favorite = ""
	end if

	if(Request.Querystring("search_published") = "0") then
	report_published = " AND c.publish = 0"
	elseif(Request.Querystring("search_published") = "1") then
	report_published = " AND c.publish = 1"
	else
	report_published = ""
	end if
	
	display_number = Request.Querystring("number")
	display_client = Request.Querystring("client")
	display_address = Request.Querystring("address")
	display_city_state = Request.Querystring("city_state")
	display_zip = Request.Querystring("zip")
	display_desc = Request.Querystring("desc")
	display_mdesc = Request.Querystring("mdesc")
	display_url = Request.Querystring("purl")
	
	displayChallenge = Request.Querystring("challenge")
	displayApproach = Request.Querystring("approach")
	displaySolution = Request.Querystring("solution")

	displayDisc = Request.Querystring("disciplines")
	displayKeywords = Request.Querystring("keywords")
	displayFunctional = Request.Querystring("functional")	
	
	'Big search filter
	set rsProjects = server.createobject("adodb.recordset")
	'sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description from ipm_project_field_value a, ipm_project_field_desc b, ipm_project c, ipm_project_discipline d, ipm_discipline e WHERE a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_REGION' AND a.projectid = c.projectid AND c.publish = 1AND item_value LIKE '%" & report_region & "%' AND d.projectid = c.projectid AND d.keyid = e.keyid AND c.name LIKE '%" & report_name & "%' AND c.clientname LIKE '%" & report_client & "%'AND c.state_id LIKE '%" & report_state & "%' AND c.projectID IN (select ipm_project.projectid from ipm_project, ipm_project_discipline where ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND keyid LIKE '%" & report_disc & "%') ORDER BY c.name"
	
	if(report_functional = "" and report_discipline = "") then
	'sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description from ipm_project_field_value a, ipm_project_field_desc b, ipm_project c WHERE a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_REGION' AND a.projectid = c.projectid AND c.publish = 1 " & report_favorite & "  AND item_value LIKE '%" & report_region & "%' AND c.name LIKE '%" & report_name & "%' AND c.clientname LIKE '%" & report_client & "%'AND c.state_id LIKE '%" & report_state & "%' ORDER BY c.name"
	sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description,c.descriptionmedium,url from ipm_project_field_desc b, ipm_project c LEFT JOIN ipm_project_field_value a ON a.projectid = c.projectid WHERE a.item_id = b.item_id  AND item_tag = 'IDAM_PROJECT_REGION'  " & report_published & " " & report_favorite & "  AND item_value LIKE '%" & report_region & "%' AND c.name LIKE '%" & report_name & "%' AND c.clientname LIKE '%" & report_client & "%'AND c.state_id LIKE '%" & report_state & "%' AND c.AVAILABLE = 'Y' ORDER BY c.name"
	elseif(report_functional <> "" and report_discipline = "") then
	sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description,c.descriptionmedium,url from ipm_project_field_desc b, ipm_project c LEFT JOIN ipm_project_field_value a ON a.projectid = c.projectid WHERE a.item_id = b.item_id  AND item_tag = 'IDAM_PROJECT_REGION'  " & report_published & " " & report_favorite & "  AND item_value LIKE '%" & report_region & "%' AND c.name LIKE '%" & report_name & "%' AND c.clientname LIKE '%" & report_client & "%'AND c.state_id LIKE '%" & report_state & "%' AND c.projectID IN (select ipm_project.projectid from ipm_project, ipm_project_discipline where ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND keyid LIKE '%" & report_functional & "%') AND c.AVAILABLE = 'Y' ORDER BY c.name"
	elseif(report_discipline <> "" and report_functional = "") then
	sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description,c.descriptionmedium,url from ipm_project_field_desc b, ipm_project c LEFT JOIN ipm_project_field_value a ON a.projectid = c.projectid WHERE a.item_id = b.item_id  AND item_tag = 'IDAM_PROJECT_REGION'  " & report_published & " " & report_favorite & "  AND item_value LIKE '%" & report_region & "%' AND c.name LIKE '%" & report_name & "%' AND c.clientname LIKE '%" & report_client & "%'AND c.state_id LIKE '%" & report_state & "%' AND c.projectID IN (select ipm_project.projectid from ipm_project, ipm_project_office where ipm_project.projectid = ipm_project_office.projectid AND AVAILABLE = 'Y' AND officeid LIKE '%" & report_discipline & "%') AND c.AVAILABLE = 'Y' ORDER BY c.name"
	else
	sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description,c.descriptionmedium,url from ipm_project_field_desc b, ipm_project c LEFT JOIN ipm_project_field_value a ON a.projectid = c.projectid WHERE a.item_id = b.item_id  AND item_tag = 'IDAM_PROJECT_REGION'  " & report_published & " " & report_favorite & "  AND item_value LIKE '%" & report_region & "%' AND c.name LIKE '%" & report_name & "%' AND c.clientname LIKE '%" & report_client & "%'AND c.state_id LIKE '%" & report_state & "%' AND c.projectID IN (select ipm_project.projectid from ipm_project, ipm_project_discipline where ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND keyid LIKE '%" & report_functional & "%') AND c.AVAILABLE = 'Y' AND c.projectID IN (select ipm_project.projectid from ipm_project, ipm_project_office where ipm_project.projectid = ipm_project_office.projectid AND AVAILABLE = 'Y' AND officeid LIKE '%" & report_discipline & "%') ORDER BY c.name"
	end if
	rsProjects.Open sql, Conn, 1, 4
	
	'Query for additional UDFs
	
	
	set rsUDF=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_desc where active = 1 order by item_name"
	rsUDF.Open sql, Conn, 1, 4
	



	
	'loop through and match checked inputs
	


	set rsLineUDF=server.createobject("adodb.recordset")
	set rsLineCaseStudies=server.createobject("adodb.recordset")
	set rsLineKeywords=server.createobject("adodb.recordset")
	
	
	end if
%>
<html>
<head>
<title>
Report
</title>
<style type="text/css">
body {
	font-size:10px;
}
td,th {
	
	border: 1px solid #777777;
	padding: 3px;
	text-align: center;
	vertical-align: top;
	font-size:10px;
}
table {
	border:1px solid #777777;
	border-collapse: collapse;
	padding:3px;
}
</style>
</head>
<body>


<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
<h4><%=report_title%></h4>
			<table width="100%">
			<!--begin table heading-->
			<tr>
				<th><b>Project</b></th>
				<%
				if(display_number = 1) then
				response.write "<th><b>Number</b></th>"
				end if
				if (display_client = 1) then
				response.write "<th><b>Owner</b></th>"
				end if
				if (display_address = 1) then
				response.write "<th><b>Address</b></th>"
				end if
				if (display_city_state = 1) then
				response.write "<th><b>Location</b></th>"
				end if
				if (display_zip = 1) then
				response.write "<th><b>ZIP</b></th>"
				end if				
				if (display_desc = 1) then
				response.write "<th><b>Summary</b></th>"
				end if
				if (display_mdesc = 1) then
				response.write "<th><b>Description</b></th>"
				end if
				if (display_url = 1) then
				response.write "<th><b>URL</b></th>"
				end if
				
			
				
				do while not rsUDF.eof
				itemTag = trim(rsUDF("item_tag"))
				if(Request.querystring(itemTag) = "1") then
				response.write "<th><b>" & rsUDF("item_name") & "</b></th>"
				end if
				
				rsUDF.moveNext
				loop
				
				if(displayDisc = 1) then
				response.write "<th><b>Discipline</b></th>"
				end if
				if(displayKeywords = 1) then
				response.write "<th><b>Keywords</b></th>"
				end if
				if(displayFunctional = 1) then
				response.write "<th><b>Functional Markets</b></th>"
				end if			
			
			if (displayChallenge = 1) then
			response.write "<th><b>Challenge</b></th>"
			end if
			if (displayApproach = 1) then
			response.write "<th><b>Approach</b></th>"
			end if	
			if (displaySolution = 1) then
			response.write "<th><b>Solution</b></th>"
			end if					
				
				%>
			</tr>
			<!--end table heading-->	

	<%
	i = 0
	do while not rsProjects.eof%>

		<!--Begin results listing-->
		
			
			
			<tr>
			<td><b><%=rsProjects("Name")%></b><%if (report_display_image = "1") then %><br /><img width="127" height="100" src="<%=sAssetPath%><%=rsProjects("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=127&height=100&qfactor=25" alt="<%=rsProjects("Name")%>" /><%end if%></td>
			<%
			if(display_number = 1) then
			response.write "<td>"& rsProjects("projectNumber")  &"&nbsp;</td>"
			end if
			if (display_client = 1) then
			response.write "<td>"& rsProjects("clientName")  &"&nbsp;</td>"
			end if
			if (display_address = 1) then
			response.write "<td>"& rsProjects("Address")  &"&nbsp;</td>"
			end if
			if (display_city_state = 1) then
			response.write "<td>"& rsProjects("city") & ", " & rsProjects("state_id")  &"&nbsp;</td>"
			end if
			if (display_zip = 1) then
			response.write "<td>"& rsProjects("zip")  &"&nbsp;</td>"
			end if			
			if (display_desc = 1) then
			response.write "<td>"& rsProjects("Description")  &"&nbsp;</td>"
			end if
			if (display_mdesc = 1) then
			response.write "<td>"& rsProjects("DescriptionMedium")  &"&nbsp;</td>"
			end if			
			
			rsUDF.moveFirst
			
			do while not rsUDF.eof
			itemTag = trim(rsUDF("item_tag"))
			
			if(Request.Querystring(itemTag) = "1") then
				
				if(rsLineUDF.State = 1) then
				rsLineUDF.close
				end if
				sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = '" & rsUDF("item_tag") & "'"
				rsLineUDF.Open sql, Conn, 1, 4
			
			if(rsLineUDF.recordCount > 0) then
			if(rsLineUDF("item_value") = "") then
			rsLineUDF("item_value") = "&nbsp;"
			end if
			response.write "<td>" & rsLineUDF("item_value") & "</td>"
			else
			response.write "<td>&nbsp;</td>"
			end if
			
			end if
			
			rsUDF.moveNext
			loop
			
''''Begin Keywords
				if(displayDisc = 1) then
					if(rsLineKeywords.State = 1) then
						rsLineKeywords.close
					end if
					sql = "select * from ipm_project_office a,ipm_office b where a.officeid = b.keyid and a.projectid = " & rsProjects("projectID") & " order by keyname"
					rsLineKeywords.Open sql, Conn, 1, 4
					
					if(rsLineKeywords.recordCount > 0) then
						lineKeywords = trim(rsLineKeywords("keyName"))
						rsLineKeywords.movenext
							do while not rsLineKeywords.eof
								lineKeywords = lineKeywords & ", " & trim(rsLineKeywords("keyName"))
								rsLineKeywords.movenext
							loop
					else
						lineKeywords = "&nbsp;"
					end if
					
					response.write "<td>" & lineKeywords & "</td>"
					
				end if
				if(displayKeywords = 1) then
					if(rsLineKeywords.State = 1) then
						rsLineKeywords.close
					end if
					sql = "select * from ipm_project_keyword a,ipm_keyword b where a.keyid = b.keyid and a.projectid = " & rsProjects("projectID") & " order by keyname"
					rsLineKeywords.Open sql, Conn, 1, 4
					
					if(rsLineKeywords.recordCount > 0) then
						lineKeywords = trim(rsLineKeywords("keyName"))
						rsLineKeywords.movenext
							do while not rsLineKeywords.eof
								lineKeywords = lineKeywords & ", " & trim(rsLineKeywords("keyName"))
								rsLineKeywords.movenext
							loop
					else
						lineKeywords = "&nbsp;"
					end if
					
					response.write "<td>" & lineKeywords & "</td>"					
				end if
				if(displayFunctional = 1) then
					if(rsLineKeywords.State = 1) then
						rsLineKeywords.close
					end if	
					sql = "select * from ipm_project_discipline a,ipm_discipline b where a.keyid = b.keyid and a.projectid = " & rsProjects("projectID") & " order by keyname"
					rsLineKeywords.Open sql, Conn, 1, 4
					
					if(rsLineKeywords.recordCount > 0) then
						lineKeywords = trim(rsLineKeywords("keyName"))
						rsLineKeywords.movenext
							do while not rsLineKeywords.eof
								lineKeywords = lineKeywords & ", " & trim(rsLineKeywords("keyName"))
								rsLineKeywords.movenext
							loop
					else
						lineKeywords = "&nbsp;"
					end if
					
					response.write "<td>" & lineKeywords & "</td>"					
				end if
			
			
			''''End Keywords
			
			''''Begin Case Studies
			
			if(displayChallenge = 1 or displayApproach = 1 or displaySolution = 1) then
			
			if(rsLineCaseStudies.State = 1) then
				rsLineCaseStudies.close
			end if
			sql = "select challenge,resolution,result from ipm_project where projectid = " & rsProjects("projectID")
			rsLineCaseStudies.Open sql, Conn, 1, 4

			if(rsLineCaseStudies("challenge") = "") then
				lineChallenge = "&nbsp;"
			else
				lineChallenge = rsLineCaseStudies("challenge")
			end if
			if(rsLineCaseStudies("resolution") = "") then
				lineApproach = "&nbsp;"
			else
				lineApproach = rsLineCaseStudies("resolution")
			end if
			if(rsLineCaseStudies("result") = "") then
				lineSolution = "&nbsp;"
			else
				lineSolution = rsLineCaseStudies("result")
			end if			
			
			if (displayChallenge = 1) then
			response.write "<td>" & lineChallenge &"</td>"
			end if
			if (displayApproach = 1) then
			response.write "<td>" & lineApproach &"</td>"
			end if	
			if (displaySolution = 1) then
			response.write "<td>" & lineSolution &"</td>"
			end if
			
			end if
			
			'''End Case Studies			
			
			response.write "</tr>"
			
			rsProjects.moveNext
			
			i = i + 1
			
			if(i mod report_page_break = 0 and not rsProjects.eof) then %>
			
			</table>
			
			<div style="page-break-before:always">&nbsp;</div> 

			<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
			<h4><%=report_title%></h4>
			<table width="100%">
			<!--begin table heading-->
			<tr>
				<th><b>Project</b></th>
				<%
				if(display_number = 1) then
				response.write "<th><b>Number</b></th>"
				end if
				if (display_client = 1) then
				response.write "<th><b>Owner</b></th>"
				end if
				if (display_address = 1) then
				response.write "<th><b>Address</b></th>"
				end if
				if (display_city_state = 1) then
				response.write "<th><b>Location</b></th>"
				end if
				if (display_zip = 1) then
				response.write "<th><b>ZIP</b></th>"
				end if				
				if (display_desc = 1) then
				response.write "<th><b>Summary</b></th>"
				end if
				if (display_mdesc = 1) then
				response.write "<th><b>Description</b></th>"
				end if
				if (display_url = 1) then
				response.write "<th><b>URL</b></th>"
				end if				
			
				rsUDF.moveFirst
				
				do while not rsUDF.eof
				itemTag = trim(rsUDF("item_tag"))
				if(Request.querystring(itemTag) = "1") then
				response.write "<th><b>" & rsUDF("item_name") & "</b></th>"
				end if
				
				rsUDF.moveNext
				loop
				
				if(displayDisc = 1) then
				response.write "<th><b>Discipline</b></th>"
				end if
				if(displayKeywords = 1) then
				response.write "<th><b>Keywords</b></th>"
				end if
				if(displayFunctional = 1) then
				response.write "<th><b>Functional Markets</b></th>"
				end if			
			
			if (displayChallenge = 1) then
			response.write "<th><b>Challenge</b></th>"
			end if
			if (displayApproach = 1) then
			response.write "<th><b>Approach</b></th>"
			end if	
			if (displaySolution = 1) then
			response.write "<th><b>Solution</b></th>"
			end if				
				
				%>
			</tr>
			<!--end table heading-->			
			<%
			
			end if			
			
			loop
			%>
		<!--End results listing-->
		</table>
	
</body>
</html>
