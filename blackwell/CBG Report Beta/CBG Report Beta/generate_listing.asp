<!--#include file="includes\config.asp" -->
<%
	projectID = prep_sql(request.querystring("pid"))
	set rsProject=server.createobject("adodb.recordset")
	sql="select a.name projectName,a.description,a.descriptionmedium,a.city,a.state_id,b.name state_name,a.address,a.zip  from ipm_project a LEFT JOIN ipm_state b ON a.state_id = b.state_id where AVAILABLE = 'Y' AND PUBLISH = 1 AND projectid = " & projectID
	rsProject.Open sql, Conn, 1, 4
Set theDoc = Server.CreateObject("ABCpdf6.Doc")

theDoc.HtmlOptions.PageCacheClear
theDoc.HtmlOptions.BrowserWidth = 640

theDoc.HtmlOptions.FontProtection = False
theDoc.HtmlOptions.FontSubstitute = False
theDoc.HtmlOptions.FontEmbed = True


theDoc.Rect.Inset -14, -18

theFont =  theDoc.EmbedFont("C:\WINDOWS\Fonts\TT0017M_.TTF", "Unicode", false, true, true)
theDoc.Font = theFont

'response.write theDoc.GetInfo(theFont, "/BaseFont:Name")

theDoc.AddImageUrl "http://idam.clarkrealty.com/PUBLIC/cbg/fact_sheet_html.asp?pid=" & projectID, 1



theDoc.Transform.Rotate 90,595,20

oriW = theDoc.Rect.Width
oriH = theDoc.Rect.Height

oriX = theDoc.Rect.Left
oriY = theDoc.Rect.Bottom


theDoc.Rect.Position 595,20
theDoc.Rect.Resize 15,16
theDoc.AddImageFile "C:\idam\BETAPUBLIC\cbg\images\pdf\corner.jpg",1

theDoc.Rect.Resize 200,200
theDoc.Pos.X = 610
theDoc.Pos.Y = 30
theDoc.addHTML "&nbsp; <font size=""1"" face=""Zapf Humanist 601 Bold BT"" color=""#996d50"">" & rsProject("projectName") & "</font>"

'Reset before adding page
theDoc.Rect.Resize oriW,oriH
theDoc.Rect.Position oriX,oriY

theDoc.Page = theDoc.AddPage()
theDoc.Transform.Reset
theDoc.Pos = "0 0"
theDoc.AddImageUrl "http://idam.clarkrealty.com/PUBLIC/cbg/fact_sheet_html_2.asp?pid=" & projectID


theDoc.Transform.Rotate 90,32,20

theDoc.Rect.Position 32,20
theDoc.Rect.Resize 15,16
theDoc.AddImageFile "C:\idam\BETAPUBLIC\cbg\images\pdf\corner_2.jpg",1
theDoc.Rect.Resize 200,200

theDoc.Pos.X = 50
theDoc.Pos.Y = 39
theDoc.addHTML "&nbsp; <font size=""1"" face=""Zapf Humanist 601 Bold BT"" color=""#996d50"">" & rsProject("projectName") & "</font>"

For i = 1 To theDoc.PageCount
  theDoc.PageNumber = i
  theDoc.Flatten
Next

theProjectName = trim(rsProject("projectName"))
theProjectName = replace(theProjectName," ","_")
theDoc.Save "C:\idam\BETAPUBLIC\cbg\pdf\" & theProjectName & "_Fact_Sheet.pdf"
Response.Redirect "http://idam.clarkrealty.com/PUBLIC/cbg/pdf/" & theProjectName & "_Fact_Sheet.pdf"

%>
