<!--#include file="includes\config.asp" -->
<%
	if request.form("submit") = "1" then
	
	'Pre-set parameters
	report_title = Request.Form("report_name")
	
	if(report_title = "") then
		report_title = "Report"
	end if
	
	theEmployees = prep_sql(Request.Form("search_employee"))
	theCompanies = "'" & replace(prep_sql(Request.Form("search_company")),",","','") & "'"

	
	if(Request.Form("search_filter") = "employee") then 'search by companies
		sql = "select * from ipm_user where userID in (" & theEmployees & ") and active = 'y' order by lastname"
	elseif(Request.Form("search_filter") = "company") then 'search by employees
		sql = "select * from ipm_user where agency in ('') and active = 'y' and userid <> 1 order by lastname"
	end if
	set rsEmployees = server.createobject("adodb.recordset")
	rsEmployees.Open sql, Conn, 1, 4
	
	
	'Query for additional UDFs
	
	
	set rsUDF=server.createobject("adodb.recordset")
	sql = "select * from ipm_user_field_desc where active = 1 order by item_name"
	rsUDF.Open sql, Conn, 1, 4
	'loop through and match checked inputs

	if(Request.Form("search_display")="basic") then 'basic
	display_firstname = 1
	display_lastname = 1
	display_image = 1
	display_title = 1
	display_company = 1
	display_bio = 1
	display_education = 1
	elseif(Request.Form("search_display")="detailed") then 'detailed
	display_firstname = Request.Form("firstname")
	display_lastname = Request.Form("lastname")
	display_image = Request.Form("image")
	display_title = Request.Form("title")
	display_company = Request.Form("company")
	display_bio = Request.Form("bio")
	display_education = Request.Form("education")
	
	display_security = Request.Form("security")
	display_phone = Request.Form("phone")
	display_cellphone = Request.Form("cellphone")
	display_email = Request.Form("email")
	end if
	
	report_output = request.form("report_output")
	set rsLineUDF=server.createobject("adodb.recordset")
	end if
%>
<%if (report_output = "html") then %>
<html>
<head>
<title>
Report
</title>
<style type="text/css">
td,th {
	
	border: 1px solid #777777;
	padding: 3px;
	text-align: center;
	vertical-align: top;
	font-family: verdana;
	font-size: 12px;
}
table {
	border:1px solid #777777;
	border-collapse: collapse;
	padding:3px;
}
th {
	background: #DDDDDD;
}
</style>
</head>
<body>
	<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
	<h2><%=report_title%></h2>
	<table width="100%">
		<!--begin table heading-->
		<tr>
			<%
			if(display_image = 1) then
			response.write "<th><b>Image</b></th>"
			end if
			if(display_firstname = 1) then
			response.write "<th><b>First Name</b></th>"
			end if
			if (display_lastname = 1) then
			response.write "<th><b>Last Name</b></th>"
			end if
			if (display_title = 1) then
			response.write "<th><b>Title</b></th>"
			end if
			if (display_company = 1) then
			response.write "<th><b>Company</b></th>"
			end if
			if (display_bio = 1) then
			response.write "<th><b>Biography</b></th>"
			end if				
			if (display_education = 1) then
			response.write "<th><b>Education</b></th>"
			end if
			if (display_security = 1) then
			response.write "<th><b>Security Level</b></th>"
			end if
			if (display_phone = 1) then
			response.write "<th><b>Phone</b></th>"
			end if
			if (display_cellphone = 1) then
			response.write "<th><b>Cellphone</b></th>"
			end if
			if (display_email = 1) then
			response.write "<th><b>E-mail</b></th>"
			end if			
			
			do while not rsUDF.eof
			itemTag = trim(rsUDF("item_tag"))
			if(Request.Form(itemTag) = "1") then
			response.write "<th><b>" & rsUDF("item_name") & "</b></th>"
			end if
			
			rsUDF.moveNext
			loop
			
			%>
		</tr>
		<!--end table heading-->
		<!--Begin results listing-->
		
			
			<%do while not rsEmployees.eof%>
			<tr>
			<%
			'''Begin Standard
			if(display_image = 1) then%>
			 <td align="center" valign="top"><img width="150" height="118" src="<%=sAssetPath%><%=rsEmployees("userID")%>&Instance=<%=siDAMInstance%>&type=user&size=1&width=150&height=118&qfactor=25" alt="<%=rsEmployees("lastname")%>" /></td>
			<%
			end if
			if(display_firstname = 1) then
			response.write "<td>" & rsEmployees("firstname") & "</td>"
			end if
			if (display_lastname = 1) then
			response.write "<td>" & rsEmployees("lastname") & "</td>"
			end if
			if (display_title = 1) then
			response.write "<td>" & rsEmployees("position") & "</td>"
			end if
			if (display_company = 1) then
			response.write "<td>" & rsEmployees("agency") & "</td>"
			end if
			if (display_bio = 1) then
			response.write "<td>" & rsEmployees("bio") & "</td>"
			end if				
			if (display_education = 1) then
			response.write "<td>" & rsEmployees("education") & "</td>"
			end if
			if (display_security = 1) then
			if(rsEmployees("securitylevel_id") = "0") then
			employeeSecurity = "Administrator"
			elseif(rsEmployees("securitylevel_id") = "1") then
			employeeSecurity = "Internal Use"
			elseif(rsEmployees("securitylevel_id") = "2") then
			employeeSecurity = "Client Use"
			elseif(rsEmployees("securitylevel_id") = "3") then
			employeeSecurity = "Public"
			else
			employeeSecurity = "N/A"
			end if
			response.write "<td>" & employeeSecurity & "</td>"
			end if
			if (display_phone = 1) then
			response.write "<td>" & rsEmployees("phone") & "</td>"
			end if
			if (display_cellphone = 1) then
			response.write "<td>" & rsEmployees("fax") & "</td>"
			end if
			if (display_email = 1) then
			response.write "<td>" & rsEmployees("email") & "</td>"
			end if			
			'''End Standard
			
			''''Begin UDFs
			
			rsUDF.moveFirst
			
			do while not rsUDF.eof
			itemTag = trim(rsUDF("item_tag"))
			
			if(Request.Form(itemTag) = "1") then
				
				if(rsLineUDF.State = 1) then
				rsLineUDF.close
				end if
				sql = "select * from ipm_user_field_value a, ipm_user_field_desc b WHERE user_id = " & rsEmployees("userID")  & " AND a.item_id = b.item_id AND item_tag = '" & rsUDF("item_tag") & "'"
				rsLineUDF.Open sql, Conn, 1, 4
			
			if(rsLineUDF.recordCount > 0) then
			if(rsLineUDF("item_value") = "") then
			rsLineUDF("item_value") = "&nbsp;"
			end if
			response.write "<td>" & rsLineUDF("item_value") & "</td>"
			else
			response.write "<td>&nbsp;</td>"
			end if
			
			end if
			
			rsUDF.moveNext
			loop
			''''End UDFs
			response.write "</tr>"
			rsEmployees.moveNext
			loop
			%>
		<!--End results listing-->
	</table>
</body>
</html>
<%elseif (report_output = "csv") then
newLine = chr(13) & chr(10)
dateToday = Now
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=" & replace(report_title," ","-") & "_-_" & DatePart( "yyyy", dateToday )  & "-" & DatePart( "m", dateToday ) & "-" & DatePart( "d", dateToday ) & ".csv"
Response.AddHeader "Content-Description", "File Transfer"

response.write report_title & newLine

			'begin header
			if(display_image = 1) then
			response.write "Image,"
			end if
			if(display_firstname = 1) then
			response.write "First Name,"
			end if
			if (display_lastname = 1) then
			response.write "Last Name,"
			end if
			if (display_title = 1) then
			response.write "Title,"
			end if
			if (display_company = 1) then
			response.write "Company,"
			end if
			if (display_bio = 1) then
			response.write "Biography,"
			end if				
			if (display_education = 1) then
			response.write "Education,"
			end if
			if (display_security = 1) then
			response.write "Security Level,"
			end if
			if (display_phone = 1) then
			response.write "Phone,"
			end if
			if (display_cellphone = 1) then
			response.write "Cellphone,"
			end if
			if (display_email = 1) then
			response.write "E-mail,"
			end if		


			
			do while not rsUDF.eof
			itemTag = trim(rsUDF("item_tag"))
			if(Request.Form(itemTag) = "1") then
			response.write """" & replace(rsUDF("item_name"),"""","""""") & """" & ","
			end if
			
			rsUDF.moveNext
			loop			
			
			response.write newLine
			'end header
			
			'begin listing
			do while not rsEmployees.eof
			if(display_firstname = 1) then
			response.write """" & replace(rsEmployees("firstname"),"""","""""") & """" & ","
			end if
			if (display_lastname = 1) then
			response.write """" & replace(rsEmployees("lastname"),"""","""""") & """" & ","
			end if
			if (display_title = 1) then
			response.write """" & replace(rsEmployees("position"),"""","""""") & """" & ","
			end if
			if (display_company = 1) then
			response.write """" & replace(rsEmployees("agency"),"""","""""") & """" & ","
			end if
			if (display_bio = 1) then
			response.write """" & replace(rsEmployees("bio"),"""","""""") & """" & ","
			end if				
			if (display_education = 1) then
			if(rsEmployees("education") <> "") then
			response.write """" & replace(rsEmployees("education"),"""","""""") & """" & ","
			end if
			end if
			if (display_security = 1) then
			if(rsEmployees("securitylevel_id") = "0") then
			employeeSecurity = "Administrator,"
			elseif(rsEmployees("securitylevel_id") = "1") then
			employeeSecurity = "Internal Use,"
			elseif(rsEmployees("securitylevel_id") = "2") then
			employeeSecurity = "Client Use,"
			elseif(rsEmployees("securitylevel_id") = "3") then
			employeeSecurity = "Public,"
			end if
			end if
			if (display_phone = 1) then
			response.write """" & replace(rsEmployees("phone"),"""","""""") & """" & ","
			end if
			if (display_cellphone = 1) then
			response.write """" & replace(rsEmployees("fax"),"""","""""") & """" & ","
			end if
			if (display_email = 1) then
			response.write """" & replace(rsEmployees("email"),"""","""""") & """" & ","
			end if				
			rsUDF.moveFirst
			
			do while not rsUDF.eof
			itemTag = trim(rsUDF("item_tag"))
			
			if(Request.Form(itemTag) = "1") then
				
				if(rsLineUDF.State = 1) then
				rsLineUDF.close
				end if
				sql = "select * from ipm_user_field_value a, ipm_user_field_desc b WHERE user_id = " & rsEmployees("userID")  & " AND a.item_id = b.item_id AND item_tag = '" & rsUDF("item_tag") & "'"
				rsLineUDF.Open sql, Conn, 1, 4
			
			if(rsLineUDF.recordCount > 0) then
			response.write """" & replace(replace(rsLineUDF("item_value"),"""",""""""),newLine," ") & """" & ","
			else
			response.write ","
			end if
			
			end if
			
			rsUDF.moveNext
			loop
			rsEmployees.moveNext
			response.write newLine
			loop
				
			
			
			'end listing
			
elseif (report_output = "pdf") then

Set theDoc = Server.CreateObject("ABCpdf6.Doc")
theDoc.HtmlOptions.Timeout = 120000

' apply a rotation transform
w = theDoc.MediaBox.Width
h = theDoc.MediaBox.Height
l = theDoc.MediaBox.Left
b = theDoc.MediaBox.Bottom 
theDoc.Transform.Rotate 90, l, b
theDoc.Transform.Translate w, 0

' rotate our rectangle
theDoc.Rect.Width = h
theDoc.Rect.Height = w

theDoc.Rect.Inset 36, 18
theDoc.Page = theDoc.AddPage()
theURL = "http://idam.clarkrealty.com:8081/report_employees_print_get.asp?" & Request.Form

theID = theDoc.AddImageUrl(theURL)

theDate = MonthName(DatePart("m",Date)) & " " & DatePart("d",Date) & ", "& DatePart("yyyy",Date)

Do
  If Not theDoc.Chainable(theID) Then Exit Do


  	theDoc.Pos.X = 670
	theDoc.Pos.Y = 7
	theDoc.addHTML  theDate
  
	theDoc.Pos.X = 400
	theDoc.Pos.Y = 7
	theDoc.addHTML "Page " & theDoc.PageNumber
	
	
	theDoc.Pos.X = 50
	theDoc.Pos.Y = 7
	theDoc.addHTML report_title

	'Reset before adding page
	'theDoc.Rect.Resize oriW,oriH
	'theDoc.Rect.Position oriX,oriY
  
  theDoc.Page = theDoc.AddPage()
  theID = theDoc.AddImageToChain(theID)
Loop

  If Not theDoc.Chainable(theID) Then
	theDoc.Pos.X = 670
	theDoc.Pos.Y = 7
	theDoc.addHTML  theDate
  
  
  	theDoc.Pos.X = 400
	theDoc.Pos.Y = 7
	theDoc.addHTML "Page " & theDoc.PageNumber
	
	theDoc.Pos.X = 50
	theDoc.Pos.Y = 7
	theDoc.addHTML report_title
  End if

For i = 1 To theDoc.PageCount
  theDoc.PageNumber = i
  theDoc.Flatten
Next
theDate = DatePart("m",Date) & "-" & DatePart("d",Date) & "-" & DatePart("yyyy",Date)
theProjectName = replace(report_title," ","_") & "_-_" & theDate &".pdf"
theDoc.Save "D:\Reporting Tool Generated PDF\" & theProjectName

FileExt = Mid(theProjectName, InStrRev(theProjectName, ".") + 1)

set stream = server.CreateObject("ADODB.Stream")
stream.type = "1"
stream.open
stream.LoadFromFile "D:\Reporting Tool Generated PDF\" & theProjectName

fileSize = stream.size

'response.write fileSize

response.clear
response.ContentType = "application/pdf"
Response.AddHeader "Content-Description", "File Transfer"
response.addheader "Content-Disposition", "attachment; filename=" & theProjectName
Response.AddHeader "Content-Length", fileSize

if(FileExt ="pdf") then
do while not(stream.eos)
response.BinaryWrite Stream.Read(8192)
response.Flush
loop
stream.Close
response.End
end if

'Response.Redirect "http://idam.clarkrealty.com/PUBLIC/cbg/pdf/" & theProjectName & ".pdf"

end if
%>