<!--#include file="includes\config.asp" -->
<%
	if request.form("submit") = "1" then
	
	'Pre-set parameters
	report_title = Request.Form("report_name")
	
	if(report_title = "") then
		report_title = "Awards Report"
	end if
	
	keywords = prep_sql(Request.Form("keywords"))
	awardsType = prep_sql(Request.Form("awards_type"))
	orgName = prep_sql(Request.Form("org_name"))
	relatedProject = prep_sql(Request.Form("related_project"))
	awardsYear = prep_sql(Request.Form("awards_years"))
	
	report_output = prep_sql(Request.Form("search_output"))
	report_display = prep_sql(Request.Form("search_display"))
	
	
	if(keywords <> "") then
		keywords_sql = " AND (headline like '%" & keywords & "%' OR publicationtitle like '%" & keywords & "%' OR content like '%" & keywords & "%' OR name like '%" & keywords & "%' OR e.item_value like '%" & keywords & "%' OR g.item_value like '%" & keywords & "%' OR i.item_value like '%" & keywords & "%')"
	end if

	if(awardsType <> "") then
		awardstype_sql = " AND type = " & awardsType
	end if
	
	if(orgName <> "") then
		orgname_sql = " AND g.item_value = '" & orgname & "'"
	end if

	if(relatedProject <> "") then
		relatedProject_sql = " AND c.projectid = " & relatedProject
	end if
	
	if(awardsYear <> "") then
		awards_sql = " AND year(post_date) = " & awardsYear
	end if		
	if (report_output <> "pdf") then
	'Search Query
	set rsAwards = server.createobject("adodb.recordset")
	
	if(Request.Form("search_display") = "detailed") then
	sql = "select distinct a.awards_id,headline,publicationtitle,e.item_value awards_level,post_date,pull_date,pdf,pdflinktitle,g.item_value org_name,k.item_value org_url,m.item_value rec_url,i.item_value rec_name from ipm_awards_field_desc d,ipm_awards_field_value e,ipm_awards_field_desc f,ipm_awards_field_value g,ipm_awards_field_desc h,ipm_awards_field_value i, ipm_awards_field_desc j, ipm_awards_field_value k, ipm_awards_field_desc l, ipm_awards_field_value m,ipm_awards a left join ipm_awards_related_projects b on a.awards_id = b.awards_id left join ipm_project c on b.projectid = c.projectid where a.show = 1 and d.item_tag = 'IDAM_AWARDS_Level' and f.item_tag = 'IDAM_AWARDS_ORGANIZATION_NAME' and h.item_tag = 'IDAM_AWARDS_RECIPIENT_NAME' and d.item_id = e.item_id and f.item_id = g.item_id and h.item_id = i.item_id and e.awards_id = a.awards_id and g.awards_id = a.awards_id and i.awards_id = a.awards_id and j.item_id = k.item_id and l.item_id = m.item_id and j.item_tag = 'IDAM_ORGANIZATION_URL' and l.item_tag = 'IDAM_AWARDS_RECIPIENT_URL' and k.awards_id = a.awards_id and m.awards_id = a.awards_id " & keywords_sql & awardstype_sql & orgname_sql & relatedproject_sql & awards_sql & " order by headline"
	else
	sql = "select distinct a.awards_id,headline,publicationtitle,e.item_value awards_level from ipm_awards_field_desc d,ipm_awards_field_value e,ipm_awards_field_desc f,ipm_awards_field_value g,ipm_awards_field_desc h,ipm_awards_field_value i,ipm_awards a left join ipm_awards_related_projects b on a.awards_id = b.awards_id left join ipm_project c on b.projectid = c.projectid where a.show = 1 and d.item_tag = 'IDAM_AWARDS_Level' and f.item_tag = 'IDAM_AWARDS_ORGANIZATION_NAME' and h.item_tag = 'IDAM_AWARDS_RECIPIENT_NAME' and d.item_id = e.item_id and f.item_id = g.item_id and h.item_id = i.item_id and e.awards_id = a.awards_id and g.awards_id = a.awards_id and i.awards_id = a.awards_id " & keywords_sql & awardstype_sql & orgname_sql & relatedproject_sql & awards_sql & " order by headline"
	end if
	
	
	rsAwards.Open sql, Conn, 1, 4
	
	set rsLineRelatedProjects = server.createobject("adodb.recordset")
	set rsLineContent = server.createobject("adodb.recordset")
	end if
	
	end if
%>
<%if (report_output = "html") then %>
<html>
<head>
<title>
Report
</title>
<style type="text/css">
td,th {
	
	border: 1px solid #777777;
	padding: 5px;
	text-align: left;
	vertical-align: top;
	font-family: verdana;
	font-size: 12px;
}
table {
	border:1px solid #777777;
	border-collapse: collapse;
}
th {
	background: #DDDDDD;
}
</style>
</head>
<body>
	<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
	<h2><%=report_title%></h2>
	<table width="100%">
		<!--begin table heading-->
		<tr>
		<%if(Request.Form("search_display") = "detailed") then%>
			<th><b>Image</b></th>
			<th><b>Name</b></th>
			<th><b>Category</b></th>
			<th><b>Description</b></th>
			<th><b>Award Level</b></th>
			<th><b>Related Project(s)</b></th>
			<th><b>Additional Info</b></th>
			<th><b>Award Date</b></th>
			<th><b>Pull Date</b></th>
			<th><b>PDF Title</b></th>
			<th><b>PDF File</b></th>
			<th><b>Organization Name</b></th>
			<th><b>Organization URL</b></th>
			<th><b>Recipient Name</b></th>
			<th><b>Recipient URL</b></th>
		<%else%>
			<th><b>Image</b></th>
			<th><b>Name</b></th>
			<th><b>Category</b></th>
			<th><b>Description</b></th>
			<th><b>Award Level</b></th>
			<th><b>Related Project(s)</b></th>
		<%end if%>
		</tr>
		<!--end table heading-->
		<!--Begin results listing-->
		
			
			<%do while not rsAwards.eof
			
				if(rsLineContent.State = 1) then
					rsLineContent.close
				end if
				
				sql = "select content,contact from ipm_awards where awards_id = " & rsAwards("awards_id")
				rsLineContent.Open sql, Conn, 1, 4
				
				if(rsLineRelatedProjects.State = 1) then
					rsLineRelatedProjects.close
				end if

				sql = "select * from ipm_awards_related_projects, ipm_project where ipm_project.projectid =  ipm_awards_related_projects.projectid and ipm_awards_related_projects.awards_id = " & rsAwards("awards_id")

				rsLineRelatedProjects.Open sql, Conn, 1, 4
				
				if(rsLineRelatedProjects.recordcount > 0) then
					lineRelatedProjects = rsLineRelatedProjects("name")
					rsLineRelatedProjects.moveNext
					
					do while not rsLineRelatedProjects.eof
					lineRelatedProjects = lineRelatedProjects & ", " & rsLineRelatedProjects("name")
					rsLineRelatedProjects.movenext
					loop
				else
					lineRelatedProjects = ""
				end if				
				
			%>
			<tr>
			
				

			<%if(Request.Form("search_display") = "detailed") then
				if(rsAwards("PDF") = 1) then 
					pdfAvailable = "Yes"
				else
					pdfAvailable = "No"
				end if


				
			%>
			
				<td align="left"><img width="109" height="115" src="<%=sAssetPath%><%=RsAwards("Awards_ID")%>&Instance=<%=siDAMInstance%>&type=awards&size=2" alt="<%=RsAwards("Headline")%>" /></td>
				<td align="left"><%=trim(RsAwards("headline"))%></td>
				<td align="left"><%=trim(RsAwards("publicationtitle"))%></td>
				<td align="left"><%=replace(rsLineContent("content"),VbCrLf,"<br />")%></td>
				<td align="left"><%=trim(RsAwards("awards_level"))%></td>
				<td align="left"><%=lineRelatedProjects%></td>
				<td align="left"><%=trim(rsLineContent("contact"))%></td>
				<td align="left"><%=RsAwards("post_date")%></td>
				<td align="left"><%=RsAwards("pull_date")%></td>
				<td align="left"><%=trim(RsAwards("PDFLinkTitle"))%></td>
				<td align="left"><%=pdfAvailable%></td>
				<td align="left"><%=trim(RsAwards("org_name"))%></td>
				<td align="left"><%=trim(RsAwards("org_url"))%></td>
				<td align="left"><%=trim(RsAwards("rec_name"))%></td>
				<td align="left"><%=trim(RsAwards("rec_url"))%></td>
				
			<%else%>			
				<td align="left"><img width="109" height="115" src="<%=sAssetPath%><%=RsAwards("Awards_ID")%>&Instance=<%=siDAMInstance%>&type=awards&size=2" alt="<%=RsAwards("Headline")%>" /></td>
				<td align="left"><%=trim(RsAwards("headline"))%></td>
				<td align="left"><%=trim(RsAwards("publicationtitle"))%></td>
				<td align="left"><%=replace(rsLineContent("content"),VbCrLf,"<br />")%></td>
				<td align="left"><%=trim(RsAwards("awards_level"))%></td>
				<td align="left"><%=lineRelatedProjects%></td>
			<%end if%>
			</tr>
			<%
			RsAwards.moveNext
			loop
			%>
		<!--End results listing-->
	</table>
</body>
</html>
<%elseif (report_output = "csv") then

newLine = chr(13) & chr(10)
dateToday = Now
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=" & replace(report_title," ","-") & "_-_" & DatePart( "yyyy", dateToday )  & "-" & DatePart( "m", dateToday ) & "-" & DatePart( "d", dateToday ) & ".csv"
Response.AddHeader "Content-Description", "File Transfer"

response.write report_title & newLine



			if(Request.Form("search_display") = "detailed") then
			response.write "Name,Category,Description,Award Level,Related Project(s),Additional Info,Award Date,Pull Date,PDF Title,PDF File,Organization Name,Organization URL,Recipient Name,Recipient URL"
			else
			response.write "Name,Category,Description,Award Level,Related Project(s)"
			end if
			response.write newLine
			'end header
			
			'begin listing
			do while not rsAwards.eof
			
				if(rsLineContent.State = 1) then
					rsLineContent.close
				end if
				
				sql = "select content,contact from ipm_awards where awards_id = " & rsAwards("awards_id")
				rsLineContent.Open sql, Conn, 1, 4
				
				if(rsLineRelatedProjects.State = 1) then
					rsLineRelatedProjects.close
				end if

				sql = "select * from ipm_awards_related_projects, ipm_project where ipm_project.projectid =  ipm_awards_related_projects.projectid and ipm_awards_related_projects.awards_id = " & rsAwards("awards_id")

				rsLineRelatedProjects.Open sql, Conn, 1, 4
				
				if(rsLineRelatedProjects.recordcount > 0) then
					lineRelatedProjects = rsLineRelatedProjects("name")
					rsLineRelatedProjects.moveNext
					
					do while not rsLineRelatedProjects.eof
					lineRelatedProjects = lineRelatedProjects & ", " & rsLineRelatedProjects("name")
					rsLineRelatedProjects.movenext
					loop
				else
					lineRelatedProjects = ""
				end if							

			if(Request.Form("search_display") = "detailed") then
			

				if(rsAwards("PDF") = 1) then 
					pdfAvailable = "Yes"
				else
					pdfAvailable = "No"
				end if
				
				response.write """" & replace(replace(trim(RsAwards("headline")),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(trim(RsAwards("publicationtitle")),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(replace(rsLineContent("content"),VbCrLf,"<br />"),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(trim(RsAwards("awards_level")),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(lineRelatedProjects,"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(trim(rsLineContent("contact")),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(RsAwards("post_date"),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(RsAwards("pull_date"),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(trim(RsAwards("PDFLinkTitle")),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(pdfAvailable,"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(trim(RsAwards("org_name")),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(trim(RsAwards("org_url")),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(trim(RsAwards("rec_name")),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(trim(RsAwards("rec_url")),"""",""""""),newLine," ") & """" & ","
							
			else
				response.write """" & replace(replace(trim(RsAwards("headline")),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(trim(RsAwards("publicationtitle")),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(replace(rsLineContent("content"),VbCrLf,"<br />"),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(trim(RsAwards("awards_level")),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(lineRelatedProjects,"""",""""""),newLine," ") & """" & ","				
			end if
			
			rsAwards.moveNext
			response.write newLine
			loop
				
			
			
			'end listing
			
elseif (report_output = "pdf") then

Set theDoc = Server.CreateObject("ABCpdf6.Doc")
theDoc.HtmlOptions.Timeout = 120000

if(Request.Form("search_display") = "detailed") then
'apply a rotation transform
w = theDoc.MediaBox.Width
h = theDoc.MediaBox.Height
l = theDoc.MediaBox.Left
b = theDoc.MediaBox.Bottom 
theDoc.Transform.Rotate 90, l, b
theDoc.Transform.Translate w, 0

'rotate our rectangle
theDoc.Rect.Width = h
theDoc.Rect.Height = w
end if

theDoc.Rect.Inset 36, 18
theDoc.Page = theDoc.AddPage()
theURL = "http://idam.clarkrealty.com:8081/awards_print_get.asp?" & Request.Form
response.write theURL


theID = theDoc.AddImageUrl(theURL)

theDate = MonthName(DatePart("m",Date)) & " " & DatePart("d",Date) & ", "& DatePart("yyyy",Date)

Do
  If Not theDoc.Chainable(theID) Then Exit Do

  	if(Request.Form("search_display") = "detailed") then
  	theDoc.Pos.X = 670
	theDoc.Pos.Y = 7
	theDoc.addHTML  theDate
  
	theDoc.Pos.X = 400
	theDoc.Pos.Y = 7
	theDoc.addHTML "Page " & theDoc.PageNumber
	
	
	theDoc.Pos.X = 50
	theDoc.Pos.Y = 7
	theDoc.addHTML report_title	
	else
  	theDoc.Pos.X = 500
	theDoc.Pos.Y = 7
	theDoc.addHTML  theDate
  
	theDoc.Pos.X = 300
	theDoc.Pos.Y = 7
	theDoc.addHTML "Page " & theDoc.PageNumber
	
	
	theDoc.Pos.X = 40
	theDoc.Pos.Y = 7
	theDoc.addHTML report_title
	end if
	
	'Reset before adding page
	'theDoc.Rect.Resize oriW,oriH
	'theDoc.Rect.Position oriX,oriY
  
  theDoc.Page = theDoc.AddPage()
  theID = theDoc.AddImageToChain(theID)
Loop

  If Not theDoc.Chainable(theID) Then
	if(Request.Form("search_display") = "detailed") then
  	theDoc.Pos.X = 670
	theDoc.Pos.Y = 7
	theDoc.addHTML  theDate
  
	theDoc.Pos.X = 400
	theDoc.Pos.Y = 7
	theDoc.addHTML "Page " & theDoc.PageNumber
	
	
	theDoc.Pos.X = 50
	theDoc.Pos.Y = 7
	theDoc.addHTML report_title	
	else
  	theDoc.Pos.X = 500
	theDoc.Pos.Y = 7
	theDoc.addHTML  theDate
  
	theDoc.Pos.X = 300
	theDoc.Pos.Y = 7
	theDoc.addHTML "Page " & theDoc.PageNumber
	
	
	theDoc.Pos.X = 40
	theDoc.Pos.Y = 7
	theDoc.addHTML report_title
	end if
  End if

For i = 1 To theDoc.PageCount
  theDoc.PageNumber = i
  theDoc.Flatten
Next
theDate = DatePart("m",Date) & "-" & DatePart("d",Date) & "-" & DatePart("yyyy",Date)
theProjectName = replace(report_title," ","_") & "_-_" & theDate &".pdf"
theDoc.Save "D:\Reporting Tool Generated PDF\" & theProjectName

FileExt = Mid(theProjectName, InStrRev(theProjectName, ".") + 1)

set stream = server.CreateObject("ADODB.Stream")
stream.type = "1"
stream.open
stream.LoadFromFile "D:\Reporting Tool Generated PDF\" & theProjectName

fileSize = stream.size

'response.write fileSize

response.clear
response.ContentType = "application/pdf"
Response.AddHeader "Content-Description", "File Transfer"
response.addheader "Content-Disposition", "attachment; filename=" & theProjectName
Response.AddHeader "Content-Length", fileSize

if(FileExt ="pdf") then
do while not(stream.eos)
response.BinaryWrite Stream.Read(8192)
response.Flush
loop
stream.Close
response.End
end if

'Response.Redirect "http://idam.clarkrealty.com/PUBLIC/cbg/pdf/" & theProjectName & ".pdf"

end if
%>
