$(document).ready(function() {


    
    $('.sub_menu a').each(function () {
     if( $(this).attr('href')==getPage() ) $(this).addClass('active'); 
    });

    $('#footer .footer_lists dl dd a').each(function () {
     if( $(this).attr('href')==getPage() ) $(this).addClass('active'); 
    });

    $('#menu a').each(function () {
     if( $(this).attr('href')==getPage() ) $(this).addClass('active'); 
    });

    $('#menu a.active').each(function () {
     $(this).parent().parent().prev().addClass('active');
    });




    //carousel settins
    $('#carousel').cycle({
        fx:     'fade',
        speed:   1200,
        timeout: 5000,
        pager:  '#nav'
    });
    
    $('#siteSearch').val('SITE SEARCH');
    $('#siteSearch').focus(function () {
         $(this).val('');
    });
    $('#siteSearch').blur(function () {
         if(!$(this).val()) $(this).val('SITE SEARCH');
    });


    $('.timeline ul').jcarousel();
    
    //style for dropdowns
    //$('.selectbox').selectbox();   
 
    //search tabs
    $('#rent').bind("click", function(){
        
        $(this).addClass('current');
        $('#sale').removeClass('current');
        $('#sale_box').hide();
        $('#rent_box').show();
    });
    $('#sale').bind("click", function(){
        
        $(this).addClass('current');
        $('#rent').removeClass('current');
        $('#rent_box').hide();
        $('#sale_box').show();
    });    
    
    //toggle search filters
    $('#search_button').bind("click", function(e){
        
        e.preventDefault();
        
        var heading = $('.search_heading');
        var dropdowns = $('.search_filters');
        
        if( heading.length > 0){
            heading.hide();
            dropdowns.show();
        }
    });
    
    // scroll top
    $('.go_top').live('click',function(){
        $('html').animate({
            scrollTop:0
        }, 'slow');
    });    
    
    //swtich between list/map view
    $('#list').bind("click", function(){
        
        $(this).addClass('active');
        $('#map').removeClass('active');
        $('#map_view').hide();
        $('#list_view').show();
    });
    $('#map').bind("click", function(){
        
        $(this).addClass('active');
        $('#list').removeClass('active');
        $('#list_view').hide();
        $('#map_view').show();
    });  
    
    //custom select
    $('.selectFancy').hover(
    function () {
        $(this).find('em').addClass('selected');
        $(this).find('span').slideDown();
    }, 
    function () {
        $(this).find('em').removeClass('selected');
        $(this).find('span').slideUp();
    }
    );    
});

function getPage () {
 var nPage = window.location.pathname;
 nPage = nPage.substring(nPage.lastIndexOf('/') + 1);
 return nPage;
}


function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}


function eraseCookie(name) {
	createCookie(name,"",-1);
}
