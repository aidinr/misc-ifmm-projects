$(document).ready(function() {
    if ($('.accordion').html()) {
        $('.accordionButton').click(function() {
            $('.accordionButton').removeClass('on');
            $('.accordionContent').slideUp(500);
            eraseCookie(String('Accordian'+getPage()).replace(/[^a-zA-Z0-9]+/g,''));
            if ($(this).next().is(':hidden') == true) {
                $(this).addClass('on');
                $(this).next().slideDown('normal');
                createCookie( String('Accordian'+getPage()).replace(/[^a-zA-Z0-9]+/g,''), 'A'+String($(this).index('.accordionButton'))  , 20)
            }
        });
        $('.accordionContent').hide();
        var a = readCookie (String('Accordian'+getPage()).replace(/[^a-zA-Z0-9]+/g,''));
        if(a) a = Math.round(a.substr(1));
        if(!a || a <0) a = 0;
        $(".accordionButton:eq("+a+")").addClass('on');
        $(".accordionContent:eq("+a+")").css('display', 'block');
        
    }
});