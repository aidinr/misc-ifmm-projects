Imports System.Data.OleDb
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions




Public Class Form1
    Inherits System.Windows.Forms.Form


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub


    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ConnectionStringIDAM As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
    Public ConnectionStringUSHMMPHOTOARCHIVE As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringUSHMMPHOTOARCHIVE")
    Public ConnectionStringUSHMMPHOTOARCHIVESOURCE As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringUSHMMPHOTOARCHIVESOURCE")
    Public IDAMUSHMMTables As String = ConfigurationSettings.AppSettings("IDAM.USHMMTables")
    Public REFRESHFROMSOURCE As String = ConfigurationSettings.AppSettings("IDAM.REFRESHFROMSOURCE")
    Public ConnectionStringUSHMMPHOTOARCHIVESQLVERSION As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringUSHMMPHOTOARCHIVESQLVERSION")
    Public IDAMROOT As String = ConfigurationSettings.AppSettings("IDAM.IDAMROOT")
    Public USHMMHIREZROOT As String = ConfigurationSettings.AppSettings("IDAM.USHMMHIREZROOT")
    Public REFRESHINDEXES As String = ConfigurationSettings.AppSettings("IDAM.REFRESHINDEXES")
    Public REBUILDINDEXES As String = ConfigurationSettings.AppSettings("IDAM.REBUILDINDEXES")




    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents chkUpdateOnly As System.Windows.Forms.CheckBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents Button20 As System.Windows.Forms.Button
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents Button22 As System.Windows.Forms.Button
    Friend WithEvents Button23 As System.Windows.Forms.Button
    Friend WithEvents Button24 As System.Windows.Forms.Button
    Friend WithEvents Button25 As System.Windows.Forms.Button
    Friend WithEvents Button26 As System.Windows.Forms.Button
    Friend WithEvents Button27 As System.Windows.Forms.Button
    Friend WithEvents Button28 As System.Windows.Forms.Button
    Friend WithEvents Button29 As System.Windows.Forms.Button
    Friend WithEvents Button31 As System.Windows.Forms.Button
    Friend WithEvents Button30 As System.Windows.Forms.Button
    Friend WithEvents Button32 As System.Windows.Forms.Button
    Friend WithEvents Button33 As System.Windows.Forms.Button
    Friend WithEvents Button34 As System.Windows.Forms.Button
    Friend WithEvents Button35 As System.Windows.Forms.Button
    Friend WithEvents TextBoxbaddesignation As System.Windows.Forms.TextBox
    Friend WithEvents Button36 As System.Windows.Forms.Button
    Friend WithEvents Button19 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.ListView1 = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.Button2 = New System.Windows.Forms.Button
        Me.chkUpdateOnly = New System.Windows.Forms.CheckBox
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.Button9 = New System.Windows.Forms.Button
        Me.Button10 = New System.Windows.Forms.Button
        Me.Button11 = New System.Windows.Forms.Button
        Me.Button12 = New System.Windows.Forms.Button
        Me.Button13 = New System.Windows.Forms.Button
        Me.Button14 = New System.Windows.Forms.Button
        Me.Button15 = New System.Windows.Forms.Button
        Me.Button16 = New System.Windows.Forms.Button
        Me.Button17 = New System.Windows.Forms.Button
        Me.Button18 = New System.Windows.Forms.Button
        Me.Button19 = New System.Windows.Forms.Button
        Me.Button20 = New System.Windows.Forms.Button
        Me.Button21 = New System.Windows.Forms.Button
        Me.Button22 = New System.Windows.Forms.Button
        Me.Button23 = New System.Windows.Forms.Button
        Me.Button24 = New System.Windows.Forms.Button
        Me.Button25 = New System.Windows.Forms.Button
        Me.Button26 = New System.Windows.Forms.Button
        Me.Button27 = New System.Windows.Forms.Button
        Me.Button28 = New System.Windows.Forms.Button
        Me.Button29 = New System.Windows.Forms.Button
        Me.Button31 = New System.Windows.Forms.Button
        Me.Button30 = New System.Windows.Forms.Button
        Me.Button32 = New System.Windows.Forms.Button
        Me.Button33 = New System.Windows.Forms.Button
        Me.Button34 = New System.Windows.Forms.Button
        Me.Button35 = New System.Windows.Forms.Button
        Me.TextBoxbaddesignation = New System.Windows.Forms.TextBox
        Me.Button36 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(192, 16)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(208, 32)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "ExecuteUSHMM Data Extraction"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(16, 368)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(472, 23)
        Me.ProgressBar1.TabIndex = 2
        '
        'ListView1
        '
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.ListView1.FullRowSelect = True
        Me.ListView1.Location = New System.Drawing.Point(16, 56)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(472, 304)
        Me.ListView1.TabIndex = 3
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "ProjectName"
        Me.ColumnHeader1.Width = 500
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(136, 416)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(232, 32)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Move Processed to Repository"
        '
        'chkUpdateOnly
        '
        Me.chkUpdateOnly.Location = New System.Drawing.Point(408, 24)
        Me.chkUpdateOnly.Name = "chkUpdateOnly"
        Me.chkUpdateOnly.Size = New System.Drawing.Size(112, 16)
        Me.chkUpdateOnly.TabIndex = 5
        Me.chkUpdateOnly.Text = "Update Only"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(16, 16)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(96, 32)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "Pre-Process FileListing"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(16, 416)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(88, 32)
        Me.Button4.TabIndex = 7
        Me.Button4.Text = "Add Asset Overrides"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(16, 476)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(96, 32)
        Me.Button5.TabIndex = 8
        Me.Button5.Text = "Project UDFS"
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(136, 480)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(232, 32)
        Me.Button6.TabIndex = 9
        Me.Button6.Text = "Project Plate Upload"
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(-14, 529)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 10
        Me.Button7.Text = "Button7"
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(147, 576)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(304, 40)
        Me.Button8.TabIndex = 11
        Me.Button8.Text = "Remap Assets / projects /alliances"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(80, 656)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(288, 32)
        Me.Button9.TabIndex = 12
        Me.Button9.Text = "Remap Project Plates"
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(96, 520)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(224, 40)
        Me.Button10.TabIndex = 13
        Me.Button10.Text = "Processed newly added assets"
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(12, 593)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(112, 48)
        Me.Button11.TabIndex = 14
        Me.Button11.Text = "Button11"
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(66, 694)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(208, 48)
        Me.Button12.TabIndex = 15
        Me.Button12.Text = "Update Security Permissions"
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(504, 56)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(224, 40)
        Me.Button13.TabIndex = 16
        Me.Button13.Text = "Step 1: Create Categories"
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(504, 112)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(224, 40)
        Me.Button14.TabIndex = 17
        Me.Button14.Text = "Step 2: Create Projects (WorkSheets)"
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(504, 168)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(224, 40)
        Me.Button15.TabIndex = 18
        Me.Button15.Text = "Step 3: Create Project Folders"
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(504, 232)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(224, 40)
        Me.Button16.TabIndex = 19
        Me.Button16.Text = "Step 4: Create Assets"
        '
        'Button17
        '
        Me.Button17.Location = New System.Drawing.Point(504, 296)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(224, 40)
        Me.Button17.TabIndex = 20
        Me.Button17.Text = "Step 5: Add Keywords and Tags"
        '
        'Button18
        '
        Me.Button18.Location = New System.Drawing.Point(504, 351)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(224, 40)
        Me.Button18.TabIndex = 21
        Me.Button18.Text = "Step 6: Add MainTable Values"
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(504, 690)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(224, 40)
        Me.Button19.TabIndex = 22
        Me.Button19.Text = "Step 7: Add Files Low Rez"
        '
        'Button20
        '
        Me.Button20.Location = New System.Drawing.Point(504, 412)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(224, 40)
        Me.Button20.TabIndex = 23
        Me.Button20.Text = "Step User Import from Fornames"
        '
        'Button21
        '
        Me.Button21.Location = New System.Drawing.Point(504, 520)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(224, 40)
        Me.Button21.TabIndex = 24
        Me.Button21.Text = "Add Invoice Carousels and Car Items"
        '
        'Button22
        '
        Me.Button22.Location = New System.Drawing.Point(504, 468)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(224, 40)
        Me.Button22.TabIndex = 25
        Me.Button22.Text = "Step User Import from Institut"
        '
        'Button23
        '
        Me.Button23.Location = New System.Drawing.Point(504, 576)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(224, 40)
        Me.Button23.TabIndex = 26
        Me.Button23.Text = "Add Invoiceto Orders"
        '
        'Button24
        '
        Me.Button24.Location = New System.Drawing.Point(504, 633)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(224, 40)
        Me.Button24.TabIndex = 27
        Me.Button24.Text = "Add OrderItems"
        '
        'Button25
        '
        Me.Button25.Location = New System.Drawing.Point(326, 520)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(148, 40)
        Me.Button25.TabIndex = 28
        Me.Button25.Text = "Add DownloadTypes"
        '
        'Button26
        '
        Me.Button26.Location = New System.Drawing.Point(734, 249)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(146, 73)
        Me.Button26.TabIndex = 29
        Me.Button26.Text = "Step 4b: Update Assets with Source ID"
        '
        'Button27
        '
        Me.Button27.Location = New System.Drawing.Point(746, 56)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(125, 40)
        Me.Button27.TabIndex = 30
        Me.Button27.Text = "ODBC Test"
        Me.Button27.UseVisualStyleBackColor = True
        '
        'Button28
        '
        Me.Button28.Location = New System.Drawing.Point(746, 112)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(125, 42)
        Me.Button28.TabIndex = 31
        Me.Button28.Text = "Update Assets"
        Me.Button28.UseVisualStyleBackColor = True
        '
        'Button29
        '
        Me.Button29.Location = New System.Drawing.Point(378, 468)
        Me.Button29.Name = "Button29"
        Me.Button29.Size = New System.Drawing.Size(96, 32)
        Me.Button29.TabIndex = 32
        Me.Button29.Text = "Process Attributes Only"
        '
        'Button31
        '
        Me.Button31.Location = New System.Drawing.Point(274, 698)
        Me.Button31.Name = "Button31"
        Me.Button31.Size = New System.Drawing.Size(224, 40)
        Me.Button31.TabIndex = 34
        Me.Button31.Text = "Copy Tables"
        '
        'Button30
        '
        Me.Button30.Location = New System.Drawing.Point(734, 520)
        Me.Button30.Name = "Button30"
        Me.Button30.Size = New System.Drawing.Size(146, 210)
        Me.Button30.TabIndex = 35
        Me.Button30.Text = "Order Import"
        Me.Button30.UseVisualStyleBackColor = True
        '
        'Button32
        '
        Me.Button32.Location = New System.Drawing.Point(734, 335)
        Me.Button32.Name = "Button32"
        Me.Button32.Size = New System.Drawing.Size(146, 73)
        Me.Button32.TabIndex = 36
        Me.Button32.Text = "Reaquire All Assets with Check"
        '
        'Button33
        '
        Me.Button33.Location = New System.Drawing.Point(886, 368)
        Me.Button33.Name = "Button33"
        Me.Button33.Size = New System.Drawing.Size(146, 73)
        Me.Button33.TabIndex = 37
        Me.Button33.Text = "test logging"
        '
        'Button34
        '
        Me.Button34.Location = New System.Drawing.Point(886, 56)
        Me.Button34.Name = "Button34"
        Me.Button34.Size = New System.Drawing.Size(146, 73)
        Me.Button34.TabIndex = 38
        Me.Button34.Text = "test bulkcopy table deletion and creation"
        '
        'Button35
        '
        Me.Button35.Location = New System.Drawing.Point(886, 147)
        Me.Button35.Name = "Button35"
        Me.Button35.Size = New System.Drawing.Size(146, 50)
        Me.Button35.TabIndex = 39
        Me.Button35.Text = "CheckDesignation"
        '
        'TextBoxbaddesignation
        '
        Me.TextBoxbaddesignation.Location = New System.Drawing.Point(931, 468)
        Me.TextBoxbaddesignation.Multiline = True
        Me.TextBoxbaddesignation.Name = "TextBoxbaddesignation"
        Me.TextBoxbaddesignation.Size = New System.Drawing.Size(239, 166)
        Me.TextBoxbaddesignation.TabIndex = 40
        '
        'Button36
        '
        Me.Button36.Location = New System.Drawing.Point(886, 222)
        Me.Button36.Name = "Button36"
        Me.Button36.Size = New System.Drawing.Size(146, 50)
        Me.Button36.TabIndex = 41
        Me.Button36.Text = "Reaquire Some Assets"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1239, 856)
        Me.Controls.Add(Me.Button36)
        Me.Controls.Add(Me.TextBoxbaddesignation)
        Me.Controls.Add(Me.Button35)
        Me.Controls.Add(Me.Button34)
        Me.Controls.Add(Me.Button33)
        Me.Controls.Add(Me.Button32)
        Me.Controls.Add(Me.Button30)
        Me.Controls.Add(Me.Button31)
        Me.Controls.Add(Me.Button29)
        Me.Controls.Add(Me.Button28)
        Me.Controls.Add(Me.Button27)
        Me.Controls.Add(Me.Button26)
        Me.Controls.Add(Me.Button25)
        Me.Controls.Add(Me.Button24)
        Me.Controls.Add(Me.Button23)
        Me.Controls.Add(Me.Button22)
        Me.Controls.Add(Me.Button21)
        Me.Controls.Add(Me.Button20)
        Me.Controls.Add(Me.Button19)
        Me.Controls.Add(Me.Button18)
        Me.Controls.Add(Me.Button17)
        Me.Controls.Add(Me.Button16)
        Me.Controls.Add(Me.Button15)
        Me.Controls.Add(Me.Button14)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.chkUpdateOnly)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.ListView1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click



        'get category id to populate
        Dim catIDUploadLocation As String = GetDataTable("select category_id from ipm_category where name = 'Projects' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Dim AllFiles As DataTable = GetDataTable("select * from filelisting where processed = 0 and files <> 'thumbs.db'", ConnectionStringIDAM)  'and oid = '041018.00'
        Dim i As Integer
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                ProgressBar1.Value = ProgressBar1.Value + 1
                If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("id"), ConnectionStringIDAM).Rows.Count = 0 Or Me.chkUpdateOnly.Checked Then
                    Dim sAlliance As String = ""
                    Dim sAllianceFolder, sVisionProjectNumber As String
                    Dim sAllianceCatID, sProjectID As String
                    Dim sDirectoryRoot As String = "V:\"
                    Dim sProjectName As String = AllFiles.Rows(i)("projectname")
                    ListView1.Items.Add(sProjectName + ": " + AllFiles.Rows(i)("files"))

                    ListView1.Refresh()
                    Me.Refresh()

                    'get vision project number
                    sVisionProjectNumber = AllFiles.Rows(i)("oid")

                    'get Alliance
                    sAlliance = AllFiles.Rows(i)("alliance")

                    'create alliance dir if needed
                    'does it exist already?
                    sAllianceCatID = ""
                    If GetDataTable("select category_id from ipm_category where parent_cat_id = " & catIDUploadLocation & " and name = '" & sAlliance.Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sAllianceCatID = GetDataTable("select category_id from ipm_category where parent_cat_id = " & catIDUploadLocation & " and name = '" & sAlliance.Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("Category_id")
                    Else
                        sAllianceCatID = CreateNewAllianceCategory(sAlliance, catIDUploadLocation, "1")
                    End If


                    'create project if needed
                    sProjectID = ""
                    If GetDataTable("select projectid from ipm_project where category_id = " & sAllianceCatID & " and name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sProjectID = GetDataTable("select projectid from ipm_project where category_id = " & sAllianceCatID & " and name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("projectid")
                    Else
                        sProjectID = CreateNewProject(sProjectName.Trim, sAllianceCatID, sVisionProjectNumber)
                    End If

                    'create project folders if needed
                    Dim sProjectFolderArray() As String
                    Dim sProjectfolderID As String = "0"
                    Try
                        If AllFiles.Rows(i)("folders").trim <> "" Then
                            sProjectFolderArray = AllFiles.Rows(i)("folders").split("\")
                            'assume value
                            Dim ii As Integer
                            For ii = 0 To sProjectFolderArray.Length - 1
                                If ii = 0 Then 'item at root

                                    sProjectfolderID = checkorCreateProjectfolder("0", sProjectFolderArray(ii), sProjectID)
                                Else
                                    If sProjectFolderArray(ii).Trim <> "" Then
                                        sProjectfolderID = checkorCreateProjectfolder(GetDataTable("select category_id from ipm_asset_category where projectid = " & sProjectID & " and name = '" & sProjectFolderArray(ii - 1).Trim.Replace("'", "''") & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id"), sProjectFolderArray(ii), sProjectID)
                                    Else
                                        'sProjectfolderID = "0"
                                    End If

                                End If
                            Next ii
                        Else
                            sProjectfolderID = "0"
                        End If
                    Catch ex As Exception
                        'put in general
                        sProjectfolderID = "0"
                    End Try



                    'project folder override for level
                    'get level
                    'select a.[approval status] from cumulus a, filelisting b where a.id = b.id and a.id = " &  AllFiles.Rows(i)("id")
                    Dim sLevel As String = "Level2"
                    Try
                        If GetDataTable("select a.[approval status] level from cumulus a, filelisting b where a.id = b.id and a.id = " & AllFiles.Rows(i)("id"), ConnectionStringIDAM).Rows(0)("level") = "L1" Then
                            sLevel = "Level1"
                        End If
                    Catch ex As Exception

                    End Try

                    'set folderid 
                    Try
                        Select Case sLevel
                            Case "Level1"
                                sProjectfolderID = GetDataTable("select category_id from ipm_asset_category where name = 'Images' and projectid = " & sProjectID, ConnectionStringIDAM).Rows(0)("category_id")
                            Case "Level2"
                                sProjectfolderID = GetDataTable("select category_id from ipm_asset_category where name = 'Supplemental Imagery' and projectid = " & sProjectID, ConnectionStringIDAM).Rows(0)("category_id")
                        End Select
                    Catch ex As Exception
                        sProjectfolderID = "0"
                    End Try



                    'get media_type
                    'get extension
                    Dim sExtensionid As String
                    Dim sExtensiontmp() = AllFiles.Rows(i)("Files").split(".")
                    Dim sExtension As String
                    If sExtensiontmp.Length = 1 Then
                        sExtension = "UNK"
                    Else
                        sExtensiontmp.Reverse(sExtensiontmp)
                        sExtension = sExtensiontmp(0)
                    End If
                    'check to see if in filetype_lookup
                    imTypeid = imTypeid + 1
                    Try
                        sExtensionid = GetDataTable("select * from ipm_filetype_lookup where extension = '" & sExtension & "'", Me.ConnectionStringIDAM).Rows(0)("media_type")
                    Catch ex As Exception

                        'add extension
                        Me.ExecuteTransaction("insert into ipm_filetype_lookup (media_type,name,extension,useicon) values (" & imTypeid & ",'" & sExtension & " File" & "','" & sExtension & "',1)")
                        sExtensionid = imTypeid
                    End Try


                    'add asset
                    Dim sAssetID As String
                    'sAssetID = CreateAsset("0","1",sAssetName,"1",sProjectID



                    'check to see if asset already exists and use update
                    If chkUpdateOnly.Checked Then
                        'update location_id,projectid,category_id,
                        If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("id") & " and projectid = " & sProjectID & " and category_id = " & sProjectfolderID, ConnectionStringIDAM).Rows.Count = 0 Then
                            'assume asset is already there just not in the right spot
                            Me.ExecuteTransaction("update ipm_asset set projectid = " & sProjectID & ",category_id = " & sProjectfolderID & " where asset_id = " & AllFiles.Rows(i)("id"))
                        End If
                    Else
                        Me.ExecuteTransaction("insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date) values ('" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sProjectID & ",'1'," & AllFiles.Rows(i)("id") & ",1,1,'" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sExtensionid & "," & sProjectID & "," & sProjectfolderID & ",'" & AllFiles.Rows(i)("filedate") & "','" & AllFiles.Rows(i)("filedate") & "')")
                    End If

                    'add keyword (Level)

                    Dim sDiscipline As String = sLevel
                    'check for discpliine
                    Dim DTDiscipline As DataTable = GetDataTable("select keyid from ipm_services where keyuse = 1 and keyname = '" & sDiscipline.Trim().Replace("'", "''") & "'", Me.ConnectionStringIDAM)
                    If DTDiscipline.Rows.Count > 0 Then
                        'discpline exists so simply reference
                        ExecuteTransaction("delete from ipm_asset_services where asset_id = " & AllFiles.Rows(i)("id") & " and keyid = " & DTDiscipline.Rows(0)("Keyid"))
                        ExecuteTransaction("insert into ipm_asset_services (keyid,asset_id) values (" & DTDiscipline.Rows(0)("Keyid") & "," & AllFiles.Rows(i)("id") & ")")
                    Else
                        'add to disipline table
                        Dim sql As String
                        sql = "exec sp_createnewkeywordServices "
                        sql += "1,"
                        sql += "'" + sDiscipline.ToString().Replace("'", "''") + "', "
                        sql += "1" + ", "
                        sql += "'Imported from external source'"
                        ExecuteTransaction(sql)
                        'get ID of repo just inserted
                        Dim NewKeywordID As String = GetDataTable("select max(keyid) maxid from ipm_services", Me.ConnectionStringIDAM).Rows(0)("maxid")
                        'add external id
                        ExecuteTransaction("insert into ipm_asset_services (keyid,projectid) values (" & NewKeywordID & "," & AllFiles.Rows(i)("id") & ")")
                    End If

                    'add user defined fields

                    Dim UDFSource As DataTable = GetDataTable("select * from cumulus where id = " & AllFiles.Rows(i)("id"), Me.ConnectionStringIDAM)
                    If UDFSource.Rows.Count > 0 Then
                        Dim IDAM_PHOTOGRAPHER_CREDIT, IDAM_PHOTOGRAPHER_CONTACT_INFO, IDAM_ASSET_RIGHTS, IDAM_ARCHIVE_CD, IDAM_CAPTION, IDAM_CUMULUS_CAPTION, IDAM_WORKING_CD As String
                        '21503637:               IDAM_PHOTOGRAPHER_CREDIT()
                        '21503638:               IDAM_PHOTOGRAPHER_CONTACT_INFO()
                        '21503639:               IDAM_ASSET_RIGHTS()
                        '21503640:               IDAM_ARCHIVE_CD()
                        '21503643:               IDAM_CAPTION()
                        '21503642:               IDAM_CUMULUS_CAPTION()
                        '21503641:               IDAM_WORKING_CD()

                        'assign photo credit
                        IDAM_PHOTOGRAPHER_CREDIT = isullcheck(UDFSource.Rows(0)("photographer(s)"))
                        If IDAM_PHOTOGRAPHER_CREDIT <> "" Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id= 21503637 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503637,'" & IDAM_PHOTOGRAPHER_CREDIT & "')")
                        End If

                        IDAM_ARCHIVE_CD = isullcheck(UDFSource.Rows(0)("Archive CD#"))
                        If IDAM_ARCHIVE_CD <> "" Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503640 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503640,'" & IDAM_ARCHIVE_CD & "')")
                        End If

                        IDAM_WORKING_CD = isullcheck(UDFSource.Rows(0)("Working CD#"))
                        If IDAM_WORKING_CD <> "" Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503641 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503641,'" & IDAM_WORKING_CD & "')")
                        End If

                        IDAM_CUMULUS_CAPTION = isullcheck(UDFSource.Rows(0)("Caption"))
                        If IDAM_CUMULUS_CAPTION <> "" And Not IDAM_CUMULUS_CAPTION.IndexOf("Adobe Photoshop 7.0") > 0 Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503642 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503642,'" & IDAM_CUMULUS_CAPTION & "')")
                        End If

                    End If


                    'add security defaults
                    Dim securityCatTable As DataTable
                    If sProjectfolderID = "0" Then
                        securityCatTable = GetDataTable("select * from ipm_project_security where projecid = " & sProjectID, Me.ConnectionStringIDAM)
                    Else
                        securityCatTable = GetDataTable("select * from ipm_category_security where category_id = " & sProjectfolderID, Me.ConnectionStringIDAM)
                    End If

                    Dim srow As DataRow
                    ExecuteTransaction("DELETE FROM ipm_ASSET_security WHERE ASSET_ID = " & AllFiles.Rows(i)("id"))
                    For Each srow In securityCatTable.Rows
                        ExecuteTransaction("insert into ipm_ASSET_security (ASSET_ID, security_id, type) values (" & AllFiles.Rows(i)("id") & ",'" & srow("security_id") & "','" & srow("type") & "')")
                    Next



                    'set processed flag
                    Me.ExecuteTransaction("update filelisting set processed = 1 where id = " & AllFiles.Rows(i)("id"))
                End If

            Catch ex As Exception
                Me.ExecuteTransaction("update filelisting set processed = 3 where id = " & AllFiles.Rows(i)("id"))
                Me.SendError(ex.Message, "MAIN CATCH", "UNKNOWN")
            End Try
        Next i
        Dim x As String = "what the"

    End Sub

    Function checkorCreateProjectfolder(ByVal parentcatid As String, ByVal sName As String, ByVal sProjectID As String, Optional ByVal bypassSec As Boolean = False) As String

        'check is exists
        Dim sProjectFolderID As String
        Try
            sProjectFolderID = GetDataTable("select category_id from ipm_asset_category where projectid = " & sProjectID & " and parent_cat_id = " & parentcatid & " and name = '" & sName.Trim.Replace("'", "''") & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")
            Return sProjectFolderID
        Catch ex As Exception
            Try
                'create category
                Dim sql As String
                sql = "exec sp_createnewassetcategory_wparent 1," & parentcatid & ","
                sql += "'" & Replace(sName, "'", "''") & "',"
                sql += sProjectID & ","
                sql += "'Y',"
                sql += "'',"
                sql += "'1'"
                ExecuteTransaction(sql)
                'get ID of category just inserted
                Dim catID As String = GetDataTable("select max(category_id) maxid from ipm_asset_category", Me.ConnectionStringIDAM).Rows(0)("maxid")
                sProjectFolderID = catID
                'add security defaults
                If Not bypassSec Then
                    sql = "select * from ipm_project_security where projectid = " & sProjectID
                    Dim securityCatTable As DataTable = GetDataTable(sql, Me.ConnectionStringIDAM)
                    Dim srow As DataRow
                    For Each srow In securityCatTable.Rows
                        sql = "insert into ipm_category_security (category_id, security_id, type) values (" & catID & ",'" & srow("security_id") & "','" & srow("type") & "')"
                        ExecuteTransaction(sql)
                    Next
                End If

                Return sProjectFolderID
            Catch exx As Exception
                Return "0"
            End Try
        End Try

    End Function

    Function addProjectfolderSecurity(ByVal groupid As String, ByVal folderid As String)
        Dim sql As String
        Try
            sql = "insert into ipm_category_security (category_id, security_id, type) values (" & folderid & ",'" & groupid & "','G')"
            ExecuteTransaction(sql)
        Catch ex As Exception
            SendError(ex.Message, "addProjectfolderSecurity", folderid + "::" + groupid)
        End Try

    End Function



    Function SendError(ByVal sError, Optional ByVal object_type = "", Optional ByVal object_id = "")
        'id	float	Unchecked
        'date	datetime	Checked
        'error_level	varchar(50)	Checked
        'machine_id	varchar(50)	Checked
        'app_id	varchar(50)	Checked
        'instance_id	varchar(50)	Checked
        'class_name	varchar(255)	Checked
        'output	varchar(4000)	Checked
        'object_type	varchar(50)	Checkeds
        'object_id	varchar(50)	Checked
        Dim sql As String
        sError = sError.ToString.Replace("'", "''")
        '' ''Dim ErrorID As String = 1
        '' ''Try
        '' ''    ErrorID = GetDataTable("select max(id) + 1 maxid from ipm_error", Me.ConnectionStringIDAM).Rows(0)("maxid")
        '' ''    If ErrorID Is System.DBNull.Value Then
        '' ''        ErrorID = 1
        '' ''    End If
        '' ''Catch ex As Exception
        '' ''    ErrorID = 1
        '' ''End Try
        Try
            sql = "insert into ipm_error (date,output,object_type,object_id) values (getdate(),'" & sError & "','" & object_type & "','" & object_id & "')"
            ExecuteTransaction(sql)
            sendMail(sError, object_type, object_id)

        Catch ex As Exception

        End Try



    End Function



    Function sendMail(ByVal sError, Optional ByVal object_type = "", Optional ByVal object_id = "")
        Dim MailReceipt As New SmtpClient
        Dim MailMessage As New MailMessage

        MailReceipt.Host = ConfigurationSettings.AppSettings("SMTP")
        MailMessage.From = New MailAddress(ConfigurationSettings.AppSettings("FromEmail"), "iDAM USHMM Nightly Alert")
        For Each maddress As String In ConfigurationSettings.AppSettings("MonitorEmail").Split(";")
            MailMessage.To.Add(maddress)
        Next
        MailMessage.Body = sError & " " & object_id
        MailMessage.Subject = "IDAM Photo Archive Nightly Alert"
        MailReceipt.Send(MailMessage)
    End Function






    Function CreateNewAllianceCategory(ByVal categoryName As String, ByVal parent_id As String, ByVal securitylevel As String) As String
        Try
            Dim sql As String
            sql = "exec sp_createnewcategory 1,"
            sql += "'" & Replace(categoryName, "'", "''") & "',"
            sql += Replace(parent_id, "CAT", "") & ","
            sql += "'Y',"
            sql += "'',"
            sql += "'" & securitylevel & "'"
            ExecuteTransaction(sql)
            'get ID of category just inserted
            Dim catID As String = GetDataTable("select max(category_id) maxid from ipm_category", Me.ConnectionStringIDAM).Rows(0)("maxid")
            'add security defaults
            sql = "select * from ipm_category_security where category_id = " & Replace(parent_id, "CAT", "")
            Dim securityCatTable As DataTable = GetDataTable(sql, Me.ConnectionStringIDAM)
            Dim srow As DataRow
            For Each srow In securityCatTable.Rows
                sql = "insert into ipm_category_security (category_id, security_id, type) values (" & catID & ",'" & srow("security_id") & "','" & srow("type") & "')"
                ExecuteTransaction(sql)
            Next
            Return catID
        Catch ex As Exception
            Return "0"
        End Try
    End Function

    Private Function CreateNewProject(ByVal sProjectName As String, ByVal sCatID As String, ByVal sProjectNumber As String) As String
        'here we go...
        'check to see if external reference
        Dim sProjectID As String = ""
        Try



            Dim sql As String
            'Dim sProjectNumber As String = ProjectItem("oid")
            'Dim sProjectName As String = ProjectItem("name")
            ExecuteTransaction("exec sp_createnewproject2 " & sCatID & ",1,'" & Replace(sProjectName, "'", "''") & "','Imported from automated procedure.','1'")
            ''get ID of category just inserted
            sProjectID = GetDataTable("select max(projectid) maxid from ipm_project", Me.ConnectionStringIDAM).Rows(0)("maxid")
            'add security defaults
            sql = "select * from ipm_category_security where category_id = " & sCatID
            Dim securityCatTable As DataTable = GetDataTable(sql, Me.ConnectionStringIDAM)
            Dim srow As DataRow
            For Each srow In securityCatTable.Rows
                sql = "insert into ipm_project_security (projectid, security_id, type) values (" & sProjectID & ",'" & srow("security_id") & "','" & srow("type") & "')"
                ExecuteTransaction(sql)
            Next
            Dim catID As String = sProjectID


            'get newly added project id
            Return sProjectID.ToString
        Catch ex As Exception
            If sProjectID <> "" Then
                Return sProjectID
            Else
                Return "0"
            End If

        End Try

    End Function


    Private Sub ExecuteZGFSQL(ByVal catID As String, ByVal sProjectNumber As String)
        Dim sql As String


        'add keywords
        'If chkprojectinformation.Checked Then
        'get the keywords
        'add project discipline = discipline, project(alliance = keyword, Type = office)
        ''discipline
        Dim SourceKeywords As DataTable = GetDataTable("select custdesignservice from projectcustomtabfields where wbs1 = '" & sProjectNumber.Trim & "' and wbs2 = ''", Me.ConnectionStringUSHMMPHOTOARCHIVE)
        If SourceKeywords.Rows.Count > 0 Then
            Dim sDiscipline As String = Me.isullcheck(SourceKeywords.Rows(0)("custdesignservice"))
            If sDiscipline <> "" Then
                'check for discpliine
                Dim DTDiscipline As DataTable = GetDataTable("select keyid from ipm_discipline where keyuse = 1 and keyname = '" & sDiscipline.Trim().Replace("'", "''") & "'", Me.ConnectionStringIDAM)
                If DTDiscipline.Rows.Count > 0 Then
                    'discpline exists so simply reference
                    ExecuteTransaction("delete from ipm_project_discipline where projectid = " & catID & " and keyid = " & DTDiscipline.Rows(0)("Keyid"))
                    ExecuteTransaction("insert into ipm_project_discipline (keyid,projectid) values (" & DTDiscipline.Rows(0)("Keyid") & "," & catID & ")")
                Else
                    'add to disipline table
                    sql = "exec sp_createnewkeywordDisciplineType "
                    sql += "1,"
                    sql += "'" + sDiscipline.ToString().Replace("'", "''") + "', "
                    sql += "1" + ", "
                    sql += "'Imported from external source'"
                    ExecuteTransaction(sql)
                    'get ID of repo just inserted
                    Dim NewKeywordID As String = GetDataTable("select max(keyid) maxid from ipm_discipline", Me.ConnectionStringIDAM).Rows(0)("maxid")
                    'add external id
                    ExecuteTransaction("insert into ipm_project_discipline (keyid,projectid) values (" & NewKeywordID & "," & catID & ")")
                End If
            End If
        End If


        'add project type
        Dim SourceType As DataTable = GetDataTable("select cfgprojecttype.* from pr,cfgprojecttype where pr.projecttype = cfgprojecttype.code and pr.wbs1 = '" & sProjectNumber & "' and pr.wbs2 = ''", Me.ConnectionStringUSHMMPHOTOARCHIVE)
        If SourceType.Rows.Count > 0 Then
            Dim sType As String = SourceType.Rows(0)("description")

            'check for discpliine
            Dim DTType As DataTable = GetDataTable("select keyid from ipm_office where keyuse = 1 and keyname = '" & sType.Trim().Replace("'", "''") & "'", Me.ConnectionStringIDAM)
            If DTType.Rows.Count > 0 Then
                'discpline exists so simply reference
                ExecuteTransaction("delete from ipm_project_office where projectid = " & catID & " and officeid = " & DTType.Rows(0)("Keyid"))
                ExecuteTransaction("insert into ipm_project_office (officeid,projectid) values (" & DTType.Rows(0)("Keyid") & "," & catID & ")")
            Else
                'add to disipline table
                sql = "exec sp_createnewkeywordOfficeType "
                sql += "1,"
                sql += "'" + sType.ToString().Replace("'", "''") + "', "
                sql += "1" + ", "
                sql += "'Imported from external source'"
                ExecuteTransaction(sql)
                'get ID of repo just inserted
                Dim NewKeywordID As String = GetDataTable("select max(keyid) maxid from ipm_office", Me.ConnectionStringIDAM).Rows(0)("maxid")
                'add external id
                ExecuteTransaction("insert into ipm_project_office (officeid,projectid) values (" & NewKeywordID & "," & catID & ")")
            End If
        End If

        'add additional project description data
        'add owners
        If 1 = 1 Then
            'add project type
            Dim SourceType2 As DataTable = GetDataTable("select principal,projmgr from pr where pr.wbs1 = '" & catID & "' and pr.wbs2 = ''", Me.ConnectionStringUSHMMPHOTOARCHIVE)
            If SourceType2.Rows.Count > 0 Then
                Dim sPrincipal As String = SourceType2.Rows(0)("principal")
                Dim sProjManger As String = SourceType2.Rows(0)("projmgr")

                'check 
                If sPrincipal <> "" Then
                    'discpline exists so simply reference
                    ExecuteTransaction("update ipm_project set principalid = '" & sPrincipal & "' where projectid = " & catID)
                End If
                If sProjManger <> "" Then
                    'discpline exists so simply reference
                    ExecuteTransaction("update ipm_project set pmid = '" & sProjManger & "' where projectid = " & catID)
                End If
            End If
        End If




    End Sub





    Public Shared Sub ExecuteTransaction(ByVal sqlstring As String)
        Dim ConnectionString As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
        Dim connection As New OleDbConnection(ConnectionString)
        Dim command As New OleDbCommand

        Dim transaction As OleDbTransaction

        ' Set the Connection to the new OleDbConnection.
        command.Connection = connection

        ' Open the connection and execute the transaction.
        Try
            connection.Open()

            ' Start a local transaction with ReadCommitted isolation level.
            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            ' Assign transaction object for a pending local transaction.
            command.Connection = connection
            command.Transaction = transaction

            ' Execute the commands.
            command.CommandText = sqlstring
            command.CommandTimeout = 4000
            command.ExecuteNonQuery()


            ' Commit the transaction.
            transaction.Commit()
            Console.WriteLine("Executed.")

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Try to rollback the transaction
            Try
                transaction.Rollback()

            Catch
                ' Do nothing here; transaction is not active.
            End Try
        End Try
        ' The connection is automatically closed when the
        ' code exits the Using block.

    End Sub

    Public Shared Function GetDataTable(ByVal query As String, ByVal ConnectionString As String) As DataTable
        'Dim connection1 As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & System.Web.HttpContext.Current.Server.MapPath(dbFile))
        Dim connection1 As OleDbConnection = New OleDbConnection(ConnectionString)

        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)

        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()

        End Try
        Return table1


        Dim x As String
        x = "asdfasdf"

    End Function
    Function isullcheck(ByVal v)
        If v Is System.DBNull.Value Then
            isullcheck = ""
        Else
            isullcheck = v
        End If
    End Function

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim sRootRepository As String = "d:\IDAM\repository\"
        Dim AllFiles As DataTable = GetDataTable("select b.asset_id, replace(a.directory,'MarketingMasters\','') directory, a.files, b.projectid, c.extension from filelisting a, ipm_asset b, ipm_filetype_lookup c where a.id = b.asset_id and b.media_type = c.media_type and a.processed = 7", ConnectionStringIDAM)
        Dim i As Integer
        Dim sExtensionid As String
        Dim sSourceFile As String
        Dim sDestinationFile As String
        Dim sTifSwitch As Boolean = False
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                If InStr(AllFiles.Rows(i)("Files"), ".jpg") > 0 Then 'only process jpg files
                    'get SourceFile
                    sSourceFile = ""
                    sDestinationFile = ""
                    sSourceFile = "x:\" & AllFiles.Rows(i)("Directory") + "\" + AllFiles.Rows(i)("Files")
                    sDestinationFile = sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource\" + AllFiles.Rows(i)("asset_id").ToString.Trim + "." + AllFiles.Rows(i)("extension").ToString.Trim
                    'is directories created?
                    If Not IO.Directory.Exists(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim) Then
                        IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim)
                        IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource")
                        IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\acrobat")
                    End If
                    'now copy files
                    'check to see if tiff exists .tif


                    Dim fileSize As Long = New System.IO.FileInfo(sSourceFile).Length

                    If IO.File.Exists(Replace(sSourceFile, ".jpg", ".tif")) Then
                        'delete jpg
                        'IO.File.Delete(sDestinationFile)
                        'IO.File.Copy(Replace(sSourceFile, ".jpg", ".tif"), Replace(sDestinationFile, ".jpg", ".tif"), False)
                        'change extension of asset
                        'ExecuteTransaction("update ipm_asset set media_type = '85', name = '" & Replace(AllFiles.Rows(i)("Files"), ".jpg", ".tif") & "', filename = '" & Replace(AllFiles.Rows(i)("Files"), ".jpg", ".tif") & "' where asset_id = " & AllFiles.Rows(i)("asset_id"))
                        'sTifSwitch = True
                    Else
                        'already there...
                        IO.File.Copy(sSourceFile, sDestinationFile, False)
                        Me.ExecuteTransaction("insert into ipm_asset_queue(instance_id,asset_id,asset_handler,status,priority,engine_id)values('IDAM_NBBJ'," & AllFiles.Rows(i)("asset_id").ToString.Trim & ",2,0,1,'20')")
                    End If

                    'now send to ipm_asset_queue and mark processed = 4
                    If sTifSwitch Then
                        Me.ExecuteTransaction("insert into ipm_asset_queue(instance_id,asset_id,asset_handler,status,priority,engine_id)values('IDAM_NBBJ'," & AllFiles.Rows(i)("asset_id").ToString.Trim & ",2,0,1,'20')")
                        Me.ExecuteTransaction("update filelisting set processed = 5 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)
                    End If
                End If
                Me.ExecuteTransaction("update filelisting set processed = 7 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)
            Catch ex As Exception
                'MsgBox(ex.Message & AllFiles.Rows(i)("asset_id"))
                Me.ExecuteTransaction("update filelisting set processed = 6 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)
            End Try
            ProgressBar1.Value = ProgressBar1.Value + 1
        Next

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        ''''''''copy field values from cumulus to filelisting
        '''''''Me.ExecuteTransaction("delete from filelisting")
        '''''''Dim AllFiles As DataTable = GetDataTable("SELECT a.id,a.[asset identifier],a.[asset name],a.[file data size],a.[creation date],a.[modification date],a.[folder name],[cumulus project name],[project number],[cumulus market name],[vision market name],[vision long name]    FROM         CUMULUS a LEFT OUTER JOIN CUMULUS_MAPPING b ON SUBSTRING(replace([folder name],'MarketingMasters\IMAGES\',''),PATINDEX('%\%',replace([folder name],'MarketingMasters\IMAGES\',''))+1,100) = b.[cumulus project name]", ConnectionStringIDAM)
        '''''''Dim i As Integer
        '''''''Dim x As Integer = 0
        '''''''Dim sql As String
        '''''''ProgressBar1.Maximum = AllFiles.Rows.Count
        '''''''ProgressBar1.Value = 0
        '''''''For i = 0 To AllFiles.Rows.Count - 1
        '''''''    Try
        '''''''        sql = "insert into filelisting (id,directory,files,filedate,assetid,processed,alliance,projectname,projectfolder,alliancefolder,oid) values " & _
        '''''''        "('" & Me.isullcheck(AllFiles.Rows(i)("id")) & "','" & Me.isullcheck(AllFiles.Rows(i)("folder name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("asset name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("creation date")) & "','" & Me.isullcheck(AllFiles.Rows(i)("asset identifier")) & "',0,'" & Me.isullcheck(AllFiles.Rows(i)("vision market name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("vision long name")).replace("'", "''") & "','','" & Me.isullcheck(AllFiles.Rows(i)("vision market name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("project number")).replace("'", "''") & "')"
        '''''''        Me.ExecuteTransaction(sql)

        '''''''    Catch ex As Exception

        '''''''        x += 1
        '''''''    End Try
        '''''''Next

        '''''''sql = "update filelisting set projectname = SUBSTRING(replace([directory],'MarketingMasters\IMAGES\',''),PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES\',''))+1,100) where projectname = ''"
        '''''''Me.ExecuteTransaction(sql)

        '''''''sql = "update filelisting set alliance = replace(SUBSTRING(replace([directory],'MarketingMasters\IMAGES',''),PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES',''))+1,PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES\',''))),'\',''),alliancefolder = replace(SUBSTRING(replace([directory],'MarketingMasters\IMAGES',''),PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES',''))+1,PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES\',''))),'\','') where alliance = ''"
        '''''''Me.ExecuteTransaction(sql)

        '''''''sql = "update filelisting set alliance = replace(SUBSTRING(replace([directory],'MarketingMasters\IMAGES',''),PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES',''))+1,PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES\',''))),'\',''),alliancefolder = replace(SUBSTRING(replace([directory],'MarketingMasters\IMAGES',''),PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES',''))+1,PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES\',''))),'\','') where alliance = '<Null>'"
        '''''''Me.ExecuteTransaction(sql)

        '''''''sql = "update filelisting set alliance = 'Miscellaneous', alliancefolder = 'Miscellaneous' where alliance = ''"
        '''''''Me.ExecuteTransaction(sql)

        '''''''sql = "update filelisting set alliance = 'Miscellaneous', alliancefolder = 'Miscellaneous' where alliance in ('_L2 SMALL SCANSJUST','_BURNED_ReadySt','_BURNEDKwangmyon','_DROP','_DROPSamsan','_L2 SMALL SCANSJUST','_L2 SMALL SCANSSCIE','Dong San Medical Center, Keimyung University','Jakarta Mixed-Use Tower','Korean Animation')"
        '''''''Me.ExecuteTransaction(sql)

        ''''''''''''''''get all others not linked to vision project
        '''''''''''''''AllFiles = GetDataTable("select a.id,a.[asset identifier],a.[asset name],a.[file data size],a.[creation date],a.[modification date],a.[folder name],SUBSTRING(replace([folder name],'MarketingMasters\IMAGES\',''),PATINDEX('%\%',replace([folder name],'MarketingMasters\IMAGES\',''))+1,100) [cumulus project name],''[project number],'Miscellaneous' [cumulus market name],'Miscellaneous' [vision market name],SUBSTRING(replace([folder name],'MarketingMasters\IMAGES\',''),PATINDEX('%\%',replace([folder name],'MarketingMasters\IMAGES\',''))+1,100) [vision long name]from cumulus a,(select id from cumulus where id not in (select a.id from cumulus a, cumulus_mapping b where SUBSTRING(replace([folder name],'MarketingMasters\IMAGES\',''),PATINDEX('%\%',replace([folder name],'MarketingMasters\IMAGES\',''))+1,100) = b.[cumulus project name])) c where a.id = c.id ", ConnectionStringIDAM)
        '''''''''''''''ProgressBar1.Maximum = AllFiles.Rows.Count
        '''''''''''''''ProgressBar1.Value = 0
        '''''''''''''''For i = 0 To AllFiles.Rows.Count - 1
        '''''''''''''''    Try

        '''''''''''''''        sql = "insert into filelisting (id,directory,files,filedate,assetid,processed,alliance,projectname,projectfolder,alliancefolder,oid) values " & _
        '''''''''''''''        "('" & Me.isullcheck(AllFiles.Rows(i)("id")) & "','" & Me.isullcheck(AllFiles.Rows(i)("folder name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("asset name")) & "','" & Me.isullcheck(AllFiles.Rows(i)("creation date")) & "','" & Me.isullcheck(AllFiles.Rows(i)("asset identifier")) & "',0,'" & Me.isullcheck(AllFiles.Rows(i)("vision market name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("vision long name")).replace("'", "''") & "','','" & Me.isullcheck(AllFiles.Rows(i)("vision market name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("project number")).replace("'", "''") & "')"
        '''''''''''''''        Me.ExecuteTransaction(sql)

        '''''''''''''''    Catch ex As Exception

        '''''''''''''''        x += 1
        '''''''''''''''    End Try
        '''''''''''''''Next






    End Sub

    Private Sub Button1_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Disposed

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click


        '21503534:       General(Staff)
        '21503535        Market Studio 28                                  
        '21503536:       Marketing(Manager)
        '21503537:       Marketing(Coordinator)
        '21503538:       Communications(Team)
        '21503539:       Photo(Administrator)

        '11175552: DOWNLOAD()
        '11175555: CREATE_COMMENTS()
        '11175556: VIEW_COMMENTS()
        '11175557: EDIT_ASSETS()
        '11175558: UPLOAD_VERSIONS()
        '11175559: ASSET_CHECKIN()
        '11175858: DOWNLOAD_TYPES()
        '21177444: VIEW_ASSOCIATED()
        '21177445: ADD_ASSOCIATED()

        'projectasset role items:
        '11175551	UPLOAD 
        '11175554: VIEW_ASSET_DETAIL()


        Me.ExecuteTransaction("delete from ipm_asset_role_override") 'clear existing
        Me.ExecuteTransaction("delete from ipm_asset_category_role_override") 'clear existing
        Dim i As Integer
        Try
            'add override for market/general staff set no upload in images
            Dim AllFiles As DataTable = GetDataTable("select category_id from ipm_asset_category  where name = 'Images' and available= 'Y'", ConnectionStringIDAM)  'and oid = '041018.00'
            For i = 0 To AllFiles.Rows.Count - 1
                Me.ExecuteTransaction("insert into ipm_asset_category_role_override (category_id,security_id,type,item_id,item_value) values (" & AllFiles.Rows(i)("category_id") & ",'21503535','G',11175551,0)")
                Me.ExecuteTransaction("insert into ipm_asset_category_role_override (category_id,security_id,type,item_id,item_value) values (" & AllFiles.Rows(i)("category_id") & ",'21503536','G',11175551,0)")
                Me.ExecuteTransaction("insert into ipm_asset_category_role_override (category_id,security_id,type,item_id,item_value) values (" & AllFiles.Rows(i)("category_id") & ",'21503537','G',11175551,0)")
                Me.ExecuteTransaction("insert into ipm_asset_category_role_override (category_id,security_id,type,item_id,item_value) values (" & AllFiles.Rows(i)("category_id") & ",'21503534','G',11175551,0)")
            Next

            'add override for general staff set no download in images
            AllFiles = GetDataTable("select category_id from ipm_asset_category where name in ('Communications','Images','Marketing Materials','Studio Assets') and available= 'Y'", ConnectionStringIDAM)  'and oid = '041018.00'
            For i = 0 To AllFiles.Rows.Count - 1
                Me.ExecuteTransaction("insert into ipm_asset_category_role_override (category_id,security_id,type,item_id,item_value) values (" & AllFiles.Rows(i)("category_id") & ",'21503534','G',11175552,0)")
            Next


            ''''''''add override general staff no download original in images
            '''''''AllFiles = GetDataTable("select a.asset_id from ipm_asset a, ipm_asset_category b where a.category_id = b.category_id and b.name = 'Images' and a.available= 'Y'", ConnectionStringIDAM)  'and oid = '041018.00'
            '''''''For i = 0 To AllFiles.Rows.Count - 1
            '''''''    Me.ExecuteTransaction("insert into ipm_asset_role_override (asset_id,security_id,type,item_id,item_value) values (" & AllFiles.Rows(i)("asset_id") & ",'21503534','G',11175552,0)")
            '''''''    Me.ExecuteTransaction("insert into ipm_asset_role_override (asset_id,security_id,type,item_id,item_value) values (" & AllFiles.Rows(i)("asset_id") & ",'21503539','G',11175552,0)")
            '''''''    Me.ExecuteTransaction("insert into ipm_asset_role_override (asset_id,security_id,type,item_id,item_value) values (" & AllFiles.Rows(i)("asset_id") & ",'21503539','G',11175858,0)")
            '''''''Next

        Catch ex As Exception

        End Try




    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'add project udfs
        'get all projects with projectnums
        Dim DT_Temp As DataTable
        Dim AllFiles As DataTable = GetDataTable("select projectid, projectnumber from ipm_project where projectnumber <> '' and available = 'Y'", ConnectionStringIDAM)  'and oid = '041018.00'
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        Dim i As Integer


        '21503540: IDAM_VISION_PROJECT_DESCRIPTION()
        '21503541: IDAM_PRIMARY_STUDIO()
        '21503542: IDAM_CLIENT_NAME()
        '21503543: IDAM_PRIMARY_PROJECT_TYPE() = market
        '21503544: IDAM_VISION_PROJECT_NAME()
        '21503545: IDAM_PRIMARY_SERVICE()
        '21503715: IDAM_ACTUAL_COMPLETION_DATE()
        '21503717: IDAM_CONSTRUCTION_START_DATE()
        '21503718: IDAM_CONSTRUCTION_END_DATE()
        '21503719: IDAM_COMPETITION()
        '21503720: IDAM_COUNTRY()
        '21503721: IDAM_GEOGRAPHIC_REGION()
        '21503722: IDAM_AWARDS()
        '21503723: IDAM_PUBLICATIONS()
        '21503724: IDAM_BIM()
        '21503546: IDAM_CONFIDENTIAL()
        '21503714: IDAM_START_DATE()
        '21503716: IDAM_OCCUPANCY_DATE()


        For i = 0 To AllFiles.Rows.Count - 1
            'IDAM_VISION_PROJECT_DESCRIPTION
            DT_Temp = GetDataTable("select description from prdescriptions where desccategory = 'PROJDESC' and wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "'", ConnectionStringUSHMMPHOTOARCHIVE)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503540")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503540,'" & RemoveHtml(DT_Temp.Rows(0)("description")).Replace("'", "''") & "')")
            End If

            'IDAM_PRIMARY_PROJECT_TYPE
            DT_Temp = GetDataTable("select b.description from pr a, cfgprojecttype b where a.projecttype = b.code and a.wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringUSHMMPHOTOARCHIVE)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503543")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503543,'" & RemoveHtml(DT_Temp.Rows(0)("description")).Replace("'", "''") & "')")
            End If


            'IDAM_CONSTRUCTION_START_DATE()
            DT_Temp = GetDataTable("select custConstructionStartDate from ProjectCustomTabFields where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringUSHMMPHOTOARCHIVE)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503717")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503717,'" & DT_Temp.Rows(0)("custConstructionStartDate") & "')")
            End If


            'IDAM_ACTUAL_COMPLETION_DATE()
            DT_Temp = GetDataTable("select ActCompletionDate from pr where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringUSHMMPHOTOARCHIVE)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503715")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503715,'" & DT_Temp.Rows(0)("ActCompletionDate") & "')")
            End If


            'IDAM_CONSTRUCTION_END_DATE()
            DT_Temp = GetDataTable("select ConstComplDate from pr where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringUSHMMPHOTOARCHIVE)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503718")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503718,'" & DT_Temp.Rows(0)("ConstComplDate") & "')")
            End If

            'IDAM_COMPETITION()
            DT_Temp = GetDataTable("select custBuildingStatusCompetition from ProjectCustomTabFields where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringUSHMMPHOTOARCHIVE)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503719")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503719,'" & Me.isullcheck(DT_Temp.Rows(0)("custBuildingStatusCompetition")).replace("'", "''") & "')")
            End If

            'IDAM_COUNTRY()
            DT_Temp = GetDataTable("select Country from pr where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringUSHMMPHOTOARCHIVE)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503720")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503720,'" & Me.isullcheck(DT_Temp.Rows(0)("Country")).replace("'", "''") & "')")
            End If

            'IDAM_GEOGRAPHIC_REGION()
            DT_Temp = GetDataTable("select custlocationcode from ProjectCustomTabFields where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringUSHMMPHOTOARCHIVE)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503721")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503721,'" & Me.isullcheck(DT_Temp.Rows(0)("custlocationcode")).replace("'", "''") & "')")
            End If

            '21503722: IDAM_AWARDS()
            Dim ii As Integer
            Dim sAwards As String = ""
            DT_Temp = GetDataTable("select description,institution,awarddate from prawards where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringUSHMMPHOTOARCHIVE)
            If DT_Temp.Rows.Count > 0 Then
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503722")
                For ii = 0 To DT_Temp.Rows.Count - 1
                    'erase any possibel previous value
                    sAwards += Me.isullcheck(DT_Temp.Rows(ii)("institution")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("awarddate")) & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("description")).replace("'", "''") & vbCrLf & vbCrLf
                Next
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503722,'" & sAwards & "')")
            End If


            'select custpublication,custpublisher,custpubdate,custpubarticle, from projects_publications order by seq
            '21503723: IDAM_PUBLICATIONS()
            Dim sPublications As String = ""
            DT_Temp = GetDataTable("select custpublication,custpublisher,custpubdate,custpubarticle from projects_publications where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = '' order by seq", ConnectionStringUSHMMPHOTOARCHIVE)
            If DT_Temp.Rows.Count > 0 Then
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503723")
                For ii = 0 To DT_Temp.Rows.Count - 1
                    'erase any possibel previous value
                    sPublications += Me.isullcheck(DT_Temp.Rows(ii)("custpubdate")) & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpublisher")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpublication")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpubarticle")).replace("'", "''") & vbCrLf & vbCrLf
                Next
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503723,'" & sPublications & "')")
            End If

            '21503714: IDAM_START_DATE()
            DT_Temp = GetDataTable("select startdate from pr where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringUSHMMPHOTOARCHIVE)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503714")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503714,'" & Me.isullcheck(DT_Temp.Rows(0)("startdate")) & "')")
            End If


            '21503716: IDAM_OCCUPANCY_DATE()
            DT_Temp = GetDataTable("select custOccupancyDate from ProjectCustomTabFields where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringUSHMMPHOTOARCHIVE)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503716")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503716,'" & Me.isullcheck(DT_Temp.Rows(0)("custOccupancyDate")) & "')")
            End If

        Next

    End Sub






    Public Function RemoveHtml(ByVal sTEXT As String) As String
        Dim sTemp As String
        sTemp = System.Text.RegularExpressions.Regex.Replace(sTEXT, "<[^>]*>", "")
        Return sTemp
    End Function

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'add project plates
        ' types of imports
        '1 projectid and projectnumber = no prob
        '2 projectid no project num = no prob
        '3 projectnum and no projectid = new project in misc folder - import from vision
        '4 neither = new project in misc folder

        'update FILELISTINGPROJECTPLATES set projectname = replace(replace(replace(directory,'x:\project plates\',''),'\fonts',''),'\links','') 

        'linked set
        'select a.id,b.projectnumber,c.name from FILELISTINGPROJECTPLATES a, project_plate_mapping b, ipm_project c where replace(replace(replace(a.directory,'x:\project plates\',''),'\fonts',''),'\links','') = b.name and b.projectnumber is not null and c.projectnumber = b.projectnumber

        'all others
        'select
        'replace(replace(replace(directory,'x:\project plates\',''),'\fonts',''),'\links','') 
        'from FILELISTINGPROJECTPLATES where id not in (
        'select a.id from FILELISTINGPROJECTPLATES a, project_plate_mapping b, ipm_project c
        'where replace(replace(replace(a.directory,'x:\project plates\',''),'\fonts',''),'\links','') = b.name
        'and b.projectnumber is not null
        'and c.projectnumber = b.projectnumber)
        'group by replace(replace(replace(directory,'x:\project plates\',''),'\fonts',''),'\links','')

        'do not upload fonts

        'get category id to populate
        ' Dim catIDUploadLocation As String = GetDataTable("select category_id from ipm_category where name = 'Projects' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Dim AllFiles As DataTable = GetDataTable("select a.filedate,a.files,c.projectid,a.id,b.projectnumber,c.name from FILELISTINGPROJECTPLATES a, project_plate_mapping b, ipm_project c where replace(replace(replace(a.directory,'x:\project plates\',''),'\fonts',''),'\links','') = b.name and b.projectnumber is not null and c.projectnumber = b.projectnumber", ConnectionStringIDAM)  'and oid = '041018.00'
        Dim i As Integer
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                ProgressBar1.Value = ProgressBar1.Value + 1
                If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("id"), ConnectionStringIDAM).Rows.Count = 0 Or Me.chkUpdateOnly.Checked Then
                    Dim sAlliance As String = ""
                    Dim sAllianceFolder, sVisionProjectNumber As String
                    Dim sAllianceCatID, sProjectfolderID As String
                    Dim sDirectoryRoot As String = "X:\"
                    Dim sProjectID As String = AllFiles.Rows(i)("projectid")
                    ListView1.Items.Add(sProjectID + ": " + AllFiles.Rows(i)("files"))

                    ListView1.Refresh()
                    Me.Refresh()


                    'create alliance dir if needed
                    'does it exist already?
                    sProjectfolderID = ""
                    If GetDataTable("select category_id from ipm_asset_category where name = 'Marketing Materials' and projectid = " & sProjectID & " and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sProjectfolderID = GetDataTable("select category_id from ipm_asset_category where name = 'Marketing Materials' and projectid = " & sProjectID & " and available = 'Y'", ConnectionStringIDAM).Rows(0)("Category_id")
                    Else
                        sProjectfolderID = "0"
                    End If


                    'get media_type
                    'get extension
                    Dim sExtensionid As String
                    Dim sExtensiontmp() = AllFiles.Rows(i)("Files").split(".")
                    Dim sExtension As String
                    If sExtensiontmp.Length = 1 Then
                        sExtension = "UNK"
                    Else
                        sExtensiontmp.Reverse(sExtensiontmp)
                        sExtension = sExtensiontmp(0)
                    End If
                    'check to see if in filetype_lookup
                    imTypeid = imTypeid + 1
                    Try
                        sExtensionid = GetDataTable("select * from ipm_filetype_lookup where extension = '" & sExtension & "'", Me.ConnectionStringIDAM).Rows(0)("media_type")
                    Catch ex As Exception

                        'add extension
                        Me.ExecuteTransaction("insert into ipm_filetype_lookup (media_type,name,extension,useicon) values (" & imTypeid & ",'" & sExtension & " File" & "','" & sExtension & "',1)")
                        sExtensionid = imTypeid
                    End Try


                    'add asset
                    Dim sAssetID As String
                    'sAssetID = CreateAsset("0","1",sAssetName,"1",sProjectID

                    If sExtension.ToLower <> "ttf" And sExtension.ToLower <> "pfm" And sExtension.ToLower <> "pfb" Then
                        'check to see if asset already exists and use update
                        If chkUpdateOnly.Checked Then
                            'update location_id,projectid,category_id,
                            If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("id") & " and projectid = " & sProjectID & " and category_id = " & sProjectfolderID, ConnectionStringIDAM).Rows.Count = 0 Then
                                'assume asset is already there just not in the right spot
                                'Me.ExecuteTransaction("update ipm_asset set projectid = " & sProjectID & ",category_id = " & sProjectfolderID & " where asset_id = " & AllFiles.Rows(i)("id"))
                            End If
                        Else
                            Me.ExecuteTransaction("insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date) values ('" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sProjectID & ",'1'," & AllFiles.Rows(i)("id") & ",1,1,'" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sExtensionid & "," & sProjectID & "," & sProjectfolderID & ",getdate(),getdate())")
                        End If
                        'set processed flag to processed
                        Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 1 where id = " & AllFiles.Rows(i)("id"))
                    Else
                        'set processed flag to no import
                        Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 5 where id = " & AllFiles.Rows(i)("id"))
                    End If


                End If

            Catch ex As Exception
                Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 3 where id = " & AllFiles.Rows(i)("id"))
                Me.SendError(ex.Message, "MAIN CATCH", "UNKNOWN")
            End Try
        Next i
        Dim x As String = "what the"

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Dim sRootRepository As String = "D:\IDAM\repository\"
        Dim AllFiles As DataTable = GetDataTable("select b.asset_id, replace(a.directory,'MarketingMasters\','') directory, a.files, b.projectid, c.extension from FILELISTINGPROJECTPLATES a, ipm_asset b, ipm_filetype_lookup c where a.id = b.asset_id and b.media_type = c.media_type and a.processed = 1", ConnectionStringIDAM)
        Dim i As Integer
        Dim sExtensionid As String
        Dim sSourceFile As String
        Dim sDestinationFile As String
        Dim sTifSwitch As Boolean = False
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try

                'get SourceFile
                sSourceFile = ""
                sDestinationFile = ""
                sSourceFile = AllFiles.Rows(i)("Directory") + "\" + AllFiles.Rows(i)("Files")
                'sSourceFile = Replace(AllFiles.Rows(i)("Directory"), "X:\", "q:\") + "\" + AllFiles.Rows(i)("Files")
                sDestinationFile = sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource\" + AllFiles.Rows(i)("asset_id").ToString.Trim + "." + AllFiles.Rows(i)("extension").ToString.Trim
                'is directories created?
                ''''''''''If Not IO.Directory.Exists(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim) Then
                ''''''''''    IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim)
                ''''''''''    IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource")
                ''''''''''    IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\acrobat")
                ''''''''''End If
                'now copy files
                'check to see if tiff exists .tif
                If IO.File.Exists(sSourceFile) Then
                    'delete jpg
                    'IO.File.Delete(sDestinationFile)
                    IO.File.Copy(sSourceFile, sDestinationFile, False)
                    'change extension of asset
                Else
                    'already there...IO.File.Copy(sSourceFile, sDestinationFile, False)
                End If

                'now send to ipm_asset_queue and mark processed = 4

                Me.ExecuteTransaction("insert into ipm_asset_queue(instance_id,asset_id,asset_handler,status,priority,engine_id)values('IDAM_NBBJ'," & AllFiles.Rows(i)("asset_id").ToString.Trim & ",2,0,1,'20')")
                Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 12 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)

            Catch ex As Exception
                'MsgBox(ex.Message & AllFiles.Rows(i)("asset_id"))
                Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 66 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)
            End Try
            ProgressBar1.Value = ProgressBar1.Value + 1
        Next
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click


        'does asset exist?
        'no - then normal routine for missing assets
        'yes

        'does alliance folder exist?
        'no then create
        'get id

        'does project exist in alliance?
        'no - then create in alliance
        'get id
        'yes...is it in proper alliance?
        'no - then remap

        'update asset with proper projectid


        'delete all


        'get category id to populate
        Dim catIDUploadLocation As String = GetDataTable("select category_id from ipm_category where name = 'Projects' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")

        'delete all categories
        Me.ExecuteTransaction("delete from ipm_category where parent_cat_id = " & catIDUploadLocation)

        'delete all projects and project folders
        Me.ExecuteTransaction("update ipm_project set available = 'N'")
        Me.ExecuteTransaction("update ipm_asset_category set available = 'N'")


        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Dim AllFiles As DataTable = GetDataTable("select * from filelisting where processed = 0 and files <> 'thumbs.db'", ConnectionStringIDAM)  'and oid = '041018.00'
        Dim i As Integer
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                ProgressBar1.Value = ProgressBar1.Value + 1
                If Me.chkUpdateOnly.Checked Then
                    Dim sAlliance As String = ""
                    Dim sAllianceFolder, sVisionProjectNumber As String
                    Dim sAllianceCatID, sProjectID As String
                    Dim sDirectoryRoot As String = "V:\"
                    Dim sProjectName As String = AllFiles.Rows(i)("projectname")
                    ListView1.Items.Add(sProjectName + ": " + AllFiles.Rows(i)("files"))

                    ListView1.Refresh()
                    Me.Refresh()

                    'get vision project number
                    sVisionProjectNumber = AllFiles.Rows(i)("oid")

                    'get Alliance
                    sAlliance = AllFiles.Rows(i)("alliance")

                    'create alliance dir if needed
                    'does it exist already?
                    sAllianceCatID = ""
                    If GetDataTable("select category_id from ipm_category where parent_cat_id = " & catIDUploadLocation & " and name = '" & sAlliance.Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sAllianceCatID = GetDataTable("select category_id from ipm_category where parent_cat_id = " & catIDUploadLocation & " and name = '" & sAlliance.Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("Category_id")
                    Else
                        sAllianceCatID = CreateNewAllianceCategory(sAlliance, catIDUploadLocation, "1")
                    End If


                    'create project if needed
                    sProjectID = ""
                    If GetDataTable("select projectid from ipm_project where category_id = " & sAllianceCatID & " and name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sProjectID = GetDataTable("select projectid from ipm_project where category_id = " & sAllianceCatID & " and name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("projectid")
                    Else
                        sProjectID = CreateNewProject(sProjectName.Trim, sAllianceCatID, sVisionProjectNumber)
                    End If

                    'create project folders if needed
                    Dim sProjectFolderArray() As String
                    Dim sProjectfolderID As String = "0"
                    Try
                        If AllFiles.Rows(i)("folders").trim <> "" Then
                            sProjectFolderArray = AllFiles.Rows(i)("folders").split("\")
                            'assume value
                            Dim ii As Integer
                            For ii = 0 To sProjectFolderArray.Length - 1
                                If ii = 0 Then 'item at root

                                    sProjectfolderID = checkorCreateProjectfolder("0", sProjectFolderArray(ii), sProjectID)
                                Else
                                    If sProjectFolderArray(ii).Trim <> "" Then
                                        sProjectfolderID = checkorCreateProjectfolder(GetDataTable("select category_id from ipm_asset_category where projectid = " & sProjectID & " and name = '" & sProjectFolderArray(ii - 1).Trim.Replace("'", "''") & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id"), sProjectFolderArray(ii), sProjectID)
                                    Else
                                        'sProjectfolderID = "0"
                                    End If

                                End If
                            Next ii
                        Else
                            sProjectfolderID = "0"
                        End If
                    Catch ex As Exception
                        'put in general
                        sProjectfolderID = "0"
                    End Try



                    'project folder override for level
                    'get level
                    'select a.[approval status] from cumulus a, filelisting b where a.id = b.id and a.id = " &  AllFiles.Rows(i)("id")
                    Dim sLevel As String = "Level2"
                    Try
                        If GetDataTable("select a.[approval status] level from cumulus a, filelisting b where a.id = b.id and a.id = " & AllFiles.Rows(i)("id"), ConnectionStringIDAM).Rows(0)("level") = "L1" Then
                            sLevel = "Level1"
                        End If
                    Catch ex As Exception

                    End Try

                    'set folderid 
                    Try
                        Select Case sLevel
                            Case "Level1"
                                sProjectfolderID = GetDataTable("select category_id from ipm_asset_category where name = 'Images' and projectid = " & sProjectID, ConnectionStringIDAM).Rows(0)("category_id")
                            Case "Level2"
                                sProjectfolderID = GetDataTable("select category_id from ipm_asset_category where name = 'Supplemental Imagery' and projectid = " & sProjectID, ConnectionStringIDAM).Rows(0)("category_id")
                        End Select
                    Catch ex As Exception
                        sProjectfolderID = "0"
                    End Try



                    'get media_type
                    'get extension
                    Dim sExtensionid As String
                    Dim sExtensiontmp() = AllFiles.Rows(i)("Files").split(".")
                    Dim sExtension As String
                    If sExtensiontmp.Length = 1 Then
                        sExtension = "UNK"
                    Else
                        sExtensiontmp.Reverse(sExtensiontmp)
                        sExtension = sExtensiontmp(0)
                    End If
                    'check to see if in filetype_lookup
                    imTypeid = imTypeid + 1
                    Try
                        sExtensionid = GetDataTable("select * from ipm_filetype_lookup where extension = '" & sExtension & "'", Me.ConnectionStringIDAM).Rows(0)("media_type")
                    Catch ex As Exception

                        'add extension
                        Me.ExecuteTransaction("insert into ipm_filetype_lookup (media_type,name,extension,useicon) values (" & imTypeid & ",'" & sExtension & " File" & "','" & sExtension & "',1)")
                        sExtensionid = imTypeid
                    End Try


                    'add asset
                    Dim sAssetID As String
                    'sAssetID = CreateAsset("0","1",sAssetName,"1",sProjectID



                    'check to see if asset already exists and use update
                    If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("id"), ConnectionStringIDAM).Rows.Count = 1 Then
                        'update location_id,projectid,category_id,
                        If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("id") & " and projectid = " & sProjectID & " and category_id = " & sProjectfolderID, ConnectionStringIDAM).Rows.Count = 0 Then
                            'assume asset is already there just not in the right spot
                            Me.ExecuteTransaction("update ipm_asset set projectid = " & sProjectID & ",category_id = " & sProjectfolderID & " where asset_id = " & AllFiles.Rows(i)("id"))
                        End If
                    Else
                        Try
                            Me.ExecuteTransaction("insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date) values ('" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sProjectID & ",'1'," & AllFiles.Rows(i)("id") & ",1,1,'" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sExtensionid & "," & sProjectID & "," & sProjectfolderID & ",'" & AllFiles.Rows(i)("filedate") & "','" & AllFiles.Rows(i)("filedate") & "')")
                        Catch ex As Exception
                            ' MsgBox(ex.Message & ": " & "insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date) values ('" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sProjectID & ",'1'," & AllFiles.Rows(i)("id") & ",1,1,'" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sExtensionid & "," & sProjectID & "," & sProjectfolderID & ",'" & AllFiles.Rows(i)("filedate") & "','" & AllFiles.Rows(i)("filedate") & "')")
                        End Try
                    End If

                    'add keyword (Level)

                    Dim sDiscipline As String = sLevel
                    'check for discpliine
                    Dim DTDiscipline As DataTable = GetDataTable("select keyid from ipm_services where keyuse = 1 and keyname = '" & sDiscipline.Trim().Replace("'", "''") & "'", Me.ConnectionStringIDAM)
                    If DTDiscipline.Rows.Count > 0 Then
                        'discpline exists so simply reference
                        ExecuteTransaction("delete from ipm_asset_services where asset_id = " & AllFiles.Rows(i)("id") & " and keyid = " & DTDiscipline.Rows(0)("Keyid"))
                        ExecuteTransaction("insert into ipm_asset_services (keyid,asset_id) values (" & DTDiscipline.Rows(0)("Keyid") & "," & AllFiles.Rows(i)("id") & ")")
                    Else
                        'add to disipline table
                        Dim sql As String
                        sql = "exec sp_createnewkeywordServices "
                        sql += "1,"
                        sql += "'" + sDiscipline.ToString().Replace("'", "''") + "', "
                        sql += "1" + ", "
                        sql += "'Imported from external source'"
                        ExecuteTransaction(sql)
                        'get ID of repo just inserted
                        Dim NewKeywordID As String = GetDataTable("select max(keyid) maxid from ipm_services", Me.ConnectionStringIDAM).Rows(0)("maxid")
                        'add external id
                        ExecuteTransaction("insert into ipm_asset_services (keyid,projectid) values (" & NewKeywordID & "," & AllFiles.Rows(i)("id") & ")")
                    End If

                    'add user defined fields

                    Dim UDFSource As DataTable = GetDataTable("select * from cumulus where id = " & AllFiles.Rows(i)("id"), Me.ConnectionStringIDAM)
                    If UDFSource.Rows.Count > 0 Then
                        Dim IDAM_PHOTOGRAPHER_CREDIT, IDAM_PHOTOGRAPHER_CONTACT_INFO, IDAM_ASSET_RIGHTS, IDAM_ARCHIVE_CD, IDAM_CAPTION, IDAM_CUMULUS_CAPTION, IDAM_WORKING_CD As String
                        '21503637:               IDAM_PHOTOGRAPHER_CREDIT()
                        '21503638:               IDAM_PHOTOGRAPHER_CONTACT_INFO()
                        '21503639:               IDAM_ASSET_RIGHTS()
                        '21503640:               IDAM_ARCHIVE_CD()
                        '21503643:               IDAM_CAPTION()
                        '21503642:               IDAM_CUMULUS_CAPTION()
                        '21503641:               IDAM_WORKING_CD()

                        'assign photo credit
                        IDAM_PHOTOGRAPHER_CREDIT = isullcheck(UDFSource.Rows(0)("photographer(s)"))
                        If IDAM_PHOTOGRAPHER_CREDIT <> "" Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id= 21503637 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503637,'" & IDAM_PHOTOGRAPHER_CREDIT & "')")
                        End If

                        IDAM_ARCHIVE_CD = isullcheck(UDFSource.Rows(0)("Archive CD#"))
                        If IDAM_ARCHIVE_CD <> "" Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503640 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503640,'" & IDAM_ARCHIVE_CD & "')")
                        End If

                        IDAM_WORKING_CD = isullcheck(UDFSource.Rows(0)("Working CD#"))
                        If IDAM_WORKING_CD <> "" Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503641 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503641,'" & IDAM_WORKING_CD & "')")
                        End If

                        IDAM_CUMULUS_CAPTION = isullcheck(UDFSource.Rows(0)("Caption"))
                        If IDAM_CUMULUS_CAPTION <> "" And Not IDAM_CUMULUS_CAPTION.IndexOf("Adobe Photoshop 7.0") > 0 Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503642 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503642,'" & IDAM_CUMULUS_CAPTION & "')")
                        End If

                    End If


                    'add security defaults
                    Dim securityCatTable As DataTable
                    If sProjectfolderID = "0" Then
                        securityCatTable = GetDataTable("select * from ipm_project_security where projecid = " & sProjectID, Me.ConnectionStringIDAM)
                    Else
                        securityCatTable = GetDataTable("select * from ipm_category_security where category_id = " & sProjectfolderID, Me.ConnectionStringIDAM)
                    End If

                    Dim srow As DataRow
                    ExecuteTransaction("DELETE FROM ipm_ASSET_security WHERE ASSET_ID = " & AllFiles.Rows(i)("id"))
                    For Each srow In securityCatTable.Rows
                        ExecuteTransaction("insert into ipm_ASSET_security (ASSET_ID, security_id, type) values (" & AllFiles.Rows(i)("id") & ",'" & srow("security_id") & "','" & srow("type") & "')")
                    Next



                    'set processed flag
                    Me.ExecuteTransaction("update filelisting set processed = 1 where id = " & AllFiles.Rows(i)("id"))
                End If

            Catch ex As Exception
                Me.ExecuteTransaction("update filelisting set processed = 3 where id = " & AllFiles.Rows(i)("id"))
                Me.SendError(ex.Message, "MAIN CATCH", "UNKNOWN")
            End Try
        Next i
        Dim x As String = "what the"








    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        'add project plates
        ' types of imports
        '1 projectid and projectnumber = no prob
        '2 projectid no project num = no prob
        '3 projectnum and no projectid = new project in misc folder - import from vision
        '4 neither = new project in misc folder

        'update FILELISTINGPROJECTPLATES set projectname = replace(replace(replace(directory,'x:\project plates\',''),'\fonts',''),'\links','') 

        'linked set
        'select a.id,b.projectnumber,c.name from FILELISTINGPROJECTPLATES a, project_plate_mapping b, ipm_project c where replace(replace(replace(a.directory,'x:\project plates\',''),'\fonts',''),'\links','') = b.name and b.projectnumber is not null and c.projectnumber = b.projectnumber

        'all others
        'select
        'replace(replace(replace(directory,'x:\project plates\',''),'\fonts',''),'\links','') 
        'from FILELISTINGPROJECTPLATES where id not in (
        'select a.id from FILELISTINGPROJECTPLATES a, project_plate_mapping b, ipm_project c
        'where replace(replace(replace(a.directory,'x:\project plates\',''),'\fonts',''),'\links','') = b.name
        'and b.projectnumber is not null
        'and c.projectnumber = b.projectnumber)
        'group by replace(replace(replace(directory,'x:\project plates\',''),'\fonts',''),'\links','')

        'do not upload fonts
        Dim sqltmp As String = ""
        sqltmp = "update FILELISTINGPROJECTPLATES set processed = 0"
        Me.ExecuteTransaction(sqltmp)

        'get category id to populate
        ' Dim catIDUploadLocation As String = GetDataTable("select category_id from ipm_category where name = 'Projects' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Dim AllFiles As DataTable = GetDataTable("select a.filedate,a.files,c.projectid,a.id,b.projectnumber,c.name from FILELISTINGPROJECTPLATES a, project_plate_mapping b, ipm_project c where replace(replace(replace(a.directory,'x:\project plates\',''),'\fonts',''),'\links','') = b.name and b.projectnumber is not null and c.projectnumber = b.projectnumber and c.available = 'Y'", ConnectionStringIDAM)  'and oid = '041018.00'
        Dim i As Integer
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                ProgressBar1.Value = ProgressBar1.Value + 1
                If Me.chkUpdateOnly.Checked Then
                    Dim sAlliance As String = ""
                    Dim sAllianceFolder, sVisionProjectNumber As String
                    Dim sAllianceCatID, sProjectfolderID As String
                    Dim sDirectoryRoot As String = "X:\"
                    Dim sProjectID As String = AllFiles.Rows(i)("projectid")
                    ListView1.Items.Add(sProjectID + ": " + AllFiles.Rows(i)("files"))

                    ListView1.Refresh()
                    Me.Refresh()


                    'create alliance dir if needed
                    'does it exist already?
                    sProjectfolderID = ""
                    If GetDataTable("select category_id from ipm_asset_category where name = 'Marketing Materials' and projectid = " & sProjectID & " and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sProjectfolderID = GetDataTable("select category_id from ipm_asset_category where name = 'Marketing Materials' and projectid = " & sProjectID & " and available = 'Y'", ConnectionStringIDAM).Rows(0)("Category_id")
                    Else
                        sProjectfolderID = "0"
                    End If


                    'get media_type
                    'get extension
                    Dim sExtensionid As String
                    Dim sExtensiontmp() = AllFiles.Rows(i)("Files").split(".")
                    Dim sExtension As String
                    If sExtensiontmp.Length = 1 Then
                        sExtension = "UNK"
                    Else
                        sExtensiontmp.Reverse(sExtensiontmp)
                        sExtension = sExtensiontmp(0)
                    End If
                    'check to see if in filetype_lookup
                    imTypeid = imTypeid + 1
                    Try
                        sExtensionid = GetDataTable("select * from ipm_filetype_lookup where extension = '" & sExtension & "'", Me.ConnectionStringIDAM).Rows(0)("media_type")
                    Catch ex As Exception

                        'add extension
                        Me.ExecuteTransaction("insert into ipm_filetype_lookup (media_type,name,extension,useicon) values (" & imTypeid & ",'" & sExtension & " File" & "','" & sExtension & "',1)")
                        sExtensionid = imTypeid
                    End Try


                    'add asset
                    Dim sAssetID As String
                    'sAssetID = CreateAsset("0","1",sAssetName,"1",sProjectID

                    If sExtension.ToLower <> "ttf" And sExtension.ToLower <> "pfm" And sExtension.ToLower <> "pfb" Then
                        'check to see if asset already exists and use update
                        If chkUpdateOnly.Checked Then
                            'update location_id,projectid,category_id,
                            If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("id") & " and projectid = " & sProjectID & " and category_id = " & sProjectfolderID, ConnectionStringIDAM).Rows.Count = 0 Then
                                'assume asset is already there just not in the right spot
                                Me.ExecuteTransaction("update ipm_asset set projectid = " & sProjectID & ",category_id = " & sProjectfolderID & " where asset_id = " & AllFiles.Rows(i)("id"))
                            End If
                        Else
                            'Me.ExecuteTransaction("insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date) values ('" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sProjectID & ",'1'," & AllFiles.Rows(i)("id") & ",1,1,'" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sExtensionid & "," & sProjectID & "," & sProjectfolderID & ",getdate(),getdate())")
                        End If
                        'set processed flag to processed
                        Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 1 where id = " & AllFiles.Rows(i)("id"))
                    Else
                        'set processed flag to no import
                        Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 5 where id = " & AllFiles.Rows(i)("id"))
                    End If


                End If

            Catch ex As Exception
                Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 3 where id = " & AllFiles.Rows(i)("id"))
                Me.SendError(ex.Message, "MAIN CATCH", "UNKNOWN")
            End Try
        Next i
        Dim x As String = "what the"
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button10_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        'Dim sql As String = "select asset_id from ipm_asset where DATEPART(month, creation_date) = '11' and DATEPART(year, creation_date) = '2007' and DATEPART(day, creation_date) = '18' "
        Dim sRootRepository As String = "d:\IDAM\repository\"
        Dim AllFiles As DataTable = GetDataTable("select b.asset_id, replace(a.directory,'MarketingMasters\','') directory, a.files, b.projectid, c.extension from filelisting a, ipm_asset b, ipm_filetype_lookup c where a.id = b.asset_id and b.media_type = c.media_type  and id in  (select asset_id from ipm_asset where DATEPART(month, creation_date) = '11' and DATEPART(year, creation_date) = '2007' and DATEPART(day, creation_date) = '18' )", ConnectionStringIDAM)
        Dim i As Integer
        Dim sExtensionid As String
        Dim sSourceFile As String
        Dim sDestinationFile As String
        Dim sTifSwitch As Boolean = False
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                If InStr(AllFiles.Rows(i)("Files"), ".jpg") > 0 Then 'only process jpg files
                    'get SourceFile
                    sSourceFile = ""
                    sDestinationFile = ""
                    sSourceFile = "x:\" & AllFiles.Rows(i)("Directory") + "\" + AllFiles.Rows(i)("Files")
                    sDestinationFile = sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource\" + AllFiles.Rows(i)("asset_id").ToString.Trim + "." + AllFiles.Rows(i)("extension").ToString.Trim
                    'is directories created?
                    If Not IO.Directory.Exists(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim) Then
                        IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim)
                        IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource")
                        IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\acrobat")
                    End If
                    'now copy files
                    'check to see if tiff exists .tif
                    If IO.File.Exists(Replace(sSourceFile, ".jpg", ".tif")) Then
                        'delete jpg
                        'IO.File.Delete(sDestinationFile)
                        'IO.File.Copy(Replace(sSourceFile, ".jpg", ".tif"), Replace(sDestinationFile, ".jpg", ".tif"), False)
                        'change extension of asset
                        ExecuteTransaction("update ipm_asset set media_type = '85', name = '" & Replace(AllFiles.Rows(i)("Files").replace("'", "''"), ".jpg", ".tif") & "', filename = '" & Replace(AllFiles.Rows(i)("Files").replace("'", "''"), ".jpg", ".tif") & "' where asset_id = " & AllFiles.Rows(i)("asset_id"))
                        sTifSwitch = True
                    Else
                        'already there...
                        If Not IO.File.Exists(sDestinationFile) Then
                            IO.File.Copy(sSourceFile, sDestinationFile, False)
                        End If
                        Me.ExecuteTransaction("insert into ipm_asset_queue(instance_id,asset_id,asset_handler,status,priority,engine_id)values('IDAM_NBBJ'," & AllFiles.Rows(i)("asset_id").ToString.Trim & ",2,0,1,'20')")
                    End If

                    'now send to ipm_asset_queue and mark processed = 4
                    If sTifSwitch Then
                        Me.ExecuteTransaction("insert into ipm_asset_queue(instance_id,asset_id,asset_handler,status,priority,engine_id)values('IDAM_NBBJ'," & AllFiles.Rows(i)("asset_id").ToString.Trim & ",2,0,1,'20')")
                        Me.ExecuteTransaction("update filelisting set processed = 5 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)
                    End If
                End If
                Me.ExecuteTransaction("update filelisting set processed = 7 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)
            Catch ex As Exception
                'MsgBox(ex.Message & AllFiles.Rows(i)("asset_id"))
                Me.ExecuteTransaction("update filelisting set processed = 6 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)
            End Try
            ProgressBar1.Value = ProgressBar1.Value + 1
        Next


    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        'Dim sql As String = "select asset_id from ipm_asset where DATEPART(month, creation_date) = '11' and DATEPART(year, creation_date) = '2007' and DATEPART(day, creation_date) = '18' "
        Dim sRootRepository As String = "d:\IDAM\repository\"
        Dim AllFiles As DataTable = GetDataTable("select b.asset_id, replace(a.directory,'MarketingMasters\','') directory, a.files, b.projectid, c.extension from filelisting a, ipm_asset b, ipm_filetype_lookup c where a.id = b.asset_id and b.media_type = c.media_type  and id in  (select asset_id from ipm_asset where DATEPART(month, creation_date) = '11' and DATEPART(year, creation_date) = '2007' and DATEPART(day, creation_date) = '18' )", ConnectionStringIDAM)
        Dim i As Integer
        Dim sExtensionid As String
        Dim sSourceFile As String
        Dim sDestinationFile As String
        Dim sTifSwitch As Boolean = False
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                If InStr(AllFiles.Rows(i)("Files"), ".jpg") > 0 Then 'only process jpg files
                    'get SourceFile
                    sSourceFile = ""
                    sDestinationFile = ""
                    sSourceFile = "x:\" & AllFiles.Rows(i)("Directory") + "\" + AllFiles.Rows(i)("Files")
                    sDestinationFile = sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource\" + AllFiles.Rows(i)("asset_id").ToString.Trim + "." + AllFiles.Rows(i)("extension").ToString.Trim
                    'is directories created?
                    ''''If Not IO.Directory.Exists(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim) Then
                    ''''    IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim)
                    ''''    IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource")
                    ''''    IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\acrobat")
                    ''''End If
                    'now copy files
                    'check to see if tiff exists .tif
                    If IO.File.Exists(Replace(sSourceFile, ".jpg", ".tif")) Then
                        'delete jpg
                        'IO.File.Delete(sDestinationFile)
                        'IO.File.Copy(Replace(sSourceFile, ".jpg", ".tif"), Replace(sDestinationFile, ".jpg", ".tif"), False)
                        'change extension of asset
                        ExecuteTransaction("update ipm_asset set media_type = '85', name = '" & Replace(AllFiles.Rows(i)("Files").replace("'", "''"), ".jpg", ".tif") & "', filename = '" & Replace(AllFiles.Rows(i)("Files").replace("'", "''"), ".jpg", ".tif") & "' where asset_id = " & AllFiles.Rows(i)("asset_id"))
                        sTifSwitch = True
                    Else
                        'already there...
                        If Not IO.File.Exists(sDestinationFile) Then
                            IO.File.Copy(sSourceFile, sDestinationFile, False)
                        End If
                        Me.ExecuteTransaction("insert into ipm_asset_queue(instance_id,asset_id,asset_handler,status,priority,engine_id)values('IDAM_NBBJ'," & AllFiles.Rows(i)("asset_id").ToString.Trim & ",2,0,1,'20')")
                    End If

                    'now send to ipm_asset_queue and mark processed = 4
                    If sTifSwitch Then
                        Me.ExecuteTransaction("insert into ipm_asset_queue(instance_id,asset_id,asset_handler,status,priority,engine_id)values('IDAM_NBBJ'," & AllFiles.Rows(i)("asset_id").ToString.Trim & ",2,0,1,'20')")
                        Me.ExecuteTransaction("update filelisting set processed = 5 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)
                    End If
                End If
                Me.ExecuteTransaction("update filelisting set processed = 7 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)
            Catch ex As Exception
                'MsgBox(ex.Message & AllFiles.Rows(i)("asset_id"))
                Me.ExecuteTransaction("update filelisting set processed = 6 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)
            End Try
            ProgressBar1.Value = ProgressBar1.Value + 1
        Next

    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        Dim i As Integer
        Dim sProjectID, sProjectfolderID, sAsset_ID As String

        'update everything to administrative security level
        'Get Template Project ID
        Dim sTemplateProjectID = "21523106"
        'Global settings
        ExecuteTransaction("update ipm_project set securitylevel_id = 0")
        Dim AllProjects As DataTable = GetDataTable("select projectid from ipm_project where available = 'Y' and projectid <> " & sTemplateProjectID, ConnectionStringIDAM)    'and oid = '041018.00' 'and projectid <> " & sTemplateProjectID
        ProgressBar1.Maximum = AllProjects.Rows.Count
        ProgressBar1.Value = 0
        Dim securityProjectTable, securityProjectFoldersTable, securityProjectFolderTable, ProjectFoldersTable, securityOverridesProjectFolderTable, securityProjectOverridesTable As DataTable
        For i = 0 To AllProjects.Rows.Count - 1
            Try
                'ensure you do not overwrite the template
                sProjectID = AllProjects.Rows(i).Item("projectid").ToString.Trim()
                'If sProjectID <> sTemplateProjectID Then
                'set project info

                'set users
                securityProjectTable = GetDataTable("select * from ipm_project_security where projectid = " & sTemplateProjectID, Me.ConnectionStringIDAM)
                ExecuteTransaction("DELETE FROM ipm_project_security WHERE projectid = " & sProjectID)
                For Each srow As DataRow In securityProjectTable.Rows
                    ExecuteTransaction("insert into ipm_project_security (projectid, security_id, type) values (" & sProjectID & ",'" & srow.Item("security_id").trim & "','" & srow.Item("type") & "')")
                Next

                'set overrides
                securityProjectOverridesTable = GetDataTable("select * from ipm_project_role_override where projectid = " & sTemplateProjectID, Me.ConnectionStringIDAM)
                ExecuteTransaction("DELETE FROM ipm_project_role_override WHERE projectid = " & sProjectID)
                For Each srowx As DataRow In securityProjectOverridesTable.Rows
                    ExecuteTransaction("insert into ipm_project_role_override (projectid, security_id, type, item_id, item_value) values (" & sProjectID & ",'" & srowx.Item("security_id").trim & "','" & srowx.Item("type") & "','" & srowx.Item("item_id") & "','" & srowx.Item("item_value") & "')")
                Next

                'for each folder...do the same.
                'get all template folders
                ProjectFoldersTable = GetDataTable("select * from ipm_asset_category where available = 'Y' and projectid = " & sTemplateProjectID, Me.ConnectionStringIDAM)
                For Each srow2 As DataRow In ProjectFoldersTable.Rows
                    'find folder from project
                    Dim sTemplateFolderID As String
                    Dim sTemplateParentFolderID As String
                    sTemplateFolderID = srow2.Item("category_id")
                    sTemplateParentFolderID = srow2.Item("parent_cat_id").ToString.Trim
                    Dim sSourceFolderID As String
                    If sTemplateParentFolderID = "0" Then
                        sSourceFolderID = GetDataTable("select * from ipm_asset_category where parent_cat_id = 0 and available = 'Y' and name = '" & srow2.Item("Name") & "' and projectid = " & sProjectID, Me.ConnectionStringIDAM).Rows(0).Item("category_id")
                    Else
                        'get parent cat name
                        Dim sTemplateParentFolderName = GetDataTable("select name from ipm_asset_category where category_id = " & sTemplateParentFolderID, Me.ConnectionStringIDAM).Rows(0).Item("name")
                        sSourceFolderID = GetDataTable("select * from ipm_asset_category where parent_cat_id = (select category_id from ipm_asset_category where available = 'Y' and projectid = " & sProjectID & " and name = '" & sTemplateParentFolderName & "') and available = 'Y' and name = '" & srow2.Item("Name") & "' and projectid = " & sProjectID, Me.ConnectionStringIDAM).Rows(0).Item("category_id")
                    End If
                    'add permissions
                    ExecuteTransaction("DELETE FROM ipm_category_security WHERE category_id = " & sSourceFolderID)
                    securityProjectFolderTable = GetDataTable("select * from ipm_category_security where category_id = " & sTemplateFolderID, Me.ConnectionStringIDAM)
                    For Each srowPFTS As DataRow In securityProjectFolderTable.Rows
                        ExecuteTransaction("insert into ipm_category_security (category_id, security_id, type) values (" & sSourceFolderID & ",'" & srowPFTS.Item("security_id").trim & "','" & srowPFTS.Item("type") & "')")
                    Next
                    'set overrides
                    ExecuteTransaction("DELETE FROM IPM_ASSET_CATEGORY_ROLE_OVERRIDE WHERE category_id = " & sSourceFolderID)
                    securityOverridesProjectFolderTable = GetDataTable("select * from IPM_ASSET_CATEGORY_ROLE_OVERRIDE where category_id = " & sTemplateFolderID, Me.ConnectionStringIDAM)
                    For Each srowPFTSOVERRIDE As DataRow In securityOverridesProjectFolderTable.Rows
                        ExecuteTransaction("insert into IPM_ASSET_CATEGORY_ROLE_OVERRIDE (category_id, security_id, type, item_id, item_value) values (" & sSourceFolderID & ",'" & srowPFTSOVERRIDE.Item("security_id").trim & "','" & srowPFTSOVERRIDE.Item("type") & "','" & srowPFTSOVERRIDE.Item("item_id") & "','" & srowPFTSOVERRIDE.Item("item_value") & "')")
                    Next
                Next

                ' End If

            Catch ex As Exception

            End Try
            ProgressBar1.Value = ProgressBar1.Value + 1
            Me.Refresh()
        Next



        'process filelist (filter out thumbs.db)
        Dim AllFiles As DataTable = GetDataTable("select asset_id,projectid,category_id from ipm_asset where available = 'Y'", ConnectionStringIDAM)  'and oid = '041018.00'
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                sProjectID = AllFiles.Rows(i).Item("projectid").ToString.Trim
                sProjectfolderID = AllFiles.Rows(i).Item("category_id").ToString.Trim
                sAsset_ID = AllFiles.Rows(i).Item("asset_id").ToString.Trim
                'add security defaults
                Dim securityCatTable As DataTable
                If sProjectfolderID = "0" Then
                    securityCatTable = GetDataTable("select * from ipm_project_security where projecid = " & sProjectID, Me.ConnectionStringIDAM)
                Else
                    securityCatTable = GetDataTable("select * from ipm_category_security where category_id = " & sProjectfolderID, Me.ConnectionStringIDAM)
                End If

                Dim srow As DataRow
                ExecuteTransaction("DELETE FROM ipm_ASSET_security WHERE ASSET_ID = " & sAsset_ID)
                For Each srow In securityCatTable.Rows
                    ExecuteTransaction("insert into ipm_ASSET_security (ASSET_ID, security_id, type) values (" & sAsset_ID & ",'" & srow("security_id") & "','" & srow("type") & "')")
                Next
            Catch ex As Exception

            End Try
            ProgressBar1.Value = ProgressBar1.Value + 1
            Me.Refresh()
        Next
        ExecuteTransaction("update ipm_asset set securitylevel_id = 0 where available = 'Y'")


    End Sub

    Private Sub chkUpdateOnly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUpdateOnly.CheckedChanged

    End Sub




























    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        'get category id to populate
        Dim catIDUploadLocation As String = GetDataTable("select category_id from ipm_category where name = 'Photo Archives' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Dim AllFiles As DataTable = GetDataTable("select * from photarch a, desi b where a.designat = b.code", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
        'Me.ExecuteTransaction("delete from ipm_asset")
        'Me.ExecuteTransaction("delete from ipm_asset_category")
        Dim i As Integer
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                ProgressBar1.Value = ProgressBar1.Value + 1
                If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("refno"), ConnectionStringIDAM).Rows.Count = 0 Or Me.chkUpdateOnly.Checked Then
                    Dim sAlliance As String = ""
                    Dim sAllianceFolder, sVisionProjectNumber As String
                    Dim sAllianceCatID, sProjectID As String
                    Dim sDirectoryRoot As String = "V:\"
                    Dim sProjectName As String = "Photo Archives Assets"
                    ''''''''ListView1.Items.Add(sProjectName + ": " + AllFiles.Rows(i)("hier_desi") + ":" + AllFiles.Rows(i)("refno").ToString)

                    '''''''ListView1.Refresh()
                    '''''''Me.Refresh()

                    'get vision project number
                    sVisionProjectNumber = ""

                    'get Alliance
                    sAlliance = ""

                    '''''''create alliance dir if needed
                    '''''''does it exist already?
                    ''''''sAllianceCatID = ""
                    ''''''If GetDataTable("select category_id from ipm_category where parent_cat_id = " & catIDUploadLocation & " and name = '" & sAlliance.Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                    ''''''    'get cat id
                    ''''''    sAllianceCatID = GetDataTable("select category_id from ipm_category where parent_cat_id = " & catIDUploadLocation & " and name = '" & sAlliance.Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("Category_id")
                    ''''''Else
                    ''''''    sAllianceCatID = CreateNewAllianceCategory(sAlliance, catIDUploadLocation, "1")
                    ''''''End If


                    'create project if needed
                    sProjectID = "21537181"
                    '''''''If GetDataTable("select projectid from ipm_project where name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                    '''''''    'get cat id
                    '''''''    sProjectID = GetDataTable("select projectid from ipm_project where name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("projectid")
                    '''''''Else
                    '''''''    sProjectID = CreateNewProject(sProjectName.Trim, 1, 1)
                    '''''''End If

                    'create project folders if needed
                    Dim sProjectFolderArray() As String
                    Dim sProjectfolderID As String = "0"
                    Try
                        If AllFiles.Rows(i)("hier_desi").trim <> "" Then
                            sProjectFolderArray = AllFiles.Rows(i)("hier_desi").trim.replace("--", "|").split("|")
                            'assume value
                            Dim ii As Integer
                            For ii = 0 To sProjectFolderArray.Length - 1
                                If ii = 0 Then 'item at root

                                    sProjectfolderID = checkorCreateProjectfolder("0", sProjectFolderArray(ii).Trim, sProjectID)
                                Else
                                    If sProjectFolderArray(ii).Trim <> "" Then
                                        sProjectfolderID = checkorCreateProjectfolder(GetDataTable("select category_id from ipm_asset_category where projectid = " & sProjectID & " and name = '" & sProjectFolderArray(ii - 1).Trim.Replace("'", "''") & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id"), sProjectFolderArray(ii).Trim, sProjectID)
                                    Else
                                        'sProjectfolderID = "0"
                                    End If

                                End If
                            Next ii
                        Else
                            sProjectfolderID = "0"
                        End If
                    Catch ex As Exception
                        'put in general
                        sProjectfolderID = "0"
                    End Try


                    'get media_type
                    'get extension
                    Dim sExtensionid As String
                    Dim sExtensiontmp
                    Dim sExtension As String = AllFiles.Rows(i)("img_format")

                    'check to see if in filetype_lookup
                    imTypeid = imTypeid + 1
                    Try
                        sExtensionid = GetDataTable("select * from ipm_filetype_lookup where extension = '" & sExtension & "'", Me.ConnectionStringIDAM).Rows(0)("media_type")
                    Catch ex As Exception

                        'add extension
                        Me.ExecuteTransaction("insert into ipm_filetype_lookup (media_type,name,extension,useicon) values (" & imTypeid & ",'" & sExtension & " File" & "','" & sExtension & "',1)")
                        sExtensionid = imTypeid
                    End Try


                    'add asset
                    Dim sAssetID As String
                    'sAssetID = CreateAsset("0","1",sAssetName,"1",sProjectID



                    'check to see if asset already exists and use update
                    If chkUpdateOnly.Checked Then
                        'update location_id,projectid,category_id,
                        If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("idrefno") & " and projectid = " & sProjectID & " and category_id = " & sProjectfolderID, ConnectionStringIDAM).Rows.Count = 0 Then
                            'assume asset is already there just not in the right spot
                            Me.ExecuteTransaction("update ipm_asset set projectid = " & sProjectID & ",category_id = " & sProjectfolderID & " where asset_id = " & AllFiles.Rows(i)("refno"))
                        End If
                    Else
                        Me.ExecuteTransaction("insert into ipm_asset(description,filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date) values ('" & Replace(AllFiles.Rows(i)("subject"), "'", "''") & "','" & Replace(AllFiles.Rows(i)("refno"), "'", "''") & "'," & sProjectID & ",'1'," & AllFiles.Rows(i)("refno") & ",1,1,'" & Replace(AllFiles.Rows(i)("refno"), "'", "''") & "'," & sExtensionid & "," & sProjectID & "," & sProjectfolderID & ",'" & AllFiles.Rows(i)("lmdate") & "','" & AllFiles.Rows(i)("date1") & "')")
                    End If

                    ''''''''''''add keyword (Level)

                    '''''''''''Dim sDiscipline As String = sLevel
                    ''''''''''''check for discpliine
                    '''''''''''Dim DTDiscipline As DataTable = GetDataTable("select keyid from ipm_services where keyuse = 1 and keyname = '" & sDiscipline.Trim().Replace("'", "''") & "'", Me.ConnectionStringIDAM)
                    '''''''''''If DTDiscipline.Rows.Count > 0 Then
                    '''''''''''    'discpline exists so simply reference
                    '''''''''''    ExecuteTransaction("delete from ipm_asset_services where asset_id = " & AllFiles.Rows(i)("id") & " and keyid = " & DTDiscipline.Rows(0)("Keyid"))
                    '''''''''''    ExecuteTransaction("insert into ipm_asset_services (keyid,asset_id) values (" & DTDiscipline.Rows(0)("Keyid") & "," & AllFiles.Rows(i)("id") & ")")
                    '''''''''''Else
                    '''''''''''    'add to disipline table
                    '''''''''''    Dim sql As String
                    '''''''''''    sql = "exec sp_createnewkeywordServices "
                    '''''''''''    sql += "1,"
                    '''''''''''    sql += "'" + sDiscipline.ToString().Replace("'", "''") + "', "
                    '''''''''''    sql += "1" + ", "
                    '''''''''''    sql += "'Imported from external source'"
                    '''''''''''    ExecuteTransaction(sql)
                    '''''''''''    'get ID of repo just inserted
                    '''''''''''    Dim NewKeywordID As String = GetDataTable("select max(keyid) maxid from ipm_services", Me.ConnectionStringIDAM).Rows(0)("maxid")
                    '''''''''''    'add external id
                    '''''''''''    ExecuteTransaction("insert into ipm_asset_services (keyid,projectid) values (" & NewKeywordID & "," & AllFiles.Rows(i)("id") & ")")
                    '''''''''''End If

                    ''''''''''''add user defined fields

                    '''''''''''Dim UDFSource As DataTable = GetDataTable("select * from cumulus where id = " & AllFiles.Rows(i)("id"), Me.ConnectionStringIDAM)
                    '''''''''''If UDFSource.Rows.Count > 0 Then
                    '''''''''''    Dim IDAM_PHOTOGRAPHER_CREDIT, IDAM_PHOTOGRAPHER_CONTACT_INFO, IDAM_ASSET_RIGHTS, IDAM_ARCHIVE_CD, IDAM_CAPTION, IDAM_CUMULUS_CAPTION, IDAM_WORKING_CD As String
                    '''''''''''    '21503637:               IDAM_PHOTOGRAPHER_CREDIT()
                    '''''''''''    '21503638:               IDAM_PHOTOGRAPHER_CONTACT_INFO()
                    '''''''''''    '21503639:               IDAM_ASSET_RIGHTS()
                    '''''''''''    '21503640:               IDAM_ARCHIVE_CD()
                    '''''''''''    '21503643:               IDAM_CAPTION()
                    '''''''''''    '21503642:               IDAM_CUMULUS_CAPTION()
                    '''''''''''    '21503641:               IDAM_WORKING_CD()

                    '''''''''''    'assign photo credit
                    '''''''''''    IDAM_PHOTOGRAPHER_CREDIT = isullcheck(UDFSource.Rows(0)("photographer(s)"))
                    '''''''''''    If IDAM_PHOTOGRAPHER_CREDIT <> "" Then
                    '''''''''''        'delete from table
                    '''''''''''        ExecuteTransaction("delete from ipm_asset_field_value where item_id= 21503637 and asset_id = " & AllFiles.Rows(i)("id"))
                    '''''''''''        'insert into table
                    '''''''''''        ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503637,'" & IDAM_PHOTOGRAPHER_CREDIT & "')")
                    '''''''''''    End If

                    '''''''''''    IDAM_ARCHIVE_CD = isullcheck(UDFSource.Rows(0)("Archive CD#"))
                    '''''''''''    If IDAM_ARCHIVE_CD <> "" Then
                    '''''''''''        'delete from table
                    '''''''''''        ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503640 and asset_id = " & AllFiles.Rows(i)("id"))
                    '''''''''''        'insert into table
                    '''''''''''        ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503640,'" & IDAM_ARCHIVE_CD & "')")
                    '''''''''''    End If

                    '''''''''''    IDAM_WORKING_CD = isullcheck(UDFSource.Rows(0)("Working CD#"))
                    '''''''''''    If IDAM_WORKING_CD <> "" Then
                    '''''''''''        'delete from table
                    '''''''''''        ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503641 and asset_id = " & AllFiles.Rows(i)("id"))
                    '''''''''''        'insert into table
                    '''''''''''        ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503641,'" & IDAM_WORKING_CD & "')")
                    '''''''''''    End If

                    '''''''''''    IDAM_CUMULUS_CAPTION = isullcheck(UDFSource.Rows(0)("Caption"))
                    '''''''''''    If IDAM_CUMULUS_CAPTION <> "" And Not IDAM_CUMULUS_CAPTION.IndexOf("Adobe Photoshop 7.0") > 0 Then
                    '''''''''''        'delete from table
                    '''''''''''        ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503642 and asset_id = " & AllFiles.Rows(i)("id"))
                    '''''''''''        'insert into table
                    '''''''''''        ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503642,'" & IDAM_CUMULUS_CAPTION & "')")
                    '''''''''''    End If

                    '''''''''''End If


                    ''''''''''add security defaults
                    '''''''''Dim securityCatTable As DataTable
                    '''''''''If sProjectfolderID = "0" Then
                    '''''''''    securityCatTable = GetDataTable("select * from ipm_project_security where projecid = " & sProjectID, Me.ConnectionStringIDAM)
                    '''''''''Else
                    '''''''''    securityCatTable = GetDataTable("select * from ipm_category_security where category_id = " & sProjectfolderID, Me.ConnectionStringIDAM)
                    '''''''''End If

                    '''''''''Dim srow As DataRow
                    '''''''''ExecuteTransaction("DELETE FROM ipm_ASSET_security WHERE ASSET_ID = " & AllFiles.Rows(i)("refno"))
                    '''''''''For Each srow In securityCatTable.Rows
                    '''''''''    ExecuteTransaction("insert into ipm_ASSET_security (ASSET_ID, security_id, type) values (" & AllFiles.Rows(i)("refno") & ",'" & srow("security_id") & "','" & srow("type") & "')")
                    '''''''''Next



                    ''''''''set processed flag
                    '''''''Me.ExecuteTransaction("update filelisting set processed = 1 where id = " & AllFiles.Rows(i)("id"))
                End If

            Catch ex As Exception
                ''''''Me.ExecuteTransaction("update filelisting set processed = 3 where id = " & AllFiles.Rows(i)("id"))
                Me.SendError(ex.Message, "MAIN CATCH", "UNKNOWN")
            End Try
        Next i
        Dim x As String = "what the"

    End Sub

    Private Sub Button18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button18.Click
        processMainAttributes()
    End Sub









    Function processMainAttributes() As Boolean


        Try

            'worksheet
            Dim imTypeid As Integer = 5000
            'process filelist (filter out thumbs.db)
            Dim AllFiles As DataTable '= GetDataTable("select wsno,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            Dim i As Integer
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0





            'drop indexes
            'Me.ExecuteTransaction(GetFileContents(System.AppDomain.CurrentDomain.BaseDirectory() + "delete_IPM_ASSET_FIELD_VALUE_indexes.sql"))



            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548377")
            Me.ExecuteTransaction("insert into ipm_asset_field_value select cast(refno as decimal) asset_id,21548377 item_id,cast(wsno as nvarchar(255)) item_value from [USHMM_PHOTOARCH].[dbo].photarch")


            'CD#
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select cd_number,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548342")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548342 item_id,cast(cd_number as nvarchar(255)) item_value from [USHMM_PHOTOARCH].[dbo].photarch")


            'date Range top
            'process filelist (filter out thumbs.db)
            ' AllFiles = GetDataTable("select date1,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548372")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548372 item_id,cast(date1 as nvarchar(255)) item_value from [USHMM_PHOTOARCH].[dbo].photarch")

            'date Range bottom
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548373")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548373 item_id,cast(date2 as nvarchar(255)) item_value from [USHMM_PHOTOARCH].[dbo].photarch where date2 <> '1899-12-30'")


            'place_name
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548330")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548330 item_id,cast(place_name as nvarchar(1000)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].holdplac b where a.placeix = b.placeix and place_name <> ''")


            'Art place_name
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548376")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548376 item_id,cast(place_name as nvarchar(1000)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].holdplac b where a.art_plaix = b.placeix and place_name <> ''")



            'state
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548333")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548333 item_id,cast(state as nvarchar(255)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].holdplac b where a.placeix = b.placeix and state <> ''")


            'city
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548334")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548334 item_id,cast(city as nvarchar(255)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].holdplac b where a.placeix = b.placeix and city <> ''")

            'region
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548331")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548331 item_id,cast(region as nvarchar(255)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].holdplac b where a.placeix = b.placeix and region <> ''")

            'country
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548332")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548332 item_id,cast(country as nvarchar(255)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].holdplac b where a.placeix = b.placeix and country <> ''")


            'category use
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548344")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(a.refno as decimal) asset_id,21548344 item_id, cast(c.descript as nvarchar(2500)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].otherlnk b, [USHMM_PHOTOARCH].[dbo].catuse c where a.refno = b.refno and b.catid = c.catid and cast(c.descript as nvarchar(2500)) <> ''")



            'Biography
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548336")
            'Me.ExecuteTransaction("insert  into ipm_asset_field_value 	select cast(a.refno as decimal) asset_id,21548336 item_id,cast (c.biocaption as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].biolink b, [USHMM_PHOTOARCH].[dbo].biograph c where a.refno = b.refno and b.bio_id = c.bio_id and cast(c.biocaption as nvarchar(max)) <> ''")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548336 item_id,[USHMM_PHOTOARCH].dbo.List_BIO(refno) item_value from [USHMM_PHOTOARCH].[dbo].photarch a")




            'Notes
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548337")
            'Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(a.refno as decimal) asset_id,21548337 item_id,cast(c.notcaption as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].notelink b, [USHMM_PHOTOARCH].[dbo].notgraph c where a.refno = b.refno and b.note_id = c.note_id and cast(c.notcaption as nvarchar(max)) <> ''")
            'accomodate duplication
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(a.refno as decimal) asset_id,21548337 item_id,cast(c.notcaption as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].notelink b, [USHMM_PHOTOARCH].[dbo].notgraph c where a.refno = b.refno and b.note_id = c.note_id and cast(c.notcaption as nvarchar(max)) <> '' and a.refno in (select asset_id from (select asset_id, count(*) cnt from (select cast(a.refno as decimal) asset_id,21548337 item_id,cast(c.notcaption as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].notelink b, [USHMM_PHOTOARCH].[dbo].notgraph c where a.refno = b.refno and b.note_id = c.note_id and cast(c.notcaption as nvarchar(max)) <> '') a group by asset_id) a where cnt = 1)")






            'Events
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548338")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548338 item_id,[USHMM_PHOTOARCH].dbo.List_EVENT(refno) item_value from [USHMM_PHOTOARCH].[dbo].photarch a")
            'Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(a.refno as decimal) asset_id,21548338 item_id,cast(c.evecaption as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].evelink b, [USHMM_PHOTOARCH].[dbo].evegraph c where a.refno = b.refno and b.eve_id = c.eve_id and cast(c.evecaption as nvarchar(max)) <> ''")


            'Simple Caption
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548335")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(a.refno as decimal) asset_id,21548335 item_id,cast(a.subject as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a where cast(a.subject as nvarchar(max)) <> ''")

            'Simple Caption Populate to Description @ Judy Cohen Request 3.15.10
            Me.ExecuteTransaction("UPDATE IPM_ASSET SET IPM_ASSET.description = ipm_asset_field_value.item_value FROM ipm_asset_field_value INNER JOIN IPM_ASSET ON (ipm_asset_field_value.asset_id = IPM_ASSET.asset_id and ipm_asset_field_value.Item_ID = 21548335)")



            'DontScan
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548340")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548340 item_id,cast(dontscan as nvarchar(255)) item_value from [USHMM_PHOTOARCH].[dbo].photarch where dontscan = 1")


            'Owned
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548341")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548341 item_id,cast(owned as nvarchar(255)) item_value from [USHMM_PHOTOARCH].[dbo].photarch where owned = 1")


            'Use On WebSite
            ProgressBar1.Value = 0
            'delete all previous values
            Me.SendError("About to execute WebSite Tag - Modified", "", "")
            'Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548562")
            'Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548562 item_id,'1' item_value from [USHMM_PHOTOARCH].[dbo].WEBHERE group by refno")

            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548562")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548562 item_id,'1' item_value from [USHMM_PHOTOARCH].[dbo].catuse a, [USHMM_PHOTOARCH].[dbo].otherlnk b where a.catid = b.catid and a.catid in (19,18)  group by cast(refno as decimal) ")


            Me.SendError("Finished executing WebSite Tag - Modified", "", "")

            'incomplete
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548339")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548339 item_id,cast(incomplete as nvarchar(255)) item_value from [USHMM_PHOTOARCH].[dbo].photarch where incomplete = 1")

            'exhibit number
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548343")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(a.refno as decimal) asset_id,21548343 item_id, cast(a.exhibitnum as nvarchar(2500)) item_value from [USHMM_PHOTOARCH].[dbo].exhilink a where  cast(a.exhibitnum as nvarchar(2500)) <> ''")

            'Designation
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548375")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548375 item_id,cast(hier_desi as nvarchar(1500)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].desi b where a.designat = b.code")

            'Designation #
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548374")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548374 item_id,cast(code as nvarchar(255)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].desi b where a.designat = b.code")

            'Art Photographer #
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548329")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548329 item_id,cast(fullname as nvarchar(500)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].fornames b where a.art_phoix  = b.nameix")


            'Photo Credit #
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548323")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548323 item_id,cast(fullname as nvarchar(500)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].fornames b where a.photoix = b.nameix")


            'Photographer #
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548324")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548324 item_id,cast(fullname as nvarchar(500)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].fornames b where a.photoix = b.nameix")




            'Creditline Info
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21575375")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21575375 item_id,cast(case when [restrict] = '' then creditline else [restrict] end as nvarchar(500)) item_value from [USHMM_PHOTOARCH].[dbo].srcelink a")



            'SOURCE #
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548325")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548325 item_id,[USHMM_PHOTOARCH].dbo.List_SOURCES(refno) item_value from [USHMM_PHOTOARCH].[dbo].photarch a")


            'PROVENANCE #
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548327")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548327 item_id,[USHMM_PHOTOARCH].dbo.List_PROVENANCE(refno) item_value from [USHMM_PHOTOARCH].[dbo].photarch a")



            'COPYRIGHT #
            'process filelist (filter out thumbs.db)
            'AllFiles = GetDataTable("select date2,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
            'ProgressBar1.Maximum = AllFiles.Rows.Count
            ProgressBar1.Value = 0
            'delete all previous values
            Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548326")
            Me.ExecuteTransaction("insert  into ipm_asset_field_value select cast(refno as decimal) asset_id,21548326 item_id,cast(c.copyright as nvarchar(500)) item_value from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].fornames b, [USHMM_PHOTOARCH].[dbo].copyrite c where a.primesrcex = b.nameix and b.copyright = c.cr_code")


            'create indexes
            'Me.ExecuteTransaction(GetFileContents(System.AppDomain.CurrentDomain.BaseDirectory() + "create_IPM_ASSET_FIELD_VALUE_indexes.sql"))



            'Process Keywords
            Me.ExecuteTransaction("delete from ipm_asset_services")
            Me.ExecuteTransaction("insert into ipm_asset_services (asset_id,keyid) select a.refno asset_id,a.keyno keyid from [USHMM_PHOTOARCH].dbo.keylink a, [USHMM_PHOTOARCH].dbo.keywords b where a.keyno = b.ixno and b.class = 'P' and a.deleted = 0")
            Me.ExecuteTransaction("delete from ipm_asset_mediatype")
            Me.ExecuteTransaction("insert into ipm_asset_mediatype (asset_id,keyid) select a.refno asset_id,a.keyno keyid from [USHMM_PHOTOARCH].dbo.keylink a, [USHMM_PHOTOARCH].dbo.keywords b where a.keyno = b.ixno and b.class = 'L' and a.deleted = 0")
            Me.ExecuteTransaction("delete from ipm_asset_illusttype")
            Me.ExecuteTransaction("insert into ipm_asset_illusttype (asset_id,keyid) select a.refno asset_id,a.keyno keyid from [USHMM_PHOTOARCH].dbo.keylink a, [USHMM_PHOTOARCH].dbo.keywords b where a.keyno = b.ixno and b.class = 'E' and a.deleted = 0")
            Me.ExecuteTransaction("delete from ipm_asset_tag")
            Me.ExecuteTransaction("insert into ipm_asset_tag (asset_id,tagid,assignedby,category_id) select a.refno asset_id,a.keyno tagid,'1' assignedby,0 category_id from [USHMM_PHOTOARCH].dbo.keylink a, [USHMM_PHOTOARCH].dbo.keywords b where a.keyno = b.ixno and b.class = 'T' and a.deleted = 0")
            Me.ExecuteTransaction("delete from ipm_tag")
            Me.ExecuteTransaction("insert into ipm_tag(tagid,tagname,taguse,createdby) select a.ixno tagid,a.keyword,1 taguse,'1'createdby from [USHMM_PHOTOARCH].dbo.keywords a,[USHMM_PHOTOARCH].dbo.keylink b where a.class = 'T' and b.keyno = a.ixno and b.deleted = 0 group by a.ixno ,a.keyword")
            Me.ExecuteTransaction("delete from ipm_services")
            Me.ExecuteTransaction("insert into ipm_services(keyid,keyname,keyuse) select a.ixno tagid,a.keyword,1 taguse from [USHMM_PHOTOARCH].dbo.keywords a,[USHMM_PHOTOARCH].dbo.keylink b where a.class = 'P' and b.keyno = a.ixno and b.deleted = 0 group by a.ixno ,a.keyword")
            Me.ExecuteTransaction("delete from ipm_mediatype")
            Me.ExecuteTransaction("insert into ipm_mediatype(keyid,keyname,keyuse) select a.ixno keyid,a.keyword,1 keyuse from [USHMM_PHOTOARCH].dbo.keywords a,[USHMM_PHOTOARCH].dbo.keylink b where a.class = 'L' and b.keyno = a.ixno and b.deleted = 0 and a.keyword <> '' group by a.ixno,a.keyword")
            Me.ExecuteTransaction("delete from ipm_illusttype")
            Me.ExecuteTransaction("insert into ipm_illusttype(keyid,keyname,keyuse) select a.ixno tagid,a.keyword,1 taguse from [USHMM_PHOTOARCH].dbo.keywords a,[USHMM_PHOTOARCH].dbo.keylink b where a.class = 'E' and b.keyno = a.ixno and b.deleted = 0 group by a.ixno ,a.keyword")


            'PHOTO TYPE and ARTIFACT
            Me.ExecuteTransaction(GetFileContents(System.AppDomain.CurrentDomain.BaseDirectory() + "update_sql_phototypeandartifact.txt"))






        Catch ex As Exception
            Me.SendError(ex.Message, "Cannot process Attributes", "UNKNOWN")
            'MsgBox("Cannot process Attributes")
            Return False
        End Try

        Return True
        Exit Function


        ' '' '' ''Update Date
        ' '' '' ''process filelist (filter out thumbs.db)
        '' '' ''AllFiles = GetDataTable("select imdate,refno from photarch order by refno", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
        '' '' ''ProgressBar1.Maximum = AllFiles.Rows.Count
        '' '' ''ProgressBar1.Value = 0
        ' '' '' ''delete all previous values
        ' '' '' '' Me.ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21548342")
        '' '' ''For i = 0 To AllFiles.Rows.Count - 1
        '' '' ''    Try
        '' '' ''        ProgressBar1.Value = ProgressBar1.Value + 1
        '' '' ''        'insert into ipm_asset_field_value
        '' '' ''        Me.ExecuteTransaction("update ipm_asset set update_date = '" & AllFiles.Rows(i)("imdate") & "' where asset_id = " & AllFiles.Rows(i)("refno"))
        '' '' ''    Catch ex As Exception
        '' '' ''        Console.WriteLine(ex.Message)
        '' '' ''    End Try
        '' '' ''Next



    End Function
















    Private Sub Button19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button19.Click

        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Dim AllFiles As DataTable = GetDataTable("select a.refno,a.img_path,b.full_path,a.wsno from photarch a, path b where a.img_path = b.path_code", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
        Dim i As Integer
        Dim sSourceFile, sDestinationFile As String
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                ProgressBar1.Value = ProgressBar1.Value + 1
                'N:\LORES\00083\ 
                'C:\idam\PhotoArchivesLozRez\00000
                sSourceFile = "C:\idam\PhotoArchivesLozRez\" & AllFiles.Rows(i)("full_path").replace("N:\LORES\", "").trim & AllFiles.Rows(i)("wsno").trim & ".jpg"
                sDestinationFile = "C:\idam\Repository\IDAM_USHMM01\21537181\resource\screen_" & AllFiles.Rows(i)("refno") & ".jpg"
                'now copy files
                If Not IO.File.Exists(sDestinationFile) Then
                    IO.File.Copy(sSourceFile, sDestinationFile, False)
                End If

                'check to see if tiff exists .tif
            Catch ex As Exception
                'MsgBox(ex.Message)
            End Try
        Next
    End Sub


    Private Sub Button20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button20.Click
        '21548416: IDAM_USER_FULLNAME()
        '21548417: IDAM_USER_HONORIFIC()
        '21548418: IDAM_USER_ADDRESS()
        '21548420: IDAM_USER_REGION()
        '21548421: IDAM_USER_FAX()
        '21548423: IDAM_USER_SPECIFICATION()
        '21548424: IDAM_USER_POSTAL_CODE()
        '21548425: IDAM_USER_INDIVIDUAL()
        '21548426: IDAM_USER_SOURCE()
        '21548427: IDAM_USER_PHOTOGRAPHER()
        '21548428: IDAM_USER_PROVENANCE()
        '21548429: IDAM_USER_COPYRIGHT()
        '21548430: IDAM_USER_PHOTO_PROCESSED()
        '21548431: IDAM_USER_PHOTO_UNPROCESSED()
        '21548432: IDAM_USER_PHOTOS_INACTIVE()
        '21548433: IDAM_USER_CHECKMY()
        '21548434: IDAM_USER_PHOTO_CREDIT()
        '21548419: IDAM_USER_COUNTRY()
        '21548422: IDAM_USER_CONTACT()
        'worksheet
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Me.ExecuteTransaction("delete  from ipm_user where oid is not null and oid <> 0")
        Dim AllNames As DataTable = GetDataTable("select a.*, isnull(firstname,'-') firstnameoverride from fornames a", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
        Dim i As Integer
        'create users
        Dim firstnameoverride As String
        For Each row As DataRow In AllNames.Rows
            firstnameoverride = ""
            firstnameoverride = row("firstnameoverride").ToString.Trim
            If firstnameoverride = "" Then
                firstnameoverride = "-"
            End If
            Me.ExecuteTransaction("insert into ipm_user (userid,oid, firstname, lastname,contact,securitylevel_id,email) values ('" & row("nameix") & "','" & row("nameix") & "','" & firstnameoverride.Trim.Replace("'", "''") & "','" & row("mainname").ToString.Trim.Replace("'", "''") & "',1,2,'" & row("email").ToString.Trim.Replace("'", "''") & "')")
        Next
        'IDAM_USER_FULLNAME()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548416")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548416 item_id,cast(fullname as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_HONORIFIC()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548417")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548417 item_id,cast(honorific as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_ADDRESS2()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548418")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548418 item_id,cast(address2 as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_REGION()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548420")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548420 item_id,cast(region as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_FAX()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548421")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548421 item_id,cast(fax as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_SPECIFICATION()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548423")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548423 item_id,cast(agencyspec as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_POSTAL_CODE()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548424")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548424 item_id,cast(postal_cd as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_INDIVIDUAL()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548425")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548425 item_id,cast(individual as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_Source()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548426")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548426 item_id,cast(source as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_PHOTOGRAPHER()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548427")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548427 item_id,cast(photograph as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_PROVENANCE()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548428")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548428 item_id,cast(provenance as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_COPYRIGHT()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548429")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548429 item_id,cast(rtrim(b.copyright) as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames a, [USHMM_PHOTOARCH].[dbo].copyrite b  where a.copyright = b.cr_code")
        'IDAM_USER_PHOTO_PROCESSED()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548430")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548430 item_id,cast(neg_proc as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_PHOTO_UNPROCESSED()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548431")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548431 item_id,cast(neg_unpr as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_PHOTOS_INACTIVE()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548432")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548432 item_id,cast(neg_inac as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_CHECKMY()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548433")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548433 item_id,cast(checkmy as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_PHOTO_CREDIT()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548434")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548434 item_id,cast(prtcredit as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_COUNTRY()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548419")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548419 item_id,cast(country as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
        'IDAM_USER_CONTACT()
        Me.ExecuteTransaction("delete from ipm_user_field_value where item_id = 21548422")
        Me.ExecuteTransaction("insert into ipm_user_field_value select nameix user_id,21548422 item_id,cast(contact as nvarchar(max)) item_value from [USHMM_PHOTOARCH].[dbo].fornames")
    End Sub

    Private Sub AddCarouoselOrderItems()
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Me.ExecuteTransaction("delete from ipm_carrousel where carrousel_id in (select orderid from ipm_order)")
        Me.ExecuteTransaction("insert into ipm_carrousel select invoice carrousel_id, case rtrim(cast(descript as varchar(1000))) when '' then invoice else substring(rtrim(invoice) + ' - ' + rtrim(cast(descript as varchar(1000))),0,149)  end as name, ltrim(cast(notes as varchar(8000)) + cast(descript as varchar(8000))) description, 1 shared, 0 list_order,initaldate created_date,'Y' available,1 active, nameid created_by,0 default_carrousel,1 collaborative, '' password   from [USHMM_PHOTOARCH].[dbo].usage")

    End Sub


    Private Sub Button21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button21.Click
        AddCarouoselOrderItems()
    End Sub

    Private Sub Button22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button22.Click
        '21548416: IDAM_USER_FULLNAME()
        '21548417: IDAM_USER_HONORIFIC()
        '21548418: IDAM_USER_ADDRESS()
        '21548420: IDAM_USER_REGION()
        '21548421: IDAM_USER_FAX()
        '21548423: IDAM_USER_SPECIFICATION()
        '21548424: IDAM_USER_POSTAL_CODE()
        '21548425: IDAM_USER_INDIVIDUAL()
        '21548426: IDAM_USER_SOURCE()
        '21548427: IDAM_USER_PHOTOGRAPHER()
        '21548428: IDAM_USER_PROVENANCE()
        '21548429: IDAM_USER_COPYRIGHT()
        '21548430: IDAM_USER_PHOTO_PROCESSED()
        '21548431: IDAM_USER_PHOTO_UNPROCESSED()
        '21548432: IDAM_USER_PHOTOS_INACTIVE()
        '21548433: IDAM_USER_CHECKMY()
        '21548434: IDAM_USER_PHOTO_CREDIT()
        '21548419: IDAM_USER_COUNTRY()
        '21548422: IDAM_USER_CONTACT()
        'worksheet
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Me.ExecuteTransaction("delete from ipm_user where userid in (select nameid from [USHMM_PHOTOARCH].[dbo].institut)")
        Dim AllNames As DataTable = GetDataTable("select * from institut", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
        Dim i As Integer
        'create users
        Dim firstnameoverride, lastnameoverride As String
        For Each row As DataRow In AllNames.Rows
            firstnameoverride = ""
            firstnameoverride = row("firstname").ToString.Trim
            If firstnameoverride = "" Then
                firstnameoverride = "-"
            End If

            lastnameoverride = ""
            lastnameoverride = row("lastname").ToString.Trim
            If lastnameoverride = "" Then
                lastnameoverride = "orgname"
            End If
            Me.ExecuteTransaction("insert into ipm_user (userid,oid, firstname, lastname,contact,securitylevel_id,email,agency) values ('" & row("nameid") & "','" & row("nameid") & "','" & firstnameoverride.Trim.Replace("'", "''") & "','" & lastnameoverride.Trim.Replace("'", "''") & "',0,2,'" & row("email").ToString.Trim.Replace("'", "''") & "','" & row("orgname").ToString.Trim.Replace("'", "''") & "')")
        Next

    End Sub
    Private Sub addOrders()
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Me.ExecuteTransaction("delete from ipm_order")
        Dim AllNames As DataTable = GetDataTable("select * from usage", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
        Dim i As Integer
        'create users
        Dim firstnameoverride, lastnameoverride As String
        For Each row As DataRow In AllNames.Rows
            firstnameoverride = ""
            firstnameoverride = row("firstname").ToString.Trim
            If firstnameoverride = "" Then
                firstnameoverride = "-"
            End If

            lastnameoverride = ""
            lastnameoverride = row("lastname").ToString.Trim
            If lastnameoverride = "" Then
                lastnameoverride = "orgname"
            End If
            Dim sql As String
            sql = "insert into ipm_order (OrderID,OrderDate,UserID,carrousel_id,status, firstname, lastname, agency, maddress1, maddress2, mcity, mstate, mzip,mcountry, baddress1, baddress2, bcity, bstate, bzip,bcountry,  bphone1, bphone2,fax,email)  values ('" & row("invoice") & "','" & row("invdate") & "','" & row("nameid") & "','" & row("invoice") & "',3,'" & _
    firstnameoverride.Trim.Replace("'", "''") & "','" & _
lastnameoverride.Trim.Replace("'", "''") & "','" & _
row("orgname").Trim.Replace("'", "''") & "','" & _
row("maddress1").Trim.Replace("'", "''") & "','" & _
row("maddress2").Trim.Replace("'", "''") & "','" & _
row("mcity").Trim.Replace("'", "''") & "','" & _
row("mstate").Trim.Replace("'", "''") & "','" & _
row("mzip").Trim.Replace("'", "''") & "','" & _
row("mcountry").Trim.Replace("'", "''") & "','" & _
row("baddress1").Trim.Replace("'", "''") & "','" & _
row("baddress2").Trim.Replace("'", "''") & "','" & _
row("bcity").Trim.Replace("'", "''") & "','" & _
row("bstate").Trim.Replace("'", "''") & "','" & _
row("bzip").Trim.Replace("'", "''") & "','" & _
row("bcountry").Trim.Replace("'", "''") & "','" & _
row("phone1").Trim.Replace("'", "''") & "','" & _
row("phone2").Trim.Replace("'", "''") & "','" & _
row("fax").Trim.Replace("'", "''") & "','" & _
row("email").Trim.Replace("'", "''") & "')"
            Me.ExecuteTransaction(sql)

        Next
    End Sub


    Private Sub Button23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button23.Click
        addOrders()
    End Sub

    Private Sub AddOrderDetails()
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Me.ExecuteTransaction("delete from ipm_order_details")
        Dim AllNamestmp As DataTable
        Dim AllNames As DataTable = GetDataTable("select * from uselink where invoice <> ''", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
        Dim i As Integer
        'create users
        Me.ExecuteTransaction("insert into ipm_order_details select cast(invoice as decimal) OrderID,cast(refno as decimal) asset_id,usefee price,repofee shippingcost,addfee AdditionalCost,0 discount,cast(qty as decimal) Quantity, 10 filetype,b.download_id download_id,3 status,refno itemname,'' notes,primesrcex userid,source sourcename, copyright copyright  from [USHMM_PHOTOARCH].[dbo].uselink a, ipm_downloadtype b where rtrim(a.descript) + ' ' + rtrim(size) = b.name and invoice <> ''")
        Dim firstnameoverride, lastnameoverride As String


    End Sub

    Private Sub Button24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button24.Click
        AddOrderDetails()

    End Sub

    Private Sub Button25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button25.Click
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Me.ExecuteTransaction("delete from ipm_downloadtype where active = 0")
        Dim AllNames As DataTable = GetDataTable("select rtrim(descript) + ' ' +rtrim(size) name from uselink group by rtrim(descript) + ' ' +rtrim(size) order by rtrim(descript) + ' ' +rtrim(size)", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
        Dim i As Integer = 1
        'create users
        Dim firstnameoverride, lastnameoverride As String
        For Each row As DataRow In AllNames.Rows

            Me.ExecuteTransaction("insert into ipm_downloadtype (Download_ID,Format,Width,Height,crop,name,active,dpi,bits,quality) values (" & i & ",10,0,0,0,'" & row("name") & "',0,0,2,2)")
            i += 1
        Next

    End Sub

    Private Sub Button26_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button26.Click
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Dim AllNames As DataTable = GetDataTable("select rtrim(descript) + ' ' +rtrim(size) name from uselink group by rtrim(descript) + ' ' +rtrim(size) order by rtrim(descript) + ' ' +rtrim(size)", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
        Dim i As Integer = 1
        'create users
        Dim firstnameoverride, lastnameoverride As String
        For Each row As DataRow In AllNames.Rows

            Me.ExecuteTransaction("insert into ipm_downloadtype (Download_ID,Format,Width,Height,crop,name,active,dpi,bits,quality) values (" & i & ",10,0,0,0,'" & row("name") & "',0,0,2,2)")
            i += 1
        Next
    End Sub








    Private Sub Button27_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button27.Click




        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Dim AllNames As DataTable = GetDataTable("select * from photarch a, desi b where a.designat = b.code", Me.ConnectionStringUSHMMPHOTOARCHIVE)  'and oid = '041018.00'
        Dim i As Integer = 1
        Dim x As DataRow() = AllNames.Select("wsno='64407'")
        'create users
        Dim firstnameoverride, lastnameoverride As String
        For Each row As DataRow In x

            'Me.ExecuteTransaction("insert into ipm_downloadtype (Download_ID,Format,Width,Height,crop,name,active,dpi,bits,quality) values (" & i & ",10,0,0,0,'" & row("name") & "',0,0,2,2)")
            i += 1
        Next
    End Sub

    Private Sub Button28_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button28.Click
        'get category id to populate
        NightlySynch()

    End Sub

    Private Sub PullFromFoxPro()
        If REFRESHFROMSOURCE.ToLower = "true" Then
            Try
                'populate temp tables
                For Each table As String In IDAMUSHMMTables.Split(",")
                    executebulktablecopy(table)
                Next

            Catch ex As Exception
                'MsgBox("Error")
                Me.SendError(ex.Message, "BULK COPY CATCH", "UNKNOWN")
                'MsgBox("bulk copy failed")
                Exit Sub
            End Try
        End If
    End Sub


    Public Function NightlySynch() As Boolean

        Me.SendError("Nightly Started", "Nightly", "UNKNOWN")

        PullFromFoxPro()


        Dim catIDUploadLocation As String = GetDataTable("select category_id from ipm_category where name = 'Photo Archives' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")
        Dim imTypeid As Integer = 5000
        Dim AllFiles As DataTable = GetDataTable("select * from [USHMM_PHOTOARCH].[dbo].photarch a, [USHMM_PHOTOARCH].[dbo].desi b where a.designat = b.code and refno not in (select asset_id refno from ipm_asset)", Me.ConnectionStringIDAM)  'and oid = '041018.00'

        Dim i As Integer
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                ProgressBar1.Value = ProgressBar1.Value + 1
                If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("refno"), ConnectionStringIDAM).Rows.Count = 0 Or Me.chkUpdateOnly.Checked Then
                    Dim sAlliance As String = ""
                    Dim sAllianceFolder, sVisionProjectNumber As String
                    Dim sAllianceCatID, sProjectID As String
                    Dim sDirectoryRoot As String = "V:\"
                    Dim sProjectName As String = "Photo Archives Assets"
                    ''''''''ListView1.Items.Add(sProjectName + ": " + AllFiles.Rows(i)("hier_desi") + ":" + AllFiles.Rows(i)("refno").ToString)

                    '''''''ListView1.Refresh()
                    '''''''Me.Refresh()

                    'get vision project number
                    sVisionProjectNumber = ""

                    'get Alliance
                    sAlliance = ""

                    'create project if needed
                    sProjectID = "21537181"
                    '''''''If GetDataTable("select projectid from ipm_project where name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                    '''''''    'get cat id
                    '''''''    sProjectID = GetDataTable("select projectid from ipm_project where name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("projectid")
                    '''''''Else
                    '''''''    sProjectID = CreateNewProject(sProjectName.Trim, 1, 1)
                    '''''''End If

                    'create project folders if needed
                    Dim sProjectFolderArray() As String
                    Dim sProjectfolderID As String = "0"
                    Try
                        If AllFiles.Rows(i)("hier_desi").trim <> "" Then


                            sProjectfolderID = CreateCheckAddProjectFolder(AllFiles.Rows(i)("hier_desi").trim)



                            '' '' ''sProjectFolderArray = AllFiles.Rows(i)("hier_desi").trim.replace("--", "|").split("|")
                            ' '' '' ''assume value
                            '' '' ''Dim ii As Integer
                            '' '' ''For ii = 0 To sProjectFolderArray.Length - 1
                            '' '' ''    If ii = 0 Then 'item at root

                            '' '' ''        sProjectfolderID = checkorCreateProjectfolder("0", sProjectFolderArray(ii).Trim, sProjectID)
                            '' '' ''    Else
                            '' '' ''        If sProjectFolderArray(ii).Trim <> "" Then
                            '' '' ''            sProjectfolderID = checkorCreateProjectfolder(GetDataTable("select category_id from ipm_asset_category where projectid = " & sProjectID & " and name = '" & sProjectFolderArray(ii - 1).Trim.Replace("'", "''") & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id"), sProjectFolderArray(ii).Trim, sProjectID)
                            '' '' ''        Else
                            '' '' ''            'sProjectfolderID = "0"
                            '' '' ''        End If

                            '' '' ''    End If
                            '' '' ''Next ii
                        Else
                            sProjectfolderID = "0"
                        End If
                    Catch ex As Exception
                        'put in general
                        sProjectfolderID = "0"
                    End Try


                    'get media_type
                    'get extension
                    Dim sExtensionid As String
                    Dim sExtensiontmp
                    Dim sExtension As String = AllFiles.Rows(i)("img_format")

                    'check to see if in filetype_lookup
                    imTypeid = imTypeid + 1
                    Try
                        sExtensionid = GetDataTable("select * from ipm_filetype_lookup where extension = '" & sExtension & "'", Me.ConnectionStringIDAM).Rows(0)("media_type")
                    Catch ex As Exception

                        'add extension
                        Me.ExecuteTransaction("insert into ipm_filetype_lookup (media_type,name,extension,useicon) values (" & imTypeid & ",'" & sExtension & " File" & "','" & sExtension & "',1)")
                        sExtensionid = imTypeid
                    End Try


                    'add asset
                    Dim sAssetID As String
                    'sAssetID = CreateAsset("0","1",sAssetName,"1",sProjectID

                    Dim sAssetName As String = ""
                    If AllFiles.Rows(i)("sufwsno").ToString.Trim <> "" Then
                        sAssetName = AllFiles.Rows(i)("wsno").ToString.Trim & "-" & AllFiles.Rows(i)("sufwsno").ToString.Trim
                    Else
                        sAssetName = AllFiles.Rows(i)("wsno").ToString.Trim
                    End If


                    'check to see if asset already exists and use update
                    If chkUpdateOnly.Checked Then
                        'update location_id,projectid,category_id,
                        If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("idrefno") & " and projectid = " & sProjectID & " and category_id = " & sProjectfolderID, ConnectionStringIDAM).Rows.Count = 0 Then
                            'assume asset is already there just not in the right spot
                            Me.ExecuteTransaction("update ipm_asset set name = '" & sAssetName & "',projectid = " & sProjectID & ",category_id = " & sProjectfolderID & ",update_date = '" & AllFiles.Rows(i)("lmdate") & "', upload_date = '" & AllFiles.Rows(i)("date1") & "' where asset_id = " & AllFiles.Rows(i)("refno"))
                        End If
                    Else
                        Me.ExecuteTransaction("insert into ipm_asset(description,filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date,oid) values ('" & Replace(AllFiles.Rows(i)("subject"), "'", "''") & "','" & Replace(AllFiles.Rows(i)("refno"), ".jpg'", "''") & "'," & sProjectID & ",'1'," & AllFiles.Rows(i)("refno") & ",1,1,'" & sAssetName & "'," & sExtensionid & "," & sProjectID & "," & sProjectfolderID & ",'" & AllFiles.Rows(i)("date1") & "','" & AllFiles.Rows(i)("lmdate") & "'," & AllFiles.Rows(i)("refno").ToString.Trim & ")")
                    End If

                    Try
                        'add file to repository
                        If Not GetAssetHiRezFileWithCheck(AllFiles.Rows(i)("refno").ToString.Trim, AllFiles.Rows(i)("img_format"), sProjectID, AllFiles.Rows(i)("img_num"), AllFiles.Rows(i)("cd_number"), False) Then
                            If GetAssetHiRezFileWithCheck(AllFiles.Rows(i)("refno").ToString.Trim, AllFiles.Rows(i)("img_format"), sProjectID, AllFiles.Rows(i)("img_num"), AllFiles.Rows(i)("cd_number"), True) Then
                                Me.SendError("", "Could not find file" & AllFiles.Rows(i)("img_num") & " - " & AllFiles.Rows(i)("cd_number"), "")
                            End If
                        End If
                        '''GetAssetHiRezFile(AllFiles.Rows(i)("refno").ToString.Trim)
                        'send to queue
                        Me.ExecuteTransaction("insert into ipm_asset_queue(instance_id,asset_id,asset_handler,status,priority,engine_id)values('IDAM_USHMM'," & AllFiles.Rows(i)("refno").ToString.Trim & ",2,0,1,'48')")
                    Catch ex As Exception
                        Me.SendError(ex.Message, "FILE CATCH", "UNKNOWN")
                    End Try


                End If

            Catch ex As Exception
                Me.SendError(ex.Message, "MAIN CATCH", "UNKNOWN")
                Exit Function
            End Try
        Next i
        If AllFiles.Rows.Count > 0 Then
            'reprocess all attributes
            Me.SendError("Processesing Attributes", "Nightly", "UNKNOWN")
            processMainAttributes()
            If REFRESHINDEXES.ToLower = "true" Then
                Me.SendError("Processesing Indexes", "Nightly", "UNKNOWN")
                'update index
                Me.ExecuteTransaction(GetFileContents(System.AppDomain.CurrentDomain.BaseDirectory() + "update_sql.txt"))
                If REBUILDINDEXES.ToLower = "true" Then
                    'rebuild index
                    Me.ExecuteTransaction(GetFileContents(System.AppDomain.CurrentDomain.BaseDirectory() + "indexrebuild_sql.txt"))
                End If

            End If
            'update stats
            Me.ExecuteTransaction("exec sp_updatestats")
            Me.SendError("Nightly Completed", "Nightly", "UNKNOWN")
        End If
    End Function


    Public Function GetFileContents(ByVal FullPath As String, _
       Optional ByRef ErrInfo As String = "") As String

        Dim strContents As String
        Dim objReader As StreamReader
        Try

            objReader = New StreamReader(FullPath)
            strContents = objReader.ReadToEnd()
            objReader.Close()
            Return strContents
        Catch Ex As Exception
            ErrInfo = Ex.Message
        End Try
    End Function




    Private Function executebulktablecopy(ByVal tablename As String) As String
        Dim connectionString As String = Me.ConnectionStringUSHMMPHOTOARCHIVESOURCE
        ' Open a connection to the AdventureWorks database.
        Dim AllFiles As DataTable = GetDataTable("select * from " & tablename, connectionString)
        Using destinationConnection As SqlConnection = New SqlConnection(Me.ConnectionStringUSHMMPHOTOARCHIVESQLVERSION)

            destinationConnection.Open()
            Dim droptablecmd As SqlCommand = New SqlCommand("delete from " & tablename, destinationConnection)
            droptablecmd.ExecuteNonQuery()
            Using bulkCopy As SqlBulkCopy = New SqlBulkCopy(destinationConnection)
                bulkCopy.DestinationTableName = tablename
                bulkCopy.BulkCopyTimeout = 4000
                Try
                    ' Write from the source to the destination.

                    bulkCopy.WriteToServer(AllFiles)

                Catch ex As Exception
                    Me.SendError(ex.Message, "BULK COPY CATCH", "UNKNOWN")
                Finally
                    ' Close the SqlDataReader. The SqlBulkCopy
                    ' object is automatically closed at the end
                    ' of the Using block.
                    destinationConnection.Close()
                End Try
            End Using

        End Using

    End Function



    Private Function executebulktablecopywithdrop(ByVal tablename As String) As String
        Dim connectionString As String = Me.ConnectionStringUSHMMPHOTOARCHIVESOURCE
        ' Open a connection to the AdventureWorks database.
        Dim AllFiles As DataTable = GetDataTable("select * from " & tablename, connectionString)
        Using destinationConnection As SqlConnection = New SqlConnection(Me.ConnectionStringUSHMMPHOTOARCHIVESQLVERSION)

            destinationConnection.Open()
            Dim droptablecmd As SqlCommand = New SqlCommand("drop table " & tablename, destinationConnection)
            droptablecmd.ExecuteNonQuery()
            Using bulkCopy As SqlBulkCopy = New SqlBulkCopy(destinationConnection)
                bulkCopy.DestinationTableName = tablename

                bulkCopy.BulkCopyTimeout = 4000
                Try
                    ' Write from the source to the destination.

                    bulkCopy.WriteToServer(AllFiles)

                Catch ex As Exception
                    Me.SendError(ex.Message, "BULK COPY CATCH", "UNKNOWN")
                Finally
                    ' Close the SqlDataReader. The SqlBulkCopy
                    ' object is automatically closed at the end
                    ' of the Using block.
                    destinationConnection.Close()
                End Try
            End Using

        End Using

    End Function


    Function ReAquireAllAssetHiRezFile() As Boolean
        Dim i As Integer
        Dim reporttxt As String = ""
        Dim AllFiles As DataTable = GetDataTable("select a.asset_id,location_id,img_num,img_format,img_path,cd_number from ipm_asset a, [USHMM_PHOTOARCH].[dbo].photarch b where b.refno = a.oid and img_num <> '' and a.oid <> '' and cd_number <> '' ", ConnectionStringIDAM)
        For i = 0 To AllFiles.Rows.Count - 1
            If GetAssetHiRezFile(AllFiles.Rows(i)("asset_id").ToString.Trim) Then
                Me.ExecuteTransaction("update ipm_asset_queue set status = 0 where asset_id = " & AllFiles.Rows(i)("asset_id").ToString.Trim & " and asset_handler = 2")
                reporttxt += "Successfully reaquired asset: " & AllFiles.Rows(i)("asset_id").ToString.Trim & vbCrLf
            End If

        Next
        SendError(reporttxt)
    End Function

    Function ReAquireFailedAssetHiRezFile() As Boolean
        Dim i As Integer
        Dim reporttxt As String = ""
        Dim AllFiles As DataTable = GetDataTable("select id,a.asset_id,location_id,img_num,img_format,img_path,cd_number from ipm_asset a, [USHMM_PHOTOARCH].[dbo].photarch b,ipm_asset_queue c where b.refno = a.oid and img_num <> '' and a.oid <> '' and cd_number <> '' and a.asset_id = c.asset_id and c.status = 2", ConnectionStringIDAM)
        For i = 0 To AllFiles.Rows.Count - 1
            If GetAssetHiRezFile(AllFiles.Rows(i)("asset_id").ToString.Trim) Then
                Me.ExecuteTransaction("update ipm_asset_queue set status = 0 where id = " & AllFiles.Rows(i)("id").ToString.Trim & " and asset_handler = 2")
                reporttxt += "Successfully reaquired asset: " & AllFiles.Rows(i)("asset_id").ToString.Trim & vbCrLf
            End If

        Next
        SendError(reporttxt)
    End Function


    Function GetAssetHiRezFile(ByVal refno As String) As Boolean
        'F:\PAHIRESTEMPLOCATION\HiRes
        'E:\IDAM\Repositories\IDAM_USHMM
        Dim sSourceRoot As String = Me.USHMMHIREZROOT
        Dim sDestRoot As String = Me.IDAMROOT


        Dim AllFiles As DataTable = GetDataTable("select asset_id,location_id,img_num,img_format,img_path,cd_number from ipm_asset a, [USHMM_PHOTOARCH].[dbo].photarch b where a.asset_id = " & refno & " and b.refno = a.oid and img_num <> '' and a.oid <> '' and cd_number <> ''", ConnectionStringIDAM)
        Dim i As Integer
        Dim sSourceFile As String
        Dim sDestinationFile As String
        Dim sTifSwitch As Boolean = False

        For i = 0 To AllFiles.Rows.Count - 1
            Try

                'get SourceFile
                sSourceFile = ""
                sDestinationFile = ""
                sSourceFile = sSourceRoot & "0" & AllFiles.Rows(i)("cd_number").ToString.Trim + "\" + AllFiles.Rows(i)("img_num").ToString.Trim + "." + AllFiles.Rows(i)("img_format").ToString.Trim
                sDestinationFile = sDestRoot + AllFiles.Rows(i)("location_id").ToString.Trim + "\resource\" + AllFiles.Rows(i)("asset_id").ToString.Trim + "." + AllFiles.Rows(i)("img_format").ToString.Trim

                If Not IO.File.Exists(sDestinationFile) Then


                    If IO.File.Exists(sSourceFile) Then
                        'jpg
                        IO.File.Copy(sSourceFile, sDestinationFile, False)
                    Else
                        'try tif override
                        If Not IO.File.Exists(sDestinationFile.Replace(AllFiles.Rows(i)("img_format").ToString.Trim, "TIF")) Then
                            IO.File.Copy(sSourceFile.Replace(AllFiles.Rows(i)("img_format").ToString.Trim, "TIF"), sDestinationFile.Replace(AllFiles.Rows(i)("img_format").ToString.Trim, "TIF"), False)
                            'update extension
                            Me.ExecuteTransaction("update ipm_asset set media_type = 3,filename='" & refno & ".tif' where asset_id = " & refno)
                            sendtoqueue(refno)
                        End If
                    End If
                End If
                Return True

            Catch ex As Exception
                'check if tif override

                Return False

                '' ''ListView1.Items.Add("Error: " & sSourceFile)
                '' ''Me.Refresh()
            End Try

            Me.Refresh()
        Next
    End Function











    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ReAquireFailedAssetHiRezFile()


        NightlySynch()
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub Button29_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button29.Click
        Me.processMainAttributes()
    End Sub

    Private Sub Button30_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        executebulktablecopy("webhere")
    End Sub

    Private Sub Button31_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button31.Click
        PullFromFoxPro()
    End Sub

    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click

    End Sub

    Private Sub Button30_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button30.Click
        PullFromFoxPro()
        AddCarouoselOrderItems()
        addOrders()
        AddOrderDetails()
    End Sub

    Private Sub Button32_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button32.Click
        'GetAssetHiRezFileWithCheck
        Dim reporttxt As String
        'select a.asset_id,location_id,img_num,img_path,cd_number from ipm_asset a, [USHMM_PHOTOARCH].[dbo].photarch b where b.refno = a.oid and img_num <> '' and a.oid <> '' and cd_number <> '' 
        Dim AllFiles As DataTable = GetDataTable("select c.extension, a.asset_id,location_id,img_num,img_format,img_path,cd_number from ipm_asset a, ipm_filetype_lookup c ,[USHMM_PHOTOARCH].[dbo].photarch b where a.media_type = c.media_type and b.refno = a.oid and img_num <> '' and a.oid <> '' and cd_number <> '' ", ConnectionStringIDAM)
        For Each row As DataRow In AllFiles.Rows
            If Not GetAssetHiRezFileWithCheck(row("asset_id"), row("extension"), row("location_id"), row("img_num"), row("cd_number"), False) Then
                If GetAssetHiRezFileWithCheck(row("asset_id"), row("extension"), row("location_id"), row("img_num"), row("cd_number"), True) Then
                    reporttxt += row("asset_id").ToString.Trim & " " & "lowres" & vbCrLf
                    Log(row("asset_id").ToString.Trim & ", " & "Found lowres")
                Else
                    reporttxt += row("asset_id").ToString.Trim & vbCrLf
                    Log(row("asset_id").ToString.Trim & ", " & "Not found")
                End If
            End If
            Log(row("asset_id").ToString.Trim & ", " & "Found")
        Next
        SendError(reporttxt)
    End Sub

    Function GetAssetHiRezFileWithCheck(ByVal asset_id As String, ByVal extension As String, ByVal location_id As String, ByVal wsno As String, ByVal cd_number As String, ByVal lowrezoverride As Boolean) As Boolean
        'F:\PAHIRESTEMPLOCATION\HiRes
        'E:\IDAM\Repositories\IDAM_USHMM
        Dim sSourceRoot As String = ""
        If lowrezoverride Then
            sSourceRoot = Me.USHMMHIREZROOT.ToString.ToLower.Replace("hires", "lores")
        Else
            sSourceRoot = Me.USHMMHIREZROOT
        End If
        Dim sDestRoot As String = Me.IDAMROOT
        Dim i As Integer
        Dim sSourceFile As String
        Dim sDestinationFile As String
        Dim sTifSwitch As Boolean = False
        Try
            'check to see if in repo already
            'use asset image format.
            If extension.Trim = "" Then
                sDestinationFile = sDestRoot + location_id.Trim + "\resource\" + asset_id.Trim
            Else
                sDestinationFile = sDestRoot + location_id.Trim + "\resource\" + asset_id.Trim + "." & extension.Trim
            End If

            If Not IO.File.Exists(sDestinationFile) Then
                'not there so try to aquire

                'try source jpg in hirez
                sSourceFile = sSourceRoot & "0" & cd_number.ToString.Trim + "\" + wsno.ToString.Trim + ".JPG"

                If Not IO.File.Exists(sSourceFile) Then
                    'try tiff counterpart
                    sSourceFile = sSourceRoot & "0" & cd_number.ToString.Trim + "\" + wsno.ToString.Trim + ".TIF"
                    If Not IO.File.Exists(sSourceFile) Then
                        'record error
                        Return False
                    Else
                        'found TIF
                        IO.File.Copy(sSourceFile, sDestinationFile.ToLower.Replace("jpg", "tif"), False)
                        Log(asset_id & ",copied tif override " & "," & lowrezoverride)
                        'use mediatype 3
                        'update extension
                        Me.ExecuteTransaction("update ipm_asset set media_type = 3,filename='" & wsno.Trim & ".tif' where asset_id = " & asset_id)
                        sendtoqueue(asset_id)
                        Return True
                    End If
                Else
                    'found JPG to use
                    IO.File.Copy(sSourceFile, sDestinationFile, False)
                    Log(asset_id & ",copied jpg " & "," & lowrezoverride)
                    'update extension
                    Me.ExecuteTransaction("update ipm_asset set media_type = 10,filename='" & wsno & ".jpg' where asset_id = " & asset_id)
                    sendtoqueue(asset_id)
                    Return True
                End If
            End If
            Return True
        Catch ex As Exception
            'check if tif override
            Return False
        End Try
    End Function

    Private Function sendtoqueue(ByVal asset_id As String)
        'check to see if already in queue
        If GetDataTable("select * from ipm_asset_queue where asset_id = " & asset_id & " and asset_handler = 2 and status = 0", ConnectionStringIDAM).Rows.Count = 0 Then
            Me.ExecuteTransaction("insert into ipm_asset_queue select 'IDAM_USHMM' instance_id,'" & asset_id & "' asset_id,2 asset_handler,null src,0 status, 1 priority ,0 attempts ,48 engine_id,getdate() date_placed, getdate() date_processed, getdate() date_complete")
            Log(asset_id & ",added to queue")
        End If

    End Function

    Private Sub Button33_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button33.Click
        Log("test")
        Log("test2")
        Log("test3")
    End Sub

    Public Sub Log(ByVal msg As String)
        If Not System.IO.Directory.Exists(Application.StartupPath) Then
            System.IO.Directory.CreateDirectory(Application.StartupPath)
        End If

        'check the file
        Dim fs As FileStream = New FileStream(Application.StartupPath & "\log.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite)
        Dim s As StreamWriter = New StreamWriter(fs)
        s.Close()
        fs.Close()

        'log it
        Dim fs1 As FileStream = New FileStream(Application.StartupPath & "\log.txt", FileMode.Append, FileAccess.Write)
        Dim s1 As StreamWriter = New StreamWriter(fs1)
        s1.Write(msg & vbCrLf)
        s1.Close()
        fs1.Close()
    End Sub



    Private Sub Button34_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button34.Click
        Me.executebulktablecopywithdrop("usage")

    End Sub






    Private Function CreateCheckAddProjectFolder(ByVal _Designation As String) As String
        'contruct sql
        Dim foldernames As String() = Regex.Split(_Designation, "--")
        'ProgressBar1.Value = ProgressBar1.Value + 1
        Dim sql As String = ""
        Dim sqlcap As String = ""
        Dim sqlinit As String = "select category_id from IPM_ASSET_CATEGORY where available = 'Y' and NAME = '" & foldernames(foldernames.Length - 1).Trim.Replace("'", "''") & "'"
        Dim i As Integer = 1
        For Each folder As String In foldernames
            If i = foldernames.Length Then Exit For
            sql = " and PARENT_CAT_ID in (select category_id from IPM_ASSET_CATEGORY where available = 'Y' and NAME = '" & folder.Trim.Replace("'", "''") & "' " & sql
            i += 1
        Next
        For x As Integer = 1 To i
            If x = foldernames.Length Then Exit For
            sqlcap = sqlcap & ")"
        Next
        sql = sqlinit & sql & sqlcap
        Dim isExistCat As DataTable = GetDataTable(sql, ConnectionStringIDAM)
        If isExistCat.Rows.Count = 0 Then
            'needs realignment
            '''''''''Dim path As String = getIDAMPath(asset("category_id").ToString.Trim, asset("name").ToString.Trim)

            'create project folders if needed
            Dim sProjectFolderArray() As String
            Dim sProjectfolderID As String = "0"
            Try
                If foldernames.Length > 0 Then
                    sProjectFolderArray = foldernames
                    'assume value
                    Dim ii As Integer
                    For ii = 0 To sProjectFolderArray.Length - 1
                        If ii = 0 Then 'item at root

                            sProjectfolderID = checkorCreateProjectfolder("0", sProjectFolderArray(ii).Trim, 21537181)
                        Else
                            If sProjectFolderArray(ii).Trim <> "" Then
                                sProjectfolderID = checkorCreateProjectfolder(sProjectfolderID, sProjectFolderArray(ii).Trim, 21537181)
                            Else
                                'sProjectfolderID = "0"
                            End If

                        End If
                    Next ii
                Else
                    sProjectfolderID = "0"
                End If
            Catch ex As Exception
                'put in general
                sProjectfolderID = "0"
            End Try

            If sProjectfolderID <> "0" Then
                'reassign the asset to the new category
                Try

                    Return sProjectfolderID
                Catch ex As Exception
                    Log(_Designation & " - ERROR")
                    Return 0
                End Try

            End If
            'TextBoxbaddesignation.Text = TextBoxbaddesignation.Text & asset("asset_id") & "," & asset("name") & "," & asset("item_value") & "," & path & vbCrLf
        Else
            Return isExistCat.Rows(0)("category_id")
        End If
    End Function


    Private Sub Button35_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button35.Click
        Dim AllFiles As DataTable = GetDataTable("select a.Asset_ID,c.CATEGORY_ID,c.name,b.item_value from IPM_ASSET a,IPM_ASSET_FIELD_VALUE b,IPM_ASSET_CATEGORY c where a.Asset_ID = b.ASSET_ID and b.Item_ID = 21548375 and a.Category_ID = c.CATEGORY_ID and a.available = 'Y' and a.projectid = 21537181  ", ConnectionStringIDAM)
        ProgressBar1.Value = 1
        ProgressBar1.Maximum = AllFiles.Rows.Count
        For Each asset As DataRow In AllFiles.Rows
            Dim catid As String = CreateCheckAddProjectFolder(asset("item_value").trim)
            If catid <> "0" Then
                ExecuteTransaction("update ipm_asset set category_id = " & catid & " where asset_id = " & asset("asset_id"))
            End If
            ProgressBar1.Value += 1
        Next
    End Sub



    Private Function getIDAMPath(ByVal category_id As String, ByRef path As String, Optional ByRef exitcall As Boolean = False) As String
        If category_id = "0" Then Return path
        Dim dt As DataTable = GetDataTable("select category_id, name from ipm_asset_category where category_id = (select parent_cat_id from ipm_asset_category where category_id = " & category_id & ")", ConnectionStringIDAM)
        If dt.Rows(0)("category_id") = "0" Then Return path
        path = dt.Rows(0)("name") & " -- " & path
        getIDAMPath(dt.Rows(0)("category_id"), path, exitcall)
        Return path
    End Function

    Private Sub Button36_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button36.Click
        'GetAssetHiRezFileWithCheck
        Dim reporttxt As String
        'select a.asset_id,location_id,img_num,img_path,cd_number from ipm_asset a, [USHMM_PHOTOARCH].[dbo].photarch b where b.refno = a.oid and img_num <> '' and a.oid <> '' and cd_number <> '' 
        Dim AllFiles As DataTable = GetDataTable("select c.extension, a.asset_id,location_id,img_num,img_format,img_path,cd_number from ipm_asset a, ipm_filetype_lookup c ,[USHMM_PHOTOARCH].[dbo].photarch b where a.media_type = c.media_type and b.refno = a.oid and img_num <> '' and a.oid <> '' and cd_number <> '' and filesize = 0", ConnectionStringIDAM)
        For Each row As DataRow In AllFiles.Rows
            If Not GetAssetHiRezFileWithCheck(row("asset_id"), row("extension"), row("location_id"), row("img_num"), row("cd_number"), False) Then
                If GetAssetHiRezFileWithCheck(row("asset_id"), row("extension"), row("location_id"), row("img_num"), row("cd_number"), True) Then
                    reporttxt += row("asset_id").ToString.Trim & " " & "lowres" & vbCrLf
                    Log(row("asset_id").ToString.Trim & ", " & "Found lowres")
                Else
                    reporttxt += row("asset_id").ToString.Trim & vbCrLf
                    Log(row("asset_id").ToString.Trim & ", " & "Not found")
                End If
            End If
            Log(row("asset_id").ToString.Trim & ", " & "Found")
        Next
        SendError(reporttxt)
    End Sub

    Private Sub ListView1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.SelectedIndexChanged

    End Sub
End Class
