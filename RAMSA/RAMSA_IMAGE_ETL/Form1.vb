Imports System.Data.OleDb
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions




Public Class Form1
    Inherits System.Windows.Forms.Form


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub


    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ConnectionStringIDAM As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
    Public ConnectionStringUSHMMPHOTOARCHIVE As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringUSHMMPHOTOARCHIVE")
    Public ConnectionStringUSHMMPHOTOARCHIVESOURCE As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringUSHMMPHOTOARCHIVESOURCE")
    Public IDAMUSHMMTables As String = ConfigurationSettings.AppSettings("IDAM.USHMMTables")
    Public REFRESHFROMSOURCE As String = ConfigurationSettings.AppSettings("IDAM.REFRESHFROMSOURCE")
    Public ConnectionStringUSHMMPHOTOARCHIVESQLVERSION As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringUSHMMPHOTOARCHIVESQLVERSION")
    Public IDAMROOT As String = ConfigurationSettings.AppSettings("IDAM.IDAMROOT")
    Public IDAMINSTANCE As String = ConfigurationSettings.AppSettings("IDAM.INSTANCE")
    Public IDAMENGINE As String = ConfigurationSettings.AppSettings("IDAM.ENGINE")
    Public USHMMHIREZROOT As String = ConfigurationSettings.AppSettings("IDAM.USHMMHIREZROOT")
    Public REFRESHINDEXES As String = ConfigurationSettings.AppSettings("IDAM.REFRESHINDEXES")
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents txtSuccess As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFailed As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Public REBUILDINDEXES As String = ConfigurationSettings.AppSettings("IDAM.REBUILDINDEXES")




    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.txtSuccess = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtFailed = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(46, 356)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(472, 23)
        Me.ProgressBar1.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(46, 90)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(167, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Process Project DB Images"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(351, 90)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(167, 23)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Process General DB Images"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'txtSuccess
        '
        Me.txtSuccess.Location = New System.Drawing.Point(46, 136)
        Me.txtSuccess.Multiline = True
        Me.txtSuccess.Name = "txtSuccess"
        Me.txtSuccess.ReadOnly = True
        Me.txtSuccess.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSuccess.Size = New System.Drawing.Size(218, 183)
        Me.txtSuccess.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(46, 117)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Results Successful"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(300, 117)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Results Failed"
        '
        'txtFailed
        '
        Me.txtFailed.Location = New System.Drawing.Point(300, 136)
        Me.txtFailed.Multiline = True
        Me.txtFailed.Name = "txtFailed"
        Me.txtFailed.ReadOnly = True
        Me.txtFailed.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFailed.Size = New System.Drawing.Size(218, 183)
        Me.txtFailed.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(46, 340)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(123, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Progressbar (%complete)"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(595, 406)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtFailed)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtSuccess)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load




        
    End Sub

    Private Sub Form1_close(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.FormClosing
        Me.Dispose()




    End Sub

    Public Shared Function GetDataTable(ByVal query As String, ByVal ConnectionString As String) As DataTable
        'Dim connection1 As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & System.Web.HttpContext.Current.Server.MapPath(dbFile))
        Dim connection1 As OleDbConnection = New OleDbConnection(ConnectionString)

        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)

        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()

        End Try
        Return table1


        Dim x As String
        x = "asdfasdf"

    End Function

    Private Sub FileSynch(strDB As String)
        Dim sRootRepository As String = IDAMROOT ' "\\10.3.12.149\IDAM_RAMSA\"
        Dim AllFiles As DataTable = GetDataTable("select  t.record_id, t.path, a.asset_id, a.projectid, t.extension_win as extension from openquery(MYSQL2, 'select record_id, last_updated, path, extension_win from " & strDB & ".item_table order by record_id desc') t join ipm_asset a on t.record_id = a.oid and a.creation_date > '08/15/2012' order by record_id desc", ConnectionStringIDAM)
        Dim i As Integer
        Dim sExtensionid As String
        Dim sSourceFile As String
        Dim sDestinationFile As String
        Dim sTifSwitch As Boolean = False
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0

        For i = 0 To AllFiles.Rows.Count - 1
            Try
                'get SourceFile
                sSourceFile = ""
                sDestinationFile = ""
                sSourceFile = AllFiles.Rows(i)("path").ToString().Replace(":", "\")
                sDestinationFile = sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource\" + AllFiles.Rows(i)("asset_id").ToString.Trim + "." + AllFiles.Rows(i)("extension").ToString.Trim
                'is directories created?
                If Not IO.Directory.Exists(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim) Then
                    IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim)
                    IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource")
                    IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\acrobat")
                End If
                'now copy files
                'check to see if tiff exists .tif


                Dim fileSize As Long = New System.IO.FileInfo(sSourceFile).Length

                'already there...
                IO.File.Copy(sSourceFile, sDestinationFile, True)
                Me.ExecuteTransaction("insert into ipm_asset_queue(instance_id,asset_id,asset_handler,status,priority,engine_id)values('" & IDAMINSTANCE & "'," & AllFiles.Rows(i)("asset_id").ToString.Trim & ",2,0,0,'" & IDAMENGINE & "')")

                'now send to ipm_asset_queue and mark processed = 7


            Catch ex As Exception
                'MsgBox(ex.Message & AllFiles.Rows(i)("asset_id"))
                txtFailed.Text = txtFailed.Text & strDB & " Asset id: " & AllFiles.Rows(i)("asset_id").ToString.Trim & vbCrLf
            End Try
            ProgressBar1.Value = ProgressBar1.Value + 1
            Me.Refresh()
        Next
        txtSuccess.Text = "Processed " & i & " items."
        Me.Refresh()
    End Sub


    Public Shared Sub ExecuteTransaction(ByVal sqlstring As String)
        Dim ConnectionString As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
        Dim connection As New OleDbConnection(ConnectionString)
        Dim command As New OleDbCommand

        Dim transaction As OleDbTransaction

        ' Set the Connection to the new OleDbConnection.
        command.Connection = connection

        ' Open the connection and execute the transaction.
        Try
            connection.Open()

            ' Start a local transaction with ReadCommitted isolation level.
            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            ' Assign transaction object for a pending local transaction.
            command.Connection = connection
            command.Transaction = transaction

            ' Execute the commands.
            command.CommandText = sqlstring
            command.CommandTimeout = 4000
            command.ExecuteNonQuery()


            ' Commit the transaction.
            transaction.Commit()
            Console.WriteLine("Executed.")

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Try to rollback the transaction
            Try
                transaction.Rollback()

            Catch
                ' Do nothing here; transaction is not active.
            End Try
        End Try
        ' The connection is automatically closed when the
        ' code exits the Using block.

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        FileSynch("Project")
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        FileSynch("General")
    End Sub
End Class