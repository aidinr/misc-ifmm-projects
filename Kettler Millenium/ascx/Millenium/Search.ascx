﻿<%@ control language="VB" autoeventwireup="false" inherits="ascx_Millenium_Search, App_Web_qjy2-1ph" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<script type="text/javascript">

    jQuery(document).ready(function() {
        jQuery('#mycarousel').jcarousel({
            vertical: true,
            scroll: 2
        });
        jQuery('#mycarousel3').jcarousel({
            scroll: 3
        });
    });

    function fireDetailsCarousel() {
        jQuery('#mycarousel2').jcarousel({
            vertical: true,
            scroll: 5
        });

    };
    function fireSearchCarousel() {
        jQuery('#mycarousel').jcarousel({
            vertical: true,
            scroll: 2
        });
    };

</script>
		
				<div id="main_search">
				<table cellpadding="0" cellspacing="0" width="100%" border="0">
					<tr>
						<td align="left" valign="top">
							<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>title_search.jpg" alt="Live Here" /><br /><br />
						</td>
						<td align="left" valign="top">
							<div id="results_labels">
							    
							     
								<table width="100%">
									<tr>
										<td align="left">Results for: <span style="color: #ffffff;" id="SearchHeaderBuilding">All Buildings</span></td>
										<td align="right"><ComponentArt:CallBack ID="CallBackSearchHeader" runat="server">
							     <Content><asp:PlaceHolder ID="PlaceHolderSearchHeader" runat="server">Showing: <asp:Label ID="lblSearchResults" runat="server"></asp:Label> results</asp:PlaceHolder>
								</Content>
								</ComponentArt:CallBack></td>
									</tr>
								</table>
								
							</div>
						</td>
						<td>
						<!--empty-->
						</td>
					</tr>
					<tr>
						<td align="left" valign="top">
						<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>image_search.jpg" alt="Live Here" />
						<br /><br /><br />
						<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>title_refine_search.jpg" alt="Refine Search" /><br /><br />
						<div id="filter">
						<div class="search_filters" style="display:none;"><div id="filter_penthouse_value">Show Penthouse Lofts</div>
                            <table cellspacing="0" cellpadding="0">
 					            <tr>
						            <td width="4" height="4" class="filter_top_left"></td>
						            <td height="4" class="filter_top"></td>
						            <td width="4" height="4" class="filter_top_right"></td>
					            </tr>                   
					            <tr>
						            <td width="4" class="filter_left"></td>
						            <td align="left" class="navigation">
						            			    
							                <a href="javascript:doFilter('penthouse','all');">Show All Penthouse Lofts</a>
						                    <ComponentArt:CallBack ID="CallBackFilterPenthouse" runat="server">
						                    <Content>
						                    <asp:PlaceHolder runat="server" ID="PlaceholderFilterPenthouse">
						                    <asp:Repeater id="filter_penthouse" runat="server">
						                    <ItemTemplate>
						                    <hr />
							                <a href="javascript:doFilter('penthouse','<%#Container.DataItem("Type")%>');"><%#Container.DataItem("Type")%></a>
						                    </ItemTemplate>
						                    </asp:Repeater>
						                    </asp:PlaceHolder>
						                    </Content>
		                                    <LoadingPanelClientTemplate>
		                                    Loading...&nbsp;<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spinner.gif" width="16" height="16" border="0">
		                                    </LoadingPanelClientTemplate>			                    
						                    </ComponentArt:CallBack>  
						           </td>
						            <td class="filter_right"></td>

					            </tr>
					            <tr>
						            <td width="4" height="4" class="filter_bottom_left"></td>
						            <td height="4" class="filter_bottom"></td>
						            <td width="4" height="4" class="filter_bottom_right"></td>
					            </tr>
				            </table>
				        </div>    										
						<!--<br />-->
						<div class="search_filters"><div id="filter_type_value">Show All Apt. Types</div>
                                                      <table cellspacing="0" cellpadding="0">
 					            <tr>
						            <td width="4" height="4" class="filter_top_left"></td>
						            <td height="4" class="filter_top"></td>
						            <td width="4" height="4" class="filter_top_right"></td>
					            </tr>                   
					            <tr>
						            <td width="4" class="filter_left"></td>
						            <td align="left" class="navigation">			    
							             <a href="javascript:doFilter('type','all');">Show All Apt. Types</a>
							                <ComponentArt:CallBack ID="CallBackFilterType" runat="server">
						                    <Content>
						                    <asp:PlaceHolder runat="server" ID="PlaceholderFilterType">
						                    <asp:Repeater id="filter_type" runat="server">
						                    <ItemTemplate>
						                    <hr />
							                <a href="javascript:doFilter('type','<%#Container.DataItem("Type")%>');"><%#Container.DataItem("Type")%></a>
						                    </ItemTemplate>
						                    </asp:Repeater>
						                    </asp:PlaceHolder>
						                    </Content>
		                                    <LoadingPanelClientTemplate>
		                                    Loading...&nbsp;<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spinner.gif" width="16" height="16" border="0">
		                                    </LoadingPanelClientTemplate>						                    
						                    </ComponentArt:CallBack> 						                    
            							 
						            </td>
						            <td class="filter_right"></td>

					            </tr>
					            <tr>
						            <td width="4" height="4" class="filter_bottom_left"></td>
						            <td height="4" class="filter_bottom"></td>
						            <td width="4" height="4" class="filter_bottom_right"></td>
					            </tr>
				            </table>
				            </div>				
						<br />
						<div class="search_filters"><div id="filter_price_value">Show All Price Range</div>
                                                      <table cellspacing="0" cellpadding="0">
 					            <tr>
						            <td width="4" height="4" class="filter_top_left"></td>
						            <td height="4" class="filter_top"></td>
						            <td width="4" height="4" class="filter_top_right"></td>
					            </tr>                   
					            <tr>
						            <td width="4" class="filter_left"></td>
						            <td align="left" class="navigation">
						                 <a href="javascript:doFilter('price','all');">Show All Price Range</a>		    
							                <ComponentArt:CallBack ID="CallBackFilterPrice" runat="server">
						                    <Content>
						                    <asp:PlaceHolder runat="server" ID="PlaceholderFilterPrice">
						                    <asp:Label ID="lblFilterPrice1" runat="server"></asp:Label>
						                    <asp:Label ID="lblFilterPrice2" runat="server"></asp:Label>
						                    <asp:Label ID="lblFilterPrice3" runat="server"></asp:Label>
						                    <asp:Label ID="lblFilterPrice4" runat="server"></asp:Label>
						                    </asp:PlaceHolder>
						                    </Content>
		                                    <LoadingPanelClientTemplate>
		                                    Loading...&nbsp;<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spinner.gif" width="16" height="16" border="0">
		                                    </LoadingPanelClientTemplate>						                    
						                    </ComponentArt:CallBack>
						            </td>
						            <td class="filter_right"></td>

					            </tr>
					            <tr>
						            <td width="4" height="4" class="filter_bottom_left"></td>
						            <td height="4" class="filter_bottom"></td>
						            <td width="4" height="4" class="filter_bottom_right"></td>
					            </tr>
				            </table>
				            </div>	
						<br />
						<div class="search_filters"><div id="filter_floor_value">Show All Floors</div>
                                                     <table cellspacing="0" cellpadding="0">
 					            <tr>
						            <td width="4" height="4" class="filter_top_left"></td>
						            <td height="4" class="filter_top"></td>
						            <td width="4" height="4" class="filter_top_right"></td>
					            </tr>                   
					            <tr>
						            <td width="4" class="filter_left"></td>
						            <td align="left" class="navigation">			    
							             <a href="javascript:doFilter('floor','all');">Show All Floors</a>
							             <hr />
							             <div id="filter_floor_buttons_container">
							                <ComponentArt:CallBack ID="CallBackFilterFloors" runat="server">
						                    <Content>
						                    <asp:PlaceHolder runat="server" ID="PlaceholderFilterFloors">							             
						                    <asp:Repeater id="filter_floor" runat="server">
						                    <ItemTemplate>
							                <div class="filter_floor_buttons" style="margin:7px;"><a href="javascript:doFilter('floor','<%#Container.DataItem("Floor")%>');"><%#Container.DataItem("Floor")%></a></div>
						                    </ItemTemplate>
						                    </asp:Repeater>
						                    </asp:PlaceHolder>
						                    </Content>
		                                    <LoadingPanelClientTemplate>
		                                    Loading...&nbsp;<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spinner.gif" width="16" height="16" border="0">
		                                    </LoadingPanelClientTemplate>						                    
						                    </ComponentArt:CallBack>
						                  </div> 							                    
            							 
						            </td>
						            <td class="filter_right"></td>

					            </tr>
					            <tr>
						            <td width="4" height="4" class="filter_bottom_left"></td>
						            <td height="4" class="filter_bottom"></td>
						            <td width="4" height="4" class="filter_bottom_right"></td>
					            </tr>
				            </table>
				            </div>					
						<br />
						
						<div id="search_carousel">
						  <a href="javascript:tb_show('', 'CarouselCart.aspx?KeepThis=true&TB_iframe=true&height=715&width=1310', false);"><img src="images/Millenium/search_carousel_title.jpg" alt="carousel title" /></a>
						   <ComponentArt:CallBack id="CallBackSearchCarousel"  CacheContent="False" runat="server" >
                           <Content> 
                                     
                                     <asp:PlaceHolder ID="PlaceholderSearchCarousel" runat="server">
                                     <ul id="mycarousel" class="jcarousel jcarousel-skin-tango">
                                     <asp:Literal ID="literalSearchCarousel" runat="server"></asp:Literal>
                                     <asp:Repeater runat="server" ID="searchCarouselRepeater">
                                     <ItemTemplate>
                                        <li onclick="loadApt('carousel',this.id);$('main_details').fade('in');naviScroll.start(4299,0);" id="<%#Container.ItemIndex%>"><img src="<%=Session("WSRetreiveAsset")%>size=1&width=73&height=74&crop=1&id=<%#Container.DataItem("Model_ASSETID")%>" width="73" height="74" /><p><%#Container.DataItem("Price_String")%><br /><small><%#Container.DataItem("Type").ToString.Replace("Bedroom", "BR")%>/<%#Container.DataItem("Bathroom").ToString%> BA</small></p></li>
                                     </ItemTemplate>
                                     </asp:Repeater>
                                     </ul>
                                     </asp:PlaceHolder>
                           
                           </Content>
                           <ClientEvents>
                            <CallbackComplete EventHandler="fireSearchCarousel" />
                           </ClientEvents>
                           </ComponentArt:CallBack> 
						 

						
						</div>
						
						</div>
						</td>
						<td align="left" valign="top">
							<div id="search_sort">
							<img id="sort_type" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_sort_unit_on_asc.jpg" alt="Sort Unit" /><img id="sort_floor" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_sort_floor_off.jpg" alt="Sort Floor" /><img id="sort_area" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_sort_area_off.jpg" alt="Sort Area" /><img id="sort_price" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_sort_price_off.jpg" alt="Sort Price" /><img id="sort_available"  src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_sort_available_off.jpg" alt="Sort Available" /><img id="sort_options" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_sort_options.jpg" alt="Sort Options" style="margin-right:0;cursor:default;" />
							</div>
						<div id="search_results">
 <ComponentArt:CallBack id="CallBack2" CacheContent="false" CssClass="CallBack" runat="server">
        <Content>
          <asp:PlaceHolder id="PlaceHolder2" runat="server">			
						<asp:Repeater id="search_results_rows" runat="server" OnItemDataBound="search_result_rows_ItemDataBound">
						<ItemTemplate>
							<div class="search_row" id="<%#Container.ItemIndex%>" onclick="loadApt('search',this.id);">
								<div class="unit" onclick="$('main_details').fade('in');naviScroll.start(4299,0);">
								<table height="75" cellpadding="0" cellspacing="0">
								<tr>
								    <td valign="middle" align="center" style="background:#fff;">
								    <img align="center" src="<%=Session("WSRetreiveAsset")%>size=1&type=asset&width=75&height=75&crop=2&cache=1&id=<%#Container.DataItem("Model_ASSETID")%>" alt="<%#Container.DataItem("Unit")%>" />
								    </td>
								    <td valign="middle" align="center" valign="middle">
								    <img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" alt="spacer" width="3" height="1"/><%#Container.DataItem("Type").ToString.Replace("Bedroom", "BR")%> / <%#Container.DataItem("Bathroom")%> BA
								    </td>
								</tr>
								</table>
								</div>
								<div class="floor" onclick="$('main_details').fade('in');naviScroll.start(4299,0);"><%#Container.DataItem("Floor")%></div>
								<div class="sqf" onclick="$('main_details').fade('in');naviScroll.start(4299,0);"><%#Container.DataItem("Area")%></div>
								<div class="price" onclick="$('main_details').fade('in');naviScroll.start(4299,0);"><%#Container.DataItem("Price_String")%></div>
								<div class="available" onclick="$('main_details').fade('in');naviScroll.start(4299,0);"><%#Container.DataItem("Available")%></div>
								
								<div class="options" onclick="addToCarousel(this.title);" title="<%#Container.DataItem("Asset_ID")%>">
								<table height="75" cellpadding="0" cellspacing="0">
								<tr>
								    <td valign="middle" align="center" style="background:#fff;">
								    <asp:Literal ID="LiteralSearchRowOptions" runat="server"></asp:Literal>
								    </td>
								</tr>
								</table>
								</div>
							</div>							
						</ItemTemplate>
						<FooterTemplate>
						<script type="text/javascript">
						    //alert('hi');
						    //ctl00_CallBackSearchHeader.callback($(document.body).getElements('.search_row').length);
						    //ctl00_CallBackSearchHeader.callback();
						    CallBackSearchHeader.callback($(document.body).getElements('.search_row').length);
						</script>
						</FooterTemplate>
						</asp:Repeater>
						<asp:Label ID="lblSearchResultsMore" runat="server"></asp:Label>
			</asp:PlaceHolder>
		</Content>
		<ClientEvents>
		<CallbackComplete EventHandler="reFade" />
		</ClientEvents>
		</ComponentArt:CallBack>
						</div>
						<div id="search_results_row_loading">
				      <table width="100%" height="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#676767">
                      <tr>
                        <td align="center">
                        <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                          <td style="font-family:tahoma;font-size:10px;">Loading...&nbsp;</td>
                          <td><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spinner.gif" width="16" height="16" border="0"></td>
                        </tr>
                        </table>
                        </td>
                      </tr>
                      </table>
						</div>
						</td>
						<td align="left" valign="top" width="55">
							<div id="search_scroll">
								<img id="search_scroll_top" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_scroll_top_off.jpg" alt="Scroll Top" /><br />
								<img id="search_scroll_up" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_scroll_up_off.jpg" alt="Scroll Up" /><br />
								<img id="search_scroll_down" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_scroll_down_off.jpg" alt="Scroll Down" /><br />
								<img id="search_scroll_bottom" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_scroll_bottom_off.jpg" alt="Scroll Bottom" /><br />
							</div>
						</td>
					</tr>
					</table>
					</div>
					
					<div id="main_details">

				<ComponentArt:CallBack id="CallBack1"  CacheContent="False" CssClass="CallBack" runat="server" >
                   <Content>
                       <asp:PlaceHolder id="PlaceHolder1" runat="server">
				<div id="details_header">
				Results for: <span style="color:#ffffff;"><asp:Label ID="lblAptNumber" runat="server"></asp:Label></span> <img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>detail_header_gap.gif" alt="gap" /> &nbsp; <asp:Label ID="lblModelNumber" runat="server"> </asp:Label> &nbsp; <img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>detail_header_gap.gif" alt="gap" /> <span style="color:#ffffff;"><asp:Label ID="lblAptType" runat="server"></asp:Label></span>

				</div>
				<div id="details_body">
					<div id="details_gallery">
					<div id="details_gallery_image1"><asp:Label ID="lblDetailGalleryMainImage1" runat="server"></asp:Label></div>
					<div id="details_gallery_image2"><asp:Label ID="lblDetailGalleryMainImage2" runat="server"></asp:Label></div>
					</div>
					<div id="details_content">
					<big>Apartment Details</big>
                            
					<br />
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td width="175">Apartment Number:</td>
							<td><b><asp:Label ID="lblAptNumber2" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">Building:</td>

							<td><b><asp:Label ID="lblAptBuilding" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">Type:</td>
							<td><b><asp:Label ID="lblAptType2" runat="server"></asp:Label></b></td>
						</tr>
						<tr>

							<td width="175">Bathroom(s):</td>
							<td><b><asp:Label ID="lblAptBath" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">Square Feet:</td>
							<td><b><asp:Label ID="lblAptArea" runat="server"></asp:Label></b></td>
						</tr>

						<tr>
							<td width="175">Floor Level:</td>
							<td><b><asp:Label ID="lblAptFloor" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">View(s):</td>
							<td><b><asp:Label ID="lblAptView" runat="server"></asp:Label></b></td>

						</tr>
						<tr>
							<td width="175">Price:</td>
							<td><b><asp:Label ID="lblAptPrice" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">Availability:</td>

							<td><b><asp:Label ID="lblAptAvailable" runat="server"></asp:Label></b></td>
						</tr>						
					</table>
					<br />
					<div id="gallery_detail_thumbs">
					<asp:Label ID="lblDetailGalleryThumbPrev" runat="server"></asp:Label><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" width="11" height="82" alt="thumbnails" />
					    <div id="gallery_detail_thumb_container"><div id="gallery_detail_thumb_images">
					    
					    <asp:Label ID="lblDetailGalleryFirstImage" runat="server"></asp:Label>
					    <asp:Repeater ID="gallery_detail_thumb_images_additional_image" runat="server">
					    <ItemTemplate>
					    <a href="javascript:loadDetailGallery('<%=Session("WSRetreiveAsset")%>size=1&crop=2&width=861&height=531&id=<%#Container.DataItem("Image_Asset_ID")%>');"><img src="<%=Session("WSRetreiveAsset")%>size=1&crop=2&width=80&height=80&id=<%#Container.DataItem("Image_Asset_ID")%>" alt="<%#Container.DataItem("Image_Asset_ID")%>" class="detail_thumb" /></a>
					    </ItemTemplate>
					    </asp:Repeater>
					    </div></div>
					<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" width="11" height="82" alt="thumbnails" /><asp:Label ID="lblDetailGalleryThumbNext" runat="server"></asp:Label>
					</div>
					<div id="gallery_detail_misc"><img id="print_button" onclick="window.print();" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>gallery_detail_button_print.jpg" alt="button" /><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" height="32" width="18" alt="spacer" /><asp:Literal ID="literalDetailAddCarousel" runat="server"></asp:Literal></div>
					</div>
					
                  
				
				</div>
				
				<div id="details_navi" style="position:relative"><asp:Literal ID="LiteralButtonBack" runat="server"></asp:Literal><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" width="474" height="30" alt="navi" /><asp:Label id="lblAptPrev" runat="server"></asp:Label><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" width="17" height="30" alt="navi" /><asp:Label id="lblAptNext" runat="server"></asp:Label></div>
				<script type="text/javascript">
				    fireDetailGallery();
				</script>
				
				
				</asp:PlaceHolder>
                </Content>
                  <ClientEvents>
                    <CallbackComplete EventHandler="fireDetailCarouselCallback" />
                  </ClientEvents>
                  </ComponentArt:CallBack>
                       
                       
                       <ComponentArt:CallBack  id="CallbackDetailCarousel" runat="server">
              <Content>
               <asp:PlaceHolder ID="PlaceholderDetailCarousel" runat="server">
              <div id="details_carousel">
                            <div style="margin-left:18px;margin-top:40px;">
				         <a href="javascript:tb_show('', 'CarouselCart.aspx?KeepThis=true&TB_iframe=true&height=750&width=1310', false);" ><img src="images/Millenium/search_carousel_title.jpg" alt="carousel title" /></a>
                                     <ul id="mycarousel2" class="jcarousel jcarousel-skin-tango">
                                     <asp:Literal ID="literalDetailCarousel" runat="server"></asp:Literal>
                                     <asp:Repeater runat="server" ID="RepeaterDetailCarousel">
                                     <ItemTemplate>
                                        <li style="cursor:pointer;" onclick="loadApt('carousel',this.id);" id="<%#Container.ItemIndex%>"><img src="<%=Session("WSRetreiveAsset")%>size=1&width=73&height=74&crop=1&id=<%#Container.DataItem("Model_ASSETID")%>" alt="<%#Container.DataItem("Asset_ID")%>" width="73" height="74" /><p><%#Container.DataItem("Price_String")%><br /><small><%#Container.DataItem("Type").ToString.Replace("Bedroom", "BR")%>/<%#Container.DataItem("Bathroom").ToString%> BA</small></p></li>
                                     </ItemTemplate>
                                     </asp:Repeater>
                                     </ul>
				    </div>	 
				        </div>    
				        </asp:PlaceHolder>
              </Content>
              <ClientEvents>
                    <CallbackComplete EventHandler="fireDetailsCarousel" />
                  </ClientEvents>
              </ComponentArt:CallBack>
                       
            </div>
              
               
            
			
			
			<ComponentArt:CallBack id="CallBackChangeCarousel"  CacheContent="False" runat="server" >
			<Content></Content>
			<ClientEvents>
			<CallbackComplete EventHandler="renderSearchCarousel" />
			</ClientEvents>
			</ComponentArt:CallBack> 
			
			       