﻿<%@ page language="VB" autoeventwireup="false" inherits="CarouselCart, App_Web_qwigy3qq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Carousel Shopping Cart</title>
    <style type="text/css" >
        @media screen {
        body 
        {
            background: url('images/Millenium/cart_bg.jpg') top left no-repeat;
            font-family: Helvetica,Arial;
            font-size: 14px;
            line-height: 18px;   
        }
        big 
        {
            font-weight: 600;
            font-size: 24px;
        }
        img 
        {
            border: 0;
        }
        #close 
        {
            position: absolute;
            right: 38px;   
            top: 15px;
        }
        #main 
        {
            margin-left:32px;
            margin-right: 30px;
            margin-bottom: 0px;
            margin-top:65px;
            width:1275px;
            float: left;
        }
        #main .item 
        {
            background: #9f9f9f;
            width: 398px;
            height: 278px;
            float: left;
            margin-left: 33px;
            margin-bottom: 18px;   
        }
        #main #item0, #main #item3 
        {
            margin-left: 0;
        }
        #main .item table 
        {
            margin-left: 20px;
            margin-top: 18px;
        }
        #main .item table div
        {
            margin-top: 14px;
        }
        #main .item table .remove
        {
            margin-left: 8px;
        }
        #main .item table .print
        {
            margin-left: 8px;
        }
        #paging 
        {
            width:398px;
            height: 46px;
            margin-left:;
            position: absolute;
            left: 471px;
            top: 667px;
        }
        #print 
        {
            
            position: absolute;
            right: 38px;   
            top: 677px;
            
        }
        .main_details 
        {
            display:none;
            visibility: hidden;
        }
        }
        
        @media print 
{
body {
	padding:0;
	margin:0;
	background: white;
	font-family: arial;
	font-size: 14px;
	color: #666666;
}
img {
	border: 0;
}
div 
{
    display: none;   
}

#container 
{
    display: block;
	width: 1358px;
	height: 766px;
	overflow: hidden;
	margin: 0 auto;
}
#page_container 
{
    display: block;   
}
.main_details
{
    display: block;
    page-break-after: always;
}
.main_details div 
{
    display: block;   
}
.details_header {
	font-size: 20px;
}
.details_header img {
	position: relative;
	top: 3px;
}
.details_body {
	width: 1308px;
	height: 553px;
	border: 0;
	background: .ece5db;
	margin-top: 10px;
}
.details_gallery {
	margin-top: 10px;
	margin-left: 10px;
	width: 650px;
	height: 531px;
	background: .ffffff;
	border: 0;
	float: left;
	overflow: auto;
}
.details_gallery_image2, .details_gallery_image1 {
	width: 650px;
	height: 531px;	
}
.details_gallery_image2 img, .details_gallery_image1 img{
	margin: 0 auto;
	vertical-align: middle;
}
.details_gallery_image2{
	position:relative;
	top:-510px;
	z-index: 2;
}
.details_gallery_image1{
	position:relative;
	z-index: 3;
}
.details_content {
	width: 400px;
	height: 510px;
	float: left;
	margin-left: 20px;
	margin-top: 12px;
}
.details_content big{
	font-size: 20px;
}
.details_content table td{
	font-size: 15px;
	border-bottom: 1px solid .d2c5b2;
	padding-bottom: 10px;
	padding-top: 10px;
	color: .6d6d6d;
}
.details_navi {
	display: none;
}
.gallery_detail_thumbs {
margin-bottom: 60px;
}
.gallery_detail_thumbs * {
	float:left;
}
.gallery_detail_thumbs img {
	cursor: pointer;
}
.gallery_detail_thumb_container {
	width: 365px;
	overflow:hidden;
	height: 82px;
}
.gallery_detail_thumb_images {
	height: 82px;
	width: 1000px;
	float: left;
}
.gallery_detail_misc {
	position: relative;
	top: 15px;
}
.gallery_detail_thumb_images img {
	margin-right: 14px;
	border: 1px solid white;
}
.gallery_detail_thumb_images img.active_thumb {
	border: 1px solid .607abf;
}

.detail_gallery_thumb_next,.detail_gallery_thumb_prev ,.lblAptNext,.lblAptPrev,.navi_search,.print_button,.details_carousel
{
    display: none;   
}
.details_carousel,.details_carousel img,.details_carousel ul,.literalDetailAddCarousel,.gallery_detail_misc 
{
    display:none; 
    visibility: hidden;  
}
#main,#close,#paging,#print 
{
    display:none;
    visibility:hidden;
}
}


    </style>
</head>
<body onclick="self.parent.resetSS();">
    <form id="form1" runat="server">
    <div id="main">
        <asp:Repeater ID="repeaterCarousel" runat="server" OnItemDataBound="repeaterCarousel_ItemDataBound">
            <ItemTemplate>
            <div class="item" id="item<%#Container.ItemIndex%>">
            <table width="353" cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign="top" width="230" align="left"><img src="<%=Session("WSRetreiveAsset")%>size=1&width=207&height=207&crop=2&id=<%#Container.DataItem("Model_ASSETID")%>" alt="Thumb" /></td>
                        <td valign="top">                        <big><%#Container.DataItem("Price_String")%></big><br /><br />                        Apartment <%#Container.DataItem("Unit")%><br />                        <%#Container.DataItem("type")%><br />                        <%#Container.DataItem("Bathroom")%> Bathroom<br />                        <%#Container.DataItem("Area")%> SF<br />                        <asp:literal ID="literalFloor" runat="server"></asp:literal> Floor<br />                        Vacant                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div>
                            <a href="CarouselCart.aspx?page=<%=request.querystring("page") %>&action=delete&deleteid=<%#Container.DataItem("ASSET_ID")%>"><img src="images/Millenium/cart_remove.jpg" alt="Remove" class="print" /></a>
                            <img src="images/Millenium/cart_print.jpg" alt="Print" class="print" />
                            </div>
                        </td>
                    </tr>
            </table>
        </div>
            </ItemTemplate>
        
        </asp:Repeater>
        
        
    </div>
    <div id="close">
        <a href="javascript:self.parent.tb_remove();"><img src="images/Millenium/cart_close.jpg" alt="Close" /></a>
    </div>
    <div id="paging">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="right"><asp:HyperLink runat="server" ID="linkPrev"><asp:Image ID="imagePrev" runat="server" ImageUrl="images/Millenium/cart_prev_off.jpg" /></asp:HyperLink></td>
                <td align="center"><asp:Literal ID="literalPaging" runat="server"></asp:Literal></td>
                <td align="left"><asp:HyperLink runat="server" ID="linkNext"><asp:Image ID="imageNext" runat="server" ImageUrl="images/Millenium/cart_next_off.jpg" /></asp:HyperLink></td>
            </tr>
        </table>
    
    </div>
        <div id="print" onclick="window.print();">
        <asp:Image ID="imagePrint" runat="server" ImageUrl="images/Millenium/cart_print_all.jpg" />
        </div>
        <script type="text/javascript">
            self.parent.fireDetailCarouselCallback();
            self.parent.CallBackSearchCarousel.Callback('');
        </script>
        
        <asp:Repeater ID="RepeaterPrint" runat="server" OnItemDataBound="repeaterPrint_ItemDataBound">
        <ItemTemplate>
        <div class="main_details">

				
				<div class="details_header">
				Results for: <span style="color:#ffffff;"><asp:Label ID="lblAptNumber" runat="server"></asp:Label></span> <img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>detail_header_gap.gif" alt="gap" /> &nbsp; <asp:Label ID="lblModelNumber" runat="server"> </asp:Label> &nbsp; <img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>detail_header_gap.gif" alt="gap" /> <span style="color:#ffffff;"><asp:Label ID="lblAptType" runat="server"></asp:Label></span>

				</div>
				<div class="details_body">
					<div class="details_gallery">
					<div class="details_gallery_image1"><asp:Label ID="lblDetailGalleryMainImage1" runat="server"></asp:Label></div>
					<div class="details_gallery_image2"><asp:Label ID="lblDetailGalleryMainImage2" runat="server"></asp:Label></div>
					</div>
					<div class="details_content">
					<big>Apartment Details</big>
                            
					<br />
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td width="175">Apartment Number:</td>
							<td><b><asp:Label ID="lblAptNumber2" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">Building:</td>

							<td><b><asp:Label ID="lblAptBuilding" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">Type:</td>
							<td><b><asp:Label ID="lblAptType2" runat="server"></asp:Label></b></td>
						</tr>
						<tr>

							<td width="175">Bathroom(s):</td>
							<td><b><asp:Label ID="lblAptBath" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">Square Feet:</td>
							<td><b><asp:Label ID="lblAptArea" runat="server"></asp:Label></b></td>
						</tr>

						<tr>
							<td width="175">Floor Level:</td>
							<td><b><asp:Label ID="lblAptFloor" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">View(s):</td>
							<td><b><asp:Label ID="lblAptView" runat="server"></asp:Label></b></td>

						</tr>
						<tr>
							<td width="175">Price:</td>
							<td><b><asp:Label ID="lblAptPrice" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">Availability:</td>

							<td><b><asp:Label ID="lblAptAvailable" runat="server"></asp:Label></b></td>
						</tr>						
					</table>
					<br />
					<div class="gallery_detail_thumbs">
					<asp:Label ID="lblDetailGalleryThumbPrev" runat="server"></asp:Label><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" width="11" height="82" alt="thumbnails" />
					    <div class="gallery_detail_thumb_container"><div class="gallery_detail_thumb_images">
					    
					    <asp:Label ID="lblDetailGalleryFirstImage" runat="server"></asp:Label>
					    <asp:Repeater ID="gallery_detail_thumb_images_additional_image" runat="server">
					    <ItemTemplate>
					    <a href="javascript:loadDetailGallery('<%=Session("WSRetreiveAsset")%>size=1&crop=2&width=861&height=531&id=<%#Container.DataItem("Image_Asset_ID")%>');"><img src="<%=Session("WSRetreiveAsset")%>size=1&crop=2&width=80&height=80&id=<%#Container.DataItem("Image_Asset_ID")%>" alt="<%#Container.DataItem("Image_Asset_ID")%>" class="detail_thumb" /></a>
					    </ItemTemplate>
					    </asp:Repeater>
					    </div></div>
					<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" width="11" height="82" alt="thumbnails" /><asp:Label ID="lblDetailGalleryThumbNext" runat="server"></asp:Label>
					</div>
					<div class="gallery_detail_misc"><img id="print_button" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>gallery_detail_button_print.jpg" alt="button" /><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" height="32" width="18" alt="spacer" /><asp:Literal ID="literalDetailAddCarousel" runat="server"></asp:Literal></div>
					</div>
					
                  
				
				</div>
				
				<div class="details_navi" style="position:relative"><asp:Literal ID="LiteralButtonBack" runat="server"></asp:Literal><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" width="474" height="30" alt="navi" /><asp:Label id="lblAptPrev" runat="server"></asp:Label><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" width="17" height="30" alt="navi" /><asp:Label id="lblAptNext" runat="server"></asp:Label></div>
            </div>
            </ItemTemplate>
            </asp:Repeater> 
     </form>
</body>
</html>
