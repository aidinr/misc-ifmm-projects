﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Serialization
Imports log4net
Imports System.Net.Mail
Imports System.Data.OleDb
Imports System
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Net


Public Class Form1
    Private Shared ReadOnly Log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Function getFeedfromWS(feedLocation As String) As String
        Dim request As WebRequest = WebRequest.Create(feedLocation)
        Dim array() As Byte = System.Text.Encoding.UTF8.GetBytes("<request>	<auth>		<type>basic</type>		<password>Burlinson1</password>		<username>mburlinson@ifmm.com</username>	</auth>	<requestId>15</requestId>	<method>		<name>getMitsPropertyUnits</name>		<params>			<propertyIds>152758</propertyIds>			<availableUnitsOnly>0</availableUnitsOnly>			<usePropertyPreferences>0</usePropertyPreferences>		</params>	</method></request>")
        request.Credentials = CredentialCache.DefaultCredentials
        request.Method = "POST"
        request.ContentLength = array.Length
        request.ContentType = "APPLICATION/XML; CHARSET=UTF-8"

        Dim dataStream As Stream = request.GetRequestStream()
        dataStream.Write(array, 0, array.Length)
        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()

        dataStream = response.GetResponseStream()
        ' Open the stream using a StreamReader for easy access.
        Dim reader As New StreamReader(dataStream)
        ' Read the content.
        Dim responseFromServer As String = reader.ReadToEnd()

        ' Clean up the streams.
        reader.Close()
        dataStream.Close()
        response.Close()
        Return responseFromServer
    End Function

    Sub ExecuteUpdate()
        log4net.Config.XmlConfigurator.Configure()
        Dim ConfigXML As New XmlDocument
        Dim PropertyNodes As XmlNodeList
        Dim BuildingNodes As XmlNodeList
        Dim emailString As String = ""
        Try
            Dim ConfigDS As New DataSet
            Log.Debug("Begin Execution")
            ConfigDS.ReadXml("Config.xml")
            ConfigXML.LoadXml(ConfigDS.GetXml)
            PropertyNodes = ConfigXML.SelectNodes("/KettlerConfiguration/Properties/Property")
        Catch ex As Exception
            Log.Error("Error Init:" & ex.Message)
            Exit Sub
        End Try
        For Each propertynode As XmlNode In PropertyNodes
            Try
                emailString = emailString & "Importing " & propertynode.Attributes("FeedLocation").Value & vbCrLf
                Dim propertyFeed As String = getFeedfromWS(propertynode.Attributes("FeedLocation").Value)

                Log.Debug("Feed Found: " & propertynode.Attributes("FeedLocation").Value)
                Dim DS As New DataSet
                DS.ReadXml(New XmlTextReader(New StringReader(propertyFeed)))
                Log.Debug("Feed Read: " & propertynode.Attributes("FeedLocation").Value)

                BuildingNodes = propertynode.ChildNodes
                Dim z As Integer


                'Catch for st charles
                If propertynode.Attributes(0).Value = "St. Charles" Then
                    z = InitUnitsStCharles(DS, BuildingNodes)

                    'ElseIf propertynode.Attributes(0).Value = "Gramercy" Then
                    '    z = InitUnitsGramercy(DS, BuildingNodes)
                Else
                    z = InitUnits(DS, BuildingNodes)
                End If

                'Log.Debug("Updating Units:")
                emailString = emailString & "Inserted " & z & " new units." & vbCrLf
                UpdateCurrentUnitsMinAndMax(DS)
                'Log.Debug("Finished Updating UpdateCurrentUnitsMinAndMax:")
                emailString = emailString & "Updated units' Price." & vbCrLf
                UpdateCurrentUnitsSqf(DS)
                emailString = emailString & "Updated units' SQF." & vbCrLf
                'Log.Debug("Finished Updating UpdateCurrentUnitsSqf:")
                UpdateCurrentUnitsAvailability(DS)
                emailString = emailString & "Updated units' Availability." & vbCrLf
                UpdateCurrentUnitsAvailabilityDate(DS)
                emailString = emailString & "Updated units' Availability Date." & vbCrLf
                'Log.Debug("Finished Updating UpdateCurrentUnitsAvailability:")
                UpdateCurrentUnitsModel(DS)
                emailString = emailString & "Updated units' Model information." & vbCrLf
                'Log.Debug("Finished Updating UpdateCurrentUnitsModel:")
                UpdateCurrentUnitsRooms(DS)
                emailString = emailString & "Updated units' Bathroom information." & vbCrLf
                'Log.Debug("Finished Updating UpdateCurrentUnitsBathroom:")
                UpdateCurrentUnitsFloor(DS)
                emailString = emailString & "Updated units' Floor information." & vbCrLf
                Log.Debug("Finished Updating UpdateCurrentUnitsFloor:")
                If propertynode.Attributes(0).Value = "Gramercy" Or propertynode.Attributes(0).Value = "Palatine" Or propertynode.Attributes(0).Value = "CrescentNinth" Then
                    UpdateCurrentUnitsAmenitiesGramercyPalatine(DS)
                Else
                    UpdateCurrentUnitsAmenities(DS)
                End If
                emailString = emailString & "Updated units' Views information." & vbCrLf
            Catch ex As Exception
                Log.Error("Error Main:" & ex.Message)
                SendError("An error has occurred. Error message: " & ex.Message)
            End Try
            emailString = emailString & propertynode.Attributes("FeedLocation").Value & " has been successfully imported into iDAM." & vbCrLf & vbCrLf

        Next
        emailString = emailString & vbCrLf & vbCrLf & "Script execution has ended. Daily update successful."
        Log.Debug("End Execution")
        SendSuccess(emailString)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click


        ExecuteUpdate()

    End Sub

    Private Sub PrintRows(ByVal ds As DataSet, ByVal label As String) 'Debug sub
        Console.WriteLine(ControlChars.Cr + label)
        Dim t As DataTable
        For Each t In ds.Tables
            Console.WriteLine("TableName: " + t.TableName)
            Dim c As DataColumn
            For Each c In t.Columns
                Console.Write(ControlChars.Tab + " " + c.ColumnName)
            Next c
            Console.WriteLine()
            Dim r As DataRow
            For Each r In t.Rows

                For Each c In t.Columns
                    Console.Write(ControlChars.Tab + " " + r(c).ToString())
                Next c
                Console.WriteLine()
            Next r
        Next t
    End Sub
    Private Function GetDataTable(ByVal ds As DataSet, ByVal TableName As String) As DataTable
        Return ds.Tables(TableName)
    End Function

    Public Shared Function GetDataTable(ByVal query As String, ByVal ConnectionString As String) As DataTable
        'Dim connection1 As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & System.Web.HttpContext.Current.Server.MapPath(dbFile))

        ConnectionString = "Provider=sqloledb;" + ConnectionString.Replace("Data Source", "Server").Replace("Initial Catalog", "Database").Replace("User Id", "Uid").Replace("Password", "Pwd")

        Dim connection1 As OleDbConnection = New OleDbConnection(ConnectionString)
        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)
        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()

        End Try
        Return table1
    End Function


    Private Function checkforduplicationwithinproject(ByVal UnitName, ByVal projectid, ByVal unitid) As Boolean
        'if duplication...remove all..
        'no duplication and unit does not exist...net create
        Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select asset_id from ipm_asset where name = @mn and available = 'Y' and projectid = @projectid", MyConnection)
        MyCommand1.SelectCommand.Parameters.Add("@mn", SqlDbType.NVarChar, 255)
        MyCommand1.SelectCommand.Parameters("@mn").Value = UnitName
        MyCommand1.SelectCommand.Parameters.Add("@projectid", SqlDbType.NVarChar, 255)
        MyCommand1.SelectCommand.Parameters("@projectid").Value = projectid
        Dim DT1 As New DataTable("assets")
        MyCommand1.Fill(DT1)
        Select Case DT1.Rows.Count
            Case Is > 1 'duplication
                'delete all

                Dim MyCommand2 As New SqlCommand("", MyConnection)
                MyCommand2.CommandText = "delete from ipm_asset where asset_id in (select asset_id from ipm_asset where name = '" & UnitName & "' and available = 'Y' and projectid = " & projectid & ")"
                MyCommand2.Connection.Open()
                MyCommand2.ExecuteNonQuery()
                MyCommand2.Connection.Close()
                Return True
            Case 0
                Return True
            Case 1
                'check for unitid to oid correctnesss

                Dim MyCommand3 As SqlDataAdapter = New SqlDataAdapter("select asset_id from ipm_asset where name = @mn and available = 'Y' and projectid = @projectid and oid = @unitid", MyConnection)
                MyCommand3.SelectCommand.Parameters.Add("@mn", SqlDbType.NVarChar, 255)
                MyCommand3.SelectCommand.Parameters("@mn").Value = UnitName
                MyCommand3.SelectCommand.Parameters.Add("@projectid", SqlDbType.NVarChar, 255)
                MyCommand3.SelectCommand.Parameters("@projectid").Value = projectid
                MyCommand3.SelectCommand.Parameters.Add("@unitid", SqlDbType.NVarChar, 255)
                MyCommand3.SelectCommand.Parameters("@unitid").Value = unitid
                Dim DT2 As New DataTable("assets")
                MyCommand3.Fill(DT2)
                If DT2.Rows.Count = 0 Then
                    'then update oid
                    Dim MyCommand2 As New SqlCommand("", MyConnection)
                    MyCommand2.CommandText = "update ipm_asset set oid = " & unitid & " where asset_id in (select asset_id from ipm_asset where name = '" & UnitName & "' and available = 'Y' and projectid = " & projectid & ")"
                    MyCommand2.Connection.Open()
                    MyCommand2.ExecuteNonQuery()
                    MyCommand2.Connection.Close()
                End If

            Case Else
                Return False
        End Select


    End Function



    Private Function oIDToAID(ByVal oID As String) As String
        Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select asset_id from ipm_asset where oid = @oid and available = 'y'", MyConnection)

        MyCommand1.SelectCommand.Parameters.Add("@oid", SqlDbType.Int)
        MyCommand1.SelectCommand.Parameters("@oid").Value = oID

        Dim DT1 As New DataTable("assets")

        Try
            MyCommand1.Fill(DT1)
        Catch ex As Exception

        End Try


        If (DT1.Rows.Count > 0) Then

            Try
                Return DT1.Rows(0)("asset_id")
            Catch ex As Exception
                Return "-1"
            End Try
        Else
            Return "-1"
        End If

    End Function

    Private Function getmodelnamebyasset_id(ByVal asset_id) As String
        Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select item_value from ipm_asset_field_value where asset_id = @oid and item_id = 229805  ", MyConnection)

        MyCommand1.SelectCommand.Parameters.Add("@oid", SqlDbType.NVarChar, 255)
        MyCommand1.SelectCommand.Parameters("@oid").Value = asset_id

        Dim DT1 As New DataTable("assets")

        MyCommand1.Fill(DT1)

        If (DT1.Rows.Count > 0) Then

            Try
                Return DT1.Rows(0)("item_value")
            Catch ex As Exception
                Return "-1"
            End Try
        Else
            Return "-1"
        End If
    End Function

    Function getmodelid(ByVal asset_id) As String
        Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select item_value from ipm_asset_field_value where asset_id = @oid and item_id = 21550857  ", MyConnection)

        MyCommand1.SelectCommand.Parameters.Add("@oid", SqlDbType.NVarChar, 255)
        MyCommand1.SelectCommand.Parameters("@oid").Value = asset_id

        Dim DT1 As New DataTable("assets")

        MyCommand1.Fill(DT1)

        If (DT1.Rows.Count > 0) Then

            Try
                Return DT1.Rows(0)("item_value")
            Catch ex As Exception
                Return "-1"
            End Try
        Else
            Return "-1"
        End If
    End Function

    Private Function GetUDFID(ByVal tag As String) As String
        Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select item_id from ipm_asset_field_desc where item_tag = @tag and active = '1'", MyConnection)

        MyCommand1.SelectCommand.Parameters.Add("@tag", SqlDbType.NVarChar, 255)
        MyCommand1.SelectCommand.Parameters("@tag").Value = tag

        Dim DT1 As New DataTable("udf")

        MyCommand1.Fill(DT1)
        If (DT1.Rows.Count > 0) Then
            Try
                Return DT1.Rows(0)("item_id")
            Catch ex As Exception
                Return "-1"
            End Try
        Else
            Return "-1"
        End If
    End Function
    Private Sub UpdateCurrentUnitsAvailability(ByVal ds As DataSet)


        Dim row As DataRow

        Dim Availability_Tag As String = "IDAM_KETTLER_AVAILABLE"
        Dim Availability_State As String

        Dim asset_id As String
        Dim item_id As String = GetUDFID(Availability_Tag)

        Try
            For Each row In ds.Tables("ILS_Unit").Rows

                asset_id = oIDToAID(row.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0).GetChildRows("Unit_Identification")(0)("IDValue"))

                If (asset_id <> "-1") Then



                    If (row.GetChildRows("ILS_Unit_Availability")(0)("VacancyClass") = "Occupied") Then
                        Availability_State = "0"
                    Else
                        Availability_State = "1"
                    End If
                    UpdateAddUDF(asset_id, item_id, Availability_State)
                    Console.WriteLine("updated availability for " + asset_id)

                End If
            Next
        Catch ex As Exception
            Log.Error("Error Init:" & ex.Message)
            SendError(ex.Message)
        End Try








    End Sub

    Private Sub UpdateCurrentUnitsAvailabilityDate(ByVal ds As DataSet)


        Dim row As DataRow

        Dim Availability_Tag As String = "IDAM_KETTLER_AVAILABILITY_DATE"


        Dim asset_id As String
        Dim item_id As String = GetUDFID(Availability_Tag)

        Try


            Dim MainXML As XmlDocument = New XmlDocument
            MainXML.LoadXml(ds.GetXml())

            Dim NodesList As XmlNodeList



            NodesList = MainXML.SelectNodes("/response/result/PhysicalProperty/Property/ILS_Unit")
            For Each UnitNode As XmlNode In NodesList
                asset_id = oIDToAID(UnitNode.SelectSingleNode("Identification").SelectSingleNode("IDValue").InnerText)
                
                If (asset_id <> "-1") Then
                    Dim node As XmlNode
                    node = UnitNode.SelectSingleNode("Availability").SelectSingleNode("VacateDate")
                    If node Is Nothing Then
                        UpdateAddUDF(asset_id, item_id, System.DBNull.Value)

                    Else
                        UpdateAddUDF(asset_id, item_id, node.Attributes("Year").Value.ToString() & "-" & node.Attributes("Month").Value.ToString() & "-" & node.Attributes("Day").Value.ToString())

                    End If

                    Console.WriteLine("updated availability date for " + asset_id)
                End If
            Next





        Catch ex As Exception
            Log.Error("Error Init:" & ex.Message)
            SendError(ex.Message)
        End Try








    End Sub
    Private Function UpdateAddUDF(ByVal asset_id, ByVal item_id, ByVal item_value) As Boolean
        Dim connection As New SqlConnection(My.Settings.ConnString)
        Dim querystring1 As String
        Dim MyCommand1 As New SqlCommand("", connection)


        MyCommand1.Parameters.Add("@item_id", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@asset_id", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@item_value", SqlDbType.NVarChar, 255)


        MyCommand1.Parameters("@item_id").Value = item_id
        MyCommand1.Parameters("@asset_id").Value = asset_id
        MyCommand1.Parameters("@item_value").Value = item_value


        querystring1 = "delete from ipm_asset_field_value where item_id = @item_id and asset_id = @asset_id"

        Try
            MyCommand1.CommandText = querystring1
            MyCommand1.Connection.Open()
            MyCommand1.ExecuteNonQuery()
            querystring1 = "insert into ipm_asset_field_value (item_id,item_value,asset_id) values (@item_id,@item_value,@asset_id)"
            MyCommand1.CommandText = querystring1
            'Console.WriteLine(MyCommand1.SelectCommand.CommandText)
            MyCommand1.ExecuteNonQuery()
            MyCommand1.Connection.Close()
        Catch ex As Exception
            Log.Error("Error Init:" & ex.Message)
            SendError(ex.Message)
        End Try




        Return True

    End Function


    Function Updatemodeldescription(ByVal asset_id, ByVal item_value) As Boolean
        Dim connection As New SqlConnection(My.Settings.ConnString)
        Dim querystring1 As String
        Dim MyCommand1 As New SqlCommand("", connection)

        MyCommand1.Parameters.Add("@asset_id", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@item_value", SqlDbType.NVarChar, 255)


        MyCommand1.Parameters("@asset_id").Value = asset_id
        MyCommand1.Parameters("@item_value").Value = item_value


        querystring1 = "update ipm_asset set description = @item_value where asset_id = @asset_id"
        MyCommand1.CommandText = querystring1


        Try
            MyCommand1.Connection.Open()
            'Console.WriteLine(MyCommand1.SelectCommand.CommandText)
            MyCommand1.ExecuteNonQuery()
            MyCommand1.Connection.Close()
        Catch ex As Exception
            Log.Error("Error Init:" & ex.Message)
            SendError(ex.Message)
        End Try




        Return True

    End Function

    Private Sub UpdateCurrentUnitsModel(ByVal DS As DataSet)


        Dim Model_ID As String = "IDAM_KETTLER_Model"
        Dim asset_id As String
        Dim unitRows As DataRow

        Try
            For Each unitRows In DS.Tables("ILS_Unit").Rows


                asset_id = oIDToAID(unitRows.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0).GetChildRows("Unit_Identification")(0)("IDValue"))

                If (asset_id <> "-1") Then
                    UpdateAddUDF(asset_id, GetUDFID(Model_ID), unitRows.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("FloorplanId"))
                    Console.WriteLine("updated modelname for " + asset_id)
                End If


            Next
        Catch ex As Exception
            Log.Error("Error Init:" & ex.Message)
            SendError(ex.Message)
        End Try


    End Sub

    Private Sub UpdateCurrentUnitsSqf(ByVal DS As DataSet)


        Dim Model_Tag As String = "IDAM_KETTLER_SQ"
        Dim asset_id As String
        Dim item_id As String = GetUDFID(Model_Tag)
        Dim unitRows As DataRow

        Try
            For Each unitRows In DS.Tables("ILS_Unit").Rows

                asset_id = oIDToAID(unitRows.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0).GetChildRows("Unit_Identification")(0)("IDValue"))

                If (asset_id <> "-1") Then
                    UpdateAddUDF(asset_id, item_id, unitRows.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("MinSquareFeet"))
                    Console.WriteLine("updated sqf for " + asset_id)
                End If


            Next
        Catch ex As Exception
            Log.Error("Error Init:" & ex.Message)
            SendError(ex.Message)
        End Try


    End Sub



    Private Sub UpdateCurrentUnitsRooms(ByVal DS As DataSet)


        Dim Bath_Tag As String = "IDAM_KETTLER_BATHROOM"
        Dim Bed_Tag As String = "IDAM_KETTLER_UNIT"
        Dim Name_Tag As String = "IDAM_KETTLER_ModelName"
        Dim Long_Tag As String = "IDAM_KETTLER_ModelTypeLong"
        Dim asset_id As String
        Dim unitRows As DataRow
        Dim modelRows As DataRow

        Dim modelID As String = ""
        Try
            For Each unitRows In DS.Tables("ILS_Unit").Rows

                asset_id = oIDToAID(unitRows.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0).GetChildRows("Unit_Identification")(0)("IDValue"))

                If (asset_id <> "-1") Then
                    modelID = unitRows.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("FloorPlanId")
                    If (modelID <> "-1") Then

                        For Each modelRows In DS.Tables("FloorPlan").Rows
                            If modelRows.GetChildRows("FloorPlan_Identification")(0)("IDValue") = modelID Then
                                UpdateAddUDF(asset_id, GetUDFID(Name_Tag), modelRows("name"))
                                UpdateAddUDF(asset_id, GetUDFID(Long_Tag), modelRows("name"))

                                For Each roomRow As DataRow In modelRows.GetChildRows("Floorplan_Room")
                                    If (roomRow("RoomType") = "Bathroom") Then
                                        UpdateAddUDF(asset_id, GetUDFID(Bath_Tag), roomRow("count"))
                                    ElseIf (roomRow("RoomType") = "Bedroom") Then
                                        If (roomRow("count") > 1) Then
                                            UpdateAddUDF(asset_id, GetUDFID(Bed_Tag), roomRow("count") & " Bedrooms")
                                        Else
                                            UpdateAddUDF(asset_id, GetUDFID(Bed_Tag), roomRow("count") & " Bedroom")
                                        End If

                                    End If

                                Next

                                Exit For
                            End If
                        Next
                       



                       
                    End If

                    Console.WriteLine("updated bathrooms and bedrooms for " + asset_id)
                End If


            Next
        Catch ex As Exception
            Log.Error("Error Init:" & ex.Message)
            SendError(ex.Message)
        End Try



    End Sub
    Private Sub UpdateCurrentUnitsFloor(ByVal DS As DataSet)


        Dim Model_Tag As String = "IDAM_KETTLER_FLOOR"
        Dim asset_id As String
        Dim item_id As String = GetUDFID(Model_Tag)
        Dim unitRows As DataRow
        Try
            For Each unitRows In DS.Tables("ILS_Unit").Rows

                asset_id = oIDToAID(unitRows.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0).GetChildRows("Unit_Identification")(0)("IDValue"))
                Dim alphcheck As Char() = unitRows.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("MarketingName").ToString.ToCharArray
                If (asset_id <> "-1") And Not Char.IsLetter(alphcheck(0)) Then

                    UpdateAddUDF(asset_id, item_id, GetFloor(unitRows.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("MarketingName")))
                    Console.WriteLine("updated floor for " + asset_id)
                End If


            Next
        Catch ex As Exception
            Log.Error("Error Init:" & ex.Message)
            SendError(ex.Message)
        End Try


    End Sub

    Private Sub UpdateCurrentUnitsAmenities(ByVal DS As DataSet)


        Dim Model_Tag As String = "IDAM_KETTLER_AMENITIES"
        Dim asset_id As String
        Dim item_id As String = GetUDFID(Model_Tag)
        Dim unitRows As DataRow
        Try
            For Each unitRows In DS.Tables("ILS_Unit").Rows

                asset_id = oIDToAID(unitRows.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0).GetChildRows("Unit_Identification")(0)("IDValue"))
                

                If (asset_id <> "-1") Then
                    Dim amenitiesRows() As DataRow = unitRows.GetChildRows("ILS_Unit_Amenity")
                    Dim amenitiesString As String = ""

                    If (amenitiesRows.Length > 0) Then

                        For Each x As DataRow In amenitiesRows
                            If (x("Description").ToString.Contains("View")) Then
                                amenitiesString = amenitiesString & x("Description").Replace("View", "").ToString.Replace("-", "") & ", "
                            End If

                        Next
                        If (amenitiesString.Length > 0) Then
                            amenitiesString = amenitiesString.Substring(0, amenitiesString.Length - 2)

                            UpdateAddUDF(asset_id, item_id, amenitiesString)
                            Console.WriteLine("updated amenities for " + asset_id)
                        Else
                            UpdateAddUDF(asset_id, item_id, "Coming soon")
                            Console.WriteLine("no amenities for " + asset_id)
                        End If
                    Else
                        UpdateAddUDF(asset_id, item_id, "Coming soon")
                        Console.WriteLine("no amenities for " + asset_id)
                    End If
                End If


            Next
        Catch ex As Exception
            Log.Error("Error Init:" & ex.Message)
            SendError(ex.Message)
        End Try


    End Sub


    Private Sub UpdateCurrentUnitsAmenitiesGramercyPalatine(ByVal DS As DataSet)


        Dim Model_Tag As String = "IDAM_KETTLER_AMENITIES"
        Dim asset_id As String
        Dim item_id As String = GetUDFID(Model_Tag)
        Dim unitRows As DataRow
        Try
            For Each unitRows In DS.Tables("ILS_Unit").Rows

                asset_id = oIDToAID(unitRows("ID"))


                If (asset_id <> "-1") Then
                    Dim amenitiesRows() As DataRow = unitRows.GetChildRows("ILS_Unit_Amenity")
                    Dim amenitiesString As String = ""

                    If (amenitiesRows.Length > 0) Then

                        For Each x As DataRow In amenitiesRows
                            amenitiesString = amenitiesString & x("Description").ToString.Replace("-", "") & ", "
                        Next
                        If (amenitiesString.Length > 0) Then
                            amenitiesString = amenitiesString.Substring(0, amenitiesString.Length - 2)

                            UpdateAddUDF(asset_id, item_id, amenitiesString)
                            Console.WriteLine("updated amenities for " + asset_id)
                        Else
                            UpdateAddUDF(asset_id, item_id, "Coming soon")
                            Console.WriteLine("no amenities for " + asset_id)
                        End If
                    Else
                        UpdateAddUDF(asset_id, item_id, "Coming soon")
                        Console.WriteLine("no amenities for " + asset_id)
                    End If
                End If


            Next
        Catch ex As Exception
            Log.Error("Error Init:" & ex.Message)
            SendError(ex.Message)
        End Try


    End Sub


    Private Sub UpdateCurrentUnitsMinAndMax(ByVal DS As DataSet)


        Dim MinPriceTag As String = "IDAM_KETTLER_MINPRICE"
        Dim MaxPriceTag As String = "IDAM_KETTLER_MAXPRICE"
        Dim PriceTag As String = "IDAM_KETTLER_PRICE"
        Dim asset_id As String

        Dim unitRows As DataRow

        Try
            For Each unitRows In DS.Tables("ILS_Unit").Rows

                asset_id = oIDToAID(unitRows.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0).GetChildRows("Unit_Identification")(0)("IDValue"))

                If (asset_id <> "-1") Then
                    UpdateAddUDF(asset_id, GetUDFID(MinPriceTag), unitRows("EffectiveRent"))
                    UpdateAddUDF(asset_id, GetUDFID(MaxPriceTag), unitRows("EffectiveRent"))
                    UpdateAddUDF(asset_id, GetUDFID(PriceTag), unitRows("EffectiveRent"))
                    Console.WriteLine("updated prices for " + asset_id)
                End If


            Next
        Catch ex As Exception
            Log.Error("Error Init:" & ex.Message)
            SendError(ex.Message)
        End Try


    End Sub

    Private Function InitUnits(ByVal DS As DataSet, ByVal Buildings As XmlNodeList) As Integer

        Dim UnitTables As DataTable = DS.Tables("ILS_Unit")

        Dim insertID As Integer
        Dim projectID As String
        Dim floor As String
        Dim categoryID As String

        Dim sql As String
        sql = "insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date,oid) values (@name,@projectid,'1',@insertID,1,1,@name,@mediatype,@projectid,@categoryid,getdate(),getdate(),@oid)"

        Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        Dim MyCommand1 As New SqlCommand("", MyConnection)

        MyCommand1.Parameters.Add("@name", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@projectid", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@mediatype", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@categoryid", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@insertid", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@oid", SqlDbType.NVarChar, 255)
        Dim i As Integer = GetMaxAssetID() + 9000
        Dim z As Integer = 0
        Dim oidCheck As String = ""
        For Each row As DataRow In UnitTables.Rows
            'oidCheck = oIDToAID(row("id"))
            'check for duplicatiuon of projectid and unit marketing name
            'if duplication...remove all..
            'no duplication and unit does not exist...net create
            Dim alphcheck As Char() = row.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("MarketingName").ToString.ToCharArray

            'If row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName").ToString = "361" Then
            'Dim s As String = ""
            ' End If


            If checkforduplicationwithinproject(row.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("MarketingName"), BuildingIDToProjectID(row.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("BuildingId"), Buildings), row.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0).GetChildRows("Unit_Identification")(0)("IDValue")) And Not Char.IsLetter(alphcheck(0)) Then
                'Log.Debug("Adding Unit: " & row("UnitName"))
                insertID = i + 1
                projectID = BuildingIDToProjectID(row.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("BuildingId"), Buildings)
                floor = GetFloor(row.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("MarketingName"))
                categoryID = GetUnitsFolderID(projectID)

                MyCommand1.CommandText = sql
                MyCommand1.Parameters("@name").Value = row.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("MarketingName").ToString
                MyCommand1.Parameters("@projectid").Value = projectID
                MyCommand1.Parameters("@mediatype").Value = "10"
                MyCommand1.Parameters("@categoryid").Value = categoryID
                MyCommand1.Parameters("@insertid").Value = insertID
                MyCommand1.Parameters("@oid").Value = row.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0).GetChildRows("Unit_Identification")(0)("IDValue").ToString

                Try
                    MyCommand1.Connection.Open()
                    MyCommand1.ExecuteNonQuery()
                    MyCommand1.Connection.Close()
                    'sendMail("Added new unit for " & row("BuildingName") & ". Unit number is " & row("UnitName"))
                Catch ex As Exception
                    Log.Error("Error Init:" & ex.Message)
                    SendError(ex.Message)
                End Try


                Console.WriteLine("inserted " & row.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("MarketingName") & "at building " & row.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("BuildingId") & " as assetID " & insertID & " into " & projectID & " folder " & categoryID)
                'Log.Debug("inserted " & row("UnitName") & "at building " & row("BuildingName") & " as assetID " & insertID & " into " & projectID & " folder " & categoryID)
                i = i + 1
                z = z + 1
            Else
                Console.WriteLine("skipped " & row.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("MarketingName") & "at building " & row.GetChildRows("ILS_Unit_Units")(0).GetChildRows("Units_Unit")(0)("BuildingId"))
            End If

        Next


        sql = "update ipm_cat_counter set counter = " & i
        Dim MyCommand2 As New SqlCommand(sql, MyConnection)

        Try
            MyCommand2.Connection.Open()
            MyCommand2.ExecuteNonQuery()
            MyCommand2.Connection.Close()
        Catch ex As Exception
            Log.Error("Error Init:" & ex.Message)
            SendError(ex.Message)
        End Try



        Return z


    End Function





    Private Function InitUnitsStCharles(ByVal DS As DataSet, ByVal Buildings As XmlNodeList) As Integer

        'specific for st charles

        Dim UnitTables As DataTable = DS.Tables("ILS_Unit")

        Dim insertID As Integer
        Dim projectID As String
        Dim floor As String
        Dim categoryID As String

        Dim sql As String
        sql = "insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date,oid) values (@name,@projectid,'1',@insertID,1,1,@name,@mediatype,@projectid,@categoryid,getdate(),getdate(),@oid)"

        Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        Dim MyCommand1 As New SqlCommand("", MyConnection)

        MyCommand1.Parameters.Add("@name", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@projectid", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@mediatype", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@categoryid", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@insertid", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@oid", SqlDbType.NVarChar, 255)
        Dim i As Integer = GetMaxAssetID()
        Dim z As Integer = 0
        Dim oidCheck As String = ""
        For Each row As DataRow In UnitTables.Rows
            'oidCheck = oIDToAID(row("id"))
            'check for duplicatiuon of projectid and unit marketing name
            'if duplication...remove all..
            'no duplication and unit does not exist...net create
            Dim alphcheck As Char() = row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName").ToString.ToCharArray
           
            'get projectid for floorplan community using building id
            'ensure proper FloorPlan
            ' If row("FloorplanID") = "62051" Then
            'Dim s As String = "debug this record"

            ' Else
            ' Return 0
            ' End If
            If BuildingIDToProjectID(row("FloorplanID"), Buildings) <> "-1" Then
                'get building ID and Projectid
                If checkforduplicationwithinproject(row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName"), BuildingIDToProjectID(row("FloorplanID"), Buildings), row("id")) And Not Char.IsLetter(alphcheck(0)) Then
                    'Log.Debug("Adding Unit: " & row("UnitName"))
                    insertID = GetMaxAssetID()
                    projectID = BuildingIDToProjectID(row("FloorplanID"), Buildings)
                    floor = GetFloor(row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName"))
                    categoryID = GetUnitsFolderID(projectID)

                    MyCommand1.CommandText = sql
                    MyCommand1.Parameters("@name").Value = row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName").ToString
                    MyCommand1.Parameters("@projectid").Value = projectID
                    MyCommand1.Parameters("@mediatype").Value = "10"
                    MyCommand1.Parameters("@categoryid").Value = categoryID
                    MyCommand1.Parameters("@insertid").Value = insertID
                    MyCommand1.Parameters("@oid").Value = row("id").ToString

                    Try
                        MyCommand1.Connection.Open()
                        MyCommand1.ExecuteNonQuery()
                        MyCommand1.Connection.Close()
                        'sendMail("Added new unit for " & row("BuildingName") & ". Unit number is " & row("UnitName"))
                    Catch ex As Exception
                        Log.Error("Error Init:" & ex.Message)
                        SendError(ex.Message)
                    End Try


                    Console.WriteLine("inserted " & row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName") & "at building " & row("BuildingID") & " as assetID " & insertID & " into " & projectID & " folder " & categoryID)
                    'Log.Debug("inserted " & row("UnitName") & "at building " & row("BuildingName") & " as assetID " & insertID & " into " & projectID & " folder " & categoryID)
                    i = i + 1
                    z = z + 1
                Else
                    Console.WriteLine("skipped " & row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName") & "at building " & row("BuildingID"))
                End If
            Else
                Console.WriteLine("skipped " & row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName") & "at FloorPlan " & row("FloorplanID"))
            End If


        Next


        Return z


    End Function


    Private Function InitUnitsGramercy(ByVal DS As DataSet, ByVal Buildings As XmlNodeList) As Integer

        'specific for st charles

        Dim UnitTables As DataTable = DS.Tables("ILS_Unit")

        Dim insertID As Integer
        Dim projectID As String
        Dim floor As String
        Dim categoryID As String

        Dim sql As String
        sql = "insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date,oid) values (@name,@projectid,'1',@insertID,1,1,@name,@mediatype,@projectid,@categoryid,getdate(),getdate(),@oid)"

        Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        Dim MyCommand1 As New SqlCommand("", MyConnection)

        MyCommand1.Parameters.Add("@name", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@projectid", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@mediatype", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@categoryid", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@insertid", SqlDbType.NVarChar, 255)
        MyCommand1.Parameters.Add("@oid", SqlDbType.NVarChar, 255)
        Dim i As Integer = GetMaxAssetID()
        Dim z As Integer = 0
        Dim oidCheck As String = ""
        For Each row As DataRow In UnitTables.Rows
            'oidCheck = oIDToAID(row("id"))
            'check for duplicatiuon of projectid and unit marketing name
            'if duplication...remove all..
            'no duplication and unit does not exist...net create
            Dim alphcheck As Char() = row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName").ToString.ToCharArray
            
            'get projectid for floorplan community using building id
            'ensure proper FloorPlan
            If BuildingIDToProjectID(row("BuildingID"), Buildings) <> "-1" Then
                'get building ID and Projectid
                If checkforduplicationwithinproject(row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName"), BuildingIDToProjectID(row("BuildingID"), Buildings), row("id")) And Not Char.IsLetter(alphcheck(0)) Then
                    'Log.Debug("Adding Unit: " & row("UnitName"))
                    insertID = GetMaxAssetID()
                    projectID = BuildingIDToProjectID(row("BuildingID"), Buildings)
                    floor = GetFloor(row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName"))
                    categoryID = GetUnitsFolderID(projectID)

                    MyCommand1.CommandText = sql
                    MyCommand1.Parameters("@name").Value = row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName").ToString
                    MyCommand1.Parameters("@projectid").Value = projectID
                    MyCommand1.Parameters("@mediatype").Value = "10"
                    MyCommand1.Parameters("@categoryid").Value = categoryID
                    MyCommand1.Parameters("@insertid").Value = insertID
                    MyCommand1.Parameters("@oid").Value = row("id").ToString

                    Try
                        MyCommand1.Connection.Open()
                        MyCommand1.ExecuteNonQuery()
                        MyCommand1.Connection.Close()
                        'sendMail("Added new unit for " & row("BuildingName") & ". Unit number is " & row("UnitName"))
                    Catch ex As Exception
                        Log.Error("Error Init:" & ex.Message)
                        SendError(ex.Message)
                    End Try


                    Console.WriteLine("inserted " & row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName") & "at building " & row("BuildingID") & " as assetID " & insertID & " into " & projectID & " folder " & categoryID)
                    'Log.Debug("inserted " & row("UnitName") & "at building " & row("BuildingName") & " as assetID " & insertID & " into " & projectID & " folder " & categoryID)
                    i = i + 1
                    z = z + 1
                Else
                    Console.WriteLine("skipped " & row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName") & "at building " & row("BuildingID"))
                End If
            Else
                Console.WriteLine("skipped " & row.GetChildRows("ILS_Unit_Unit")(0)("MarketingName") & "at FloorPlan " & row("FloorplanID"))
            End If


        Next


        Return z


    End Function



    Private Function BuildingIDToProjectID(ByVal BuildingID As String, ByVal Buildings As XmlNodeList) As String

        Dim ProjectID As String = ""
        For Each building As XmlNode In Buildings
            If (building.Attributes("id").Value = BuildingID) Then
                ProjectID = building.Attributes("ProjectID").Value
            End If
        Next
        If (ProjectID = "") Then
            Return "-1"
        Else
            Return ProjectID
        End If


    End Function




    Private Function GetMaxAssetID() As Integer
        Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select counter from ipm_cat_counter", MyConnection)
        Dim DT1 As New DataTable("counter")
        MyCommand1.Fill(DT1)

        Dim MyCommand2 As New SqlCommand("update ipm_cat_counter set counter = counter + 1", MyConnection)
        MyCommand2.Connection.Open()
        MyCommand2.ExecuteNonQuery()
        MyCommand2.Connection.Close()

        Return DT1.Rows(0)("counter")
    End Function
    Private Function GetFloor(ByVal UnitNumber As String) As String
        'trim characters at front
        If (UnitNumber.Length = "5") Then
            UnitNumber.Substring(2, UnitNumber.Length - 2)
        End If
        If (UnitNumber.Length = "3") Then
            Return UnitNumber.Substring(0, 1)
        Else 'Assume a building is no taller than 99 floors and unit numbers is 2 digits
            Try
                Return UnitNumber.Substring(0, 2)
            Catch Ex As Exception
                Return "0"
            End Try
        End If
    End Function
    Private Function GetUnitsFolderID(ByVal ProjectID As String) As String
        ''Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        ''Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select category_id from ipm_asset_category where projectid = @projectid and name = @units and available = 'y'", MyConnection)

        ''MyCommand1.SelectCommand.Parameters.Add("@projectid", SqlDbType.NVarChar, 255)
        ''MyCommand1.SelectCommand.Parameters("@projectid").Value = ProjectID
        ''MyCommand1.SelectCommand.Parameters.Add("@units", SqlDbType.NVarChar, 255)
        ''MyCommand1.SelectCommand.Parameters("@units").Value = "Units"

        ''Dim DT1 As New DataTable("assets")

        ''MyCommand1.Fill(DT1)

        ''If (DT1.Rows.Count > 0) Then

        ''    Try
        ''        Return DT1.Rows(0)("category_id")
        ''    Catch ex As Exception
        ''        Return "-1"
        ''    End Try
        ''Else
        ''    'add units folder
        Return checkorCreateProjectfolder("0", "Units", ProjectID)


        ''Return "-1"
        ''End If

    End Function


    Public Shared Sub ExecuteTransaction(ByVal sqlstring As String)
        Dim ConnectionString As String = My.Settings.ConnString

        ConnectionString = "Provider=sqloledb;" + ConnectionString.Replace("Data Source", "Server").Replace("Initial Catalog", "Database").Replace("User Id", "Uid").Replace("Password", "Pwd")

        'Provider=sqloledb;Data Source=192.168.1.48;Initial Catalog=WDCEPDATA;User Id=sa;Password=;
        'Server=192.168.1.48;Database=IDAM_KETTLER;Uid=idam;Pwd=w3barchiv3s;

        Dim connection As New OleDbConnection(ConnectionString)
        Dim command As New OleDbCommand
        Dim transaction As OleDbTransaction

        ' Set the Connection to the new OleDbConnection.
        command.Connection = connection

        ' Open the connection and execute the transaction.
        Try
            connection.Open()

            ' Start a local transaction with ReadCommitted isolation level.
            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            ' Assign transaction object for a pending local transaction.
            command.Connection = connection
            command.Transaction = transaction

            ' Execute the commands.
            command.CommandText = sqlstring
            command.ExecuteNonQuery()

            ' Commit the transaction.
            transaction.Commit()
            Console.WriteLine("Executed.")

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Try to rollback the transaction
            Try
                transaction.Rollback()

            Catch
                ' Do nothing here; transaction is not active.
            End Try
        End Try
        ' The connection is automatically closed when the
        ' code exits the Using block.

    End Sub





    Function checkorCreateProjectfolder(ByVal parentcatid As String, ByVal sName As String, ByVal sProjectID As String, Optional ByVal bypassSec As Boolean = False) As String

        'check is exists
        Dim sProjectFolderID As String
        Try
            sProjectFolderID = GetDataTable("select category_id from ipm_asset_category where projectid = " & sProjectID & " and parent_cat_id = " & parentcatid & " and name = '" & sName.Trim.Replace("'", "''") & "' and available = 'Y'", My.Settings.ConnString).Rows(0)("category_id")
            Return sProjectFolderID
        Catch ex As Exception
            Try
                'create category
                Dim sql As String
                sql = "exec sp_createnewassetcategory_wparent 1," & parentcatid & ","
                sql += "'" & Replace(sName, "'", "''") & "',"
                sql += sProjectID & ","
                sql += "'Y',"
                sql += "'',"
                sql += "'1'"
                ExecuteTransaction(sql)
                'get ID of category just inserted
                Dim catID As String = GetDataTable("select max(category_id) maxid from ipm_asset_category", My.Settings.ConnString).Rows(0)("maxid")
                sProjectFolderID = catID
                'add security defaults
                If Not bypassSec Then
                    sql = "select * from ipm_project_security where projectid = " & sProjectID
                    Dim securityCatTable As DataTable = GetDataTable(sql, My.Settings.ConnString)
                    Dim srow As DataRow
                    For Each srow In securityCatTable.Rows
                        sql = "insert into ipm_category_security (category_id, security_id, type) values (" & catID & ",'" & srow("security_id") & "','" & srow("type") & "')"
                        ExecuteTransaction(sql)
                    Next
                End If

                Return sProjectFolderID
            Catch exx As Exception
                Return "0"
            End Try
        End Try


    End Function



    Private Function GetFloorFolderID(ByVal Floor As String, ByVal ProjectID As String) As String
        Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select category_id from ipm_asset_category where projectid = @projectid and name = @floor and available = 'y'", MyConnection)

        MyCommand1.SelectCommand.Parameters.Add("@projectid", SqlDbType.NVarChar, 255)
        MyCommand1.SelectCommand.Parameters("@projectid").Value = ProjectID
        MyCommand1.SelectCommand.Parameters.Add("@floor", SqlDbType.NVarChar, 255)
        MyCommand1.SelectCommand.Parameters("@floor").Value = Floor

        Dim DT1 As New DataTable("assets")

        MyCommand1.Fill(DT1)

        If (DT1.Rows.Count > 0) Then

            Try
                Return DT1.Rows(0)("category_id")
            Catch ex As Exception
                Return "-1"
            End Try
        Else
            Return "-1"
        End If

    End Function



    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '' ''For Each row In GetDataTable("select a.Item_Value modelid, b.Item_Value modelname from IPM_ASSET_FIELD_value a, IPM_ASSET_FIELD_VALUE b where a.Item_ID = 21550857 and b.item_id = 229806 and a.ASSET_ID = b.ASSET_ID and a.ASSET_ID in (select ASSET_ID from IPM_ASSET where ProjectID in ((SELECT     ProjectID  FROM          IPM_PROJECT WHERE      (Category_ID = 452624) AND (ProjectID = 452692 OR ProjectID = 452691 OR ProjectID = 452690 OR ProjectID = 452689 OR ProjectID = 452688 OR ProjectID = 452687 OR ProjectID = 452686 OR ProjectID = 452685 OR ProjectID = 452684 OR ProjectID = 452683 OR ProjectID = 452685))))group by a.Item_Value , b.item_value", My.Settings.ConnString).Rows
        '' ''    checkcreatemodel(row("modelid"), row("modelname"), 453505)
        '' ''Next

        '' ''Exit Sub

        ExecuteUpdate()
        Me.Close()
        Me.Dispose()
    End Sub

    Function checkcreatemodel(ByVal modelid, ByVal modelname, ByVal projectid)
        'get model name
        Dim sModelName As String = modelname

        'check to see if exists
        If GetDataTable("select asset_id from ipm_asset where projectid = " & projectid & " and name = '" & sModelName.Trim.Replace("'", "''") & "' and available = 'Y'", My.Settings.ConnString).rows.count = 0 Then

            'not found
            'add
            Dim sql As String
            sql = "insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,description,media_type,projectid,category_id,upload_date,update_date,oid,version_id) values (@name,@projectid,'1',@insertID,1,1,@name,@description,@mediatype,@projectid,@categoryid,getdate(),getdate(),@oid,0)"
            Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
            Dim MyCommand2 As New SqlCommand("", MyConnection)

            MyCommand2.Parameters.Add("@name", SqlDbType.NVarChar, 255)
            MyCommand2.Parameters.Add("@description", SqlDbType.NVarChar, 255)
            MyCommand2.Parameters.Add("@projectid", SqlDbType.NVarChar, 255)
            MyCommand2.Parameters.Add("@mediatype", SqlDbType.NVarChar, 255)
            MyCommand2.Parameters.Add("@categoryid", SqlDbType.NVarChar, 255)
            MyCommand2.Parameters.Add("@insertid", SqlDbType.NVarChar, 255)
            MyCommand2.Parameters.Add("@oid", SqlDbType.NVarChar, 255)
            Dim i As Integer = GetMaxAssetID()

            MyCommand2.CommandText = sql
            MyCommand2.Parameters("@name").Value = sModelName.Trim
            MyCommand2.Parameters("@description").Value = modelid.Trim
            MyCommand2.Parameters("@projectid").Value = projectid
            MyCommand2.Parameters("@mediatype").Value = "10"
            MyCommand2.Parameters("@categoryid").Value = "0"
            MyCommand2.Parameters("@insertid").Value = i
            MyCommand2.Parameters("@oid").Value = i

            MyCommand2.Connection.Open()
            MyCommand2.ExecuteNonQuery()
            MyCommand2.Connection.Close()

        End If

    End Function


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim ConfigDS As New DataSet

        ConfigDS.ReadXml("Config.xml")
        Dim ConfigXML As New XmlDocument
        Dim PropertyNodes As XmlNodeList
        ConfigXML.LoadXml(ConfigDS.GetXml)

        PropertyNodes = ConfigXML.SelectNodes("/KettlerConfiguration/Properties/Property")

        For Each propertynode As XmlNode In PropertyNodes
            Dim propertyFeed As String = propertynode.Attributes("FeedLocation").Value
            Dim DS As New DataSet
            DS.ReadXml(propertyFeed)

            UpdateCurrentUnitsModelNamePLUSCREATE(DS)


            Exit For
        Next



    End Sub




    Private Sub UpdateCurrentUnitsModelNamePLUSCREATE(ByVal DS As DataSet)

        For Each modelRows In DS.Tables("Model").Rows

            Dim floorPlanID = modelRows("FloorplanID")
            Dim modelID = modelRows("ID")

            Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("update ipm_asset set description = @floorplanid where description = @modelid", MyConnection)

            MyCommand1.SelectCommand.Parameters.Add("@modelid", SqlDbType.NVarChar, 255)
            MyCommand1.SelectCommand.Parameters("@modelid").Value = modelID

            MyCommand1.SelectCommand.Parameters.Add("@floorplanid", SqlDbType.NVarChar, 255)
            MyCommand1.SelectCommand.Parameters("@floorplanid").Value = floorPlanID

            Dim DT1 As New DataTable("assets")

            MyCommand1.Fill(DT1)

            Log.Debug("Updated " & modelID & " to " & floorPlanID)
            Console.WriteLine("Updated " & modelID & " to " & floorPlanID)
        Next

    End Sub

    Function getmodelassetid(ByVal mn As String) As String
        Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select asset_id from ipm_asset where name = @mn and available = 'Y' and projectid = 453505 and category_id = 0 ", MyConnection)

        MyCommand1.SelectCommand.Parameters.Add("@mn", SqlDbType.NVarChar, 255)
        MyCommand1.SelectCommand.Parameters("@mn").Value = mn

        Dim DT1 As New DataTable("assets")

        MyCommand1.Fill(DT1)

        If (DT1.Rows.Count > 0) Then
            Return DT1.Rows(0)("asset_id")
        Else
            'add model

            Dim sql As String
            sql = "insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date,oid) values (@name,@projectid,'1',@insertID,1,1,@name,@mediatype,@projectid,@categoryid,getdate(),getdate(),@oid)"

            Dim MyCommand2 As New SqlCommand("", MyConnection)

            MyCommand2.Parameters.Add("@name", SqlDbType.NVarChar, 255)
            MyCommand2.Parameters.Add("@projectid", SqlDbType.NVarChar, 255)
            MyCommand2.Parameters.Add("@mediatype", SqlDbType.NVarChar, 255)
            MyCommand2.Parameters.Add("@categoryid", SqlDbType.NVarChar, 255)
            MyCommand2.Parameters.Add("@insertid", SqlDbType.NVarChar, 255)
            MyCommand2.Parameters.Add("@oid", SqlDbType.NVarChar, 255)
            Dim i As Integer = GetMaxAssetID()

            MyCommand2.CommandText = sql
            MyCommand2.Parameters("@name").Value = mn.Trim
            MyCommand2.Parameters("@projectid").Value = "453505"
            MyCommand2.Parameters("@mediatype").Value = "10"
            MyCommand2.Parameters("@categoryid").Value = "0"
            MyCommand2.Parameters("@insertid").Value = i
            MyCommand2.Parameters("@oid").Value = i

            MyCommand2.Connection.Open()
            MyCommand2.ExecuteNonQuery()
            MyCommand2.Connection.Close()

            Return i
        End If
    End Function
    Sub SendError(ByVal sError As String, Optional ByVal object_type As String = "", Optional ByVal object_id As String = "")

        Dim sql As String
        sError = sError.ToString.Replace("'", "''")
        Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        sql = "insert into ipm_error (date,output,object_type,object_id) values (getdate(),'" & sError & "','" & object_type & "','" & object_id & "')"
        Dim mycommand1 As New SqlCommand(sql, MyConnection)

        Try

            mycommand1.Connection.Open()
            mycommand1.ExecuteNonQuery()
            mycommand1.Connection.Close()
            sendMail(sError, object_type, object_id)

        Catch ex As Exception

        End Try



    End Sub
    Sub SendSuccess(ByVal sMessage)

        Try

            sendMail(sMessage)

        Catch ex As Exception

        End Try



    End Sub
    Sub sendMail(ByVal sMessage As String, Optional ByVal object_type As String = "", Optional ByVal object_id As String = "")

        Dim mail As MailMessage = New MailMessage

        Dim smtp As New SmtpClient()

        smtp.Host = "mail.ifmm.com"
        smtp.UseDefaultCredentials = True
        smtp.Port = 25

        mail.From = New MailAddress(My.Settings.FromEmail, "Kettler Alert")
        For Each maddress As String In My.Settings.MonitorEmail.Split(";")
            mail.To.Add(maddress)
        Next
        mail.Body = sMessage & vbCrLf & object_type & vbCrLf & object_id
        mail.Subject = "Kettler Nightly Update Alert"
        smtp.Send(mail)
    End Sub

End Class
