$(document).ready(function() {

if($('.modal').html()) {

var modalCSS ='\
<style type="text/css">\
#modalImg{\
position: fixed;\
margin: auto;\
top: 5%;\
left: 15%;\
border: 8px solid #000;\
padding: 0;\
display: none;\
max-width: 1000px;\
overflow: hidden;\
z-index: 112;\
}\
#modalImg img {\
padding: 0;\
margin: 0 0 0 0;\
display: block;\
float: left;\
z-index: 113;\
max-width: 1000px;\
max-height: 750px;\
}\
#modalImg a {\
position: absolute;\
display: block;\
right: 0;\
width: 30px;\
height: 30px;\
background: transparent url(assets/images/closebox.png) no-repeat scroll 100% 2px;\
cursor: pointer;\
}\
#modalImg a:hover {\
background-position: 100% -28px;\
}\
#modalOverlay {\
position: fixed;\
margin: 0;\
top: 0;\
left: 0;\
width: 100%;\
height: 100%;\
padding: 0;\
display: none;\
background: #646464;\
z-index: 111;\
}\
#modalLoading {\
left: 48%;\
position: fixed;\
z-index: 114;\
top: 33%;\
width: 32px;\
height: 32px;\
padding: 20px;\
border: 5px solid #111;\
background: #fff;\
display: none;\
}\
</style>';

 $('head').append(modalCSS);
 $('body').append('<div id="modalImg"></div><div id="modalOverlay"></div><div id="modalLoading"><img src="assets/images/loading.gif" alt="Loading" title="Loading" /></div>');
 $("a.modal").click(function () { 
	$("#modalImg").html('<img src="'+ this.href +'" alt="" onload="modalShow(this)" /><a></a>');
	$("#modalImg").css("opacity", "0");
	$("#modalImg").css("display", "block");

	$("#modalLoading").css("opacity", "0");
	$("#modalLoading").css("display", "block");
	$("#modalLoading").animate({"opacity": 1},1100);

	$("#modalOverlay").css("opacity", "0");
	$("#modalOverlay").css("display", "block");
	$("#modalOverlay").animate({"opacity": .8});
	return false;
 });
 $("#modalImg").click(function () { 
	$("#modalImg").css("display", "none");
	$("#modalOverlay").css("display", "none");
	$("#modalImg").removeAttr('style');
	$("#modalImg").html('');
	return false;
 });

}

});

function modalShow(e) {
 var x = ($('body').width() / 2 ) - (e.width/2);
 $('#modalImg').css('left',x+'px');
 $('#modalImg').animate({"opacity": 1});
 $("#modalLoading").css("display", "none");
}
