$(document).ready(function () {
 if($('#banner').html() && $('#bannerText').html()) {

    $('#bannerText p').append('<span class="bannerPrev"></span><span class="bannerNext"></span>');
    $('#bannerText p strong').css('display','none');
    $('#bannerText p strong').first().css('display','block');

   if(false /*@cc_on || @_jscript_version < 99 @*/) { 
     $('#bannerText p strong').css('opacity','.99');
     $('#bannerText p em').css('opacity','.99');
   }

    $('#bannerText p em').css('display','none');
    $('#bannerText p em').first().css('display','block');

    var bodyWidth = $('body').width();
    var bannerOffset = 0;
    if (bodyWidth < 1600) {
        bannerOffset = Math.round((1600 - bodyWidth) / 2);
    }

    $('#banner p').css('marginLeft', '-' + String(bannerOffset) + 'px');

    var movingBanner = null;


    $('.bannerNext').click(function () {

    var bodyWidth = $('body').width();
    var bannerOffset = 0;
    if (bodyWidth < 1600) {
        bannerOffset = Math.round((1600 - bodyWidth) / 2);
    }

        if (!movingBanner) {
            movingBanner = 1;
            var bannerFirst = $('#banner p img').first();
            $('#banner p').animate({
                marginLeft: '-' + (1600 + bannerOffset) + 'px'
            }, 600, function () {
                $("#banner p").append(bannerFirst);
                $("#banner p").css('marginLeft', '-' + bannerOffset + 'px');
                movingBanner = null;
            });

            $('#bannerText p strong').first().fadeOut(300);
            $('#bannerText p em').first().fadeOut(300, function () {
                $("#bannerText p").css('display', 'none');
                $("#bannerText").append($('#bannerText p').first());
                $("#bannerText p").first().css('display', 'block');
                $('#bannerText p strong').first().fadeIn(300);
                $('#bannerText p em').first().fadeIn(300);
            });
        }
    });

    $('.bannerPrev').click(function () {

    var bodyWidth = $('body').width();
    var bannerOffset = 0;
    if (bodyWidth < 1600) {
        bannerOffset = Math.round((1600 - bodyWidth) / 2);
    }

        if (!movingBanner) {
            movingBanner = 1;
            var bannerLast = $('#banner p img').last();
            $("#banner p").prepend(bannerLast);
            $("#banner p").css('marginLeft', '-' + (1600 + bannerOffset) + 'px');
            $('#banner p').animate({
                marginLeft: '-' + bannerOffset + 'px'
            }, 600, function () {
                movingBanner = null;
            });

            $('#bannerText p strong').first().fadeOut(300);
            $('#bannerText p em').first().fadeOut(300, function () {
                $("#bannerText p").css('display', 'none');
                $("#bannerText").prepend($('#bannerText p').last());
                $("#bannerText p").first().css('display', 'block');
                $('#bannerText p strong').first().fadeIn(300);
                $('#bannerText p em').first().fadeIn(300);
            });
        }
    });

 }
});