<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>About Us: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="ThreeColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="#" title="Firm Profile" class="selected">Firm Profile</a>
						</p>
						<p>
							<a href="clients.aspx" title="Clients">Clients</a>
						</p>
						<p>
							<a href="history.aspx" title="History">History</a>
						</p>
						<p>
							<a href="management.aspx" title="Management">Management</a>
						</p>
						<p>
							<a href="sustainability.aspx" title="Sustainability">Sustainability</a>
						</p>
						<p>
							<a href="community.aspx" title="Community">Community</a>
						</p>
					</div>
				</div>
				<div class="col2">
					<h1>Firm Profile</h1>
					<p>
						 McKissack & McKissack is a woman/minority-owned organization specializing in architecture & interiors, program & construction management, planning & facilities management, environmental engineering and transportation.  Founded by Deryl McKissack, PE, PMP in 1990, it has expanded nationwide with offices in Washington, DC, Chicago, Baltimore, Los Angeles, Miami, Atlanta and Orlando.  McKissack & McKissack employs 150 professionals and is certified by many states and municipalities as a minority/woman-owned business.
					</p>
					<p>
						As a leading program management organization, McKissack & McKissack has managed $15 billion in construction and is ranked by Engineering News-Record as one of the top 50 Program Management and top 100 Construction Management For-Fee firms in the United States.  Moreover, we are ranked by the Washington Business Journal as one of the top 25 Environmental Consultant firms in the Washington Metropolitan area.</p>
					<p>
						 McKissack & McKissack’s diversity of services allows us to successfully manage clients’ projects from conception through completion, whether the project is new construction, renovation, reconstruction, environmental design or transportation.  Additionally, our services are provided to a diverse and demanding client base, including federal, state and local governments; developers; corporations; healthcare institutions; colleges and universities; K-12 schools; and municipal entities.</p>
					<p>
						 McKissack & McKissack is proud of its ability to take on difficult and complex assignments that require the skills of many disciplines.  In each assignment, McKissack & McKissack professionals strive to be creative yet practical, to provide outstanding technical expertise and to be especially responsive to client needs, budgets and schedules.
					</p>
				</div>
				<div class="col3">
					<div class="pad75">
					</div>
					<h1>Vision</h1>
					<p>
						 Becoming &#147;national leaders&#148; in our core capabilities.
					</p>
					<p>
						 Being dedicated to &#147;nurturing our talent&#148; to be exceptional in out areas of expertise.
					</p>
					<p>
						 &#147;Using our resources wisely&#148; to expand into new markets that strategically align with our clients.
					</p>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a><em>&#169; 2011 Copyright McKissack &#38; McKissack</em></address>
				<p>
					<a href="#">Document Management</a> &#8195;|&#8195; <a href="#">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
	<div id="header">
		<div>
			<p id="logoType">
				<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
			</p>
			<p id="logoIcon">
				<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
			</p>
			<p id="menu">
				<a href="./about.aspx" class="selected">About Us</a>
				<a href="./services_overview.aspx">Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./contracts.aspx">Contracts</a>
				<a href="./culture.aspx">Careers</a>
				<a href="./contact.aspx">Contact</a>
			</p>
		</div>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/dynamic/bannerAbout.png'); height: 382px"></div>
</body>
</html>