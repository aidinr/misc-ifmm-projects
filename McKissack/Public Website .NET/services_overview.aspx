<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Service Overview: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="ThreeColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="./" class="selected">Overview</a>
						</p>
						<p>
							<a href="./ai.aspx">Architecture &#38; Interiors</a>
						</p>
						<p>
							<a href="./construction_management.aspx">Program &#38; Construction Management</a>
						</p>
						<p>
							<a href="./planning.aspx">Planning &#38; Facilities Management</a>
						</p>
						<p>
							<a href="./environmental.aspx">Environmental Engineering</a>
						</p>
						<p>
							<a href="./transportation.aspx">Transportation</a>
						</p>
					</div>
				</div>
				<div class="col2">
					<h1>Overview</h1>
					<p>
						 McKissack & McKissack specializes in architecture & interiors, program & construction management, planning & facilities management, environmental engineering and transportation.  Underpinned by our seasoned personnel, each department has a proven history of effectively managing the resources and relationships necessary to achieve a client’s desired outcome.  McKissack & McKissack has established itself as a leader in all of its disciplines and has a successful track record of delivering superior results that meet or exceed client objectives. 
					</p>
					</div>
				<div class="col3">
					<div class="pad75">
					</div>
					<p>
						 McKissack&#146;s effort to complete the project on-time and within our budget allowed us to concentrate on the development lifecycle to maximize our product.
					</p>
					<address><strong>Jeff Howe</strong><br />
					 Cheif Executive Officer<br />
					 ABC Development </address>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a><em>&#169; 2011 Copyright McKissack &#38; McKissack</em></address>
				<p>
					<a href="#">Document Management</a> &#8195;|&#8195; <a href="#">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
				<a href="./about.aspx">About Us</a>
				<a href="./services_overview.aspx" class="selected">Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./contracts.aspx">Contracts</a>
				<a href="./culture.aspx">Careers</a>
				<a href="./contact.aspx">Contact</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/dynamic/banner2.png'); height: 382px"></div>
</body>
</html>