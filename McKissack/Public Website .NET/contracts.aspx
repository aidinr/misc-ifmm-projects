<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Culture: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="ThreeColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="./" class="selected">GSA Contracts</a>
						</p>
						<p>
							<a href="./">Seaport-E</a>
						</p>
						<p>
							<a href="./">Team Members</a>
						</p>
						<p>
							<a href="./">Services</a>
						</p>
						<p>
							<a href="./">References</a>
						</p>
						<p>
							<a href="./">Quality Assurance</a>
						</p>
						<p>
							<a href="./">Contact</a>
						</p>

					</div>
				</div>
				<div class="col2">
					<h1>GSA Contracts</h1>
					<p>
						 The culture of McKissack & McKissack is guided by a passion for excellence.  Along with her management team, Deryl McKissack has created an organization that is customer focused, financially strong and prepared to build on its impressive record of professional triumphs and community service.  McKissack & McKissack offers a well-balanced corporate culture that embraces diversity, nurtures professional growth and rewards hard work.  Moreover, our compensation packages are thorough and include benefits to address both our employees' work and personal lives. 
					</p>
					<p>
						<a href="jobs.aspx"><strong>VIEW JOB OPENINGS</strong></a>
					</p>
				</div>
				<div class="col3">
					<div class="pad75">
					</div>
					<p>"We understand that reputations are based not on what you promise but on what you deliver. Therefore we treat every project, large or small, with the same passion for excellence, because in the end we know that our legacy will be based on the quality of the work we do."	</p>
					<address><strong>Deryl McKissack, PE, PMP </strong><br />
					 President and Chief Executive Officer<br />
					 </address>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a><em>&#169; 2011 Copyright McKissack &#38; McKissack</em></address>
				<p>
					<a href="#">Document Management</a> &#8195;|&#8195; <a href="#">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="./about.aspx">About Us</a>
				<a href="./services_overview.aspx">Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./contracts.aspx" class="selected">Contracts</a>
				<a href="./culture.aspx" >Careers</a>
				<a href="./contact.aspx">Contact</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/dynamic/bannerAbout.png'); height: 382px"></div>
</body>
</html>