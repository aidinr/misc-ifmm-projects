<%@ Page Language="VB" AutoEventWireup="false" CodeFile="transportation.aspx.vb" Inherits="_transportation" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Architecture &#38; Interiors: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="TwoColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="./services_overview.aspx" >Overview</a>
						</p>
						<p>
							<a href="./ai.aspx" >Architecture &#38; Interiors</a>
						</p>
						<p>
							<a href="./construction_management.aspx" >Program &#38; Construction Management</a>
						</p>
						<p>
							<a href="./planning.aspx" >Planning &#38; Facilities Management</a>
						</p>
						<p>
							<a href="./environmental.aspx" >Environmental Engineering</a>
						</p>
						<p>
							<a href="./transportation.aspx" class="selected">Transportation</a>
						</p>
					</div>
				</div>
				<div class="col2 wide">
					<h1>Transportation</h1>
					<p>
						 McKissack & McKissack’s highly experienced staff of engineers and specialists have successfully managed and implemented transportation project solutions for a wide range of clients.  Our experience covers a broad spectrum—from managing road improvements to upgrades for an international airport.  McKissack & McKissack provides the critical expertise to formulate and evaluate an array of project management, system management, land use management and pricing options.  We ensure that our focused transportation project management, through all phases of concept design and construction, will result in the most effective and efficient method of implementing a project.  On each assignment, the firm’s knowledgeable project team precisely follows local, state and federal guidelines in the construction and completion of major capital improvement initiatives.  Services include:
					</p>
					<p>
						 Engineering :: Program Management :: Construction Management :: Design/Build :: Traffic Engineering :: On-Site Document Control :: Scheduling :: Transportation Planning :: Environmental Planning :: Traffic Operations :: Urban Design :: Public Participation :: Partnering :: Research
					</p>
					<h2 class="dotted">Featured Projects <a href="search_results.aspx?keyword=&tid=&sid=2400162">View All</a></h2>
					<div class="featured">
					
					<asp:Repeater ID="rptFeatured" runat="server" OnItemDataBound="rptFeatured_ItemDataBound">
					<ItemTemplate>
					    <asp:literal ID="ltrlclasstag" runat="server"></asp:literal>
						<a href="project.aspx?pid=<%#Container.DataItem("projectid")%>">
						<img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("projectid")%>&type=project&crop=1&size=1&height=162&width=229" alt="<%#Container.DataItem("name")%>" title="<%#Container.DataItem("name").trim%>"  />
						<strong><%#Container.DataItem("name")%></strong><span></span>
						</a>
					</p>
					</ItemTemplate>
					</asp:Repeater>
						<div class="c"></div>
					</div>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a><em>&#169; 2011 Copyright McKissack &#38; McKissack</em></address>
				<p>
					<a href="#">Document Management</a> &#8195;|&#8195; <a href="#">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
				<a href="./about.aspx">About Us</a>
				<a href="./services_overview.aspx"  class="selected">Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./contracts.aspx">Contracts</a>
				<a href="./culture.aspx">Careers</a>
				<a href="./contact.aspx">Contact</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/dynamic/bannerMLK.png'); height: 382px"></div>
</body>
</html>