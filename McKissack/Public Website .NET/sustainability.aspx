<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>About Us: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 170px;">
			<div class="ThreeColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="about.aspx" title="Firm Profile">Firm Profile</a>
						</p>
						<p>
							<a href="clients.aspx" title="Clients">Clients</a>
						</p>
						<p>
							<a href="history.aspx" title="History">History</a>
						</p>
						<p>
							<a href="management.aspx" title="Management">Management</a>
						</p>
						<p>
							<a href="sustainability.aspx" title="Sustainability" class="selected">Sustainability</a>
						</p>
						<p>
							<a href="community.aspx" title="Community">Community</a>
						</p>
					</div>
				</div>
				<div class="col2">
					<h1>Sustainability</h1>
					<p>
						 McKissack & McKissack views the goals of efficiency and sustainable design as interconnected with the firm’s dedication to construction excellence.  We respond to environmental challenges with new approaches, materials and methods that result in energy conservation and efficiency.  With many strategies achieved at no added cost, our full-discipline approach includes concepts such as adaptive reuse, waste reduction, value engineering and use of recovered materials.  Moreover, our professionals are well versed in the design and project management of sustainable projects, and our Leadership in Energy and Environmental Design (LEED) experience covers a wide range of projects, including commercial, public assembly, correctional, laboratory and educational, among others.  To achieve sustainability goals, McKissack & McKissack provides regular in-house education and actively encourages staff members to become LEED Accredited Professionals.  We urge our personnel to attend lectures, seminars and courses that explain new environmentally conscious approaches and to incorporate these approaches in our daily work.  McKissack & McKissack is a member of the U.S. Green Building Council.
					</p>
					
				</div>
				<div class="col3">
					<div class="pad75">
					</div>
					<h1>Vision</h1>
					<p>
						 Becoming &#147;national leaders&#148; in our core capabilities.
					</p>
					<p>
						 Being dedicated to &#147;nurturing our talent&#148; to be exceptional in out areas of expertise.
					</p>
					<p>
						 &#147;Using our resources wisely&#148; to expand into new markets that strategically align with our clients.
					</p>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a><em>&#169; 2011 Copyright McKissack &#38; McKissack</em></address>
				<p>
					<a href="#">Document Management</a> &#8195;|&#8195; <a href="#">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
	<div id="header">
		<div>
			<p id="logoType">
				<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
			</p>
			<p id="logoIcon">
				<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
			</p>
			<p id="menu">
				<a href="./about.aspx" class="selected">About Us</a>
				<a href="./services_overview.aspx">Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./contracts.aspx">Contracts</a>
				<a href="./culture.aspx">Careers</a>
				<a href="./contact.aspx">Contact</a>
			</p>
		</div>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/dynamic/bannerMLK.png'); height: 170px"></div>
</body>
</html>