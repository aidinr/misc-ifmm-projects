﻿Imports System.Data.SqlClient
Imports System.Data


Partial Class _Search_Results
    Inherits System.Web.UI.Page


    Protected Sub _Search_Results_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim _sid As integer = 0
            If IsNumeric(Request.QueryString("sid")) Then
                _sid = Request.QueryString("sid")
            End If
            Dim _tid As integer = 0
            If IsNumeric(Request.QueryString("tid")) Then
                _tid = Request.QueryString("tid")
            End If
            Dim _keyword As String = ""
            If Not Request.QueryString("keyword") Is Nothing Then
                _keyword = Request.QueryString("keyword")
            End If
            Dim _sidname, _tidname, _sidid, _tidid As String


            'get servicesf
            Dim DT2 As New DataTable("Services")
            DT2 = mmfunctions.GetDataTable("select * from ipm_discipline where keyuse = 1")

            For Each row As DataRow In DT2.Rows
                If row("keyid") = _sid Then
                    literalServices.Text += "<h3><a href=""search_results.aspx?keyword=" & _keyword & "&tid=" & Request.QueryString("tid") & "&sid=" & row("keyid") & """ class=""selected"">" & row("keyname") & "</a></h3>"
                    _sidname = row("keyname")
                    _sidid = row("keyid")
                Else
                    literalServices.Text += "<h3><a href=""search_results.aspx?keyword=" & _keyword & "&tid=" & Request.QueryString("tid") & "&sid=" & row("keyid") & """>" & row("keyname") & "</a></h3>"
                End If
            Next
            

            'get breadcrumbs
            If _keyword <> "" Then
                ltrlbreadcrumbs.Text += "<a href=""search_results.aspx?keyword=" & _keyword & """>" & _keyword & "</a>"
            End If
            If Not _sidname Is Nothing Then
                If ltrlbreadcrumbs.Text <> "" Then
                    ltrlbreadcrumbs.Text += " / "
                End If
                ltrlbreadcrumbs.Text += "<a href=""search_results.aspx?sid=" & _sidid & """>" & _sidname & "</a>"
            End If
            If Not _tidname Is Nothing Then
                If ltrlbreadcrumbs.Text <> "" Then
                    ltrlbreadcrumbs.Text += " / "
                End If
                ltrlbreadcrumbs.Text += "<a href=""search_results.aspx?tid=" & _tidid & """>" & _tidname & "</a>"
            End If



         

            'rptGrid.DataSource = DT1
            'rptGrid.DataBind()

            'get keywords

            Dim DT3 As New DataTable("Type")
            DT3 = mmfunctions.GetDataTable("select * from ipm_office where keyuse = 1 and keyid in (select officeid keyid from ipm_project_office a, ipm_project b where a.projectid = b.projectid and b.publish = 1 and available = 'Y') order by keyname")

            For Each row As DataRow In DT3.Rows
                If row("keyid") = _tid Then
                    literalType.Text += "<h3><a href=""search_results.aspx?keyword=" & _keyword & "&sid=" & Request.QueryString("sid") & "&tid=" & row("keyid") & """ class=""selected"">" & row("keyname") & "</a></h3>"
                    _tidname = row("keyname")
                    _tidid = row("keyid")
                Else
                    literalType.Text += "<h3><a href=""search_results.aspx?keyword=" & _keyword & "&sid=" & Request.QueryString("sid") & "&tid=" & row("keyid") & """>" & row("keyname") & "</a></h3>"
                End If
            Next
            'get grid results
            Dim DT1 As New DataTable("Projects")
            DT1 = mmfunctions.ResultsDataTable(_keyword, _tidid, _sidid)
            Dim DV As DataView = New DataView(DT1)


            'Add Sorting
            If Session("searchSort") = "" Then
                Session("searchSort") = "name"
            Else
                If Request.QueryString("sort") Is Nothing Then
                    Session("searchSort") = "name"
                Else
                    Session("searchSort") = Request.QueryString("sort")
                End If

            End If
            If Session("searchSortby") = "" Then
                Session("searchSortby") = "asc"
            Else
                If Not Request.QueryString("sort") Is Nothing Then
                    If Session("searchSortby") = "asc" Then
                        Session("searchSortby") = "desc"
                    Else
                        Session("searchSortby") = "asc"
                    End If
                End If

                End If
                DV.Sort = Session("searchSort") & " " & Session("searchSortby")

                'cache dv
                Session("DV") = DV
                If DV.Table.Rows.Count = 0 Then
                    ltrlnoresults.Text = "No projects available."
                End If
                'page dv
                Dim pgDataSc As PagedDataSource = New PagedDataSource
                pgDataSc.DataSource = DV
                pgDataSc.AllowPaging = True
                pgDataSc.PageSize = CInt(System.Configuration.ConfigurationManager.AppSettings("SearchPageSize"))
                If Request.QueryString("page") Is Nothing Or Request.QueryString("page") = "0" Then
                    pgDataSc.CurrentPageIndex = 0
                Else
                    pgDataSc.CurrentPageIndex = Request.QueryString("page") - 1
                End If

                If pgDataSc.PageCount <= pgDataSc.CurrentPageIndex Then
                    pgDataSc.CurrentPageIndex = pgDataSc.PageCount - 1
                End If

                rptGrid.DataSource = pgDataSc
                rptGrid.DataBind()

                Dim pageCount As Integer = pgDataSc.PageCount


                Dim pagingString As String = ""

                '<a href="#" class="prev">Previous</a><span><em>P</em>age</span>1<span>of <strong>15</strong></span><a href="#" class="next">Next</a>


                If pageCount > 0 Then
                    For i As Integer = 0 To pageCount - 1
                        If (i = pgDataSc.CurrentPageIndex) Then
                            pagingString = pagingString & "<div class=""pageitemhighlight""><b>" & (i + 1) & "</b> </div>"
                        Else
                            pagingString = pagingString & "<div class=""pageitem""><a href=""javascript:switchPage(" & i & ");"">" & (i + 1) & "</a></div> "
                        End If

                    Next
                    pagingString = pagingString & "<br /><br />"
                End If
                pagingString = "<a href=""search_results.aspx?keyword=" & _keyword & "&sid=" & Request.QueryString("sid") & "&tid=" & Request.QueryString("tid") & "&page=" & pgDataSc.CurrentPageIndex & """ class=""prev"">Previous</a><span>Page<strong>" & pgDataSc.CurrentPageIndex + 1 & "</strong> of <strong>" & pageCount & "</strong></span><a href=""search_results.aspx?keyword=" & _keyword & "&sid=" & Request.QueryString("sid") & "&tid=" & Request.QueryString("tid") & "&page=" & pgDataSc.CurrentPageIndex + 2 & """ class=""next"">Next</a>"
                literalPaging.Text = pagingString
                'literalPagingBottom.Text = pagingString




        Catch ex As Exception

        End Try


    End Sub

    Protected Sub rptGridDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptGrid.ItemDataBound
        'get location info
        Dim ltrlType As Literal = e.Item.FindControl("ltrlType")
        Dim sql As String = "select top 1 keyname from ipm_discipline a, ipm_project_discipline b where a.keyid = b.keyid and b.projectid = " & e.Item.DataItem.row.itemarray(0)
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)
        ltrlType.Text = ""
        If DT1.Rows.Count > 0 Then
            For Each row As DataRow In DT1.Rows
                ltrlType.Text += row("keyname") & " "
            Next
        Else
            ltrlType.Text = "N/A"
        End If


    End Sub
End Class
