<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Careers: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="ThreeColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="./culture.aspx" >Culture</a>
						</p>
						<p>
							<a href="./jobs.aspx">Job Openings</a>
						</p>
						<p>
							<a href="./benefits.aspx">Benefits</a>
						</p>
						<p>
							<a href="./student_internships.aspx" class="selected">Student Internships</a>
						</p>
					</div>
				</div>
				<div class="col2">
					<h1>Student Internships</h1>
					<p>
						 McKissack & McKissack offers summer internships in its Washington, DC and Chicago offices for high school and college students interested in the architecture and engineering fields.  Participants have the opportunity to not only gain valuable work experience with a distinguished firm, but also learn firsthand about the daily responsibilities associated with the industry.  Additionally, students have the chance to garner real world work experience from various projects throughout the summer, both in the office and on-site.  These internships are paid and last roughly 10 weeks.  McKissack & McKissack looks to expand these enriching opportunities to their other offices in the near future. 
					</p>
					</div>
				<div class="col3">
					<div class="pad75">
					</div>
					<p>"Through patience and perseverance, great knowledge can be obtained.  This is exactly the environment that is provided at McKissack & McKissack.  I know I will carry the knowledge gained here for the rest of my career." </p>
					<address><strong>Rose Lucas, Intern</strong><br />
					 </address>
					 
					 <p>“Working here at McKissack & McKissack has allowed me to enrich my study in the field of architecture, in ways I would never have been able to otherwise.” </p>
					<address><strong>Assia Belguedj, Intern</strong><br />
					 </address>

				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a><em>&#169; 2011 Copyright McKissack &#38; McKissack</em></address>
				<p>
					<a href="#">Document Management</a> &#8195;|&#8195; <a href="#">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="./about.aspx">About Us</a>
				<a href="./services_overview.aspx">Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./contracts.aspx">Contracts</a>
				<a href="./culture.aspx" class="selected">Careers</a>
				<a href="./contact.aspx">Contact</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/dynamic/bannerAbout.png'); height: 382px"></div>
</body>
</html>