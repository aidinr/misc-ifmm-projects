﻿Imports System.Data.SqlClient
Imports System.Data


Partial Class _jobs
    Inherits System.Web.UI.Page


    Protected Sub _jobs_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim RepeaterJobs As Repeater = FindControl("RepeaterJobs")
        Dim sql As String = "select * from ipm_jobs where show = 1 and post_date <= getdate() and pull_date >= getdate()"
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)
        RepeaterJobs.DataSource = DT1
        RepeaterJobs.DataBind()
        If DT1.Rows.Count = 0 Then
            ltrlnojobs.Text = "<br><Br><Br>There are no available jobs currently advertised."
        End If

    End Sub
End Class
