<%@ Page Language="VB" AutoEventWireup="false" CodeFile="history.aspx.vb" Inherits="_HistoryTimeline" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>History: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/jquery.carousel.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
											


<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 170px;">
			<div class="TwoColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="about.aspx" title="Firm Profile">Firm Profile</a>
						</p>
						<p>
							<a href="clients.aspx" title="Clients">Clients</a>
						</p>
						<p>
							<a href="history.aspx" title="History" class="selected">History</a>
						</p>
						<p>
							<a href="management.aspx" title="Management">Management</a>
						</p>
						<p>
							<a href="sustainability.aspx" title="Sustainability">Sustainability</a>
						</p>
						<p>
							<a href="community.aspx" title="Community">Community</a>
						</p>
					</div>
				</div>
				<div class="col2">
					<h1>History</h1>
					<p>
						 McKissack & McKissack was founded by Deryl McKissack in 1990.  When Ms. McKissack established her company, she was the fifth generation of her family to carry on the building tradition.  McKissack & McKissack is an outgrowth of the oldest minority-owned architecture/engineering firm in the United States.  Its roots go back to before the Civil War, when a slave named Moses McKissack learned the building trade from his overseer.  It was his grandson, Moses III, who launched the first McKissack & McKissack in Nashville, Tennessee.  The year was 1905.  
					</p>
					<h2>Company Timeline</h2>
					<div id="companyTimeline">
						<div id="timeline">
							<div class="events">
								<ul>
								
								
<asp:Repeater ID="RepeaterTimeline" runat="server" >
<ItemTemplate>
<li>
<strong><%#Container.DataItem("name")%></strong>
<span><img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("asset_id")%>&type=asset&crop=<%#Container.DataItem("crop")%>&size=1&height=107&width=170" alt="<%#Container.DataItem("name")%>" /></span>
<em><%#Container.DataItem("description")%></em>
</li>
</ItemTemplate>
</asp:Repeater> 

								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a><em>&#169; 2011 Copyright McKissack &#38; McKissack</em></address>
				<p>
					<a href="#">Document Management</a> &#8195;|&#8195; <a href="#">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="./about.aspx" class="selected">About Us</a>
				<a href="./services_overview.aspx" >Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./contracts.aspx">Contracts</a>
				<a href="./culture.aspx" >Careers</a>
				<a href="./contact.aspx">Contact</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/dynamic/bannerMLK.png'); height: 170px;"></div>
</body>
</html>