<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ai.aspx.vb" Inherits="_ai" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Architecture &#38; Interiors: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="TwoColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="./services_overview.aspx" >Overview</a>
						</p>
						<p>
							<a href="./ai.aspx" class="selected">Architecture &#38; Interiors</a>
						</p>
						<p>
							<a href="./construction_management.aspx">Program &#38; Construction Management</a>
						</p>
						<p>
							<a href="./planning.aspx">Planning &#38; Facilities Management</a>
						</p>
						<p>
							<a href="./environmental.aspx">Environmental Engineering</a>
						</p>
						<p>
							<a href="./transportation.aspx">Transportation</a>
						</p>
					</div>
				</div>
				<div class="col2 wide">
					<h1>Architecture &#38; Interiors</h1>
					<p>
						 A core value of McKissack & McKissack is the belief that the creation of successful public places must respond to the client's programmatic and budget constraints, as well as make a physical and aesthetic contribution to its surroundings.  Rather than approach design with a preconceived mindset, we look to the site for inspiration and recognize that regional distinctions often form the basis for a definition of the appropriate character.   This approach, along with properly managing surrounding space, pedestrian activity, site configuration and a spatial program, yields a complete design.  The best works of design and architecture epitomize the perfect balance of necessity and creativity.  The results of this process are structures that are functionally proficient as well as inspirational.  Services include:
					</p>
					<p>
						 Master Planning :: Detailed Programming :: Space Planning Standards :: Concept Development :: Blocking and Stacking :: Design :: Site Evaluation :: Facilities Investigation :: Best Use Analysis :: Furniture Standards :: Materials Selection :: Bid Specifications :: Construction Administration and/or Management :: Scheduling :: Budget Control
					</p>
					<h2 class="dotted">Featured Projects <a href="search_results.aspx?keyword=&tid=&sid=2400158">View All</a></h2>
					<div class="featured">
					<asp:Repeater ID="rptFeatured" runat="server" OnItemDataBound="rptFeatured_ItemDataBound">
					<ItemTemplate>
					    <asp:literal ID="ltrlclasstag" runat="server"></asp:literal>
						<a href="project.aspx?pid=<%#Container.DataItem("projectid")%>">
						<img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("projectid")%>&type=project&crop=1&size=1&height=162&width=229" alt="<%#Container.DataItem("name")%>" title="<%#Container.DataItem("name").trim%>"  />
						<strong><%#Container.DataItem("name")%></strong><span></span>
						</a>
					</p>
					</ItemTemplate>
					</asp:Repeater><div class="c"></div>
					</div>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a><em>&#169; 2011 Copyright McKissack &#38; McKissack</em></address>
				<p>
					<a href="#">Document Management</a> &#8195;|&#8195; <a href="#">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="about.aspx" >About Us</a>
			<a href="services_overview.aspx" class="selected">Services</a>
			<a href="search_results.aspx" >Portfolio</a>
			<a href="contracts.aspx">Contracts</a>
			<a href="culture.aspx">Careers</a>
			<a href="contact.aspx">Contact</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/dynamic/bannerMLK.png'); height: 382px"></div>
</body>
</html>