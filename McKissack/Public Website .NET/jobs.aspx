<%@ Page Language="VB" AutoEventWireup="false" CodeFile="jobs.aspx.vb" Inherits="_jobs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Job Openings: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/jquery.accordion.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub jobs">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="TwoColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="./culture.aspx">Culture</a>
						</p>
						<p>
							<a href="./" class="selected">Job Openings</a>
						</p>
						<p>
							<a href="./benefits.aspx">Benefits</a>
						</p>
						<p>
							<a href="./student_internships.aspx">Student Internships</a>
						</p>
					</div>
				</div>
				<div class="col2">
					<h1>Job Openings</h1>
					<div class="accordion">
						<h2><a href="#">Job Title</a><em><a href="#">Location</a></em></h2>
						<div id="billow1">
						
						
<asp:Literal ID="ltrlnojobs" runat=server></asp:Literal>	
									
<asp:Repeater ID="RepeaterJobs" runat="server" >
<ItemTemplate>
<div class="accordionButton">
<strong><%#Container.DataItem("job_title")%></strong>
</div>
<div class="accordionContent">
<p>
 <%#Container.DataItem("description").replace(vbcrlf,"<br>")%>
</p>
<ul>
<li><%#Container.DataItem("experience").replace(vbcrlf,"<br>")%></li>
</ul>
<p>
Please send cover letter, resume, and salary requirements to:
</p>
<p>
 <%#Container.DataItem("contact").replace(vbcrlf,"<br>")%>
</p>
</div>
</ItemTemplate>
</asp:Repeater> 
						
						
						</div>
						<div class="c"></div>
					</div>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a><em>&#169; 2011 Copyright McKissack &#38; McKissack</em></address>
				<p>
					<a href="#">Document Management</a> &#8195;|&#8195; <a href="#">Sitemap</a>
				</p>
				<div>
					<a class="up"></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="./about.aspx" >About Us</a>
				<a href="./services_overview.aspx" >Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./contracts.aspx">Contracts</a>
				<a href="./culture.aspx" class="selected">Careers</a>
				<a href="./contact.aspx">Contact</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/dynamic/bannerClients.png'); height: 382px"></div>
</body>
</html>