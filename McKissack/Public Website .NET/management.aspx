<%@ Page Language="VB" AutoEventWireup="false" CodeFile="management.aspx.vb" Inherits="_management" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Management: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/jquery.accordion.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub management">
	<div id="bg">
		<div id="wrapper" style="margin-top: 170px;">
			<div class="TwoColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="about.aspx" title="Firm Profile" >Firm Profile</a>
						</p>
						<p>
							<a href="clients.aspx" title="Clients" >Clients</a>
						</p>
						<p>
							<a href="history.aspx" title="History">History</a>
						</p>
						<p>
							<a href="management.aspx" title="Management" class="selected">Management</a>
							<span>

							<a href="board.html" title="Board of Directors">Board of Directors</a>

							<a href="leadership.html" title="Leadership">Leadership</a>

							</span>
						</p>
						<p>
							<a href="sustainability.aspx" title="Sustainability">Sustainability</a>
						</p>
						<p>
							<a href="community.aspx" title="Community">Community</a>
						</p>
					</div>
				</div>
				<div class="col2">
					<h1>Management</h1>
					<p id="breadcrumbs">
						<!--<a href="#" class="selected">Board of Directors</a><a href="#">Leadership</a>-->
					</p>
					<div class="accordion">
						<h2>McKISSACK &#38; McKISSACK WASHINGTON, INC.</h2>
						<div id="billow1">
						
						
									
<asp:Repeater ID="RepeaterWashington" runat="server" >
<ItemTemplate>
<div class="accordionButton">
<strong><%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%></strong> / <%#Container.DataItem("position")%>
</div>
<div class="accordionContent">
<p class="accordionLeft">
<%#Container.DataItem("bio").replace(vbcrlf,"<br>")%>
</p>
<p class="accordionRight">
<img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("userid")%>&type=contact&crop=1&size=1&height=313&width=219" title="<%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%>" alt="<%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%>" />
` <a href="mailto:<%#Container.DataItem("email")%>" class="email">email</a>
</p>
</div>
</ItemTemplate>
</asp:Repeater> 
						
						
						</div>
						<div class="c"></div>
						<div class="accordion">
							<h2>McKISSACK &#38; McKISSACK MIDWEST, INC.</h2>
							<div id="billow2">
								
<asp:Repeater ID="RepeaterMidwest" runat="server" >
<ItemTemplate>
<div class="accordionButton">
<strong><%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%></strong> / <%#Container.DataItem("position")%>
</div>
<div class="accordionContent">
<p class="accordionLeft">
<%#Container.DataItem("bio").replace(vbcrlf,"<br>")%>
</p>
<p class="accordionRight">
<img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("userid")%>&type=contact&crop=1&size=1&height=313&width=219" title="<%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%>" alt="<%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%>" />
` <a href="mailto:<%#Container.DataItem("email")%>" class="email">email</a>
</p>
</div>
</ItemTemplate>
</asp:Repeater> 
								
								
							</div>
							<div class="c"></div>
						</div>
						
						
						
						
						
						
						
						<div class="c"></div>
						<div id="leadershipdiv" >
						<div class="accordion">
							<h2>McKISSACK &#38; McKISSACK LEADERSHIP</h2>
							<div id="billow3">
								
<asp:Repeater ID="RepeaterLeadership" runat="server" >
<ItemTemplate>
<div class="accordionButton">
<strong><%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%></strong> / <%#Container.DataItem("position")%>
</div>
<div class="accordionContent">
<p class="accordionLeft">
<%#Container.DataItem("bio").replace(vbcrlf,"<br>")%>
</p>
<p class="accordionRight">
<img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("userid")%>&type=contact&crop=1&size=1&height=313&width=219" title="<%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%>" alt="<%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%>" />
` <a href="mailto:<%#Container.DataItem("email")%>" class="email">email</a>
</p>
</div>
</ItemTemplate>
</asp:Repeater> 
								
								
							</div>
							<div class="c"></div>
						</div>
						</div>
						
						
						
						
						
						
						
						
						
						
						
						
					</div>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a><em>&#169; 2011 Copyright McKissack &#38; McKissack</em></address>
				<p>
					<a href="#">Document Management</a> &#8195;|&#8195; <a href="#">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="./about.aspx" class="selected">About Us</a>
				<a href="./services_overview.aspx" >Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./contracts.aspx">Contracts</a>
				<a href="./culture.aspx" >Careers</a>
				<a href="./contact.aspx">Contact</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/dynamic/bannerMLK.png'); height: 170px"></div>

</body>
</html>