<%@ Page Language="VB" AutoEventWireup="false" CodeFile="index.aspx.vb" Inherits="_Index" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
<script type="text/javascript" src="assets/js/jquery.banner.js"></script>
</head>
<body>
<div id="page" class="home">
	<div id="bg">
		<div id="wrapper">
			<div id="banners">
				<div id="banner">
					<p>
					
<asp:Repeater ID="RepeaterFProjects" runat="server" >
<ItemTemplate>
<img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("asset_id")%>&type=asset&crop=1&size=1&height=500&width=1600" alt="<%#Container.DataItem("name")%>" title="<%#Container.DataItem("name")%>" />
</ItemTemplate>
</asp:Repeater>
					</p>
				</div>
				<div id="bannerText">
				
				
<asp:Repeater ID="RepeaterFProjectsDesc" runat="server" OnItemDataBound="RepeaterFProjectsDesc_ItemDataBound">
<ItemTemplate>
<p><strong><a href="test.html"><%#Container.DataItem("large_text")%></a></strong><em><%#Container.DataItem("small_text")%></em></p>
</ItemTemplate>
</asp:Repeater>				
				
				</div>
			</div>
			<div id="content">
				<div class="featured">
					<p>
						<a href="history.aspx">
						<img src="assets/images/dynamic/featured1.png" alt="Search Our Portfolio" title="Search Our Portfolio" />
						<strong>Experience Our History</strong>
						<em>Built on the McKissack legacy of quality and integrity and provide added value to our clients, partners and employees.</em>
						</a>
					</p>
					<p>
						<a href="services_overview.aspx">
						<img src="assets/images/dynamic/featured2.png" alt="Experience Our History" title="Experience Our History" />
						<strong>View Our Services</strong>
						<em>McKissack has established itself as a leader in all of its disciplines.</em>
						</a>
					</p>
					<p class="last">
						<a href="search_results.aspx">
						<img src="assets/images/dynamic/featured3.png" alt="Watch the McKissack Video" title="Watch the McKissack Video" />
						<strong>Search Our Portfolio</strong>
						<em>Discover award-winning projects from architecture and interiors to transpiration and interiors.</em>
						</a>
					</p>
					<div class="c"></div>
				</div>
			</div>
		</div>
		<div id="footer">


			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a><em>&#169; 2011 Copyright McKissack &#38; McKissack</em></address>
				<p>
					<a href="#">Document Management</a> &#8195;|&#8195; <a href="#">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="./about.aspx">About Us</a>
			<a href="./services_overview.aspx">Services</a>
			<a href="./search_results.aspx">Portfolio</a>
			<a href="./contracts.aspx">Contracts</a>
			<a href="./culture.aspx">Careers</a>
			<a href="./contact.aspx">Contact</a>
		</p>
	</div>
</div>
<div id="bannerBg"></div>
</body>
</html>