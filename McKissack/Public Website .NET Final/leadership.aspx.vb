﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class _leadership
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sql As String
        Dim RepeaterLeadership As Repeater = FindControl("RepeaterLeadership")
        sql = "select o.Item_value, * from ipm_user a join ipm_user_field_value b on a.userid = b.user_id and b.item_id =2400744 left outer join  ipm_user_field_value o on a.userid = o.user_id and o.item_id =2401874 where b.item_value = 1 and a.active='Y' order by isnull(cast(o.Item_value as int),50000), UserID"
        Dim DT3 As New DataTable("FProjects")
        DT3 = mmfunctions.GetDataTable(sql)
        RepeaterLeadership.DataSource = DT3
        RepeaterLeadership.DataBind()




    End Sub

    Protected Sub RepeaterLeadership_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepeaterLeadership.ItemDataBound
        Dim emaillink As Literal = e.Item.FindControl("emaillink")
        If e.Item.DataItem("email") <> "" Then
            emaillink.Text = "<a href=""mailto:" & e.Item.DataItem("email") & """ class=""email"">email</a>"
        End If

    End Sub

End Class
