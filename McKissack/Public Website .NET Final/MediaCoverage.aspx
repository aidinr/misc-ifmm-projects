<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MediaCoverage.aspx.vb" Inherits="_MediaCoverage" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Media Coverage: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>

<style type="text/css">

.media .TwoColumns .col2 #mediaArea {
 height: 400px;
 overflow: auto;
}

.media .TwoColumns .col2 #mediaArea address {
 color: #676767;
 font-size: 13px;
 line-height: 15px;
 font-weight: 300;
 border: 0;
 margin: 8px 50px 0 0;
 padding: 0 0 20 0;
 font-style: normal;
}

.media .TwoColumns .col2 #mediaArea h2 {
 font-size: 17px;
 line-height: 22px;
 color: #EF9E1B;
 font-weight: 600;
 border: 0;
 margin: 14px 40px 3px 0;
 padding: 0;
}

.media .TwoColumns .col2 #mediaArea h3 {
 color: #444;
 font-size: 13px;
 line-height: 16px;
 font-weight: 600;
 border: 0;
 margin: 0 40px 0 0;
 padding: 0;
}

.media .TwoColumns .col2 #mediaArea p {
 color: #676767;
 font-size: 13px;
 line-height: 15px;
 font-weight: 300;
 border: 0;
 margin: 8px 50px 18px 0;
 border-bottom: 1px dotted #bbb;
 padding: 0 0 18px 0;
}

.media .TwoColumns .col2 #mediaArea p span {
 display: block;
 padding: 10px 0 0 0;
 font-weight: 600;
}

#dropSelectDiv {
border: 1px solid #bababa;
width: 125px;
margin: 16px 0 10px 0;
}

ul#dropSelect {
margin:0;
padding:0;
list-style: none;
z-index: 990;
position: relative;
background:#EBEAE8;
width: 125px;
}

#dropSelect li {
padding: 0;
margin: 0;
z-index: 992;
width: 125px;
}

#dropSelect li a {
display:block;
background:#EBEAE8;
text-decoration:none;
color:#333;
white-space:nowrap;
padding:5px 12px;
cursor: default;
}

#dropSelect li li a {
cursor: pointer;
}

#dropSelect li a:hover {
background:#E8D5A8;
}

#dropSelect li ul {
position:absolute;
visibility:hidden;
margin:0 0 0 -1px;
padding:0;
border: 1px solid #bababa;
}

div#page.sub.media div#bg div#wrapper div.TwoColumns div.col2 div#mediaArea p span {
font-size: 13px;
color: #F5B973;
font-weight: normal;
}

div#page.sub.media div#bg div#wrapper div.TwoColumns div.col2 div#mediaArea p span a {
font-size: 13px;
color: #EF9226;
font-weight: normal;
}

</style>


</head>
<body>
<div id="page" class="sub media">
	<div id="bg">

		<div id="wrapper" style="margin-top: 382px;">
			<div class="TwoColumns">
				<div class="col1">
					<div id="sideMenu"> 
						<p> 
							<a href="PressReleasesHeadlines.aspx" title="Press Releases &#38; Headlines">Press Releases &#38; Headlines</a> 
						</p> 
						<p> 
							<a href="MediaCoverage.aspx" title="Media Coverage" class="selected">Media Coverage</a> 
						</p> 
						<p> 
							<a href="PressKit.aspx" title="Press Kit">Press Kit</a> 
						</p> 
					</div> 

				</div>

				<div class="col2">
					<h1>Media Coverage</h1>



<div id="dropSelectDiv">
<ul id="dropSelect">
    <li><a>Select Year</a>
        <ul>
        </ul>

    </li>
</ul>
</div>



					<div id="mediaArea">
<asp:Repeater ID="media1" runat="server" >
<ItemTemplate>

<address><%#Container.DataItem("MediaDate")%></address>
<h2><%#Container.DataItem("Headline")%></h2>
<h3><%#Container.DataItem("PublicationTitle")%></h3>
<p>
<%#Container.DataItem("Content")%>
<span><asp:Literal ID=pdflink runat=server></asp:Literal><asp:Literal ID=linksep runat=server></asp:Literal><asp:Literal ID=medialink runat=server></asp:Literal></span> 
</p>
</ItemTemplate>
</asp:Repeater> 

					</div>

				</div>
				<div class="c"></div>
			</div>

		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address>

				<p>
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a>

				</p>
				<div><a class="up"></a></div>
			</div>
		</div>

	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="/" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>

		</p>
		<p id="logoIcon">

			<a href="/" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="about.aspx">About Us</a>
			<a href="services_overview.aspx">Services</a>
			<a href="search_results.aspx" >Portfolio</a>
			<a href="culture.aspx">Careers</a>
			<a href="./PressReleasesHeadlines.aspx" class="selected">Press Room</a> 
			<a href="contact.aspx">Offices</a>
		</p>
	</div>
</div>

<script type="text/javascript">
var timeout = 500;
var closetimer = 0;
var ddmenuitem = 0;

function dropSelect_open() {
    dropSelect_canceltimer();
    dropSelect_close();
    ddmenuitem = $(this).find('ul').css('visibility', 'visible');
}

function dropSelect_close() {
    if (ddmenuitem) ddmenuitem.css('visibility', 'hidden');
}

function dropSelect_timer() {
    closetimer = window.setTimeout(dropSelect_close, timeout);
}

function dropSelect_canceltimer() {
    if (closetimer) {
        window.clearTimeout(closetimer);
        closetimer = null;
    }
}

$(document).ready(function () {

    var nodup = [];
    $('#mediaArea address').each(function(){
        var y = $(this).html()
        y = y.substring(y.length-4);
        if(!nodup[y]) {
            nodup[y] = true;
            $('#dropSelect li ul').append('<li><a class="dateScroll">' + y + ' News Items</a></li>');
        }
    });

    $('#dropSelect > li').bind('mouseover', dropSelect_open);
    $('#dropSelect > li').bind('mouseout', dropSelect_timer);
    $('.dateScroll').click(function () {
       var p1 = $("address:eq(0)" ,"#mediaArea").offset();
       var p2 = $("address:contains(" + $(this).text().replace(/\D/g,"") + "):first" ,"#mediaArea").offset();
       var dx = 8;
      $("#mediaArea").scrollTop( p2.top - p1.top + dx );
      $('#dropSelect li a').first().html($(this).html()) 
      dropSelect_close();
    });
});

</script>

<div id="bannerSub" style="background-image:url('assets/images/dynamic/headerMediaCoverage.jpg'); height: 382px"></div>
</body>
</html>
