<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GSA Schedule: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
<style type="text/css">
.auto-style1 {
	font-size: 10px;
}
</style>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22905241-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="ThreeColumns">
				<div class="col1">
					<div id="sideMenu"> 
						<p>
							<a href="about.aspx" title="Firm Profile" >Firm Profile</a>

						</p>
						<p>
							<a href="clients.aspx" title="Clients" >Clients</a>
						</p>

						<p>
							<a href="history.aspx" title="History">History</a>
						</p>
						<p>
							<a href="management.aspx" title="Management">Management</a>
						</p>

						<p>
							<a href="sustainability.aspx" title="Sustainability">Sustainability</a>
						</p>
						<p>
							<a href="community.aspx" title="Community">Community</a>
						</p>

						<p>
							<a href="contracts.aspx" title="Contracts">Contracts</a>
							<span>
							<a href="contracts.aspx" title="Contracts Overview">Overview</a>
							<a href="gsa_schedule.aspx" title="GSA Schedule" class="selected">GSA Schedule</a>
							<a href="seaport_e.aspx" title="SeaPort-e">SeaPort-e</a>
							</span>
						</p>
					</div>
				</div>
				<div class="col2">
					<h1>GSA Schedule</h1>
					<p class="auto-style1">
						 Under the U.S. General Services Administration (GSA) 
						 Schedules Program, GSA establishes long-term 
						 government-wide contracts with commercial firms to 
						 provide access to supplies and services that can be 
						 ordered directly from GSA Schedule contractors or 
						 through GSA Advantage!, an online shopping and 
						 ordering system. <br />
						 <br />
						 McKissack &amp; McKissack's specific contract under this 
						 schedule is for Professional Engineering Services, SIN 
						 871-7: Construction Management.<br />
						 <br />
						 <b>SIN 871-7: Construction Management</b><br />
						 Customer agencies shall utilize Construction Managers 
						 as their principal agents to advise on or manage the 
						 process over a project regardless of the project 
						 delivery method used. The Construction Manager 
						 frequently helps the customer agency identify which 
						 delivery method is the best for a project. <br />
						 <br />
						 The construction management approach utilizes a firm 
						 (or team of firms) with construction, design and 
						 management expertise to temporarily expand a customer 
						 agency&#39;s capabilities, so that it can successfully 
						 accomplish a program or project. The Construction 
						 Manager also provides expert advice in support of the 
						 customer agency&#39;s decisions in the implementation of 
						 the project.<br />
						 <br />
						 The following representative services are covered under 
						 construction management: <br />
						 <br />
						 <ul>
						 <li>Project design phase</li> 
						 <li>Project procurement phase</li>
						 <li>Project construction phase</li>
						 <li>Commissioning</li>
						 <li>Testing</li>
						 <li>Claims</li>
						 <li>Post construction</li>
						 </ul>
						 </p>
					<p>
						
					</p>
				</div>
				<div class="col3">
					<div class="pad75">
					</div>
					<p>
					<b>Contract Type</b><br />
					Professional Engineering Services, SIN 871-7: Construction Management
					</p>
					<p>
					<b>Contract Number</b><br />
					GS-10F-0385S FSC Group 87
					</p>
					<p>
					<b>Services</b><br />
					Professional Engineering<br /> 
					Program Management<br />
					Project Management<br />
					Construction Management<br />
					</p>
					</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address>
				<p>
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="./about.aspx" class="selected">About Us</a>
				<a href="./services_overview.aspx">Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./culture.aspx" >Careers</a>
				<a href="./PressReleasesHeadlines.aspx">Press Room</a>
				<a href="./contact.aspx">Offices</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/contracts.jpg'); height: 382px"></div>
</body>
</html>
