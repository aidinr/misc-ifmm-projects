﻿Imports System.Data.SqlClient
Imports System.Data


Partial Class _HistoryTimeline
    Inherits System.Web.UI.Page

    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim RepeaterTimeline As Repeater = FindControl("RepeaterTimeline")
        Dim sql As String = "select a.asset_id, a.name,a.description,CASE b.item_value WHEN '' THEN '2' WHEN '0' THEN '2' ELSE b.item_value END as crop from ipm_asset a,ipm_asset_field_value b where a.asset_id = b.asset_id and b.item_id = 2400801 and available = 'Y' and projectid = 2400793 order by name asc"
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)

        RepeaterTimeline.DataSource = DT1
        RepeaterTimeline.DataBind()

    End Sub
End Class
