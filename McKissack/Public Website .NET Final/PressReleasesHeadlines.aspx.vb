Imports System.Data.SqlClient
Imports System.Data


Partial Class _PressReleasesHeadlines
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

	Dim NewsID As String = Request.QueryString("NewsID")
	Dim sql as String

	If NewsID Is Nothing Then
	   sql = "Select top 1 News_Id From IPM_NEWS WHERE (Type = 0 OR Type = 3) AND Post_Date <= GETDATE() AND Pull_Date > GETDATE() AND Show = 1 Order By Post_Date DESC"
	   Dim DT as New DataTable("FProjects")
	   DT = mmfunctions.GetDataTable(sql)
	   NewsID = DT.Rows(0)(0).ToString()
	   'NewsID = "2401404"
	End If

        Dim rptNews As Repeater = FindControl("RepeaterNews")
	Dim rptProjects as Repeater = FindControl("RepeaterProjects")
	Dim rptArchive as Repeater = FindControl("RepeaterArchive")
	Dim rptContact as Repeater = FindControl("RepeaterContact")

	sql = "SELECT a.News_Id, DATENAME(month, Post_Date) + ' ' + DATENAME(day, Post_Date) + ', ' +DATENAME( year, Post_Date) AS NewsDate, a.Headline, a.PublicationTitle, a.Content, Picture from IPM_NEWS a WHERE (a.Type = 0 OR a.Type = 3) AND a.Post_Date <= GETDATE() AND a.Pull_Date > GETDATE()  AND Show = 1  AND a.News_Id = " & NewsID

        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)
        rptNews.DataSource = DT1
        rptNews.DataBind()

	sql = "SELECT a.ProjectID, b.Name from IPM_NEWS_RELATED_PROJECTS a JOIN IPM_PROJECT b ON a.ProjectID = b.ProjectID WHERE b.Show = 1 AND a.News_Id = " & NewsID
        Dim DT2 As New DataTable("FProjects")
        DT2 = mmfunctions.GetDataTable(sql)
	if DT2.Rows.Count > 0 Then
	        rptProjects.DataSource = DT2
        	rptProjects.DataBind()
	End If

	sql = "SELECT a.News_Id, DATENAME(month, Post_Date) + ' ' + DATENAME( year, Post_Date) AS NewsDate, a.Headline from IPM_NEWS a WHERE (a.Type = 0 OR a.Type = 3) AND a.Post_Date <= GETDATE() AND a.Pull_Date > GETDATE()  AND Show = 1  Order By Post_Date DESC"
        Dim DT3 As New DataTable("FProjects")
        DT3 = mmfunctions.GetDataTable(sql)
	if DT3.Rows.Count > 0 Then
	        rptArchive.DataSource = DT3
        	rptArchive.DataBind()
	End If

	sql = "SELECT a.Contact from IPM_NEWS a WHERE a.News_Id = " & NewsID
        Dim DT4 As New DataTable("FProjects")
        DT4 = mmfunctions.GetDataTable(sql)
	        rptContact.DataSource = DT4
        	rptContact.DataBind()


    End Sub

    Protected Sub RepeaterNews_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepeaterNews.ItemDataBound
        Dim newsimage As Literal = e.Item.FindControl("newsimage")
        If e.Item.DataItem("Picture") = "1" Then
            newsimage.Text = "<p class=""headerImg""><img src=""" & Session("WSRetreiveAsset") & "id=" & e.Item.DataItem("News_Id") & "&type=news&crop=1&size=1&height=200&width=400"" alt="""" /></p>   "
        End If

    End Sub


    Protected Sub RepeaterContact_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepeaterContact.ItemDataBound
        Dim contact As Literal = e.Item.FindControl("contact")
	Dim theContact as string = e.Item.DataItem("Contact").Replace(ControlChars.Cr, "<br />")
        If e.Item.DataItem("Contact") = "" Then
            contact.Text = "N/A"
	Else
	    Dim result As String = Regex.Replace(theContact, "(?<email>\b[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b)", "<a href=""mailto:${email}"">${email}</a>")
	    contact.Text = result
        End If

    End Sub

public function GetTitleText(title As Object) As String
    If String.IsNullOrEmpty(title.ToString()) Then
       Return ""
    Else
       Return "<strong>" & title.ToString() & "</strong></br>"
    End If
end function

End Class
