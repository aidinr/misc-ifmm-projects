﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections.Generic

Partial Class _Project
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim theProjectID = Request.QueryString("pid")

        Try

        
            Dim previousPageCheck As String = "search_results.aspx"
            If InStr(Request.UrlReferrer.AbsoluteUri, "search_results.aspx") > 0 Then
                previousPageCheck = Request.UrlReferrer.AbsoluteUri
            End If
            linkBacktoResults.NavigateUrl = previousPageCheck
        Catch ex As Exception

        End Try



        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select isnull(b.item_value,a.name) longname, a.* from ipm_project a left outer join ipm_project_field_value b on a.projectid = b.projectid and b.item_id = 2400075 where a.projectid = @projectid", MyConnection)
        Dim userid As New SqlParameter("@projectid", SqlDbType.Decimal)
        MyCommand1.SelectCommand.Parameters.Add(userid)
        MyCommand1.SelectCommand.Parameters("@projectid").Value = theProjectID
        Dim DT As New DataTable("Project")
        MyCommand1.Fill(DT)

        If (DT.Rows.Count > 0) Then





            pTitle.Text = DT.Rows(0)("longname")
            Dim LiteralpName2 As Literal = FindControl("LiteralpName2")
            LiteralpName2.Text = DT.Rows(0)("longname")
            LiteralpName1.Text = DT.Rows(0)("longname")
            pAddress.Text = DT.Rows(0)("city")
            Try

                If DT.Rows(0)("state_id").ToString.Trim <> "" Then
                    pAddress.Text += ", " & DT.Rows(0)("state_id")
                End If
            Catch ex As Exception

            End Try
            pDescription.Text = DT.Rows(0)("descriptionmedium")

            'get tags
            '<a href="#">Government</a>, <a href="#">Sustainable</a>, <a href="#">Federal</a>, <a href="#">LEED Certified</a>
            Dim taglist As String = "<h3>Tags</h3><h4>"
            Dim taglisttmp As String = ""
            Dim DT2 As DataTable = mmfunctions.GetDataTable("select * from (select a.keyname, a.keyid,'s' ftype  from ipm_discipline a, ipm_project_discipline b where a.keyid = b.keyid and a.keyuse = 1 and b.projectid = " & theProjectID & " union select a.keyname, a.keyid,'t' ftype from ipm_office a, ipm_project_office b where a.keyid = b.officeid and a.keyuse = 1 and b.projectid = " & theProjectID & " ) a order by keyname")
            For Each row As DataRow In DT2.Rows
                If row("ftype") = "s" Then
                    taglisttmp += "<a href=""search_results.aspx?sid=" & row("keyid") & """>" & row("keyname") & "</a>, "
                Else
                    taglisttmp += "<a href=""search_results.aspx?tid=" & row("keyid") & """>" & row("keyname") & "</a>, "
                End If

            Next
            'Dim DT3 As DataTable = mmfunctions.GetDataTable("select a.keyname, a.keyid from ipm_office a, ipm_project_office b where a.keyid = b.officeid and a.keyuse = 1 and b.projectid = " & theProjectID & " order by keyname")
            'For Each row As DataRow In DT3.Rows
            '    taglisttmp += "<a href=""search_results.aspx?tid=" & row("keyid") & """>" & row("keyname") & "</a>, "
            'Next

            If taglisttmp <> "" Then
                taglist = taglist & taglisttmp.Substring(0, taglisttmp.Length - 2) & "</h4>"
                pTags.Text = taglist
            Else
                pTags.Text = ""
            End If

            'pRelated
            '<h4><a href="#">U.S. Treasuty Building</a></h4>
            pRelated.Text = "<h3>Related Projects</h3>"
            Dim DT4 As DataTable = mmfunctions.GetDataTable("select a.projectid, a.name from ipm_project a, ipm_project_related b where b.ref_id = a.projectid and b.project_id = " & theProjectID & " order by name")
            For Each row As DataRow In DT4.Rows
                pRelated.Text += "<h4><a href=""project.aspx?pid=" & row("projectid") & """>" & row("name") & "</a></h4>"
            Next
            If pRelated.Text = "<h3>Related Projects</h3>" Then

                pRelated.Text = ""
            End If
            'add images
            'select asset_id from ipm_asset where projectid = 2400036 and asset_id in (select asset_id from ipm_asset_services a, ipm_services b where a.keyid = b.keyid and b.keyname = 'Project Website Image') and available = 'Y' 
            Dim DT5 As DataTable = mmfunctions.GetDataTable("select asset_id from ipm_asset where projectid = " & theProjectID & " and asset_id in (select asset_id from ipm_asset_services a, ipm_services b where a.keyid = b.keyid and b.keyname = 'Project Website Image') and available = 'Y'  order by update_date desc")
            For Each row As DataRow In DT5.Rows

                pImages.Text += "<li><a href=""" & Session("WSRetreiveAsset") & "id=" & row("asset_id") & "&type=asset&crop=0&size=1&height=474&width=560"" rel=""" & Session("WSRetreiveAsset") & "id=" & row("asset_id") & "&type=asset&crop=0&size=1&height=1474&width=1560""><img src=""" & Session("WSRetreiveAsset") & "id=" & row("asset_id") & "&type=asset&crop=1&size=1&height=100&width=100"" alt=""Click to Enlarge"" title=""Click to Enlarge"" /></a></li>"

            Next
            If DT5.Rows.Count > 0 Then


                pImage.Text = "<a href=""" & Session("WSRetreiveAsset") & "id=" & DT5.Rows(0)("asset_id") & "&type=asset&crop=0&size=1&height=1474&width=1560"" class=""modal""><img src=""" & Session("WSRetreiveAsset") & "id=" & DT5.Rows(0)("asset_id") & "&type=asset&crop=0&size=1&height=474&width=560"" alt="""" title="""" /></a>"
            Else
                pImage.Text = "<a href=""" & Session("WSRetreiveAsset") & "id=" & theProjectID & "&type=project&crop=0&size=1&height=474&width=560"" class=""modal""><img src=""" & Session("WSRetreiveAsset") & "id=" & theProjectID & "&type=project&crop=0&size=1&height=474&width=560"" alt="""" title="""" /></a>"
            End If


            'get index for next and previous
            'Dim DV As DataView = CType(Session("DV"), DataView)
            Dim dct As New Dictionary(Of String, String)
            If Not Session("DV_Dct") Is Nothing Then
                dct = CType(Session("DV_Dct"), Dictionary(Of String, String))
            End If

            If dct Is Nothing Then
                Dim DT1 As New DataTable("Projects")
                DT1 = mmfunctions.ResultsDataTable("", Nothing, Nothing)
                'cache dv
                Dim ix As Integer = 0
                For Each row As DataRow In DT1.Rows
                    dct.Add(ix, row("projectid"))
                    ix += 1
                Next
                Session("DV_Dct") = dct
            End If
            If (dct.Count > 0) Then

                Dim i As Integer = 0

                For Each item As String In dct.Keys
                    If dct(item) = theProjectID Then
                        i = item
                    End If
                Next


                Dim _test As String = ""
                If (i < dct.Count - 1) Then
                    linkNextProject.NavigateUrl = "project.aspx?pid=" & dct(i + 1)
                    linkNextProject.Text = "NEXT"
                Else
                    linkNextProject.NavigateUrl = "project.aspx?pid=" & dct(0)
                    linkNextProject.Text = "NEXT"
                End If

                If (i = 0) Then
                    linkPrevProject.NavigateUrl = "project.aspx?pid=" & dct(dct.Count - 1)
                    linkPrevProject.Text = "PREVIOUS"
                Else
                    linkPrevProject.NavigateUrl = "project.aspx?pid=" & dct(i - 1)
                    linkPrevProject.Text = "PREVIOUS"
                End If


            End If


        End If




    End Sub


End Class
