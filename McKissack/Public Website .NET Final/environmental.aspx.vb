﻿Imports System.Data.SqlClient
Imports System.Data


Partial Class _environmental
    Inherits System.Web.UI.Page


    Protected Sub _environmental_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim rptFeatured As Repeater = FindControl("rptFeatured")
        '2400158	Architectural and Interiors
        '2400159	Program and Construction Managment
        '2400160	Planning and Facilities Management
        '2400161: Environmental(Engineering)
        '2400162: Transportation()
        Dim sKeyid As String = 2400161
        Dim sql As String = "select top 3 * from (select  a.projectid,a.name,1 porder from ipm_project a, ipm_project_field_value b, ipm_project_discipline c where b.item_id = 2400773 and a.projectid = b.projectid and a.available = 'Y' and b.item_value = 1 and a.projectid = c.projectid and c.keyid = " & sKeyid & " union select a.projectid,a.name, 2 porder from ipm_project a, ipm_project_field_value b where b.item_id = 2400773 and a.projectid = b.projectid and a.available = 'Y' and b.item_value = 1 and a.projectid not in (select  a.projectid from ipm_project a, ipm_project_field_value b, ipm_project_discipline c where b.item_id = 2400773 and a.projectid = b.projectid and a.available = 'Y' and b.item_value = 1 and a.projectid = c.projectid and c.keyid = " & sKeyid & ") ) a order by porder asc,NewId()"
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)
        rptFeatured.DataSource = DT1
        rptFeatured.DataBind()
    End Sub
    Protected Sub rptFeatured_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptFeatured.ItemDataBound
        Try

            Dim classtag As Literal = e.Item.FindControl("ltrlclasstag")
            If e.Item.ItemIndex = 2 Then
                classtag.Text = "<p class=""last"">"
            Else
                classtag.Text = "<p>"
            End If

        Catch ex As Exception

        End Try

    End Sub
End Class
