<%@ Page Language="VB" AutoEventWireup="false" CodeFile="planning.aspx.vb" Inherits="_planning" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Planning &#38; Facilities Management: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="TwoColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="./services_overview.aspx" >Overview</a>
						</p>
						<p>
							<a href="./ai.aspx" >Architecture &#38; Interiors</a>
						</p>
						<p>
							<a href="./construction_management.aspx" >Program &#38; Construction Management</a>
						</p>
						<p>
							<a href="./planning.aspx" class="selected">Planning &#38; Facilities Management</a>
						</p>
						<p>
							<a href="./environmental.aspx">Environmental Engineering</a>
						</p>
						<p>
							<a href="./transportation.aspx">Transportation</a>
						</p>
					</div>
				</div>
				<div class="col2 wide">
					<h1>Planning &#38; Facilities Management</h1>
					<p>
						McKissack &#38; McKissack is a proven leader in providing master planning; programming; and space and facilities management services.  Our expertise has been recognized as a strategic and extremely valuable asset in all phases of a project &#8211; from pre-design to occupancy.  Through planning, we provide a clear, comprehensive picture of a client's current facility as well as immediate and future needs, then establish goals to ensure that the new facility will meet their requirements.  During occupancy/activation, non-construction elements are purchased and installed, then personnel and equipment are identified and moved to their new location.  In addition, McKissack & McKissack assists clients in managing the maintenance and operations of their facilities, which can encompass the use of Computer-Aided Facilities Management software.  Services include:
					</p>
					<p>
						 Master Planning :: Programming :: Space Management :: Communications :: Occupancy Management :: Facilities Management
					</p>
					<h2 class="dotted">Featured Projects <a href="search_results.aspx?keyword=&tid=&sid=2400160">View All</a></h2>
					<div class="featured">
							
					<asp:Repeater ID="rptFeatured" runat="server" OnItemDataBound="rptFeatured_ItemDataBound">
					<ItemTemplate>
					    <asp:literal ID="ltrlclasstag" runat="server"></asp:literal>
						<a href="project.aspx?pid=<%#Container.DataItem("projectid")%>">
						<img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("projectid")%>&type=project&crop=1&size=1&height=162&width=229" alt="<%#Container.DataItem("name")%>" title="<%#Container.DataItem("name").trim%>"  />
						<strong><%#Container.DataItem("name")%></strong><span></span>
						</a>
					
					</ItemTemplate>
					</asp:Repeater>
						<div class="c"></div>
					</div>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address>
				<p>
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="./about.aspx" >About Us</a>
				<a href="./services_overview.aspx" class="selected">Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./culture.aspx" >Careers</a>
				<a href="./PressReleasesHeadlines.aspx">Press Room</a>
				<a href="./contact.aspx">Offices</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/services_planning.jpg'); height: 382px"></div>
</body>
</html>
