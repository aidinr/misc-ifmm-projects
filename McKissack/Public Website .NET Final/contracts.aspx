﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Contracts: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
<style type="text/css">
.auto-style1 {
	font-size: 10px;
} 
</style>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22905241-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="ThreeColumns">
				<div class="col1">
					<div id="sideMenu"> 
						<p>
							<a href="about.aspx" title="Firm Profile" >Firm Profile</a>

						</p>
						<p>
							<a href="clients.aspx" title="Clients" >Clients</a>
						</p>

						<p>
							<a href="history.aspx" title="History">History</a>
						</p>
						<p>
							<a href="management.aspx" title="Management">Management</a>
						</p>

						<p>
							<a href="sustainability.aspx" title="Sustainability">Sustainability</a>
						</p>
						<p>
							<a href="community.aspx" title="Community">Community</a>
						</p>

						<p>
							<a href="contracts.aspx" title="Contracts">Contracts</a>
							<span>
							<a href="contracts.aspx" title="Contracts Overview" class="selected">Overview</a>
							<a href="gsa_schedule.aspx" title="GSA Schedule">GSA Schedule</a>
							<a href="seaport_e.aspx" title="SeaPort-e">SeaPort-e</a>
							</span>
						</p>
					</div>
				</div>
				<div class="col2">
					<h1>Overview</h1>
					<p class="auto-style1">
						 McKissack &#38; McKissack prides itself in its capacity to work with some of the nation's top public agencies.  As an approved contractor on the following contracts, we offer clients rapid access to a wide range of services:
					<br /><br />
						<ul>
						<li> U.S. General Services Administration &#8211; Professional Engineering Services Schedule</li>
						<li>Naval Sea Systems Command &#8211; SeaPort-e</li>
						</ul>
					<br/>						 
					</p>
				</div>
				<div class="col3">
					<div class="pad75">
					</div>
					<p>
					
					&ldquo;I have worked with McKissack &#38; McKissack on a number of the largest and most significant public works projects in the history of the District of Columbia. They have provided the highest level of professional service. The principals and staff have consistently demonstrated a capacity to solve complex problems, meet challenging schedules, manage tight budgets and ultimately ensure quality results.&rdquo;

					</p>
					<address><strong>Allen Y. Lew</strong><br/>
					<i>Former Chief Executive Officer, D.C. Sports &#38; Entertainment Commission</i><br />
					</address>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address>
				<p>
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="./about.aspx" class="selected">About Us</a>
				<a href="./services_overview.aspx">Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./culture.aspx" >Careers</a>
				<a href="./PressReleasesHeadlines.aspx">Press Room</a>
				<a href="./contact.aspx">Offices</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/contracts.jpg'); height: 382px"></div>
</body>
</html>
