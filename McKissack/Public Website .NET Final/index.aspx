﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="index.aspx.vb" Inherits="_Index" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js?v=2"></script>
<script type="text/javascript" src="assets/js/jquery.banner.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22905241-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
<body>
<div id="page" class="home">
	<div id="bg">
	<div id="bannerBg"></div>
		<div id="wrapper">
			<div id="banners">
				<div id="banner" class="wait7">
					<p>
<asp:Repeater ID="RepeaterFProjects" runat="server" >
<ItemTemplate>
<img longdesc="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("asset_id")%>&type=asset&crop=1&size=1&height=500&width=1600&qfactor=50" alt="" title="" />
</ItemTemplate>
</asp:Repeater>
					</p>
				</div>
				<div id="bannerText">
				
<asp:Repeater ID="RepeaterFProjectsDesc" runat="server" OnItemDataBound="RepeaterFProjectsDesc_ItemDataBound">
<ItemTemplate>
<p><strong><a href="<%#Container.DataItem("url")%>"><%#Container.DataItem("large_text")%></a></strong><em><%#Container.DataItem("small_text")%></em></p>
</ItemTemplate>
</asp:Repeater>				
				
				</div>
			</div>
			<div id="content">
				<div class="featured">
					<p>
						<a href="services_overview.aspx">
						<img src="assets/images/dynamic/featured2.png" alt="Experience Our History" title="Experience Our History" />
						<strong>View Our Services</strong>
						<em>We provide a diversity of services that underpins our ability to meet a wide range of project needs.</em>
						</a>
					</p>
					<p>
						<a href="search_results.aspx">
						<img src="assets/images/dynamic/featured3.png" alt="Watch the McKissack Video" title="Watch the McKissack Video" />
						<strong>Search Our Portfolio</strong>
						<em>We deliver superior results while meeting or exceeding client objectives.  </em>
						</a>
					</p>
					<p class="last">
						<a href="PressReleasesHeadlines.aspx">
						<img ID="headlineImage" runat="server" src="assets/images/dynamic/headlineBlank.jpg" alt="See Our Latest News" title="See Our Latest News" />
						<strong>See Our Latest News</strong>
						<asp:Literal ID="newsDateHeadline" runat="server"></asp:Literal>
						</a>
					</p>

					<div class="c"></div>
				</div>
			</div>
		</div>
		<div id="footer">


			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address>
				<p>
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="./about.aspx">About Us</a>
			<a href="./services_overview.aspx">Services</a>
			<a href="./search_results.aspx">Portfolio</a>
			<a href="./culture.aspx">Careers</a>
			<a href="./PressReleasesHeadlines.aspx">Press Room</a>
			<a href="./contact.aspx">Offices</a>
		</p>
	</div>
</div>
<script type="text/javascript"
src="http://dynosize.ifmmdev.com/plain/foot.js"></script>
</body>
</html>
