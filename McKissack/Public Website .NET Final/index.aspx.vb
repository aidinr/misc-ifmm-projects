﻿Imports System.Data.SqlClient
Imports System.Data


Partial Class _Index
    Inherits System.Web.UI.Page

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim sql As String = "select b.name pname,a.asset_id,b.projectid from ipm_asset a, ipm_project b, ipm_asset_field_value c where a.projectid = b.projectid and b.available = 'Y' and a.available = 'Y' and b.favorite = 1 and b.publish = 1 and a.asset_id = c.asset_id and c.item_id in (select item_id from ipm_asset_field_desc where item_tag = 'IDAM_WEBSITE_FEATURED') and c.item_value = 1"
        'select a.name,a.asset_id,b.item_value large_text,c.item_value small_text, d.item_value url from ipm_asset a, ipm_asset_field_value b, ipm_asset_field_value c, ipm_asset_field_value d where a.asset_id = b.asset_id And a.asset_id = c.asset_id And a.asset_id = d.asset_id and b.item_id =2400778 and c.item_id = 2400779 and d.item_id = 2400780
        Dim sql As String = "select a.name,a.asset_id,b.item_value large_text,c.item_value small_text, d.item_value url from ipm_asset a, ipm_asset_field_value b, ipm_asset_field_value c, ipm_asset_field_value d where a.asset_id = b.asset_id And a.asset_id = c.asset_id And a.asset_id = d.asset_id and b.item_id =2400778 and c.item_id = 2400779 and d.item_id = 2400780 and a.projectid = 2400777 and a.available ='Y' "
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)

        RepeaterFProjects.DataSource = DT1
        RepeaterFProjects.DataBind()

        RepeaterFProjectsDesc.DataSource = DT1
        RepeaterFProjectsDesc.DataBind()
	
	sql = "SELECT TOP 1 UPPER(LEFT(DATENAME(month, Post_Date),3)) + ' ' +DATENAME( year, Post_Date) AS NewsDate, a.Headline, Picture, News_Id from IPM_NEWS a WHERE (a.Type = 0 OR a.Type = 3) AND a.Post_Date <= GETDATE() AND a.Pull_Date > GETDATE()   AND Show = 1 Order By a.Post_Date DESC"

	Dim DT2 as New DataTable("FProjects")
	DT2 = mmfunctions.GetDataTable(sql)

	If DT2.Rows.Count > 0 Then
		Dim temp as String
		temp = "<em>" + DT2.Rows(0)(0).ToString() + " | "  + DT2.Rows(0)(1).ToString()  + "</em>"
		newsDateHeadline.Text = temp
		If + DT2.Rows(0)(2).ToString().Trim() = "1" Then
			headlineImage.src = Session("WSRetreiveAsset")  + "id=" +  DT2.Rows(0)(3).ToString() + "&type=news&crop=1&size=1&height=147&width=296"
		End If
	End If

    End Sub

    Protected Sub RepeaterFProjectsDesc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepeaterFProjectsDesc.ItemDataBound
        Try
            Dim literalservices As Literal = e.Item.FindControl("literalservices")
            Dim _datatable As DataTable = mmfunctions.GetDataTableOLE("select keyname from ipm_discipline a, ipm_project_discipline b where a.keyid = b.keyid and a.keyuse = 1 and b.projectid = " & e.Item.DataItem(2))
            Dim i As Integer = 1
            literalservices.Text = ""
            For Each row As DataRow In _datatable.Rows
                If i = _datatable.Rows.Count Then
                    literalservices.Text += row("keyname")
                Else
                    literalservices.Text += row("keyname") & " / "
                End If
                i += 1
            Next
        Catch ex As Exception

        End Try

    End Sub
End Class
