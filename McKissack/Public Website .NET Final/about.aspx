﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>About Us: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="ThreeColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="about.aspx" title="Firm Profile" class="selected">Firm Profile</a>

						</p>
						<p>
							<a href="clients.aspx" title="Clients" >Clients</a>
						</p>

						<p>
							<a href="history.aspx" title="History">History</a>

						</p>
						<p>
							<a href="management.aspx" title="Management">Management</a>
						</p>

						<p>
							<a href="sustainability.aspx" title="Sustainability">Sustainability</a>
						</p>

						<p>
							<a href="community.aspx" title="Community">Community</a>
						</p>

						<p>
							<a href="contracts.aspx" title="Contracts">Contracts</a>
						</p>

					</div>
				</div>
				<div class="col2">
					<h1>Firm Profile</h1>
					<p>
						 McKissack &#38; McKissack is a woman/minority-owned organization specializing in <a href="ai.aspx">architecture &#38; interiors</a>, <a href="construction_management.aspx">program &#38; construction management</a>, <a href="planning.aspx">planning &#38; facilities management</a>, <a href="environmental.aspx">environmental engineering</a> and <a href="transportation.aspx">transportation</a>.  Founded by Deryl McKissack, P.E., PMP in 1990, it has expanded nationwide with offices in Washington, DC, Chicago, Baltimore, Los Angeles, Miami, Atlanta and Orlando.  McKissack &#38; McKissack employs 150 professionals and is certified by many states and municipalities as a minority/woman-owned business.
					</p>
					<p>
						As a leading program management organization, McKissack &#38; McKissack is managing $15 billion in construction and is ranked by <i>Engineering News-Record</i> as one of the top 50 Program Management and top 100 Construction Management For-Fee firms in the United States.  Moreover, we are ranked by the <i>Washington Business Journal</i> as one of the top 25 Environmental Consultant firms in the Washington Metropolitan area.</p>
					<p>
						 McKissack &#38; McKissack’s diversity of services allows us to successfully manage clients’ projects from conception through completion, whether the project is new construction, renovation, reconstruction, environmental design or transportation.  Additionally, our services are provided to a diverse and demanding client base, including federal, state and local governments; developers; corporations; healthcare institutions; colleges and universities; K-12 schools; and municipal entities.</p>
					<p>
						 McKissack &#38; McKissack is proud of its ability to take on difficult and complex assignments that require the skills of many disciplines.  In each assignment, McKissack &#38; McKissack professionals strive to be creative yet practical, to provide outstanding technical expertise and to be especially responsive to client needs, budgets and schedules.
					</p>
				</div>
				<div class="col3">
					<div class="pad75">
					</div>
					<h1>Vision</h1>
					<p>
					Build upon the “McKissack Legacy of quality and integrity” and provide added value to our clients, partners and employees by:
					</p>
					<ul>
					<li>Becoming <u>national leaders</u> in our core capabilities.</li>					
					<li>Being dedicated to <u>nurturing our talent</u> to be exceptional in our areas of expertise.</li>
					<li><u>Using our resources wisely</u> to expand into new markets that strategically align with our clients.</li>
					</ul>
					<p>
					We plan to create the “McKissack Experience” of giving everyone who associates with us a competitive edge.
					</p>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address>
				<p>
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
	<div id="header">
		<div>
			<p id="logoType">
				<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
			</p>
			<p id="logoIcon">
				<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
			</p>
			<p id="menu">
				<a href="./about.aspx" class="selected">About Us</a>
				<a href="./services_overview.aspx">Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./culture.aspx">Careers</a>
				<a href="./PressReleasesHeadlines.aspx">Press Room</a>
				<a href="./contact.aspx">Offices</a>
			</p>
		</div>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/firm_profile.jpg'); height: 382px"></div>
</body>
</html>
