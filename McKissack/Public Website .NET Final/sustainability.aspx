﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sustainability.aspx.vb" Inherits="_sustainability" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sustainability: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22905241-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="TwoColumns">
				<div class="col1">
					<div id="sideMenu">

						<p>
							<a href="about.aspx" title="Firm Profile" >Firm Profile</a>

						</p>
						<p>
							<a href="clients.aspx" title="Clients" >Clients</a>
						</p>

						<p>
							<a href="history.aspx" title="History">History</a>

						</p>
						<p>
							<a href="management.aspx" title="Management">Management</a>
						</p>

						<p>
							<a href="sustainability.aspx" title="Sustainability" class="selected">Sustainability</a>
						</p>

						<p>
							<a href="community.aspx" title="Community">Community</a>
						</p>

						<p>
							<a href="contracts.aspx" title="Contracts">Contracts</a>
						</p>

					</div>
				</div>
				<div class="col2 wide">
					<h1>Sustainability</h1>
					<p>
						McKissack &#38; McKissack views the goals of efficiency and sustainable design as interconnected with the firm&#146;s dedication to construction excellence. We respond to environmental challenges with new approaches, materials and methods that result in energy conservation and efficiency. 
						With many strategies achieved at no added cost, our full-discipline approach includes concepts such as adaptive reuse, waste reduction, value engineering and use of recovered materials. Moreover, our professionals are well versed in the design and project management of sustainable projects,
						and our Leadership in Energy and Environmental Design (LEED) experience covers a wide range of projects, including commercial, public assembly, correctional, laboratory and educational, among others. To achieve sustainability goals, McKissack &#38; McKissack provides regular in-house education 
						and actively encourages staff members to become LEED Accredited Professionals or Green Associates. We urge our personnel to attend lectures, seminars and courses that explain new environmentally conscious approaches and to incorporate these approaches in our daily work. McKissack &#38; McKissack is a member of the U.S. Green Building Council.
					</p>
					
					<h2 class="dotted">Featured Projects <a href="search_results.aspx?keyword=&tid=2401012&sid=">View All</a></h2>
					<div class="featured">
					<asp:Repeater ID="rptFeatured" runat="server" OnItemDataBound="rptFeatured_ItemDataBound">
					<ItemTemplate>
					    <asp:literal ID="ltrlclasstag" runat="server"></asp:literal>
						<a href="project.aspx?pid=<%#Container.DataItem("projectid")%>">
						<img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("projectid")%>&type=project&crop=1&size=1&height=162&width=229" alt="<%#Container.DataItem("name")%>" title="<%#Container.DataItem("name").trim%>"  />
						<strong><%#Container.DataItem("name")%></strong><span></span>
						</a>
					
					</ItemTemplate>
					</asp:Repeater><div class="c"></div>
					</div>
					
					
				</div>
				<div class="c">
				</div>
			</div>
			<div class="c">
			</div>
		</div>
	</div>
	<div id="footer">
		<div class="footer">
			<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address>
			<p>
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a>
			</p>
			<div>
				<a class="up"></a>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="./about.aspx" class="selected">About Us</a>
			<a href="./services_overview.aspx">Services</a>
			<a href="./search_results.aspx">Portfolio</a>
			<a href="./culture.aspx">Careers</a>
			<a href="./PressReleasesHeadlines.aspx">Press Room</a>
			<a href="./contact.aspx">Offices</a>
		</p>
	</div>
</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/sustainability.jpg'); height: 382px">
</div>
</body>
</html>
