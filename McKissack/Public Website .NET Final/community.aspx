﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Community: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22905241-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="ThreeColumns">
				<div class="col1">
					<div id="sideMenu">

						<p>
							<a href="about.aspx" title="Firm Profile" >Firm Profile</a>

						</p>
						<p>
							<a href="clients.aspx" title="Clients" >Clients</a>
						</p>

						<p>
							<a href="history.aspx" title="History">History</a>

						</p>
						<p>
							<a href="management.aspx" title="Management">Management</a>
						</p>

						<p>
							<a href="sustainability.aspx" title="Sustainability">Sustainability</a>
						</p>

						<p>
							<a href="community.aspx" title="Community" class="selected">Community</a>
						</p>

						<p>
							<a href="contracts.aspx" title="Contracts">Contracts</a>
						</p>

					</div>
				</div>
				<div class="col2">
					<h1>Community</h1>
					<p>
					Part of being a great company is being a great citizen in our community.  McKissack &#38; McKissack fulfills this social responsibility through contributions, memberships and volunteer activities.  Whether expanding opportunities for housing and homeownership to vulnerable populations; providing student scholarship funds; mentoring future leaders; or sponsoring community improvement days, McKissack &#38; McKissack and its personnel generously give their time and resources.  
					</p>
					<p>
					McKissack &#38; McKissack is a proud contributor to the 
					<a class="hoverInfo">Trust for the National Mall<span><strong>Trust for the National Mall</strong>The Trust for the National Mall (TNM) is the official nonprofit partner of the National Park Service (NPS) that is helping to restore, revitalize and preserve the National Mall in Washington, DC.  As the NPS undertakes the National Mall Plan—which addresses park use, maintenance and facilities—TNM is involved in raising the necessary funding; connecting visitors to the park’s rich history; developing engaging programs and events that will educate and inspire current and future generations; and funding an endowment to ensure the future maintenance and upkeep of the National Mall.</span></a>, 
					the official nonprofit partner of the National Park Service dedicated to restoring and improving Washington, DC’s National Mall.  This organization is aligned with our desire to build and preserve national treasures, as evident by our work on the 
					<a href="http://mckissackstaging.webarchives.com/project.aspx?pid=2400062">Lincoln Memorial</a>, 
					<a href="http://mckissackstaging.webarchives.com/project.aspx?pid=2400110">Thomas Jefferson Memorial</a>, 
					<a href="http://mckissackstaging.webarchives.com/project.aspx?pid=2400036">Martin Luther King, Jr. National Memorial</a> and 
					<a href="http://mckissackstaging.webarchives.com/project.aspx?pid=2400543">National Museum of African American History and Culture</a>.  
					</p>
					<p>
					We believe that one cannot underestimate the importance of an education.  To that end, McKissack &#38; McKissack is a contributor to programs that mentor, educate and fund young students, such as the 
					<a class="hoverInfo">College Success Foundation<span><strong>College Success Foundation</strong>The College Success Foundation (CSF) provides college access and scholarship programs for middle and high school students in the District of Columbia Wards 7 and 8, serving low-income, underserved students who face multiple barriers to success in college and in life.  CSF works with public and private partners to ensure that scholars receive the educational and financial incentives necessary to achieve their college and career dreams. </span></a>, 
					<a class="hoverInfo">Living Classrooms Foundation<span><strong>Living Classrooms Foundation</strong>Living Classrooms Foundation is a nonprofit organization that provides hands-on education and job skills training for students from diverse backgrounds, with a special emphasis on serving at-risk youth.  Its “learning by doing” education programs emphasize applied learning of math, science, literacy, history, economics and ecology, with the objectives of career development, community service, elevating self-esteem and fostering multicultural exchange.</span></a>, 
					<a class="hoverInfo">MTTG Dream Design Build Scholarship<span><strong>MTTG Dream Design Build Scholarship</strong>The MTTG Dream Design Build Scholarship seeks to identify minority high school seniors who are continuing their education in the disciplines of architecture, engineering, construction management or building trades.  This scholarship program seeks to build relationships with and provide financial resources to the future generation of building and construction professionals.</span></a>, 
					<a class="hoverInfo">ACE Mentor Program<span><strong>ACE Mentor Program</strong>The ACE Mentor Program of the Greater Washington Metropolitan Area, Inc. (ACE DC) is a partnership of architects, construction managers, engineers, designers, professional societies and organizations, high schools and universities.  ACE DC is an award-winning, nonprofit organization that provides mentors who guide student teams from the District of Columbia, Northern Virginia and Maryland public schools as they explore educational and career opportunities in architecture, construction and engineering.  It also offers college access through scholarships as well as admissions and portfolio guidance.</span></a> 
					and 
					<a class="hoverInfo">TransTech Academy at Cardozo High School<span><strong>TransTech Academy at Cardozo High School</strong>The TransTech Academy, located at Cardozo High School in Washington, DC, was established in 1991 as the first transportation studies academy in the region.  It provides students with experiences that instill the values of education, constructive employment and careers in various modes of the transportation industry and link this education to the real world.  Moreover, the program serves more than 100 students at Cardozo, of whom 95% graduate and 85% go on to college.</span></a>.  
					Directly, McKissack &#38; McKissack has undertaken projects that enhance, restore and innovate public school systems, such as the District of Columbia Public Schools, Chicago Public Schools and Los Angeles Unified School District.  We are a longtime contributor to the 
					<a class="hoverInfo">Windows of Opportunity<span><strong>Windows of Opportunity</strong>Windows of Opportunity is the nonprofit affiliate of the Chicago Housing Authority.  It leverages private resources to create partnerships and programs that foster self-sufficiency for some of Chicago's most economically and socially disadvantaged citizens.  The organization has three primary roles: increasing services for youth in public housing; fostering a collaborative approach among service providers to better serve public housing residents; and reducing the isolation experienced by public housing residents by creating access to opportunities that are available to all Chicago residents.  </span></a>, 
					the nonprofit arm of the Chicago Housing Authority that provides scholarships to its residents.  Additionally, we actively participate in the 
					<a class="hoverInfo">National Forum for Black Public Administrators (NFBPA)<span><strong>National Forum for Black Public Administrators</strong>The National Forum for Black Public Administrators (NFBPA) is a professional membership organization dedicated to the advancement of black leadership in the public sector.  It provides intensive training, a powerful network of black public leadership and professional development programs, including raising scholarship funds for aspiring public administrators.  </span></a>, 
					which supports educational development and scholarships for aspiring public administrators.  Deryl McKissack is Chair of NFBPA’s annual National Leadership Awards Dinner, the organization’s lead fundraising event.  She is also a former Advisory Board Member of the 
					<a class="hoverInfo">University of the District of Columbia Foundation, Inc.<span><strong>University of the District of Columbia Foundation</strong>The University of the District of Columbia Foundation, Inc. serves as the primary link between the university and private, professional and business communities in the District of Columbia and the nation.  Its primary responsibility is to work with the Office of Development to raise awareness of the need for private support, to solicit gifts on behalf of the university and to effectively carry out its fiduciary responsibilities in the management of the assets received by the foundation.</span></a>, 
					which promotes the need for private support of the university and solicits and manages contributions.  For the 
					<a class="hoverInfo">Miami Art Museum<span><strong>Miami Art Museum</strong>The Miami Art Museum of Dade County (MAM) advances public knowledge and appreciation of art, architecture and design.  MAM is also dedicated to education and scholarship, taking a variety of approaches to interpret its collection and exhibitions for its many audiences.  MAM engages the community in lively exchange, fosters fresh ideas and conveys the excitement of the creative process.  </span></a>, 
					Ms. McKissack serves on the Board of Trustees, which strives to advance knowledge and appreciation of art, architecture and design.
					</p>
					<!--
					<p>
						 McKissack &#38; McKissack is a proud contributor to the <a class="hoverInfo">Trust for the National Mall<span>
							<strong>Trust for the National Mall</strong> Sit amet Consectetur adipiscing elit. Mauris lobortis, 
							massa non auctor feugiat, metus sapien mattis risus, ut mollis elit tortor ac lectus. 
							Vestibulum vel turpis massa. Proin vitae urna lectus, cursus placerat urna. Donec mollis,
							nunc eget pulvinar tristique, turpis magna venenatis erat, non varius nulla magna id eros. Duis at arcu id orci volutpat porta blandit quis orci.
							</span></a>, 
						the official non-profit partner of the National Park Service dedicated to restoring and improving Washington, DC's National Mall.  This organization is aligned with our desire to build and preserve national treasures, including our work on the Lincoln Memorial, Thomas Jefferson Memorial, Martin Luther King, Jr. National Memorial and National Museum of African American History and Culture.
					</p>
					<p>
						We believe that one cannot underestimate the importance of an education.  To that end, McKissack &#38; McKissack is a contributor to the 
							<a class="hoverInfo">College Success Foundation<span>
							<strong>College Success Foundation</strong> Sit amet Consectetur adipiscing elit. Mauris lobortis, 
							massa non auctor feugiat, metus sapien mattis risus, ut mollis elit tortor ac lectus. 
							Vestibulum vel turpis massa. Proin vitae urna lectus, cursus placerat urna. Donec mollis,
							nunc eget pulvinar tristique, turpis magna venenatis erat, non varius nulla magna id eros. Duis at arcu id orci volutpat porta blandit quis orci.
							</span></a>, Living Classrooms Foundation and ACE Mentor Program, which mentor, educate and fund young students.  Directly, McKissack &#38; McKissack has undertaken projects that enhance, restore and eventually innovate public schools systems, such as the District of Columbia Public Schools,  Chicago Public Schools and Los Angeles Unified School District.  
						We are a longtime contributor to the <a class="hoverInfo">Windows of Opportunity<span>
							<strong>Windows of Opportunity</strong> Sit amet Consectetur adipiscing elit. Mauris lobortis, 
							massa non auctor feugiat, metus sapien mattis risus, ut mollis elit tortor ac lectus. 
							Vestibulum vel turpis massa. Proin vitae urna lectus, cursus placerat urna. Donec mollis,
							nunc eget pulvinar tristique, turpis magna venenatis erat, non varius nulla magna id eros. Duis at arcu id orci volutpat porta blandit quis orci.
							</span></a>, the non-profit arm of the Chicago Housing Authority, which provides scholarships to its residents.  Additionally, we actively participate in the National Forum for Black Public Administrators (NFBPA), which supports educational development and scholarships for aspiring public administrators.  
						Deryl McKissack is Chair of NFBPA's annual National Leadership Awards Dinner, the organization’s lead fundraising event.   She is also a former Advisory Board Member of the University of the District of Columbia Foundation, Inc., which promotes the need for private support of the university as well as solicits and manages contributions.  For the Miami Art Museum, Ms. McKissack serves on the Board of Trustees, which among its purposes is to advance knowledge and appreciation of art, architecture and design.
					</p>
					-->
					
				 </div>
				<div class="col3">
					<div class="pad75">
					</div>
					
					<p>
						 “McKissack &#38; McKissack has been an extraordinary supporter of the NFBPA through the outstanding leadership Deryl McKissack has shown over the years…This is but one example of her support for the field of public administration and her unwavering ‘give back’ attitude that she displays in every community she serves.  It is certainly an honor, and a pleasure, to work with her and her firm.”
					</p>
					
						 <address><strong>Jerry N. Johnson</strong><br/><i>General Manager/CEO, Washington Suburban Sanitary Commission; President, National Forum for Black Public Administrators</i>
						</address>
					</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address>
				<p>
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
	<div id="header">
		<div>
			<p id="logoType">
				<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
			</p>
			<p id="logoIcon">
				<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
			</p>
			<p id="menu">
				<a href="./about.aspx" class="selected">About Us</a>
				<a href="./services_overview.aspx">Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./culture.aspx">Careers</a>
				<a href="./PressReleasesHeadlines.aspx">Press Room</a>
				<a href="./contact.aspx">Offices</a>
			</p>
		</div>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/about_community.jpg'); height: 382px"></div>
</body>
</html>
