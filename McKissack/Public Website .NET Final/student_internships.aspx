﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Careers: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22905241-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="TwoColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="./culture.aspx">Culture</a>
						</p>
						<p>
							<a href="./jobs.aspx">Job Openings</a>
						</p>
						<p>
							<a href="./benefits.aspx">Benefits</a>
						</p>
						<p>
							<a href="./student_internships.aspx" class="selected">Student Internships</a>
						</p>
					</div>
				</div>
				<div class="col2">
					<h1>Student Internships</h1>
					<p>
						 McKissack &#38; McKissack offers summer internships in its Washington, DC and Chicago offices for high school and college students interested in the architecture and engineering fields. Participants have the opportunity to not only gain valuable work experience with a distinguished firm, but also learn firsthand about the daily responsibilities associated with the industry. Additionally, students have the chance to garner real world work experience from various projects throughout the summer, both in the office and on site. These internships are paid and last roughly 10 weeks. McKissack &#38; McKissack looks to expand these enriching opportunities to their other offices in the near future.
					</p>

				<div class="c"></div>
				<div class="quotes">
				 <p>
					Through patience and perseverance, great knowledge can be obtained. This is exactly the environment that is provided at McKissack &#38; McKissack. I know I will carry the knowledge gained here for the rest of my career.
				 </p>
				 <address><strong>Rose Lucas, Intern</strong><br /></address>
				 <p>
					Working here at McKissack &#38; McKissack has allowed me to enrich my study in the field of architecture, in ways I would never have been able to otherwise.
				 </p>
				 <address><strong>Assia Belguedj, Intern</strong><br /></address>
				</div>

				</div>

			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address>
				<p>
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a>
				</p>
				<div>
					<a class="up"></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="./about.aspx">About Us</a>
			<a href="./services_overview.aspx">Services</a>
			<a href="./search_results.aspx">Portfolio</a>
			<a href="./culture.aspx" class="selected">Careers</a>
			<a href="./PressReleasesHeadlines.aspx">Press Room</a>
			<a href="./contact.aspx">Offices</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/Student_Internships.jpg'); height: 382px">
</div>
</body>
</html>
