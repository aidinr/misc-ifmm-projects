﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Data
Imports System.Data.SqlClient

Public Class mmfunctions
    Public Shared Function GetDataTableOLE(ByVal query As String) As DataTable
        Dim connection1 As OleDbConnection = New OleDbConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRINGOLE"))
        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)
        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()
        End Try
        GetDataTableOLE = table1
    End Function

    Public Shared Function GetDataTable(ByVal query As String) As DataTable
        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim sql As String = query
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("DTABLE")
        Try
            MyCommand1.Fill(DT1)
        Finally
            MyConnection.Close()
        End Try
        GetDataTable = DT1
    End Function


    Public Shared Function GetDataSet(ByVal query As String) As DataSet
        Dim connection1 As OleDbConnection = New OleDbConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRINGOLE"))
        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)
        Dim table1 As New DataSet
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()
        End Try
        GetDataSet = table1
    End Function


    Public Shared Function ResultsDataTable(ByVal _searchFilter As String, ByVal tid As String, ByVal sid As String) As DataTable

        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(MakeSQL(tid, sid, False), System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim userid As New SqlParameter("@keyword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid)
        MyCommand1.SelectCommand.Parameters("@keyword").Value = "%" & _searchFilter.Replace("'", "''") & "%"
        Dim DT1 As New DataTable("Projects")
        MyCommand1.Fill(DT1)
        Return DT1
    End Function

    Public Shared Function MakeSQL(ByVal tid As String, ByVal sid As String, Optional ByVal blnUseLimiter As Boolean = True) As String

        Dim sqllimiter As String = ""

        Dim sql As String = "select projectid,name,state_id,city from ipm_project where available = 'Y' and publish = 1 and search_tags like @keyword "
        If IsNumeric(tid) Then
            sql += " and projectid in (select projectid from ipm_project_office where officeid = " & tid & ") "
        End If
        If IsNumeric(sid) Then
            sql += " and projectid in (select projectid from ipm_project_discipline where keyid = " & sid & ") "
        End If
        'add limiter
        If blnUseLimiter Then
            Dim sqltemp As String = sql
            sql = "select top " & System.Configuration.ConfigurationManager.AppSettings("MaxResults") & " * from (" & sqltemp & ") a"
        End If
        'add ordering
        'sql += " order by name asc"
        Return sql

    End Function



End Class
