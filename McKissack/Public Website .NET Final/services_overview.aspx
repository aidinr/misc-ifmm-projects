﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Service Overview: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="ThreeColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="./" class="selected">Overview</a>
						</p>
						<p>
							<a href="./ai.aspx">Architecture &#38; Interiors</a>
						</p>
						<p>
							<a href="./construction_management.aspx">Program &#38; Construction Management</a>
						</p>
						<p>
							<a href="./planning.aspx">Planning &#38; Facilities Management</a>
						</p>
						<p>
							<a href="./environmental.aspx">Environmental Engineering</a>
						</p>
						<p>
							<a href="./transportation.aspx">Transportation</a>
						</p>
					</div>
				</div>
				<div class="col2">
					<h1>Overview</h1>
					<p>
						 McKissack &#38; McKissack specializes in <a href="ai.aspx">architecture &#38; interiors</a>, <a href="construction_management.aspx">program &#38; construction management</a>, <a href="planning.aspx">planning & facilities management</a>, <a href="environmental.aspx">environmental engineering</a> and <a href="transportation.aspx">transportation</a>.  Underpinned by our seasoned personnel, each department has a proven history of effectively managing the resources and relationships necessary to achieve a client&#146;s desired outcome.  McKissack & McKissack has established itself as a leader in all of its disciplines and has a successful track record of delivering superior results that meet or exceed client objectives. 
					</p>
					</div>
				<div class="col3">
					<div class="pad75">
					</div>
					<p>
					
					&ldquo;McKissack &#38; McKissack re-establishes the lost art of the ‘Master Builder.’  In the role of program manager and/or Design-Builder this firm exceeds.  As in any business, the people make the difference.  The leadership of McKissack & McKissack has excelled in the selection of its staff.&rdquo;

					</p>
					<address><strong>Dr. Ed Jackson, Jr.</strong><br/>
					 <i>Executive Architect, Washington, DC Martin Luther King, Jr. National Memorial
					 Project Foundation, Inc.</i></address>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address>
				<p>
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
				<a href="./about.aspx">About Us</a>
				<a href="./services_overview.aspx" class="selected">Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./culture.aspx">Careers</a>
				<a href="./PressReleasesHeadlines.aspx">Press Room</a>
				<a href="./contact.aspx">Offices</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/services.jpg'); height: 382px"></div>
</body>
</html>
