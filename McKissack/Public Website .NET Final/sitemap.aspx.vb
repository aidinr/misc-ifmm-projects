Imports System.Web
Imports System.Data.SqlClient
Imports System.Data

Partial Class sitemap
    Inherits System.Web.UI.Page
    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.AddHeader("Content-type", "application/xml")

        Dim out As String = "<?xml version=""1.0"" encoding=""UTF-8"" ?>" & vbNewLine
        out &= "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">" & vbNewLine
        Dim url() As String = { _
"http://www.mckissackdc.com/", _
"http://www.mckissackdc.com/about.aspx", _
"http://www.mckissackdc.com/clients.aspx", _
"http://www.mckissackdc.com/history.aspx", _
"http://www.mckissackdc.com/management.aspx", _
"http://www.mckissackdc.com/leadership.aspx", _
"http://www.mckissackdc.com/sustainability.aspx", _
"http://www.mckissackdc.com/community.aspx", _
"http://www.mckissackdc.com/services_overview.aspx", _
"http://www.mckissackdc.com/ai.aspx", _
"http://www.mckissackdc.com/construction_management.aspx", _
"http://www.mckissackdc.com/planning.aspx", _
"http://www.mckissackdc.com/environmental.aspx", _
"http://www.mckissackdc.com/transportation.aspx", _
"http://www.mckissackdc.com/search_results.aspx", _
"http://www.mckissackdc.com/culture.aspx", _
"http://www.mckissackdc.com/jobs.aspx", _
"http://www.mckissackdc.com/benefits.aspx", _
"http://www.mckissackdc.com/PressReleasesHeadlines.aspx", _
"http://www.mckissackdc.com/MediaCoverage.aspx", _
"http://www.mckissackdc.com/PressKit.aspx", _
"http://www.mckissackdc.com/contact.aspx" _
}


        Dim yesterday As String = Date.Now.AddDays(-1).ToString("yyyy-MM-dd")
        For i As Integer = 0 To url.Length - 1
            out &= "<url><loc>" & url(i).Trim & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
        Next

        Dim Sql As String = "SELECT a.News_Id  from IPM_NEWS a WHERE (a.Type = 0 OR a.Type = 3) AND a.Post_Date <= GETDATE() AND a.Pull_Date > GETDATE()  AND Show = 1  Order By Post_Date DESC"
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(Sql)
        If DT1.Rows.Count > 0 Then
            For i As Integer = 0 To DT1.Rows.Count - 1
                out &= "<url><loc>http://www.mckissackdc.com/PressReleasesHeadlines.aspx?NewsID=" & DT1.Rows(i)("News_ID").ToString.Trim & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
            Next
        End If

        Dim DT2 As New DataTable("Projects")
        DT2 = mmfunctions.ResultsDataTable("", "", "", Request.QueryString("sort"))
        Dim DV As DataView = New DataView(DT2)

        For Each row As DataRow In DV.Table.Rows
            out &= "<url><loc>http://www.mckissackdc.com/project.aspx?pid=" & row("projectid").ToString.Trim & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
        Next

        out &= "</urlset>"

        Response.Write(out)

    End Sub

End Class
