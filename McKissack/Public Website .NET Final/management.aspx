﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="management.aspx.vb" Inherits="_management" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Management: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/jquery.accordion.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub management">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="TwoColumns">
				<div class="col1">
					<div id="sideMenu">

						<p>
							<a href="about.aspx" title="Firm Profile" >Firm Profile</a>

						</p>
						<p>
							<a href="clients.aspx" title="Clients" >Clients</a>
						</p>

						<p>
							<a href="history.aspx" title="History">History</a>

						</p>
						<p>
							<a href="management.aspx" title="Management">Management</a>
							<span>

							<a href="management.aspx" class="selected" title="Board of Directors">Board of Directors</a>

							<a href="leadership.aspx" title="Leadership">Leadership</a>

							</span>
						</p>

						<p>
							<a href="sustainability.aspx" title="Sustainability">Sustainability</a>
						</p>

						<p>
							<a href="community.aspx" title="Community">Community</a>
						</p>

						<p>
							<a href="contracts.aspx" title="Contracts">Contracts</a>
						</p>
					</div>
				</div>
				<div class="col2">
					<h1>Board of Directors</h1>
					<p id="breadcrumbs">
						<!--<a href="#" class="selected">Board of Directors</a><a href="#">Leadership</a>-->
					</p>
					<div class="accordion">
						<h2>McKISSACK &#38; McKISSACK of WASHINGTON, INC.</h2>
						<div id="billow1">
						
						
									
<asp:Repeater ID="RepeaterWashington" runat="server" >
<ItemTemplate>
<div class="accordionButton">
<strong><%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%></strong> / <%# Container.DataItem("position2")%>
</div>
<div class="accordionContent">
<p class="accordionLeft">
<%#Container.DataItem("bio").replace(vbcrlf,"<br>")%>
</p>
<p class="accordionRight">
<img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("userid")%>&type=contact&crop=1&size=1&height=313&width=219" title="<%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%>" alt="<%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%>" />
<br/><asp:Literal ID=emaillink runat=server></asp:Literal>
<br /><br /><br />
</p>
</div>
</ItemTemplate>
</asp:Repeater> 
						
						
						</div>
						<div class="c"></div>
						<div class="accordion">
							<h2>McKISSACK &#38; McKISSACK MIDWEST, INC.</h2>
							<div id="billow2">
								
<asp:Repeater ID="RepeaterMidwest" runat="server" >
<ItemTemplate>
<div class="accordionButton">
<strong><%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%></strong> / <%# Container.DataItem("position2")%>
</div>
<div class="accordionContent">
<p class="accordionLeft">
<%#Container.DataItem("bio").replace(vbcrlf,"<br>")%>
</p>
<p class="accordionRight">
<img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("userid")%>&type=contact&crop=1&size=1&height=313&width=219" title="<%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%>" alt="<%#Container.DataItem("firstname")%> <%#Container.DataItem("lastname")%>" />
<br/><asp:Literal ID=emaillinkmidwest runat=server></asp:Literal>
<br /><br /><br />
</p>
</div>
</ItemTemplate>
</asp:Repeater> 
								
								
							</div>
							<div class="c"></div>
						</div>
						
						
						
						
						
						<div class="c"></div>
								
<asp:Repeater ID="RepeaterLeadership" runat="server" >
</asp:Repeater> 
						
						
						
						
						
						
						
						
						
						
						
						
					</div>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address>
				<p>
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
				<a href="./about.aspx" class="selected">About Us</a>
				<a href="./services_overview.aspx" >Services</a>
				<a href="./search_results.aspx">Portfolio</a>
				<a href="./culture.aspx" >Careers</a>
				<a href="./PressReleasesHeadlines.aspx">Press Room</a>
				<a href="./contact.aspx">Offices</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/about_management.jpg'); height: 382px"></div>

</body>
</html>
