<%@ Page Language="VB" AutoEventWireup="false" CodeFile="project.aspx.vb" Inherits="_Project" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><asp:Literal ID=LiteralpName1 runat=server></asp:Literal>: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/jquery.carousel.js"></script>
<script type="text/javascript" src="assets/js/jquery.litestbox.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 170px;">
			<div class="project">
				<div class="projectNav">
					<p>
						<!--<a href="#" class="BackToResults">Back to Search Results</a>-->
						<asp:HyperLink ID="linkBacktoResults" runat=server Text="Back to Project List" CssClass="prevProject"></asp:HyperLink>
						<asp:HyperLink ID="linkPrevProject" runat=server Text=Previous></asp:HyperLink><font color="#B8B8B8"> &#124; </font><asp:HyperLink ID="linkNextProject" runat=server Text=Next CssClass="nextProject"></asp:HyperLink>
					</p>
				</div>
				<div class="col1">
					<h1><asp:Literal ID=pTitle runat=server></asp:Literal></h1>
					<h2><asp:Literal ID=pAddress runat=server></asp:Literal></h2>
					<p>
						 <asp:Literal ID=pDescription runat=server></asp:Literal>
					</p>
					<!--<h3>Client</h3>
					<h4><a href="#">United States Alchohol Tobacco and Firearms</a></h4>
					<h3>Awards</h3>
					<h4>American Institute of Architects (AIA)</h4>
					<h4>National Honor Award for Architecture</h4>-->
					<asp:Literal ID=pTags runat=server></asp:Literal>
					
					<asp:Literal ID=pRelated runat=server></asp:Literal>
					<!--
					<p class="email">
						<a href="mailto:support@mckissack.com?subject=<asp:Literal ID=LiteralpName2 runat=server></asp:Literal>?&body=Project URL: http://192.168.1.41:10004/project.aspx?pid=<%=Request.QueryString("pid") %>">Email</a> 
					</p>-->	
					
				</div>
				<div class="col2">
					<p id="image">
						<span><asp:Literal ID=pImage runat=server></asp:Literal></span>
					</p>
					<div class="threeThumbs">
						<div id="carousel">
							<div class="images">
								<ul>
								<asp:Literal ID=pImages runat=server></asp:Literal>
									<!--<li><a href="assets/images/dynamic/atf1_medium.jpg" rel="assets/images/dynamic/atf1.jpg"><img src="assets/images/dynamic/atf1_thumb.jpg" alt="Click to Enlarge" title="Click to Enlarge" /></a></li>
									<li><a href="assets/images/dynamic/atf2_medium.jpg" rel="assets/images/dynamic/atf2.jpg"><img src="assets/images/dynamic/atf2_thumb.jpg" alt="Click to Enlarge" title="Click to Enlarge" /></a></li>
									<li><a href="assets/images/dynamic/atf3_medium.jpg" rel="assets/images/dynamic/atf3.jpg"><img src="assets/images/dynamic/atf3_thumb.jpg" alt="Click to Enlarge" title="Click to Enlarge" /></a></li>
									<li><a href="assets/images/dynamic/atf4_medium.jpg" rel="assets/images/dynamic/atf4.jpg"><img src="assets/images/dynamic/atf4_thumb.jpg" alt="Click to Enlarge" title="Click to Enlarge" /></a></li>
									<li><a href="assets/images/dynamic/atf5_medium.jpg" rel="assets/images/dynamic/atf5.jpg"><img src="assets/images/dynamic/atf5_thumb.jpg" alt="Click to Enlarge" title="Click to Enlarge" /></a></li>
									<li><a href="assets/images/dynamic/atf1_medium.jpg" rel="assets/images/dynamic/atf1.jpg"><img src="assets/images/dynamic/atf1_thumb.jpg" alt="Click to Enlarge" title="Click to Enlarge" /></a></li>
									<li><a href="assets/images/dynamic/atf2_medium.jpg" rel="assets/images/dynamic/atf2.jpg"><img src="assets/images/dynamic/atf2_thumb.jpg" alt="Click to Enlarge" title="Click to Enlarge" /></a></li>
									<li><a href="assets/images/dynamic/atf3_medium.jpg" rel="assets/images/dynamic/atf3.jpg"><img src="assets/images/dynamic/atf3_thumb.jpg" alt="Click to Enlarge" title="Click to Enlarge" /></a></li>
									<li><a href="assets/images/dynamic/atf4_medium.jpg" rel="assets/images/dynamic/atf4.jpg"><img src="assets/images/dynamic/atf4_thumb.jpg" alt="Click to Enlarge" title="Click to Enlarge" /></a></li>
									<li><a href="assets/images/dynamic/atf5_medium.jpg" rel="assets/images/dynamic/atf5.jpg"><img src="assets/images/dynamic/atf5_thumb.jpg" alt="Click to Enlarge" title="Click to Enlarge" /></a></li>-->
								</ul>
								<div class="c"></div>
							</div>
						</div>
					</div>
					<p class="bottom">
					</p>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address>
				<p>
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
			<a href="about.aspx">About Us</a>
			<a href="services_overview.aspx">Services</a>
			<a href="search_results.aspx" class="selected">Portfolio</a>
			<a href="culture.aspx">Careers</a>
			<a href="./PressReleasesHeadlines.aspx">Press Room</a>
			<a href="contact.aspx">Contact</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/dynamic/banner5.png'); height: 170px"></div>
</body>
</html>
