Imports System.Data.SqlClient
Imports System.Data


Partial Class _MediaCoverage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim rptFeatured As Repeater = FindControl("rptFeatured")
        'Type-ID 1	Media Coverage


	Dim sql as String = "SELECT a.News_Id, a.PDF, CASE WHEN a.PDFLinkTitle = '' THEN 'Download PDF' ELSE a.PDFLinkTitle END AS PDFLinkTitle, b.Item_Value as MediaDate, a.Post_Date, a.PublicationTitle, a.Headline, a.Content, c.Item_Value as MediaURL from IPM_NEWS a LEFT JOIN IPM_NEWS_FIELD_VALUE b ON a.News_Id = b.News_Id AND b.Item_Id = 2401395  LEFT JOIN IPM_NEWS_FIELD_VALUE c ON a.News_Id = c.News_Id AND c.Item_Id = 2401467 WHERE a.Type = 1 AND a.Post_Date <= GETDATE() AND a.Pull_Date > GETDATE() AND Show = 1 ORDER BY a.Post_Date DESC"


        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)
        media1.DataSource = DT1
        media1.DataBind()
    End Sub


    Protected Sub media1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles media1.ItemDataBound
        Dim pdflink As Literal = e.Item.FindControl("pdflink")
        If e.Item.DataItem("PDF") = "1" Then
            pdflink.Text = "<a href=""" & Session("WSRetreiveAsset") & "id=" & e.Item.DataItem("News_Id") & "&type=newspdf&size=0&cache=1"" >" & e.Item.DataItem("PDFLinkTitle") & "</a>"
        End If

        Dim medialink As Literal = e.Item.FindControl("medialink")
	If NOT IsDBNull(e.Item.DataItem("MediaURL")) Then
	        If NOT String.IsNullOrEmpty(e.Item.DataItem("MediaURL"))  Then
        	    medialink.Text = "<a href=""" & e.Item.DataItem("MediaURL") & """ >View More &#187;</a>"
        	End If
	End If

        Dim linksep As Literal = e.Item.FindControl("linksep")
	If NOT IsDBNull(e.Item.DataItem("MediaURL")) Then
	        If (e.Item.DataItem("PDF") = "1") AND (NOT String.IsNullOrEmpty(e.Item.DataItem("MediaURL"))) Then
        	    linksep.Text = " | "
        	End If
	End If

  End Sub


End Class
