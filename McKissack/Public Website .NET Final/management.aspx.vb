﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class _management
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim RepeaterWashington As Repeater = FindControl("RepeaterWashington")
        Dim sql As String = "select a.*,c.item_value position2 from ipm_user a left outer join ipm_user_field_value c on  c.user_id = a.userid and c.item_id = 2400752 left outer join  ipm_user_field_value b on a.userid = b.user_id and b.item_id =2400745 where b.item_value = 1 and active='Y'"
        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)
        RepeaterWashington.DataSource = DT1
        RepeaterWashington.DataBind()


        Dim RepeaterMidwest As Repeater = FindControl("RepeaterMidwest")
        sql = "select a.*,c.item_value position2 from ipm_user a left outer join ipm_user_field_value c on  c.user_id = a.userid and c.item_id = 2400751 left outer join  ipm_user_field_value b on a.userid = b.user_id and b.item_id =2400746  where b.item_value = 1 and active='Y'"
        Dim DT2 As New DataTable("FProjects")
        DT2 = mmfunctions.GetDataTable(sql)
        RepeaterMidwest.DataSource = DT2
        RepeaterMidwest.DataBind()

       



    End Sub

    Protected Sub RepeaterWashington_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepeaterWashington.ItemDataBound
        Dim emaillink As Literal = e.Item.FindControl("emaillink")
        If e.Item.DataItem("email") <> "" Then
            emaillink.Text = "<a href=""mailto:" & e.Item.DataItem("email") & """ class=""email"">email</a>"
        End If

    End Sub

    Protected Sub RepeaterMidwest_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepeaterMidwest.ItemDataBound
        Dim emaillink As Literal = e.Item.FindControl("emaillinkmidwest")
        If e.Item.DataItem("email") <> "" Then
            emaillink.Text = "<a href=""mailto:" & e.Item.DataItem("email") & """ class=""email"">email</a>"
        End If
    End Sub
End Class
