var scripts = document.getElementsByTagName('script');
var jsPath = './';
if (scripts) {
    if (scripts[0].src) jsPath = scripts[0].src.split("assets")[0];
}

$(document).ready(function () {

    if ($('.hoverInfo').html()) {
        $('.hoverInfo').hover(
        function (e) {
            $('.col3').prepend('<div class="hoverInfoPop"><h2></h2><p>' + $(this).children('span').html() + '</p><address class="hoverInfoPop"></address></div>');

            var offset = $(this).offset();
            var BottomPosition = $(window).height() + $(document).scrollTop();

            if ((offset.top + $('.col3 div.hoverInfoPop').height() - 50) > BottomPosition) {
		var o1 = (offset.top - 448);
                $('.col3 div.hoverInfoPop').css('top', (offset.top - 248 - $('.col3 div.hoverInfoPop').height()) + 'px');
		var o2 = $('.col3 div.hoverInfoPop').css('top').replace(/\D/g,'') ;
		var o3 = $('.col3 div.hoverInfoPop h2').css('top').replace(/\D/g,'');
                $('.col3 div.hoverInfoPop h2').css('top', Number(o1) - Number(o2) + Number(o3)  + 'px');
            } else {
		$('.col3 .hoverInfoPop h2').css('top' , (0 - $('.col3 .hoverInfoPop').css("top").replace(/\D/g,'') + 125 ) + 'px');
                $('.col3 div.hoverInfoPop').css('top', (offset.top - 448) + 'px');
            }

        }, function () {
            $('.hoverInfoPop').remove();
        });
    }

    if ($('#banner').html() && $('#bannerText').html()) {
        autoSquish();
    }

    if ($('.col2 .featured').html()) {

        $('.col2 .featured strong').each(function () {
            $(this).before('<em>' + $(this).html() + '</em>');
        })

        $('.col2 .featured strong').css('opacity', '.9');
        $('.col2 .featured a').hover(function () {
            $('strong', this).stop().animate({
                "opacity": 1
            }, 100);
        }, function () {
            $('strong', this).stop().animate({
                "opacity": .9
            }, 100);
        });
    }

    if ($('#timeline').html()) {
        $('#timeline').before('<div class="edge"></div><a class="prev"></a><a class="next"></a>');
        var position = 0;
        var positionMax = $("#timeline li").size();
        $('.prev').click(function () {
            if (position > 0) position--;
        });
        $('.next').click(function () {
            if (position < positionMax - 3) position++;
        });
        $("#timeline").jCarouselLite({
            btnNext: ".next",
            btnPrev: ".prev",
            speed: 600,
            circular: false,
            afterEnd: function (a) {
                if (position == 0) $('.prev').css('display', 'none');
                else $('.prev').css('display', 'block');
                if (position == positionMax - 3) $('.next').css('display', 'none');
                else $('.next').css('display', 'block');
            }
        });
    }

    if ($('#carousel').html() && $('#image').html()) {

        var position = 0;
        var imageCount = $("#carousel li").size();
        $('.images a').first().addClass('selected');

        if (imageCount > 6) {
            $('div.project .col2 #carousel div').css('position', 'absolute');
            $('#carousel').before('<a class="prev"></a><a class="next"></a>');
            $('.prev').click(function () {
                if (position > 0) position--;
            });
            $('.next').click(function () {
                if (position < imageCount - 6) position++;
            });
            $("#carousel").jCarouselLite({
                btnNext: ".next",
                btnPrev: ".prev",
                visible: 6,
                speed: 600,
                circular: false,
                afterEnd: function (a) {
                    if (position == 0) $('.prev').css('display', 'none');
                    else $('.prev').css('display', 'block');
                    if (position == imageCount - 6) $('.next').css('display', 'none');
                    else $('.next').css('display', 'block');
                }
            });
        } else if (imageCount == 6) {
            $('div.project .col2 #carousel ul li').css('margin-right', '11px');
            $('div.project .col2 .threeThumbs').css('margin-left', '22px');
            $('div.project .col2 .threeThumbs').css('width', '560px');
        } else if (imageCount == 5) {
            $('div.project .col2 .threeThumbs').css('margin-left', '73px');
            $('div.project .col2 .threeThumbs').css('width', '461px');
        } else if (imageCount == 4) {
            $('div.project .col2 .threeThumbs').css('margin-left', '120px');
            $('div.project .col2 .threeThumbs').css('width', '368px');
        } else if (imageCount == 3) {
            $('div.project .col2 .threeThumbs').css('margin-left', '164px');
            $('div.project .col2 .threeThumbs').css('width', '275px');
        } else if (imageCount == 2) {
            $('div.project .col2 .threeThumbs').css('margin-left', '214px');
            $('div.project .col2 .threeThumbs').css('width', '179px');
        } else {
            $('div.project .col2 .threeThumbs').css('display', 'none');
            $('div.project .col2 #image').css('margin-bottom', '20px');
        }

        $('#image a').append('<em class="plus"></em>')
        $('.images li a').each(function () {
            $(this).click(function () {
                $('#image img').attr('src', $(this).attr('href'));
                $('#image a').attr('href', $(this).attr('rel'));
                $('.images a').removeClass();
                $(this).addClass('selected');
                return false;
            });

        })

    }

    if ($('#SelectviewModeGrid').html()) {
        if (readCookie('viewMode') == 'list') {
            createCookie('viewMode', 'list', 99);
            $('#portfolioResults').removeClass('grid');
            $('#portfolioResults').addClass('list');
            $('#SelectviewModeGrid').css('border', '1px solid #7A7A7A');
            $('#SelectviewModeList').css('border', '1px solid #EF9D33');
            $('#portfolioResults').css('visibility', 'visible');
        }
        else {
            $('#portfolioResults').css('visibility', 'visible');
        }
        $('#SelectviewModeGrid').click(function () {
            eraseCookie('viewMode');
            $('#portfolioResults').removeClass('list');
            $('#portfolioResults').addClass('grid');
            $('#SelectviewModeGrid').css('border', '1px solid #EF9D33');
            $('#SelectviewModeList').css('border', '1px solid #7A7A7A');
            $('#portfolioResults').css('visibility', 'visible');
            return false;
        });
        $('#SelectviewModeList').click(function () {
            createCookie('viewMode', 'list', 99);
            $('#portfolioResults').removeClass('grid');
            $('#portfolioResults').addClass('list');
            $('#SelectviewModeGrid').css('border', '1px solid #7A7A7A');
            $('#SelectviewModeList').css('border', '1px solid #EF9D33');
            $('#portfolioResults').css('visibility', 'visible');
            return false;
        });

        $('#portfolioResults.grid a strong').each(function () {
            if ($(this).height() > 20) {
                $(this).css('padding', '5px 10px 15px 10px');
            }
        });
    }

    $('.up').click(function () {
        if ($('#footer').html()) {
            if ($('#footer div.footer div a.up').hasClass('maximized')) {
                $('#footer div.footer div a.up').removeClass('maximized');
                $('#bottomNav').animate({
                    height: '9px',
                    top: '-9px'
                }, 400);
                $('#footer div.footer div').animate({
                    marginTop: '0'
                }, 340);
                return false;
            } else if ($('#footer #bottomNav').html()) {
                $('#footer div.footer div a.up').addClass('maximized');
                $('#bottomNav').animate({
                    height: '240px',
                    top: '-240px'
                }, 400);
                $('#footer div.footer div').animate({
                    marginTop: '-204px'
                }, 490);
                return false;
            } else {
                $('#footer div.footer div a.up').addClass('maximized');
                $.get("bottomNav.txt", null, function (data) {
                    $('#footer').prepend(data);
                    $('#bottomNav').animate({
                        height: '240px',
                        top: '-240px'
                    }, 400);
                    $('#footer div.footer div').animate({
                        marginTop: '-204px'
                    }, 490);
                    return false;
                });
            }
        }
    });


    $('div.project .col2 #image span img').load(function () {
        if ($(this).attr('height') < 474) $(this).css('marginTop', ((474 - $(this).attr('height')) / 2) + 'px')
        else $(this).css('marginTop', 0);
    });

    $('#header #menu a').last().addClass('last');

    if ($('.col2 #viewSelect #pageNumberArea').html() && $('#portfolioResults').html()) {
        $('.results').append('<div id="viewSelectBottom"><p id="pageNumberAreaBottom">' + $('#viewSelect #pageNumberArea').html() + '</p></div>');
    }

});

function autoSquish() {
    var windowHeight = $(window).height();
    var bodyHeight = $('body').height();
    if ($('#banner').html() && windowHeight < bodyHeight) {
        var newBannerHeight = 500 + windowHeight - bodyHeight - 4;
        if (newBannerHeight < 400) {
            newBannerHeight = 400;
        }
        $('#banners, #banner, #banner div, #bannerBg').css('height', newBannerHeight + 'px');
    }
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

if (false /*@cc_on || @_jscript_version < 99 @*/ ) {
    document.write('<meta http-equiv="X-UA-Compatible" content="IE=7" /><meta http-equiv="cleartype" content="off" />');
}

$(document).ready(function () {
    var font = $('body').css('font-family');
    //$('body').prepend('<div style="white-space:nowrap;font-family: arial, helvetica, sans-serif; position: fixed;bottom: 10px; right: 10px; padding: 5px; background: #f0f0f0; border: 2px solid #FCB124; z-index: 99999">Change Font to: <input type="text" id="FontSelectInput" value="'+ font.replace(/"/g,"'") +'"/><input type="button" id="FontSelectButton" value="Change Font!" /><br /><em style="color: #E31C17">Type in a font name or paste the "Use this font" link from Google</em></div>');
    $('#FontSelectButton').click(function () {
        var f = $('#FontSelectInput').val();
        if (f.indexOf("<link") != -1) {
            $('head').append(f);
            $('body').css('fontFamily', '"' + f.split("family=")[1].split("'")[0].replace(/\+/g, ' ') + '"');
        } else {
            $('body').css('fontFamily', f);
        }
    });
});

if (location.href=='http://www.mckissackdc.com/index.aspx' || location.href=='http://www.mckissackdc.com/' || location.href=='http://mckissackdc.com/index.aspx' || location.href=='http://mckissackdc.com/') {

$('#headlineImage').attr('src', 'http://dynosize.ifmmdev.com/plain/video.png');
$('.featured p.last a strong').last().html('Watch Video');
$('.featured p.last a em').last().html('Learn about our history and vision.');

$(document).ready(function() {

var modalCSS ='\
<style type="text/css">\
#modalIframe{\
position: fixed;\
margin: auto;\
top: 90px;\
left: 15%;\
border: 8px solid #000;\
padding: 0;\
display: none;\
z-index: 112;\
opacity: 0;\
visibility: hidden\
}\
#modalIframe iframe {\
padding: 0;\
margin: 0 0 0 0;\
display: block;\
float: left;\
z-index: 113;\
width: 400px;\
height: 225px;\
background: #000;\
}\
#modalIframe a {\
position: absolute;\
display: block;\
top: 0;\
right: 0;\
width: 30px;\
height: 30px;\
background: transparent url(http://dynosize.ifmmdev.com/plain/closebox.png) no-repeat scroll 100% 2px;\
cursor: pointer;\
}\
#modalIframe a:hover {\
background-position: 100% -28px;\
}\
#modalOverlay {\
position: fixed;\
margin: 0;\
top: 0;\
left: 0;\
width: 100%;\
height: 100%;\
padding: 0;\
display: none;\
background: #707070;\
z-index: 111;\
}\
#modalLoading {\
left: 48%;\
position: fixed;\
z-index: 114;\
top: 33%;\
width: 32px;\
height: 32px;\
padding: 20px;\
border: 5px solid #111;\
background: #fff;\
display: none;\
}\
</style>';

 $('head').append(modalCSS);
 $('body').append('<div id="modalIframe"></div><div id="modalOverlay"></div><div id="modalLoading"><img src="http://dynosize.ifmmdev.com/plain/loading.gif" alt="Loading" title="Loading" /></div>');
 $("#modalIframe").click(function () { 
	$("#modalIframe").css("visibility", "hidden");
	$("#modalIframe").css("opacity", "0");
	$("#modalIframe").css("display", "none");
	$("#modalIframe").removeAttr('style');
	$("#modalIframe").html('');
	jQuery("#modalOverlay").css("display", "none");
	return false;
 });

$('.featured p.last a').last().click(function() {
	$("#modalIframe").removeAttr('style');
	$("#modalIframe").css("display", "block");
	$("#modalLoading").css("opacity", "0");
	$("#modalLoading").css("display", "block");
	$("#modalIframe").html('<iframe width="400" height="225" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src="http://player.vimeo.com/video/29121065?title=0&amp;byline=0&amp;portrait=0" onload="modalShow(this)"></iframe><a></a>');
	$("#modalLoading").animate({"opacity": 1},1100);
	$("#modalOverlay").css("opacity", "0");
	$("#modalOverlay").css("display", "block");
	$("#modalOverlay").animate({"opacity": .8});
	return false;
});


});

function modalShow(e) {
 var x = ($('body').width() / 2 ) - (e.width/2);
 $('#modalIframe').css('left',x+'px');
 $('#modalIframe').animate({"opacity": 1});
 $("#modalLoading").css("display", "none");
 $("#modalIframe").css("visibility", "visible");
 $("#modalIframe").css("opacity", "1");
}

}
