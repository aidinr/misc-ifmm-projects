var movingBanner = null;

$(document).ready(function () {

 if($('#banner').html() && $('#bannerText').html()) {

   var ii=0;
   $("img", "#banner").each(function() {
	var href = $("a:eq(" + $("img", "#banner").index( this ) + ")", "#bannerText").attr("href");
	var src = $(this).attr("longdesc");
	var alt = $(this).attr("alt");
	$(this).replaceWith( "<a href=\"" + href + "\"><img id=\"BannerImage_"+ ii +"\" longdesc=\"" + src + "\" onload=\"LoadImage("+ (ii) +")\" alt=\"" + alt + "\" />" + "</a>" );
	ii++;
   });

    $('#banner p a img:eq(0)').attr('src',$('#banner p a img:eq(0)').attr('longdesc'));

    var bodyWidth = $('body').width();
    var bannerOffset = 0;
    if (bodyWidth < 1600) {
        bannerOffset = Math.round((1600 - bodyWidth) / 2);
    }
    $('#banner p').css('marginLeft', '-' + String(bannerOffset) + 'px');

    $('#bannerText p').append('<span class="bannerPrev"></span><span class="bannerNext"></span>');


    $('#bannerText p strong').css('display','none');
    $('#bannerText p strong').first().css('display','block');

   if(false /*@cc_on || @_jscript_version < 99 @*/) { 
     $('#bannerText p strong').css('opacity','.99');
     $('#bannerText p em').css('opacity','.99');
   }

    $('#bannerText p em').css('display','none');
    $('#bannerText p em').first().css('display','block');

    $('.bannerNext').click(function () {
	if( !$('#banner p a img:eq(1)').attr('longdesc')==false) {
		$('#banner p a img:eq(1)').load(function() {
		    $('.bannerNext').removeClass('loading');
		    BannerNext ();
		});
		$('.bannerNext').addClass('loading');
		return;
	}

	if (BannerAuto) clearInterval(BannerAuto);
	BannerNext ();
    });

    function BannerNext () {

    var bodyWidth = $('body').width();
    var bannerOffset = 0;
    if (bodyWidth < 1600) {
        bannerOffset = Math.round((1600 - bodyWidth) / 2);
    }

        if (!movingBanner) {

	if( !$('#banner p a img:eq(1)').attr('longdesc')==false) return;
	    $('.bannerPrev').removeClass('loading');
	    $('.bannerNext').removeClass('loading');
            movingBanner = 1;
            var bannerFirst = $('#banner p a').first();
            var bannerFirstLink =  $("#bannerText a").first().attr("href");
            $('#banner p').animate({
                marginLeft: '-' + (1600 + bannerOffset) + 'px'
            }, 600, function () {
                $("#banner p").append(bannerFirst);
                $("#banner p").css('marginLeft', '-' + bannerOffset + 'px');
                movingBanner = null;
            });

            $('#bannerText p strong').first().fadeOut(300);
            $('#bannerText p em').first().fadeOut(300, function () {
                $("#bannerText p").css('display', 'none');
                $("#bannerText").append($('#bannerText p').first());
                $("#bannerText p").first().css('display', 'block');
                $('#bannerText p strong').first().fadeIn(300);
                $('#bannerText p em').first().fadeIn(300);
            });
        }
    }

    $('.bannerPrev').click(function () {

	if( !$('#banner p a img').last().attr('longdesc')==false) {
		$('#banner p a img').last().load(function() {
		   if ( $('.bannerPrev').hasClass('loading') ) {
		    $('.bannerPrev').removeClass('loading');
		    BannerPrev ();
		   }
		});
		if (BannerAuto) clearInterval(BannerAuto);
		$('.bannerPrev').addClass('loading');
		return;
	}
	BannerPrev ();
    });

function BannerPrev () {

    if( !$('#banner p a img').last().attr('longdesc')==false) return;

    if (BannerAuto) clearInterval(BannerAuto);
    var bodyWidth = $('body').width();
    var bannerOffset = 0;
    if (bodyWidth < 1600) {
        bannerOffset = Math.round((1600 - bodyWidth) / 2);
    }

        if (!movingBanner) {
            movingBanner = 1;
            var bannerLast = $('#banner p a').last();
            var bannerLastLink =  $("#bannerText a").last().attr("href");
            $("#banner p").prepend(bannerLast);
            $("#banner p").css('marginLeft', '-' + (1600 + bannerOffset) + 'px');
            $('#banner p').animate({
                marginLeft: '-' + bannerOffset + 'px'
            }, 600, function () {
                movingBanner = null;
            });

            $('#bannerText p strong').first().fadeOut(300);
            $('#bannerText p em').first().fadeOut(300, function () {
                $("#bannerText p").css('display', 'none');
                $("#bannerText").prepend($('#bannerText p').last());
                $("#bannerText p").first().css('display', 'block');
                $('#bannerText p strong').first().fadeIn(300);
                $('#bannerText p em').first().fadeIn(300);
            });
        }
    }

   if ( $('#banner').attr('class') ) {
    var delay = ($('#banner').attr('class').replace('wait', '') * 1000);
    if(delay>0) {
     var BannerAuto = setInterval(function () {
      BannerNext();
     }, delay);
    }
   }
 }
});

function LoadImage (iif) {
  $('#BannerImage_'+ iif ).attr('lang', (iif) );
  $('#BannerImage_'+ iif ).removeAttr('longdesc');
  $('#BannerImage_'+ iif ).removeAttr('onload');
  if(  $('#BannerImage_'+ (iif+1) ).attr('longdesc') != undefined ) 
   $('#BannerImage_'+ (iif+1) ).attr('src', $('#BannerImage_'+ (iif+1) ).attr('longdesc'));
}
