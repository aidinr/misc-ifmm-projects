if ($('#headlineImage').attr('id')=='headlineImage') {

$(document).ready(function() {

var modalCSS ='\
<style type="text/css">\
#modalIframe{\
position: fixed;\
margin: auto;\
top: 90px;\
left: 15%;\
border: 8px solid #000;\
padding: 0;\
display: none;\
z-index: 1112;\
opacity: 0;\
visibility: hidden\
}\
#modalIframe iframe {\
padding: 0;\
margin: 0 0 0 0;\
display: block;\
float: left;\
z-index: 1113;\
width: 640px;\
height: 360px;\
background: #000;\
}\
#modalIframe a {\
position: absolute;\
display: block;\
top: 0;\
right: 0;\
width: 30px;\
height: 30px;\
background: transparent url(/assets/images/closebox.png) no-repeat scroll 100% 2px;\
cursor: pointer;\
}\
#modalIframe a:hover {\
background-position: 100% -28px;\
}\
#modalOverlay {\
position: fixed;\
margin: 0;\
top: 0;\
left: 0;\
width: 100%;\
height: 100%;\
padding: 0;\
display: none;\
background: #707070;\
z-index: 1111;\
}\
#modalLoading {\
left: 48%;\
position: fixed;\
z-index: 1114;\
top: 33%;\
width: 32px;\
height: 32px;\
padding: 20px;\
border: 5px solid #111;\
background: #fff;\
display: none;\
}\
</style>';

 $('head').append(modalCSS);
 $('body').append('<div id="modalIframe"></div><div id="modalOverlay"></div><div id="modalLoading"><img src="/assets/images/loading.gif" alt="Loading" title="Loading" /></div>');
 $("#modalIframe").click(function () { 
	$("#modalIframe").css("visibility", "hidden");
	$("#modalIframe").css("opacity", "0");
	$("#modalIframe").css("display", "none");
	$("#modalIframe").removeAttr('style');
	$("#modalIframe").html('');
	jQuery("#modalOverlay").css("display", "none");
	return false;
 });

$('a.v').click(function() {
	$("#modalIframe").removeAttr('style');
	$("#modalIframe").css("display", "block");
	$("#modalLoading").css("opacity", "0");
	$("#modalLoading").css("display", "block");
	$("#modalIframe").html('<iframe width="640" height="360" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src="'+ $(this).attr('href') +'" onload="modalShow(this)"></iframe><a></a>');
	$("#modalLoading").animate({"opacity": 1},1100);
	$("#modalOverlay").css("opacity", "0");
	$("#modalOverlay").css("display", "block");
	$("#modalOverlay").animate({"opacity": .8});
	return false;
});


});

function modalShow(e) {
 var x = ($('body').width() / 2 ) - (e.width/2);
 $('#modalIframe').css('left',x+'px');
 $('#modalIframe').animate({"opacity": 1});
 $("#modalLoading").css("display", "none");
 $("#modalIframe").css("visibility", "visible");
 $("#modalIframe").css("opacity", "1");
}

}