function tinyMCEinit () {

  var script = document.createElement( 'script' );
  script.type = 'text/javascript';
  script.src = 'tiny_mce/jquery.tinymce.js';
  $('head').append(script);

 $('textarea.cmsRich').tinymce({
     script_url: 'tiny_mce/tiny_mce.js',
     plugins: "safari,inlinepopups,contextmenu,noneditable,nonbreaking,xhtmlxtras,template,advlist",
     theme: "advanced",
     convert_urls: true,
     theme_advanced_buttons1: "fontsizeselect,forecolor,backcolor,|,removeformat",
     theme_advanced_buttons2: "justifyleft,justifycenter,justifyright,justifyfull",
     theme_advanced_buttons3: "bold,italic,underline,strikethrough,|,sub,sup",
     theme_advanced_buttons4: "charmap,|,link,unlink",
     paste_text_sticky : true,
     paste_text_sticky_default : true,
     theme_advanced_toolbar_align: "center",
     theme_advanced_statusbar_location: "none",
     theme_advanced_resizing: true,
     forced_root_block: false,
     force_br_newlines: true,
     force_p_newlines: false,
     language: "en"
 });
}
