String.prototype.capitalize = function(){
  return this.replace( /(^|[_\-])([a-z])/g , function(m,p1,p2){ return p1+p2.toUpperCase(); } );
};

// Start Modal //
$('body').append('<a class="cmsLogout">LOGOUT</a>');
$('body.cms').prepend('<a class="cmsEditMetadata">METADATA</a>');

$('body .cmsGroup').prepend('<a class="cmsGroupAdd">ADD</a>');

$('body .cms.cmsType_TextSingle').each(function(){
 $(this).prepend('<a class="cmsEdit  '+ getCMSname($(this).attr('class')) +'">EDIT</a>');
})
$('body .cms.cmsType_TextMulti').each(function(){
 $(this).prepend('<a class="cmsEdit  '+ getCMSname($(this).attr('class')) +'">EDIT</a>');
})
$('body .cms.cmsType_Textset').each(function(){
 $(this).prepend('<a class="cmsEdit  '+ getCMSname($(this).attr('class')) +'">EDIT</a>');
})
$('body .cms.cmsType_Rich').each(function(){
 $(this).prepend('<a class="cmsEdit  '+ getCMSname($(this).attr('class')) +'">EDIT</a>');
})
$('.cms.cmsType_Image').each(function(){
 $(this).before('<a class="cmsEdit  '+ getCMSname($(this).attr('class')) +'">EDIT</a>');
})
$('.cms.cmsType_File').each(function(){
 $(this).before('<a class="cmsEdit  '+ getCMSname($(this).attr('class')) +'">EDIT</a>');
})

$('body').append('<div id="cmsModalIframe"><iframe width="100" height="100" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src="/assets/cms/loading.gif"></iframe></div><div id="cmsModalOverlay"></div><div id="cmsModalLoading"></div>');

function cmsModalShow() {
 $('#cmsModalLoading').animate({opacity: 0}, 400, function() {$('#cmsModalLoading').hide()});

 var x = ($('body').width() / 2 ) - ($('#cmsModalIframe').outerWidth()/2);
 $('#cmsModalIframe').css('left',x+'px').css('visibility', 'visible').css('display','block').css('opacity','0').animate({'opacity': 1},800);
}

function cmsModalUpdated() {
 $('#cmsModalIframe').fadeOut(400);
 $('#cmsModalOverlay').animate({opacity: 1}, 400, function() {document.location=document.location});
 return true;
}

function cmsModalClose(x) {
 if(x) {
  $('#cmsModalIframe').fadeOut(400);
  $('#cmsModalOverlay').animate({opacity: 0}, 400, function() {$('#cmsModalOverlay').hide()});
  return false;
 }
 var answer = confirm('Any changes you made will be lost unless you save them.')
 if (answer) {
  $('#cmsModalIframe').fadeOut(400);
  $('#cmsModalOverlay').animate({opacity: 0}, 400, function() {$('#cmsModalOverlay').hide()});
 }
 return false;
}

$(document).keyup(function(e) {
 if (e.keyCode == 27 && $('#cmsModalIframe').is(':visible')) {
  cmsModalClose();
  return false;
 }
});

$('a.cmsLogout').click(function () {
 $.get('/assets/cms/logout.aspx?v=' + new Date().getTime(), function(data) {
  document.location=document.location;
 });
 return false;
});

$('a.cmsEdit, a.cmsEditMetadata').click(function () {
 if($(this).parent().hasClass('cms')) var obj = $(this).parent();
 else if($(this).next().hasClass('cms')) var obj = $(this).next();

 var WidthHeight = '';
 if( getVal('cmsWidth_', obj.attr('class')) || getVal('cmsHeight_', obj.attr('class'))) {
  WidthHeight = '&width=' + getVal('cmsWidth_', obj.attr('class')) + '&height=' + getVal('cmsHeight_', obj.attr('class'));
 }

 var h = 400;
 h= $(window).height() - 170;

 if ($(this).hasClass('cmsEditMetadata')) h=406;
 else if (getVal('cmsType_', obj.attr('class'))=='TextSingle') h=180;
 else if (getVal('cmsType_', obj.attr('class'))=='TextMulti') h=400;
 else if (getVal('cmsType_', obj.attr('class'))=='Image') h=210;
 else if (getVal('cmsType_', obj.attr('class'))=='File') h=310;
 else if (getVal('cmsType_', obj.attr('class'))=='Rich') h=450;

 var t = '';
 var n = '';
 var p = '';

 if ($(this).hasClass('cmsEditMetadata')) {
  var t = 'Metadata';
  var p = getPage(document.location); 
  if(p) {
   var n = String(p.replace(/\.[^/.]+$/, "")).capitalize().replace(/-/g,' ');
   p = String(p.replace(/\.[^/.]+$/, ""));
  }
  else var n = String(window.location.hostname);
 }

 else {
  var t = getVal('cmsType_', String(obj.attr('class')));
  var n = getVal('cmsName_', String(obj.attr('class')));
 }

 $('#cmsModalLoading').css('opacity','.4').show().animate({'opacity': 1},300);
 $('#cmsModalOverlay').css('opacity', '0').show().animate({'opacity': .8},300);
 $('#cmsModalIframe').css('display', 'block').css('visibility','visible').css('opacity','0');
 $('#cmsModalIframe iframe:eq(0)').attr('width','800').attr('height',h).attr('src','/assets/cms/cms.html?name='+ encodeURIComponent(n) + WidthHeight + '&type=' + encodeURIComponent(t) + '&page=' + encodeURIComponent(p) );
 return false;
});

function getVal (prefix, str) {
 return String(new RegExp(prefix + "[\\w]*").exec(str)).substr(prefix.length)
}

// End Modal //

// Start Login //

if(window.location.search.substr(0,5)=='?edit' && readCookie('SiteEdit')!='1') {
 $('.cmsEdit, .cmsEditMetadata, .cmsLogout').remove();
 $('.cms').removeClass('cms');
 setTimeout("cmsLogin()",800);
}

else if( window.location.search.substr(0,5)=='?edit') {
 document.location=String(document.location).split('?')[0].split('#')[0];
}

function cmsLogin () {
 $('#cmsModalLoading').css('opacity','.3').show().animate({'opacity': 1},300);
 $('#cmsModalOverlay').css('opacity', '0').show().animate({'opacity': .8},300);
 $('#cmsModalIframe').css('display', 'block').css('visibility','visible').css('opacity','0');
 $('#cmsModalIframe iframe:eq(0)').attr('width','800').attr('height','240').attr('src','/assets/cms/login.html');
 return false;
}

// End Login //

function getPage () {
 var nPage = window.location.pathname;
 nPage = nPage.substring(nPage.lastIndexOf('/') + 1);
 //nPage = nPage.substring(0, nPage.indexOf('.'));
 return nPage;
}

function getCMSname(str) {
 return str.match(/cmsName_[0-9a-zA-Z\-_]+/)[0];
}