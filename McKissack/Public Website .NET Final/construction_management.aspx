<%@ Page Language="VB" AutoEventWireup="false" CodeFile="construction_management.aspx.vb" Inherits="_construction_management" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Program &#38; Construction Management: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 382px;">
			<div class="TwoColumns">
				<div class="col1">
					<div id="sideMenu">
						<p>
							<a href="./services_overview.aspx" >Overview</a>
						</p>
						<p>
							<a href="./ai.aspx" >Architecture &#38; Interiors</a>
						</p>
						<p>
							<a href="./construction_management.aspx" class="selected">Program &#38; Construction Management</a>
						</p>
						<p>
							<a href="./planning.aspx">Planning &#38; Facilities Management</a>
						</p>
						<p>
							<a href="./environmental.aspx">Environmental Engineering</a>
						</p>
						<p>
							<a href="./transportation.aspx">Transportation</a>
						</p>
					</div>
				</div>
				<div class="col2 wide">
					<h1>Program &#38; Construction Management</h1>
					<p>
						McKissack &#38; McKissack, as the program manager for large and complex projects, provides single-source capabilities for managing projects from initial planning through occupancy and operation. Throughout the lifecycle of projects, thousands of activities must be addressed and integrated in a timely and strategic manner to achieve success. Projects today are more technical, and clients demand higher quality within tighter budgets and shorter time frames. Decisions need to be made proactively, responding to all project stakeholders and considering all program requirements.  At McKissack & McKissack, we have a proven history of successfully answering these challenges for both new construction and renovation, in both public and private sectors.  Services include:
					</p>
					<p>
						 Program Development :: Design :: Procurement ::  Construction :: Closeout :: Relocation
					</p>
					<h2 class="dotted">Featured Projects <a href="search_results.aspx?keyword=&tid=&sid=2400159">View All</a></h2>
					<div class="featured">
						
						<asp:Repeater ID="rptFeatured" runat="server" OnItemDataBound="rptFeatured_ItemDataBound">
						<ItemTemplate>
					    <asp:literal ID="ltrlclasstag" runat="server"></asp:literal>
						<a href="project.aspx?pid=<%#Container.DataItem("projectid")%>">
						<img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("projectid")%>&type=project&crop=1&size=1&height=162&width=229" alt="<%#Container.DataItem("name")%>" title="<%#Container.DataItem("name").trim%>"  />
						<strong><%#Container.DataItem("name")%></strong><span></span>
						</a>
					
					</ItemTemplate>
					</asp:Repeater>
						<div class="c"></div>
					</div>
				</div>
				<div class="c"></div>
			</div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address>
				<p>
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
<a href="about.aspx" >About Us</a>
			<a href="services_overview.aspx" class="selected">Services</a>
			<a href="search_results.aspx" >Portfolio</a>
			<a href="culture.aspx">Careers</a>
			<a href="./PressReleasesHeadlines.aspx">Press Room</a>
			<a href="contact.aspx">Offices</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/services_pmcm.jpg'); height: 382px"></div>
</body>
</html>
