Imports System.Data.SqlClient
Imports System.Data

Partial Class _PressKit
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim rptPresskit As Repeater = FindControl("RepeaterPresskit")
        Dim rptContact As Repeater = FindControl("RepeaterContact")
	Dim sql as String = "SELECT b.NAME, a.Asset_ID, ISNULL(c.Item_Value, '0') as PrimaryItem FROM IPM_ASSET a JOIN IPM_ASSET_CATEGORY b ON a.Category_ID = b.CATEGORY_ID  LEFT JOIN IPM_ASSET_FIELD_VALUE c ON a.Asset_ID = c. Asset_ID AND c.Item_ID = 2401563 LEFT JOIN IPM_ASSET_FIELD_VALUE d on a.Asset_ID = d.Asset_ID AND d.Item_ID = 2401564 WHERE a.PROJECTID = 2401457 AND a.Available = 'Y' AND b.Available = 'Y' Order By d.Item_Value"

        Dim DT1 As New DataTable("FProjects")
        DT1 = mmfunctions.GetDataTable(sql)
	rptPresskit.DataSource = DT1 
        rptPresskit.DataBind()
	sql = "SELECT b.FirstName, b.LastName, b.Position, b.Email, b.Phone, c.Item_value Fax, b.WorkAddress, b.WorkCity, b.WorkState, b.WorkZip  FROM IPM_PROJECT_CONTACT a JOIN IPM_USER b ON a.UserID = b.UserID Left JOIN IPM_USER_FIELD_VALUE c ON a.UserID = c.User_ID AND c.Item_ID = 2401566 WHERE a.ProjectID = 2401457"
        Dim DT2 As New DataTable("FProjects")
        DT2 = mmfunctions.GetDataTable(sql)
        rptContact.DataSource = DT2
        rptContact.DataBind()
    End Sub


    Protected Sub RepeaterContact_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepeaterContact.ItemDataBound
	If e.Item.ItemType = ListItemType.Item Then 

	        Dim ContactText As Literal = e.Item.FindControl("ContactText")
		Dim theText as String
		theText = "<p>" & e.Item.DataItem("FirstName") & " " & e.Item.DataItem("LastName") & "<br />"
		If NOT IsDBNull(e.Item.DataItem("Position")) Then
	        	If NOT String.IsNullOrEmpty(e.Item.DataItem("Position"))  Then
        		    theText += e.Item.DataItem("Position") & "<br />"
        		End If
		End If
		If NOT IsDBNull(e.Item.DataItem("WorkAddress")) Then
	        	If NOT String.IsNullOrEmpty(e.Item.DataItem("WorkAddress"))  Then
        		    theText += e.Item.DataItem("WorkAddress") & "</a><br />"
        		End If
		End If
		If NOT IsDBNull(e.Item.DataItem("WorkCity")) Then
	        	If NOT String.IsNullOrEmpty(e.Item.DataItem("WorkCity"))  Then
        		    theText += e.Item.DataItem("WorkCity")
        		End If
		End If
		If NOT IsDBNull(e.Item.DataItem("WorkState")) Then
	        	If NOT String.IsNullOrEmpty(e.Item.DataItem("WorkState"))  Then
        		    theText += " " & e.Item.DataItem("WorkState")
        		End If
		End If
		If NOT IsDBNull(e.Item.DataItem("WorkZip")) Then
	        	If NOT String.IsNullOrEmpty(e.Item.DataItem("WorkZip"))  Then
        		    theText += " " & e.Item.DataItem("WorkZip")
        		End If
		End If
		If NOT IsDBNull(e.Item.DataItem("Phone")) Then
	        	If NOT String.IsNullOrEmpty(e.Item.DataItem("Phone"))  Then
        		    theText += "<br />T " & e.Item.DataItem("Phone")
        		End If
		End If




		If NOT IsDBNull(e.Item.DataItem("Fax")) Then
	        	If NOT String.IsNullOrEmpty(e.Item.DataItem("Fax"))  Then
        		    theText += "<br />F " & e.Item.DataItem("Fax")
        		End If
		End If




		If NOT IsDBNull(e.Item.DataItem("Email")) Then
	        	If NOT String.IsNullOrEmpty(e.Item.DataItem("Email"))  Then
        		    theText += "<br /><a href=""mailto:" & e.Item.DataItem("Email") & """ >" & e.Item.DataItem("Email") & "</a>"
        		End If
		End If


		theText += "<br /><br /></p>"

		ContactText.Text = theText
	End If

    End Sub


End Class
