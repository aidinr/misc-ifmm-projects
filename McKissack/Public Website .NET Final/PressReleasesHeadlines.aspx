﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PressReleasesHeadlines.aspx.vb" Inherits="_PressReleasesHeadlines" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<title>Press Releases &#38; Headlines: McKissack &#38; McKissack</title> 
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" /> 
<link rel="stylesheet" type="text/css" href="assets/css/main.css" /> 
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> 
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script> 

<style type="text/css">

.sub.press .ThreeColumns .col2 p.headerImg {
 display: block;
 margin: 0;
 padding: 22px 0 0 0;
 width: 400px;
 max-height: 200px;
 overflow: hidden;
}

.sub.press .ThreeColumns .col2 p.headerImg img {
 display: block;
 margin: 0;
 padding: 0;
 border: 0;
 cursor: default;
 max-width: 400px;
 max-height: 200px;
}

.sub.press .ThreeColumns .col2 h2 {
 color: #616161;
}

.sub.press .ThreeColumns .col2 h3 {
 color: #616161;
 margin: 0 0 0 0;
 padding: 0 0 0 0;
}

.sub.press .ThreeColumns .col2 #RelatedProjects p {
 margin: 8px 0 0 0;
 border-top: 1px dotted #BFBFBF;
 position: relative;
 padding: 0;
}

.sub.press .ThreeColumns .col2 #RelatedProjects p a, .sub.press .ThreeColumns .col2 #RelatedProjects p a:link, .sub.press .ThreeColumns .col2 #RelatedProjects p a:visited {
 border: 0;
 padding: 10px 0 0 0;
 display:  block;
 position: relative;
}

.sub.press .ThreeColumns .col2 #RelatedProjects p img {
 margin: 0;
 padding: 0;
 border: 1px solid #a1a1a1;
 display: block;
 width: 82px;
}

.sub.press .ThreeColumns .col2 #RelatedProjects p a:hover img {
 border: 1px solid #F09D19;
}
.sub.press .ThreeColumns .col2 #RelatedProjects p strong {
 margin: 0;
 padding: 0;
 color: #F09D19;
 display: block;
 width: 300px;
 font-size: 13px;
 line-height: 16px;
 position: absolute;
 left: 100px;
 top: 7px;
}

.sub.press .ThreeColumns .col2 #RelatedProjects p a:hover strong {
 text-decoration: underline;
}

.sub.press .ThreeColumns .col3 p a, .sub.press .ThreeColumns .col3 p a:link, .sub.press .ThreeColumns .col3 p a:visited {
 color: #fff;
 text-decoration: underline;
}

.sub.press .ThreeColumns .col3 p a:hover {
 color: #935B03;
}

.sub.press .ThreeColumns .col3 #NewsArchiveWidget {
 visibility: hidden;
}
.sub.press .ThreeColumns .col3 #NewsArchiveWidget ul#ScrollUpDown {
 position: relative;
 width: 254px;
 list-style: none;
 margin: 0 auto;
 padding: 0 0 6px 0;
 background: #CF8617;
}

.sub.press .ThreeColumns .col3 #NewsArchiveWidget ul#ScrollUpDown li {
 margin: 0 6px 0 6px;
 border-bottom: 1px dotted #E09928;
 padding: 6px 0 6px 0;
 line-height: 17px;
 display: block;
}

.sub.press .ThreeColumns .col3 #NewsArchiveWidget ul#ScrollUpDown li a {
 dispaly: block;
}

.sub.press .ThreeColumns .col3 #NewsArchiveWidget ul#ScrollUpDown li:hover, .sub.press .ThreeColumns .col3 #NewsArchiveWidget ul#ScrollUpDown li a:hover {
 background: #DF9728;
}

.sub.press .ThreeColumns .col3 #NewsArchiveWidget ul#ScrollUpDown li strong {
 display: block;
 font-style: normal;
 font-size: 11px;
 line-height: 11px;
 padding: 0 0 4px 0;
 color: #9D5E0D;
 text-transform: uppercase;
}

.sub.press .ThreeColumns .col3 #NewsArchiveWidget ul#ScrollUpDown li span {
 display: block;
 font-style: normal;
 font-size: 13px;
 line-height: 17px;
 color: #FBE9D9;
 padding: 0 10px 0 0;
}

.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetUp {
 background: #D89021;
 width: 242px;
 margin: 0 auto 0 auto;
 border: 6px solid #CF8617;
 height: 20px;
 cursor: default;
}

.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetDown {
 background: #D89021;
 width: 242px;
 margin: 0 auto 0 auto;
 border: 6px solid #CF8617;
 height: 20px;
 cursor: default;
}

.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetUp a {
 display: block;
 visibility: hidden;
 cursor: pointer;
 margin: 0;
 padding: 0;
 height: 20px;
 background: #DF9728 url(http://codebonus.com/clients/mckissack/NewsUpDown.gif) no-repeat scroll 50% 6px;
}

.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetDown a {
 display: block;
 cursor: pointer;
 margin: 0;
 padding: 0;
 height: 20px;
 background: #DF9728 url(http://codebonus.com/clients/mckissack/NewsUpDown.gif) no-repeat scroll 50% -34px;
}

.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetUp a:hover, .sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetDown a:hover {
background-color: #E8991C;
}

#dropSelectDiv {
border: 1px solid #bababa;
width: 139px;
margin: 16px 0 10px 16px;
}

#dropSelectDiv * {
list-style: none;
}

ul#dropSelect {
margin:0;
padding:0;
list-style: none;
z-index: 990;
position: relative;
background:#EBEAE8;
width: 139px;
}

#dropSelect li {
padding: 0;
margin: 0;
z-index: 992;
width: 139px;
}

#dropSelect li a, #dropSelect li a:link, #dropSelect li a:visited {
display:block;
background:#EBEAE8;
text-decoration:none;
color:#333;
white-space:nowrap;
padding:5px 12px;
cursor: default;
font-style: normal;
}

#dropSelect li li a {
cursor: pointer;
}

#dropSelect li a:hover {
background:#E8D5A8;
}

#dropSelect li ul {
position:absolute;
visibility:hidden;
margin:0 0 0 -1px;
padding:0;
border: 1px solid #bababa;
}

</style>

</head> 
<body> 
<div id="page" class="sub press"> 
	<div id="bg"> 
		<div id="wrapper" style="margin-top: 382px;"> 
			<div class="ThreeColumns"> 
				<div class="col1"> 
					<div id="sideMenu"> 
						<p> 
							<a href="PressReleasesHeadlines.aspx" title="Press Releases &#38; Headlines" class="selected">Press Releases &#38; Headlines</a> 
						</p> 
						<p> 
							<a href="MediaCoverage.aspx" title="Media Coverage">Media Coverage</a> 
						</p> 
						<p> 
							<a href="PressKit.aspx" title="Press Kit">Press Kit</a> 
						</p> 
					</div> 
				</div> 
				<div class="col2"> 
					<h1>Press Releases &#38; Headlines</h1>

					<asp:Repeater ID="RepeaterNews" runat="server" >
					<ItemTemplate>

					<asp:Literal ID=newsimage runat=server></asp:Literal>

					<p class="headerDate">
					<%#Container.DataItem("NewsDate")%>
					</p> 

					<h2><%#Container.DataItem("Headline")%></h2> 



<p> 
<%# GetTitleText(Container.DataItem("PublicationTitle")) %>
<%#Container.DataItem("Content")%>
</p> 

					</ItemTemplate>
					</asp:Repeater> 


					<div id="RelatedProjects">

					<asp:Repeater ID="RepeaterProjects" runat="server" >
					<HeaderTemplate>
					<h2>Related Project(s)</h2>
					</HeaderTemplate> 
					<ItemTemplate>
					<p> 
					 <a href="project.aspx?pid=<%#Container.DataItem("ProjectId")%>">
<img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("projectid")%>&type=project&crop=1&size=1&height=162&width=229" alt="<%#Container.DataItem("name")%>" title="<%#Container.DataItem("name").trim%>"  />
					 <strong><%#Container.DataItem("name").trim%></strong>
					 </a>
					</p> 
					</ItemTemplate>
					</asp:Repeater>

					</div> 

				</div> 
				<div class="col3"> 
					<div class="pad75"> 
					</div> 
					<h1>Contact Information</h1> 
					<p> 
					<asp:Repeater ID="RepeaterContact" runat="server" >
					<ItemTemplate>
					<asp:Literal ID=contact runat=server></asp:Literal>
					</ItemTemplate>
					</asp:Repeater>
					<br />
					<br />
					</p>

					<div id="NewsArchiveWidget"> 
					<h1>News Archive</h1> 

					<div id="dropSelectDiv">
					<ul id="dropSelect">
					    <li><a>Select Year</a>
					        <ul>
					        </ul>
					    </li>
					</ul>
					</div>

					<asp:Repeater ID="RepeaterArchive" runat="server" >
					<HeaderTemplate>
					<ul id="ScrollUpDown">
					</HeaderTemplate> 
					<ItemTemplate>
					<li>
					 <a href="PressReleasesHeadlines.aspx?NewsID=<%#Container.DataItem("News_Id")%>">
					 <strong><%#Container.DataItem("NewsDate")%></strong>
					 <span><%#Container.DataItem("Headline")%></span>
					 </a>
					</li>
					</ItemTemplate>
					<FooterTemplate>
					</ul>
					</FooterTemplate>
					</asp:Repeater>

					</div> 

				</div> 
				<div class="c"></div> 
			</div> 
		</div> 
		<div id="footer"> 
			<div class="footer"> 
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address> 
				<p> 
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&#38;VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a> 
				</p> 
				<div><a class="up"></a></div> 
			</div> 
		</div> 
	</div> 
	<div id="header"> 
		<div> 
			<p id="logoType"> 
				<a href="/" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a> 
			</p> 
			<p id="logoIcon"> 
				<a href="/" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a> 
			</p> 
			<p id="menu"> 
				<a href="./about.aspx">About Us</a> 
				<a href="./services_overview.aspx">Services</a> 
				<a href="./search_results.aspx">Portfolio</a> 
				<a href="./culture.aspx">Careers</a> 
				<a href="./PressReleasesHeadlines.aspx" class="selected">Press Room</a> 
				<a href="./contact.aspx">Offices</a> 
			</p> 
		</div> 
	</div> 

</div> 

<script type="text/javascript">
$('.col2').css('min-height', '740px');
var nodup = [];
$('#ScrollUpDown li strong').each(function () {
    var y = $(this).html()
    y = y.substring(y.length - 4);
    if (!nodup[y]) {
        nodup[y] = true;
        $('#dropSelect li ul').append('<li><a class="dateScroll">' + y + ' News Items</a></li>');
    }
});
var pPos = new Array();
var iii = 0;
$('#NewsArchiveWidget ul#ScrollUpDown li').each(function () {
    pPos[iii++] = 0 - Number($(this).position().top);
});
$('.dateScroll').click(function () {
    idx = $($("#NewsArchiveWidget ul#ScrollUpDown li strong:contains(" + $(this).text().replace(/\D/g, "") + ")").first().parent().parent()).index()
    var p = pPos[idx];
    $('#NewsArchiveWidget ul#ScrollUpDown li').first().css('marginTop', p + 'px');
    if ($('#ScrollUpDown').height() < Math.abs(p)) {
        $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetDown a').hide();
    } else {
        $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetDown a').css('visibility', 'visible');
        $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetDown a').show();
    }
    if (p < -2) {
        $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetUp a').css('visibility', 'visible');
        $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetUp a').show();
    }
    $('#dropSelect li a').first().html($(this).html())
    dropSelect_close();
});
if ($('#NewsArchiveWidget').html()) {
    $('#NewsArchiveWidget ul#ScrollUpDown').first().before('<div id="NewsArchiveWidgetUp"><a></a></div>');
    $('#NewsArchiveWidget ul#ScrollUpDown').first().after('<div id="NewsArchiveWidgetDown"><a></a></div>');
    var moving = 0;
    var idx = 0;
    var positionMaxHeight = $('#NewsArchiveWidget ul#ScrollUpDown').height();
    $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget ul#ScrollUpDown').css('overflow', 'hidden');
    $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget ul#ScrollUpDown').css('height', '245px');
    $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget').css('visibility', 'visible');
    $('#NewsArchiveWidgetUp a').click(function () {
        if (moving == 1) return false;
        if (idx < 2) $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetUp a').fadeOut();
        if ($('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetDown a').css('display') == 'none') $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetDown a').fadeIn();
        if (Math.abs($('#NewsArchiveWidget ul#ScrollUpDown li').first().css('marginTop').replace('px', '')) > 0) {
            moving = 1;
            // get the correct index
            --idx;
            // get the li height
            var liheight = $('#NewsArchiveWidget ul#ScrollUpDown li:eq(' + idx + ')').outerHeight();
            $('#NewsArchiveWidget ul#ScrollUpDown li').first().animate({
                marginTop: '+=' + liheight
            }, 400, function () {
                moving = 0;
            });
        }
    });
    $('#NewsArchiveWidgetDown a').click(function () {
        if (moving == 1) return false;
        if ($('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetUp a').css('visibility') == 'hidden' || $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetUp a').css('display') == 'none') {
            $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetUp a').hide();
            $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetUp a').css('visibility', 'visible');
            $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetUp a').fadeIn();
        }
        if (Math.abs($('#NewsArchiveWidget ul#ScrollUpDown li').first().css('marginTop').replace('px', '')) < (positionMaxHeight - Number($('#NewsArchiveWidget ul#ScrollUpDown').height()))) {
            moving = 1;
            // get the li height
            var liheight = $('#NewsArchiveWidget ul#ScrollUpDown li:eq(' + idx + ')').outerHeight();
            // increase the li index
            ++idx;
            $('#NewsArchiveWidget ul#ScrollUpDown li').first().animate({
                marginTop: '-=' + liheight
            }, 400, function () {
                moving = 0;
            });
        } else $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetDown a').fadeOut();
    });
}
var timeout = 500;
var closetimer = 0;
var ddmenuitem = 0;

function dropSelect_open() {
    dropSelect_canceltimer();
    dropSelect_close();
    ddmenuitem = $(this).find('ul').css('visibility', 'visible');
}

function dropSelect_close() {
    if (ddmenuitem) ddmenuitem.css('visibility', 'hidden');
}

function dropSelect_timer() {
    closetimer = window.setTimeout(dropSelect_close, timeout);
}

function dropSelect_canceltimer() {
    if (closetimer) {
        window.clearTimeout(closetimer);
        closetimer = null;
    }
}
$('#dropSelect > li').bind('mouseover', dropSelect_open);
$('#dropSelect > li').bind('mouseout', dropSelect_timer);

function findYear(y) {
    return /\d{4}/.exec(y)[0];
}
var thisYear = findYear($('.headerDate').first().html());
idx = $($("#NewsArchiveWidget ul#ScrollUpDown li strong:contains(" + thisYear + ")").first().parent().parent()).index()
var p = pPos[idx];
$('#NewsArchiveWidget ul#ScrollUpDown li').first().css('marginTop', p + 'px');
if ($('#ScrollUpDown').height() < Math.abs(p)) {
    $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetDown a').hide();
} else {
    $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetDown a').css('visibility', 'visible');
    $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetDown a').show();
}
if (p < -2) {
    $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetUp a').css('visibility', 'visible');
    $('.sub.press .ThreeColumns .col3 #NewsArchiveWidget #NewsArchiveWidgetUp a').show();
}
</script>

<div id="bannerSub" style="background-image:url('/assets/images/dynamic/headerPressReleaseHeadlines.jpg'); height: 382px"></div> 
</body> 
</html>
