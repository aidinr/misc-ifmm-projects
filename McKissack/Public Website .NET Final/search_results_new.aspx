<%@ Page Language="VB" AutoEventWireup="false" CodeFile="search_results.aspx.vb" Inherits="_Search_Results" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Search Results: McKissack &#38; McKissack</title>
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/jquery.carousel.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
</head>
<body>
<div id="page" class="sub">
	<div id="bg">
		<div id="wrapper" style="margin-top: 142px;">
			<div class="results">
				<div class="col1">
					<h1>Portfolio</h1>
					<form  id="PortfolioSearchTopLeft" onsubmit="return false;">
						<div>
							<!--<input type="text" id="PortfolioSearchTopLeftText" value="<%=System.Web.HttpUtility.UrlDecode(request.querystring("keyword")) %>" onkeydown="javascript:if(event.keyCode==13) {location.href='search_results.aspx?keyword='+PortfolioSearchTopLeftText.value+'&sid='+getParameter('sid')+'&tid='+getParameter('tid'); return false}" />-->
							<input type="text" id="PortfolioSearchTopLeftText" value="<%=System.Web.HttpUtility.UrlDecode(request.querystring("keyword")) %>" onkeydown="javascript:if(event.keyCode==13) {location.href='search_results.aspx?keyword='+PortfolioSearchTopLeftText.value+'&sid=&tid='; return false}" />
							<!--<input type="button" id="PortfolioSearchTopLeftSubmit" value="Go" onclick="location.href='search_results.aspx?keyword='+PortfolioSearchTopLeftText.value+'&sid='+getParameter('sid')+'&tid='+getParameter('tid');" />-->
							<input type="button" id="PortfolioSearchTopLeftSubmit" value="Go" onclick="location.href='search_results.aspx?keyword='+PortfolioSearchTopLeftText.value+'&sid=&tid=';" />
							
						</div>
					</form>
					<h2>View by service <a href="search_results.aspx">Clear Filters</a></h2>
					<asp:Literal ID=literalServices runat=server></asp:Literal>
					
					<h2>View by project type <a href="search_results.aspx">Clear Filters</a></h2>
					<asp:Literal ID=literalType runat=server></asp:Literal>
					
				</div>
				<div class="col2">
					<div id="viewSelect">
						<p id="viewMode">View Mode<a href="#" id="SelectviewModeGrid" class="selected">Grid</a><a href="#" id="SelectviewModeList">List</a></p>
						<p id="pageNumberArea"><asp:literal ID=literalPaging runat=server></asp:literal></p>
					</div>
					<p id="breadcrumbs">Search Results: <asp:Literal ID="ltrlbreadcrumbs" runat=server></asp:Literal></p>
				</div>
				<div id="portfolioResults" class="grid">
					<h1><a href="javascript:location.href='search_results.aspx?keyword=<%=System.Web.HttpUtility.UrlDecode(request.querystring("keyword")) %>&sid='+getParameter('sid')+'&tid='+getParameter('tid')+'&sort=name';" class="projectName">Project Name</a><a href="#" class="projectType">Type</a><a href="javascript:location.href='search_results.aspx?keyword=<%=System.Web.HttpUtility.UrlDecode(request.querystring("keyword")) %>&sid='+getParameter('sid')+'&tid='+getParameter('tid')+'&sort=city';" class="projectLocation">Location</a></h1>
					
					<asp:Literal ID=ltrlnoresults runat=server></asp:Literal>
					<asp:Repeater ID=rptGrid runat=server OnItemDataBound="rptGridDataBound">
					<ItemTemplate>
					<p>
						<a href="project.aspx?pid=<%#Container.DataItem("projectid")%>">
						<img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("projectid")%>&type=project&crop=1&size=1&height=82&width=117" alt="" title="" class="small" />
						<img src="<%=Session("WSRetreiveAsset") %>id=<%#Container.DataItem("projectid")%>&type=project&crop=1&size=1&height=300&width=325" alt="" title="" class="medium" />
						<strong><%#Container.DataItem("name")%></strong>
						<em><asp:Literal ID=ltrlType runat=server></asp:Literal></em>
						<span><%#Container.DataItem("city")%>, <%#Container.DataItem("state_id")%></span>
						</a>
					</p>
					</ItemTemplate>
					</asp:Repeater>
				
				</div>
					
					<div class="col2">
					<div id="viewSelect">
						<!--<p id="viewMode">View Mode<a href="#" id="SelectviewModeGrid" class="selected">Grid</a><a href="#" id="SelectviewModeList">List</a></p>-->
						<p id="pageNumberArea"><asp:literal ID=literalPaging2 runat=server></asp:literal></p>
					</div>
					<!--<p id="breadcrumbs">Search Results: <asp:Literal ID="ltrlbreadcrumbs2" runat=server></asp:Literal></p>-->
				</div>
							
					<!-- <asp:literal ID=literalPagingBottom runat=server></asp:literal> -->
				
				
				
			</div>
			<div class="c"></div>
		</div>
		<div id="footer">
			<div class="footer">
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address>
				<p>
					<a href="#">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a>
				</p>
				<div><a class="up"></a></div>
			</div>
		</div>
	</div>
</div>
<div id="header">
	<div>
		<p id="logoType">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="logoIcon">
			<a href="index.aspx" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a>
		</p>
		<p id="menu">
				<a href="./about.aspx">About Us</a>
				<a href="./services_overview.aspx">Services</a>
				<a href="./search_results.aspx" class="selected">Portfolio</a>
				<a href="./culture.aspx">Careers</a>
				<a href="./PressReleasesHeadlines.aspx">Press Room</a>
				<a href="./contact.aspx">Offices</a>
		</p>
	</div>
</div>
<div id="bannerSub" style="background-image:url('assets/images/dynamic/banner4.png'); height: 142px"></div>

<script type="text/javascript" >

    function getParameter(parameterName) {
        try {
            var queryString = window.location.href;
            var parameterName = parameterName + "=";
            if (queryString.length > 0) {
                begin = queryString.indexOf(parameterName);
                if (begin != -1) {
                    begin += parameterName.length;
                    end = queryString.indexOf("&", begin);
                    if (end == -1) {
                        end = queryString.length
                    }
                    return unescape(queryString.substring(begin, end));
                }
                return "";
            }
        }
        catch (err) {
            //alert(err.description);
        }
    }
</script>
</body>
</html>
