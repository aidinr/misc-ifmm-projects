﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PressKit.aspx.vb" Inherits="_PressKit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<% 
Dim filepath As String = Server.MapPath("~") & "assets\cms\data\" 
%>
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<title><% Response.Write(CMSFunctions.GetMetaTitle(filepath, "presskit"))%></title>
<meta name="description" content="<% Response.Write(CMSFunctions.GetMetaDescription(filepath,"presskit")) %>" />      
<meta name="keywords" content="<% Response.Write(CMSFunctions.GetMetaKeywords(filepath,"presskit")) %>" />
<link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css" /> 
<link rel="stylesheet" type="text/css" href="assets/css/main.css" /> 
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> 
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script> 

<style type="text/css">


.kit .ThreeColumns .col2 h1 {
margin-bottom: 18px;
}

.kit .ThreeColumns .col2 p {
 margin: 0 20px 16px 2px;
 line-height: 20px;
 padding: 0 0 16px 0;
 border-bottom: 1px dotted #BFBFBF;
 font-weight: 600;
 color: #676767;
}

.kit .ThreeColumns .col2 p strong {
 display: inline;
 font-size: 18px;
 padding: 0 2px 0 0;
}

.col3 a, .col3 a:link, .col3 a:hover  {
 color: #fff;
 text-decoration: underline;
}

.col3 a:hover  {
 color: #935B03;
 text-decoration: underline;
}

.col2 p em {
 margin-left: 15px;
 font-style: normal;
}

.d {
display: none;
}

</style>

</head> 
<body class="cms"> 
<div id="page" class="sub kit"> 
	<div id="bg"> 
		<div id="wrapper" style="margin-top: 382px;"> 
			<div class="ThreeColumns"> 
				<div class="col1"> 
					<div id="sideMenu"> 
						<p> 
							<a href="PressReleasesHeadlines.aspx" title="Press Releases &#38; Headlines">Press Releases &#38; Headlines</a> 
						</p> 
						<p> 
							<a href="MediaCoverage.aspx" title="Media Coverage">Media Coverage</a> 
						</p> 
						<p> 
							<a href="PressKit.aspx" title="Press Kit" class="selected">Press Kit</a> 
						</p> 
					</div> 
				</div> 
				<div class="col2"> 
					<h1>Press Kit</h1>

					<asp:Repeater ID="RepeaterPresskit" runat="server" >
					<ItemTemplate>
					<p>
					<%# IIf ( Container.DataItem("PrimaryItem") = "1", "<strong>", "<em>")%> 
					<%#Container.DataItem("NAME")%>
					<%# IIf ( Container.DataItem("PrimaryItem") = "1", "</strong>", "</em>")%>
					&nbsp; / &nbsp; <a href="<%=Session("WSDownloadAsset") %>assetid=<%#Container.DataItem("Asset_ID")%>&dtype=assetdownload&size=0">Download PDF</a>
					</p>
					</ItemTemplate>
					</asp:Repeater>

				</div> 
				<div class="col3"> 
					<div class="pad75"> 
					</div> 

					<p class="cms cmsType_TextMulti cmsName_Press_Kit_Quote">
						 <% Response.Write(CMSFunctions.GetTextFormat(filepath, "Press_Kit_Quote"))%> 
					</p>

					
						<address><strong class="cms cmsType_TextMulti cmsName_Press_Kit_Quote_Name"><% Response.Write(CMSFunctions.GetTextFormat(filepath, "Press_Kit_Quote_Name"))%> </strong><br/><i class="cms cmsType_TextMulti cmsName_Press_Kit_Quote_Credentials"><% Response.Write(CMSFunctions.GetTextFormat(filepath, "Press_Kit_Quote_Credentials"))%> </i>

						</address>
										
					<asp:Repeater ID="RepeaterContact" runat="server">
					<HeaderTemplate>
					<h1>Contact Information</h1>
					</HeaderTemplate>
					<ItemTemplate> 
					<asp:Literal ID="ContactText" runat="server"></asp:Literal>
					</ItemTemplate>
					</asp:Repeater>
 
				</div> 
				<div class="c"></div> 
			</div> 
		</div> 
		<div id="footer"> 
			<div class="footer"> 
				<address><a href="http://www.mckissackdc.com/">McKissack &#38; McKissack</a></address> 
				<p> 
					<a href="https://www.designbidbuild.net/mckissack/index1.php?ID=McKissack&#38;VIEW=McKissack">Document Management</a> &#8195;|&#8195; <a class="up">Sitemap</a> 
				</p> 
				<div><a class="up"></a></div> 
			</div> 
		</div> 
	</div> 
	<div id="header"> 
		<div> 
			<p id="logoType"> 
				<a href="/" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a> 
			</p> 
			<p id="logoIcon"> 
				<a href="/" title="Home: McKissack &#38; McKissack">McKissack &#38; McKissack</a> 
			</p> 
			<p id="menu"> 
				<a href="./about.aspx">About Us</a> 
				<a href="./services_overview.aspx">Services</a> 
				<a href="./search_results.aspx">Portfolio</a> 
				<a href="./culture.aspx">Careers</a> 
				<a href="./PressReleasesHeadlines.aspx" class="selected">Press Room</a> 
				<a href="./contact.aspx">Offices</a> 
			</p> 
		</div> 
	</div> 

</div> 

<script type="text/javascript">

</script>

<div id="bannerSub" style="background-image:url('assets/images/dynamic/headerPressKit.jpg'); height: 382px"></div> 
</body> 
</html>
