history.navigationMode = 2;

function scrollMiddle() {
    scroll((1777 - Math.round(String($(window).width()).replace(/[^0-9]/g, ''))) / 2, 0)
}
$(window).unload(function () {
    $('body').val('');
    scrollMiddle();
});
$(document).ready(function () {

    scrollMiddle();
    $("a.gal").fancybox({
        'transitionIn': 'none',
        'transitionOut': 'none',
        'titlePosition': 'over',
        'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
            return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &emsp; <strong>' + title + '</strong>' : '') + (currentIndex > 0 ? '<em class="prev" onclick="$.fancybox.prev();">Prev</em>' : '') + (currentIndex + 1 < Math.round($('.gal').length) ? '<em class="next" onclick="$.fancybox.next();">Next</em>' : '') + '</span>';
        }
    });

    $("#contact a").first().fancybox({
        'type': 'iframe',
        'transitionIn': 'none',
        'transitionOut': 'none',
        'titlePosition': 'outside',
        'titleFormat': function () {return false;}
    });

});