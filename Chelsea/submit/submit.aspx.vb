﻿Imports System.Net.Mail
Imports System.IO
Imports System.Threading

Partial Class contact_submit
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim EmailBody As StringBuilder = New StringBuilder
        Dim message As MailMessage = New MailMessage(ConfigurationManager.AppSettings("ContactEmailFrom").ToString(), ConfigurationManager.AppSettings("ContactEmailTo").ToString())

            If String.IsNullOrEmpty(Request.Params("cf_email")) Then
            Response.Clear()
            Response.ClearHeaders()
            Response.ClearContent()
            Response.Write("-1")
            Response.Flush()
            Return
            Exit Sub
            End If

            If Not String.IsNullOrEmpty(Request.Params("cf_mail")) Then
            Response.Clear()
            Response.ClearHeaders()
            Response.ClearContent()
            Response.Write("-2")
            Response.Flush()
            Return
            Exit Sub
            End If

            If Not String.IsNullOrEmpty(Request.Params("cf_company")) Then
            Response.Clear()
            Response.ClearHeaders()
            Response.ClearContent()
            Response.Write("-2")
            Response.Flush()
            Return
            Exit Sub
            End If

            If Not String.IsNullOrEmpty(Request.Params("email")) Then
            Response.Clear()
            Response.ClearHeaders()
            Response.ClearContent()
            Response.Write("-2")
            Response.Flush()
            Return
            Exit Sub
            End If

        Try
            EmailBody.Append("First Name: " + Request.Params("cf_name_first") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Last Name: " + Request.Params("cf_name_last") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Email: " + Request.Params("cf_email") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Telephone Number: " + Request.Params("cf_phone") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Address: " + Request.Params("cf_address") + Environment.NewLine + Environment.NewLine)

            EmailBody.Append("IP Address: " + Request.ServerVariables("REMOTE_ADDR").ToString())

            'message.To.Add(ConfigurationManager.AppSettings("ContactEmailTo").ToString())
            message.IsBodyHtml = False
            message.Body = EmailBody.ToString()
            'message.From = New System.Net.Mail.MailAddress(ConfigurationManager.AppSettings("ContactEmailFrom").ToString())
            message.Subject = "Park Chelsea Contact Form Submision"

            Dim SmtpMail As New SmtpClient
            Dim basicAuthenticationInfo As New System.Net.NetworkCredential(ConfigurationManager.AppSettings("SMTPUser").ToString(), ConfigurationManager.AppSettings("SMTPPassword").ToString())

            SmtpMail.Host = ConfigurationManager.AppSettings("SMTPServer").ToString()
            SmtpMail.UseDefaultCredentials = False
            SmtpMail.Credentials = basicAuthenticationInfo
            SmtpMail.Send(message)
            WriteToFile()

            Dim Response As HttpResponse = Context.Response

            Response.Clear()
            Response.ClearContent()
            Response.ClearHeaders()
            Response.BufferOutput = False
            Response.ContentType = "text/html"
            Context.Response.Write("1")
            Response.Flush()
            Try
                Response.End()
            Catch ex As Exception

            End Try
            Thread.Sleep(5000)
        Catch ex As Exception
            Response.Clear()
            Response.ClearHeaders()
            Response.ClearContent()
            Response.Write("-9")
            Response.Write(ex.Message)
            Response.Flush()
            Try
                Response.End()
            Catch ex1 As Exception

            End Try
            Thread.Sleep(5000)
        End Try

    End Sub
    Public Sub WriteToFile()
        Try
            Dim FILE_NAME As String = Server.MapPath("./Contacts.txt")
            Dim Msg As String
            Msg = Request.Params("cf_name_first") & "|" & Request.Params("cf_name_last") & "|" & Request.Params("cf_email") & "|" & Request.Params("cf_phone") & "|" & Request.Params("cf_address") & "|"
            Msg += Request.ServerVariables("REMOTE_ADDR").ToString()
            Msg += Environment.NewLine

            If System.IO.File.Exists(FILE_NAME) = True Then
                Dim file_stream As FileStream = File.Open(FILE_NAME, FileMode.Append)
                Dim bytes As Byte() = New UTF8Encoding().GetBytes(Msg)
                file_stream.Write(bytes, 0, bytes.Length)
                file_stream.Flush()
                file_stream.Close()
            End If
        Catch ex As Exception

        End Try
      
    End Sub
End Class
