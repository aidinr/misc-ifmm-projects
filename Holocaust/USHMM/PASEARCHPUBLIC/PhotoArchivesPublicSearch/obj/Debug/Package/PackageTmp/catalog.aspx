﻿<%@ Page Language="vb" AutoEventWireup="false" Debug="true" CodeBehind="catalog.aspx.vb" Inherits="PhotoArchivesPublicSearch.catalog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" style="background:#ffffff;" >
<head runat="server">
 <title>Research | Collections | Photo Archives Online Catalog</title>
<link type="text/css" rel="stylesheet" href="css/normal_body_new.css" />
<link type="text/css" rel="stylesheet" href="css/style_new.css" />
<link type="text/css" rel="stylesheet" href="css/style2_new.css" />
<link type="text/css" rel="stylesheet" href="css/style3_new.css" />

<link rel="stylesheet" media="print" title="print" href="css/normal_body_print_new.css" />

<script type="text/javascript">
<!--
    function openPopup(cUrl, iwidth, iheight, cWindow) {
        if ((cWindow == null) || isWhiteSpace(cWindow))
            cWindow = "popWin";


        newWindow = window.open(cUrl, cWindow, 'width=' + iwidth + ', height=' + iheight + ', top=' + (screen.height - iheight) / 2 + ', left=' + (screen.width - iwidth) / 2 + ', channelmode=0,dependent=0,directories=0,fullscreen=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,toolbar=0');

        //    newWindow = window.open( cUrl, cWindow, 'width=' + iwidth + ', height=' + iheight + ', top=' + (screen.height-iheight)/2 + ', left=' + (screen.width-iwidth)/2 + ', scrollBars=yes, resizable=yes' );
        newWindow.focus();
    }


//-->
</script>
<style type="text/css">
hr { margin-bottom: 20px; }
.column { clear: both; overflow: hidden; width: 550px; }
.column1 { float: left; padding-right: 10px; }
</style>

</head>
<body style="background:#ffffff;">
<!-- content -->
        <div id="full_content" style="background:#ffffff;">
        <h1>Photo Archives Online Catalog</h1>

        <!-- /uia/template/photoqry.utp; /uia/template/photodoc.utp; /uia/template/photomen.utp -->
<!--        <form method="post" action="http://inquery.ushmm.org/uia-cgi/uia_query" name="colform" onSubmit="return submitformPhoto(this)"> -->
        <form style="margin: 0;" method="get" action="result.aspx" name="colform">


            <input type="hidden" name="max_docs" value="1000">

            <div class="column">
                <div class="column1">
                    <h2>Search Photos</h2>

                    <input type="text" name="search"><br />

                    <div class="box">
                        <input type="submit" name="Submit" value="Search" class="input_button" />

                    </div>

                    <div class="box">
                        <input type="reset" value="Clear" class="input_button" /><br>
                    </div>
                </div>

                <div class="column1">
                    <h2>Type</h2>

                    <select name="query_append" style="margin-bottom: 10px;">
                        <option value="" selected="selected">All</option>
                        <option value="typehistphoto">Historical Photo</option>
                        <option value="typeinstphoto">Institutional Photo</option>
                        <option value="typeinstartifact">USHMM Artifact</option>
                    </select><br />

                    <div class="box">
                        <p><a href="javascript: openPopup( 'http://www.ushmm.org/helpdocs/inquerylang.htm', 700, 500 );">&nbsp;Help&nbsp;</a></p>
                    </div>

                </div>
            </div>
        </form>

        <br />

        <h2>Definition of Types:</h2>

        <p>Historical photographs are images of people, places and events that relate to the Holocaust and its broader historical context.</p>

        <p>Institutional photographs are images of the US Holocaust Memorial Museum building, exhibitions, artifacts, events, programs, council and staff.</p>

        <p>USHMM Artifact photographs are images of objects in the Museum’s collections.</p>


        <hr />


        <p>Approximately 20% of the 85,000 <b>historical</b> photographs in the Photo Archives are available through this online catalog. More are being added each month. In general, only photographs belonging to the Museum or determined to be in the public domain are featured in this online catalog. However, in a few instances special permission has been obtained to include photographs belonging to other archives or collectors.</p>

        <p>Approximately 600 of the 15,000 <b>institutional</b> photographs of the Museum (images of the Museum and its programs) are available through this online catalog.</p>

        <p>A small sampling of photographs of the more than 10,000 <b>artifacts</b> in the Museum is available through this online catalog.</p>


        <p>To search the Photo Archives online catalog:</p>

        <ul>
            <li>Select the type of photograph you desire (historical, institutional, artifact, or all)</li>
            <li>Enter a keyword, phrase, personal name or geographic location into the box marked “Search Photos,” and</li>
            <li>Click on “Search.”</li>
        </ul>
    </div>
</body>
</html>