﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="detail.aspx.vb" Inherits="PhotoArchivesPublicSearch.detail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Photo Archives</title>
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript">
    function doSearch() {
        window.location.href = "detail.aspx?Ssearch=" + document.getElementById("ss").value;
    }
</script>
</head>
<body>
<form runat="server" id = 'form1'>
<!--HEADER_START-->
<div class="header_main">
	<div class="header_top_main">
    	<div class="header_top">
        	<div class="top1">
            	<div class="top1_left">
                    <div class="navigation">
                    	<ul>
                        	<li><a href="#">EVENTS</a></li>
                            <li><a href="#">HOURS &amp; DIRECTIONS</a></li>
                            <li><a href="#">SUPPORT THE MUSEUM</a></li>
                        </ul>
                    </div>
                    <div class="donet_button"><a href="#">DONATE</a></div>
                </div>
            </div>
            <div class="top2">
            	<div class="top2_left">
                	<ul>
                        <li><a href="#">Learn About</a><br /><span><a href="#">THE HOLOCAUST</a></span></li>
                        <li><a href="#">Remember </a><br /><span><a href="#"> VICTIMS AND SURVIVORS</a></span></li>
                        <li><a href="#">Confront</a><br /><span class="geno_border"><a href="#">GENOCIDE</a></span><span class="anti_nav"><a href="#">ANTISENITISM</a></span></li>
                    </ul>
                </div>
                <div class="top2_right">
                    <form action="/" method="get">
                      <ul>
                    	<li><input type="text" name="q" value="Site Search" onclick="clickclear(this, 'Site Search')" onblur="this.value='Site Search'" /></li>
                        <li class="search_button"><input type="submit" value="Search" /></li>
                      </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="header_bot_main">
    	<div class="header_bot">
        	<div class="head_bot_left">
            	<div class="head_bot_nav">
                	<ul>
                    	<li><a href="#">HOME</a></li>
                        <li><img src="images/na_bullet.png" alt="" /><a href="#">RESERCH</a></li>
                        <li><img src="images/na_bullet.png" alt="" /><a href="#">PHOTO ARCHIVES</a></li>
                        <li><img src="images/na_bullet.png" alt="" /><a href="#">SEARCH THE COLLECTON</a></li>
                    </ul>
                </div>
                <div class="hed_bot_le_bot">
                	<h2>Photo Archives</h2>
                    <div class="hed_bot_search">
                    	<form method="get" action="/">
                    	  <ul>
                        	<li class="sc"><input type="text" runat="server" id="ss" name="Search Online Catalog" value="Search Online Catalog" onclick="clickclear(this, 'Search Online Catalog')" onblur="if (this.value == '') {this.value='Search Online Catalog';}" />
                        	<p><select>
                        	<option>All &emsp;</option>
                        	<option>Historical Photo &emsp;</option>
                        	<option>Institutional Photo &emsp;</option>
                        	<option>USHMM Artifact &emsp;</option>
                        	</select></p>
                        	</li>
                                <li class="search_button2">
                                    <input type="button" value = "SEARCH" onclick="javascript: doSearch();"  /></li>
                          </ul>
                        </form>
                    </div>
                </div>
            </div>
            <div class="head_bot_right">
       	    	<h2><a href="#"><img src="images/hed_bo_ri_text_img.png" alt="" /></a></h2>
            </div>
        </div>
    </div>
</div>
<!--HEADER_END-->
<div class="wrapper">
<!--CONTAIN_START-->
	<div class="contain">
    	<div class="contain_main">
        	<div class="contain_top_a">
            	<div class="con_top_1a"><a href="#">SHARE THIS</a><img src="images/1a.png" alt="" /></div>
                <div class="con_top_2a">
                	<ul>
                    	<li><a href="#" class="left_arrow"></a></li>
                    	<li class="pre"><asp:HyperLink ID="topPrevLink" runat="server">PREVIOUS</asp:HyperLink></li>
                        <li class="pre"><a href="result.aspx?max_docs=1000&search=<%=server.urlencode( Session("SearchString")&"")%>&Submit=Search">BACK</a></li>
                        <li><asp:HyperLink ID="topNextLink" runat="server">NEXT</asp:HyperLink></li>
                    	<li><a href="#" class="right_arrow"></a></li>
                    </ul>
              </div>
                <div class="con_top_3a">
                	<a href="cart.aspx"></a>( <asp:label runat="server" id="LabelItems" /> )
                </div>
            </div>
            <div class="contain_bottom_a">
            	<div class="con_lefta">
                	<div class="banner">
                    	<img src="<%=Session("WSRetreiveAsset") & "qfactor=2&width=480&height=480&crop=0&size=1&type=asset&id=" %><%=request.querystring("id")%>"  border="0"> </img>
                    </div>
                    <div class="banner_bot">
                    	<h1><a href="http://online.ushmm.org/photoarchive/recognize/main_form.php?photonum=<%#photonum%>">RECOGNIZE SOMEONE?</a></h1>
                        <h2><asp:literal runat="server" id="lblTitle2"/></h2>
                    	<p>
                        	  <asp:Label ID="lblDescription" runat="server"></asp:Label><br /><br />
                            
                             <asp:Label ID="lblEvent" runat="server"></asp:Label><br /><br />
                            
                            
                        </p>
                    </div>
                </div>
                <div class="con_righta">
                	<div class="cart">
                	  <asp:LinkButton runat="server" id="AddtoCart" text=" ADD TO CART" />
<!--
                    	<img src="images/cart_2.png" alt="" />ADD TO CART
-->
                        </div>
                    <div class="photo_ass" style="width: 250px">
                    	<ul>
                        	<li class="photo">PHOTO INFORMATION</li>
                            <li><asp:Label ID="lblNumber" runat="server"></asp:Label></li>
                            <li><span id="123"><table cellpadding="0" cellspacing="0" ><tr><td style="width:100px"><b>Date:</b></td><td><asp:Label ID="lblDate" runat="server"></asp:Label></td></tr></table></span></li>
                            <li><asp:Label ID="lblLocation" runat="server"></asp:Label></li>
                            <li><asp:Label ID="lblCredit" runat="server"></asp:Label> </li>
                            <li><asp:Label ID="lblCopyright" runat="server"></asp:Label></li>


                        </ul>
                    </div>
                    <div class="photo_ass" style="width: 250px">
                    	<ul>
                        	<li class="photo">Subject Classification</li>
                            <li> <asp:Label  ID="lblDesignation" runat="server"></asp:Label>  </li>
                        </ul>
                        <ul> 
                            <li class="photo">Keywords</li>
                            <li> 
                                 <asp:Repeater ID="RepeaterKeywords" runat="server">
                                 <ItemTemplate>
                                  <a class="cart" href="result.aspx?search=<%#trim(Container.DataItem)%>"> 
                                <%#Trim(Container.DataItem)%></a><br>
                                 </ItemTemplate> 
                                 </asp:Repeater>
                            </li> 
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--CONTAIN_CLOSE-->  
</div>
<!--FOOTER_START-->
<div class="footer_main">
	<div class="footer_1">
		<div class="footer_top">
    		<ul>
        	<li><img src="images/footer_round.png" alt=""  /><a href="#">Plan a Visit</a></li>
            <li><img src="images/footer_round.png" alt=""  /><a href="#">The Permanent Exhibition</a></li>
            <li><img src="images/footer_round.png" alt=""  /><a href="#">Special Exhibitions</a></li>
        </ul>
        	<ul>
        	<li><img src="images/footer_round.png" alt=""  /><a href="#">Ask a Question</a></li>
            <li><img src="images/footer_round.png" alt=""  /><a href="#">Academic Research</a></li>
            <li><img src="images/footer_round.png" alt=""  /><a href="#">Family &amp; genealogic Research</a></li>
        </ul>
            <ul>
                <li><img src="images/footer_round.png" alt=""  /><a href="#">New to Teaching<br />&nbsp;&nbsp;&nbsp; the Holocaust?</a></li>
                <li><img src="images/footer_round.png" alt=""  /><a href="#">Find Lesson Plans</a></li>
            </ul>
            <ul>
                <li><img src="images/footer_round.png" alt=""  /><a href="#">Browse Upcoming Courses<br />&nbsp;&nbsp;&nbsp; &amp; Workshops</a></li>
            </ul>    	
    	</div>
    	<div class="footer_bottom">
        	<div class="footer_bo">
        		<div class="footer_bo_top">
            		<div><img src="images/footer_logo.png" alt="" /></div>
            	</div>
                <div class="footer_bo_con">
                	<p>
                    	<span>UNITED STATES HOLOCAUST MEMORIAL MUSEUM</span><br />
                        100 Raoul Wallenberg Place, SW<br />
                        Washington, DC 20024-2126<br />
                        Main telephone:202.488.0400<br />
                        TTY:202.488.0406
                    </p>
                </div>
                <div class="footer_bo_bottom">
                	<ul>
                    	<li><a href="#">CONTACT THE MUSEUM</a></li>
                        <li><a href="#">COPYRIGHT &amp; LEGAL</a></li>
                        <li><a href="#">INFORMATION</a></li>
                        <li><a href="#">SITEMAP</a></li>
                    </ul>
                </div>
           	</div>   
            <div class="footer_search">
            	<div class="get">GET THE LATEST NEWS</div>
                <div class="get_2">
                	<h1>
                    	<input type="text" name="Your email address" value="Your email address" onclick="clickclear(this, 'Your email address')" onblur="this.value='Your email address'" />
                    </h1>
                    <h2><a href="#">SUBSCRIBE</a></h2>
                </div>
            </div>        
        </div>
    </div>
</div>
<!--FOOTER_CLOSE-->
</form>
</body>
</html>





