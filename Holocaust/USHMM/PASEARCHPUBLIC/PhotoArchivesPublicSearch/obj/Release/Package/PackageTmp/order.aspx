﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="order.aspx.vb" Inherits="PhotoArchivesPublicSearch.order" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<link rel="stylesheet" href="css/normal_body.css">
<link rel="stylesheet" href="css/normal_body_print.css">
<link rel="stylesheet" href="css/style.css">
<title>Order</title>

<script language="javascript" src="js/general.js"></script>

    <style type="text/css">
        .style1
        {
            width: 300px;
        }
    </style>
</head>
<body bgcolor="#D2D7A4" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">
    <form id="form1" runat="server">
    <div>
<table width="80%" align="center" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="arial black">
        <span class="Text40"></span><span class="Text30"><b>Ordering</b></span><br><br>

    </td>
</tr>

<tr>
    <td class="Text14 arial black Textheight">
    
        <span class="red">
        <asp:Label runat="server" ID="lblError"></asp:Label>
        </span>
    
        <span class="green">
        If you would like to use any image(s) from this search, please note the photograph numbers(s) and submit the request form <a class="red LinkBlack" href="#order">below</a>. The Photo Research Coordinator will then contact you regarding the terms and conditions of use.  <b>Due to the high volume of requests, it can take up to three weeks to process requests for permission to use photographs.</b>  Requests will be processed in the order they are received.<br><br>

        Once you have received permission to use the photographs, you can order a print or high resolution scan of any image in this search.  The options for reproductions are as follows:<br><br>
        </span>

        <ul type="disc">
            <li><b>Digital prints</b> output from 300 dpi high resolution digital files. The cost is $17 per print and the turnaround time is 2 weeks. <b>Orders must be paid by check or money order.</b> We cannot accept credit card payments. Prints are shipped via U.S. Airmail at no additional cost.  Alternatively, Federal Express shipping is available at your expense (for which we require a Fed Ex account number or major credit card number).<br><br></li>
            <li><b>Custom Lab prints</b> made from negatives on file in the Photo Archives. The Photo Archives has negatives for approximately half of its collection.  Most are not original negatives, but negatives made from copy prints. The cost is $22 per print, with a turnaround time of 3-4 weeks. <b>Orders must be paid by check or money order.</b> We cannot accept credit card payment. Prints are shipped via U.S. Airmail at no additional cost. Alternatively, Federal Express shipping is available at your expense (for which we require a Fed Ex account number or major credit card number).<br><br></li>

            <li><b>High Resolution digital files.</b> [Preferred option for orders of more than 10 photos.] These are 300 dpi high-resolution jpeg scans (4MB). After we have approved the order, we will put you in touch with our digital lab, with which you must arrange pre-payment by credit card.  The cost is $15 per image; $10 per CD-ROM; and shipping charges (either by U.S. Airmail or Federal Express).  The turnaround time is less than a week. <b>Orders must be paid in advance by credit card only.</b><br><br></li>
            <li><b>High Resolution digital files sent by e-mail.</b> The same 300 dpi high resolution jpeg scans can be sent to you by e-mail by our digital lab. After we have approved the order, we will put you in touch with our digital lab, with which you must arrange pre-payment by credit card.  The service is billed at $80 per hour, with a $20 minimum charge. The turnaround time is 1-2 days. <b>Orders must be paid in advance by credit card only.</b><br></li>
        </ul>
    </td>
</tr>


<tr>
    <td>
        <a name="order">&nbsp;</a><br>

        <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td bgcolor="#8D8D5E">
                <img src="/images/spacer.gif" width="1" height="7" alt=""><br>
            </td>

        </tr>

        <tr>
            <td>
                <img src="/images/spacer.gif" width="1" height="2" alt=""><br>
            </td>
        </tr>

        <tr>
            <td bgcolor="#8D8D5E">

                <img src="/images/spacer.gif" width="1" height="2" alt=""><br>
            </td>
        </tr>
        </table><br><br>
    </td>
</tr>


<tr>
    <td>
        <!-- form -->


        <input type="hidden" name="subject" value="Photo Research and Reproduction Request Form">
        <input type="hidden" name="mailto" value="photoarchives@ushmm.org">
<!--
        <input type="hidden" name="mailto" value="achu@ushmm.org">
-->


        <table class="arial green Textheight" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="2">
                <span class="Text16 black"><b>Photo Research and Reproduction Request Form:</b></span><br>

                <span class="Text11 black">* denotes required fields</span><br><br>
            </td>
        </tr>

        <tr>
            <td valign="top" style="padding-right: 17px;"><span class="Text11 black"><sup><b>*</b></sup> </span><span class="Text14 green">First name:</span></td>
            <td valign="top"><asp:TextBox ID="fname" runat="server"  Width="270"></asp:TextBox>
            <asp:RequiredFieldValidator id="reqFName" runat="server" ControlToValidate="fname"  Font-Size=Small  ErrorMessage="<br>You must provide your first name." Display="Dynamic"></asp:RequiredFieldValidator>
            </td>

        </tr>

        <tr>
            <td valign="top" style="padding-right: 17px;"><span class="Text11 black"><sup><b>*</b></sup> </span><span class="Text14">Last name:</span></td>
            <td valign="top"><asp:TextBox ID="lname" runat="server" Width="270"></asp:TextBox>
            <asp:RequiredFieldValidator id="reqLName" runat="server" ControlToValidate="lname" Font-Size=Small  ErrorMessage="<br>You must provide your last name." Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>

        <tr>

            <td valign="top" style="padding-right: 17px;" nowrap><span class="Text11 black"><sup><b>*</b></sup> </span><span class="Text14">Sponsoring organization/<br>Company/Self:</span></td>
            <td valign="top"><asp:TextBox ID="aff" runat="server" Width="270"></asp:TextBox>
            <asp:RequiredFieldValidator id="reqAff" runat="server" ControlToValidate="aff" Font-Size=Small  ErrorMessage="<br>You must provide your sponsoring organization/company." Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>

        <tr>
            <td><br></td>
        </tr>

        <tr>
            <td valign="top"><span class="Text11 black"><sup><b>*</b></sup> </span><span class="Text14">Address:</span></td>
            <td valign="top">
                <table class="Text14 arial green Textheight" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" style="padding-right: 7px;">Street:<br></td>

                    <td valign="top"><asp:TextBox ID="street" runat="server" Width="220"></asp:TextBox> 
                    <asp:RequiredFieldValidator  Font-Size=Small id="reqStreet" runat="server" ControlToValidate="street" ErrorMessage="<br>You must provide your street address." Display="Dynamic"></asp:RequiredFieldValidator>
                    <br />
                    </td>
                </tr>

                <tr>
                    <td valign="top" style="padding-right: 7px;">Apt/Suite:<br></td>
                    <td valign="top"><asp:TextBox ID="apt" runat="server" Width="220"></asp:TextBox><br>
                    </td>
                </tr>

                <tr>

                    <td valign="top" style="padding-right: 7px;">City:<br></td>
                    <td valign="top"><asp:TextBox ID="city" runat="server" Width="220"></asp:TextBox> <asp:RequiredFieldValidator  Font-Size=Small id="reqCity" runat="server" ControlToValidate="city" ErrorMessage="<br>You must provide your City." Display="Dynamic"></asp:RequiredFieldValidator>
                    <br>
                    </td>
                </tr>
                <tr>

                    <td valign="top" style="padding-right: 7px;">State/Province:<br></td>
                    <td valign="top"><asp:TextBox ID="state" runat="server" Width="220"></asp:TextBox> <asp:RequiredFieldValidator  Font-Size=Small id="reqState" runat="server" ControlToValidate="state" ErrorMessage="<br>You must provide your State/Province." Display="Dynamic"></asp:RequiredFieldValidator>
                    <br>
                    </td>
                </tr>                
                <tr>
                    <td valign="top" style="padding-right: 7px;">Country:<br></td>
                    <td valign="top"><asp:TextBox ID="country" runat="server" Width="220"></asp:TextBox> <asp:RequiredFieldValidator  Font-Size=Small id="reqCountry" runat="server" ControlToValidate="country" ErrorMessage="<br>You must provide your Country." Display="Dynamic"></asp:RequiredFieldValidator>
                    <br>
                    </td>
                </tr>

                <tr>
                    <td valign="top" style="padding-right: 7px;" nowrap>Zipcode/Postal code:<br></td>
                    <td valign="top"><asp:TextBox ID="zip" runat="server" Width="220"></asp:TextBox> <asp:RequiredFieldValidator  Font-Size=Small id="reqZip" runat="server" ControlToValidate="zip" ErrorMessage="<br>You must provide your Zip/Postal Code." Display="Dynamic"></asp:RequiredFieldValidator>
                    <br />
                    <br></td>
                </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td><br></td>
        </tr>

        <tr>
            <td valign="top" style="padding-right: 17px;"><span class="Text11 black"><sup><b>*</b></sup> </span><span class="Text14">Daytime telephone:</span></td>
            <td valign="top"><asp:TextBox ID="phone" runat="server" Width="270"></asp:TextBox> <asp:RequiredFieldValidator  Font-Size=Small id="reqPhone" runat="server" ControlToValidate="phone" ErrorMessage="<br>You must provide your phone number." Display="Dynamic"></asp:RequiredFieldValidator>
            <br>
            </td>
        </tr>

        <tr>
            <td valign="top" style="padding-right: 17px;"><span class="Text14">Fax:</span></td>
            <td valign="top"><asp:TextBox ID="fax" runat="server" Width="270"></asp:TextBox><br></td>
        </tr>

        <tr>
            <td valign="top"><span class="Text11 black"><sup><b>*</b></sup> </span><span class="Text14">Email:</span></td>

            <td valign="top"><asp:TextBox ID="email" runat="server" Width="270"></asp:TextBox>
            <asp:RegularExpressionValidator ID="reqEmail" runat="server" ControlToValidate="email" ErrorMessage="<br>You must provide a valid e-mail address" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            
             <input class="spf" type="text" name="mail" id="mail" value=""/>
             <input type="text" name="message" id="message" value=""/>
             <input type="text" name="first_name" id="first_name" value=""/>
             <input type="text" name="fullname" id="fullname" value=""/>
             <input type="text" name="e_mail" id="e_mail" value=""/>
             <textarea name="comments" id="comments" rows="5" cols="4" ></textarea>



            <br>
            </td>
        </tr>

        <tr>
            <td><br><br></td>
        </tr>


        <tr>
            <td colspan="2"><span class="Text16 black"><b>Description of Project:</b></span><br><br></td>

        </tr>

        <tr>
            <td valign="top" style="padding-right: 17px;"><span class="Text11 black"><sup><b>*</b></sup> </span><span class="Text14">Project Type:</span></td>
            <td valign="top"><asp:DropDownList runat="server" name="type" ID="DropDownProjectType"></asp:DropDownList> <asp:RequiredFieldValidator  Font-Size=Small id="reqType" runat="server" ControlToValidate="phone" ErrorMessage="<br>You must provide your project type." Display="Dynamic"></asp:RequiredFieldValidator>
            <br>
            </td>
        </tr>

        <tr>

            <td><br></td>
        </tr>

        <tr>
            <td valign="top" style="padding-right: 17px;"><span class="Text11 black"><sup><b>*</b></sup> </span><span class="Text14">Project title/subject matter:</span></td>
            <td valign="top"><asp:TextBox id="title" runat="server" TextMode=MultiLine Width="270" Height="100" Rows="5"></asp:TextBox> <asp:RequiredFieldValidator  Font-Size=Small id="reqTitle" runat="server" ControlToValidate="title" ErrorMessage="<br>You must provide your project title/subject matter." Display="Dynamic"></asp:RequiredFieldValidator>
            <br>
            
            </td>
        </tr>


        <tr>
            <td><br><br></td>
        </tr>
                <span class="black"><b>Need assistance finding photographs?</b><br><span class="Text11 black">(Please make your research request as specific as possible)</span><br>
                <asp:TextBox id="assistance" runat="server" Width="270" TextMode=MultiLine Height="100" Rows="5"></asp:TextBox><br>
                </span>
            </td>
        </tr>
        <tr>
            <td><br><br></td>
        </tr>

        <tr>
            <td colspan="2" class="Text14">
                <span class="Text18 black"><b>Questions/comments:</b></span><br>
                <asp:TextBox id="question" runat="server" TextMode=MultiLine Width="270" Height="100" Rows="5"></asp:TextBox>
            </td>
        </tr>


        <tr>
            <td><br><br></td>
        </tr>


        <tr> 
            <td colspan="2">
                    <asp:button ID="buttonSubmit" type="submit" name="buttonSubmit" onclick="buttonSubmit_onClick" text="Submit Request" runat="server"/>
            
                <asp:Label ID="thank_you" runat="server" Text="Label" Visible="False"></asp:Label>
            </td>

        </tr>
        </table>


    </td>
</tr>


<tr>
    <td>
        <br><a class="Text14 arial red LinkBlack" href="javascript: window.close();">[Close]</a><br>

    </td>
</tr>

</table>
    
    </div>
    </form>
</body>
</html>
