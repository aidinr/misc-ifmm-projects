﻿<%@ Page Language="vb"  AutoEventWireup="false"  Debug="true" CodeBehind="result.aspx.vb" Inherits="PhotoArchivesPublicSearch.result" %>

<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Photo Archives</title>
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-latest.js"></script>
<style type="text/css">
.container { width:73px; margin: 0 auto;}
div.trigger { padding: 0px 0px 0px 0px; background:url(images/minus.png) no-repeat right 10px; width:73px; font-weight: normal; margin:0px 0px 0px 0px; font-family:"HelveticaNeueLT Std Med"; font-size:11px; color:#6799d8; line-height:26px; padding-left:0px; cursor:pointer;}
div.trigger2 { padding: 0px 0px 0px 0px; background:url(images/minus.png) no-repeat right 10px; width:55px; font-weight: normal; margin:0px 0px 0px 0px; font-family:"HelveticaNeueLT Std Med"; font-size:11px; color:#6799d8; line-height:26px; padding-left:0px; cursor:pointer;}
div.trigger a { color: #6799d8; text-decoration: none; display: block;}
div.trigger img{ vertical-align:middle; margin:-2px 8px 0px 0px;}

.toggle_container .block{}
.toggle_container{ position:absolute; background:#fff; border:solid 1px #6799d8; overflow:hidden; width:100px; margin-top:5px;}
.toggle_container ul{ padding:5px; margin:0px; overflow:hidden;}
.toggle_container ul li{ font-family:"HelveticaNeueLT Std Med"; font-size:11px; color:#6799d8; padding-left:5px; line-height:18px;}
div.trigger3 { padding: 4px 0px 0px 10px; background:url(images/drop_menu_2.png) no-repeat right 10px; width:60px; font-weight: normal; margin:0px 0px 0px 0px; font-family:"HelveticaNeueLT Std Med"; font-size:11px; color:#fff; line-height:16px; cursor:pointer;}
div.trigger3 a { color: #6799d8; text-decoration: none; display: block;}

div.trigger_main{ padding: 4px 0px 0px 10px; background:url(images/drop_menu_2.png) no-repeat right 10px; width:140px; font-weight: normal; margin:0px 0px 0px 0px; font-family:"HelveticaNeueLT Std Med"; font-size:11px; color:#fff; line-height:16px; cursor:pointer;}
div.trigger_main a { color: #6799d8; text-decoration: none; display: block;}

div.active{ background-position:right 10px;}
.toggle_container3{ position:absolute; background:#fff; border:solid 1px #6799d8; overflow:hidden; width:100px; margin-top:5px;}
.toggle_container3 ul{ padding:5px; margin:0px; overflow:hidden;}
.toggle_container3 ul li{ float:none !important; font-family:"HelveticaNeueLT Std Med"; font-size:12px; color:#6799d8; padding-left:5px; line-height:18px;}

.toggle_container_main{ position:absolute; background:#fff; border:solid 1px #6799d8; overflow:hidden; width:159px; margin-top:5px;}
.toggle_container_main ul{ padding:5px; margin:0px; overflow:hidden;}
.toggle_container_main ul li{ float:none !important; font-family:"HelveticaNeueLT Std Med"; font-size:12px; color:#6799d8; padding-left:5px; line-height:18px;}
</style>
<script type="text/javascript">
    function doSearch() {

        window.location.href = "result.aspx?LocaleFilter=<%=Session("LocaleFilter")%>&TypeFilter=<%=Session("TypeFilter")%>&DateFromFilter=<%=Session("DateFromFilter")%>&DateToFilter=<%=Session("DateToFilter")%>&SortOrder=" + document.getElementById("SortBy").value;

        
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {

        $(".toggle_container").hide();

        $("div.trigger").click(function () {
            $(this).toggleClass("active").next().slideToggle("slow");
        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function () {

        $(".toggle_container").hide();

        $("div.trigger2").click(function () {
            $(this).toggleClass("active").next().slideToggle("slow");
        });

    });
</script>

<script type="text/javascript">
    $(document).ready(function () {

        $(".toggle_container").hide();

        $("div.trigger3").click(function () {
            $(this).toggleClass("active").next().slideToggle("slow");
        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function () {

        $(".toggle_container").hide();

        $("div.trigger_main").click(function () {
            $(this).toggleClass("active").next().slideToggle("slow");
        });

    });
</script>

<style type="text/css" media="screen">
<!--
	UL.tabNavigation {
		list-style: none;
		margin: 20px 0px 0px 240px;
		padding: 0;
	}

	UL.tabNavigation LI {
		display: inline;
	}

	UL.tabNavigation LI A {
		padding: 3px 20px 3px 20px;
		color: #000;
		text-decoration: none;
	}

	UL.tabNavigation LI A.3,
	UL.tabNavigation LI A:hover {
		color: #fff;
		padding-top: 7px;
	}
	
	UL.tabNavigation LI A:focus {
		outline: 0;
	}

	div.tabs > div {
		padding: 18px 5px 5px 5px;
		margin-top: 10px;
		width:700px;
		 border-top:dashed 1px #cecece;
	}
	
	div.tabs > div h2 {
		margin-top: 0;
	}

	#first {}

	#second {}

	#third {}
	
	.waste {
		min-height: 1000px;
	}
-->
</style>
<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
    $(function () {
        var tabContainers = $('div.tabs > div');
        tabContainers.hide().filter(':first').show();

        $('div.tabs ul.tabNavigation a').click(function () {
            tabContainers.hide();
            tabContainers.filter(this.hash).show();
            $('div.tabs ul.tabNavigation a').removeClass('selected');
            $(this).addClass('selected');
            return false;
        }).filter(':first').click();
    });

    function SubmitDates() {
        window.location.href = 'result.aspx?SortOrder=<%=Request("SortOrder") & ""%>&TypeFilter=<%=Session("TypeFilter")%>&amp;LocaleFilter=<%=Session("LocaleFilter")%>&amp;DateFromFilter=' + document.getElementById("datepicker-example1").value + '&amp;DateToFilter=' + document.getElementById("datepicker-example2").value;
    }
</script>
<link href="js/left_menu.css" rel="stylesheet" type="text/css" />
<script src="js/main.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jq1.js"></script>
<script src="js/ddaccordion.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    ddaccordion.init({
        headerclass: "expandable", //Shared CSS class name of headers group that are expandable
        contentclass: "categoryitems", //Shared CSS class name of contents group
        revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
        mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
        collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
        defaultexpanded: [0], //index of content(s) open by default [index1, index2, etc]. [] denotes no content
        onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
        animatedefault: false, //Should contents open by default be animated into view?
        persiststate: true, //persist state of opened contents within browser session?
        toggleclass: ["", "openheader"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
        togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
        animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
        oninit: function (headers, expandedindices) { //custom code to run when headers have initalized
            //do nothing
        },
        onopenclose: function (header, index, state, isuseractivated) { //custom code to run whenever a header is opened or closed
            //do nothing
        }
    })
</script>
<link rel="stylesheet" href="js/104fc4056ad53fbc1d0eb8dfa97c7010_6678662645.css" type="text/css" />
<script language="javascript" type="text/javascript">
    function clickclear(thisfield, defaulttext) {
        if (thisfield.value == defaulttext) {
            thisfield.value = "";
        }
    }
</script>

<link href="js/style1.css" rel="stylesheet" type="text/css" />
<style type="text/css">
#carousel{ margin:0px; overflow:hidden;}
#slides{overflow:hidden; position:relative; width:250px; margin:-1px 0px 0px -18px; padding-top:15px; *padding-top:0px; *margin-top:-65px;}
#slides ul{position:relative;left:0;top:0;list-style:none;margin:0;padding:0;width:750px;}
#slides li{width:250px;float:left; text-align:center;}
#slides li img{padding:4px;}
#buttons{padding:0 0 0px 0;float:right;}
#buttons a{display:block;width:31px;height:32px;text-indent:-999em;float:left; outline:0; position:relative; z-index:9999;}
a#prev{background:url(images/arrow1.png) 0 -31px no-repeat; margin-top:45px;}
a#next{background:url(images/arrow1.png) -32px -31px no-repeat; margin-left:138px; margin-top:45px;}
</style>
<script src="js/lightbox-form.js" type="text/javascript"></script>
<script src="js/lightbox-form01.js" type="text/javascript"></script>
<script src="js/lightbox-form02.js" type="text/javascript"></script>
<script src="js/lightbox-form03.js" type="text/javascript"></script>
<script src="js/lightbox-form04.js" type="text/javascript"></script>
<script src="js/lightbox-form05.js" type="text/javascript"></script>
<script src="js/lightbox-form06.js" type="text/javascript"></script>
<script src="js/lightbox-form07.js" type="text/javascript"></script>
<script src="js/lightbox-form08.js" type="text/javascript"></script>
<script src="js/lightbox-form09.js" type="text/javascript"></script>
<script src="js/lightbox-form10.js" type="text/javascript"></script>
<script src="js/jquery-1.3.2.js" type="text/javascript"></script>

<script type="text/javascript" src="js/c3c7293f00563706bd22bc16055aad92_7006693911.js"></script>
<script type="text/javascript" src="js/main.js"></script>
</head>
<body>
<form runat="server" id = "Form1">
<div class="header_main">
<!--	<div class="header_top_main">
    	<div class="header_top">
        	<div class="top1">
            	<div class="top1_left">
                	
                    <div class="navigation">
                    	<ul>
                        	<li><a href="#">EVENTS</a></li>
                            <li><a href="#">HOURS &amp; DIRECTIONS</a></li>
                            <li><a href="#">SUPPORT THE MUSEUM</a></li>
                        </ul>
                    </div>
                    <div class="donet_button"><a href="#">DONATE</a></div>
                </div>

            </div>
            <div class="top2">
            	<div class="top2_left">
                	<ul>
                        <li><a href="#">Learn About</a><br /><span><a href="#">THE HOLOCAUST</a></span></li>
                        <li><a href="#">Remember </a><br /><span><a href="#"> VICTIMS AND SURVIVORS</a></span></li>
                        <li><a href="#">Confront</a><br /><span class="geno_border"><a href="#">GENOCIDE</a></span><span class="anti_nav"><a href="#">ANTISENITISM</a></span></li>
                    </ul>
                </div>
                <div class="top2_right">
                	<ul>
                    	<li><input type="text" name="Ex:kristallnacht" value="Ex:kristallnacht" onclick="clickclear(this, 'Ex:kristallnacht')" onblur="this.value='Ex:kristallnacht'" /></li>
                        <li class="search_button"><a href="#">SEARCH</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div> -->
    <div class="header_bot_main">
    	<div class="header_bot">
        	<div class="head_bot_left">
            	<div class="head_bot_nav">
                	<ul>
                    	<li><a href="#">HOME</a></li>
                        <li><img src="images/na_bullet.png" alt="" /><a href="#">RESEARCH</a></li>
                        <li><img src="images/na_bullet.png" alt="" /><a href="#">PHOTO ARCHIVES</a></li>
                        <li><img src="images/na_bullet.png" alt="" /><a href="#">SEARCH THE COLLECTON</a></li>
                    </ul>
                </div>
                <div class="hed_bot_le_bot">
                	<h2>Photo Archives</h2>
                    <div class="hed_bot_search">
                    	<form method="get" action="/">
                    	  <ul>
                        	<li class="sc"><input type="text" name="Search Online Catalog" value="Search Online Catalog" onclick="clickclear(this, 'Search Online Catalog')" onblur="if (this.value == '') {this.value='Search Online Catalog';}" />
                        	<p><select>
                        	<option>All &emsp;</option>
                        	<option>Historical Photo &emsp;</option>
                        	<option>Institutional Photo &emsp;</option>
                        	<option>USHMM Artifact &emsp;</option>
                        	</select></p>
                        	</li>
                            <li class="search_button2"><asp:Button runat="server" id="searchCatalog" Text="SEARCH" /></li>
                          </ul>
                        </form>
                    </div>
                </div>
            </div>
            <div class="head_bot_right">
       	    	<h2><a href="#"><img src="images/hed_bo_ri_text_img.png" alt="" /></a></h2>
            </div>
        </div>
    </div>
</div>
<!--
<link rel="stylesheet" type="text/css" href="js/styles.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
-->
<div class="wrapper">
   <div class="contain">
    	<div class="contain_laft">
        	<div class="con_left">
                <div class="left_top">
                    <h2>Filter Search Results: </h2>
                    <div class="arrowlistmenu">
                    	<h3 class="menuheader expandable">LOCATION</h3>
                        <ul class="categoryitems">
                            <asp:Repeater ID="Repeater4" runat="server">
                              <ItemTemplate>
                        	    <li><a href="result.aspx?SortOrder=<%=Request("SortOrder") & ""%>&DateToFilter=<%=Session("DateToFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;TypeFilter=<%=Session("TypeFilter")%>&amp;LocaleFilter=<%#Container.DataItem("item_value")%>"><%#Container.DataItem("item_value")%> (<%#Container.DataItem("itemcount")%>)</a></li>
                            </itemtemplate>
                           </asp:Repeater>
                        </ul>
                        <h3 class="menuheader expandable">DATE RANGE</h3>
                        <ul class="categoryitems">
                            <li class="inpu">
                                <!--<input type="text" value="" name=""/>-->
                                <input id="datepicker-example1" value ="<%=Session("DateFromFilter")%>"  type="text" /><button style="margin-left: -19px; margin-top: 3px;" type="button" class="Zebra_DatePicker_Icon Zebra_DatePicker_Icon_Inside">Pick a date</button>
                            </li>
                            <li>
                                <h4>Through</h4>
                            </li>
                            <li class="inpu">
                                <input  id="datepicker-example2" value ="<%=Session("DateToFilter")%>" type="text" /><button style="margin-left: -19px; margin-top: 3px;" type="button" class="Zebra_DatePicker_Icon Zebra_DatePicker_Icon_Inside">Pick a date</button><a href="javascript: SubmitDates(); ">Go</a>
                            </li>
                        </ul>
                        <h3 class="menuheader expandable">TYPE</h3>
                        <ul class="categoryitems">
                            <asp:Repeater ID="Repeater5" runat="server">
                              <ItemTemplate>
                        	    <li><a href="result.aspx?SortOrder=<%=Request("SortOrder") & ""%>&DateToFilter=<%=Session("DateToFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;LocaleFilter=<%=Session("LocaleFilter")%>&amp;TypeFilter=<%#Container.DataItem("item_value")%>"><%#Container.DataItem("item_value")%> (<%#Container.DataItem("itemcount")%>)</a></li>
                            </itemtemplate>
                           </asp:Repeater>
                        </ul>
                    </div>
                </div>
                <div class="con_left_con">
                    <h1><asp:LinkButton runat="server" id = "ClearSearch"  text="Clear Search Filters" /> </h1>
                </div>
                <div class="con_le_bottom">
                    <h1>Search other Collections:</h1>
                    <ul>
                        <li><img src="images/left_4.png" alt="" /><a href="#">Collections</a></li>
                        <li><img src="images/left_4.png" alt="" /><a href="#">Library</a></li>
                        <li><img src="images/left_4.png" alt="" /><a href="#">Name's List</a></li>
                        <li><img src="images/left_4.png" alt="" /><a href="#">Holocaust Encyclopedia</a></li>
                    </ul>
                </div>
			</div>
        </div>
        <div class="contain_right">
        	<div class="top_title_box">
            	<div class="title_box1"></BR>
                	Displaying <b><asp:Label ID="lblTotalAssets" runat="server"></asp:Label></b> <% If Session("SearchString") & "" = "" And Request.QueryString("search") & "" = "" Then%> items <% Else%> results for <b>"<asp:Label ID="lblQueryText" runat="server"></asp:Label>"</b><% End If %> 
                    <h2><a href="Cart.aspx"><img src="images/cart_icon.png" alt="" /> ( <asp:label runat="server" id="LabelItems" /> )</a></h2>
                </div>
                <div class="title_bottom_sea">
                	<h1><asp:HyperLink ID="topPrevLink" runat="server"><img src="images/arrow_left_sear.png" alt="" /> PREVIOUS</asp:HyperLink> </h1>
                    <ul>
                        <% Dim i As Integer = 0 %>
                        <% i=IIf(Request("Page")&"" <> "",  Request("Page"), 0) %>
                        <% If i > 5 and i< totalPages - 5 then %>
                               <%For i2 As Integer = (i-4) to (i+5)%>
                               	<% If i2<=totalPages+1 then %>	
                                <% If i = i2 then %>
                                    <li><a class="selected" style="color:Red;" href="Result.aspx?SortOrder=<%=Request("SortOrder")%>&Page=<%=i2%>&amp;LocaleFilter=<%=Session("LocaleFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;DateToFilter=<%=Session("DateToFilter")%>"><%=i2%></a></li>
                                    <%Else %>
                                    <li><a  href="Result.aspx?SortOrder=<%=Request("SortOrder")%>&Page=<%=i2%>&amp;LocaleFilter=<%=Session("LocaleFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;DateToFilter=<%=Session("DateToFilter")%>"><%=i2%></a></li>
                                    <%End If %>
                                <%End If %>
                                <% next%> 
                        <% ElseIf i<=5 then %>
                               <%For i2 As Integer = 1 to 10%>
                               	<% If i2<=totalPages+1 then %>	
                                <% If i = i2 then %>
                                    <li><a class="selected" style="color:Red;" href="Result.aspx?SortOrder=<%=Request("SortOrder")%>&Page=<%=i2%>&amp;LocaleFilter=<%=Session("LocaleFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;DateToFilter=<%=Session("DateToFilter")%>"><%=i2%></a></li>
                                    <%Else %>
                                    <li><a  href="Result.aspx?SortOrder=<%=Request("SortOrder")%>&Page=<%=i2%>&amp;LocaleFilter=<%=Session("LocaleFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;DateToFilter=<%=Session("DateToFilter")%>"><%=i2%></a></li>
                                    <%End If %>
                                <%End If %>
                                <% next%> 
                        <% ElseIf i>= totalPages - 5 then  %>
                               <%For i2 As Integer = totalPages - 10 to totalPages %>
                               	<% If i2<=totalPages+1 then %>	
                                <% If i = i2 then %>
                                    <li><a class="selected" style="color:Red;" href="Result.aspx?SortOrder=<%=Request("SortOrder")%>&Page=<%=i2%>&amp;LocaleFilter=<%=Session("LocaleFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;DateToFilter=<%=Session("DateToFilter")%>"><%=i2%></a></li>
                                    <%Else %>
                                    <li><a  href="Result.aspx?SortOrder=<%=Request("SortOrder")%>&Page=<%=i2%>&amp;LocaleFilter=<%=Session("LocaleFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;DateToFilter=<%=Session("DateToFilter")%>"><%=i2%></a></li>
                                    <%End If %>
                                <%End If %>
                                <% next%> 
                        <% End If %>
                        
                    </ul>
                  	<h2><asp:HyperLink ID="topNextLink" runat="server">Next <img src="images/arrow_right_sear.png" alt="" /></asp:HyperLink> </h2>
                </div>
            </div>
            <div class="sisply_box1_sear">
            	<h1>DISPLAY OPTIONS</h1>
            <h2>SORT BY</h2>
                <select id="SortBy" onchange="javascript: doSearch();">
                  <option <% If Request("SortOrder") & "" = "Date" Then %>selected <% End If %>  value="Date">Date</option>
                  <option <% If Request("SortOrder") & "" = "Rating" Then %>selected <%ElseIf Request("SortOrder") & "" = "" then%> selected <% End If %> value="Rating">Popularity</option>
                  <option <% If Request("SortOrder") & "" = "Title" Then %>selected <% End If %> value = "Title">Title</option>
                </select>
                
            </div>
            
            <div class="tabs">
                <ul class="tabNavigation">
                    <li><a class="" href="#first"><img src="images/icon_img2.png" alt="" /></a></li>
                    <li><a class="" href="#second"><img src="images/icon_img1.png" alt="" /></a></li>
                </ul>
                <div style="display: none;" id="second">
                	<div class="tab1_main">
                        <ul>
                            <%itemcount = pagesize*ipage %>
                            <asp:Repeater ID="Repeater3" runat="server">
                          <ItemTemplate>
                            <% itemcount=itemcount+1 %>
                            <li><a href="detail.aspx?id=<%#Container.DataItem("asset_id")%>&amp;search=<%=server.urlencode(Session("SearchString") & "")%>&amp;index=<%=itemcount %>"><img src="<%=Session("WSRetreiveAsset") & "qfactor=2&amp;width=160&amp;height=160&amp;crop=0&amp;size=1&amp;type=asset&amp;id=" %><%#Container.DataItem("asset_id")%>" alt="" /></a></li>
                            
                            <%# IIf(Repeater3.Items.Count < Container.ItemIndex -3, IIf((Container.ItemIndex + 1) Mod 4 = 0, "</ul><ul>", ""), "")%>
                            
                            </ItemTemplate>
                           </asp:Repeater>
                        </ul>
                    </div>
              	</div>
                <div style="display: none;" id="first">
                	<div class="con_right_list">
                        <%itemcount = pagesize*ipage %>
                          <asp:Repeater ID="Repeater1" runat="server">
                          <ItemTemplate>

                        <div class="con_ri_list_box1">
	                        <div class="list_pro_img1 qmparent" onclick="openbox<%#Container.ItemIndex+1%>()">
                           		<img src="<%=Session("WSRetreiveAsset") & "qfactor=2&amp;width=120&amp;height=120&amp;crop=0&amp;size=1&amp;type=asset&amp;id=" %><%#Container.DataItem("asset_id")%>" alt="" />
                            </div>
                            <div class="list_pro_img1_detail">
                                <p>
                                    <%itemcount = itemcount+1 %>
                                    <span class="list_tit1"><a class="list_tit1" href="detail.aspx?id=<%#Container.DataItem("asset_id")%>&amp;search=<%=server.urlencode( Session("SearchString")&"")%>&amp;index=<%=itemcount %>"><%#formatdescription(Container.DataItem("description"))%></a><br /></span>
                                    <%#formatdescription2(Container.DataItem("description"))%><br />
                                    <span class="list_last_det">#<%#Container.DataItem("name")%>| <%#GetCategories(Container.DataItem("asset_id").ToString)%> | <%#DateTime.Parse(Container.DataItem("EventDate").ToString()).Year%></span>
                                </p>
                            </div>
                        </div>
  </ItemTemplate>
  </asp:Repeater>

     <div class="title_bottom_sea">
                	<h1><asp:HyperLink ID="bottomPrevLink" runat="server"><img src="images/arrow_left_sear.png" alt="" /> PREVIOUS</asp:HyperLink> </h1>
                    <ul>
                        <% i  = 0 %>
                        <% i=IIf(Request("Page")&"" <> "",  Request("Page"), 0) %>
                        <% If i > 5 and i< totalPages - 5 then %>
                               <%For i2 As Integer = (i-4) to (i+5)%>
                               	<% If i2<=totalPages+1 then %>	
                                <% If i = i2 then %>
                                    <li><a class="selected" style="color:Red;" href="Result.aspx?SortOrder=<%=Request("SortOrder")%>&Page=<%=i2%>&amp;LocaleFilter=<%=Session("LocaleFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;DateToFilter=<%=Session("DateToFilter")%>"><%=i2%></a></li>
                                    <%Else %>
                                    <li><a  href="Result.aspx?SortOrder=<%=Request("SortOrder")%>&Page=<%=i2%>&amp;LocaleFilter=<%=Session("LocaleFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;DateToFilter=<%=Session("DateToFilter")%>"><%=i2%></a></li>
                                    <%End If %>
                                <%End If %>
                                <% next%> 
                        <% ElseIf i<=5 then %>
                               <%For i2 As Integer = 1 to 10%>
                               	<% If i2<=totalPages+1 then %>	
                                <% If i = i2 then %>
                                    <li><a class="selected" style="color:Red;" href="Result.aspx?SortOrder=<%=Request("SortOrder")%>&Page=<%=i2%>&amp;LocaleFilter=<%=Session("LocaleFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;DateToFilter=<%=Session("DateToFilter")%>"><%=i2%></a></li>
                                    <%Else %>
                                    <li><a  href="Result.aspx?SortOrder=<%=Request("SortOrder")%>&Page=<%=i2%>&amp;LocaleFilter=<%=Session("LocaleFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;DateToFilter=<%=Session("DateToFilter")%>"><%=i2%></a></li>
                                    <%End If %>
                                <%End If %>
                                <% next%> 
                        <% ElseIf i>= totalPages - 5 then  %>
                               <%For i2 As Integer = totalPages - 10 to totalPages %>
                               	<% If i2<=totalPages+1 then %>	
                                <% If i = i2 then %>
                                    <li><a class="selected" style="color:Red;" href="Result.aspx?SortOrder=<%=Request("SortOrder")%>&Page=<%=i2%>&amp;LocaleFilter=<%=Session("LocaleFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;DateToFilter=<%=Session("DateToFilter")%>"><%=i2%></a></li>
                                    <%Else %>
                                    <li><a  href="Result.aspx?SortOrder=<%=Request("SortOrder")%>&Page=<%=i2%>&amp;LocaleFilter=<%=Session("LocaleFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;DateToFilter=<%=Session("DateToFilter")%>"><%=i2%></a></li>
                                    <%End If %>
                                <%End If %>
                                <% next%> 
                        <% End If %>
                        
                    </ul>
                  	<h2><asp:HyperLink ID="bottomNextLink" runat="server">Next <img src="images/arrow_right_sear.png" alt="" /></asp:HyperLink> </h2>
                </div>
                    </div>
                </div>
          	</div>
            
        </div>
    </div>
</div>
<!--FOOTER_START-->
<div class="footer_main">
	<div class="footer_1">
	<!--	<div class="footer_top">
    		<ul>
        	<li><img src="images/footer_round.png" alt=""  /><a href="#">Plan a Visit</a></li>
            <li><img src="images/footer_round.png" alt=""  /><a href="#">The Permanent Exhibition</a></li>
            <li><img src="images/footer_round.png" alt=""  /><a href="#">Special Exhibitions</a></li>
        </ul>
        	<ul>
        	<li><img src="images/footer_round.png" alt=""  /><a href="#">Ask a Question</a></li>
            <li><img src="images/footer_round.png" alt=""  /><a href="#">Academic Research</a></li>
            <li><img src="images/footer_round.png" alt=""  /><a href="#">Family &amp; genealogic Research</a></li>
        </ul>
            <ul>
                <li><img src="images/footer_round.png" alt=""  /><a href="#">New to Teaching<br />&nbsp;&nbsp;&nbsp; the Holocaust?</a></li>
                <li><img src="images/footer_round.png" alt=""  /><a href="#">Find Lesson Plans</a></li>
            </ul>
            <ul>
                <li><img src="images/footer_round.png" alt=""  /><a href="#">Browse Upcoming Courses<br />&nbsp;&nbsp;&nbsp; &amp; Workshops</a></li>
            </ul>    	
    	</div> -->
    	<div class="footer_bottom">
        	<div class="footer_bo">
        		<div class="footer_bo_top">
            		<div><img src="images/footer_logo.png" alt="" /></div>
            	</div>
                <div class="footer_bo_con">
                	<p>
                    	<span>UNITED STATES HOLOCAUST MEMORIAL MUSEUM</span><br />
                        100 Raoul Wallenberg Place, SW<br />
                        Washington, DC 20024-2126<br />
                        Main telephone:202.488.0400<br />
                        TTY:202.488.0406
                    </p>
                </div>
                <div class="footer_bo_bottom">
                	<ul>
                        <li><a href="http://www.ushmm.org/information/about-the-museum"> ABOUT THE MUSEUM</a></li>
                    	<li><a href="http://www.ushmm.org/information/contact-the-museum">CONTACT THE MUSEUM</a></li>
                        <li><a href="http://www.ushmm.org/copyright-and-legal-information/terms-of-use">TERMS OF USE</a></li>
                        <li><a href="http://www.ushmm.org/copyright-and-legal-information/privacy-policy">PRIVACY POLICY</a></li>
                        <li><a href="http://www.ushmm.org/information/access">ACCESSIBILITY</a></li>
                        <li><a href="http://www.ushmm.org/copyright-and-legal-information/legal-and-tax-status-information">COPYRIGHT &amp; LEGAL</a></li>
                    </ul>
                </div>
           	</div>   
        </div>
    </div>
</div>
<!--FOOTER_CLOSE-->

<div id="filter" onclick="closebox()"></div>

<div id="box" class="box">
	<div class="pop_inner_box1">
    	<div class="inner_box1_left"><a href="#"><img src="images/img2_pop.png" alt="" /></a></div>
        <div class="inner_box1_right">
       		<p>Exterior view of the house belonging to the school mistress where Klara, Alfred and Ernst <a href="#">Moritz</a> stayed after arriving in France.<br /><span>#91146</span></p>
        	<h1><a href="Shopping Cart.html"><img src="images/cart_pop.png" alt="" /> Add to cart</a></h1>
        </div>
        
  </div>
</div>
 <asp:Repeater ID="Repeater9" runat="server">
  <ItemTemplate>

                    <div id="filter<%#Container.ItemIndex+1%>" onclick="closebox<%#Container.ItemIndex+1%>()"></div>
                    <div id="box<%#Container.ItemIndex+1%>" class="box">
	                    <div class="pop_inner_box1">
    	                    <div class="inner_box1_left"><a href="#"><img src="<%=Session("WSRetreiveAsset") & "qfactor=2&amp;width=200&amp;height=200&amp;crop=0&amp;size=1&amp;type=asset&amp;id=" %><%#Container.DataItem("asset_id")%>" alt="" /></a></div>
                            <div class="inner_box1_right">
       		                    <p><%#formatdescription(Container.DataItem("description"))%><span>#<%#Container.DataItem("name")%><br /></span></p>
        	                    <h1><a href="result.aspx?SortOrder=<%=Request("SortOrder")%>&LocaleFilter=<%=Session("LocaleFilter")%>&amp;TypeFilter=<%=Session("TypeFilter")%>&amp;DateFromFilter=<%=Session("DateFromFilter")%>&amp;DateToFilter=<%=Session("DateToFilter")%>&amp;page=<%=Request.QueryString("page")%>&amp;AddToCart=<%#Container.DataItem("asset_id")%>"><img src="images/cart_pop.png" alt="" /> Add to cart</a></h1>
                            </div>    
                      </div>
                    </div>
</itemtemplate>
</asp:Repeater>
<script type="text/javascript">

</script> 
 
</form>
</body>
</html>









