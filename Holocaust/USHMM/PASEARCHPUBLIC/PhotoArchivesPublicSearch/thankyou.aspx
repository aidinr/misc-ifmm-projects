﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="thankyou.aspx.vb" Inherits="PhotoArchivesPublicSearch.thankyou" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<link rel="stylesheet" href="css/normal_body.css">
<link rel="stylesheet" href="css/normal_body_print.css">
<link rel="stylesheet" href="css/style.css">
<title>Thank you!</title>

<script language="javascript" src="js/general.js"></script>

    </head>

<body bgcolor="#D2D7A4" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">
    <form id="form1" runat="server">
    <div>

<table width="80%" align="center" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="arial black">
        <span class="Text30"><b>
        <br />
        <br />
        <br />
        Thank you.</b></span><br><br>

    </td>
</tr>

<tr>
    <td class="Text14 arial black Textheight">
        Your order request has been received.&nbsp;&nbsp; Order #<%response.write(request.querystring("oid")) %><br />
        <br />
        You will receive an email referencing your items shortly.&nbsp;  
    </td>
    </tr>
    </table> 
    </div>
    </form>
</body>
</html>
