function addbookmark()
{
  if (document.all)
     window.external.AddFavorite(bookmarkurl,bookmarktitle)
}


function CheckForm()
{
  if ( isWhiteSpace( document.SiteSearch.query.value ) )
  {
	alert( "Please enter a search term." );
    return false;
  }
  return true;
}


function CheckFormExtended()
{
    if ( isWhiteSpace( document.SiteSearchExtended.query.value ) )
    {
        alert( "Please enter a search term." );
        document.SiteSearchExtended.query.focus();
        return false;
    }

// Google Search
// -- the inquery uses uia\template\ushmmqry.utp to display search results
// therefore cannot use the same file for the google search results,
// unable to pass parameters.
    if ( document.SiteSearchExtended.query_append.value.indexOf( 'webpage' ) >= 0 ) {
        window.location = "http://www.ushmm.org/shared/search/searchresults.php?cx=008795841384874293445:jtbtbquu4k8&sa=Search&cof=FORID%3A11&q="
        + document.SiteSearchExtended.query.value
        return false;
    }

    return true;
}


function isWhiteSpace( s )
{
   for( Pos = 0; Pos < s.length; Pos++ )
   {
       if( s.charAt(Pos) > ' ' ) return false;
   }
   // All characters are whitespace
   return true;
}


function submitform()
{
  if ( CheckForm() )
  {
    document.SiteSearch.submit();
  }
}


function submitformExtended()
{
  if ( CheckFormExtended() )
  {
    document.SiteSearchExtended.submit();
  }
}


function openPopup ( cUrl, iwidth, iheight, cWindow )
{
    if( ( cWindow == null ) || isWhiteSpace( cWindow ) )
        cWindow = "popWin";


    newWindow = window.open( cUrl, cWindow, 'width=' + iwidth + ', height=' + iheight + ', top=' + (screen.height-iheight)/2 + ', left=' + (screen.width-iwidth)/2 + ', channelmode=0,dependent=0,directories=0,fullscreen=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,toolbar=0' );

//    newWindow = window.open( cUrl, cWindow, 'width=' + iwidth + ', height=' + iheight + ', top=' + (screen.height-iheight)/2 + ', left=' + (screen.width-iwidth)/2 + ', scrollBars=yes, resizable=yes' );
    newWindow.focus();
}


function loadCalendar( calname )
{
  if( isWhiteSpace( calname ) ) calname = "PublicPrograms";

  if( calname == "PublicPrograms" )
      top.location.href = "/calendar/";
  else if( calname == "HolocaustStudies" ) {
	  var datCurrent  = new Date( );
	  var y = datCurrent.getFullYear();
	  var m = datCurrent.getMonth() + 1;
	  var d = datCurrent.getDate();
	  datThreeMonths = new Date( y, m + 3, d );
	  var y2 = datThreeMonths.getFullYear();
	  var m2 = datThreeMonths.getMonth() + 1;
	  var d2 = datThreeMonths.getDate();
	  var r = "/calendar/index.php?CalYear=" + y + "&CalMonth=" + m + "&CalDay=" + d + "&startdate=" + m + "/" + d + "/" + y + "&enddate=" + m2 + "/" + d2 + "/" + y2 + "&calid=3";
//      alert( r );
	  top.location.href = r;
  }
  else if( calname == "Taskforce" )
	top.location.href = "http://www.ushmm.org/calendar/?calid=4";
  else
      top.location.href = "/calendar/";
}


function emailCheck( emailStr )
{
    var emailPat=/^(.+)@(.+)$/
    var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
    var validChars="\[^\\s" + specialChars + "\]"
    var quotedUser="(\"[^\"]*\")"
    var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
    var atom=validChars + '+'
    var word="(" + atom + "|" + quotedUser + ")"
    var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
    var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
    var matchArray=emailStr.match(emailPat)
    if (matchArray==null)
    {
        // alert("Email address seems incorrect (check @ and .'s)")
        return false
    }

    var user=matchArray[1]
    var domain=matchArray[2]
    if (user.match(userPat)==null)
    {
        // alert("The username doesn't seem to be valid.")
        return false
    }

    var IPArray=domain.match(ipDomainPat)
    if (IPArray!=null)
    {
        for (var i=1;i<=4;i++)
        {
            if (IPArray[i]>255)
            {
                //alert("Destination IP address is invalid!")
                return false
            }
        }

        return true
    }

    // Domain is symbolic name
    var domainArray=domain.match(domainPat)
    if (domainArray==null)
    {
        //alert("The domain name doesn't seem to be valid.")
        return false
    }

    var atomPat=new RegExp(atom,"g")
    var domArr=domain.match(atomPat)
    var len=domArr.length
    if (domArr[domArr.length-1].length<2 || domArr[domArr.length-1].length>3)
    {
        //alert("The address must end in a three-letter domain, or two letter country.")
        return false
    }
    if (len<2)
    {
        var errStr="This address is missing a hostname!"
        //alert(errStr)
        return false
    }

    return true;
}


function printPage()
{
  if( window.print ) // NS4, IE5
    window.print();
  else // unsupported feature
    alert( "Sorry, your browser doesn't support this automatic print feature. Please right-click on the page and choose the Print option." );
}



var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function PrintFormattedPage()
{
  if (pr) // NS4, IE5
    window.print()
  else if (da && !mac) // IE4 (Windows)
    vbPrintPage()
  else // other browsers
    alert("Sorry, your browser doesn't support this feature.");

  top.close();
  return false;
}

if (da && !pr && !mac) with (document) {
  writeln('<OBJECT ID="WB" WIDTH="0" HEIGHT="0" CLASSID="clsid:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>');
  writeln('<' + 'SCRIPT LANGUAGE="VBScript">');
  writeln('Sub window_onunload');
  writeln('  On Error Resume Next');
  writeln('  Set WB = nothing');
  writeln('End Sub');
  writeln('Sub vbPrintPage');
  writeln('  OLECMDID_PRINT = 6');
  writeln('  OLECMDEXECOPT_DONTPROMPTUSER = 2');
  writeln('  OLECMDEXECOPT_PROMPTUSER = 1');
  writeln('  On Error Resume Next');
  writeln('  WB.ExecWB OLECMDID_PRINT, OLECMDEXECOPT_PROMPTUSER');
  writeln('End Sub');
  writeln('<' + '/SCRIPT>');
}



function getObjId(n, d)
{
  var p,i,x;

  if( !d ) d = document;

  if( (p = n.indexOf("?")) > 0 && parent.frames.length )
  {
    d = parent.frames[n.substring(p+1)].document;
    n = n.substring(0,p);
  }
  if( !(x = d[n]) && d.all ) x = d.all[n];

  for( i = 0; !x && i < d.forms.length; i++ ) x = d.forms[i][n];

  for( i = 0; !x && d.layers && i < d.layers.length; i++ )
    x = getObjId(n,d.layers[i].document);

  if( d.getElementById )
    if( d.getElementById(n) ) x = d.getElementById(n);

  return x;
}


function browserFix(obj)
{
  var obj_id, obj_fix;

  obj_id = getObjId(obj);

  if( obj_id.style )
    obj_fix = obj_id.style;
  else
    obj_fix = obj_id;

  return obj_fix;
}


function setVisibility(obj, v)
{
   var obj_id;

   obj_id = getObjId(obj);

   if( !obj_id ) return;

   if( obj_id.style )
   {
      v = v == 'show' ? 'visible' : (v == 'hide' ? 'hidden' : v);
      obj_id.style.visibility = v;
   }
   else
   {
      v = v == 'visible' ? 'show' : (v == 'hidden' ? 'hide' : v);
      obj_id.visibility = v;
   }
}


function setImageSrc(img, s)
{
   document.images[img].src = s;
}


function load_sites( theurl )
{
   if ( theurl != '' )
      window.open( theurl, "_top"  );
}


function goBack()
{
    location.href = document.referrer;

}


