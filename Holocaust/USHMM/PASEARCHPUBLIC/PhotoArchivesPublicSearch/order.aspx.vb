﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient

Imports System.Net.Mail

Imports WebArchives.Idam.Web.Core

Imports System.Web.Helpers


Partial Public Class order
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        PopulateProjectType()

    End Sub

    Public Function GetProjectType() As String
        Dim sql As String
        sql = "select item_default_value from ipm_order_field_desc where item_tag = 'idam_order_project_type' and active = 1"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("DownloadType")

        Dim projecttype As String = ""

        MyCommand1.Fill(DT1)

        If (DT1.Rows.Count > 0) Then

            projecttype = DT1.Rows(0)("item_default_value")

        End If

        Return projecttype

    End Function

    Public Sub PopulateProjectType()

        Dim ProjectType() As String
        ProjectType = GetProjectType().Split(";")

        If ProjectType.Length > 0 Then


            If (ProjectType(ProjectType.Length - 1) = "") Then
                ReDim Preserve ProjectType(ProjectType.Length - 2)
            End If

        End If

        DropDownProjectType.DataSource = ProjectType
        DropDownProjectType.DataBind()

    End Sub

    Public Function GetDownloadTypeName(ByVal download_id As String) As String
        Dim sql As String
        Dim returnString As String = ""
        sql = "select name from ipm_downloadtype where active = 1 and download_id = " & download_id
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("DownloadTypeName")
        MyCommand1.Fill(DT1)

        If (DT1.Rows.Count > 0) Then
            returnString = DT1.Rows(0)("name")
        End If

        Return returnstring
    End Function

    Public Function GetCarouselMaxID() As String
        Dim sql As String
        sql = "select max(carrousel_id) As MaxCarouselID from ipm_carrousel"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("DownloadType")
        MyCommand1.Fill(DT1)
        Dim maxID As String = ""

        maxID = DT1.Rows(0)("MaxCarouselID")

        Return maxID
    End Function

    Public Function GetOrderMaxID() As String
        Dim sql As String
        sql = "select max(orderid) As MaxOrderID from ipm_order"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("MaxOrderID")
        MyCommand1.Fill(DT1)
        Dim maxID As String = ""

        maxID = DT1.Rows(0)("MaxOrderID")

        Return maxID
    End Function

    Public Function GetOrderDetailMaxID() As String
        Dim sql As String
        sql = "select max(orderItemID) As MaxOrderDetailID from ipm_order_details"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("MaxOrderDetailID")
        MyCommand1.Fill(DT1)
        Dim maxID As String = ""

        maxID = DT1.Rows(0)("MaxOrderDetailID")

        Return maxID
    End Function
    Public Function GetUserMaxID() As String
        Dim sql As String
        sql = "select max(userid) As userid from ipm_user"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("User")
        MyCommand1.Fill(DT1)
        Dim maxID As String = ""

        maxID = DT1.Rows(0)("userid")

        Return maxID
    End Function

    Public Function GetAssetSource(ByVal AssetID As String) As String

        Dim sql As String
        sql = "select item_value from ipm_asset_field_value where asset_id = " & AssetID & " and item_id = 21548325" ' IDAM_PHOTOGRAPHER
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("Source")
        MyCommand1.Fill(DT1)
        Dim returnString = ""

        If (DT1.Rows.Count > 0) Then
            returnString = DT1.Rows(0)("item_value")
        End If

        Return returnString

    End Function

    Public Function GetAssetCopyRight(ByVal AssetID As String) As String

        Dim sql As String
        sql = "select item_value from ipm_asset_field_value where asset_id = " & AssetID & " and item_id = 21548326" ' IDAM_ARTIFACT_PHOTOGRAPHER
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("Copyright")
        MyCommand1.Fill(DT1)
        Dim returnString = ""

        If (DT1.Rows.Count > 0) Then
            returnString = DT1.Rows(0)("item_value")
        End If

        Return returnString

    End Function

    Public Function GetAssetName(ByVal AssetID As String) As String

        Dim sql As String
        sql = "select name from ipm_asset where available = 'y' and asset_id =  " & AssetID
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("Name")
        MyCommand1.Fill(DT1)
        Dim returnString = ""

        If (DT1.Rows.Count > 0) Then
            returnString = DT1.Rows(0)("name")
        End If

        Return returnString

    End Function

    Public Function GetAssetDescription(ByVal AssetID As String) As String

        Dim sql As String
        sql = "select description from ipm_asset where available = 'y' and asset_id =  " & AssetID
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("Name")
        MyCommand1.Fill(DT1)
        Dim returnString = ""

        If (DT1.Rows.Count > 0) Then
            returnString = DT1.Rows(0)("description")
        End If

        Return returnString

    End Function

    Public Function CreateCarousel(ByVal user_id As String) As String

        Dim cart As Hashtable = Session("CartItems")

        Dim insertID As String = GetCarouselMaxID() + 1

        Dim sql As String = "insert into ipm_carrousel (carrousel_id,name,description,shared,created_date,available,active,created_by,default_carrousel,collaborative) VALUES (@carrousel_id,@name,@description,1,getdate(),'Y',1,@user_id,0,1)"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        MyConnection.Open()
        Dim sqlCommand As SqlCommand = New SqlCommand(sql, MyConnection)
        sqlCommand.Parameters.Add("@carrousel_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@user_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@name", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@description", SqlDbType.NVarChar, 255)

        sqlCommand.Parameters("@carrousel_id").Value = insertID
        sqlCommand.Parameters("@user_id").Value = user_id
        sqlCommand.Parameters("@name").Value = "Carousel " & insertID
        sqlCommand.Parameters("@description").Value = "Carousel " & insertID & ", created by web request"

        sqlCommand.ExecuteNonQuery()



        For Each entry As DictionaryEntry In cart
            sql = "insert into ipm_carrousel_item (carrousel_id,asset_id,available,active,inserted_by,available_date) VALUES (@carrousel_id,@asset_id,'Y',1,1,getdate())"
            sqlCommand = New SqlCommand(sql, MyConnection)
            sqlCommand.Parameters.Add("@carrousel_id", SqlDbType.NVarChar, 255)
            sqlCommand.Parameters.Add("@asset_id", SqlDbType.NVarChar, 255)

            sqlCommand.Parameters("@carrousel_id").Value = insertID
            sqlCommand.Parameters("@asset_id").Value = entry.Key.ToString.Split("_")(0)

            'check for duplicates
            If Not getDataTable("select asset_id from ipm_carrousel_item where asset_id = " & entry.Key.ToString.Split("_")(0) & " and carrousel_id = " & insertID).Rows.Count > 0 Then
                sqlCommand.ExecuteNonQuery()
            End If

        Next
        MyConnection.Close()
        Return insertID

    End Function
    Public Function CreateOrder(ByVal carrousel_id As String, ByVal user_id As String) As String
        Dim cart As Hashtable = Session("CartItems")

        Dim insertID As String = GetOrderMaxID() + 1

        Dim emailString As String
        Dim emailItems As String = ""
        emailString = Date.Now & vbCrLf & vbCrLf
        emailString = emailString & "Dear " & Request.Form("fname") & " " & Request.Form("lname") & ", " & vbCrLf & vbCrLf
        emailString = emailString & "Thank you for your order." & vbCrLf & vbCrLf
        emailString = emailString & "We have received your request for the following archive item(s):" & vbCrLf & vbCrLf

        Dim emailMuseumString As String = ""
        emailMuseumString = emailMuseumString & "You have received an order number " & insertID & vbCrLf & vbCrLf
        emailMuseumString = emailMuseumString & ConfigurationSettings.AppSettings("idamAdminPath") & "IDAM.aspx?page=OrdersDetails&id=" & insertID & vbCrLf & vbCrLf
        emailMuseumString = emailMuseumString & "Please use the link below to process the order:" & vbCrLf & vbCrLf
        emailMuseumString = emailMuseumString & "To view all orders, please follow this link:" & vbCrLf & vbCrLf
        emailMuseumString = emailMuseumString & ConfigurationSettings.AppSettings("idamAdminPath") & "IDAM.aspx?page=Orders" & vbCrLf & vbCrLf
        emailMuseumString = emailMuseumString & "Thank you." & vbCrLf & vbCrLf
        emailMuseumString = emailMuseumString & "This e-mail was generated automatically."


        Dim sql As String = "insert into ipm_order (orderid,carrousel_id,orderdate,userid,sessionid,status,firstname,lastname,agency,maddress1,maddress2,mcity,mstate,mzip,mcountry,baddress1,baddress2,bcity,bstate,bzip,bcountry,bphone1,fax,email,shippingtype) VALUES (@order_id,@carrousel_id,getdate(),@user_id,@sessionid,0,@firstname,@lastname,@agency,@maddress1,@maddress2,@mcity,@mstate,@mzip,@mcountry,@baddress1,@baddress2,@bcity,@bstate,@bzip,@bcountry,@bphone1,@fax,@email,@shippingtype)"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        MyConnection.Open()
        Dim sqlCommand As SqlCommand = New SqlCommand(sql, MyConnection)
        sqlCommand.Parameters.Add("@order_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@carrousel_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@user_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@sessionid", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@firstname", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@lastname", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@agency", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@maddress1", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@maddress2", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@mcity", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@mstate", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@mzip", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@mcountry", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@baddress1", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@baddress2", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@bcity", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@bstate", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@bzip", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@bcountry", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@bphone1", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@fax", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@email", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@shippingtype", SqlDbType.NVarChar, 255)



        sqlCommand.Parameters("@order_id").Value = insertID
        sqlCommand.Parameters("@carrousel_id").Value = carrousel_id
        sqlCommand.Parameters("@user_id").Value = user_id
        sqlCommand.Parameters("@sessionid").Value = Session.SessionID.Substring(0, 10)
        sqlCommand.Parameters("@firstname").Value = Request.Form("fname")
        sqlCommand.Parameters("@lastname").Value = Request.Form("lname")
        sqlCommand.Parameters("@agency").Value = Request.Form("aff")
        sqlCommand.Parameters("@maddress1").Value = Request.Form("street")
        sqlCommand.Parameters("@maddress2").Value = Request.Form("apt")
        sqlCommand.Parameters("@mcity").Value = Request.Form("city")
        sqlCommand.Parameters("@mstate").Value = Request.Form("state")
        sqlCommand.Parameters("@mzip").Value = Request.Form("zip")
        sqlCommand.Parameters("@mcountry").Value = Request.Form("country")
        sqlCommand.Parameters("@baddress1").Value = Request.Form("street")
        sqlCommand.Parameters("@baddress2").Value = Request.Form("apt")
        sqlCommand.Parameters("@bcity").Value = Request.Form("city")
        sqlCommand.Parameters("@bstate").Value = Request.Form("state")
        sqlCommand.Parameters("@bzip").Value = Request.Form("zip")
        sqlCommand.Parameters("@bcountry").Value = Request.Form("country")
        sqlCommand.Parameters("@bphone1").Value = Request.Form("phone")
        sqlCommand.Parameters("@fax").Value = Request.Form("fax")
        sqlCommand.Parameters("@email").Value = Request.Form("email")
        sqlCommand.Parameters("@shippingtype").Value = 0

        sqlCommand.ExecuteNonQuery()


        'insert assistance request
        sql = "insert into ipm_order_field_value (orderid,item_id,item_value) values (@orderid,@item_id,@item_value)"
        sqlCommand = New SqlCommand(sql, MyConnection)
        sqlCommand.Parameters.Add("@orderid", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_value", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters("@orderid").Value = insertID
        sqlCommand.Parameters("@item_id").Value = 21552509
        sqlCommand.Parameters("@item_value").Value = Request.Form("assistance")
        sqlCommand.ExecuteNonQuery()

        'insert project type
        sql = "insert into ipm_order_field_value (orderid,item_id,item_value) values (@orderid,@item_id,@item_value)"
        sqlCommand = New SqlCommand(sql, MyConnection)
        sqlCommand.Parameters.Add("@orderid", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_value", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters("@orderid").Value = insertID
        sqlCommand.Parameters("@item_id").Value = 21552486
        sqlCommand.Parameters("@item_value").Value = Request.Form("DropDownProjectType")
        sqlCommand.ExecuteNonQuery()

        'insert project description
        sql = "insert into ipm_order_field_value (orderid,item_id,item_value) values (@orderid,@item_id,@item_value)"
        sqlCommand = New SqlCommand(sql, MyConnection)
        sqlCommand.Parameters.Add("@orderid", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_value", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters("@orderid").Value = insertID
        sqlCommand.Parameters("@item_id").Value = 21552487
        sqlCommand.Parameters("@item_value").Value = Request.Form("title")
        sqlCommand.ExecuteNonQuery()

        'insert questions and comments
        sql = "insert into ipm_order_field_value (orderid,item_id,item_value) values (@orderid,@item_id,@item_value)"
        sqlCommand = New SqlCommand(sql, MyConnection)
        sqlCommand.Parameters.Add("@orderid", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_value", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters("@orderid").Value = insertID
        sqlCommand.Parameters("@item_id").Value = 21552510
        sqlCommand.Parameters("@item_value").Value = Request.Form("question")
        sqlCommand.ExecuteNonQuery()

        Dim Source As String
        Dim Copyright As String
        Dim Name As String
        Dim Description As String


        Dim i As Integer = 1


        For Each entry As DictionaryEntry In cart


            Name = GetAssetName(entry.Key.ToString.Split("_")(0))
            Description = GetAssetDescription(entry.Key.ToString.Split("_")(0))
            Copyright = GetAssetCopyRight(entry.Key.ToString.Split("_")(0))
            Source = GetAssetSource(entry.Key.ToString.Split("_")(0))

            If (Description.Length > 36) Then
                Description = Description.Substring(0, 36)
            End If

            sql = "insert into ipm_order_details (orderid,asset_id,price,shippingcost,additionalcost,discount,quantity,download_id,status,itemname,sourcename,copyright,deliverytype) VALUES (@orderid,@asset_id,@price,@shippingcost,@additionalcost,@discount,@quantity,@download_id,@status,@itemname,@sourcename,@copyright,@deliverytype)"
            sqlCommand = New SqlCommand(sql, MyConnection)
            'sqlCommand.Parameters.Add("@orderitemid", SqlDbType.NVarChar, 255)
            sqlCommand.Parameters.Add("@orderid", SqlDbType.NVarChar, 255)
            sqlCommand.Parameters.Add("@asset_id", SqlDbType.NVarChar, 255)
            sqlCommand.Parameters.Add("@price", SqlDbType.NVarChar, 255)
            sqlCommand.Parameters.Add("@shippingcost", SqlDbType.NVarChar, 255)
            sqlCommand.Parameters.Add("@additionalcost", SqlDbType.NVarChar, 255)
            sqlCommand.Parameters.Add("@discount", SqlDbType.NVarChar, 255)
            sqlCommand.Parameters.Add("@quantity", SqlDbType.NVarChar, 255)
            sqlCommand.Parameters.Add("@download_id", SqlDbType.NVarChar, 255)
            sqlCommand.Parameters.Add("@status", SqlDbType.NVarChar, 255)
            sqlCommand.Parameters.Add("@itemname", SqlDbType.NVarChar, 255)
            sqlCommand.Parameters.Add("@sourcename", SqlDbType.NVarChar, 255)
            sqlCommand.Parameters.Add("@copyright", SqlDbType.NVarChar, 255)
            sqlCommand.Parameters.Add("@deliverytype", SqlDbType.NVarChar, 150)

            'sqlCommand.Parameters("@orderitemid").Value = GetOrderDetailMaxID() + 1
            sqlCommand.Parameters("@orderid").Value = insertID
            sqlCommand.Parameters("@asset_id").Value = entry.Key.ToString.Split("_")(0)
            sqlCommand.Parameters("@price").Value = 0
            sqlCommand.Parameters("@shippingcost").Value = 0
            sqlCommand.Parameters("@additionalcost").Value = 0
            sqlCommand.Parameters("@discount").Value = 0
            sqlCommand.Parameters("@quantity").Value = entry.Value.ToString.Split("_")(0)
            sqlCommand.Parameters("@download_id").Value = entry.Value.ToString.Split("_")(1)
            sqlCommand.Parameters("@status").Value = 0
            sqlCommand.Parameters("@itemname").Value = Name
            sqlCommand.Parameters("@sourcename").Value = Source
            sqlCommand.Parameters("@copyright").Value = Copyright
            Dim deliverytype As String = ""
            Select Case GetDownloadTypeName(entry.Value.ToString.Split("_")(1)).Trim
                Case "Reference Images (e-mailed)"
                    deliverytype = "IDAMEXPRESS"
                Case "Custom Lab prints (mailed)"
                    deliverytype = "PRINT"
                Case "High Resolution Digital Files (mailed CD)"
                    deliverytype = "CD/DVD"
                Case "Digital Prints (mailed)"
                    deliverytype = "PRINT"
                Case "High Resolution Digital Files (e-mailed CD)"
                    deliverytype = "CD/DVD"
            End Select
            sqlCommand.Parameters("@deliverytype").Value = deliverytype





            sqlCommand.ExecuteNonQuery()
            emailItems = emailItems & "Item " & i & vbCrLf
            emailItems = emailItems & "============" & vbCrLf
            emailItems = emailItems & "Quantity: " & entry.Value.ToString.Split("_")(0) & vbCrLf
            emailItems = emailItems & "Photo Number: " & Name & vbCrLf
            emailItems = emailItems & "Title: " & Description & "..." & vbCrLf
            emailItems = emailItems & "Format: " & GetDownloadTypeName(entry.Value.ToString.Split("_")(1)) & vbCrLf
            emailItems = emailItems & "Copyright: " & Copyright & vbCrLf
            emailItems = emailItems & "Source: " & Source & vbCrLf
            emailItems = emailItems & vbCrLf & vbCrLf

            i = i + 1

        Next
        MyConnection.Close()
        emailString = emailString & emailItems & vbCrLf
        'emailString = emailString & "" & vbCrLf & vbCrLf
        emailString = emailString & "Sincerely," & vbCrLf
        emailString = emailString & "USHMM"
        Dim mailMessage As New MailMessage

        Dim MailReceipt As New SmtpClient
        For Each address As String In ConfigurationSettings.AppSettings("MonitorEmail").Split(";")
            mailMessage.To.Add(address)
        Next
        mailMessage.From = New MailAddress(ConfigurationSettings.AppSettings("FromEmail"), "USHMM Photo Archives")
        MailReceipt.Host = ConfigurationSettings.AppSettings("SMTP")
        mailMessage.Subject = "Order # " & insertID & " Received"
        mailMessage.Body = emailMuseumString
        Try
            MailReceipt.Send(mailMessage)
        Catch ex As Exception

        End Try

        Dim mailMessageRequestor As New MailMessage
        mailMessageRequestor.Subject = "USHMM Order # " & insertID & " Received"
        mailMessageRequestor.Body = emailString
        mailMessageRequestor.From = New MailAddress(ConfigurationSettings.AppSettings("FromEmail"), "USHMM Photo Archives")
        mailMessageRequestor.To.Add(Request.Form("email"))
        Try
            MailReceipt.Send(mailMessageRequestor)
        Catch ex As Exception

        End Try
        Return insertID
    End Function

    Public Function CheckUser(ByVal Email As String) As String
        Dim sql As String
        sql = "select userid from ipm_user where active = 'y' and email = @email"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        MyCommand1.SelectCommand.Parameters.Add("@email", SqlDbType.NVarChar, 255)
        MyCommand1.SelectCommand.Parameters("@email").Value = Email
        Dim DT1 As New DataTable("Contact")
        MyCommand1.Fill(DT1)

        If (DT1.Rows.Count > 0) Then
            Return DT1.Rows(0)("userid")
        Else
            Return 0
        End If

    End Function

    Public Function CreateUser(ByVal FirstName As String, ByVal LastName As String, ByVal Email As String, ByVal Phone As String, ByVal Company As String, ByVal Address1 As String, ByVal Address2 As String, ByVal City As String, ByVal State As String, ByVal Zip As String, ByVal Country As String) As String
        Dim insertID As String = GetUserMaxID() + 1

        Dim sql As String = "insert into ipm_user (userid,securitylevel_id,firstname,lastname,email,phone,agency,regdate,regupdatedate,active,createdby,contact) values (@userid,@securitylevel_id,@firstname,@lastname,@email,@phone,@agency,getdate(),getdate(),'Y',@createdby,@contact)"
        '@userid,@securitylevel_id,@firstname,@lastname,@email,@phone,getdate(),getdate(),'Y',@workaddress,@workaddress2,@workcity,@workstate,@workzip,@workcountry,1,1
        'userid, securitylevel_id, FirstName, LastName, Email, Phone, regdate, regupdatedate, active, workaddress, workaddress2, workcity, workstate, workzip, workcountry, createdby, contact
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        MyConnection.Open()
        Dim sqlCommand As SqlCommand = New SqlCommand(sql, MyConnection)

        sqlCommand.Parameters.Add("@userid", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@securitylevel_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@firstname", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@lastname", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@email", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@phone", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@agency", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@createdby", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@contact", SqlDbType.NVarChar, 255)


        sqlCommand.Parameters("@userid").Value = insertID
        sqlCommand.Parameters("@securitylevel_id").Value = 3
        sqlCommand.Parameters("@firstname").Value = FirstName
        sqlCommand.Parameters("@lastname").Value = LastName
        sqlCommand.Parameters("@email").Value = Email
        sqlCommand.Parameters("@phone").Value = Phone
        sqlCommand.Parameters("@agency").Value = Company
        sqlCommand.Parameters("@createdby").Value = 1
        sqlCommand.Parameters("@contact").Value = 1

        sqlCommand.ExecuteNonQuery()

        sql = "insert into ipm_user_field_value (user_id,item_id,item_value) values (@user_id,@item_id,@item_value)"
        sqlCommand = New SqlCommand(sql, MyConnection)
        sqlCommand.Parameters.Add("@user_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_value", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters("@user_id").Value = insertID
        sqlCommand.Parameters("@item_id").Value = 21548418 'idam_user_address
        sqlCommand.Parameters("@item_value").Value = Address1 & vbCrLf & Address2
        sqlCommand.ExecuteNonQuery()

        sql = "insert into ipm_user_field_value (user_id,item_id,item_value) values (@user_id,@item_id,@item_value)"
        sqlCommand = New SqlCommand(sql, MyConnection)
        sqlCommand.Parameters.Add("@user_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_value", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters("@user_id").Value = insertID
        sqlCommand.Parameters("@item_id").Value = 21548420 'idam_user_region
        sqlCommand.Parameters("@item_value").Value = State
        sqlCommand.ExecuteNonQuery()

        sql = "insert into ipm_user_field_value (user_id,item_id,item_value) values (@user_id,@item_id,@item_value)"
        sqlCommand = New SqlCommand(sql, MyConnection)
        sqlCommand.Parameters.Add("@user_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_value", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters("@user_id").Value = insertID
        sqlCommand.Parameters("@item_id").Value = 21548424 'postalcode/zip
        sqlCommand.Parameters("@item_value").Value = Zip
        sqlCommand.ExecuteNonQuery()

        sql = "insert into ipm_user_field_value (user_id,item_id,item_value) values (@user_id,@item_id,@item_value)"
        sqlCommand = New SqlCommand(sql, MyConnection)
        sqlCommand.Parameters.Add("@user_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_id", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters.Add("@item_value", SqlDbType.NVarChar, 255)
        sqlCommand.Parameters("@user_id").Value = insertID
        sqlCommand.Parameters("@item_id").Value = 21548419 'idam_user_country
        sqlCommand.Parameters("@item_value").Value = Country
        sqlCommand.ExecuteNonQuery()

        MyConnection.Close()

        Return insertID
    End Function


    Public Sub buttonSubmit_onClick()
        If Request.Form("mail") = "" And Request.Form("message") = "" And Request.Form("first_name") = "" And Request.Form("fullname") = "" And Request.Form("e_mail") = "" And Request.Form("comments") = "" Then
            Dim theUserID As String = ""
            theUserID = CheckUser(Request.Form("email"))

            If (theUserID = "0") Then
                theUserID = CreateUser(Request.Form("fname"), Request.Form("lname"), Request.Form("email"), Request.Form("phone"), Request.Form("aff"), Request.Form("street"), Request.Form("apt"), Request.Form("city"), Request.Form("state"), Request.Form("zip"), Request.Form("country"))
            End If
            Dim orderid As String = CreateOrder(CreateCarousel(theUserID), theUserID)
            Session("CartItems") = New Hashtable
            Response.Redirect("thankyou.aspx?oid=" & orderid)
        Else
            Response.Write("Thank You (1)")
        End If

    End Sub




    Private Function getDataTable(ByVal query As String) As DataTable
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(query, MyConnection)
        Dim DT1 As New DataTable
        MyCommand1.Fill(DT1)
        Return DT1
    End Function


End Class