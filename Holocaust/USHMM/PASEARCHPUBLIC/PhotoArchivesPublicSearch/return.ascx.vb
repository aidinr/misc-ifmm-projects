﻿Imports System.Net

Partial Public Class _return
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim referringPage As String = ""
        Try
            referringPage = Request.UrlReferrer.ToString()
        Catch ex As Exception
            referringPage = ""
        End Try

        If (referringPage <> "" And InStr(referringPage, System.Configuration.ConfigurationManager.AppSettings("PACURL"), CompareMethod.Text) = 0) Then
            If (referringPage.IndexOf("result.aspx", StringComparison.CurrentCultureIgnoreCase) > 0 Or referringPage.IndexOf("detail.aspx", StringComparison.CurrentCultureIgnoreCase) > 0 Or referringPage.IndexOf("cart.aspx", StringComparison.CurrentCultureIgnoreCase) > 0) Then
                'keep current session based link
                linkReturn.Text = Session("returnLink")
            ElseIf (referringPage.IndexOf("catalog.aspx", StringComparison.CurrentCultureIgnoreCase) > 0) Then
                'Session("returnLink") = "<div class=""box""><p><a href=""catalog.aspx"">NEW QUERY</a></p></div>"
                Session("returnLink") = ""
                linkReturn.Text = Session("returnLink")

            Else
                Dim thePage As String = readPage(referringPage)

                Dim startTag As String = thePage.IndexOf("<title>", StringComparison.CurrentCultureIgnoreCase)
                Dim endTag As String = thePage.IndexOf("</title>", StringComparison.CurrentCultureIgnoreCase)

                Dim theTitle As String = Mid(thePage, startTag + 8, endTag - startTag - 8)


                If (referringPage.IndexOf("ushmm.org", StringComparison.CurrentCultureIgnoreCase) > 0) Then

                    'Dim theTitleArray As String() = theTitle.Split("|")

                    'Session("returnLink") = "<div class=""box""><p><a href=""" & referringPage & """>RETURN TO " & theTitleArray(theTitleArray.Length - 1).Trim & "</a></p></div>"
                    Session("returnLink") = "<div class=""box""><p><a href=""" & referringPage & """>RETURN TO " & theTitle.Trim & "</a></p></div>"
                    linkReturn.Text = Session("returnLink")

                Else
                    'Session("returnLink") = "<div class=""box""><p><a href=""catalog.aspx"">NEW QUERY</a></p></div>"
                    Session("returnLink") = ""
                    linkReturn.Text = Session("returnLink")
                End If
            End If
        Else
            'Session("returnLink") = "<div class=""box""><p><a href=""catalog.aspx"">NEW QUERY</a></p></div>"
            Session("returnLink") = ""
            linkReturn.Text = Session("returnLink")
        End If
       

        



    End Sub

    Function readPage(ByVal theURL As String) As String
        Try

        
            Dim clientreq As HttpWebRequest
            clientreq = HttpWebRequest.Create(theURL)
            Dim clientres As HttpWebResponse = clientreq.GetResponse
            Dim data As IO.Stream = clientres.GetResponseStream
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd

            Return str
        Catch ex As Exception
            Return "<title>Previous Site </title>"
        End Try

    End Function

End Class