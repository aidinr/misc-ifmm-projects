﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient

Imports WebArchives.iDAM.Web.Core

Partial Public Class detail
    Inherits System.Web.UI.Page
    Public spAssetId As String
    Dim pageSize As Integer = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings("pagesize"))
    Public photonum As string
    Private Sub detail_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Response.Redirect("Catalog.aspx")
    End Sub

    Private Function CheckValidPublicAsset(ByVal id As String) As Boolean
        Try
            Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))

            Dim MyCommand3 As SqlDataAdapter = New SqlDataAdapter("select asset_id from ipm_asset_field_value Where item_value like '%1%' and item_id = 21548562 and asset_id = @asset_id", MyConnection)
            Dim DT3 As New DataTable("Check")

            MyCommand3.SelectCommand.Parameters.Add("@asset_id", SqlDbType.NVarChar, 255)
            MyCommand3.SelectCommand.Parameters("@asset_id").Value = id
            MyCommand3.Fill(DT3)
            If DT3.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try

    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim RowNumber As String = Trim(Request.QueryString("id"))
        
        If Request.QueryString("Ssearch") & "" <> "" then
            Session("SearchString")=Request.QueryString("Ssearch")
            Response.Redirect("Result.aspx")
        End If

        
        'select asset_id from ipm_asset_field_value Where item_value like '%1%' and item_id = 21548562
        If Not CheckValidPublicAsset(RowNumber) Then
            Response.Redirect("result.aspx")
            Response.End()
            Exit Sub
        End If

        







        Dim theQuery As String = Server.UrlDecode(Request.QueryString("search"))
       
        Dim downloadType As String = Request.Form("DropDownList1")
        Dim quantity As String = Request.Form("quantity")
        Dim quantityInt As Integer
        If (Int32.TryParse(quantity, quantityInt) = False) Then
            quantityInt = 0
        End If
        Dim cartitems As Hashtable
        'DAMJAN COMMENT
        'DropDownList1.DataSource = GetDownloadType()
        'DropDownList1.DataTextField = "name"
        'DropDownList1.DataValueField = "download_id"
        'DropDownList1.DataBind()
        'DropDownList1.SelectedValue = "21176991" 'Default id for e-mail preview
        cartitems = Session("CartItems")
        


        PopulateFields(RowNumber, theQuery)
        If IsPostBack Then
            'check for cart add
            'add item to cart
            'If (quantityInt > 0) Then
            Session("CartItems") = addItemToCart(spAssetId, 21176991, 1, cartitems)
                'DAMJAN
                'Me.LabelItemsNotice.Text = "Item added to cart."
            'Else
                'DAMJAN
                'e.LabelItemsNotice.Text = "Item was not added to cart. Please input a number larger than 0."
            'End If

        End If
        'DAMJAN
        calcCartItems(cartitems, Me.LabelItems)
        If Request.QueryString("index") Is Nothing Then
            'Me.topNext.Visible = False
            'Me.topPrev.Visible = False
            'Me.bottomNext.Visible = False
            'Me.bottomPrev.Visible = False
            'DAMJAN
            'Me.documentpageindicators.Visible = False
            'Me.linkList.Visible = False
            'Me.LinkListBottom.Visible = False

        End If

        Dim returnControl As Web.UI.Control

        returnControl = Me.LoadControl("return.ascx")
        'DAMJAN
        'placeholderTop.Controls.Add(returnControl)
    End Sub
    Public Shared Sub calcCartItems(ByVal cartitems As Hashtable, ByRef LabelItems As Label)
        LabelItems.Text = cartitems.Count
    End Sub
    Private Function addItemToCart(ByVal assetid As String, ByVal downloadType As String, ByVal quantity As String, ByVal cartitems As Hashtable) As Hashtable
        Dim newquantity As Integer = 0

        If Not cartitems.ContainsKey(assetid & "_" & downloadType) Then
            cartitems.Add(assetid & "_" & downloadType, quantity & "_" & downloadType)
        Else
            For Each entry As DictionaryEntry In cartitems
                If (entry.Key = assetid & "_" & downloadType) Then
                    newquantity = CInt(quantity) + CInt(entry.Value.ToString.Split("_")(0))
                End If
            Next
            cartitems.Remove(assetid & "_" & downloadType)
            cartitems.Add(assetid & "_" & downloadType, newquantity & "_" & downloadType)
        End If
        Return cartitems
    End Function
    Public Sub PopulateFields(ByVal RowNumber As String, ByVal QueryString As String)

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))


        Dim idamSQL As String
        Dim idamSQL2 As String

        Dim sA_mediatype As String = "All"
        Dim sadtcreatemodify As String = "0"
        Dim sadt As String = "2"
        Dim sadtmonth As String = ""
        Dim sadtdays As String = ""
        Dim sadtbetweendate As String = ""
        Dim sadtanddate As String = ""
        Dim sprojectfilter As String = ""
        Dim sfiletypefilter As String = ""
        Dim sservicesfilter As String = ""
        Dim smediatypefilter As String = ""
        Dim sIllustTypeFilter As String = ""
        Dim alistcategoryid As String = ""
        Dim blnRating As String = True
        Dim sProject As String = ""
        Dim sOrderBy As String = ""
        Dim sSortOrder As String = ""
        Dim AssetID As String = RowNumber

        ''idamSQL = WebArchives.iDAM.Web.Core.IDAMQueryService.MakeAssetSQLUseIView("1", "0", QueryString, sA_mediatype, sadtcreatemodify, sadt, sadtmonth, sadtdays, sadtbetweendate, sadtanddate, "", "", "", "", "", sprojectfilter, sfiletypefilter, sservicesfilter, smediatypefilter, sIllustTypeFilter, blnRating, "IDAM_USHMM", "", sProject, sOrderBy, sSortOrder, "", "", "", "", "", "", "", "", "", "", "", False, "0", False, False, "")
        ''idamSQL2 = idamSQL
        ''idamSQL = Replace(idamSQL, "select a.* from(", "select * from (		select row_number() over (order by a.rating desc) as row, a.* from ( select top 10000 a.* from(") + " order by rating desc ) a ) b where (row = @row)"
        ''idamSQL = Replace(idamSQL, "a.update_date,a.asset_id", "a.update_date,a.asset_id,description")
        ''idamSQL2 = Replace(idamSQL2, "select a.* from(", "select count(*) results from (		select row_number() over (order by a.rating desc) as row, a.* from ( select top 10000 a.* from(") + " order by rating desc ) a ) b"
        'idamSQL3Constraints = Replace(idamSQL3Constraints, "select a.* from(", "select asset_id from (		select row_number() over (order by a.rating desc) as row, a.* from ( select top 1000 a.* from(") + " order by rating desc ) a ) b where (row between @rowMin and @rowMax)"
        'idamSQL3Constraints = "select asset_id,description from ipm_asset where asset_id IN (" & idamSQL3Constraints & ")"
        ''Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(idamSQL, MyConnection)
        ''Dim MyCommand4 As SqlDataAdapter = New SqlDataAdapter(idamSQL2, MyConnection)
        ' ''MyCommand2.SelectCommand.Parameters.Add("@row", SqlDbType.NVarChar, 255)
        ' ''MyCommand2.SelectCommand.Parameters("@row").Value = RowNumber


        'put security check in here...

        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter("select name, description from ipm_asset where asset_id = " & AssetID, MyConnection)

        Dim DT2 As New DataTable("Asset")

        MyCommand2.Fill(DT2)
        '' ''MyCommand4.Fill(DT4)


        spAssetId = AssetID
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select item_tag,item_value from ipm_asset_field_desc a, ipm_asset_field_value b where a.item_id = b.item_id and b.asset_id = @asset_id and active = 1 and viewable = 1", MyConnection)
        Dim DT1 As New DataTable("UDF")

        MyCommand1.SelectCommand.Parameters.Add("@asset_id", SqlDbType.NVarChar, 255)
        MyCommand1.SelectCommand.Parameters("@asset_id").Value = AssetID

        Dim MyCommand3 As SqlDataAdapter = New SqlDataAdapter("select dbo.list_tags(@asset_id) tags, dbo.list_services(@asset_id) people, dbo.list_mediatype(@asset_id) location, dbo.list_illusttype(@asset_id) events", MyConnection)
        Dim DT3 As New DataTable("Tags")

        MyCommand3.SelectCommand.Parameters.Add("@asset_id", SqlDbType.NVarChar, 255)
        MyCommand3.SelectCommand.Parameters("@asset_id").Value = AssetID

        MyCommand1.Fill(DT1)
        MyCommand3.Fill(DT3)


        If Not (DT2.Rows.Count = 0) Then
            lblTitle2.Text = DT2(0)("description").split(vbCrLf)(0)
            lblDescription.Text = DT2(0)("description").ToString.Replace(lblTitle2.Text, "")
            lblNumber.Text = "<table cellpadding=""0"" cellspacing=""0"" ><tr><td style=""width:100px""><b>Photograph:</b></td><td>#" & DT2(0)("name").trim & "</td></tr></table>" 
            'DAMJAN
            photonum = DT2(0)("name").trim
            'linkRecognize.NavigateUrl = "http://online.ushmm.org/photoarchive/recognize/?photonum=" & DT2(0)("name").trim
            Dim sIDAM_SOURCE, sIDAM_PROVENANCE, sIDAM_CREDITLINE As String

            sIDAM_SOURCE = ""
            sIDAM_PROVENANCE = ""
            sIDAM_CREDITLINE = ""

            For Each row As DataRow In DT1.Rows
                Select Case row("item_tag")
                    Case "IDAM_COPYRIGHT"
                        lblCopyright.Text = "<table cellpadding=""0"" cellspacing=""0"" ><tr><td style=""width:100px""><b>Copyright:</b></td><td>" & row("item_value") & "</td></tr></table>"
                    Case "IDAM_DESIGNATION_DESCRIPTION"

                        'get designation ctegory

                        lblDesignation.Text = "<a class=""cart"" href=""result.aspx?folderid=" & GetCategoryID(spAssetId) & """>" & row("item_value") & "</a>"
                    Case "IDAM_PLACE_NAME"
                        lblLocation.Text = "<table cellpadding=""0"" cellspacing=""0"" ><tr><td style=""width:100px""><b>Locale:</b></td><td>" & row("item_value") & "</td></tr></table>"

                    Case "IDAM_SOURCE"
                        sIDAM_SOURCE = row("item_value").replace("(Primary)", "").trim
                        'If lblCredit.Text <> "" Then
                        '    lblCredit.Text = row("item_value").replace("(Primary)", "").trim & ", " & lblCredit.Text
                        'Else
                        '    lblCredit.Text = row("item_value").replace("(Primary)", "").trim
                        'End If
                    Case "IDAM_PROVENANCE"
                        sIDAM_PROVENANCE = row("item_value")
                        'If lblCredit.Text <> "" Then
                        '    lblCredit.Text = lblCredit.Text & ", courtesy of " & row("item_value")
                        'Else
                        '    lblCredit.Text = "courtesy of " & row("item_value")
                        'End If
                    Case "IDAM_CREDITLINE"
                        sIDAM_CREDITLINE = row("item_value")

                    Case "IDAM_BIOGRAPHICAL"
                        If lblDescription.Text <> "" Then
                            lblDescription.Text = lblDescription.Text & "<br><br>" & row("item_value").ToString.Replace(vbCrLf, "<br>").Replace(vbCr, "<br>")
                        Else
                            lblDescription.Text = lblDescription.Text & row("item_value").ToString.Replace(vbCrLf, "<br>")
                        End If


                    Case "IDAM_SIMPLE_CAPTION"
                        'DAMJAN
                        'lblSubTitle.Text = row("item_value").ToString.Replace(vbCrLf, "<br>")
                    Case "IDAM_PHOTOGRAPHER"
                        'DAMJAN
                        'lblPhotographer.Text = "<table cellpadding=""0"" cellspacing=""0"" ><tr><td style=""width:120px""><b>Photographer:</b></td><td>" & row("item_value") & "</td></tr></table>"

                    Case "IDAM_EVENT"
                        lblEvent.Text = "<p>" & row("item_value").replace(vbCrLf, "<br>") & "</p>"

                    Case "IDAM_DATE_RANGE_START"
                        Dim tmpDate As String
                        If CType(row("item_value"), Date).Month = "1" And CType(row("item_value"), Date).Day = "1" Or (CType(row("item_value"), Date).Month = "12" And CType(row("item_value"), Date).Day = "30") Then
                            'assume a year
                            If CType(row("item_value"), Date).Year <> "1899" Then
                                tmpDate = CType(row("item_value"), Date).Year
                            End If

                        Else
                            tmpDate = FormatDateTime(CType(row("item_value"), Date), DateFormat.LongDate)
                        End If
                        If Not tmpDate Is Nothing Then


                            If lblDate.Text <> "" Then
                                lblDate.Text = tmpDate & " - " & lblDate.Text.Replace("<br>", "").Replace("<b><i>Date:</i></b> around ", "") & "<br>"
                            Else
                                lblDate.Text = tmpDate
                            End If
                        End If

                    Case "IDAM_DATE_RANGE_END"
                        Dim tmpDate As String
                        If (CType(row("item_value"), Date).Month = "1" And CType(row("item_value"), Date).Day = "1") Or (CType(row("item_value"), Date).Month = "12" And CType(row("item_value"), Date).Day = "30") Then
                            'assume a year
                            tmpDate = CType(row("item_value"), Date).Year
                        Else
                            tmpDate = FormatDateTime(CType(row("item_value"), Date), DateFormat.LongDate)
                        End If
                        If tmpDate <> "1899" Then
                            If lblDate.Text <> "" Then
                                lblDate.Text = lblDate.Text.Replace("<br>", "") & " - " & tmpDate & "<br>"
                            Else
                                lblDate.Text = " around " & tmpDate & "<br>"
                            End If
                        End If





                    Case "IDAM_WORKSHEET"
                        'lblNumber.Text = row("item_value")
                End Select
            Next

            
'            lblDate.Text = "<table cellpadding=""0"" cellspacing=""0"" ><tr><td style=""width:100px""><b>Date:</b></td><td>" & lblDate.Text & "</td></tr></table>"




            If sIDAM_CREDITLINE = "" Then
                If sIDAM_PROVENANCE <> "" And sIDAM_SOURCE <> "" Then
                    lblCredit.Text = sIDAM_SOURCE & ", courtesy of " & sIDAM_PROVENANCE
                Else
                    If sIDAM_SOURCE <> "" Then
                        lblCredit.Text = sIDAM_SOURCE
                    Else
                        lblCredit.Text = "Courtesy of " & sIDAM_PROVENANCE
                    End If
                End If
            Else
                lblCredit.Text = sIDAM_CREDITLINE
            End If
            lblCredit.Text = "<table cellpadding=""0"" cellspacing=""0"" ><tr><td style=""width:100px""><b>Credit:</b></td><td>" & lblCredit.Text & "</td></tr></table>"

            Dim Tags() As String = Split(DT3(0)("tags"), ",")
            Dim People() As String = Split(DT3(0)("people"), ",")
            Dim Location() As String = Split(DT3(0)("location"), ",")
            Dim Events() As String = Split(DT3(0)("events"), ",")

            'clean keywords
            For i As Integer = 1 To Tags.Length
                If InStrRev(Tags(i - 1), " N") > 0 Then
                    'Trim last 2 chars
                    Tags(i - 1) = Tags(i - 1).Substring(0, Tags(i - 1).Length - 2)
                End If
            Next
            For i As Integer = 1 To People.Length
                If InStrRev(People(i - 1), " N") > 0 Then
                    'Trim last 2 chars
                    People(i - 1) = People(i - 1).Substring(0, People(i - 1).Length - 2)
                End If
            Next
            For i As Integer = 1 To Location.Length
                If InStrRev(Location(i - 1), " N") > 0 Then
                    'Trim last 2 chars
                    Location(i - 1) = Location(i - 1).Substring(0, Location(i - 1).Length - 2)
                End If
            Next
            For i As Integer = 1 To Events.Length
                If InStrRev(Events(i - 1), " N") > 0 Then
                    'Trim last 2 chars
                    Events(i - 1) = Events(i - 1).Substring(0, Events(i - 1).Length - 2)
                End If
            Next

            Dim Keywords(Tags.Length + People.Length + Location.Length + Events.Length) As String

            Dim TagsIndex As Integer
            Dim PeopleIndex As Integer
            Dim LocationIndex As Integer
            Dim EventsIndex As Integer

            If (Tags.Length = 0) Then
                TagsIndex = 1
            Else
                TagsIndex = Tags.Length
            End If

            If (People.Length = 0) Then
                PeopleIndex = 1
            Else
                PeopleIndex = People.Length
            End If

            If (Location.Length = 0) Then
                LocationIndex = 1
            Else
                LocationIndex = Location.Length
            End If

            If (Events.Length = 0) Then
                EventsIndex = 1
            Else
                EventsIndex = Events.Length
            End If

            Tags.CopyTo(Keywords, 0)
            People.CopyTo(Keywords, TagsIndex)
            Location.CopyTo(Keywords, TagsIndex + PeopleIndex)
            Events.CopyTo(Keywords, TagsIndex + PeopleIndex + LocationIndex)

            RepeaterKeywords.DataSource = Keywords
            RepeaterKeywords.DataBind()

            Dim ds As DataSet = CType(IDAMWebSession.Session("dsResults"), DataSet)
            If Not IDAMWebSession.Session("ProjectGeneralItemTotal") Is Nothing Then
                'DAMJAN
                'lblTotalDocument.Text = "of " & IDAMWebSession.Session("ProjectGeneralItemTotal")
            End If


            Dim currentindex, iprevassetid, inextassetid As Integer
            currentindex = Request.QueryString("index")
            'DAMJAN
            'lblCurrentDocument.Text = currentindex
            If Not ds Is Nothing Then

                iprevassetid = GetAssetIDbyIndex(ds, currentindex - 1)
                inextassetid = GetAssetIDbyIndex(ds, currentindex + 1)
                If (currentindex = 1) And Not (currentindex = IDAMWebSession.Session("ProjectGeneralItemTotal")) Then
                    topNextLink.NavigateUrl = "detail.aspx?search=" & QueryString & "&id=" & inextassetid & "&index=" & currentindex + 1
                    lnkRightArrow.OnClientClick = "javascript: location.href = 'detail.aspx?search=" & QueryString & "&id=" & inextassetid & "&index=" & currentindex + 1 & "'; return false;"
                    'DAMJAN
                    'bottomNext.NavigateUrl = "detail.aspx?search=" & QueryString & "&id=" & inextassetid & "&index=" & currentindex + 1

                ElseIf (currentindex = IDAMWebSession.Session("ProjectGeneralItemTotal")) Then
                    If currentindex > 1 Then
                        topPrevLink.NavigateUrl = "detail.aspx?search=" & QueryString & "&id=" & iprevassetid & "&index=" & currentindex - 1
                        lnkLeftArrow.OnClientClick = "javascript: location.href = 'detail.aspx?search=" & QueryString & "&id=" & iprevassetid & "&index=" & currentindex - 1 & "';  return false;"
                        'DAMJAN
                        'bottomPrev.NavigateUrl = "detail.aspx?search=" & QueryString & "&id=" & iprevassetid & "&index=" & currentindex - 1
                    Else
                    End If

                Else
                    topNextLink.NavigateUrl = "detail.aspx?search=" & QueryString & "&id=" & inextassetid & "&index=" & currentindex + 1
                    lnkRightArrow.OnClientClick = "javascript: location.href = 'detail.aspx?search=" & QueryString & "&id=" & inextassetid & "&index=" & currentindex + 1 & "'; return false;"
                    'DAMJAN
                    'bottomNext.NavigateUrl = "detail.aspx?search=" & QueryString & "&id=" & inextassetid & "&index=" & currentindex + 1

                    topPrevLink.NavigateUrl = "detail.aspx?search=" & QueryString & "&id=" & iprevassetid & "&index=" & currentindex - 1
                    lnkLeftArrow.OnClientClick = "javascript: location.href = 'detail.aspx?search=" & QueryString & "&id=" & iprevassetid & "&index=" & currentindex - 1 & "';  return false;"
                    'DAMJAN
                    'bottomPrev.NavigateUrl = "detail.aspx?search=" & QueryString & "&id=" & iprevassetid & "&index=" & currentindex - 1

                End If

                Dim PageNumber As Integer = Math.Ceiling(currentindex / pageSize)

                'DAMJAN
                'LinkList.NavigateUrl = "result.aspx?search=" & QueryString & "&page=" & PageNumber
                'DAMJAN
                'LinkListBottom.NavigateUrl = "result.aspx?search=" & QueryString & "&page=" & PageNumber
            End If
        End If

    End Sub



    Public Function GetCategoryID(ByVal assetid) As String
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select category_id from ipm_asset where asset_id = " & assetid, MyConnection)
        Dim DT1 As New DataTable("catid")

        'formsof(inflectional,france) and formsof(inflectional,italy)'

        MyCommand1.Fill(DT1)

        Return DT1.Rows(0)("category_id")

    End Function



    Private Sub RenderPaging()
        Try
            Dim showpreviouslink As String = ""
            Dim shownextlink As String = ""
            Dim ds As DataSet = CType(IDAMWebSession.Session("dsResults"), DataSet)
            Dim currentIndex As Integer = DetermineIndex(ds, Request.QueryString("ID"))
            If currentIndex > 1 Then
                showpreviouslink = "<a href=""AssetDetail.aspx?id=" & GetAssetIDbyIndex(ds, currentIndex - 1) & """><<</a>"
            End If
            If currentIndex < ds.Tables(0).Rows.Count Then
                shownextlink = "<a href=""AssetDetail.aspx?id=" & GetAssetIDbyIndex(ds, currentIndex + 1) & """>>></a>"
            End If
            ''''''''''''''''''''LiteralPagingHolder.Text = showpreviouslink & " " & currentIndex & " of " & ds.Tables(0).Rows.Count & " " & shownextlink
        Catch ex As Exception

        End Try

    End Sub
    Private Function DetermineIndex(ByVal ds As DataSet, ByVal id As String) As Integer
        Dim i As Integer = 1
        For Each row As DataRow In ds.Tables(0).Rows
            If id = CType(row("asset_id"), String) Then
                Return i
            End If
            i = i + 1
        Next
    End Function
    Private Function GetAssetIDbyIndex(ByVal ds As DataSet, ByVal index As Integer) As String
        Dim i As Integer = 1
        For Each row As DataRow In ds.Tables(0).Rows
            If index = i Then
                Return CType(row("asset_id"), String)
            End If
            i = i + 1
        Next
    End Function



    Function BoldSearchQueries(ByVal query As String, ByVal text As String) As String

        Return ""

    End Function

    Public Function GetDownloadType() As DataTable
        Dim sql As String
        sql = "select download_id,name from ipm_downloadtype where active = 1 order by name asc"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("DownloadType")
        MyCommand1.Fill(DT1)
        Return DT1
    End Function


End Class