﻿    <%@ Page Language="vb" EnableViewState="true" AutoEventWireup="false" CodeBehind="Cart.aspx.vb" Inherits="PhotoArchivesPublicSearch.Cart" %>

    <%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shopping Cart</title>
<link href="css/Stylesheet.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript">
    function doSearch() {
        window.location.href = "cart.aspx?Ssearch=" + document.getElementById("ss").value;
    }
</script>
<script type="text/javascript" src="js/main.js"></script>
    <script language="JavaScript">
    <!--
        function popUp(URL) {
            day = new Date();
            id = day.getTime();
            eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=1,menubar=0,resizable=0,width=800,height=800,left = 440,top = 125');");
        }

    //-->
        </script>

        <script type="text/javascript">
            var bln = 0;
            function callbackComplete(sender, eventArgs) {
               // location.href="cart.aspx"
                
            }

            function RemoveItem(id) {
                CallbackUpdateCart.Callback('remove', id);
            }

            function UpdateCart() {
                
                var cartItems = new Array();
                var cartQuantities = new Array();
                var cartDownloadType = new Array();

                cartItems = document.getElementsByName("assetid");
                cartQuantities = document.getElementsByName("quantity");
                cartDownloadType = document.getElementsByTagName("select");
                var cartTuple = new Array(cartItems.length);

                for (i = 0; i < cartItems.length; i++) {

                    cartTuple[i] = cartItems[i].value + "_" + cartQuantities[i].value + "_" + cartDownloadType[i+1].options[cartDownloadType[i+1].selectedIndex].value; 
                    
                }


                CallbackUpdateCart.Callback('update', cartTuple);

            }

            function validateQuantity(quantity) {
                if (quantity.value != parseInt(quantity.value)) {
                    alert("Please input numbers only.");
                    quantity.value = ""
                    quantity.focus();
                }

            }
        </script>

</head>
<body>
<!--HEADER_START-->
<div class="header_main">
<!--	<div class="header_top_main">
    	<div class="header_top">
        	<div class="top1">
            	<div class="top1_left">
                    <div class="navigation">
                    	<ul>
                        	<li><a href="#">EVENTS</a></li>
                            <li><a href="#">HOURS &amp; DIRECTIONS</a></li>
                            <li><a href="#">SUPPORT THE MUSEUM</a></li>
                        </ul>
                    </div>
                    <div class="donet_button"><a href="#">DONATE</a></div>
                </div>
            </div>
            <div class="top2">
            	<div class="top2_left">
                	<ul>
                        <li><a href="#">Learn About</a><br /><span><a href="#">THE HOLOCAUST</a></span></li>
                        <li><a href="#">Remember </a><br /><span><a href="#"> VICTIMS AND SURVIVORS</a></span></li>
                        <li><a href="#">Confront</a><br /><span class="geno_border"><a href="#">GENOCIDE</a></span><span class="anti_nav"><a href="#">ANTISENITISM</a></span></li>
                    </ul>
                </div>
                <div class="top2_right">
                    <form action="/" method="get">
                      <ul>
                    	<li><input type="text" name="q" value="Site Search" onclick="clickclear(this, 'Site Search')" onblur="this.value='Site Search'" /></li>
                        <li class="search_button"><input type="submit" value="Search" /></li>
                      </ul>
                    </form>
                </div>
            </div>
        </div>
    </div> -->
    <div class="header_bot_main">
    	<div class="header_bot">
        	<div class="head_bot_left">
            	<div class="head_bot_nav">
                	<ul>
                    	<li><a href="http://www.ushmm.org">HOME</a></li>
                        <li><img src="images/na_bullet.png" alt="" /><a href="http://www.ushmm.org/research/research-in-collections/search-the-collections">RESEARCH</a></li>
                        <li><img src="images/na_bullet.png" alt="" /><a href="http://www.ushmm.org/research/research-in-collections/overview/photo-archives">PHOTO ARCHIVES</a></li>
                        <li><img src="images/na_bullet.png" alt="" /><a href="#">SEARCH THE COLLECTION</a></li>
                    </ul>
                </div>
                <div class="hed_bot_le_bot">
                	<h2>Photo Archives</h2>
                    <div class="hed_bot_search">
                    	<form method="get" action="/">
                    	  <ul>
                        	<li class="sc"><input type="text" runat="server" id = "ss" name="Search Online Catalog" value="Search Online Catalog" onkeydown="javascript: if (event.keyCode==13) {doSearch(); return false;}" onclick="clickclear(this, 'Search Online Catalog')" onblur="if (this.value == '') {this.value='Search Online Catalog';}" />
                        	<p><select>
                        	<option>All &emsp;</option>
                        	<option>Historical Photo &emsp;</option>
                        	<option>Institutional Photo &emsp;</option>
                        	<option>USHMM Artifact &emsp;</option>
                        	</select></p>
                        	</li>
                                <li class="search_button2"><input type="button" value = "SEARCH" onclick="javascript: doSearch();"/></li>
                          </ul>
                        </form>
                    </div>
                </div>
            </div>
            <div class="head_bot_right">
       	    	<h2><a href="#"><img src="images/hed_bo_ri_text_img.png" alt="" /></a></h2>
            </div>
        </div>
    </div>
</div>
<!--HEADER_END-->
<div class="wrapper">
<!--CONTAIN_START-->
	<div class="contain">
    	<div class="contain_top_shop">
        	<h2>Shopping Cart</h2>
        </div>
        <div class="contain_bot_shop">
        	<div class="con_bot_left">
            	<div class="bot_left_top_shop">
                    <h2>Ordering:</h2>
                    <p>
                        If you would like to use any image(s) from this search, please note the photograph numbers(s)<br />
                        and submit the request form below. The Photo Research Coordinator will then contact you<br />
                        regarding the terms and conditions of use.<span> Due to the high volume of requests, it can take<br />
                        up to three weeks to process requests for permission to use photographs.</span> Requests will be<br />
                        processed in the order they are received.<br /><br />
                        Once you have received permission to use the photographs, you can order a print or high<br />
                        resolution scan of any image in this search. The options for reproductions are as follows:
                    </p>
                <div class="con_bot_box1_shop">
        <form id="form1" action=Cart.aspx runat="server">
        <br />
            <table class="style1" width="100%" cellpadding="0" cellspacing="0" border="0">
                
                        <!--<asp:Label ID="LabelItems" runat="server" EnableTheming="False" Font-Bold="True"></asp:Label>
                        items in Cart&nbsp;-->
                <tr>
                <td width="28"><img src="/images/spacer.gif" width="28" height="1" alt=""></td>
                    <td>
                    
                        
                        <ComponentArt:CallBack ID="CallbackUpdateCart" runat="server" >
                            <ClientEvents>
                                <CallbackComplete EventHandler="callbackComplete" />
                            </ClientEvents>
                            <Content>
                                <asp:Repeater ID="list" runat="Server" OnItemDataBound="list_ItemDataBound">
                         
                                    <ItemTemplate>
                	                    <div class="pro_left_shop">
                	                    <input type="hidden" name="assetid" value="<%#Container.DataItem("asset_id")%>_<%#Container.DataItem("downloadid")%>" />
                    	                    <h2><a href="detail.aspx?id=<%#Container.DataItem("asset_id")%>"><img  src="<%=Session("WSRetreiveAsset") & "qfactor=2&width=120&height=120&crop=0&size=1&type=asset&id=" %><%#Container.DataItem("asset_id")%>" border="0" /></a></h2>
                                            <h3>Photo: #<%#Container.DataItem("name")%></h3>
                  	                    </div>
                  	                    <div class="pro_right_shop" style="width:420px">
                  		                    <h2 >
                        	                    <%#Container.DataItem("description")%>
                                            </h2>
                                            <ul>
                        	                    <li>
                            	                    <div class="quant_shopp">
                                	                    <div class="quan_text_shop">QUANTITY</div>
                                                        <div class="fv">
                                                          <input type="text" class="numberOnly" name="quantity" size="5" maxlength="5" value="<%#GetQuantity(Container.DataItem("asset_id").tostring+"_"+Container.DataItem("downloadid").tostring)%>" onkeyup="validateQuantity(this);">
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                            	                    <div class="type_shopp">
                                	                    <div class="type_text_shop">TYPE</div>
                                                        <div class="fv">
                                                          <asp:DropDownList class="downloadtype" ID="DropDownList1" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                            	                    <div class="update_button"><input type="button" onclick="UpdateCart();" value="Update" /></div>
                                                    <div class="delete_shop"><a href="javascript:RemoveItem('<%#Container.DataItem("asset_id")%>_<%#Container.DataItem("downloadid")%>');">Delete</a></div>
                                                </li>
                                            </ul>
                  	                    </div>


                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                
                            </Content>
                           
                        </ComponentArt:CallBack>
                        <br />
                        <table class="style1">
                            <tr>
                                <td>
                                   <div class="update_button"><input style="width:120px" type="button" id="btnrequestphoto2" value="Request Photos" onclick="popUp('order.aspx');return false;"></input></div>
                                </td>
                                <td align="right">
                                    <!--<asp:Button ID="Button2" runat="server" Text="Update Cart"  />-->
                                    <input type="hidden" id="removeitemholder" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td width="28"><img src="/images/spacer.gif" width="28" height="1" alt=""></td>
                </tr>
            </table>
 
        </form>
              	</div>
            </div>
            <div class="con_bot_right">
            	<h2>file formats:</h2>
                <p>
                	<span>High- Resolution digital files sent by e-mail.</span> High- Resolution digital files sent by e-mail. These are 300 dpi high-resolution jpeg or tiff scans (4MB, 3000 pixels). After we have approved the order, we will put you in touch with our digital lab, with which you must arrange pre-payment by credit card. The service is billed at $80 per hour, with a $20 minimum charge. The turnaround time is 1-2 days. *Rush service is available with applicable fees.<br />
                    <br />
                    <span>Low- Resolution digital files sent by e-mail.</span>These are approximately 100 dpi low-resolution jpeg scans (2MB, 900 pixels). After we have approved the order, we will email you the images.<br /><br />
                    <span>High- Resolution digital files on CD.</span>  [Preferred option for orders of more than 10 photos]. These are 300 dpi high-resolution jpeg scans (4MB, 3000 pixels). After we have approved the order, we will put you in touch with our digital lab, with which you must arrange pre-payment by credit card. The cost is $15 per image; $10 per CD-ROM. CDs are shipped either via U.S. Airmail or via FedEx for which a Fed Ex account number is required. The turnaround time 1-2 week to receive your CD.<br /><br />
                    <span>Digital prints.</span>8 inches by 10 inches print output from 300 dpi high-resolution digital files. After we have approved the order, we will put you in touch with our digital lab, with which you must arrange pre-payment by credit card. The cost is $17 per print. Prints are shipped via U.S. Airmail or via FedEx for which a Fed Ex account number is required. The turnaround time is 1-2 weeks to receive your prints.<br /><br />
                    <span>Custom Lab prints. </span>8 inches by 10 inches print made from copy negatives on file in the US Holocaust Memorial Museum’s collection. Please note, that these are not prints from original negatives, but copy negatives. After we have approved the order, we will send you an invoice for the prints, which must paid for by check or money order. We cannot accept credit card payment. The cost is $22 per print. Prints are shipped via U.S. Airmail or via FedEx for which a Fed Ex account number is required. The turnaround time is 4-5 weeks to receive your prints.<br /><br />
                </p>
            </div>
        </div>
    </div>
<!--CONTAIN_END-->
</div>
<!--FOOTER_START-->
<div class="footer_main">
	<div class="footer_1">
	<!--	<div class="footer_top">
    		<ul>
        	<li><img src="images/footer_round.png" alt=""  /><a href="#">Plan a Visit</a></li>
            <li><img src="images/footer_round.png" alt=""  /><a href="#">The Permanent Exhibition</a></li>
            <li><img src="images/footer_round.png" alt=""  /><a href="#">Special Exhibitions</a></li>
        </ul>
        	<ul>
        	<li><img src="images/footer_round.png" alt=""  /><a href="#">Ask a Question</a></li>
            <li><img src="images/footer_round.png" alt=""  /><a href="#">Academic Research</a></li>
            <li><img src="images/footer_round.png" alt=""  /><a href="#">Family &amp; genealogic Research</a></li>
        </ul>
            <ul>
                <li><img src="images/footer_round.png" alt=""  /><a href="#">New to Teaching<br />&nbsp;&nbsp;&nbsp; the Holocaust?</a></li>
                <li><img src="images/footer_round.png" alt=""  /><a href="#">Find Lesson Plans</a></li>
            </ul>
            <ul>
                <li><img src="images/footer_round.png" alt=""  /><a href="#">Browse Upcoming Courses<br />&nbsp;&nbsp;&nbsp; &amp; Workshops</a></li>
            </ul>    	
    	</div>   -->
    	<div class="footer_bottom">
        	<div class="footer_bo">
        		<div class="footer_bo_top">
            		<div><img src="images/footer_logo.png" alt="" /></div>
            	</div>
                <div class="footer_bo_con">
                	<p>
                    	<span>UNITED STATES HOLOCAUST MEMORIAL MUSEUM</span><br />
                        100 Raoul Wallenberg Place, SW<br />
                        Washington, DC 20024-2126<br />
                        Main telephone: 202.488.0400<br />
                        TTY: 202.488.0406
                    </p>
                </div>
                <div class="footer_bo_bottom">
                	<ul>
                    	<li><a href="http://www.ushmm.org/information/about-the-museum"> ABOUT THE MUSEUM</a></li>
                    	<li><a href="http://www.ushmm.org/information/contact-the-museum">CONTACT THE MUSEUM</a></li>
                        <li><a href="http://www.ushmm.org/copyright-and-legal-information/terms-of-use">TERMS OF USE</a></li>
                        <li><a href="http://www.ushmm.org/copyright-and-legal-information/privacy-policy">PRIVACY</a></li>
                        <li><a href="http://www.ushmm.org/information/access">ACCESSIBILITY</a></li>
                        <li><a href="http://www.ushmm.org/copyright-and-legal-information/legal-and-tax-status-information">LEGAL</a></li>

                    </ul>
                </div>
           	</div>   
        </div>
    </div>
</div>
<!--FOOTER_CLOSE-->
</body>
</html>







    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Research | Collections | Photo Archives Online Catalog</title>
    <link rel="stylesheet" href="css/normal_body.css">
    <link rel="stylesheet" href="css/normal_body_print.css">
    <link rel="stylesheet" href="css/style.css">

        <style type="text/css">
            .style1
            {
                width: 100%;
            }
        </style>
        <style type="text/css">
    #padding { padding: 20px; }

    .box { float: left; width: auto; margin-right: 5px; padding: 3px 10px; color: #cecece; font-weight: bold; text-transform: uppercase; border: 1px outset; background: #d46a29; }
    .box p { margin: 0; padding: 0; }
    .box a:link, .box a:visited, .box a:hover { color: #fff; }
    </style>
    </head>
    <body bgcolor="#D2D7A4" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">
    
    </body>
    </html>
