﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient

Imports WebArchives.Idam.Web.Core

Partial Public Class result
    Inherits System.Web.UI.Page

    Public pageSize As Integer = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings("pagesize"))
    Public ipage As Integer = 0
    Public itemcount As Integer = 0
    Public totalPages As Integer = 0
    Public totalAssets As Integer = 0

    Private Sub result_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Response.Redirect("Catalog.aspx")
    End Sub


    Private Function addItemToCart(ByVal assetid As String, ByVal downloadType As String, ByVal quantity As String, ByVal cartitems As Hashtable) As Hashtable
        Dim newquantity As Integer = 0

        If Not cartitems.ContainsKey(assetid & "_" & downloadType) Then
            cartitems.Add(assetid & "_" & downloadType, quantity & "_" & downloadType)
        Else
            For Each entry As DictionaryEntry In cartitems
                If (entry.Key = assetid & "_" & downloadType) Then
                    newquantity = CInt(quantity) + CInt(entry.Value.ToString.Split("_")(0))
                End If
            Next
            cartitems.Remove(assetid & "_" & downloadType)
            cartitems.Add(assetid & "_" & downloadType, newquantity & "_" & downloadType)
        End If
        Return cartitems
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim DS As New DataSet
        Dim theQuery As String = IIf(Session("SearchString") & "" = "", Request.QueryString("search") & "", Session("SearchString") & "")
        Dim thePage As String = Request.QueryString("page")
        Dim theLocaleFilter As String = Request.QueryString("LocaleFilter") & ""
        Session("LocaleFilter") = theLocaleFilter
        Session("TypeFilter") = Request.QueryString("TypeFilter") & ""
        Session("DateFromFilter") = Request.QueryString("DateFromFilter") & ""
        Session("DateToFilter") = Request.QueryString("DateToFilter") & ""

        If (thePage = "") Then
            thePage = 1
        End If

        Dim cartitems As Hashtable
        If Request.QueryString("AddToCart") & "" <> "" Then
            cartitems = Session("CartItems")
            Session("CartItems") = addItemToCart(Request.QueryString("AddToCart"), 21176991, 1, cartitems)
            Response.Redirect("result.aspx?SortOrder=" & Request("SortOrder")& "&search=" & theQuery & "&page=" & thePage & "&LocaleFilter=" & Session("LocaleFilter") & "&TypeFilter=" & Session("TypeFilter") & "&DateFromFilter=" & Session("DateFromFilter") & "&DateToFilter=" & Session("DateToFilter"), False)

        End If


        DS = GetSearchResults(theQuery, thePage)
        Repeater1.DataSource = DS
        Repeater1.DataBind()
        Repeater3.DataSource = DS
        Repeater3.DataBind()
        Repeater9.DataSource = DS
        Repeater9.DataBind()

        'DAMJAN COMMENT
        'DropDownList1.DataSource = GetDownloadType()
        'DropDownList1.DataTextField = "name"
        'DropDownList1.DataValueField = "download_id"
        'DropDownList1.DataBind()
        'DropDownList1.SelectedValue = "21176991" 'Default id for e-mail preview
        cartitems = Session("CartItems")

        calcCartItems(cartitems, LabelItems)


        'lblTotalAssets.Text = GetTotalAssets()
        lblQueryText.Text = theQuery
        If Not IDAMWebSession.Session("ProjectGeneralItemTotal") Is Nothing Then
            If CType(IDAMWebSession.Session("ProjectGeneralItemTotal"), Integer) > (pageSize * thePage) Then
                '        lblResultTop.Text = pageSize * thePage
            Else
                '       lblResultTop.Text = IDAMWebSession.Session("ProjectGeneralItemTotal")
            End If
        Else
            '  lblResultTop.Text = pageSize * thePage
        End If



        'lblResultBottom.Text = (pageSize * thePage) - (pageSize - 1)


        Dim returnControl As Web.UI.Control

        returnControl = Me.LoadControl("return.ascx")

        '        placeholderTop.Controls.Add(returnControl)

    End Sub
    Public Shared Sub calcCartItems(ByVal cartitems As Hashtable, ByRef LabelItems As Label)
        LabelItems.Text = cartitems.Count
    End Sub

    Public Function GetSearchResults(ByVal QueryString As String, ByVal QueryPage As String) As DataSet
        'To be changed to read from iDAMAssetService

        Dim SearchArray() As String = Split(QueryString)
        Dim ActualQueryString As String



        'ActualQueryString = "formsof(inflectional," & SearchArray(0) & ") "

        'For i As Integer = 1 To UBound(SearchArray)
        'ActualQueryString = ActualQueryString & "and formsof(inflectional," & SearchArray(i) & ") "
        'Next

        ' ActualQueryString = ActualQueryString

        Dim DS As New DataSet
        Dim DSResults As New DataSet

        Dim ResultsSQL As String
        Dim TotalResultsSQL As String
        Dim idamSQL3Constraints As String

        Dim sA_mediatype As String = "All"
        Dim sadtcreatemodify As String = "0"
        Dim sadt As String = "2"
        Dim sadtmonth As String = ""
        Dim sadtdays As String = ""
        Dim sadtbetweendate As String = ""
        Dim sadtanddate As String = ""
        Dim sprojectfilter As String = ""
        Dim sCarouselFilter As String = ""
        Dim sfiletypefilter As String = ""
        Dim sservicesfilter As String = ""
        Dim smediatypefilter As String = ""
        Dim sIllustTypeFilter As String = ""
        Dim alistcategoryid As String = ""
        Dim blnRating As String = True
        Dim sProject As String = ""
        Dim sOrderBy As String = ""
        Dim sSortOrder As String = Request("SortOrder") & ""
        Dim sFolderFilter As String = ""
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))


        'set sotr
        IDAMWebSession.Session("sPGSort") = "DESC"
        If Not Request.QueryString("folderid") Is Nothing Then
            Try

                sFolderFilter = System.Web.HttpUtility.UrlDecode(Replace(CType(Request.QueryString("folderid"), String), "'", ""))
            Catch ex As Exception
                sFolderFilter = ""
            End Try

        Else
            sFolderFilter = ""
        End If

        If Not Request.QueryString("cid") Is Nothing Then
            Try

                sCarouselFilter = System.Web.HttpUtility.UrlDecode(Replace(CType(Request.QueryString("cid"), String), "'", ""))
            Catch ex As Exception
                sCarouselFilter = ""
            End Try

        Else
            sCarouselFilter = ""
        End If


        Dim sSearchType As String = "exact"
        Try
            If QueryString.Split(" ")(1).ToString.Trim.Length > 0 Then
                sSearchType = ""
            End If
        Catch ex As Exception
            sSearchType = "exact"
        End Try

        'get udf filters
        Dim UDFFiltersIDS, UDFFiltersValues As String
        '21575366: IDAM_PHOTO_TYPE()
        '21575367: IDAM_PHOTO_ARTIFACT()
        Select Case Request.QueryString("query_append")
            Case "typeinstphoto"
                UDFFiltersIDS = System.Configuration.ConfigurationManager.AppSettings("IDAMASSETFILTERITEMID") & ",21575366"
                UDFFiltersValues = "1,Institutional"
            Case "typehistphoto"
                UDFFiltersIDS = System.Configuration.ConfigurationManager.AppSettings("IDAMASSETFILTERITEMID") & ",21575366"
                UDFFiltersValues = "1,Historical"
            Case "typeinstartifact"
                UDFFiltersIDS = System.Configuration.ConfigurationManager.AppSettings("IDAMASSETFILTERITEMID") & ",21575367"
                UDFFiltersValues = "1,1"
            Case Else
                UDFFiltersIDS = System.Configuration.ConfigurationManager.AppSettings("IDAMASSETFILTERITEMID")
                UDFFiltersValues = "1"
        End Select

        If Session("LocaleFilter") <> "" Then
            UDFFiltersIDS = UDFFiltersIDS & ",21548330"
            UDFFiltersValues = UDFFiltersValues & "," & Session("LocaleFilter")
        End If
        If Session("TypeFilter") <> "" Then
            UDFFiltersIDS = UDFFiltersIDS & ",21575366"
            UDFFiltersValues = UDFFiltersValues & "," & Session("TypeFilter")
        End If



        IDAMWebSession.Config.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("IDAM.ConnectionString")
        ResultsSQL = WebArchives.iDAM.Web.Core.IDAMQueryService.MakeAssetSQLUseIView("1", "0", QueryString, sA_mediatype, sadtcreatemodify, sadt, sadtmonth, sadtdays, sadtbetweendate, sadtanddate, "", "", "", "", "", sprojectfilter, sfiletypefilter, sservicesfilter, smediatypefilter, sIllustTypeFilter, blnRating, "IDAM_USHMM", "", sProject, sOrderBy, sSortOrder, UDFFiltersIDS, UDFFiltersValues, sCarouselFilter, "", sFolderFilter, "", "", "", sSearchType, "", "", False, "0", False, False, "")

        If Session("DateFromFilter") & "" <> "" And Session("DateToFilter") & "" <> "" Then
            ResultsSQL = ResultsSQL & " and ((select top 1 convert(datetime, case when isdate(item_value) =1 then item_value else '1/1/2099' end) from ipm_asset_field_value v99 where v99.asset_id = a.asset_id and v99.item_id=21548372) between convert(datetime, '" & Session("DateFromFilter") & "') and convert(datetime,'" & Session("DateToFilter") & "')) "
        ElseIf Session("DateToFilter") & "" <> "" Then
            ResultsSQL = ResultsSQL & " and ((select top 1 convert(datetime, case when isdate(item_value) =1 then item_value else '1/1/2099' end) from ipm_asset_field_value v99 where v99.asset_id = a.asset_id and v99.item_id=21548372) < convert(datetime,'" & Session("DateToFilter") & "')) "
        ElseIf Session("DateFromFilter") & "" <> "" Then
            ResultsSQL = ResultsSQL & " and ((select top 1 convert(datetime, case when isdate(item_value) =1 then item_value else '1/1/1900' end) from ipm_asset_field_value v99 where v99.asset_id = a.asset_id and v99.item_id=21548372) > convert(datetime,'" & Session("DateFromFilter") & "')) "

        End If


        ResultsSQL = ResultsSQL.Replace("ipm_asset,*,", "ipm_asset,(search_tags),")
        'ResultsSQL = ResultsSQL.Replace("(.2)", "(.001)")

        'capture dataset here for indexing
        Dim SQLAdAssetList As SqlDataAdapter = New SqlDataAdapter(ResultsSQL & " order by rating desc, asset_id desc", MyConnection)
        'Dim SQLAdAssetList As SqlDataAdapter = New SqlDataAdapter(ResultsSQL, MyConnection)
        Dim DSAssetList As New DataTable("AssetList")
        SQLAdAssetList.Fill(DSAssetList)
        DSResults.Tables.Add(DSAssetList)
        IDAMWebSession.Session("dsResults") = DSResults
        IDAMWebSession.Session("ProjectGeneralItemTotal") = DSResults.Tables(0).Rows.Count


        'Dim tt As String = WebArchives.iDAM.Web.Core.IDAMQueryService.MakeSQLPagedIView(ResultsSQL, 0, IDAMWebSession.Session("ProjectGeneralItemTotal"), 0)
        ipage = QueryPage - 1
        ResultsSQL = WebArchives.iDAM.Web.Core.IDAMQueryService.MakeSQLPagedIView(ResultsSQL, 0, pageSize, ipage)

        ResultsSQL = WebArchives.iDAM.Web.Core.IDAMQueryService.AddAssetAttributesWithDescription(ResultsSQL)



        'ResultsSQL = ResultsSQL.Replace("(filename,name,search_tags,description)", "(search_tags)")

        Dim ResultsSQLFinal As String
        ResultsSQLFinal = ResultsSQL

        ResultsSQL = ResultsSQL.Replace("c.description", "c.description, (select top 1 item_value from ipm_asset_field_Value where item_id = 21548372 and asset_id = c.asset_id) as EventDate ")
        If sSortOrder = "Title" then
             ResultsSQL = Replace(ResultsSQL, "order by a.rating asc", " order by (select top 1 ltrim(rtrim(description)) from ipm_asset where asset_id = a.asset_id) desc ")
             ResultsSQL = Replace(ResultsSQL, "order by a.rating desc", " order by (select top 1 ltrim(rtrim(description)) from ipm_asset where asset_id = a.asset_id) asc ")
             ResultsSQL = Replace(ResultsSQL, "order by rating desc", " order by (select top 1 ltrim(rtrim(description)) from ipm_asset where asset_id = a.asset_id) asc ")
            
        ElseIf sSortOrder = "Date" then
             ResultsSQL = Replace(ResultsSQL, "order by a.rating asc", " order by  (select top 1 convert(datetime, case when isdate(item_value) =1 then item_value else '1/1/2099' end) from ipm_asset_field_Value where item_id = 21548372 and asset_id = a.asset_id) desc ")
             ResultsSQL = Replace(ResultsSQL, "order by a.rating desc", " order by  (select top 1 convert(datetime, case when isdate(item_value) =1 then item_value else '1/1/2099' end) from ipm_asset_field_Value where item_id = 21548372 and asset_id = a.asset_id) asc ")
             ResultsSQL = Replace(ResultsSQL, "order by rating desc", " order by  (select top 1 convert(datetime, case when isdate(item_value) =1 then item_value else '1/1/2099' end) from ipm_asset_field_Value where item_id = 21548372 and asset_id = a.asset_id) asc ")
        End If

        Dim MyResults As SqlDataAdapter = New SqlDataAdapter(ResultsSQL, MyConnection)

        Dim DTResults As New DataTable("Results")

        MyResults.Fill(DTResults)


        DS.Tables.Add(DTResults)


        totalAssets = IDAMWebSession.Session("ProjectGeneralItemTotal")

        totalPages = Int(IDAMWebSession.Session("ProjectGeneralItemTotal") / pageSize)

        lblTotalAssets.Text = FormatNumber(IDAMWebSession.Session("ProjectGeneralItemTotal"), 0, , , TriState.True)



        If (QueryPage = "1") Then
            If totalPages = 0 Then
            Else
                topNextLink.NavigateUrl = "result.aspx?SortOrder=" & Request("SortOrder") & "&search=" & Server.UrlEncode(QueryString) & "&page=" & QueryPage + 1 & "&LocaleFilter=" & Session("LocaleFilter") & "&TypeFilter=" & Session("TypeFilter") & "&DateFromFilter=" & Session("DateFromFilter") & "&DateToFilter=" & Session("DateToFilter")
                bottomNextLink.NavigateUrl = "result.aspx?SortOrder=" & Request("SortOrder") & "&search=" & Server.UrlEncode(QueryString) & "&page=" & QueryPage + 1 & "&LocaleFilter=" & Session("LocaleFilter") & "&TypeFilter=" & Session("TypeFilter") & "&DateFromFilter=" & Session("DateFromFilter") & "&DateToFilter=" & Session("DateToFilter")

            End If


            If totalPages = 0 Then
            Else
                '                bottomNextLink.NavigateUrl = "result.aspx?search=" & Server.UrlEncode(QueryString) & "&page=" & QueryPage + 1
            End If

        ElseIf (QueryPage = totalPages + 1) Then
            topPrevLink.NavigateUrl = "result.aspx?SortOrder=" & Request("SortOrder") & "&search=" & Server.UrlEncode(QueryString) & "&page=" & QueryPage - 1 & "&LocaleFilter=" & Session("LocaleFilter") & "&TypeFilter=" & Session("TypeFilter") & "&DateFromFilter=" & Session("DateFromFilter") & "&DateToFilter=" & Session("DateToFilter")
            bottomPrevLink.NavigateUrl = "result.aspx?SortOrder=" & Request("SortOrder") & "&search=" & Server.UrlEncode(QueryString) & "&page=" & QueryPage - 1 & "&LocaleFilter=" & Session("LocaleFilter") & "&TypeFilter=" & Session("TypeFilter") & "&DateFromFilter=" & Session("DateFromFilter") & "&DateToFilter=" & Session("DateToFilter")

        Else
            topNextLink.NavigateUrl = "result.aspx?SortOrder=" & Request("SortOrder") & "&search=" & Server.UrlEncode(QueryString) & "&page=" & QueryPage + 1 & "&LocaleFilter=" & Session("LocaleFilter") & "&TypeFilter=" & Session("TypeFilter") & "&DateFromFilter=" & Session("DateFromFilter") & "&DateToFilter=" & Session("DateToFilter")
            topPrevLink.NavigateUrl = "result.aspx?SortOrder=" & Request("SortOrder") & "&search=" & Server.UrlEncode(QueryString) & "&page=" & QueryPage - 1 & "&LocaleFilter=" & Session("LocaleFilter") & "&TypeFilter=" & Session("TypeFilter") & "&DateFromFilter=" & Session("DateFromFilter") & "&DateToFilter=" & Session("DateToFilter")
            bottomNextLink.NavigateUrl = "result.aspx?SortOrder=" & Request("SortOrder") & "&search=" & Server.UrlEncode(QueryString) & "&page=" & QueryPage + 1 & "&LocaleFilter=" & Session("LocaleFilter") & "&TypeFilter=" & Session("TypeFilter") & "&DateFromFilter=" & Session("DateFromFilter") & "&DateToFilter=" & Session("DateToFilter")
            bottomPrevLink.NavigateUrl = "result.aspx?SortOrder=" & Request("SortOrder") & "&search=" & Server.UrlEncode(QueryString) & "&page=" & QueryPage - 1 & "&LocaleFilter=" & Session("LocaleFilter") & "&TypeFilter=" & Session("TypeFilter") & "&DateFromFilter=" & Session("DateFromFilter") & "&DateToFilter=" & Session("DateToFilter")

        End If

        'DAMJAN ASSIGN TO LOCALE Filter
        'ResultsSQL = WebArchives.iDAM.Web.Core.IDAMQueryService.MakeAssetSQLUseIView("1", "0", QueryString, sA_mediatype, sadtcreatemodify, sadt, sadtmonth, sadtdays, sadtbetweendate, sadtanddate, "", "", "", "", "", sprojectfilter, sfiletypefilter, sservicesfilter, smediatypefilter, sIllustTypeFilter, blnRating, "IDAM_USHMM", "", sProject, sOrderBy, sSortOrder, UDFFiltersIDS, UDFFiltersValues, sCarouselFilter, "", sFolderFilter, "", "", "", sSearchType, "", "", False, "0", False, False, "")
        'ResultsSQL = ResultsSQL.Replace("ipm_asset,*,", "ipm_asset,(search_tags),")
        Dim ResultsSQL3 = ResultsSQLFinal
        ResultsSQL = "select rtrim(rtrim(reverse( substring(reverse(ltrim(rtriM(v.item_value))), 0, charindex(' ', reverse(ltrim(rtriM(v.item_value)))) )) )) as item_value, count(rtrim(rtrim(reverse( substring(reverse(ltrim(rtriM(v.item_value))), 0, charindex(' ', reverse(ltrim(rtriM(v.item_value)))) )) ))) itemcount from (" & Replace(ResultsSQLFinal, "top " & pageSize, "top 1000000") & ") t join ipm_asset_field_value v on t.asset_id = v.asset_id and v.item_id = (select item_id from ipm_asset_field_desc where item_tag = 'IDAM_PLACE_NAME') and isnull(rtrim(rtrim(reverse( substring(reverse(ltrim(rtriM(v.item_value))), 0, charindex(' ', reverse(ltrim(rtriM(v.item_value)))) )) )), '') <> '' group by rtrim(rtrim(reverse( substring(reverse(ltrim(rtriM(v.item_value))), 0, charindex(' ', reverse(ltrim(rtriM(v.item_value)))) )) )) "
        MyResults = New SqlDataAdapter(ResultsSQL, MyConnection)
        Dim DTResults2 As New DataTable("Results")
        MyResults.Fill(DTResults2)
        Repeater4.DataSource = DTResults2
        Repeater4.DataBind()

        'DAMJAN ASSIGN TO Type Filter
        ResultsSQL3 = "select ltrim(rtrim(v.item_value)) item_value, count(ltrim(rtrim(v.item_value))) itemcount from (" & Replace(ResultsSQL3, "top " & pageSize, "top 1000000") & ") t join ipm_asset_field_value v on t.asset_id = v.asset_id and v.item_id = (select item_id from ipm_asset_field_desc where item_tag = 'IDAM_PHOTO_TYPE') and isnull(ltrim(rtrim(v.item_value)), '') <> ''  group by ltrim(rtrim(v.item_value)) "
        MyResults = New SqlDataAdapter(ResultsSQL3, MyConnection)
        Dim DTResults3 As New DataTable("Results")
        MyResults.Fill(DTResults3)
        Repeater5.DataSource = DTResults3
        Repeater5.DataBind()


        Return DS

    End Function
    Public Function formatdescription(ByVal description As String) As String
        Dim tmpdesc, x As String
        Dim sdesclength As Integer = 160
        tmpdesc = "" 'description.Split(".  ")(0)
        Dim itemcount As Integer = 0
        'description = description.Replace(".  ", "ZZZ")
        description = description.split(vbCrLf)(0)
        For Each item As String In description.Split({".  "},StringSplitOptions.None)
            If itemcount = 0 And ((tmpdesc.Length < sdesclength And description.Length > tmpdesc.Length + 2) Or (tmpdesc.Trim.Length = (InStrRev(tmpdesc.Trim, "Dr", , CompareMethod.Text) + 2)) Or (tmpdesc.Trim.Length = (InStrRev(tmpdesc.Trim, "Mr", , CompareMethod.Text) + 2)) Or (tmpdesc.Trim.Length = (InStrRev(tmpdesc.Trim, "Mrs", , CompareMethod.Text) + 3)) Or (tmpdesc.Trim.Length = (InStrRev(tmpdesc.Trim, "Ms", , CompareMethod.Text) + 2))) Then
                tmpdesc = tmpdesc & item.Trim & ".  "
                itemcount = itemcount + 1
                If InStrRev(tmpdesc.Trim, "Dr", , CompareMethod.Text) > 0 Then
                    itemcount = 0
                End If
            End If
        Next
        Return tmpdesc.Replace("..", ".").Replace(". .", ".")


    End Function

    Public Function formatdescription2(ByVal description As String) As String
        Dim tmpdesc, x As String
        Dim sdesclength As Integer = 160
        tmpdesc = "" 'description.Split(".  ")(0)
        If description <> "" then
            description = description.Replace(vbCrLf, "").Replace(vbCr, "").Replace(formatdescription(description).Trim().Replace(vbCrLf, "").Replace(vbCr, ""), "")
        End If
        For Each item As String In description.Split(". ")
            If (tmpdesc.Length < sdesclength And description.Length > tmpdesc.Length + 2) Or (tmpdesc.Trim.Length = (InStrRev(tmpdesc.Trim, "Dr", , CompareMethod.Text) + 2)) Or (tmpdesc.Trim.Length = (InStrRev(tmpdesc.Trim, "Mr", , CompareMethod.Text) + 2)) Or (tmpdesc.Trim.Length = (InStrRev(tmpdesc.Trim, "Mrs", , CompareMethod.Text) + 3)) Or (tmpdesc.Trim.Length = (InStrRev(tmpdesc.Trim, "Ms", , CompareMethod.Text) + 2)) Then
                tmpdesc = tmpdesc & item.Trim & ".  "
            End If
        Next
        If Not Session("SearchString") & "" = "" Then
            Return tmpdesc.Replace(Session("SearchString") & "", "<SPAN>" & Session("SearchString") & "</SPAN>")
        Else
            Return tmpdesc
        End If

    End Function

    Public Function GetTotalAssets() As String
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select count(*) assets from ipm_asset where available = 'y'", MyConnection)
        Dim DT1 As New DataTable("Assets")

        'formsof(inflectional,france) and formsof(inflectional,italy)'

        MyCommand1.Fill(DT1)

        Return DT1.Rows(0)("assets")

    End Function

    Protected Sub searchCatalog_Click(ByVal sender As Object, ByVal e As EventArgs) Handles searchCatalog.Click
        Dim DS As DataSet
        Session("LocaleFilter") = ""
        Session("TypeFilter") = ""
        Session("DateFromFilter") = ""
        Session("DateToFilter") = ""
        Dim SearchString As String = ""
        If Request.Form("Search Online Catalog") & "" = "Search Online Catalog" Then
            SearchString = ""
        Else
            SearchString = Request.Form("Search Online Catalog")
        End If

        DS = GetSearchResults(SearchString, 1)
        Session("SearchString") = SearchString
        Repeater1.DataSource = DS
        Repeater1.DataBind()
        Repeater3.DataSource = DS
        Repeater3.DataBind()
        Repeater9.DataSource = DS
        Repeater9.DataBind()

        Dim theQuery As String = IIf(Session("SearchString") & "" = "", Request.QueryString("search") & "", Session("SearchString") & "")
        lblQueryText.Text = theQuery

    End Sub

    Public Function GetCategories(assetid As String) As String
        Dim sRootchk As Integer = 0
        Dim MaxLength As Integer = 24
        Dim i As Integer
        Dim srenderedtext, sName As String
        Dim sqltemp As String
        Dim sSeperator = "&nbsp;--&nbsp;"
        Dim rsCatCookie As New DataTable
        Dim bexit As Boolean = False
        Dim CatArray As New ArrayList
        Dim sDelimiter As String = "_777_"

        'you suck!  please make this recursive.
        Do While Not bexit
            If rsCatCookie.Rows.Count = 0 Then
                rsCatCookie = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select * from ipm_asset_category where category_id = (select Category_ID from ipm_asset where asset_id = " & assetid & ")")
            Else
                rsCatCookie = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select * from ipm_asset_category where category_id = " & rsCatCookie.Rows(0)("parent_cat_id"))
            End If
            CatArray.Add(rsCatCookie.Rows(0)("name"))
            If rsCatCookie.Rows(0)("parent_cat_id") = "0" Then
                bexit = True
                Exit Do
            End If

        Loop

        For ii As Integer = CatArray.Count - 1 To 0 Step -1
            If sRootchk = 0 Or CatArray(ii).ToString.Trim <> "Root" Then
                If CatArray(ii).ToString.Trim = "Root" Then
                    sName = "Explore"
                Else
                    sName = CatArray(ii).ToString.Trim
                End If
                srenderedtext = srenderedtext + replace(sName, "&", "&amp;")
                srenderedtext = srenderedtext + sSeperator
                If CatArray(ii).ToString.Trim = "Root" Then
                    sRootchk = 1
                End If
            End If
        Next

        Return Left(srenderedtext, srenderedtext.LastIndexOf("--"))
    End Function

    Protected Sub ClearSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ClearSearch.Click
        searchCatalog_Click(sender, e)
    End Sub
End Class