﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient

Imports WebArchives.iDAM.Web.Core

Partial Public Class Cart
    Inherits System.Web.UI.Page
   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("Ssearch") & "" <> "" then
            Session("SearchString")=Request.QueryString("Ssearch")
            Response.Redirect("Result.aspx")
        End If

    
        Dim cartitems As Hashtable
        cartitems = Session("CartItems")
        PhotoArchivesPublicSearch.detail.calcCartItems(cartitems, Me.LabelItems)
        'If cartitems.Count > 0 Then
        PopulateCart(cartitems)

        Button2.OnClientClick = "UpdateCart()"

        'End If

        'Dim returnControl As Web.UI.Control

        'returnControl = Me.LoadControl("return.ascx")

        'placeholderTop.Controls.Add(returnControl)

    End Sub
    Private Sub PopulateCart(ByVal cartitems As Hashtable)
        If cartitems.Count > 0 Then


            Dim instatement As String = ""
            Dim sqlunion As String = ""
            For Each key As String In cartitems.Keys
                If sqlunion <> "" Then
                    sqlunion += " UNION "
                End If
                sqlunion += "select ipm_asset.*, ipm_downloadtype.name as downloadname, ipm_downloadtype.download_id as downloadid from ipm_asset, ipm_downloadtype where asset_id = " & key.Split("_")(0) & " and download_id = " & key.Split("_")(1) ' cartitems(key).ToString.Split("_")(1)

            Next
            'remove last ','
            Dim sql As String

            Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sqlunion, MyConnection)
            Dim DT1 As New DataTable("Assets")
            MyCommand1.Fill(DT1)


            ' ''For Each row As DataRow In DT1.Rows

            ' ''    For Each entry As DictionaryEntry In cartitems
            ' ''        If (row("asset_id") = entry.Key) Then
            ' ''            row("downloadtype") = entry.Value.ToString.Split("_")(1)
            ' ''        End If
            ' ''    Next

            ' ''Next

            list.DataSource = DT1
            list.DataBind()
        End If

    End Sub

    Public Function GetQuantity(ByVal asset_id As String) As String
        Dim cartitems As Hashtable
        cartitems = Session("CartItems")

        Return cartitems.Item(asset_id).ToString.Split("_")(0)
    End Function

    Private Sub CallbackUpdateCart_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallbackUpdateCart.Callback
        Dim cartitems As Hashtable
        cartitems = Session("CartItems")

        Dim key As String
        Dim quantity As String
        Dim quantityInt As Integer
        Dim downloadtype As String



        If e.Parameters(0) = "remove" Then
            cartitems.Remove(e.Parameters(1))
            Session("CartItems") = cartitems
        ElseIf e.Parameters(0) = "update" Then
            cartitems.Clear()
            For Each tuple As String In e.Parameters(1).Split(",")
                key = tuple.Split("_")(0) + "_" + tuple.Split("_")(3)
                quantity = tuple.Split("_")(2)

                downloadtype = tuple.Split("_")(3)
                If (Int32.TryParse(quantity, quantityInt) = True) Then
                    If (quantityInt > 0) Then
                        cartitems.Add(key, quantityInt & "_" & downloadtype)
                    End If
                End If
            Next
            Session("CartItems") = cartitems
        End If
        If cartitems.Count >= 0 Then
            PopulateCart(cartitems)
        End If
        Me.list.RenderControl(e.Output)
    End Sub

    Public Function GetDownloadType() As DataTable
        Dim sql As String
        sql = "select download_id,name from ipm_downloadtype where active = 1 order by description"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connString"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("DownloadType")
        MyCommand1.Fill(DT1)
        Return DT1
    End Function

    Protected Sub list_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)


        Dim DT1 As DataTable
        Dim i As Integer = 0
        DT1 = GetDownloadType()

        If (e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item) Then


            Dim theList As DropDownList = e.Item.FindControl("DropDownList1")
            theList.DataSource = DT1
            theList.DataTextField = "name"
            theList.DataValueField = "download_id"
            For Each row As DataRow In DT1.Rows
                If (row("download_id") = e.Item.DataItem("downloadid")) Then
                    theList.SelectedValue = row("download_id")
                End If

                i = i + 1
            Next

            theList.DataBind()
        End If
    End Sub



End Class