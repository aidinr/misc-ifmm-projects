﻿Imports System.Web.SessionState
Imports WebArchives.iDAM.WebCommon.Security
Imports System.Net


Public Class Global_asax
    Inherits System.Web.HttpApplication

    Dim sBaseIP As String = System.Configuration.ConfigurationManager.AppSettings("sBaseIP") '"192.168.1.48:8020"
    Dim sBaseInternalIP As String = System.Configuration.ConfigurationManager.AppSettings("sBaseIP") '"208.255.178.48"
    Dim sBaseInstance As String = System.Configuration.ConfigurationManager.AppSettings("sBaseInstance") '"IDAM_USHMM"
    Dim sVSIDAMClientUpload As String = System.Configuration.ConfigurationManager.AppSettings("sVSIDAMClientUpload") '"idamClientUpload"
    Dim sVSIDAMClientDownload As String = System.Configuration.ConfigurationManager.AppSettings("sVSIDAMClientDownload") '"IDAM"
    Dim sVSIDAMClient As String = System.Configuration.ConfigurationManager.AppSettings("sVSIDAMClient") '"IDAMBROWSESERVICE"
    Dim sVSIDAMClient110Upload As String = System.Configuration.ConfigurationManager.AppSettings("sVSIDAMClient110Upload") '"IDAMFileUpload"

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
        'init cart
        Dim Cart As New Hashtable 
        Session("CartItems") = Cart


        ' Code that runs when a new session is started

        Dim assetRetrieve = sBaseIP & "/" & sVSIDAMClient
        Dim assetDownload = sBaseIP & "/" & sVSIDAMClientDownload

        Dim theLogin As String = System.Configuration.ConfigurationManager.AppSettings("IDAMClientlogin")
        Dim thePassword As String = System.Configuration.ConfigurationManager.AppSettings("IDAMClientpassword")
        thePassword = WebArchives.iDAM.WebCommon.Security.Encrypt(thePassword)

        Dim loginStatus As Boolean = False

        Dim loginResult1 As String = ""

        Dim sessionID As String = "(S(" & GetSessionID("http://" & assetRetrieve & "/GetSessionID.aspx") & "))"
        Dim iDAMBrowse As New browsesecure.BrowseService
        iDAMBrowse.Url = "http://" & assetRetrieve & "/" & sessionID & "/" & "BrowseService.asmx"
        'Response.Write(iDAMBrowse.Url)
        Dim x As New WebArchives.iDAM.WebCommon.IDAM6BrowseService("IDAMCLIENT.USHMM.ORG/IMAGES")
        Try

            x.LoginSecure(theLogin, thePassword, "IDAM_USHMM")


            '''''loginResult1 = iDAMBrowse..Login(theLogin, thePassword, sBaseInstance, True)

        Catch Ex As Exception
            Response.Write(Ex)
        End Try

        

        Session("WSRetreiveAsset") = x.GetRetrieveAssetURLBase & "?instance=" & sBaseInstance & "&amp;"
        

    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

    Public Shared Function GetSessionID(ByVal URL As String) As String
        Try
            Dim clientreq As HttpWebRequest
            clientreq = HttpWebRequest.Create(URL)
            clientreq.Timeout = CType(10000, Integer)
            Dim clientres As HttpWebResponse = clientreq.GetResponse
            Dim data As IO.Stream = clientres.GetResponseStream
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            If str.IndexOf("SESSIONID") >= 0 Then
                Dim strtidx As Integer = str.IndexOf("SESSIONID=") + 10
                Return str.Substring(strtidx, str.IndexOf("</span>") - strtidx)
            Else
                Throw New Exception("No session ID found. check URL if it has the proper format" + URL)
            End If
        Catch ex As Exception
            Throw New Exception("WebService not available." + URL)
        End Try
    End Function
End Class