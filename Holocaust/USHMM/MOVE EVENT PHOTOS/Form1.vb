﻿Imports System.Data.OleDb
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.IO
Imports log4net
Imports log4net.Config



Public Class Form1
    Private Shared log As log4net.ILog


    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        log = log4net.LogManager.GetLogger("USHMM_EVENTS_MOVE_LOG")
        log4net.Config.XmlConfigurator.Configure()

        'GetFileContents(System.AppDomain.CurrentDomain.BaseDirectory() + "sqlassetselect.txt")
        Dim source As String = ConfigurationSettings.AppSettings("source")
        Dim destination As String = ConfigurationSettings.AppSettings("destination")

        Try
            Dim sql As String
            Dim assetswithoutmainfile As String = ""
            Dim assetsfound As String = ""
            Dim assetsfoundbadfile As String = ""
            Dim AllFiles As DataTable = GetDataTable(GetFileContents(System.AppDomain.CurrentDomain.BaseDirectory() + "sqlassetselect.txt"), ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM"))
            Dim srow As DataRow
            Dim i As Integer = 0
            ProgressBar1.Maximum = AllFiles.Rows.Count
            For Each srow In AllFiles.Rows
                'does main file exist?
                ProgressBar1.Value = i

                'Dim possiblefiles As String() = Directory.GetFiles(sRepoPath, srow("asset_id").ToString.Trim & ".*")
                'For Each file As String In possiblefiles

                'thumb
                'screen
                'original


                'Next
                If Not IO.File.Exists(source + srow("asset_id").ToString.Trim + "." + srow("extension").ToString.Trim.ToUpper) Then
                    'file doesn exist
                    assetswithoutmainfile += srow("asset_id").ToString.Trim & vbCrLf
                    log.Debug("," & "Original:" & "," & srow("asset_id").ToString.Trim)
                Else
                    'check if main is right format
                    Try
                        'copy orig file
                        IO.File.Copy(source + srow("asset_id").ToString.Trim + "." + srow("extension").ToString.Trim.ToUpper, destination + srow("asset_id").ToString.Trim + "." + srow("extension").ToString.Trim.ToUpper, False)
                        assetsfound = srow("asset_id").ToString.Trim
                    Catch ex As Exception
                        assetsfoundbadfile += srow("asset_id").ToString.Trim & vbCrLf
                        log.Debug("," & "Original:" & "," & srow("asset_id").ToString.Trim)
                    End Try

                    Try
                        'copy screen file
                        IO.File.Copy(source + "screen_" & srow("asset_id").ToString.Trim + "." + srow("extension").ToString.Trim.ToUpper, destination + "screen_" & srow("asset_id").ToString.Trim + "." + srow("extension").ToString.Trim.ToUpper, False)
                    Catch ex As Exception
                        assetsfoundbadfile += "Screen:" & srow("asset_id").ToString.Trim & vbCrLf
                        log.Debug("," & "Screen:" & "," & srow("asset_id").ToString.Trim)
                    End Try

                    Try
                        'copy thumb file
                        IO.File.Copy(source + "thumb_" & srow("asset_id").ToString.Trim + "." + srow("extension").ToString.Trim.ToUpper, destination + "thumb_" & srow("asset_id").ToString.Trim + "." + srow("extension").ToString.Trim.ToUpper, False)

                    Catch ex As Exception
                        assetsfoundbadfile += "Thumb:" & srow("asset_id").ToString.Trim & vbCrLf
                        log.Debug("," & "Thumb:" & "," & srow("asset_id").ToString.Trim)
                    End Try



                End If

                i += 1
                Me.Refresh()
                Me.Update()
            Next

            TextBoxassetsfound.Text = assetsfound
            TextBoxassetsfoundbadfile.Text = assetsfoundbadfile
            TextBoxassetswithoutmainfile.Text = assetswithoutmainfile
            Dim x As Integer
            x = 1
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally

        End Try

    End Sub




    Public Function GetFileContents(ByVal FullPath As String, _
       Optional ByRef ErrInfo As String = "") As String

        Dim strContents As String
        Dim objReader As StreamReader
        Try

            objReader = New StreamReader(FullPath)
            strContents = objReader.ReadToEnd()
            objReader.Close()
            Return strContents
        Catch Ex As Exception
            ErrInfo = Ex.Message
        End Try
    End Function

    Public Shared Sub ExecuteTransaction(ByVal sqlstring As String)
        Dim ConnectionString As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
        Dim connection As New OleDbConnection(ConnectionString)
        Dim command As New OleDbCommand

        Dim transaction As OleDbTransaction

        ' Set the Connection to the new OleDbConnection.
        command.Connection = connection

        ' Open the connection and execute the transaction.
        Try
            connection.Open()

            ' Start a local transaction with ReadCommitted isolation level.
            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            ' Assign transaction object for a pending local transaction.
            command.Connection = connection
            command.Transaction = transaction

            ' Execute the commands.
            command.CommandText = sqlstring
            command.CommandTimeout = 4000
            command.ExecuteNonQuery()


            ' Commit the transaction.
            transaction.Commit()
            Console.WriteLine("Executed.")

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Try to rollback the transaction
            Try
                transaction.Rollback()

            Catch
                ' Do nothing here; transaction is not active.
            End Try
        End Try
        ' The connection is automatically closed when the
        ' code exits the Using block.

    End Sub

    Public Shared Function GetDataTable(ByVal query As String, ByVal ConnectionString As String) As DataTable
        'Dim connection1 As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & System.Web.HttpContext.Current.Server.MapPath(dbFile))
        Dim connection1 As OleDbConnection = New OleDbConnection(ConnectionString)

        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)

        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()

        End Try
        Return table1



    End Function
    Function isullcheck(ByVal v)
        If v Is System.DBNull.Value Then
            isullcheck = ""
        Else
            isullcheck = v
        End If
    End Function



    Private Sub ProgressBar1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProgressBar1.Click

    End Sub
End Class
