﻿Imports System.Data.OleDb
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net.Mail

Imports Leadtools
Imports Leadtools.Codecs

Public Class Form1

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        

    End Sub


    Public Shared Sub ExecuteTransaction(ByVal sqlstring As String)
        Dim ConnectionString As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
        Dim connection As New OleDbConnection(ConnectionString)
        Dim command As New OleDbCommand

        Dim transaction As OleDbTransaction

        ' Set the Connection to the new OleDbConnection.
        command.Connection = connection

        ' Open the connection and execute the transaction.
        Try
            connection.Open()

            ' Start a local transaction with ReadCommitted isolation level.
            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            ' Assign transaction object for a pending local transaction.
            command.Connection = connection
            command.Transaction = transaction

            ' Execute the commands.
            command.CommandText = sqlstring
            command.CommandTimeout = 4000
            command.ExecuteNonQuery()


            ' Commit the transaction.
            transaction.Commit()
            Console.WriteLine("Executed.")

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Try to rollback the transaction
            Try
                transaction.Rollback()

            Catch
                ' Do nothing here; transaction is not active.
            End Try
        End Try
        ' The connection is automatically closed when the
        ' code exits the Using block.

    End Sub

    Public Shared Function GetDataTable(ByVal query As String, ByVal ConnectionString As String) As DataTable
        'Dim connection1 As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & System.Web.HttpContext.Current.Server.MapPath(dbFile))
        Dim connection1 As OleDbConnection = New OleDbConnection(ConnectionString)

        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)

        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()

        End Try
        Return table1



    End Function
    Function isullcheck(ByVal v)
        If v Is System.DBNull.Value Then
            isullcheck = ""
        Else
            isullcheck = v
        End If
    End Function



    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        RasterCodecs.CodecsPath = "C:\IDAM\BIN\CODECS\14.5.0.70(2.0)"
        MsgBox("codecs loaded")
        Dim _codecs As RasterCodecs
        _codecs = New RasterCodecs

        'Kramer_20080925-8956
        Dim image As Leadtools.Codecs.CodecsImageInfo = _codecs.GetInformation("C:\Users\jonburlinson\Desktop\Kramer_20080925-8956.tif", False)

        Dim image3 As IRasterImage = _codecs.Load("C:\Users\jonburlinson\Desktop\Kramer_20080925-8956.tif")
        Try
            Dim sql As String
            Dim assetswithoutmainfile As String = ""
            Dim assetsfound As String = ""
            Dim assetsfoundbadfile As String = ""
            Dim sRepoPath As String = ConfigurationSettings.AppSettings("IDAM.IDAMROOT")
            sql = "select a.asset_id,a.media_type,a.filename,b.extension from ipm_asset a,ipm_filetype_lookup b where a.media_type = b.media_type and a.available = 'Y' and a.filesize = 0 and a.asset_id not in (select refno from [USHMM_PHOTOARCH].[dbo].photarch where sufwsno <> '') and a.media_type not in (5006,21352885,5085,21472985,53181,21335427)"
            Dim AllFiles As DataTable = GetDataTable(sql, ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM"))
            Dim srow As DataRow
            MsgBox("read rows")
            For Each srow In AllFiles.Rows
                'does main file exist?


                'Dim possiblefiles As String() = Directory.GetFiles(sRepoPath, srow("asset_id").ToString.Trim & ".*")
                'For Each file As String In possiblefiles

                'Next
                If Not IO.File.Exists(sRepoPath + srow("asset_id").ToString.Trim + "." + srow("extension").ToString.Trim.ToUpper) Then
                    'file doesn exist
                    assetswithoutmainfile += srow("asset_id").ToString.Trim & vbCrLf
                Else
                    'check if main is right format
                    Try
                        Dim image2 As IRasterImage = _codecs.Load(sRepoPath + srow("asset_id").ToString.Trim + "." + srow("extension").ToString.Trim)

                        assetsfound += srow("asset_id").ToString.Trim + "," + image2.OriginalFormat.ToString & "," & srow("extension").ToString.Trim & vbCrLf
                        image2.Dispose()
                    Catch ex As Exception
                        assetsfoundbadfile += srow("asset_id").ToString.Trim & vbCrLf
                    End Try
                End If



            Next

            TextBoxassetsfound.Text = assetsfound
            TextBoxassetsfoundbadfile.Text = assetsfoundbadfile
            TextBoxassetswithoutmainfile.Text = assetswithoutmainfile
            Dim x As Integer
            x = 1
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally

        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim Sql As String
        Sql = "select * from short_list"
        Dim AllFiles As DataTable = GetDataTable(Sql, ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM"))
        For Each File As DataRow In AllFiles.Rows

        Next
    End Sub
End Class
