﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Services.aspx.vb" Inherits="Services" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<script type="text/javascript" src="js/jquery-1.3.min.js"></script>
<script type="text/javascript" src="js/navi.js"></script>
<script type="text/javascript" src="js/swfobject.js"></script>
<script type="text/javascript" src="js/thickbox-compressed.js"></script>

<style type="text/css">
	@import url("css/thickbox.css");
 	@import url("css/style.css");
</style>
    <title>Ramsgate - Services</title>
</head>
<body style="background:url('images/bg_services.jpg') top center no-repeat #d4bf78;">
    <form id="form1" runat="server">
  <div id="logo">
	<div><a href="Default.aspx"><img src="images/ramsgate_logo.jpg" alt="Ramsgate logo" /></a></div>
	</div>
	<div id="container">
	
		<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td width="218" valign="top">
					<div id="navi">
						<div><a href="about_us.aspx"><img class="navi" src="images/nav_about_off.png" alt="About Us" /></a></div>
						<div><img src="images/nav_gap.jpg" alt="gap" /></div>
						<div><a href="construction.aspx"><img class="navi" src="images/nav_construction_off.png" alt="Construction" /></a></div>
						<div><img src="images/nav_gap.jpg" alt="gap" /></div>
						<div><a href="services.aspx"><img src="images/nav_services_on.png" alt="Services" /></a></div>
						<div><img src="images/nav_gap.jpg" alt="gap" /></div>
						<div><a href="certification.aspx"><img class="navi" src="images/nav_certification_off.png" alt="Certification" /></a></div>
						<div><img src="images/nav_gap.jpg" alt="gap" /></div>
						<div><a href="our_work.aspx"><img class="navi" src="images/nav_work_off.png" alt="Our Work" /></a></div>
						<div><img src="images/nav_gap.jpg" alt="gap" /></div>
						<div><a href="clients.aspx"><img class="navi" src="images/nav_clients_off.png" alt="Contact" /></a></div>
						<div><img src="images/nav_gap.jpg" alt="gap" /></div>
						<div><a href="contact.aspx"><img class="navi" src="images/nav_contact_off.png" alt="Contact" /></a></div>
						<div><a href="http://www.gsa.gov" target="_blank"><img src="images/gsa.png" alt"SBA" /></a></div>
						<div><img src="images/cc.png" alt="Visa and Mastercard" /></div>
						<div><a href="contact_form.asp?KeepThis=true&TB_iframe=true&height=500&width=550" class="thickbox" title="Email to a Friend"><img class="navi" src="images/help.png" alt"How can we help?" /></a></div>
						</div>
			</td>
			<td valign="top">
					<div id="content">
						<div id="content_text" style="margin-bottom:0;">
						<div class="crumbs"><a href="Default.aspx">HOME</a>  &nbsp; | &nbsp; VOICE/VIDEO/DATA/SECURITY</div>
						<img src="images/title_services.jpg" alt="Services" /><br /><br />
						<asp:Literal ID="literalContent" runat="server"></asp:Literal>
						</div>
						<div style="margin-left:20px;margin-bottom:20px;">
						<a href="voice.aspx"><img src="images/voice.jpg" alt="voice" /></a><a href="data.aspx"><img src="images/data.jpg" alt="data" style="margin-left:12px;" /></a><a href="video.aspx"><img src="images/video.jpg" alt="video" style="margin-left:12px;" /></a><a href="security.aspx"><img src="images/security.jpg" alt="security" style="margin-left:12px;" /></a>
						</div>
					</div>
			</td>
			</tr>
		</table>
		
		
	</div>
		<div id="footer">
			<table cellpadding="0" cellspacing="0" width="100%">
			<td align="left" valign="top">
			© 2010 Ramsgate Corporation. Licensed and insured in Washingtotn, DC, Maryland, and Virginia<br />
			817 Heron Drive, Silver Spring, MD 20901<br />
			301.830.5395, Fax 301.431.2101<br />
			<a href="mailto:info@ramsgatecorp.com">info@ramsgatecorp.com</a><br />
			</td>
			<td align="right" valign="top"><a href="http://www.ifmm.com" target="_blank">Designed by IFMM</a></td>
		</table>
		</div>
	
	<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-11196936-1");
pageTracker._trackPageview();
} catch(err) {}</script>
    </form>
</body>
</html>
