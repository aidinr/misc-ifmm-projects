﻿Imports System.IO
Partial Class Services
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Session("login") <> "1") Then
            'Open a file for reading
            Dim FILENAME As String = ConfigurationSettings.AppSettings("incPhysicalPath") & "Services.inc"

            'Get a StreamReader class that can be used to read the file
            Dim objStreamReader As StreamReader
            objStreamReader = File.OpenText(FILENAME)
            Dim contents As String = objStreamReader.ReadToEnd()
            objStreamReader.Close()
            literalContent.Text = contents
        Else
            literalContent.Text = "<iframe src =""" & ConfigurationSettings.AppSettings("incPath") & "Services.inc"" width=""100%"" height=""400px""> </iframe> "
        End If
      

    End Sub
End Class
