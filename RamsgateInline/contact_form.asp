<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
function recaptcha_challenge_writer(publickey)
recaptcha_challenge_writer = "<script type=""text/javascript"">" & "var RecaptchaOptions = {" & "   theme : 'white'," & "   tabindex : 0" & "};" & "</script>" & "<script type=""text/javascript"" src=""http://api.recaptcha.net/challenge?k=" & publickey & "&error=" & request.querystring("captchaerror") & """></script>" & "<noscript>" &   "<iframe src=""http://api.recaptcha.net/noscript?k=" & publickey &"error=" & request.querystring("captchaerror") &  """ frameborder=""1""></iframe><br>" &    "<textarea name=""recaptcha_challenge_field"" rows=""3""cols=""40""></textarea>" &    "<input type=""hidden"" name=""recaptcha_response_field""value=""manual_challenge"">" & "</noscript>"
end function

function recaptcha_confirm(privkey,rechallenge,reresponse)
' Test the captcha field

Dim VarString
VarString = "privatekey=" & privkey & "&remoteip=" & Request.ServerVariables("REMOTE_ADDR") &  "&challenge=" & rechallenge & "&response=" & reresponse

Dim objXmlHttp
Set objXmlHttp = Server.CreateObject("Msxml2.ServerXMLHTTP")
objXmlHttp.open "POST", "http://api-verify.recaptcha.net/verify",False
objXmlHttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
objXmlHttp.send VarString

Dim ResponseString
ResponseString = split(objXmlHttp.responseText, vblf)
Set objXmlHttp = Nothing

if ResponseString(0) = "true" then
  'They answered correctly
   recaptcha_confirm = "true"
else
  'They answered incorrectly
   recaptcha_confirm = ResponseString(1)
end if

end function



On Error Resume Next

Err.Clear

strErrorUrl = "contact.asp"

If Request.ServerVariables("REQUEST_METHOD") = "POST" Then

	Err.Clear

recaptcha_reply = recaptcha_confirm("6Lff4gUAAAAAANHMqyHLvT9iktuUFP0lNzdqAn5e",Request.Form("recaptcha_challenge_field"),Request.Form("recaptcha_response_field"))
	
if(recaptcha_reply = "true") then

'if(true) then
		
	
Set Mail = Server.CreateObject("Persits.MailSender")

			Mail.Host = "mail.ifmm.com" ' Specify a valid SMTP server

                        Mail.From = "support@ifmm.com" ' Specify sender's address

                        Mail.FromName = "Ramsgate Comments Submission" ' Specify sender's name

                        Mail.Subject = "Ramsgate Comments"

                        

                        Mail.AddAddress "alfred@webarchives.com","Alfred"
			Mail.AddAddress "mburlinson@ifmm.com","Mark"
			'Mail.AddAddress "alfred@webarchives.com","Alfred"

			
			strMsgHeader = "The following submitted a comment:" & vbCrLf & vbCrLf

			  strMsgInfo = strMsgInfo & "Name: " & Request("sender_name") & vbCrLf
			  strMsgInfo = strMsgInfo & "Company: " & Request("sender_company") & vbCrLf
			  strMsgInfo = strMsgInfo & "Phone: " & Request("sender_phone") & vbCrLf
			  strMsgInfo = strMsgInfo & "E-mail: " & Request("sender_email") & vbCrLf
			  strMsgInfo = strMsgInfo & "Comments: " & Request("message") & vbCrLf
			  
			strMsgFooter =  vbCrLf & vbCrLf & "End of submission."
			
			Mail.Body =  strMsgHeader & strMsgInfo & strMsgFooter
                                   
                        'on error resume next     

                        Mail.Send

                       

                        If Err <> 0 Then

                                    Response.Redirect "contact_form.asp?status=failed&err=" & err

                        else     

                                   Response.Redirect "contact_form.asp?status=sent"

                        End If

                        set Mail = nothing

	

else
	Response.Redirect "contact.asp?status=failed" & "&captchaerror=" & recaptcha_reply
	end if


End If

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
body, td {
    font-family: Arial;
    font-size: 12px;
    color: #000000;
}
.error 
{
    color: red;
    font-weight: 500;
}
</style>
<script type="text/javascript" src="js/jquery-1.3.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('form').validate();
    });
</script>

<title>Ramsgate - Contact Form</title>
</head>
<body>
 <div>
 	<form method="post" action="contact_form.asp">
	<%if request.querystring("status") = "sent" then response.write "<span class=""error"">Your comment has been sent. Thank you.<br /><br /></span>" end if
	  if request.querystring("status") = "failed" then response.write "<span class=""error"">A technical error has occured. Please try again.<br /><br /></span>" end if
	%>
    <table width="100%" cellpadding="5" cellspacing="5">
        <tr>
        <td colspan="2">
        <b>Fill in the fields below to contact us.</b><br /><br />
        </td>

        </tr>
        <tr>
            <td valign="top"><b>Name:</b></td>
            <td><input type="text" id="sender_name" name="sender_name" size="39" class="required" /></td>
        </tr>
	<tr>
            <td valign="top"><b>Company:</b></td>
            <td><input type="text" id="sender_company" name="sender_company" size="39" class="required" /></td>
        </tr>	
	<tr>
            <td valign="top"><b>Phone:</b></td>
            <td><input type="text" id="sender_phone" name="sender_phone" size="39" class="required" /></td>
        </tr>		
        <tr>
            <td valign="top"><b>E-mail Address:</b></td>
            <td><input type="text" name="sender_email" size="39" class="required email" /></td>

        </tr>
        <tr>
            <td valign="top"><b>Message:</b></td>

            <td><textarea rows="5" cols="30" name="message"></textarea></td>
        </tr>
        <tr>
            <td valign="top"><b>Anti bot Verification:</b></td>
            <td><%response.write recaptcha_challenge_writer("6Lff4gUAAAAAADPKsQ7BHl7nAxqiJkDZGLIKbwp1")%></td>
        </tr>        
        <tr>

            <td valign="top"><b>Send Page</b></td>
            <td>
            <input type="hidden" name="submit" value="1" />
            <input type="submit" value="Submit Comments" />
            </td>

        </tr>
                                      
    </table>
    </form>
    </div>


</body>
</html>
