﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="js/jquery-1.3.min.js"></script>
<script type="text/javascript" src="js/navi.js"></script>
<script type="text/javascript" src="js/swfobject.js"></script>
<script type="text/javascript" src="js/thickbox-compressed.js"></script>
<style type="text/css">
	@import url("css/thickbox.css");
 	@import url("css/style.css");
</style>
<style>
#flash {
	position: absolute;
	left:50%;
	margin-left: -318px;
	z-index: 5;
	top: 0;
}
#navi_home {
	position: absolute;
	left: 50%;
	margin-left: -512px;
	z-index: 10;
	top: 192px;
}
#content_home {
	position: absolute;
	left: 50%;
	margin-left: -318px;
	z-index: 10;
	top: 561px;
	background: #720421;
	width: 827px;
	height: 217px;

}
#content_title_home {
	font-family: helvetica,arial;
	font-size: 26px;
	line-height: 32px;
	color:white;
	
}
.content_text_home {
	font-family: helvetica,arial;
	font-size: 20px;
	line-height: 32px;
	color:white;
}
#content_text_home b {
	font-weight: 500;

}
#footer_home {
	position: absolute;
	left: 50%;
	margin-left: -595px;
	z-index: 10;
	top: 230px;
	width: 980px;
	height: 100px;
	color: #333333;
}
#footer_home a {
	color: #720421;
}
</style>
<title>Ramsgate - Home Page</title>
</head>
<body style="background:url('images/bg_home.jpg') center repeat-y">
    <form id="form1" runat="server">
   <div id="flash"><a href="construction.aspx"><img src="images/index_left.jpg" src="Construction Services" /></a><a href="services.aspx"><img src="images/index_right.jpg" src="Construction Services" /></a></div>
	<div id="logo">
	<div><a href="Default.aspx"><img src="images/ramsgate_logo.jpg" alt="Ramsgate logo" /></a></div>
	</div>
	<div id="navi_home">
					<div><a href="about_us.aspx"><img class="navi" src="images/nav_about_off.png" alt="About Us" /></a></div>
					<div><img src="images/nav_gap.jpg" alt="gap" /></div>
					<div><a href="construction.aspx"><img class="navi" src="images/nav_construction_off.png" alt="Construction" /></a></div>
					<div><img src="images/nav_gap.jpg" alt="gap" /></div>
					<div><a href="services.aspx"><img class="navi" src="images/nav_services_off.png" alt="Services" /></a></div>
					<div><img src="images/nav_gap.jpg" alt="gap" /></div>
					<div><a href="certification.aspx"><img class="navi" src="images/nav_certification_off.png" alt="Certification" /></a></div>
					<div><img src="images/nav_gap.jpg" alt="gap" /></div>
					<div><a href="our_work.aspx"><img class="navi" src="images/nav_work_off.png" alt="Our Work" /></a></div>
					<div><img src="images/nav_gap.jpg" alt="gap" /></div>
					<div><a href="clients.aspx"><img class="navi" src="images/nav_clients_off.png" alt="Contact" /></a></div>
					<div><img src="images/nav_gap.jpg" alt="gap" /></div>
					<div><a href="contact.aspx"><img class="navi" src="images/nav_contact_off.png" alt="Contact" /></a></div>
					<div><a href="http://www.gsa.gov" target="_blank"><img src="images/gsa.png" alt"SBA" /></a></div>
					<div><img src="images/cc.png" alt="Visa and Mastercard" /></div>
					<div><a href="contact_form.asp?KeepThis=true&TB_iframe=true&height=500&width=550" class="thickbox" title="Email to a Friend"><img class="navi" src="images/help.png" alt"How can we help?" /></a></div>
	</div>
	<div id="content_home">
		<div style="margin-left:40px;margin-right:40px;margin-top:25px;margin-bottom:20px;">
		<div id="content_title_home">
		Ramsgate Corporation	
		</div>
	<asp:Literal ID="literalContent" runat="server"></asp:Literal>
	</div>
	<div id="footer_home">
		<table cellpadding="0" cellspacing="0" width="100%">
			<td align="left" valign="top">
			© 2010 Ramsgate Corporation. Licensed and insured in Washingtotn, DC, Maryland, and Virginia<br />
			817 Heron Drive, Silver Spring, MD 20901<br />
			301.830.5395, Fax 301.431.2101<br />
			<a href="mailto:info@ramsgatecorp.com">info@ramsgatecorp.com</a><br />
			</td>
			<td align="right" valign="top"><a href="http://www.ifmm.com" target="_blank">Designed by IFMM</a></td>
		</table>
	</div>
	<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-11196936-1");
pageTracker._trackPageview();
} catch(err) {}</script>
    </form>
</body>
</html>
