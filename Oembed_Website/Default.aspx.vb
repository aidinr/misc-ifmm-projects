﻿Imports System.Web.Services.Description

Partial Class _Default
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       
    End Sub

    Protected Sub btnGetContent_Click(sender As Object, e As System.EventArgs) Handles btnGetContent.Click
        Dim ws As New localhost.WebService
        'Dim request As New OembedWebserviceCall.GetVideoRequest
        Dim jsonResult As String = ""
        Dim htmlResult As String = ""
        Dim xmlResult As String = ""

        ws.PreAuthenticate = False
        ws.Credentials = System.Net.CredentialCache.DefaultCredentials

        jsonResult = ws.GetIDAMAsset(assetId.Text, htmlResult, xmlResult)

        Me.medialiteral.Text = htmlResult

        Me.xmlliteral.Text = xmlResult

        Me.jsonliteral.Text = jsonResult


    End Sub

End Class
