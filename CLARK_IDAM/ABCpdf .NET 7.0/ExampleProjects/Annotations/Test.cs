// ===========================================================================
//	�2006 WebSupergoo. All rights reserved.
// ===========================================================================

using System;
using System.IO;
using System.Collections;
using WebSupergoo.ABCpdf7;
using WebSupergoo.ABCpdf7.Objects;


namespace WebSupergoo.Annotations
{
	public class Test
	{
		class Server
		{
			public static string MapPath(string fileName)
			{
				string theBase = Directory.GetCurrentDirectory();
				return Directory.GetParent(theBase).Parent.FullName + "\\" + fileName;
			}
		}
		public static void Main()
		{
			AnnotationsTest(Server.MapPath("Annotations.pdf"));
			VerifyFileAndMakeReport(Server.MapPath("Annotations.pdf"), Server.MapPath("VerificationReport.pdf"));
		}

		public static void AnnotationsTest(string outputFile)
		{
			try 
			{
				Doc theDoc = new Doc();
				theDoc.Font = theDoc.AddFont("Helvetica");
				theDoc.FontSize = 36;

				//Create interactive form
				InteractiveForm form = new InteractiveForm(theDoc);
				theDoc.Pos.X = 40;
				theDoc.Pos.Y = theDoc.MediaBox.Top - 40;
				theDoc.AddText("Interactive Form annotations");

				//Radio buttons
				form.AddRadioButtonGroup(new string[2] { "40 610 80 650", "40 660 80 700" }, "RadioGroupField", 0);
				theDoc.Pos.String = "100 696";
				theDoc.AddText("RadioButton 1");
				theDoc.Pos.String = "100 646";
				theDoc.AddText("RadioButton 2");

				//Text fields
				FormField text = form.AddTextField("40 530 300 580", "TextField1", "Hello World!");
				text.DefaultAppearance = "/TimesRoman 36 Tf 0 0 1 rg";
				text.BorderColor = "0 0 0";
				text.FillColor = "220 220 220";
				text.TextAlign = "Left";

				text = form.AddTextField("40 460 300 510", "TextField2", "Text Field");
				text.BorderColor = "0 0 0";
				text.DefaultAppearance = "/TimesRoman 36 Tf 0 0 1 rg";
				text.TextAlign = "Left";
				text.SetFlag(FormField.FieldFlag.Password);

				text = form.AddTextField("320 460 370 580", "TextField3", "Vertical");
				text.BorderColor = "0 0 0";
				text.DefaultAppearance = "/TimesRoman 36 Tf 0 0 0 rg";
				text.Rotate = 90;

				//Combobox field
				FormField combo = form.AddChoiceField("ComboBox", "40 390 300 440", "ComboBoxField");
				combo.DefaultAppearance = "/TimesRoman 24 Tf 0 0 0 rg";
				combo.AddOptions(new string[] { "ComboBox Item 1", "ComboBox Item 2", "ComboBox Item 3" });

				//Listbox field
				FormField listbox = form.AddChoiceField("ListBox", "40 280 300 370", "ListBoxField");
				listbox.DefaultAppearance = "/TimesRoman 24 Tf 0 0 0 rg";
				listbox.AddOptions(new string[] { "ListBox Item 1", "ListBox Item 2", "ListBox Item 3" });

				//Checkbox field
				form.AddCheckbox("40 220 80 260", "CheckBoxField", true);
				theDoc.Pos.String = "100 256";
				theDoc.AddText("Check Box");

				//Pushbutton field
				FormField button = form.AddButton("40 160 200 200", "ButtonField", "Button");
				button.BorderColor = "0 0 0";
				button.BorderStyle = "Beveled";

				//Markup annotations
				theDoc.Page = theDoc.AddPage();
				theDoc.Pos.X = 40;
				theDoc.Pos.Y = theDoc.MediaBox.Top - 40;
				theDoc.AddText("Markup annotations");

				SquareAnnotation square = new SquareAnnotation(theDoc, "40 560 300 670", "255 0 0", "0 0 255");
				square.BorderWidth = 8;

				LineAnnotation line = new LineAnnotation(theDoc, "100 565 220 665", "255 0 0");
				line.BorderWidth = 12;
				line.RichTextCaption = "<span style= \"font-size:36pt; color:#FF0000\">Line</span>";

				theDoc.FontSize = 24;
				theDoc.Pos.String = "400 670";
				int id = theDoc.AddText("Underline");
				TextMarkupAnnotation markup = new TextMarkupAnnotation(theDoc, id, "Underline", "0 255 0");
				theDoc.Pos.String = "400 640";
				id = theDoc.AddText("Highlight");

				markup = new TextMarkupAnnotation(theDoc, id, "Highlight", "255 255 0");
				theDoc.Pos.String = "400 610";
				id = theDoc.AddText("StrikeOut");

				markup = new TextMarkupAnnotation(theDoc, id, "StrikeOut", "255 0 0");
				theDoc.Pos.String = "400 580";

				id = theDoc.AddText("Squiggly");
				markup = new TextMarkupAnnotation(theDoc, id, "Squiggly", "0 0 255");

				CircleAnnotation circle = new CircleAnnotation(theDoc, "80 320 285 525", "255 255 0", "255 128 0");
				circle.BorderWidth = 20;
				circle.BorderStyle = "Dashed";
				circle.BorderDash = "[3 2]";

				LineAnnotation arrowLine = new LineAnnotation(theDoc, "385 330 540 520", "255 0 0");
				arrowLine.LineEndingsStyle = "ClosedArrow ClosedArrow";
				arrowLine.BorderWidth = 6;
				arrowLine.FillColor = "255 0 0";

				PolygonAnnotation polygon = new PolygonAnnotation(theDoc, "100 70 50 120 50 220 100 270 200 270 250 220 250 120 200 70", "255 0 0", "0 255 0");
				PolygonAnnotation cloudyPolygon = new PolygonAnnotation(theDoc, "400 70 350 120 350 220 400 270 500 270 550 220 550 120 500 70", "255 0 0", "64 85 255");
				cloudyPolygon.CloudyEffect = 1;

				//Movie annotations
				//WMV is courtesy of NASA - http://www.nasa.gov/wmv/30873main_cardiovascular_300.wmv
				theDoc.Page = theDoc.AddPage();
				theDoc.Pos.X = 40;
				theDoc.Pos.Y = theDoc.MediaBox.Top - 40;
				theDoc.AddText("Multimedia features");

				theDoc.FontSize = 24;

				theDoc.Pos.String = "40 690";
				theDoc.AddText("Flash movie:");
				MovieAnnotation movie1 = new MovieAnnotation(theDoc, "80 420 520 650", Server.MapPath("ABCpdf.swf"));

				theDoc.Pos.String = "40 400";
				theDoc.AddText("Video File:");
				MovieAnnotation movie2 = new MovieAnnotation(theDoc, "80 40 520 360", Server.MapPath("video.wmv"));

				theDoc.Page = theDoc.AddPage();
				theDoc.FontSize = 36;
				theDoc.Pos.X = 40;
				theDoc.Pos.Y = theDoc.MediaBox.Top - 40;
				theDoc.AddText("Other types of annotations");

				//Sticky note annotation
				theDoc.FontSize = 24;
				theDoc.Pos.String = "40 680";
				theDoc.AddText("Text annotation");
				TextAnnotation textAnnotation = new TextAnnotation(theDoc, "340 660 360 680", "550 650 600 750", "6 sets of 13 pages. Trim to 5X7.");

				//File attachment annotation
				theDoc.Pos.String = "40 640";
				theDoc.AddText("File Attachment annotation");
				FileAttachmentAnnotation fileAttachMent = new FileAttachmentAnnotation(theDoc, "340 620 360 640", Server.MapPath("video.WMV"));

				//StampAnnotations
				theDoc.Pos.String = "40 600";
				theDoc.AddText("Stamp annotations");
				StampAnnotation stamp1 = new StampAnnotation(theDoc, "340 560 540 600", "DRAFT", "0 0 128");
				StampAnnotation stamp2 = new StampAnnotation(theDoc, "340 505 540 545", "FINAL", " 0 128 0");
				StampAnnotation stamp3 = new StampAnnotation(theDoc, "340 450 540 490", "NOT APPROVED", "128 0 0");

				// Add signature fields last so that entire document is signed
				theDoc.PageNumber = 1;

				//Signature field
				FormField signature1 = form.AddSignature("40 100 240 150", "Signature1",
					Server.MapPath("JohnSmith.pfx"), "111111", "I am the author", "New York",
					"Digitally signed by {0}\nReason: {2}\nLocation: {3}\nDate: {1:yyyy.MM.dd}");

				//Signature field
				FormField signature2 = form.AddSignature("340 100 540 150", "Signature2",
					Server.MapPath("JohnSmith.pfx"), "111111", "I updated the document", "New York",
					"Digitally signed by {0}\nReason: {2}\nLocation: {3}\nDate: {1:yyyy.MM.dd}");

				theDoc.Save(outputFile);
			}
			catch (Exception e)
			{
				Console.WriteLine("An exception occured during document signing:");
				Console.WriteLine("Message: " + e.Message);
				Console.WriteLine("Source: " + e.Source);
			}
		}

		public static void VerifyFileAndMakeReport(string fileName, string reportFileName)
		{
			try
			{
				Doc reportDoc = new Doc();
				reportDoc.Rect.Inset(20, 20);

				reportDoc.FontSize = 20;
				reportDoc.AddText("Signature verification report\n");
				reportDoc.Pos.Y -= 20;
				reportDoc.FontSize = 16;
				reportDoc.AddText("File name: " + fileName + "\n");

				Doc subjectDoc = new Doc();
				subjectDoc.Read(fileName);

				// certificates are used for validating the X.509 signature
				// you may wish to obtain root certificates from a trusted authority
				string[] certs = Server.MapPath("JohnSmith.cer").Split(new char[] { ';' });

				int sigCount = 0;
				foreach (Field theField in subjectDoc.Form.Fields) {
					if (theField is Signature) {
						sigCount++;
						Signature theSig = (Signature)theField;
						bool certificateValid = theSig.Validate(certs);
						string reportHtml = "Signature name: " + theSig.Name + "<BR>" +
							"Signed by: " + theSig.Signer + "<BR>" +
							"Reason: " + theSig.Reason + "<BR>" +
							"Date (UTC Time): " + theSig.SigningUtcTime + "<BR>" +
							"Location: " + theSig.Location + "<BR>" +
							"Is document modified: " + theSig.IsModified.ToString() + "<BR>" +
							"Is certificate valid: " + certificateValid.ToString() + "<BR>" +
							"Is document valid: " + (certificateValid && !theSig.IsModified).ToString() + "<BR>";

						reportDoc.TextStyle.LineSpacing = 10;
						reportDoc.AddHtml(reportHtml);
					}
				}

				if ( sigCount == 0 )
					reportDoc.AddText("The document is not signed.");
				reportDoc.Save(reportFileName);
			}
			catch (Exception e)
			{
				Console.WriteLine("An exception occured during document signing:");
				Console.WriteLine("Message: " + e.Message);
				Console.WriteLine("Source: " + e.Source);
			}
		}
	}
}

