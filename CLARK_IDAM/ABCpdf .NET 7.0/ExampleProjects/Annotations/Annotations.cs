// ===========================================================================
//	©2006 WebSupergoo. All rights reserved.
// ===========================================================================

using System;
using System.Globalization;
using System.IO;
using WebSupergoo.ABCpdf7;
using WebSupergoo.ABCpdf7.Objects;


namespace WebSupergoo.Annotations
{
	/// <summary>
	/// Base class for all types of annotations
	/// </summary>
	public class Annotation
	{
		/// <summary>
		/// Constructor. Creates annotation and registers it in the page Annots[] array
		/// </summary>
		/// <param name="doc">Document to add annotation to</param>
		protected Annotation(Doc doc)
		{
			mDoc = doc;
			mId = mDoc.AddObject("<< /Type /Annot /F 4>>");
			mDoc.SetInfo(mDoc.Page, "/Annots*[]:Ref", mId.ToString());
			mDoc.SetInfo(mId, "/P:Ref", mDoc.Page.ToString());
		}

		/// <summary>
		/// Construct annotation from the existing object id
		/// </summary>
		/// <param name="fieldId">Annotation id</param>
		/// <param name="doc">Document in which annotation exists</param>
		internal Annotation(int fieldId, Doc doc)
		{
			mId = fieldId;
			mDoc = doc;
		}

		/// <summary>
		/// Internal routine for conversion of color string to pdf format
		/// </summary>
		/// <param name="colorString">Color description in the same format as XColor.String</param>
		/// <returns>pdf array describing color</returns>
		internal static string Color2PdfArray ( string colorString)
		{
			XColor color = new XColor();
			color.String = colorString;
			return "[" + String.Format(NumberFormatInfo.InvariantInfo , "{0:0.#####} {1:0.#####} {2:0.#####}", (double)color.Red / 255 , (double)color.Green / 255, (double)color.Blue / 255) + "]";
		}

		/// <summary>
		/// Internal routine for conversion of double to pdf format
		/// </summary>
		/// <param name="d">Double value</param>
		/// <returns>pdf representaion of the number</returns>
		protected string DoubleToPdfNumber (double d)
		{
			return d.ToString("0.#####");
		}

		/// <summary>
		/// Border width of the annotation
		/// </summary>
		public double BorderWidth
		{
			set 
			{
				mDoc.SetInfo(mId, "/BS/W:Num", DoubleToPdfNumber(value));
			}
		}

		/// <summary>
		/// Border style of the annotation. Possible values are Solid, Dashed, Beveled, Inset and Underline
		/// </summary>
		public string BorderStyle
		{
			set 
			{
				if (value == "Solid" || value == "Dashed" || value == "Beveled" || value == "Inset" || value == "Underline")
					mDoc.SetInfo(mId, "/BS/S:Name", value.Substring(0, 1));
			}
		}

		/// <summary>
		/// Dash pattern of the Border. Used when Borderstyle is "Dashed"
		/// </summary>
		public string BorderDash
		{
			set { mDoc.SetInfo(mId, "/BS/D", value); }
		}

		/// <summary>
		/// Bounding rectangle of the annotation
		/// </summary>
		public string Rect
		{
			set { mDoc.SetInfo(mId, "/Rect:Rect", value); }
		}

		/// <summary>
		/// Color of the annotation. This color is used for the following purposes
		/// - the background of the annotationís icon when closed 
		/// - The title bar of the annotationís pop-up window 
		/// - The border of a link annotation 
		/// </summary>
		public string Color
		{
			set { mDoc.SetInfo(mId, "/C", Color2PdfArray(value)); }
		}

		/// <summary>
		/// Text to be displayed for the annotation 
		/// </summary>
		public string Contents
		{
			set	{ mDoc.SetInfo(mId, "Contents:Text", value); }
		}

		/// <summary>
		/// Internal routine for file embedding
		/// </summary>
		/// <param name="fileName">File name</param>
		/// <returns>Id of the newly created stream</returns>
		protected int EmbedFile(string fileName)
		{
			FileStream file = File.OpenRead(fileName);
			byte[] membuf =new byte[file.Length];
			file.Read(membuf, 0, (int)file.Length);

			int streamId = mDoc.AddObject("<< /Length 0 >>stream\r\nendstream\r\n");
			StreamObject so = (StreamObject)mDoc.ObjectSoup[streamId];
			so.SetData(membuf);
			return streamId;
		}

		/// <summary>
		/// Update the appearance stream for the annotation.
		/// </summary>
		public void UpdateAppearance() {
			IndirectObject io = mDoc.ObjectSoup[mId];
			if (io is ABCpdf7.Objects.Annotation) {
				ABCpdf7.Objects.Annotation a = (ABCpdf7.Objects.Annotation)io;
				a.UpdateAppearance();
			}
		}

		/// <summary>
		/// Id of the annotation object
		/// </summary>
		protected int mId;
		public int Id
		{
			get {return mId;}
		}

		/// <summary>
		/// Parent doc of the annotation
		/// </summary>
		protected Doc mDoc;
	}

	/// <summary>
	/// Form field annotation
	/// </summary>
	public class FormField: Annotation
	{
		/// <summary>
		/// Flags which can be used for different form fields
		/// </summary>
		public enum FieldFlag
		{
			//Buttons
			NoToggleToOff = 15,
			Radio = 16,
			Pushbutton = 17, 
			RadiosInUnison = 26,
			//Text
			Multiline = 13,
			Password = 14,
			FileSelect = 21,
			DoNotSpellCheck = 23,
			DoNotScroll = 24,
			Comb = 25,
			RichText = 26,
			//Choice fields
			Combo = 18,
			Edit = 19,
			Sort = 20,
			MultiSelect = 22,
			CommitOnSelChange = 27
		}

		/// <summary>
		/// Form field constructor
		/// </summary>
		/// <param name="fieldId">Field Id</param>
		/// <param name="doc">Parent document</param>
		internal FormField(int fieldId, Doc doc) :base (fieldId, doc) {}
		/// <summary>
		/// This property is specific for the choice filed objects
		/// </summary>
		/// <param name="options">Array of possible option strings</param>
		public void AddOptions(string[] options)
		{
			string array = "[";
			foreach (string item in options)
				array += "(" + item + ") ";
			array += "]";
			mDoc.SetInfo(mId, "/Opt", array);
		}
		/// <summary>
		/// Set annotation flag
		/// </summary>
		/// <param name="flag">See FieldFlag enum for description of the flags</param>
		public void SetFlag(FieldFlag flag)
		{
			string flagsString = mDoc.GetInfo(mId, "/Ff:Num");
			int flags = int.Parse(flagsString);
			flags |= 1 << ((int)flag -1);
			Flags = flags;
		}

		/// <summary>
		/// Annotation flags
		/// </summary>
		public int Flags
		{
			set {mDoc.SetInfo(mId, "/Ff:Num", value.ToString());}
			get { return int.Parse(mDoc.GetInfo(mId, "/Ff:Num"));}
		}

		/// <summary>
		/// The default appearance string containing a sequence of valid
		/// page-content graphics or text state operators that define such 
		/// properties as the field text size and color
		/// </summary>
		public string DefaultAppearance
		{
			set { mDoc.SetInfo(mId, "/DA:Text", value);}
		}
	
		/// <summary>
		/// Border color of the form field
		/// </summary>
		public string BorderColor
		{
			set { mDoc.SetInfo(mId, "/MK/BC", Color2PdfArray(value));	}
		}
		/// <summary>
		/// Fill color of the form field
		/// </summary>
		public string FillColor
		{
			set { mDoc.SetInfo(mId, "/MK/BG", Color2PdfArray(value) );	}
		}

		/// <summary>
		/// Text align for the form field
		/// </summary>
		public string TextAlign 
		{
			set 
			{
				if (value == "Left")
					mDoc.SetInfo(mId, "/Q:Num", "0");
				else if (value == "Center")
					mDoc.SetInfo(mId, "/Q:Num", "1");
				else if (value == "Right")
					mDoc.SetInfo(mId, "/Q:Num", "2");
			}
		}

		/// <summary>
		/// The number of degrees by which the form field annotation is rotatedcounterclockwise
		///  relative to the page. The value must be a multiple of 90.Default value: 0 
		/// </summary>
		public int Rotate
		{
			set { mDoc.SetInfo(mId, "/MK/R:Num", value.ToString()); }
		}
	}
	/// <summary>
	/// Interactive form class. This class is used to add different types of the form fields
	/// </summary>
	public class InteractiveForm
	{
		/// <summary>
		/// Interactive form class constructor. Adds AcroForm key to the document
		/// </summary>
		/// <param name="inDoc">Parent doc</param>
		public InteractiveForm(Doc inDoc)
		{
			mDoc = inDoc;
			int eid = mDoc.GetInfoInt(mDoc.Root, "/AcroForm:Ref");
			if (eid == 0) {
				string theVal = mDoc.GetInfo(mDoc.Root, "/AcroForm");
				if (theVal == "")
					theVal = "<< /Fields [] >>";
				eid = mDoc.AddObject(theVal);
			}
			//add font for text fields
			int theFont = mDoc.AddFont("Times-Roman");
			mDoc.SetInfo(eid, "/DR/Font/TimesRoman:Ref", theFont.ToString());
			mDoc.SetInfo(mDoc.Root, "/AcroForm:Ref", eid.ToString());
		}

		/// <summary>
		/// Add check box form field
		/// </summary>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <param name="inValue">Initial value</param>
		/// <returns>Form field object</returns>
		public FormField AddCheckbox(string inRect, string inName, bool inValue)
		{
			MakeDefaultCheckBoxAppearance();
			int fieldId = mDoc.AddObject("<< /Type /Annot /Subtype /Widget /FT /Btn /F 4 >>");
			RegisterField(fieldId, inName, inRect);
			mDoc.SetInfo(fieldId, "/AP/N/Yes:Ref", mYesCheckBoxAppearance.ToString());
			mDoc.SetInfo(fieldId, "/AP/N/Off:Ref", mOffCheckBoxAppearance.ToString());
			string state = (inValue)? "Yes" : "Off";
			mDoc.SetInfo(fieldId, "/V:Name", state);
			mDoc.SetInfo(fieldId, "/AS:Name", state);
			return new FormField(fieldId, mDoc);
		}

		/// <summary>
		/// Add choice form field
		/// </summary>
		/// <param name="type">Type of the choice field. Possible values are Combobox and Listbox</param>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <returns>Form field object</returns>
		public FormField AddChoiceField(string type, string inRect, string inName)
		{
			if (type == "ComboBox" || type == "ListBox")
			{
				int fieldId = mDoc.AddObject("<< /FT /Ch /Ff 0 /MK << /BG [ 1 ] /BC [ 0 0 0 ]>>"+
					"/Type /Annot /Subtype /Widget /F 4  /DA (/TimesRoman 12 Tf 0 g) /Opt [] /I [0]>>");
				if (type == "ComboBox")
					mDoc.SetInfo(fieldId, "/Ff:Num", "131072");
				RegisterField(fieldId, inName, inRect);
				return new FormField(fieldId, mDoc);
			}
			return null;
		}

		/// <summary>
		/// Add text field
		/// </summary>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <param name="inText">Initial value</param>
		/// <returns>Form field object</returns>
		public FormField AddTextField(string inRect, string inName, string inText)
		{
			int	fieldId = mDoc.AddObject("<< /Type /Annot /Subtype /Widget /FT /Tx /Ff 4096 /F 4 /Q 1 >>");
			mDoc.SetInfo(fieldId, "/V:Text", inText);
			RegisterField(fieldId, inName, inRect);
			return new FormField(fieldId, mDoc);
		}

		/// <summary>
		/// Add signature field
		/// </summary>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <param name="inFileName">Path to the X.509 file containing the private key</param>
		/// <param name="inFileKey">Password required to use the private key</param>
		/// <param name="inReason">Reason why the signature is being signed (may be null)</param>
		/// <param name="inLocation">Location at which the signing is taking place (may be null)</param>
		/// <returns>Form field object</returns>
		public FormField AddSignature(string inRect, string inName, string inFileName, string inFileKey, string inReason, string inLocation)
		{
			return AddSignature(inRect, inName, inFileName, inFileKey, inReason, inLocation, null);
		}

		/// <summary>
		/// Add signature field
		/// </summary>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <param name="inFileName">Path to the X.509 file containing the private key</param>
		/// <param name="inFileKey">Password required to use the private key</param>
		/// <param name="inReason">Reason why the signature is being signed (may be null)</param>
		/// <param name="inLocation">Location at which the signing is taking place (may be null)</param>
		/// <param name="inAppearanceTextFormat">Format string of the Text (may be null). {0} is the Signer, {1} is the Date/Time (UTC), {2} is the Reason, and {3} is the Location.</param>
		/// <returns>Form field object</returns>
		public FormField AddSignature(string inRect, string inName, string inFileName, string inFileKey, string inReason, string inLocation, string inAppearanceTextFormat) {
			if (mSig != null) {
				mSig.Commit();
				mSig = null;
			}

			// NB If you don't want your signature to print then set the /F flag to 0
			int fieldId = mDoc.AddObject("<< /DA (/TimesRoman 0 Tf 0 g) /Subtype /Widget /F 4 /FT /Sig /Type /Annot >>");
			int sigDictId = mDoc.AddObject(@"<< /Filter /Adobe.PPKLite /SubFilter /adbe.pkcs7.detached>>");
			mDoc.SetInfo(fieldId, "/V:Ref", sigDictId.ToString());
			RegisterField(fieldId, inName, inRect);

			mDoc.Form.Refresh();
			mSig = (WebSupergoo.ABCpdf7.Objects.Signature)mDoc.Form[inName];
			mSig.Sign(inFileName, inFileKey);
			if (inReason != null)
				mDoc.SetInfo(sigDictId, "/Reason:Text", inReason);
			if (inLocation != null)
				mDoc.SetInfo(sigDictId, "/Location:Text", inLocation);
			if (mSig.Signer != null)
				mDoc.SetInfo(sigDictId, "/Name:Text", mSig.Signer);
			if (inAppearanceTextFormat != null) {
				XRect rect = new XRect();
				rect.String = mDoc.GetInfo(fieldId, "rect");
				rect.Pin = 1;
				rect.Width = 200;	// Change this to fit the Text
				rect.Height = 60;	// Change this to fit the Text
				mDoc.SetInfo(fieldId, "/Rect:Rect", rect.String);
				rect.String = mDoc.GetInfo(fieldId, "rect");

				string theRect = mDoc.Rect.String;
				int theFont = mDoc.Font;
				int fontSize = mDoc.FontSize;
				double charSpacing = mDoc.TextStyle.CharSpacing;
				double lineSpacing = mDoc.TextStyle.LineSpacing;

				mDoc.Rect.String = rect.String;
				int fontId = mDoc.AddFont("Times-Roman");
				mDoc.Font = fontId;
				mDoc.FontSize = 12;
				mDoc.TextStyle.CharSpacing = 0;
				mDoc.TextStyle.LineSpacing = 2;
				string text = string.Format(inAppearanceTextFormat,
					mSig.Signer, mSig.SigningUtcTime, inReason, inLocation);
				int textId = mDoc.AddText(text);

				string textStream = mDoc.GetInfo(textId, "stream");
				int l1 = textStream.IndexOf("/Fabc", 0, textStream.Length);
				int l2 = textStream.IndexOf(" ", l1, textStream.Length - l1);
				string fontOperator = textStream.Substring(l1, l2 - l1);

				mDoc.Delete(textId);
				mDoc.Rect.String = theRect;
				mDoc.Font = theFont;
				mDoc.FontSize = fontSize;
				mDoc.TextStyle.CharSpacing = charSpacing;
				mDoc.TextStyle.LineSpacing = lineSpacing;

				string xObjString = "<< /Resources << /ProcSet [ /PDF /Text ] >> /Subtype /Form /Type /XObject"
					+ " /BBox [" + rect.String + "] >>stream\n";
				int appearanceId = mDoc.AddObject(xObjString + textStream + "\n endstream");
				mDoc.SetInfo(appearanceId, "/Resources/Font" + fontOperator + ":Ref", fontId.ToString());

				mDoc.SetInfo(fieldId, "/AP*/N:Ref", appearanceId.ToString());
			}

			int eid = int.Parse(mDoc.GetInfo(mDoc.Root, "/AcroForm:Ref"));
			mDoc.SetInfo(eid, "/SigFlags:Num", "3");
			return new FormField(fieldId, mDoc);
		}

		/// <summary>
		/// Add button field
		/// </summary>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <param name="inCaption">Caption of the button</param>
		/// <returns>Form field object</returns>
		public FormField AddButton(string inRect, string inName, string inCaption)
		{
			int fieldId = mDoc.AddObject("<< /FT /Btn /F 4 /Ff 65536 /MK <</BG [ 0.752930 ]/CA (test) >> /Type /Annot /Subtype /Widget /DA (/HeBo 18 Tf 0 g) >>");
			mDoc.SetInfo(fieldId, "/MK/CA:Text", inCaption);
			RegisterField(fieldId, inName, inRect);
			return new FormField(fieldId, mDoc);
		}

		/// <summary>
		/// Add radio button group
		/// </summary>
		/// <param name="inRects">Radio buttons rectangles</param>
		/// <param name="inName">Name of the radio button form field</param>
		/// <param name="SelectedButton">Number of initially selected button</param>
		/// <returns>Form field object</returns>
		public FormField AddRadioButtonGroup(string[] inRects, string inName, int SelectedButton)
		{
			MakeDefaultRadioBtnAppearance();
			int fieldId = mDoc.AddObject("<< /FT /Btn /Ff 49152 /Kids [] /DA (/ZaDb 0 Tf 0 g)>>");

			mDoc.SetInfo(fieldId, "/T:Text", inName);
			mDoc.SetInfo(fieldId, "/V:Name", SelectedButton.ToString());

			int eid = int.Parse(mDoc.GetInfo(mDoc.Root, "/AcroForm:Ref"));
			mDoc.SetInfo(eid, "/Fields*[]:Ref", fieldId.ToString());

			for (int i=0; i < inRects.Length; i++)
			{
				string buttonName = i.ToString();
				int button = mDoc.AddObject("<</Type /Annot /Subtype /Widget /F 4 >>");
				mDoc.SetInfo(button, "/Parent:Ref", fieldId.ToString());
				if (i == SelectedButton)
					mDoc.SetInfo(button, "/AS:Name", buttonName);
				else
					mDoc.SetInfo(button, "/AS:Name", "Off");
				mDoc.SetInfo(button, "/P:Ref", mDoc.Page.ToString());
				mDoc.SetInfo(button, "/Rect:Rect", inRects[i]);

				mDoc.SetInfo(button, "/AP/N/" + buttonName + ":Ref", mYesRadioBtnAppearance.ToString());
				mDoc.SetInfo(button, "/AP/N/Off:Ref", mOffRadioBtnAppearance.ToString());
				mDoc.SetInfo(mDoc.Page, "/Annots*[]:Ref", button.ToString());
				mDoc.SetInfo(fieldId, "/Kids[]:Ref", button.ToString());
			}
			return new FormField(fieldId, mDoc);
		}

		/// <summary>
		/// Register form field in the AcroForm dictionary and in the page Annots array
		/// </summary>
		/// <param name="fieldId"></param>
		/// <param name="fieldName"></param>
		/// <param name="fieldRect"></param>
		private void RegisterField(int fieldId, string fieldName,  string fieldRect)
		{
			mDoc.SetInfo(fieldId, "/T:Text", fieldName);
			mDoc.SetInfo (mDoc.Page, "/Annots*[]:Ref", fieldId.ToString());
			mDoc.SetInfo (fieldId, "/P:Ref", mDoc.Page.ToString());
			int eid = int.Parse(mDoc.GetInfo(mDoc.Root, "/AcroForm:Ref"));
			mDoc.SetInfo(eid, "/Fields*[]:Ref", fieldId.ToString());
			mDoc.SetInfo(fieldId, "/Rect:Rect", fieldRect);
		}

		/// <summary>
		/// Create default appearances for check box fields
		/// </summary>
		public void MakeDefaultCheckBoxAppearance()
		{
			if ( mYesCheckBoxAppearance < 0 && mOffCheckBoxAppearance < 0)
			{
				const string xObjString = "<< /Resources << /ProcSet [ /PDF /Text ] >> " +
					"/Subtype /Form /Type /XObject /BBox [ 0 0 20 20 ] >>stream\n" +
					"q 2 w 1 g 0 0 20 20 re f 0 g 0 0 20 20 re s BT /ZaDb 20 Tf 3 3 Td (MARKER) Tj ET Q\n" +
					"endstream";

				int theFont = mDoc.AddFont("ZapfDingbats");
				mYesCheckBoxAppearance = mDoc.AddObject(xObjString.Replace("MARKER", "8"));
				mOffCheckBoxAppearance = mDoc.AddObject(xObjString.Replace("MARKER", " "));
				mDoc.SetInfo(mYesCheckBoxAppearance, "/Resources/Font/ZaDb:Ref", theFont.ToString());
				mDoc.SetInfo(mOffCheckBoxAppearance, "/Resources/Font/ZaDb:Ref", theFont.ToString());
			}
		}

		/// <summary>
		/// Create default appearances for radio button fields
		/// </summary>
		public void MakeDefaultRadioBtnAppearance()
		{
			if (mYesRadioBtnAppearance < 0 && mOffRadioBtnAppearance < 0)
			{
				string theRect = mDoc.Rect.String;
				mDoc.Rect.String = "1 1 19 19";
				int id = mDoc.AddOval(false);
				string circle = mDoc.GetInfo(id, "stream");
				mDoc.Delete(id);
				string theColor = mDoc.Color.String;
				mDoc.Color.String = "0 0 0";
				mDoc.Rect.String = "5 5 15 15";
				id = mDoc.AddOval(true);
				string pupil = mDoc.GetInfo(id, "stream");
				mDoc.Delete(id);
				mDoc.Color.String = theColor;
				mDoc.Rect.String = theRect;

				const string xObjString = "<< /Resources << /ProcSet [ /PDF /Text ] >> " +
					"/Subtype /Form /Type /XObject /BBox [ 0 0 20 20 ] >>stream\n";
				mYesRadioBtnAppearance = mDoc.AddObject(xObjString + circle + pupil +"\n endstream");
				mOffRadioBtnAppearance = mDoc.AddObject(xObjString + circle + "\n endstream");
			}
		}


		private int mYesCheckBoxAppearance = -1;
		private int mOffCheckBoxAppearance = -1;

		private int mYesRadioBtnAppearance = -1;
		private int mOffRadioBtnAppearance = -1;

		private WebSupergoo.ABCpdf7.Objects.Signature mSig = null;

		private Doc mDoc;
	}

	/// <summary>
	/// Polyline annotation
	/// </summary>
	public class PolylineAnnotation : Annotation
	{
		/// <summary>
		/// Add polyline annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="polygon">Polyline</param>
		/// <param name="borderColor">Line color</param>
		public PolylineAnnotation(Doc doc, string polygon, string borderColor) : base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "PolyLine");
			mDoc.SetInfo(mId, "/Vertices", "["+ polygon + "]");
			mDoc.SetInfo(mId, "/Rect", "[" + GetBoundingRect(polygon) + "]");
			mDoc.SetInfo(mId, "/C", Annotation.Color2PdfArray(borderColor));
		}

		/// <summary>
		/// Get bounding rectangle of the polyline
		/// </summary>
		/// <param name="verticesString">Vertices of the polyline</param>
		/// <returns>Bounding rectangle</returns>
		protected  XRect GetBoundingRect (string verticesString)
		{
			string[] vertices = verticesString.Split(new char[] {' '});
			XRect rect = new XRect();
			rect.Left = double.MaxValue;
			rect.Right = double.MinValue;
			rect.Top = double.MinValue;
			rect.Bottom = double.MaxValue;
			
			for (int i = 0; i < vertices.Length; i++)
			{
				double v = double.Parse(vertices[i],NumberFormatInfo.InvariantInfo );
				if ( i%2 == 0 )
				{
					if ( v < rect.Left )
						rect.Left = v;
					else if (v > rect.Right )
						rect.Right = v;
				}
				else
				{
					if ( v < rect.Bottom )
						rect.Bottom = v;
					else if (v > rect.Top )
						rect.Top = v;
				}
			}
			return rect;
		}

		/// <summary>
		/// Line ending styles. Must be a string containing line ending styles
		///  for the beginning and end of the line, e.g. "OpenArrow OpenArrow".
		///  Possible values for lineending styles are: Square, Circle, Diamond,
		///  OpenArrow, ClosedArrow, None, Butt, ROpenArrow, RClosedArrow, Slash
		/// </summary>
		public string LineEndingsStyle
		{
			set 
			{
				string[] styles = value.Split(new char[] {' '});
				if ( styles.Length == 2 )
					mDoc.SetInfo(mId, "/LE", string.Format("[ /{0} /{1} ]", styles[0], styles[1]));
			}
		}
	}

	/// <summary>
	/// Polygon annotation
	/// </summary>
	public class PolygonAnnotation : PolylineAnnotation
	{
		/// <summary>
		/// Add polygon annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="polygon">Polygon vertices</param>
		/// <param name="borderColor">Border color</param>
		/// <param name="fillColor">Fill color</param>
		public PolygonAnnotation(Doc doc, string polygon, string borderColor, string fillColor) : base(doc, polygon, borderColor)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "Polygon");
			mDoc.SetInfo(mId, "/IC", Annotation.Color2PdfArray(fillColor));
		}

		/// <summary>
		/// Intensity of the cloudy border effect. Default value: 0
		/// </summary>
		public double CloudyEffect
		{
			set 
			{
				if ( value > 0 )
				{
					mDoc.SetInfo(mId, "/BE/S:Name", "C");
					mDoc.SetInfo(mId, "/BE/I:Num", DoubleToPdfNumber(value));
				}
			}
		}
}


	/// <summary>
	/// Stamp annotation
	/// </summary>
	public class StampAnnotation : Annotation
	{
		/// <summary>
		/// Add stamp annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect">Annotation rectangle</param>
		/// <param name="type">Name of the predefined stamp annotation</param>
		public StampAnnotation(Doc doc, string rect, string caption, string color): base(doc)
		{
			GenerateAppearance(caption, color);
			mDoc.SetInfo(mId, "/AP/N:Ref", mAppearance.ToString());
			mDoc.SetInfo(mId, "/Subtype:Name", "Stamp");
			mDoc.SetInfo(mId, "/Contents:Text", caption);
			XRect newRect = new XRect();
			newRect.String = rect;
			newRect.Width = newRect.Height * width2height;
			mDoc.SetInfo(mId, "/Rect:Rect", newRect.String);
		}

		/// <summary>
		/// Generates appearance stream for the annotation
		/// </summary>
		/// <param name="text">Text to be displayed on the stamp</param>
		/// <param name="color">Stamp color</param>
		public void GenerateAppearance(string text, string color)
		{
			if (mAppearance < 0)
			{
				string theRect = mDoc.Rect.String;
				double theWidth = mDoc.Width;
				string theColor = mDoc.Color.String;
				int theFont = mDoc.Font;
				int fontSize = mDoc.FontSize;
				double charSpacing = mDoc.TextStyle.CharSpacing;
				int alpha= mDoc.Color.Alpha;

				mDoc.Rect.String = "0 0 600 44";
				mDoc.Color.String = color;
				mDoc.Font = mDoc.AddFont("Helvetica-BoldOblique");
				mDoc.FontSize = 28;
				mDoc.Pos.String = "10 35";
				mDoc.TextStyle.CharSpacing = -1;
				int textId = mDoc.AddText(text);
				string caption = mDoc.GetInfo(textId, "stream");

				int l1 = caption.IndexOf("/Fabc", 0, caption.Length);
				int l2 = caption.IndexOf(" ", l1, caption.Length - l1);
				string fontOperator = caption.Substring(l1, l2-l1);

				XRect rect = new XRect();
				rect.String = mDoc.GetInfo(textId, "rect");
				mDoc.Delete(textId);
				mDoc.Rect.Width = 23 + rect.Width;

				int rectId = mDoc.FrameRect(10, 10, true);
				string frameRect = mDoc.GetInfo(rectId, "stream");
				mDoc.Delete(rectId);
			
				mDoc.Color.Alpha = 25;
				rectId = mDoc.FillRect(10, 10);
				mDoc.Color.Alpha = alpha;
				string fillRect = mDoc.GetInfo(rectId, "stream");
				l1 = fillRect.IndexOf("/Gabc", 0, fillRect.Length);
				l2 = fillRect.IndexOf(" ", l1, fillRect.Length - l1);
				string gsOperator = fillRect.Substring(l1, l2-l1);
				mDoc.Delete(rectId);

				
				width2height = mDoc.Rect.Width / mDoc.Rect.Height;
				string xObjString = "<< /Resources << /ProcSet [ /PDF /Text ] >> /Subtype /Form /Type /XObject " +
					"/BBox [" + mDoc.Rect.String + "] >>stream\n";
				mAppearance = mDoc.AddObject(xObjString + caption + frameRect + fillRect + "\n endstream");
				mDoc.SetInfo(mAppearance, "/Resources/Font"+fontOperator+":Ref" , mDoc.Font.ToString());	
				mDoc.SetInfo(mAppearance, "/Resources/ExtGState"+gsOperator, mDoc.GetInfo(mDoc.Page, "/Resources/ExtGState"+gsOperator));
				mDoc.SetInfo(mDoc.Page, "/Resources/ExtGState"+gsOperator + ":Del", "");
				
				mDoc.Color.String = theColor;
				mDoc.Rect.String = theRect;
				mDoc.Width = theWidth;
				mDoc.FontSize = fontSize;
				mDoc.Font = theFont;
				mDoc.TextStyle.CharSpacing = charSpacing;
				mDoc.Color.Alpha = alpha;
			}
		}
		int mAppearance = -1;
		double width2height = 0;
	}
	/// <summary>
	/// Line annotation
	/// </summary>
	public class LineAnnotation: Annotation
	{
		/// <summary>
		/// Add line annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="line">Line start and end points</param>
		/// <param name="color">Color of the line</param>
		public LineAnnotation(Doc doc, string line, string color): base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "Line");
			mDoc.SetInfo(mId, "/L", "["+ line + "]");
			mDoc.SetInfo(mId, "/Rect", "["+ line + "]");
			mDoc.SetInfo(mId, "/C", Annotation.Color2PdfArray(color));
		}

		/// <summary>
		/// Line ending styles. Must be a string containing line ending styles
		///  for the beginning and end of the line, e.g. "OpenArrow OpenArrow".
		///  Possible values for lineending styles are: Square, Circle, Diamond,
		///  OpenArrow, ClosedArrow, None, Butt, ROpenArrow, RClosedArrow, Slash
		/// </summary>
		public string LineEndingsStyle
		{
			set 
			{
				string[] styles = value.Split(new char[] {' '});
				if ( styles.Length == 2 )
					mDoc.SetInfo(mId, "/LE", string.Format("[ /{0} /{1} ]", styles[0], styles[1]));
			}
		}

		/// <summary>
		/// Line Caption
		/// </summary>
		public string Caption
		{
			set 
			{
				mDoc.SetInfo(mId, "/Cap:Bool", "true");
				mDoc.SetInfo(mId, "/Contents:Text", value);
			}
		}

		/// <summary>
		/// Line caption in rich text format
		/// </summary>
		public string RichTextCaption
		{
			set 
			{
				mDoc.SetInfo(mId, "/Cap:Bool", "true");
				mDoc.SetInfo(mId, "/RC:Text", value);
			}
		}

		/// <summary>
		/// Line fill color
		/// </summary>
		public string FillColor
		{
			set { mDoc.SetInfo(mId, "/IC", Annotation.Color2PdfArray(value)); }
		}
	}

	/// <summary>
	/// Square annotation
	/// </summary>
	public class SquareAnnotation: Annotation
	{
		/// <summary>
		/// Add square annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent Doc</param>
		/// <param name="rect">Annotation rectangle</param>
		/// <param name="borderColor">Border color</param>
		/// <param name="fillColor">Fill color</param>
		public SquareAnnotation(Doc doc, string rect, string borderColor, string fillColor): base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "Square");
			mDoc.SetInfo(mId, "/Rect", "["+ rect + "]");
			mDoc.SetInfo(mId, "/C", Annotation.Color2PdfArray(borderColor));
			mDoc.SetInfo(mId, "/IC", Annotation.Color2PdfArray(fillColor));
		}
	}

	/// <summary>
	/// Circle Annotation
	/// </summary>
	public class CircleAnnotation: Annotation
	{
		/// <summary>
		/// Add circle annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect">Annotation rectangle</param>
		/// <param name="borderColor">Border color</param>
		/// <param name="fillColor">Fill color</param>
		public CircleAnnotation(Doc doc, string rect, string borderColor, string fillColor): base (doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "Circle");
			mDoc.SetInfo(mId, "/Rect", "["+ rect + "]");
			mDoc.SetInfo(mId, "/C", Annotation.Color2PdfArray(borderColor));
			mDoc.SetInfo(mId, "/IC", Annotation.Color2PdfArray(fillColor));
		}
	}
	/// <summary>
	/// Movie annotation
	/// </summary>
	public class MovieAnnotation : Annotation
	{
		/// <summary>
		/// Add movie annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect">Annotation rectangle</param>
		/// <param name="fileName">Movie file name</param>
		public MovieAnnotation(Doc doc, string rect, string fileName) : base(doc)
		{
			string contentType = "";
			if (fileName.EndsWith(".swf"))
				contentType = "application/x-shockwave-flash";
			else if (fileName.EndsWith(".wmv"))
				contentType = "video/x-ms-wmv";
			else if (fileName.EndsWith(".mpg"))
				contentType = "video/mpeg";
			else if (fileName.EndsWith(".avi"))
				contentType = "video/avi";

			if ( contentType != "" )
			{
				mDoc.SetInfo(mId, "/Subtype:Name", "Screen");
				mDoc.SetInfo(mId, "/Rect", "["+ rect + "]");

				FileInfo fileInfo = new FileInfo(fileName);
				int fileSpec = mDoc.AddObject("<</F ("+ fileInfo.Name +") /Type /Filespec >>");

				int mediaClip = mDoc.AddObject("<< /S /MCD /P << /TF (TEMPACCESS) >> >>");
				mDoc.SetInfo( mediaClip, "/CT:Text", contentType);

				int rendition = mDoc.AddObject("<< /S /MR >>");
				int pageVisibleAction = mDoc.AddObject(" << /OP 4 /S /Rendition >> ");

				mDoc.SetInfo(mId, "/AA*/PV:Ref", pageVisibleAction.ToString());
				mDoc.SetInfo(mId, "/AA*/PV*/AN:Ref", mId.ToString());
				mDoc.SetInfo(mId, "/AA*/PV*/R:Ref", rendition.ToString());
				mDoc.SetInfo(mId, "/AA*/PV*/R*/C:Ref", mediaClip.ToString());
				mDoc.SetInfo(mId, "/AA*/PV*/R*/C*/D:Ref", fileSpec.ToString());

				int streamId = EmbedFile(fileName);
				mDoc.SetInfo(mId, "/AA*/PV*/R*/C*/D*/EF/F:Ref", streamId.ToString());
			}
		}
	}

	/// <summary>
	/// File attachment annotation
	/// </summary>
	public class FileAttachmentAnnotation : Annotation
	{
		/// <summary>
		/// Add file attachment annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect">Annotation rectangle</param>
		/// <param name="fileName">File name</param>
		public FileAttachmentAnnotation(Doc doc, string rect, string fileName): base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "FileAttachment");
			mDoc.SetInfo(mId, "/Rect:Rect", rect);
			FileInfo fileInfo = new FileInfo(fileName);
			int fileSpec = mDoc.AddObject("<< /F ("+ fileInfo.Name +") /Type /Filespec /EF <<  >> >>");
			mDoc.SetInfo(mId, "/FS:Ref", fileSpec.ToString());

			int streamId = EmbedFile(fileName);
			mDoc.SetInfo(mId, "/FS*/EF/F:Ref", streamId.ToString());
		}
	}
	
	/// <summary>
	/// Popup annotation
	/// </summary>
	public class PopupAnnotation : Annotation
	{
		/// <summary>
		/// Add popup annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect">Annotation rectangle</param>
		/// <param name="parentId">Id of the parent annotation</param>
		public PopupAnnotation(Doc doc, string rect, int parentId) : base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "Popup");
			mDoc.SetInfo(mId, "/Parent:Ref", parentId.ToString());
			mDoc.SetInfo(mId, "/Rect:Rect", rect);
			mDoc.SetInfo(mId, "/Open:Bool", "true");
		}
	}

	/// <summary>
	/// Text annotation ("Sticky note")
	/// </summary>
	public class TextAnnotation : Annotation
	{
		/// <summary>
		/// Add text annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect1">Annotation rectangle</param>
		/// <param name="rect2">Rectangle of the "sticky note" popup</param>
		/// <param name="contents">Contents of the text annotation</param>
		public TextAnnotation(Doc doc, string rect1, string rect2, string contents) : base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "Text");
			mDoc.SetInfo(mId, "/Name:Name", "Comment");

			mDoc.SetInfo(mId, "/Contents:Text", contents);
			mDoc.SetInfo(mId, "/Rect:Rect", rect1);
			if (rect2 != "")
			{
				PopupAnnotation popup = new PopupAnnotation(doc, rect2, mId);
				mDoc.SetInfo(mId, "/Popup:Ref", popup.Id.ToString());
			}
		}
	}

	/// <summary>
	/// Text markup annotation
	/// </summary>
	public class TextMarkupAnnotation : Annotation
	{
		/// <summary>
		/// Add text markup annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="quadPoints">Quad points of the text.</param>
		/// <param name="markupType">Type of the markup annotation. Possible values are Highlight, Underline, Squiggly and StrikeOut</param>
		/// <param name="color">Markup color</param>
		public TextMarkupAnnotation(Doc doc, string quadPoints, string markupType, string color): base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", markupType);
			mDoc.SetInfo(mId, "QuadPoints", "[" + quadPoints + "]");
			Color = color;
		}

		/// <summary>
		/// Add text markup annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="id">Id of the Text object</param>
		/// <param name="markupType">Type of the markup annotation. Possible values are Highlight, Underline, Squiggly and StrikeOut</param>
		/// <param name="color">Markup color</param>
		public TextMarkupAnnotation(Doc doc, int id, string markupType, string color): base(doc)
		{
			XRect rect = new XRect();
			rect.String = doc.GetInfo(id, "Rect");
			mDoc.SetInfo(mId, "/Subtype:Name", markupType);
			string quadPoints = "[ " + String.Format(NumberFormatInfo.InvariantInfo , "{0:0.#####} {1:0.#####} {2:0.#####} {3:0.#####} {4:0.#####} {5:0.#####} {6:0.#####} {7:0.#####}", 
				rect.Left, rect.Top, rect.Right, rect.Top, rect.Left, rect.Bottom, rect.Right, rect.Bottom) + " ]";
			mDoc.SetInfo(mId, "/QuadPoints", quadPoints );
			mDoc.SetInfo(mId, "/Rect:Rect", rect.String);
			Color = color;
		}
	}
}
