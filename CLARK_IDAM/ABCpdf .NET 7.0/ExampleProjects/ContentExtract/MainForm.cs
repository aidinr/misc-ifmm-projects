// ===========================================================================
//	�2007 WebSupergoo. All rights reserved.
// ===========================================================================

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using System.Drawing.Imaging;
using WebSupergoo.ABCpdf7;
using WebSupergoo.ABCpdf7.Objects;

namespace WebSupergoo.ContentExtract
{
	/// <summary>
	/// Summary description for MainForm.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private ObjectExtractor _cachedExtractor;
		private Bitmap _cachedBitmap;
		private System.Windows.Forms.MainMenu mbar;
		private System.Windows.Forms.MenuItem mitemFile;
		private System.Windows.Forms.MenuItem mitemOpen;
		private System.Windows.Forms.MenuItem mitemExit;
		private System.Windows.Forms.MenuItem mitemSep1;
		private System.Windows.Forms.ListView lst;
		private System.Windows.Forms.Splitter split;
		private System.Windows.Forms.PictureBox pbox;
		private System.Windows.Forms.OpenFileDialog dlgOpenFile;
		private System.Windows.Forms.Panel pnlPicture;
		private System.Windows.Forms.ColumnHeader cheaderType;
		private System.Windows.Forms.ColumnHeader cheaderName;
		private System.Windows.Forms.ColumnHeader cheaderInfo1;
		private System.Windows.Forms.ColumnHeader cheaderInfo2;
		private System.Windows.Forms.ColumnHeader cheaderID;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			lst.ListViewItemSorter = new ListViewItemComparer();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mbar = new System.Windows.Forms.MainMenu();
			this.mitemFile = new System.Windows.Forms.MenuItem();
			this.mitemOpen = new System.Windows.Forms.MenuItem();
			this.mitemSep1 = new System.Windows.Forms.MenuItem();
			this.mitemExit = new System.Windows.Forms.MenuItem();
			this.lst = new System.Windows.Forms.ListView();
			this.cheaderID = new System.Windows.Forms.ColumnHeader();
			this.cheaderType = new System.Windows.Forms.ColumnHeader();
			this.cheaderName = new System.Windows.Forms.ColumnHeader();
			this.cheaderInfo1 = new System.Windows.Forms.ColumnHeader();
			this.cheaderInfo2 = new System.Windows.Forms.ColumnHeader();
			this.split = new System.Windows.Forms.Splitter();
			this.pbox = new System.Windows.Forms.PictureBox();
			this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
			this.pnlPicture = new System.Windows.Forms.Panel();
			this.pnlPicture.SuspendLayout();
			this.SuspendLayout();
			// 
			// mbar
			// 
			this.mbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																				 this.mitemFile});
			// 
			// mitemFile
			// 
			this.mitemFile.Index = 0;
			this.mitemFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mitemOpen,
																					  this.mitemSep1,
																					  this.mitemExit});
			this.mitemFile.Text = "&File";
			// 
			// mitemOpen
			// 
			this.mitemOpen.Index = 0;
			this.mitemOpen.Text = "&Open...";
			this.mitemOpen.Click += new System.EventHandler(this.mitemOpen_Click);
			// 
			// mitemSep1
			// 
			this.mitemSep1.Index = 1;
			this.mitemSep1.Text = "-";
			// 
			// mitemExit
			// 
			this.mitemExit.Index = 2;
			this.mitemExit.Text = "E&xit";
			this.mitemExit.Click += new System.EventHandler(this.mitemExit_Click);
			// 
			// lst
			// 
			this.lst.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																				  this.cheaderID,
																				  this.cheaderType,
																				  this.cheaderName,
																				  this.cheaderInfo1,
																				  this.cheaderInfo2});
			this.lst.Dock = System.Windows.Forms.DockStyle.Left;
			this.lst.FullRowSelect = true;
			this.lst.HideSelection = false;
			this.lst.Location = new System.Drawing.Point(0, 0);
			this.lst.Name = "lst";
			this.lst.Size = new System.Drawing.Size(608, 566);
			this.lst.TabIndex = 0;
			this.lst.View = System.Windows.Forms.View.Details;
			this.lst.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lst_ColumnClick);
			this.lst.SelectedIndexChanged += new System.EventHandler(this.lst_SelectedIndexChanged);
			// 
			// cheaderID
			// 
			this.cheaderID.Text = "ID";
			// 
			// cheaderType
			// 
			this.cheaderType.Text = "Type";
			this.cheaderType.Width = 86;
			// 
			// cheaderName
			// 
			this.cheaderName.Text = "Name";
			// 
			// cheaderInfo1
			// 
			this.cheaderInfo1.Text = "Info 1";
			this.cheaderInfo1.Width = 77;
			// 
			// cheaderInfo2
			// 
			this.cheaderInfo2.Text = "Info 2";
			this.cheaderInfo2.Width = 300;
			// 
			// split
			// 
			this.split.Location = new System.Drawing.Point(608, 0);
			this.split.Name = "split";
			this.split.Size = new System.Drawing.Size(3, 566);
			this.split.TabIndex = 1;
			this.split.TabStop = false;
			// 
			// pbox
			// 
			this.pbox.Location = new System.Drawing.Point(0, 0);
			this.pbox.Name = "pbox";
			this.pbox.Size = new System.Drawing.Size(120, 272);
			this.pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pbox.TabIndex = 2;
			this.pbox.TabStop = false;
			// 
			// dlgOpenFile
			// 
			this.dlgOpenFile.DefaultExt = "pdf";
			this.dlgOpenFile.Filter = "PDF Document (*.pdf)|*.pdf";
			this.dlgOpenFile.Title = "Open a PDF Document";
			// 
			// pnlPicture
			// 
			this.pnlPicture.AutoScroll = true;
			this.pnlPicture.Controls.Add(this.pbox);
			this.pnlPicture.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlPicture.Location = new System.Drawing.Point(611, 0);
			this.pnlPicture.Name = "pnlPicture";
			this.pnlPicture.Size = new System.Drawing.Size(93, 566);
			this.pnlPicture.TabIndex = 3;
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(704, 566);
			this.Controls.Add(this.pnlPicture);
			this.Controls.Add(this.split);
			this.Controls.Add(this.lst);
			this.Menu = this.mbar;
			this.Name = "MainForm";
			this.Text = "Content Extract";
			this.pnlPicture.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainForm());
		}

		private void Clear(){
			if(pbox.Image!=null){
				pbox.Image.Dispose();
				pbox.Image = null;
			}
			lst.Items.Clear();
		}

		private void Open(string filePath){
			Doc doc = new Doc();
			doc.Read(filePath);
			Text = "Content Extract - "+filePath;
			Clear();

			ObjectSoup soup = doc.ObjectSoup;
			int count = soup.Count;
			ArrayList itemList = new ArrayList(count);
			for(int i = 0; i<count; ++i){
				IndirectObject obj = soup[i];
				if(obj==null)
					continue;

				try{
					ObjectExtractor extractor = ObjectExtractor.FromIndirectObject(obj);
					ListViewItem item = new ListViewItem(extractor.GetInfo());
					item.Tag = extractor;
					itemList.Add(item);
				}catch(Exception exc){
					ObjectExtractor extractor = new ObjectExtractor(obj);
					ListViewItem item = new ListViewItem(
						extractor.GetErrorInfo(exc.Message));
					item.Tag = extractor;
					itemList.Add(item);
				}
			}
			lst.BeginUpdate();
			try{
				lst.Items.AddRange((ListViewItem[])
					itemList.ToArray(typeof(ListViewItem)));
			}finally{
				lst.EndUpdate();
			}
		}

		private ListViewItem GetSelectedItem(){
			ListViewItem item = lst.FocusedItem;
			if(item!=null && item.Selected)
				return item;
			if(lst.SelectedItems.Count>0)
				return lst.SelectedItems[0];

			return null;
		}


		private void mitemExit_Click(object sender, System.EventArgs e) {
			Close();
		}

		private void mitemOpen_Click(object sender, System.EventArgs e) {
			if(dlgOpenFile.ShowDialog()!=DialogResult.OK)
				return;

			try{
				Open(dlgOpenFile.FileName);
			}catch(Exception exc){
				MessageBox.Show(exc.Message);
			}
		}

		private void lst_SelectedIndexChanged(object sender, System.EventArgs e) {
			ListViewItem item = GetSelectedItem();
			if(item==null)
				pbox.Image = null;
			else{
				ObjectExtractor extractor = item.Tag as ObjectExtractor;
				if(extractor==null)
					pbox.Image = null;
				else if(extractor==_cachedExtractor)
					pbox.Image = _cachedBitmap;
				else{
					try{
						Bitmap bitmap = extractor.GetBitmap();
						pbox.Image = bitmap;
						if(bitmap!=null){
							if(_cachedBitmap!=null){
								_cachedBitmap.Dispose();
								_cachedBitmap = null;
							}
							_cachedExtractor = extractor;
							_cachedBitmap = bitmap;
						}
					}catch(Exception exc){
						MessageBox.Show(exc.Message);
					}
				}
			}
		}

		private void lst_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e) {
			ListViewItemComparer comparer = lst.ListViewItemSorter as ListViewItemComparer;
			if(comparer!=null){
				if(e.Column==comparer.Column){
					if(comparer.Order==SortOrder.Ascending)
						comparer.Order = SortOrder.Descending;
					else
						comparer.Order = SortOrder.Ascending;
				}else{
					comparer.Column = e.Column;
					comparer.Order = SortOrder.Ascending;
				}
			}
			lst.Sort();
		}
	}
}
