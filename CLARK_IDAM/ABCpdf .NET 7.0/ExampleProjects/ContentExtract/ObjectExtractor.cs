// ===========================================================================
//	�2007 WebSupergoo. All rights reserved.
// ===========================================================================

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Text;
using WebSupergoo.ABCpdf7;
using WebSupergoo.ABCpdf7.Objects;

namespace WebSupergoo.ContentExtract
{
	/// <summary>
	/// Summary description for ObjectExtractor.
	/// </summary>
	public class ObjectExtractor
	{
		public static ObjectExtractor FromIndirectObject(IndirectObject obj){
			if(obj==null)
				throw new ArgumentNullException("obj", "IndirectObject obj cannot be null.");

			if(obj is StreamObject)
				return StreamObjectExtractor.FromStreamObject((StreamObject)obj);
			if(obj is Annotation)
				return new AnnotationExtractor((Annotation)obj);
			if(obj is Page)
				return new PageExtractor((Page)obj);
			if(obj is Pages)
				return new PagesExtractor((Pages)obj);
			if(obj is FontObject)
				return new FontObjectExtractor((FontObject)obj);
			if(obj is Field)
				return FieldExtractor.FromField((Field)obj);
			if(obj is GraphicsState)
				return new GraphicsStateExtractor((GraphicsState)obj);
			if(obj is Bookmark)
				return BookmarkExtractor.FromBookmark((Bookmark)obj);
			if(obj is ColorSpace)
				return new ColorSpaceExtractor((ColorSpace)obj);
			if(obj is Catalog)
				return new CatalogExtractor((Catalog)obj);

			return new ObjectExtractor(obj);
		}

		private IndirectObject _obj;

		public ObjectExtractor(IndirectObject obj){
			_obj = obj;
		}

		public IndirectObject Object{ get{ return _obj; } }

		public virtual string[] GetInfo(){
			return new string[]{ GetIDString(), GetTypeName() };
		}

		public string[] GetErrorInfo(string message){
			return new string[]{ GetIDString(), GetTypeName(), "(Error!)", "", message };
		}

		protected string GetIDString(){ return _obj.ID.ToString(); }
		protected string GetTypeName(){ return _obj.GetType().Name; }

		public virtual Bitmap GetBitmap(){ return null; }

		protected static string FormatList(string[] list){
			if(list==null || list.Length<=0)
				return "";

			StringBuilder builder = new StringBuilder();
			builder.Append(list[0]);
			for(int i = 1; i<list.Length; ++i)
				builder.Append(", ").Append(list[i]);

			return builder.ToString();
		}
	}

	public class CatalogExtractor: ObjectExtractor
	{
		public CatalogExtractor(Catalog obj): base(obj){}

		public new Catalog Object{ get{ return (Catalog)base.Object; } }

		public override string[] GetInfo() {
			Catalog obj = Object;
			Outline outline = obj.Outline;
			Pages pages = obj.Pages;
			return new string[]{ GetIDString(), GetTypeName(), "", "",
				string.Format("Outline ID:[{0}] Pages ID:[{1}]",
					outline==null? "": outline.ID.ToString(),
					pages==null? "": pages.ID.ToString()) };
		}
	}

	public class ColorSpaceExtractor: ObjectExtractor
	{
		public ColorSpaceExtractor(ColorSpace obj): base(obj){}

		public new ColorSpace Object{ get{ return (ColorSpace)base.Object; } }

		public override string[] GetInfo(){
			ColorSpace obj = Object;
			IccProfile iccProfile = obj.IccProfile;
			return new string[]{ GetIDString(), GetTypeName(), obj.Name,
				obj.ColorSpaceType.ToString(),
				string.Format("Components:[{0}] IccProfile ID:[{1}]",
					obj.Components, iccProfile==null? "": iccProfile.ID.ToString()) };
		}
	}

	public class BookmarkExtractor: ObjectExtractor
	{
		public static BookmarkExtractor FromBookmark(Bookmark obj){
			if(obj is Outline)
				return new OutlineExtractor((Outline)obj);

			return new BookmarkExtractor(obj);
		}

		public BookmarkExtractor(Bookmark obj): base(obj){}

		public new Bookmark Object{ get{ return (Bookmark)base.Object; } }

		public override string[] GetInfo(){
			Bookmark obj = Object;
			Bookmark parent = obj.Parent;
			return new string[]{ GetIDString(), GetTypeName(), "", obj.Title,
				string.Format("PageID:[{0}] Parent ID:[{1}] Count:[{2}] Open:[{3}]",
				obj.PageID, parent==null? "": parent.ID.ToString(),
				obj.Count, obj.Open) };
		}
	}

	public class OutlineExtractor: BookmarkExtractor
	{
		public OutlineExtractor(Outline obj): base(obj){}

		public new Outline Object{ get{ return (Outline)base.Object; } }
	}

	public class GraphicsStateExtractor: ObjectExtractor
	{
		public GraphicsStateExtractor(GraphicsState obj): base(obj){}

		public new GraphicsState Object{ get{ return (GraphicsState)base.Object; } }
	}

	public class FieldExtractor: ObjectExtractor
	{
		public static FieldExtractor FromField(Field obj){
			if(obj is Signature)
				return new SignatureExtractor((Signature)obj);

			return new FieldExtractor(obj);
		}

		public FieldExtractor(Field obj): base(obj){}

		public new Field Object{ get{ return (Field)base.Object; } }

		public override string[] GetInfo(){
			Field obj = Object;
			return new string[]{ GetIDString(), GetTypeName(),
				obj.Name, obj.Value,
				string.Format("FieldType:[{0}] Format:[{1}] MultiSelect:[{2}] Options[{3}]",
					obj.FieldType, obj.Format, obj.MultiSelect,
					FormatList(obj.Options)) };
		}
	}

	public class SignatureExtractor: FieldExtractor
	{
		public SignatureExtractor(Signature obj): base(obj){}

		public new Signature Object{ get{ return (Signature)base.Object; } }

		public override string[] GetInfo() {
			Signature obj = Object;
			return new string[]{ GetIDString(), GetTypeName(), "",
				obj.Signer, string.Format(
				"IsModified:[{0}] SigningUtcTime:[{1}] Location:[{2}] Reason[{3}]",
				obj.IsModified, obj.SigningUtcTime, obj.Location, obj.Reason) };
		}

	}

	public class FontObjectExtractor: ObjectExtractor
	{
		public FontObjectExtractor(FontObject obj): base(obj){}

		public new FontObject Object{ get{ return (FontObject)base.Object; } }
	}

	public class AnnotationExtractor: ObjectExtractor
	{
		public AnnotationExtractor(Annotation obj): base(obj){}

		public new Annotation Object{ get{ return (Annotation)base.Object; } }

		public override string[] GetInfo(){
			Annotation obj = Object;
			return new string[]{ GetIDString(), GetTypeName(),
				obj.FullName, obj.SubType,
				string.Format("FieldType:[{0}] FieldValue:[{1}] Contents:[{2}]",
					obj.FieldType, obj.FieldValue, obj.Contents) };
		}
	}

	public class PageExtractor: ObjectExtractor
	{
		public PageExtractor(Page obj): base(obj){}

		public new Page Object{ get{ return (Page)base.Object; } }

		public override string[] GetInfo(){
			Page obj = Object;
			Pages parent = obj.Parent;
			return new string[]{ GetIDString(), GetTypeName(), "", "",
				string.Format("Rotation:[{0}] Parent ID:[{1}]",
					obj.Rotation, parent==null? "": parent.ID.ToString()) };
		}
	}

	public class PagesExtractor: ObjectExtractor
	{
		public PagesExtractor(Pages obj): base(obj){}

		public new Pages Object{ get{ return (Pages)base.Object; } }

		public override string[] GetInfo() {
			Pages obj = Object;
			Pages parent = obj.Parent;
			return new string[]{ GetIDString(), GetTypeName(), "", "",
				string.Format("Count:[{0}] Parent ID:[{1}]",
					obj.Count, parent==null? "": parent.ID.ToString()) };
		}
	}

	public class StreamObjectExtractor: ObjectExtractor
	{
		public static StreamObjectExtractor FromStreamObject(StreamObject obj){
			if(obj is PixMap)
				return new PixMapExtractor((PixMap)obj);
			if(obj is Layer)
				return LayerExtractor.FromLayer((Layer)obj);
			if(obj is IccProfile)
				return new IccProfileExtractor((IccProfile)obj);

			return new StreamObjectExtractor(obj);
		}

		public StreamObjectExtractor(StreamObject obj): base(obj){}

		public new StreamObject Object{ get{ return (StreamObject)base.Object; } }

		public override string[] GetInfo(){
			StreamObject obj = Object;
			return new string[]{ GetIDString(), GetTypeName(), "",
				obj.Compression.ToString(),
				string.Format("Length:[{0}]", obj.Length) };
		}
	}

	public class IccProfileExtractor: StreamObjectExtractor
	{
		public IccProfileExtractor(IccProfile obj): base(obj){}

		public new IccProfile Object{ get{ return (IccProfile)base.Object; } }
	}

	public class PixMapExtractor: StreamObjectExtractor
	{
		private static readonly string[] _colorSpaceFormat = {
			"{0}\u2014{1} comp \xd7 {2} bit",
			"{0}\u2014{1} comp \xd7 {2} bits",
			"{0}\u2014{1} comps \xd7 {2} bit",
			"{0}\u2014{1} comps \xd7 {2} bits"
		};

		public PixMapExtractor(PixMap obj): base(obj){}

		public new PixMap Object{ get{ return (PixMap)base.Object; } }

		public override string[] GetInfo(){
			PixMap obj = Object;
			int components = obj.Components;
			int bitsPerComponent = obj.BitsPerComponent;
			return new string[]{ GetIDString(), GetTypeName(),
				obj.Doc.GetInfo(obj.ID, "/Name:Name"),
				string.Format("{0}\xd7{1} {2}", obj.Width, obj.Height,
					obj.Compression),
				string.Format(_colorSpaceFormat[
					(components<=1? 0: 2)+(bitsPerComponent<=1? 0: 1)],
					obj.ColorSpaceType, components, bitsPerComponent) };
		}

		public override Bitmap GetBitmap() {
			PixMap obj = Object;
			ColorSpaceType colorSpaceType = obj.ColorSpaceType;
			int components = obj.Components;
			int bitsPerComponent = obj.BitsPerComponent;

			if(colorSpaceType==ColorSpaceType.Indexed){
				obj.Realize();
				colorSpaceType = obj.ColorSpaceType;
				components = obj.Components;
				bitsPerComponent = obj.BitsPerComponent;
			}
			switch(colorSpaceType){
				case ColorSpaceType.DeviceRGB:
				case ColorSpaceType.CalRGB:
					if(components==3 && bitsPerComponent==8)
						return new PixMapStreamData(obj).GetBitmapFromDeviceRGB8();
					break;
				case ColorSpaceType.DeviceGray:
				case ColorSpaceType.CalGray:
					if(components==1){
						if(bitsPerComponent==8)
							return new PixMapStreamData(obj).GetBitmapFromDeviceGray8();
						if(bitsPerComponent==1)
							return new PixMapStreamData(obj).GetBitmapFromDeviceGray1();
					}
					break;
				case ColorSpaceType.DeviceCMYK:
					if(components==4 && bitsPerComponent==8)
						return new PixMapStreamData(obj).GetBitmapFromDeviceCMYK8();
					break;
				case ColorSpaceType.Indexed: break;
				default:
					if(components==3 && bitsPerComponent==8)
						return new PixMapStreamData(obj).GetBitmapFromDeviceRGB8();
					if(components==1){
						if(bitsPerComponent==8)
							return new PixMapStreamData(obj).GetBitmapFromDeviceGray8();
						if(bitsPerComponent==1)
							return new PixMapStreamData(obj).GetBitmapFromDeviceGray1();
					}
					if(components==4 && bitsPerComponent==8)
						return new PixMapStreamData(obj).GetBitmapFromDeviceCMYK8();
					break;
			}

			return base.GetBitmap();
		}

		private class PixMapStreamData
		{
			private delegate int PixMapDataProcessor(ref byte[] data, ref int stride);

			private PixMap _obj;

			public PixMapStreamData(PixMap obj){
				_obj = obj;
			}

			public Bitmap GetBitmapFromDeviceRGB8(){
				return GetBitmapFromPixmap(PixelFormat.Format24bppRgb,
					_obj.Width*3, new PixMapDataProcessor(ProcessDeviceRGB8Data));
			}
			private int ProcessDeviceRGB8Data(ref byte[] data, ref int stride){
				int len = data.Length;
				for(int dataIndex = 0; dataIndex+3<=len; dataIndex += stride){
					int iEnd = Math.Min(len, dataIndex+stride);
					for(int i = dataIndex; i+3<=iEnd; i += 3){
						byte t = data[i];
						data[i] = data[i+2];
						data[i+2] = t;
					}
				}
				return len;
			}

			public Bitmap GetBitmapFromDeviceGray8(){
				Bitmap bitmap = GetBitmapFromPixmap(PixelFormat.Format8bppIndexed,
					_obj.Width, null);
				ColorPalette palette = bitmap.Palette;
				for(int i = 0; i<256; ++i)
					palette.Entries[i] = Color.FromArgb(i, i, i);
				bitmap.Palette = palette;
				return bitmap;
			}

			public Bitmap GetBitmapFromDeviceGray1(){
				return GetBitmapFromPixmap(PixelFormat.Format1bppIndexed,
					(_obj.Width+7)/8, null);
			}

			public Bitmap GetBitmapFromDeviceCMYK8(){
				return GetBitmapFromPixmap(PixelFormat.Format24bppRgb,
					_obj.Width*4, new PixMapDataProcessor(ProcessDeviceCMYK8Data));
			}
			private int ProcessDeviceCMYK8Data(ref byte[] data, ref int stride){
				const byte maxV = byte.MaxValue;
				int len = data.Length;
				int newStride = _obj.Width*3;
				int newI = 0;
				for(int dataIndex = 0, newDataIndex = 0; dataIndex+4<=len;
					dataIndex += stride, newDataIndex += newStride)
				{
					int iEnd = Math.Min(len, dataIndex+stride);
					newI = newDataIndex;
					for(int i = dataIndex; i+4<=iEnd; i += 4, newI += 3){
						uint blackScale = (byte)(maxV-data[i+3])*65536u/maxV;
						byte cyan = data[i];
						byte magenta = data[i+1];
						byte yellow = data[i+2];
						data[newI] = (byte)((byte)(maxV-yellow)*blackScale/65536u);
						data[newI+1] = (byte)((byte)(maxV-magenta)*blackScale/65536u);
						data[newI+2] = (byte)((byte)(maxV-cyan)*blackScale/65536u);
					}
				}
				stride = newStride;
				return newI;
			}

			private Bitmap GetBitmapFromPixmap(PixelFormat pixelFormat,
				int pixmapStride, PixMapDataProcessor processData)
			{
				if(!_obj.Decompress())
					return null;

				int width = _obj.Width;
				Bitmap bitmap = new Bitmap(width, _obj.Height, pixelFormat);
				try{
					CopyToBitmap(bitmap, pixmapStride, processData);
				}catch{
					bitmap.Dispose();
					throw;
				}
				return bitmap;
			}

			private void CopyToBitmap(Bitmap outBitmap, int pixmapStride,
				PixMapDataProcessor processData)
			{
				int width = _obj.Width;
				int height = _obj.Height;

				byte[] data = _obj.GetData();
				int len = data.Length;
				if(processData!=null)
					len = processData(ref data, ref pixmapStride);

				BitmapData bitmapData = outBitmap.LockBits(
					new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly,
					outBitmap.PixelFormat);
				try{
					int dataIndex = 0;
					long scan = bitmapData.Scan0.ToInt64();
					int bitmapStride = bitmapData.Stride;
					for(int y = 0; y<height && dataIndex+pixmapStride<=len;
						++y, dataIndex += pixmapStride, scan += bitmapStride) {
						System.Runtime.InteropServices.Marshal.Copy(
							data, dataIndex, new IntPtr(scan), pixmapStride);
					}
				}finally{
					outBitmap.UnlockBits(bitmapData);
				}
			}
		}
	}

	public class LayerExtractor: StreamObjectExtractor
	{
		public static LayerExtractor FromLayer(Layer obj){
			return new LayerExtractor(obj);
		}

		public LayerExtractor(Layer obj): base(obj){}

		public new Layer Object{ get{ return (Layer)base.Object; } }

		public override string[] GetInfo() {
			Layer obj = Object;
			return new string[]{ GetIDString(), GetTypeName(), "",
				obj.Compression.ToString(),
				string.Format("Length:[{0}] Rect:[{1}]",
					obj.Length, obj.Rect.String) };
		}
	}
}
