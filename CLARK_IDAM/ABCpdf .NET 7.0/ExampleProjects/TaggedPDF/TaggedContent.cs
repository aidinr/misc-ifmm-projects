// ===========================================================================
//	�2006 WebSupergoo. All rights reserved.
// ===========================================================================

using System;
using System.Collections;
using WebSupergoo.ABCpdf7;
using System.IO;

namespace TaggedPDF {
	/// <summary>
	/// This class is used for construction of tagged pdf content and insertion into the document
	/// </summary>
	public class TaggedContent {
		#region Members
		/// <summary>
		/// Unique index for marked content sequence.
		/// </summary>
		private int mIndexOfMarkedContentSequence = 0;
		/// <summary>
		/// Stack of tagged items
		/// </summary>
		private Stack mTaggedItems = new Stack();
		/// <summary>
		/// Last used number for graphic state command (e.g. GS7 for mGSNumber = 7)
		/// </summary>
		/// <summary>
		/// Parent doc
		/// </summary>
		private Doc mDoc;
		/// <summary>
		/// Pdf content string
		/// </summary>
		private string mContents = "";
		/// <summary>
		/// Temporary storage for parts of the content representing different pages
		/// </summary>
		private ArrayList mPageStore = new ArrayList();

		#endregion

		#region Properties
		/// <summary>
		/// References to all used tagged items
		/// </summary>
		private TaggedItemRefs mTaggedItemRefs;
		/// <summary>
		/// References to all used tagged items
		/// </summary>
		public TaggedItemRefs TaggedItemRefs {
			get {
				if(mTaggedItemRefs == null) {
					mTaggedItemRefs = new TaggedItemRefs(mDoc);
				}

				return mTaggedItemRefs;
			}
		}
		/// <summary>
		/// The logical structure of the tagged document is described by
		/// a hierarchy of objects called the structure hierarchy or
		/// structure tree.
		/// The structure tree root is at the root of this hierarchy.
		/// </summary>
		private StructureTreeRoot mStructureTreeRoot;
		/// <summary>
		/// The logical structure of the tagged document is described by
		/// a hierarchy of objects called the structure hierarchy or
		/// structure tree.
		/// The structure tree root is at the root of this hierarchy.
		/// </summary>
		public StructureTreeRoot StructureTreeRoot {
			get {
				CreateStructureTreeRootOnDemand();
				return mStructureTreeRoot;
			}
		}
		/// <summary>
		/// Role map object which provides mapping to standard structure types
		/// </summary>
		private RoleMap mRoleMap;
		/// <summary>
		/// Role map object which provides mapping to standard structure types
		/// </summary>
		public RoleMap RoleMap {
			get {
				if(mRoleMap == null)
					mRoleMap = new RoleMap(mDoc, StructureTreeRoot);
				return mRoleMap;
			}
		}
		/// <summary>
		/// Class map object which provides the association between class names and
		/// attribute objects
		/// </summary>
		public ClassMap mClassMap;
		/// <summary>
		/// Class map object which provides the association between class names and
		/// attribute objects
		/// </summary>
		public ClassMap ClassMap {
			get {
				if(mClassMap == null)
					mClassMap = new ClassMap(mDoc, StructureTreeRoot);
				return mClassMap;
			}
		}
		/// <summary>
		/// Tagged pdf mode flag
		/// </summary>
		public bool IsTaggedPdf {
			get {
				return LastTaggedItem != null;
			}
		}
		/// <summary>
		/// Last\current tagged item
		/// </summary>
		public StructureElement LastTaggedItem {
			get {
				return (mTaggedItems.Count == 0) ?
					null : mTaggedItems.Peek() as StructureElement;
			}
			set {
				mTaggedItems.Push(value);
			}
		}
		#endregion

		#region Initialize/Finalize methods
		/// <summary>
		/// Initialize object with document argument
		/// </summary>
		/// <param name="theDoc">document argument</param>
		public TaggedContent(Doc theDoc) {
			mDoc = theDoc;
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Begin tagged item object
		/// </summary>
		/// <param name="name">tagged item name</param>
		/// <returns>unique id for created tagged item object</returns>
		public int BeginTaggedItem(string name) {
			return BeginTaggedItem(new StructureElement(mDoc, name));
		}
		/// <summary>
		/// Begin tagged item object
		/// </summary>
		/// <param name="el">tagged item</param>
		/// <returns>unique id for created tagged item object</returns>
		public int BeginTaggedItem(StructureElement el) {
			// update structure elements index of marked content sequence
			el.MarkedContentSequenceIndex = mIndexOfMarkedContentSequence;
			++mIndexOfMarkedContentSequence;

			// we need page id to assign StructParents key
			AddPageOnDemand();

			// add structure element reference
			TaggedItemRefs.AddChild(el.Id);

			// is this a tagged item root or it is a  child of previously added tagged item
			bool isRootTaggedItem = !IsTaggedPdf;

			// select parent tagged item id( structure tree root or existing tagged item )
			int rootTaggedItemID =
				( isRootTaggedItem ) ? StructureTreeRoot.Id : LastTaggedItem.Id;

			// update tagged item with reference on its marked content sequence
			//el.ChildrensMarkedContentSequenceRef = mIndexOfMarkedContentSequence - 1;

			if(!isRootTaggedItem) {
				// update parent with reference on child
				LastTaggedItem.AddChild(el);
			}
			else {
				StructureTreeRoot.AddChild(el.Id);
			}

			// update stack of tagged item ids
			LastTaggedItem = el;

			// set reference on parent item( for root tagged item it is structure tree root )
			el.Parent = rootTaggedItemID;

			// set reference for page which contains this tagged item
			//el.PageRef = mDoc.Page;

			return el.Id;
		}
		/// <summary>
		/// End tagged items
		/// </summary>
		public void EndTaggedItems() {
			while(IsTaggedPdf) {
				EndTaggedItem();
			}
		}
		/// <summary>
		/// End tagged item object
		/// </summary>
		public void EndTaggedItem() {
			// pop from stack of tagged item ids
			mTaggedItems.Pop();
		}
		/// <summary>
		/// Append a string of raw pdf content
		/// </summary>
		/// <param name="theContent"></param>
		public void AddString(string theContent) {
			BeginMarkedContentSequence();
			mContents += theContent;
			EndMarkedContentSequence();
		}
		/// <summary>
		/// Start new page
		/// </summary>
		public void StartNewPage() {
			mDoc.Page = mDoc.AddPage();
			mPageStore.Add(mContents);
			mContents = "";  
		}
		/// <summary>
		/// Write content to the doc
		/// </summary>
		public void AddToDoc() {
			AddPageOnDemand();
			for (int i = 0; i < mPageStore.Count; i++ ) {
				mDoc.PageNumber = i+1;
				mDoc.SetInfo(mDoc.FrameRect(), "stream", mPageStore[i] as string);
			}
			if (mContents.Length > 0) {
				mDoc.PageNumber++;
				mDoc.SetInfo(mDoc.FrameRect(), "stream", mContents);
			}
		}

		/// <summary>
		/// Adds a block of tagged text
		/// </summary>
		/// <param name="tagName"> ag name</param>
		/// <param name="text">The text to be added </param>
		public void AddTaggedText(string tagName, string text) {
			BeginTaggedItem(tagName);
			AddTaggedText(text);
			EndTaggedItem();
		}
		/// <summary>
		/// Adds a block of tagged text. Uses current tagged item.
		/// </summary>
		/// <param name="text">The text to be added</param>
		public void AddTaggedText(string text) {
			int theID = mDoc.AddHtml(text);
			if (theID > 0)
				SavePageContent(false);

			while (mDoc.Chainable(theID) || theID == 0) {
				string tagName = LastTaggedItem.Type;
				EndTaggedItem();
				StartNewPage();

				BeginTaggedItem(tagName);
				if (theID != 0)
					theID = mDoc.AddHtml("", theID);
				else
					theID = mDoc.AddHtml(text);
				SavePageContent(false);
			}
			ClearPageContent();
		}
		/// <summary>
		/// Adds tagged image
		/// </summary>
		/// <param name="tagName">Tag name</param>
		/// <param name="fileName">A file path to the image</param>
		public void AddTaggedImage(string tagName, string fileName) {
			BeginTaggedItem(tagName);
			AddTaggedImage(fileName);
			EndTaggedItem();
		}
		/// <summary>
		/// Adds tagged image. Current tagged item is used.
		/// </summary>
		/// <param name="fileName">A file path to the image</param>
		public void AddTaggedImage(string fileName) {
			if (!File.Exists(fileName))
				return;
			XImage image = new XImage();
			image.SetFile(fileName);
			mDoc.AddImage(fileName);
			SavePageContent(true);
		}
		#endregion

		#region Utility methods
		/// <summary>
		/// Add page on demand (if document doesn't contain any)
		/// </summary>
		private void AddPageOnDemand() {
			if (mDoc.PageCount == 0)
				mDoc.Page = mDoc.AddPage();
		}
		/// <summary>
		/// Add structure tree root on demand
		/// </summary>
		private void CreateStructureTreeRootOnDemand() {
			if(mStructureTreeRoot == null) {
				mStructureTreeRoot = new StructureTreeRoot(mDoc, TaggedItemRefs);
			}
		}
		/// <summary>
		/// Begin marked content sequence
		/// </summary>
		public void BeginMarkedContentSequence() {
			mContents += " /" + LastTaggedItem.Type + "<< /MCID ";
			mContents += LastTaggedItem.MarkedContentSequenceIndex.ToString();
			mContents += " >>";
			mContents += "BDC";
		}
		/// <summary>
		/// End marked content sequence
		/// </summary>
		public void EndMarkedContentSequence() {
			mContents += " EMC";
			LastTaggedItem.ChildrensMarkedContentSequenceRef = mIndexOfMarkedContentSequence - 1;
		}
		/// <summary>
		/// Save current page contents
		/// </summary>
		/// <param name="clear">Whether it is necessary to delete page comntents after saving</param>
		public void SavePageContent(bool clear) {
			string pageNr = (mDoc.PageNumber - 1).ToString();
			string contentObj = mDoc.GetInfo(mDoc.Root, "/Pages*/Kids*[" + pageNr + "]*/Contents[0]:Ref");
			int id = int.Parse(contentObj);
			string streamText = mDoc.GetInfo(id, "Stream");
			string contentString = "\r\n" + streamText;
			if (clear)
				mDoc.Delete(id);

			BeginMarkedContentSequence();
			mContents += contentString;
			EndMarkedContentSequence();
		}
		/// <summary>
		/// Clear the content streams for all pages
		/// </summary>
		public void ClearPageContent() {
			for (int i = mDoc.PageCount - 1; i >= 0; i-- ) {
				string contentObj = mDoc.GetInfo(mDoc.Root, "/Pages*/Kids*[" + i + "]*/Contents[0]:Ref");
				if (contentObj != "") {
					int id = int.Parse(contentObj);
					mDoc.Delete(id);
				}
			}
		}

		#endregion
	}
}
