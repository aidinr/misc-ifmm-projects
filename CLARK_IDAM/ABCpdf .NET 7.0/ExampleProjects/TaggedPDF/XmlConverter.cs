// ===========================================================================
//	�2006 WebSupergoo. All rights reserved.
// ===========================================================================

using System;
using System.Xml;
using System.Collections;
using WebSupergoo.ABCpdf7;
using System.IO;

namespace TaggedPDF {
	/// <summary>
	/// Parses xml files and converts them to the tagged pdf
	/// </summary>
	public class XmlConverter {
		#region Members
		/// <summary>
		/// Base directory for resolving relative URIs
		/// </summary>
		private string mBaseDirectory;
		/// <summary>
		/// Tagged pdf content of generated file
		/// </summary>
		protected TaggedContent mContent;
		/// <summary>
		/// Parent document
		/// </summary>
		protected Doc mDoc;
		/// <summary>
		/// Xml reader for parsing document
		/// </summary>
		protected XmlTextReader mReader;
		/// <summary>
		/// Stack of font sizes
		/// </summary>
		protected Stack mFontSizeStack = new Stack();
		/// <summary>
		/// Stack of font faces
		/// </summary>
		protected Stack mFontFaceStack = new Stack();
		/// <summary>
		/// Stack of font colors
		/// </summary>
		protected Stack mFontColorStack = new Stack();
		/// <summary>
		/// Stack of paragraph alignments
		/// </summary>
		protected Stack mAlignStack = new Stack();
		/// <summary>
		/// Stack of text styles
		/// </summary>
		protected Stack mTextStyleStack = new Stack();
		/// <summary>
		/// Height of the current line
		/// </summary>
		protected int mNewLineAdvance;
		/// <summary>
		/// Background color
		/// </summary>
		string mBackgroundColor;
		/// <summary>
		/// Background image
		/// </summary>
		string mBackgroundImage;
		#endregion

		#region Handlers
		/// <summary>
		/// Handle new paragraph tag P
		/// </summary>
		protected void HandleP() {
			mDoc.Pos.X = mDoc.Rect.Left;

			XTextStyle theTextStyle = new XTextStyle();
			theTextStyle.String = (mTextStyleStack.Peek() as XTextStyle).String;

			if (mReader.GetAttribute("fontsize") != null)
				mFontSizeStack.Push(int.Parse(mReader.GetAttribute("fontsize")));
			else
				mFontSizeStack.Push(mFontSizeStack.Peek());

			ExtractParagraphAttributes(theTextStyle);

			mTextStyleStack.Push(theTextStyle);

		}
		/// <summary>
		/// Handle end of the paragraph
		/// </summary>
		protected void HandleCloseP() {
			mAlignStack.Pop();
			mFontSizeStack.Pop();
			XTextStyle textStyle = mTextStyleStack.Pop() as XTextStyle;
			mDoc.Pos.Y -= textStyle.ParaSpacing + mNewLineAdvance;
			mNewLineAdvance = 0;
		}
		/// <summary>
		/// Handle headers tag ( H1, H2 ... H6)
		/// </summary>
		protected void HandleH() {
			mDoc.Pos.X = mDoc.Rect.Left;

			XTextStyle theTextStyle = new XTextStyle();
			theTextStyle.String = (mTextStyleStack.Peek() as XTextStyle).String;

			switch (mReader.Name.ToLower()) {
				case "h1":
					mFontSizeStack.Push(28);
					break;
				case "h2":
					mFontSizeStack.Push(24);
					break;
				case "h3":
					mFontSizeStack.Push(20);
					break;
				case "h4":
					mFontSizeStack.Push(16);
					break;
				case "h5":
					mFontSizeStack.Push(10);
					break;
				case "h6":
					mFontSizeStack.Push(6);
					break;
			}
			ExtractParagraphAttributes(theTextStyle);
			mTextStyleStack.Push(theTextStyle);
		}
		/// <summary>
		/// Handle Font tag
		/// </summary>
		protected void HandleFont() {
			if (mReader.GetAttribute("face") != null)
				mFontFaceStack.Push(mReader.GetAttribute("face"));
			else
				mFontFaceStack.Push(mFontFaceStack.Peek());

			if (mReader.GetAttribute("size") != null)
				mFontSizeStack.Push(int.Parse(mReader.GetAttribute("size")));
			else
				mFontSizeStack.Push(mFontSizeStack.Peek());

			if (mReader.GetAttribute("color") != null)
				mFontColorStack.Push(mReader.GetAttribute("color"));
			else
				mFontColorStack.Push(mFontColorStack.Peek());

		}
		/// <summary>
		/// Handle end of the font tag
		/// </summary>
		protected void HandleCloseFont() {
			mFontFaceStack.Pop();
			mFontSizeStack.Pop();
			mFontColorStack.Pop();
		}
		/// <summary>
		/// Handle image tag
		/// </summary>
		protected void HandleImage() {
			if (mReader.GetAttribute("alt") != null) {
				StructureElement element = mContent.LastTaggedItem;
				element.AltDescription = mReader.GetAttribute("alt");
			}
			if (mReader.GetAttribute("src") != null)
				ProcessImage(mReader.GetAttribute("src"));
		}
		/// <summary>
		/// Handle B tag
		/// </summary>
		protected void HandleB() {
			XTextStyle theStyle = new XTextStyle();
			theStyle.String = (mTextStyleStack.Peek() as XTextStyle).String;
			theStyle.Bold = true;
			mTextStyleStack.Push(theStyle);
		}
		/// <summary>
		/// Handle end of the B tag
		/// </summary>
		protected void HandleCloseB() {
			mTextStyleStack.Pop();
		}
		/// <summary>
		/// Handle I tag
		/// </summary>
		protected void HandleI() {
			XTextStyle theStyle = new XTextStyle();
			theStyle.String = (mTextStyleStack.Peek() as XTextStyle).String;
			theStyle.Italic = true;
			mTextStyleStack.Push(theStyle);
		}
		/// <summary>
		/// Handle end of the I tag
		/// </summary>
		protected void HandleCloseI() {
			mTextStyleStack.Pop();
		}
		/// <summary>
		/// Handle U tag
		/// </summary>
		protected void HandleU() {
			XTextStyle theStyle = new XTextStyle();
			theStyle.String = (mTextStyleStack.Peek() as XTextStyle).String;
			theStyle.Underline = true;
			mTextStyleStack.Push(theStyle);
		}
		/// <summary>
		/// Handle end of the U tag
		/// </summary>
		protected void HandleCloseU() {
			mTextStyleStack.Pop();
		}
		/// <summary>
		/// Handle Strike tag
		/// </summary>
		protected void HandleStrike() {
			XTextStyle theStyle = new XTextStyle();
			theStyle.String = (mTextStyleStack.Peek() as XTextStyle).String;
			theStyle.Strike = true;
			mTextStyleStack.Push(theStyle);
		}
		/// <summary>
		/// Handle end of the Strike tag
		/// </summary>
		protected void HandleCloseStrike() {
			mTextStyleStack.Pop();
		}
		/// <summary>
		/// Handle BR tag
		/// </summary>
		protected void HandleBR() {
			mDoc.Pos.X = mDoc.Rect.Left;
			mDoc.Pos.Y -= mNewLineAdvance;
			mNewLineAdvance = 0;			
		}


		/// <summary>
		/// Handle BODY tag
		/// </summary>
		protected void HandleBody() {
			if (mReader.GetAttribute("bgcolor") != null)
				SetBackgroundColor(mReader.GetAttribute("bgcolor"));
			if (mReader.GetAttribute("background") != null)
				SetBackgroundImage(mReader.GetAttribute("background"));
			if (mReader.GetAttribute("leftmargin") != null)
				mDoc.Rect.Left += int.Parse(mReader.GetAttribute("leftmargin"));
			if (mReader.GetAttribute("rightmargin") != null)
				mDoc.Rect.Right -= int.Parse(mReader.GetAttribute("leftmargin"));
			if (mReader.GetAttribute("topmargin") != null)
				mDoc.Rect.Top -= int.Parse(mReader.GetAttribute("topmargin"));
			if (mReader.GetAttribute("bottommargin") != null)
				mDoc.Rect.Bottom += int.Parse(mReader.GetAttribute("bottommargin"));
		}

		/// <summary>
		/// New page handler. Sets background color and image when new page is added.
		/// </summary>
		protected void HandleNewPage() {
			if ( mBackgroundColor != null)
				SetBackgroundColor(mBackgroundColor);

			if ( mBackgroundImage != null )
				SetBackgroundImage(mBackgroundImage);
		}

		#endregion

		#region Utilities
		/// <summary>
		/// Extracts all common atrribute of p, h1, ... h6 tags 
		/// </summary>
		/// <param name="textStyle">Textstyle descibed by the tag attributes</param>
		private void ExtractParagraphAttributes(XTextStyle textStyle) {
			if (mReader.GetAttribute("align") != null) {
				switch (mReader.GetAttribute("align")) {
					case "left":
						mAlignStack.Push(0.0);
						break;
					case "right":
						mAlignStack.Push(1.0);
						break;
					case "center":
						mAlignStack.Push(0.5);
						break;
				}
			}
			else
				mAlignStack.Push(mAlignStack.Peek());

			if (mReader.GetAttribute("charspacing") != null)
				textStyle.CharSpacing = ConvertDouble.FromString(mReader.GetAttribute("charspacing"));

			if (mReader.GetAttribute("wordspacing") != null)
				textStyle.WordSpacing = ConvertDouble.FromString(mReader.GetAttribute("wordspacing"));

			if (mReader.GetAttribute("justification") != null)
				textStyle.Justification = ConvertDouble.FromString(mReader.GetAttribute("justification"));

			if (mReader.GetAttribute("bold") != null)
				textStyle.Bold = bool.Parse(mReader.GetAttribute("bold"));

			if (mReader.GetAttribute("italic") != null)
				textStyle.Italic = bool.Parse(mReader.GetAttribute("italic"));

			if (mReader.GetAttribute("underline") != null)
				textStyle.Underline = bool.Parse(mReader.GetAttribute("underline"));

			if (mReader.GetAttribute("strike") != null)
				textStyle.Strike = bool.Parse(mReader.GetAttribute("strike"));

			if (mReader.GetAttribute("strike2") != null)
				textStyle.Strike2 = bool.Parse(mReader.GetAttribute("strike2"));

			if (mReader.GetAttribute("outline") != null)
				textStyle.Outline = ConvertDouble.FromString(mReader.GetAttribute("outline"));

			if (mReader.GetAttribute("linespacing") != null)
				textStyle.LineSpacing = ConvertDouble.FromString(mReader.GetAttribute("linespacing"));

			if (mReader.GetAttribute("paraspacing") != null) {
				textStyle.ParaSpacing = ConvertDouble.FromString(mReader.GetAttribute("paraspacing"));
				mDoc.Pos.Y -= textStyle.ParaSpacing;
			}

			if (mReader.GetAttribute("leftmargin") != null)
				textStyle.LeftMargin = ConvertDouble.FromString(mReader.GetAttribute("leftmargin"));

			if (mReader.GetAttribute("indent") != null)
				textStyle.Indent = ConvertDouble.FromString(mReader.GetAttribute("indent"));

			if (mReader.GetAttribute("leftmargin") != null)
				textStyle.LeftMargin = ConvertDouble.FromString(mReader.GetAttribute("leftmargin"));
		}

		/// <summary>
		/// Process arbitrary tag
		/// </summary>
		protected void ProcessTag() {
			mContent.BeginTaggedItem(mReader.Name);

			switch (mReader.Name.ToLower()) {
				case "p":
					HandleP();
					break;
				case "h1":
				case "h2":
				case "h3":
				case "h4":
				case "h5":
				case "h6":
					HandleH();
					break;
				case "font":
					HandleFont();
					break;
				case "img":
					HandleImage();
					break;
				case "b":
					HandleB();
					break;
				case "br":
					HandleBR();
					break;
				case "i":
					HandleI();
					break;
				case "u":
					HandleU();
					break;
				case "strike":
					HandleStrike();
					break;
				case "body":
					HandleBody();
					break;
			}
		}
		/// <summary>
		/// Process closing of arbitrary tag
		/// </summary>
		/// <param name="mReader">Xml reader to read data from</param>
		private void ProcessCloseTag(XmlReader mReader) {
			mContent.EndTaggedItem();

			switch (mReader.Name.ToLower()) {
				case "p":
				case "h1":
				case "h2":
				case "h3":
				case "h4":
				case "h5":
				case "h6":
					HandleCloseP();
					break;
				case "font":
					HandleCloseFont();
					break;
				case "b":
					HandleCloseB();
					break;
				case "i":
					HandleCloseI();
					break;
				case "u":
					HandleCloseU();
					break;
				case "strike":
					HandleCloseStrike();
					break;
			}
		}
		/// <summary>
		/// Adds block of text to the output content
		/// </summary>
		/// <param name="text">Text data</param>
		protected void ProcessText(string text) {
			mDoc.TextStyle.String = (mTextStyleStack.Peek() as XTextStyle).String;
			mDoc.FontSize = (int) mFontSizeStack.Peek();
			mDoc.Font = mDoc.AddFont( (string)mFontFaceStack.Peek() );
			mDoc.Color.String = (string)mFontColorStack.Peek();
			mDoc.HPos = (double)mAlignStack.Peek();
	
			if ( mDoc.Pos.X == mDoc.Rect.Right)
				mDoc.Pos.Y -= mNewLineAdvance;

			int initialY = (int)mDoc.Pos.Y;

			int theID = mDoc.AddHtml(text);
			if (theID > 0)
				mContent.SavePageContent(false);

			while (mDoc.Chainable(theID) || theID == 0) {
				string tagName = mContent.LastTaggedItem.Type;
				mContent.EndTaggedItem();
				mContent.StartNewPage();
				HandleNewPage();
				mContent.BeginTaggedItem(tagName);
				if (theID != 0)
					theID = mDoc.AddHtml("", theID);
				else
					theID = mDoc.AddHtml(text);
				mContent.SavePageContent(false);
			}
			mContent.ClearPageContent();

			if (mDoc.Pos.Y > initialY)
				mNewLineAdvance = mDoc.FontSize;
			else
				mNewLineAdvance = Math.Max(mDoc.FontSize, mNewLineAdvance);
		}
		/// <summary>
		/// Adds image object to the output content
		/// </summary>
		/// <param name="fileName">Image file name</param>
		protected void ProcessImage(string fileName) {
			XImage image = new XImage();
			if (!Path.IsPathRooted(fileName))
				fileName = mBaseDirectory + fileName;

			if (!File.Exists(fileName))
				return;
			image.SetFile(fileName);

			HandleBR();

			if ( mDoc.Pos.Y - image.Height < mDoc.Rect.Bottom) {
				mContent.EndTaggedItem();
				mContent.StartNewPage();
				HandleNewPage();
				mContent.BeginTaggedItem(mContent.LastTaggedItem.Type);
			}

			string curRect = mDoc.Rect.String;
			double curY = mDoc.Pos.Y;
			mDoc.HPos = (double)mAlignStack.Peek();

			if (image.Width < mDoc.Rect.Width)
				mDoc.Rect.Left = mDoc.Rect.Left + (mDoc.Rect.Width - image.Width)* mDoc.HPos;
			else
				mDoc.Rect.Left = 0;
			mDoc.Rect.Right = mDoc.Rect.Left + image.Width;
			mDoc.Rect.Top = curY;
			mDoc.Rect.Bottom = mDoc.Rect.Top - image.Height;

			mDoc.AddImage(fileName);

			mDoc.Rect.String = curRect;
			mDoc.Pos.Y = curY;

			mContent.SavePageContent(true);

			mDoc.Pos.X += image.Width;
			mNewLineAdvance = image.Height;
			HandleBR();
			image.Dispose();
			mContent.EndTaggedItem();
		}

		/// <summary>
		/// Adds background image to the document
		/// </summary>
		/// <param name="fileName"></param>
		protected void SetBackgroundImage(string fileName) {		
			XImage image = new XImage();
			if (!Path.IsPathRooted(fileName))
				fileName = mBaseDirectory + fileName;
			if (File.Exists(fileName)) {
				mContent.BeginTaggedItem("background");
				mBackgroundImage = fileName;
				string curRect = mDoc.Rect.String;
				image.SetFile(fileName);
				mDoc.Rect.String = mDoc.MediaBox.String;
				mDoc.AddImage(image);
				mContent.SavePageContent(true);
				mDoc.Rect.String = curRect;
				mContent.EndTaggedItem();
			}
		}
		/// <summary>
		/// Sets background color for the document
		/// </summary>
		/// <param name="color">Background color</param>
		protected void SetBackgroundColor(string color) {
			mContent.BeginTaggedItem("background");
			mBackgroundColor = color;
			string curRect = mDoc.Rect.String;
			string curColor = mDoc.Color.String;
			mDoc.Rect.String = mDoc.MediaBox.String;
			mDoc.Color.String = color;
			mDoc.FillRect();
			mContent.SavePageContent(true);
			mContent.EndTaggedItem();
			mDoc.Rect.String = curRect;
			mDoc.Color.String = curColor;
		}

		/// <summary>
		/// Set default values for text styles and other formatting attributes
		/// </summary>
		protected void Init() {
			mFontSizeStack.Clear();
			mFontSizeStack.Push(10);
			mFontFaceStack.Clear();
			mFontFaceStack.Push("Times-Roman");
			mFontColorStack.Clear();
			mFontColorStack.Push("black");
			mAlignStack.Clear();
			mAlignStack.Push(0.0);
			mNewLineAdvance = 0;
			mTextStyleStack.Clear();
			mTextStyleStack.Push(new XTextStyle());
			mBackgroundColor = null;
			mBackgroundImage = null;
		}
		#endregion

		#region Public methods
		/// <summary>
		/// Constructor
		/// </summary>
		public XmlConverter() {
			Init();
		}
		/// <summary>
		/// Converts xml file to the tagged pdf
		/// </summary>
		/// <param name="fileName">Xml file name</param>
		public void Convert(string fileName, string pdfFilePath) {
			mBaseDirectory = Path.GetDirectoryName(fileName) + Path.DirectorySeparatorChar;

			mDoc = new Doc();
			mContent = new TaggedContent(mDoc);
			Init();

			try {
				mReader = new XmlTextReader(fileName);
				mReader.WhitespaceHandling = WhitespaceHandling.None;

				while (mReader.Read()) {
					switch (mReader.NodeType) {
						case XmlNodeType.Element:
							string tagName = mReader.Name;
							ProcessTag();
							break;
						case XmlNodeType.Text:
							string text = mReader.Value;
							ProcessText(text);
							break;
						case XmlNodeType.EndElement:
							ProcessCloseTag(mReader);
							break;
					}
				}
			}
			finally {
				if (mReader!=null) {
					mReader.Close();
					mContent.AddToDoc();
					mDoc.Save(pdfFilePath);
				}
			}
		}
		#endregion
	}
}
