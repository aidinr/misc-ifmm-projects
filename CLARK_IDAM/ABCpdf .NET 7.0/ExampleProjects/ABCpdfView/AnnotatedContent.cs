// ===========================================================================
//	�2006 WebSupergoo. All rights reserved.
// ===========================================================================

using System;
using System.Xml;
using System.Globalization;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections;
using System.Windows.Forms;
using System.IO;
using WebSupergoo.ABCpdf7;
using WebSupergoo.ABCpdf7.Atoms;
using WebSupergoo.ABCpdf7.Objects;


namespace ABCpdfControls {
	/// <summary>
	/// Annotated page content.
	/// </summary>
	internal class AnnotatedContent {
		private Doc mDoc;
		private NumberFormatInfo mFormatProvider;
		const string kDefaultTextProperties = " 0 Tc 0 Tw 100 Tz 0 TL 0 Ts ";

		public AnnotatedContent(Doc inDoc) {
			mDoc = inDoc;
			mFormatProvider = new NumberFormatInfo();
			mFormatProvider.NumberDecimalSeparator = ".";
			mFormatProvider.NumberDecimalDigits = 5;
		}
		/// <summary>
		/// Extract text block information from the xml decription
		/// </summary>
		/// <param name="node">Xml node corresonding to the text block</param>
		/// <returns>Text Block</returns>
		private TextBlock ParseTextBlock(XmlNode node) {
			NumberFormatInfo theFormatProvider = new NumberFormatInfo();
			theFormatProvider.NumberDecimalSeparator = ".";

			string text = "";
			float w = 0;
			if (node.ChildNodes.Count == 0 || (node.ChildNodes.Count > 0 && node.ChildNodes[0].Name != "tspan")) {
				text = node.InnerText;
				string lengthStr = node.Attributes["pdf_w1000"].Value;
				w = float.Parse(lengthStr, theFormatProvider)/ 1000;
			}
			else {
				for (int j = 0; j < node.ChildNodes.Count; j++) {
					XmlNode childNode = node.ChildNodes.Item(j);
					if (childNode.Name == "tspan") {
						text += childNode.InnerText;
						string lengthStr = childNode.Attributes["pdf_w1000"].Value;
						w += float.Parse(lengthStr, theFormatProvider)/ 1000;
					}
				}
			}

			if (text == "" || w == 0)
				return null;

			float h = 1;
			GraphicsPath thePath = new GraphicsPath();
			thePath.AddRectangle(new RectangleF(0, -(float)0.2*h, w, h));

			string textMatrix = node.Attributes["pdf_Trm"].Value;
			string[] valuesStr = textMatrix.Split(' ');
			Matrix theMatrix = new Matrix(float.Parse(valuesStr[0], theFormatProvider), float.Parse(valuesStr[1], theFormatProvider), float.Parse(valuesStr[2], theFormatProvider), float.Parse(valuesStr[3], theFormatProvider), float.Parse(valuesStr[4], theFormatProvider), float.Parse(valuesStr[5], theFormatProvider));
			thePath.Transform(theMatrix);

			TextBlock theBlock = new TextBlock(text, thePath, node);
			if (node.Attributes["pdf_StreamID"] != null)
				theBlock.StreamId = int.Parse(node.Attributes["pdf_StreamID"].Value);

			if (node.Attributes["pdf_StreamOffset"] != null)
				theBlock.StreamOffset = int.Parse(node.Attributes["pdf_StreamOffset"].Value);

			if (node.Attributes["pdf_StreamLength"] != null)
				theBlock.Length = int.Parse(node.Attributes["pdf_StreamLength"].Value);

			if (node.Attributes["fill"] != null) {
				string colorStr = node.Attributes["fill"].Value;
				colorStr = colorStr.Replace("rgb(", "");
				colorStr = colorStr.Replace(")", "");
				XColor theColor = new XColor();
				theColor.String = colorStr;
				theBlock.TextColor = theColor.Color;
			}
			else
				theBlock.TextColor = Color.Black;

			if (node.Attributes["font-family"] != null)
				theBlock.FontFamily = node.Attributes["font-family"].Value;

			if (node.Attributes["font-size"] != null)
				theBlock.FontSize = float.Parse(node.Attributes["font-size"].Value, mFormatProvider);

			if (node.Attributes["pdf_Op"] != null)
				theBlock.PDFOperator = node.Attributes["pdf_Op"].Value;

			if (node.Attributes["pdf_Tf"] != null)
				theBlock.FontOperator = node.Attributes["pdf_Tf"].Value;

			if (node.Attributes["pdf_Ts"] != null)
				theBlock.TextRise = float.Parse(node.Attributes["pdf_Ts"].Value, mFormatProvider);

			if (node.Attributes["pdf_Tw"] != null)
				theBlock.WordSpacing = float.Parse(node.Attributes["pdf_Tw"].Value, mFormatProvider);

			if (node.Attributes["pdf_Tc"] != null)
				theBlock.CharacterSpacing = float.Parse(node.Attributes["pdf_Tc"].Value, mFormatProvider);

			if (node.Attributes["pdf_Tl"] != null)
				theBlock.TextLeading = float.Parse(node.Attributes["pdf_Tl"].Value, mFormatProvider);

			if (node.Attributes["pdf_Th"] != null)
				theBlock.HorizontalScaling = float.Parse(node.Attributes["pdf_Th"].Value, mFormatProvider);

			theBlock.TextRenderingMatrix = theMatrix;

			return theBlock;
		}

		/// <summary>
		/// Returns Text Blocks from the page
		/// </summary>
		/// <param name="mDoc">Document</param>
		/// <returns>ArrayList containing text blocks</returns>
		public ArrayList Parse(Doc mDoc) {
			ArrayList theBlocks = new ArrayList();
			mDoc.Flatten();
			string content = mDoc.GetText("SVG+");

			StringReader xmlReader = new StringReader(content);
			XmlDocument xmlDoc = new XmlDocument();

			try {
				xmlDoc.Load(xmlReader);
			}
			catch (Exception e) {
				MessageBox.Show("Error deconstructing SVG: " + e.Message);
			}

			theBlocks.Clear();
			XmlNode svg = xmlDoc["svg"];
			int svgCount = (svg != null) ? svg.ChildNodes.Count : 0;
			for (int i = 0; i < svgCount; i++) {
				XmlNode theOperator = svg.ChildNodes.Item(i);
				if (theOperator.Name == "text") {
					TextBlock theBlock = ParseTextBlock(theOperator);
					if (theBlock != null)
						theBlocks.Add(theBlock);
				}
			}
			return theBlocks;
		}

		public void UpdateTextBlock(TextBlock inBlock, string updatedString) {
			if (inBlock != null && inBlock.StreamId != 0 && inBlock.StreamOffset > 0 && inBlock.Length > 0) {
				StreamObject streamObject = (StreamObject)mDoc.ObjectSoup[inBlock.StreamId];
				streamObject.Decompress();

				string stream = streamObject.GetText();

				stream = stream.Remove(inBlock.StreamOffset, inBlock.Length);
						
				updatedString = updatedString.Replace("\\", "\\\\");
				updatedString = updatedString.Replace("(", "\\(");
				updatedString = updatedString.Replace(")", "\\)");

				if (inBlock.Node.Name == "text")
					updatedString = "(" + updatedString + ") Tj";
				stream = stream.Insert(inBlock.StreamOffset, updatedString);
				streamObject.SetText(stream);
			}
		}

		string UnicodeToFontEncoding(Doc inDoc, string inFont, string inText) {
			// trivial mapping not currently accounting for the font encoding - hence inFont is unused
			string theText = inText;
			theText = theText.Replace("\\", "\\\\");
			theText = theText.Replace("(", "\\(");
			theText = theText.Replace(")", "\\)");
			return theText;
		}

		public string GetFontOperator( int FontObectID ) {
			string fontStr = mDoc.GetInfo(mDoc.Page, "/Resources/Font");
			string fontCommand = "";
			Atom fontAtom = Atom.FromString(fontStr);
			DictAtom fontDict = fontAtom as DictAtom;
			if (fontAtom is DictAtom) {
				IDictionaryEnumerator it = fontDict.GetEnumerator();
				while (it.MoveNext()) {
					if ((it.Value is RefAtom) && ((it.Value as RefAtom).ID == FontObectID)) {
						fontCommand = it.Key.ToString();
						break;
					}
				}
			}
			if (fontCommand == "") {
				if ( !(fontAtom is DictAtom) )
					fontDict = new DictAtom();

				Atom fontRef = Atom.FromString(FontObectID.ToString()+" 0 R");
				fontCommand = "Fabc"+FontObectID.ToString();
				fontDict.Add(fontCommand, fontRef);
				mDoc.SetInfo(mDoc.Page, "/Resources/Font", fontDict.ToString());
			}

			return fontCommand;
		}

		public void UpdateTextBlock(TextBlock inBlock, string inText, Color inColor, string font) {
			if (inBlock != null && inBlock.StreamId != 0 && inBlock.StreamOffset > 0 && inBlock.Length > 0) {
				StreamObject streamObject = (StreamObject)mDoc.ObjectSoup[inBlock.StreamId];
				streamObject.Decompress();
				string stream = streamObject.GetText();
				stream = stream.Remove(inBlock.StreamOffset, inBlock.Length);

				string updatedString = inBlock.PDFOperator;
				if (inBlock.Text != inText) {
					updatedString = inText;
					UnicodeToFontEncoding(mDoc, inBlock.FontFamily, updatedString);
					if (inBlock.Node.Name == "text")
						updatedString = "(" + updatedString + ") Tj";
				}

				if (inBlock.TextColor != inColor) {
					string colorOP = String.Format(mFormatProvider, " {0:F} {1:F} {2:F} rg ", inColor.R /255.0, inColor.G /255.0, inColor.B /255.0);
					string oldColorOP = String.Format(mFormatProvider, " {0:F} {1:F} {2:F} rg", inBlock.TextColor.R /255.0, inBlock.TextColor.G /255.0, inBlock.TextColor.B /255.0);
					updatedString = colorOP + updatedString + oldColorOP;
				}

				string fontOp = "";
				if (font != "") {
					int fontId = mDoc.AddFont(font);
					if (fontId > 0)
						fontOp = GetFontOperator(fontId);
				}

				if ( fontOp != "")
					updatedString = " /" + fontOp + " " + inBlock.FontSize + " Tf " + updatedString + " /" + inBlock.FontOperator + " " + inBlock.FontSize + " Tf ";

				stream = stream.Insert(inBlock.StreamOffset, updatedString);
				streamObject.SetText(stream);
			}
		}

		public string GetTextPropertiesString(TextBlock inBlock) {
			return " " + inBlock.TextRise.ToString(mFormatProvider) + " " + 
				inBlock.WordSpacing.ToString(mFormatProvider) + " " +
				inBlock.CharacterSpacing.ToString(mFormatProvider) + " " +
				inBlock.TextLeading.ToString(mFormatProvider) + " " +
				inBlock.HorizontalScaling.ToString(mFormatProvider) + " ";
		}

		public void DeleteTextBlock(TextBlock inBlock) {
			if (inBlock != null && inBlock.StreamId != 0 && inBlock.StreamOffset > 0 && inBlock.Length > 0) {
				StreamObject streamObject = (StreamObject)mDoc.ObjectSoup[inBlock.StreamId];
				streamObject.Decompress();
				string stream = streamObject.GetText();

				if (inBlock.StreamOffset + inBlock.Length < stream.Length)
					stream = stream.Remove(inBlock.StreamOffset, inBlock.Length);
				streamObject.SetText(stream);
			}
		}

		public void MoveTextBlock(TextBlock inBlock, int xOffset, int yOffset) {
			Matrix moveMatrix = new Matrix(1, 0, 0, 1, xOffset, yOffset);
			TransformTextBlock(inBlock, moveMatrix);
		}


		public void TransformTextBlock(TextBlock inBlock, Matrix ctm) {
			if (inBlock != null && inBlock.StreamId != 0 && inBlock.StreamOffset > 0 && inBlock.Length > 0) {
				DeleteTextBlock(inBlock);
				string colorOP = String.Format(mFormatProvider, " {0:F} {1:F} {2:F} rg ", inBlock.TextColor.R /255.0, inBlock.TextColor.G /255.0, inBlock.TextColor.B /255.0);
				Matrix m = inBlock.TextRenderingMatrix;
				string textMatrix = String.Format(mFormatProvider, "{0:F} {1:F} {2:F} {3:F} {4:F} {5:F} Tm ", m.Elements[0], m.Elements[1], m.Elements[2], m.Elements[3], m.Elements[4], m.Elements[5]);
				string fontInfo = " /" + inBlock.FontOperator + " 1 Tf ";

				string ctmString = String.Format(mFormatProvider, " {0:F} {1:F} {2:F} {3:F} {4:F} {5:F} cm ", ctm.Elements[0], ctm.Elements[1], ctm.Elements[2], ctm.Elements[3], ctm.Elements[4], ctm.Elements[5]);

				string textBlockStr = "q " + ctmString + "BT" + kDefaultTextProperties + colorOP + textMatrix + fontInfo + GetTextPropertiesString(inBlock) + inBlock.PDFOperator + " ET Q";
				mDoc.SetInfo(mDoc.FrameRect(), "stream", textBlockStr);
			}
		}
	}
}
