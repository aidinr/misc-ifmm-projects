// ===========================================================================
//	AdvancedGraphics				�2005 WebSupergoo. All rights reserved.
// ===========================================================================

#define CONTENT_VALIDATION

using System;
using System.Globalization;
using WebSupergoo.ABCpdf7;
using WebSupergoo.ABCpdf7.Atoms;
using WebSupergoo.ABCpdf7.Objects;
using System.Collections;
using System.Windows.Forms;


namespace AdvancedGraphics
{
	/// <summary>
	/// PDFContent class is a simple constructor of the pdf content.
	/// It provides easy access to the most of the commands described in the PDF Reference
	/// </summary>
	public class PDFContent
	{
		public PDFContent(Doc theDoc) 
		{
			mDoc = theDoc;
			mFormatProvider = new NumberFormatInfo();
			mFormatProvider.NumberDecimalSeparator = ".";
			mFormatProvider.NumberDecimalDigits = 5;
		}
		/// <summary>
		/// Begin new subpath
		/// </summary>
		/// <param name="x"> x coordinate</param>
		/// <param name="y"> y coordinate</param>
		public void Move(double x, double y)	
		{
			mContents += String.Format(mFormatProvider, " {0:F} {1:F} m", x, y);
			#if (CONTENT_VALIDATION)
			Validate("m");
			#endif
		}
		/// <summary>
		/// Append straight line segment to path
		/// </summary>
		/// <param name="x"> x coordinate </param>
		/// <param name="y"> y coordinate </param>
		public void Line(double x, double y) 
		{	
			mContents += String.Format(mFormatProvider, " {0:F} {1:F} l", x, y);
			#if (CONTENT_VALIDATION)
			Validate("l");
			#endif
		}
		/// <summary>
		/// Append rectangle to path.
		/// </summary>
		/// <param name="x"> Bottom left x coordinate </param>
		/// <param name="y"> Bottom left y coordinate </param>
		/// <param name="w">width</param>
		/// <param name="h">height</param>
		public void Rect(double x, double y, double w, double h)	
		{
			mContents += String.Format(mFormatProvider, " {0:F} {1:F} {2:F} {3:F} re", x, y, w, h);
			#if (CONTENT_VALIDATION)
			Validate("re");
			#endif
		}
		/// <summary>
		/// Append curved segment to path (three control points)
		/// </summary>
		/// <param name="x1">x coordinate (1st control point)</param>
		/// <param name="y1">y coordinate (1st control point)</param>
		/// <param name="x2">x coordinate (2nd control point)</param>
		/// <param name="y2">y coordinate (2nd control point)</param>
		/// <param name="x3">x coordinate (3rd control point)</param>
		/// <param name="y3">y coordinate (3rd control point)</param>
		public void Bezier(double x1, double y1, double x2, double y2, double x3, double y3)	
		{
			mContents += String.Format(mFormatProvider, " {0:F} {1:F} {2:F} {3:F} {4:F} {5:F} c", x1, y1, x2, y2, x3, y3);
			#if (CONTENT_VALIDATION)
			Validate("c");
			#endif
		}
		/// <summary>
		/// Close subpath
		/// </summary>
		public void Close() {
			mContents += " h";
		}
		/// <summary>
		/// Stroke path
		/// </summary>
		public void Stroke() {
			mContents += " S";
			#if (CONTENT_VALIDATION)
			Validate("S");
			#endif

		}
		/// <summary>
		/// Fill path using nonzero winding number rule
		/// </summary>
		public void Fill() {
			mContents += " f";
			#if (CONTENT_VALIDATION)
			Validate("f");
			#endif
		}
		/// <summary>
		/// Fill path using even-odd rule
		/// </summary>
		public void FillEvenOddRule() 
		{
			mContents += " f*";
			#if (CONTENT_VALIDATION)
			Validate("f");
			#endif
		}
		/// <summary>
		/// Set clipping path using nonzero winding number rule
		/// </summary>
		public void Clip() {
			mContents += " W n";
			#if (CONTENT_VALIDATION)
			Validate("W");
			#endif
		}
		/// <summary>
		/// Set clipping path using even-odd rule
		/// </summary>
		public void ClipEvenOddRule() 
		{
			mContents += " W* n";
			#if (CONTENT_VALIDATION)
			Validate("W");
			#endif
		}
		/// <summary>
		/// Save graphics state
		/// </summary>
		public void SaveState() {
			mContents += " q";
			#if (CONTENT_VALIDATION)
			Validate("q");
			#endif
		}
		/// <summary>
		/// Restore graphics state
		/// </summary>
		public void RestoreState() 
		{
			mContents += " Q";
			#if (CONTENT_VALIDATION)
			Validate("Q");
			#endif
		}
		/// <summary>
		/// Set line width
		/// </summary>
		/// <param name="v">Line width</param>
		public void SetLineWidth(double v) 
		{
			mContents += String.Format(mFormatProvider, " {0:F} w", v);
			#if (CONTENT_VALIDATION)
			Validate("w");
			#endif
		}
		/// <summary>
		/// The line cap for the ends of any lines to be stroked
		/// </summary>
		public enum LineCap
		{
			Butt,
			Round,
			ProjectingSquare
		}
		/// <summary>
		/// Set line cap style
		/// </summary>
		/// <param name="v"></param>
		public void SetLineCap(int v) 
		{
			mContents += String.Format(mFormatProvider, " {0} J", v);
			#if (CONTENT_VALIDATION)
			Validate("J");
			#endif
		}
		/// <summary>
		/// The line join for the shape of joints between connected segments of a path
		/// </summary>
		public enum LineJoin
		{
			Miter,
			Round,
			Bevel
		}
		/// <summary>
		/// Set line join style
		/// </summary>
		/// <param name="v">Line join style</param>
		public void SetLineJoin(int v) 
		{
			mContents += String.Format(mFormatProvider, " {0} j", v);
			#if (CONTENT_VALIDATION)
			Validate("j");
			#endif
		}
		/// <summary>
		/// Set miter limit
		/// </summary>
		/// <param name="v">Miter limit</param>
		public void SetMiterLimit(double v) 
		{
			mContents += String.Format(mFormatProvider, " {0:F} M", v);
			#if (CONTENT_VALIDATION)
			Validate("M");
			#endif
		}
		/// <summary>
		/// Set line dash pattern
		/// </summary>
		/// <param name="dashPattern">Dash pattern</param>
		public void LineDash( string dashPattern) 
		{
			mContents += " " + dashPattern + " d";
			#if (CONTENT_VALIDATION)
			Validate("d");
			#endif
		}
		/// <summary>
		/// Set gray level for stroking operations
		/// </summary>
		/// <param name="w">level of gray color</param>
		public void SetGrayStrokeColor(double w) 
		{
			mContents += String.Format(mFormatProvider, " {0:F} G", w);
			#if (CONTENT_VALIDATION)
			Validate("G");
			#endif
		}
		/// <summary>
		/// Set gray level for nonstroking operations
		/// </summary>
		/// <param name="w">level of gray color</param>
		public void SetGrayNonStrokeColor(double w) 
		{
			mContents += String.Format(mFormatProvider, " {0:F} g", w);
			#if (CONTENT_VALIDATION)
			Validate("g");
			#endif
		}
		/// <summary>
		/// Set RGB color for stroking operations
		/// </summary>
		/// <param name="r">level of red color</param>
		/// <param name="g">level of green color </param>
		/// <param name="b">level of blue color</param>
		public void SetRGBStrokeColor(double r, double g, double b) 
		{
			mContents += String.Format(mFormatProvider, " {0:F} {1:F} {2:F} RG", r, g, b);
			#if (CONTENT_VALIDATION)
			Validate("RG");
			#endif
		}
		/// <summary>
		/// Set RGB color for nonstroking operations
		/// </summary>
		/// <param name="r">level of red color</param>
		/// <param name="g">level of green color </param>
		/// <param name="b">level of blue color</param>
		public void SetRGBNonStrokeColor(double r, double g, double b) 
		{
			mContents += String.Format(mFormatProvider, " {0:F} {1:F} {2:F} rg", r, g, b);
			#if (CONTENT_VALIDATION)
			Validate("rg");
			#endif
		}
		/// <summary>
		/// Set CMYK color for stroking operations
		/// </summary>
		/// <param name="c">level of cyan color</param>
		/// <param name="m">level of magenta color</param>
		/// <param name="y">level of yellow color</param>
		/// <param name="k">level of black color</param>
		public void SetCMYKStrokeColor(double c, double m, double y, double k) 
		{
			mContents += String.Format(mFormatProvider, " {0:F} {1:F} {2:F} {3:F} K", c, m, y, k);
			#if (CONTENT_VALIDATION)
			Validate("K");
			#endif
		}
		/// <summary>
		/// Set CMYK color for nonstroking operations
		/// </summary>
		/// <param name="c">level of cyan color</param>
		/// <param name="m">level of magenta color</param>
		/// <param name="y">level of yellow color</param>
		/// <param name="k">level of black color</param>
		public void SetCMYKNonStrokeColor(double c, double m, double y, double k) 
		{
			mContents += String.Format(mFormatProvider, " {0:F} {1:F} {2:F} {3:F} k", c, m, y, k);
			#if (CONTENT_VALIDATION)
			Validate("k");
			#endif
		}
		/// <summary>
		/// Concatenate matrix to current transformation matrix
		/// </summary>
		/// <param name="a">transformation matrix parameter</param>
		/// <param name="b">transformation matrix parameter</param>
		/// <param name="c">transformation matrix parameter</param>
		/// <param name="d">transformation matrix parameter</param>
		/// <param name="h">transformation matrix parameter</param>
		/// <param name="v">transformation matrix parameter</param>
		public void Transform(double a, double b, double c, double d, double h, double v) 
		{
			mContents += String.Format(mFormatProvider, " {0:F} {1:F} {2:F} {3:F} {4:F} {5:F} cm", a, b, c, d, h, v);
			#if (CONTENT_VALIDATION)
			Validate("cm");
			#endif
		}
		/// <summary>
		/// Append arc segment to path
		/// </summary>
		/// <param name="start">Start angle of the arc in degrees</param>
		/// <param name="end">End angle of the arc in degrees</param>
		/// <param name="cx">Horizontal center of the arc</param>
		/// <param name="cy">Vertical center of the arc</param>
		/// <param name="rx">Horizontal radius of the arc</param>
		/// <param name="ry">Vertical radius of the arc</param>
		/// <param name="angle">Rotate angle</param>
		/// <param name="inMove">If true, move to the first point of the arc</param>
		public void Arc(double start, double end, double cx, double cy, double rx, double ry, double angle, bool inMove) 
		{
			angle = angle * Math.PI / 180;
			start = start * Math.PI / 180;
			end = end * Math.PI / 180;
			// for efficiency reasons we should probably suppress calculation of bounds rect here
			const int theN = 8;
			const int theNum = (theN * 3) - 2; // start and end only have one 	control point

			double delta = (end - start) / (theN - 1);
			double kv = 4 * (1 - Math.Cos(delta / 2)) / (3 * Math.Sin(delta / 2));
			double[] x = new double[theNum];
			double[] y = new double[theNum];
			double ca = 0, sa = 0, tx = 0, ty = 0;
			int i = 0, n = 0;

			// make points
			for (i = 0; i < theN; i++) 
			{
				// establish point
				n = i * 3;
				ca = Math.Cos((i * delta) + start);
				sa = Math.Sin((i * delta) + start);
				x[n] = rx * ca;
				y[n] = ry * sa;
				sa = kv * rx * sa;
				ca = kv * ry * ca;
				// establish control points
				if ((n + 1) < theNum) x[n + 1] = x[n] - sa;
				if ((n + 1) < theNum) y[n + 1] = y[n] + ca;
				if (n > 0) x[n - 1] = x[n] + sa;
				if (n > 0) y[n - 1] = y[n] - ca;
			}
			// translate and rotate
			ca = Math.Cos(angle);
			sa = Math.Sin(angle);
			for (i = 0; i < theNum; i++) 
			{
				tx = (x[i] * ca) - (y[i] * sa) + cx;
				ty = (x[i] * sa) + (y[i] * ca) + cy;
				x[i] = tx; y[i] = ty;
			}
			// draw ellipse
			if (inMove) Move(x[0], y[0]);
			for (i = 0; i < (theN - 1); i++) 
			{
				n = i * 3;
				Bezier(x[n + 1], y[n + 1], x[n + 2], y[n + 2], x[n + 3], y[n + 3]);
			}
			#if (CONTENT_VALIDATION)
			Validate("c");
			#endif
		}
		/// <summary>
		/// Append pdf content
		/// </summary>
		/// <param name="theContent"></param>
		public void AddContent(PDFContent theContent)
		{
			mContents += theContent.mContents;
		}
		/// <summary>
		/// Add graphic state dictionaries to the page resources
		/// </summary>
		/// <param name="enumerable">Graphic state dictionaries</param>
		private void WriteGStates(IEnumerable enumerable)
		{
			IEnumerator theEnumerator = enumerable.GetEnumerator();
			while ( theEnumerator.MoveNext() )
			{
				ExtGState gstate;
				if (enumerable is Hashtable)
					gstate = ((DictionaryEntry)theEnumerator.Current).Value as ExtGState;
				else
					gstate = theEnumerator.Current as ExtGState;

				int theID = mDoc.AddObject(gstate.PdfObject);
				string theRef = theID.ToString() + " 0 R";

				mDoc.SetInfo(mDoc.Page, "/Resources/ExtGState/" + gstate.Name, theRef);
			}
		}
		/// <summary>
		/// Write content to the doc
		/// </summary>
		public void AddToDoc()
		{
			if (mDoc.PageCount == 0)
				mDoc.Page = mDoc.AddPage();

			WriteGStates(mNonStrokeExtGStates);
			WriteGStates(mStrokeExtGStates);
			WriteGStates(mBlendModeExtGStates);
			WriteGStates(mArbitraryExtGStates);

			mDoc.SetInfo(mDoc.FrameRect(), "stream", mContents);

			#if (CONTENT_VALIDATION)
			if (mErrors.Count > 0)
			{
				string errors = "The content contains following errors:";
				int errorsToShow = (mErrors.Count > 20)? 20: mErrors.Count;
				for (int i = 0; i< errorsToShow; i++)
					errors += "\n" + (mErrors[i] as string);
				if (errorsToShow < mErrors.Count)
					errors += "\n...";

				MessageBox.Show (errors, "PDFContent", 
					MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
			#endif
		}
		/// <summary>
		/// Extended graphic state
		/// </summary>
		private class ExtGState
		{
			public string Name = "";
			public string PdfObject = "";
		}
		/// <summary>
		/// Set nonstroking alpha constant
		/// </summary>
		/// <param name="ca">Nonstroking alpha constant</param>
		public void SetNonStrokeAlpha(double ca)
		{
			string val = ca.ToString(mFormatProvider);
			ca = Double.Parse(val, mFormatProvider);

			if ( mNonStrokeExtGStates.ContainsKey(ca) )
			{
				ExtGState gstate = mNonStrokeExtGStates[ca] as ExtGState;
				mContents += " /" + gstate.Name + " gs";
			}
			else
			{
				ExtGState gstate = new ExtGState();
				gstate.Name = GetNextGSName();
				gstate.PdfObject = "<< /Type/ExtGState\n /ca "+ val + "\n >>";
				mNonStrokeExtGStates.Add(ca, gstate);
				mContents += " /" + gstate.Name + " gs";
			}
			#if (CONTENT_VALIDATION)
			Validate("gs");
			#endif
		}
		/// <summary>
		/// Set stroking alpha constant
		/// </summary>
		/// <param name="CA">Stroking alpha constant</param>
		public void SetStrokeAlpha(double CA)
		{
			string val = CA.ToString(mFormatProvider);
			CA = Double.Parse(val, mFormatProvider);

			if ( mNonStrokeExtGStates.ContainsKey(CA) )
			{
				ExtGState gstate = mStrokeExtGStates[CA] as ExtGState;
				mContents += " /" + gstate.Name + " gs";
			}
			else
			{
				ExtGState gstate = new ExtGState();
				gstate.Name = GetNextGSName();
				gstate.PdfObject = "<< /Type/ExtGState\n /CA "+ val + "\n >>";
				mStrokeExtGStates.Add(CA, gstate);
				mContents += " /" + gstate.Name + " gs";
			}
			#if (CONTENT_VALIDATION)
			Validate("gs");
			#endif
		}
		/// <summary>
		/// Set graphic state
		/// </summary>
		/// <param name="pdfFormattedDict">Graphic State dictionary in pdf native format</param>
		public void SetGraphicState(string pdfFormattedDict)
		{
			ExtGState gstate = new ExtGState();
			gstate.Name = GetNextGSName();
			gstate.PdfObject = pdfFormattedDict;
			mArbitraryExtGStates.Add(gstate);
			mContents += " /" + gstate.Name + " gs";
			#if (CONTENT_VALIDATION)
			Validate("gs");
			#endif
		}
		/// <summary>
		/// Set blend mode
		/// </summary>
		/// <param name="blendMode">Blend mode</param>
		public void SetBlendMode(string blendMode)
		{
			if ( mBlendModeExtGStates.ContainsKey(blendMode) )
			{
				ExtGState gstate = mBlendModeExtGStates[blendMode] as ExtGState;
				mContents += " /" + gstate.Name + " gs";
			}
			else
			{
				string name = GetNextGSName();

				ExtGState gstate = new ExtGState();
				gstate.Name = name;
				gstate.PdfObject = "<< /Type/ExtGState\n /BM /"+ blendMode + "\n >>";
				mBlendModeExtGStates.Add(blendMode, gstate);
				mContents += " /" + gstate.Name + " gs";
			}
			#if (CONTENT_VALIDATION)
			Validate("gs");
			#endif
		}
        /// <summary>
        /// Get next available name for gs pdf command
        /// </summary>
        /// <returns>Name of the gs command</returns>
		private string GetNextGSName()
		{
			for (int i = mGSNumber; ; i++)
			{
				string gsName = "GS" + i.ToString();
				string test = mDoc.GetInfo(mDoc.Page, "/Resources/ExtGState/" + gsName);
				if (test == "")
				{
					mGSNumber = i + 1;
					return gsName;
				}
			}
		}
		/// <summary>
		/// Set rendering intent
		/// </summary>
		/// <param name="intent">Rendering intent</param>
		public void SetRenderingIntent(string intent)
		{
			mContents += " /" + intent + " ri";
		}
		/// <summary>
		/// Add image to the pdf content.
		/// </summary>
		/// <param name="imageID">ID of the image returned by Doc.AddImage method</param>
		public void DoImage(int imageID)
		{
			imageID = int.Parse(mDoc.GetInfo( imageID , "XObject"));

			string xObjectStr = mDoc.GetInfo(mDoc.Page, "/Resources/XObject");
			string doImageCommand = "";
			Atom xObjectAtom = Atom.FromString(xObjectStr);
			DictAtom xObjectDict = xObjectAtom as DictAtom;
			if (xObjectAtom is DictAtom)
			{
				IDictionaryEnumerator it = xObjectDict.GetEnumerator();
				while (it.MoveNext())
				{
					if ((it.Value is RefAtom) && ((it.Value as RefAtom).ID == imageID))
					{
						doImageCommand = it.Key.ToString();
						break;
					}
				}
			}
			if ( doImageCommand != "")
				mContents += " /"+doImageCommand + " Do";
		}
		/// <summary>
		/// Begin text object
		/// </summary>
		public void BeginText()
		{
			mContents += " BT";
			#if (CONTENT_VALIDATION)
			Validate("BT");
			#endif
		}
		/// <summary>
		/// End text object
		/// </summary>
		public void EndText()
		{
			mContents += " ET";
			#if (CONTENT_VALIDATION)
			Validate("ET");
			#endif
		}
		/// <summary>
		/// Text rendering mode
		/// </summary>
		public enum TextRenderingMode
		{
			FillText,
			StrokeText,
			FillThenStrokeText,
			Invisible,
			FillTextAndAddForClipping,
			StrokeTextAndAddForClipping,
			FillThenStrokeTextAndAddForClipping,
			AddForClipping
		}
		/// <summary>
		/// Set text rendering mode
		/// </summary>
		/// <param name="mode">Text rendering mode</param>
		public void SetTextRenderingMode(TextRenderingMode mode)
		{
			mContents += String.Format(" {0:D} Tr", mode);
			#if (CONTENT_VALIDATION)
			Validate("Tr");
			#endif
		}
		/// <summary>
		/// Set character spacing
		/// </summary>
		/// <param name="tc">Character spacing</param>
		public void SetCharacterSpacing(double tc)
		{
			mContents += String.Format(mFormatProvider, " {0:F} Tc", tc);
			#if (CONTENT_VALIDATION)
			Validate("Tc");
			#endif
		}
		/// <summary>
		/// Set Word Spacing
		/// </summary>
		/// <param name="tw">Word spacing</param>
		public void SetWordSpacing(double tw)
		{
			mContents += String.Format(mFormatProvider, " {0:F} Tw", tw);
			#if (CONTENT_VALIDATION)
			Validate("Tw");
			#endif
		}
		/// <summary>
		/// Set horizontal text scaling
		/// </summary>
		/// <param name="th">Horizontal text scaling</param>
		public void SetHorizontalScaling(double th)
		{
			mContents += String.Format(mFormatProvider, " {0:F} Tz", th);
			#if (CONTENT_VALIDATION)
			Validate("Tz");
			#endif
		}
		/// <summary>
		/// Set text leading
		/// </summary>
		/// <param name="tl">Text leading</param>
		public void SetTextLeading(double tl)
		{
			mContents += String.Format(mFormatProvider, " {0:F} TL", tl);
			#if (CONTENT_VALIDATION)
			Validate("TL");
			#endif
		}
		/// <summary>
		/// Set text rise
		/// </summary>
		/// <param name="ts">Text rise</param>
		public void SetTextRise(double ts)
		{
			mContents += String.Format(mFormatProvider, " {0:F} Ts", ts);
			#if (CONTENT_VALIDATION)
			Validate("Ts");
			#endif
		}
		/// <summary>
		/// Show text
		/// </summary>
		/// <param name="text">Text string</param>
		public void ShowTextString(string text)
		{
			mContents += string.Format(" ({0}) Tj", text);
			#if (CONTENT_VALIDATION)
			Validate("Tj");
			#endif
		}
		/// <summary>
		/// Set text font and size
		/// </summary>
		/// <param name="FontObectID">Font object ID</param>
		/// <param name="fontSize">Font size</param>
		public void SetFont( int FontObectID, int fontSize )
		{
			string fontStr = mDoc.GetInfo(mDoc.Page, "/Resources/Font");
			string fontCommand = "";
			Atom fontAtom = Atom.FromString(fontStr);
			DictAtom fontDict = fontAtom as DictAtom;
			if (fontAtom is DictAtom)
			{
				IDictionaryEnumerator it = fontDict.GetEnumerator();
				while (it.MoveNext())
				{
					if ((it.Value is RefAtom) && ((it.Value as RefAtom).ID == FontObectID))
					{
						fontCommand = it.Key.ToString();
						break;
					}
				}
			}
			if (fontCommand == "")
			{
				if ( !(fontAtom is DictAtom) )
					fontDict = new DictAtom();

				Atom fontRef = Atom.FromString(FontObectID.ToString()+" 0 R");
				fontCommand = "Fabc"+FontObectID.ToString();
				fontDict.Add(fontCommand, fontRef);
				mDoc.SetInfo(mDoc.Page, "/Resources/Font", fontDict.ToString());
			}
			if ( fontCommand != "" )
			{
				mContents += string.Format(" /{0} {1} Tf", fontCommand, fontSize);
				#if (CONTENT_VALIDATION)
				Validate("Tf");
				#endif
			}
		}
		/// <summary>
		/// Set text matrix and text line matrix
		/// </summary>
		/// <param name="a">Text matrix parameter</param>
		/// <param name="b">Text matrix parameter</param>
		/// <param name="c">Text matrix parameter</param>
		/// <param name="d">Text matrix parameter</param>
		/// <param name="e">Text matrix parameter</param>
		/// <param name="f">Text matrix parameter</param>
		public void SetTextMatrix(double a, double b, double c, double d, double e, double f) 
		{
			mContents += String.Format(mFormatProvider, " {0:F} {1:F} {2:F} {3:F} {4:F} {5:F} Tm", a, b, c, d, e, f);
			#if (CONTENT_VALIDATION)
			Validate("Tm");
			#endif
		}
		/// <summary>
		/// Move to start of next text line
		/// </summary>
		public void NextLine() 
		{
			mContents += " T*";
			#if (CONTENT_VALIDATION)
			Validate("T*");
			#endif
		}
		/// <summary>
		/// Move text position
		/// </summary>
		/// <param name="tx">x offset from the start of the current line</param>
		/// <param name="ty">y offset from the start of the current line</param>
		public void TextMove(double tx, double ty) 
		{
			mContents += String.Format(mFormatProvider, " {0:F} {1:F} Td", tx, ty);
			#if (CONTENT_VALIDATION)
			Validate("Td");
			#endif
		}
		/// <summary>
		/// Last used number for graphic state command (e.g. GS7 for mGSNumber = 7)
		/// </summary>
		private int mGSNumber = 0;
		/// <summary>
		/// Parent doc
		/// </summary>
		private Doc mDoc;
		/// <summary>
		/// Pdf content string
		/// </summary>
		private string mContents = "";
		/// <summary>
		/// Number format for string conversion
		/// </summary>
		private NumberFormatInfo mFormatProvider;
		/// <summary>
		/// Hashtable of used extended graphic states for stroke color transparency
		/// </summary>
		private Hashtable mStrokeExtGStates = new Hashtable();
		/// <summary>
		/// Hashtable of used extended graphic states for nonstroke color transparency
		/// </summary>
		private Hashtable mNonStrokeExtGStates = new Hashtable();
		/// <summary>
		/// Hashtable of used extended graphic states for blending modes
		/// </summary>
		private Hashtable mBlendModeExtGStates = new Hashtable();
		/// <summary>
		/// List of used ExtGStates
		/// </summary>
		private ArrayList mArbitraryExtGStates = new ArrayList();

		#if (CONTENT_VALIDATION)
		/// <summary>
		/// Pdf command validation
		/// </summary>
		/// <param name="command">Pdf command</param>
		private void Validate(string command)
		{
			int generalOperation =  Array.BinarySearch(mGeneralOps, command);
			int textOperation =  Array.BinarySearch(mTextOps, command);

			if (generalOperation > 0 && mBeginTextOpen)
				mErrors.Add("Illegal operation '" + command + "' inside text object");
			else if (textOperation > 0 && !mBeginTextOpen )
				mErrors.Add("Illegal operation '" + command + "' outside text object");
			else
			{
				switch (command)
				{
					case "q":
						mOpenQCount++;
						break;
					case "Q":
						mOpenQCount--;
						if (mOpenQCount < 0)
							mErrors.Add("Invalid restore");
						break;
					case "BT":
						mBeginTextOpen = true;
						break;
					case "ET":
						mBeginTextOpen = false;
						break;
				}
			}
		}
		/// <summary>
		/// Array of pdf commands
		/// </summary>
		string[] mGeneralOps = {"b", "B", "b*", "B*", "c", "d", "f",
								  "F", "f*", "g", "G", "h", "i", "j", "J", "k", "l", "m",
								  "M", "n", "re", "rg", "RG", "ri", "s", "S", "v", "w", "W", "W*",
								  "y"};
		/// <summary>
		/// Array of pdf commands used for text output
		/// </summary>
		string[] mTextOps = {"T*", "Tc", "Td", "TD", "Tf", "Tj", "TJ", "TL",
							   "Tm", "Tr", "Ts", "Tw", "Tz"};

		/// <summary>
		/// Number of open "q" operators
		/// </summary>
		int mOpenQCount = 0;
		/// <summary>
		/// True if Begin text operator wasn't closed by End text operator
		/// </summary>
		bool mBeginTextOpen = false;
		/// <summary>
		/// List of errors in pdf content
		/// </summary>
		ArrayList mErrors = new ArrayList();
		#endif

	}
}
