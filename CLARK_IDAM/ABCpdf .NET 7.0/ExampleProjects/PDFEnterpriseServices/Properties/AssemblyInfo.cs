﻿// ===========================================================================
//	©2006 WebSupergoo. All rights reserved.
// ===========================================================================

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("PdfEnterpriseServices")]
[assembly: AssemblyDescription("Demonstration of using .NET Enterprise Services with ABCpdf")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("WebSupergoo")]
[assembly: AssemblyProduct("PdfEnterpriseServices")]
[assembly: AssemblyCopyright("Copyright © WebSupergoo 2006")]
[assembly: AssemblyTrademark("WebSupergoo")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("719b52ef-01b1-478d-805d-6b9edca4f749")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
