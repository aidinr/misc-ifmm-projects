// ===========================================================================
//	�2006 WebSupergoo. All rights reserved.
// ===========================================================================

#if DEBUG
#define CONTENT_VALIDATION
#endif

using System;
using System.Globalization;
using WebSupergoo.ABCpdf7;
using WebSupergoo.ABCpdf7.Atoms;
using WebSupergoo.ABCpdf7.Objects;
using System.Collections;
using System.Windows.Forms;


namespace WebSupergoo.ABCpdf7.Drawing {
	#region PDFContent
	/// <summary>
	/// PDFContent class is a simple constructor of the pdf content.
	/// It provides easy access to the most of the commands described in the PDF Reference
	/// </summary>
	internal class PDFContent {
		#region Constructors
		internal PDFContent(Doc theDoc) {
			mDoc = theDoc;
			mFormatProvider = new NumberFormatInfo();
			mFormatProvider.NumberDecimalSeparator = ".";
			mFormatProvider.NumberDecimalDigits = 5;
		}
		#endregion

		#region PDF content methods
		/// <summary>
		/// Begin new subpath
		/// </summary>
		/// <param name="x"> x coordinate</param>
		/// <param name="y"> y coordinate</param>
		internal void Move(double x, double y) {
			AppendParameter(x);
			AppendParameter(y);
			mContents.Append(" m");
#if (CONTENT_VALIDATION)
			Validate("m");
#endif
		}
		/// <summary>
		/// Append straight line segment to path
		/// </summary>
		/// <param name="x"> x coordinate </param>
		/// <param name="y"> y coordinate </param>
		internal void Line(double x, double y) {	
			AppendParameter(x);
			AppendParameter(y);
			mContents.Append(" l");
#if (CONTENT_VALIDATION)
			Validate("l");
#endif
		}
		/// <summary>
		/// Append rectangle to path.
		/// </summary>
		/// <param name="x"> Bottom left x coordinate </param>
		/// <param name="y"> Bottom left y coordinate </param>
		/// <param name="w">width</param>
		/// <param name="h">height</param>
		internal void Rect(double x, double y, double w, double h) {
			AppendParameter(x);
			AppendParameter(y);
			AppendParameter(w);
			AppendParameter(h);
			mContents.Append(" re");
#if (CONTENT_VALIDATION)
			Validate("re");
#endif
		}
		/// <summary>
		/// Append curved segment to path (three control points)
		/// </summary>
		/// <param name="x1">x coordinate (1st control point)</param>
		/// <param name="y1">y coordinate (1st control point)</param>
		/// <param name="x2">x coordinate (2nd control point)</param>
		/// <param name="y2">y coordinate (2nd control point)</param>
		/// <param name="x3">x coordinate (3rd control point)</param>
		/// <param name="y3">y coordinate (3rd control point)</param>
		internal void Bezier(double x1, double y1, double x2, double y2, double x3, double y3) {
			AppendParameter(x1);
			AppendParameter(y1);
			AppendParameter(x2);
			AppendParameter(y2);
			AppendParameter(x3);
			AppendParameter(y3);
			mContents.Append(" c");
#if (CONTENT_VALIDATION)
			Validate("c");
#endif
		}
		/// <summary>
		/// Close subpath
		/// </summary>
		internal void Close() {
			mContents.Append(" h");
		}
		/// <summary>
		/// Stroke path
		/// </summary>
		internal void Stroke() {
			mContents.Append(" S");
#if (CONTENT_VALIDATION)
			Validate("S");
#endif

		}
		/// <summary>
		/// Fill path using nonzero winding number rule
		/// </summary>
		internal void Fill() {
			mContents.Append(" f");
#if (CONTENT_VALIDATION)
			Validate("f");
#endif
		}
		/// <summary>
		/// Fill path using even-odd rule
		/// </summary>
		internal void FillEvenOddRule() {
			mContents.Append(" f*");
#if (CONTENT_VALIDATION)
			Validate("f");
#endif
		}
		/// <summary>
		/// Set clipping path using nonzero winding number rule
		/// </summary>
		internal void Clip() {
			mContents.Append(" W n");
#if (CONTENT_VALIDATION)
			Validate("W");
#endif
		}
		/// <summary>
		/// Set clipping path using even-odd rule
		/// </summary>
		internal void ClipEvenOddRule() {
			mContents.Append(" W* n");
#if (CONTENT_VALIDATION)
			Validate("W");
#endif
		}
		/// <summary>
		/// Save graphics state
		/// </summary>
		internal void SaveState() {
			mContents.Append(" q");
#if (CONTENT_VALIDATION)
			Validate("q");
#endif
		}
		/// <summary>
		/// Restore graphics state
		/// </summary>
		internal void RestoreState() {
			mContents.Append(" Q");
#if (CONTENT_VALIDATION)
			Validate("Q");
#endif
		}
		/// <summary>
		/// Set line width
		/// </summary>
		/// <param name="v">Line width</param>
		internal void SetLineWidth(double v) {
			AppendParameter(v);
			mContents.Append(" w");
#if (CONTENT_VALIDATION)
			Validate("w");
#endif
		}
		/// <summary>
		/// The line cap for the ends of any lines to be stroked
		/// </summary>
		internal enum LineCap {
			Butt,
			Round,
			ProjectingSquare
		}
		/// <summary>
		/// Set line cap style
		/// </summary>
		/// <param name="v">Line cap</param>
		internal void SetLineCap(int v) {
			AppendParameter(v);
			mContents.Append(" J");
#if (CONTENT_VALIDATION)
			Validate("J");
#endif
		}
		/// <summary>
		/// The line join for the shape of joints between connected segments of a path
		/// </summary>
		internal enum LineJoin {
			Miter,
			Round,
			Bevel
		}
		/// <summary>
		/// Set line join style
		/// </summary>
		/// <param name="v">Line join style</param>
		internal void SetLineJoin(int v) {
			AppendParameter(v);
			mContents.Append(" j");
#if (CONTENT_VALIDATION)
			Validate("j");
#endif
		}
		/// <summary>
		/// Set miter limit
		/// </summary>
		/// <param name="v">Miter limit</param>
		internal void SetMiterLimit(double v) {
			AppendParameter(v);
			mContents.Append(" M");
#if (CONTENT_VALIDATION)
			Validate("M");
#endif
		}
		/// <summary>
		/// Set line dash pattern
		/// </summary>
		/// <param name="dashPattern">Dash pattern</param>
		internal void LineDash( string dashPattern) {
			mContents.Append(" ").Append(dashPattern).Append(" d");
#if (CONTENT_VALIDATION)
			Validate("d");
#endif
		}
		/// <summary>
		/// Set gray level for stroking operations
		/// </summary>
		/// <param name="w">Level of gray color</param>
		internal void SetGrayStrokeColor(double w) {
			AppendParameter(w);
			mContents.Append(" G");
#if (CONTENT_VALIDATION)
			Validate("G");
#endif
		}
		/// <summary>
		/// Set gray level for nonstroking operations
		/// </summary>
		/// <param name="w">Level of gray color</param>
		internal void SetGrayNonStrokeColor(double w) {
			AppendParameter(w);
			mContents.Append(" g");
#if (CONTENT_VALIDATION)
			Validate("g");
#endif
		}
		/// <summary>
		/// Set RGB color for stroking operations
		/// </summary>
		/// <param name="r">Level of red color</param>
		/// <param name="g">Level of green color </param>
		/// <param name="b">Level of blue color</param>
		internal void SetRGBStrokeColor(double r, double g, double b) {
			AppendParameter(r);
			AppendParameter(g);
			AppendParameter(b);
			mContents.Append(" RG");
#if (CONTENT_VALIDATION)
			Validate("RG");
#endif
		}
		/// <summary>
		/// Set RGB color for nonstroking operations
		/// </summary>
		/// <param name="r">Level of red color</param>
		/// <param name="g">Level of green color </param>
		/// <param name="b">Level of blue color</param>
		internal void SetRGBNonStrokeColor(double r, double g, double b) {
			AppendParameter(r);
			AppendParameter(g);
			AppendParameter(b);
			mContents.Append(" rg");
#if (CONTENT_VALIDATION)
			Validate("rg");
#endif
		}
		/// <summary>
		/// Set CMYK color for stroking operations
		/// </summary>
		/// <param name="c">Level of cyan color</param>
		/// <param name="m">Level of magenta color</param>
		/// <param name="y">Level of yellow color</param>
		/// <param name="k">Level of black color</param>
		internal void SetCMYKStrokeColor(double c, double m, double y, double k) {
			AppendParameter(c);
			AppendParameter(m);
			AppendParameter(y);
			AppendParameter(k);
			mContents.Append(" K");
#if (CONTENT_VALIDATION)
			Validate("K");
#endif
		}
		/// <summary>
		/// Set CMYK color for nonstroking operations
		/// </summary>
		/// <param name="c">Level of cyan color</param>
		/// <param name="m">Level of magenta color</param>
		/// <param name="y">Level of yellow color</param>
		/// <param name="k">Level of black color</param>
		internal void SetCMYKNonStrokeColor(double c, double m, double y, double k) {
			AppendParameter(c);
			AppendParameter(m);
			AppendParameter(y);
			AppendParameter(k);
			mContents.Append(" k");
#if (CONTENT_VALIDATION)
			Validate("k");
#endif
		}
		/// <summary>
		/// Concatenate matrix to current transformation matrix
		/// </summary>
		/// <param name="a">transformation matrix parameter</param>
		/// <param name="b">transformation matrix parameter</param>
		/// <param name="c">transformation matrix parameter</param>
		/// <param name="d">transformation matrix parameter</param>
		/// <param name="h">transformation matrix parameter</param>
		/// <param name="v">transformation matrix parameter</param>
		internal void Transform(double a, double b, double c, double d, double h, double v) {
			AppendParameter(a);
			AppendParameter(b);
			AppendParameter(c);
			AppendParameter(d);
			AppendParameter(h);
			AppendParameter(v);
			mContents.Append(" cm");
#if (CONTENT_VALIDATION)
			Validate("cm");
#endif
		}
		/// <summary>
		/// Append arc segment to path
		/// </summary>
		/// <param name="start">Start angle of the arc in degrees</param>
		/// <param name="end">End angle of the arc in degrees</param>
		/// <param name="cx">Horizontal center of the arc</param>
		/// <param name="cy">Vertical center of the arc</param>
		/// <param name="rx">Horizontal radius of the arc</param>
		/// <param name="ry">Vertical radius of the arc</param>
		/// <param name="angle">Rotate angle</param>
		/// <param name="inMove">If true, move to the first point of the arc</param>
		internal void Arc(double start, double end, double cx, double cy, double rx, double ry, double angle, bool inMove) {
			angle = angle * Math.PI / 180;
			start = start * Math.PI / 180;
			end = end * Math.PI / 180;
			// for efficiency reasons it might be a good idea to suppress the calculation of the bounds rect here
			const int theN = 8;
			const int theNum = (theN * 3) - 2; // start and end only have one 	control point

			double delta = (end - start) / (theN - 1);
			double kv = 4 * (1 - Math.Cos(delta / 2)) / (3 * Math.Sin(delta / 2));
			double[] x = new double[theNum];
			double[] y = new double[theNum];
			double ca = 0, sa = 0, tx = 0, ty = 0;
			int i = 0, n = 0;

			// make points
			for (i = 0; i < theN; i++) {
				// establish point
				n = i * 3;
				ca = Math.Cos((i * delta) + start);
				sa = Math.Sin((i * delta) + start);
				x[n] = rx * ca;
				y[n] = ry * sa;
				sa = kv * rx * sa;
				ca = kv * ry * ca;
				// establish control points
				if ((n + 1) < theNum) x[n + 1] = x[n] - sa;
				if ((n + 1) < theNum) y[n + 1] = y[n] + ca;
				if (n > 0) x[n - 1] = x[n] + sa;
				if (n > 0) y[n - 1] = y[n] - ca;
			}
			// translate and rotate
			ca = Math.Cos(angle);
			sa = Math.Sin(angle);
			for (i = 0; i < theNum; i++) {
				tx = (x[i] * ca) - (y[i] * sa) + cx;
				ty = (x[i] * sa) + (y[i] * ca) + cy;
				x[i] = tx; y[i] = ty;
			}
			// draw ellipse
			if (inMove) Move(x[0], y[0]);
			for (i = 0; i < (theN - 1); i++) {
				n = i * 3;
				Bezier(x[n + 1], y[n + 1], x[n + 2], y[n + 2], x[n + 3], y[n + 3]);
			}
#if (CONTENT_VALIDATION)
			Validate("c");
#endif
		}
		/// <summary>
		/// Append pdf content
		/// </summary>
		/// <param name="theContent"></param>
		internal void AddContent(PDFContent theContent) {
			mContents.Append(theContent.mContents.ToString());
		}
		/// <summary>
		/// Add graphic state dictionaries to the page resources
		/// </summary>
		/// <param name="enumerable">Graphic state dictionaries</param>
		private void WriteGStates(IEnumerable enumerable) {
			IEnumerator theEnumerator = enumerable.GetEnumerator();
			while ( theEnumerator.MoveNext() ) {
				ExtGState gstate;
				if (enumerable is Hashtable)
					gstate = ((DictionaryEntry)theEnumerator.Current).Value as ExtGState;
				else
					gstate = theEnumerator.Current as ExtGState;

				int theID = mDoc.AddObject(gstate.PdfObject);
				string theRef = theID.ToString() + " 0 R";

				mDoc.SetInfo(mDoc.Page, "/Resources/ExtGState/" + gstate.Name, theRef);
			}
		}
		/// <summary>
		/// Add ImageXObject dictionaries to the page resources
		/// </summary>
		/// <param name="enumerable">Image dictionaries</param>
		private void WriteXObjects(IEnumerable enumerable) {
			IEnumerator theEnumerator = enumerable.GetEnumerator();
			while ( theEnumerator.MoveNext() ) {
				ImageXObject xobject;
				if (enumerable is Hashtable)
					xobject = ((DictionaryEntry)theEnumerator.Current).Value as ImageXObject;
				else
					xobject = theEnumerator.Current as ImageXObject;

				StreamObject sobject = (StreamObject)StreamObject.FromString(xobject.PdfObject);
				mDoc.ObjectSoup.Add(sobject);
				sobject.ClearData();
				sobject.SetData(xobject.Data);
				mDoc.SetInfo(mDoc.Page, "/Resources/XObject/" + xobject.Name, string.Format("{0} 0 R", sobject.ID));
				mDoc.SetInfo(sobject.ID, "/Filter", "/DCTDecode");
			}
		}
		
		/// <summary>
		/// Write content to the doc
		/// </summary>
		internal void AddToDoc() {
			if (mDoc.PageCount == 0)
				mDoc.Page = mDoc.AddPage();

			WriteGStates(mNonStrokeExtGStates);
			WriteGStates(mStrokeExtGStates);
			WriteGStates(mBlendModeExtGStates);
			WriteGStates(mArbitraryExtGStates);
			WriteXObjects(mImageXObjects);

			mDoc.SetInfo(mDoc.FrameRect(), "stream", mContents.ToString());
			
#if (CONTENT_VALIDATION)
			if (mErrors.Count > 0) {
				string errors = "The content contains following errors:";
				int errorsToShow = (mErrors.Count > 20)? 20: mErrors.Count;
				for (int i = 0; i< errorsToShow; i++)
					errors += "\n" + (mErrors[i] as string);
				if (errorsToShow < mErrors.Count)
					errors += "\n...";

				MessageBox.Show (errors, "PDFContent", 
					MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
#endif
		}

		#region ExtGState
		/// <summary>
		/// Extended graphic state
		/// </summary>
		private class ExtGState {
			internal string Name = "";
			internal string PdfObject = "";
		}
		#endregion
		/// <summary>
		/// Image XObject
		/// </summary>
		private class ImageXObject {
			internal string Name = "";
			internal string PdfObject = "";
			internal byte[] Data = null;
		}

		/// <summary>
		/// Set nonstroking alpha constant
		/// </summary>
		/// <param name="ca">Nonstroking alpha constant</param>
		internal void SetNonStrokeAlpha(double ca) {
			string val = ca.ToString(mFormatProvider);
			ca = Double.Parse(val, mFormatProvider);

			if ( mNonStrokeExtGStates.ContainsKey(ca) ) {
				ExtGState gstate = mNonStrokeExtGStates[ca] as ExtGState;
				mContents.Append(" /").Append(gstate.Name).Append(" gs");
			}
			else {
				ExtGState gstate = new ExtGState();
				gstate.Name = GetNextGSName();
				gstate.PdfObject = "<< /Type/ExtGState\n /ca "+ val + "\n >>";
				mNonStrokeExtGStates.Add(ca, gstate);
				mContents.Append(" /").Append(gstate.Name).Append(" gs");
			}
#if (CONTENT_VALIDATION)
			Validate("gs");
#endif
		}
		/// <summary>
		/// Set stroking alpha constant
		/// </summary>
		/// <param name="CA">Stroking alpha constant</param>
		internal void SetStrokeAlpha(double CA) {
			string val = CA.ToString(mFormatProvider);
			CA = Double.Parse(val, mFormatProvider);

			if ( mStrokeExtGStates.ContainsKey(CA) ) {
				ExtGState gstate = mStrokeExtGStates[CA] as ExtGState;
				mContents.Append(" /").Append(gstate.Name).Append(" gs");
			}
			else {
				ExtGState gstate = new ExtGState();
				gstate.Name = GetNextGSName();
				gstate.PdfObject = "<< /Type/ExtGState\n /CA "+ val + "\n >>";
				mStrokeExtGStates.Add(CA, gstate);
				mContents.Append(" /").Append(gstate.Name).Append(" gs");
			}
#if (CONTENT_VALIDATION)
			Validate("gs");
#endif
		}
		/// <summary>
		/// Set graphic state
		/// </summary>
		/// <param name="pdfFormattedDict">Graphic State dictionary in pdf native format</param>
		internal void SetGraphicState(string pdfFormattedDict) {
			ExtGState gstate = new ExtGState();
			gstate.Name = GetNextGSName();
			gstate.PdfObject = pdfFormattedDict;
			mArbitraryExtGStates.Add(gstate);
			mContents.Append(" /").Append(gstate.Name).Append(" gs");
#if (CONTENT_VALIDATION)
			Validate("gs");
#endif
		}
		/// <summary>
		/// Set blend mode
		/// </summary>
		/// <param name="blendMode">Blend mode</param>
		internal void SetBlendMode(string blendMode) {
			if ( mBlendModeExtGStates.ContainsKey(blendMode) ) {
				ExtGState gstate = mBlendModeExtGStates[blendMode] as ExtGState;
				mContents.Append(" /").Append(gstate.Name).Append(" gs");
			}
			else {
				string name = GetNextGSName();

				ExtGState gstate = new ExtGState();
				gstate.Name = name;
				gstate.PdfObject = "<< /Type/ExtGState\n /BM /"+ blendMode + "\n >>";
				mBlendModeExtGStates.Add(blendMode, gstate);
				mContents.Append(" /").Append(gstate.Name).Append(" gs");
			}
#if (CONTENT_VALIDATION)
			Validate("gs");
#endif
		}
		/// <summary>
		/// Get next available name for Do pdf command
		/// </summary>
		/// <returns>Name of the gs command</returns>
		private string GetNextXObjectName() {
			for (int i = mXObjectNumber; ; i++) {
				string objName = "Iabc" + i.ToString();
				string test = mDoc.GetInfo(mDoc.Page, "/Resources/ImageXObject/" + objName);
				if (test == "") {
					mXObjectNumber = i + 1;
					return objName;
				}
			}
		}
		/// <summary>
		/// Get next available name for gs pdf command
		/// </summary>
		/// <returns>Name of the gs command</returns>
		private string GetNextGSName() {
			for (int i = mGSNumber; ; i++) {
				string gsName = "GS" + i.ToString();
				string test = mDoc.GetInfo(mDoc.Page, "/Resources/ExtGState/" + gsName);
				if (test == "") {
					mGSNumber = i + 1;
					return gsName;
				}
			}
		}
		/// <summary>
		/// Set rendering intent
		/// </summary>
		/// <param name="intent">Rendering intent</param>
		internal void SetRenderingIntent(string intent) {
			mContents.Append(" /").Append(intent).Append(" ri");
		}
		/// <summary>
		/// Add image to the pdf content.
		/// </summary>
		/// <param name="imageID">ID of the image returned by Doc.AddImage method</param>
		internal void DoImage(int imageID) {
			imageID = int.Parse(mDoc.GetInfo( imageID , "XObject"));

			string xObjectStr = mDoc.GetInfo(mDoc.Page, "/Resources/XObject");
			string doImageCommand = "";
			Atom xObjectAtom = Atom.FromString(xObjectStr);
			DictAtom xObjectDict = xObjectAtom as DictAtom;
			if (xObjectAtom is DictAtom) {
				IDictionaryEnumerator it = xObjectDict.GetEnumerator();
				while (it.MoveNext()) {
					if ((it.Value is RefAtom) && ((it.Value as RefAtom).ID == imageID)) {
						doImageCommand = it.Key.ToString();
						break;
					}
				}
			}
			if ( doImageCommand != "")
				mContents.Append(" /").Append(doImageCommand).Append(" Do");
		}
		/// <summary>
		/// Begin text object
		/// </summary>
		internal void BeginText() {
			mContents.Append(" BT");
#if (CONTENT_VALIDATION)
			Validate("BT");
#endif
		}
		/// <summary>
		/// End text object
		/// </summary>
		internal void EndText() {
			mContents.Append(" ET");
#if (CONTENT_VALIDATION)
			Validate("ET");
#endif
		}
		/// <summary>
		/// Set text rendering mode
		/// </summary>
		/// <param name="mode">Text rendering mode</param>
		internal void SetTextRenderingMode(TextRenderingMode mode) {
			AppendParameter((double)mode);
			mContents.Append(" Tr");
#if (CONTENT_VALIDATION)
			Validate("Tr");
#endif
		}
		/// <summary>
		/// Set character spacing
		/// </summary>
		/// <param name="tc">Character spacing</param>
		internal void SetCharacterSpacing(double tc) {
			AppendParameter(tc);
			mContents.Append(" Tc");
#if (CONTENT_VALIDATION)
			Validate("Tc");
#endif
		}
		/// <summary>
		/// Set Word Spacing
		/// </summary>
		/// <param name="tw">Word spacing</param>
		internal void SetWordSpacing(double tw) {
			AppendParameter(tw);
			mContents.Append(" Tw");
#if (CONTENT_VALIDATION)
			Validate("Tw");
#endif
		}
		/// <summary>
		/// Set horizontal text scaling
		/// </summary>
		/// <param name="th">Horizontal text scaling</param>
		internal void SetHorizontalScaling(double th) {
			AppendParameter(th);
			mContents.Append(" Tz");
#if (CONTENT_VALIDATION)
			Validate("Tz");
#endif
		}
		/// <summary>
		/// Set text leading
		/// </summary>
		/// <param name="tl">Text leading</param>
		internal void SetTextLeading(double tl) {
			AppendParameter(tl);
			mContents.Append(" TL");
#if (CONTENT_VALIDATION)
			Validate("TL");
#endif
		}
		/// <summary>
		/// Set text rise
		/// </summary>
		/// <param name="ts">Text rise</param>
		internal void SetTextRise(double ts) {
			AppendParameter(ts);
			mContents.Append(" Ts");
#if (CONTENT_VALIDATION)
			Validate("Ts");
#endif
		}
		/// <summary>
		/// Show text
		/// </summary>
		/// <param name="text">Text string</param>
		internal void ShowTextString(string text) {
			mContents.AppendFormat(" ({0}) Tj", text);
#if (CONTENT_VALIDATION)
			Validate("Tj");
#endif
		}
		/// <summary>
		/// Set text font and size
		/// </summary>
		/// <param name="FontObectID">Font object ID</param>
		/// <param name="fontSize">Font size</param>
		internal void SetFont( int FontObectID, double fontSize ) {
			string fontStr = mDoc.GetInfo(mDoc.Page, "/Resources/Font");
			string fontCommand = "";
			Atom fontAtom = Atom.FromString(fontStr);
			DictAtom fontDict = fontAtom as DictAtom;
			if (fontAtom is DictAtom) {
				IDictionaryEnumerator it = fontDict.GetEnumerator();
				while (it.MoveNext()) {
					if ((it.Value is RefAtom) && ((it.Value as RefAtom).ID == FontObectID)) {
						fontCommand = it.Key.ToString();
						break;
					}
				}
			}
			if (fontCommand == "") {
				if ( !(fontAtom is DictAtom) )
					fontDict = new DictAtom();

				Atom fontRef = Atom.FromString(FontObectID.ToString()+" 0 R");
				fontCommand = "Fabc"+FontObectID.ToString();
				fontDict.Add(fontCommand, fontRef);
				mDoc.SetInfo(mDoc.Page, "/Resources/Font", fontDict.ToString());
			}
			if ( fontCommand != "" ) {
				mContents.AppendFormat(" /{0} {1} Tf", fontCommand, fontSize);
#if (CONTENT_VALIDATION)
				Validate("Tf");
#endif
			}
		}
		/// <summary>
		/// Set text matrix and text line matrix
		/// </summary>
		/// <param name="a">Text matrix parameter</param>
		/// <param name="b">Text matrix parameter</param>
		/// <param name="c">Text matrix parameter</param>
		/// <param name="d">Text matrix parameter</param>
		/// <param name="e">Text matrix parameter</param>
		/// <param name="f">Text matrix parameter</param>
		internal void SetTextMatrix(double a, double b, double c, double d, double e, double f) {
			AppendParameter(a);
			AppendParameter(b);
			AppendParameter(c);
			AppendParameter(d);
			AppendParameter(e);
			AppendParameter(f);
			mContents.Append(" Tm");
#if (CONTENT_VALIDATION)
			Validate("Tm");
#endif
		}
		/// <summary>
		/// Move to start of next text line
		/// </summary>
		internal void NextLine() {
			mContents.Append(" T*");
#if (CONTENT_VALIDATION)
			Validate("T*");
#endif
		}
		/// <summary>
		/// Move text position
		/// </summary>
		/// <param name="tx">x offset from the start of the current line</param>
		/// <param name="ty">y offset from the start of the current line</param>
		internal void TextMove(double tx, double ty) {
			AppendParameter(tx);
			AppendParameter(ty);
			mContents.Append(" Td");
#if (CONTENT_VALIDATION)
			Validate("Td");
#endif
		}
		
		private void AppendParameter(double v) {
			double iv = (double)System.Decimal.Truncate((decimal)v);
			mContents.AppendFormat(mFormatProvider, (v - iv) != 0 ? " {0:F}" : " {0}", v);
		}
		#endregion
		
		#region Declare variables
		private int mXObjectNumber = 0;
		/// <summary>
		/// Last used number for graphic state command (e.g. GS7 for mGSNumber = 7)
		/// </summary>
		private int mGSNumber = 0;
		/// <summary>
		/// Parent doc
		/// </summary>
		internal Doc mDoc;
		/// <summary>
		/// Pdf content string
		/// </summary>
		//internal string mContents = "";
		internal System.Text.StringBuilder mContents = new System.Text.StringBuilder();
		/// <summary>
		/// Number format for string conversion
		/// </summary>
		private NumberFormatInfo mFormatProvider;
		/// <summary>
		/// Hashtable of used extended graphic states for stroke color transparency
		/// </summary>
		private Hashtable mStrokeExtGStates = new Hashtable();
		/// <summary>
		/// Hashtable of used extended graphic states for nonstroke color transparency
		/// </summary>
		private Hashtable mNonStrokeExtGStates = new Hashtable();
		/// <summary>
		/// Hashtable of used extended graphic states for blending modes
		/// </summary>
		private Hashtable mBlendModeExtGStates = new Hashtable();
		/// <summary>
		/// List of used ExtGStates
		/// </summary>
		private ArrayList mArbitraryExtGStates = new ArrayList();
		/// <summary>
		/// Hashtable of used XObjects for images
		/// </summary>
		private Hashtable mImageXObjects = new Hashtable();

#if (CONTENT_VALIDATION)
		/// <summary>
		/// Pdf command validation
		/// </summary>
		/// <param name="command">Pdf command</param>
		private void Validate(string command) {
			int generalOperation =  Array.BinarySearch(mGeneralOps, command);
			int textOperation =  Array.BinarySearch(mTextOps, command);

			if (generalOperation > 0 && mBeginTextOpen)
				mErrors.Add("Illegal operation '" + command + "' inside text object");
			else if (textOperation > 0 && !mBeginTextOpen )
				mErrors.Add("Illegal operation '" + command + "' outside text object");
			else {
				switch (command) {
					case "q":
						mOpenQCount++;
						break;
					case "Q":
						mOpenQCount--;
						if (mOpenQCount < 0)
							mErrors.Add("Invalid restore");
						break;
					case "BT":
						mBeginTextOpen = true;
						break;
					case "ET":
						mBeginTextOpen = false;
						break;
				}
			}
		}
		/// <summary>
		/// Array of pdf commands
		/// </summary>
		string[] mGeneralOps = {"b", "B", "b*", "B*", "c", "d", "f",
								   "F", "f*", "g", "G", "h", "i", "j", "J", "k", "l", "m",
								   "M", "n", "re", "rg", "RG", "ri", "s", "S", "v", "w", "W", "W*",
								   "y"};
		/// <summary>
		/// Array of pdf commands used for text output
		/// </summary>
		string[] mTextOps = {"T*", "Tc", "Td", "TD", "Tf", "Tj", "TJ", "TL",
								"Tm", "Tr", "Ts", "Tw", "Tz"};

		/// <summary>
		/// Number of open "q" operators
		/// </summary>
		int mOpenQCount = 0;
		/// <summary>
		/// True if Begin text operator wasn't closed by End text operator
		/// </summary>
		bool mBeginTextOpen = false;
		/// <summary>
		/// List of errors in pdf content
		/// </summary>
		ArrayList mErrors = new ArrayList();
#endif
		#endregion
	}
	#endregion
}
