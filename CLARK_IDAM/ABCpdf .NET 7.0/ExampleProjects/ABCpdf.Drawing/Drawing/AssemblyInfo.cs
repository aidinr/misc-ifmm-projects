// ===========================================================================
//	�2006 WebSupergoo. All rights reserved.
// ===========================================================================

using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("ABCpdf.Drawing")]
[assembly: AssemblyDescription("ABCpdf.Drawing for graphics output to PDF")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("WebSupergoo	")]
[assembly: AssemblyProduct("ABCpdf")]
[assembly: AssemblyCopyright("WebSupergoo")]
[assembly: AssemblyTrademark("WebSupergoo")]
[assembly: AssemblyCulture("")]		
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyKeyName("")]
