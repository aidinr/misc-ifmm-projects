// ===========================================================================
//	�2006 WebSupergoo. All rights reserved.
// ===========================================================================

using System;
using System.Collections;

namespace WebSupergoo.ABCpdf7.Drawing {
	#region PageCollection
	/// <summary>
	/// Represents a collection of Page objects for a PDF document.
	/// </summary>
	public sealed class PageCollection : ReadOnlyCollectionBase {

		public PageCollection(IList sourceList) {
			InnerList.AddRange(sourceList);
		}

		public Page this[int index] {
			get {
				return (Page)InnerList[index];
			}
		}

		public int IndexOf(Page value) {
			return InnerList.IndexOf(value);
		}

		public bool Contains(Page value) {
			return InnerList.Contains(value);
		}
	}
	#endregion
}
