<SCRIPT language ="C#" runat ="server">
int ValidateInt(string str, bool positive, string errorMessage)
{
	try {
		int i = Convert.ToInt32(str);
		if (positive && i < 0)
			throw null;
		return i;
	}
	catch {
		Session["warning"] = errorMessage;
		Response.Redirect("warning.aspx");
		return 0;
	}
}
int ValidateRangeInt(string str, int a, int b, string errorMessage)
{
	try {
		int i = Convert.ToInt32(str);
		if (i >= a && i <= b)
			return i;
		else
			throw null;
	}
	catch {
		Session["warning"] = errorMessage;
		Response.Redirect("warning.aspx");
		return 0;
	}
}
double ValidateDouble(string str, bool positive, string errorMessage)
{
	try {
		double d = Convert.ToDouble(str);
		if (positive && d < 0)
			throw null;
		return d;
	}
	catch {
		Session["warning"] = errorMessage;
		Response.Redirect("warning.aspx");
		return 0;
	}
}
double ValidateRangeDouble(string str, double a, double b, string errorMessage)
{
	try {
		double d = Convert.ToDouble(str);
		if (d >= a && d <= b)
			return d;
		else
			throw null;
	}
	catch {
		Session["warning"] = errorMessage;
		Response.Redirect("warning.aspx");
		return 0;
	}
}
</SCRIPT>
