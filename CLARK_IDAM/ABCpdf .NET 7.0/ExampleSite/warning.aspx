<%@ Page %>

<%@ Assembly Name="ABCpdf" %>
<%@ Import Namespace="WebSupergoo.ABCpdf6" %>
<html>
<head>
	<title>ABCpdf Error</title>
	<link href="mystyles.css" rel="stylesheet">
</head>
<body>
	<h1>
		<img height="60" src="images/goo.gif" width="58" align="right"></h1>
	<h1>
		<font color="#000000">ABCpdf Error</font></h1>
	<p>
		Your request couldn't be completed because of the following error:
	</p>
	<p>
		<b>
			<%
				Response.Write(Session["warning"]);
			%>
		</b>
	</p>
	<p>
		Use the <a href="javascript:history.back();">back</a> button to return to the form
		page and correct the error.
	</p>
</body>
</html>
