@echo off

echo +------------------------------+
echo +   WebSupergoo ABCpdf .NET    +
echo +------------------------------+
echo Registering type library ...
echo.

set REGASM=%windir%\Microsoft.NET\Framework\v2.0.50727\regasm.exe
set ABCDIR=%programfiles%\WebSupergoo\ABCpdf .NET 7.0\Common\

if not exist "%REGASM%" (
	echo %REGASM% not found.
	goto end
)

if not exist "%ABCDIR%ABCpdf.dll" set ABCDIR=.\
if not exist "%ABCDIR%ABCpdf.dll" (
	echo ABCpdf.dll not found.
	goto end

)

if not exist "%ABCDIR%ABCpdf.tlb" (
	echo Type library file ABCpdf.tlb not found.
	goto end
)



%REGASM% "%ABCDIR%ABCpdf.dll" /tlb:"%ABCDIR%ABCpdf.tlb" /codebase /nologo
if errorlevel 1 (
	echo Operation failed.
	goto end
)



:end
echo.
pause
