﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ResumeHTMLControl.ascx.vb" Inherits="ResumeHTMLControl" %>
<style  type="text/css">
.header {
	font-family: Times News Roman;
	color: #000000;
	font-size: 20pt;
	line-height: 24pt;
	font-weight: bold;
	margin-bottom: 2px;
	color: rgb(153,109,80);
}
.header2 {
	font-family: Zapf Humanist 601 BT, Arial;
	color: #000000;
	font-size: 9pt;
	line-height: 12pt;
	font-weight: normal;
	position:relative;
	top:-1px;
	font-style: italic;
	
}
.header3 {
	font-family: Zapf Humanist 601 BT, Arial;
	color: rgb(153,109,80);
	font-size: 9pt;
	line-height: 12pt;
	font-weight: normal;
	position:relative;
	top:-1px;
	
}
.subheader {
	font-family: Zapf Humanist 601 BT, Arial;
	font-size: 8pt;
	color: #000000;
	line-height: 10pt;
	position:relative;
	top:-2px;
}
.subheader2, .subheader3, .subheader4 {
	font-family: Zapf Humanist 601 BT, Arial;
	color: rgb(153,109,80);
	font-size: 8pt;
	line-height: 10pt;
	font-weight: bold;
}
.subheader2 {
	margin-top: 10px;
	font-weight: normal;
}
.subheader3 {
	font-weight: normal;
	margin-bottom: 4px;
}
.subheader4 {
	font-weight: bold;
	font-size: 8pt;
	margin-top: 5px;
	color: #000000;
}
.common {
	font-family: Zapf Humanist 601 BT;
	font-size: 8pt;
	color: #000000;
	line-height: 10pt;
	width: 95%


}
.black 
{
    color: #000000;
}
img {
	position:relative;
	top:2px;
	margin-bottom: 8px;
}
.projectvalue {
	line-height: 7.5pt;
}
</style>
<table width="600">
		<tr>
		<td width="600" valign="top">
				<table width="90%" style="margin-top:125px;position:relative;left:10px;" align="center">
				<tr>
					<td width="34%" valign="top">
					<div style="width:93%;">
					<div class="header"><asp:label ID="userName" runat="server"></asp:label></div>
					<div class="header2"><asp:label ID="userTitle" runat="server"></asp:label></div>
					<div class="header3"><asp:label ID="userCompany" runat="server"></asp:label></div>
					<div class="subheader2" style="margin-top:15px;">
					<asp:literal ID="literalContactTitle" runat="server"></asp:literal>
					</div>
					<div class="subheader3" style="color:Black;"><asp:Label ID="labelContact" runat="server"></asp:Label></div>
                    <asp:Label ID="labelEducationTitle" runat="server"></asp:Label>
					<asp:Label ID="labelEducation" runat="server" CssClass="black"></asp:Label>
                    <asp:Label ID="labelLicenseTitle" runat="server"></asp:Label>
					<asp:Label ID="labelLicense" runat="server" CssClass="black"></asp:Label>
                    <asp:Label ID="labelAwardsTitle" runat="server"></asp:Label>
					<asp:Label ID="labelAwards" runat="server" CssClass="black"></asp:Label>
					</td>
					<td width="66%" valign="top" align="left">
					<div class="common">
					<asp:Label ID="labelBio" runat="server"></asp:Label>
					<asp:Repeater ID="userExperience" runat="server" OnItemDataBound="RepeaterProjects_ItemDataBound">
					<HeaderTemplate>
					<div class="subheader2">PROJECT EXPERIENCE</div>
					</HeaderTemplate>
					<ItemTemplate>
					    <div class="subheader4"><%#Container.DataItem("name")%></div>
					    <div class="common"><asp:Label ID="labelValue" runat="server" CssClass="projectvalue"></asp:Label><%#Container.DataItem("description")%></div>
					</ItemTemplate>
					</asp:Repeater>
					</div>
					</td>

				</tr>	
				</table>
				</td>
				</tr>
	</table> 