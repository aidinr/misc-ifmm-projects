﻿<%@ control language="VB" autoeventwireup="false"  CodeFile="ResumeHTMLControl3.ascx.vb"  inherits="ResumeHTMLControl3" %>
<style  type="text/css">
td {
	font-family: Zapf Humanist 601 BT;
	font-size: 7.5pt;
	line-height: 9pt;
	color: #000000;
}
.header {
	font-family: Times New Roman;
	color: #996d50;
	font-size: 21pt;
	line-height: 23pt;
	color: rgb(153,109,80);
	
}
.subheader {
	font-size: 9pt;
	color: #000000;
}
.subheader2 {
	color: rgb(153,109,80);
	font-size: 8pt;
}
.subheader3 {
	font-size: 7.5pt;
	line-height: 10.5pt;
	font-weight: bold;
	color: #000000;
}
.employeeImage 
{
    margin-right: 8px;   
}
.projectvalue {
	line-height: 7.5pt;
}
</style>
<table width="612">
		<tr>
		<td width="612" valign="top">
		<asp:Literal ID="Page1Header" runat="server"></asp:Literal>
		<tr>
		    <td width="100%">
		    
		    <asp:Label ID="labelTitle" runat="server" CssClass="header"></asp:Label>
		    
		    <asp:Repeater ID="repeaterEmployees" runat="server" OnItemDataBound="repeaterEmployees_ItemDataBound">
		    <ItemTemplate>
		    <asp:Literal ID="literalPageBreak" runat="server"></asp:Literal>
		    <hr style="position:relative;top:-3px;width:100%;height:1px;border-bottom:0;border-left:0;border-right:0;border-top:1px solid #5a471c;color:#5a471c" /><br />
		    <table width="100%">
		        <tr>
		            <td valign="top" width="158"><asp:Image ID="employeeImage" runat="server" CssClass="employeeImage" /><br /><br />
		            <asp:Label ID="employeeImageURL" runat="server"></asp:Label>
		            </td>
		            <td valign="top">
		                <span class="subheader2"><%#Container.DataItem("firstname").ToString.ToUpper & " " & Container.DataItem("lastname").ToString.ToUpper%></span><br />
		                 <span style="line-height:11pt;"><i><%#Container.DataItem("position")%></i> | <i><%#Container.DataItem("agency")%></i><br /></span>
		                <br />
		                <asp:Label ID="shortbio" runat="server"></asp:Label>
		                <br /><br />
		               <span style="line-height:11pt;"> <asp:Label ID="employeeEducationTitle" runat="server" CssClass="subheader2"></asp:Label></span>
		                <asp:Label ID="employeeEducation" runat="server"></asp:Label>
		            </td>
		        </tr>
		    </table>
		    <asp:Literal ID="literalPageEnd" runat="server"></asp:Literal>
		    </ItemTemplate>
		    </asp:Repeater>