﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient


Partial Class ResumeHTMLControl3
    Inherits System.Web.UI.UserControl

    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim adjust_y As String = "33"
        'Dim adjust_x As String = "55"

        Dim adjust_y As String = Request.QueryString("y")
        Dim adjust_x As String = Request.QueryString("x")

        Dim reportHeader As String = Request.QueryString("report")
        Dim reportLetterhead As String = Request.QueryString("letterhead")
        Dim reportTitle As String = Request.QueryString("title")

        If (reportLetterhead = "1") Then
            'Page1Header.Text = "<table style=""margin-top:105px;position:relative;left:5px;"" width=""88%"" cellpadding=""12"" cellspacing=""12"" align=""center"">"

            Page1Header.Text = "<div align=""right"" style=""margin-top: " & adjust_y & "px;margin-right: " & adjust_x & "px;height:22px;font-family: Zapf Humanist 601 BT;color:#5a471c;font-size:22px;padding-top:4px;"">" & reportHeader & "</div><table style=""margin-top:28px;position:relative;left:5px;"" width=""88%"" cellpadding=""12"" cellspacing=""12"" align=""center"">"
        Else
            Page1Header.Text = "<div align=""right"" style=""margin-top:" & adjust_y & "px;margin-right: " & adjust_x & "px;height:22px;font-family: Zapf Humanist 601 BT;color:#5a471c;font-size:22px;padding-top:4px;"">" & reportHeader & "</div><table style=""margin-top:28px;position:relative;left:5px;"" width=""88%"" cellpadding=""12"" cellspacing=""12"" align=""center"">"
        End If

        labelTitle.Text = reportTitle

        Dim theSession As String = Request.QueryString("s")
        Dim theUser As String = Request.QueryString("uid")

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))

        Dim sql As String = "select isnull(a.externaluserid,99) employeenum,  firstname, lastname, agency, position, a.userid userid, picture, c.item_value shortbio from ipm_resume_item b,ipm_user a left join ipm_user_field_value c on a.userid = c.user_id and c.item_id = 21767702 where a.userid = b.userid and b.session_id = @sessionid and a.active = 'Y' group by firstname, lastname, agency, position, bio, a.userid, picture,c.item_value, isnull(a.externaluserid,99) order by employeenum asc"
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim SessionIDParam As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
        SessionIDParam.Value = theSession
        MyCommand1.SelectCommand.Parameters.Add(SessionIDParam)        


        Dim DT1 As New DataTable("resume")

        MyCommand1.Fill(DT1)


        repeaterEmployees.DataSource = DT1
        repeaterEmployees.DataBind()


    End Sub

    Public Sub RepeaterEmployees_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        Dim theSession As String = Request.QueryString("s")

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim labelEducationTitle As Label = e.Item.FindControl("employeeEducationTitle")
            Dim labelEducation As Label = e.Item.FindControl("employeeEducation")
            Dim literalPageBreak As Literal = e.Item.FindControl("literalPageBreak")
            Dim literalPageEnd As Literal = e.Item.FindControl("literalPageEnd")
            Dim employeeImage As Image = e.Item.FindControl("employeeImage")
            Dim employeeImageURL As Label = e.Item.FindControl("employeeImageURL")
            Dim shortbio As Label = e.Item.FindControl("shortbio")

            Dim adjust_y As String = "33"
            Dim adjust_x As String = "55"

            Dim reportHeader As String = Request.QueryString("report")


            Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))


            Dim sql2 As String = "select * from ipm_user_field_value where user_id = @userid and item_id in (21767697,21767699,21767700) order by item_id"
            Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)
            'hard code: award: 21600162


            Dim UserID2 As New SqlParameter("@userid", SqlDbType.VarChar, 255)

            UserID2.Value = e.Item.DataItem("userid")

            MyCommand2.SelectCommand.Parameters.Add(UserID2)


            Dim DT2 As New DataTable("education")


            MyCommand2.Fill(DT2)

            If (DT2.Rows.Count > 0) Then

                For Each x As DataRow In DT2.Rows

                    If (x("item_value") <> "") Then
                        labelEducationTitle.Text = "Education" & "<br />"
                        labelEducation.Text = labelEducation.Text & x("item_value") & "<br />"
                    End If


                Next

            End If


            employeeImage.ImageUrl = Session("WSRetreiveAsset") & "type=contact&size=1&width=270&height=196&crop=1&id=" & e.Item.DataItem("userid")
            employeeImage.Width = 135
            employeeImage.Height = 98
            employeeImageURL.Visible = False
            'employeeImageURL.Text = Session("WSRetreiveAsset") & "&type=contact&size=1&width=130&height=130&crop=1&id=" & e.Item.DataItem("userid") & "&session=" & theSession
            If (Not IsDBNull(e.Item.DataItem("shortbio"))) Then
                shortbio.Text = e.Item.DataItem("shortbio").ToString.Replace(vbCrLf, "<br />")
            End If


            If (e.Item.ItemIndex = 3) Then
                literalPageBreak.Text = "<div style=""page-break-after:always"">&nbsp;</div><table style=""margin-top:80px;position:relative;left:5px;"" width=""88%"" cellpadding=""12"" cellspacing=""12"" align=""center""><tr><td>"
            ElseIf (e.Item.ItemIndex Mod 6 = 0 And e.Item.ItemIndex <> 0) Then
                literalPageBreak.Text = "<div style=""page-break-after:always"">&nbsp;</div>" & "<table width=""612""><tr><td width=""612"" valign=""top"">" & "<div align=""right"" style=""margin-top:" & adjust_y & "px;margin-right: " & adjust_x & "px;height:22px;font-family: Zapf Humanist 601 BT;color:#5a471c;font-size:22px;padding-top:4px;"">" & reportHeader & "</div><table style=""margin-top:28px;position:relative;left:5px;"" width=""88%"" cellpadding=""12"" cellspacing=""12"" align=""center"">" & "<tr><td width=""100%"">"
            ElseIf (e.Item.ItemIndex Mod 3 = 0 And e.Item.ItemIndex <> 3 And e.Item.ItemIndex <> 0) Then
                literalPageBreak.Text = "<div style=""page-break-after:always"">&nbsp;</div><table style=""margin-top:80px;position:relative;left:5px;"" width=""88%"" cellpadding=""12"" cellspacing=""12"" align=""center""><tr><td>"
            End If

            If (e.Item.ItemIndex = 2) Then
                literalPageEnd.Text = "</tr></table></td></tr></table>"
            ElseIf (e.Item.ItemIndex Mod 6 = 5) Then
                literalPageEnd.Text = "</tr></table></td></tr></table>"
            ElseIf (e.Item.ItemIndex Mod 3 = 2 And e.Item.ItemIndex <> 2) Then
                literalPageEnd.Text = "</td></tr></table>"
            End If

        End If





    End Sub

End Class
