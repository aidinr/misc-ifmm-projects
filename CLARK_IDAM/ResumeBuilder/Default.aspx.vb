﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text

Imports WebSupergoo.ABCpdf7
Imports WebSupergoo.ABCpdf7.Objects
Imports WebSupergoo.ABCpdf7.Atoms
Imports WebSupergoo.ABCpdf7.Operations

Partial Class _Default
    Inherits System.Web.UI.Page

    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Call CheckLogin()

        'If (Request.Form("submit") = "1") Then
        'GeneratePDF()
        'End If

        Dim sql As String = "select firstname + ' ' + lastname name,userid from ipm_user where active = 'y' and userid <> 1 order by firstname"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))

        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim DT1 As New DataTable("employees")

        MyCommand1.Fill(DT1)

        listBoxEmployees.SelectionMode = ListSelectionMode.Multiple
        listBoxEmployees.DataSource = DT1
        listBoxEmployees.DataValueField = "userID"
        listBoxEmployees.DataTextField = "name"
        listBoxEmployees.DataBind()


        sql = "select distinct agency from ipm_user where active = 'y' and agency <> '' order by agency "

        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim DT2 As New DataTable("companies")

        MyCommand2.Fill(DT2)

        listBoxCompanies.SelectionMode = ListSelectionMode.Single
        listBoxCompanies.DataSource = DT2
        listBoxCompanies.DataValueField = "agency"
        listBoxCompanies.DataTextField = "agency"
        listBoxCompanies.DataBind()
        listBoxCompanies.Items.Insert(0, "All")

        If (ConfigurationSettings.AppSettings("resume1") = "true") Then
            placeholderResume1.Visible = True
        Else
            placeholderResume1.Visible = False
        End If
        If (ConfigurationSettings.AppSettings("resume2") = "true") Then
            placeholderResume2.Visible = True
        Else
            placeholderResume2.Visible = False
        End If
        If (ConfigurationSettings.AppSettings("resume3") = "true") Then
            placeholderResume3.Visible = True
        Else
            placeholderResume3.Visible = False
        End If

    End Sub

    Public Sub CheckLogin()
        If ConfigurationSettings.AppSettings("UseSecurityOverride") Is Nothing Then
            If (InStr(Request.ServerVariables("SCRIPT_NAME"), "login.aspx") = 0) Then
                If (Session("login") <> "1") Then
                    Response.Redirect("Login.aspx?ref=" & Request.ServerVariables("script_name"))
                    Response.End()
                End If
            End If
        Else
            Session("UserID") = "1"
        End If

    End Sub

    Public Sub Callback1_callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles Callback1.Callback

        Dim s As String = e.Parameters(1)

        If (s <> "") Then

            s = "and userid not in (" & s & ")"

        End If

        Dim sql As String = ""
        Dim MyParameter As SqlParameter = New SqlParameter("@agency", SqlDbType.VarChar, 255)
        If (e.Parameter = "All") Then
            sql = "select firstname + ' ' + lastname name,userid from ipm_user where active = 'y' " & s & " and userid <> 1  order by firstname"
        Else
            sql = "select firstname + ' ' + lastname name,userid from ipm_user where active = 'y' and agency = @agency " & s & " and userid <> 1  order by firstname"

            MyParameter.Value = e.Parameters(0)
        End If


        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))


        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        If (e.Parameter <> "All") Then
            MyCommand1.SelectCommand.Parameters.Add(MyParameter)
        End If


        Dim DT1 As New DataTable("employees")

        MyCommand1.Fill(DT1)

        listBoxEmployees.SelectionMode = ListSelectionMode.Multiple
        listBoxEmployees.DataSource = DT1
        listBoxEmployees.DataValueField = "userID"
        listBoxEmployees.DataTextField = "name"
        listBoxEmployees.DataBind()

        listBoxEmployees.RenderControl(e.Output)
    End Sub

    Public Sub Callback2_callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles Callback2.Callback

        Dim sql As String = "select * from ipm_project_contact a, ipm_project b where available = 'y' and a.projectid = b.projectid and a.userid = @userid"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))


        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim MyParameter1 As SqlParameter = New SqlParameter("@userid", SqlDbType.VarChar, 255)
        MyParameter1.Value = e.Parameters(0)

        MyCommand1.SelectCommand.Parameters.Add(MyParameter1)


        Dim DT1 As New DataTable("projects")

        MyCommand1.Fill(DT1)

        If (e.Parameters(1) = 0) Then
            listBoxSelectedProjects.SelectionMode = ListSelectionMode.Single
        ElseIf (e.Parameters(1) = 1) Then
            listBoxSelectedProjects.SelectionMode = ListSelectionMode.Multiple
        End If
        listBoxSelectedProjects.DataSource = DT1
        listBoxSelectedProjects.DataValueField = "projectID"
        listBoxSelectedProjects.DataTextField = "name"
        listBoxSelectedProjects.Width = 200
        listBoxSelectedProjects.Height = 360
        listBoxSelectedProjects.CssClass = "selectmulti"
        listBoxSelectedProjects.DataBind()

        checkBoxSelectAllProjects.Checked = False

        placeholderSelectedProjects.RenderControl(e.Output)




    End Sub


    Public Sub Callback3_callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles Callback3.Callback

        If (e.Parameters(0) = "hide") Then
            placeholderDescriptions.Visible = False

        ElseIf (e.Parameters(0) = "true") Then
            'select all path, hide everything but field selector, only if there are projects available

            If (e.Parameters(1) <> "undefined") Then
                placeholderDescriptions.Visible = True
                selectedDescriptionText.Visible = False
                labelCustomDescription.Visible = False
            End If


        Else
            'normal path
            placeholderDescriptions.Visible = True
            selectedDescriptionText.Visible = True
            labelCustomDescription.Visible = True

            Dim sql As String = "select description from ipm_project where projectid = @projectid and available = 'y'"

            Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))

            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
            Dim ProjectID As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
            MyCommand1.SelectCommand.Parameters.Add(ProjectID)
            MyCommand1.SelectCommand.Parameters("@projectid").Value = e.Parameters(1)
            Dim DT1 As New DataTable("projectdescription")

            MyCommand1.Fill(DT1)

            Dim sql2 As String = "select * from ipm_project_contact where userid = @userid and projectid = @projectid"

            Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)
            Dim ProjectIDParam As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
            Dim UserIDParam As New SqlParameter("@userid", SqlDbType.VarChar, 255)
            MyCommand2.SelectCommand.Parameters.Add(ProjectIDParam)
            MyCommand2.SelectCommand.Parameters("@projectid").Value = e.Parameters(1)
            MyCommand2.SelectCommand.Parameters.Add(UserIDParam)
            MyCommand2.SelectCommand.Parameters("@userid").Value = e.Parameters(2)
            Dim DT2 As New DataTable("projectuserdescription")

            MyCommand2.Fill(DT2)

            If (DT1.Rows(0)("Description").ToString.Length > 160) Then
                selectedDescriptionText.Text = DT1.Rows(0)("Description").ToString.Substring(0, 160) & "..."
            Else
                selectedDescriptionText.Text = DT1.Rows(0)("Description")
            End If

            selectedDescriptionTextLong.Text = DT1.Rows(0)("Description")

            If (DT2.Rows.Count > 0) Then
                If (Not IsDBNull(DT2.Rows(0)("Description"))) Then
                    labelCustomDescription.Text = DT2.Rows(0)("Description")
                End If


            End If


        End If


        placeholderDescriptions.RenderControl(e.Output)

    End Sub


    Public Sub CallBack4_CallBack(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles Callback4.Callback

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim SessionID As String = Session.SessionID




        If (e.Parameters(0) = "add") Then

            Dim projects() As String = e.Parameters(2).Split(",")

            Dim CurrentUserID As String = Session("UserID")

            Dim sql As String = "insert into ipm_resume_item (session_id,userid,projectid,type,description,created_by,insert_date,update_date) values (@sessionid,@userid,@projectid,@type,@description,@createdby,getdate(),getdate())"
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

            Dim SessionIDParam As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
            Dim UserID As New SqlParameter("@userid", SqlDbType.VarChar, 255)
            Dim ProjectID As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
            Dim Type As New SqlParameter("@type", SqlDbType.VarChar, 255)
            Dim Description As New SqlParameter("@description", SqlDbType.VarChar, 255)
            Dim CreatedBy As New SqlParameter("@createdby", SqlDbType.VarChar, 255)
            Dim DT1 As New DataTable("InsertTable")


            Dim sql3 As String = "delete from ipm_project_contact where userid = @userid and projectid = @projectid"
            Dim MyCommand3 As SqlDataAdapter = New SqlDataAdapter(sql3, MyConnection)
            Dim UserIDParam As New SqlParameter("@userid", SqlDbType.VarChar, 255)
            Dim ProjectIDParam As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
            Dim DescriptionParam As New SqlParameter("@description", SqlDbType.Text)

            For Each s As String In projects

                s = Trim(s)

                MyCommand1 = New SqlDataAdapter(sql, MyConnection)

                SessionIDParam = New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
                UserID = New SqlParameter("@userid", SqlDbType.VarChar, 255)
                ProjectID = New SqlParameter("@projectid", SqlDbType.VarChar, 255)
                Type = New SqlParameter("@type", SqlDbType.VarChar, 255)
                Description = New SqlParameter("@description", SqlDbType.Text)
                CreatedBy = New SqlParameter("@createdby", SqlDbType.VarChar, 255)
                DT1 = New DataTable("InsertTable")

                DeleteResumeItem(e.Parameters(1), s)


                MyCommand1.SelectCommand.Parameters.Add(SessionIDParam)
                MyCommand1.SelectCommand.Parameters("@sessionid").Value = SessionID

                MyCommand1.SelectCommand.Parameters.Add(UserID)
                MyCommand1.SelectCommand.Parameters("@userid").Value = e.Parameters(1)

                MyCommand1.SelectCommand.Parameters.Add(ProjectID)
                MyCommand1.SelectCommand.Parameters("@projectid").Value = s

                MyCommand1.SelectCommand.Parameters.Add(Type)
                MyCommand1.SelectCommand.Parameters("@type").Value = e.Parameters(3)

                MyCommand1.SelectCommand.Parameters.Add(Description)

                If (projects.Length > 1) Then
                    MyCommand1.SelectCommand.Parameters("@description").Value = GetProjectDescription(s)
                Else
                    MyCommand1.SelectCommand.Parameters("@description").Value = e.Parameters(4)
                End If



                MyCommand1.SelectCommand.Parameters.Add(CreatedBy)
                MyCommand1.SelectCommand.Parameters("@createdby").Value = CurrentUserID



                MyCommand1.Fill(DT1)
                If (e.Parameters(3) = "1") Then

                    sql3 = "delete from ipm_project_contact where userid = @userid and projectid = @projectid"
                    MyCommand3 = New SqlDataAdapter(sql3, MyConnection)

                    UserIDParam = New SqlParameter("@userid", SqlDbType.VarChar, 255)
                    ProjectIDParam = New SqlParameter("@projectid", SqlDbType.VarChar, 255)
                    DescriptionParam = New SqlParameter("@description", SqlDbType.VarChar, 255)

                    UserIDParam.Value = e.Parameters(1)
                    ProjectIDParam.Value = e.Parameters(2)
                    DescriptionParam.Value = e.Parameters(4)

                    MyCommand3.SelectCommand.Parameters.Add(UserIDParam)
                    MyCommand3.SelectCommand.Parameters.Add(ProjectIDParam)


                    MyCommand3.SelectCommand.Connection.Open()
                    MyCommand3.SelectCommand.ExecuteNonQuery()
                    MyCommand3.SelectCommand.Connection.Close()



                    sql3 = "insert into ipm_project_contact (userid,projectid,description) VALUES(@userid,@projectid,@description)"

                    MyCommand3 = New SqlDataAdapter(sql3, MyConnection)

                    UserIDParam = New SqlParameter("@userid", SqlDbType.VarChar, 255)
                    ProjectIDParam = New SqlParameter("@projectid", SqlDbType.VarChar, 255)
                    DescriptionParam = New SqlParameter("@description", SqlDbType.VarChar, 255)
                    DescriptionParam.Value = e.Parameters(4)

                    UserIDParam.Value = e.Parameters(1)
                    ProjectIDParam.Value = e.Parameters(2)

                    MyCommand3.SelectCommand.Parameters.Add(UserIDParam)
                    MyCommand3.SelectCommand.Parameters.Add(ProjectIDParam)
                    MyCommand3.SelectCommand.Parameters.Add(DescriptionParam)


                    Dim DT3 As New DataTable("UpdateTable")
                    MyCommand3.Fill(DT3)

                End If

            Next

        ElseIf (e.Parameters(0) = "quickadd") Then

            Dim SelectedUsers() As String = e.Parameters(1).Split(",")

            Dim CurrentUserID As String = Session("UserID")

            Dim sql As String = "insert into ipm_resume_item (session_id,userid,projectid,type,description,created_by,insert_date,update_date) values (@sessionid,@userid,@projectid,@type,@description,@createdby,getdate(),getdate())"
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

            Dim SessionIDParam As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
            Dim UserID As New SqlParameter("@userid", SqlDbType.VarChar, 255)
            Dim ProjectID As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
            Dim Type As New SqlParameter("@type", SqlDbType.VarChar, 255)
            Dim Description As New SqlParameter("@description", SqlDbType.VarChar, 255)
            Dim CreatedBy As New SqlParameter("@createdby", SqlDbType.VarChar, 255)
            Dim DT1 As New DataTable("InsertTable")
            Dim DT3 As New DataTable("Projects")
            For Each s As String In SelectedUsers



                s = Trim(s)

                DT3 = GetProjects(s)

                DeleteResumeItem(e.Parameters(1))

                If (DT3.Rows.Count = 0) Then
                    MyCommand1 = New SqlDataAdapter(sql, MyConnection)

                    SessionIDParam = New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
                    UserID = New SqlParameter("@userid", SqlDbType.VarChar, 255)
                    ProjectID = New SqlParameter("@projectid", SqlDbType.VarChar, 255)
                    Type = New SqlParameter("@type", SqlDbType.VarChar, 255)
                    Description = New SqlParameter("@description", SqlDbType.VarChar, 255)
                    CreatedBy = New SqlParameter("@createdby", SqlDbType.VarChar, 255)


                    MyCommand1.SelectCommand.Parameters.Add(SessionIDParam)
                    MyCommand1.SelectCommand.Parameters("@sessionid").Value = SessionID

                    MyCommand1.SelectCommand.Parameters.Add(UserID)
                    MyCommand1.SelectCommand.Parameters("@userid").Value = s

                    MyCommand1.SelectCommand.Parameters.Add(ProjectID)
                    MyCommand1.SelectCommand.Parameters("@projectid").Value = 0

                    MyCommand1.SelectCommand.Parameters.Add(Type)
                    MyCommand1.SelectCommand.Parameters("@type").Value = 0

                    MyCommand1.SelectCommand.Parameters.Add(Description)
                    MyCommand1.SelectCommand.Parameters("@description").Value = ""

                    MyCommand1.SelectCommand.Parameters.Add(CreatedBy)
                    MyCommand1.SelectCommand.Parameters("@createdby").Value = CurrentUserID

                    DT1 = New DataTable("InsertTable")

                    MyCommand1.Fill(DT1)
                Else
                    For Each p As DataRow In DT3.Rows

                        MyCommand1 = New SqlDataAdapter(sql, MyConnection)

                        SessionIDParam = New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
                        UserID = New SqlParameter("@userid", SqlDbType.VarChar, 255)
                        ProjectID = New SqlParameter("@projectid", SqlDbType.VarChar, 255)
                        Type = New SqlParameter("@type", SqlDbType.VarChar, 255)
                        Description = New SqlParameter("@description", SqlDbType.VarChar, 255)
                        CreatedBy = New SqlParameter("@createdby", SqlDbType.VarChar, 255)


                        MyCommand1.SelectCommand.Parameters.Add(SessionIDParam)
                        MyCommand1.SelectCommand.Parameters("@sessionid").Value = SessionID

                        MyCommand1.SelectCommand.Parameters.Add(UserID)
                        MyCommand1.SelectCommand.Parameters("@userid").Value = s

                        MyCommand1.SelectCommand.Parameters.Add(ProjectID)
                        MyCommand1.SelectCommand.Parameters("@projectid").Value = p("projectid")

                        MyCommand1.SelectCommand.Parameters.Add(Type)
                        MyCommand1.SelectCommand.Parameters("@type").Value = 0

                        MyCommand1.SelectCommand.Parameters.Add(Description)
                        MyCommand1.SelectCommand.Parameters("@description").Value = p("description")

                        MyCommand1.SelectCommand.Parameters.Add(CreatedBy)
                        MyCommand1.SelectCommand.Parameters("@createdby").Value = CurrentUserID

                        DT1 = New DataTable("InsertTable")

                        MyCommand1.Fill(DT1)

                    Next
                End If



            Next
        ElseIf (e.Parameters(0) = "quickaddsingle") Then

            Dim SelectedUsers() As String = e.Parameters(1).Split(",")

            Dim CurrentUserID As String = Session("UserID")

            Dim sql As String = "insert into ipm_resume_item (session_id,userid,projectid,type,description,created_by,insert_date,update_date) values (@sessionid,@userid,@projectid,@type,@description,@createdby,getdate(),getdate())"
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

            Dim SessionIDParam As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
            Dim UserID As New SqlParameter("@userid", SqlDbType.VarChar, 255)
            Dim ProjectID As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
            Dim Type As New SqlParameter("@type", SqlDbType.VarChar, 255)
            Dim Description As New SqlParameter("@description", SqlDbType.VarChar, 255)
            Dim CreatedBy As New SqlParameter("@createdby", SqlDbType.VarChar, 255)
            Dim DT1 As New DataTable("InsertTable")
            For Each s As String In SelectedUsers

                s = Trim(s)


                DeleteResumeItem(e.Parameters(1))

                MyCommand1 = New SqlDataAdapter(sql, MyConnection)

                SessionIDParam = New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
                UserID = New SqlParameter("@userid", SqlDbType.VarChar, 255)
                ProjectID = New SqlParameter("@projectid", SqlDbType.VarChar, 255)
                Type = New SqlParameter("@type", SqlDbType.VarChar, 255)
                Description = New SqlParameter("@description", SqlDbType.VarChar, 255)
                CreatedBy = New SqlParameter("@createdby", SqlDbType.VarChar, 255)


                MyCommand1.SelectCommand.Parameters.Add(SessionIDParam)
                MyCommand1.SelectCommand.Parameters("@sessionid").Value = SessionID

                MyCommand1.SelectCommand.Parameters.Add(UserID)
                MyCommand1.SelectCommand.Parameters("@userid").Value = s

                MyCommand1.SelectCommand.Parameters.Add(ProjectID)
                MyCommand1.SelectCommand.Parameters("@projectid").Value = 0

                MyCommand1.SelectCommand.Parameters.Add(Type)
                MyCommand1.SelectCommand.Parameters("@type").Value = 0

                MyCommand1.SelectCommand.Parameters.Add(Description)
                MyCommand1.SelectCommand.Parameters("@description").Value = ""

                MyCommand1.SelectCommand.Parameters.Add(CreatedBy)
                MyCommand1.SelectCommand.Parameters("@createdby").Value = CurrentUserID

                DT1 = New DataTable("InsertTable")

                MyCommand1.Fill(DT1)

            Next
        ElseIf (e.Parameters(0) = "delete") Then

            Dim sql As String = "delete from ipm_resume_item where userid = @userid and session_id = @sessionid"
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

            Dim SessionIDParam As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
            Dim UserID As New SqlParameter("@userid", SqlDbType.VarChar, 255)

            MyCommand1.SelectCommand.Parameters.Add(SessionIDParam)
            MyCommand1.SelectCommand.Parameters("@sessionid").Value = SessionID

            MyCommand1.SelectCommand.Parameters.Add(UserID)
            MyCommand1.SelectCommand.Parameters("@userid").Value = e.Parameters(1)

            Dim DT1 As New DataTable("DeleteTable")

            MyCommand1.Fill(DT1)
        ElseIf (e.Parameters(0) = "deleteall") Then

            Dim sql As String = "delete from ipm_resume_item where session_id = @sessionid"
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

            Dim SessionIDParam As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
            Dim UserID As New SqlParameter("@userid", SqlDbType.VarChar, 255)

            MyCommand1.SelectCommand.Parameters.Add(SessionIDParam)
            MyCommand1.SelectCommand.Parameters("@sessionid").Value = SessionID

            Dim DT1 As New DataTable("DeleteTable")

            MyCommand1.Fill(DT1)

        End If


        Dim sql2 As String = "select distinct b.lastname lastname, b.firstname firstname, a.userid userid from ipm_resume_item a, ipm_user b where session_id = @sessionid and a.userid = b.userid order by lastname"
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)
        Dim SessionIDParam2 As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(SessionIDParam2)
        MyCommand2.SelectCommand.Parameters("@sessionid").Value = SessionID

        Dim DT2 As New DataTable("SelectedTable")

        MyCommand2.Fill(DT2)

        RepeaterSelections.DataSource = DT2
        RepeaterSelections.DataBind()
        RepeaterSelections.RenderControl(e.Output)




    End Sub

    Public Function DeleteResumeItem(ByVal userid As String) As Boolean
        Dim SessionID As String = Session.SessionID
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim sql As String = "delete from ipm_resume_item where userid = @userid and session_id = @sessionid"
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim SessionIDParam As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
        Dim UserIDParam As New SqlParameter("@userid", SqlDbType.VarChar, 255)
        Dim ProjectIDParam As New SqlParameter("@projectid", SqlDbType.VarChar, 255)

        MyCommand1.SelectCommand.Parameters.Add(SessionIDParam)
        MyCommand1.SelectCommand.Parameters("@sessionid").Value = SessionID

        MyCommand1.SelectCommand.Parameters.Add(UserIDParam)
        MyCommand1.SelectCommand.Parameters("@userid").Value = userid

        Dim dt1 As New DataTable("delete")

        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function DeleteResumeItem(ByVal userid As String, ByVal projectid As String) As Boolean
        Dim SessionID As String = Session.SessionID
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim sql As String = "delete from ipm_resume_item where projectid = @projectid and userid = @userid and session_id = @sessionid"
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim SessionIDParam As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
        Dim UserIDParam As New SqlParameter("@userid", SqlDbType.VarChar, 255)
        Dim ProjectIDParam As New SqlParameter("@projectid", SqlDbType.VarChar, 255)

        MyCommand1.SelectCommand.Parameters.Add(SessionIDParam)
        MyCommand1.SelectCommand.Parameters("@sessionid").Value = SessionID

        MyCommand1.SelectCommand.Parameters.Add(UserIDParam)
        MyCommand1.SelectCommand.Parameters("@userid").Value = userid

        MyCommand1.SelectCommand.Parameters.Add(ProjectIDParam)
        MyCommand1.SelectCommand.Parameters("@projectid").Value = projectid

        Dim dt1 As New DataTable("delete")

        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Sub RepeaterSelections_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim userID As String = e.Item.DataItem("userid")
            Dim sessionID As String = Session.SessionID
            Dim projectList As Label = e.Item.FindControl("labelSelectedProjects")

            Dim sql As String = "select * from ipm_resume_item a, ipm_project b where a.userid = @userid and session_id = @sessionid and a.projectid = b.projectid order by b.name"

            Dim SessionIDParam As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
            Dim UserIDParam As New SqlParameter("@userid", SqlDbType.VarChar, 255)

            Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

            MyCommand1.SelectCommand.Parameters.Add(SessionIDParam)
            MyCommand1.SelectCommand.Parameters.Add(UserIDParam)

            MyCommand1.SelectCommand.Parameters("@sessionid").Value = sessionID
            MyCommand1.SelectCommand.Parameters("@userid").Value = userID

            Dim dt1 As DataTable = New DataTable("projectlists")

            MyCommand1.Fill(dt1)

            Dim t As String = ""

            For Each x As DataRow In dt1.Rows
                If (x("projectid") = "0") Then
                    projectList.Text = "No project.."
                Else
                    If (x("type") = "0") Then
                        t = "(d)"
                    Else
                        t = "(c)"
                    End If
                    projectList.Text = projectList.Text & x("name") & " " & t & ", "
                End If


            Next
            If (projectList.Text.Length > 0) Then
                projectList.Text = projectList.Text.Substring(0, projectList.Text.Length - 2)
            End If


        End If

    End Sub



    Public Function GetReport(ByVal url As String) As Doc
        Dim theDoc As New Doc

        theDoc.HtmlOptions.ContentCount = 10 ' Otherwise the page will be assumed to be invalid.
        theDoc.HtmlOptions.RetryCount = 10 ' Try to obtain html page 10 times
        theDoc.HtmlOptions.Timeout = 180000 ' The page must be obtained in less then 10 seconds

        theDoc.Rect.Inset(0, 10) ' set up document
        theDoc.Rect.Position(5, 15)
        theDoc.Rect.Width = 602
        theDoc.Rect.Height = 767
        theDoc.HtmlOptions.PageCacheEnabled = False
        Dim theId As Integer = theDoc.AddImageUrl(url, True, 0, True)

        While (theDoc.Chainable(theId))

            theDoc.Page = theDoc.AddPage()
            theId = theDoc.AddImageToChain(theId)
        End While

        'Flatten document

        For i = 1 To theDoc.PageCount
            theDoc.PageNumber = i
            theDoc.Flatten()
            i += 1
        Next
        Return theDoc
    End Function



    Public Sub GeneratePDF(ByVal resumetype As Integer)


        Dim adjustX As String = head_x.SelectedValue
        Dim adjustY As String = head_y.SelectedValue

        Dim SessionID As String = Session.SessionID
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim sql2 As String = "select distinct a.userid,session_id,case rtrim(isnull(b.externaluserid,99)) when '' then 99 else rtrim(isnull(b.externaluserid,99)) end employeenum from ipm_resume_item a, ipm_user b where a.userid = b.userid and session_id = @sessionid and a.userid <> '' order by employeenum asc"
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)
        Dim SessionIDParam2 As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(SessionIDParam2)
        MyCommand2.SelectCommand.Parameters("@sessionid").Value = SessionID

        Dim DT2 As New DataTable("SelectedTable")
        MyCommand2.Fill(DT2)
        Dim theDoc As Doc = New Doc()
        theDoc.HtmlOptions.Timeout = 120000
        theDoc.HtmlOptions.PageCacheClear()
        theDoc.HtmlOptions.PageCacheEnabled = False
        theDoc.HtmlOptions.PageCachePurge()
        theDoc.HtmlOptions.BrowserWidth = 640

        theDoc.HtmlOptions.ImageQuality = 93

        theDoc.HtmlOptions.FontEmbed = True
        theDoc.HtmlOptions.FontProtection = False
        theDoc.HtmlOptions.FontSubstitute = False

        Call theDoc.EmbedFont("C:\WINDOWS\Fonts\TT0017M.TTF", "Latin", False, True, True)
        Call theDoc.EmbedFont("C:\WINDOWS\Fonts\tt0015m.ttf", "Latin", False, True, True)
        Call theDoc.EmbedFont("C:\WINDOWS\Fonts\TT0016M.TTF", "Latin", False, True, True)



        Dim oriW As Integer = theDoc.Rect.Width
        Dim oriH As Integer = theDoc.Rect.Height

        Dim oriX As Integer = theDoc.Rect.Left
        Dim oriY As Integer = theDoc.Rect.Bottom

        Dim theUrl As String = ""
        Dim report_letterhead As String = ""
        Dim reportheader As String = ""
        Dim reporttitle As String = ""

        If (checkboxLetterhead.Checked = True) Then
            report_letterhead = "1"
        Else
            report_letterhead = "0"
        End If

        If (checkboxCustomizedHeader.Checked = True) Then
            reportheader = textboxCustomizedHeader.Text
        Else
            reportheader = ""
        End If

        If (checkboxCustomizedTitle.Checked = True) Then
            reporttitle = textboxCustomizedTitle.Text
        Else
            reporttitle = "Meet Our People"
        End If
        Dim reportnopagebreak As String = ""
        Dim checkboxNoPageBreaks As CheckBox = FindControl("checkboxNoPageBreaks")
        If (checkboxNoPageBreaks.Checked = True) Then
            reportnopagebreak = "1"
        Else
            reportnopagebreak = "0"
        End If


        Dim i As Integer = 1
        Dim pageCount As Integer = 1
        Dim theID As Integer

        Dim logoFile As String = ConfigurationSettings.AppSettings("imagepath") & ConfigurationSettings.AppSettings("logofile")

        Dim headerFile As String = ConfigurationSettings.AppSettings("imagepath")

        If (checkboxCustomizedLogo.Checked = True) Then
            If (dropdownCustomizedLogo.SelectedValue = "CBG") Then
                headerFile = headerFile & "data_header_people_cbg.jpg"
            ElseIf (dropdownCustomizedLogo.SelectedValue = "CRC") Then
                headerFile = headerFile & "data_header_people_crc.jpg"
            ElseIf (dropdownCustomizedLogo.SelectedValue = "CR") Then
                headerFile = headerFile & "data_header_people_cr.jpg"
            ElseIf (dropdownCustomizedLogo.SelectedValue = "CRM") Then
                headerFile = headerFile & "data_header_people_crm.jpg"
            ElseIf (dropdownCustomizedLogo.SelectedValue = "DFLLC") Then
                headerFile = headerFile & "data_header_people_dfllc.jpg"
            End If

        Else
            headerFile = headerFile & "data_header_people_plain.jpg"
        End If

        If (resumetype = 1 Or resumetype = 2) Then

            For Each x As DataRow In DT2.Rows

                If (resumetype = 1) Then
                    theUrl = ConfigurationSettings.AppSettings("pdfurl") & "ResumeHTML.aspx?s=" & x("session_id") & "&uid=" & x("userid") & "&type=" & resumetype & "&x=" & adjustX & "&y=" & adjustY & "&pb=" & reportnopagebreak
                ElseIf (resumetype = 2) Then
                    theUrl = ConfigurationSettings.AppSettings("pdfurl") & "ResumeHTML.aspx?s=" & x("session_id") & "&uid=" & x("userid") & "&type=" & resumetype & "&report=" & reportheader & "&letterhead=" & report_letterhead & "&x=" & adjustX & "&y=" & adjustY & "&pb=" & reportnopagebreak
                End If

                'Response.Redirect(theUrl)
                'Exit Sub
                Dim offset As Integer = 145 + adjustY
                Dim offsetfrombottomoriginal As Integer = 75
                Dim offsetfrombottom As Integer = 75
                If (report_letterhead = "1") Then
                    offset = 230 + adjustY
                    offsetfrombottom = 75
                    offsetfrombottomoriginal = 75
                End If

                Select Case i
                    Case 0, 1 'first resume
                        Select Case resumetype
                            Case 2
                                theDoc.Rect.Resize(oriW, oriH - offset)
                                theDoc.Rect.Position(oriX, oriY + offsetfrombottom)
                            Case 1
                                theDoc.Rect.Resize(oriW, oriH)
                                theDoc.Rect.Position(oriX, oriY)
                        End Select
                    Case Else 'all others
                        Select Case resumetype
                            Case 2
                                theDoc.Rect.Resize(oriW, oriH - offset)
                                theDoc.Rect.Position(oriX, oriY + offsetfrombottom)
                            Case 1
                                theDoc.Rect.Resize(oriW, oriH)
                                theDoc.Rect.Position(oriX, oriY)
                        End Select
                        theDoc.Transform.Reset()
                        theDoc.Page = theDoc.AddPage()
                End Select

                'Add Resume HTML
                theID = theDoc.AddImageUrl(theUrl, True, oriW, True)



              

                Do 'Handle Each Page
                    'Is Paged
                    If Not theDoc.Chainable(theID) Then
                        Exit Do
                    End If


                    'reset rect pos
                    theDoc.Rect.Resize(oriW, oriH)
                    theDoc.Rect.Position(oriX, oriY)


                    If (resumetype = 1) Then
                        theDoc.Rect.Resize(102, 45)
                        theDoc.Rect.Move(55, 710)
                        theDoc.AddImageFile(logoFile, 1)
                    ElseIf (resumetype = 2) Then

                        If (report_letterhead = "1") Then
                            theDoc.Rect.Resize(612, 92)

                            If pageCount = 1 Then 'Not (pageCount Mod 2 = 0) Then
                                'First Page
                                theDoc.Rect.Move(0, 700)
                                theDoc.AddImageFile(headerFile, 1)
                            Else
                                'Second Page +
                                theDoc.Rect.Move(0, 700)
                                theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "data_header_plain.jpg", 1)
                            End If



                        End If



                        If pageCount = 1 Then 'Not (pageCount Mod 2 = 0) Then
                            theDoc.Transform.Rotate(90, 595, 20)
                            If (report_letterhead = "1") Then
                                theDoc.Rect.Position(612, 33)
                                theDoc.Rect.Resize(15, 15)
                                theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "corner.jpg", 1)
                            End If
                            theDoc.Rect.Position(627, 21)
                            theDoc.Rect.Resize(300, 20)
                            theDoc.AddHtml("&nbsp; <font size=""2"" face=""Zapf Humanist 601 BT"" color=""#5a471c"">" & GetUserName(x("userid")) & "</font>")
                        Else
                            theDoc.Transform.Rotate(90, 32, 20)
                            If (report_letterhead = "1") Then
                                theDoc.Rect.Position(52, 7)
                                theDoc.Rect.Resize(15, 15)
                                theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "corner_2.jpg", 1)
                            End If
                            theDoc.Rect.Position(67, 4)
                            theDoc.Rect.Resize(300, 20)
                            theDoc.AddHtml("&nbsp; <font size=""2"" face=""Zapf Humanist 601 BT"" color=""#5a471c"">" & GetUserName(x("userid")) & "</font>")
                        End If

                    End If

                    'theDoc.Rect.Resize(oriW, oriH)
                    'theDoc.Rect.Position(oriX, oriY)

                    theDoc.Rect.Resize(oriW, oriH - offset)


                    If (report_letterhead = "1") Then
                        offsetfrombottom = offsetfrombottomoriginal - 20
                    Else
                        offsetfrombottom = offsetfrombottomoriginal - 60
                    End If


                    theDoc.Rect.Position(oriX, oriY + offsetfrombottom)
                    theDoc.Transform.Reset()
                    theDoc.Page = theDoc.AddPage()
                    theID = theDoc.AddImageToChain(theID)

                    pageCount = pageCount + 1

                Loop



                'reset rect pos
                theDoc.Rect.Resize(oriW, oriH)
                theDoc.Rect.Position(oriX, oriY)

                'is this a paged HTML?
                If Not theDoc.Chainable(theID) Then
                    If (resumetype = 1) Then
                        theDoc.Rect.Resize(102, 45)
                        theDoc.Rect.Move(55, 710)
                        theDoc.AddImageFile(logoFile, 1)

                    ElseIf (resumetype = 2) Then

                        If (report_letterhead = "1") Then
                            theDoc.Rect.Position(oriX, oriY)
                            theDoc.Rect.Resize(612, 92)
                            theDoc.Rect.Move(0, 700)

                            If pageCount = 1 Then 'Not (pageCount Mod 2 = 0) Then
                                theDoc.AddImageFile(headerFile, 1)
                            Else
                                theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "data_header_plain.jpg", 1)
                            End If

                        End If

                        If Not (pageCount Mod 2 = 0) Then
                            theDoc.Transform.Rotate(90, 595, 20)
                            If (report_letterhead = "1") Then
                                theDoc.Rect.Position(612, 33)
                                theDoc.Rect.Resize(15, 15)
                                theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "corner.jpg", 1)
                            End If
                            theDoc.Rect.Position(627, 21)
                            theDoc.Rect.Resize(300, 20)
                            theDoc.AddHtml("&nbsp; <font size=""2"" face=""Zapf Humanist 601 BT"" color=""#5a471c"">" & GetUserName(x("userid")) & "</font>")
                        Else
                            theDoc.Transform.Rotate(90, 32, 20)
                            If (report_letterhead = "1") Then
                                theDoc.Rect.Position(52, 7)
                                theDoc.Rect.Resize(15, 15)
                                theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "corner_2.jpg", 1)
                            End If
                            theDoc.Rect.Position(67, 4)
                            theDoc.Rect.Resize(300, 20)
                            theDoc.AddHtml("&nbsp; <font size=""2"" face=""Zapf Humanist 601 BT"" color=""#5a471c"">" & GetUserName(x("userid")) & "</font>")
                        End If



                    End If

                    pageCount = pageCount + 1
                End If
                i = i + 1
            Next

        ElseIf (resumetype = 3) Then
            theUrl = ConfigurationSettings.AppSettings("pdfurl") & "ResumeHTML.aspx?s=" & SessionID & "&type=" & resumetype & "&report=" & reportheader & "&letterhead=" & report_letterhead & "&title=" & reporttitle & "&x=" & adjustX & "&y=" & adjustY & "&pb=" & reportnopagebreak


            If (i > 1) Then


                theDoc.Rect.Resize(oriW, oriH)
                theDoc.Rect.Position(oriX, oriY)

                theDoc.Transform.Reset()
                theDoc.Page = theDoc.AddPage()
            End If

            theID = theDoc.AddImageUrl(theUrl)


            Do
                If Not theDoc.Chainable(theID) Then
                    Exit Do
                End If

                If (report_letterhead = "1") Then

                    theDoc.Rect.Resize(612, 92)
                    theDoc.Rect.Move(0, 700)
                    If Not (pageCount Mod 2 = 0) Then
                        theDoc.AddImageFile(headerFile, 1)
                    Else
                        theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "data_header_plain.jpg", 1)
                    End If

                End If

                If Not (pageCount Mod 2 = 0) Then
                    theDoc.Transform.Rotate(90, 595, 20)
                    If (report_letterhead = "1") Then
                        theDoc.Rect.Position(612, 33)
                        theDoc.Rect.Resize(15, 15)
                        theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "corner.jpg", 1)
                    End If
                    theDoc.Rect.Position(627, 21)
                    theDoc.Rect.Resize(300, 20)
                    theDoc.AddHtml("&nbsp; <font size=""2"" face=""Zapf Humanist 601 BT"" color=""#5a471c"">" & reporttitle & "</font>")
                Else
                    theDoc.Transform.Rotate(90, 32, 20)
                    If (report_letterhead = "1") Then
                        theDoc.Rect.Position(52, 7)
                        theDoc.Rect.Resize(15, 15)
                        theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "corner_2.jpg", 1)
                    End If
                    theDoc.Rect.Position(67, 4)
                    theDoc.Rect.Resize(300, 20)
                    theDoc.AddHtml("&nbsp; <font size=""2"" face=""Zapf Humanist 601 BT"" color=""#5a471c"">" & reporttitle & "</font>")
                End If

                theDoc.Rect.Resize(oriW, oriH)
                theDoc.Rect.Position(oriX, oriY)

                theDoc.Transform.Reset()
                theDoc.Page = theDoc.AddPage()
                theID = theDoc.AddImageToChain(theID)

                pageCount = pageCount + 1

            Loop

            If Not theDoc.Chainable(theID) Then

                If (report_letterhead = "1") Then

                    theDoc.Rect.Resize(612, 92)
                    theDoc.Rect.Move(0, 700)

                    If Not (pageCount Mod 2 = 0) Then
                        theDoc.AddImageFile(headerFile, 1)
                    Else
                        theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "data_header_plain.jpg", 1)
                    End If

                End If

                If Not (pageCount Mod 2 = 0) Then
                    theDoc.Transform.Rotate(90, 595, 20)
                    If (report_letterhead = "1") Then
                        theDoc.Rect.Position(612, 33)
                        theDoc.Rect.Resize(15, 15)
                        theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "corner.jpg", 1)
                    End If
                    theDoc.Rect.Position(627, 21)
                    theDoc.Rect.Resize(300, 20)
                    theDoc.AddHtml("&nbsp; <font size=""2"" face=""Zapf Humanist 601 BT"" color=""#5a471c"">" & reporttitle & "</font>")
                Else
                    theDoc.Transform.Rotate(90, 32, 20)
                    If (report_letterhead = "1") Then
                        theDoc.Rect.Position(52, 7)
                        theDoc.Rect.Resize(15, 15)
                        theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "corner_2.jpg", 1)
                    End If
                    theDoc.Rect.Position(67, 4)
                    theDoc.Rect.Resize(300, 20)
                    theDoc.AddHtml("&nbsp; <font size=""2"" face=""Zapf Humanist 601 BT"" color=""#5a471c"">" & reporttitle & "</font>")
                End If

                pageCount = pageCount + 1
            End If
            i = i + 1
        End If



        For i = 1 To theDoc.PageCount
            theDoc.PageNumber = i
            theDoc.Flatten()
        Next

        Dim theDate As String = MonthName(DatePart("m", Date.Now)) & "-" & DatePart("d", Date.Now) & "_-_" & DatePart("h", Date.Now) & "-" & DatePart("n", Date.Now) & "-" & DatePart("s", Date.Now)
        Dim theProjectName As String = "Resume" & "_-_" & theDate & ".pdf"

        theDoc.Save(ConfigurationSettings.AppSettings("pdfpath") & theProjectName)

        Dim stream As System.IO.FileStream

        stream = New FileStream(ConfigurationSettings.AppSettings("pdfpath") & theProjectName, FileMode.Open)
        Dim filesize As Integer = stream.Length

        Response.Clear()
        Response.ContentType = "application/pdf"
        Response.AddHeader("Content-Description", "File Transfer")
        Response.AddHeader("Content-Disposition", "attachment; filename=" & theProjectName)
        Response.AddHeader("Content-Length", filesize)

        Dim b(8192) As Byte

        Do While stream.Read(b, 0, b.Length) > 0
            Response.BinaryWrite(b)
            Response.Flush()
        Loop

        stream.Close()
        Response.End()

    End Sub

    Sub button1_click(ByVal sender As Object, ByVal e As EventArgs) Handles button1.Click
        GeneratePDF(1)
    End Sub

    Sub button2_click(ByVal sender As Object, ByVal e As EventArgs) Handles button2.Click
        GeneratePDF(2)
    End Sub

    Sub button3_click(ByVal sender As Object, ByVal e As EventArgs) Handles button3.Click
        GeneratePDF(3)
    End Sub

    Public Function GetProjectDescription(ByVal projectID As String) As String

        Dim projectDescription As String

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim sql2 As String = "select description from ipm_project where projectid = @projectid and available = 'y'"
        Dim projectIDParam As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        projectIDParam.Value = projectID
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)

        MyCommand2.SelectCommand.Parameters.Add(projectIDParam)

        Dim dt1 As New DataTable
        MyCommand2.Fill(dt1)

        projectDescription = Trim(dt1.Rows(0)("description"))

        Return projectDescription

    End Function

    Public Function GetUserName(ByVal userID As String) As String

        Dim userName As String

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim sql2 As String = "select firstname, lastname from ipm_user where userid = @userid and active = 'y'"
        Dim projectIDParam As New SqlParameter("@userid", SqlDbType.VarChar, 255)
        projectIDParam.Value = userID
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)

        MyCommand2.SelectCommand.Parameters.Add(projectIDParam)

        Dim dt1 As New DataTable
        MyCommand2.Fill(dt1)

        userName = Trim(dt1.Rows(0)("firstname")) & " " & Trim(dt1.Rows(0)("lastname"))

        Return userName

    End Function

    Public Function GetProjects(ByVal userID As String) As DataTable


        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim sql2 As String = "select a.projectid projectid,b.description description from ipm_project_contact a, ipm_project b  where a.userid = @userid and a.projectid = b.projectid and b.available = 'y'"
        Dim projectIDParam As New SqlParameter("@userid", SqlDbType.VarChar, 255)
        projectIDParam.Value = userID
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)

        MyCommand2.SelectCommand.Parameters.Add(projectIDParam)

        Dim dt1 As New DataTable
        MyCommand2.Fill(dt1)

        Return dt1

    End Function

End Class
