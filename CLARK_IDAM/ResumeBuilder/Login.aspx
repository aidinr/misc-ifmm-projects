﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Resume Builder</title>
<style type="text/css">
  	@import url("css/style.css");
</style>    
</head>
<body>
    <form id="form1" runat="server">
    <div id="container">
        <div id="header">
        <table width="100%">
        <tr>
            <td align="right" height="100" valign="middle"><img src="images/logo.jpg" alt="logo" style="margin-right:30px;" /></td>
        </tr>
        </table>        
        </div>
        <div id="subheader">
        <img src="images/title.jpg" alt="Resume Builder" />
        </div>
        <div id="content">
            <table>
					<tr>
						<td>Username:</td>
						<td><input type="text" name="username" /></td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type="password" name="password" /></td>
					</tr>
					<tr>
						<td><input type="hidden" name="submit" value="1" /><input type="hidden" name="ref" value="<%=Request.querystring("ref")%>" /></td>
						<td><input type="submit" value="Login" /></td>
					</tr>					
				</table>
        </div>
    </div>
    </form>
</body>
</html>
