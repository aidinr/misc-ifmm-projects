﻿<%@ control language="VB" autoeventwireup="false"  CodeFile="ResumeHTMLControl2.ascx.vb" inherits="ResumeHTMLControl2" %>
<style  type="text/css">
td {
	font-family: Zapf Humanist 601 BT;
	font-size: 7.5pt;
	line-height: 9pt;
	color: #000000;
}
.header {
	font-family: Times New Roman;
	color: #996d50;
	font-size: 21pt;
	line-height: 23pt;
	color: rgb(153,109,80);
	
}
.subheader {
	font-size: 9pt;
	color: #000000;
}
.subheader2 {
	color: rgb(153,109,80);
	font-size: 8pt;
	padding-top:15px;
	padding-bottom:5px;
}
.subheader3 {
	font-size: 7.5pt;
	line-height: 10.5pt;
	font-weight: bold;
	color: #000000;
}
.subheader4 {
	color: rgb(153,109,80);
	font-size: 8pt;
	line-height: 24pt;
}
.employeeImage 
{
    margin-right: 8px;   
}
.projectvalue {
	line-height: 7.5pt;
}
</style>

<table width="612">
		<tr>
		    <td width="612" valign="top">
		        <asp:Literal ID="Page1Header" runat="server"></asp:Literal>
		        <table style="position:relative;left:45px;" width="492px" align="left">
		            <tr>
		                <td width="100%">
		                        <asp:Label ID="labelName" runat="server" CssClass="header"></asp:Label>
		                        <hr style="position:relative;top:-3px;width:100%;height:1px;border-bottom:0;border-left:0;border-right:0;border-top:1px solid #5a471c;color:#5a471c" />
		                        <asp:Label ID="labelTitle" runat="server" CssClass="subheader"></asp:Label>
		                        <br /><br />
		                        <div class="subheader2">CAREER HIGHLIGHTS</div>
		                        <asp:literal ID="labelCareer" runat="server"></asp:literal>

		                        <asp:literal ID="labelEducationTitle" runat="server"/></asp:literal>
		                        <asp:literal ID="labelEducation" runat="server"></asp:literal>
                                <asp:literal ID="labelLicenseTitle" runat="server"/></asp:literal>
		                        <asp:literal ID="labelLicense" runat="server"></asp:literal>

		                        <asp:literal ID="labelExperienceTitle" runat="server"/></asp:literal>
		                        <asp:literal ID="labelExperience" runat="server"></asp:literal>
		                    </td>
		                </tr>
                </table>
		    </td>
		</tr>
</table>
<%if request.querystring("pb") = "0"  then %>
<%if projectslisted then %>
<div style="page-break-after:always"></div>
<%end if %>
<%end if %>
<table width="612">
		<tr>
		<td width="612" valign="top">
<asp:Literal ID="Page2Header" runat="server"></asp:Literal>

<table style="position:relative;left:45px;" width="492px" align="left" cellpadding="0" cellspacing="0">
<tr>
<td>
<asp:Repeater ID="repeaterProjects" runat="server" OnItemDataBound="RepeaterProjects_ItemDataBound">
<HeaderTemplate><span class="subheader4">REPRESENTATIVE PROJECT LISTING</span><table width="100%" cellpadding="0" cellspacing="0"></HeaderTemplate>
<ItemTemplate>
<tr><td valign="top" align="left" class="projectvalue">
<b><%#Container.DataItem("name")%></b>
</td>
<td valign="top" align="right" class="projectvalue">
<b><%#Container.DataItem("city")%>, <%#Container.DataItem("state_id")%></b>
</td>
</tr>
<tr>
<td colspan="2" class="projectvalue">
<asp:Label ID="labelValue" runat="server" CssClass="projectvalue"></asp:Label><%#Container.DataItem("description")%>
</td>
</tr>
<tr>
<td colspan="2" style="line-height:6pt;">&nbsp;&nbsp;&nbsp;</td>
</tr>
</ItemTemplate>
<FooterTemplate>
</table>
</FooterTemplate>
</asp:Repeater>
</td>
</tr>
</table>



		    </td>
		</tr>
</table>