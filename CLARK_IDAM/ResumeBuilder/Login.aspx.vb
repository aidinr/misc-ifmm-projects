﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class Login
    Inherits System.Web.UI.Page
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.Form("submit") = "1" Then


            Dim username As String = Request.Form("username")
            Dim password As String = Request.Form("password")
            Dim ref As String = Request.Form("ref")

            Dim sql As String = "select * from ipm_user where active = 'y' and login = @login and password = @password"

            Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))

            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
            Dim Login As New SqlParameter("@login", SqlDbType.VarChar, 255)
            Dim PasswordParam As New SqlParameter("@password", SqlDbType.VarChar, 255)
            MyCommand1.SelectCommand.Parameters.Add(Login)
            MyCommand1.SelectCommand.Parameters.Add(PasswordParam)
            MyCommand1.SelectCommand.Parameters("@login").Value = username
            MyCommand1.SelectCommand.Parameters("@password").Value = password
            Dim DT1 As New DataTable("logintable")

            MyCommand1.Fill(DT1)

            If (ref = "") Then
                ref = "ResumeBuilder.aspx"
            End If

            If (Trim(username <> "")) Then
                If (DT1.Rows.Count > 0) Then
                    Session("UserID") = DT1.Rows(0)("userid")
                    Session("Login") = 1
                    Response.Redirect(ref)
                    Response.End()
                Else
                    Response.Redirect("Login.aspx?err=1&ref=" & ref)
                    Response.End()
                End If
            End If
            

        End If
    End Sub
End Class
