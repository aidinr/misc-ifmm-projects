﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class ResumeHTML
    Inherits System.Web.UI.Page
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim webControl As Web.UI.Control

        If (Request.QueryString("type") = "1") Then
            webControl = Me.LoadControl("ResumeHTMLControl.ascx")
        ElseIf (Request.QueryString("type") = "2") Then
            webControl = Me.LoadControl("ResumeHTMLControl2.ascx")
        ElseIf (Request.QueryString("type") = "3") Then
            webControl = Me.LoadControl("ResumeHTMLControl3.ascx")
        End If


        resumePlaceholder.Controls.Add(webControl)
    End Sub
End Class
