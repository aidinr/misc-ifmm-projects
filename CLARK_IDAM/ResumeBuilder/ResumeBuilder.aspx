﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" ValidateRequest="false" enableEventValidation="false" Inherits="_Default" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Resume Builder</title>
    <script type="text/javascript" src="js/jquery-1.3.min.js"></script>
<style type="text/css">
  	@import url("css/style.css");
</style> 

<script type="text/javascript">
    $(document).ready(function() {
        var s = "";
        $('#listBoxCompanies').change(function() {

            $('#listBoxSelectedEmployees option').each(function() {
                s = s + $(this).attr("value") + ",";
            });

            if (s != "") {

                s = s.substring(s.length - 1, 0);

            }

            Callback1.callback($('#listBoxCompanies option:selected').attr("value"), s);

        });

        $('#listBoxSelectedEmployees').change(function() {
            if ($('#checkBoxSelectAllProjects').attr("checked") == true) {
                Callback2.callback($('#listBoxSelectedEmployees').attr("value"), 1);
            }
            else {
                Callback2.callback($('#listBoxSelectedEmployees').attr("value"), 0);
            }
        });

        $('#checkBoxSelectAllProjects').change(function() {
            /*if ($(this).attr("checked") == true) {
            Callback2.callback($('#listBoxSelectedEmployees').attr("value"), 1);
            Callback3.callback($('#checkBoxSelectAllProjects').attr("checked"), $('#listBoxSelectedProjects option:selected').attr("value"), $('#listBoxSelectedEmployees option:selected').attr("value"));
            }
            else {
            Callback2.callback($('#listBoxSelectedEmployees').attr("value"), 0);
            Callback3.callback("hide");
            }*/

            selectAllProjects();


        });

        var employees = "";

        $('#buttonQuickAdd').click(function() {
            employees = "";
            $('#listBoxEmployees option:selected').each(function() {

                employees = employees + $(this).attr("value") + ", ";



            });
            quickAdd(employees);
            return false;


        });

        $('#buttonQuickAddSingle').click(function() {
            //alert($('#listBoxSelectedEmployees').attr("value"));
            quickAddSingle($('#listBoxSelectedEmployees').attr("value"));
            return false;
        });

        $('#buttonDeleteAll').click(function() {
            //alert($('#listBoxSelectedEmployees').attr("value"));

            var deleteAll = confirm("Do you want to clear all selected employees?");

            if (deleteAll = true) {
                clearAll();
            }

            return false;

            
           
        });


        Callback4.callback();

    });

  


    function moveLeft() {
        $('#listBoxSelectedEmployees option:selected').each(function() {
            addOption = "<option value=" + $(this).attr("value") + ">" + $(this).text() + "</option>"
            $('#listBoxEmployees').append(addOption);
        });
        $('#listBoxSelectedEmployees option:selected').remove();
    };
    function moveRight() {
        $('#listBoxEmployees option:selected').each(function() {
            addOption = "<option value=" + $(this).attr("value") + ">" + $(this).text() + "</option>"
            $('#listBoxSelectedEmployees').append(addOption);
        });
        $('#listBoxEmployees option:selected').remove();
    };
    function selectAllProjects() {
        if ($('#checkBoxSelectAllProjects').attr("checked") == true) {
            $('#listBoxSelectedProjects').attr("multiple", "multiple");
            $('#listBoxSelectedProjects option').each(function() {

                $(this).attr("selected", "selected");

            });
            $('#listBoxSelectedProjects').attr("disabled", "disabled");
            Callback3.callback($('#checkBoxSelectAllProjects').attr("checked"), $('#listBoxSelectedProjects option:selected').attr("value"), $('#listBoxSelectedEmployees option:selected').attr("value"))
        }
        else {
            $('#listBoxSelectedProjects').removeAttr("disabled");
            $('#listBoxSelectedProjects').removeAttr("multiple");
            $('#listBoxSelectedProjects option').each(function() {

                $(this).removeAttr("selected");

            });
            Callback3.callback("hide");

        }

        //rebind

        $('#listBoxSelectedProjects').change(function() {
            Callback3.callback($('#checkBoxSelectAllProjects').attr("checked"), $('#listBoxSelectedProjects option:selected').attr("value"), $('#listBoxSelectedEmployees option:selected').attr("value"))
        }); 
    };
    
    function toggleDescriptions(){
                if ($('#checkBoxSelectAllProjects').attr("checked") == true) {

                $('#selectedDescription, #customDescription').css("display", "none");

            }
            else {
                $('#selectedDescription, #customDescription').css("display", "block");
            }

        };

        var projects = "";

        function selectProject() {
            //type 0 means project description
            projects = "";
            $('#listBoxSelectedProjects option:selected').each(
            function() {

                projects = projects + $(this).attr("value") + ",";

            });
            projects = projects.substring(0, projects.length - 1);
            Callback4.callback("add", $('#listBoxSelectedEmployees option:selected').attr("value"), projects, "0", $('#selectedDescriptionTextLong').text());
        };
        function selectUser() {
            if ($('#labelCustomDescription').attr("value") == undefined) {
                    alert("Please fill in user description.");
                }
                else {

                    var saveDesc = confirm("Do you want to save this new description?");
                    if (saveDesc == true) {
                        Callback4.callback("add", $('#listBoxSelectedEmployees option:selected').attr("value"), $('#listBoxSelectedProjects option:selected').attr("value"), "1", $('#labelCustomDescription').attr("value"));
                    }
                    else {
                        Callback4.callback("add", $('#listBoxSelectedEmployees option:selected').attr("value"), $('#listSelectedBoxProjects option:selected').attr("value"), "0", $('#labelCustomDescription').attr("value"));
                    }


                }

            };

        function deleteResume(userID) {
            Callback4.callback("delete", userID);

        };

        function quickAdd(userID) {
            Callback4.callback("quickadd", userID);
        }
        function quickAddSingle(userID) {
            Callback4.callback("quickaddsingle", userID);
        }
        function clearAll() {
            Callback4.callback("deleteall");
        }        

</script>   
</head>
<body>
    <form id="form1" runat="server">
    <div id="container">
        <div id="header">
        <table width="100%">
        <tr>
            <td align="right" height="100" valign="middle"><img src="images/logo.jpg" alt="logo" style="margin-right:30px;" /></td>
        </tr>
        </table>
        </div>
        <div id="subheader">
        <img src="images/title.jpg" alt="Resume Builder" />
        </div>
        <div id="content">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td><img src="images/step1.jpg" alt="" /></td>
                    <td><img src="images/step2.jpg" alt="" /></td>
                    <td><img src="images/step3.jpg" alt="" /></td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top">
                                
                                FILTER BY COMPANIES<br />
                                <asp:ListBox ID="listBoxCompanies" runat="server" Width="200" CssClass="selectmulti" Rows=1></asp:ListBox><br />
                                EMPLOYEES<br />
                                <ComponentArt:CallBack ID="Callback1" runat="server">
				                <Content>
                                <asp:ListBox ID="listBoxEmployees" runat="server" Width="200" Height="310" CssClass="selectmulti"></asp:ListBox>
                                </Content>
                                </ComponentArt:CallBack>
                                <asp:Button ID="buttonQuickAdd" runat="server" Text="V  Quick Add (All projects)" />
                                </td>
                                <td valign="top">
                                    <br /><br /><br /><br /><br /><br /><br /><br />
                                    <a href="javascript:moveRight();"><img src="images/right.jpg" alt="Move Right" /></a><br /><br />
                                    <a href="javascript:moveLeft();"><img src="images/left.jpg" alt="Move Left" /></a>
                                </td>
                                <td valign="top">
                                SELECTED EMPLOYEES<br />
                                <asp:ListBox ID="listBoxSelectedEmployees" Width="200" Height="360" runat="server" CssClass="selectmulti"></asp:ListBox>
                                <br />
                                <asp:Button ID="buttonQuickAddSingle" runat="server" Text="V  Quick Add (No projects)" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                    SELECTED PROJECTS<br />
                    <ComponentArt:CallBack ID="Callback2" runat="server">
                    <Content>
                    <asp:PlaceHolder ID="placeholderSelectedProjects" runat="server">
                    <asp:ListBox ID="listBoxSelectedProjects" Width="200" Height="360" runat="server" CssClass="selectmulti"></asp:ListBox><br />
                    </asp:PlaceHolder>
                    </Content>
                    <ClientEvents>
                     <CallbackComplete EventHandler="selectAllProjects" />
                    </ClientEvents>
                    </ComponentArt:CallBack>
                    <asp:CheckBox ID="checkBoxSelectAllProjects" runat="server"  />SELECT ALL
                    </td>
                    <td valign="top">
                    <ComponentArt:CallBack ID="Callback3" runat="server">
                    <Content>
                    <asp:PlaceHolder ID="placeholderDescriptions" runat="server" Visible="false">
                        <table width="100%">
                        <tr>
                        <td align="left"><asp:DropDownList ID="dropdownSelectDescription" runat="server"><asp:ListItem Value="1" Text="Project Short Description"></asp:ListItem></asp:DropDownList> </td>
                        <td align="right"> <a href="javascript:selectProject();"><img src="images/select.jpg" alt="Select" /></a></td>
                        </tr>
                        </table>
                        <br />
                        <div id="selectedDescription">
                        <asp:Label ID="selectedDescriptionText" runat="server"></asp:Label>
                        </div>
                        <div id="selectedDescriptionLong" style="display:none">
                        <asp:Label ID="selectedDescriptionTextLong" runat="server"></asp:Label>
                        </div>                        
                        <br />
                        <table width="100%" id="customDescription">
                        <tr>
                            <td align="left"><b>Custom Project Experience</b></td>
                            <td align="right"><a href="javascript:selectUser();"><img src="images/add.jpg" alt="Add" /></a></td>
                        </tr>
                        </table>
                        <asp:TextBox textmode="MultiLine" ID="labelCustomDescription" runat="server" Width="250" Wrap="true" Height="180"></asp:TextBox>
                    </asp:PlaceHolder>
                    </Content>
                    <ClientEvents>
                     <CallbackComplete EventHandler="toggleDescriptions" />
                    </ClientEvents>
                    </ComponentArt:CallBack>
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%">
                <tr>
                    <td valign="top" width="450"><img src="images/employee_names.jpg" alt="Employee names" /></td>
                    <td valign="top" width="205"><img src="images/format_options.jpg" alt="Format options" /></td>
                    <td valign="top"><img src="images/generate_resume.jpg" alt="Generate" /></td>
                </tr>
                <tr>
                    <td valign="top">
                    <br />
                    <asp:Button ID="buttonDeleteAll" Text="Clear Selections" runat="server" /><br /><br />
                    <ComponentArt:CallBack ID="Callback4" runat="server">
			            <Content>
			            <asp:Repeater ID="RepeaterSelections" runat="server" OnItemDataBound="RepeaterSelections_ItemDataBound">
			            <HeaderTemplate>
			            <table cellpadding="3" width="100%">
			            </HeaderTemplate>
			            <ItemTemplate>
			            <tr>
			            <td valign="top"><b style="font-size:14px;"><%#Container.DataItem("firstname") & " " & Container.DataItem("lastname")%></b>
			            <br />
			            Projects: <asp:Label ID="labelSelectedProjects" runat="server"></asp:Label>
			            </td>
			            <td width="70"><a href="javascript:deleteResume(<%#Container.DataItem("userid")%>);"><img src="images/remove.jpg" alt="Remove" /></a></td>
			            </tr>
			            </ItemTemplate>
			            <FooterTemplate></table></FooterTemplate>
			            </asp:Repeater>
			            </Content>
			            </ComponentArt:CallBack>                    
                   </td>
                    <td valign="top">
                    <br />
                     <table>
                        <tr>
                            <td valign="top"><asp:CheckBox ID="checkboxCustomizedHeader" runat="server" /></td>
                            <td valign="top">Cust. Report Header <br /><asp:TextBox ID="textboxCustomizedHeader" runat="server" Rows="1"></asp:TextBox></td>
                        </tr>
                        <tr>
			                <td valign="top">
			                    <asp:DropDownList ID="head_x" runat="server">
			                    <asp:ListItem Value="106" Text="-51"></asp:ListItem>
					                <asp:ListItem Value="103" Text="-48"></asp:ListItem>
					                <asp:ListItem Value="100" Text="-45"></asp:ListItem>
					                <asp:ListItem Value="97" Text="-42"></asp:ListItem>
					                <asp:ListItem Value="94" Text="-39"></asp:ListItem>
					                <asp:ListItem Value="91" Text="-36"></asp:ListItem>
					                <asp:ListItem Value="88" Text="-33"></asp:ListItem>
					                <asp:ListItem Value="85" Text="-30"></asp:ListItem>
					                <asp:ListItem Value="82" Text="-27"></asp:ListItem>
					                <asp:ListItem Value="79" Text="-24"></asp:ListItem>
					                <asp:ListItem Value="76" Text="-21"></asp:ListItem>
					                <asp:ListItem Value="73" Text="-18"></asp:ListItem>
					                <asp:ListItem Value="70" Text="-15"></asp:ListItem>
					                <asp:ListItem Value="67" Text="-12"></asp:ListItem>
					                <asp:ListItem Value="64" Text="-9"></asp:ListItem>
					                <asp:ListItem Value="61" Text="-6"></asp:ListItem>
					                <asp:ListItem Value="58" Text="-3"></asp:ListItem>
			                        <asp:ListItem Text="0" Value="55" Selected=true></asp:ListItem>
			                        <asp:ListItem Value="52" Text="3"></asp:ListItem>
                                    <asp:ListItem Value="49" Text="6"></asp:ListItem>
					                <asp:ListItem Value="46" Text="9"></asp:ListItem>
					                <asp:ListItem Value="43" Text="12"></asp:ListItem>
					                <asp:ListItem Value="40" Text="15"></asp:ListItem>
					                <asp:ListItem Value="37" Text="18"></asp:ListItem>
					                <asp:ListItem Value="34" Text="21"></asp:ListItem>
					                <asp:ListItem Value="31" Text="24"></asp:ListItem>
					                <asp:ListItem Value="34" Text="27"></asp:ListItem>
					                <asp:ListItem Value="37" Text="30"></asp:ListItem>
					                <asp:ListItem Value="40" Text="33"></asp:ListItem>
					                <asp:ListItem Value="43" Text="36"></asp:ListItem>
			                    </asp:DropDownList>
				                </td>
			                <td>Adjust Title's X Position</td>
		                </tr>	
		                <tr>
			                <td valign="top">	
			                        <asp:DropDownList ID="head_y" runat="server">
					                <asp:ListItem Value="-15" Text="-51"></asp:ListItem>	
					                <asp:ListItem Value="-12" Text="-48"></asp:ListItem>
					                <asp:ListItem Value="-9" Text="-45"></asp:ListItem>
					                <asp:ListItem Value="-6" Text="-42"></asp:ListItem>
					                <asp:ListItem Value="-3" Text="-39"></asp:ListItem>
					                <asp:ListItem Value="0" Text="-36"></asp:ListItem>
					                <asp:ListItem Value="3" Text="-33"></asp:ListItem>
					                <asp:ListItem Value="6" Text="-30"></asp:ListItem>
					                <asp:ListItem Value="9" Text="-27"></asp:ListItem>
					                <asp:ListItem Value="12" Text="-24"></asp:ListItem>
					                <asp:ListItem Value="15" Text="-21"></asp:ListItem>
					                <asp:ListItem Value="18" Text="-18"></asp:ListItem>
					                <asp:ListItem Value="21" Text="-15"></asp:ListItem>
					                <asp:ListItem Value="24" Text="-12"></asp:ListItem>
					                <asp:ListItem Value="27" Text="-9"></asp:ListItem>
					                <asp:ListItem Value="30" Text="-6"></asp:ListItem>
					                <asp:ListItem Value="33" Text="-3"></asp:ListItem>
					                <asp:ListItem value="36" Text="0" Selected=true></asp:ListItem>
					                <asp:ListItem Value="39" Text="3"></asp:ListItem>
					                <asp:ListItem Value="42" Text="6"></asp:ListItem>
					                <asp:ListItem Value="45" Text="9"></asp:ListItem>
					                <asp:ListItem Value="48" Text="12"></asp:ListItem>
					                <asp:ListItem Value="51" Text="15"></asp:ListItem>
					                <asp:ListItem Value="54" Text="18"></asp:ListItem>
					                <asp:ListItem Value="57" Text="21"></asp:ListItem>
					                <asp:ListItem Value="60" Text="24"></asp:ListItem>
					                <asp:ListItem Value="63" Text="27"></asp:ListItem>
					                <asp:ListItem Value="66" Text="30"></asp:ListItem>
				                </asp:DropDownList>
				                </td>
			                <td>Adjust Title's Y Position</td>
		                </tr>		                        
                        <tr>
                            <td valign="top"><asp:CheckBox ID="checkboxCustomizedTitle" runat="server" /></td>
                            <td valign="top">Cust. Report Title <br /><asp:TextBox ID="textboxCustomizedTitle" runat="server" Rows="1"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td valign="top"><asp:CheckBox ID="checkboxCustomizedLogo" runat="server" /></td>
                            <td valign="top">Cust. Logo <br /><asp:DropDownList ID="dropdownCustomizedLogo" runat="server"><asp:ListItem Text="Clark Builders Group" Value="CBG"></asp:ListItem><asp:ListItem Text="Clark Realty Capital" Value="CRC"></asp:ListItem><asp:ListItem Text="Clark Realty" Value="CR"></asp:ListItem><asp:ListItem Text="Clark Realty Management" Value="CRM"></asp:ListItem><asp:ListItem Text="Duxbury Financial" Value="DFLLC"></asp:ListItem></asp:DropDownList></td>
                        </tr>

                        <tr>
                            <td valign="top"><asp:CheckBox ID="checkboxLetterhead" runat="server" /></td>
                            <td valign="top">Show Letterhead</td>
                        </tr>                                                                        
                        <tr>
                            <td valign="top"><asp:CheckBox ID="checkboxNoPageBreaks" runat="server" /></td>
                            <td valign="top">No Page Breaks</td>
                        </tr>                                                                        
                    </table>
                    </td>
                    <td valign="top">
                    
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <asp:PlaceHolder ID="placeholderResume1" runat="server">
                            <tr>
                                <td valign="top"><br /><img src="images/preview1.jpg" alt="1" /><br /><br /></td>
                                <td valign="top"><br /><b>1. SINGLE PAGE RESUME</b><br /><br /><asp:ImageButton ID="button1" runat="server" ImageUrl="images/generate.jpg" /></td>
                            </tr>
                            </asp:PlaceHolder> 
                            <asp:PlaceHolder ID="placeholderResume2" runat="server">
                            <tr>
                                <td valign="top"><img src="images/preview2.jpg" alt="2" /><br /><br /></td>
                                <td valign="top"><b>2. MULTI PAGE RESUME</b><br /><br /><asp:ImageButton ID="button2" runat="server" ImageUrl="images/generate.jpg"  /></td>
                            </tr>
                            </asp:PlaceHolder> 
                            <asp:PlaceHolder ID="placeholderResume3" runat="server">
                            <tr>
                                <td valign="top"><img src="images/preview3.jpg" alt="3" /><br /><br /></td>
                                <td valign="top"><b>3.GROUP RESUME</b><br /><br /><asp:ImageButton ID="button3" runat="server" ImageUrl="images/generate.jpg"  /></td>
                            </tr>                                             
                            </asp:PlaceHolder>         
                        </table>
                    
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <input type="hidden" value="1" name="submit" />
    </form>
</body>
</html>
