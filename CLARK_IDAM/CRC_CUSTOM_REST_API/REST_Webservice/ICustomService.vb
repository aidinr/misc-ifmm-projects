﻿Imports System.ServiceModel
' NOTE: You can use the "Rename" command on the context menu to change the interface name "IService1" in both code and config file together.
<ServiceContract()>
Public Interface ICustomService

    <OperationContract()>
    <WebInvoke(Method:="GET",
        ResponseFormat:=WebMessageFormat.Json,
         BodyStyle:=WebMessageBodyStyle.Bare,
        UriTemplate:="json?type={type}")>
    Function GetJson(ByVal type As String) As String



    '   Function GetAssetXML(ByVal type As String) As List(Of ProjectData)


    ' TODO: Add your service operations here

End Interface

' Use a data contract as illustrated in the sample below to add composite types to service operations.

<DataContract()>
Public Class CompositeType

    <DataMember()>
    Public Property BoolValue() As Boolean

    <DataMember()>
    Public Property StringValue() As String


End Class
