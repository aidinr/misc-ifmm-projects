﻿Imports System
Imports System.ServiceModel.Web
Imports System.Web
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data
Imports WebArchives.iDAM.WebCommon.Security
Imports System.Net
Imports System.IO
Imports System.Web.Script.Serialization

' NOTE: You can use the "Rename" command on the context menu to change the class name "Service1" in code, svc and config file together.
Public Class CustomService
    Implements ICustomService
    Dim sBaseIP As String = ConfigurationSettings.AppSettings("IDAMLocation")
    Dim sBaseInstance As String = ConfigurationSettings.AppSettings("IDAMInstance")
    Dim sVSIDAMClient As String = ConfigurationSettings.AppSettings("IDAMLocationBrowse")
    Dim sSMILUrl As String = ConfigurationSettings.AppSettings("SMILUrl")
    Dim ClientSessionID As String

    Dim WSRetreiveAsset As String

    Public Sub New()
        '        If WSRetreiveAsset = "" Then
        Session_Start()
        '        End If
    End Sub

    Public Function GetJson(ByVal type As String) As String Implements ICustomService.GetJson
        Select Case type
            Case "projects"
                Dim p As New Projects
                Dim p2 As List(Of ProjectData)
                Session_Start()
                p2 = GetProject().ToList()
                Dim j As New JavaScriptSerializer
                j.MaxJsonLength = Int32.MaxValue
                Dim myResponseBody As String = j.Serialize(p2)
                Return myResponseBody

            Case "awards"

                Dim p2 As List(Of AwardsData)
                Session_Start()
                p2 = GetAward().ToList()
                Dim j As New JavaScriptSerializer
                j.MaxJsonLength = Int32.MaxValue
                Dim myResponseBody As String = j.Serialize(p2)
                Return myResponseBody
            Case "news"
                Dim p2 As List(Of NewsData)
                Session_Start()
                p2 = GetNews().ToList()
                Dim j As New JavaScriptSerializer
                j.MaxJsonLength = Int32.MaxValue
                Dim myResponseBody As String = j.Serialize(p2)
                Return myResponseBody
            Case Else
                Return ""
        End Select
    End Function


    Private Function GetProject() As ProjectData()
        Dim objProject() As ProjectData
        objProject = AssignProjectDatafromDB()
        Return objProject
    End Function
    Private Function GetAward() As AwardsData()
        Dim objAward() As AwardsData
        objAward = AssignAwardDatafromDB()
        Return objAward
    End Function
    Private Function GetNews() As NewsData()
        Dim objNews() As NewsData
        objNews = AssignNewsDatafromDB()
        Return objNews
    End Function
    Private Function AssignProjectDatafromDB() As ProjectData()

        Dim DT As New DataTable

        DT = GetProjectDataDT()

        Dim pd() As ProjectData

        If DT.Rows.Count >= 1 Then
            ReDim pd(DT.Rows.Count - 1)

            For i As Integer = 0 To DT.Rows.Count - 1
                Dim p As New ProjectData
                p.ProjectId = DT.Rows(i)("ProjectId") & ""
                p.Favorite = DT.Rows(i)("Favorite") & ""
                p.Name = DT.Rows(i)("Name") & ""
                p.LongDescription = DT.Rows(i)("Description") & ""
                p.MediumDescription = DT.Rows(i)("DescriptionMedium") & ""
                p.ShortDescription = ""
                p.City = DT.Rows(i)("City") & ""
                p.State = DT.Rows(i)("State_id") & ""
                p.Update_Date = DT.Rows(i)("Update_Date") & ""

                Dim DT2 As New DataTable
                DT2 = GetProjectAssetsDT(DT.Rows(i)("projectid").ToString())
                If DT2.Rows.Count >= 1 Then
                    Dim s(DT2.Rows.Count - 1) As ImageGalleryData

                    For i2 As Integer = 0 To DT2.Rows.Count - 1
                        Dim tempData As New ImageGalleryData()

                        tempData.URL = WSRetreiveAsset & "id=" & DT2.Rows(i2)("Asset_id").ToString() & "&type=asset&size=1&width=400&height=400"
                        tempData.IDAM_CRC_3X2 = IIf((DT2.Rows(i2)("IDAM_CRC_3X2") & "").ToString().Trim() = "1", True, False)
                        tempData.IDAM_CRC_2X1 = IIf((DT2.Rows(i2)("IDAM_CRC_2X1") & "").ToString().Trim() = "1", True, False)
                        tempData.IDAM_CRC_1X1L = IIf((DT2.Rows(i2)("IDAM_CRC_1X1L") & "").ToString().Trim() = "1", True, False)
                        tempData.IDAM_CRC_1X1R = IIf((DT2.Rows(i2)("IDAM_CRC_1X1R") & "").ToString().Trim() = "1", True, False)

                        s(i2) = tempData

                    Next
                    p.ImageGallery = s

                End If
                pd(i) = p

            Next


        End If
        Return pd
    End Function



    Private Function AssignAwardDatafromDB() As AwardsData()

        Dim DT As New DataTable

        DT = GetAwardDataDT()

        Dim pd() As AwardsData

        If DT.Rows.Count >= 1 Then
            ReDim pd(DT.Rows.Count - 1)

            For i As Integer = 0 To DT.Rows.Count - 1
                Dim p As New AwardsData

                p.AwardsId = DT.Rows(i)("Awards_id") & ""
                p.Name = DT.Rows(i)("Name") & ""
                p.Category = DT.Rows(i)("Category") & ""
                p.ProjectName = DT.Rows(i)("Project_Name") & ""
                p.RecipientName = DT.Rows(i)("Recepient_Name") & ""
                p.OrganizationName = DT.Rows(i)("Organization_Name") & ""
                p.OrganizationURL = DT.Rows(i)("Organization_URL") & ""
                p.Creation_Date = DT.Rows(i)("Creation_Date") & ""
                p.Pull_Date = DT.Rows(i)("Pull_Date") & ""
                p.Post_Date = DT.Rows(i)("Post_Date") & ""
                pd(i) = p

            Next


        End If
        Return pd
    End Function

    Private Function AssignNewsDatafromDB() As NewsData()

        Dim DT As New DataTable

        DT = GetNewsDataDT()

        Dim pd() As NewsData

        If DT.Rows.Count >= 1 Then
            ReDim pd(DT.Rows.Count - 1)

            For i As Integer = 0 To DT.Rows.Count - 1
                Dim p As New NewsData

                p.NewsId = DT.Rows(i)("news_id") & ""
                p.PostDate = DT.Rows(i)("post_date") & ""
                p.Headline = DT.Rows(i)("Headline") & ""
                p.PublicationTitle = DT.Rows(i)("PublicationTitle") & ""
                p.Content = DT.Rows(i)("Content") & ""
                p.MainImage = WSRetreiveAsset & "id=" & DT.Rows(i)("News_id").ToString() & "&type=news&size=1&width=400&height=400"
                p.Creation_Date = DT.Rows(i)("Creation_Date") & ""
                p.Pull_Date = DT.Rows(i)("Pull_Date") & ""

                pd(i) = p

            Next


        End If
        Return pd
    End Function

    Private Function GetNewsDataDT() As DataTable
        Dim myConnString = ConfigurationSettings.AppSettings("IDAMDBConnString") '"Server=" & ConfigurationSettings.AppSettings("IDAMDBServer") & ";Database=" & sBaseInstance & ";Uid=" & ConfigurationSettings.AppSettings("IDAMLogin") & ";Pwd=" & ConfigurationSettings.AppSettings("IDAMPassword") & ";"
        Dim MyConnection As SqlConnection = New SqlConnection(myConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("  select distinct n.News_id, n.post_date, ltrim(rtrim(n.Headline)) as Headline, ltrim(rtrim(n.PublicationTitle)) as PublicationTitle, ltrim(rtriM(n.Content)) as Content, Creation_Date, Pull_Date   from ipm_News n  where n.show = 1 and pull_date >= convert(varchar(10), getdate(), 101) order by Post_Date desc", MyConnection)


        Dim DT1 As New DataTable("NewsData")
        MyCommand1.Fill(DT1)
        Return DT1
    End Function
    Private Function GetProjectDataDT() As DataTable
        Dim myConnString = ConfigurationSettings.AppSettings("IDAMDBConnString") '"Server=" & ConfigurationSettings.AppSettings("IDAMDBServer") & ";Database=" & sBaseInstance & ";Uid=" & ConfigurationSettings.AppSettings("IDAMLogin") & ";Pwd=" & ConfigurationSettings.AppSettings("IDAMPassword") & ";"
        Dim MyConnection As SqlConnection = New SqlConnection(myConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("  select  projectid, Favorite, ltrim(rtrim(Name)) Name, ltrim(rtrim(Description)) Description, ltrim(rtrim(DescriptionMedium)) DescriptionMedium, City, State_id, Update_date from ipm_project p where available = 'Y' and (select count(*) from ipm_project_field_value f where item_id in (select item_id from ipm_project_field_desc where item_tag= 'IDAM_PROJECT_CR_WEBSITE') and f.projectid = p.projectid and ltrim(rtriM(item_value)) = '1') >= 1 ", MyConnection)


        Dim DT1 As New DataTable("ProjectData")
        MyCommand1.Fill(DT1)
        Return DT1
    End Function

    Private Function GetAwardDataDT() As DataTable
        Dim myConnString = ConfigurationSettings.AppSettings("IDAMDBConnString") '"Server=" & ConfigurationSettings.AppSettings("IDAMDBServer") & ";Database=" & sBaseInstance & ";Uid=" & ConfigurationSettings.AppSettings("IDAMLogin") & ";Pwd=" & ConfigurationSettings.AppSettings("IDAMPassword") & ";"
        Dim MyConnection As SqlConnection = New SqlConnection(myConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(" select distinct a.awards_id, ltrim(rtrim(Headline)) as Name, ltrim(rtrim(p.name)) as Project_Name, at.type_value as Category,  ltrim(rtrim(rn.item_value)) as Recepient_Name,  ltrim(rtrim(o.item_value)) as Organization_Name,  ltrim(rtrim(ou.item_value)) as Organization_URL, a.Creation_Date, a.Pull_Date, a.Post_Date   from ipm_awards a  left outer join ipm_awards_related_projects ap on a.awards_id = ap.awards_id  left outer join ipm_project p on p.projectid = ap.projectid  left outer join ipm_awards_type at on at.type_id = a.type  left outer join ipm_awards_field_value rn on rn.awards_id = a.awards_id and rn.item_id in (select item_id from ipm_awards_field_desc where item_tag = 'IDAM_AWARDS_RECIPIENT_NAME')  left outer join ipm_awards_field_value o on o.awards_id = a.awards_id and o.item_id in (select item_id from ipm_awards_field_desc where item_tag = 'IDAM_AWARDS_ORGANIZATION_NAME')  left outer join ipm_awards_field_value ou on ou.awards_id = a.awards_id and ou.item_id in (select item_id from ipm_awards_field_desc where item_tag = 'IDAM_ORGANIZATION_URL')   where a.show = 1 and pull_date >= convert(varchar(10), getdate(), 101) order by Post_Date desc", MyConnection)


        Dim DT1 As New DataTable("AwardData")
        MyCommand1.Fill(DT1)
        Return DT1
    End Function
    Private Function GetProjectAssetsDT(projectid As String) As DataTable
        Dim myConnString = ConfigurationSettings.AppSettings("IDAMDBConnString") '"Server=" & ConfigurationSettings.AppSettings("IDAMDBServer") & ";Database=" & sBaseInstance & ";Uid=" & ConfigurationSettings.AppSettings("IDAMLogin") & ";Pwd=" & ConfigurationSettings.AppSettings("IDAMPassword") & ";"
        Dim MyConnection As SqlConnection = New SqlConnection(myConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("  select a.asset_id, crc3x2.item_value as IDAM_CRC_3X2, crc2x1.item_value as IDAM_CRC_2X1, crc1x1L.item_value as IDAM_CRC_1X1L, crc1x1R.item_value as IDAM_CRC_1X1R   from ipm_asset a join ipm_asset_Field_value v on a.asset_id = v.asset_id and v.item_id in (select d.item_id from ipm_asset_Field_desc d where d.item_tag ='IDAM_IMAGE_GALLERY' and d.active=1) left outer join ipm_asset_Field_value crc3x2 on a.asset_id = crc3x2.asset_id and crc3x2.item_id in (select d.item_id from ipm_asset_Field_desc d where d.item_tag ='IDAM_CRC_3X2' and d.active=1) left outer join ipm_asset_Field_value crc2x1 on a.asset_id = crc2x1.asset_id and crc2x1.item_id in (select d.item_id from ipm_asset_Field_desc d where d.item_tag ='IDAM_CRC_2X1' and d.active=1) left outer join ipm_asset_Field_value crc1x1L on a.asset_id = crc1x1L.asset_id and crc1x1L.item_id in (select d.item_id from ipm_asset_Field_desc d where d.item_tag ='IDAM_CRC_1X1L' and d.active=1) left outer join ipm_asset_Field_value crc1x1R on a.asset_id = crc1x1R.asset_id and crc1x1R.item_id in (select d.item_id from ipm_asset_Field_desc d where d.item_tag ='IDAM_CRC_1X1R' and d.active=1)   where projectid = @projectid and active = 1 and available = 'Y' and securitylevel_id = 3 and ltrim(rtrim(v.item_Value)) = '1' ", MyConnection)
        MyCommand1.SelectCommand.Parameters.Add("@projectid", SqlDbType.NVarChar, 255)
        MyCommand1.SelectCommand.Parameters("@projectid").Value = projectid

        Dim DT1 As New DataTable("Assets")
        MyCommand1.Fill(DT1)
        Return DT1
    End Function


    Sub Session_Start()
        ' Code that runs when a new session is started
        ' Code that runs when a new session is started
        Dim assetRetrieve = sBaseIP & "/" & sVSIDAMClient

        Dim theLogin As String = ConfigurationSettings.AppSettings("IDAMLogin")
        Dim thePassword As String = ConfigurationSettings.AppSettings("IDAMPassword")
        Dim loginStatus As Boolean = False
        Dim loginResult1 As String = ""
        Dim sessionID As String = "(S(" & GetSessionID("http://" & assetRetrieve & "/GetSessionID.aspx") & "))"
        ClientSessionID = sessionID
        Try

            loginResult1 = LoginToClient("http://" & assetRetrieve & "/" & sessionID & "/Login.aspx", theLogin, thePassword, sBaseInstance)

        Catch Ex As Exception
            Throw Ex
        End Try

        WSRetreiveAsset = "http://" & assetRetrieve & "/" + sessionID + "/RetrieveAsset.aspx?instance=" & sBaseInstance & "&"

        Dim loginResult2 As String = ""


    End Sub




    Private Function LoginToClient(ByVal pURL As String, ByVal pUsername As String, ByVal pPassword As String, ByVal pInstanceID As String) As Boolean
        Dim fullURL As String
        Try

            Dim wc As New Net.WebClient
            Dim result As String
            fullURL = GetLoginRequestURL(pURL, pUsername, pPassword, pInstanceID, False, False, False, 2)
            Dim data As IO.Stream = wc.OpenRead(fullURL)
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            Return (str.IndexOf("SUCCESS") >= 0)
        Catch ex As Exception
            'Log.Error("error logging in to :" + fullURL, ex)
            Return False
        End Try
    End Function


    Public Shared Function GetSessionID(ByVal URL As String) As String
        Try
            Dim clientreq As HttpWebRequest
            clientreq = HttpWebRequest.Create(URL)
            clientreq.Timeout = CType(10000, Integer)
            Dim clientres As HttpWebResponse = clientreq.GetResponse
            Dim data As IO.Stream = clientres.GetResponseStream
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            If str.IndexOf("SESSIONID") >= 0 Then
                Dim strtidx As Integer = str.IndexOf("SESSIONID=") + 10
                Return str.Substring(strtidx, str.IndexOf("</span>") - strtidx)
            Else
                Throw New Exception("No session ID found. check URL if it has the proper format" + URL)
            End If
        Catch ex As Exception
            Throw New Exception("WebService not available." + URL)
        End Try
    End Function

    Public Const LOGIN As String = "txtLogin"
    Public Const PASSWORD As String = "txtPassword"
    Public Const INSTANCEID As String = "txtInstanceID"
    Public Const DIRECT As String = "direct"
    Public Const ENCRYPTED As String = "encrypted"
    Public Const AUTHENTICATIONLEVEL As String = "level"
    Public Const SECUREMODE As String = "secure"
    Public Const LDAPDOMAIN As String = "ld"
    Public Const LDAPPATH As String = "ldp"

    Public Shared Function GetLoginRequestURL(ByVal pLocation As String, ByVal pLogin As String, ByVal pPassword As String, ByVal pInstanceID As String, ByVal pSecureMode As Boolean, Optional ByVal pEncrypted As Boolean = False, Optional ByVal pDirectMode As Boolean = False, Optional ByVal pAuthenticationLevel As Integer = 0, Optional ByVal pLDAPDomain As String = "", Optional ByVal pLDAPPath As String = "") As String
        If pLocation Is Nothing Then
            Throw New ArgumentException("location cannot be null")
        End If
        If pLogin Is Nothing Then
            Throw New ArgumentException("login cannot be null")
        End If
        If pInstanceID Is Nothing Then
            Throw New ArgumentException("pInstanceID cannot be null")
        End If
        If pPassword Is Nothing Then
            Throw New ArgumentException("pPassword cannot be null")
        End If

        Dim url As New Text.StringBuilder
        url.Append(pLocation)
        url.Append("?")
        url.Append(LOGIN).Append("=").Append(pLogin).Append("&")
        url.Append(PASSWORD).Append("=").Append(pPassword).Append("&")
        url.Append(INSTANCEID).Append("=").Append(pInstanceID).Append("&")
        url.Append(DIRECT).Append("=").Append(pDirectMode.ToString).Append("&")
        url.Append(ENCRYPTED).Append("=").Append(pEncrypted.ToString).Append("&")
        url.Append(AUTHENTICATIONLEVEL).Append("=").Append(pAuthenticationLevel.ToString).Append("&")
        url.Append(SECUREMODE).Append("=").Append(pSecureMode.ToString).Append("&")
        If pLDAPDomain <> "" Then
            url.Append(LDAPDOMAIN).Append("=").Append(pLDAPDomain.ToString).Append("&")
        End If
        If pLDAPPath <> "" Then
            url.Append(LDAPPATH).Append("=").Append(pLDAPPath.ToString)
        End If


        Return url.ToString

    End Function

End Class

Public Class Projects
    Public Property Projects As ProjectData()
End Class

Public Class ProjectData
    Public Property ProjectId As String
    Public Property Favorite As Boolean
    Public Property Name As String
    Public Property ShortDescription As String
    Public Property MediumDescription As String
    Public Property LongDescription As String
    Public Property City As String
    Public Property State As String
    Public Property ImageGallery As ImageGalleryData()
    Public Property Update_Date As String

End Class

Public Class ImageGalleryData
    Public Property URL As String
    Public Property IDAM_CRC_3X2 As Boolean
    Public Property IDAM_CRC_2X1 As Boolean
    Public Property IDAM_CRC_1X1L As Boolean
    Public Property IDAM_CRC_1X1R As Boolean

End Class

Public Class AwardsData
    Public Property AwardsId As String
    Public Property Name As String
    Public Property ProjectName As String
    Public Property Category As String
    Public Property RecipientName As String
    Public Property OrganizationName As String
    Public Property OrganizationURL As String
    Public Property Creation_Date As String
    Public Property Pull_Date As String
    Public Property Post_Date As String
End Class

Public Class NewsData
    Public Property NewsId As String
    Public Property PostDate As String
    Public Property Headline As String
    Public Property PublicationTitle As String
    Public Property Content As String
    Public Property MainImage As String
    Public Property Creation_Date As String
    Public Property Pull_Date As String

End Class

