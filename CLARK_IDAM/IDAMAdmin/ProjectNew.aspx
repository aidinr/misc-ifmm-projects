<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.ProjectNew" ValidateRequest="false" CodeBehind="ProjectNew.aspx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD>
<title>Project</title>
<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
<meta name="vs_defaultClientScript" content="JavaScript">
<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</HEAD>
<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
<link href="treeStyle.css" type="text/css" rel="stylesheet">
<link href="navStyle.css" type="text/css" rel="stylesheet" >
<link href="tabStripStyle.css" type="text/css" rel="stylesheet" />    
<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
<link href="css/uniform.default.css" rel="stylesheet" type="text/css" />

<style>BODY { MARGIN: 0px }
	</style>
	<body onload="this.focus">
		<form id="Form" method="post" runat="server">
			<script type="text/javascript">
  

  
function DoSearchFilter2(searchvalue)
{

<%response.write (GridProjectTemplates.ClientID)%>.Filter("name LIKE '%" + searchvalue + "%' OR description LIKE '%" + searchvalue + "%'");

<%response.write (GridProjectTemplates.ClientID)%>.Render();

}
  
  
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridKeywords.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    GridKeywords.Edit(GridKeywords.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    GridKeywords.EditComplete();     
  }

  function insertRow()
  {
    GridKeywords.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    GridKeywords.Delete(GridKeywords.GetRowFromClientId(rowId)); 
  }

<%if request.querystring("newid") <> "" then%>
window.parent.NavigateToProject('<%response.write (request.querystring("newid"))%>');
window.parent.CloseDialogWindowX();
<%end if%>

<%if not WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("PROJECT_GENERAL_VIEW") then%>
	alert ('You do not have permissions to edit a project.  Please contact your system administrator to add this capability.');
	window.parent.CloseDialogWindowX();
<%end if%>

<%if not WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("ADD_PROJECT") then%>
	alert ('You do not have permissions to add a project.  Please contact your system administrator to add this capability.');
	window.parent.CloseDialogWindowX();
<%end if%>

  <%if instr(request.querystring("id"),"PRJ") = 0 then%>
	
	//window.parent.CloseDialogWindowX();
  <%end if%>
			</script>
			<%response.write (request.querystring("newid"))%>
			<input type="hidden" name="catid" id="catid" value="<%response.write (replace(request.querystring("p"),"CAT",""))%>">
						<div style="padding:20px;position:relative;">
			<div style="width:100%">
			<div style="padding:10px;border:1px solid;background-color:white;position:relative;">
			<table><tr><td><img src="images/project34.gif"></td><td>Create/Edit Project</td></tr></table><br>
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Create/Modify a project by entering a project name and optional description.  </div><br>
  <div style="width=100%">

			
			
			
	
			
			
			
			
			
	<COMPONENTART:TABSTRIP id="TabStripCategory" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataProjectEdit.xml"
		CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
		DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
		<ItemLooks>
			<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10" 
 LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif" 
 HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23" 
 LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
			<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10" 
 LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif" 
 LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
		</ItemLooks>
	</COMPONENTART:TABSTRIP>
	
	<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageCategoryPage" runat="server">
		<ComponentArt:PageView CssClass="PageContent2" runat="server" ID="Pageview1">
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Enter a name, description and security level for this project.</div>
			<br>
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">Name </font>
					</td>
					<td colspan="2" align="left" valign="top" width=100%>
						<asp:TextBox id="Name" runat="server" CssClass="InputFieldMain"  Height=25px></asp:TextBox></td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">Security Level</font></td>
					<td class="PageContent"  width=100% align="left" valign="top">
						<asp:DropDownList ID="SecurityLevel" CssClass="InputFieldMain"  Height=25px Runat="server">
							<asp:ListItem Value="0">Administrator</asp:ListItem>
							<asp:ListItem Value="1">Internal Use</asp:ListItem>
							<asp:ListItem Value="2">Client Use</asp:ListItem>
							<asp:ListItem Value="3">Public</asp:ListItem>
						</asp:DropDownList>
					</td>
					<td class="PageContent" width="80%" align="left" valign="top">
						<input type="checkbox" name="chkactive" checked value="ON">Active</td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">Description</font></td>
					<td class="PageContent" colspan="2" align="left" valign="top"  width=100%>
						<asp:TextBox id="Description" TextMode="MultiLine" runat="server" CssClass="InputFieldMain100P"  Style="height:200px" Height="200px" ></asp:TextBox></td>
				</tr>
			</table>
		</ComponentArt:PageView>
		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Category_Members">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Change permissions for this object by selecting a group or user from the available list and adding to the active list.</div><br>






								
								
								
						
																				
		
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td class="PageContent" width="360" valign=top>
<!--available list-->
<b>Available Groups/Users</b><br><br>



<COMPONENTART:GRID id="GridPermissionAvailable" runat="server" pagerposition="2" ScrollBar="Off" Sort="lastname asc" Height="10" Width="250" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategory" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" AllowPaging="true" cachecontent="false" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategory">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategory">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                            
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategory" InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataCellClientTemplateId="TypeIconTemplateCategory" DataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Firstname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="firstname" Width="70"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Lastname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="lastname" Width="100"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" DataCellCssClass="LastDataCell" AllowEditing="false" AllowGrouping="False" Width="120" SortedDataCellCssClass="SortedDataCell" DataField="description"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<div style=  "text-align:right;BORDER-RIGHT: 1px solid; PADDING-RIGHT: 4px; BORDER-TOP: 1px solid; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: 1px solid; PADDING-TOP: 4px; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: gainsboro">
<b>Search for user/group:</b> <input Class="InputFieldMain" type=text value="" id=search_filter  style="width:120px" onkeyup="javascript:<%response.write( GridPermissionAvailable.ClientId)%>.Filter('firstname LIKE \'%' + this.value + '%\' or lastname LIKE \'%' + this.value + '%\'');">
</div>
					</td>
					<td class="PageContent" width="120" valign= middle>

		<asp:ImageButton ImageUrl="images/bt_u_fwd.gif" Runat=server id="imgbtnAddPermissionsnew"></asp:ImageButton><br><br>
		<asp:ImageButton ImageUrl="images/bt_u_rwd.gif" Runat=server id="imgbtnRemovePermissions"></asp:ImageButton> 
					
					</td>
					<td class="PageContent" width="100%" valign=top>
					
					
					
					
<!--active list-->
<b>Active Groups/Users</b><br><br>


<COMPONENTART:GRID id="GridPermissionActive" runat="server" autoPostBackonDelete="true" PageSize="10" pagerposition="2" ScrollBar="Off" Sort="typeofobject asc" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategoryActive" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="false" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" AllowPaging="true" cachecontent="false" PagerStyle="Numbered"  >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategoryActive">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateCategoryActive">
          <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategoryActive">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                                
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategoryActive" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataField="imagesource" DataCellClientTemplateId="TypeIconTemplateCategoryActive" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True"  />
<componentart:GridColumn AllowEditing="false" HeadingText="Name" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" FixedWidth="false"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Description" DataCellCssClass="LastDataCell" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="description" width="100" FixedWidth="true"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<br>
<asp:Button Runat=server ID=btnApplyPermissions Text="Apply permissions to all descendants"></asp:Button>
<br><asp:CheckBox Runat=server Checked ID=chkapplyoverrides></asp:CheckBox>Include overrides<br>
<asp:CheckBox Runat=server Checked ID="chkapplytofolders"></asp:CheckBox>Include folders
<div id="applypermissionsspinner" style="display:none;  font-family: tahoma, verdana;font-size: 9px;font-weight: bold;"><br><table><tr valign=top ><td><IMG SRC="images/spinner.gif"></td><td style="font-family: tahoma, verdana;font-size: 9px;font-weight: bold;"> Applying permissions...  Please note this can take a couple minutes to complete.</td></tr></table> </div>
					</td>					
				</tr>
			</table>													
		</ComponentArt:PageView>
		
		
		
		
		
		
		
		
		
		
		
		
<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Import_From">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Import project information from external source.  First filter the list below to find the project you want to import.  Select the project and choose the options below to import.  Finally, click the "import project" button on the bottom of the page to start the import process.</div><br>

															









<table cellpadding=0 cellspacing=0 ><tr><td width="420" nowrap  ><font size=1><b>Step 1:</b> Find the project to import (Be sure to select a single project).</font></td><td align=right width=100% nowrap ><b><font size=1>Search for Project:</font></b><img src="images/spacer.gif" width=5 height=1></td><td align=right width=50 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:150px;height:15px;padding:1px;"  onkeyup="DoSearchFilter(this.value);"></td></tr></table> 

<div style="padding-top:10px;"><div style="padding:3px;"></div></div>

<COMPONENTART:GRID 
id="GridExternalProjects" 
runat="server" 
AutoFocusSearchBox="false"
pagerposition="2"
ScrollBar="Off"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ClientSideOnCallbackError="onCallbackError"
ScrollPopupClientTemplateId="ScrollPopupTemplate" 
Sort="name,projectyear asc"
Height="190px" Width="100%"
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
EnableViewState="true"
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRowCategory" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="20" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="True" 
GroupingPageSize="5" 
PagerTextCssClass="GridFooterTextCategory" 
PagerStyle="Numbered" 
PageSize="5" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeader" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="False" 
AllowSorting="True"
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
ScrollBarWidth="15" 
AllowPaging="true">
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
<div style="height:300px;margin-left:200px;"><img src=images/spacer.gif height=50px><br><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplate">
            <a href="javascript:editGridPopup('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border="0" > </a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplate">
            <img src="images/projcat16.gif" border="0" > 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateGoto">
            <img src="images/i_rate.gif" border="0" > 
          </ComponentArt:ClientTemplate>                                      
</ClientTemplates>
<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
EditFieldCssClass="EditDataField"
EditCommandClientTemplateId="EditCommandTemplate"
InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="oid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center" AllowSorting="False" AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="False" HeadingText="#"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="oid" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="False" HeadingText="Name"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="False" HeadingText="Client"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="80" DataField="clientname" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="False" HeadingText="Year"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="projectyear" Width="40" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="False" HeadingText="City"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="40" DataField="city" ></componentart:GridColumn>
<componentart:GridColumn  DataCellCssClass="LastDataCellPostings" AllowEditing="False" HeadingText="State"  AllowGrouping="False" Width="80" DataField="state" ></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>

<hr noshade width=100% size=1>
<br>
<table cellpadding=0 cellspacing=0 ><tr><td width="100%" nowrap  ><font size=1><b>Step 2:</b> Select the optional data to import from this project (Selecting no options will simply create the project with the name seen above).</font></td></tr></table> 
<br>
 <asp:CheckBox id="chkprojectinformation"  Checked runat="server"></asp:CheckBox>Project Information&nbsp;&nbsp;&nbsp; <asp:CheckBox Checked id="chkclientdata" runat="server"></asp:CheckBox>Client Data&nbsp;&nbsp;&nbsp; <asp:CheckBox Checked id="chkcontacts" runat="server"></asp:CheckBox>Contacts&nbsp;&nbsp;&nbsp; <asp:CheckBox Checked id="chkowners" runat="server"></asp:CheckBox>Owners
 <br><br>
 <hr noshade width=100% size=1><br>
<table cellpadding=0 cellspacing=0 ><tr><td width="100%" nowrap  ><font size=1><b>Step 3:</b> Click the "Import Project" button to create the project in IDAM.</font></td></tr></table> 
<br>
<asp:Button id="btnImportProject" runat="server" cssclass="button" Text="Import Project!"></asp:Button>
<br><br>





</ComponentArt:PageView>

		
		
		

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Role_Override">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Change permissions for this object by selecting a group or user from the available list and adding to the active list.</div><br>
															
		
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td class="PageContent" width="360" valign=top>
<!--available list-->
<b>Active Groups/Users</b><br>
<img src="images/spacer.gif" width="300" height="1"><br>




<COMPONENTART:GRID id="GridRoleActivePermissions" AllowMultipleSelect="False" ClientSideOnSelect="AJAXRefreshRoles" runat="server" pagerposition="2" ScrollBar="Off" Sort="name asc" Height="100" Width="300" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateRoleOActive" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" AllowPaging="true" cachecontent="false" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateRoleOActive">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateroleOActive">
          <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TypeIconTemplateRoleOActive">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                                
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategory" InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataField="imagesource" DataCellClientTemplateId="TypeIconTemplateRoleOActive" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True"  />
<componentart:GridColumn AllowEditing="false" HeadingText="Name" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" FixedWidth="false"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="Description" DataCellCssClass="LastDataCell" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="description" width="100" FixedWidth="true"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="True" DataCellCssClass="LastDataCell" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  width="1" FixedWidth="true"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<div style=  "text-align:right;BORDER-RIGHT: 1px solid; PADDING-RIGHT: 4px; BORDER-TOP: 1px solid; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: 1px solid; PADDING-TOP: 4px; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: gainsboro">
<b>Search for user/group:</b> <input Class="InputFieldMain" type=text value="" id=search_filter  style="width:120px" onkeyup="javascript:<%response.write( GridRoleActivePermissions.ClientId)%>.Filter('name LIKE \'%' + this.value + '%\'');">
</div>
					</td>
					<td class="PageContent" width="120" valign= middle>


					
					</td>
					<td class="PageContent" width="100%" valign=top>
					
					
					
					
<!--active list-->
<b>Role Overrides</b><br><br>










<script language=javascript>
function AJAXRefreshRoles(item)
  {
	var itemvaluetmp;
	itemvaluetmp = item.GetMember('uniqueid').Value;
	<%=TreeView_RolesCallback.ClientID%>.Callback(itemvaluetmp);
	return true;
  }  
  


function Object_PopUp(strUrl, popupName, intHeight, intWidth)
{


	if (strUrl == null || strUrl.Length <= 0)
		return;

	var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=no,resizable=yes,toolbar=no";
	//var strFeatures = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;
	//var strFeatures += "'"
	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures , false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
}


function Object_PopUp_Roles()
{
Object_PopUp('RolesEdit.aspx','Edit_Role',700,700);

}

function changerole()
{
if (confirm('Are you sure you want to change this users role?  This will erase any previous role overrides as well.')) {
__doPostBack('Roles','');
}
}

</script>
<table width=100%><tr>
<td width=100% style="FONT-SIZE: 11px; ">Role List</td>
<td width=200px align=right><asp:Button   id="roleOverrides" runat="server" Text="Clear Role Overrides"></asp:Button></td>
</tr></table>	


<COMPONENTART:CALLBACK id="TreeView_RolesCallback" runat="server" CacheContent="false">
			<CONTENT>
				<ComponentArt:TreeView id="TreeView_Roles" Height="280" Width="420px" 
				AutoPostBackOnNodeCheckChanged="true"
				DragAndDropEnabled="false" 
				NodeEditingEnabled="false" 
				KeyboardEnabled="true" 
				CssClass="TreeView" 
				NodeCssClass="TreeNode" 
				SelectedNodeCssClass="SelectedTreeNode" 
				HoverNodeCssClass="HoverTreeNode" 
				NodeEditCssClass="NodeEdit" 
				LineImageWidth="19" 
				LineImageHeight="20" 
				DefaultImageWidth="16" DefaultImageHeight="16" ItemSpacing="0" NodeLabelPadding="3" 
				ParentNodeImageUrl="images/folders.gif" LeafNodeImageUrl="images/folder.gif" ShowLines="true" 
				LineImagesFolderUrl="images/lines/" 
				EnableViewState="true"  
				runat="server" >
				</ComponentArt:TreeView>
			</CONTENT>
	<LOADINGPANELCLIENTTEMPLATE>
		<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
			<TR>
				<TD align="center">
					<TABLE cellSpacing="0" cellPadding="0" border="0">
						<TR>
							<TD style="FONT-SIZE: 10px">Processing Download Request...
							</TD>
							<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
	</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>
		
									









					</td>					
				</tr>
			</table>													
		</ComponentArt:PageView>
		
		
		
		
		
		
		

		
		
		
		
<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Project_Templates">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Apply a project structure (project folders, permissions, overrides etc.) to your existing project from available projects below.  First filter the list below to find the project you want to use.  Select the project and choose the options below to build.  Finally, click the "Apply Template" button on the bottom of the page to start the build process.</div><br>


<table cellpadding=0 cellspacing=0 ><tr><td width="420" nowrap  ><font size=1><b>Step 1:</b> Find the project to build from (Be sure to select a single project).</font></td><td align=right width=100% nowrap ><b><font size=1>Search for Project:</font></b><img src="images/spacer.gif" width=5 height=1></td><td align=right width=50 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter_templates  style="width:150px;height:15px;padding:1px;" onkeyup="javascript:DoSearchFilter2(this.value);"></td></tr></table> 

<div style="padding-top:10px;"><div style="padding:3px;"></div></div>

<COMPONENTART:GRID 
id="GridProjectTemplates" 
runat="server" 
AutoFocusSearchBox="false"
pagerposition="2"
ScrollBar="Off"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ClientSideOnCallbackError="onCallbackError"
ScrollPopupClientTemplateId="ScrollPopupTemplate" 
Sort="favorite desc"
Height="190px" Width="786px"
fillwidth="true"
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateProjects" 
EnableViewState="true"
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRowCategory" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="20" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="True" 
GroupingPageSize="5" 
PagerTextCssClass="GridFooterTextCategory" 
PagerStyle="Numbered" 
PageSize="5" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeader" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="False" 
AllowSorting="True"
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
ScrollBarWidth="15" 
AllowPaging="true">
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateProjects">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateProjects">
            <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border="0" > </a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateProjects">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateProjects">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateProjects">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplateProjects">
            <A href="IDAM.aspx?page=## DataItem.GetMember("page").Value ##&ID=## DataItem.GetMember("projectid").Value ##&type=## DataItem.GetMember("typeofobject").Value ##"><img border=0 src="images/goto.gif"></a>
          </ComponentArt:ClientTemplate>                         
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
            EditFieldCssClass="EditDataField"
            EditCommandClientTemplateId="EditCommandTemplate"
            InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="projectid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false" AllowSorting="false" DataCellClientTemplateId="TypeIconTemplateProjects" dataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<ComponentArt:GridColumn Align="Center" Visible="False" AllowEditing="false" AllowSorting="false" DataCellClientTemplateId="LookupProjectTemplateProjects" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="False" HeadingText="Name"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" width="120" FixedWidth="True"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" Visible="False" AllowEditing="false" Width="150" AllowGrouping="False"  SortedDataCellCssClass="SortedDataCell" DataField="description"  FixedWidth="True"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Type" Visible="False" AllowEditing="false" AllowGrouping="False"   SortedDataCellCssClass="SortedDataCell" width="25" FixedWidth="True" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Updated" Visible="False" AllowEditing="false" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" width="45" FixedWidth="True" SortedDataCellCssClass="SortedDataCell"  DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Category" AllowEditing="False" SortedDataCellCssClass="SortedDataCell" DataField="category_id" width="145" FixedWidth="True" ForeignTable="category" ForeignDataKeyField="category_id" ForeignDisplayField="categoryname" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Security Level" Visible="True" AllowEditing="False" SortedDataCellCssClass="SortedDataCell" width="95" FixedWidth="True" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Project Date" Visible="False" AllowEditing="false" FormatString="MMM yyyy" SortedDataCellCssClass="SortedDataCell"  DataField="projectdate"></componentart:GridColumn>
<componentart:GridColumn HeadingText="User" Visible="False" AllowEditing="false" SortedDataCellCssClass="SortedDataCell"  DataField="userid" ForeignTable="Users" ForeignDataKeyField="userid" ForeignDisplayField="FullName"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Assets" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid" ForeignTable="Assets" ForeignDataKeyField="uniqueid" width="45" FixedWidth="True" ForeignDisplayField="acount"></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" AllowEditing="false" Visible="False" AllowSorting="False" DataCellClientTemplateId="EditTemplateProjects" EditControlType="EditCommand"  DataCellCssClass="LastDataCellPostings" width="45" FixedWidth="True" Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="page" SortedDataCellCssClass="SortedDataCell" DataField="page"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="favorite" SortedDataCellCssClass="SortedDataCell" DataField="favorite"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>


<hr noshade width=100% size=1>
<br>
<table cellpadding=0 cellspacing=0 ><tr><td width="100%" nowrap  ><font size=1><b>Step 2:</b> Select the optional data to import from this project (Selecting no options will simply create the project with the name seen above).</font></td></tr></table> 
<br>
 <asp:CheckBox id="chkProjectPermissions"  Checked runat="server"></asp:CheckBox>Project Permissions&nbsp;&nbsp;&nbsp; <asp:CheckBox Checked id="chkProjectfolders" runat="server"></asp:CheckBox>Project Folders&nbsp;&nbsp;&nbsp; <asp:CheckBox id="Checkbox3" runat="server" Enabled=false></asp:CheckBox>Project Assets (Shortcuts)&nbsp;&nbsp;&nbsp; 
 <br><br>
 <hr noshade width=100% size=1><br>
<table cellpadding=0 cellspacing=0 ><tr><td width="100%" nowrap  ><font size=1><b>Step 3:</b> Click the "Apply Template" button to create the source project folders in your project in IDAM.</font></td></tr></table> 
<br>
<asp:Button id="btnApplyTemplate" runat="server" cssclass="button" Text="Apply Template!"></asp:Button>
<br><br>





</ComponentArt:PageView>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
	
	
	
			
	</ComponentArt:MultiPage>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
					<div style="text-align:right;"><br>
			<asp:Button id="btnProjectSave" runat="server" cssclass="button" Text="Save"></asp:Button><img src="images/spacer.gif" width=5 height=1><input type="button" class="button" onclick="<%if request.querystring("ID") <> "" then response.write ("window.parent.NavigateToProject('" & replace(request.querystring("id"),"PRJ","") & "')") %>;window.parent.CloseDialogWindowX();" value="Close" />
			</div>
	</div>
</div>

			
			
			
			

			</div>
			
			
			
			
			
			
			
			
			</div>
			

			
			</div>
			</div>
			
	<script language=javascript >
TabStripCategory.SelectTabById('<%response.write (request.querystring("tab"))%>');
function DoSearchFilter(searchvalue)
{
<%response.write (GridExternalProjects.ClientID)%>.Filter("clientname LIKE '%" + searchvalue + "%' OR name LIKE '%" + searchvalue + "%' OR city LIKE '%" + searchvalue + "%' OR state LIKE '%" + searchvalue + "%' OR oid LIKE '%" + searchvalue + "%'");
<%response.write (GridExternalProjects.ClientID)%>.Render();
}

function confirmApplyPermissions()
{
if (confirm('Are you sure you want to overwrite the permissions for all descendants?'))
	{
		document.getElementById("applypermissionsspinner").style.display="block";
		return true;
	}else{
		return false;
	}
}

</script>	
			
			
		</form>
	</body>
</HTML>
