<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.AdvancedSearch" CodeBehind="AdvancedSearch.ascx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<div class="previewpaneMyProjects ui-accordion-header ui-helper-reset ui-state-active " id="toolbarheading" style="z-index:99;border-top:0px;border-bottom:0px;">
    <!--Project cookie and top menu-->
    <table cellspacing="0" cellpadding="4" id="table2" width="100%">
        <tr>
            <td valign="top" width="1" nowrap>
                <!--<img style="BORDER-RIGHT:#cbcbcb 4px solid; BORDER-TOP:#cbcbcb 4px solid; BORDER-LEFT:#cbcbcb 4px solid; COLOR:#cbcbcb; BORDER-BOTTOM:#cbcbcb 4px solid; BACKGROUND-COLOR:#cbcbcb"
					src="images/search19rev.gif">-->
                <img src="images/ui/search_ico_bg.gif" height="42" width="42">
            </td>
            <td valign="top">
                <font face="Verdana" size="3"><b>
						<asp:Label id="lblheading" runat="server"></asp:Label></b></font><br><font face="Verdana" size="1">
						Advanced search enables you to access more detailed parameters for searching<br>for both assets and projects.  Choose the options below to filter your search results.
				</font>
            </td>
            <td id="Test" valign="top" align="right">
                <div style="padding-top: 2px">
                    <!--[ <a href="#">help</a> ]-->
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="3">
            </td>
        </tr>
    </table>
</div>
<!--end preview pane-->
<div class="previewpaneSubResults" style="z-index:1">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td align="left" valign="middle" width="100%" style="height: 20px">
            </td>
            <td align="left" width="200" style="height: 16px" nowrap>
                
            </td>
        </tr>
    </table>
</div>

<script type="text/javascript">

    function ExecuteSubmit()
    {
    document.forms[0].action="IDAM.aspx?page=Results";
    document.forms[0].submit();
    }
    
    function closealltags() {

        $("#JQADVSADx").accordion("option", "active", false);
        $("#JQADVSAUDFx").accordion("option", "active", false);
        $("#JQADVSATKx").accordion("option", "active", false);
        $("#JQADVSATTx").accordion("option", "active", false);
        $("#JQADVSATTTx").accordion("option", "active", false);
        $("#JQADVSASx").accordion("option", "active", false);
        $("#JQADVSPAOx").accordion("option", "active", false);
        $("#JQADVSPDx").accordion("option", "active", false);
        $("#JQADVSPUDFx").accordion("option", "active", false);
        $("#JQADVSPK1x").accordion("option", "active", false);
        $("#JQADVSPK2x").accordion("option", "active", false);
        $("#JQADVSPK3x").accordion("option", "active", false);

				
	
    }
    
    
    function openalltags()
    {


        $("#JQADVSADx").accordion("option", "active", 0);
        $("#JQADVSAUDFx").accordion("option", "active", 0);
        $("#JQADVSATKx").accordion("option", "active", 0);
        $("#JQADVSATTx").accordion("option", "active", 0);
        $("#JQADVSATTTx").accordion("option", "active", 0);
        $("#JQADVSASx").accordion("option", "active", 0);
        $("#JQADVSPAOx").accordion("option", "active", 0);
        $("#JQADVSPDx").accordion("option", "active", 0);
        $("#JQADVSPUDFx").accordion("option", "active", 0);
        $("#JQADVSPK1x").accordion("option", "active", 0);
        $("#JQADVSPK2x").accordion("option", "active", 0);
        $("#JQADVSPK3x").accordion("option", "active", 0);

        window.setTimeout('sizeCenterPane()', 2000);		
	
    }    

</script>

<script language="JavaScript" src="js/CalendarPopup.js"></script>
<div id="subtabset" style="position:relative;top:-24px;z-index:2; font-size:10px;">
<input type="radio" id="radio1" name="subtab" checked="checked" /><label for="radio1">General</label><img src="images/spacer.gif" width="2" alt="" />
</div>
<div style="border-top:1px solid silver;position:relative;top:-45px;z-index:1;margin-left:11px;"></div>
<div class="previewpaneSubResults" style="z-index:999;position:relative;top:-42px;margin-left:500px;">
<table  style="WIDTH:100%;" cellpadding="0" cellspacing="0" >
	<tr>
		<td align="right" style="FONT-SIZE: 10px; FONT-FAMILY: Verdana"><div style="padding-right:10px;">[ <a href="javascript:closealltags();">close 
				all</a> ] [ <a href="javascript:openalltags();">open all</a> ]</div></td>
	</tr>
</table>
</div>
<script>
    $(function () {
        $("#subtab").button();
        $("#subtabset").buttonset();
    });
	</script>

<div class="previewpaneProjects" id="toolbar" style="position:relative;top:-29px;">
    




    <!--END Project cookie and top menu-->
    <!--projectcentermain-->
    <div id="projectcentermainx" >












    <script>
        $(function () {
            $("#JQADVSADSEARx").accordion({
                autoHeight: false,
                navigation: true,
                collapsible: true,
                change: function (event, ui) {
                    $.cookie('JQADVSADSEARx', $("#JQADVSADSEARx").accordion("option", "active"));
                }
            });
        });
        $(document).ready(function () {
            if ($.cookie('JQADVSADSEARx') == "false") {
                $("#JQADVSADSEARx").accordion("option", "active", false);
            }
        });
</script>
<div id="JQADVSADSEARx" style="padding-bottom:5px;"> 
	<h3><a href="#section1">Search</a></h3> 


        
        <div class="SnapTreeviewTagx" >
            <div class="carouselshortlistx">
                <componentart:CallBack ID="CALLBACKInformation" runat="server" CacheContent="false">
                    <ClientEvents>
                        <CallbackError EventHandler="GenericCallback_onCallbackError" />
                    </ClientEvents>
                    <Content>
                        <asp:PlaceHolder ID="PlaceholderTagInformation" runat="server">
                            
                            Search String(s):<br>
                            <asp:TextBox ID="Migrated_keyword" TextMode="SingleLine" CssClass="InputFieldMain" runat="server" Style="width: 220px"></asp:TextBox><br>
                            <br>
                            <input class="preferences" type="radio" value="all" name="KeywordSearchType" checked>
                            with <b>all</b> of the words</FONT><br>
                            <input class="preferences" type="radio" value="exact" name="KeywordSearchType">
                            with the <b>exact phrase</b></FONT><br>
                            <input class="preferences" type="radio" value="atleastone" name="KeywordSearchType">
                            with <b>at least one</b> of the words</FONT><br>
                            <br>
                            <input type="button" Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:ExecuteSubmit();" value="Search"><br>
                            <br>
                            <div style="padding: 5px;">
                                <input class="preferences" type="radio" value="asset" name="SearchType" checked onclick="javascript:setoptions('assetoptions');">
                                Asset Options</FONT><br>
                                <input class="preferences" type="radio" value="project" name="SearchType" onclick="javascript:setoptions('projectoptions');">
                                Project Options</B></FONT><br>
                                <!--<input class="preferences" type="radio" value="people" name="SearchType" onclick="javascript:setoptions('peopleoptionsxx');">
								People Options</FONT>-->
                            </div>
                            <br>
                        </asp:PlaceHolder>
                    </Content>
                    <LoadingPanelClientTemplate>
                        <img height="16" src="images/spinner.gif" width="16" border="0">
                    </LoadingPanelClientTemplate>
                </componentart:CallBack>
            </div>
            <!--carouselshortlist-->
        </div>
        </div>




        <!--SnapTreeview-->
        <div id="assetoptions">
            <div class="preferencesx" >
            <br />
                <b>Search Options (Assets)</b><br /><br /></div>
            <div >
                <div id="AssetLeftColumn" style="vertical-align: top; ">
                    <ComponentArt:Snap id="TagSecurity" runat="server" Fillcontainer="false" Height="20px" MustBeDocked="true"
					DockingStyle="TransparentRectangle" CurrentDockingIndex="1" DraggingStyle="GhostCopy" CurrentDockingContainer="AssetLeftColumn"
					DockingContainers="AssetLeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false" AutoCallBackOnExpand="false" Visible=false>
					<Header>
						<div style="CURSOR: move; width: 100%;">
							<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagSecurity.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
									<td onmousedown="<% Response.Write(TagSecurity.ClientID) %>.StartDragging(event);" >Asset 
										Type</td>
								</tr>
							</table>
						</div>
					</Header>
					<CollapsedHeader>
						<div style="CURSOR: move; width: 100%;">
							<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagSecurity.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
									<td onmousedown="<% Response.Write(TagSecurity.ClientID) %>.StartDragging(event);">Asset 
										Type</td>
								</tr>
							</table>
						</div>
					</CollapsedHeader>
					<Content>
						<div class="SnapTreeviewTag" style="height:100%;background-color:white;">
							<div class="carouselshortlist">
								<asp:Literal ID="LiteralFileType" Runat="server"></asp:Literal>
								<br>
								<img src="images/spacer.gif" width="1" height="4" border="0"><br>
								<br>
								<br>
								<br>
								<input type="button" class="button" value="Search"><br>
								<br>
								<br>
							</div>
						</div> 
					</Content>
				</ComponentArt:Snap>
				<div class="SnapHeaderProjectsFiller"></div>
<script>
    $(function () {
        $("#JQADVSADx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQADVSADx', $("#JQADVSADx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQADVSADx') == "false") {
            $("#JQADVSADx").accordion("option", "active", false);
        }
    });
</script>
<div id="JQADVSADx" style="padding-bottom:5px;"> 
	<h3><a href="#section1">Asset Date</a></h3> 
                            <div class="SnapTreeviewTagx" >
                                <div class="carouselshortlistx">
                                    <br>
                                    <br>
                                    <div id="assetfilterdatetagx">
                                        Date type:<br>
                                        <select class="preferences" id="adtcreatemodify" name="adtcreatemodify">
                                            <option value="1" selected>Last Modified Date</option>
                                            <option value="0">Uploaded Date</option>
                                            <option value="2">Created Date</option>
                                        </select><br>
                                        <br>
                                        <b>
                                            <img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="#666666"><input class="preferences" type="radio" value="0" name="adt">in 
											the last <input type="text" size="9" name="adtmonth"> months<br>
										</font><b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="#666666"><input class="preferences" type="radio" value="1" name="adt">in 
											the last <input type="text" size="9" name="adtdays"> days<br>
										</font><b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="#666666"><input class="preferences" type="radio" value="2" name="adt">between 
											&nbsp; <input type="text" name="adtbetweendate" size="9"></font>

                                        <script language="JavaScript">
var cal_1 = new CalendarPopup();
cal_1.showYearNavigation();
                                        </script>

                                        <script language="JavaScript">
var cal_2 = new CalendarPopup();
cal_2.showYearNavigation();
                                        </script>

                                        <a href="#" onclick="cal_1.select(document.forms[0].adtbetweendate,'anchor1','MM/dd/yyyy'); return false;" name="anchor1" id="anchor1"><font face="Arial" size="1">select a date</font></a>
                                        <br>
                                        <b>
                                            <img border="0" src="images/spacer.gif" height="7" width="63"></b><font face="Arial" size="1" color="#666666">and&nbsp;&nbsp;
											<input type="text" name="adtanddate" size="9"> </font><a href="#" onclick="cal_2.select(document.forms[0].adtanddate,'anchor2','MM/dd/yyyy'); return false;" name="anchor2" id="anchor2"><font face="Arial" size="1">select a date</font></a><font face="Arial" size="1" color="#666666"><br>
										</font>
                                        <br>
                                        <br>
                                        <br>
                                        <input type="button" Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:ExecuteSubmit();" value="Search"><br>
                                        <br>
                                        <br>
                                    </div>
                                    <!--carouselshortlist-->
                                </div>
                                <!--SnapTreeview-->
                            </div>
                        </div>
                    <div class="SnapHeaderProjectsFiller">
                    </div>
<script>
$(function () {
    $("#JQADVSAUDFx").accordion({
        autoHeight: false,
        navigation: true,
        collapsible: true,
        change: function (event, ui) {
            $.cookie('JQADVSAUDFx', $("#JQADVSAUDFx").accordion("option", "active"));
        }
    });
});
$(document).ready(function () {
    if ($.cookie('JQADVSAUDFx') == "false") {
        $("#JQADVSAUDFx").accordion("option", "active", false);
    }
});
</script>
<div id="JQADVSAUDFx" style="padding-bottom:5px;"> 
	<h3><a href="#section1">Custom Fields</a></h3> 
                            <div class="SnapTreeviewTag" >
                                <div class="carouselshortlist">
                                    <componentart:CallBack ID="CALLBACKUDFMain" runat="server" CacheContent="false">
                                        <ClientEvents>
                                            <CallbackError EventHandler="GenericCallback_onCallbackError" />
                                        </ClientEvents>
                                        <Content>
                                            <asp:Literal ID="Literal_UDFMAIN" runat="server"></asp:Literal>
                                        </Content>
                                        <LoadingPanelClientTemplate>
                                            <img height="16" src="images/spinner.gif" width="16" border="0">
                                        </LoadingPanelClientTemplate>
                                    </componentart:CallBack>
                                    <br>
                                    <input type="button" Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:ExecuteSubmit();" value="Search"><br>
                                    <br>
                                    <br>
                                </div>
                                <!--carouselshortlist-->
                            </div>
                            <!--SnapTreeview-->
                        </div>
                    <div class="SnapHeaderProjectsFiller">
                    </div>
                   <script>
                       $(function () {
                           $("#JQADVSATKx").accordion({
                               autoHeight: false,
                               navigation: true,
                               collapsible: true,
                               change: function (event, ui) {
                                   $.cookie('JQADVSATKx', $("#JQADVSATKx").accordion("option", "active"));
                               }
                           });
                       });
                       $(document).ready(function () {
                           if ($.cookie('JQADVSATKx') == "false") {
                               $("#JQADVSATKx").accordion("option", "active", false);
                           }
                       });
</script>
<div id="JQADVSATKx" style="padding-bottom:5px;"> 
	<h3><a href="#section1"><asp:Literal ID=ltrlMediaTypeFilter runat=server></asp:Literal></a></h3> 
                            <div class="SnapTreeviewTagx" >
                                <div class="SnapProjectsSearchx">
                                    <asp:Literal ID="LiteralKeywords1Title" runat="server"></asp:Literal><br>
                                    <img src="images/spacer.gif" width="1" height="4" border="0"><br>
                                    <input class="preferences" type="radio" value="and" name="KeywordServicesSearchType" checked>
                                    And (assets matching all selected keywords)<br>
                                    <input class="preferences" type="radio" value="or" name="KeywordServicesSearchType">
                                    Or (assets matching any selected keywords)
                                    <br>
                                    <br>
                                    <div style="width: 100%; float: left;">
                                        <componentart:CallBack ID="CALLBACKKeywordsServices" runat="server" CacheContent="false">
                                            <ClientEvents>
                                                <CallbackError EventHandler="GenericCallback_onCallbackError" />
                                            </ClientEvents>
                                            <Content>
                                                <asp:Literal ID="LiteralKeywords1" runat="server"></asp:Literal>
                                            </Content>
                                            <LoadingPanelClientTemplate>
                                                <img height="16" src="images/spinner.gif" width="16" border="0">
                                            </LoadingPanelClientTemplate>
                                        </componentart:CallBack>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <div style="width: 100%; float: left;">
                                        <input type="button" Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:ExecuteSubmit();" value="Search"><br>
                                    </div>
                                    <br>
                                    <br>
                                </div>
                                <!--carouselshortlist-->
                            </div>
                            <!--SnapTreeview-->
                        </div>
                    <div class="SnapHeaderProjectsFiller">
                    </div>
                  <script>
                      $(function () {
                          $("#JQADVSATTx").accordion({
                              autoHeight: false,
                              navigation: true,
                              collapsible: true,
                              change: function (event, ui) {
                                  $.cookie('JQADVSATTx', $("#JQADVSATTx").accordion("option", "active"));
                              }
                          });
                      });
                      $(document).ready(function () {
                          if ($.cookie('JQADVSATTx') == "false") {
                              $("#JQADVSATTx").accordion("option", "active", false);
                          }
                      });
</script>
<div id="JQADVSATTx" style="padding-bottom:5px;"> 
	<h3><a href="#section1"><asp:Literal ID=ltrlIllustTypeFilter runat=server></asp:Literal></a></h3> 
                            <div class="SnapTreeviewTagx" >
                                <div class="SnapProjectsSearchx">
                                    <asp:Literal ID="LiteralKeywords2Title" runat="server"></asp:Literal><br>
                                    <input class="preferences" type="radio" value="and" name="KeywordMediaTypeSearchType" checked>
                                    And (assets matching all selected keywords)<br>
                                    <input class="preferences" type="radio" value="or" name="KeywordMediaTypeSearchType">
                                    Or (assets matching any selected keywords)
                                    <br>
                                    <br>
                                    <div style="width: 100%; float: left;">
                                        <componentart:CallBack ID="CALLBACKKeywordsMediaType" runat="server" CacheContent="false">
                                            <ClientEvents>
                                                <CallbackError EventHandler="GenericCallback_onCallbackError" />
                                            </ClientEvents>
                                            <Content>
                                                <asp:Literal ID="LiteralKeywords2" runat="server"></asp:Literal>
                                            </Content>
                                            <LoadingPanelClientTemplate>
                                                <img height="16" src="images/spinner.gif" width="16" border="0">
                                            </LoadingPanelClientTemplate>
                                        </componentart:CallBack>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <div style="width: 100%; float: left;">
                                        <input type="button" Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:ExecuteSubmit();" value="Search"><br>
                                    </div>
                                    <br>
                                    <br>
                                </div>
                                <!--carouselshortlist-->
                            </div>
                            <!--SnapTreeview-->
                        </div>
                    <div class="SnapHeaderProjectsFiller">
                    </div>
<script>
    $(function () {
        $("#JQADVSATTTx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQADVSATTTx', $("#JQADVSATTTx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQADVSATTTx') == "false") {
            $("#JQADVSATTTx").accordion("option", "active", false);
        }
    });
</script>
<div id="JQADVSATTTx" style="padding-bottom:5px;"> 
	<h3><a href="#section1"><asp:Literal ID=ltrlKeywordFilter runat=server></asp:Literal></a></h3> 
                            <div class="SnapTreeviewTagx" >
                                <div class="SnapProjectsSearchx">
                                    <asp:Literal ID="LiteralKeywords3Title" runat="server"></asp:Literal><br>
                                    <img src="images/spacer.gif" width="1" height="4" border="0"><br>
                                    <input class="preferences" type="radio" value="and" name="KeywordIllustTypeSearchType" checked>
                                    And (assets matching all selected keywords)<br>
                                    <input class="preferences" type="radio" value="or" name="KeywordIllustTypeSearchType">
                                    Or (assets matching any selected keywords)
                                    <br>
                                    <br>
                                    <div style="width: 100%; float: left;">
                                        <componentart:CallBack ID="CALLBACKKeywordsIllustType" runat="server" CacheContent="false">
                                            <ClientEvents>
                                                <CallbackError EventHandler="GenericCallback_onCallbackError" />
                                            </ClientEvents>
                                            <Content>
                                                <asp:Literal ID="LiteralKeywords3" runat="server"></asp:Literal>
                                            </Content>
                                            <LoadingPanelClientTemplate>
                                                <img height="16" src="images/spinner.gif" width="16" border="0">
                                            </LoadingPanelClientTemplate>
                                        </componentart:CallBack>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <div style="width: 100%; float: left;">
                                        <input type="button" Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:ExecuteSubmit();" value="Search"><br>
                                    </div>
                                    <br>
                                    <br>
                                </div>
                                <!--carouselshortlist-->
                            </div>
                            <!--SnapTreeview-->
                        </div>
                    <div class="SnapHeaderProjectsFiller">
                    </div>
                   <script>
                       $(function () {
                           $("#JQADVSASx").accordion({
                               autoHeight: false,
                               navigation: true,
                               collapsible: true,
                               change: function (event, ui) {
                                   $.cookie('JQADVSASx', $("#JQADVSASx").accordion("option", "active"));
                               }
                           });
                       });
                       $(document).ready(function () {
                           if ($.cookie('JQADVSASx') == "false") {
                               $("#JQADVSASx").accordion("option", "active", false);
                           }
                       });
</script>
<div id="JQADVSASx" style="padding-bottom:5px;"> 
	<h3><a href="#section1">Asset Size</a></h3> 
                            <div class="SnapTreeviewTag" >
                                <div class="carouselshortlist">
                                    <div id="assetfiltersizetag">
                                        <font face="Arial" size="1" color="#666666">
											<br>
										</font><b>
                                            <img border="0" src="images/spacer.gif" height="7" width="21"></b><select size="1" class="preferences" name="asizeleastmost">
                                                <option value="1">at least</option>
                                                <option value="0">at most</option>
                                            </select><font face="Arial" size="1" color="#666666"> <input type="text" size="9" name="asize">
											KB<br>
											<br>
										</font>
                                    </div>
                                    <br>
                                    <br>
                                    <input type="button" Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:ExecuteSubmit();" value="Search"><br>
                                    <br>
                                    <br>
                                </div>
                                <!--carouselshortlist-->
                            </div>
                            <!--SnapTreeview-->
                        </div>
                    <div class="SnapHeaderProjectsFiller">
                    </div>
                </div>
            </div>
            <br>
      
        </div>
        <!--asset options-->
        <div id="projectoptions">
            <div class="preferences" >
                <b>Search Options (Projects)</b><br /><br /></div>
            <div >
                <div id="ProjectLeftColumn" style="vertical-align: top; height: 2500px">
                   <script>
                       $(function () {
                           $("#JQADVSPAOx").accordion({
                               autoHeight: false,
                               navigation: true,
                               collapsible: true,
                               change: function (event, ui) {
                                   $.cookie('JQADVSPAOx', $("#JQADVSPAOx").accordion("option", "active"));
                               }
                           });
                       });
                       $(document).ready(function () {
                           if ($.cookie('JQADVSPAOx') == "false") {
                               $("#JQADVSPAOx").accordion("option", "active", false);
                           }
                       });
</script>
<div id="JQADVSPAOx" style="padding-bottom:5px;"> 
	<h3><a href="#section1">Project Address</a></h3> 
                            <div class="SnapTreeviewTag" >
                                <div class="carouselshortlist">
                                    <br>
                                    <br>
                                    <b>
                                        <img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="">Address<u><br>
										</u></font><b><img border="0" src="images/spacer.gif" height="7" width="45"></b><font face="Arial" size="1" color="#666666"><input type="text" class="InputFieldMain" size="30" name="Address"></font><u><font face="Arial" size="1" color="#CC6600"><br>
										</font></u><b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="">City<u><br>
										</u></font><b><img border="0" src="images/spacer.gif" height="7" width="45"></b><font face="Arial" size="1" color="#666666"><input type="text" class="InputFieldMain" size="30" name="City"></font><u><font face="Arial" size="1" color="#CC6600"><br>
										</font></u><b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="">State<u><br>
										</u></font><b><img border="0" src="images/spacer.gif" height="7" width="45"></b>
                                    <asp:DropDownList Style="border: 1px dotted; padding: 5px;" ID="DropDownList_State" DataTextField="name" DataValueField="state_id" runat="server">
                                        <asp:ListItem Value="" Selected="true">Select a State</asp:ListItem>
                                    </asp:DropDownList>
                                    <u><font face="Arial" size="1" color="#CC6600">
											<br>
										</font></u><b>
                                            <img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="">Zip<u><br>
										</u></font><b><img border="0" src="images/spacer.gif" height="7" width="45"></b><font face="Arial" size="1" color="#666666"><input type="text" class="InputFieldMain" size="9" name="Zip"><br>
										<asp:Literal ID="IDAM_COUNTRY" Runat=server Visible=False ></asp:Literal>
									</font>
                                    <br>
                                    <input type="button" Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:ExecuteSubmit();" value="Search"><br>
                                    <br>
                                </div>
                                <!--SnapTreeview-->
                            </div>
                       </div>
                    <div class="SnapHeaderProjectsFiller">
                    </div>
                    <script>
                        $(function () {
                            $("#JQADVSPDx").accordion({
                                autoHeight: false,
                                navigation: true,
                                collapsible: true,
                                change: function (event, ui) {
                                    $.cookie('JQADVSPDx', $("#JQADVSPDx").accordion("option", "active"));
                                }
                            });
                        });
                        $(document).ready(function () {
                            if ($.cookie('JQADVSPDx') == "false") {
                                $("#JQADVSPDx").accordion("option", "active", false);
                            }
                        });
</script>
<div id="JQADVSPDx" style="padding-bottom:5px;"> 
	<h3><a href="#section1">Project Date</a></h3> 
                            <div class="SnapTreeviewTag" >
                                <div class="carouselshortlist">
                                    <br>
                                    Date type:<br>
                                    <select class="preferences" id="pdtcreatemodify" name="pdtcreatemodify">
                                        <option value="1" selected>Last Modified Date</option>
                                        <option value="0">Uploaded Date</option>
                                        <option value="2">Created Date</option>
                                    </select><br>
                                    <br>
                                    <b>
                                        <img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="#666666"><input class="preferences" type="radio" value="0" name="pdt">in 
										the last <input type="text" size="9" name="pdtmonth"> months<br>
									</font><b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="#666666"><input class="preferences" type="radio" value="1" name="adt">in 
										the last <input type="text" size="9" name="pdtdays"> days<br>
									</font><b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="#666666"><input class="preferences" type="radio" value="2" name="adt">between 
										&nbsp; <input type="text" name="pdtbetweendate" size="9"></font>

                                    <script language="JavaScript">
var cal_3 = new CalendarPopup();
cal_3.showYearNavigation();
                                    </script>

                                    <script language="JavaScript">
var cal_4 = new CalendarPopup();
cal_4.showYearNavigation();
                                    </script>

                                    <a href="#" onclick="cal_3.select(document.forms[0].pdtbetweendate,'anchor3','MM/dd/yyyy'); return false;" name="anchor3" id="anchor3"><font face="Arial" size="1">select a date</font></a>
                                    <br>
                                    <b>
                                        <img border="0" src="images/spacer.gif" height="7" width="63"></b><font face="Arial" size="1" color="#666666">and&nbsp;&nbsp;
										<input type="text" name="pdtanddate" size="9"> </font><a href="#" onclick="cal_4.select(document.forms[0].pdtanddate,'anchor4','MM/dd/yyyy'); return false;" name="anchor4" id="anchor4"><font face="Arial" size="1">select a date</font></a><font face="Arial" size="1" color="#666666"><br>
									</font>
                                    <br>
                                    <br>
                                    <br>
                                    <input type="button" Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:ExecuteSubmit();" value="Search"><br>
                                    <br>
                                    <br>
                                </div>
                                <!--SnapTreeview-->
                            </div>
                        </div>
                    <div class="SnapHeaderProjectsFiller">
                    </div>
                    <script>
                        $(function () {
                            $("#JQADVSPUDFx").accordion({
                                autoHeight: false,
                                navigation: true,
                                collapsible: true,
                                change: function (event, ui) {
                                    $.cookie('JQADVSPUDFx', $("#JQADVSPUDFx").accordion("option", "active"));
                                }
                            });
                        });
                        $(document).ready(function () {
                            if ($.cookie('JQADVSPUDFx') == "false") {
                                $("#JQADVSPUDFx").accordion("option", "active", false);
                            }
                        });
</script>
<div id="JQADVSPUDFx" style="padding-bottom:5px;"> 
	<h3><a href="#section1">Custom Fields</a></h3> 
                            <div class="SnapTreeviewTag" >
                                <div class="carouselshortlist">
                                    <componentart:CallBack ID="Callback1" runat="server" CacheContent="false">
                                        <ClientEvents>
                                            <CallbackError EventHandler="GenericCallback_onCallbackError" />
                                        </ClientEvents>
                                        <Content>
                                            <asp:Literal ID="Literal_UDFPMAIN" runat="server"></asp:Literal>
                                        </Content>
                                        <LoadingPanelClientTemplate>
                                            <img height="16" src="images/spinner.gif" width="16" border="0">
                                        </LoadingPanelClientTemplate>
                                    </componentart:CallBack>
                                    <br>
                                    <input type="button" Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:ExecuteSubmit();" value="Search"><br>
                                    <br>
                                    <br>
                                </div>
                                <!--carouselshortlist-->
                            </div>
                            <!--SnapTreeview-->
                        </div>
                    <div class="SnapHeaderProjectsFiller">
                    </div>
                   <script>
                       $(function () {
                           $("#JQADVSPK1x").accordion({
                               autoHeight: false,
                               navigation: true,
                               collapsible: true,
                               change: function (event, ui) {
                                   $.cookie('JQADVSPK1x', $("#JQADVSPK1x").accordion("option", "active"));
                               }
                           });
                       });
                       $(document).ready(function () {
                           if ($.cookie('JQADVSPK1x') == "false") {
                               $("#JQADVSPK1x").accordion("option", "active", false);
                           }
                       });
</script>
<div id="JQADVSPK1x" style="padding-bottom:5px;"> 
	<h3><a href="#section1"><asp:Literal id=LiteralKeywordsP1aTitle runat=server></asp:Literal></a></h3> 
                            <div class="SnapTreeviewTagx" >
                                <div class="SnapProjectsSearchx">
                                    <img src="images/spacer.gif" width="188" height="1"><br>
                                    <div style="padding: 5px">
                                        <div style="width: 100%; float: left;">
                                            <input class="preferences" type="radio" value="and" name="KeywordDisciplineSearchType" checked>
                                            And (projects matching all selected keywords)<br>
                                            <input class="preferences" type="radio" value="or" name="KeywordDisciplineSearchType">
                                            Or (projects matching any selected keywords)
                                            <br>
                                            <br>
                                            <div style="width: 100%; float: left;">
                                                <componentart:CallBack ID="CALLBACKKeywordsDiscipline" runat="server" CacheContent="false">
                                                    <ClientEvents>
                                                        <CallbackError EventHandler="GenericCallback_onCallbackError" />
                                                    </ClientEvents>
                                                    <Content>
                                                        <asp:Literal ID="LiteralKeywordsP1" runat="server"></asp:Literal>
                                                    </Content>
                                                    <LoadingPanelClientTemplate>
                                                        <img height="16" src="images/spinner.gif" width="16" border="0">
                                                    </LoadingPanelClientTemplate>
                                                </componentart:CallBack>
                                            </div>
                                            <div style="width: 100%; float: left;">
                                                <br>
                                                <br>
                                                <input type="button" Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:ExecuteSubmit();" value="Search"><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--padding-->
                        </div>
                      </div>
                    <div class="SnapHeaderProjectsFiller">
                    </div>
                    <script>
                        $(function () {
                            $("#JQADVSPK2x").accordion({
                                autoHeight: false,
                                navigation: true,
                                collapsible: true,
                                change: function (event, ui) {
                                    $.cookie('JQADVSPK2x', $("#JQADVSPK2x").accordion("option", "active"));
                                }
                            });
                        });
                        $(document).ready(function () {
                            if ($.cookie('JQADVSPK2x') == "false") {
                                $("#JQADVSPK2x").accordion("option", "active", false);
                            }
                        });
</script>
<div id="JQADVSPK2x" style="padding-bottom:5px;"> 
	<h3><a href="#section1"><asp:Literal ID=LiteralKeywordsP2aTitle runat=server></asp:Literal></a></h3> 
                            <div class="SnapTreeviewTagx" >
                                <div class="SnapProjectsSearchx">
                                    <img src="images/spacer.gif" width="188" height="1"><br>
                                    <div style="padding: 5px">
                                        <div style="width: 100%; float: left;">
                                            <input class="preferences" type="radio" value="and" name="KeywordKeywordsSearchType" checked>
                                            And (projects matching all selected keywords)<br>
                                            <input class="preferences" type="radio" value="or" name="KeywordKeywordsSearchType">
                                            Or (projects matching any selected keywords)
                                            <br>
                                            <br>
                                            <div style="width: 100%; float: left;">
                                                <componentart:CallBack ID="Callback2" runat="server" CacheContent="false">
                                                    <ClientEvents>
                                                        <CallbackError EventHandler="GenericCallback_onCallbackError" />
                                                    </ClientEvents>
                                                    <Content>
                                                        <asp:Literal ID="LiteralKeywordsP2" runat="server"></asp:Literal>
                                                    </Content>
                                                    <LoadingPanelClientTemplate>
                                                        <img height="16" src="images/spinner.gif" width="16" border="0">
                                                    </LoadingPanelClientTemplate>
                                                </componentart:CallBack>
                                            </div>
                                            <div style="width: 100%; float: left;">
                                                <br>
                                                <br>
                                                <input type="button" Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:ExecuteSubmit();" value="Search"><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--padding-->
                        </div></div>
                    <div class="SnapHeaderProjectsFiller">
                    </div>
                    <script>
                        $(function () {
                            $("#JQADVSPK3x").accordion({
                                autoHeight: false,
                                navigation: true,
                                collapsible: true,
                                change: function (event, ui) {
                                    $.cookie('JQADVSPK3x', $("#JQADVSPK3x").accordion("option", "active"));
                                }
                            });
                        });
                        $(document).ready(function () {
                            if ($.cookie('JQADVSPK3x') == "false") {
                                $("#JQADVSPK3x").accordion("option", "active", false);
                            }
                        });
</script>
<div id="JQADVSPK3x" style="padding-bottom:5px;"> 
	<h3><a href="#section1"><asp:Literal ID=LiteralKeywordsP3aTitle runat=server></asp:Literal></a></h3> 
                            <div class="SnapTreeviewTagx" >
                                <div class="SnapProjectsSearchx">
                                    <img src="images/spacer.gif" width="188" height="1"><br>
                                    <div style="padding: 5px">
                                        <div style="width: 100%; float: left;">
                                            <input class="preferences" type="radio" value="and" name="KeywordOfficeSearchType" checked>
                                            And (projects matching all selected keywords)<br>
                                            <input class="preferences" type="radio" value="or" name="KeywordOfficeSearchType">
                                            Or (projects matching any selected keywords)
                                            <br>
                                            <br>
                                            <div style="width: 100%; float: left;">
                                                <componentart:CallBack ID="Callback3" runat="server" CacheContent="false">
                                                    <ClientEvents>
                                                        <CallbackError EventHandler="GenericCallback_onCallbackError" />
                                                    </ClientEvents>
                                                    <Content>
                                                        <asp:Literal ID="LiteralKeywordsP3" runat="server"></asp:Literal>
                                                    </Content>
                                                    <LoadingPanelClientTemplate>
                                                        <img height="16" src="images/spinner.gif" width="16" border="0">
                                                    </LoadingPanelClientTemplate>
                                                </componentart:CallBack>
                                            </div>
                                            <div style="width: 100%; float: left;">
                                                <br>
                                                <br>
                                                <input type="button" Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:ExecuteSubmit();" value="Search"><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--padding-->
                        </div>
                        </div>
                    <div class="SnapHeaderProjectsFiller">
                    </div>
                </div>
            </div>
            <br/>
        </div>
        <!--Project options-->
        <br/>
      
    </div>
    
</div>

<script language="javascript" type="text/javascript">
//closealltags();
function setoptions(page) {

    $('#assetoptions').hide('fast', function () {
        // Animation complete.
    });
    $('#projectoptions').hide('fast', function () {
        // Animation complete.
    });
    $('#'+page).show('slow', function () {
        // Animation complete.
    });
 


    window.setTimeout('sizeCenterPane()', 2000);

}

</script>

