<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UploadFolders.aspx.vb" Inherits="IdamAdmin.UploadFolders" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title></title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">


		<style>BODY { MARGIN: 0px; font-family:Verdana; font-size:11px; }
	       
	</style>
</head>
<body bgcolor="white">
    <form id="form1" runat="server">
    <div style="PADDING-RIGHT:10px; PADDING-LEFT:10px; PADDING-BOTTOM:10px; PADDING-TOP:10px;  POSITION:relative; ">
    
    <div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Select the folder to upload into.</div>
    <div style="text-align:center;"><br>
Project: [ <b><asp:label id="LabelProjectName" runat="server">Label</asp:label></b> ]<br></div>
        <b>Available Preferred Folders</b><div style="border:1px solid #B7B4B4;padding:10px;">
            <div style="height: 350px; overflow-x: hidden; overflow-y: auto;">
                <asp:Literal ID="nofoldersavailable" runat="server"></asp:Literal>
                <asp:Repeater ID="folderlist" runat="server">
                    <ItemTemplate>
            <tr>
            <td class="style2" style="color: #FF0000">
            </td>
            <td class="style8">
            <input type=button <%#isbuttonactive(CType(DataBinder.Eval(Container.DataItem, "ID"), String))%> onclick="selectpath(<%#DataBinder.Eval(Container.DataItem, "ID") %>);"  value="Upload Here!"/>
            </td>
            <td><img src="images/ui/categorytype0.gif" /></td>
            <td class="style3" nowrap>
            <%#renderpath(CType(DataBinder.Eval(Container.DataItem, "ID"), String))%>&nbsp;&nbsp;<font style="color: #FF0000"><%#isnew(CType(DataBinder.Eval(Container.DataItem, "ID"), String))%></font></td>
            
            </tr>
        </ItemTemplate>
        <FooterTemplate>
        </table>
        </FooterTemplate>
        <HeaderTemplate>
        <table class="style1">
        </HeaderTemplate>
        </asp:repeater> 
        
            
            
        
        </div>
        </div>
        
          <div style="text-align:center;">
        <br />

<table><tr><td><asp:Button ID="btnUploadCurrentFolder" runat="server" Text="Upload To Current Folder" /></td><td><img src="images/ui/categorytype0.gif" /></td><td><asp:label id="LabelCategoryName" runat="server">Label</asp:label> </td></tr></table>
<br></div>
        
        
    
        <!--<asp:CheckBox ID="CheckBox1" runat="server" Text="Do not show this dialog box again." />-->
        <br />
<COMPONENTART:CALLBACK id="CallbackUploadFolders" runat="server" CacheContent="false" >
<CONTENT>
<asp:Literal ID=LiteralUploadFolders runat=server></asp:Literal>
</CONTENT>
</COMPONENTART:CALLBACK>
    
    </div>
    <div style="text-align:center;">
        &nbsp;&nbsp;<!--<input type="button" onclick="window.parent.CloseDialogWindowX();" ID="ButtonCancel"  value="Cancel" />--></div>
        <script type="text/javascript" language=javascript>

            function selectpath(id) {

                CallbackUploadFolders.Callback(id);
            }
        </script>
    </form>
</body>
</html>
