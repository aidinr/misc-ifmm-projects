<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.BrowserCheckJava" CodeBehind="BrowserCheckJava.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>BrowserCheckJava</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <meta http-equiv="refresh" content="1;url=IDAM.aspx?Java=False&r=<%=server.urlencode(request.querystring("r"))%>">
  </head>
  <body>

    <form id="Form1" method="post" runat="server">



<p align="center">





<!--
<APPLET id="VersionRedirector" CODE="VersionRedirector.class" WIDTH="1" HEIGHT="1"  VIEWASTEXT>
	<param name="URL" value="IDAM.aspx">
</APPLET>
-->









<script type="text/javascript" src="includes/CookieScripts.js"></script>
<script language=Javascript>





function GetVersion()
{
try
{
    var sVersion = document.VersionRedirector.getVersion();
    var sVendor = document.VersionRedirector.getVendor();
    if (sVersion != '')
	{
	SetCookie('BrowserCheckJava', 'True', exp);
	SetCookie('BrowserCheckJavaVersion', sVersion, exp);
	SetCookie('BrowserCheckJavaVendor', sVendor, exp);
	window.location ="IDAM.aspx?version=" + sVersion + "&vendor=" + sVendor + "&r=<%=server.urlencode(request.querystring("r"))%>"
	}else{
	SetCookie('BrowserCheckJava', 'false', exp);
	window.location ="IDAM.aspx?Java=False&r=<%=server.urlencode(request.querystring("r"))%>"
	}
}
catch(error)
{
	SetCookie('BrowserCheckJava', 'false', exp);
	window.location ="IDAM.aspx?Java=False&r=<%=server.urlencode(request.querystring("r"))%>"
}

    
}
//GetVersion();
</script>


<br>
<FONT face="Arial" size="1">Checking Java compatibility...
								<br>
								<BR>
								<img border="0" src="images/loading.gif" width="78" height="7"><br>
<BR>
Note: Please allow 10 seconds for the Java Version Check Applet to load and 
redirect to the homepage.
<br>
If the applet cannot load then the page will automatically redirect within 10 
seconds.<br>
If you do not wish to wait then please click <a href="IDAM.aspx?java=false">here</a> to forward to the homepage 
with Java disabled.</FONT><br>



<script language="JavaScript" type="text/javascript" src="js/NoIEActivate.js"></script>

    </form>

  </body>
</html>
