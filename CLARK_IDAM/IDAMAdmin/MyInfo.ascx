<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.MyInfo" CodeBehind="MyInfo.ascx.vb" %>
<script src="js/password.js" type="text/javascript"></script>
<br />
<fieldset>

<legend ><b>Edit Profile</b></legend>
    <div style="padding:10px;">
    <font color=red><asp:Literal ID="ltrlnotification" runat="server"></asp:Literal></font>
	<table style="width: 100%">


         <tr>
        <td style="height: 26px">
            Email
        </td>
        <td style="height: 26px">
            <strong>
                <asp:TextBox ID="myinfo_Email" runat="server" Width="200px"></asp:TextBox>
            </strong>
        </td>
    </tr>


    <tr>
        <td style="height: 26px">
            Title
        </td>
        <td style="height: 26px">
            <strong>
                <asp:TextBox ID="myinfo_Position" runat="server" Width="200px"></asp:TextBox>
            </strong>
        </td>
    </tr>
    <tr>
        <td>
            Address
        </td>
        <td>
            <asp:TextBox ID="myinfo_WorkAddress" runat="server" Width="200px" Text=""></asp:TextBox>
               
        </td>
    </tr>
    <tr>
        <td style="height: 26px">
            City
        </td>
        <td style="height: 26px">
            <strong>
                <asp:TextBox ID="myinfo_WorkCity" runat="server" CausesValidation="True" Width="200px"></asp:TextBox>
                    
            </strong>
        </td>
    </tr>
    <tr>
        <td style="height: 26px">
            State
        </td>
        <td style="height: 26px">
           <asp:DropDownList DataTextField="name" DataValueField="state_id" id="myinfo_Workstate" Height=25px  runat="server"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="height: 26px">
            Zip
        </td>
        <td style="height: 26px">
            <strong>
                <asp:TextBox ID="myinfo_WorkZip" runat="server" CausesValidation="True" Width="200px"></asp:TextBox>
                   
            </strong>
        </td>
    </tr>
    <tr>
        <td style="height: 26px">
            Phone #
        </td>
        <td style="height: 26px">
            <strong>
                <asp:TextBox ID="myinfo_Phone" runat="server" CausesValidation="True" Width="200px"></asp:TextBox>
                   
            </strong>
        </td>
    </tr>
     <tr>
        <td style="height: 26px">
            Cell #
        </td>
        <td style="height: 26px">
            <strong>
                <asp:TextBox ID="myinfo_cell" runat="server" CausesValidation="True" Width="200px"></asp:TextBox>
                   
            </strong>
        </td>
    </tr>
   
</table>
<br />


<input type="submit" id="btnmodifyacc" value="Update Profile"  onclick="SubmitProfile();" class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" />
    </div>
</fieldset>
<br />

<fieldset>
<legend ><b>Edit Password</b></legend>
    <div style="padding:10px;">
        
	<table style="width: 100%">
    <tr>
        <td style="height: 26px">
            Existing Password
        </td>
        <td style="height: 26px">
            <strong>
                <asp:TextBox ID="myinfo_existingpassword" TextMode="Password" runat="server" CausesValidation="True"
                    Width="150px"></asp:TextBox>
            </strong>
        </td>
    </tr>
    <tr>
        <td>
            New Password
        </td>
        <td>
            <asp:TextBox ID="myinfo_newpassword" runat="server" TextMode="Password" Width="150px"
                Text=""></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 26px">
            Re-Enter Password
        </td>
        <td style="height: 26px">
            <strong>
                <asp:TextBox ID="myinfo_newpassword2" TextMode="Password" runat="server" CausesValidation="True"
                    Width="150px"></asp:TextBox>
            </strong>
        </td>
    </tr>
    <tr>
        <td style="height: 26px">
            Password Strength
        </td>
        <td style="height: 26px">
            <div style="border: 0px solid gray; width: 154px;">
                <div id="progressBar" style="font-size: 1px; height: 20px; width: 0px; border: 1px solid white;">
                </div>
            </div>
        </td>
    </tr>
</table>
<br />
    <input type="submit" id="Submit1" value="Update Password"  onclick="SubmitPassword();" class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" />
    </div>
</fieldset>


<script type="text/javascript">
var _prefix = '<asp:Literal ID="ltrlprefix" runat="server"></asp:Literal>';
function SubmitProfile() {
    CallbackGenericDialog.Callback("MyInfo", "UPDATEPROFILE", _prefix, $("input:text").serialize() + "&" + $("select").serialize());
}
function SubmitPassword() {
    CallbackGenericDialog.Callback("MyInfo", "UPDATEPASSWORD", _prefix, $("input:password").serialize());
}
</script>