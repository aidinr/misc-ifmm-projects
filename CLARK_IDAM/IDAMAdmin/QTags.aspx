﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="QTags.aspx.vb" Inherits="IdamAdmin.QTags" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     Add a Tag
    <table cellpadding="0" cellspacing="0">
      <tr>
        <td>
          <ComponentArt:CallBack ID="CALLBACKComboBoxTagging" runat="server" CacheContent="false"
          LoadingPanelFadeMaximumOpacity="20" LoadingPanelFadeDuration="20">
            <Content>
              <ComponentArt:ComboBox ID="SNAPTAGGINGComboBoxTagging" runat="server"
              RunningMode="CallBack" AutoHighlight="false" AutoComplete="true" AutoFilter="true" CssClass="comboBox" HoverCssClass="comboBoxHover"
              FocusedCssClass="comboBoxHover"
              TextBoxCssClass="comboTextBox" TextBoxHoverCssClass="comboBoxHover" DropDownCssClass="comboDropDown"
              ItemCssClass="comboItem" ItemHoverCssClass="comboItemHover" SelectedItemCssClass="comboItemHover"
              DropHoverImageUrl="images/drop_hover.gif" DropImageUrl="images/drop.gif" DropDownPageSize="13" Width="200" Visible="False">
              </ComponentArt:ComboBox>
            </Content>
          </ComponentArt:CallBack>
        </td>
        <td>
          <div style="padding-left:10px";>
            [ <a href="javascript:IDAMTagAction('AddTag|'+SNAPTAGGINGComboBoxTagging.get_text());">add</a> ]
          </div>
        </td>
      </tr>
    </table>
    <br />
    <div style="height: 280px; margin-bottom: -10px; overflow: auto">
      <ComponentArt:CallBack ID="SNAPTAGGINGCallbackTags" runat="server" CacheContent="false" LoadingPanelFadeMaximumOpacity="20" LoadingPanelFadeDuration="20">
        <Content>
          <asp:Literal ID="SNAPTAGGINGLtrlTagListing" runat="server" Text="No tags available for selected assets." Visible="True"></asp:Literal>
          <asp:Repeater ID="SNAPTAGGINGRptrTagListing" runat="server">
            <ItemTemplate>
              <a border="0" href="javascript:IDAMTagAction('Modify|"
                <%#DataBinder.Eval(Container.DataItem, "tagstatus")%>
                  |<%#DataBinder.Eval(Container.DataItem, "tagid")%>
                    |<%#DataBinder.Eval(Container.DataItem, "ids")%>
                      ');">
                      <img border="0" src="images/"<%#DataBinder.Eval(Container.DataItem, "tagstatus")%>.gif" />
              </a>&nbsp;&nbsp;<a href="javascript:IDAMTagSearch('"
                <%#DataBinder.Eval(Container.DataItem, "tagname")%>
                  ');"><%#DataBinder.Eval(Container.DataItem, "tagname")%>
              </a><br>
                            
            </ItemTemplate>
            <HeaderTemplate>
            </HeaderTemplate>
            <FooterTemplate>
            </FooterTemplate>
          </asp:Repeater>
    </div>
    </form>
</body>
</html>
