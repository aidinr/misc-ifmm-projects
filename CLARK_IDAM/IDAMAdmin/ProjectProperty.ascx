﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProjectProperty.ascx.vb" Inherits="IdamAdmin.ProjectProperty" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<script type="text/javascript">
    function onCallbackError(e) {

        alert(e);
    }
</script>
<style type="text/css">
.InputFieldMain 
{
    margin:2px;
}
a 
{
    text-decoration: none;
}
a:hover 
{
    text-decoration: underline;
}
</style>
<script language=javascript>
    function onPage(newPage) {
        return true;
    }
    
    function editGridPopup(rowId)
  {
	var rowdata;
	var userid;
	rowdata = <% Response.Write(GridSpaces.ClientID) %>.GetRowFromClientId(rowId);
	space_id = rowdata.GetMember('space_id').Value;
	projectid = rowdata.GetMember('projectid').Value;
    Object_PopUp_Space(space_id,projectid); 
  }
  
      function newGridPopup()
  {
    Object_PopUp_SpaceNew(<% Response.Write(request.querystring("id")) %>); 
  }
  
    function deleteRow(rowId)
  {
    <% Response.Write(GridSpaces.ClientID) %>.Delete(<% Response.Write(GridSpaces.ClientID) %>.GetRowFromClientId(rowId)); 
  }


</script>
	<div class="PageContent" style="padding:10px;">
		    
		[ <a href="javascript:newGridPopup();" >Add new space</a>  ]
		<br /><br />
		
<COMPONENTART:GRID 
id="GridSpaces" 
runat="server" 

AutoFocusSearchBox="false"
AutoCallBackOnInsert="true"
AutoCallBackOnUpdate="true"
AutoCallBackOnDelete="true"

ScrollBar="Off"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"

ScrollPopupClientTemplateId="ScrollPopupTemplate" 
Height="10" Width="100%"
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
LoadingPanelFadeDuration="200"
LoadingPanelFadeMaximumOpacity="95"
LoadingPanelEnabled="true"
EnableViewState="true"
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRowCategory" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="20" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="True" 
GroupingPageSize="5" 
PagerTextCssClass="GridFooterTextCategory" 



GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeader" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="true" 
AllowSorting="False"
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
ScrollBarWidth="1" 
allowtextselection="false"
AllowPaging="false" 
 PageSize="100">
<ClientEvents>
  </ClientEvents>
<ClientTemplates>
<ComponentArt:ClientTemplate Id="LookupProjectTemplate">

</ComponentArt:ClientTemplate>
<ComponentArt:ClientTemplate Id="ActionProjectTemplate">


            <a href="javascript:editGridPopup('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border="0" > </a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>




</ComponentArt:ClientTemplate>
<componentart:ClientTemplate ID="LoadingFeedbackTemplate"> 
<table height="1px" width="100%" ><tr><td valign="center" align="center">
<table cellspacing="0" cellpadding="0" border="0"  background="images/loadingback.gif" height="70px" width="300px">
<tr>
<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
<td style="font-size:10px;font-family:Verdana;" width="60px" align="right">Loading...&nbsp;</td>
<td><img src="images/spinner2.gif"  border="0"></td>
<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
</tr>
</table>
</td></tr></table>
</componentart:ClientTemplate>                            
</ClientTemplates>


<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
EditFieldCssClass="EditDataField"
EditCommandClientTemplateId="EditCommandTemplate"
InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="space_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asch10.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="desch10.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="10" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" Align="Center" AllowSorting="false" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplate" HeadingCellCssClass="FirstHeadingCell" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Name" AllowEditing="false" Width="200" TextWrap=true  SortedDataCellCssClass="SortedDataCell" DataField="suite" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Size" AllowEditing="false" Width="90"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  DataField="SF"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Space ID" AllowEditing="false" visible="false" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  DataField="space_id"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Tenant" AllowEditing="false" Width="200"  SortedDataCellCssClass="SortedDataCell" DataField="shortname" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Space Asset ID"   SortedDataCellCssClass="SortedDataCell" DataField="asset_id" Visible="false" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Space Asset ID"   SortedDataCellCssClass="SortedDataCell" DataField="projectid" Visible="false" ></componentart:GridColumn>
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" Align="Left" AllowSorting="false" AllowEditing="false" DataCellClientTemplateId="ActionProjectTemplate" HeadingCellCssClass="FirstHeadingCell" HeadingText="Action" HeadingImageWidth="14" HeadingImageHeight="16" width="80" AllowGrouping="false"/>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>

<br /><br />

<table width="100%" border="0">
		    <tr>
		        <td width="100">GLA</td>
		        <td><asp:TextBox id="GLA"   Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		    </tr>
		    <tr>
		        <td width="100">Parking</td>
		        <td ><asp:TextBox id="Parking"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		    </tr>
		    <tr>
		        <td width="100">Traffic</td>
		        <td ><asp:TextBox id="Parking_Ratio"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		    </tr>
		    <tr>
		        <td colspan="2">
		        <table width="100%" cellpadding="0" cellspacing="0">
		            <tr>
		            <td width="88">Demographics</td>
		            <td><hr /></td>
		            </tr>
		        </table>
		        </td>
		    </tr>
		    <tr>
		        <td width="100" valign="top"></td>
		        <td>
		            <table cellpadding="0" cellspacing="0">
		                <tr>
		                        <td></td>
		                        <td><asp:TextBox id="p3miletitle"  Width="15px" CssClass="InputFieldMain" runat="server" Text="3"></asp:TextBox> Miles</td>
		                        <td><asp:TextBox id="p5miletitle"  Width="15px" CssClass="InputFieldMain" runat="server" Text="5"></asp:TextBox> Miles</td>
		                        <td><asp:TextBox id="p10miletitle"  Width="15px" CssClass="InputFieldMain" runat="server" Text="10"></asp:TextBox> Miles</td>
		                </tr>
		                <tr>
		                        <td><asp:TextBox id="Populationlabel"  Width="100px" CssClass="InputFieldMain" runat="server" Text="Population"></asp:TextBox></td>
		                        <td><asp:TextBox id="p3mile"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		                        <td><asp:TextBox id="p5mile"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		                        <td><asp:TextBox id="p10mile"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		                </tr>
		                <tr>
		                        <td><asp:TextBox id="Householdslabel"  Width="100px" CssClass="InputFieldMain" runat="server" Text="Households"></asp:TextBox></td>
		                        <td><asp:TextBox id="h3mile"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		                        <td><asp:TextBox id="h5mile"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		                        <td><asp:TextBox id="h10mile"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		                </tr>
		                <tr>
		                        <td><asp:TextBox id="AvgHHIncomelabel"  Width="100px" CssClass="InputFieldMain" runat="server" Text="Avg. HH Income"></asp:TextBox></td>
		                        <td><asp:TextBox id="ahh3mile"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		                        <td><asp:TextBox id="ahh5mile"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		                        <td><asp:TextBox id="ahh10mile"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		                </tr>
		                <tr>
		                        <td><asp:TextBox id="MedHHIncomelabel"  Width="100px" CssClass="InputFieldMain" runat="server" Text="Med. HH Income"></asp:TextBox></td>
		                        <td><asp:TextBox id="mhh3mile"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		                        <td><asp:TextBox id="mhh5mile"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		                        <td><asp:TextBox id="mhh10mile"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
		                </tr>
		            </table>
		        </td>
		    </tr>
		    <tr>
		        <td colspan="2">
		        <table width="100%" cellpadding="0" cellspacing="0">
		            <tr>
		            <td width="88">Utilities</td>
		            <td><hr /></td>
		            </tr>
		        </table>
		        </td>
		     </tr>
		     <tr>
		        <td width="100" valign="top"></td>
		        <td><asp:TextBox  id="Utilities"  Wrap=False  style="height:220px;overflow: scroll;overflow-y: scroll;overflow-x: scroll; overflow:moz-scrollbars-both;" height="224px" TextMode="MultiLine" CssClass="InputFieldMain100P" runat="server"></asp:TextBox></td>
		    </tr>		    			    		    		    
		</table>
		<br />
		<asp:Button ID="saveInfo" CssClass="InputButtonMain" Text="Edit Details" runat="server"/>
		
        <br /><br />
        

		
<COMPONENTART:GRID 
id="GridTenants2" 
runat="server"
visible="false"
ItemDraggingEnabled="False"
AutoFocusSearchBox="false"
AutoCallBackOnInsert="true"
AutoCallBackOnUpdate="true"
AutoCallBackOnDelete="true"
pagerposition="2"
ScrollBar="Off"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ScrollPopupClientTemplateId="ScrollPopupTemplate" 
Height="10" Width="100%"
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplate2" 
LoadingPanelFadeDuration="200"
LoadingPanelFadeMaximumOpacity="95"
LoadingPanelEnabled="true"
EnableViewState="true"
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRowCategory" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="20" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="True" 
GroupingPageSize="5" 
PagerTextCssClass="GridFooterTextCategory" 
PagerStyle="Numbered" 
PageSize="20" 
ManualPaging="true"
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeader" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="true" 
AllowSorting="False"
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
ScrollBarWidth="15" 
allowtextselection="false"
AllowPaging="true" 
ClientSideOnPage="onPage">
<ClientEvents>
  </ClientEvents>
<ClientTemplates>
<ComponentArt:ClientTemplate Id="TenantImageTemplate">
Tenant Image
</ComponentArt:ClientTemplate>
<ComponentArt:ClientTemplate Id="TenantActionTemplate">
<a href="Tenant.aspx?id=## DataItem.GetMember("tenant_id").Value ##">Edit</a> <a href="Tenant.aspx?id=## DataItem.GetMember("tenant_id").Value ##&action=delete">Delete</a>
</ComponentArt:ClientTemplate>
<componentart:ClientTemplate ID="LoadingFeedbackTemplate2"> 
<table height="1px" width="100%" ><tr><td valign="center" align="center">
<table cellspacing="0" cellpadding="0" border="0"  background="images/loadingback.gif" height="70px" width="300px">
<tr>
<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
<td style="font-size:10px;font-family:Verdana;" width="60px" align="right">Loading...&nbsp;</td>
<td><img src="images/spinner2.gif"  border="0"></td>
<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
</tr>
</table>
</td></tr></table>
</componentart:ClientTemplate>   

</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
EditFieldCssClass="EditDataField"
 DataKeyField="tenant_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asch10.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="desch10.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="10" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" Align="Center" AllowSorting="false" AllowEditing="false" DataCellClientTemplateId="TenantImageTemplate" HeadingCellCssClass="FirstHeadingCell" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" width="100" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Tenant Name" TextWrap="true" AllowEditing="false" Width="600" SortedDataCellCssClass="SortedDataCell" DataField="shortname" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Tenant Asset ID" AllowEditing="false"  SortedDataCellCssClass="SortedDataCell" DataField="asset_id" visible="false"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Tenant ID" AllowEditing="false"  SortedDataCellCssClass="SortedDataCell" DataField="tenant_id" visible="false"></componentart:GridColumn>
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" Align="Center" AllowSorting="false" AllowEditing="false" DataCellClientTemplateId="TenantActionTemplate" HeadingCellCssClass="FirstHeadingCell" HeadingText="Action" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" FixedWidth="True" Width="80"/>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>		
		</div>

