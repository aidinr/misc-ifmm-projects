<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.ApprovalManager"  CodeBehind="ApprovalManager.ascx.vb" %>
<div class="previewpaneMyProjects ui-accordion-header ui-helper-reset ui-state-active " id="toolbarheading" style="z-index:99;border-top:0px;border-bottom:0px;">
	<!--Project cookie and top menu-->
		
		<table cellspacing="0"  id="table2" width="100%">
			<tr>
			<td >
			<div style="PADDING-RIGHT:5px;PADDING-LEFT:5px;FLOAT:left;PADDING-BOTTOM:5px;PADDING-TOP:5px"><img src="images/ui/appman_ico_bg.gif" ></div>
			<div style="PADDING-RIGHT:5px;PADDING-LEFT:5px;FLOAT:left;PADDING-BOTTOM:5px;PADDING-TOP:5px">
						<font face="Verdana" size="1"><b>Approval Manager</b><br>
				<span style="FONT-WEIGHT: 400">View and manage items requiring your approval.<br></span>
				
				</font>
			</div>
			</td>
			<td id="Test" valign="top" align="right">
			
			</td></tr>
			<tr><td valign="top" colspan="2">
			</td>
			</tr>
		</table>
		
</div> <!--end preview pane-->

<script language="javascript">
var xoffsetpopup
xoffsetpopup = -100
var yoffsetpopup 
yoffsetpopup = -400
</script>

<div class="previewpaneProjects" id="toolbar">

<script language=javascript>
  
        // Forces the grid to adjust to the new size of its container          
      function resizeGridUploads(DomElementId, NewPaneHeight, NewPaneWidth)
      {
        <% Response.Write(GridUploads.ClientID) %>.Render();
        //alert('here');
      } 
  function editGrid(rowId)
  {
    <% Response.Write(GridUploads.ClientID) %>.Edit(<% Response.Write(GridUploads.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  

  function onInsert(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
   function editGridDT(rowId)
  {

    <% Response.Write(GridDeleted.ClientID) %>.Edit(<% Response.Write(GridDeleted.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  

  function onInsertDT(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  


  function onInsertIT(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
    function ShowContextMenu(item, column, evt) 
  {
    <% Response.Write(GridUploads.ClientID) %>.Select(item); 
   
    return false; 
  }
  
  function onUpdate(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }
  
    
  function onUpdateDT(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }
  
  
    
  function onUpdateIT(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }


  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridUploads.ClientID) %>.Page(1); 
  }

  function onDelete(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
  
  function editRow()
  {
    <% Response.Write(GridUploads.ClientID) %>.EditComplete();     
  }

  function insertRow()
  {
    <% Response.Write(GridUploads.ClientID) %>.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    <% Response.Write(GridUploads.ClientID) %>.Delete(<% Response.Write(GridUploads.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  
    function onDeleteDT(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
  
  function editRowDT()
  {
    <% Response.Write(GridDeleted.ClientID) %>.EditComplete();     
  }

  function insertRowDT()
  {
    <% Response.Write(GridDeleted.ClientID) %>.EditComplete(); 
  }

  function deleteRowDT(rowId)
  {
    <% Response.Write(GridDeleted.ClientID) %>.Delete(<% Response.Write(GridDeleted.ClientID) %>.GetRowFromClientId(rowId)); 
  }




</script>


					<div id="trailimageid">
									<div class="SnapProjectsWrapperPopup" style="WIDTH:599px">					
										<COMPONENTART:CALLBACK id="CallbackProjectAssetOverlay" runat="server" CacheContent="false">
											<CONTENT>
												<asp:PlaceHolder ID="PlaceHolderAssetPreviewPage" runat="server">
													
													<!--SnapProjectsOverlay-->
													<div class="SnapProjectsOverlay" >
														<font face="Verdana" size="1">
														<table id="table3">
															<tr>

																<td>
														
																<font size="1"><b>Image:</b></font></span><font size="1">
																</font></td>

																<td align="left" >

																<font size="1">
<asp:Literal ID="ProjectAssetOverlayCurrentImageName" Runat=server></asp:Literal></font></td>
												
  															</tr>
															<tr >

																<td><font size="1"><b>Location(s):</b></font></span><font size="1">
																</font></td>

																<td align="left" >
																
																<font size="1">
<asp:Literal ID="ProjectAssetOverlayCurrentProjectName" Runat=server></asp:Literal></font></a><font size="1">
																</font></td>

															</tr>
															</table>
														<br>
														<asp:Image id="ProjectAssetOverlayCurrentImage" runat="server" ImageUrl="~/common/images/spacer.gif"></asp:Image>
														
														<br><!--
[ <asp:Literal ID="LiteralRotateRight" Runat=server></asp:Literal>rotate right</a> ] [ <asp:Literal ID="LiteralRotateLeft" Runat=server></asp:Literal>rotate left</a> ]
													<br>-->
														<table id="table2">
															<tr>

																<td>
														
																<font size="1">Description:</font></span><font size="1">
																</font></td>
										
																<td align="left" width="80%">

																<font size="1">
<asp:Literal ID="LiteralAssetDescription" Runat=server></asp:Literal></font></td>
												
  															</tr>
															<tr >
																<td><font size="1">Owner</font><font size="1">:</font></span><font size="1">
																</font></td>
																<td align="left" width="80%">
																
																<font size="1">
<asp:Literal ID="LiteralAssetOwner" Runat=server></asp:Literal></font></a><font size="1">
																</font></td>
															</tr>
															<tr >
																<td noWrap>
																
																<font size="1">Date Created:</font></span><font size="1">
																</font></td>
																<td>
																<font size="1">October 17, 2003</font></span><font size="1">
																</font></td>
															</tr>
															<tr>


																<td noWrap><font size="1">Type</font><font size="1">:</font></span><font size="1">
																</font></td>
												
																<td>
																<font size="1">
<asp:Literal ID="LiteralAssetType" Runat=server></asp:Literal></font></td>
											

  															</tr>
															<tr>

																<td noWrap><font size="1">File Size</font><font size="1">:</font></span><font size="1">
																</font></td>

																<td>
																<font size="1"> 
																
																<asp:Literal ID="LiteralAssetSize" Runat=server></asp:Literal></a> </font>
																</td>
									
  															</tr>
															<tr>

																<td noWrap>
										
																<font size="1">Keywords:</font></span><font size="1">
																</font></td>
										
																<td>
																<font size="1">
<asp:Literal ID="LiteralAssetKeywords" Runat=server></asp:Literal></font></td>
											
  															</tr>
															
														</table>
													</div><!--END SnapProjectsOverlay-->
												</asp:PlaceHolder>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<div class="SnapProjectsOverlay" >
													<TABLE cellSpacing="0" cellPadding="0" border="0" height="500" width=100%>
														<TR>
															<TD align="center">
																<TABLE cellSpacing="0" cellPadding="0" border="0">
																	<TR>
																		<TD style="FONT-SIZE: 10px">Loading...
																		</TD>
																		<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																	</TR>
																</TABLE>
															</TD>
															<td><IMG height="16" src="images/spacer.gif" width="16" height="500" border="0">
															</td>
														</TR>
													</TABLE>
												</div>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
									</div><!--padding-->
								</div>
							<!--END SnapProjectAssetOverlay-->

							
							
<script language=javascript type=text/javascript>

	
	
	
	
	
	/*
Simple Image Trail script- By JavaScriptKit.com
Visit http://www.javascriptkit.com for this script and more
This notice must stay intact
*/

var offsetfrommouse=[xoffsetpopup,yoffsetpopup]; //image x,y offsets from cursor position in pixels. Enter 0,0 for no offset
//var offsetfrommouse=[-150,-500]; //image x,y offsets from cursor position in pixels. Enter 0,0 for no offset
var displayduration=0; //duration in seconds image should remain visible. 0 for always.
var currentimageheight = 400;	// maximum image size.


function gettrailobj(){
if (document.getElementById)
return document.getElementById("trailimageid").style
else if (document.all)
return document.all.trailimagid.style
}

function gettrailobjnostyle(){
if (document.getElementById)
return document.getElementById("trailimageid")
else if (document.all)
return document.all.trailimagid
}


function truebody() {
    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    var is_safari = navigator.userAgent.toLowerCase().indexOf('safari') > -1;
    var is_opera = navigator.userAgent.toLowerCase().indexOf('opera') > -1; 
    if (is_chrome || is_safari || is_opera) {
        return document.body;
    }
    else {
        return document.documentElement;
    }
}


function showtrail(imagename,title,description,ratingaverage,ratingnumber,showthumb,height,filetype){

	if (height > 0){
		currentimageheight = height;
	}

	document.onmousemove=followmouse;

	cameraHTML = '';

	if ( !ratingnumber ){
		ratingnumber = 0;
		ratingaverage = 0;
	}

	for(x = 1; x <= 5; x++){

		if (ratingaverage >= 1){
			cameraHTML = cameraHTML + '<img src="/images/camera_1.gif">';
		} else if (ratingaverage >= 0.5){
			cameraHTML = cameraHTML + '<img src="/images/camera_05.gif">';
		} else {
			cameraHTML = cameraHTML + '<img src="/images/camera_0.gif">';
		}
	
		ratingaverage = ratingaverage - 1;
	}

	cameraHTML = cameraHTML + ' (' + ratingnumber + ' Review';
	if ( ratingnumber > 1 ) cameraHTML += 's';
	cameraHTML = cameraHTML + ')';

	newHTML = '<div style="padding: 5px; background-color: #FFF; border: 1px solid #888;">';
	newHTML = newHTML + '<h1>' + title + '</h1>';
	//newHTML = newHTML + 'Rating: ' + cameraHTML + '<br/>';
	newHTML = newHTML + description + '<br/>';

	if (showthumb > 0){
		newHTML = newHTML + '<div align="center" style="padding: 8px 2px 2px 2px;">';
		if(filetype == 8) { // Video
			newHTML = newHTML +	'<object width="380" height="285" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0">';
			newHTML = newHTML + '<param name="movie" value="video_loupe.swf">';
			newHTML = newHTML + '<param name="quality" value="best">';
			newHTML = newHTML + '<param name="loop" value="true">';

			newHTML = newHTML + '<param name="FlashVars" value="videoLocation=' + imagename + '">';
			newHTML = newHTML + '<EMBED SRC="video_loupe.swf" LOOP="true" QUALITY="best" FlashVars="videoLocation=' + imagename + '" WIDTH="380" HEIGHT="285">';
			newHTML = newHTML + '</object></div>';
		} else {
			newHTML = newHTML + '<img src="' + imagename + '" border="0"></div>';
		}
	}

	newHTML = newHTML + '</div>';
	//gettrailobjnostyle().innerHTML = newHTML;
	gettrailobj().display="inline";
}


function hidetrail(){
	gettrailobj().innerHTML = " ";
	gettrailobj().display="none"
	document.onmousemove=""
	gettrailobj().left="-1000px"

}

function followmouse(e){

	var xcoord=offsetfrommouse[0]
	var ycoord=offsetfrommouse[1]

	var docwidth=document.all? truebody().scrollLeft+truebody().clientWidth : pageXOffset+window.innerWidth-15
	var docheight=document.all? Math.min(truebody().scrollHeight, truebody().clientHeight) : Math.min(window.innerHeight)

	//if (document.all){
	//	gettrailobjnostyle().innerHTML = 'A = ' + truebody().scrollHeight + '<br>B = ' + truebody().clientHeight;
	//} else {
	//	gettrailobjnostyle().innerHTML = 'C = ' + document.body.offsetHeight + '<br>D = ' + window.innerHeight;
	//}

	if (typeof e != "undefined"){
		if (docwidth - e.pageX < 380){
			xcoord = e.pageX - xcoord - 1000; // Move to the left side of the cursor
		} else {
			xcoord += e.pageX;
		}
		if (docheight - e.pageY < (currentimageheight + 260)){
			ycoord += e.pageY - Math.max(0,(260 + currentimageheight + e.pageY - docheight - truebody().scrollTop));
		} else {
			ycoord += e.pageY;
		}

	} else if (typeof window.event != "undefined"){
		if (docwidth - event.clientX < 380){
			xcoord = event.clientX + truebody().scrollLeft - xcoord - 1000; // Move to the left side of the cursor
		} else {
			xcoord += truebody().scrollLeft+event.clientX
		}
		if (docheight - event.clientY < (currentimageheight + 260)){
			ycoord += event.clientY + truebody().scrollTop - Math.max(0,(260 + currentimageheight + event.clientY - docheight));
		} else {
			ycoord += truebody().scrollTop + event.clientY;
		}
	}

	var docwidth=document.all? truebody().scrollLeft+truebody().clientWidth : pageXOffset+window.innerWidth-15
	var docheight=document.all? Math.max(truebody().scrollHeight, truebody().clientHeight) : Math.max(document.body.offsetHeight, window.innerHeight)
		if(ycoord < 0) { ycoord = ycoord*-1; }
	gettrailobj().left=xcoord+"px"
	gettrailobj().top=ycoord+"px"

}




  
  
function PreviewOverlayOnSingleClickAssetsFromIcon(RowId,eventObject)
  {
	try
	{
		
		var itemvaluetmp;
		var itemvaluenametmp;
		var itemvaluefiletypetmp;
		var item 
		itemRow = <% Response.Write(GridUploads.ClientID) %>.GetRowFromClientId(RowId);
		itemvaluetmp = itemRow.GetMember('asset_id').Value;
		itemvaluenametmp = itemRow.GetMember('name').Value;
		itemvaluefiletypetmp = itemRow.GetMember('timage').Value;

		<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);

		 
	}  //end try  
	catch(err)
	{
	txt="There was an error on this page.\n\n"
	txt+="Error description: " + err.description

	}

}


  
function PreviewOverlayOnSingleClickAssetsFromIconh(RowId,eventObject)
  {
	try
	{
		
		var itemvaluetmp;
		var itemvaluenametmp;
		var itemvaluefiletypetmp;
		var item 
		itemRow = <% Response.Write(GridHistory.ClientID) %>.GetRowFromClientId(RowId);
		itemvaluetmp = itemRow.GetMember('asset_id').Value;
		itemvaluenametmp = itemRow.GetMember('name').Value;
		itemvaluefiletypetmp = itemRow.GetMember('timage').Value;

		<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);

		 
	}  //end try  
	catch(err)
	{
	txt="There was an error on this page.\n\n"
	txt+="Error description: " + err.description

	}

}


</script>
							
							

<div style="BORDER-RIGHT:0px solid; PADDING-RIGHT:5px; BORDER-TOP:0px solid; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:0px solid; PADDING-TOP:15px; BORDER-BOTTOM:0px solid" id="browsecenter">



<img src="images/spacer.gif"  height="15" width="5"><br>
<!--<div style="padding:10px;border-left:1px #969698 solid; border-right:1px #969698 solid;border-bottom:1px #969698 solid;border-top:1px #969698 solid;">-->







<div style="PADDING-RIGHT:10px;PADDING-BOTTOM:10px">
<b>Uploaded items requiring your approval.</b>
</div>

<asp:Literal id="LiteralNoRecentAssets" Text="No items require your approval" visible=false runat="server" />


<COMPONENTART:GRID id="GridUploads" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ClientSideOnSelect="BrowseOnSingleClickAssets" ScrollPopupClientTemplateId="ScrollPopupTemplate" Sort="update_date desc" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateRecentAssets" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="true" GroupingPageSize="1" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true" >
<ClientTemplates>
<ComponentArt:ClientTemplate Id="EditTemplateRecentAssets">
           <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border="0" > </a> | <a href="javascript:DownloadAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateRecentAssets">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateRecentAssets">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateRecentAssets">
            <A href="## DataItem.GetMember("alink").Value ##"><img src="## DataItem.GetMember("timage").Value ##" border="0" onmouseover="javascript:showtrailpreload('## DataItem.GetMember("asset_id").Value ##','','','5','1',270,7);PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.ClientId ##',event);" onmouseout="javascript:hidetrail();"></a>
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplateRecentAssets">
            <A href="## DataItem.GetMember("alink").Value ##"><img border=0 src="images/goto.gif"></a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="LookupCategoryTemplateRecentAssets">
            <A href="javascript:GotoLocation('0','## DataItem.GetMember("projectid").Value ##');">## DataItem.GetMember("projectname").Value ##</a>
          </ComponentArt:ClientTemplate>    
          <ComponentArt:ClientTemplate Id="LookupCategoryIconTemplateRecentAssets">
            <img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif">
          </ComponentArt:ClientTemplate>                    
<componentart:ClientTemplate ID="LoadingFeedbackTemplateRecentAssets">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>                                 
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateRecentAssets" InsertCommandClientTemplateId="InsertCommandTemplateRecentAssets" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplateRecentAssets" dataField="timage" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="50" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="User" AllowEditing="false" Width="80" SortedDataCellCssClass="SortedDataCell" DataField="fullname"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="False" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="False" DataCellClientTemplateId="LookupCategoryTemplateRecentAssets" HeadingText="Project" Width="80" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="projectname" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Type" AllowEditing="false" Width="80" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Uploaded" AllowEditing="false" Width="80" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Status" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="status" Width="110" FixedWidth="True" ForeignTable="PriorityTable" ForeignDataKeyField="priority_id" ForeignDisplayField="priority_value"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="True" HeadingText="Add Comment" Width="120" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="comment" ></componentart:GridColumn>
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowSorting="False" AllowEditing="false" DataCellClientTemplateId="EditTemplateRecentAssets" EditControlType="EditCommand" Width="80" Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>

</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<!--<componentart:GridColumn AllowEditing="false" HeadingText="Assets" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid" ForeignTable="Assets" ForeignDataKeyField="uniqueid" ForeignDisplayField="acount"></componentart:GridColumn>-->
<br><br><input type="button" onclick="<%response.write( GridUploads.ClientId)%>.Filter('name LIKE \'%%\' ');" value="Refresh" >







<div style="PADDING-TOP:10px"><div style="PADDING-RIGHT:2px;PADDING-LEFT:2px;PADDING-BOTTOM:2px;PADDING-TOP:2px"></div></div>







<div style="PADDING-RIGHT:10px;PADDING-BOTTOM:10px">
<b>Approval history.</b>
</div>
<asp:Literal id="LiteralNoHistory" Text="No history available" visible=false runat="server" />



<COMPONENTART:GRID id="GridHistory" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ClientSideOnSelect="BrowseOnSingleClickAssets" ScrollPopupClientTemplateId="ScrollPopupTemplate" Sort="update_date desc" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateRecentAssetsh" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="true" GroupingPageSize="1" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true" >
<ClientTemplates>
<ComponentArt:ClientTemplate Id="EditTemplateRecentAssetsh">
           <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border="0" > </a> | <a href="javascript:DownloadAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateRecentAssetsh">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateRecentAssetsh">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateRecentAssetsh">
            <A href="## DataItem.GetMember("alink").Value ##"><img src="## DataItem.GetMember("timage").Value ##" border="0" onmouseover="javascript:showtrailpreload('## DataItem.GetMember("asset_id").Value ##','','','5','1',270,7);PreviewOverlayOnSingleClickAssetsFromIconh('## DataItem.ClientId ##',event);" onmouseout="javascript:hidetrail();"></a>
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplateRecentAssetsh">
            <A href="## DataItem.GetMember("alink").Value ##"><img border=0 src="images/goto.gif"></a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="LookupCategoryTemplateRecentAssetsh">
            <A href="javascript:GotoLocation('0','## DataItem.GetMember("projectid").Value ##');">## DataItem.GetMember("projectname").Value ##</a>
          </ComponentArt:ClientTemplate>    
          <ComponentArt:ClientTemplate Id="LookupCategoryIconTemplateRecentAssetsh">
            <img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif">
          </ComponentArt:ClientTemplate>                    
<componentart:ClientTemplate ID="LoadingFeedbackTemplateRecentAssetsh">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>                                 
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateRecentAssetsh" InsertCommandClientTemplateId="InsertCommandTemplateRecentAssets" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplateRecentAssetsh" dataField="timage" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="50" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="User" AllowEditing="false" Width="80" SortedDataCellCssClass="SortedDataCell" DataField="fullname"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="False" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="False" DataCellClientTemplateId="LookupCategoryTemplateRecentAssetsh" HeadingText="Project" Width="80" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="projectname" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Type" AllowEditing="false" Width="80" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Uploaded" AllowEditing="false" Width="80" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Status" AllowEditing="False" SortedDataCellCssClass="SortedDataCell" DataField="type" Width="110" FixedWidth="True" ForeignTable="PriorityTable" ForeignDataKeyField="priority_id" ForeignDisplayField="priority_value"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>

</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<!--<componentart:GridColumn AllowEditing="false" HeadingText="Assets" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid" ForeignTable="Assets" ForeignDataKeyField="uniqueid" ForeignDisplayField="acount"></componentart:GridColumn>-->
<br><br><input type="button" onclick="<%response.write( GridHistory.ClientId)%>.Filter('name LIKE \'%%\' ');" value="Refresh" >






<div style="DISPLAY:none">


<div style="PADDING-TOP:10px"><div style="PADDING-RIGHT:2px;PADDING-LEFT:2px;PADDING-BOTTOM:2px;PADDING-TOP:2px"></div></div>

<div style="PADDING-RIGHT:10px;PADDING-BOTTOM:10px">
<b>Deleted items requiring your approval.</b>
</div>



<COMPONENTART:GRID id="GridDeleted" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ClientSideOnInsert="onInsertDT" ClientSideOnUpdate="onUpdateDT" ClientSideOnDelete="onDeleteDT" ClientSideOnCallbackError="onCallbackError" ScrollPopupClientTemplateId="ScrollPopupTemplate" Sort="date_placed desc" Height="10" Width="99%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplatep" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="20" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="True" GroupingPageSize="5" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="20" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplatep">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=125 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplatep">
            <a href="javascript:editGridDT('## DataItem.ClientId ##');">Edit</a> | <a href="javascript:deleteRowDT('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplatep">
            <a href="javascript:editRowDT();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplatep">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplatep">
          <img src="## DataItem.GetMember("imagesource").Value ##" border="0"  > 
          </ComponentArt:ClientTemplate>           
          <ComponentArt:ClientTemplate Id="TemplocationTemplatep">
          <a href="## DataItem.GetMember("src").Value ##" target=_blank>test</a>
          </ComponentArt:ClientTemplate> 
          <ComponentArt:ClientTemplate Id="AssetLinkTemplatep">
          ## DataItem.GetMember("asset_name").Value ##
          </ComponentArt:ClientTemplate>  
                   
          
                             
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplatep" InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="ID" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplatep"  HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="False" HeadingText="Asset" DataCellClientTemplateId="AssetLinkTemplatep" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="asset_name" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="False" HeadingText="Instance" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="instance_id" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Source" AllowEditing="false" DataCellClientTemplateId="TemplocationTemplatep" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="src"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Handler" AllowEditing="False" SortedDataCellCssClass="SortedDataCell" DataField="asset_handler" Width="80" FixedWidth="True" ForeignTable="HandlerTable" ForeignDataKeyField="handler_id" ForeignDisplayField="handler_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Added" AllowEditing="false" FormatString="MM/dd/yyyy hh:mm:ss" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="date_complete"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Engine" AllowEditing="true" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="engine_id"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Priority" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="priority" Width="80" FixedWidth="True" ForeignTable="PriorityTable" ForeignDataKeyField="priority_id" ForeignDisplayField="priority_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Status" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="status" Width="80" FixedWidth="True" ForeignTable="StatusTable" ForeignDataKeyField="status_id" ForeignDisplayField="status_value"></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplatep" EditControlType="EditCommand" Align="Center" Width="80" FixedWidth="True"  DataCellCssClass="LastDataCell" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="imagesource" SortedDataCellCssClass="SortedDataCell" DataField="imagesource"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="ID" SortedDataCellCssClass="SortedDataCell" DataField="ID"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<!--<componentart:GridColumn AllowEditing="false" HeadingText="Assets" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid" ForeignTable="Assets" ForeignDataKeyField="uniqueid" ForeignDisplayField="acount"></componentart:GridColumn>-->
<input type="button" onclick="<%response.write( GridDeleted.ClientId)%>.Filter('asset_name LIKE \'%%\' ');" value="Refresh" >






</div>







      </div><!--END browsecenter-->
</div><!--END MAIN-->
      
      
      
      
      


      <script type=text/javascript>
  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

  
  function BrowseOnDoubleClick(SelectedGridRow)
  {
  
  	var itemvaluetmp;
	itemvaluetmp = SelectedGridRow.GetMember('uniqueid').Value;
	if (itemvaluetmp.substring(0,1) == 'P') 
	{
		itemvaluetmp = itemvaluetmp.replace('P_','');
		window.location.href = 'IDAM.aspx?page=Project&id=' + itemvaluetmp + '&type=project'
    
	} else {
		itemvaluetmp = itemvaluetmp.replace('C_','');
		window.location.href = 'IDAM.aspx?page=Browse&id=' + itemvaluetmp + '&type=category';
	}
	
	return true;
  
  
  
	
  }
  
  
  
//setup form switch for delete or multi asset zip download
function DownloadAssets(ids){
	var sAssets;
	var downloadtype;
	sAssets = <%response.write( GridUploads.ClientId)%>.GetSelectedItems();
	ids = ids.replace('0 ','');
	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Download selected assets?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (arraylist == ''){
					arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
					} else {
					if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
					arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					}
					}
				}
				
				arraylist= arraylist.substring(1, arraylist.length);
				downloadtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				downloadtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	downloadtype = 'single';
	}
	if (downloadtype == 'multi') {
		if (arraylist != ''){
		<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('multi,'+arraylist);		
		}
	}
	if (downloadtype == 'single') {
	/*assume single*/
	<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('single,'+ids);
	}

}

  
    function highlightProjectToggle(checkbox,projectid)
  {
  var highlightbox;
  highlightbox = document.getElementById('projecthighlight_' + projectid);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  

  </script
      >
      
<div style="PADDING-LEFT:10px"><div style="PADDING-RIGHT:3px;PADDING-LEFT:3px;PADDING-BOTTOM:3px;PADDING-TOP:3px"></div></div>






<script language=javascript>
//thumbnailsarea.style.display = 'none';
</script>

<script language=javascript type="text/javascript">
function showtrailpreload(imageid,title,description,ratingaverage,ratingnumber,showthumb,height,filetype)
{
	showtrail('<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?id=' + imageid + '&amp;instance=<%response.write (IDAMInstance)%>&amp;type=asset&amp;size=2&amp;height=399&amp;width=370',title,description,'5','1',270,7);
}

function gotopagelink(subtype)
{
	var newurl = 'IDAM.aspx?Page=' + subtype;
	window.location.href = newurl;
	return true;
}

//setup goto for grid nav
function GotoLocation(ctype,cid)
	{
	if (ctype == '1') {
		window.location = 'IDAM.aspx?page=Project&Id=<%response.write (request.querystring("Id"))%>&type=project&c=' + cid;
	} else{
		window.location = 'IDAM.aspx?page=Project&Id=' + cid + '&type=project';
	}
	return true;
	}
	
	
  
</script>

