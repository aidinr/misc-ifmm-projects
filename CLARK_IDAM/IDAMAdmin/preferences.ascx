<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.Preferences" CodeBehind="preferences.ascx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<div class="previewpaneMyProjects ui-accordion-content ui-helper-reset ui-widget-content ui-accordion-content-active " id="toolbarheading" style="z-index:99;border-top:0px;border-bottom:0px;">
	<!--Project cookie and top menu-->
	<table cellspacing="0" cellpadding="4" id="table2" width="100%">
		<tr>
			<td valign="top" width="167">
				<font face="Verdana" size="1"><b>My Preferences</b><br>
				</font>
			</td>
			<td valign="top">
			</td>
			<td valign="top" align="right">
				<div style="PADDING-TOP:2px">
				</div>
			</td>
		</tr>
		<tr>
			<td valign="top" colspan="3">
			</td>
		</tr>
	</table>
</div> <!--end preview pane-->
<div id="subtabsetbottomlinefix" style="border-top:1px solid silver;z-index:2;position:relative;top:-1px;"></div>
    

 <link type="text/css" rel="stylesheet" href="http://jqueryui.com/themes/base/ui.all.css" />


<div id="subtabset" style="position:relative;top:-5px;z-index:2; font-size:10px;">
<input type="radio" id="radio1" name="subtab" checked="checked" /><label for="radio1">General</label><img src="images/spacer.gif" width="2" alt="" />
</div>


<script>
    $(function () {
        $("#subtab").button();
        $("#subtabset").buttonset();
    });
	</script>
<div class="previewpaneProjects" id="toolbar"   style="padding:10px;">


	<script type="text/javascript">
  
$(document).ready(function(){
    $('#switcher').themeswitcher({cookieName:'jqueryuithemeloaded', width: 160,height: 400});
    
  });


  function setImageQuality(_val)
  {
  <%=CallBackPreferences.ClientID %>.Callback('ImageQuality,'+_val);
  }
  function setImageCrop(_val)
  {
  
  if (_val==true){
	<%=CallBackPreferences.ClientID %>.Callback('ImageCrop,1');
	}else{
	<%=CallBackPreferences.ClientID %>.Callback('ImageCrop,0');
	}
  }
  function setSlideShowCoolIris(_val)
  {
  
  if (_val==true){
	<%=CallBackPreferences.ClientID %>.Callback('SlideShowCoolIris,1');
	}else{
	<%=CallBackPreferences.ClientID %>.Callback('SlideShowCoolIris,0');
	}
  }  
  
    function setDescriptionsInResults(_val)
  {
  if (_val==true){
	<%=CallBackPreferences.ClientID %>.Callback('DescriptionsInResults,1');
	}else{
	<%=CallBackPreferences.ClientID %>.Callback('DescriptionsInResults,0');
	}
  }  
  
  
  
  function setGridThumbSize(_val)
  {
	<%=CallBackPreferences.ClientID %>.Callback('GridThumbSize,'+_val);
  }  
  
  
  
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <%=GridPreferences.ClientID %>.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    <%=GridPreferences.ClientID%>.Edit(<%=GridPreferences.ClientID %>.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    <%=GridPreferences.ClientID %>.EditComplete();     
  }

  function insertRow()
  {
    <%=GridPreferences.ClientID %>.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    <%=GridPreferences.ClientID %>.Delete(GridPreferences.GetRowFromClientId(rowId)); 
  }

      
	</script>
	<br>
	<br>
	<div style="BORDER-RIGHT:#b7b4b4 1px solid; PADDING-RIGHT:5px; BORDER-TOP:#b7b4b4 1px solid; PADDING-LEFT:5px; FONT-WEIGHT:normal; FONT-SIZE:10px; PADDING-BOTTOM:5px; BORDER-LEFT:#b7b4b4 1px solid; COLOR:#3f3f3f; PADDING-TOP:5px; BORDER-BOTTOM:#b7b4b4 1px solid; FONT-FAMILY:verdana; BACKGROUND-COLOR:#e4e4e4">Modify 
		Preferences by clicking the edit links.</div>
	<br>
	<DIV>
		<ComponentArt:Grid id="GridPreferences" RunningMode="Callback" CssClass="Grid" GroupByTextCssClass="GroupByText"
			GroupingNotificationTextCssClass="GridHeaderText" ShowFooter="false" PageSize="20" ImagesBaseUrl="images/"
			EditOnClickSelectedItem="true" AllowEditing="true" Sort="Preference" ScrollBar="Off" ScrollTopBottomImagesEnabled="true"
			ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/"
			ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip"
			ScrollBarWidth="16" ScrollPopupClientTemplateId="ScrollPopupTemplate" Width="100%" Height="207"
			AutoCallBackOnDelete="true" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" runat="server"
			PagerStyle="Numbered" PagerTextCssClass="PagerText">
			<Levels>
				<ComponentArt:GridLevel DataKeyField="PreferenceKey" ShowTableHeading="false" ShowSelectorCells="false"
					HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive"
					HeadingTextCssClass="HeadingCellText" DataCellCssClass="DataCell" RowCssClass="Row" SelectedRowCssClass="SelectedRow"
					SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" ColumnReorderIndicatorImageUrl="reorder.gif"
					SortedDataCellCssClass="SortedDataCell" SortImageWidth="14" SortImageHeight="14" EditCellCssClass="EditDataCell"
					EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate">
					<Columns>
						<ComponentArt:GridColumn DataField="PreferenceKey" HeadingText="PreferenceKey" Visible="false" />
						<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" AllowEditing="False" DataField="Preference"
							HeadingText="Preference Name" />
						<ComponentArt:GridColumn DataField="Value" HeadingText="Value" Width="50" FixedWidth="True" />
						<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowSorting="False"
							DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Width="100" Align="Center" FixedWidth="True" />
					</Columns>
				</ComponentArt:GridLevel>
			</Levels>
			<ClientTemplates>
				<ComponentArt:ClientTemplate Id="EditTemplate">
					<a href="javascript:editGrid('## DataItem.ClientId ##');">Edit</a>
				</ComponentArt:ClientTemplate>
				<ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> | <a href="javascript:_ctl0_GridPreferences.EditCancel();">Cancel</a>
          </ComponentArt:ClientTemplate>
				<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> | <a href="javascript:_ctl0_GridPreferences.EditCancel();">Cancel</a>
          </ComponentArt:ClientTemplate>
				<ComponentArt:ClientTemplate Id="LoadingFeedbackTemplate">
					<table cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td style="font-size:10px;">Loading...&nbsp;</td>
							<td><img src="images/spinner.gif" width="16" height="16" border="0"></td>
						</tr>
					</table>
				</ComponentArt:ClientTemplate>
				<ComponentArt:ClientTemplate Id="Column1TemplateEdit">
					<table cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td><img src="images/9.gif" width="16" height="16" border="0"></td>
							<td style="padding-left:2px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;"><nobr>## 
										DataItem.GetMember("Preference").Value ##</nobr></div>
							</td>
						</tr>
					</table>
				</ComponentArt:ClientTemplate>
				<ComponentArt:ClientTemplate Id="ScrollPopupTemplate">
					<table cellspacing="0" cellpadding="2" border="0" class="ScrollPopup">
						<tr>
							<td style="width:20px;"><img src="images/9.gif" width="16" height="16" border="0"></td>
							<td style="width:130px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;width:130px;"><nobr>## 
										DataItem.GetMember("Preference").Value ##</nobr></div>
							</td>
						</tr>
					</table>
				</ComponentArt:ClientTemplate>
			</ClientTemplates>
		</ComponentArt:Grid></DIV>
	<DIV>&nbsp;</DIV>
	<DIV><STRONG>Options</STRONG></DIV>
	<DIV>&nbsp;</DIV>
	<DIV>
		<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="0">
			<TR>
				<TD style="WIDTH: 144px">Image Quality:</TD>
				<TD>
					<DIV style="FLOAT: left"><INPUT id="ImageQualityLossless" onclick="javascript:setImageQuality('2')" type="radio"
							name="ImageQuality">Best Quality (High Bandwidth) <INPUT id="ImageQualityHigh" onclick="javascript:setImageQuality('25')" type="radio" name="ImageQuality">High 
						(Local Network) <INPUT id="ImageQualityMedium" onclick="javascript:setImageQuality('50')" type="radio"
							name="ImageQuality">Medium (Optimized) <INPUT id="ImageQualityLow" onclick="javascript:setImageQuality('150')" type="radio" name="ImageQuality">Low 
						(Dialup)
					</DIV>
				</TD>
			</TR>
			<TR>
				<TD style="WIDTH: 144px">Image Crop:&nbsp;&nbsp;&nbsp;</TD>
				<TD>
					<input type="checkbox" onclick="javascript:setImageCrop(this.checked);" value="1" id="CheckBoxImageCrop"></TD>
			</TR>
			<TR>
				<TD style="WIDTH: 144px">Grid Thumbnail Size:
				</TD>
				<TD><INPUT id="GridThumbSizeLarge" onclick="javascript:setGridThumbSize('160')" type="radio" value="GridThumbSizeLarge"
						name="GridThumbSize">Large <INPUT id="GridThumbSizeMedium" onclick="javascript:setGridThumbSize('100')" type="radio"
						value="Radio2" name="GridThumbSize">Medium <INPUT id="GridThumbSizeSmall" onclick="javascript:setGridThumbSize('65')" type="radio"
						value="Radio3" name="GridThumbSize">Small <INPUT id="GridThumbSizeVerySmall" onclick="javascript:setGridThumbSize('35')" type="radio"
						value="Radio3" name="GridThumbSize">Very Small</TD>
			</TR>
			<TR>
				<TD style="WIDTH: 144px">Use CoolIris(tm) SlideShow (Beta):<br /><a href="http://www.cooliris.com/">Get the plugin here.</a></TD>
				<TD>
					<input type="checkbox" onclick="javascript:setSlideShowCoolIris(this.checked);" value="1" id="CheckBoxSlideShowIris"></TD>
			</TR>	
            <TR>
				<TD style="WIDTH: 144px">Do Not Search Description:<br /></TD>
				<TD>
					<input type="checkbox" onclick="javascript:setDescriptionsInResults(this.checked);" value="1" id="IDAMRemoveDescriptionsInSearch"></TD>
			</TR>						
			<TR>
				<TD style="WIDTH: 144px"></TD>
				<TD></TD>
			</TR>
		</TABLE>
	</DIV>
    <script type="text/javascript"
  src="http://jqueryui.com/themeroller/themeswitchertool/">
</script>
<br />
<b>Change Theme:</b><br /><br />
    <div id="switcher"></div>
    <br />[ <a href="javascript:cleartheme();">clear theme</a> ]
	<DIV>
	</DIV>
	<DIV>
		<ComponentArt:CallBack id="CallBackPreferences" Debug="false" runat="server">
			<Content></Content>
		</ComponentArt:CallBack></DIV>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<script>
			function cleartheme()
            {
            SetCookie('jqueryuithemeloaded','');
            window.location.href = window.location.href;
            }
<%
        Dim IDAMQfactor As integer
        IDAMQfactor = WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMQfactor
		Select Case WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMQfactor
            Case Is = 150
				%>document.getElementById('ImageQualityLow').checked=true;<%
            Case Is = 50 
				%>document.getElementById('ImageQualityMedium').checked=true;<%
            Case Is = 25 
				%>document.getElementById('ImageQualityHigh').checked=true;<%
            Case Is = 2
				%>document.getElementById('ImageQualityLossless').checked=true;<%				
            Case Else
				%>document.getElementById('ImageQualityHigh').checked=true;<%
        End Select
        Dim IDAMGridThumbnailWidth As integer
        IDAMGridThumbnailWidth = WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMGridThumbnailWidth
		Select Case WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMGridThumbnailWidth
            Case Is = 160
				%>document.getElementById('GridThumbSizeLarge').checked=true;<%
            Case Is = 100
				%>document.getElementById('GridThumbSizeMedium').checked=true;<%
            Case Is = 65 
				%>document.getElementById('GridThumbSizeSmall').checked=true;<%
            Case Is = 35
				%>document.getElementById('GridThumbSizeVerySmall').checked=true;<%				
            Case Else
				%>document.getElementById('GridThumbSizeSmall').checked=true;<%
        End Select        
        if WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMGridThumbnailCrop = "1" then
        %>document.getElementById('CheckBoxImageCrop').checked=true;<%
        end if      
        
    %>
    if(GetCookie('IDAMSlideShowCoolIris')=='1')
    {
    document.getElementById('CheckBoxSlideShowIris').checked=true;
    }
    if(GetCookie('IDAMRemoveDescriptionsInSearch')=='1')
    {
    document.getElementById('IDAMRemoveDescriptionsInSearch').checked=true;
    }    
	
	</script>
</div>
