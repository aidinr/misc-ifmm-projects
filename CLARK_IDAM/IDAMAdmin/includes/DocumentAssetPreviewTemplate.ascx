<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.DocumentAssetPreviewTemplate" CodeBehind="DocumentAssetPreviewTemplate.ascx.vb" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<style>
.StealthMainAssetImagecss
{
	border:solid 1px black;position: relative; top: 0px; left: 0px; filter: alpha(opacity=0.1);
}
</style>

<script>
     var dragsource;
     window.onerror = null;
    function resizemainimagewrapper() {

    try{
        dragsource = <% Response.Write(ComboBoxDragOptions.ClientID) %>.getSelectedItem().get_value();
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').attr('src', dragsource);
        } catch(e){}
    try{
        var _MainAssetImagewidth;
        var _MainAssetImageheight;
        _MainAssetImagewidth = $('#<% Response.Write(MainAssetImage.ClientID) %>').attr('width');
        _MainAssetImageheight = $('#<% Response.Write(MainAssetImage.ClientID) %>').attr('height');
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').attr('width', _MainAssetImagewidth);
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').attr('height', _MainAssetImageheight);
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').css('top', '-' + $('#<% Response.Write(MainAssetImage.ClientID) %>').attr('height') + 'px');

       } catch(e){}

        }
    
    
    function changedragresolution() {
    try{
        imageloaded = false;
        var selitem = <% Response.Write(ComboBoxDragOptions.ClientID) %>.getSelectedItem().get_text();
        var selitemval = <% Response.Write(ComboBoxDragOptions.ClientID) %>.getSelectedItem().get_value();
        dragsource = selitemval;
        resizemainimage();
    } catch(e){}
    }
    
    
    function resizemainimage() {
    try{
        var _MainAssetImagewidth;
        var _MainAssetImageheight;
        _MainAssetImagewidth = $('#<% Response.Write(MainAssetImage.ClientID) %>').attr('width');
        _MainAssetImageheight = $('#<% Response.Write(MainAssetImage.ClientID) %>').attr('height');
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').attr('width', _MainAssetImagewidth);
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').attr('height', _MainAssetImageheight);
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').attr('src', dragsource);
    } catch(e){}
    }
 


    function Object_PopUpNoScrollbars(strUrl, popupName, intHeight, intWidth) {
        if (strUrl == null || strUrl.Length <= 0)
            return;
        var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=no,resizable=yes,toolbar=no";
        if (intHeight != null)
            strFeatures += ",height=" + intHeight;
        if (intWidth != null)
            strFeatures += ",width=" + intWidth;
        if (popupName == null || popupName.Length <= 0) {
            var theWindow = window.open(strUrl, "PopUpWindow", strFeatures, false);
        }
        else {
            var theWindow = window.open(strUrl, popupName, strFeatures, false);
        }
        theWindow.focus();
    }
    
    </script>

<table border="0" width="100%" id="">
	<tr>
		<td width="640"  align="left" valign="top" nowrap>
        <div style="height:500px; overflow:hidden;">
			<COMPONENTART:CALLBACK id="MainAssetImageCallBack" runat="server" CacheContent="false">	
			<ClientEvents>
            <CallbackComplete EventHandler="resizemainimagewrapper" />
            </ClientEvents>	 
				<Content>
                    <asp:PlaceHolder runat="server" ID="phimages">
                        <table><tr><td nowrap><b><asp:Literal ID="LiteralDragOption" runat="server" Visible="true"></asp:Literal></b></td><td>
                        <ComponentArt:ComboBox ID="ComboBoxDragOptions" KeyboardEnabled="false" runat="server" AutoHighlight="false"
                AutoComplete="true" AutoFilter="true" DataTextField="OptionText" DataValueField="OptionID"
                CssClass="ComboBoxDragOptionscomboBox" HoverCssClass="ComboBoxDragOptionscomboBoxHover"
                FocusedCssClass="ComboBoxDragOptionscomboBoxHover" TextBoxCssClass="ComboBoxDragOptionsTextBox"
                TextBoxHoverCssClass="ComboBoxDragOptionscomboBoxHover" DropDownCssClass="comboDropDown"
                ItemCssClass="ComboBoxDragOptionscomboBoxItem" ItemHoverCssClass="ComboBoxDragOptionscomboBoxItemHover"
                SelectedItemCssClass="ComboBoxDragOptionscomboBoxItemHover" DropHoverImageUrl="images/dropFilters.gif"
                DropImageUrl="images/dropFilters.gif" DropDownPageSize="10" Width="200">
                <ClientEvents><Change EventHandler="changedragresolution" /></ClientEvents>
            </ComponentArt:ComboBox></td><td>
            <!--<asp:Literal ID="LiteralLiveLink" runat="server" Visible="true"></asp:Literal>--></td></tr></table>
                        <asp:Image BorderWidth="0" ID="MainAssetImage" runat="server"></asp:Image>
                        <!--<div style="border:solid 2px black;position: absolute; top: 70px; left: 29px;filter: alpha(opacity=0.5);" id="divStealthMainAssetImage">-->
                            <asp:Image BorderWidth="0" ID="StealthMainAssetImage" Visible="true" runat="server" AlternateText="Drag Source Loaded" CssClass="StealthMainAssetImagecss"></asp:Image>
                        <!--</div>-->
                    </asp:PlaceHolder>
                </Content>
				<LOADINGPANELCLIENTTEMPLATE>
					<img border="0" src="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURL(Request.QueryString("Id"), 640, 480, False, 0, 2) & "&cache=1"%>">
				</LOADINGPANELCLIENTTEMPLATE>
			</COMPONENTART:CALLBACK>
			</div>
			<asp:Literal ID="LiteralFLVPlayer" Runat=server Visible=false></asp:Literal>


		</td>
		<td align="left" valign="top">
		<div style="padding:5px;">
		<asp:Literal ID="LiteralAssetSummary" Runat=server></asp:Literal>
		</div><!--padding 5px on all-->				
		</td>
	</tr>
	<tr>
		<td colspan="2" align="left" valign="top">
		
			<COMPONENTART:CALLBACK id="MainAssetPageCallBack" runat="server" CacheContent="false">
				<CONTENT>
					<asp:Literal ID="LiteralAssetPaging" Runat=server></asp:Literal>
				</CONTENT>
				<LOADINGPANELCLIENTTEMPLATE>
				Available page(s) for preview:  Loading...
				</LOADINGPANELCLIENTTEMPLATE>
			</COMPONENTART:CALLBACK>
			
		</td>
	</tr>
</table>
<input type="hidden" name="pptURL" id="pptURL" value="" />
<input type="hidden" name="browseURL" id="browseURL" value="" />
<input type="hidden" name="instance" id="instance" value="" />
<input type="hidden" name="id" id="id" value="" />
<input type="hidden" name="ids" id="ids" value="" />
<script language="javascript">

  function gotopage(tab)
  {

	// Get names
	var queryString = window.location.href.substring((window.location.href.indexOf('?') + 1));
	var array1 = queryString.split('&'); // Each array element is in format "fieldname=value"
	var IDValue;
	for(var i = 0; i < array1.length; i++){
		var tempArray = array1[i].split('='); // Separate fieldname and value
		if (tempArray[0]=='ID') {
			IDValue = tempArray[1]
		}
		if (tempArray[0]=='id') {
			IDValue = tempArray[1]
		}
		if (tempArray[0]=='Id') {
			IDValue = tempArray[1]
		}				
		//eval(tempArray[0] + " = \"" + tempArray[1] + "\"");
	}
  
  
  var newurl = 'IDAM.aspx?Page=Project&ID=' + IDValue + '&type=Project&subtype=' + tab.ID;
  //alert(newurl);
  window.location.href = newurl;
  return true;
  }
  
function getassetpage(PAGEID)
{
    <% Response.Write(MainAssetImageCallBack.ClientID) %>.Callback(PAGEID);
    <% Response.Write(MainAssetPageCallBack.ClientID) %>.Callback(PAGEID);
}
function openFull()
	{
	  flvplayer = document.getElementById("flvplayer");
	  flvplayer.Rewind();
	  //alert('hi');
	  //var flvObject = flvplayer.getVariables("myFLVPlayback");
	  //flvObject.Stop();
	  var fs = window.open( "flvplayerfs.aspx?asset_id=<%response.write(request.querystring("id"))%>&idam=getSmil.aspx&skinswf=clearOverAll.swf" ,"FullScreenVideo", "toolbar=no,width=" + screen.availWidth  + ",height=" + screen.availHeight + ",status=no,resizable=yes,fullscreen=yes,scrollbars=no");
	  fs.focus();
	}	
	 
function RotateAsset(asset_id,degrees)
{
<% Response.Write(MainAssetImageCallBack.ClientID) %>.Callback('ROTATE'+','+asset_id+','+degrees);	
    <% Response.Write(MainAssetPageCallBack.ClientID) %>.Callback('1');														
}

function downloadassetpageasjpeg(ipage)
{
window.location='DownloadPreview.aspx?id=<%response.write(request.querystring("id"))%>&page='+ipage;	
}
function downloadassetpageaspppt(ipage,media_type,asset_id)
{
myForm=document.forms[0];
document.getElementById("id").value='<%response.write(request.querystring("id")) %>_'+ipage+'_'+media_type+'_'+asset_id;
document.getElementById("pptURL").value='<%response.write(pPPTURL) %>';
document.getElementById("browseURL").value='<%response.write(pBROWSEURL) %>';
document.getElementById("instance").value='<%response.write(WebArchives.iDAM.Web.Core.IDAMWebSession.config.IDAMInstance)%>';
myForm.action = '<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMPPTExtractorLocation %>Default.aspx?type=downloadslide';
myForm.submit();
myForm.action = '';
myForm.target = '';
//myForm.target = 'myNewWin';
//window.open("","myNewWin","width=200,height=150,toolbar=0"); 
//var a = window.setTimeout("myForm.submit();",500); 
//var a = window.setTimeout("myForm.action = '';",600); 
//var a = window.setTimeout("myForm.target = '';",700); 
//return true;
}


var mousestate;
var mousetriggered;
var popuplocation;
mousetriggered = true;
var imageloaded;
imageloaded = false;


$(document).ready(function() {

    $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').animate({
       opacity: 0.1
      }, 0, function() {
    });
    
    
    
    
    

    $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').load(function() {
    if (!imageloaded) {
    imageloaded = true;
    resizemainimagewrapper();
    }
    });
    
    
    $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').mousedown(function() {
    if (!imageloaded)
    alert('Image not available yet.  Please try again or press F5 to try to reload the source image.');
    });

});

$(document).mousemove(function() {
try
{
dragsource = <% Response.Write(ComboBoxDragOptions.ClientID) %>.getSelectedItem().get_value();
} catch(e){}
try
{
resizemainimagewrapper();
} catch(e){}
});

</script>