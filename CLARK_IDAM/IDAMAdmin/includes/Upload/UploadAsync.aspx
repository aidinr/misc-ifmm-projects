﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UploadAsync.aspx.vb" Inherits="IdamAdmin.UploadAsync" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML>
<!--
/*EventArgs
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://creativecommons.org/licenses/MIT/
 */
--> 
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">
<title>iDAM Upload 2.0</title>

    

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/base/jquery-ui.css" id="theme">

<link rel="stylesheet" href="styles/jquery.fileupload-ui.css">
<link rel="stylesheet" href="styles/style.css">
    <style type="text/css">
        #description
        {
            height: 43px;
            width: 525px;
             
        }
    </style>

<script type="text/javascript" src="../../js/jquery.1.6.1.min.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.1.8.13.min.js"></script>
<script type="text/javascript" src="../../js/jquery.layout.min.js" ></script>
<script type="text/javascript" src="../../js/jquery.effects.slide.js"></script>
<script type="text/javascript" src="../../js/jquery.multiSelect.js" ></script>
<script type="text/javascript" src="../../js/jquery.cookie.js" ></script>
<script type="text/javascript" src="../../js/jquery.colorbox-min.js" ></script>

<script type="text/javascript" src="scripts/jquery.tmpl.min.js"></script>
<script type="text/javascript" src="scripts/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="scripts/jquery.fileupload.js"></script>
<script type="text/javascript" src="scripts/jquery.fileupload-ui.js"></script>
<script type="text/javascript" src="scripts/application.js"></script>

    <script>
        if ($.cookie('jqueryuithemeloaded') != null) {
            document.writeln('<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/' + $.cookie("jqueryuithemeloaded").replace(" ", "-").toLowerCase() + '/jquery-ui.css" rel="stylesheet" type="text/css"  media="all"/>');
        }
        
    </script>



</head>
<body onload="this.focus">
<script>
    <asp:literal id="JSVars" runat="server"/>
    function navigateParent() {
        //alert('np');
        if ((getParameter("pa") != "null") && (getParameter("pa") != "")) {
            //alert('a');
            window.opener.NavigateToAsset2(getParameter("pa"));
        } else {
            if (projectID != "") {
                if (categoryID != "") {
                    window.opener.NavigateToProjectFolder2(projectID, categoryID);
                } else {
                    window.opener.NavigateToProjectFolder2(projectID, '');
                }
            }
        }
        window.close();

    }
    var windowopener = window.opener;
    var upload = 0;
    var _info2 = navigator.userAgent;
    var Mac = (_info2.indexOf("Mac") > 0);

    function getParameter(parameterName) {
        try {
            var queryString = window.location.href;
            // Add "=" to the parameter name (i.e. parameterName=value)
            var parameterName = parameterName + "=";
            if (queryString.length > 0) {
                // Find the beginning of the string
                begin = queryString.indexOf(parameterName);
                // If the parameter name is not found, skip it, otherwise return the value
                if (begin != -1) {
                    // Add the length (integer) to the beginning
                    begin += parameterName.length;
                    // Multiple parameters are separated by the "&" sign
                    end = queryString.indexOf("&", begin);
                    if (end == -1) {
                        end = queryString.length
                    }
                    // Return the string
                    return unescape(queryString.substring(begin, end));
                }
                // Return "null" if no parameter has been found
                return "null";
            }
        }
        catch (err) {
            alert(err.description);
        }

    }

    function navigateToParentWindow() {

        if ((getParameter("pa") != "null") && (getParameter("pa") != "")) {
            window.opener.NavigateToAsset2(getParameter("pa"));
        } else {
            if (projectID != "") {
                if (categoryID != "") {
                    window.opener.NavigateToProjectFolder3(projectID, categoryID);
                } else {
                    window.opener.NavigateToProjectFolder3(projectID, '');
                }
            }
        }
    }






    	<%if request.querystring("closewindow") = "true" then%>
        <%="var projectID;projectID='" & request.querystring("projectid") & "';var categoryID;categoryID='" &  request.querystring("category_id") & "';"%>
    
		if (getParameter("error") != "true") {
				alert("The upload was successfull.");
			    navigateToParentWindow();
				window.close();
		} else {
		   
			if ("null" === getParameter("msg")) {
				alert("The upload failed. Please try again, or contact your system administrator if the problem persist");
			} else {
				alert(getParameter("msg"));
			}
			if (getParameter("closewindow") == "true") {
				navigateParent();
			}
		}
		
		<%end if%>















		</script>

        


<div id="fileupload">
    <form action="UploadAsyncHandler.ashx" method="POST" enctype="multipart/form-data">
   


	<script language="JavaScript">

    		


    <asp:literal id="ErrorLiteral" runat="server"/>
    
		var _info = navigator.userAgent;
        var ie = (_info.indexOf("MSIE") > 0);
        var win = (_info.indexOf("Win") > 0);
			
										</script>


Upload asset to project: [ <b>
<asp:label id="LabelProjectName" runat="server">Label</asp:label></b>]<br>
Category: [ <b>
<asp:label id="LabelCategoryName" runat="server">Label</asp:label></b>]<br>
<asp:literal id="PARENT_ASSET" Visible="False" Runat="server"></asp:literal><br />

<%If javauploadurl <> "" Then%>
<div style="position:relative;left:565px;top:-40px;">
<input type=button class="fileinput-button" name=switchtojavaupload id=switchtojavaupload value="Switch to Batch Upload" onclick="window.location.href='<%=javauploadurl %>';" />
</div>
<% end if%>

<input type=hidden name=GetRetrieveIconURLBase id=GetRetrieveIconURLBase value="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveIconURLBase %>" />
<input type=hidden name=isessionid id=isessionid value="<%=session.sessionid %>" />
<input type=hidden name=objectid id=objectid value="<%=request.querystring("objectid") %>" />
<input type=hidden name=projectid id=projectid value="<%=request.querystring("projectid") %>" />
<input type=hidden name=categoryid id=categoryid value="<%=request.querystring("categoryid") %>" />
<input type=hidden name=uid id=uid value="<%=request.querystring("uid") %>" />
<input type=hidden name=instance id=instance value="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMInstance %>" />
<input type=hidden name=pa id=pa value="<%=request.querystring("pa") %>" />

    

    Step 1: <b>Add Description</b> (optional)<br />

            

 

<div style=" padding:10px;font-family: verdana;
                        color: #3F3F3F; font-size: 10px; font-weight: normal;">
                        <div style="float:left;" class="ui-state-default ui-corner-all" title=".ui-icon-notice"><span class="ui-icon ui-icon-notice"></span></div><div style="float:left;position:relative;top:3px;padding-left:10px;width:450px;"> 
                        Add a description or keyword(s) to your assets before uploading.</div>
                        </div>
                        <div style="float:left;width:100%;"></div>
                        <div style=" padding:10px;font-family: verdana;
                        color: #3F3F3F; font-size: 10px; font-weight: normal;">
                        Description<br />

<textarea id=description name=description style="width:400px" ></textarea><br />
<!--Keywords<br />
<button type="button" class="fileinput-button">Add Tag</button>
<button type="button" class="fileinput-button">Add Services</button>
<button type="button" class="fileinput-button">Add Illusttype</button>
<button type="button" class="fileinput-button">Add MediaType</button>-->
</div>


                        <br />
                      


Step 2: <b>Add Files</b>
<br /><br />

        <div class="fileupload-buttonbar">
            <label class="fileinput-button">
                <span>Add files...</span>
                <input type="file" name="files[]" multiple="multiple" />
            </label>
            <button type="submit" class="start">Start upload</button>
            <button type="reset" class="cancel">Cancel upload</button> 
            <!--<button type="button" class="delete">Delete all files</button>-->
			<div class="fileupload-progressbar"></div>
        </div>
    </form>
    <div class="fileupload-content" style="min-height:280px;">
<div style=" padding:10px;font-family: verdana;
                        color: #3F3F3F; font-size: 10px; font-weight: normal;">
                        <div style="float:left;" class="ui-state-default ui-corner-all" title=".ui-icon-notice"><span class="ui-icon ui-icon-notice"></span></div><div style="float:left;position:relative;top:3px;padding-left:10px;width:450px;"> 
                        Upload files by selecting the "Add files..." button
                        <script>
                        	var _info = navigator.userAgent;
                            var ie = (_info.indexOf("MSIE") > 0);
                            var win = (_info.indexOf("Win") > 0);
                               if (!ie) {document.writeln('or by dragging and dropping files from your desktop here');}
                        </script>
                        .  Once all files have been selected, press the "Start upload" button to begin the upload process.</div>
                        </div>
                        <div style="width:100%;"></div>
                        <br />
                      
        <table class="files"></table>
    </div>
</div>
<script id="template-upload" type="text/x-jquery-tmpl">
    <tr class="template-upload{{if error}} ui-state-error{{/if}}">
        <td class="preview"></td>
        <td class="name">${name}</td>
        <td class="size">${sizef}</td>
        {{if error}}
            <td class="error" colspan="2">Error:
                {{if error === 'maxFileSize'}}File is too big
                {{else error === 'minFileSize'}}File is too small
                {{else error === 'acceptFileTypes'}}Filetype not allowed
                {{else error === 'maxNumberOfFiles'}}Max number of files exceeded
                {{else}}${error}
                {{/if}}
            </td>
        {{else}}
            <td class="progress"><div></div></td>
            <td class="start"><button>Start</button></td>
        {{/if}}
        <td class="cancel"><button>Cancel</button></td>
    </tr>
</script>
<script id="template-download" type="text/x-jquery-tmpl">
    <tr class="template-download{{if error}} ui-state-error{{/if}}">
        {{if error}}
            <td></td>
            <td class="name">${name}</td>
            <td class="size">${sizef}</td>
            <td class="error" colspan="2">Error:
                {{if error === 1}}File exceeds upload_max_filesize (php.ini directive)
                {{else error === 2}}File exceeds MAX_FILE_SIZE (HTML form directive)
                {{else error === 3}}File was only partially uploaded
                {{else error === 4}}No File was uploaded
                {{else error === 5}}Missing a temporary folder
                {{else error === 6}}Failed to write file to disk
                {{else error === 7}}File upload stopped by extension
                {{else error === 'maxFileSize'}}File is too big
                {{else error === 'minFileSize'}}File is too small
                {{else error === 'acceptFileTypes'}}Filetype not allowed
                {{else error === 'maxNumberOfFiles'}}Max number of files exceeded
                {{else error === 'uploadedBytes'}}Uploaded bytes exceed file size
                {{else error === 'emptyResult'}}Empty file upload result
                {{else}}${error}
                {{/if}}
            </td>
        {{else}}
            <td class="preview">
                {{if thumbnail_url}}
                    <a href="${url}" target="_blank"><img src="${thumbnail_url}"></a>
                {{/if}}
            </td>
            <td class="name">
                <a href="${url}"{{if thumbnail_url}} target="_blank"{{/if}}>${name}</a>
            </td>
            <td class="size">${sizef}</td>
            <td colspan="2"></td>
        {{/if}}
        <td class="delete">
            <button data-type="${delete_type}" data-url="${delete_url}">Delete</button>
        </td>
    </tr>
</script>


</body> 
</html>
