﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="JSLibNoCache.aspx.vb" Inherits="IdamAdmin.JSLibNoCache" %>
function switchView(view) {
if (view == 'list') {
setLastView ('list');
document.getElementById('<%Response.Write(WebArchives.iDAM.Web.Core.IDAMWebSession.Config.Thumb_Icon_ClientID)%>').src="images/icon_thumb_on.gif";
document.getElementById('<%Response.Write(WebArchives.iDAM.Web.Core.IDAMWebSession.Config.List_Icon_ClientID)%>').src="images/icon_list_off.gif";
executeViewCallback(view);
} else {
setLastView ('thumb');
document.getElementById('<%Response.Write(WebArchives.iDAM.Web.Core.IDAMWebSession.Config.Thumb_Icon_ClientID)%>').src="images/icon_thumb_off.gif";
document.getElementById('<%Response.Write(WebArchives.iDAM.Web.Core.IDAMWebSession.Config.List_Icon_ClientID)%>').src="images/icon_list_on.gif";
executeViewCallback(view);
}
}
function switchViewResults(view) {
if (view == 'list') {
setLastView ('list');
document.getElementById('<%Response.Write(WebArchives.iDAM.Web.Core.IDAMWebSession.Config.Thumb_Icon_ClientID)%>').src="images/icon_thumb_on.gif";
document.getElementById('<%Response.Write(WebArchives.iDAM.Web.Core.IDAMWebSession.Config.List_Icon_ClientID)%>').src="images/icon_list_off.gif";
executeViewCallback(view);
} else {
setLastView ('thumb');
document.getElementById('<%Response.Write(WebArchives.iDAM.Web.Core.IDAMWebSession.Config.Thumb_Icon_ClientID)%>').src="images/icon_thumb_off.gif";
document.getElementById('<%Response.Write(WebArchives.iDAM.Web.Core.IDAMWebSession.Config.List_Icon_ClientID)%>').src="images/icon_list_on.gif";
executeViewCallback(view);
}
}
