<%function formattime(mytime)
	if (hour(mytime) >= 1) and (hour(mytime) <= 11) then 
		myhour = hour(mytime)
		myperiod = "am"
	end if	
	if minute(mytime) < 10 then
		mymin = "0" & minute(mytime)
	else
		mymin = minute(mytime)
	end if
	if (hour(mytime) >= 13) and (hour(mytime) <= 23) then 
		myhour = hour(mytime) - 12
		myperiod = "pm" 
	end if 
	'Response.Write myhour & ":" & mymin & " " & myperiod
	Response.Write formatDateTime(mytime, 2) & " " & myhour & myperiod
end function


function roundTime(time)
	Response.Write hour(time)
	myhour= hour(time)
	mymin= minute(time)
	if (myhour = 3) or (myhour = 2 and mymin > 0) or (myhour = 4 and mymin = 0) then
		Response.Write "3AM"
	elseif (myhour = 6) or (myhour = 4 and mymin > 0) or (myhour = 4 and mymin = 0) then
		Response.Write "6AM"
	end if
end function


function getRole(roleItemId)
   If instr(1,session("Roles"),"," & trim(roleItemId) & ",") <> 0 then 
      getRole =  true
   Else
      getRole =  false
   End If
end function

function getFieldIsEditable(FieldItemId)
   If instr(1,session("FieldRoles"),"," & FieldItemId & "_Editable,") <> 0 then 
      getFieldIsEditable =  true
   Else
      getFieldIsEditable =  false
   End If
end function


function getFieldIsViewable(FieldItemId)
   If instr(1,session("FieldRoles"),"," & FieldItemId & "_Viewable,") <> 0 then 
      getFieldIsViewable =  true
   Else
      getFieldIsViewable =  false
   End If
end function

'DataPoint Logging Scripts

function log_login()
	if blnLogging then
		Conn.execute "insert into ipm_usage_login (session_id,user_id,referrer,login_date,browser) values (" & Session.SessionID & "," & IDAMWebSession.IDAMUser.userid & ",'" & Request.ServerVariables ("HTTP_REFERER") & "','" & date() & " " & time() & "','" & Request.ServerVariables ("HTTP_USER_AGENT") & "')"
	end if
end function


function log_Search()
	if trim(iCountResults ) = "" then iCountResults = 0
	if blnLogging then
		Conn.execute "insert into ipm_usage_Search (session_id,user_id,Keyword,Services,MediaType,IllustType,referrer,Search_date,browser,Type,num_results) values (" & Session.SessionID & "," & IDAMWebSession.IDAMUser.userid & ",'" & replace(request.form("keyword"),"'","''") & "','" & replace(request.form("keywordservices"),"'","''") & "','" & replace(request.form("keywordmediatype"),"'","''") &  "','" & replace(request.form("keywordillustrationtype"),"'","''") & "','" & Request.ServerVariables ("HTTP_REFERER") & "','" & date() & " " & time() & "','" & Request.ServerVariables ("HTTP_USER_AGENT") & "','QIK'," & iCountResults & ")"
	end if
end function



function log_Search_Adv()
	if trim(iCountResults ) = "" then iCountResults = 0
	if blnLogging then
		Conn.execute "insert into ipm_usage_Search (session_id,user_id,Keyword,Services,MediaType,IllustType,referrer,Search_date,browser,Type,num_results) values (" & Session.SessionID & "," & IDAMWebSession.IDAMUser.userid & ",'" & replace(request.form("keyword"),"'","''") & "','" & replace(request.form("keywordservices"),"'","''") & "','" & replace(request.form("keywordmediatype"),"'","''") &  "','" & replace(request.form("keywordillustrationtype"),"'","''") & "','" & Request.ServerVariables ("HTTP_REFERER") & "','" & date() & " " & time() & "','" & Request.ServerVariables ("HTTP_USER_AGENT") & "','ADV'," & iCountResults & ")"
	end if
end function


function log_Project_View()
	if blnLogging then
		if request.querystring("c") = "" then
			sLogCat = 0
		end if
		Conn.execute "insert into ipm_usage_Project (session_id,user_id,Project_id,Category_id,referrer,Access_date) values (" & Session.SessionID & "," & IDAMWebSession.IDAMUser.userid & "," & request.querystring("p") & "," & sLogCat & ",'" & Request.ServerVariables ("HTTP_REFERER") & "','" & date() & " " & time() & "')"
		if blnRating then
			Conn.execute "exec sp_updateprojectrating " & request.querystring("p") & ",1"
		end if
	end if
end function

function log_Project_View()
	if blnLogging then
		if request.querystring("c") = "" then
			sLogCat = 0
		else
			sLogCat = trim(request.querystring("c"))
		end if
		Conn.execute "insert into ipm_usage_Project (session_id,user_id,Project_id,Category_id,referrer,Access_date) values (" & Session.SessionID & "," & IDAMWebSession.IDAMUser.userid & "," & request.querystring("p") & "," & sLogCat & ",'" & Request.ServerVariables ("HTTP_REFERER") & "','" & date() & " " & time() & "')"
		if blnRating then
			Conn.execute "exec sp_updateprojectrating " & request.querystring("p") & ",1"
		end if
	end if
end function

function log_Asset_View()
	if blnLogging then
		if request.querystring("c") = "" then
			sLogCat = 0
		else
			sLogCat = trim(request.querystring("c"))
		end if
		if trim(request.querystring("p")) = "" then
			sLogProj = 0
		else
			sLogProj = trim(request.querystring("p"))
		end if		
		if trim(request.querystring("post")) = "" then
			sLogPost = 0
		else
			sLogPost = trim(request.querystring("post"))
		end if			
		Conn.execute "insert into ipm_usage_Asset (session_id,user_id,Asset_id,Project_id,Category_id,Post_id,referrer,Access_date) values (" & Session.SessionID & "," & IDAMWebSession.IDAMUser.userid & "," & request.querystring("a") & "," & sLogProj & "," & sLogCat & "," & sLogPost & ",'" & Request.ServerVariables ("HTTP_REFERER") & "','" & date() & " " & time() & "')"
		if blnRating then
			if instr(Request.ServerVariables("HTTP_REFERER"),"project_upload_asset.asp") = 0 then
				'Conn.execute "exec sp_updateassetrating " & request.querystring("a") & ",1"
				Conn.execute "update ipm_asset set rating = (select rating from ipm_asset where asset_id = " & request.querystring("a") & ") + 1 where asset_id = " & request.querystring("a") 
			end if
		end if
	end if
end function


function log_Download(refAsset_id)
	if refAsset_id <> 0 then
		sAsset_ID = refAsset_id
	end if
	if blnLogging then
		if sDownload_ID = "" then
			sDownload_ID = 0
		end if	
		Conn.execute  "insert into ipm_usage_Download (session_id,user_id,Download_Type,Asset_id,Download_id,referrer,Access_date) values (" & Session.SessionID & "," & IDAMWebSession.IDAMUser.userid & ",'" & sDType & "'," & sAsset_ID & "," & sDownload_ID & ",'" & Request.ServerVariables ("HTTP_REFERER") & "','" & date() & " " & time() & "')"
		if blnRating then
			Conn.execute "exec sp_updateassetrating " & sAsset_ID & ",4"
		end if
	end if
end function

function getviewstate(vspage)
on error resume next
	Dim objSvrHTTP
	Set objSvrHTTP = Server.CreateObject("Msxml2.XMLHTTP.4.0")
	objSvrHTTP.open "GET", vspage, false

	objSvrHTTP.send 
	result =  objSvrHTTP.responseText 
	beginpos =Instr(result,"VIEWSTATE"" value=""")
	result =Mid(result,beginpos,len(result))
	endpos =Instr(result,""" />")
	result = Mid(result,19,endpos - 19)
	getviewstate = result
end function

Function FormatSize(dwFileSize)
  Dim dwKB 
  Dim dwMB 
  Dim dwGB 
  dwKB = 1024
  dwMB = dwKB * 1024
  dwGB = dwMB * 1024
  Dim dwNumber, dwRemainder 
  Dim strNumber 
  dwFileSize = clng(dwFileSize)

  If (dwFileSize < dwKB) Then
    dwRemainder = (dwFileSize * 100 / dwKB) * 10
    'strNumber = Format(dwRemainder, "#,###")
    strNumber = formatnumber(dwRemainder,0)& " B"
  Else
    If (dwFileSize < dwMB) Then
      dwNumber = dwFileSize / dwKB
      dwRemainder = (dwFileSize * 100 / dwKB) * 0.01
      'strNumber = Format(dwRemainder, "#,###")
      strNumber = formatnumber(dwRemainder,1) & " KB"
    Else
      If (dwFileSize < dwGB) Then
        dwNumber = dwFileSize / dwMB
        dwRemainder = (dwFileSize * 100 / dwMB) * 0.01
        'strNumber = Format(dwRemainder, "#,###")
        strNumber = formatnumber(dwRemainder)& " MB"
      Else
        If (dwFileSize >= dwGB) Then
          dwNumber = dwFileSize / dwGB
          dwRemainder = (dwFileSize * 100 / dwGB) * 0.01
          'strNumber = Format(dwRemainder, "#,###")
          strNumber = formatnumber(dwRemainder)& " GB"
        End If
    End If
    End If
   End If
  FormatSize = strNumber
End Function
 %>