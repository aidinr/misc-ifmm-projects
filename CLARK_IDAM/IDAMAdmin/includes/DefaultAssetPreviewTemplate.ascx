<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.DefaultAssetPreviewTemplate" CodeBehind="DefaultAssetPreviewTemplate.ascx.vb" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<style>
.StealthMainAssetImagecss
{
	border:solid 1px black;position: relative; top: 0px; left: 0px; filter: alpha(opacity=0.1);
}
</style>

<script type="text/javascript">
    var dragsource

    function resizemainimagewrapper() {

    try{
        dragsource = <% Response.Write(ComboBoxDragOptions.ClientID) %>.getSelectedItem().get_value();
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').attr('src', dragsource);
    } catch(e){}
try{
        var _MainAssetImagewidth;
        var _MainAssetImageheight;
        _MainAssetImagewidth = $('#<% Response.Write(MainAssetImage.ClientID) %>').attr('width');
        _MainAssetImageheight = $('#<% Response.Write(MainAssetImage.ClientID) %>').attr('height');
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').attr('width', _MainAssetImagewidth);
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').attr('height', _MainAssetImageheight);
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').css('top', '-' + $('#<% Response.Write(MainAssetImage.ClientID) %>').attr('height') + 'px');

   } catch(e){}

    }
    
    
    function changedragresolution() {
    try{
    
        var selitem = <% Response.Write(ComboBoxDragOptions.ClientID) %>.getSelectedItem().get_text();
        var selitemval = <% Response.Write(ComboBoxDragOptions.ClientID) %>.getSelectedItem().get_value();
        dragsource = selitemval;
        resizemainimage();
    } catch(e){}
    }
    
    
    function resizemainimage() {
    try{
        var _MainAssetImagewidth;
        var _MainAssetImageheight;
        _MainAssetImagewidth = $('#<% Response.Write(MainAssetImage.ClientID) %>').attr('width');
        _MainAssetImageheight = $('#<% Response.Write(MainAssetImage.ClientID) %>').attr('height');
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').attr('width', _MainAssetImagewidth);
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').attr('height', _MainAssetImageheight);
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').attr('src', dragsource);
    } catch(e){}
    }
 
 
</script>

<table border="0" width="100%" id="">
	<tr>
		<td width=640 height=480  align="left" valign="top" nowrap>
<div style="height:500px; overflow:hidden;">
<ComponentArt:CallBack ID="MainAssetImageCallBack" runat="server" CacheContent="false">
<ClientEvents>
<CallbackComplete EventHandler="resizemainimagewrapper" />
</ClientEvents>
    <Content>
        <asp:PlaceHolder runat="server" ID="phimages">
            
            <asp:Image BorderWidth="0" ID="MainAssetImage" runat="server"></asp:Image><br />
            <asp:Image BorderWidth="0" ID="StealthMainAssetImage" Visible="true" runat="server" AlternateText="test" CssClass="StealthMainAssetImagecss"></asp:Image>
           
        </asp:PlaceHolder>
    </Content>
    <LoadingPanelClientTemplate>
        <img border="0" src="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURL(Request.QueryString("Id"), 640, 480, False, 0, 2) & "&cache=1"%>">
    </LoadingPanelClientTemplate>
</ComponentArt:CallBack>
</div>


			<asp:Literal ID="LiteralFLVPlayer" Runat=server Visible=false></asp:Literal>


		</td>
		<td align="left" valign="top">
		<div style="padding:5px;">
		<asp:Literal ID="LiteralAssetSummary" Runat=server></asp:Literal>
		<br /><br /><b><asp:Literal ID="LiteralDragOption" runat="server" Visible="true"></asp:Literal></b><br />
            <ComponentArt:ComboBox ID="ComboBoxDragOptions" runat="server" AutoHighlight="false"
    AutoComplete="false" AutoFilter="true" DataTextField="OptionText" DataValueField="OptionID"
    CssClass="ComboBoxDragOptionscomboBox" HoverCssClass="ComboBoxDragOptionscomboBoxHover"
    FocusedCssClass="ComboBoxDragOptionscomboBoxHover" TextBoxCssClass="ComboBoxDragOptionsTextBox"
    TextBoxHoverCssClass="ComboBoxDragOptionscomboBoxHover" DropDownCssClass="comboDropDown"
    ItemCssClass="ComboBoxDragOptionscomboBoxItem" ItemHoverCssClass="ComboBoxDragOptionscomboBoxItemHover"
    SelectedItemCssClass="ComboBoxDragOptionscomboBoxItemHover" DropHoverImageUrl="images/dropFilters.gif"
    DropImageUrl="images/dropFilters.gif" DropDownPageSize="10" width="200"  >
    <ClientEvents><Change EventHandler="changedragresolution" /><Init EventHandler="changedragresolution" /></ClientEvents>
</ComponentArt:ComboBox>



<!--<asp:Literal ID="LiteralLiveLink" runat="server" Visible="true"></asp:Literal>-->
		</div><!--padding 5px on all-->				
		</td>
	</tr>
	<tr>
		<td colspan="2" align="left" valign="top">
		
			<COMPONENTART:CALLBACK id="MainAssetPageCallBack" runat="server" CacheContent="false">
				<CONTENT>
					<asp:Literal ID="LiteralAssetPaging" Runat=server></asp:Literal>
				</CONTENT>
				<LOADINGPANELCLIENTTEMPLATE>
				Available page(s) for preview:  Loading...
				</LOADINGPANELCLIENTTEMPLATE>
			</COMPONENTART:CALLBACK>
			
		
		</td>
	</tr>
</table>
<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("EDIT_ASSETS","asset",spAsset_ID) Then%>
        <div style="position:relative;top:2px;z-index:1;">
		<br><b>Rotate:</b> [ <a href="javascript:RotateAsset('<%response.write(request.querystring("ID"))%>','-90');">left</a> ] [ <a href="javascript:RotateAsset('<%response.write(request.querystring("ID"))%>','90');">right</a> ]
     	</div>
		<%end if%>
<script language="javascript">

  function gotopage(tab)
  {

	// Get names
	var queryString = window.location.href.substring((window.location.href.indexOf('?') + 1));
	var array1 = queryString.split('&'); // Each array element is in format "fieldname=value"
	var IDValue;
	for(var i = 0; i < array1.length; i++){
		var tempArray = array1[i].split('='); // Separate fieldname and value
		if (tempArray[0]=='ID') {
			IDValue = tempArray[1]
		}
		if (tempArray[0]=='id') {
			IDValue = tempArray[1]
		}
		if (tempArray[0]=='Id') {
			IDValue = tempArray[1]
		}				
		//eval(tempArray[0] + " = \"" + tempArray[1] + "\"");
	}
  
  
  var newurl = 'IDAM.aspx?Page=Project&ID=' + IDValue + '&type=Project&subtype=' + tab.ID;
  //alert(newurl);
  window.location.href = newurl;
  return true;
  }
  
function getassetpage(PAGEID)
{
    <% Response.Write(MainAssetImageCallBack.ClientID) %>.Callback(PAGEID);
    <% Response.Write(MainAssetPageCallBack.ClientID) %>.Callback(PAGEID);
}
function openFull()
	{
	  flvplayer = document.getElementById("flvplayer");
	  flvplayer.Rewind();
	  //alert('hi');
	  //var flvObject = flvplayer.getVariables("myFLVPlayback");
	  //flvObject.Stop();
	  var fs = window.open( "flvplayerfs.aspx?asset_id=<%response.write(request.querystring("id"))%>&idam=getSmil.aspx&skinswf=clearOverAll.swf" ,"FullScreenVideo", "toolbar=no,width=" + screen.availWidth  + ",height=" + screen.availHeight + ",status=no,resizable=yes,fullscreen=yes,scrollbars=no");
	  fs.focus();
	}	
	 
function RotateAsset(asset_id,degrees)
{
<% Response.Write(MainAssetImageCallBack.ClientID) %>.Callback('ROTATE'+','+asset_id+','+degrees);	
	 
    window.setTimeout('RefreshPageFR()',2000);							
}

function RefreshPageFR()
{
window.location.href=window.location.href
}

var mousestate;
var mousetriggered;
var popuplocation;
mousetriggered = true;
    



$(document).ready(function() {

    $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').animate({
       opacity: 0.0
      }, 0, function() {
      
    });
    
    window.setTimeout('resizemainimagewrapper()',500);
      

    $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').mousedown(function() {
        $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').mousestate = true;
    });
    $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').mouseup(function() {
        mousestate = false;
    }); 
    $('#<% Response.Write(StealthMainAssetImage.ClientID) %>').mousemove(function() {
    try{
            dragsource = <% Response.Write(ComboBoxDragOptions.ClientID) %>.getSelectedItem().get_value();
            resizemainimage();
            if (mousestate) {
                if (mousetriggered) {
                    mousetriggered = false;
                }
           }
       } catch(e){}
    });
});

</script>