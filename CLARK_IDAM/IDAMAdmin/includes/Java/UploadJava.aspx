
<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.UploadJava" CodeBehind="UploadJava.aspx.vb" %>
<%@ Register TagPrefix="upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Upload</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../multipageStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../treeStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../navStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../tabStripStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../navBarStyle.css" type="text/css" rel="stylesheet">
		<style>
		</style>
	</HEAD>
	<body onload="this.focus">
		<script>
	
		function navigateParent() {
		//alert('np');
				if ((getParameter("pa") != "null")&&(getParameter("pa") != "")) {
					//alert('a');
					window.opener.NavigateToAsset2(getParameter("pa"));
				} else {
					if (getParameter("projectid") != "null") {
						if (getParameter("categoryid") != "null") {
							window.opener.NavigateToProjectFolder2(getParameter("projectid"),getParameter("categoryid"));
						} else {
							window.opener.NavigateToProjectFolder2(getParameter("projectid"),'');
						}
					}
				}
				window.close();
			
		}
	var windowopener=window.opener;
		</script>
		<script src="script.js" type="text/javascript"></script>
		<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; PADDING-TOP: 10px">
			<div style="WIDTH: 100%">
			
			<%if request.querystring("closewindow")<>"true" then%>
			
			
			
			
				<div style="BORDER-RIGHT: 1px solid; PADDING-RIGHT: 10px; BORDER-TOP: 1px solid; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; BORDER-LEFT: 1px solid; PADDING-TOP: 10px; BORDER-BOTTOM: 1px solid; POSITION: relative; BACKGROUND-COLOR: white">
					<div style="BORDER-RIGHT: #b7b4b4 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #b7b4b4 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: normal; FONT-SIZE: 10px; PADDING-BOTTOM: 5px; BORDER-LEFT: #b7b4b4 1px solid; COLOR: #3f3f3f; PADDING-TOP: 5px; BORDER-BOTTOM: #b7b4b4 1px solid; FONT-FAMILY: verdana; BACKGROUND-COLOR: #e4e4e4">Upload 
						assets to the following location:</div>
					<table>
						<tr>
							<td width="400">
								<div style="FONT-SIZE: 10px; WIDTH: 100%"><br>
									Upload asset to project: [ <b>
										<asp:label id="LabelProjectName" runat="server">Label</asp:label></b>]<br>
									Category: [ <b>
										<asp:label id="LabelCategoryName" runat="server">Label</asp:label></b>]<br>
									<asp:literal id="PARENT_ASSET" Visible="False" Runat="server"></asp:literal><br>
									
									<form id="Form1" runat="server">
										<%if request.querystring("pa") = "" and Session("UseJavaUpload") = "1" then%>
										<input type=checkbox onclick="javascript:setCreateSubFoldersCheckbox()" ID="createsubfolders">&nbsp;Create subfolders (When uploading folders 
										with subfolders)
										<%end if%>
										<!--<input id="createsubfoldershidden"  type="hidden" value="true" name="createsubfoldershidden">-->
										<br>
										<br>
										<script language="JavaScript">
    <asp:literal id="ErrorLiteral" runat="server"/>
    
		var _info = navigator.userAgent;
        var ie = (_info.indexOf("MSIE") > 0);
        var win = (_info.indexOf("Win") > 0);
			
										</script>
										<%if Session("UseJavaUpload") = "1" then%>
										<script language="JavaScript">

		
        if(win)
        {
        
            if(ie)
            {

		    document.writeln('<object classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93"');
		    document.writeln('      width= "290" height= "290" id="rup"');
		    document.writeln('      codebase="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLPRefix%>java.sun.com/update/1.5.0/jinstall-1_5-windows-i586.cab#version=1,4,1">');
		    document.writeln('<param name="archive" value="dndplus.jar">');
		    document.writeln('<param name="code" value="com.radinks.dnd.DNDAppletPlus">');
		    document.writeln('<param name="name" value="Rad Upload Plus">');
           }
            else
            {
                document.writeln('<object type="application/x-java-applet;version=1.4.1"');
                document.writeln('width= "290" height= "290"  id="rup">');
                document.writeln('<param name="archive" value="dndplus.jar">');
                document.writeln('<param name="code" value="com.radinks.dnd.DNDAppletPlus">');
                document.writeln('<param name="name" value="Rad Upload Plus">');
                document.writeln('<param name="MAYSCRIPT" value="yes">');
            }
        }
        else
        {
            /* mac and linux */
            document.writeln('<applet ');
            document.writeln('              archive  = "dndplus.jar"');
            document.writeln('                      code     = "com.radinks.dnd.DNDAppletPlus"');
            document.writeln('                      name     = "Rad Upload Plus"');
            document.writeln('                      hspace   = "0"');
            document.writeln('                      vspace   = "0" MAYSCRIPT');
            document.writeln('                      width = "290"');
            document.writeln('                      height = "290"');
            document.writeln('                      align    = "middle" id="rup">');
        }

/******    BEGIN APPLET CONFIGURATION PARAMETERS   ******/
	
    document.writeln('<param name="max_upload" value="2000000">');
    document.writeln('<param name="message" value="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLPRefix%><%response.write(sBaseURL)%>/imagejava.html">');
    document.writeln('<param name="url" value="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLPRefix%><%response.write(sBaseURL)%>/includes/java/UploadJava.aspx?NeatUpload_PostBackID=<%=spPostBackID%>&projectid=<%response.write(request.querystring("projectid"))%>&categoryid=<%response.write(request.querystring("categoryid"))%>&objectid=<%response.write(request.querystring("objectid"))%>&pa=<%response.write(request.querystring("pa"))%>&ap=<%response.write(WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("REQUIRE_APPROVAL",spWOType,spWOID,sProjectid))%>&uid=<%response.write(webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.UserID.tostring)%>&CfgId=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.ActiveConfigId%>&sf=<%=request.querystring("sf")%>">');
    document.writeln('<param name="message" value="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLPRefix%><%response.write(sBaseURL)%>/imagejava.html">');
    document.writeln('<param name="full_path" value="yes">');
    document.writeln('<param name="send_button" value="1">');
    document.writeln('<param name="queue" value="yes">');
    document.writeln('<param name="jsnotify" value="no">');
    document.writeln('<param name="browse" value="yes">');
    document.writeln('<param name="browse_button" value="yes">');
    document.writeln('<param name="show_thumb" value="yes">');
    document.writeln('<param name="permission_denied" value="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLPRefix%><%response.write(sBaseURL)%>/includes/java/denied.html">');
    //document.writeln('<param name="external_redir" value="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLPRefix%><%response.write(sBaseURL)%>/includes/java/blank.html">');
    document.writeln('<param name="external_redir" value="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLPRefix%><%response.write(sBaseURL)%>/includes/java/UploadJava.aspx?projectid=<%response.write(request.querystring("projectid"))%>&categoryid=<%response.write(request.querystring("categoryid"))%>&pa=<%response.write(request.querystring("pa"))%>&closewindow=true">');
    document.writeln('<param name="external_target" value="_top">');
    document.writeln('<param name="redirect_delay" value="1000">');


/******    END APPLET CONFIGURATION PARAMETERS     ******/
       if(win)
	   {
		  document.writeln('</object>');
	   }
	   else

	   {

		  document.writeln('</applet>');
	   }
    

    
    
    
										</script>
										<%else%>
										<%'non java upload%>
										<script type="text/javascript">
										    window.onload = function() {
										        var inlineProgressBar = NeatUploadPB.prototype.Bars["progressBar"];
										        var origDisplay = inlineProgressBar.Display;
										        inlineProgressBar.Display = function() {
										            var elem = document.getElementById(this.ClientID);
										            elem.parentNode.style.display = "block";
										            origDisplay.call(this);
										        }
										        inlineProgressBar.EvalOnClose = "NeatUploadMainWindow.document.getElementById('" + inlineProgressBar.ClientID + "').parentNode.style.display = \"none\";";
										    }
                                        </script>
										Upload File(s)
										<br>
										 										<Upload:InputFile Class="InputFieldMain" id="InputFile1" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile2" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile3" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile4" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile5" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile6" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile7" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile8" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile9" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile10" runat="server"></Upload:InputFile>
										<br>
										
                                        <div style="display: none; height:90px">
										<Upload:ProgressBar Inline="true" id="progressBar" runat="server" Triggers="btnUpload linkButton commandButton htmlInputButtonButton htmlInputButtonSubmit"></Upload:ProgressBar>
										</div>
										<asp:Button id="btnUpload" CssClass="InputButtonMain" runat="server" Text="Upload Assets"></asp:Button>
										<%end if%>
								</div>
							</td>
							<td vAlign="top" align="right">
								<br>
								<!--<asp:Button id="Switch_Simple" CssClass="InputButtonMain" runat="server" Text="Switch upload type"></asp:Button><br>-->
								<IMG height="25" src="images/spacer.gif" width="1"><br>
								<div style="BORDER-RIGHT:#b7b4b4 1px solid;PADDING-RIGHT:8px;BORDER-TOP:#b7b4b4 1px solid;PADDING-LEFT:8px;FONT-WEIGHT:normal;FONT-SIZE:10px;PADDING-BOTTOM:8px;BORDER-LEFT:#b7b4b4 1px solid;COLOR:#3f3f3f;PADDING-TOP:8px;BORDER-BOTTOM:#b7b4b4 1px solid;FONT-FAMILY:verdana;BACKGROUND-COLOR:#e4e4e4;TEXT-ALIGN:left">To 
									upload an asset, click on the "BROWSE" button and choosing a file from the 
									browse dialog box. You may upload more than one asset by repeating the act as 
									many times as needed.
									<br>
									<br>
									If Java is enabled, then you may also upload assets by simply dragging the 
									files from your desktop or folder onto the gray box on the left.
									<br>
									<br>
									To begin the upload process, simply click the "SEND" or "Upload Assets" button 
									at the bottom of the list. A progress indicator will show once you begin the 
									upload progress.
									<br>
									<br>
									If you would like to upload a folder and create the assets in the sub folders 
									of the source, simply click the "Create subfolders" checkbox (Java upload 
									only).
									<br>
									<br>
									Note: Maximum file(s) size is 2Gb. The time it takes to upload the asset(s) 
									depends on your network connectivity. Remote access may greatly reduce upload 
									performance.<br>
									<br>
									Contact your system administrator for any additional help.
								</div>
							</td>
						</tr>
					</table>
					</FORM>
				</div>
				<%end if%>
			</div>
			<%if request.querystring("closewindow")<>"true" then%>
			<div style="PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 10px; TEXT-ALIGN: left; align: right">
				<div style="TEXT-ALIGN: right"><br>
					<IMG height="1" src="images/spacer.gif" width="5"><input onclick="window.close();" type="button" value="Close">
				</div>
			</div>
			<%end if%>
		</div>

		
		
		
		<script language="javascript">
		function setcreatesubfolders()
		{
		//alert(document.getElementById('createsubfolders').checked);
			if (document.getElementById('createsubfolders').checked)
			{
				
				document.getElementById('createsubfoldershidden').value=true;
			}else{
				document.getElementById('createsubfoldershidden').value=false;
			}
			//alert(document.Form1.createsubfoldershidden.value);
		}
	
		<%if request.querystring("closewindow") = "true" then%>
		if (getParameter("error") != "true") {
				alert("The upload was successfull.");
			    navigateToParentWindow();
				window.close();
		} else {
		   
			if ("null" === getParameter("msg")) {
				alert("The upload failed. Please try again, or contact your system administrator if the problem persist");
			} else {
				alert(getParameter("msg"));
			}
			if (getParameter("closewindow") == "true") {
				navigateParent();
			}
		}
		
		<%end if%>
		
		
		
			function setCreateSubFoldersCheckbox()
			{
				var loc=location.href;
				loc = loc.replaceAll( "&sf=1", "" )
				loc = loc.replaceAll( "&sf=0", "" )
				if (document.getElementById('createsubfolders').checked) 
					{
						window.location = loc+'&sf=1';
					} else {
						window.location = loc+'&sf=0';
					}
			}


			String.prototype.replaceAll = function( 
			strTarget, // The substring you want to replace
			strSubString // The string you want to replace in.
			){
			var strText = this;
			var intIndexOfMatch = strText.indexOf( strTarget );
			// Keep looping while an instance of the target string
			// still exists in the string.
			while (intIndexOfMatch != -1){
			// Relace out the current instance.
			strText = strText.replace( strTarget, strSubString )
			// Get the index of any next matching substring.
			intIndexOfMatch = strText.indexOf( strTarget );
			}
			// Return the updated string with ALL the target strings
			// replaced out with the new substring.
			return( strText );
			}
			
			function getParameter2 ( queryString, parameterName ) {
			// Add "=" to the parameter name (i.e. parameterName=value)
			var parameterName = parameterName + "=";
			if ( queryString.length > 0 ) {
			// Find the beginning of the string
			begin = queryString.indexOf ( parameterName );
			// If the parameter name is not found, skip it, otherwise return the value
			if ( begin != -1 ) {
			// Add the length (integer) to the beginning
			begin += parameterName.length;
			// Multiple parameters are separated by the "&" sign
			end = queryString.indexOf ( "&" , begin );
			if ( end == -1 ) {
			end = queryString.length
			}
			// Return the string
			return unescape ( queryString.substring ( begin, end ) );
			}
			// Return "null" if no parameter has been found
			return "null";
			}
			}
<%if Session("UseJavaUpload") = "1" then%>
if (getParameter2(location.href,"sf")=="0") {
document.getElementById('createsubfolders').checked=false;
}else{
document.getElementById('createsubfolders').checked=true;
}



<%end if%>

//alert('<%response.write(request.querystring("closewindow"))%>');

<%if request.querystring("closewindow")<>"true" then%>

function r_getFormData()
{
try

{

    if(document.forms[0] == undefined)
    {
        return;
    }
    for(i = 0 ; i < document.forms[0].elements.length ; i++)
    {
		try
			{
				//alert(document.forms[0].elements[i].name+'='+document.forms[0].elements[i].value);
				document.rup.jsAddTextField(document.forms[0].elements[i].name,document.forms[0].elements[i].value);
			}
		catch (err)
			{
				//alert(err.description);web
				
			}
    }
 }
 catch(err)
 {
 
 }
}

<%end if%>
		</script>
		
	</body>
</HTML>
