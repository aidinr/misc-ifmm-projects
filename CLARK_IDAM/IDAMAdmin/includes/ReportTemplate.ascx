﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ReportTemplate.ascx.vb" Inherits="IdamAdmin.ReportTemplate" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="CArtChart" Namespace="ComponentArt.Web.Visualization.Charting" Assembly="ComponentArt.Web.Visualization.Charting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>iDAM&trade; Reporting Tool</title>
    <script type="text/javascript">

        function generateReport(reportID) {

            CallbackReport.callback(reportID);
            document.getElementById("report").style.display = "block";

        };
        
    </script>

<script type="text/javascript">
    function get_png_image(img) { return img.replace("gif", "png"); };

    function get_group_text(idx) {
       return ReportGrid.get_table().get_columns()[idx].get_headingText();
   };
</script>


   <link href="css/gridStyle.css" type="text/css" rel="stylesheet" />
   <link href="css/menuStyle.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
            
    </style>
</head>
<body>
    
    <div style="padding-left:10px;">
    <div id="report" style="width:100%;overflow:auto;display:none;">
                                   <ComponentArt:CallBack ID="CallbackReport" runat="server">
                                        <Content>
                                                <asp:PlaceHolder ID="PlaceHolderReport" runat="server">
                                                 <div style="text-align:left;"><asp:HyperLink runat="server" ID="linkCSV"></asp:HyperLink></div><br />
                                                    <div style="font-size:12pt;"><b><asp:Literal runat="server" ID="ltrltitle"></asp:Literal></b></div>
                                                <CArtChart:Chart id="Chart1" RenderingPrecision="0.1" Width="200" Height="300" runat="server" SaveImageOnDisk="True" ></CArtChart:Chart>
                                                <br />
                                                <ComponentArt:Grid ID="ReportGrid"
                                                                ShowHeader="true"
                                                                CssClass="Gridx"
                                                                FooterCssClass="GridFooterx"
                                                                 HeaderCssClass="GridHeaderx"
                                                                RunningMode="Client"
                                                                PagerStyle="Numbered"
                                                                PagerTextCssClass="PagerText"
                                                                PageSize="18"
                                                                GroupingPageSize="18"
                                                                ImagesBaseUrl="images/"
                                                                Width="99%"
                                                                Height="100"
                                                                runat="server"
                                                                AllowHorizontalScrolling="false"
                                                                AllowVerticalScrolling="false"
                                                                ShowSearchBox="true"
                                                                GroupingMode="ConstantRecords"
                                                                GroupByCssClass=""
                                                                GroupByTextCssClass="txt"
                                                                GroupBySectionCssClass="grp"
                                                                GroupBySectionSeparatorCssClass=""
                                                                GroupingNotificationTextCssClass="txt"
                                                                GroupingNotificationText="Drag a column to this area to group by it."
                                                                TreeLineImagesFolderUrl="images/lines/"
                                                                TreeLineImageWidth="11"
                                                                TreeLineImageHeight="11"
                                                                IndentCellWidth="16"
                                                                PreExpandOnGroup="true"
                                                                IndentCellCssClass="ind"
                                                                 ManualPaging="true"
                                                                  
                                                                >
                                                              <Levels >
                                                                  <ComponentArt:GridLevel
                                                                    ShowTableHeading="false"
                                                                    ShowSelectorCells="false"
                                                                    SelectorCellCssClass="SelectorCell"
                                                                    SelectorCellWidth="18"
                                                                    SelectorImageUrl="selector.gif"
                                                                    SelectorImageWidth="17"
                                                                    SelectorImageHeight="15"
                                                                    HeadingSelectorCellCssClass="SelectorCell"
                                                                    HeadingCellCssClass="HeadingCell"
                                                                    HeadingRowCssClass="HeadingRow"
                                                                    HeadingTextCssClass="HeadingCellText"
                                                                    DataCellCssClass="DataCell"
                                                                    RowCssClass="Row"
                                                                    SelectedRowCssClass="SelectedRow"
                                                                    SortAscendingImageUrl="asc.gif"
                                                                    SortDescendingImageUrl="desc.gif"
                                                                    SortImageWidth="10"
                                                                    SortImageHeight="10"
                                                                    GroupHeadingCssClass="grp-hd"
                                                                    GroupHeadingClientTemplateId="GroupHeadingTemplate"
                                                                    ColumnReorderIndicatorImageUrl="reorder.gif"

                                                                    >
                                                                  </ComponentArt:GridLevel>
                                                                </Levels>
                                                                <ClientTemplates>
                                                                <ComponentArt:ClientTemplate Id="GroupHeadingTemplate">
                                                                    <span>## get_group_text(DataItem.get_column()) ##: ## DataItem.get_columnValue() ##</span>
                                                                </ComponentArt:ClientTemplate>
                                                                  <ComponentArt:ClientTemplate Id="GoToAssetIconTemplate">
                                                                    <a href="IDAM.aspx?page=Asset&type=asset&c=&Id=## DataItem.GetMember('Asset_ID').Value ##"><img src="images/goto.gif" width="16" height="17" border="0" alt="goto" /></a>
                                                                  </ComponentArt:ClientTemplate>
                                                                  <ComponentArt:ClientTemplate Id="GoToProjectIconTemplate">
                                                                    <a href="IDAM.aspx?page=Project&type=project&c=&Id=## DataItem.GetMember('ProjectID').Value ##>"><img src="images/goto.gif" width="16" height="17" border="0" alt="goto" /></a>
                                                                  </ComponentArt:ClientTemplate>
                                                                  <ComponentArt:ClientTemplate Id="GoToCategoryIconTemplate">
                                                                    <a href="IDAM.aspx?page=Browse&id=## DataItem.GetMember('CATEGORY_ID').Value ##>&type=category"><img src="images/goto.gif" width="16" height="17" border="0" alt="goto" /></a>
                                                                  </ComponentArt:ClientTemplate>

                                                                </ClientTemplates> 
                                                             </ComponentArt:Grid>
                                                             
                                                             
                                                               

                                                </asp:PlaceHolder>
                                        </Content>
                                   </ComponentArt:CallBack>
                                                                <ComponentArt:Menu
                                                                Id="ColumnContextMenu"
                                                                RunAt="server"
                                                                ContextMenu="Custom"
                                                                SiteMapXmlFile="/includes/columnContextMenu.xml"
                                                                ExpandTransition="fade"
                                                                Orientation="Vertical"
                                                                ExpandDuration="0"
                                                                CollapseDuration="0"
                                                                DefaultItemLookId="ItemLook"
                                                                DefaultGroupExpandOffsetY="-5"
                                                                CssClass="mnu"
                                                                ShadowEnabled="true"
                                                                 ImagesBaseUrl="images/menu/"
                                                                  
                                                                >
                                                                <ItemLooks>
                                                                    <ComponentArt:ItemLook LookId="ItemLook" CssClass="itm" HoverCssClass="itm-h" />
                                                                    <ComponentArt:ItemLook LookId="BreakItemLook" CssClass="br" />
                                                                </ItemLooks>

                                                                <ClientTemplates>
                                                                    <ComponentArt:ClientTemplate ID="ItemTemplate">
                                                                        <div>
                                                                            <span class="ico ##DataItem.get_value();##"></span>
                                                                            <span class="txt">## DataItem.get_text(); ##</span>
                                                                        </div>
                                                                    </ComponentArt:ClientTemplate>
                                                                </ClientTemplates>
                                                            </ComponentArt:Menu>
                                                          
                                                             
                                                            
                                                            
                            </div>
                       
    </div>

    <script type="text/javascript">
        generateReport('<%response.write(request.querystring("rid")) %>');
    </script>
</body>
</html>