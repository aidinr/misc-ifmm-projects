<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.UDFEdit" ValidateRequest="False" CodeBehind="UDFEdit.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>User Defined Fields</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
<link href="treeStyle.css" type="text/css" rel="stylesheet">
<link href="navStyle.css" type="text/css" rel="stylesheet" >
<link href="tabStripStyle.css" type="text/css" rel="stylesheet" /> 
<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
<link href="calendarStyle.css" type="text/css" rel="stylesheet" />
<style>
BODY { MARGIN: 0px }
</style>
<body onload="this.focus">
<form id="Form" method="post" runat="server">
<script type="text/javascript">
  
  function onInsert(item)
  {
   
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
  function onUpdate(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridUDF.Page(1); 
  }

  function onDelete(item)
  {

    
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    
  }
  
  function editGrid(rowId)
  {
    GridUDF.Edit(GridUDF.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    GridUDF.EditComplete();     
  }

  function insertRow()
  {
    GridUDF.EditComplete(); 
  }

  function deleteRow(rowId)
  {

    GridUDF.Delete(GridUDF.GetRowFromClientId(rowId)); 
  }
function resizeGrid(DomElementId, NewPaneHeight, NewPaneWidth)
{
GridUDF.Render();
} 
      
			</script>
			

			<div  style="margin:20px 20px 20px 20px;padding:20px;border:1px solid;background-color:white;">		
			
			<table><tr><td><img src="images/top_contacts.gif"></td><td><b>Modify <%response.write(spUDFTYPE)%> User Defined Fields</b></td></tr></table>
			<br>
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Modify UDF Fields by clicking the edit or delete links.  To add a new User Defined Field (UDF), click the "Add UDF" button.  Note:  You cannot permanently delete a UDF from the system.</div><br>
  <table  cellpadding=0 cellspacing =0><tr><td width="20" style="font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;" align=left nowrap><b>Find:&nbsp;</b><img src="images/spacer.gif" width=5 height=1></td><td align=left width=250 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:150px;height:18x;padding:1px;"  onkeyup="javascript:<%response.write( GridUDF.ClientId)%>.Filter('item_name LIKE \'%' + this.value + '%\' or item_group LIKE  \'%' + this.value + '%\'');"></td></tr></table>
  <br>

<div style="width:100%;">
<componentart:Grid id="GridUDF" ClientSideOnResize="resizeGrid" RunningMode="Callback" CssClass="Grid" GroupByTextCssClass="GroupByText"
                    GroupingNotificationTextCssClass="GridHeaderText" ShowFooter="true" PageSize="20"
                    ImagesBaseUrl="images/" EditOnClickSelectedItem="true" AllowEditing="true" Sort="item_name"
                    ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2"
                    ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16"
                    ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip"
                    ScrollBarWidth="16" ScrollPopupClientTemplateId="ScrollPopupTemplate" Width="100%"
                     Height="207" AutoCallBackOnDelete="true" AutoCallBackOnInsert="true" AllowHtmlContent="true" 
                    AutoCallBackOnUpdate="true" runat="server" PagerStyle="Numbered" PagerTextCssClass="PagerText"
                    ClientSideOnInsert="onInsert" ClientSideOnUpdate="onUpdate" ClientSideOnDelete="onDelete">
                    <levels>
					<ComponentArt:GridLevel DataKeyField="item_id" ShowTableHeading="false" ShowSelectorCells="false" HeadingCellCssClass="HeadingCell"
						HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive" HeadingTextCssClass="HeadingCellText"
						DataCellCssClass="DataCell" RowCssClass="Row" SelectedRowCssClass="SelectedRow" SortAscendingImageUrl="asc.gif"
						SortDescendingImageUrl="desc.gif" ColumnReorderIndicatorImageUrl="reorder.gif" SortedDataCellCssClass="SortedDataCell"
						SortImageWidth="14" SortImageHeight="14" EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField"
						EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate">
						<Columns>
						<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" DataCellClientTemplateId="Column1Template"  HeadingText="!" DataField="imagesource" FixedWidth="True" Width="20"/>
							<ComponentArt:GridColumn DataField="item_name" HeadingText="Name"  Width="60" />
							<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Tab" AllowEditing="true" Width="60" SortedDataCellCssClass="SortedDataCell" DataField="item_tab"  ></componentart:GridColumn>
							<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Group" AllowEditing="true" Width="60" SortedDataCellCssClass="SortedDataCell" DataField="item_group"  ></componentart:GridColumn>
							<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Field_Type" AllowEditing="true" Width="200"   SortedDataCellCssClass="SortedDataCell" DataField="item_type" ForeignTable="Field_Type" ForeignDataKeyField="field_id" ForeignDisplayField="field_name" ></componentart:GridColumn>
							<ComponentArt:GridColumn DataField="item_tag" HeadingText="Tag" Width="60"/>
							<ComponentArt:GridColumn DataField="item_description" HeadingText="Description" Width="60"/>
							<ComponentArt:GridColumn DataField="item_default_value" HeadingText="Default_Value" Width="150" />
							<ComponentArt:GridColumn DataField="viewable" HeadingText="Visible" Width="40" />
							<ComponentArt:GridColumn DataField="editable" HeadingText="Edit" Width="40" />
							<ComponentArt:GridColumn DataField="active" HeadingText="Active" Width="40"/>
							<ComponentArt:GridColumn DataField="filter" HeadingText="Filter" Width="40"/>
							<ComponentArt:GridColumn DataField="system" HeadingText="System"  />
							
							<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" Width="80" HeadingText="Edit Command" AllowSorting="False" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand"  Align="Left" />
							<ComponentArt:GridColumn DataField="item_id" Visible="false" />
						</Columns>
					</ComponentArt:GridLevel>
				</levels>
                    <clienttemplates>
					<ComponentArt:ClientTemplate Id="EditTemplate">
            <a href="javascript:editGrid('## DataItem.ClientId ##');">Edit</a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> | <a href="javascript:GridUDF.EditCancel();">Cancel</a>
          </ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> | <a href="javascript:GridUDF.EditCancel();">Cancel</a>
          </ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="LoadingFeedbackTemplate">
						<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td style="font-size:10px;">Loading...&nbsp;</td>
								<td><img src="images/spinner.gif" width="16" height="16" border="0"></td>
							</tr>
						</table>
					</ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="Column1Template">
						<img src="images/## DataItem.GetMember("imagesource").Value ##" width="16" height="16" border="0">
					</ComponentArt:ClientTemplate>
					
					
				</clienttemplates>
                </componentart:Grid>
</div>

            <br>
            <input type="button" onclick="GridUDF.Page(GridUDF.PageCount-1);GridUDF.Table.AddRow();"
                value="Add <%response.write(spUDFTYPE)%> UDF" />
            <div style="text-align: right;">
                <br>
                
            </div>
        </div>

    </form>
</body>
</html>
