<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.RepositoryEdit" ValidateRequest="False" CodeBehind="RepositoryEdit.aspx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Repository</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
		<link href="treeStyle.css" type="text/css" rel="stylesheet">
		<link href="navStyle.css" type="text/css" rel="stylesheet">
		<link href="tabStripStyle.css" type="text/css" rel="stylesheet">
		<link href="navBarStyle.css" type="text/css" rel="stylesheet">
		<style>BODY { MARGIN: 0px }
		</style>
	</HEAD>
	<body onload="this.focus">
		<form id="Form" method="post" runat="server">
			<script type="text/javascript">
  
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridKeywords.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    GridKeywords.Edit(GridKeywords.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    GridKeywords.EditComplete();     
  }

  function insertRow()
  {
    GridKeywords.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    GridKeywords.Delete(GridKeywords.GetRowFromClientId(rowId)); 
  }

<%if request.querystring("newid") <> "" then%>
window.opener.NavigateToRepository();
window.close();
<%end if%>

  <%if instr(request.querystring("id"),"REPO") = 0 then%>
	alert ('Error: ' & request.querystring("id"));
	window.close();
  <%end if%>
			</script>
			<%'response.write (request.querystring('newid'))%>
			<input type="hidden" name="catid" id="catid" value="<%'response.write (replace(request.querystring('p'),'CAT',''))%>">
			<div style="PADDING-RIGHT:20px;PADDING-LEFT:20px;PADDING-BOTTOM:20px;PADDING-TOP:20px;POSITION:relative">
				<div>
					<div style="BORDER-RIGHT:1px solid; PADDING-RIGHT:10px; BORDER-TOP:1px solid; PADDING-LEFT:10px; PADDING-BOTTOM:10px; BORDER-LEFT:1px solid; PADDING-TOP:10px; BORDER-BOTTOM:1px solid; POSITION:relative; BACKGROUND-COLOR:white">
						<table>
							<tr>
								<td><img src="images/repository16b.jpg"></td>
								<td>Create/Edit Repository</td>
							</tr>
						</table>
						<div style="BORDER-RIGHT:#b7b4b4 1px solid; PADDING-RIGHT:5px; BORDER-TOP:#b7b4b4 1px solid; PADDING-LEFT:5px; FONT-WEIGHT:normal; FONT-SIZE:10px; PADDING-BOTTOM:5px; BORDER-LEFT:#b7b4b4 1px solid;  COLOR:#3f3f3f; PADDING-TOP:5px; BORDER-BOTTOM:#b7b4b4 1px solid; FONT-FAMILY:verdana; BACKGROUND-COLOR:#e4e4e4">Create 
							a repository by entering a repository name and optional description.</div>
						<br>
						<div style="WIDTH: 100%">
							<COMPONENTART:TABSTRIP id="TabStripCategory" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataRepository.xml"
								CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
								DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
								<ItemLooks>
									<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
									<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
								</ItemLooks>
							</COMPONENTART:TABSTRIP>
							<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageCategoryPage" runat="server">
								<ComponentArt:PageView CssClass="PageContent2" runat="server" ID="Pageview1">
									<DIV style="BORDER-RIGHT: #b7b4b4 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #b7b4b4 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: normal; FONT-SIZE: 10px; PADDING-BOTTOM: 5px; BORDER-LEFT: #b7b4b4 1px solid; COLOR: #3f3f3f; PADDING-TOP: 5px; BORDER-BOTTOM: #b7b4b4 1px solid; FONT-FAMILY: verdana; BACKGROUND-COLOR: #e4e4e4">Enter 
										a name, description and security level for this repository.</DIV>
									<BR>
									<TABLE id="Table1" cellSpacing="0" cellPadding="3" width="100%" border="0">
										<TR>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Name 
				<asp:RequiredFieldValidator id="RequiredFieldValidator1"
					runat="server" ErrorMessage="[!]" ControlToValidate="Name"></asp:RequiredFieldValidator></FONT>
											</TD>
											<TD vAlign="top" align="left" width="100%">
												<asp:TextBox id="Name" runat="server" CssClass="InputFieldMain" Height=25px ></asp:TextBox></TD>
										</TR>
										<TR>
											<TD class="PageContent" vAlign="top" noWrap align="left" width="120">
												<FONT face="Verdana" size="1">Description</FONT></TD>
											<TD class="PageContent" vAlign="top" align="left" width="180%">
												<asp:TextBox id="Description" runat="server" CssClass="InputFieldMain" style="height:100px"  TextMode="MultiLine"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD class="PageContent" vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Protocol</FONT></TD>
											<TD class="PageContent" vAlign="top" align="left" width="100%">
												<asp:DropDownList id="Protocol" Runat="server" CssClass="InputFieldMain" Height=25px >
													<asp:ListItem Value="0">File (Network)</asp:ListItem>
													<asp:ListItem Value="1">File (Local)</asp:ListItem>
													<asp:ListItem Value="2">FTP</asp:ListItem>
													<asp:ListItem Value="3">SCP</asp:ListItem>
												</asp:DropDownList></TD>
										</TR>
									</TABLE>
									<hr>
									<TABLE id="Table1" cellSpacing="0" cellPadding="3" width="100%" border="0">
										<TR>
											<TD vAlign="top" noWrap align="left" width="120">
												<font face="Verdana" size="1">Allow use as: </font>
											</TD>
											<TD vAlign="top" align="left" width="100%">
												<font size="1" face="Verdana">
												<asp:CheckBox id="AllowInput" CssClass="InputFieldMainCheckbox" runat="server" ></asp:CheckBox>Input 
													Repository<br>
												<asp:CheckBox id="AllowOutput" CssClass="InputFieldMainCheckbox"  runat="server" ></asp:CheckBox>Output Repository<br>
												<asp:CheckBox id="LowBandwidth" CssClass="InputFieldMainCheckbox"  runat="server" ></asp:CheckBox>Low Bandwidth</font></TD>
										</TR>
									</TABLE>
								
								</ComponentArt:PageView>
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Category_Members">
									<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Modify the properties of this repository.</div>
									<br>
									<TABLE id="Table1" cellSpacing="0" cellPadding="3" width="100%" border="0">
										<TR>
											<TD vAlign="top" noWrap align="left" width="120">
												<font face="Verdana" size="1">Server Name <asp:RequiredFieldValidator id="Requiredfieldvalidator2"
					runat="server" ErrorMessage="[!]" ControlToValidate="ServerName"></asp:RequiredFieldValidator></font>
											</TD>
											<TD vAlign="top" align="left" width="100%">
												<asp:TextBox id="ServerName" runat="server"  CssClass="InputFieldMain" Height=25px></asp:TextBox></TD>
										</TR>
										<TR>
											<TD class="PageContent" vAlign="top" noWrap align="left" width="120">
												<font face="Verdana" size="1">Location <asp:RequiredFieldValidator id="Requiredfieldvalidator3"
					runat="server" ErrorMessage="[!]" ControlToValidate="Location"></asp:RequiredFieldValidator></font></TD>
											<TD class="PageContent" vAlign="top" align="left" width="180%">
												<asp:TextBox id="Location" runat="server"  CssClass="InputFieldMain" Height=25px></asp:TextBox></TD>
										</TR>
										<TR>
											<TD class="PageContent" vAlign="top" noWrap align="left" width="120">
												<font face="Verdana" size="1">Sub Location</font></TD>
											<TD class="PageContent" vAlign="top" align="left" width="100%">
												<asp:TextBox id="SubLocation" runat="server"  CssClass="InputFieldMain" Height=25px></asp:TextBox></TD>
										</TR>
									</TABLE>
									<table border="0" width="100%" id="table2">
										<tr>
											<td width="88"><b><font face="Verdana" size="1">Authentication</font></b></td>
											<td><hr>
											</td>
										</tr>
									</table>
									<TABLE id="Table1" cellSpacing="0" cellPadding="3" width="100%" border="0">
										<TR>
											<TD vAlign="top" noWrap align="left" width="120">
												<font face="Verdana" size="1">&nbsp;</font></TD>
											<TD vAlign="top" align="left" width="100%">
												<font size="1" face="Verdana">
												    <asp:RadioButton ID=chkauth0  Runat=server Text="Default (Same as Instance)" GroupName=chkauth></asp:RadioButton> 
													<br>
													<asp:RadioButton ID=chkauth1  Runat=server   Text="Windows Authentication" GroupName=chkauth></asp:RadioButton> 
													<br>
													<asp:RadioButton ID=chkauth2  Runat=server   Text="Use the following" GroupName=chkauth></asp:RadioButton>  
													</font></TD>
										</TR>
										<TR>
											<TD vAlign="top" noWrap align="left" width="120">
												<font face="Verdana" size="1">Username</font></TD>
											<TD vAlign="top" align="left" width="100%">
												<asp:TextBox id="Username" runat="server"  CssClass="InputFieldMain" Height=25px></asp:TextBox></TD>
										</TR>
										<TR>
											<TD vAlign="top" noWrap align="left" width="120">
												<font face="Verdana" size="1">Password</font></TD>
											<TD vAlign="top" align="left" width="100%">
												<asp:TextBox TextMode=Password   id="Passwordfield" runat="server"  CssClass="InputFieldMain" Height=25px></asp:TextBox></TD>
										</TR>
									</TABLE>
									<hr>
									<TABLE id="table3" cellSpacing="0" cellPadding="3" width="100%" border="0">
										<TR>
											<TD vAlign="top" noWrap align="left" width="120">
												<font face="Verdana" size="1">Port Number</font></TD>
											<TD vAlign="top" align="left" width="100%">
												<asp:TextBox id="PortNumber" runat="server"  CssClass="InputFieldMain" Height=25px></asp:TextBox></TD>
										</TR>
									</TABLE>
								</ComponentArt:PageView>
							</ComponentArt:MultiPage>
							<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
								<div style="TEXT-ALIGN:right"><br>
									<asp:Button id="btnProjectSave" runat="server" Text="Save"></asp:Button><img src="images/spacer.gif" width="5" height="1"><input type="button" onclick="window.opener.NavigateToRepository();window.close();" value="Close">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</form>
	</body>
</HTML>
