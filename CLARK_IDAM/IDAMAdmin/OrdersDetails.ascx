<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI"  %>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.OrdersDetails" CodeBehind="OrdersDetails.ascx.vb" %>
<div class="previewpaneMyProjects ui-accordion-header ui-helper-reset ui-state-active " id="toolbarheading" style="z-index:99;border-top:0px;border-bottom:0px;">
	<!--Project cookie and top menu-->
		<table cellspacing="0" cellpadding="4" id="table2" width="100%">
			<tr>
			
			<td valign="top" width="1" nowrap >
					<img style="padding-top:5px;" src="images/ui/carousel_ico_bg.gif" height=42 width=42>
			</td>
			<td valign="top">
						<font face="Verdana" size="1"><b>Order Summary</b><br>
				
				<asp:Literal id="LiteralInformation" Text="" visible=true runat="server" />
				</font>
				
			</td>
			<td id="Test" valign="top" align="right">

			
							
			<!--<div style="padding-top:2px;">
			[ <a href="#">help</a> ]
				</div>-->
			</td></tr>
			<tr><td valign="top" colspan="2">
			</td>
			</tr>
		</table>
</div> <!--end preview pane-->

<script language="javascript">
var xoffsetpopup
xoffsetpopup = -100
var yoffsetpopup 
yoffsetpopup = -400
</script>
<!--<script language="javascript"  src="js/filesearchhover.js"/>-->
<style>
.projectcentertest { Z-INDEX: 100; WIDTH: 100%; HEIGHT: 100% }
</style>
<script type="text/javascript">
//<![CDATA[    ]
//]]>
</script>
<script language=javascript>
function showlogs(id)
{
ShowDialogWindowX(1340,1340,972,'IDAMExpress.aspx?Orderid=<%=request.querystring("ID")%>&historyonly=1' + '&x=' + urlhelper(),'IDAMExpress Assets','contentCssEXPRESS','iFrameCss');
}
  
  
function GridAssets_onCallbackComplete(sender, eventArgs)
{
<%Response.Write(CallbackTotals.ClientID)%>.Callback();
}
  
  
        // Forces the grid to adjust to the new size of its container          
 function resizeGridAssets(DomElementId, NewPaneHeight, NewPaneWidth)
      {
        <% Response.Write(GridAssets.ClientID) %>.Render();
        //alert('here');
      } 
  function editGrid(rowId)
  {

    <% Response.Write(GridAssets.ClientID) %>.Edit(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  

  

  function onInsert(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
    function ShowContextMenu(item, column, evt) 
  {
    <% Response.Write(GridAssets.ClientID) %>.Select(item); 
   
    return false; 
  }
  
  function onUpdate(item)
  {
      //if (confirm("Update record?"))
        return true; 
      //else
      //return false; 

  }

  function onCallbackError()
  {
    alert("error");
  }

  function onDelete(item)
  {

      if (confirm("Remove this item from this order?"))
        return true; 
      else
        return false; 

  }
  
  function editRow()
  {
    <% Response.Write(GridAssets.ClientID) %>.EditComplete();     
  }

  function insertRow()
  {
    <% Response.Write(GridAssets.ClientID) %>.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    <% Response.Write(GridAssets.ClientID) %>.Delete(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(rowId)); 
  }


  
  
  
    function ThumbnailCallBackPage(page)
  {
    <% Response.Write(ThumbnailCallBack.ClientID) %>.Callback(page);
  }
  

    function ThumbnailCallBackPageProjects(page)
  {
    <% Response.Write(CallBackProjects.ClientID) %>.Callback(page);
  }
  

  
  
  
  

//setup form switch for delete or multi asset zip download
function DownloadAssets(ids){
	var sAssets;
	var downloadtype;
	sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
	ids = ids.replace('0 ','');
	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Download selected assets?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (arraylist == ''){
					arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
					} else {
					if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
					arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					}
					}
				}
				
				arraylist= arraylist.substring(1, arraylist.length);
				downloadtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				downloadtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	downloadtype = 'single';
	}
	if (downloadtype == 'multi') {
		if (arraylist != ''){
		<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('multi,'+arraylist);		
		}
	}
	if (downloadtype == 'single') {
	/*assume single*/
	<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('single,'+ids);
	}

}

  
  

function PreviewOverlayOnSingleClickAssetsFromIcon(RowId,eventObject)
  {
	try
	{
		var itemvaluetmp;
		var itemvaluenametmp;
		var itemvaluefiletypetmp;
		var item 
		itemRow = <% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(RowId);
		itemvaluetmp = itemRow.GetMember('asset_id').Value;
		itemvaluenametmp = itemRow.GetMember('assetname').Value;
		
		itemvaluefiletypetmp = getURLPreviewIcon() + itemRow.GetMember('media_type').Value
		<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);		 
	}  //end try  
	catch(err)
	{
	txt="There was an error on this page.\n\n"
	txt+="Error description: " + err.description
	alert(txt);
	}
	
	//return true;
}
	 



function AssetPreview(Asset_ID,cid)
{
	//get state
	
	// Response.Write(SnapProjectAssetOverlay.ClientID) %>.Expand();

	//if ( Response.Write(SnapProjectAssetOverlay.ClientID) %>.IsMinimized)
	//{
	// Response.Write(SnapProjectAssetOverlay.ClientID) %>.UnMinimize();
	//}
	//response.write( SnapProjectAssetOverlay.ClientId)%>.Callback(cid);
}

//setup goto for grid nav
function GotoLocation(ctype,cid)
	{
	//if (ctype == '0') {
		window.location = 'IDAM.aspx?page=Project&Id=<%response.write (request.querystring("Id"))%>&type=project&c=' + cid;
	//} else{
	//	window.location = 'IDAM.aspx?page=Project&Id=<%response.write (request.querystring("Id"))%>&type=project';
	//}
	return true;
	}
	
	
	
  function onUpdate(item)
  {

      //if (confirm("Update record?"))
        return true; 
      //else
      //  return false; 

  }


  function onInsert(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }





  function onDelete(item)
  {

      if (confirm("Remove this item from this order?"))
        return true; 
      else
        return false; 

  }	
	
	
function editRow()
  {
    <% Response.Write(GridAssets.ClientID) %>.EditComplete();     
  }


function editGrid(RowId)
  {
    itemRow = <% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(RowId);
	itemvaluetmp = itemRow.GetMember('asset_id').Value;
    window.location = 'IDAM.aspx?page=Asset&ID=' + itemvaluetmp + '&type=asset'
  }


function CheckAllItems()
{

if(<% Response.Write(GridAssets.ClientID) %>.get_pageSize()<<% Response.Write(GridAssets.ClientID) %>.get_recordCount()) itemcnt=<% Response.Write(GridAssets.ClientID) %>.get_pageSize();
if(<% Response.Write(GridAssets.ClientID) %>.get_pageSize()><% Response.Write(GridAssets.ClientID) %>.get_recordCount()) itemcnt=<% Response.Write(GridAssets.ClientID) %>.get_recordCount();
var itemIndex = 0;
for (var x = 1; x <= itemcnt; x++)
{
<% Response.Write(GridAssets.ClientID) %>.Select(<% Response.Write(GridAssets.ClientID) %>.Table.GetRow(x-1),true);
}
<% Response.Write(GridAssets.ClientID) %>.Render();
}

    

    
function MM_findObj(n, d) { //v3.0
var p,i,x; if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function CheckAllItemsThumbs() { //Set All checkboxes, if un-checked, check them.
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
var dummyobjectvalue = checks[0].value;
var firstobjectcheckedstate = document.getElementById('aselect_' + dummyobjectvalue);
var bcheck = firstobjectcheckedstate.checked

if (bcheck) {
bcheck = true;
} else {
bcheck = false;
}

for (i=0; i<checks.length; i++) {
MM_findObj('aselect_' + checks[i].value).checked = bcheck ;
highlightAssetToggleII(document.getElementById('aselect_' + checks[i].value),checks[i].value); 
}

}
}




    function highlightAssetToggleII(checkbox,asset_id)
  {
  var highlightbox;
  highlightbox = document.getElementById('assethighlight_' + asset_id);
  if (checkbox.checked) {
  	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
	checkbox.checked = false;

  } else
  {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  checkbox.checked = true;
  }}

//setup form switch for delete or multi asset zip download
function DownloadAssetsThumbs(ids){
	var arraylist;
	var i;
	var sAssets;
	var downloadtype;
	arraylist = '';
	ids = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
	if (arraylist.split(',').length > 2) {
			 if (confirm("Download selected assets?"))
			 {
				arraylist= arraylist.substring(1, arraylist.length);
				downloadtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				downloadtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	downloadtype = 'single';
	}
	if (downloadtype == 'multi') {
		if (arraylist != ''){
		<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('multi,'+arraylist);		
		}
	}
	if (downloadtype == 'single') {
	/*assume single*/
	<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('single,'+ids);
	}

}


  function onDeleteAsset(item)
  {

      //if (confirm("Delete this asset?"))
        return true; 
      //else
      //  return false; 

  }


  function deleteRow(ids)
  {

  //check to see if multi select
	var sAssets;
	var addtype;
	<%If bThumbnailView Then%>
    var arraylist;
	var i;
	arraylist = '';
	idstmp = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
	if (arraylist.split(',').length > 2) {
			 if (confirm("Remove selected items from this order?"))
			 {
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				addtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	addtype = 'single';
	}
    <%else%>
          
       
       
    sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();

	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Remove selected items from this order?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (arraylist == ''){
						if (sAssets[i]!=null){
							arraylist = ',' + sAssets[i].GetMember("orderitemid").Value;
						}
					} else {
						if (sAssets[i]!=null){
							if (arraylist.indexOf(',' +  sAssets[i].GetMember("orderitemid").Value) == -1) {
								arraylist = arraylist + ',' +  sAssets[i].GetMember("orderitemid").Value;
							}
						}
					}
				}
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				addtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	addtype = 'single';
	}
    <%end if%>
	//alert(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(ids));
	if (addtype == 'multi') {
		if (arraylist != ''){
		
		
		//override filter call
		<%response.write( GridAssets.ClientId)%>.Filter("DELETE " + arraylist);
		<%response.write( GridAssets.ClientId)%>.Page(0);
		//return false;
		}
	}
	if (addtype == 'single') {
	/*assume single*/
      if (confirm("Remove this item from this order?")){
       <% Response.Write(GridAssets.ClientID) %>.Delete(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(ids));

        }
      else {
        }
	} 
  }



</script>

<script>
  function isdefined(variable)
  {
    return (typeof (window[variable]) == "undefined") ? false : true;
  }

  var timeoutDelay = 100;
  var nb = "&nbsp;"; // non-breaking space

  function buildPager(grid, pagerSpan, First, Last)
  {
    var pager = "";
    var mid = Math.floor(pagerSpan / 2);
    var startPage = grid.PageCount <= pagerSpan ? 0 : Math.max(0, grid.CurrentPageIndex - mid);
    // adjust range for last few pages
    if (grid.PageCount > pagerSpan)
    {
      startPage = grid.CurrentPageIndex < (grid.PageCount - mid) ? startPage : grid.PageCount - pagerSpan;
    }

    var endPage = grid.PageCount <= pagerSpan ? grid.PageCount : Math.min(startPage + pagerSpan, grid.PageCount);

    if (grid.PageCount > pagerSpan && grid.CurrentPageIndex > mid)
    {
      pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(0);return false;\">&laquo; First</a>" + nb + "..." + nb;
      pager += "<a href=\"#\" onclick=\"" + grid.Id + ".PreviousPage();return false;\">&lt;</a>" + nb;
    }

    for (var page = startPage; page < endPage; page++)
    {
      var showPage = page + 1;
      if (page == grid.CurrentPageIndex)
      {
        pager += showPage + nb;
      }
      else 
      {
        pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(" + page + ");return false;\">" + showPage + "</a>" + nb;
      }
    }

    if (grid.PageCount > pagerSpan && grid.CurrentPageIndex < grid.PageCount - mid)
    {
      pager += "<a href=\"#\" onclick=\"" + grid.Id + ".NextPage();return false;\">></a>" + nb + "..." + nb;
      pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(" + (grid.PageCount - 1) + ");return false;\">Last &raquo;</a>" + nb;
    }

    return pager;
  }

  function buildPageXofY(grid, Page, of, items)
  {
    // Note: You can trap an empty Grid here to change the default "Page 1 of 0 (0 items)" text
    var pageXofY = Page + nb + "<b>" + (grid.CurrentPageIndex + 1) + "</b>" + nb + of + nb + "<b>" + (grid.PageCount) + "</b>" + nb + "(" + grid.RecordCount + nb + items + ")";

    return pageXofY;
  }

  function showCustomFooter()
  {
    var gridId = "<%response.write( GridAssets.ClientId)%>";
    if (isdefined(gridId))
    {
      var grid = <%response.write( GridAssets.ClientId)%>;

      var Page = "Page";
      var of = "of";
      var items = "items";

      var pagerSpan = 5; // should be at least 2
      var First = "First";
      var Last = "Last";
      var cssClass = "GridFooterText";

      var footer = buildPager(grid, pagerSpan, First, Last);
      document.getElementById("tdPager").innerHTML = "<div style='white-space:nowrap;display:inline;' class=\"" + cssClass + "\">" + footer + "</div>";

      footer = buildPageXofY(grid, Page, of, items);
      document.getElementById("tdIndex").innerHTML = "<div class=\"" + cssClass + "\">" + footer + "</div>";
    }
    else 
    {
      setTimeout("showCustomFooter();", timeoutDelay);
    }
  }

  function onPage(newPage)
  {
    // delay call so that Grid's client properties have their new values
    setTimeout("showCustomFooter();",timeoutDelay);

    return true;
  }

  function onLoad()
  {
    showCustomFooter();
  }
  
  
  
  
  
  
  
</script> 

<div id="subtabset" style="position:relative;top:-4px;z-index:2; font-size:10px;">
<input type="radio" id="radio1" name="subtab"  checked="checked"  /><label for="radio1">General</label><img src="images/spacer.gif" width="2" alt="" /></div>
<div style="border-top:1px solid silver;position:relative;top:-25px;z-index:1;margin-left:0px;"></div>

<script>
    $(function () {
        $("#subtab").button();
        $("#subtabset").buttonset();
    });
	</script>

<div class="previewpaneProjects" id="toolbar">
<div style="position:relative;width:100%;height:0px;top:-22px;padding-left:0px;float:left;display:none;">
	<div id=link_Assets_div class=projecttabshighlight></div>
</div>
	<!--END Project cookie and top menu-->
	<!--projectcentermain-->
	<br>

    <b><asp:HyperLink CssClass="projecttabs" id="link_Assets" NavigateUrl="javascript:showlist('assetlist');"
									runat="server">Assets</asp:HyperLink></b>
	<div id="projectcentermain" style="PADDING-RIGHT: 10px;PADDING-LEFT: 10px;PADDING-TOP: 15px">


<div id="projectcentermain"  class="projectcentermainpd">
		<ComponentArt:Snap CurrentDockingIndex="1" ClientSideCookieName="ORDERDETAILPAGESNAPSNAPDetails" ClientSideCookieEnabled="True"  id="SNAPOrderInfoMain" runat="server" AllowDrag="False" FillWidth="True" FillHeight="True" Height="100%"
			MustBeDocked="true" DockingStyle="TransparentRectangle" CurrentDockingContainer="projectcentermain"
			DockingContainers="projectcentermain" 
			 MinimizeDuration="300" MinimizeSlide="Linear" >
			<Header>
				<div style=" width: 100%;">
					<table class="SnapHeaderProjectsxxxx" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
							<td align=left ><img src="images/exparrow.gif" onclick="<% Response.Write(SNAPOrderInfoMain.ClientID)%>.ToggleExpand();"><b>Custom Order Details</b>
								<%'response.write(spProjectName)%>
								</td>
							<td width="15" style="cursor: hand" align="right"></td>
							<td width="15" style="cursor: hand" align="right"></td>
							<td width="15" style="cursor: hand" align="right"></td>
						</tr>
					</table>
				</div>
			</Header>
			<CollapsedHeader>
				<div style=" width: 100%;">
					<table class="SnapHeaderProjectsxxxxx" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
							<td align=left ><img src="images/colarrow.gif" onclick="<% Response.Write(SNAPOrderInfoMain.ClientID) %>.ToggleExpand();"><b>Custom Order Details</b>
								<%'response.write(spProjectName)%>
								</td>
							<td width="15" style="cursor: hand" align="right"></td>
							<td width="10" style="cursor: hand" align="right"></td>
						</tr>
					</table>
				</div>
				<img src="images/spacer.gif" width="188" height="0"><br>
			</CollapsedHeader>
			<Content>
			
			
			<div class="SnapProjectsWrapperxxxx">
					<div class="SnapProjectsxxx">





				<TABLE id="Table2" cellSpacing="0" cellPadding="1" width="500" border="0">
					<TR>
						<TD vAlign="middle" noWrap align="left" height="25" colSpan="4"><FONT face="Verdana" size="1"></FONT></TD>
					</TR>
				</TABLE>
				
				
				<COMPONENTART:TABSTRIP id="UDFTabStrip" width="100%" runat="server" ImagesBaseUrl="images/" ScrollingEnabled="true"
								CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
								DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" ScrollLeftLookId="ScrollItem" ScrollRightLookId="ScrollItem">
								<ItemLooks>
									<ComponentArt:ItemLook LookId="ScrollItem" CssClass="ScrollItem" HoverCssClass="ScrollItemHover" LabelPaddingLeft="5" LabelPaddingRight="5" LabelPaddingTop="0" LabelPaddingBottom="0" />
									<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />	
									<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />																																																																		
								</ItemLooks>
								
								</COMPONENTART:TABSTRIP>
								<div class="subrightsideplaceholderdiv">	
									<div class="subrightsideplaceholderdivheading">
									</div>
										<ComponentArt:MultiPage id="Multipage1" CssClass="subMultiPageUserPage"   runat="server">
										<ComponentArt:PageView CssClass="PageContent2" runat="server" ID="Pageview3">
											<COMPONENTART:CALLBACK id="CallbackUDFTab" runat="server" CacheContent="false" >
											<CONTENT>	
											<asp:Literal ID="Literal_UDFMAIN" Runat=server></asp:Literal>	
											<asp:PlaceHolder id="PlaceHolder_UDFEditor" runat="server"></asp:PlaceHolder>									
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
											</LOADINGPANELCLIENTTEMPLATE>
											</COMPONENTART:CALLBACK>
										</ComponentArt:PageView>
										</ComponentArt:MultiPage>
								</div>
<input type="button" onclick="javascript:idamaction.value='editOrder';PageForm.submit();return true;" value="Save" id="Save">
					
					</div>
				</div> <!--SnapProjectsWrapper-->
			
			
			
			
			
			
			
			
			
			
			
			
</Content>
</ComponentArt:Snap>
				

<ComponentArt:Snap CurrentDockingIndex="0" ClientSideCookieName="CUSTOMORDERDETAILPAGESNAPSNAPDetails" ClientSideCookieEnabled="True"  id="SNAPCUSTOMOrderInfoMain" runat="server" AllowDrag="False" FillWidth="True" FillHeight="True" Height="100%"
			MustBeDocked="true" DockingStyle="TransparentRectangle" CurrentDockingContainer="projectcentermain"
			DockingContainers="projectcentermain" 
			 MinimizeDuration="300" MinimizeSlide="Linear" >
			<Header>
				<div style=" width: 100%;">
					<table class="SnapHeaderProjectsxxxx" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
							<td align=left ><img src="images/exparrow.gif" onclick="<% Response.Write(SNAPCUSTOMOrderInfoMain.ClientID)%>.ToggleExpand();"><b>Order Details</b>
								<%'response.write(spProjectName)%>
								</td>
							<td width="15" style="cursor: hand" align="right"></td>
							<td width="15" style="cursor: hand" align="right"></td>
							<td width="15" style="cursor: hand" align="right"></td>
						</tr>
					</table>
				</div>
			</Header>
			<CollapsedHeader>
				<div style=" width: 100%;">
					<table class="SnapHeaderProjectsxxxx" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
							<td align=left ><img src="images/colarrow.gif" onclick="<% Response.Write(SNAPCUSTOMOrderInfoMain.ClientID)%>.ToggleExpand();"><b>Order Details</b>
								<%'response.write(spProjectName)%>
								</td>
							<td width="15" style="cursor: hand" align="right"></td>
							<td width="10" style="cursor: hand" align="right"></td>
						</tr>
					</table>
				</div>
				<img src="images/spacer.gif" width="188" height="0"><br>
			</CollapsedHeader>
			<Content>
				
			
				<div class="SnapProjectsWrapperxxx">
					<div class="SnapProjectsxxxxx">
					
					
					
					
					
					
					
					
					
					
					
					<P>
						
				<TABLE id="Table1" cellSpacing="0" cellPadding="1" width="350" border="0">
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="25"><FONT face="Verdana" size="1"><STRONG>Invoice</STRONG></FONT></TD>
						<TD vAlign="middle" align="left" width="100%" colSpan="3" height="25"><asp:Literal ID=ltrlinvoice Runat=server></asp:Literal></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="19"><FONT face="Verdana" size="1">
								<FONT face="Verdana" size="1"><STRONG>UserID</STRONG></FONT></FONT></TD>
						<TD vAlign="middle" align="left" width="310" height="19">
							<asp:TextBox id="txtuserid" ReadOnly="true" runat="server" Height="15px" CssClass="InputFieldMain" ></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="17%" height="19"><FONT face="Verdana" size="1">
							</FONT>
						</TD>
						<TD vAlign="middle"  nowrap align="left" width="61%" height="19">
							[ <a onclick="javascript:popupusers();">select</a> ] [ <a onclick="javascript:Object_PopUp_UserNew();" >new</a> ] [ <a onclick="javascript:Object_PopUp_User('<%=spOrderUserID%>');" >edit</a> ]</TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="19"><FONT face="Verdana" size="1">
								Company</FONT></TD>
						<TD vAlign="middle" align="left" width="310" height="19">
							<asp:TextBox id="txtagency" runat="server" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="17%" height="19"><FONT face="Verdana" size="1">
							</FONT>
						</TD>
						<TD vAlign="middle"  nowrap align="left" width="61%" height="19">
							</TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77"><FONT size="1">Firstname&nbsp;</FONT>
						</TD>
						<TD vAlign="middle" align="left" width="310">
							<asp:TextBox id="txtfirstname" runat="server" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="17%">
						</TD>
						<TD vAlign="middle" align="left" width="61%"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16"><FONT size="1">Lastname&nbsp;</FONT>
						</TD>
						<TD vAlign="middle" align="left" width="310" height="16">
							<asp:TextBox id="txtlastname" runat="server" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="17%" height="16">
						</TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
				</TABLE>
					
					
					<HR width="100%" SIZE="1" color=WhiteSmoke>
					
					
					<table>
					<tr>
					<td>
					
				<TABLE id="Table2" cellSpacing="0" cellPadding="1" width="350" border="0">
					<TR>
						<TD vAlign="middle" noWrap align="left" height="25" colSpan="4"><FONT face="Verdana" size="1"><STRONG>Mailing 
									Address</STRONG></FONT></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="19"><FONT face="Verdana" size="1">Address</FONT></TD>
						<TD vAlign="middle" align="left" width="302" height="19">
							<asp:TextBox id="txtmaddress1" runat="server" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="19"><FONT face="Verdana" size="1"></FONT></TD>
						<TD vAlign="middle" align="left" width="61%" height="19"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77"><FONT size="1">Address</FONT></TD>
						<TD vAlign="middle" align="left" width="302">
							<asp:TextBox id="txtmaddress2" runat="server" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19"></TD>
						<TD vAlign="middle" align="left" width="61%"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16"><FONT size="1">City</FONT></TD>
						<TD vAlign="middle" align="left" width="302" height="16">
							<asp:TextBox id="txtmcity" runat="server" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16">State</TD>
						<TD vAlign="middle" noWrap align="left" width="303" height="16">
							<asp:TextBox id="txtmstate" runat="server" Width="176px" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
					
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16">Country</TD>
						<TD vAlign="middle" noWrap align="left" width="303" height="16">
							<asp:TextBox id="txtmcountry" runat="server" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16">Zip</TD>
						<TD vAlign="middle" noWrap align="left" width="303" height="16">
							<asp:TextBox id="txtmzip" runat="server" Width="88px" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
				</TABLE>
					
					
					
					</td>
					<td>
					<TABLE id="Table2" cellSpacing="0" cellPadding="1" width="350" border="0">
					<TR>
						<TD vAlign="middle" noWrap align="left" height="25" colSpan="4"><FONT face="Verdana" size="1"><STRONG>Billing 
									Address </STRONG>[ <a onclick="javascript:copyfrommailing();">copy from mailing</a> ]</FONT></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="19"><FONT face="Verdana" size="1">Address</FONT></TD>
						<TD vAlign="middle" align="left" width="302" height="19">
							<asp:TextBox id="txtbaddress1" runat="server" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="19"><FONT face="Verdana" size="1"></FONT></TD>
						<TD vAlign="middle" align="left" width="61%" height="19"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77"><FONT size="1">Address</FONT></TD>
						<TD vAlign="middle" align="left" width="302">
							<asp:TextBox id="txtbaddress2" runat="server" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19"></TD>
						<TD vAlign="middle" align="left" width="61%"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16"><FONT size="1">City</FONT></TD>
						<TD vAlign="middle" align="left" width="302" height="16">
							<asp:TextBox id="txtbcity" runat="server" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16">State</TD>
						<TD vAlign="middle" noWrap align="left" width="303" height="16">
							<asp:TextBox id="txtbstate" runat="server" Width="176px" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
					
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16">Country</TD>
						<TD vAlign="middle" noWrap align="left" width="303" height="16">
							<asp:TextBox id="txtbcountry" runat="server" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16">Zip</TD>
						<TD vAlign="middle" noWrap align="left" width="303" height="16">
							<asp:TextBox id="txtbzip" runat="server" Width="88px" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
				</TABLE>
					</td>
					</tr>
					</table>
					
					
			
				<HR width="100%" SIZE="1" color=WhiteSmoke>
				
				
				
				
				<table>
					<tr>
					<td>
					
					<TABLE id="Table2" cellSpacing="0" cellPadding="1" width="350" border="0">
					<TR>
						<TD vAlign="middle" noWrap align="left" height="25" colSpan="4"><FONT face="Verdana" size="1"><STRONG>
									Additional Information</STRONG></FONT></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="19"><FONT face="Verdana" size="1">Phone 
								1</FONT></TD>
						<TD vAlign="middle" align="left" width="302" height="19">
							<asp:TextBox id="txtphone1" runat="server" Height="15px" CssClass="InputFieldMain" Width="176px"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="19"><FONT face="Verdana" size="1"></FONT></TD>
						<TD vAlign="middle" align="left" width="61%" height="19"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77"><FONT size="1">Phone 2</FONT></TD>
						<TD vAlign="middle" align="left" width="302">
							<asp:TextBox id="txtphone2" runat="server" Width="176px" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19"></TD>
						<TD vAlign="middle" align="left" width="61%"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16"><FONT size="1">Fax</FONT></TD>
						<TD vAlign="middle" align="left" width="302" height="16">
							<asp:TextBox id="txtfax" runat="server" Width="176px" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16">Email</TD>
						<TD vAlign="middle" noWrap align="left" width="303" height="16">
							<asp:TextBox id="txtemail" runat="server" Width="176px" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
					<TR>
						<TD vAlign="top" noWrap align="left" width="77" height="16">Shipping Type</TD>
						<TD vAlign="middle" noWrap align="left" width="303" height="16">
							<asp:TextBox id="txtshippingtype" ReadOnly="true" runat="server" Width="176px" CssClass="InputFieldMain"
								TextMode="SingleLine"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
				</TABLE>
					
					</td>
					<td>
					<TABLE id="Table2" cellSpacing="0" cellPadding="1" width="350" border="0">
					<TR>
						<TD vAlign="middle" noWrap align="left" height="25" colSpan="4"><FONT face="Verdana" size="1"><STRONG>
									Payment Information</STRONG></FONT></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="19"><FONT face="Verdana" size="1">
								Method</FONT></TD>
						<TD vAlign="middle" align="left" width="302" height="19">
							<asp:TextBox id="txtmethod" runat="server" Height="15px" CssClass="InputFieldMain" Width="176px"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="19"><FONT face="Verdana" size="1"></FONT></TD>
						<TD vAlign="middle" align="left" width="61%" height="19"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77"><FONT size="1">CC Type</FONT></TD>
						<TD vAlign="middle" align="left" width="302">
							<asp:TextBox id="txtcctype" runat="server" Width="176px" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19"></TD>
						<TD vAlign="middle" align="left" width="61%"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16"><FONT size="1">CC Number</FONT></TD>
						<TD vAlign="middle" align="left" width="302" height="16">
							<asp:TextBox id="txtccnumber" runat="server" Width="176px" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16">CC Exp</TD>
						<TD vAlign="middle" noWrap align="left" width="303" height="16">
							<asp:TextBox id="txtccexp" runat="server" Width="64px" Height="15px" CssClass="InputFieldMain"></asp:TextBox>&nbsp; 
							mm/yyyy</TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16">CC Name</TD>
						<TD vAlign="middle" noWrap align="left" width="303" height="16">
							<asp:TextBox id="txtccname" runat="server" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" noWrap align="left" width="77" height="16">CC Security</TD>
						<TD vAlign="middle" noWrap align="left" width="303" height="16">
							<asp:TextBox id="txtccsecurity" runat="server" Width="32px" Height="15px" CssClass="InputFieldMain"></asp:TextBox></TD>
						<TD vAlign="middle" align="right" width="19" height="16"></TD>
						<TD vAlign="middle" align="left" width="61%" height="16"></TD>
					</TR>
				</TABLE>
					</td>
					</tr>
					</table>
					<input type="button" onclick="javascript:idamaction.value='editOrder';PageForm.submit();return true;" value="Save" id="Save"><br><br>
</div>
</div> <!--SnapProjectsWrapper-->
			</Content>
		</ComponentArt:Snap>
	</div>
	<!--END projectcentermain-->







		
<COMPONENTART:CALLBACK id="CallbackTotals" runat="server" CacheContent="false">
												<CONTENT>


<asp:Repeater id="RepeaterTotals" runat="server" Visible="True">
<ItemTemplate>
<tr><td  align=left nowrap>
<%#DataBinder.Eval(Container.DataItem, "name")%>
</td>
<td align=left  nowrap >
<%#DataBinder.Eval(Container.DataItem, "quantity")%>
</td>
<td align=left  nowrap >
<%#DataBinder.Eval(Container.DataItem,"unitprice","{0:c}")%>
</td>
<td align=left  nowrap >
<%#DataBinder.Eval(Container.DataItem,"totalshipping","{0:c}")%>
</td>
<td align=left  nowrap >
<%#DataBinder.Eval(Container.DataItem,"TotalAdditional","{0:c}")%>
</td>
<td align=left  nowrap style="color:red;">
<%#DataBinder.Eval(Container.DataItem,"total","{0:c}")%>
</td>
</tr>
</ItemTemplate>
<HeaderTemplate>
<table  cellpadding=0 cellspacing =0 width="500">
<tr><td  align=left nowrap style="font-weight:bold;">
Description
</td>
<td align=left  nowrap style="font-weight:bold;">
QTY.
</td>
<td align=left  nowrap style="font-weight:bold;">
Unit Price
</td>
<td align=left  nowrap style="font-weight:bold;">
Usage
</td>
<td align=left  nowrap style="font-weight:bold;">
Additional
</td>
<td align=left  nowrap style="font-weight:bold;">
Item Total
</td>
</tr>
</HeaderTemplate>
		
<FooterTemplate>

</FooterTemplate> 
	
</asp:Repeater>
<asp:Literal id="OrderTotals" Text="" visible=true runat="server" />
</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>

</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>












<div id="AssetListing" style="display:block;">
			
<!--Filters Grid Box-->
<!--<b><div style="font-family: verdana;padding-bottom:3px;">Filters</div></b>-->

<div style="display:none;">
<asp:Literal id="LiteralNoFilters" Text="No filters available" visible=false runat="server" />
 

<table cellpadding=0 cellspacing=0><tr><td width="120" nowrap ><b><asp:Literal id="ltrlProjectFilter" Text="" runat="server" /></b></td>
<td width=100%><ComponentArt:Menu id="MenuFilterProjects" 
      ScrollingEnabled="true"
      Orientation="horizontal"
      CssClass="TopGroupPaddingBottom5"
      DefaultGroupCssClass="Group"
      DefaultItemLookId="DefaultItemLook"
      ScrollUpLookId="ScrollUpItemLook"
      ScrollDownLookId="ScrollDownItemLook"
      DefaultGroupItemSpacing="0"
      Visible="False"
      ExpandDelay="100"
      ImagesBaseUrl="images/"
      EnableViewState="false"
      runat="server">
    <ItemLooks>
      <ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="ItemFilters" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
      <ComponentArt:ItemLook LookID="DefaultItemLookPopup" CssClass="ItemFiltersPopup" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
      <ComponentArt:ItemLook LookID="BoldItemLook" CssClass="Itembold" HoverCssClass="ItemHbold" ExpandedCssClass="ItemExpbold" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
      <ComponentArt:ItemLook LookID="ScrollUpItemLook" ImageUrl="scroll_up.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
      <ComponentArt:ItemLook LookID="ScrollDownItemLook" ImageUrl="scroll_down.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
    </ItemLooks>
    </ComponentArt:Menu></td></tr></table>
    
    
    
    
    

	

		<table cellpadding=0 cellspacing=0 ><tr><td width="120" nowrap ><b><font size=1><asp:Literal id="ltrlFileTypeFilter" Text="" runat="server" /></font></b></td>
		<td width=100%><ComponentArt:Menu id="MenuFilterFileType" 
			ScrollingEnabled="true"
			Orientation="horizontal"
			CssClass="TopGroupPaddingBottom5"
			DefaultGroupCssClass="Group"
			DefaultItemLookId="DefaultItemLook"
			ScrollUpLookId="ScrollUpItemLook"
			ScrollDownLookId="ScrollDownItemLook"
			DefaultGroupItemSpacing="0"
			Visible="False"
			ExpandDelay="100"
			ImagesBaseUrl="images/"
			EnableViewState="false"
			runat="server">
			<ItemLooks>
			<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="ItemFilters" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
			<ComponentArt:ItemLook LookID="DefaultItemLookPopup" CssClass="ItemFiltersPopup" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
			<ComponentArt:ItemLook LookID="BoldItemLook" CssClass="Itembold" HoverCssClass="ItemHbold" ExpandedCssClass="ItemExpbold" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
			<ComponentArt:ItemLook LookID="ScrollUpItemLook" ImageUrl="scroll_up.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
			<ComponentArt:ItemLook LookID="ScrollDownItemLook" ImageUrl="scroll_down.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
			</ItemLooks>
			</ComponentArt:Menu></td></tr>
		</table>

	
	

		<table cellpadding=0 cellspacing=0 ><tr><td width="120" nowrap ><b><font size=1><asp:Literal id="ltrlKeywordFilter" Text="" runat="server" /></font></b></td>
		<td width=100%><ComponentArt:Menu id="MenuFilterServices" 
			ScrollingEnabled="true"
			Orientation="horizontal"
			CssClass="TopGroupPaddingBottom5"
			DefaultGroupCssClass="Group"
			DefaultItemLookId="DefaultItemLook"
			ScrollUpLookId="ScrollUpItemLook"
			ScrollDownLookId="ScrollDownItemLook"
			DefaultGroupItemSpacing="0"
			Visible="False"
			ExpandDelay="100"
			ImagesBaseUrl="images/"
			EnableViewState="false"
			runat="server">
			<ItemLooks>
			<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="ItemFilters" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
			<ComponentArt:ItemLook LookID="DefaultItemLookPopup" CssClass="ItemFiltersPopup" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
			<ComponentArt:ItemLook LookID="BoldItemLook" CssClass="Itembold" HoverCssClass="ItemHbold" ExpandedCssClass="ItemExpbold" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
			<ComponentArt:ItemLook LookID="ScrollUpItemLook" ImageUrl="scroll_up.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
			<ComponentArt:ItemLook LookID="ScrollDownItemLook" ImageUrl="scroll_down.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
			</ItemLooks>
			</ComponentArt:Menu></td></tr>
		</table>

	
	

		<table cellpadding=0 cellspacing=0><tr><td width="120" nowrap><b><font size=1><asp:Literal id="ltrlMediaTypeFilter" Text="" runat="server" /></font></b></td>
			<td width=100%><ComponentArt:Menu id="MenuFilterMediaType" 
				ScrollingEnabled="true"
				Orientation="horizontal"
				CssClass="TopGroupPaddingBottom5"
				DefaultGroupCssClass="Group"
				DefaultItemLookId="DefaultItemLook"
				ScrollUpLookId="ScrollUpItemLook"
				ScrollDownLookId="ScrollDownItemLook"
				DefaultGroupItemSpacing="0"
				Visible="False"
				ExpandDelay="100"
				ImagesBaseUrl="images/"
				EnableViewState="false"
				runat="server">
				<ItemLooks>
				<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="ItemFilters" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
				<ComponentArt:ItemLook LookID="DefaultItemLookPopup" CssClass="ItemFiltersPopup" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
				<ComponentArt:ItemLook LookID="BoldItemLook" CssClass="Itembold" HoverCssClass="ItemHbold" ExpandedCssClass="ItemExpbold" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
				<ComponentArt:ItemLook LookID="ScrollUpItemLook" ImageUrl="scroll_up.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
				<ComponentArt:ItemLook LookID="ScrollDownItemLook" ImageUrl="scroll_down.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
				</ItemLooks>
				</ComponentArt:Menu></td></tr>
		</table>    

	
	
	
	
	
		<table cellpadding=0 cellspacing=0><tr><td width="120" nowrap><b><font size=1><asp:Literal id="ltrlIllustTypeFilter" Text="" runat="server" /></font></b></td>
			<td width=100%><ComponentArt:Menu id="MenuFilterIllustType" 
				ScrollingEnabled="true"
				Orientation="horizontal"
				CssClass="TopGroupPaddingBottom5"
				DefaultGroupCssClass="Group"
				DefaultItemLookId="DefaultItemLook"
				ScrollUpLookId="ScrollUpItemLook"
				ScrollDownLookId="ScrollDownItemLook"
				DefaultGroupItemSpacing="0"
				Visible="False"
				ExpandDelay="100"
				ImagesBaseUrl="images/"
				EnableViewState="false"
				runat="server">
				<ItemLooks>
				<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="ItemFilters" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
				<ComponentArt:ItemLook LookID="DefaultItemLookPopup" CssClass="ItemFiltersPopup" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
				<ComponentArt:ItemLook LookID="BoldItemLook" CssClass="Itembold" HoverCssClass="ItemHbold" ExpandedCssClass="ItemExpbold" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
				<ComponentArt:ItemLook LookID="ScrollUpItemLook" ImageUrl="scroll_up.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
				<ComponentArt:ItemLook LookID="ScrollDownItemLook" ImageUrl="scroll_down.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
				</ItemLooks>
				</ComponentArt:Menu></td></tr>
		</table>    
</div>
<!--<table  cellpadding=0 cellspacing =0><tr><td width="125" align=left nowrap><b>Filter Items:</b><img src="images/spacer.gif" width=5 height=1></td><td align=left width=250 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:150px;height:15px;padding:1px;"  onkeydown="javascript:if(event.keyCode==13) {DoSearchFilter(this.value); return false}"></td></tr></table>-->
<asp:Panel Runat=server ID=sortordercustompannel Visible=False>
	<table  cellpadding=0 cellspacing =0 ><tr><td width="125" align=left nowrap><b>Apply Ordering:</b><img src="images/spacer.gif" width=5 height=1></td><td align=left width=350 nowrap ><asp:DropDownList onchange="__doPostBack('__Page', this.selectedIndex);" DataTextField="sort_name" DataValueField="sort_id" id="select_ordering" style="visibility: inherit;" runat="server"></asp:DropDownList>&nbsp;[ <a href="#"  onclick="javasciprt:Object_PopUp_SortEdit('edit');">edit</a> ]&nbsp;[ <a href="#" onclick="javasciprt:Object_PopUp_SortEdit('new');">create new</a> ]</td></tr></table>
	</asp:Panel>


		
		
			
	<table cellpadding=0 cellspacing=0 width=100%><tr width=1><td></td><td width=150 nowrap ></td><td width=100% align=right nowrap  >
				<div style="padding:5px;font-family: verdana; color: #3F3F3F; font-size: 10px;font-weight: normal;display:none;">
				<asp:Literal ID="carousel_download_literal" Runat="server"></asp:Literal>&nbsp;[ <a href="javascript:DownloadPPT('0');">save to PowerPoint</a> ]&nbsp;[ <a href="javascript:ShowSlideShowPrep('0');">view slideshow</a> ]&nbsp;[ <a href="javascript:showContactSheet();">contact sheet</a> ]
				</div>
				
				</td></tr></table>
<input type="hidden" name="view_all" value="" >
	
			
<div id="contactsheet" style="display:none;">

<!--Start Rounded Box-->
<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%">
<div class="sidemodule">
    <div class="modulecontainer sidebar">
        <div class="first">
            <span class="first"></span><span class="last"></span>
        </div>
        <div>
            <table border="0" width="100%">
				<tr>
					<td width="4">&nbsp;</td>
					<td valign="top"><!--End Top Rounded Box-->

            <!--<form method="GET" target="NewWindow" id="contactsheetForm" action="contact_sheet.asp">-->
              <p><b><font face="Verdana" size="1">Contact Sheet Settings<br>
				<br>
				</font></b><font face="arial"  size="1">Heading (Optional - Max 3 lines)</font><font face="Arial" size="1" color=""><font face="Arial" size="1" color="#000000"><img border="0" src="images/spacer.gif" height="1" width="82"><br>
				</font><b>
				&nbsp;<textarea rows="3" name="Heading" cols="62"></textarea><br>
				</b></font><br>
					<font face="arial" size="1">layout</font><font face="Arial" size="1" color="#000000"><img border="0" src="images/spacer.gif" height="1" width="82"><br>
				</font><font face="Arial" size="1" color="">
				<b>
<select   size="1" name="columns" class="more_projects">
                <option  value=2>2 X 2</option>
                <option  value=3>3 X 4</option>
                <option  value=4>4 X 5</option>
              </select></b></font><br>
              <br>
				</font></font>
				<!--<font face="arial" size="1" color="">Selection</font><font face="Arial" size="1" color=""><font face="Arial" size="1" color="#000000"><img border="0" src="images/spacer.gif" height="1" width="82"><br>
				</font><font face="Arial" size="1" color="">
				<b>
<select    size="1" name="selection" class="more_projects">
                <option  value=2>Only Selected Items</option>
              </select></b></font>--></p>
				<table border="0" width="100%" id="table1">
					<tr>
						<td valign="top"><font face="Arial" size="1" color=""><font face="Arial" size="1" color="#000000">
						Optional
				Fields<br>
				</font>
              <font color="#808080" size="1" face="Arial">
              <input class="Preferences" type="checkbox" name="ffiletype" value="1" checked >FileType 
				Icon</font><font face="Arial" size="1" color="#000000"><br>
				</font>
              <font color="#808080" size="1" face="Arial">
              <input class="Preferences" type="checkbox" name="ffilename" value="1" checked >Filename<br>
              <input class="Preferences" type="checkbox" name="ffilesize" value="1" checked >Size<br>
              <input class="Preferences" type="checkbox" name="fdate" value="1" checked >Date<br>
              <input class="Preferences" type="checkbox" name="fdimensions" value="1" checked >Dimensions<br>
              <input class="Preferences" type="checkbox" name="fprojectname" value="1" checked >Project 
						Name<br></td>
					<td>&nbsp;</td>
					<td valign="top">
<div style="width: 250px; height: 200px; overflow: auto;" >			
<asp:Repeater id="RepeaterCSUDF" runat="server" Visible="True">
<ItemTemplate>
	<font color="#808080" size="1" face="Arial"><input class="Preferences" type="checkbox" name="udf" value="<%#DataBinder.Eval(Container.DataItem, "item_id")%>"><%#DataBinder.Eval(Container.DataItem, "item_name")%></font><br>
</ItemTemplate>

<HeaderTemplate>
		<font face="Arial" size="1" color="#000000">Additional User Defined Fields<br></font>
</HeaderTemplate>
		
<FooterTemplate>

</FooterTemplate> 
	
</asp:Repeater>
</div>					
			
					
					
					
					
              </td>
					</tr>
				</table>
					<br><font size="1">Note:&nbsp; </font>
				<font face="Arial" size="1" color="">
				Selecting too many fields may prevent the page from printing 
				properly.  Please use your Print Preview function to 
				ensure that the contact sheet page breaks are aligned properly 
				before printing.<br>
				</b></font><font face="Arial" size="1" color="#000000"><br>
				</font><font face="Arial" size="1" color=""><br>
				<br>
              <input type="button" onclick="javascript:submitContactSheet();" value="View" name="Add">&nbsp;<input type="button" onclick="javascript:showContactSheet();" value="Cancel" name="Cancel"></p></p>
              <input type="hidden" name="actionType" value="edit">
              <input type="hidden" name="carr" value="<%=request.querystring("Carousel_id")%>" >
              <input type="hidden" name="asset_array" value="" >
            <!--</form>-->
            <p>
            
            


<!--Close Rounded Box-->
					</td>
					<td width="4">&nbsp;</td>
				</tr>
			</table>
			</div>
        <div class="last">
        	<span class="first"></span><span class="last"></span>
        </div>
    </div>
</div>
<!--End Rounded Box-->
<!--Start Close Rounded Box Table-->
								</td>
								<td><img border="0" src="images/spacer.gif" height="2" width="11"></td>
							</tr>
</table>
<!--End Close Rounded Box Table-->
</div>

	
<table cellSpacing="0" cellPadding="0" width="100%" >
	<tr>
		<td width=14><div style="padding:4px;"></div></td>
		<td width=14></td>
		<td width=25><div style="padding:4px;"></div></td>
		<td width=14></td>
		<td width=100% align=right style="padding-right:3px;padding-bottom:8px;" >

		[ <a onclick="javascript: Object_PopUp('forms/invoice_reproduction_user_fees.asp?oid=<%response.write(request.querystring("id"))%>','Form',600,800);">view invoice</a> ] [ <a onclick="javascript: Object_PopUp('forms/itemized_invoice_reproduction_user_fees.asp?oid=<%response.write(request.querystring("id"))%>','Form',600,800);">view itemized invoice</a> ] [ <a onclick="javascript: Object_PopUp('forms/lab_order.asp?oid=<%response.write(request.querystring("id"))%>','Form',600,800);">view order authorization</a> ]<!-- [ <a onclick="javascript: Object_PopUp('forms/authorization.asp?oid=<%response.write(request.querystring("id"))%>','Form',600,800);">view authorization</a> ]--> [ <a onclick="javascript: showlogs(<%=request.querystring("ID")%>);">delivery history</a> ]

		</td>		
	</tr>
</table>
	
									
<div class="SnapHeaderViewIcons" style="display:block;" >
<table cellSpacing="0" cellPadding="0" width="100%" >
	<tr>
		<td width=14><div style="padding:4px;"></div></td>
		<td width=14></td>
		<td width=25><div style="padding:4px;"></div></td>
		<td width=14></td>
		<td width=100% align=right style="padding-right:3px;padding-top:2px;" >
		
		
		
		[ <asp:HyperLink id=CheckAllItemsJavascriptFunction runat="server">select all</asp:HyperLink> ]
		
		
		
		
		
		</td>		
	</tr>
</table>
</div>


<div class="assetGirdPagingHeading" id="assetGirdPagingHeading" style="display:block;" >
<div id="CustomFooter" style="width:100%;border-top:none;">
<table width="100%">
<tr>
<td id="tdPager" align="left"></td>
<td id="tdIndex" align="right"></td>
</tr>
</table>
</div>
</div>		
				
				
			
			
			
			
	<script language="javascript">
	function loadCarouselContextMenu(evt, id) {
		loadContextMenu(evt, id, <%=GridAssets.ClientID%>);
	}
	</script>
			
			
											

	<!--GroupBy="categoryname ASC"-->
	<COMPONENTART:GRID 
	id="GridAssets" 
	runat="server" 
	AutoFocusSearchBox="false"
	AutoCallBackOnInsert="true"
	AutoCallBackOnUpdate="true"
	AutoCallBackOnDelete="true"
    ClientSideOnLoad="onLoad"   
	pagerposition="2"
	ScrollBar="Off"
	ScrollTopBottomImagesEnabled="true"
	ScrollTopBottomImageHeight="2"
	ScrollTopBottomImageWidth="16"
	ScrollImagesFolderUrl="images/scroller/"
	ScrollButtonWidth="16"
	ScrollButtonHeight="17"
	ScrollBarCssClass="ScrollBar"
	ScrollGripCssClass="ScrollGrip"
	ClientSideOnInsert="onInsert"
	ClientSideOnUpdate="onUpdate"
	ClientSideOnDelete="onDeleteAsset"
	ClientSideOnCallbackError="onCallbackError"
	ScrollPopupClientTemplateId="ScrollPopupTemplate" 
	Height="10" Width="100%"
	LoadingPanelPosition="TopCenter" 
	LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
	LoadingPanelFadeDuration="200"
	LoadingPanelFadeMaximumOpacity="95"
	EnableViewState="true"
	GroupBySortImageHeight="10" 
	GroupBySortImageWidth="10" 
	GroupBySortDescendingImageUrl="group_desc.gif" 
	GroupBySortAscendingImageUrl="group_asc.gif" 
	GroupingNotificationTextCssClass="GridHeaderText" 
	AlternatingRowCssClass="AlternatingRowCategory" 
	IndentCellWidth="22" 
	TreeLineImageHeight="19" 
	TreeLineImageWidth="20" 
	TreeLineImagesFolderUrl="images/lines/" 
	PagerImagesFolderUrl="images/pager/" 
	ImagesBaseUrl="images/" 
	PreExpandOnGroup="True" 
	GroupingPageSize="5" 
	PagerTextCssClass="GridFooterTextCategory" 
	PagerStyle="Numbered" 
	GroupByTextCssClass="GroupByText" 
	GroupByCssClass="GroupByCell" 
	FooterCssClass="GridFooter" 
	HeaderCssClass="GridHeader" 
	SearchOnKeyPress="true" 
	SearchTextCssClass="GridHeaderText" 
	AllowEditing="true" 
	AllowSorting="False"
	ShowSearchBox="false" 
	ShowHeader="false" 
	ShowFooter="false" 
	CssClass="Grid" 
	RunningMode="callback" 
	ScrollBarWidth="15" 
	AllowPaging="false"
	PageSize="1000" >
<ClientEvents>
    <CallbackComplete EventHandler="GridAssets_onCallbackComplete" />
  </ClientEvents>

	<ClientTemplates>


	<ComponentArt:ClientTemplate Id="EditTemplate">
			 <img src="images/13Save2.gif" border=0 alt="Download" onClick="loadCarouselContextMenu(event, '## DataItem.GetMember("asset_id").Value ##');" /> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border=0></a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Remove from carousel" border=0></a>
			</ComponentArt:ClientTemplate>
	<ComponentArt:ClientTemplate Id="EditTemplateNoRemove">
			<a href="javascript:DownloadAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border=0></a> 
			</ComponentArt:ClientTemplate>			
			<ComponentArt:ClientTemplate Id="EditCommandTemplate">
				<a href="javascript:editRow();">Update</a> 
			</ComponentArt:ClientTemplate>
			<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
				<a href="javascript:insertRow();">Insert</a> 
			</ComponentArt:ClientTemplate>          
			<ComponentArt:ClientTemplate Id="LookupProjectTemplate">
			<table border=0 cellpadding=0 cellspacing=0 height=25><tr><td valign=middle align=center height=25><A href="## getALink() #### DataItem.GetMember("asset_id").Value ##"><img border=0 src="## getURLPreviewSml() #### DataItem.GetMember("asset_id").Value ##" onmouseover="javascript:showtrailpreload('## DataItem.GetMember("asset_id").Value ##','','','5','1',270,7);PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.ClientId ##',event);" onmouseout="javascript:hidetrail();" ></a></td></tr></table> 
			</ComponentArt:ClientTemplate> 
			<ComponentArt:ClientTemplate Id="LookupAssetPopupTemplate">
				<A href="#" onclick="javascript:PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.ClientId ##',event);"><img border=0 src="images/15printPreview.gif"></a>
			</ComponentArt:ClientTemplate> 
			<ComponentArt:ClientTemplate Id="LookupProjectTemplatePreview">
				<A href="javascript:AssetPreview('## DataItem.ClientId ##');"><img border=0 src="images/15printPreview.gif"></a>
			</ComponentArt:ClientTemplate>              
			<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
				<table height="1px" width="100%" ><tr><td valign="center" align="center">
				<table cellspacing="0" cellpadding="0" border="0"  background="images/loadingback.gif" height="70px" width="300px">
				<tr>
					<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
					<td style="font-size:10px;font-family:Verdana;" width="60px" align="right">Loading...&nbsp;</td>
					<td><img src="images/spinner2.gif"  border="0"></td>
					<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
				</tr>
				</table>
				</td></tr></table>
			</componentart:ClientTemplate>   
</ClientTemplates>


	<Levels>
	<componentart:GridLevel EditCellCssClass="EditDataCell"
				EditFieldCssClass="EditDataField"
				EditCommandClientTemplateId="EditCommandTemplate"
				InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="orderitemid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asch10.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="desch10.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="10" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
	<Columns>

	<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" AllowSorting="false" Align="Center" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplate" HeadingCellCssClass="FirstHeadingCell" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="14" FixedWidth="True" />
	<componentart:GridColumn AllowEditing="True" DataCellCssClass="DataCell" HeadingText="Name"  Width="70" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="assetname" ></componentart:GridColumn>
	<componentart:GridColumn AllowEditing="false" DataCellCssClass="DataCell" HeadingText="Source"  Width="90" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="sourcename" ></componentart:GridColumn>
	<componentart:GridColumn AllowEditing="True" DataCellCssClass="DataCell" HeadingText="Copyright" Width="90" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="copyright" ></componentart:GridColumn>
	<componentart:GridColumn AllowEditing="True" DataCellCssClass="DataCell" HeadingText="Notes" Width="90" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="notes" ></componentart:GridColumn>
	<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="True" HeadingText="Format"  Width="90" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="download_id" ForeignTable="EXPORT" ForeignDataKeyField="download_id" ForeignDisplayField="downloadname" ></componentart:GridColumn>
	<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="True" HeadingText="Delivery"   Width="90" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="deliverytype" ForeignTable="deliverytypetable" ForeignDataKeyField="deliverytype" ForeignDisplayField="deliverytype" ></componentart:GridColumn>
	<componentart:GridColumn AllowEditing="True"  HeadingText="Quantity"  Align="Right" Width="60" SortedDataCellCssClass="SortedDataCell" DataField="Quantity"></componentart:GridColumn>
	<componentart:GridColumn AllowEditing="True"  FormatString="c" HeadingText="Unit Price" Align="Right" Width="60"  SortedDataCellCssClass="SortedDataCell" DataField="Price"></componentart:GridColumn>
	<componentart:GridColumn AllowEditing="True"   FormatString="c"  HeadingText="Usage Fee" Align="Right" Width="60"  SortedDataCellCssClass="SortedDataCell" DataField="shippingcost"></componentart:GridColumn>
	<componentart:GridColumn AllowEditing="True"  FormatString="c"   HeadingText="Add Fee" Align="Right"  Width="60"  SortedDataCellCssClass="SortedDataCell" DataField="additionalcost"></componentart:GridColumn>
	<componentart:GridColumn DataCellCssClass="TotalsDataCell"   FormatString="c"   HeadingText="Total Cost" Align="Right" Width="80" AllowEditing="false"  SortedDataCellCssClass="SortedDataCell" DataField="TotalItemCost" ></componentart:GridColumn>
	<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Status" Align="Right"  Width="90" AllowEditing="true"  SortedDataCellCssClass="SortedDataCell" DataField="status" ForeignTable="StatusTable" ForeignDataKeyField="status" ForeignDisplayField="Name" ></componentart:GridColumn>
    <ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowSorting="False" AllowEditing="false" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Width="100"  Align="Center" />
	<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
	<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="orderitemid" SortedDataCellCssClass="SortedDataCell" DataField="orderitemid"></componentart:GridColumn>
	<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="media_type" SortedDataCellCssClass="SortedDataCell" DataField="media_type"></componentart:GridColumn>
	<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="download_id" SortedDataCellCssClass="SortedDataCell" DataField="download_id"></componentart:GridColumn>
	<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="status" SortedDataCellCssClass="SortedDataCell" DataField="status"></componentart:GridColumn>
	
	</Columns>
	</componentart:GridLevel>
	</Levels>
	<ServerTemplates>
	<ComponentArt:GridServerTemplate Id="PickerTemplate">
	<Template>

	<input id="txtImageUrl" type="text" value="<%# Container.DataItem("name") %>"/>


	</Template>
	</ComponentArt:GridServerTemplate>




	</ServerTemplates>
	

	</COMPONENTART:GRID>



<br>
			<input type="button" onclick="GridAssets.Page(GridAssets.PageCount-1);GridAssets.Table.AddRow();" value="Add Order Item" />


<div style="padding:10px;"><asp:Literal id="literalNoAssets" Text="No assets available" visible=false runat="server" /></div>










											<style>
											div.thumbnailbox {float:left!important; width:220px!important; width:180px; height:320px!important; height:220px;}
											div.thumbnailboxsc {width:170px;height:220px;}
											div.asset_name      { width:164px; overflow: hidden;padding-left:5px;
																text-overflow-mode:ellipsis;font-size:8px }
											div.asset_date      { width:120px; overflow: hidden;padding-left:5px;
																text-overflow-mode:ellipsis;font-size:8px }  
											div.asset_filetype  { width:80px; overflow: hidden;
																text-overflow-mode:ellipsis;font-size:8px }                  
											div.asset_filesize  { width:60px; overflow: hidden;
																text-overflow-mode:ellipsis;font-size:8px }    
											div.asset_owner     { width:120px; overflow: hidden;padding-left:5px;
																text-overflow-mode:ellipsis;font-size:8px }    
											div.asset_links    {width:100%;padding-left:2px; height:4px;overflow:hidden;background-image: url(images/dottedline.gif);background-repeat:repeat-x;}                                                                           
											span.nowrap       { white-space : nowrap; }
											div.attributed-to { position: relative;left:8px }
											</style>

											<COMPONENTART:CALLBACK id="ThumbnailCallBack" runat="server" CacheContent="false">
												<CONTENT>
													<!--hidden Thumbs-->
													<asp:Literal id="ltrlPager" Text="" runat="server" />
													<asp:Repeater id=Thumbnails runat="server" Visible="False">
														<ItemTemplate>
															<div valign="Top"  class="thumbnailbox" > 
																<div class="thumbnailboxsc"><!--Asset Thumbnails-->
																	<div align="left"><!--PADDING LEFT X1-->
																		<div align="left"><!--PADDING LEFT X2-->
																			<table  class="bluelightlight" border="0" cellspacing="0" width="15">
																				<tr>
																				<td  class="bluelightlight" valign="top">
																					<div align="left" id="assethighlight_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" style="border:1px white solid;">
																						<table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse"><tr><td>
																						<table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse" height="120" width="160">
																						<tr class="bluelightlight" width="100%" align="center" height="120" width="160"><td class="bluelightlight" width="100%" height="120" width="160" align="center" valign="bottom" bordercolor="#808080" bordercolorlight="#808080" nowrap><img src="images\spacer.gif" height="0" width="159"><br><a id="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" href="javascript:highlightAssetToggleII(document.getElementById('aselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>'),<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);"><img alt="" border="0" src="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?instance=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMInstance%>&type=asset&size=2&height=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.ConfigurationService.GetConfigurationSetting(WebArchives.iDAM.Services.ConfigurationService.WebAdminConfigKey.IDAMThumbnailHeight)%>&width=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.ConfigurationService.GetConfigurationSetting(WebArchives.iDAM.Services.ConfigurationService.WebAdminConfigKey.IDAMThumbnailWidth)%>&qfactor=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMQfactor%>&id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" onmouseover="javascript:showtrailpreload('<%#DataBinder.Eval(Container.DataItem, "asset_id")%>','','','5','1',270,7);PreviewOverlayOnThumbSingleClickAssets('<%#DataBinder.Eval(Container.DataItem, "asset_id")%>','<%#DataBinder.Eval(Container.DataItem, "name").Replace("'","\'")%>','',event);" onmouseout="javascript:hidetrail();" ></a></td><td><img src="images\spacer.gif" width="0" height="140"></td>
																						</tr>
																						</table></td></tr></table>
																					</div>
																				</td>
																				</tr>
																				<tr>
																				<td>      
																					<!--PADDING-->
																					<div style="padding:5px;">             
																						<table width="100%" cellspacing="0" cellpadding="0" >
																							<tr>
																								<td nowrap width="5"><img src=""></td>
																								<td nowrap><div class="asset_name"><span class="nowrap"><b><font face="Verdana" size="1"><%#DataBinder.Eval(Container.DataItem, "name")%></font></b></span></div></td>
																								<td align="right" width="20"><font color="#808080" face="Arial" size="1"><input onclick="highlightAssetToggle(this,<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);" class="preferences" type="checkbox" id="aselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" value="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"><input type=hidden id=selectassethidden value="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"></font></td>
																							</tr>
																						</table>
																						<table width="100%"  cellspacing="0" cellpadding="0" >
																							<tr>
																								<td nowrap width="100%" align="left"><div class="asset_filetype"><span class="nowrap"><font face="Verdana" size="1"><%# GetAssetFileTypeName(CType(DataBinder.Eval(Container.DataItem, "media_type"), String))%></font></span></div></td>
																							<td align="right"><div class="asset_filesize"><span class="nowrap"><font face="Verdana" size="1"><%# FormatSize(CType(DataBinder.Eval(Container.DataItem, "filesize"), Long))%></font></span></div></td>
																							</tr>
																						</table>
																						<table width="100%" cellspacing="0" cellpadding="0" >
																							<tr>
																								<td nowrap><font face="Verdana" size="1">Updated</font></td>
																								<td align="left" width="868"><div class="asset_date"><span class="nowrap"><font face="Verdana" size="1">

																							<%# String.Format("{0:MM/dd/yy hh:mm tt}", (DataBinder.Eval(Container.DataItem, "update_date")))%></font></span></div></td>

																							</tr>
																							<tr>
																								<td nowrap><font face="Verdana" size="1">Created By</font></td>
																								<td align="left" width="868"><div class="asset_owner"><span class="nowrap"><font face="Verdana" size="1"><%# GetAssetOwner(CType(DataBinder.Eval(Container.DataItem, "asset_id"), String))%></font></span></div></td>
																							</tr>
																							<tr>
																								<td nowrap><font face="Verdana" size="1">Project</font></td>
																								<td align="left" width="868"><div class="asset_owner"><span class="nowrap"><font face="Verdana" size="1"><a href="IDAM.aspx?page=Project&ID=<%# DataBinder.Eval(Container.DataItem, "projectid")%>&type=project"><%# GetAssetProjectName(CType(DataBinder.Eval(Container.DataItem, "projectid"), String))%></a></font></span></div></td>
																							</tr>
																						</table>
																						<div style="padding-top:3px;">
																							<div class="asset_links"></div>
																						</div>
																						<table width=100%><tr><td>
																						<b><font face="Verdana" size="1"><a href="IDAM.aspx?page=Asset&ID=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=asset">View Details</a></font><br>
																						
																						<font face="Verdana" size="1"><a onclick="loadContextMenuThumbs(event, '0 <%#DataBinder.Eval(Container.DataItem, "asset_id")%>');">Download</a></font><br>
																						
																						</td><td align=right valign=top>
																						<%if spOwned then%><b><font face="Verdana" size="1"><a href="javascript:DeleteSelected('0 <%#DataBinder.Eval(Container.DataItem, "asset_id")%>');">Remove</a></font></b><%end if%></td></tr></table>
																					</div>                        
																					<!--END PADDING-->
														                        
																				</td>
																				</tr>
																			</table>
																		</div><!--END PADDING LEFT X2-->
																		<img border="0" src="images/spacer.gif" width="135" height="8">
																	</div><!--END PADDING LEFT X1-->
																</div><!--end Asset Thumbnail span-->
															</div><!--end inner span-->
														</ItemTemplate>
													
														<HeaderTemplate>
															<!--Start Thumbnail Float-->
															<table width=100%><tr><td>
																<div valign="Bottom" style="float:left!important;">       
														</HeaderTemplate>
																
														<FooterTemplate>
															</td></tr></table>
														</FooterTemplate> 
															
													</asp:Repeater>
													<asp:Literal id="ltrlPagingBottom" Text="" runat="server" />
												</CONTENT>
												<LOADINGPANELCLIENTTEMPLATE>
													<TABLE height="300" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD align="center">
																<TABLE cellSpacing="0" cellPadding="0" border="0">
																	<TR>
																		<TD style="FONT-SIZE: 10px">Loading...
																		</TD>
																		<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
													</TABLE>
												</LOADINGPANELCLIENTTEMPLATE>
											</COMPONENTART:CALLBACK>

							<!--SNAP OVERLAY WAS HERE-->

					<div id=trailimageid>
									<div class="SnapProjectsWrapperPopup" style="width:599px;">					
										<COMPONENTART:CALLBACK id="CallbackProjectAssetOverlay" runat="server" CacheContent="false">
											<CONTENT>
												<asp:PlaceHolder ID="PlaceHolderAssetPreviewPage" runat="server">
													<!--assetOptions-->
													<!--
													<div class='assetOptionsPopup'>
													
													
													
													
													
														<table border="0" width="94%" cellspacing="0" cellpadding="0" id="table1">
															<tr>
																<td >

																[<font size="1"><asp:Literal ID="LiteralAssetPopupLinksDownload" Runat=server></asp:Literal>
																download</a></font></td>
																

																
																																
																<td nowrap >

																<font size="1"> ] [ <a href="asfdas">export
																</a> </font>
															</td>
																<td nowrap >

																<font size="1"> 
															<select size="1" name="D1">
															<option>Select Type</option>
															<option>TIFF Large</option>
																</select></font></td>
																<td >

																 ] [ <a href="asd"><font size="1">reconvert</font></a><font size="1">
																</font>
									
  																</td>
																<td >

																<font size="1"> ] [ <asp:Literal ID="LiteralAssetPopupLinksCarousel" Runat=server></asp:Literal>add to 
																carrousel</a></font></td>
																<td ">

																<font size="1"> ] [ <asp:Literal ID="LiteralAssetPopupLinksView" Runat=server></asp:Literal>view 
																details</a></font></td>
																<td>

															 ] [ <a href="#ee"><font size="1">delete</font></a><font size="1"> ]
																</font>
												
  																</td>
															</tr>
														</table>
														
														
													</div>--><!--END assetOptions-->
													
													<!--SnapProjectsOverlay-->
													<div class="SnapProjectsOverlay" >
														<font face="Verdana" size="1">
														<table id="table3">
															<tr>

																<td>
														
																<font size="1"><b>Image:</b></font></span><font size="1">
																</font></td>

																<td align="left" >

																<font size="1"><asp:Literal ID="ProjectAssetOverlayCurrentImageName" Runat=server></asp:Literal></font></td>
												
  															</tr>
															<tr >

																<td><font size="1"><b>Location(s):</b></font></span><font size="1">
																</font></td>

																<td align="left" >
																
																<font size="1"><asp:Literal ID="ProjectAssetOverlayCurrentProjectName" Runat=server></asp:Literal></font></a><font size="1">
																</font></td>

															</tr>
															</table>
														<br>
														<asp:Image id="ProjectAssetOverlayCurrentImage" runat="server" ImageUrl="~/common/images/spacer.gif"></asp:Image>
														
														<!--<br>
													[ <asp:Literal ID="LiteralRotateRight" Runat=server></asp:Literal>rotate right</a> ] [ <asp:Literal ID="LiteralRotateLeft" Runat=server></asp:Literal>rotate left</a> ]-->
													<br>
														<table id="table2">
															<tr>

																<td>
														
																<font size="1">Description:</font></span><font size="1">
																</font></td>
										
																<td align="left" width="80%">

																<font size="1"><asp:Literal ID="LiteralAssetDescription" Runat=server></asp:Literal></font></td>
												
  															</tr>
															<tr >
																<td><font size="1">Owner</font><font size="1">:</font></span><font size="1">
																</font></td>
																<td align="left" width="80%">
																
																<font size="1"><asp:Literal ID="LiteralAssetOwner" Runat=server></asp:Literal></font></a><font size="1">
																</font></td>
															</tr>
															<tr >
																<td noWrap>
																
																<font size="1">Date Created:</font></span><font size="1">
																</font></td>
																<td>
																<font size="1"><asp:Literal ID="LiteralAssetDateCreated" Runat=server></asp:Literal></font></span><font size="1">
																</font></td>
															</tr>
															<tr>


																<td noWrap><font size="1">Type</font><font size="1">:</font></span><font size="1">
																</font></td>
												
																<td>
																<font size="1"><asp:Literal ID="LiteralAssetType" Runat=server></asp:Literal></font></td>
											

  															</tr>
															<tr>

																<td noWrap><font size="1">File Size</font><font size="1">:</font></span><font size="1">
																</font></td>

																<td>
																<font size="1"> 
																
																<asp:Literal ID="LiteralAssetSize" Runat=server></asp:Literal></a> </font>
																</td>
									
  															</tr>
															<tr>

																<td noWrap>
										
																<font size="1">Keywords:</font></span><font size="1">
																</font></td>
										
																<td>
																<font size="1"><asp:Literal ID="LiteralAssetKeywords" Runat=server></asp:Literal></font></td>
											
  															</tr>
															
														</table>
													</div><!--END SnapProjectsOverlay-->
												</asp:PlaceHolder>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<div class="SnapProjectsOverlay" >
													<TABLE cellSpacing="0" cellPadding="0" border="0" height="500" width=100%>
														<TR>
															<TD align="center">
																<TABLE cellSpacing="0" cellPadding="0" border="0">
																	<TR>
																		<TD style="FONT-SIZE: 10px">Loading...
																		</TD>
																		<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																	</TR>
																</TABLE>
															</TD>
															<td><IMG height="16" src="images/spacer.gif" width="16" height="500" border="0">
															</td>
														</TR>
													</TABLE>
												</div>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK></div></div>
							<!--END SnapProjectAssetOverlay-->
							
							
							
			

</div><!--end asset listings frame-->



<div id="ProjectListing" style="display:none;">

<!--Main Grid Box-->




<table width="100%">
        <tr>
          <td align=left width="30%"></td>
<td align=right width="70%"> </td></tr></table>





<!--No Projects warning-->
<asp:Literal id="literalNoProjects" Text="No projects available" visible=false runat="server" />





<COMPONENTART:CALLBACK id="CallbackProjects" runat="server" CacheContent="true">
<CONTENT>




<!--hidden Thumbs-->

<asp:Literal id="LiteralTopPagerProjects" Text="" runat="server" />
<asp:Repeater id="RepeaterProjects" runat="server" Visible="False">
<ItemTemplate>














<div valign="Top" style="float:left!important; width:220px!important; width:180px; height:220px!important; height:220px;">
<!--Project Thumbnails-->
<div style='width:170px;height:220px;'>
        <div align="left">
                    <div align="left">
                      <table  class="bluelightlight" border="0" cellspacing="0" width="15">
                        <tr>
                          <td  class="bluelightlight" valign="top">
                          <div align="left" id="projecthighlight_<%#DataBinder.Eval(Container.DataItem, "projectid")%>" style="border:1px white solid;">
                          <table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse"><tr><td>
                            <table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse" height="120" width="160">
                              <tr class="bluelightlight" width="100%" align="center" height="120" width="160"><td class="bluelightlight" width="100%" height="120" width="160" align="center" valign="bottom" bordercolor="#808080" bordercolorlight="#808080" nowrap><img src="images\spacer.gif" height="0" width="159"><br><a id="25257" href="cd_asset.asp?p=50155&a=25257&pc=&pcc=&c=&post="><img alt="" border="0" src="
  <%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?id=<%#DataBinder.Eval(Container.DataItem, "projectid")%>&instance=<%#DataBinder.Eval(Container.DataItem, "instance")%>&type=project&size=2&height=120&width=160"></a></td><td><img src="images\spacer.gif" width="0" height="140"></td>

                              </tr>
                            </table></td></tr></table>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td >      
                          
                          
                          
                          
                          
                          
<div style="padding:5px;">             
<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
<td nowrap width="5"><img src="images/projcat16.jpg"></td>
	<td nowrap><div class="asset_name"><span class="nowrap"><b><font face="Verdana" size="1"><%#DataBinder.Eval(Container.DataItem, "name")%></font></b></span></div></td>
	<td align="right" width="20"><font color="#808080" face="Arial" size="1"><input onclick="highlightProjectToggle(this,<%#DataBinder.Eval(Container.DataItem, "projectid")%>);" class="preferences" type="checkbox" name="pselect_<%#DataBinder.Eval(Container.DataItem, "projectid")%>" value="<%#DataBinder.Eval(Container.DataItem, "projectid")%>"></font></td>
</tr>
</table>

<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
	<td nowrap><font face="Verdana" size="1">Updated</font></td>
	<td align="left" width="868"><div class="asset_date"><span class="nowrap"><font face="Verdana" size="1">

<%# String.Format("{0:MM/dd/yy hh:mm tt}", (DataBinder.Eval(Container.DataItem, "update_date")))%></font></span></div></td>

</tr>
</table>
<div style="padding-top:3px;">
<div class="asset_links"></div></div>
<b><font face="Verdana" size="1"><a href="#">View Details</a></font><br>       
</div>                        
                          
                          
							</td>
                        </tr>
                      </table>
                    </div>
                    
                   <img border="0" src="images/spacer.gif" width="135" height="8">
                   </div>
                   </div><!--end Project Thumbnail span-->

                   </div><!--end inner span-->





</ItemTemplate>
<HeaderTemplate>
<!--Start Thumbnail Float-->
<table width=100%><tr><td>
<div valign="Bottom" style="float:left!important;">       
</HeaderTemplate>
<FooterTemplate>
</td></tr></table>
</FooterTemplate> 
</asp:Repeater>
<asp:Literal id="LiteralBottomPagerProjects" Text="" runat="server" />

<!--hidden Thumbs-->



											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<TABLE height="300" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px">Loading...
																	</TD>
																	<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>

<div style="padding:25px;"></div>


</div><!--end project listing frame-->

































</div>
</div>





















































      <script type=text/javascript>
  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

  
  function BrowseOnDoubleClick(SelectedGridRow)
  {
  
  	var itemvaluetmp;
	itemvaluetmp = SelectedGridRow.GetMember('asset_id').Value;
	if (itemvaluetmp.substring(0,1) == 'P') 
	{
		itemvaluetmp = itemvaluetmp.replace('P_','');
		window.location.href = 'IDAM.aspx?page=Project&id=' + itemvaluetmp
    
	} else {
		itemvaluetmp = itemvaluetmp.replace('C_','');
		window.location.href = 'IDAM.aspx?page=Browse&id=' + itemvaluetmp + '&type=category';
	}
	
	return true;
  
  
  
	
  }
  
  function highlightAssetToggle(checkbox,asset_id)
  {
  var highlightbox;
  highlightbox = document.getElementById('assethighlight_' + asset_id);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  
  
  function highlightProjectToggle(checkbox,projectid)
  {
  var highlightbox;
  highlightbox = document.getElementById('projecthighlight_' + projectid);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  

  </script>



      
      
      
      
      
      
      
      
      
      
      
      
<div style="padding-left:10px;"><div style="padding:3px;"></div></div>




<script language=javascript>
//thumbnailsarea.style.display = 'none';
</script>




<script language=javascript type="text/javascript">

function showtrailpreload(imageid,title,description,ratingaverage,ratingnumber,showthumb,height,filetype)
{
	showtrail('<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?id=' + imageid + '&amp;instance=<%response.write (IDAMInstance)%>&amp;type=asset&amp;size=2&amp;height=399&amp;width=370',title,description,'5','1',270,7);

}

</script>	



<script>




function showlist(list){
		
}

function switchToPojectList() {
  setLastViewResults ('project');	
}
function switchToAssetList() {
  setLastViewResults ('asset');
}

		
function AddToCarouselFromPG(ids)
  {
  //check to see if multi select
	var sAssets;
	var addtype;
	<%If bThumbnailView Then%>
        
    
    var arraylist;
	var i;
	arraylist = '';
	ids = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
	if (arraylist.split(',').length > 2) {
			 if (confirm("Add selected assets to carousel?"))
			 {
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				addtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	addtype = 'single';
	}
       
    
    <%else%>
        
        
    sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
	ids = ids.replace('0 ','');
	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Add selected assets to carousel?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (arraylist == ''){
					arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
					} else {
					if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
					arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					}
					}
				}
				
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				addtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	addtype = 'single';
	}
        

    <%end if%>
	
	
	if (addtype == 'multi') {
		if (arraylist != ''){
		
		carousel_callback.Callback('1,' + arraylist + ',AddMultiToCarousel');
		}
	}
	
	if (addtype == 'single') {
	/*assume single*/
	carousel_callback.Callback('1,' + ids + ',AddToCarousel');
	} 
  }	
  
    function PreviewOverlayOnThumbSingleClickAssets(aid,aname,afiletype,eventObject)
  {
	<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(aid + ',Asset' + ',' + aname + ',' + afiletype);
  } 
  




  	
function DeleteSelected(ids)
  {
  //check to see if multi select
	var sAssets;
	var addtype;
  
    
    var arraylist;
	var i;
	arraylist = '';
	ids = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
	if (arraylist.split(',').length > 2) {
			 if (confirm("Remove selected assets from this carousel?"))
			 {
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			 }
			 else
			 {
			    if (confirm("Remove individual asset from this carousel?"))
			    {
				/*delete single*/
				addtype = 'single';
				} else {
				/*delete nothing*/
				addtype = 'donothing';
				}
			 }
	}
	else {
				if (confirm("Remove this item from this order?"))
			    {
				/*delete single*/
				addtype = 'single';
				} else {
				/*delete nothing*/
				addtype = 'donothing';
				}
	}
       
    
	
	
	if (addtype == 'multi') {
		if (arraylist != ''){
		<%response.write(ThumbnailCallBack.ClientID)%>.Callback('DeleteMulti,' + arraylist );
		}
	}
	
	if (addtype == 'single') {
	/*assume single*/
	<%response.write(ThumbnailCallBack.ClientID)%>.Callback('DeleteSingle,' + ids );
	} 
  }	



function DoSearchFilter(searchvalue)
{
//SetCookie ('SearchValueOrders', searchvalue, exp);
<%response.write (GridAssets.ClientID)%>.Filter('IDAM'+searchvalue);
}


function Object_PopUp_SortEdit(tab)
{
  var dropdown = document.getElementById('<%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_select_ordering');
  var myindex  = dropdown.selectedIndex;
  var SelValue = dropdown.options[myindex].value;
  if (tab == 'edit')
  {
  Object_PopUp('CarouselSortEdit.aspx?ID=' + SelValue + '&carousel_id=<%=request.querystring("ID")%>','New_Sort',480,660);
  } else {
  Object_PopUp('CarouselSortEdit.aspx?carousel_id=<%=request.querystring("ID")%>','New_Sort',480,660);
  }
}	

function RefreshCarousel(id)
{
	if (id!='')
		{
			window.location.href = 'IDAM.aspx?page=Carousel&id=' + id 
		} 
}



function ShowSlideShowPrep(ids)

 {
  //check to see if multi select
	var sAssets;
	var addtype;
	<%If bThumbnailView Then%>
        
    
    var arraylist;
	var i;
	arraylist = '';
	ids = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 0) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	if (arraylist.split(',').length == 1) {
		alert('Please select the asset(s) you wish to view.');
	}
	if (arraylist.split(',').length > 2) {
			 
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			
	}
	else {
	
	addtype = 'single';
	}
       
    if (arraylist.split(',').length > 1) {

    ShowSlideShow(arraylist);
    <%else%>
        
    <%response.write( GridAssets.ClientId)%>.Render;   
    sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
    if (sAssets.length == 0) {
		alert('Please select the asset(s) you wish to save to a Slideshow.');
	}
	ids = ids.replace('0 ','');
	var arraylist;
	var i;
	arraylist = '';

	if (sAssets.length > 0) {

		
		for (i = 0; i < sAssets.length; i++) {
			if (arraylist == ''){
			try
			{
			arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
			}
			catch (err)
			{}
			
			} else {
			try
			{
			if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
			arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
			}
			}
			catch (err)
			{}
			}
		}
		
		arraylist= arraylist.substring(1, arraylist.length);
		addtype = 'multi';

	}
	else {
	
	addtype = 'single';
	}
        
	if (sAssets.length > 0) {
	ShowSlideShow(arraylist);
    <%end if%>

	
	
	}

	
}



//setup form switch for delete or multi asset zip download
function DownloadPPT(ids)


 {

  //check to see if multi select
	var sAssets;
	var addtype;
	<%If bThumbnailView Then%>
        
    
    var arraylist;
	var i;
	arraylist = '';
	ids = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
    if (arraylist.split(',').length == 1) {
		alert('Please select the asset(s) you wish to save to Powerpoint.');
	}
	if (arraylist.split(',').length > 2) {
			 
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			
	}
	else {
	/*download single*/
	addtype = 'single';
	}
       
    if (arraylist.split(',').length > 1) {
    <%else%>
    <%response.write( GridAssets.ClientId)%>.Render;   
    sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
    if (sAssets.length == 0) {
		alert('Please select the asset(s) you wish to save to Powerpoint.');
	}
    //alert(sAssets);
	ids = ids.replace('0 ','');
	var arraylist;
	var i;
	arraylist = '';

	if (sAssets.length > 0) {

		/*download multi*/
		for (i = 0; i < sAssets.length; i++) {
			if (arraylist == ''){
			try
			{
			arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
			}
			catch (err)
			{}
			
			} else {
			try
			{
			if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
			arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
			}
			}
			catch (err)
			{}
			}
		}
		
		arraylist= arraylist.substring(1, arraylist.length);
		addtype = 'multi';

	}
	else {
	/*download single*/
	addtype = 'single';
	}
        
	if (sAssets.length > 0) {
    <%end if%>

	
	/*assume single*/
	//alert(arraylist);

	<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback("ppt",escape("<%=_carouselName%>"),arraylist,"<%=request.queryString("ID")%>");
	
	//window.open('<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLLocationIDAMDownloadPPT & "?filename=" + server.URLEncode(_carouselName + ".ppt")+ "&assetids="%>' + arraylist + '<%="&instanceid=" & IDAMInstance%>','ConvertingImage','width=500,height=75,location=no');
	//return true;
	
	}

}





function showContactSheet() {
	if (document.getElementById('contactsheet').style.display == "block") {
		document.getElementById('contactsheet').style.display = "none";
	} else {
	if (document.getElementById('contactsheet').style.display == "none") {
		document.getElementById('contactsheet').style.display = "block";
	}
	}
}




  

//submit contact sheet
function submitContactSheet() {

	//get all selected values...
	var field;

<%If bThumbnailView Then%>
        
    
    var arraylist;
	var i;
	arraylist = '';

	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
	if (arraylist.split(',').length > 2) {
			 
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			
	}
	else {
	/*download single*/
	addtype = 'single';
	}
       
    
    <%else%>
        
        
    sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();

	
	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length >= 1) {

		/*download multi*/
		for (i = 0; i < sAssets.length; i++) {
			if (arraylist == ''){
			try
			{
			arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
			}
			catch (err)
			{}
			
			} else {
			try
			{
			if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
			arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
			}
			}
			catch (err)
			{}
			}
		}
		
		arraylist= arraylist.substring(1, arraylist.length);
		addtype = 'multi';

	}
	else {
	/*download single*/
	addtype = 'single';
	}
        

    <%end if%>
	

	document.forms[0].asset_array.value = arraylist;

	if (arraylist.split(',').length >= 1)
		{
		redirectOutput(document.forms[0]);
		//document.forms[0].submit();
			//Object_PopUp('ContactSheet.aspx?uid=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.UserID.tostring%>&Heading=' + replace(document.forms[0].Heading.value,chr(13),"<BR>") + '&columns=' + document.forms[0].columns.value + '&ffilename=' + document.forms[0].ffilename.value + '&ffilesize=' + document.forms[0].ffilesize.value + '&fdimensions=' + document.forms[0].fdimensions.value + '&fdate=' + document.forms[0].fdate.value + '&ffiletype=' + document.forms[0].ffiletype.value + '&asset_array=' + arraylist,'ContactSheet',1024,768);
			
		} else
			alert('Nothing selected');
			return false;
		{
	
	}
}
  
  function redirectOutput(myForm) {
		//var w = window.open('ContactSheet.aspx?uid=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.UserID.tostring%>&Heading=' + document.forms[0].Heading.value + '&columns=' + document.forms[0].columns.value + '&ffilename=' + document.forms[0].ffilename.value + '&ffilesize=' + document.forms[0].ffilesize.value + '&fdimensions=' + document.forms[0].fdimensions.value + '&fdate=' + document.forms[0].fdate.value + '&ffiletype=' + document.forms[0].ffiletype.value + '&asset_array=' + document.forms[0].asset_array.value,'ContactSheet','width=1024,height=768,directories=no,location=no,menubar=no,center=yes,scrollbars=yes,resizable=yes,toolbar=no');
		//myForm.target = 'ContactSheet';
		myForm.action = 'ContactSheet.aspx?sort_id=&id=<%=request.querystring("id")%>&uid=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.UserID.tostring%>&Heading=' + document.forms[0].Heading.value + '&columns=' + document.forms[0].columns.value + '&ffilename=' + document.forms[0].ffilename.value + '&ffilesize=' + document.forms[0].ffilesize.value + '&fdimensions=' + document.forms[0].fdimensions.value + '&fdate=' + document.forms[0].fdate.value + '&ffiletype=' + document.forms[0].ffiletype.value + '&asset_array=' + document.forms[0].asset_array.value 
		myForm.target = '_Blank';
		myForm.submit();
		myForm.action = '';
		myForm.target = '';
		return true;
	}
  
function viewall()
{
<%If bThumbnailView Then%>

 <%else%>
 <%response.write( GridAssets.ClientId)%>.set_callbackParameter('viewall');
<%response.write( GridAssets.ClientId)%>.callback();
<%response.write( GridAssets.ClientId)%>.set_pageSize(1000);
 setTimeout("showCustomFooter();", 1000);
 <%end if%>
}

function executeSortCallback()
{
//<%response.write( GridAssets.ClientId)%>.set_callbackParameter('customsort');
//<%response.write( GridAssets.ClientId)%>.callback();
var sortColumn = <%response.write( GridAssets.ClientId)%>.get_table().get_columns()[1];
alert(sortColumn.get_visible());
sortColumn.set_visible(true);
alert(sortColumn.get_visible());
<%response.write( GridAssets.ClientId)%>.render();
}

function DownloadCarousel(id)
  {  
  var answer = false;
<%  If WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("DOWNLOAD_ASSOCIATED_CAROUSEL") Then %>
answer =   confirm ("Do you want to include associated assets?");
<%end if%>
<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('carousel',id, answer, '<%=Server.UrlDecode(_carouselName)%>')

   }	


function GridAssets_onItemExternalDrop(sender, eventArgs)
{
//check for multi move

  var draggedItem = eventArgs.get_item();
  var draggedItemValue;
  var targetControl = eventArgs.get_targetControl();
  var target = eventArgs.get_target();
draggedItemValue = draggedItem.Id;
<%response.write( GridAssets.ClientId)%>.Filter("SORT," + draggedItemValue + ',' + target.GetMember("s_value").Value);
<%response.write( GridAssets.ClientId)%>.sort(1,true);
return true;
	
} 


function setPageSize(ps)
{
	SetCookie ('IDAMassetgridPageSize',ps, exp);
	__doPostBack('__Page', '');
}


</script>
<script language=javascript >


function UDFTabCallback(id)
{
<%Response.Write(CallbackUDFTab.ClientID)%>.Callback(id);
}

function popupusers()
{

ShowDialogWindowX(700,700,700,'UserLookup.aspx?x=' + urlhelper,'Select Users','contentCssKeyword','iFrameCss',true);

}

function populateparentfields(rowdata)
{
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtagency').value=rowdata.GetMember('agency').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtfirstname').value=rowdata.GetMember('firstname').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtlastname').value=rowdata.GetMember('lastname').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtmaddress1').value=rowdata.GetMember('workaddress').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtmaddress2').value=rowdata.GetMember('workaddress2').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtmstate').value=rowdata.GetMember('workstate').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtmzip').value=rowdata.GetMember('workzip').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtmcity').value=rowdata.GetMember('workcity').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtmcountry').value=rowdata.GetMember('workcountry').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtbaddress1').value=rowdata.GetMember('workaddress').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtbaddress2').value=rowdata.GetMember('workaddress2').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtbstate').value=rowdata.GetMember('workstate').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtbzip').value=rowdata.GetMember('workzip').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtbcity').value=rowdata.GetMember('workcity').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtbcountry').value=rowdata.GetMember('workcountry').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtphone1').value=rowdata.GetMember('phone').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtfax').value=rowdata.GetMember('fax').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtemail').value=rowdata.GetMember('email').Value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtuserid').value=rowdata.GetMember('userid').Value;	
}

function copyfrommailing()
{
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtbaddress1').value=document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtmaddress1').value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtbaddress2').value=document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtmaddress2').value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtbstate').value=document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtmstate').value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtbzip').value=document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtmzip').value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtbcity').value=document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtmcity').value;
	document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtbcountry').value=document.getElementById('<%response.write(SNAPCUSTOMOrderInfoMain.ClientID)%>_txtmcountry').value;
}


function Object_PopUp(strUrl, popupName, intHeight, intWidth)
{


	if (strUrl == null || strUrl.Length <= 0)
		return;

	var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=yes,resizable=yes,toolbar=no";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;
	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures , false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
}



</script>

















