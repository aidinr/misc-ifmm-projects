<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.UploadExpress" CodeBehind="UploadExpress.aspx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>IDAMExpress</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet" />
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet" />
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet" />
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet" />
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet" />
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet" />
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet" />
		<link href="treeStyle.css" type="text/css" rel="stylesheet" />
		<link href="navStyle.css" type="text/css" rel="stylesheet"  />
		<link href="tabStripStyle.css" type="text/css" rel="stylesheet" />    
		<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
		<link href="calendarStyle.css" type="text/css" rel="stylesheet" />
										<style>BODY { MARGIN: 0px }
	</style>
	<body onload="this.focus">
		<form id="Form" method="post" runat="server">
			<script type="text/javascript">
  
   
 function onPickerChange(picker)
      {
        <% Response.Write(Calendar1.ClientID) %>.SetSelectedDate(picker.GetSelectedDate());
      }
      function onPickerMouseUp()
      { 
        if (<% Response.Write(Calendar1.ClientID) %>.PopUpObjectShowing)
        {
          event.cancelBubble=true;
          event.returnValue=false;
          return false;
        }
        else
        {
          return true;
        }
      }
      function OkClick()
      {
        <% Response.Write(Picker1.ClientID) %>.SetSelectedDate(<% Response.Write(Calendar1.ClientID) %>.GetSelectedDate());
        <% Response.Write(Calendar1.ClientID) %>.Hide();
      }
      function CancelClick()
      {
        <% Response.Write(Calendar1.ClientID) %>.Hide();
      }

 
 
  
  
  
  
  
  
  
   
   

    <asp:literal id="ErrorLiteral" runat="server"/>
  
			</script>
			
			
			
			
			
			
			
			
			
			
		
	
			
			
		
<div id=emaillookpupdropdown style="BORDER-RIGHT:1px solid; BORDER-TOP:1px solid; DISPLAY:none; Z-INDEX:999; BORDER-LEFT:1px solid; WIDTH:350px; PADDING-TOP:1px; BORDER-BOTTOM:1px solid; POSITION:absolute; HEIGHT:300px; BACKGROUND-COLOR:white;"> 
<div><input type=button onclick="javascript:emaillookpupdropdown.style.display='none';" value=close title=close></div>
	<COMPONENTART:GRID id="GridEmailList" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true"
	ClientSideOnSelect="selectemaillistfromdropdown" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true"
	ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/"
	ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip"
	ScrollPopupClientTemplateId="ScrollPopupTemplate" Height="260px" Width="350px" LoadingPanelPosition="TopCenter"
	LoadingPanelClientTemplateId="LoadingFeedbackTemplateUpload" EnableViewState="true" GroupBySortImageHeight="10"
	GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif"
	GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategoryDropdown"
	IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/"
	PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="true" GroupingPageSize="1"
	PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText"
	GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderDropdown" SearchOnKeyPress="true"
	SearchTextCssClass="GridHeaderText" AllowEditing="False" AllowSorting="False" ShowSearchBox="false"
	ShowHeader="false" ShowFooter="true" CssClass="GridDropdown" RunningMode="callback" ScrollBarWidth="15"
	AllowPaging="true" >
	<ClientTemplates>
		<componentart:ClientTemplate ID="LoadingFeedbackTemplateUpload">
			<div style="height:150px;"><table cellspacing="0" height=150 cellpadding="0" border="0">
					<tr>
						<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height="25" width="1"><br>
							<img src="images/spinner.gif" width="16" height="16" border="0"> Loading...
						</td>
					</tr>
				</table>
			</div>
		</componentart:ClientTemplate>
		<ComponentArt:ClientTemplate Id="ViewEmailListTemplate">
	<span>## DataItem.GetMember("firstname").Value ## ## DataItem.GetMember("lastname").Value ## <b>(## DataItem.GetMember("email").Value ##)</b></span>
	</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="EditCommandTemplateUpload">
			<a href="javascript:editRow();">Update</a>
		</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="InsertCommandTemplateUpload">
			<a href="javascript:insertRow();">Insert</a>
		</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="TypeIconTemplateUpload">
			<img src="images/projcat16.gif" border="0">
		</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="LookupProjectTemplateUpload">
			<A href="IDAM.aspx?page=Project&ID=## DataItem.GetMember("userid").Value ##&type=project"><img border="0" src="images/goto.gif"></A>
		</ComponentArt:ClientTemplate>
	</ClientTemplates>
	<Levels>
		<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateUpload" InsertCommandClientTemplateId="InsertCommandTemplateUpload" DataKeyField="userid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellTextDropdown" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssetsDropdown" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssetsDropdown" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssetsDropdown" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActiveDropdown" ShowTableHeading="False" ShowHeadingCells="False">
			<Columns>
				<componentart:GridColumn AllowEditing="False" Width="250" AllowGrouping="False" DataCellClientTemplateId="ViewEmailListTemplate" SortedDataCellCssClass="SortedDataCell" DataField="email" DataCellCssClass="FirstDataCellPostings"></componentart:GridColumn>
				<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="userid"></componentart:GridColumn>
				<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="email"></componentart:GridColumn>
				<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="firstname"></componentart:GridColumn>
				<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="lastname"></componentart:GridColumn>
			</Columns>
		</componentart:GridLevel>
	</Levels>
	</COMPONENTART:GRID>
	
</div>	
			
			
			
			
			
			
			
			
			
			
			
			<%response.write (request.querystring("newid"))%>
			<input type="hidden" name="catid" id="catid" value="<%response.write (replace(request.querystring("p"),"CAT",""))%>">
			<div style="padding:20px;position:relative;">
			<div style="100%">
			<div style="padding:10px;border:1px solid;background-color:white;position:relative;">

			
  <div style="width=100%">
<div style="padding:5px;padding-top:15px;padding-bottom:15px;border:0px solid #B7B4B4;  text-align:center;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;margin-bottom:3px;
  font-weight: normal;">Upload location:
			
			
<!--GroupBy="categoryname ASC"-->
<div id="side_results" style="width: 100%; border:0px solid #B7B4B4;" > 
Project: [ <b><asp:label id="LabelProjectName" runat="server">Label</asp:label></b> ]<br>
Folder:  [ <b><asp:label id="LabelCategoryName" runat="server">Label</asp:label></b> ]<br>
</div>
	
		</div>	
			<div style="display:none;padding:5px;border:0px solid #B7B4B4;  font-family: verdana; color:
			 #3F3F3F; font-size: 10px;  font-weight: normal;"></div>
			
			
		<!--	
	<COMPONENTART:TABSTRIP id="TabStripCarousel" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataIDAMExpress.xml"
		CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
		DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
		<ItemLooks>
			<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10" 
 LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif" 
 HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23" 
 LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
			<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10" 
 LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif" 
 LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
		</ItemLooks>
	</COMPONENTART:TABSTRIP>-->
	
	<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageCategoryPage" runat="server"> 
		
		
		
		
		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="IDAMExpress">

<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">IDAMExpress upload is a great way to request files from an individual who may not have an IDAM user account.  Users will receive an email with a link to a secure location on the web to upload their file(s).  Once uploaded, the files will simply show up in the location displayed above with a notification to you when they have arrived.<br><br>Enter the recipient(s) email address in the location below to invite a user to upload file(s).</div>
			<br>
	

            
            
            <table cellspacing="0" cellpadding="3" border="0" width="100%">
             
				<tr>
					<td width="120" align="left" valign="top" nowrap>
					
						 
						
					</td>
					<td align="left" valign="top" width="100%">
					<font face="Verdana" size="1">							

							<asp:Literal Runat=server ID=emailconfirmation></asp:Literal><asp:Literal Runat=server ID=emailhistory></asp:Literal>
							<div id="showprogress" style="display:none">
							<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD align="center">
										<TABLE cellSpacing="0" cellPadding="0" border="0">
											<TR>
												<TD style="FONT-SIZE: 10px">Processing IDAMExpress Delivery...Please wait. 
												</TD>
												<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
							</div>	
					</font>
					</td>
				</tr>
			</table>
            
            
            
            
            
             <table cellspacing="0" cellpadding="3" border="0" width="100%">
             
				<tr>
					<td width="120" align="left" valign="top" nowrap>
						<b>
						<font face="Verdana" size="1">Sent To: * </font>
						</b>
					</td>
					<td align="left" valign="top" width="100%">
						<asp:TextBox id="Emaillist" TextMode="MultiLine" runat="server" style="height:50px" Height="50px" CssClass="InputFieldMain100P" ></asp:TextBox><br><font face="Verdana" size="1">*Seperate multiple email addresses with a semi colon (;).</font></td>
					
			
				</tr>
            
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<b>
						<font face="Verdana" size="1">Subject:</font></b></td>
					<td class="PageContent" align="left" valign="top"  width="100%">
 <font size="1" face="Arial">
            
						<asp:TextBox id="EmailSubject" runat="server" style="height:25px" Height="25px" CssClass="InputFieldMain100P" ></asp:TextBox>

     
</font>    </td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<b>
						<font face="Verdana" size="1">Message:</font></b></td>
					<td class="PageContent" align="left" valign="top"  width="100%">
 <font size="1" face="Arial">            
						<asp:TextBox id="EmailMessage"  TextMode="MultiLine" runat="server" style="height:160px" Height="160px" CssClass="InputFieldMain100P" ></asp:TextBox></font>
						</td>
				</tr>	
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<b><font face="Verdana" size="1">Options</font></b></td>
					<td class="PageContent" align="left" valign="top"  width="100%"><hr noshade size=1>    </td>
				</tr>	
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<b><font face="Verdana" size="1">Expiration Date:</font></b>(optional) </td>
					<td class="PageContent" align="left" valign="top"  width="100%">
				 <table onmouseup="onPickerMouseUp()" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td><ComponentArt:Calendar id="Picker1" 
          ControlType="Picker" 
          ClientSideOnSelectionChanged="onPickerChange" 
          PickerFormat="Custom" 
          PickerCustomFormat="MMMM d yyyy" 
          PickerCssClass="pickerForm" 
          runat="server" 
          /></td>
      <td style="font-size:10px;">&nbsp;</td>
      <td><img id="calendar_button" alt="" onclick="<% Response.Write(Calendar1.ClientID) %>.SetSelectedDate(<% Response.Write(Picker1.ClientID) %>.GetSelectedDate());<% Response.Write(Calendar1.ClientID) %>.Show();" class="calendar_button" src="images/btn_calendar.gif" width="25" height="22" /></td>
    </tr>
    </table>

    <ComponentArt:Calendar runat="server"
      id="Calendar1"
      AllowMonthSelection="false"
      AllowMultipleSelection="false"
      AllowWeekSelection="false"
      CalendarCssClass="calendar"
      CollapseOnSelect="false"
      ControlType="Calendar"
      DayCssClass="day"
      DayHeaderCssClass="dayheader"
      DayHoverCssClass="dayhover"
      DayNameFormat="FirstTwoLetters"
      OtherMonthDayCssClass="othermonthday"
      OtherMonthDayHoverCssClass="othermonthdayhover"
      PopUp="Custom"
      PopUpExpandControlId="calendar_button"
      SelectedDayCssClass="selectedday" 
      SelectedDayHoverCssClass="selecteddayhover"
      ShowCalendarTitle="false"
      SwapDuration="300"
      SwapSlide="Linear"      
      Width="200"
      >
      <HeaderClientTemplate>
        <div class="header">
          <span style="float:left;">
            <span class="headerbutton" onclick="Calendar1.GoPrevYear()"><img src="images/i_closed.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoPrevMonth()"><img src="images/cal_prevMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoToday()"><img src="images/bullet.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoNextMonth()"><img src="images/cal_nextMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoNextYear()"><img src="images/i_open.gif"></span>
          </span>
          <span style="float:right;">##Calendar1.FormatDate(Calendar1.VisibleDate,"MMMM yyyy")##</span>
        </div>
      </HeaderClientTemplate>
      <FooterClientTemplate>
        <div class="footer">
          <span style="float:right;">
            <input type="button" value="&nbsp; Ok &nbsp;" onclick="OkClick()" class="button" />
            <input type="button" value="Cancel" onclick="CancelClick()" class="button" />
          </span>
        </div>
      </FooterClientTemplate>
    </ComponentArt:Calendar>
				</td> 
				</tr>			
				
				
				
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<b>
						<font face="Verdana" size="1">Password:</font></b>(optional) </td>
					<td class="PageContent" align="left" valign="top"  width="100%">
 <font size="1" face="Arial">
            
						 <asp:TextBox ID="uploadPassword" TextMode="SingleLine" Width="100" MaxLength="20" CssClass="InputFieldMain100P" Runat="server"></asp:TextBox>  
						 <div style="padding:5px;padding-left:0px;  font-family: verdana; color: #3F3F3F; font-size: 10px;font-weight: normal;">
						 
						<br>
						
						<asp:CheckBox ID="chkNotify1" Runat=server Checked=False></asp:CheckBox><font face="Verdana" size="1"> Notify me when the files are uploaded</font><br>
						<asp:CheckBox ID="chkNotify2" Runat=server Checked=False></asp:CheckBox><font face="Verdana" size="1"> Notify me on all events</font><br>
						</div>
						<asp:Button ID="btnsendmail" Runat=server  Text="Send IDAMExpress Upload Request"></asp:Button>
						
     
</font>    </td>

				</tr>					
<tr> 
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
					
						</td>
					<td class="PageContent" align="left" valign="top"  width="100%">
 </td>
				</tr>					
 
								
			</table>
			
</font>
	</ComponentArt:PageView>



		
		


		
	</ComponentArt:MultiPage>
	<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
					<div style="text-align:right;"><br>
			<input type="button" onclick="window.parent.NavigateToCarousel();window.close();" value="Close" />
			</div>
	</div>
</div>

			
			
			
			

			</div>
			
			
			
			
			
			
			
			
			</div>
			

			
			</div>
			</div>
			
		
			
			
		</form>



<script language="JavaScript" type="text/JavaScript">




function MM_findObj(n, d) { //v3.0
var p,i,x; if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}


function CheckAll(fieldName,Num) { //Set All checkboxes, if un-checked, check them.
if (MM_findObj(fieldName).length > 1) { 
var checks = MM_findObj(fieldName)
var bcheck = MM_findObj(fieldName)[1].checked
if (bcheck) {
bcheck = false;
} else {
bcheck = true;
}
for (i=0; i<checks.length; i++) {
MM_findObj(fieldName)[i].checked = bcheck ; 
}

}
}


  
function openpopup(assetid,x,y){
var popurl= "image_pop.asp?a=" + assetid + "&c=<%=request.querystring("c")%>" + "&p=<%=request.querystring("p")%>"
var xwidth = x
var yheight = y
winpops=window.open(popurl,"","width=" + xwidth + ",height=" + yheight + ",location=no,")
}

function CheckAllItemsThumbs() { //Set All checkboxes, if un-checked, check them.
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
var dummyobjectvalue = checks[0].value;
var firstobjectcheckedstate = document.getElementById('aselect_' + dummyobjectvalue);
var bcheck = firstobjectcheckedstate.checked

if (bcheck) {
bcheck = true;
} else {
bcheck = false;
}

for (i=0; i<checks.length; i++) {
MM_findObj('aselect_' + checks[i].value).checked = bcheck ;
highlightAssetToggleII(document.getElementById('aselect_' + checks[i].value),checks[i].value); 
}

}
}


function ShowSlideShow(){
	var response = confirm('Show slideshow fullscreen?');
	if (response) {
		window.open("./slideshow/slideshow.asp?car=<%=Request.QueryString("Carousel_id")%>&uid=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.UserID.tostring%>&usl=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.userAccess.ToString%>","","width=1024,height=800,fullscreen=yes,location=no,");
	} else {
		window.open("./slideshow/slideshow.asp?car=<%=Request.QueryString("Carousel_id")%>&uid=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.UserID.tostring%>&usl=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.userAccess.ToString%>","","width=1024,height=800,location=no,");
	}
}



 





 
function showMenu(id, eventObj) {

    var elmobj = document.getElementById(id);
    var xoffsettype = 260;
   	if (id!='emaillookpupdropdown')
	{
	xoffsettype += 35;
	//hide others
	document.getElementById('emaillookpupdropdown').style.display='none';
	} else
	{
	//document.getElementById('categorylookpupdropdown').style.display='none';
	}
	ypos=260;
	xpos=162;
    elmobj.style.posLeft=xpos+"px";
    elmobj.style.posTop=ypos+"px";
    elmobj.style.left=xpos+"px";
    elmobj.style.top=ypos+"px";   
    
    
 
    
    if (elmobj.style.display=='block')
		{
		if (eventObj.type!='keyup'){
		    elmobj.style.display='none';
		    }
		} else {
			elmobj.style.display='block';
		}
}



  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

function showemaillookupforce(){

var elmprojectlookpupdropdown = document.getElementById('emaillookpupdropdown')
//elmprojectlookpupdropdown.style.display='block';
//document.getElementById('_ctl0_SNAPUploadWizard_project_id').value='';

if (document.getElementById('Emaillist').value.length!=1) {



	var existinglist = document.getElementById('Emaillist').value;
	var searchstring;
	if(existinglist.lastIndexOf(";")>1){
	//check to see if starting 
		searchstring=LTrim(Right(existinglist,existinglist.length-existinglist.lastIndexOf(";")-1));
	}
	else {
	//assume first entry - replace
	searchstring=existinglist;
	}
	//alert(searchstring);
GridEmailList.Filter('email LIKE \'%' + searchstring + '%\' OR firstname like  \'%' + searchstring + '%\' OR lastname like  \'%' + searchstring + '%\'');
}
}

// Removes leading whitespaces
function LTrim( value ) {
	
	var re = /\s*((\S+\s*)*)/;
	return value.replace(re, "$1");
	
}

// Removes ending whitespaces
function RTrim( value ) {
	
	var re = /((\s*\S+)*)\s*/;
	return value.replace(re, "$1");
	
}

// Removes leading and ending whitespaces
function trim( value ) {
	
	return LTrim(RTrim(value));
	
}


function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}

function Left(str, n){
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}




	
function selectemaillistfromdropdown(item)
  {
    if (document.getElementById('emaillookpupdropdown').style.display=='none'){return;} 
	var itemvaluetmp;
	var existinglist;
	itemvaluetmp = item.GetMember('userid').Value;
	itemvaluenametmp = item.GetMember('email').Value;
	existinglist = document.getElementById('Emaillist').value;
	if(existinglist.lastIndexOf(";")>1){
	//check to see if starting 
		document.getElementById('Emaillist').value=Left(existinglist,existinglist.lastIndexOf(";")+1)+itemvaluenametmp+'; ';
	}
	else {
	//assume first entry - replace
	document.getElementById('Emaillist').value=itemvaluenametmp+'; ';
	}
	//alert(document.getElementById('Emaillist').value.substring(document.getElementById('Emaillist').value.indexOf(";")+1, document.getElementById('Emaillist').value.lastIndexOf(";")));

	//document.getElementById('Emaillist').value=document.getElementById('Emaillist').value+' '+itemvaluenametmp+';';

	//showprojectlookup();
	document.getElementById('emaillookpupdropdown').style.display='none';
	return true;
  }  

</script>




 














		
	</body>
</HTML>


 
