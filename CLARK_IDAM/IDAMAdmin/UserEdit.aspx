<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.UserEdit"  ValidateRequest="False" CodeBehind="UserEdit.aspx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>User</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
		<link href="treeStyle.css" type="text/css" rel="stylesheet">
		<link href="navStyle.css" type="text/css" rel="stylesheet">
		<link href="tabStripStyle.css" type="text/css" rel="stylesheet">
		<link href="navBarStyle.css" type="text/css" rel="stylesheet">
		<style>BODY { MARGIN: 0px }
	</style>
</HEAD>
	<body onload="this.focus">
		<form id="Form" method="post" runat="server">
		<SCRIPT LANGUAGE="JavaScript" SRC="js/CalendarPopup.js"></SCRIPT>
			<script type="text/javascript">
  
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridKeywords.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    GridKeywords.Edit(GridKeywords.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    GridKeywords.EditComplete();     
  }

  function insertRow()
  {
    GridKeywords.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    GridKeywords.Delete(GridKeywords.GetRowFromClientId(rowId)); 
  }

<%if request.querystring("newid") <> "" then%>
//window.opener.NavigateToPage('User');
//window.close();
<%end if%>


			</script>
			<%'response.write (request.querystring('newid'))%>
			<input type="hidden" name="catid" id="catid" value="<%'response.write (replace(request.querystring('p'),'CAT',''))%>">
			<div  style="PADDING-RIGHT:20px;PADDING-LEFT:20px;PADDING-BOTTOM:20px;PADDING-TOP:20px;POSITION:relative">
				<div>
					<div style="BORDER-RIGHT:1px solid; PADDING-RIGHT:10px; BORDER-TOP:1px solid; PADDING-LEFT:10px; PADDING-BOTTOM:10px; BORDER-LEFT:1px solid; PADDING-TOP:10px; BORDER-BOTTOM:1px solid; POSITION:relative; BACKGROUND-COLOR:white">
						<table>
							<tr>
								<td><img src="images/user16.gif"></td>
								<td>Create/Edit User</td>
							</tr>
						</table>
						<div style="BORDER-RIGHT:#b7b4b4 1px solid; PADDING-RIGHT:5px; BORDER-TOP:#b7b4b4 1px solid; PADDING-LEFT:5px; FONT-WEIGHT:normal; FONT-SIZE:10px; PADDING-BOTTOM:5px; BORDER-LEFT:#b7b4b4 1px solid; COLOR:#3f3f3f; PADDING-TOP:5px; BORDER-BOTTOM:#b7b4b4 1px solid; FONT-FAMILY:verdana; BACKGROUND-COLOR:#e4e4e4">Create 
							a user by entering a user name and description.</div>
						<br>
						<div style="WIDTH: 100%">
							<COMPONENTART:TABSTRIP id="TabStripCategory" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataUser.xml"
								CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
								DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
								<ItemLooks>
								<ComponentArt:ItemLook LookId="ScrollItem" CssClass="ScrollItem" HoverCssClass="ScrollItemHover" LabelPaddingLeft="5" LabelPaddingRight="5" LabelPaddingTop="0" LabelPaddingBottom="0" />
									<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />	
									<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />											</ItemLooks>
							</COMPONENTART:TABSTRIP>
							<div class="subrightsideplaceholderdiv">	
									<div class="subrightsideplaceholderdivheading"></div>
									<ComponentArt:MultiPage id="MultiPageCategory" CssClass="subMultiPageUserPage" runat="server">
							
							
							
							
							
							
			
								<ComponentArt:PageView CssClass="PageContent2" runat="server" ID="User_General">
<DIV style="BORDER-RIGHT: #b7b4b4 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #b7b4b4 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: normal; FONT-SIZE: 10px; PADDING-BOTTOM: 5px; BORDER-LEFT: #b7b4b4 1px solid; COLOR: #3f3f3f; PADDING-TOP: 5px; BORDER-BOTTOM: #b7b4b4 1px solid; FONT-FAMILY: verdana; BACKGROUND-COLOR: #e4e4e4">Enter 
										a name, description and security level for this user.</DIV>
									<BR>
									<FONT face="Verdana" size="1" color=Red><b><%
									if sError = "1" then
										response.write ("Please use a unique login or username.")
									end if
									%></b></font>
									<TABLE id="Table1" cellSpacing="0" cellPadding="3" width="100%" border="0">
									<TR>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">User Name
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="215">
												<asp:TextBox id="username" runat="server" CssClass="InputFieldMain" Height=25px ></asp:TextBox></TD>
											<TD vAlign="top" align="right" width="400">
												<FONT face="Verdana" size="1">Use LDAP Authentication</FONT>
											</TD>
											<TD vAlign="top" align="left" width="100%">
												<asp:CheckBox id="ldapauthentication" runat="server"></asp:CheckBox></TD>
										</TR>
									<TR><TD vAlign="top" noWrap align="left" width="120">
											</TD>
											<TD vAlign="top" noWrap align="left"  width="100%" colspan="3"><FONT face="Verdana" size="1">The user name should be fully qualified<br>Windows user name: DOMAIN\USERNAME<br>LDAP user name: cn=USERNAME,ou=ORG_UNIT,o=ORG</font></TD>
										</TR>	
										<TR>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Login
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="100%" colspan="3">
												<asp:TextBox id="idamlogin" runat="server" CssClass="InputFieldMain" Height=25px ></asp:TextBox></TD>
										</TR>
										<TR>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Password
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="100%" colspan="3">
												<input type=password autocomplete=off class=InputFieldMain height=25px id=idampassword name=idampassword value="<%response.write(trim(rpPassword))%>"> &nbsp;<asp:Button id="bntClearPassword" TabIndex="5" runat="server" Text="Clear Password"></asp:Button>
												
										</TR>
										<TR>
											<TD class="PageContent" vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Security Level</FONT></TD>
											<TD class="PageContent" vAlign="top" align="left" width="100%" colspan="3">
												<asp:DropDownList id="SecurityLevel" Runat="server" CssClass="InputFieldMain" Height=25px >
													<asp:ListItem Value="0">Administrator</asp:ListItem>
													<asp:ListItem Value="1">Internal Use</asp:ListItem>
													<asp:ListItem Value="2">Client Use</asp:ListItem>
													<asp:ListItem Value="3">Public</asp:ListItem>
												</asp:DropDownList></TD>
										</TR>
										<TR>
											<TD class="PageContent" vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">User Type</FONT></TD>
											<TD class="PageContent" vAlign="top" align="left" width="100%" colspan="3">
												<asp:DropDownList id="usertype" Runat="server" CssClass="InputFieldMain" Height=25px >
													<asp:ListItem Value="">Select a user type</asp:ListItem>
													<asp:ListItem Value="0">User</asp:ListItem>
													<asp:ListItem Value="1">Contact</asp:ListItem>
												</asp:DropDownList></TD>
										</TR>	
										<TR>
											<TD class="PageContent" vAlign="top" noWrap align="left" width="120">
												<FONT face="Verdana" size="1">Description</FONT></TD>
											<TD class="PageContent" vAlign="top" align="left" width="180%" colspan="3">
												<asp:TextBox id="Description" runat="server" CssClass="InputFieldMain100P" Height=100px  TextMode="MultiLine"></asp:TextBox></TD>
										</TR>									
										</table>
										<table border="0" width="100%" id="table2">
										<tr>
											<td width="88"><b><font face="Verdana" size="1">User Info</font></b></td>
											<td><hr>
											</td>
										</tr>
									</table>
									<TABLE id="Table1" cellSpacing="0" cellPadding="3" width="100%" border="0">
										<TR>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">First Name
											<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage="[!]" ControlToValidate="firstname"></asp:RequiredFieldValidator></FONT>
											</TD>
											<TD vAlign="top" align="left" width="100%" colspan="3">
												<asp:TextBox id="firstname" runat="server" CssClass="InputFieldMain" Height=25px ></asp:TextBox></TD>
										</TR>
										<TR>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Last Name
											<asp:RequiredFieldValidator id="Requiredfieldvalidator4" runat="server" ErrorMessage="[!]" ControlToValidate="lastname"></asp:RequiredFieldValidator></FONT>
											</TD>
											<TD vAlign="top" align="left" width="13%" >
												<asp:TextBox  id="lastname" runat="server" CssClass="InputFieldMain" Height=25px ></asp:TextBox></TD>
												<TD vAlign="top" align="right" width="17%">
												<FONT face="Verdana" size="1">Employee#
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="61%">
												<asp:TextBox id="employeenumber" runat="server" CssClass="InputFieldMain" Height=25px ></asp:TextBox></TD>
										</TR>		
										<tr>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Email
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="13%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px  ID="email"></asp:TextBox>
											</TD>
											<TD vAlign="top" align="right" width="17%">
												<FONT face="Verdana" size="1">Company
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="61%">
												<asp:TextBox id="company" runat="server" CssClass="InputFieldMain" Height=25px ></asp:TextBox></TD>
										</tr>
										<tr>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Department
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="13%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px  ID="Department"></asp:TextBox>
											</TD>
											<TD vAlign="top" align="right" width="17%">
												<FONT face="Verdana" size="1">
												Phone
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="61%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px  ID="Phone"></asp:TextBox>
											</TD>
										</tr>
										<tr>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Title
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="13%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px  ID="Title"></asp:TextBox>
											</TD>
											<TD vAlign="top" align="right" width="17%">
												<FONT face="Verdana" size="1">Cell Phone
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="61%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px  ID="CellPhone"></asp:TextBox>
											</TD>
										</tr>
										<tr>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Work Address
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="13%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px  ID="WorkAddress"></asp:TextBox>
											</TD>
											<TD vAlign="top" align="right" width="17%">
												<FONT face="Verdana" size="1">City
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="61%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px  ID="City"></asp:TextBox>
											</TD>
										</tr>
										<TR>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">State
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="13%">
												<asp:DropDownList DataTextField="name" DataValueField="state_id" id="state" CssClass="InputFieldMain" Height=25px  runat="server"></asp:DropDownList>
											</TD>
											<TD vAlign="top" align="right" width="17%">
												<FONT face="Verdana" size="1">Zip
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="61%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px  ID="Zip"></asp:TextBox>
											</TD>
										</TR>	
																				
									</TABLE>							
									
								</ComponentArt:PageView>
							
								
								
								
								
								
			
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="User_Image">
									<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
									font-size: 10px;
									font-weight: normal;">Modify the user image.</div>
									<br>
									<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px"><!--padding-->							
<table border="0" width="100%" id="">
	<tr>
		<td width=200 align="left" valign="top">
		<div style="padding:5px;border:1px dotted #E0DCDC;">
		<img border="0" src="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?id=<%response.write(request.querystring("Id"))%>&instance=<%response.write(IDAMInstance)%>&type=contact&size=1&width=200&height=200&cache=1"></div>
		<br>
		<font face=verdana size=1>
		        Upload Image:<br>
        </font>
 <font face="Arial" size="2">
		<Upload:InputFile id="thefile" Class="InputFieldFileMain" name="thefile" size="14" runat="server" /></font>
		 

		
		
		
		
		</td>
		<td align="left" valign="top"></td>
	</tr>
	<tr>
		<td colspan="2" align="left" valign="top"></td>
	</tr>
</table>
<br>
<input type='button' Class="InputButtonMain" onclick="javascript:idamaction.value='editUserDetails';Form.submit();return true;"  width=40 value="Save" id="upload" name="upload">
</div><!--padding-->
<input type="hidden" name="idamaction" id="idamaction" value="">
								</ComponentArt:PageView>
								
								
								
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="User_UDF">
									<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
									font-size: 10px;
									font-weight: normal;">Modify the user defined fields.  Note; if tabs are available, please be sure to save your work in between navigating to new tabs.</div><br>
								<COMPONENTART:TABSTRIP id="UDFTabStrip" width="100%" runat="server" ImagesBaseUrl="images/" ScrollingEnabled="true"
								CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
								DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" ScrollLeftLookId="ScrollItem" ScrollRightLookId="ScrollItem">
								<ItemLooks>
									<ComponentArt:ItemLook LookId="ScrollItem" CssClass="ScrollItem" HoverCssClass="ScrollItemHover" LabelPaddingLeft="5" LabelPaddingRight="5" LabelPaddingTop="0" LabelPaddingBottom="0" />
									<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />	
									<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />																																																																		
								</ItemLooks>
								
								</COMPONENTART:TABSTRIP>
								<div class="subrightsideplaceholderdiv">	
									<div class="subrightsideplaceholderdivheading">
									</div>
										<ComponentArt:MultiPage id="Multipage1" CssClass="subMultiPageUserPage"   runat="server">
										<ComponentArt:PageView CssClass="PageContent2" runat="server" ID="Pageview3">
											<COMPONENTART:CALLBACK id="CallbackUDFTab" runat="server" CacheContent="false" >
											<CONTENT>	
											<asp:Literal ID="Literal_UDFMAIN" Runat=server></asp:Literal>	
											<asp:PlaceHolder id="PlaceHolder_UDFEditor" runat="server"></asp:PlaceHolder>									
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
											</LOADINGPANELCLIENTTEMPLATE>
											</COMPONENTART:CALLBACK>
										</ComponentArt:PageView>
										</ComponentArt:MultiPage>
								</div>
								
						
					
					
					
					
					
					
					</ComponentArt:PageView>
								
								
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="User_BIO">
									<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
									font-size: 10px;
									font-weight: normal;">Modify the user's bio.</div><br>
									<table width=100%>
									<TR>
											<TD class="PageContent" vAlign="top" noWrap align="left" width="120">
												<FONT face="Verdana" size="1">Biography</FONT></TD>
											<TD class="PageContent" vAlign="top" align="left" width="180%" colspan="3">
												<asp:TextBox id="bio" runat="server" CssClass="InputFieldMain100P" style="Height:400px"  TextMode="MultiLine"></asp:TextBox></TD>
										</TR>	
									</table>
									
									
								</ComponentArt:PageView>
								
								
								
								
								
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="User_Membership">
									<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
									font-size: 10px;
									font-weight: normal;">Modify the assignment to groups.</div><br>
									
								
								
								
								
						
																				
		
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td class="PageContent" width="360" valign=top>
<!--available list-->
<b>Available Groups</b><br><br>



<COMPONENTART:GRID id="GridPermissionAvailable" runat="server" pagerposition="2" ScrollBar="Off" Sort="lastname asc" Height="10" Width="250" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategory" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" AllowPaging="true" cachecontent="false" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategory">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategory">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                            
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategory" InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataCellClientTemplateId="TypeIconTemplateCategory" DataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Firstname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="firstname" Width="70"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Lastname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="lastname" Width="100"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" DataCellCssClass="LastDataCell" AllowEditing="false" AllowGrouping="False" Width="120" SortedDataCellCssClass="SortedDataCell" DataField="description"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<div style=  "text-align:right;BORDER-RIGHT: 1px solid; PADDING-RIGHT: 4px; BORDER-TOP: 1px solid; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: 1px solid; PADDING-TOP: 4px; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: gainsboro">
<b>Search for user/group:</b> <input Class="InputFieldMain" type=text value="" id=search_filter  style="width:120px" onkeyup="javascript:<%response.write( GridPermissionAvailable.ClientId)%>.Filter('firstname LIKE \'%' + this.value + '%\' or lastname LIKE \'%' + this.value + '%\'');">
</div>
					</td>
					<td class="PageContent" width="120" valign= middle>
		
		<asp:ImageButton ImageUrl="images/bt_u_fwd.gif" Runat=server id="imgbtnAddPermissions"></asp:ImageButton><br><br>
		<asp:ImageButton ImageUrl="images/bt_u_rwd.gif" Runat=server id="imgbtnRemovePermissions"></asp:ImageButton>
					
					</td>
					<td class="PageContent" width="100%" valign=top>
					
					
					
					
<!--active list-->
<b>Active Groups</b><br><br>


<COMPONENTART:GRID id="GridPermissionActive" runat="server" autoPostBackonDelete="true" PageSize="10" pagerposition="2" ScrollBar="Off" Sort="typeofobject asc" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategoryActive" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="false" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" AllowPaging="true" cachecontent="false" PagerStyle="Numbered"  >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategoryActive">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateCategoryActive">
          <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategoryActive">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                                
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategoryActive" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataField="imagesource" DataCellClientTemplateId="TypeIconTemplateCategoryActive" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True"  />
<componentart:GridColumn AllowEditing="false" HeadingText="Name" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" FixedWidth="false"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Description" DataCellCssClass="LastDataCell" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="description" width="100" FixedWidth="true"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
					</td>					
				</tr>
			</table>													
									

								</ComponentArt:PageView>
								
								
								
								
								
								
								
								
								
								
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="User_Assign_Roles">
									<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
									font-size: 10px;
									font-weight: normal;">Modify the user's assigned roles.</div><br>
									
Choose a Role Set<br>	
<table width=100%><tr>
<td width=300px><asp:DropDownList DataTextField="name"  DataValueField="role_id" id="Roles" style="width:250px"  runat="server"></asp:DropDownList></td>
<td width=100px><asp:Button id="Refresh" runat="server" Text="Refresh"></asp:Button></td>
<td width=100%><asp:Button id="Manage_Role_Sets" runat="server"  Text="Manage Role Sets"></asp:Button></td>
</tr></table>				
<br>


<script language=javascript>
function Object_PopUp(strUrl, popupName, intHeight, intWidth)
{


	if (strUrl == null || strUrl.Length <= 0)
		return;

	var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=no,resizable=yes,toolbar=no";
	//var strFeatures = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;
	//var strFeatures += "'"
	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures , false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
}


function Object_PopUp_Roles()
{
Object_PopUp('RolesEdit.aspx','Edit_Role',700,700);

}

function changerole()
{
if (confirm('Are you sure you want to change this users role?  This will erase any previous role overrides as well.')) {
__doPostBack('Roles','');
}
}

</script>
<table width=100%><tr>
<td width=100% style="FONT-SIZE: 11px; ">Role List</td>
<td width=200px align=right><asp:Button   id="roleOverrides" runat="server" Text="Clear Role Overrides"></asp:Button></td>
</tr></table>	
		<ComponentArt:TreeView id="TreeView_Roles" Height="380" Width="100%" 
		AutoPostBackOnNodeCheckChanged="true"
		DragAndDropEnabled="false" 
		NodeEditingEnabled="false" 
		KeyboardEnabled="true" 
		CssClass="TreeView" 
		NodeCssClass="TreeNode" 
		SelectedNodeCssClass="SelectedTreeNode" 
		HoverNodeCssClass="HoverTreeNode" 
		NodeEditCssClass="NodeEdit" 
		LineImageWidth="19" 
		LineImageHeight="20" 
		DefaultImageWidth="16" DefaultImageHeight="16" ItemSpacing="0" NodeLabelPadding="3" 
		ParentNodeImageUrl="images/folders.gif" LeafNodeImageUrl="images/folder.gif" ShowLines="true" 
		LineImagesFolderUrl="images/lines/" 
		EnableViewState="true"  
		runat="server" >
        </ComponentArt:TreeView>
									
									
								</ComponentArt:PageView>
								
								
								
								
								
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Pageview1">
									<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
									font-size: 10px;
									font-weight: normal;">Modify the user's manager approval status.  Select from the list of available users or groups from the left side (note: you can use the CTRL key to select more than one member) and click the forward button to apply to the list on the right.</div><br>
									
									
									
						
								
						
																				
		
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td class="PageContent" width="360" valign=top>
<!--available list-->
<b>Available Manager Groups/Users</b><br><br>


<COMPONENTART:GRID id="GridPermissionAvailableManagers" runat="server" pagerposition="2" ScrollBar="Off" Sort="lastname asc" Height="10" Width="250" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategoryAM" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" AllowPaging="true" cachecontent="false" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategoryAM">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategoryAM">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                            
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategoryAM" InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataCellClientTemplateId="TypeIconTemplateCategoryAM" DataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Firstname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="firstname" Width="70"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Lastname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="lastname" Width="100"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" DataCellCssClass="LastDataCell" AllowEditing="false" AllowGrouping="False" Width="120" SortedDataCellCssClass="SortedDataCell" DataField="description"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<div style=  "text-align:right;BORDER-RIGHT: 1px solid; PADDING-RIGHT: 4px; BORDER-TOP: 1px solid; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: 1px solid; PADDING-TOP: 4px; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: gainsboro">
<b>Search for user/group:</b> <input Class="InputFieldMain" type=text value="" id=search_filter  style="width:120px" onkeyup="javascript:<%response.write( GridPermissionAvailableManagers.ClientId)%>.Filter('firstname LIKE \'%' + this.value + '%\' or lastname LIKE \'%' + this.value + '%\'');">
</div>
					</td>
					<td class="PageContent" width="120" valign= middle>
		
		<asp:ImageButton ImageUrl="images/bt_u_fwd.gif" Runat=server id="imgbtnAddPermissionsManagers"></asp:ImageButton><br><br>
		<asp:ImageButton ImageUrl="images/bt_u_rwd.gif" Runat=server id="imgbtnRemovePermissionsManagers"></asp:ImageButton>
					
					</td>
					<td class="PageContent" width="100%" valign=top>
					
					
					
					
<!--active list-->
<b>Assigned Manager Groups/Users</b><br><br>


<COMPONENTART:GRID id="GridAssignedManagers" runat="server" autoPostBackonDelete="true" PageSize="10" pagerposition="2" ScrollBar="Off" Sort="typeofobject asc" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategoryActiveAM" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="false" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" AllowPaging="true" cachecontent="false" PagerStyle="Numbered"  >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategoryActiveAM">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateCategoryActiveAM">
          <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategoryActiveAM">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                                
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategoryActiveAM" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataField="imagesource" DataCellClientTemplateId="TypeIconTemplateCategoryActiveAM" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True"  />
<componentart:GridColumn AllowEditing="false" HeadingText="Name" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" FixedWidth="false"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" DataCellCssClass="LastDataCell" AllowEditing="false" AllowGrouping="False" Width="120" SortedDataCellCssClass="SortedDataCell" DataField="description"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
					</td>					
				</tr>
			</table>													
									

								
								
								
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
								</ComponentArt:PageView>
								
								
								
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Pageview2">
									<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
									font-size: 10px;
									font-weight: normal;">Modify the user's files.</div><br>
									Coming soon.
									
									
								</ComponentArt:PageView>
								
								
								
								
								
								
								

							</ComponentArt:MultiPage>
							</div>
							<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
								<div style="TEXT-ALIGN:right"><br>
									<asp:Button id="btnProjectSave" TabIndex="0" runat="server" Text="Save"></asp:Button><img src="images/spacer.gif" width="5" height="1"><input type="button" onclick="window.opener.NavigateToPage('<%if request.querystring("type") = "contact" then 
										response.write ("Contact") 
										else 
										response.write ("User") 
										end if%>');window.close();" value="Close">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
<script language=javascript >
TabStripCategory.SelectTabById('<%response.write (request.querystring("tab"))%>');

function UDFTabCallback(id)
{
<%Response.Write(CallbackUDFTab.ClientID)%>.Callback(id);
}



</script>
		</form>
	</body>
</HTML>
