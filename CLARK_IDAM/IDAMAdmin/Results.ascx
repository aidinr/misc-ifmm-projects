<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.Results" enableViewState="False" CodeBehind="Results.ascx.vb" %>
<div class="previewpaneMyProjects ui-accordion-content ui-helper-reset ui-widget-content ui-accordion-content-active" id="toolbarheading" style="z-index:99;border-top:0px;border-bottom:0px;">
<!--Project cookie and top menu-->
<table cellspacing="0" cellpadding="4" id="table2" width="100%">
<tr><td valign="top" width="1" nowrap>
<img src="images/ui/search_ico_bg.gif" height="42" width="42">
</td>
<td valign="top">
<font face="Verdana" size="3"><b>Search Results</b></font><br>
<font face="Verdana" size="1">
<b>Keyword:</b> <span style="font-weight: 400"><%response.write(Session("keyword"))%></span> <asp:Literal id="LiteralAltKeywordSpellingSuggestion" Text="No filters available" visible=true runat="server" />
</font>

</td>
<td id="Test" valign="top" align="right">



<!--<div style="padding-top:2px;">
[ <a href="#">help</a> ]
</div>-->
</td></tr>
<tr><td valign="top" colspan="2">
</td>
</tr>
</table>
</div><!--end preview pane-->
<div id="subtabsetbottomlinefix" style="border-top:1px solid silver;z-index:2;position:relative;top:-1px;"></div>
<div class="previewpaneSubResults"></div>
<script language="javascript">
    var xoffsetpopup
    xoffsetpopup = -100
    var yoffsetpopup
    yoffsetpopup = -400
</script>
<!--<script language="javascript"  src="js/filesearchhover.js"/>-->
<style>
.projectcentertest { Z-INDEX: 100; WIDTH: 100%; HEIGHT: 100% }
</style>
<script type="text/javascript">
//<![CDATA[    ]
//]]>
</script>
<script type="text/javascript"  src="js/jquery.tabSlideOut.v1.3.js"></script>
<script type="text/javascript" language="javascript">

$(document).ready(function() {
$(function(){
        $('.slide-out-div').tabSlideOut({
            tabHandle: '.handle',                     //class of the element that will become your tab
            pathToTabImage: '/images/side_tab_filters.jpg', //path to the image for the tab //Optionally can be set using css
            imageHeight: '122px',                     //height of tab image           //Optionally can be set using css
            imageWidth: '30px',                       //width of tab image            //Optionally can be set using css
            tabLocation: 'right',                      //side of screen where tab lives, top, right, bottom, or left
            speed: 300,                               //speed of animation
            action: 'click',                          //options: 'click' or 'hover', action to trigger animation
            topPos: '250px',                          //position from the top/ use if tabLocation is left or right
            leftPos: '20px',                          //position from left/ use if tabLocation is bottom or top
            fixedPosition: true                      //options: true makes it stick(fixed position) on scroll
            
        });

        

    });

 //$('.handle').click(function(){       });

 


    function overlayclickclose() {
        if (closedialog) {
            $('#dialogAssetFiltersNew').dialog('close');
        }
        //set to one because click on dialog box sets to zero
        closedialog = 1;
    }

    $('#dialogAssetFiltersNew').dialog({ autoOpen: false,
        open: function() { closedialog = 0; $(document).bind('click', overlayclickclose); },
        focus: function() { closedialog = 0; },
        close: function() { $(document).unbind('click'); }
    });

    $('#assetfilternewaddfilter_clear').click(function() {
        ClearFilters();
        DoSearchFilter('filter', document.getElementById('ctl14_search_filter_asset').value);
        return false;
    });
    
    $(window).resize(function() {
        try
        {
            DoSearchFilter('filter', document.getElementById('ctl14_search_filter_asset').value);
            ReSizeassetfilterscrollbars();
        } catch(x) {}

    });
         
ReSizeassetfilterscrollbars();
    
});

function ReSizeassetfilterscrollbars(){     
var p = $('.slide-out-div');
var position = p.position();
var tagpos = ($(window).height() - position.top - 120);
$('#assetfilterscrollbars').height(tagpos);
}

function LoadFiltersCallback(){
var p = $('.slide-out-div');
var position = p.position();
var tagpos = ($(window).width() - position.left);
if (tagpos == 0) {
<%response.write(CallbackAssetFiltersNew.ClientID)%>.Callback('forcebuild');
}
}




// Forces the grid to adjust to the new size of its container          
function resizeGridAssets(DomElementId, NewPaneHeight, NewPaneWidth)
{
<% Response.Write(GridAssets.ClientID) %>.Render();
<% Response.Write(GridProjects.ClientID) %>.Render();
//alert('here');
} 
function editGrid(rowId)
{
//alert('here2');
<% Response.Write(GridAssets.ClientID) %>.Edit(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(rowId)); 
}




function onInsert(item)
{

if (confirm("Insert record?"))
return true; 
else
return false; 

}

function ShowContextMenu(item, column, evt) 
{
//alert('here3');
<% Response.Write(GridAssets.ClientID) %>.Select(item); 

return false; 
}

function onUpdate(item)
{
if (confirm("Update record?"))
return true; 
else
return false; 

}

function onCallbackError(excString)
{
if (confirm('Invalid data has been entered. View details?')) alert(excString); 
}

function onDelete(item)
{

if (confirm("Delete record?"))
return true; 
else
return false; 

}

function editRow()
{
//alert('here4');
<% Response.Write(GridAssets.ClientID) %>.EditComplete();     
}

function insertRow()
{
//alert('here5');
<% Response.Write(GridAssets.ClientID) %>.EditComplete(); 
}



function deleteRow(ids)
{
//check to see if multi select
var sAssets;
var addtype;

sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();

var arraylist;
var i;
arraylist = '';
if (sAssets.length > 1) {
if (confirm("Delete selected assets?"))
{
/*download multi*/
for (i = 0; i < sAssets.length; i++) {
if (arraylist == ''){
if (sAssets[i]!=null){
arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
}
} else {
if (sAssets[i]!=null){
if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
}
}
}
}
arraylist= arraylist.substring(1, arraylist.length);
addtype = 'multi';
}
else
{
/*download single*/
addtype = 'single';

}
}
else {
/*download single*/
addtype = 'single';
}


if (addtype == 'multi') {
if (arraylist != ''){
//override filter call
<%response.write( GridAssets.ClientId)%>.Filter("DELETE " + arraylist);
<%response.write( GridAssets.ClientId)%>.Page(0);
//return false;
}
}
if (addtype == 'single') {
/*assume single*/
if (confirm("Delete this asset?")){
<% Response.Write(GridAssets.ClientID) %>.Delete(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(ids));

}
else {
}
} 
}

function CallbackAssetFiltersNew_onComplete()
{
sizeCenterPane();
}

function CallbackTHUMBLIST_onCallbackComplete()
{
sizeCenterPane();
var p = $('.slide-out-div');
var position = p.position();
var tagpos = ($(window).width() - position.left);
if (tagpos > 50) {
<%response.write(CallbackAssetFiltersNew.ClientID)%>.Callback('forcebuild');
}
document.getElementById('radio1id').innerHTML="<span class='ui-button-text'>Assets ("+GetCookie('AssetResultsItemTotal')+")</span>";
try
{
    
	if ( GetCookie('PROJECTPAGESNAPProjectAssetsFilters').split(",")[6] == 0  )
	{
		//<%'response.write(CallbackAssetFilters.ClientID)%>.Callback('forcebuild');
	}
} catch(x) {}
}
function CheckAllItemsAnyMode()
{
if (getLastView() == 'thumb')
{
CheckAllItemsThumbs();
}else{
CheckAllItems();
}
}

function deleteRowProject(ids)
{
//check to see if multi select
var sAssets;
var addtype;

sAssets = <%response.write( GridProjects.ClientId)%>.GetSelectedItems();

var arraylist;
var i;
arraylist = '';
if (sAssets.length > 1) {
if (confirm("Delete selected projects?"))
{
/*download multi*/
for (i = 0; i < sAssets.length; i++) {
if (arraylist == ''){
if (sAssets[i]!=null){
arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
}
} else {
if (sAssets[i]!=null){
if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
}
}
}
}
arraylist= arraylist.substring(1, arraylist.length);
addtype = 'multi';
}
else
{
/*download single*/
addtype = 'single';

}
}
else {
/*download single*/
addtype = 'single';
}


if (addtype == 'multi') {
if (arraylist != ''){
//override filter call
<%response.write( GridProjects.ClientId)%>.Filter("DELETE " + arraylist);
<%response.write( GridProjects.ClientId)%>.Page(0);
//return false;
}
}
if (addtype == 'single') {
/*assume single*/
if (confirm("Delete this project?")){
<% Response.Write(GridProjects.ClientID) %>.Delete(<% Response.Write(GridProjects.ClientID) %>.GetRowFromClientId(ids));

}
else {
}
} 
}







function ThumbnailCallBackPage(page)
{
<% Response.Write(ThumbnailCallBack.ClientID) %>.Callback(page);
}


function ThumbnailCallBackPageProjects(page)
{
<% Response.Write(CallBackProjects.ClientID) %>.Callback(page);
}







//setup form switch for delete or multi asset zip download
function DownloadAssets(ids){
var sAssets;
var downloadtype;
sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
ids = ids.replace('0 ','');
var arraylist;
var i;
arraylist = '';
if (sAssets.length > 1) {
if (confirm("Download selected assets?"))
{
/*download multi*/
for (i = 0; i < sAssets.length; i++) {
if (arraylist == ''){
arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
} else {
if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
}
}
}

arraylist= arraylist.substring(1, arraylist.length);
downloadtype = 'multi';
}
else
{
/*download single*/
downloadtype = 'single';

}
}
else {
/*download single*/
downloadtype = 'single';
}
if (downloadtype == 'multi') {
if (arraylist != ''){
<%=idamadmin.idam.downloadcheck_callback_clientID%>.Callback('multi,'+arraylist);		
}
}
if (downloadtype == 'single') {
/*assume single*/
<%=idamadmin.idam.downloadcheck_callback_clientID%>.Callback('single,'+ids);
}

}




function PreviewOverlayOnSingleClickAssetsFromIcon(RowId,eventObject)
{
try
{

var itemvaluetmp;
var itemvaluenametmp;
var itemvaluefiletypetmp;
var item 
itemRow = <% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(RowId);
itemvaluetmp = itemRow.GetMember('asset_id').Value;
itemvaluenametmp = itemRow.GetMember('name').Value;
itemvaluefiletypetmp = getURLPreviewIcon() + itemRow.GetMember('imagesource').Value;


}  //end try  
catch(err)
{
txt="There was an error on this page.\n\n"
txt+="Error description: " + err.description
alert(txt);
}

//return true;
}




function AssetPreview(Asset_ID,cid)
{
//get state

// Response.Write(SnapProjectAssetOverlay.ClientID) %>.Expand();

//if ( Response.Write(SnapProjectAssetOverlay.ClientID) %>.IsMinimized)
//{
// Response.Write(SnapProjectAssetOverlay.ClientID) %>.UnMinimize();
//}
//response.write( SnapProjectAssetOverlay.ClientId)%>.Callback(cid);
}

//setup goto for grid nav
function GotoLocation(pid,cid)
{
window.location = 'IDAM.aspx?page=Project&Id=' + pid + '&type=project&c=' + cid;
return true;
}



function onUpdate(item)
{

if (confirm("Update record?"))
return true; 
else
return false; 

}


function onInsert(item)
{

if (confirm("Insert record?"))
return true; 
else
return false; 

}




function onDelete(item)
{

if (confirm("Delete record?"))
return true; 
else
return false; 

}	


function editRow()
{
//alert('here6');
<% Response.Write(GridAssets.ClientID) %>.EditComplete();     
}


function editGrid(RowId)
{
//alert('here7');
itemRow = <% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(RowId);
itemvaluetmp = itemRow.GetMember('asset_id').Value;
window.location = 'IDAM.aspx?page=Asset&ID=' + itemvaluetmp + '&type=asset'
}
function editGridProject(RowId)
{
itemRow = <% Response.Write(GridProjects.ClientID) %>.GetRowFromClientId(RowId);
itemvaluetmp = itemRow.GetMember('projectid').Value;
window.location = 'IDAM.aspx?page=Project&ID=' + itemvaluetmp + '&type=project'
}



function CheckAllItems()
{

var itemcnt;
if(<% Response.Write(GridAssets.ClientID) %>.get_pageSize()<<% Response.Write(GridAssets.ClientID) %>.get_recordCount()) itemcnt=<% Response.Write(GridAssets.ClientID) %>.get_pageSize();
if(<% Response.Write(GridAssets.ClientID) %>.get_pageSize()><% Response.Write(GridAssets.ClientID) %>.get_recordCount()) itemcnt=<% Response.Write(GridAssets.ClientID) %>.get_recordCount();
if(itemcnt==100)itemcnt=99;

var itemIndex = 0;
for (var x = 1; x <= itemcnt; x++)
{
<% Response.Write(GridAssets.ClientID) %>.Select(<% Response.Write(GridAssets.ClientID) %>.Table.GetRow(x-1),true);
}
<% Response.Write(GridAssets.ClientID) %>.Render();
}

function MM_findObj(n, d) { //v3.0
var p,i,x; if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function CheckAllItemsThumbs() { //Set All checkboxes, if un-checked, check them.
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
var dummyobjectvalue = checks[0].value;
var firstobjectcheckedstate = document.getElementById('aselect_' + dummyobjectvalue);
var bcheck = firstobjectcheckedstate.checked

if (bcheck) {
bcheck = true;
} else {
bcheck = false;
}

for (i=0; i<checks.length; i++) {
MM_findObj('aselect_' + checks[i].value).checked = bcheck ;
highlightAssetToggleII(document.getElementById('aselect_' + checks[i].value),checks[i].value,true); 
}

}
}



function highlightAssetToggleII(checkbox,asset_id,keyoverride)
{
var highlightbox;
highlightbox = document.getElementById('assethighlight_' + asset_id);
if (checkbox.checked) {
highlightbox.style.border = '1px solid white';
highlightbox.style.backgroundColor = 'white';
checkbox.checked = false;

} else
{

var gAgent=navigator.userAgent.toLowerCase();
var gbFirefox=(gAgent.indexOf("firefox")!=-1);
if (!gbFirefox){
e=window.event||window.Event;
if (e!=undefined){
if((!e.ctrlKey)&&(!keyoverride)){
ClearAllItemsThumb();
}
}
}
highlightbox.style.border = '1px solid black';
highlightbox.style.backgroundColor = '#FFEEC2';
checkbox.checked = true;
}

if (DialogTagging.get_isShowing()==true) {
    Project_GridAssets_onSelect();
} 
}

//setup form switch for delete or multi asset zip download
function DownloadAssetsThumbs(ids){
var arraylist;
var i;
var sAssets;
var downloadtype;
arraylist = '';
ids = ids.replace('0 ','');
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
for (i=0; i<checks.length; i++) {
if (MM_findObj('aselect_' + checks[i].value).checked) {
arraylist = arraylist + ',' +  checks[i].value;
}
}
}
//alert(arraylist + arraylist.split(',').length)
if (arraylist.split(',').length > 2) {
if (confirm("Download selected assets?"))
{
arraylist= arraylist.substring(1, arraylist.length);
downloadtype = 'multi';
}
else
{
/*download single*/
downloadtype = 'single';

}
}
else {
/*download single*/
downloadtype = 'single';
}
if (downloadtype == 'multi') {
if (arraylist != ''){
<%=idamadmin.idam.downloadcheck_callback_clientID%>.Callback('multi,'+arraylist);		
}
}
if (downloadtype == 'single') {
/*assume single*/
<%=idamadmin.idam.downloadcheck_callback_clientID%>.Callback('single,'+ids);
}

}

function onDeleteAsset(item)
{

// if (confirm("Delete record?"))
return true; 
// else
//   return false; 

}
function AssetFiltersCallback_onCallbackComplete(sender, eventArgs)
{
//setTimeout("showCustomFooter();",timeoutDelay);
}


</script>

<script>
function isdefined(variable)
{
return (typeof (window[variable]) == "undefined") ? false : true;
}
var timeoutDelay = 1400;
var nb = "&nbsp;"; // non-breaking space
function buildPager(grid, pagerSpan, First, Last)
{
var pager = "";
var mid = Math.floor(pagerSpan / 2);
var startPage = grid.PageCount <= pagerSpan ? 0 : Math.max(0, grid.CurrentPageIndex - mid);
// adjust range for last few pages
if (grid.PageCount > pagerSpan)
{
startPage = grid.CurrentPageIndex < (grid.PageCount - mid) ? startPage : grid.PageCount - pagerSpan;
}

var endPage = grid.PageCount <= pagerSpan ? grid.PageCount : Math.min(startPage + pagerSpan, grid.PageCount);

if (grid.PageCount > pagerSpan && grid.CurrentPageIndex > mid)
{
pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(0);return false;\">&laquo; First</a>" + nb + "..." + nb;
pager += "<a href=\"#\" onclick=\"" + grid.Id + ".PreviousPage();return false;\">&lt;</a>" + nb;
}

for (var page = startPage; page < endPage; page++)
{
var showPage = page + 1;
if (page == grid.CurrentPageIndex)
{
pager += showPage + nb;
}
else 
{
pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(" + page + ");return false;\">" + showPage + "</a>" + nb;
}
}

if (grid.PageCount > pagerSpan && grid.CurrentPageIndex < grid.PageCount - mid)
{
pager += "<a href=\"#\" onclick=\"" + grid.Id + ".NextPage();return false;\">></a>" + nb + "..." + nb;
pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(" + (grid.PageCount - 1) + ");return false;\">Last &raquo;</a>" + nb;
}

return pager;
}
function buildPageXofY(grid, Page, of, items)
{
// Note: You can trap an empty Grid here to change the default "Page 1 of 0 (0 items)" text
var pageXofY = Page + nb + "<b>" + (grid.CurrentPageIndex + 1) + "</b>" + nb + of + nb + "<b>" + (grid.PageCount) + "</b>" + nb + "(" + grid.RecordCount + nb + items + ")";

return pageXofY;
}
function showCustomFooter()
{
//alert('here8');
var gridId = "<%response.write( GridAssets.ClientId)%>";
if (isdefined(gridId))
{
var grid = <%response.write( GridAssets.ClientId)%>;

var Page = "Page";
var of = "of";
var items = "items";

var pagerSpan = 5; // should be at least 2
var First = "First";
var Last = "Last";
var cssClass = "GridFooterText";

var footer = buildPager(grid, pagerSpan, First, Last);
document.getElementById("tdPager").innerHTML = "<div style='white-space:nowrap;display:inline;' class=\"" + cssClass + "\">" + footer + "</div>";

footer = buildPageXofY(grid, Page, of, items);
document.getElementById("tdIndex").innerHTML = "<div class=\"" + cssClass + "\">" + footer + "</div>";
}
else 
{
setTimeout("showCustomFooter();", timeoutDelay);
}
}
function onPage(newPage)
{
// delay call so that Grid's client properties have their new values
setTimeout("showCustomFooter();",timeoutDelay);
return true;
}
function onLoad()
{
showCustomFooter();
}



function showCustomFooterP()
{
var gridId = "<%response.write( GridProjects.ClientId)%>";
if (isdefined(gridId))
{
var grid = <%response.write( GridProjects.ClientId)%>;

var Page = "Page";
var of = "of";
var items = "items";

var pagerSpan = 5; // should be at least 2
var First = "First";
var Last = "Last";
var cssClass = "GridFooterText";

var footer = buildPager(grid, pagerSpan, First, Last);
document.getElementById("tdPagerP").innerHTML = "<div style='white-space:nowrap;display:inline;' class=\"" + cssClass + "\">" + footer + "</div>";

footer = buildPageXofY(grid, Page, of, items);
document.getElementById("tdIndexP").innerHTML = "<div class=\"" + cssClass + "\">" + footer + "</div>";
}
else 
{
setTimeout("showCustomFooterP();", timeoutDelay);
}
}

function onPageP(newPage)
{
// delay call so that Grid's client properties have their new values
setTimeout("showCustomFooterP();",timeoutDelay);

return true;
}

function onLoadP()
{
showCustomFooterP();
}




</script> 
   <style type="text/css">
      
      .slide-out-div {
          padding: 20px;
          width: 250px;
          background: #fffffb;
          border: 1px solid #DFDFDF;
          z-index:1;
          height:100%;
      }   
      

      </style>



<div id="subtabset" style="position:relative;top:-24px;z-index:2; font-size:10px;">
<input type="radio" id="radio1" name="subtab" checked="checked" onclick="showlist('assetlist');"/><label id="radio1id" for="radio1">Assets</label><img src="images/spacer.gif" width="2" alt="" />
<input type="radio" id="radio2" name="subtab"  onclick="showlist('projectlist');"/><label id="radio2id" for="radio2"><asp:literal  id="ltrl_link_Projects" runat="server"></asp:literal></label><img src="images/spacer.gif" width="2" alt="" />
</div>


    <script>
        $(function () {
            $("#subtab").button();
            $("#subtabset").buttonset();
        });
	</script>

<div class="previewpaneProject" id="toolbar" style="font-size:10px;margin-top:-10px;">


<!--END Project cookie and top menu-->
<!--projectcentermain-->
<div id="projectcentermain" >



<div id="AssetListing" style="display:none;">


                 
<div class="slide-out-div">
<a class="handle" onclick="javascript:LoadFiltersCallback();"></a>
            <h3></h3>
            <p>
            </p>
            
    <div id="filter_assets">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td width="120" align="left" nowrap>
                    <div style="padding-left: 4px; padding-bottom: 10px;">
                        <b>Search Within Results:</b></div>
                    <div id="search_input_filter">
                        <asp:TextBox ID="search_filter_asset" runat="server" onkeydown="javascript:if(event.keyCode==13) {DoSearchFilter('filter',this.value); return false}"
                            type="text" value="" name="search_filter_asset"></asp:TextBox>
                        <a href="#" style="border:0px;"><img style="border:0px;" height="19" src="images/search_input_button.jpg" width="35" onclick="javascript:DoSearchFilter('filter',document.getElementById('ctl14_search_filter_asset').value); return false;"></a></div>
                </td>
            </tr>
        </table>
        <br />
        <div style="padding-bottom: 5px;">
            <table width="100%">
                <tr>
                    <td>
                        <b>iDAM Filters:</b>
                    </td>
                    <td align="right" class="assetfilternewaddfilter">
                        <div id="assetfilternewaddfilter_clear">
                            [ <a href="#">clear</a> ]</div>
                    </td>
                </tr>
            </table>
        </div>
       
       
       
            
       <div id="assetfilterscrollbars" style="overflow-x: hidden;overflow-y:auto;height:300px;">
       
       
       
                                    <ComponentArt:CallBack ID="CallbackAssetFiltersNew" runat="server" CacheContent="false" >
                                        <ClientEvents>
<CallbackComplete EventHandler="CallbackAssetFiltersNew_onComplete" />
                                        </ClientEvents>
                                        <Content><br />
                                        &nbsp;&nbsp;&nbsp;No filters available.  
                                            <asp:Literal ID="LiteralAssetFiltersNew" Text="" runat="server" />
                                        </Content>
                                        
                                    </ComponentArt:CallBack>
                                    <div id="dialogAssetFiltersNew" title="Dialog Title" >

                                        <ComponentArt:CallBack ID="CallbackAssetFiltersContent" runat="server" CacheContent="false">
                                            <ClientEvents>
                                            </ClientEvents>
                                            <Content>
                                            <asp:PlaceHolder runat="server" ID="PLFiltersContentHeading" >
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div id="search_input_dialog_filter">
                                                          
                                                                <asp:Literal ID="LiteralAssetFiltersContentTextObject" Text="" runat="server"></asp:Literal>
                                                          
                                                                <input type="text" id="assetfiltertextfilter" onkeydown="javascript:if(event.keyCode==13) {CallbackAssetFiltersContent.Callback(filterobjecttype,'textfilter',this.value); return false}"
                                                                    name="assetfiltertextfilter" />
                                                                <a href="#" style="border:0px;">
                                                                    <img style="border:0px;" height="19" src="images/search_input_button.jpg" width="35" onclick="javascript:CallbackAssetFiltersContent.Callback(filterobjecttype,'textfilter',document.getElementById('assetfiltertextfilter').value); return false;"></a></div>
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:PlaceHolder>
                                                                               
                                            
                                                <asp:Literal ID="LiteralAssetFiltersContent" Text="" runat="server"></asp:Literal>
                                            </Content>
                                           
                                        </ComponentArt:CallBack>
                                    </div>
                                    
                                    
                                    </div> <!--scrollbars-->
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
    </div>
</div>
                          
     <script>
         $(function () {
             $("#JQRESASSETS").accordion({
                 autoHeight: false,
                 navigation: true,
                 collapsible: true,
                 change: function (event, ui) {
                     $.cookie('JQRESASSETS', $("#JQRESASSETS").accordion("option", "active"));
                 }
             });
         });
         $(document).ready(function () {
             if ($.cookie('JQRESASSETS') == "false") {
                 $("#JQRESASSETS").accordion("option", "active", false);
             }
         });

</script> 
 
<div id="JQRESASSETS" style="padding-bottom:15px;"> 
	<h4><a href="#section1">Assets</a></h4> 

<div class="MultiPageContent">
                 <br />
                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td valign="top" align="left" height="21" colspan="2">
                            <!--BreadCrumb-->
                          
                                    
                          
                          
<div class="SnapHeaderViewIcons" style="display:block;" >
<table cellSpacing="0" cellPadding="0" width="100%" >
<tr valign =top>
<td width=14><div style="padding:4px;">View</div></td>
<td width=14><div style="padding-top:4px;padding-bottom:4px;"><a href="#"><asp:Image onclick="javascript:switchViewResults('list');" id="list_icon_GridAssets" runat="server"></asp:Image></a></div></td>
<td width=25><div style="padding:4px;">|</div></td>
<td width=14><div style="padding-top:4px;padding-bottom:4px;"><a href="#"><asp:Image onclick="javascript:switchViewResults('thumb');"  id="thumb_icon_GridAssets" runat="server"></asp:Image></a></div></td>
<td width=100% nowrap align="right" valign="middle" style="padding-right:8px;" >

<table cellSpacing="0" cellPadding="0"><tr><td>[ page size <a id="pagesize20" href="javascript:setPageSize(20);">20</a> <a id="pagesize50" href="javascript:setPageSize(50);">50</a> <a id="pagesize100" href="javascript:setPageSize(100);">100</a> ] [ <asp:HyperLink id=CheckAllItemsJavascriptFunction runat="server">select all</asp:HyperLink> ]<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("EDIT_ASSETS") Then%>&nbsp;[ <a href="javascript:TagAssets();">tag assets</a>&nbsp;or&nbsp;<a href="javascript:showqtagging();">Qtag</a> ]<%else %><%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("EDIT_QTAGS") Then%>[ <a href="javascript:showqtagging();">Qtag</a> ]<%End If %><%end if%>&nbsp;[ <a href="javascript:ShowSlideShowResults();">slideshow</a> ]</td></tr></table>

</td>
</tr>
</table>
</div>
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                            
                            </td>
                        </tr>
                        
                        <tr>
                            <td align="left" valign="top">
                                <div class="content_main">
                                    
                                    
                                    <!--Main Assets-->
                                    
                                    
                                  
                                    
                                    
                                    
                                    


<script>

function executeViewCallback(view)
{
if (view=='list') {
document.getElementById('assetGirdPagingHeading').style.display='block';
}else{
document.getElementById('assetGirdPagingHeading').style.display='none';
}
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(view);
}



function Project_GridAssets_onSelect(){
//alert('here9');
  var sAssets;
  if (getLastView() == 'thumb')
  {
		var arraylist;
		var i;
		arraylist = '';
		if (MM_findObj('selectassethidden').length > 1) { 
			var checks = MM_findObj('selectassethidden')
			for (i=0; i<checks.length; i++) {
				if (MM_findObj('aselect_' + checks[i].value).checked) {
					arraylist = arraylist + ',' +  checks[i].value;
				}
			}
		}
  		if (arraylist.split(',').length > 1) {
			arraylist= arraylist.substring(1, arraylist.length);
			IDAMTagids=arraylist;
			 if (DialogTagging.get_isShowing()==true){
            SNAPTAGGINGCallbackTags.Callback('View|'+arraylist);SNAPTAGGINGCallbackServices.Callback('View|'+arraylist);SNAPTAGGINGCallbackMedia.Callback('View|'+arraylist);SNAPTAGGINGCallbackIllust.Callback('View|'+arraylist);}
        } else {
        return true;
        }
  }else{
    sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
    var arraylist;
    var i;
    arraylist = '';
    if (sAssets.length > 0) {
		    /*download multi*/
		    for (i = 0; i < sAssets.length; i++) {
			    if (sAssets[i] != null) {
				    if (arraylist == ''){
					    arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
				    } else {
					    if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
						    arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					    }
				    }
			    }
		    }
		    arraylist= arraylist.substring(1, arraylist.length);
		    IDAMTagids=arraylist;
		     if (DialogTagging.get_isShowing()==true){
		    SNAPTAGGINGCallbackTags.Callback('View|'+arraylist);SNAPTAGGINGCallbackServices.Callback('View|'+arraylist);SNAPTAGGINGCallbackMedia.Callback('View|'+arraylist);SNAPTAGGINGCallbackIllust.Callback('View|'+arraylist);}
    } else {
        return true;
    }
  
  }
}

</script>
<style>
div.thumbnailbox {float:left!important; width:220px!important; width:180px; height:320px!important; height:220px;}
div.thumbnailboxsc {width:170px;height:220px;}
div.asset_name      { width:164px; overflow: hidden;padding-left:5px;
text-overflow-mode:ellipsis;font-size:8px }
div.asset_date      { width:120px; overflow: hidden;padding-left:5px;
text-overflow-mode:ellipsis;font-size:8px }  
div.asset_filetype  { width:80px; overflow: hidden;
text-overflow-mode:ellipsis;font-size:8px }                  
div.asset_filesize  { width:60px; overflow: hidden;
text-overflow-mode:ellipsis;font-size:8px }    
div.asset_owner     { width:120px; overflow: hidden;padding-left:5px;
text-overflow-mode:ellipsis;font-size:8px }    
div.asset_links    {width:100%;padding-left:2px; height:4px;overflow:hidden;
                    border: none 0; 
border-top: 1px dashed #000;/*the border*/
height: 1px;/*whatever the total width of the border-top and border-bottom equal*/
                    
                    
                    }                                                                           
span.nowrap       { white-space : nowrap; }
div.attributed-to { position: relative;left:8px }
</style>
<div class="assetGirdPagingHeading" id="assetGirdPagingHeading" style="display:block;" >
<div id="CustomFooter" style="width:100%;border-top:none;">
<table width="100%">
<tr>
<td id="tdPager" align="left"></td>
<td id="tdIndex" align="right"></td>
</tr>
</table>
</div>
</div>
<script language="javascript">
function loadProjectContextMenu(evt, id) {
//alert('here10');
loadContextMenu(evt, id, <%response.write(GridAssets.ClientID)%>);
}

function TNC_onCallbackComplete() {
sizeCenterPane();
}
</script>


<COMPONENTART:CALLBACK   id="CallbackTHUMBLIST" runat="server" CacheContent="false">
<ClientEvents>
<CallbackError EventHandler="GenericCallback_onCallbackError" />
<CallbackComplete EventHandler="CallbackTHUMBLIST_onCallbackComplete" />
</ClientEvents>
<CONTENT>

<table >
<tr><td id="gridassetswrapper"  style="width:100%;">

<COMPONENTART:GRID 
id="GridAssets" 
runat="server" 
AutoFocusSearchBox="false"
AutoCallBackOnInsert="true"
AutoCallBackOnUpdate="true"
AutoCallBackOnDelete="true"
ClientSideOnPage="onPage"
ClientSideOnLoad="onLoad"
FillContainer="true"
ScrollBar="Off"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ClientSideOnInsert="onInsert"
ClientSideOnUpdate="onUpdate"
ClientSideOnDelete="onDeleteAsset"
ClientSideOnCallbackError="onCallbackError"
ScrollPopupClientTemplateId="ScrollPopupTemplate" 
Height="10" 
Width="100%"
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
LoadingPanelFadeDuration="200"
LoadingPanelFadeMaximumOpacity="95"
LoadingPanelEnabled="true"
EnableViewState="true"
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRowCategory" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="20" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="True" 
GroupingPageSize="5" 
PagerTextCssClass="GridFooterTextCategory" 
PagerStyle="Numbered" 
PageSize="20" 
ManualPaging="true"
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeader" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="False" 
AllowSorting="False"
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
ScrollBarWidth="15" 
AllowPaging="true" >
<ClientEvents>
<CallbackComplete EventHandler="GridAssets_onCallbackComplete" />
   <ItemSelect EventHandler="Project_GridAssets_onSelect" />
    <ItemUnSelect EventHandler="Project_GridAssets_onSelect" /> 
</ClientEvents>
<ClientTemplates>
<ComponentArt:ClientTemplate Id="ItemDraggingTemplate">
<table width=10><tr><td valign=middle><img src="images/assetdragicon.gif" border=0 ></td><td valign=middle nowrap  ><div style="width:100px;height:20px;border:single solid black; font-family: Verdana; font-weight:bold;font-size:8pt; text-align :center;vertical-align:middle;">## ctl04_ctl00_SNAPProjectAssets_GridAssets.getSelectedItems().length < 2 ? 'One item' : ctl04_ctl00_SNAPProjectAssets_GridAssets.getSelectedItems().length + ' items' ## selected. </div></td></tr></table>
</ComponentArt:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplate">
<div style="width:120px;">
<img src="images/13Save2.gif" border=0  onClick="loadProjectContextMenu(event, '## DataItem.ClientId ##');"> | <a href="javascript:AddToCarouselFromPG('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border=0></a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
</div>
</ComponentArt:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateNoDownload">
<div style="width:120px;">
<img src="images/13Save2.gif" border=0  onClick="loadProjectContextMenu(event, '## DataItem.ClientId ##');"> | <a href="javascript:AddToCarouselFromPG('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border=0></a> 
</div>
</ComponentArt:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditCommandTemplate">
<a href="javascript:editRow();">Update</a> 
</ComponentArt:ClientTemplate>
<ComponentArt:ClientTemplate Id="FileSizeTemplate">
## getIDAMGridSizeFormat(DataItem.GetMember("filesize").Value) ##
</ComponentArt:ClientTemplate> 
<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
<a href="javascript:insertRow();">Insert</a> 
</ComponentArt:ClientTemplate>    
<ComponentArt:ClientTemplate Id="TypeIconTemplate">
<div style="width:65px;">
<img src="## getURLPreviewIcon() ####DataItem.GetMember("imagesource").Value ##" border="0"  > 
</div>
</ComponentArt:ClientTemplate>  
<ComponentArt:ClientTemplate Id="TypeFolderTemplate">
<table width="100">
<tr><td   ><A href="javascript:GotoLocation('## DataItem.GetMember("projectid").Value ##','');"><img border=0 src="images/ProjCat16.gif"></a></td><td style="overflow:hidden;" ><a href="IDAM.aspx?page=Project&id=## DataItem.GetMember("projectid").Value ##&type=project">## DataItem.GetMember("projectname").Value ##</a></td></tr>
<tr><td  ><A href="javascript:GotoLocation('## DataItem.GetMember("projectid").Value ##','## DataItem.GetMember("category_id").Value ##');"><img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif"></a></td><td style="overflow:hidden;" ><a href="IDAM.aspx?page=Project&id=## DataItem.GetMember("projectid").Value ##&type=project&c=## DataItem.GetMember("category_id").Value ##">## DataItem.GetMember("categoryname").Value ##</a></td></tr>
</table>
</ComponentArt:ClientTemplate>  
<ComponentArt:ClientTemplate Id="LookupRating">
<div style="width:50px;">
<img border=0 src="images/ratingg##if (DataItem.GetMember("rating").Value>=0&&DataItem.GetMember("rating").Value<=49){'_1'}if (DataItem.GetMember("rating").Value>=50&&DataItem.GetMember("rating").Value<=99){'_2'}if (DataItem.GetMember("rating").Value>=100&&DataItem.GetMember("rating").Value<=249){'_3'}if (DataItem.GetMember("rating").Value>=250&&DataItem.GetMember("rating").Value<=499){'_4'}if (DataItem.GetMember("rating").Value>=500&&DataItem.GetMember("rating").Value<=1000){'_5'}##.gif" alt="## DataItem.GetMember("rating").Value ##"> 
</div>
</ComponentArt:ClientTemplate> 
<ComponentArt:ClientTemplate Id="LookupProjectTemplate">
 <div id="## DataItem.GetMember("asset_id").Value ##" class="asset_image">
<table border=0 cellpadding=0 cellspacing=0 height="## getIDAMGridThumbnailHeight() ##"><tr><td valign=middle align=center height=## getIDAMGridThumbnailHeight() ##>
<A href="## getALink() #### DataItem.GetMember("asset_id").Value ##"><img border=0 src="## getURLPreview() #### DataItem.GetMember("asset_id").Value ##"></a></td></tr></table>
</div> 
</ComponentArt:ClientTemplate> 
<ComponentArt:ClientTemplate Id="LookupNameTemplate">
<div style="width:120px; overflow:hidden;">## DataItem.GetMember("name").Value ##</div>
</ComponentArt:ClientTemplate> 
<ComponentArt:ClientTemplate Id="LookupAssetPopupTemplate">
<A href="#" onclick="javascript:PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.ClientId ##',event);"><img border=0 src="images/15printPreview.gif"></a>
</ComponentArt:ClientTemplate> 
<ComponentArt:ClientTemplate Id="LookupProjectTemplatePreview">
<A href="javascript:AssetPreview('## DataItem.ClientId ##');"><img border=0 src="images/15printPreview.gif"></a>
</ComponentArt:ClientTemplate>           
<ComponentArt:ClientTemplate Id="LookupCategoryTemplate">
<A href="javascript:GotoLocation('## DataItem.GetMember("projectid").Value ##','## DataItem.GetMember("category_id").Value ##');">## DataItem.GetMember("categoryname").Value ##</a>
</ComponentArt:ClientTemplate>    
<ComponentArt:ClientTemplate Id="LookupCategoryIconTemplate">
<A href="javascript:GotoLocation('## DataItem.GetMember("ispost").Value ##','## DataItem.GetMember("category_id").Value ##');"><img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif"></a>
</ComponentArt:ClientTemplate>    
<componentart:ClientTemplate ID="LoadingFeedbackTemplate"> 
<table height="1px" width="100%" ><tr><td valign="center" align="center">
<table cellspacing="0" cellpadding="0" border="0"  background="images/loadingback.gif" height="70px" width="300px">
<tr>
<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
<td style="font-size:10px;font-family:Verdana;" width="60px" align="right">Loading...&nbsp;</td>
<td><img src="images/spinner2.gif"  border="0"></td>
<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
</tr>
</table>
</td></tr></table>
</componentart:ClientTemplate>                                 
</ClientTemplates>


<Levels>
<componentart:GridLevel DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asch10.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="desch10.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="10" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" Width="65" Align="Center" TextWrap="true" AllowSorting="false" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplate" HeadingCellCssClass="FirstHeadingCell" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" FixedWidth="True" />
<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" TextWrap="true" AllowSorting="false" dataField="imagesource"  DataCellCssClass="DataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="19" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="False" Width="120" DataCellClientTemplateId="LookupNameTemplate"  FixedWidth="true" HeadingText="Name" TextWrap="true" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" DataCellClientTemplateId="FileSizeTemplate" TextWrap="true" HeadingText="Filesize" SortedDataCellCssClass="SortedDataCell" DataField="filesize"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Type" AllowEditing="false" TextWrap="true" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Updated" AllowEditing="false" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Security Level" AllowEditing="false" TextWrap="true" SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="User" AllowEditing="false" TextWrap="true" SortedDataCellCssClass="SortedDataCell"   DataField="FullName"></componentart:GridColumn>
<componentart:GridColumn Visible="False" DataCellCssClass="DataCell" AllowEditing="False" TextWrap="true" DataCellClientTemplateId="LookupCategoryIconTemplate" HeadingText=" " HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" Align="Right" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="ispost" Width="19" FixedWidth="True"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="False" HeadingText="Folder" FixedWidth="true" Width="120" TextWrap="true" DataCellClientTemplateId="TypeFolderTemplate"  AllowGrouping="True" SortedDataCellCssClass="SortedDataCell"   DataField="categoryname" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="true" DataCellClientTemplateId="LookupRating" TextWrap="true" HeadingText="Rating" DataCellCssClass="DataCell"  SortedDataCellCssClass="SortedDataCell" DataField="rating" ></componentart:GridColumn>
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" Width="120" FixedWidth="true" HeadingText="Action" AllowEditing="false" TextWrap="true" AllowSorting="false" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand"  Align="Center" ></componentart:GridColumn>
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" Width="120" FixedWidth="true" Visible="False" HeadingText="Action" TextWrap="true" AllowSorting="false" AllowEditing="false" DataCellClientTemplateId="EditTemplateNoDownload" EditControlType="EditCommand"   Align="Center" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" TextWrap="true" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="category_id" TextWrap="true" SortedDataCellCssClass="SortedDataCell" DataField="category_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="ispost" TextWrap="true" SortedDataCellCssClass="SortedDataCell" DataField="ispost"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="timage" TextWrap="true" SortedDataCellCssClass="SortedDataCell" DataField="timage"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" TextWrap="true" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
<componentart:GridColumn Visible="False" DataCellCssClass="DataCell" HeadingText="ProjectName" TextWrap="true" AllowEditing="false"   SortedDataCellCssClass="SortedDataCell" DataField="projectname" ></componentart:GridColumn>

</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>

</td></tr></table>


<div style="padding:10px;"><asp:Literal id="literalNoAssets" Text="No assets available" visible=false runat="server" /></div>



<COMPONENTART:CALLBACK id="ThumbnailCallBack" runat="server" CacheContent="false">
<ClientEvents>
<CallbackError EventHandler="GenericCallback_onCallbackError" />
<CallbackComplete EventHandler="TNC_onCallbackComplete" />
</ClientEvents>
<CONTENT>
<!--hidden Thumbs-->

<asp:Literal id="ltrlPager" Text="" runat="server" />

<asp:Repeater id=Thumbnails runat="server" Visible="True">
<ItemTemplate>
<div valign="Top"  class="thumbnailbox" ><div class="thumbnailboxsc"><div align="left"><div align="left"><table  class="bluelightlight" border="0" cellspacing="0" width="15"><tr><td  class="bluelightlight" valign="top">
<div align="left" id="assethighlight_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" style="border:1px white solid;">
<table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse"><tr><td>
<table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse" height="120" width="160">
<tr class="bluelightlight" width="100%" align="center" height="120" width="160"><td class="bluelightlight" width="100%" height="120" width="160" align="center" valign="bottom" bordercolor="#808080" bordercolorlight="#808080" nowrap><img src="images\spacer.gif" height="0" width="159"><br><a id="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" class="asset_image" onclick="javascript:highlightAssetToggleII(document.getElementById('aselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>'),<%#DataBinder.Eval(Container.DataItem, "asset_id")%>,false);"><img alt="" border="0" src="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?instance=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMInstance%>&type=asset&size=2&height=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.ConfigurationService.GetConfigurationSetting(WebArchives.iDAM.Services.ConfigurationService.WebAdminConfigKey.IDAMThumbnailHeight)%>&width=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.ConfigurationService.GetConfigurationSetting(WebArchives.iDAM.Services.ConfigurationService.WebAdminConfigKey.IDAMThumbnailWidth)%>&qfactor=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMQfactor%>&id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"  ></a></td><td><img src="images\spacer.gif" width="0" height="140"></td>
</tr></table></td></tr></table></div></td></tr><tr><td><div style="padding:5px;"><table width="100%" cellspacing="0" cellpadding="0" ><tr><td nowrap width="5"><img src="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveIconURLBase%>?instance=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMInstance%>&size=0&id=<%#DataBinder.Eval(Container.DataItem, "imagesource")%>"></td>
<td nowrap><div class="asset_name"><span class="nowrap"><b><font face="Verdana" size="1"><%#DataBinder.Eval(Container.DataItem, "name")%></font></b></span></div></td>
<td align="right" width="20"><font color="#808080" face="Arial" size="1"><input onclick="highlightAssetToggle(this,<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);" class="preferences" type="checkbox" id="aselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" value="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"><input type=hidden id=selectassethidden value="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"></font></td></tr></table><table width="100%"  cellspacing="0" cellpadding="0" ><tr><td nowrap width="100%" align="left"><div class="asset_filetype"><span class="nowrap"><font face="Verdana" size="1"><%#DataBinder.Eval(Container.DataItem, "media_type_name")%></font></span></div></td>
<td align="right"><div class="asset_filesize"><span class="nowrap"><font face="Verdana" size="1"><%# FormatSize(CType(DataBinder.Eval(Container.DataItem, "filesize"), Long))%></font></span></div></td>
</tr></table><table width="100%" cellspacing="0" cellpadding="0" ><tr><td nowrap><font face="Verdana" size="1">Updated</font></td><td align="left" width="868"><div class="asset_date"><span class="nowrap"><font face="Verdana" size="1"><%# String.Format("{0:MM/dd/yy hh:mm tt}", (DataBinder.Eval(Container.DataItem, "update_date")))%></font></span></div></td></tr><tr><td nowrap><font face="Verdana" size="1">Created By</font></td>
<td align="left" width="868"><div class="asset_owner"><span class="nowrap"><font face="Verdana" size="1"><%#DataBinder.Eval(Container.DataItem, "fullname")%></font></span></div></td>
</tr><tr><td nowrap><font face="Verdana" size="1">Project</font></td>
<td align="left" width="868"><div class="asset_owner"><span class="nowrap"><font face="Verdana" size="1"><A href="javascript:GotoLocation('<%#DataBinder.Eval(Container.DataItem, "projectid")%>','');"><%#DataBinder.Eval(Container.DataItem, "projectname")%></font></span></div></td>
</tr></table><div style="padding-top:3px;"><div class="asset_links"></div></div><table width=100%><tr><td>
<b><font face="Verdana" size="1"><a href="IDAM.aspx?page=Asset&ID=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=asset">View Details</a></font><br>
<font face="Verdana" size="1"><a onclick="loadContextMenuThumbs(event, '0 <%#DataBinder.Eval(Container.DataItem, "asset_id")%>');">Download</a></font><br>
<font face="Verdana" size="1"><a href="javascript:AddToCarouselFromPG('0 <%#DataBinder.Eval(Container.DataItem, "asset_id")%>');">Add to Carousel</a></font><br></b>    
</td><td align=right valign=top><b><font face="Verdana" size="1"><a href="javascript:DeleteSelected('0 <%#DataBinder.Eval(Container.DataItem, "asset_id")%>');">Delete</a></font></b>
</td></tr></table></div></td></tr></table></div><img border="0" src="images/spacer.gif" width="135" height="8"></div></div></div>




</ItemTemplate>

<HeaderTemplate>
<!--Start Thumbnail Float-->
<table width=100%><tr><td>
<div valign="Bottom" style="float:left!important;">       
</HeaderTemplate>

<FooterTemplate>
</td></tr></table>
</FooterTemplate> 

</asp:Repeater>
<asp:Literal id="ltrlPagingBottom" Text="" runat="server" />
</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
<TABLE height="300" cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD align="center">
<TABLE cellSpacing="0" cellPadding="0" border="0">
<TR>
<TD style="FONT-SIZE: 10px">Loading...
</TD>
<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
</TR>
</TABLE>
</TD>
</TR>
</TABLE>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>







</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
<TABLE height="300" cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD align="center">
<TABLE cellSpacing="0" cellPadding="0" border="0">
<TR>
<TD style="FONT-SIZE: 10px">Loading...
</TD>
<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
</TR>
</TABLE>
</TD>
</TR>
</TABLE>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>


                                    
                                    
                                   
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                </div>
                            </td>
                        </tr>
                        
                    </table>
                    <br />
                </div>






</div><!--accord window-->
</div><!--end asset listings frame-->



<div id="ProjectListing" style="display:none;">



     <script>
         $(function () {
             $("#JQRESPROJECTS").accordion({
                 autoHeight: false,
                 navigation: true,
                 collapsible: true,
                 change: function (event, ui) {
                     $.cookie('JQRESPROJECTS', $("#JQRESPROJECTS").accordion("option", "active"));
                 }
             });
         });
         $(document).ready(function () {
             if ($.cookie('JQRESPROJECTS') == "false") {
                 $("#JQRESPROJECTS").accordion("option", "active", false);
             }
         });

</script> 
 
<div id="JQRESPROJECTS" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Projects</a></h3> 

   <div>


<!--Main Grid Box-->




<table width="100%">
<tr>
<td align=left width="30%"></td>
<td align=right width="70%"> </td></tr></table>
<%if GridProjects.visible then%>
<table  cellpadding=0 cellspacing =0><tr><td width="125" align=left nowrap><b>Filter Results:</b><img src="images/spacer.gif" width=5 height=1></td><td align=left width=250 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:150px;height:15px;padding:1px;" onkeydown="javascript:if(event.keyCode==13) {<%response.write( GridProjects.ClientId)%>.Filter('name LIKE \'%' + this.value + '%\'');setTimeout('showCustomFooterP();',2000); return false}"></td></tr></table>
<%end if%>



<div id="CustomFooterP" style="width:100%;border:solid 0px grey;border-top:none;">
<table width="100%">
<tr>
<td id="tdPagerP" align="left"></td>
<td id="tdIndexP" align="right"></td>
</tr>
</table>
</div> 


<!--No Projects warning-->
<asp:Literal id="literalNoProjects" Text="No projects available" visible=false runat="server" />


<COMPONENTART:GRID 
id="GridProjects" 
runat="server" 
AutoCallBackOnInsert="true"
AutoCallBackOnUpdate="true"
AutoCallBackOnDelete="true"
AutoFocusSearchBox="false"
ClientSideOnPage="onPageP"
ClientSideOnLoad="onLoadP"
pagerposition="2"
ScrollBar="Off"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ClientSideOnInsert="onInsert"
ClientSideOnUpdate="onUpdate"
ClientSideOnDelete="onDelete"
ClientSideOnCallbackError="onCallbackError"
ClientSideOnDoubleClick="BrowseOnDoubleClickProject" 
Height="100" Width="100%" 
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateProjects" 
EnableViewState="true"
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRow" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="true" 
GroupingPageSize="1" 
PagerTextCssClass="GridFooterText" 
PagerStyle="Numbered" 
PageSize="20" 
sort="rank desc"
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeader" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="false" 
ShowSearchBox="true" 
ShowHeader="false" 
CssClass="Grid" 
RunningMode="callback" 
ScrollBarWidth="15" 
AllowReordering="False"
AllowPaging="True" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateProjects">
<div><table cellspacing="0" cellpadding="0" border="0">
<tr>
<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
</tr>
</table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateProjects">
<a href="javascript:editGridProject('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border="0" > </a> | <a href="javascript:deleteRowProject('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
</ComponentArt:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditCommandTemplateProjects">
<a href="javascript:editRow();">Update</a> 
</ComponentArt:ClientTemplate>
<ComponentArt:ClientTemplate Id="InsertCommandTemplateProjects">
<a href="javascript:insertRow();">Insert</a> 
</ComponentArt:ClientTemplate>      
<ComponentArt:ClientTemplate Id="TypeIconTemplateProjects">
<img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
</ComponentArt:ClientTemplate>        
<ComponentArt:ClientTemplate Id="LookupProjectTemplateProjects">
<a href="IDAM.aspx?page=## DataItem.GetMember("page").Value ##&ID=## DataItem.GetMember("projectid").Value ##&type=## DataItem.GetMember("typeofobject").Value ##"><img border=0 src="images/goto.gif"></a>
</ComponentArt:ClientTemplate>  
<ComponentArt:ClientTemplate Id="ProjectNameClientTemplate">
<table><tr><td valign=top><img border=0 src="images/projcat16.gif" /></td><td><div style="color:Gray;" ><b>## DataItem.GetMember("name").Value ##</b><br />## DataItem.GetMember("description").Value ##</div></td></tr></table>
</ComponentArt:ClientTemplate>		
<ComponentArt:ClientTemplate Id="LookupRatingP">
<img border=0 src="images/ratingg##if (DataItem.GetMember("rank").Value>=0&&DataItem.GetMember("rank").Value<=49){'_1'}if (DataItem.GetMember("rank").Value>=50&&DataItem.GetMember("rank").Value<=99){'_2'}if (DataItem.GetMember("rank").Value>=100&&DataItem.GetMember("rank").Value<=249){'_3'}if (DataItem.GetMember("rank").Value>=250&&DataItem.GetMember("rank").Value<=499){'_4'}if (DataItem.GetMember("rank").Value>=500&&DataItem.GetMember("rank").Value<=1000){'_5'}##.gif" alt="## DataItem.GetMember("rank").Value ##"> 
</ComponentArt:ClientTemplate>     
<ComponentArt:ClientTemplate Id="TypeIconTemplateProjectII">
<a href="IDAM.aspx?page=Project&ID=## DataItem.GetMember("projectid").Value ##&type=project"><img border=0 src="## getProjectURLPreview() ##&crop=1&height=65&width=90&id=## DataItem.GetMember("projectid").Value ##" ></A>
</ComponentArt:ClientTemplate>                   
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
EditFieldCssClass="EditDataField"
EditCommandClientTemplateId="EditCommandTemplate"
InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="projectid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn  Align="Center" AllowEditing="false" DataCellClientTemplateId="TypeIconTemplateProjectII"
																HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif"
																HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="90"   FixedWidth="True" />
<componentart:GridColumn AllowEditing="True" Width="275"  TextWrap="true" DataCellClientTemplateId="ProjectNameClientTemplate" DataCellCssClass="DataCell" HeadingText="Name" 
															 AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name"></componentart:GridColumn>

<componentart:GridColumn HeadingText="Type" Visible="False" AllowEditing="false" AllowGrouping="False" Width="50"  SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Updated" AllowEditing="false" Width="80" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Security Level" AllowEditing="False" Width="90" SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Project Date" AllowEditing="false" Width="90" FormatString="MMM yyyy" SortedDataCellCssClass="SortedDataCell"  DataField="projectdate"></componentart:GridColumn>
<componentart:GridColumn HeadingText="User" AllowEditing="false" Width="100" SortedDataCellCssClass="SortedDataCell"  DataField="userid" ForeignTable="Users" ForeignDataKeyField="userid" ForeignDisplayField="FullName"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Assets" Width="40" FormatString="n" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid" ForeignTable="Assets" ForeignDataKeyField="uniqueid" ForeignDisplayField="acount"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Rating" DataCellClientTemplateId="LookupRatingP" Width="40"  DataField="rank" ></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" AllowEditing="false" Width="80"  DataCellCssClass="LastDataCellPostings" AllowSorting="False" DataCellClientTemplateId="EditTemplateProjects" EditControlType="EditCommand"   Align="Center" />
<ComponentArt:GridColumn  AllowEditing="false" Width="10" AllowSorting="False"  DataCellCssClass="LastDataCellPostings" Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="page" SortedDataCellCssClass="SortedDataCell" DataField="page"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="page" SortedDataCellCssClass="SortedDataCell" DataField="description"></componentart:GridColumn>

</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>







<COMPONENTART:CALLBACK id="CallbackProjects" runat="server" CacheContent="true">
<ClientEvents>
<CallbackError EventHandler="GenericCallback_onCallbackError" />
</ClientEvents>
<CONTENT>
<!--hidden Thumbs-->

<asp:Literal id="LiteralTopPagerProjects" Text="" runat="server" />
<asp:Repeater id="RepeaterProjects" runat="server" Visible="False">
<ItemTemplate>

<div valign="Top" style="float:left!important; width:220px!important; width:180px; height:220px!important; height:220px;">
<!--Project Thumbnails-->
<div style='width:170px;height:220px;'>
<div align="left">
<div align="left">
<table  class="bluelightlight" border="0" cellspacing="0" width="15">
<tr>
<td  class="bluelightlight" valign="top">
<div align="left" id="projecthighlight_<%#DataBinder.Eval(Container.DataItem, "projectid")%>" style="border:1px white solid;">
<table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse"><tr><td>
<table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse" height="120" width="160">
<tr class="bluelightlight" width="100%" align="center" height="120" width="160"><td class="bluelightlight" width="100%" height="120" width="160" align="center" valign="bottom" bordercolor="#808080" bordercolorlight="#808080" nowrap><img src="images\spacer.gif" height="0" width="159"><br><a id="25257" href="cd_asset.asp?p=50155&a=25257&pc=&pcc=&c=&post="><img alt="" border="0" src="
<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?id=<%#DataBinder.Eval(Container.DataItem, "projectid")%>&instance=<%#DataBinder.Eval(Container.DataItem, "instance")%>&type=project&size=2&height=120&width=160"></a></td><td><img src="images\spacer.gif" width="0" height="140"></td>

</tr>
</table></td></tr></table>
</div>
</td>
</tr>
<tr>
<td >
<div style="padding:5px;">             
<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
<td nowrap width="5"><img src="images/projcat16.jpg"></td>
<td nowrap><div class="asset_name"><span class="nowrap"><b><font face="Verdana" size="1"><%#DataBinder.Eval(Container.DataItem, "name")%></font></b></span></div></td>
<td align="right" width="20"><font color="#808080" face="Arial" size="1"><input onclick="highlightProjectToggle(this,<%#DataBinder.Eval(Container.DataItem, "projectid")%>);" class="preferences" type="checkbox" name="pselect_<%#DataBinder.Eval(Container.DataItem, "projectid")%>" value="<%#DataBinder.Eval(Container.DataItem, "projectid")%>"></font></td>
</tr>
</table>

<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
<td nowrap><font face="Verdana" size="1">Updated</font></td>
<td align="left" width="868"><div class="asset_date"><span class="nowrap"><font face="Verdana" size="1">

<%# String.Format("{0:MM/dd/yy hh:mm tt}", (DataBinder.Eval(Container.DataItem, "update_date")))%></font></span></div></td>

</tr>
</table>
<div style="padding-top:3px;">
<div class="asset_links"></div></div>
<b><font face="Verdana" size="1"><a href="#">View Details</a></font><br>       
</div>                        
</td>
</tr>
</table>
</div>

<img border="0" src="images/spacer.gif" width="135" height="8">
</div>
</div><!--end Project Thumbnail span-->

</div><!--end inner span-->
</ItemTemplate>
<HeaderTemplate>
<!--Start Thumbnail Float-->
<table width=100%><tr><td>
<div valign="Bottom" style="float:left!important;">       
</HeaderTemplate>
<FooterTemplate>
</td></tr></table>
</FooterTemplate> 
</asp:Repeater>
<asp:Literal id="LiteralBottomPagerProjects" Text="" runat="server" />

<!--hidden Thumbs-->



</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
<TABLE height="300" cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD align="center">
<TABLE cellSpacing="0" cellPadding="0" border="0">
<TR>
<TD style="FONT-SIZE: 10px">Loading...
</TD>
<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
</TR>
</TABLE>
</TD>
</TR>
</TABLE>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>

<div style="padding:25px;"></div>

</div>
</div><!--end accord windows-->
</div><!--end project listing frame-->




</div>
</div>







<script type=text/javascript>
    // Image preloading
    var img1 = new Image();
    img1.src = 'images/header_hoverBg.gif';
    var img2 = new Image();
    img2.src = 'images/header_activeBg.gif';
    var img3 = new Image();
    img3.src = 'images/spinner.gif';



    function BrowseOnDoubleClick(SelectedGridRow) {

        var itemvaluetmp;
        itemvaluetmp = SelectedGridRow.GetMember('asset_id').Value;
        if (itemvaluetmp.substring(0, 1) == 'P') {
            itemvaluetmp = itemvaluetmp.replace('P_', '');
            window.location.href = 'IDAM.aspx?page=Project&id=' + itemvaluetmp

        } else {
            itemvaluetmp = itemvaluetmp.replace('C_', '');
            window.location.href = 'IDAM.aspx?page=Browse&id=' + itemvaluetmp + '&type=category';
        }

        return true;




    }


    function BrowseOnDoubleClickProject(SelectedGridRow) {

        var itemvaluetmp;
        itemvaluetmp = SelectedGridRow.GetMember('projectid').Value;




        window.location.href = 'IDAM.aspx?page=Project&id=' + itemvaluetmp



        return true;




    }

    function highlightAssetToggle(checkbox, asset_id) {
        var highlightbox;
        highlightbox = document.getElementById('assethighlight_' + asset_id);
        if (checkbox.checked) {
            highlightbox.style.border = '1px solid black';
            highlightbox.style.backgroundColor = '#FFEEC2';
        } else {
            highlightbox.style.border = '1px solid white';
            highlightbox.style.backgroundColor = 'white';
        } if (DialogTagging.get_isShowing() == true) {
            Project_GridAssets_onSelect();
        }
    }


    function highlightProjectToggle(checkbox, projectid) {
        var highlightbox;
        highlightbox = document.getElementById('projecthighlight_' + projectid);
        if (checkbox.checked) {
            highlightbox.style.border = '1px solid black';
            highlightbox.style.backgroundColor = '#FFEEC2';
        } else {
            highlightbox.style.border = '1px solid white';
            highlightbox.style.backgroundColor = 'white';
        } if (DialogTagging.get_isShowing() == true) {
            Project_GridAssets_onSelect();
        }
    }


</script>











<div style="padding-left:10px;"><div style="padding:3px;"></div></div>




<script language=javascript>
    //thumbnailsarea.style.display = 'none';
</script>




<script language=javascript type="text/javascript">

    function showtrailpreload(imageid, title, description, ratingaverage, ratingnumber, showthumb, height, filetype) {
        showtrail('<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?id=' + imageid + '&amp;instance=<%response.write (WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMInstance)%>&amp;type=asset&amp;size=2&amp;height=399&amp;width=370', title, description, '5', '1', 270, 7);

    }

</script>	



<script language=javascript type="text/javascript">

//search results
if (getResultsSetting() == 'project') {
document.getElementById('AssetListing').style.display = "none";
document.getElementById('ProjectListing').style.display = "block";


} else {
document.getElementById('ProjectListing').style.display = "none";
document.getElementById('AssetListing').style.display = "block";


}


function showlist(list){

if (list == "assetlist") {


document.getElementById('AssetListing').style.display = "block";
document.getElementById('ProjectListing').style.display = "none";

switchToAssetList();
} else {


document.getElementById('AssetListing').style.display = "none";
document.getElementById('ProjectListing').style.display = "block";

switchToPojectList();
}  	

}

function switchToPojectList() {
setLastViewResults ('project');	
}
function switchToAssetList() {
setLastViewResults ('asset');
}



<% if (request.form("searchtype")="project") then %>
showlist('projectlist');
<%else %>
showlist('assetlist');
<% end if %>




function AddToCarouselFromPG(ids)
{
//check to see if multi select
var sAssets;
var addtype;

if (getLastView()=='thumb')
{

var arraylist;
var i;
arraylist = '';
ids = ids.replace('0 ','');
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
for (i=0; i<checks.length; i++) {
if (MM_findObj('aselect_' + checks[i].value).checked) {
arraylist = arraylist + ',' +  checks[i].value;
}
}
}
//alert(arraylist + arraylist.split(',').length)
if (arraylist.split(',').length > 2) {
if (confirm("Add selected assets to carousel?"))
{
arraylist= arraylist.substring(1, arraylist.length);
addtype = 'multi';
}
else
{
/*download single*/
addtype = 'single';

}
}
else {
/*download single*/
addtype = 'single';
}


}else{


sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
ids = ids.replace('0 ','');
var arraylist;
var i;
arraylist = '';
if (sAssets.length > 1) {
if (confirm("Add selected assets to carousel?"))
{
/*download multi*/
for (i = 0; i < sAssets.length; i++) {
if (arraylist == ''){
arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
} else {
if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
}
}
}

arraylist= arraylist.substring(1, arraylist.length);
addtype = 'multi';
}
else
{
/*download single*/
addtype = 'single';

}
}
else {
/*download single*/
addtype = 'single';
}

}


if (addtype == 'multi') {
if (arraylist != ''){

carousel_callback.Callback('1,' + arraylist + ',AddMultiToCarousel');
}
}

if (addtype == 'single') {
/*assume single*/
carousel_callback.Callback('1,' + ids + ',AddToCarousel');
} 
}	

function PreviewOverlayOnThumbSingleClickAssets(aid,aname,afiletype,eventObject)
{


} 



function DeleteSelected(ids)
{
//check to see if multi select
var sAssets;
var addtype;


var arraylist;
var i;
arraylist = '';
ids = ids.replace('0 ','');
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
for (i=0; i<checks.length; i++) {
if (MM_findObj('aselect_' + checks[i].value).checked) {
arraylist = arraylist + ',' +  checks[i].value;
}
}
}
//alert(arraylist + arraylist.split(',').length)
if (arraylist.split(',').length > 2) {
if (confirm("Delete selected assets?"))
{
arraylist= arraylist.substring(1, arraylist.length);
addtype = 'multi';
}
else
{
if (confirm("Delete individual asset?"))
{
/*delete single*/
addtype = 'single';
} else {
/*delete nothing*/
addtype = 'donothing';
}
}
}
else {
if (confirm("Delete this asset?"))
{
/*delete single*/
addtype = 'single';
} else {
/*delete nothing*/
addtype = 'donothing';
}
}




if (addtype == 'multi') {
if (arraylist != ''){
<%response.write(ThumbnailCallBack.ClientID)%>.Callback('DeleteMulti,' + arraylist );
}
}

if (addtype == 'single') {
/*assume single*/
<%response.write(ThumbnailCallBack.ClientID)%>.Callback('DeleteSingle,' + ids );
} 
}	



  
function TagAssets()
  {
  //check to see if multi select
	var sAssets;
	var addtype;
	if (getLastView() == 'thumb')
        {
    
		var arraylist;
		var i;
		arraylist = '';
		if (MM_findObj('selectassethidden').length > 1) { 
			var checks = MM_findObj('selectassethidden')
			for (i=0; i<checks.length; i++) {
				if (MM_findObj('aselect_' + checks[i].value).checked) {
					arraylist = arraylist + ',' +  checks[i].value;
				}
			}
		}
		if (arraylist.split(',').length > 1) {

			arraylist= arraylist.substring(1, arraylist.length);
			addtype = 'multi';

		} else {
			alert('Select an asset to tag.');
			return;
		}

    } else {

		sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
		var arraylist;
		var i;
		arraylist = '';
		if (sAssets.length > 0) {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (sAssets[i] != null) {
						if (arraylist == ''){
							arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
						} else {
							if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
								arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
							}
						}
					}
				}
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
		} else {
			alert('Select an asset to tag.');
			return;
		}
    }
	opentagwindow(arraylist);
  }
	  

  
  

  
function ShowSlideShowResults()
  {
  //check to see if multi select
	var sAssets;
	var addtype;
	if (getLastView() == 'thumb')
        {
    
		var arraylist;
		var i;
		arraylist = '';
		if (MM_findObj('selectassethidden').length > 1) { 
			var checks = MM_findObj('selectassethidden')
			for (i=0; i<checks.length; i++) {
				if (MM_findObj('aselect_' + checks[i].value).checked) {
					arraylist = arraylist + ',' +  checks[i].value;
				}
			}
		}
		if (arraylist.split(',').length > 1) {

			arraylist= arraylist.substring(1, arraylist.length);
			addtype = 'multi';

		} else {
		    if(GetCookie('IDAMSlideShowCoolIris')!='1'&&GetCookie('BrowserCheckSilverlight')!='True')
		    {
			    alert('Select asset(s) to preview in the slideshow.');
			} else {
			    ShowSlideShowUseSessionSql('usesessionsqlresult');
			}
		    return;
		}

    } else {

		sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
		var arraylist;
		var i;
		arraylist = '';
		if (sAssets.length > 0) {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (sAssets[i] != null) {
						if (arraylist == ''){
							arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
						} else {
							if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
								arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
							}
						}
					}
				}
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
		} else {
			if(GetCookie('IDAMSlideShowCoolIris')!='1'&&GetCookie('BrowserCheckSilverlight')!='True')
		    {
		        alert('Select asset(s) to preview in the slideshow.');
		    } else {
		        ShowSlideShowUseSessionSql('usesessionsqlresult');
		    }
		    return;
		}
    }
	ShowSlideShow(arraylist);
  }





//initialize the callback controls - clear vars and execute initial callback
SetCookie ('SearchValueProjectAssets', '', exp);
SetCookie ('tFilter', '', exp);
SetCookie ('sFilter', '', exp);
SetCookie ('mFilter', '', exp);
SetCookie ('iFilter', '', exp);
SetCookie ('cFilter', '', exp);
SetCookie ('pFilter', '', exp);
SetCookie ('tagFilter', '', exp);
if (getLastView()!='thumb') {
document.getElementById('assetGirdPagingHeading').style.display='block';
}else{
document.getElementById('assetGirdPagingHeading').style.display='none';
}

function setPageSize(ps)
{
	SetCookie ('IDAMassetgridPageSize',ps, exp);
	<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
	document.getElementById('pagesize20').style.fontWeight='normal';
	document.getElementById('pagesize50').style.fontWeight='normal';
	document.getElementById('pagesize100').style.fontWeight='normal';
	if (ps==20){
	document.getElementById('pagesize20').style.fontWeight='bold';
	}
	if (ps==50){
	document.getElementById('pagesize50').style.fontWeight='bold';
	}
	if (ps==100){
	document.getElementById('pagesize100').style.fontWeight='bold';
	}
}
try{
switch(GetCookie('IDAMassetgridPageSize'))
{
case '20': 
document.getElementById('pagesize20').style.fontWeight='bold';
break; 
case '50': 
document.getElementById('pagesize50').style.fontWeight='bold';
break; 
case '100': 
document.getElementById('pagesize100').style.fontWeight='bold';
break; 											
default: 
document.getElementById('pagesize20').style.fontWeight='bold';
break; 
}  
}catch(ex){}
if (navigator.userAgent.indexOf("Firefox")!=-1){
window.setTimeout('<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView()+","+"initialize")',100); 
}else{

<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView()+","+"initialize");
}








function GridAssets_onCallbackComplete(sender, eventArgs) {
showCustomFooter();
sizeCenterPane();
var p = $('.slide-out-div');
var position = p.position();
var tagpos = ($(window).width() - position.left);
//(sender.get_parameter(0) == 'search' || sender.get_parameter(0) == 'filter') &&
try
{
    if ( tagpos > 50) {
        
        CallbackAssetFiltersNew.Callback('forcebuild'); 
        try {
            $(dialogAssetFiltersNew).dialog('close');
        } catch (ex)
{ } 
    }
    if ((sender.get_parameter(0) == 'projectpagefilter') && tagpos > 50) {
        CallbackFilterCrumbs.Callback();
        CallbackAssetFiltersNew.Callback('forcebuild');
        try {
            $(dialogAssetFiltersNew).dialog('close');
        } catch (ex)
{ } 
    }
}catch(ex){}
return true;
}










function DoSearchFilter(stype,searchval)
{

if (stype=='search'){
ClearFilters();
SetCookie("sFilter", "");
SetCookie ('SearchValueProjectAssets', document.getElementById('quicksearchkeyword').value, exp);
firsttabfilterclick = true;
}
if (stype=='filter'){
SetCookie ('SearchValueProjectAssets', searchval, exp);
}
//stype='';

<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView(),stype,searchval);
//<%response.write( GridAssets.ClientId)%>.Filter(stype.concat(document.getElementById('quicksearchkeyword').value + ' ',document.getElementById('search_filter').value + ' ', searchval));
}











function FilterSecOpCl(section,status)
{
try{
CallbackAssetFiltersNew.Callback('showhidepreview',section,status);
}catch(ex)
{}
}


function FilterProject_Del(pId)
{
try{
SetCookie ('pFilter','', exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterPorjectFilterByID(pIndexID)
{
try{
CallbackAssetFiltersContent.Callback('project','index',pIndexID);
}catch(ex)
{}
}


function FilterProject_Add(pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('pFilter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}




function FilterFolder_Del(pId)
{
try{
SetCookie ('cFilter','', exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterFolderFilterByID(pIndexID)
{
try{
CallbackAssetFiltersContent.Callback('folder','index',pIndexID);
}catch(ex)
{}
}


function FilterFolder_Add(pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('cFilter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}








function FilterTags_Del(pId)
{
try{
var newCookie;
newCookie = '';//GetCookie('tagFilter').replace('@'+pId,'')
SetCookie ('tagFilter',newCookie, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterTagsFilterByID(pIndexID)
{
try{
CallbackAssetFiltersContent.Callback('tags','index',pIndexID);
}catch(ex)
{}
}


function FilterTags_Add(pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('tagFilter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}








function FilterFileType_Del(pId)
{
try{
SetCookie ('tFilter','', exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterFileTypeFilterByID(pIndexID)
{
try{
CallbackAssetFiltersContent.Callback('filetype','index',pIndexID);
}catch(ex)
{}
}


function FilterFileType_Add(pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('tFilter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}








function FilterService_Del(pId)
{
try{
var keyArr = GetCookie('sFilter').split(',');
for (var i=0; i<keyArr.length; i+=1) {
if (pId==keyArr[i]) {
keyArr.splice(i,1);
}
}
SetCookie ('sFilter',keyArr, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterServiceFilterByID(pIndexID)
{
try{
CallbackAssetFiltersContent.Callback('service','index',pIndexID);
}catch(ex)
{}
}


function FilterService_Add(pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('sFilter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}






function FilterMedia_Del(pId)
{
try{
var keyArr = GetCookie('mFilter').split(',');
for (var i=0; i<keyArr.length; i+=1) {
if (pId==keyArr[i]) {
keyArr.splice(i,1);
}
}
SetCookie ('mFilter',keyArr, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterMediaFilterByID(pIndexID)
{
try{
CallbackAssetFiltersContent.Callback('mediatype','index',pIndexID);
}catch(ex)
{}
}


function FilterMedia_Add(pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('mFilter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}






function FilterIllusttype_Del(pId)
{
try{
var keyArr = GetCookie('iFilter').split(',');
for (var i=0; i<keyArr.length; i+=1) {
if (pId==keyArr[i]) {
keyArr.splice(i,1);
}
}
SetCookie ('iFilter',keyArr, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterIllusttypeFilterByID(pIndexID)
{
try{
CallbackAssetFiltersContent.Callback('illusttype','index',pIndexID);
}catch(ex)
{}
}


function FilterIllusttype_Add(pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('iFilter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}



function FilterDyn_Del(tag,type,pId)
{
try{
var newCookie;
newCookie = ''
SetCookie ('dyn'+tag+'Filter',newCookie, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}

function FilterDynFilterByID(tag,type,pIndexID)
{
try{
CallbackAssetFiltersContent.Callback(tag,'index',pIndexID);
}catch(ex)
{}
}

function FilterDyn_Add(tag,type,pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('dyn'+tag+'Filter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}
</script>
