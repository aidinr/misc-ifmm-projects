<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.AssetTaggingD" EnableViewstate="True" CodeBehind="AssetTaggingD.ascx.vb" %>
<script language="javascript" type="text/javascript">							
function BrowseOnSingleClickAssets(item)
  {
	var itemvaluetmp;
	var itemvaluenametmp;
	var itemvaluefiletypetmp;
	itemvaluetmp = item.GetMember('asset_id').Value;
	itemvaluenametmp = item.GetMember('name').Value;
	itemvaluefiletypetmp = item.GetMember('imagesource').Value;
	
	//<%=pAssetImageClientID%>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);

	return true;
  }  

function CallBack1_onCallbackError(sender, eventArgs) 
{
  // event handler logic goes here 
  // eventArgs is an empty EventArgs instance
  alert('ah buh');
}
function CallBack1_onBeforeCallback(sender, eventArgs) 
{
  // event handler logic goes here 
  // eventArgs is of CallBackEventArgs type 
   alert(eventArgs.get_parameter())
}
   
      function resizeGrid(DomElementId, NewPaneHeight, NewPaneWidth)
      {
        <%=pGridAssetsClientID%>.Render();
      }  
      //-->
    </script>	
        <script type="text/javascript">

    // Ensure that folders are grouped together 
    function SortHandler(column, desc)
    {
      // multiple sort, giving the top priority to IsFolder
      <%=pGridAssetsClientID%>.SortMulti([4,!desc,column.ColumnNumber,desc]);

      // re-draw the grid
      <%=pGridAssetsClientID%>.Render();

      // cancel default sort
      return false;
    }
    


function CheckAllItems()
    {
    var itemIndex = 0;
	for (var x = 0; x <= <%=pGridAssetsClientID%>.PageSize; x++)
		{
		 <%=pGridAssetsClientID%>.Select(<%=pGridAssetsClientID%>.Table.GetRow(x),true);
		}

      
      <%=pGridAssetsClientID%>.Render();
    }
    
function CopyTags(item)
{
doCopy = confirm("Copy tags?"); 
if (doCopy)
    {

		<% =pCALLBACKKeywordsServicesClientID %>.Callback();
		//<% =pCALLBACKKeywordsIllustTypeClientID %>.Callback(item);
		//<% =pCALLBACKKeywordsMediaTypeClientID %>.Callback(item);
		//<% =pCALLBACKInformationClientID %>.Callback(item);
		//<% =pCALLBACKUDFMainClientID %>.Callback(item);
	//	<% =pCALLBACKSecurityTagClientID %>.Callback(item);
		
	}
	

}    
    </script>
 <SCRIPT LANGUAGE="JavaScript" SRC="js/CalendarPopup.js"></SCRIPT>

			<div style="PADDING-RIGHT:20px;PADDING-LEFT:20px;PADDING-BOTTOM:1px;PADDING-TOP:20px;POSITION:relative">
				<div>
					<div style="BORDER-RIGHT:1px solid; PADDING-RIGHT:10px; BORDER-TOP:1px solid; PADDING-LEFT:10px; PADDING-BOTTOM:1px; BORDER-LEFT:1px solid; PADDING-TOP:10px; BORDER-BOTTOM:1px solid; POSITION:relative; BACKGROUND-COLOR:white">

						<div style="BORDER-RIGHT:#b7b4b4 1px solid; PADDING-RIGHT:5px; BORDER-TOP:#b7b4b4 1px solid; PADDING-LEFT:5px; FONT-WEIGHT:normal; FONT-SIZE:10px; PADDING-BOTTOM:5px; BORDER-LEFT:#b7b4b4 1px solid; COLOR:#3f3f3f; PADDING-TOP:5px; BORDER-BOTTOM:#b7b4b4 1px solid; FONT-FAMILY:verdana; BACKGROUND-COLOR:#e4e4e4">Tag 
							assets by selecting each asset from the listing below and setting the proper 
							attributes in the information section.&nbsp; Appending the tags means to keep 
							any existing information about the selected assets and adding any new 
							tags.&nbsp; Overwriting the tags means to remove any existing information about 
							the asset and replace with the new tags.&nbsp; Use the splitter window to 
							switch from full asset grid view to preview.&nbsp; You can edit asset 
							information, user defined fields, keywords and security settings from here.</div>
						<br>
						
						
		<ComponentArt:Splitter runat="server" id="Splitter1"  ImagesBaseUrl="images/" >
		
        <Layouts>
		   <ComponentArt:SplitterLayout>
            <Panes Orientation="Horizontal" SplitterBarCollapseImageUrl="splitter_horCol.gif" SplitterBarCollapseHoverImageUrl="splitter_horColHover.gif" SplitterBarExpandImageUrl="splitter_horExp.gif" SplitterBarExpandHoverImageUrl="splitter_horExpHover.gif" SplitterBarCollapseImageWidth="5" SplitterBarCollapseImageHeight="116" SplitterBarCssClass="HorizontalSplitterBarAssetTagging" SplitterBarCollapsedCssClass="CollapsedHorizontalSplitterBar" SplitterBarActiveCssClass="ActiveSplitterBar" SplitterBarWidth="5">
              <ComponentArt:SplitterPane PaneContentId="TreeViewContent" Width="300" MaxWidth="600" AllowScrolling="false" CssClass="SplitterPane" />
              <ComponentArt:SplitterPane Width="100%">
                <Panes Orientation="Vertical" SplitterBarCollapseImageUrl="splitter_verCol.gif" SplitterBarCollapseHoverImageUrl="splitter_verColHover.gif" SplitterBarExpandImageUrl="splitter_verExp.gif" SplitterBarExpandHoverImageUrl="splitter_verExpHover.gif" SplitterBarCollapseImageWidth="116" SplitterBarCollapseImageHeight="5" SplitterBarCssClass="VerticalSplitterBarAssetTagging" SplitterBarCollapsedCssClass="CollapsedVerticalSplitterBar" SplitterBarActiveCssClass="ActiveSplitterBar" SplitterBarWidth="5">
                  
                  <ComponentArt:SplitterPane PaneContentId="GridContent" Height="100%" MinHeight="150"  CssClass="DetailsPane" AllowScrolling="true" />             
                </Panes>
              </ComponentArt:SplitterPane>
            </Panes>
          </ComponentArt:SplitterLayout>  
        </Layouts>
        <Content>
          <ComponentArt:SplitterPaneContent id="TreeViewContent">
            <table class="HeadingCell" style="width:100%;height:25px;" cellpadding="0" cellspacing="0">
              <tr>
                <td class="HeadingCellText">Tags</td>
                <td  align=right style="FONT-SIZE: 10px; FONT-FAMILY: Verdana">[<a href="javascript:closealltags();">close all</a>] [<a href="javascript:openalltags();">open all</a>]</td>
              </tr>
            </table>
           
            
            <div style="padding:5px;padding-right:10px;position:relative;overflow:auto;height:475px;">
            <div id="LeftColumn" >
							<ComponentArt:Snap  ClientSideCookieEnabled="True" ClientSideCookieName="ASSETTAGGINGPAGE_SNAP_TagInformation" id="TagInformation" runat="server" width="400px" MustBeDocked="True" DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagInformation.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
												<td onmousedown="TagInformation.StartDragging(event);" onmouseup="explorecarousel();">Information</td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagInformation.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
												<td onmousedown="TagInformation.StartDragging(event);">Information</td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">




<COMPONENTART:CALLBACK id="CALLBACKInformation" runat="server" CacheContent="false">
											<CONTENT>
<asp:PlaceHolder ID=PlaceholderTagInformation runat=server >
Name:<br>
<asp:TextBox ID="name" TextMode=SingleLine CssClass="InputFieldMain" style="width:90%" runat="server" value=""></asp:TextBox>
<!--<input id="nametmp" type=text class="InputFieldMain" style="width:90%" value="">-->
Description:<br>
<asp:TextBox id="description" TextMode=MultiLine Rows=4 cssclass="InputFieldMain" runat="server" style="width:90%;height:90px"></asp:TextBox>
<!--<textarea id="description" class="InputFieldMain"  style="width:90%;height:90px"></textarea>-->
</asp:PlaceHolder>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div><!--carouselshortlist-->
</div><!--SnapTreeview-->
								</Content>
							</ComponentArt:Snap>
<div  class="SnapHeaderProjectsFiller"></div>	
							<ComponentArt:Snap   ClientSideCookieEnabled="True" ClientSideCookieName="ASSETTAGGINGPAGE_SNAP_TagSecurity" id="TagSecurity" runat="server"  MustBeDocked="True" DockingStyle="TransparentRectangle"  DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagSecurity.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagSecurity.StartDragging(event);" onmouseup="explorecarousel();">Security</td></tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagSecurity.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagSecurity.StartDragging(event);">Security</td></tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>

									<div class="SnapTreeviewTag" style="background-color:white;">	
									<div class="carouselshortlist">
									
										<COMPONENTART:CALLBACK id="CALLBACKSecurityTag" runat="server" CacheContent="false">
											<CONTENT>
												<asp:PlaceHolder ID="PlaceholderSecurityTag" runat="server" >
													Security Level:<br>
													<asp:Literal ID="LiteralSecurityLevelDropdown" Runat=server></asp:Literal><br>
													<asp:CheckBox id="CheckBoxSecurityTag" runat="server" Checked="False"></asp:CheckBox>Force security level change
												</asp:PlaceHolder>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>

									</div><!--carouselshortlist-->
									</div><!--SnapTreeview-->


								</Content>
							</ComponentArt:Snap>		
<div  class="SnapHeaderProjectsFiller"></div>
<ComponentArt:Snap   ClientSideCookieEnabled="True" ClientSideCookieName="ASSETTAGGINGPAGE_SNAP_TagUserDefinedFields" id="TagUserDefinedFields" runat="server"  MustBeDocked="True" DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagUserDefinedFields.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagUserDefinedFields.StartDragging(event);" onmouseup="explorecarousel();">UDF</td></tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagUserDefinedFields.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagUserDefinedFields.StartDragging(event);">UDF</td></tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">


<COMPONENTART:CALLBACK id="CALLBACKUDFMain" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="Literal_UDFMAIN" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>


</div><!--carouselshortlist-->
</div><!--SnapTreeview-->



								</Content>
							</ComponentArt:Snap>
<div  class="SnapHeaderProjectsFiller"></div>	
							<asp:Literal ID="LiteralKeywords1aTitle" Runat=server></asp:Literal><asp:Literal ID="LiteralKeywords1bTitle" Runat=server></asp:Literal>
									
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">
<img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=Services','Keywords_Services','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsServices" runat="server" CacheContent="false">
										  <ClientEvents>
    <CallbackError EventHandler="CallBack1_onCallbackError" />
<BeforeCallback EventHandler="CallBack1_onBeforeCallback" />

  </ClientEvents>

											<CONTENT>

<asp:Literal ID="LiteralKeywords1" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div><!--carouselshortlist-->
</div><!--SnapTreeview-->


<div  class="SnapHeaderProjectsFiller"></div>	
							<ComponentArt:Snap ClientSideCookieEnabled="True" ClientSideCookieName="ASSETTAGGINGPAGE_SNAP_TagKeywordMediaType"  id="TagKeywordMediaType" width="400px"  runat="server" MustBeDocked="True" DockingStyle="TransparentRectangle"  DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagKeywordMediaType.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagKeywordMediaType.StartDragging(event);" onmouseup="explorecarousel();"><asp:Literal ID="LiteralKeywords2aTitle" Runat=server></asp:Literal></td></tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagKeywordMediaType.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagKeywordMediaType.StartDragging(event);"><asp:Literal ID="LiteralKeywords2bTitle" Runat=server></asp:Literal></td></tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">
<img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=MediaType','Keywords_MediaType','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsMediaType" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="LiteralKeywords2" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div><!--carouselshortlist-->
</div><!--SnapTreeview-->



								</Content>
							</ComponentArt:Snap>
							
		
<div  class="SnapHeaderProjectsFiller"></div>	
							<ComponentArt:Snap  ClientSideCookieEnabled="True" ClientSideCookieName="ASSETTAGGINGPAGE_SNAP_TagKeywordIllustType" id="TagKeywordIllustType"  width="400px" runat="server"  MustBeDocked="True" DockingStyle="TransparentRectangle"  DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagKeywordIllustType.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagKeywordIllustType.StartDragging(event);" onmouseup="explorecarousel();"><asp:Literal ID="LiteralKeywords3aTitle" Runat=server></asp:Literal></td></tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagKeywordIllustType.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagKeywordIllustType.StartDragging(event);"><asp:Literal ID="LiteralKeywords3bTitle" Runat=server></asp:Literal></td></tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">
<img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=IllustType','Keywords_IllustType','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsIllustType" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="LiteralKeywords3" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>

</div><!--carouselshortlist-->
</div><!--SnapTreeview-->



								</Content>
							</ComponentArt:Snap>
							
		
							
		
<div  class="SnapHeaderProjectsFiller"></div>	

            </div><!--leftcolumn-->
            </div><!--padding-->
          </ComponentArt:SplitterPaneContent>
          
          
          <ComponentArt:SplitterPaneContent id="ImagePreview"> 
                         <table class="HeadingCell" style="width:100%;height:25px;" cellpadding="0" cellspacing="0">
              <tr>
                <td class="HeadingCellText">Image Preview</td>
              </tr>
            </table>
									<div style="padding:0px;overflow:hidden;padding:5px;background-color:white;">
										<COMPONENTART:CALLBACK id="AssetImage" runat="server" CacheContent="false">
											<CONTENT>
											<asp:Placeholder ID=assetpreview Runat=server>
												<TABLE height="100" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px" width=10>
																	<!--<asp:Literal ID="CurrentImageName" Runat=server></asp:Literal><br>-->
																	<asp:Image id="CurrentImage" runat="server" ImageUrl="images/spacer.gif"></asp:Image></TD>
																	<td width=100% valign=top >
																	<div style= "PADDING-LEFT: 5px; FONT-SIZE: 8pt; FONT-FAMILY: Verdana">
<asp:Literal ID="LiteralAssetInfoSummary" Runat=server></asp:Literal></div>
																	</td>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
												</asp:PlaceHolder>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px">Loading...
																	</TD>
																	<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
									</div>
            
          </ComponentArt:SplitterPaneContent>

          <ComponentArt:SplitterPaneContent id="GridContent"> 
             
      

<div  style="padding:5px;overflow:scroll;height:500px;position:relative;">      

<asp:Literal id="literalNoAssets" Text="No assets available" visible=false runat="server" />

<!--GroupBy="categoryname ASC"-->
											<COMPONENTART:GRID 
												id="GridAssets" 
												ClientSideOnSelect="BrowseOnSingleClickAssets" 
												runat="server" 
												AutoFocusSearchBox="false"
												AutoCallBackOnInsert="true"
												AutoCallBackOnUpdate="true"
												AutoCallBackOnDelete="true"
												pagerposition="2"
												ScrollBar="Off"
												ScrollTopBottomImagesEnabled="true"
												ScrollTopBottomImageHeight="2"
												ScrollTopBottomImageWidth="16"
												ScrollImagesFolderUrl="images/scroller/"
												ScrollButtonWidth="16"
												ScrollButtonHeight="17"
												ScrollBarCssClass="ScrollBar"
												ScrollGripCssClass="ScrollGrip"
												AllowHtmlContent="true" 
												ScrollPopupClientTemplateId="ScrollPopupTemplate" 
												Sort="name desc"
												Height="500px" Width="100%"
												LoadingPanelPosition="TopCenter" 
												LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
												EnableViewState="true"
												GroupBySortImageHeight="10" 
												GroupBySortImageWidth="10" 
												GroupBySortDescendingImageUrl="group_desc.gif" 
												GroupBySortAscendingImageUrl="group_asc.gif" 
												GroupingNotificationTextCssClass="GridHeaderText" 
												AlternatingRowCssClass="AlternatingRowCategory" 
												IndentCellWidth="22" 
												TreeLineImageHeight="19" 
												TreeLineImageWidth="20" 
												TreeLineImagesFolderUrl="images/lines/" 
												PagerImagesFolderUrl="images/pager/" 
												ImagesBaseUrl="images/" 
												PreExpandOnGroup="True" 
												GroupingPageSize="5" 
												PagerTextCssClass="GridFooterTextCategory" 
												PagerStyle="Numbered" 
												GroupByTextCssClass="GroupByText" 
												GroupByCssClass="GroupByCell" 
												FooterCssClass="GridFooter" 
												HeaderCssClass="GridHeader" 
												SearchOnKeyPress="true" 
												SearchTextCssClass="GridHeaderText" 
												AllowEditing="true" 
												AllowSorting="False"
												ShowSearchBox="false" 
												ShowHeader="false" 
												ShowFooter="true" 
												CssClass="Grid" 
												RunningMode="callback" 
												ScrollBarWidth="15" 
												AllowPaging="false" 
												PageSize="1000">

<ClientTemplates>
<ComponentArt:ClientTemplate Id="EditTemplateAssets">
           <a href="javascript:DownloadAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> | <a href="javascript:AddToCarousel('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/3Drafts.gif" alt="Edit" border=0></a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateAssets">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateAssets">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>   
          <ComponentArt:ClientTemplate Id="TypeImageTemplateAssets">
            <div style= "WIDTH: 50px; HEIGHT: 50px; "><img src="## DataItem.GetMember("mainimagesource").Value ##" border="0" ></div>
          </ComponentArt:ClientTemplate>    
          <ComponentArt:ClientTemplate Id="TypeIconTemplateAssets">
            <div style="TEXT-ALIGN: left"><img src="## DataItem.GetMember("imagesource").Value ##" border="0" >&nbsp;## DataItem.GetMember("media_type_name").Value ##</div>
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplateAssets">
            <A href="javascript:CopyTags('## DataItem.ClientId ##');"><img border=0 src="images/copytags.gif"></a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="LookupCategoryTemplateAssets">
            <A href="javascript:GotoLocation('0','## DataItem.GetMember("projectid2").Value ##');">## DataItem.GetMember("projectname2").Value ##</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="LookupKeywordsTemplateAssets">
            <img border=0 src="images/moreinfo.gif" style="CURSOR: hand" alt="## DataItem.GetMember("search_keyword_new").Value ##">## DataItem.GetMember("search_keyword_new").Value ##
          </ComponentArt:ClientTemplate>           
          <ComponentArt:ClientTemplate Id="LookupUDFsTemplateAssets">
            <img border=0 src="images/moreinfo.gif" style="CURSOR: hand" alt="## DataItem.GetMember("search_ukeyword_new").Value ##">## DataItem.GetMember("search_ukeyword_new").Value ##
          </ComponentArt:ClientTemplate>                
          <ComponentArt:ClientTemplate Id="LookupCategoryIconTemplateAssets">
            <img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif">
          </ComponentArt:ClientTemplate>                    
<componentart:ClientTemplate ID="LoadingFeedbackTemplateAssets">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>                                 
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeImageTemplateAssets" dataField="mainimagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="52" FixedWidth="True" />
<ComponentArt:GridColumn DataCellCssClass="DataCell" Align="Center" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplateAssets" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="16" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="false" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="false" DataCellClientTemplateId="LookupUDFsTemplateAssets" HeadingText="UDF's" Width="50" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="search_ukeyword_new" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="false" DataCellClientTemplateId="LookupKeywordsTemplateAssets" HeadingText="Keywords" Width="50" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="search_keyword_new" ></componentart:GridColumn>
<ComponentArt:GridColumn AllowEditing="false" DataCellClientTemplateId="TypeIconTemplateAssets" dataField="imagesource" DataCellCssClass="DataCell" HeadingText="Type" AllowGrouping="false" Width="50" />
<componentart:GridColumn DataCellCssClass="DataCell" Visible="False" HeadingText="Type" AllowEditing="false" Width="50" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Updated" AllowEditing="false" Width="50" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Security Level" AllowEditing="false" Width="80" SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid2"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="mainimagesource"></componentart:GridColumn>

</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
      
      
      
      
      
      
      
      
      

</div>      
      
      

      
      
      
      
      
      
      
      
      
      
      
      
      
          </ComponentArt:SplitterPaneContent>

         
        </Content>          
      </ComponentArt:Splitter>
      
    <div class="HeadingCell" style="WIDTH:100%;HEIGHT:25px" cellpadding="0" cellspacing="0">
              
            </div>  						
						
<br>
<div style="WIDTH: 100%;TEXT-ALIGN: left">
<asp:Button Runat=server ID=Button1 Text="Append to Selected"></asp:Button>&nbsp;<asp:Button Runat=server ID="Button2" Text="Overwrite Selected"></asp:Button></div><br><br>
					</div>
				</div>				
<input type=hidden id="typemod"></div>

