<%@ Reference Control="~/PreviewPane.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.UserLookup" CodeBehind="UserLookup.aspx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
      <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>User</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
		<link href="treeStyle.css" type="text/css" rel="stylesheet">
		<link href="navStyle.css" type="text/css" rel="stylesheet">
		<link href="tabStripStyle.css" type="text/css" rel="stylesheet">
		<link href="navBarStyle.css" type="text/css" rel="stylesheet">
		<style>BODY { MARGIN: 0px;   background-color:white;}
	</style>
</HEAD>
	<body onload="this.focus">
		<form id="Form" method="post" runat="server">
		<SCRIPT LANGUAGE="JavaScript" SRC="js/CalendarPopup.js"></SCRIPT>
			<script type="text/javascript">

  
        // Forces the grid to adjust to the new size of its container          
      function resizeGrid2(DomElementId, NewPaneHeight, NewPaneWidth)
      {
        <% Response.Write(GridUser.ClientID) %>.Render();
        //alert('here');
      } 
  
    function editGridPopup(rowId)
  {
	var rowdata;
	var userid;
	rowdata = <% Response.Write(GridUser.ClientID) %>.GetRowFromClientId(rowId);
	userid = rowdata.GetMember('userid').Value;
    Object_PopUp_User(userid); 
  }
    function editRow(item)
  {

 <% Response.Write(GridUser.ClientID) %>.EditComplete();  

  }
  
  
  





  
  
  
    
  function onInsert(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
    function ShowContextMenu(item, column, evt) 
  {
    <% Response.Write(GridUser.ClientID) %>.Select(item); 
   
    return false; 
  }
  
  function onUpdate(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridUser.ClientID) %>.Page(1); 
  }

  function onDelete(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
 
  function insertRow()
  {
    <% Response.Write(GridUser.ClientID) %>.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    <% Response.Write(GridUser.ClientID) %>.Delete(<% Response.Write(GridUser.ClientID) %>.GetRowFromClientId(rowId)); 
  }



  function onInsertCategory(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  

  
  function onUpdateCategory(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }



  function onDeleteCategory(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
  

  
  

  

</script>


									<div class="SnapProjectsWrapper">
									<div style="padding:25px;">
										<div class="SnapProjectsAssets" >



<div style="padding-top:10px;"><div style="padding:3px;"></div></div>

<table cellpadding=0 cellspacing=0 ><tr><td width="220" nowrap  ></td><td align=right width=100% nowrap ><b><font size=1>Search Users:</font></b><img src="images/spacer.gif" width=5 height=1></td><td align=right width=50 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:70px"  onkeyup="javascript:DoSearchFilter(this.value);"></td></tr></table> 

<div style="padding-top:10px;"><div style="padding:3px;"></div></div>







<COMPONENTART:GRID 

id="GridUser" 


												runat="server" 
												AutoFocusSearchBox="false"
												AutoCallBackOnInsert="true"
												AutoCallBackOnUpdate="true"
												AutoCallBackOnDelete="true"
												pagerposition="2"
												ScrollBar="Off"
												ScrollTopBottomImagesEnabled="true"
												ScrollTopBottomImageHeight="2"
												ScrollTopBottomImageWidth="16"
												ScrollImagesFolderUrl="images/scroller/"
												ScrollButtonWidth="16"
												ScrollButtonHeight="17"
												ScrollBarCssClass="ScrollBar"
												ScrollGripCssClass="ScrollGrip"
												ClientSideOnInsert="onInsert"
												ClientSideOnUpdate="onUpdate"
												ClientSideOnDelete="onDelete"
												ClientSideOnCallbackError="onCallbackError"
												ScrollPopupClientTemplateId="ScrollPopupTemplate" 
												Sort="lastname,firstname asc"
												Height="10" Width="100%"
												LoadingPanelPosition="TopCenter" 
												LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
												EnableViewState="true"
												GroupBySortImageHeight="10" 
												GroupBySortImageWidth="10" 
												GroupBySortDescendingImageUrl="group_desc.gif" 
												GroupBySortAscendingImageUrl="group_asc.gif" 
												GroupingNotificationTextCssClass="GridHeaderText" 
												AlternatingRowCssClass="AlternatingRowCategory" 
												IndentCellWidth="22" 
												TreeLineImageHeight="19" 
												TreeLineImageWidth="20" 
												TreeLineImagesFolderUrl="images/lines/" 
												PagerImagesFolderUrl="images/pager/" 
												ImagesBaseUrl="images/" 
												PreExpandOnGroup="True" 
												GroupingPageSize="5" 
												PagerTextCssClass="GridFooterTextCategory" 
												PagerStyle="Numbered" 
												PageSize="20" 
												GroupByTextCssClass="GroupByText" 
												GroupByCssClass="GroupByCell" 
												FooterCssClass="GridFooter" 
												HeaderCssClass="GridHeader" 
												SearchOnKeyPress="true" 
												SearchTextCssClass="GridHeaderText" 
												AllowEditing="true" 
												AllowSorting="False"
												ShowSearchBox="false" 
												ShowHeader="false" 
												ShowFooter="true" 
												CssClass="Grid" 
												RunningMode="callback" 
												ScrollBarWidth="15" 
												AllowPaging="true">
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
<table height="1px" width="100%" ><tr><td valign="center" align="center">
<table cellspacing="0" cellpadding="0" border="0"  background="images/loadingback.gif" height="70px" width="300px">
<tr>
	<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
	<td style="font-size:10px;font-family:Verdana;" width="60px" align="right">Loading...&nbsp;</td>
	<td><img src="images/spinner2.gif"  border="0"></td>
	<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
</tr>
</table>
</td></tr></table>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplate">
            <a href="javascript:localpopulateparentfields('## DataItem.ClientId ##');">select </a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplate">
            <img src="images/user16.jpg" border="0" > 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateGoto">
            <img src="images/i_rate.gif" border="0" > 
          </ComponentArt:ClientTemplate>                                      
</ClientTemplates>
<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
EditFieldCssClass="EditDataField"
EditCommandClientTemplateId="EditCommandTemplate"
InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="userid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asch10.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desch10.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="10" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center" AllowSorting="False" AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="True" HeadingText="Firstname"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="80" DataField="firstname" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="True" HeadingText="Lastname"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="80" DataField="lastname" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="True" HeadingText="Company"   AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="80" DataField="agency" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="userid"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="email"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="phone"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="fax"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="workaddress"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="workaddress2"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="workaddress3"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="workstate"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="workzip"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="workcity"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="workcountry"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="cell"></componentart:GridColumn>
<ComponentArt:GridColumn AllowSorting="False" HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand"  Width="80" Align="Center" DataCellCssClass="LastDataCellPostings" />
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
 <script type=text/javascript>
  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

  
  function BrowseOnDoubleClick(SelectedGridRow)
  {
  
  	var itemvaluetmp;
	itemvaluetmp = SelectedGridRow.GetMember('uniqueid').Value;
	if (itemvaluetmp.substring(0,1) == 'P') 
	{
		itemvaluetmp = itemvaluetmp.replace('P_','');
		window.location.href = 'IDAM.aspx?page=Project&id=' + itemvaluetmp + '&type=project'
    
	} else {
		itemvaluetmp = itemvaluetmp.replace('C_','');
		window.location.href = 'IDAM.aspx?page=Browse&id=' + itemvaluetmp + '&type=category';
	}
	
	return true;
  
  
  
	
  }
  
    function highlightProjectToggle(checkbox,projectid)
  {
  var highlightbox;
  highlightbox = document.getElementById('projecthighlight_' + projectid);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  
  

  


function DoSearchFilter(searchvalue)
{

<%response.write (GridUser.ClientID)%>.Filter("agency LIKE '%" + searchvalue + "%' OR firstname LIKE '%" + searchvalue + "%' OR lastname LIKE '%" + searchvalue + "%'");
<%response.write (GridUser.ClientID)%>.Render();

}

function localpopulateparentfields(rowId)
{
	var rowdata;
	rowdata = <% Response.Write(GridUser.ClientID) %>.GetRowFromClientId(rowId);
	window.parent.populateparentfields(rowdata);
	window.parent.CloseDialogWindowX();
}
  </script>
</div>   
</div><!--END browsecenter-->
</div><!--END MAIN-->
</form>
	</body>
</HTML>

