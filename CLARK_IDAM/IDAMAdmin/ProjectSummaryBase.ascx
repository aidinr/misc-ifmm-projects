<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.ProjectSummaryBase" enableViewState="False" CodeBehind="ProjectSummaryBase.ascx.vb" %>
<table border="0" width="100%" id="">
	<tr>
		<td width="1" align="left" valign="top">
			<asp:Literal ID="LiteralProjectSummaryImage" Runat="server"></asp:Literal></div>
		</td>
		<td align="left" valign="top">
			<div style="padding-left:10px;width:95%;">
				<asp:Literal ID="LiteralProjectSummary" Runat="server"></asp:Literal></div>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="left" valign="top"></td>
	</tr>
</table>
<img src="images/spacer.gif" width="188" height="1">