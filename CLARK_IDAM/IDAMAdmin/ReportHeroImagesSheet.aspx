<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.ReportHeroImagesSheet" CodeBehind="ReportHeroImagesSheet.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>ReportHeroImagesSheet</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <style>div.break {page-break-before:always}</style> 
  </head>
  <body topmargin="0" style="font-size: 9px;" leftmargin="0">

    <form id="Form1" method="post" runat="server">
<font face="Verdana" >
<%

'Check Login - Put this in and inc file
dim xWidth,xpWidth,iProjectID,sqltemp,sLineImage,ThumbnailImageHeight,ThumbnailImageWidth as string
sLineImage = "images/line_som.jpg"
iProjectID = replace(request.querystring("ID"),"0 ","")

xWidth = 3
xpWidth = 3


Dim Conn As New ADODB.Connection
Conn.Open(ConnectionString)

Dim rsAssets,rsAssetsUDF,rsAssetInfoProject,rsAssetInfo As ADODB.Recordset

rsAssets=server.createobject("adodb.recordset") 
rsAssetsUDF=server.createobject("adodb.recordset")
rsAssetInfoProject=server.createobject("adodb.recordset")

sqltemp = "select name, descriptionmedium from ipm_project where projectid =  " & iProjectID 
rsAssetInfoProject.Open (sqltemp, Conn, 1, 4)
if not rsAssetInfoProject.EOF then
	'title
	%>
	<div style="padding:5px"><%
	response.write ("<b>" & rsAssetInfoProject("name").Value & "</b><br>")
	%><%
end if

dim sThumbLocationID,sThumbAssetID,sThumbDescription ,sThumbName,sThumbProjectName
dim sThumbRepo as string
dim irowcount,pagebreak,xHeight,acount,iout,iin,sPage,sType,strSubLocationTmp,strSubLocation,strSubLocation1,strSubLocation2 

rsAssetInfo=server.createobject("adodb.recordset")














'handle hero image.
	%>
	
	

	
	
	
	<div style="padding:0px">
	<b>Hero Image:</b>
	</div><%
sqltemp = "select a.asset_id from ipm_asset_services a, ipm_asset b where b.projectid = " & iProjectID & " and a.keyid = 21577306 and a.asset_id = b.asset_id and b.available = 'Y'"
rsAssets.Open (sqltemp, Conn, 1, 4)
if rsAssets.recordcount > 0 then
irowcount = 0
xWidth = 2
select case xWidth 
	case 2
		ThumbnailImageHeight =299
		ThumbnailImageWidth = 299
		pagebreak = 2
	case 3
		ThumbnailImageHeight =200
		ThumbnailImageWidth = 200
		pagebreak = 3
	case 4
		ThumbnailImageHeight =120
		ThumbnailImageWidth = 120
		pagebreak = 4
end select
xHeight = rsAssets.recordcount / xWidth  + 1

aCount = 0
%>
         <%if rsAssets.recordcount >= 1 then%>
         <%for iout = 1 to xHeight%>  
              <table align=left border="0" cellspacing="1" width="5" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                <tr>
                	<%for iin = 1 to xWidth %>
                	<%if not rsAssets.EOF then %>
<%'get asset information
if rsAssetInfo.state then rsAssetInfo.close

sqltemp = "select wpixel,hpixel,description,media_type,asset_id,name,filesize,repository_id,location_id,projectid,update_date from ipm_asset where asset_id = " & rsAssets("asset_id").Value
sPage = "asset_view.asp"
sType = "asset"

rsAssetInfo.Open (sqltemp, Conn, 1, 4)
if rsAssetInfo.eof then
sThumbRepo = 0
sThumbAssetID = rsAssets("asset_id").Value
sThumbLocationID = rsAssets("location_ID").Value
sThumbDescription = rsAssets("description").Value
sThumbName = rsAssets("name").Value
sThumbProjectName = "N/A"
else
sThumbRepo = rsAssetInfo("repository_id").Value
sThumbAssetID = rsAssetInfo("asset_id").Value
sThumbLocationID = rsAssetInfo("location_id").Value
sThumbDescription = rsAssetInfo("description").Value
sThumbName = rsAssetInfo("name").Value
sThumbProjectName = ""
end if
%>                	
                  <td width="50" valign="top">
                    <div align="left">
                      <table class="bluelightlight" border="0" cellspacing="0" width="15">
                        <tr>
                          <td class="bluelightlight" valign="top">
                          <div align="left">
                            <table class="bluelightlight" border="0" cellspacing="0" width="5" cellpadding="0">
                              <tr class="bluelightlight">
				<%
				select case sThumbRepo
					case "0"
						strSubLocationTmp = strSubLocation 
					case "1"
						strSubLocationTmp = strSubLocation1
					case "2"
						strSubLocationTmp = strSubLocation2
				end select%>
	
       <td class="bluelightlight" height="<%=ThumbnailImageHeight%>" width="<%=ThumbnailImageWidth%>" valign="middle" align="center"><img border="0" src="images/spacer.gif" width="<%=ThumbnailImageWidth%>" height="0"><img alt="" border="0" src="
  
  		http://<%response.write(ProjectImageLocation)%>/<%response.write(WSLocationClient)%>/RetrieveAsset.aspx?id=<%=sThumbAssetID%>&instance=<%response.write(IDAMInstance)%>&type=asset&size=1&width=<%=800%>&height=<%=600%>&Cache=1&Public=1">

                                                               </td>
                                <td class="bluelightlight" width="5" valign="top">
                                </td>
                				<td class="bluelightlight" width="0" valign="top"><img border="0" src="images/spacer.gif" width="0" height="<%=ThumbnailImageHeight%>">
                				</td>
                              </tr>
                            </table>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td >                          
                            <table border="0" width="100%" bordercolor="#C0C0C0" style="border-collapse: collapse">
							<tr>
								<td colspan="3">
								<table border="0" width="100%" cellspacing="1" cellpadding="0">
								<tr>
								<td valign=top width=1>
								</td>
								<td>
								<img border="0" src="dash.jpg" width="4" height="4"><img border="0" src="images/spacer.gif" width="3" height="3"><font color="#808080" style="font-size: 9px;"><%=left(sThumbName,45)%></font><br>
								<img border="0" src="dash.jpg" width="4" height="4"><img border="0" src="images/spacer.gif" width="3" height="3"><font color="#808080" style="font-size: 9px;"><%=rsAssetInfo("wpixel").Value%> X <%=rsAssetInfo("hpixel").Value%></font><br>
	                       	  </td>
								</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td>
							
	                       	  </td>
								<td></td>
								
							</tr>
							</table>
							</td>
                        </tr>
                      </table>
                    </div>
                   <img border="0" src="images/spacer.gif" width="135" height="8"></td>
                  <%rsAssets.movenext
                  end if
                  next%>
                  </tr>
              </table>
           <%if (iout mod (pagebreak) = 0) and not rsAssets.eof then 
			response.write ("<DIV style=""page-break-after: always;""></DIV>") 
           end if%>
         	<%next%>
         	<%else%>
              
         	<%end if%>
<!--</td></tr></table>-->
<%
irowcount = irowcount + 1
%>
<%end if%>      


<br><img src="images/spacer.gif" width="200" height="1"><br>






	<%response.write ("<DIV style=""page-break-after: always;""></DIV>") %>

	



<%
'handle Gallery images.
	%>
	<div style="padding:0px">
	<b>Gallery Image(s):</b>
	</div><%
rsAssets.close	
sqltemp = "select a.asset_id from ipm_asset_services a, ipm_asset b where b.projectid = " & iProjectID & " and a.keyid = 21576095 and a.asset_id = b.asset_id and b.available = 'Y'"
rsAssets.Open (sqltemp, Conn, 1, 4)
if rsAssets.recordcount > 0 then
irowcount = 0
xWidth = 2
select case xWidth 
	case 2
		ThumbnailImageHeight =299
		ThumbnailImageWidth = 299
		pagebreak = 2
	case 3
		ThumbnailImageHeight =200
		ThumbnailImageWidth = 200
		pagebreak = 3
	case 4
		ThumbnailImageHeight =120
		ThumbnailImageWidth = 120
		pagebreak = 4
end select
xHeight = rsAssets.recordcount / xWidth  + 1
aCount = 0
%>
         <%if rsAssets.recordcount >= 1 then%>
         <%for iout = 1 to xHeight%>  
              <table align=center border="0" cellspacing="1" width="5" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                <tr>
                	<%for iin = 1 to xWidth %>
                	<%if not rsAssets.EOF then %>
<%'get asset information
if rsAssetInfo.state then rsAssetInfo.close
sqltemp = "select wpixel,hpixel,description,media_type,asset_id,name,filesize,repository_id,location_id,projectid,update_date from ipm_asset where asset_id = " & rsAssets("asset_id").Value
sPage = "asset_view.asp"
sType = "asset"

rsAssetInfo.Open (sqltemp, Conn, 1, 4)
if rsAssetInfo.eof then
sThumbRepo = 0
sThumbAssetID = rsAssets("asset_id").Value
sThumbLocationID = rsAssets("location_ID").Value
sThumbDescription = rsAssets("description").Value
sThumbName = rsAssets("name").Value
sThumbProjectName = "N/A"
else
sThumbRepo = rsAssetInfo("repository_id").Value
sThumbAssetID = rsAssetInfo("asset_id").Value
sThumbLocationID = rsAssetInfo("location_id").Value
sThumbDescription = rsAssetInfo("description").Value
sThumbName = rsAssetInfo("name").Value
sThumbProjectName = ""
end if
%>                	
                  <td width="50" valign="top">
                    <div align="left">
                      <table class="bluelightlight" border="0" cellspacing="0" width="15">
                        <tr>
                          <td class="bluelightlight" valign="top">
                          <div align="left">
                            <table class="bluelightlight" border="0" cellspacing="0" width="5" cellpadding="0">
                              <tr class="bluelightlight">
				<%
				select case sThumbRepo
					case "0"
						strSubLocationTmp = strSubLocation 
					case "1"
						strSubLocationTmp = strSubLocation1
					case "2"
						strSubLocationTmp = strSubLocation2
				end select%>
	
       <td class="bluelightlight" height="<%=ThumbnailImageHeight%>" width="<%=ThumbnailImageWidth%>" valign="middle" align="center"><img border="0" src="images/spacer.gif" width="<%=ThumbnailImageWidth%>" height="0"><img alt="" border="0" src="
  
  		http://<%response.write(ProjectImageLocation)%>/<%response.write(WSLocationClient)%>/RetrieveAsset.aspx?id=<%=sThumbAssetID%>&instance=<%response.write(IDAMInstance)%>&type=asset&size=1&width=<%=ThumbnailImageWidth%>&height=<%=ThumbnailImageHeight%>&Cache=1&Public=1">

                                                               </td>
                                <td class="bluelightlight" width="5" valign="top">
                                </td>
                				<td class="bluelightlight" width="0" valign="top"><img border="0" src="images/spacer.gif" width="0" height="<%=ThumbnailImageHeight%>">
                				</td>
                              </tr>
                            </table>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td >                          
                            <table border="0" width="100%" bordercolor="#C0C0C0" style="border-collapse: collapse">
							<tr>
								<td colspan="3">
								<table border="0" width="100%" cellspacing="1" cellpadding="0">
								<tr>
								<td valign=top width=1>
								</td>
								<td>
								<img border="0" src="dash.jpg" width="4" height="4"><img border="0" src="images/spacer.gif" width="3" height="3"><font color="#808080" style="font-size: 9px;"><%=left(sThumbName,45)%></font><br>
								<img border="0" src="dash.jpg" width="4" height="4"><img border="0" src="images/spacer.gif" width="3" height="3"><font color="#808080" style="font-size: 9px;"><%=rsAssetInfo("wpixel").Value%> X <%=rsAssetInfo("hpixel").Value%></font><br>
	                       	  </td>
								</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td>
							
	                       	  </td>
								<td></td>
								
							</tr>
							</table>
							</td>
                        </tr>
                      </table>
                    </div>
                   <img border="0" src="images/spacer.gif" width="135" height="8"></td>
                  <%rsAssets.movenext
                  end if
                  next%>
                  </tr>
              </table>
           <%if (iout mod (pagebreak) = 0) and not rsAssets.eof then 
			response.write ("<DIV style=""page-break-after: always;""></DIV>") 
				%>
	<div style="padding:0px">
	<b>Gallery Image(s):</b>
	</div><%
           end if%>
         	<%next%>
         	<%else%>
              
         	<%end if%>
<!--</td></tr></table>-->
<%
irowcount = irowcount + 1
%>
<%end if%>      

























	<%response.write ("<DIV style=""page-break-after: always;""></DIV>") %>

	



<%
'handle remaining images.
	%>
	<div style="padding:0px">
	<b>All Image(s):</b>
	</div><%
rsAssets.close	
sqltemp = "select asset_id from ipm_asset where projectid = " & iProjectID & " and media_type in (select media_type from ipm_filetype_lookup where (useimageconvert = 1 and useicon = 0) OR media_type = 2404) and available = 'Y'"
rsAssets.Open (sqltemp, Conn, 1, 4)
if rsAssets.recordcount > 0 then
irowcount = 0
xWidth = 3
select case xWidth 
	case 2
		ThumbnailImageHeight =299
		ThumbnailImageWidth = 299
		pagebreak = 2
	case 3
		ThumbnailImageHeight =200
		ThumbnailImageWidth = 200
		pagebreak = 3
	case 4
		ThumbnailImageHeight =120
		ThumbnailImageWidth = 120
		pagebreak = 4
end select
xHeight = rsAssets.recordcount / xWidth  + 1
aCount = 0
%>
         <%if rsAssets.recordcount >= 1 then%>
         <%for iout = 1 to xHeight%>  
              <table align=center border="0" cellspacing="1" width="5" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                <tr>
                	<%for iin = 1 to xWidth %>
                	<%if not rsAssets.EOF then %>
<%'get asset information
if rsAssetInfo.state then rsAssetInfo.close
sqltemp = "select wpixel,hpixel,description,media_type,asset_id,name,filesize,repository_id,location_id,projectid,update_date from ipm_asset where asset_id = " & rsAssets("asset_id").Value
sPage = "asset_view.asp"
sType = "asset"

rsAssetInfo.Open (sqltemp, Conn, 1, 4)
if rsAssetInfo.eof then
sThumbRepo = 0
sThumbAssetID = rsAssets("asset_id").Value
sThumbLocationID = rsAssets("location_ID").Value
sThumbDescription = rsAssets("description").Value
sThumbName = rsAssets("name").Value
sThumbProjectName = "N/A"
else
sThumbRepo = rsAssetInfo("repository_id").Value
sThumbAssetID = rsAssetInfo("asset_id").Value
sThumbLocationID = rsAssetInfo("location_id").Value
sThumbDescription = rsAssetInfo("description").Value
sThumbName = rsAssetInfo("name").Value
sThumbProjectName = ""
end if
%>                	
                  <td width="50" valign="top">
                    <div align="left">
                      <table class="bluelightlight" border="0" cellspacing="0" width="15">
                        <tr>
                          <td class="bluelightlight" valign="top">
                          <div align="left">
                            <table class="bluelightlight" border="0" cellspacing="0" width="5" cellpadding="0">
                              <tr class="bluelightlight">
				<%
				select case sThumbRepo
					case "0"
						strSubLocationTmp = strSubLocation 
					case "1"
						strSubLocationTmp = strSubLocation1
					case "2"
						strSubLocationTmp = strSubLocation2
				end select%>
	
       <td class="bluelightlight" height="<%=ThumbnailImageHeight%>" width="<%=ThumbnailImageWidth%>" valign="middle" align="center"><img border="0" src="images/spacer.gif" width="<%=ThumbnailImageWidth%>" height="0"><img alt="" border="0" src="
  
  		http://<%response.write(ProjectImageLocation)%>/<%response.write(WSLocationClient)%>/RetrieveAsset.aspx?id=<%=sThumbAssetID%>&instance=<%response.write(IDAMInstance)%>&type=asset&size=1&width=<%=ThumbnailImageWidth%>&height=<%=ThumbnailImageHeight%>&cache=1&Public=1">

                                                               </td>
                                <td class="bluelightlight" width="5" valign="top">
                                </td>
                				<td class="bluelightlight" width="0" valign="top"><img border="0" src="images/spacer.gif" width="0" height="<%=ThumbnailImageHeight%>">
                				</td>
                              </tr>
                            </table>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td >                          
                            <table border="0" width="100%" bordercolor="#C0C0C0" style="border-collapse: collapse">
							<tr>
								<td colspan="3">
								<table border="0" width="100%" cellspacing="1" cellpadding="0">
								<tr>
								<td valign=top width=1>
								</td>
								<td>
								<img border="0" src="dash.jpg" width="4" height="4"><img border="0" src="images/spacer.gif" width="3" height="3"><font color="#808080" style="font-size: 9px;"><%=left(sThumbName,45)%></font><br>
								<img border="0" src="dash.jpg" width="4" height="4"><img border="0" src="images/spacer.gif" width="3" height="3"><font color="#808080" style="font-size: 9px;"><%=rsAssetInfo("wpixel").Value%> X <%=rsAssetInfo("hpixel").Value%></font><br>
	                       	  </td>
								</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td>
							
	                       	  </td>
								<td></td>
								
							</tr>
							</table>
							</td>
                        </tr>
                      </table>
                    </div>
                   <img border="0" src="images/spacer.gif" width="135" height="8"></td>
                  <%rsAssets.movenext
                  end if
                  next%>
                  </tr>
              </table>
           <%if (iout mod (pagebreak) = 0) and not rsAssets.eof then 
			response.write ("<DIV style=""page-break-after: always;""></DIV>") 
				%>
	<div style="padding:0px">
	<b>All Image(s):</b>
	</div><%
           end if%>
         	<%next%>
         	<%else%>
              
         	<%end if%>
<!--</td></tr></table>-->
<%
irowcount = irowcount + 1
%>
<%end if%>      

















































</div>












    </form>

  </body>
</html>
