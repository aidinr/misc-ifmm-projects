<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.RoleItemEdit" ValidateRequest="False" CodeBehind="RoleItemEdit.aspx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Create/Edit Role Item</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
			<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
			<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
				<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
					<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
						<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
							<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
								<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
									<link href="treeStyle.css" type="text/css" rel="stylesheet">
										<link href="navStyle.css" type="text/css" rel="stylesheet" >
										    <link href="tabStripStyle.css" type="text/css" rel="stylesheet" />    
												<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
										<style>BODY { MARGIN: 0px }
	</style>
	<body onload="this.focus">
		<form id="Form" method="post" runat="server">
		
		
		
		<script language=javascript>

  

  

  function onInsertCategory(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }

  function onUpdateCategory(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }


  


</script>
<script type="text/javascript">
function nodeSelect(node)
{
document.getElementById('item_group').value = node.Text; 
Menu1.Hide(); 
}
</script>
		
<script type="text/javascript">
  

<%if request.querystring("newid") <> "" then%>
//window.opener.NavigateToCategory('<%response.write (request.querystring("newid"))%>');
//window.close();
<%end if%>

<%if instr(request.querystring("id"),"CAT") = 0  then%>
<%if request.querystring("parent_id") = ""  then%>
//window.close();
<%end if%>
<%end if%>

</script>

			
			<input type="hidden" name="catid" id="catid" value="<%response.write (replace(request.querystring("id"),"CAT",""))%>">
			<input type="hidden" name="parent_id" id="parent_id" value="<%response.write (request.querystring("parent_id"))%>">
			<div style="padding:20px;position:relative;">
			<div style="width:100%">
			<div style="padding:10px;border:1px solid;background-color:white;position:relative;">
			<table><tr><td><img src="images/top_folder.gif"></td><td>Create/Edit 
				Role Item</td></tr></table><br>

  <div style="width=100%">

			
			
			
	
			
			
			
			
			
	<COMPONENTART:TABSTRIP id="TabStripCategory" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataRoleSet.xml"
		CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
		DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
		<ItemLooks>
			<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10" 
 LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif" 
 HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23" 
 LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
			<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10" 
 LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif" 
 LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
		</ItemLooks>
	</COMPONENTART:TABSTRIP>
	
	<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageCategoryPage" runat="server">
		<ComponentArt:PageView CssClass="PageContent2" runat="server" ID="Pageview1">
			<div style="width:100%;padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Create/Modify a Role Item.  </div>
			<br>
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td width="120" align="left" nowrap>
						<font face="Verdana" size="1">Role Item Name </font>
					</td>
					<td align="left" valign="top" width=100%>
						<asp:TextBox id="categoryName" runat="server" Width="99%"></asp:TextBox></td>
				</tr>
				<tr>
	
	
					<td class="PageContent" width="120" align="left" nowrap>
						<font face="Verdana" size="1">Group</font></td>

	
					<td class="PageContent"  width="180%" align="left" valign="top">
	


<table cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td><asp:TextBox id="item_group" runat="server" style="width:200px;"/></td>
      <td><ComponentArt:Menu id="Menu1" 
      Style="position:relative;left:-1px;"
      ExpandOnClick="true"
      ImagesBaseUrl="images/"
      width="17" height="20"
      runat="server">
    <Items>
      <ComponentArt:MenuItem id="item1" Look-ImageUrl="dropdown.gif" Look-CssClass="Empty" SubGroupExpandDirection="BelowRight" SubGroupExpandOffsetX="2" SubGroupExpandOffsetY="-2" Look-Width="17" Look-Height="20" >
        <ComponentArt:MenuItem TemplateId="TreeViewTemplate" Look-CssClass="Empty" Id="TreeViewItem" />
      </ComponentArt:MenuItem> 
    </Items>
    <Templates>    
      <ComponentArt:NavigationCustomTemplate id="TreeViewTemplate">
        <Template>
          <ComponentArt:TreeView id="TreeViewGroups" Height="100%" Width="216" 
            DragAndDropEnabled="false" 
            NodeEditingEnabled="false"
            KeyboardEnabled="true"
            CssClass="TreeView" 
            NodeCssClass="TreeNode" 
            SelectedNodeCssClass="SelectedTreeNode" 
            HoverNodeCssClass="HoverTreeNode"
            NodeEditCssClass="NodeEdit"
            LineImageWidth="19" 
            LineImageHeight="20"
            DefaultImageWidth="16" 
            DefaultImageHeight="16"
            ItemSpacing="0" 
            NodeLabelPadding="3"
            
            ShowLines="false" 
            LineImagesFolderUrl="images/lines/"
            EnableViewState="true"
            ClientSideOnNodeSelect="nodeSelect"
            SiteMapXmlFile="treeDataGroupDropdown.xml"
            runat="server" />
        </Template>
      </ComponentArt:NavigationCustomTemplate>
    </Templates>
    </ComponentArt:Menu></td>
    </tr>
    </table>



</td>
		
				</tr>
				<tr>
	
	
					<td class="PageContent" width="120" align="left" nowrap>
						<font face="Verdana" size="1">Assign to Object</font></td>

	
						<td class="PageContent"  width="180%" align="left" valign="top">
	
	
						<asp:DropDownList ID="Object_Type" width="216" Runat="server">
							<asp:ListItem Value="0">None</asp:ListItem>
							<asp:ListItem Value="1">User</asp:ListItem>
							<asp:ListItem Value="2">Project</asp:ListItem>
							<asp:ListItem Value="3">Asset</asp:ListItem>
							<asp:ListItem Value="4">Application</asp:ListItem>
							<asp:ListItem Value="5">Projects and Assets (Special)</asp:ListItem>
						</asp:DropDownList></td>

				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" nowrap>
						<font face="Verdana" size="1">Allow Use</font></td>
					<td class="PageContent"  width="180%" align="left" valign="top">
	
	
<asp:CheckBox  Runat=server id="chkactive"></asp:CheckBox></td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">Description</font></td>
					<td class="PageContent" align="left" valign="top"  width=100%>
						<asp:TextBox id="categoryDescription" TextMode="MultiLine" runat="server" Height="50px" Width="99%"></asp:TextBox></td>
				</tr>
			</table>
			
			
			
			<table border="0" width="100%" id="table2">
										<tr>
											<td width="116"><b><font face="Verdana" size="1">Data Type</font></b></td>
											<td><hr>
											</td>
										</tr>
									</table>
			




<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td width="120" align="left" nowrap>
						<font face="Verdana" size="1">Tag </font>
					</td>
					<td align="left" valign="top" width=100%>
	
						<asp:TextBox id="Item_Tag" runat="server" Width="99%"></asp:TextBox>
			





					</td>
				</tr>
				<tr>
	
	
					<td class="PageContent" width="120" align="left" nowrap>
						<font face="Verdana" size="1">Default Value</font></td>

	
					<td class="PageContent"  width="180%" align="left" valign="top">
					<asp:CheckBox  Runat=server id="Default_Value"></asp:CheckBox> Allow</td>
		
				</tr>
				</table>
			




			
		</ComponentArt:PageView>
		
	</ComponentArt:MultiPage>
	<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
					<div style="text-align:right;"><br>
			<asp:Button id="btnCategorySave" runat="server" Text="Save"></asp:Button><img src="images/spacer.gif" width=5 height=1><input type="button" onclick="window.opener.RefreshRoles('1');window.close();" value="Close" />
			</div>
	</div>
</div>

			
			
			
			

			</div>
			
			
			
			
			
			
			
			
			</div>
			

			
			</div>
			</div>
			
		
			
			
		</form>
	</body>
</HTML>
