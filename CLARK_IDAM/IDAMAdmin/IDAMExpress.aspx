<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.IDAMExpress" EnableViewState="True" ValidateRequest="False" CodeBehind="IDAMExpress.aspx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>IDAMExpress</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
			<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
			<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
				<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
					<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
						<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
							<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
								<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
									<link href="treeStyle.css" type="text/css" rel="stylesheet">
										<link href="navStyle.css" type="text/css" rel="stylesheet" >
										    <link href="tabStripStyle.css" type="text/css" rel="stylesheet" />    
												<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
										<style>BODY { MARGIN: 0px }
	</style>
	<body onload="this.focus">
		<form id="Form" method="post" runat="server">
			<script type="text/javascript">
			

			
			
  function showlogs(item)
  {
  Object_PopUp('IDAMExpressLog.aspx?sid='+item,'Logs',700,1000);
  }
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridAssets.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    <%response.write(GridAssets.ClientID)%>.Edit(<%response.write(GridAssets.ClientID)%>.GetRowFromClientId(rowId)); 
  }
  
    function editGridHistory(rowId)
  {
    <%response.write(GridHistory.ClientID)%>.Edit(<%response.write(GridHistory.ClientID)%>.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    <%response.write(GridAssets.ClientID)%>.EditComplete();     
  }
  
    function editRowhistory()
  {
    <%response.write(GridHistory.ClientID)%>.EditComplete();     
  }

  function insertRow()
  {
    <%response.write(GridAssets.ClientID)%>.EditComplete(); 
  }



  function deleteRow(ids)
  {

  //check to see if multi select
	var sAssets;
	var addtype;
	
    sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();

	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Delete selected assets?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (arraylist == ''){
						if (sAssets[i]!=null){
							arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
						}
					} else {
						if (sAssets[i]!=null){
							if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
								arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
							}
						}
					}
				}
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				addtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	addtype = 'single';
	}


	if (addtype == 'multi') {
		if (arraylist != ''){
		
		<%response.write( GridAssets.ClientId)%>.Filter("DELETE " + arraylist);
		<%response.write( GridAssets.ClientId)%>.Page(0);

		}
	}
	if (addtype == 'single') {
	/*assume single*/
      if (confirm("Delete this asset?")){
       <% Response.Write(GridAssets.ClientID) %>.Delete(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(ids));

        }
	} 
  }

<%if request.querystring("newid") <> "" then%>
window.opener.NavigateToCarousel('<%response.write (request.querystring("newid"))%>');
//window.close();
<%end if%>

  <%if instr(request.querystring("id"),"CAR") = 0 then%>
	//alert ('Error: ' & request.querystring("id"));
	//window.close();
  <%end if%>
  
  
  
  
  
  
  
  
  
  
  

//submit contact sheet
function submitContactSheet() {
	//get all selected values...
	var field;
	var arraylist=new Array();

	var arraylist;
	var i;
	var sAssets;
	var downloadtype;
	arraylist = '';
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	document.Form.asset_array.value = arraylist;

	if (arraylist.split(',').length > 1)
		{
			document.Form.method="GET";
			document.Form.action="contact_sheet.asp";
			document.Form.target="_blank";
			document.Form.submit();
			document.Form.method="POST";
			document.Form.action="";
			document.Form.target="";

		} else
			alert('Nothing selected');
			return false;
		{
	
	}
}
  
  
  

  
			</script>
			
			
			
			
			
			
			
			
			
			
		
<script language="JavaScript" type="text/JavaScript">




function MM_findObj(n, d) { //v3.0
var p,i,x; if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}


function CheckAll(fieldName,Num) { //Set All checkboxes, if un-checked, check them.
if (MM_findObj(fieldName).length > 1) { 
var checks = MM_findObj(fieldName)
var bcheck = MM_findObj(fieldName)[1].checked
if (bcheck) {
bcheck = false;
} else {
bcheck = true;
}
for (i=0; i<checks.length; i++) {
MM_findObj(fieldName)[i].checked = bcheck ; 
}

}
}


  
function openpopup(assetid,x,y){
var popurl= "image_pop.asp?a=" + assetid + "&c=<%=request.querystring("c")%>" + "&p=<%=request.querystring("p")%>"
var xwidth = x
var yheight = y
winpops=window.open(popurl,"","width=" + xwidth + ",height=" + yheight + ",location=no,")
}

function CheckAllItemsThumbs() { //Set All checkboxes, if un-checked, check them.
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
var dummyobjectvalue = checks[0].value;
var firstobjectcheckedstate = document.getElementById('aselect_' + dummyobjectvalue);
var bcheck = firstobjectcheckedstate.checked

if (bcheck) {
bcheck = true;
} else {
bcheck = false;
}

for (i=0; i<checks.length; i++) {
MM_findObj('aselect_' + checks[i].value).checked = bcheck ;
highlightAssetToggleII(document.getElementById('aselect_' + checks[i].value),checks[i].value); 
}

}
}


function ShowSlideShow(){
	var response = confirm('Show slideshow fullscreen?');
	if (response) {
		window.open("./slideshow/slideshow.asp?car=<%=Request.QueryString("Carousel_id")%>&uid=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.UserID.tostring%>&usl=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.userAccess.ToString%>","","width=1024,height=800,fullscreen=yes,location=no,");
	} else {
		window.open("./slideshow/slideshow.asp?car=<%=Request.QueryString("Carousel_id")%>&uid=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.UserID.tostring%>&usl=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.userAccess.ToString%>","","width=1024,height=800,location=no,");
	}
}



//setup form switch for delete or multi asset zip download
function DownloadPPT(ids){
	var arraylist;
	var i;
	var sAssets;
	var downloadtype;
	arraylist = '';
	ids = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
	
	
	/*download single*/
	downloadtype = 'single';

	if (arraylist.split(',').length > 1) {
	/*assume single*/
	
	window.open('<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLLocationIDAMDownloadPPT & "?assetids="%>' + arraylist + '<%="&instanceid=" & IDAMInstance%>','ConvertingImage','width=500,height=75,location=no');
	//return true;
	
	}

}

<%if webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.userid.tostring = "" then%>
window.close()
<%end if%>




 
function showMenu(id, eventObj) {

    var elmobj = document.getElementById(id);
    var xoffsettype = 260;
   	if (id!='emaillookpupdropdown')
	{
	xoffsettype += 35;
	//hide others
	document.getElementById('emaillookpupdropdown').style.display='none';
	} else
	{
	//document.getElementById('categorylookpupdropdown').style.display='none';
	}
	ypos=215;
	xpos=162;
    elmobj.style.posLeft=xpos+"px";
    elmobj.style.posTop=ypos+"px";
    elmobj.style.left=xpos+"px";
    elmobj.style.top=ypos+"px";   
    
    
 
    
    if (elmobj.style.display=='block')
		{
		if (eventObj.type!='keyup'){
		    elmobj.style.display='none';
		    }
		} else {
			elmobj.style.display='block';
		}
}



  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

function showemaillookupforce(){

var elmprojectlookpupdropdown = document.getElementById('emaillookpupdropdown')
//elmprojectlookpupdropdown.style.display='block';
//document.getElementById('_ctl0_SNAPUploadWizard_project_id').value='';

if (document.getElementById('Emaillist').value.length!=1) {



	var existinglist = document.getElementById('Emaillist').value;
	var searchstring;
	if(existinglist.lastIndexOf(";")>1){
	//check to see if starting 
		searchstring=LTrim(Right(existinglist,existinglist.length-existinglist.lastIndexOf(";")-1));
	}
	else {
	//assume first entry - replace
	searchstring=existinglist;
	}
	//alert(searchstring);
GridEmailList.Filter('email LIKE \'%' + searchstring + '%\' OR firstname like  \'%' + searchstring + '%\' OR lastname like  \'%' + searchstring + '%\'');
}
}

// Removes leading whitespaces
function LTrim( value ) {
	
	var re = /\s*((\S+\s*)*)/;
	return value.replace(re, "$1");
	
}

// Removes ending whitespaces
function RTrim( value ) {
	
	var re = /((\s*\S+)*)\s*/;
	return value.replace(re, "$1");
	
}

// Removes leading and ending whitespaces
function trim( value ) {
	
	return LTrim(RTrim(value));
	
}


function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}

function Left(str, n){
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}




	
function selectemaillistfromdropdown(item)
  {
  if (document.getElementById('emaillookpupdropdown').style.display=='none'){return;} 
	var itemvaluetmp;
	var existinglist;
	itemvaluetmp = item.GetMember('userid').Value;
	itemvaluenametmp = item.GetMember('email').Value;
	existinglist = document.getElementById('Emaillist').value;
	if(existinglist.lastIndexOf(";")>1){
	//check to see if starting 
		document.getElementById('Emaillist').value=Left(existinglist,existinglist.lastIndexOf(";")+1)+itemvaluenametmp+'; ';
	}
	else {
	//assume first entry - replace
	document.getElementById('Emaillist').value=itemvaluenametmp+'; ';
	}
	//alert(document.getElementById('Emaillist').value.substring(document.getElementById('Emaillist').value.indexOf(";")+1, document.getElementById('Emaillist').value.lastIndexOf(";")));

	//document.getElementById('Emaillist').value=document.getElementById('Emaillist').value+' '+itemvaluenametmp+';';

	//showprojectlookup();
	document.getElementById('emaillookpupdropdown').style.display='none';
	return true;
  }  

</script>
	
			
			
		
<div id=emaillookpupdropdown style="BORDER-RIGHT:1px solid; BORDER-TOP:1px solid; DISPLAY:none; Z-INDEX:999; BORDER-LEFT:1px solid; WIDTH:350px; PADDING-TOP:1px; BORDER-BOTTOM:1px solid; POSITION:absolute; HEIGHT:300px; BACKGROUND-COLOR:white;"> 
<div><input type=button onclick="javascript:emaillookpupdropdown.style.display='none';" value=close title=close></div>
	<COMPONENTART:GRID id="GridEmailList" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true"
	ClientSideOnSelect="selectemaillistfromdropdown" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true"
	ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/"
	ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip"
	ScrollPopupClientTemplateId="ScrollPopupTemplate" Height="260px" Width="350px" LoadingPanelPosition="TopCenter"
	LoadingPanelClientTemplateId="LoadingFeedbackTemplateUpload" EnableViewState="true" GroupBySortImageHeight="10"
	GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif"
	GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategoryDropdown"
	IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/"
	PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="true" GroupingPageSize="1"
	PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText"
	GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderDropdown" SearchOnKeyPress="true"
	SearchTextCssClass="GridHeaderText" AllowEditing="False" AllowSorting="False" ShowSearchBox="false"
	ShowHeader="false" ShowFooter="true" CssClass="GridDropdown" RunningMode="callback" ScrollBarWidth="15"
	AllowPaging="true" >
	<ClientTemplates>
		<componentart:ClientTemplate ID="LoadingFeedbackTemplateUpload">
			<div style="height:150px;"><table cellspacing="0" height=150 cellpadding="0" border="0">
					<tr>
						<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height="25" width="1"><br>
							<img src="images/spinner.gif" width="16" height="16" border="0"> Loading...
						</td>
					</tr>
				</table>
			</div>
		</componentart:ClientTemplate>
		<ComponentArt:ClientTemplate Id="ViewEmailListTemplate">
	<span>## DataItem.GetMember("firstname").Value ## ## DataItem.GetMember("lastname").Value ## <b>(## DataItem.GetMember("email").Value ##)</b></span>
	</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="EditCommandTemplateUpload">
			<a href="javascript:editRow();">Update</a>
		</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="InsertCommandTemplateUpload">
			<a href="javascript:insertRow();">Insert</a>
		</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="TypeIconTemplateUpload">
			<img src="images/projcat16.gif" border="0">
		</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="LookupProjectTemplateUpload">
			<A href="IDAM.aspx?page=Project&ID=## DataItem.GetMember("userid").Value ##&type=project"><img border="0" src="images/goto.gif"></A>
		</ComponentArt:ClientTemplate>
	</ClientTemplates>
	<Levels>
		<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateUpload" InsertCommandClientTemplateId="InsertCommandTemplateUpload" DataKeyField="userid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellTextDropdown" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssetsDropdown" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssetsDropdown" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssetsDropdown" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActiveDropdown" ShowTableHeading="False" ShowHeadingCells="False">
			<Columns>
				<componentart:GridColumn AllowEditing="False" Width="250" AllowGrouping="False" DataCellClientTemplateId="ViewEmailListTemplate" SortedDataCellCssClass="SortedDataCell" DataField="email" DataCellCssClass="FirstDataCellPostings"></componentart:GridColumn>
				<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="userid"></componentart:GridColumn>
				<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="email"></componentart:GridColumn>
				<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="firstname"></componentart:GridColumn>
				<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="lastname"></componentart:GridColumn>
			</Columns>
		</componentart:GridLevel>
	</Levels>
	</COMPONENTART:GRID>
	
</div>	
			
			
			
			
			
			
			
			
			
			
			
			<%response.write (request.querystring("newid"))%>
			<input type="hidden" name="catid" id="catid" value="<%response.write (replace(request.querystring("p"),"CAT",""))%>">
			<div style="padding:20px;position:relative;">
			
			<div style="100%">
			<div style="padding:10px;border:1px solid;background-color:white;position:relative;">
			<table><tr><td><b>IDAM Express</b></td></tr></table>
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Send files using IDAM Express</div><br>
  <div style="width=100%">

			
			
			
	
			
			
			
			
			
	<COMPONENTART:TABSTRIP id="TabStripCarousel" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataIDAMExpress.xml"
		CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
		DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
		<ItemLooks>
			<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10" 
 LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif" 
 HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23" 
 LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
			<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10" 
 LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif" 
 LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
		</ItemLooks>
	</COMPONENTART:TABSTRIP>
	
	<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageCategoryPage" runat="server"> 
		
		
		
		
		
		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="History">


<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">view the history of IDAMExpress</div>
			<br>
		
		
		
		
		
																
																	
<COMPONENTART:GRID 
id="GridHistory" 
runat="server" 
SelfReferencing="true"
AutoFocusSearchBox="false"
AutoCallBackOnInsert="true"
AutoCallBackOnUpdate="true"
AutoCallBackOnDelete="true"
ClientSideOnDelete="onDelete"
ClientSideOnUpdate="onUpdate"
pagerposition="2"
ScrollBar="Off"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ScrollPopupClientTemplateId="ScrollPopupTemplate" 
Sort="date_placed desc"
Height="10" Width="100%"
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategory" 
EnableViewState="true"
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRowCategory" 
IndentCellWidth="22" 
IndentCellCssClass="IndentCell" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/postinglines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="true" 
GroupingPageSize="1" 
PagerTextCssClass="GridFooterTextCategory" 
PagerStyle="Numbered" 
PageSize="15" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeader" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="true" 
AllowSorting="False"
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="server" 
ScrollBarWidth="15" 
AllowPaging="true">
<ClientTemplates>
		<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategory">
		<div><table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
		</tr>
		</table></div>
		</componentart:ClientTemplate>
		  <ComponentArt:ClientTemplate Id="EditTemplateCategory">
            <a href="javascript:showlogs('## DataItem.GetMember("id").Value ##');">logs</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateCategory">
            <a href="javascript:editRowhistory();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRowhistory();">Update</a> 
          </ComponentArt:ClientTemplate>          
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateCategory">
            <a href="javascript:insertRowCategory();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRowCategory();">Insert</a> 
          </ComponentArt:ClientTemplate>             
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategory">
            <img border=0 src="images/group16.jpg">
          </ComponentArt:ClientTemplate>    
          <ComponentArt:ClientTemplate Id="TypeIconTemplate">
            <img border=0 src="images/user16.jpg">
          </ComponentArt:ClientTemplate>               
 
          <ComponentArt:ClientTemplate Id="EditTemplateAssetPostings">
           <a href="javascript:DownloadPostingAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> | <a href="javascript:AddToCarousel('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/3Drafts.gif" alt="Edit" border=0></a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateAssetPostings">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateAssetPostings">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      


                  


          
                              
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" DataMember="Main" 
            EditFieldCssClass="EditDataField"
            EditCommandClientTemplateId="EditCommandTemplateCategory"
            InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowSorting="False" AllowEditing="false"   DataCellClientTemplateId="TypeIconTemplateCategory" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings"  HeadingImageWidth="9" HeadingImageHeight="19" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn HeadingText="Created By" AllowEditing="false" Width="80" DataCellCssClass="DataCell" SortedDataCellCssClass="SortedDataCell"  DataField="fullname" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Sent" AllowEditing="false" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" Width="150" DataCellCssClass="DataCell" DataField="date_placed"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Expires" AllowEditing="false" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" Width="150"  DataCellCssClass="DataCell" DataField="date_expired"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Subject" AllowEditing="true" Width="150" AllowGrouping="False" DataCellCssClass="DataCell" SortedDataCellCssClass="SortedDataCell"  DataField="subject"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Message" AllowEditing="true" Width="150" AllowGrouping="False" DataCellCssClass="DataCell" SortedDataCellCssClass="SortedDataCell"  DataField="message"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Active" AllowEditing="true" Width="50" AllowGrouping="False" DataCellCssClass="DataCell" SortedDataCellCssClass="SortedDataCell"  DataField="active"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Require Password" AllowEditing="true" Width="150" AllowGrouping="False" DataCellCssClass="DataCell" SortedDataCellCssClass="SortedDataCell"  DataField="authenticate"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Password" AllowEditing="true" Width="100" AllowGrouping="False" DataCellCssClass="DataCell" SortedDataCellCssClass="SortedDataCell"  DataField="password"></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" AllowSorting="False" AllowEditing="false" DataCellClientTemplateId="EditTemplateCategory" Width="150" DataCellCssClass="LastDataCellPostings" EditControlType="EditCommand" Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>




<componentart:GridLevel EditCellCssClass="EditDataCell"
            EditFieldCssClass="EditDataField"
            EditCommandClientTemplateId="EditCommandTemplate"
            InsertCommandClientTemplateId="InsertCommandTemplate" DataMember="AssetsTitle" DataKeyField="id" SelectedRowCssClass="Row" DataCellCssClass="DataCellCategory" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellCategory" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverCategory" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowCategory" SortImageHeight="19" RowCssClass="Row" ShowTableHeading="False" ShowSelectorCells="False" ShowHeadingCells="False" HeadingCellActiveCssClass="HeadingCellActiveCategory">
<Columns>

<componentart:GridColumn DataCellCssClass="DataCell" Width="500" AllowEditing="false"  AllowGrouping="False" DataField="title" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="refid"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="refid2"></componentart:GridColumn>
</Columns>

</componentart:GridLevel>







<componentart:GridLevel EditCellCssClass="EditDataCell"
            EditFieldCssClass="EditDataField"
            EditCommandClientTemplateId="EditCommandTemplate"
            InsertCommandClientTemplateId="InsertCommandTemplate" DataMember="Assets" DataKeyField="idassets" SelectedRowCssClass="Row" DataCellCssClass="DataCellCategory" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellCategory" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverCategory" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowCategory" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActiveCategory">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="16" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell"  AllowEditing="false" HeadingText="Name"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell"  AllowEditing="false" HeadingText="Project"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="projectname" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="idassets"></componentart:GridColumn>
</Columns>

</componentart:GridLevel>




<componentart:GridLevel EditCellCssClass="EditDataCell"
            EditFieldCssClass="EditDataField"
            EditCommandClientTemplateId="EditCommandTemplate"
            InsertCommandClientTemplateId="InsertCommandTemplate" DataMember="Emails" DataKeyField="idemails" SelectedRowCssClass="Row" DataCellCssClass="DataCellCategory" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellCategory" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverCategory" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowCategory" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActiveCategory">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="16" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell"  AllowEditing="false" HeadingText="Email"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="emailvalue" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="idemails"></componentart:GridColumn>
</Columns>

</componentart:GridLevel>








</Levels>
</COMPONENTART:GRID>
							
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		</ComponentArt:PageView>
		
		
		
		
		
		
		
		
		
		
		
		
		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="IDAMExpress">






<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Enter the recipient(s) email address in the location below to invite a user to download your files.</div>
			<br>
	

            
            
            <table cellspacing="0" cellpadding="3" border="0" width="100%">
             
				<tr>
					<td width="120" align="left" valign="top" nowrap>
					
						 
						
					</td>
					<td align="left" valign="top" width="100%">
					<font face="Verdana" size="1">							

							<asp:Literal Runat=server ID=emailconfirmation></asp:Literal><asp:Literal Runat=server ID=emailhistory></asp:Literal>
							<div id="showprogress" style="display:none">
							<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD align="center">
										<TABLE cellSpacing="0" cellPadding="0" border="0">
											<TR>
												<TD style="FONT-SIZE: 10px">Processing IDAMExpress Delivery...Please wait. 
												</TD>
												<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
							</div>	
					</font>
					</td>
				</tr>
			</table>
            
            
            
            
            
             <table cellspacing="0" cellpadding="3" border="0" width="100%">
             
				<tr>
					<td width="120" align="left" valign="top" nowrap>
						<b>
						<font face="Verdana" size="1">Sent To: * </font>
						</b>
					</td>
					<td align="left" valign="top" width="50%">
						<asp:TextBox id="Emaillist" TextMode="MultiLine" runat="server" style="height:50px" Height="50px" CssClass="InputFieldMain100P" ></asp:TextBox><br><font face="Verdana" size="1">*Seperate multiple email addresses with a semi colon (;).</font></td>
					
					
					<td align="left" valign="top" width="50%" rowspan="5">
					
					

					
					<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;margin-bottom:3px;
  font-weight: normal;">List of files to send.  You may remove any file by clicking the delete icon to the right of the file.</div>
<!--GroupBy="categoryname ASC"-->
<div id="side_results" style="width: 100%; border:1px solid #B7B4B4;padding:10px;height: 424px; overflow: auto;" > 
	<COMPONENTART:GRID 
	id="GridAssets" 
	runat="server" 
	AutoFocusSearchBox="false"
	AutoCallBackOnInsert="true"
	AutoCallBackOnUpdate="true"
	AutoCallBackOnDelete="true"
	pagerposition="2"
	ScrollBar="Off"
	ScrollTopBottomImagesEnabled="true"
	ScrollTopBottomImageHeight="2"
	ScrollTopBottomImageWidth="16"
	ScrollImagesFolderUrl="images/scroller/"
	ScrollButtonWidth="16"
	ScrollButtonHeight="17"
	ScrollBarCssClass="ScrollBar"
	ScrollGripCssClass="ScrollGrip"
	ClientSideOnCallbackError="onCallbackError"
	ScrollPopupClientTemplateId="ScrollPopupTemplate" 
	Sort="name asc"
	Height="10" Width="100%"
	LoadingPanelPosition="TopCenter" 
	LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
	EnableViewState="true"
	GroupBySortImageHeight="10" 
	GroupBySortImageWidth="10" 
	GroupBySortDescendingImageUrl="group_desc.gif" 
	GroupBySortAscendingImageUrl="group_asc.gif" 
	GroupingNotificationTextCssClass="GridHeaderText" 
	AlternatingRowCssClass="AlternatingRowCategory" 
	IndentCellWidth="22" 
	TreeLineImageHeight="19" 
	TreeLineImageWidth="20" 
	TreeLineImagesFolderUrl="images/lines/" 
	PagerImagesFolderUrl="images/pager/" 
	ImagesBaseUrl="images/" 
	PreExpandOnGroup="True" 
	GroupingPageSize="5" 
	PagerTextCssClass="GridFooterTextCategory" 
	PagerStyle="Numbered" 
	PageSize="1000" 
	GroupByTextCssClass="GroupByText" 
	GroupByCssClass="GroupByCell" 
	FooterCssClass="GridFooter" 
	HeaderCssClass="GridHeader" 
	SearchOnKeyPress="true" 
	SearchTextCssClass="GridHeaderText" 
	AllowEditing="true" 
	AllowSorting="False"
	ShowSearchBox="false" 
	ShowHeader="false" 
	ShowFooter="false" 
	CssClass="Grid" 
	RunningMode="callback" 
	ScrollBarWidth="15" 
	AllowPaging="true" >
	<ClientTemplates>
	<ComponentArt:ClientTemplate Id="EditTemplatega">
			<a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Remove from carousel" border=0></a>
			</ComponentArt:ClientTemplate>
	<ComponentArt:ClientTemplate Id="EditTemplateNoRemovega">
			<a href="javascript:DownloadAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border=0></a> 
			</ComponentArt:ClientTemplate>			
			<ComponentArt:ClientTemplate Id="EditCommandTemplatega">
				<a href="javascript:editRow();">Update</a> 
			</ComponentArt:ClientTemplate>
			<ComponentArt:ClientTemplate Id="InsertCommandTemplatega">
				<a href="javascript:insertRow();">Insert</a> 
			</ComponentArt:ClientTemplate>    
			<ComponentArt:ClientTemplate Id="TypeIconTemplatega">

			</ComponentArt:ClientTemplate>        
			
			<ComponentArt:ClientTemplate Id="LookupAssetPopupTemplate">
				<A href="#" onclick="javascript:PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.ClientId ##',event);"><img border=0 src="images/15printPreview.gif"></a>
			</ComponentArt:ClientTemplate> 
			<ComponentArt:ClientTemplate Id="LookupProjectTemplate">

			</ComponentArt:ClientTemplate>           
			
			<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
			<table cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
			</tr>
			</table>
			</componentart:ClientTemplate>                                 
	</ClientTemplates>


	<Levels>
	<componentart:GridLevel EditCellCssClass="EditDataCell"
				EditFieldCssClass="EditDataField"
				EditCommandClientTemplateId="EditCommandTemplate"
				InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
	<Columns>

	<componentart:GridColumn DataCellCssClass="FirstDataCellPostings" AllowEditing="false" HeadingText="File(s) to Deliver" Width="240" AllowGrouping="False" DataField="name" ></componentart:GridColumn>
	<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowSorting="False" AllowEditing="false" DataCellClientTemplateId="EditTemplatega" EditControlType="EditCommand" Width="50"  Align="Center" />
	<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
	<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
	
	

	</Columns>
	</componentart:GridLevel>
	</Levels>

	</COMPONENTART:GRID>
	</div>

					</td>
				</tr>
            
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<b>
						<font face="Verdana" size="1">Subject:</font></b></td>
					<td class="PageContent" align="left" valign="top"  width="50%">
 <font size="1" face="Arial">
            
						<asp:TextBox id="EmailSubject" runat="server" style="height:25px" Height="25px" CssClass="InputFieldMain100P" ></asp:TextBox>

     
</font>    </td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<b>
						<font face="Verdana" size="1">Message:</font></b></td>
					<td class="PageContent" align="left" valign="top"  width="50%">
 <font size="1" face="Arial">
            
						<asp:TextBox id="EmailMessage"  TextMode="MultiLine" runat="server" style="height:200px" Height="200px" CssClass="InputFieldMain100P" ></asp:TextBox>

     
</font>    </td>
				</tr>	
				
								<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<b><font face="Verdana" size="1">Options</font></b></td>
					<td class="PageContent" align="left" valign="top"  width="50%"><hr noshade size=1>    </td>
				</tr>	
				
				
				
				
				
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<b>
						<font face="Verdana" size="1">Password:</font></b>(optional) </td>
					<td class="PageContent" align="left" valign="top"  width="50%">
 <font size="1" face="Arial">
            
						 <asp:TextBox ID="downloadPassword" TextMode="SingleLine" Width="100" MaxLength="20" CssClass="InputFieldMain100P" Runat="server"></asp:TextBox>  
						 <div style="padding:5px;padding-left:0px;  font-family: verdana; color: #3F3F3F; font-size: 10px;font-weight: normal;">
						<asp:CheckBox ID=chkDocumentLockdown Runat=server Checked=False></asp:CheckBox><font face="Verdana" size="1"> Lock <b>documents</b> to PDF Format</font><br>
						<asp:CheckBox ID="chkImageLockdown" Runat=server Checked=False></asp:CheckBox><font face="Verdana" size="1"> Lock <b>images</b> to </font>  <asp:DropDownList ID="downloadIdList" Visible =True Runat=server></asp:DropDownList>
						
						<br>
						
						<asp:CheckBox ID="chkNotify1" Runat=server Checked=False></asp:CheckBox><font face="Verdana" size="1"> Notify me when the files are downloaded</font><br>
						<asp:CheckBox ID="chkNotify2" Runat=server Checked=False></asp:CheckBox><font face="Verdana" size="1"> Notify me on all events</font><br>
						</div>
						<asp:Button ID=btnsendmail Runat=server  Text="IDAMExpress my files"></asp:Button>
						
     
</font>    </td>
<td align="left" valign="top" width="50%" rowspan="2">
												 

					</td>
				</tr>					
<tr> 
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
					
						</td>
					<td class="PageContent" align="left" valign="top"  width="50%">
 <font size="1" face="Arial">
            
						
     
</font>    </td>
				</tr>					
 
								
			</table>
			
</font>
	</ComponentArt:PageView>



		
		


		
	</ComponentArt:MultiPage>
	<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
					<div style="text-align:right;"><br>
			<input type="button" onclick="window.close();" value="Close" />
			</div>
	</div>
</div>

			
			
			
			

			</div>
			
			
			
			
			
			
			
			
			</div>
			

			
			</div>
			</div>
			
		
			
			
		</form>








<script language="javascript">

function onDeleteAsset() {

}
  

function showContactSheet() {
	if (contactsheet.style.display == "block") {
		contactsheet.style.display = "none";
	} else {
	if (contactsheet.style.display == "none") {
		contactsheet.style.display = "block";
	}
	}
}

 

<%if request.querystring("tab") = "email" then%>
TabStripCarousel.SelectTabById('Carousel_Email');
<%end if%>

function view_email_history(ID)
{
window.open('CarouselemailHistory.aspx?ID=' + ID + '<%="&instance=" & IDAMInstance%>','EmailHistory','width=700,height=500,location=yes,resizable=yes');
}
  
function Object_PopUp(strUrl, popupName, intHeight, intWidth)
{


	if (strUrl == null || strUrl.Length <= 0)
		return;

	var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=yes,resizable=yes,toolbar=no";
	//var strFeatures = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;
	//var strFeatures += "'"
	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures , false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
}



</script>	














		
	</body>
</HTML>








