<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.CarouselNew" EnableViewState="true" ValidateRequest="False" CodeBehind="CarouselNew.aspx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Carousel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
			<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
			<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
				<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
					<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
						<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
							<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
								<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
									<link href="treeStyle.css" type="text/css" rel="stylesheet">
										<link href="navStyle.css" type="text/css" rel="stylesheet" >
										    <link href="tabStripStyle.css" type="text/css" rel="stylesheet" />    
												<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
										<style>BODY { MARGIN: 0px }
	</style>
	<body onload="this.focus">
		<form id="Form" method="post" runat="server">
			<script type="text/javascript">
  
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridKeywords.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    GridKeywords.Edit(GridKeywords.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    GridKeywords.EditComplete();     
  }

  function insertRow()
  {
    GridKeywords.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    GridKeywords.Delete(GridKeywords.GetRowFromClientId(rowId)); 
  }

<%if request.querystring("newid") <> "" then%>
window.parent.NavigateToCarousel('<%response.write (request.querystring("newid"))%>');
//window.close();
<%end if%>

  <%if instr(request.querystring("id"),"CAR") = 0 then%>
	//alert ('Error: ' & request.querystring("id"));
	//window.close();
  <%end if%>
  
  
  
  
  
  
  
  
  
  
  

//submit contact sheet
function submitContactSheet() {
	//get all selected values...
	var field;
	var arraylist=new Array();

	var arraylist;
	var i;
	var sAssets;
	var downloadtype;
	arraylist = '';
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	document.Form.asset_array.value = arraylist;

	if (arraylist.split(',').length > 1)
		{
			document.Form.method="GET";
			document.Form.action="contact_sheet.asp";
			document.Form.target="_blank";
			document.Form.submit();
			document.Form.method="POST";
			document.Form.action="";
			document.Form.target="";

		} else
			alert('Nothing selected');
			return false;
		{
	
	}
}
  
  
  

  
			</script>
			
			
			
			
			
			
			
			
			
			
		
<script language="JavaScript" type="text/JavaScript">




function MM_findObj(n, d) { //v3.0
var p,i,x; if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}


function CheckAll(fieldName,Num) { //Set All checkboxes, if un-checked, check them.
if (MM_findObj(fieldName).length > 1) { 
var checks = MM_findObj(fieldName)
var bcheck = MM_findObj(fieldName)[1].checked
if (bcheck) {
bcheck = false;
} else {
bcheck = true;
}
for (i=0; i<checks.length; i++) {
MM_findObj(fieldName)[i].checked = bcheck ; 
}

}
}


  
function openpopup(assetid,x,y){
var popurl= "image_pop.asp?a=" + assetid + "&c=<%=request.querystring("c")%>" + "&p=<%=request.querystring("p")%>"
var xwidth = x
var yheight = y
winpops=window.open(popurl,"","width=" + xwidth + ",height=" + yheight + ",location=no,")
}

function CheckAllItemsThumbs() { //Set All checkboxes, if un-checked, check them.
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
var dummyobjectvalue = checks[0].value;
var firstobjectcheckedstate = document.getElementById('aselect_' + dummyobjectvalue);
var bcheck = firstobjectcheckedstate.checked

if (bcheck) {
bcheck = true;
} else {
bcheck = false;
}

for (i=0; i<checks.length; i++) {
MM_findObj('aselect_' + checks[i].value).checked = bcheck ;
highlightAssetToggleII(document.getElementById('aselect_' + checks[i].value),checks[i].value); 
}

}
}


function ShowSlideShow(){
	var response = confirm('Show slideshow fullscreen?');
	if (response) {
		window.open("./slideshow/slideshow.asp?car=<%=Request.QueryString("Carousel_id")%>&uid=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.UserID.tostring%>&usl=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.userAccess.ToString%>","","width=1024,height=800,fullscreen=yes,location=no,");
	} else {
		window.open("./slideshow/slideshow.asp?car=<%=Request.QueryString("Carousel_id")%>&uid=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.UserID.tostring%>&usl=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.userAccess.ToString%>","","width=1024,height=800,location=no,");
	}
}



//setup form switch for delete or multi asset zip download
function DownloadPPT(ids){
	var arraylist;
	var i;
	var sAssets;
	var downloadtype;
	arraylist = '';
	ids = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
	
	
	/*download single*/
	downloadtype = 'single';

	if (arraylist.split(',').length > 1) {
	/*assume single*/
	
	window.open('<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLLocationIDAMDownloadPPT & "?assetids="%>' + arraylist + '<%="&instanceid=" & IDAMInstance%>','ConvertingImage','width=500,height=75,location=no');
	//return true;
	
	}

}

<%if WebArchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.userid.tostring.trim = "" then%>
window.parent.CloseDialogWindowXCarousel();
<%end if%>




 
function showMenu(id, eventObj) {

    var elmobj = document.getElementById(id);
    var xoffsettype = 260;
   	if (id!='emaillookpupdropdown')
	{
	xoffsettype += 35;
	//hide others
	document.getElementById('emaillookpupdropdown').style.display='none';
	} else
	{
	//document.getElementById('categorylookpupdropdown').style.display='none';
	}
	ypos=215;
	xpos=162;
    elmobj.style.posLeft=xpos+"px";
    elmobj.style.posTop=ypos+"px";
    elmobj.style.left=xpos+"px";
    elmobj.style.top=ypos+"px";   
    
    
 
    
    if (elmobj.style.display=='block')
		{
		if (eventObj.type!='keyup'){
		    elmobj.style.display='none';
		    }
		} else {
			elmobj.style.display='block';
		}
}



  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

function showemaillookupforce(){
var elmprojectlookpupdropdown = document.getElementById('emaillookpupdropdown')
//elmprojectlookpupdropdown.style.display='block';
//document.getElementById('_ctl0_SNAPUploadWizard_project_id').value='';

if (document.getElementById('Emaillist').value.length!=1) {



	var existinglist = document.getElementById('Emaillist').value;
	var searchstring;
	if(existinglist.lastIndexOf(";")>1){
	//check to see if starting 
		searchstring=LTrim(Right(existinglist,existinglist.length-existinglist.lastIndexOf(";")-1));
	}
	else {
	//assume first entry - replace
	searchstring=existinglist;
	}
	//alert(searchstring);
GridEmailList.Filter('email LIKE \'%' + searchstring + '%\' OR firstname like  \'%' + searchstring + '%\' OR lastname like  \'%' + searchstring + '%\'');
}
}

// Removes leading whitespaces
function LTrim( value ) {
	
	var re = /\s*((\S+\s*)*)/;
	return value.replace(re, "$1");
	
}

// Removes ending whitespaces
function RTrim( value ) {
	
	var re = /((\s*\S+)*)\s*/;
	return value.replace(re, "$1");
	
}

// Removes leading and ending whitespaces
function trim( value ) {
	
	return LTrim(RTrim(value));
	
}


function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}

function Left(str, n){
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}




	
function selectemaillistfromdropdown(item)
  {
	var itemvaluetmp;
	var existinglist;
	itemvaluetmp = item.GetMember('userid').Value;
	itemvaluenametmp = item.GetMember('email').Value;
	existinglist = document.getElementById('Emaillist').value;
	if(existinglist.lastIndexOf(";")>1){
	//check to see if starting 
		document.getElementById('Emaillist').value=Left(existinglist,existinglist.lastIndexOf(";")+1)+itemvaluenametmp+'; ';
	}
	else {
	//assume first entry - replace
	document.getElementById('Emaillist').value=itemvaluenametmp+'; ';
	}
	//alert(document.getElementById('Emaillist').value.substring(document.getElementById('Emaillist').value.indexOf(";")+1, document.getElementById('Emaillist').value.lastIndexOf(";")));

	//document.getElementById('Emaillist').value=document.getElementById('Emaillist').value+' '+itemvaluenametmp+';';

	//showprojectlookup();
	document.getElementById('emaillookpupdropdown').style.display='none';
	return true;
  }  

</script>
	
			
			
		
<div id=emaillookpupdropdown style="BORDER-RIGHT:1px solid; BORDER-TOP:1px solid; DISPLAY:none; Z-INDEX:999; BORDER-LEFT:1px solid; WIDTH:350px; PADDING-TOP:1px; BORDER-BOTTOM:1px solid; POSITION:absolute; HEIGHT:2px; BACKGROUND-COLOR:white"> 
<div><input type=button onclick="javascript:emaillookpupdropdown.style.display='none';" value=close title=close></div>
	<COMPONENTART:GRID id="GridEmailList" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true"
	ClientSideOnSelect="selectemaillistfromdropdown" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true"
	ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/"
	ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip"
	ScrollPopupClientTemplateId="ScrollPopupTemplate" Height="260px" Width="350px" LoadingPanelPosition="TopCenter"
	LoadingPanelClientTemplateId="LoadingFeedbackTemplateUpload" EnableViewState="true" GroupBySortImageHeight="10"
	GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif"
	GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategoryDropdown"
	IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/"
	PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="true" GroupingPageSize="1"
	PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText"
	GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderDropdown" SearchOnKeyPress="true"
	SearchTextCssClass="GridHeaderText" AllowEditing="False" AllowSorting="False" ShowSearchBox="false"
	ShowHeader="false" ShowFooter="true" CssClass="GridDropdown" RunningMode="callback" ScrollBarWidth="15"
	AllowPaging="true" >
	<ClientTemplates>
		<componentart:ClientTemplate ID="LoadingFeedbackTemplateUpload">
			<div style="height:150px;"><table cellspacing="0" height=150 cellpadding="0" border="0">
					<tr>
						<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height="25" width="1"><br>
							<img src="images/spinner.gif" width="16" height="16" border="0"> Loading...
						</td>
					</tr>
				</table>
			</div>
		</componentart:ClientTemplate>
		<ComponentArt:ClientTemplate Id="ViewEmailListTemplate">
	<span>## DataItem.GetMember("firstname").Value ## ## DataItem.GetMember("lastname").Value ## <b>(## DataItem.GetMember("email").Value ##)</b></span>
	</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="EditCommandTemplateUpload">
			<a href="javascript:editRow();">Update</a>
		</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="InsertCommandTemplateUpload">
			<a href="javascript:insertRow();">Insert</a>
		</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="TypeIconTemplateUpload">
			<img src="images/projcat16.gif" border="0">
		</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="LookupProjectTemplateUpload">
			<A href="IDAM.aspx?page=Project&ID=## DataItem.GetMember("userid").Value ##&type=project"><img border="0" src="images/goto.gif"></A>
		</ComponentArt:ClientTemplate>
	</ClientTemplates>
	<Levels>
		<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateUpload" InsertCommandClientTemplateId="InsertCommandTemplateUpload" DataKeyField="userid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellTextDropdown" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssetsDropdown" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssetsDropdown" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssetsDropdown" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActiveDropdown" ShowTableHeading="False" ShowHeadingCells="False">
			<Columns>
				<componentart:GridColumn AllowEditing="False" Width="250" AllowGrouping="False" DataCellClientTemplateId="ViewEmailListTemplate" SortedDataCellCssClass="SortedDataCell" DataField="email" DataCellCssClass="FirstDataCellPostings"></componentart:GridColumn>
				<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="userid"></componentart:GridColumn>
				<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="email"></componentart:GridColumn>
				<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="firstname"></componentart:GridColumn>
				<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="userid" SortedDataCellCssClass="SortedDataCell" DataField="lastname"></componentart:GridColumn>
			</Columns>
		</componentart:GridLevel>
	</Levels>
	</COMPONENTART:GRID>
	
</div>	
			
			
			
			
			
			
			
			
			
			
			
			<%response.write (request.querystring("newid"))%>
			<input type="hidden" name="catid" id="catid" value="<%response.write (replace(request.querystring("p"),"CAT",""))%>">
			<div style="padding:20px;position:relative;">
			
			<div style="width:100%">
			<div style="padding:10px;border:1px solid;background-color:white;position:relative;">
			<table><tr><td>Create/Edit Carousel</td></tr></table>
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Create a carousel by entering a carousel name and optional description.</div><br>
  <div style="width:100%">

			
			
			
	
			
			
			
			
			
	<COMPONENTART:TABSTRIP id="TabStripCarousel" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataCarousel.xml"
		CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
		DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
		<ItemLooks>
			<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10" 
 LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif" 
 HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23" 
 LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
			<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10" 
 LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif" 
 LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
		</ItemLooks>
	</COMPONENTART:TABSTRIP>
	
	<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageCategoryPage" runat="server">
		<ComponentArt:PageView CssClass="PageContent2" runat="server" ID="Pageview1">
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Enter a name, and description for this carousel.</div>
			<br>
	
 <font size="1" face="Arial">
            
             <table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td width="120" align="left" valign="top" nowrap>
						<b>
						<font face="Verdana" size="1">Name: </font>
						</b>
					</td>
					<td align="left" valign="top" width="50%">
						<asp:TextBox id="Name" runat="server" CssClass="InputFieldMain100P" Height=25px></asp:TextBox></td>
					<td align="left" valign="top" width="50%" rowspan="5">
					<%if Request.QueryString("Carousel_id") <> "" then%>
						<input class="submitButton" onclick="deleteCarousel();" type="button" value="Delete Carousel">

						<br><br>
						</font>
 <font size="1" face="Verdana">

						<table cellSpacing="0" cellPadding="0" border="0" id="table1">
							<tr>
								<td align="right" nowrap><span class="RegularText"><b>
								<font size="1">Contents:</font></b><font size="1">&nbsp;</font></span></td>
								<td width="200"><font size="1"><asp:Literal id="Literalcontents" Text="" visible="true"    runat="server" />&nbsp;Items</font></td>
							</tr>
</font>
 <font size="1" face="Arial">
            
 <font size="1" face="Verdana">
            
							<tr>
								<td colSpan="2" nowrap>
								<font face="Verdana" size="1">
								</font></td>
							</tr>
							<tr>
								<td align="right" nowrap><font face="Verdana" size="1"><span class="RegularText"><b>
								Creation Date:</b>&nbsp;</span></font></td>
								<td width="200"><font face="Verdana" size="1"><asp:Literal id="Literalcreationdate" Text="" visible="true"    runat="server" /> 
								</font> </td>
							</tr>
							<tr>
								<td colSpan="2" nowrap>
								<font face="Verdana" size="1">
								</font></td>
							</tr>
							<tr>
								<td align="right" nowrap><font face="Verdana" size="1"><span class="RegularText"><b>
								Last Modified:</b>&nbsp;</span></font></td>
								<td width="200"><font face="Verdana" size="1"><asp:Literal id="Literalmodifieddate" Text="" visible="true"    runat="server" />
								</font> </td>
							</tr>
							<tr>
								<td colSpan="2" nowrap>
								<font face="Verdana" size="1">
								</font></td>
							</tr>
							<tr>
								<td align="right" nowrap><font face="Verdana" size="1"><span class="RegularText"><b>
								Owner:</b>&nbsp;</span></font></td>
								<td width="200"><font face="Verdana" size="1"><asp:Literal id="Literaluser" Text="" visible="true"    runat="server" /></font></td>
							</tr>
							<tr>
								<td colSpan="2" nowrap>
								<font face="Verdana" size="1">
								</font></td>
							</tr>
							<tr>
								<td align="right" nowrap><font face="Verdana" size="1"><span class="RegularText"><b>
								Public:</b>&nbsp;</span></font></td>
								<td width="200"><font face="Verdana" size="1"><asp:Literal id="Literalsharing" Text="" visible="true"    runat="server" /></font></td>
							</tr>
							<tr>
								<td align="right" nowrap><font face="Verdana" size="1"><span class="RegularText"><b>
								Password:</b>&nbsp;</span></font></td>
								<td width="200"><font face="Verdana" size="1"><asp:Literal id="Literalpassword" Text="" visible="true" runat="server" /></font></td>
							</tr>
							<tr>
								<td colSpan="2" nowrap>
								<font face="Verdana" size="1">
								</font></td>
							</tr>
							<tr>
								<td align="right" height="13" nowrap><span class="RegularText"><b>
								<font size="1" face="Verdana">Collaborate</font></b></span><font face="Verdana"><span class="RegularText"><font size="1"><b>:</b>&nbsp;</font></span></font></td>
								<td width="200" height="13"><font face="Verdana" size="1"><asp:Literal id="Literaleditable" Text="" visible="true"    runat="server" /></font></td>
							</tr>
						</table>
						 <br><input class="submitButton" onclick="cloneCarousel();" type="button" onclick="javascript:cloneCarousel()" value="Clone Carousel"><br>
</font><%end if%>
 										<br>
</font>

<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  vertical-align:top;font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">
 <font size="1" face="Arial">
            
						<span ><font face="Verdana">Carousels are personal online 
						workspaces for working with assets.<br>
						<br>
						<b>Key Features:</b><br>
						Manage a collection of assets by adding and removing assets to your default carousel using the
						available functionality in most asset listings.  <br><br>
						Carousels can be emailed, shared for public use, or used collaborately by allowing others to add and remove
						assets.<br><br>
						Carousel asset collections can then be downloaded, used to generate contact sheets, or extracted as Powerpoint
						slideshows.
						</font></span></div>

            
						&nbsp;</td>
				</tr>
            
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<b>
						<font face="Verdana" size="1">Description:</font></b></td>
					<td class="PageContent" align="left" valign="top"  width="50%">
 <font size="1" face="Arial">
            
						<asp:TextBox id="Description" TextMode="MultiLine" runat="server" style="height:300px" Height="300px" CssClass="InputFieldMain100P" ></asp:TextBox>

     
</font>    </td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
 <b>
 <font size="1" face="Verdana">
            
             			Make Public:</font></b></td>
					<td class="PageContent" align="left" valign="top"  width="50%">
 <font size="1" face="Arial">
            <asp:CheckBox id="chkshared" runat="server"></asp:CheckBox>
						</font></td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
 <b>
 <font size="1" face="Verdana">
            
             			Password:</font></b></td>
					<td class="PageContent" align="left" valign="top"  width="50%">
 <font size="1" face="Arial">
            <asp:TextBox id="PasswordField" namd="PasswordField" TextMode= "SingleLine" runat="server" style="height:25px" Height="25px" CssClass="InputFieldMain" ></asp:TextBox>
						</font></td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<b>
						<font size="1" face="Verdana"></font></b></td>
					<td class="PageContent" align="left" valign="top"  width="50%">
 <font size="1" face="Arial">
            
						 <asp:CheckBox id="chkeditable" Visible=false runat="server"></asp:CheckBox></font></td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<b>
						<font face="Verdana" size="1">Keyword:</font></b></td>
					<td class="PageContent" align="left" valign="top"  width="50%">
 <font size="1" face="Arial">
            
						<asp:DropDownList CssClass="InputFieldMain" Height=25px ID="KeywordSelected" DataTextField="keyname" DataValueField="keyid" width="180" Runat="server">
						
						</asp:DropDownList>

     
</font>    </td>
				</tr>
								
			</table>
</font>
 


		</ComponentArt:PageView>
		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Carousel_Email">






<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Enter the recipient's email address in the location below to invite a user to view/edit your carousel.</div>
			<br>
	

            
            
            <table cellspacing="0" cellpadding="3" border="0" width="100%">
             
				<tr>
					<td width="120" align="left" valign="top" nowrap>
					
						 
						
					</td>
					<td align="left" valign="top" width="100%">
					<font face="Verdana" size="1"><asp:Literal Runat=server ID=emailconfirmation></asp:Literal><asp:Literal Runat=server ID=emailhistory></asp:Literal></font>
					</td>
				</tr>
			</table>
            
            
            
            
            
             <table cellspacing="0" cellpadding="3" border="0" width="100%">
             
				<tr>
					<td width="120" align="left" valign="top" nowrap>
						<b>
						<font face="Verdana" size="1">Email(s): </font>
						</b>
					</td>
					<td align="left" valign="top" width="50%">
						<asp:TextBox id="Emaillist" TextMode="MultiLine" runat="server" style="height:50px" Height="50px" CssClass="InputFieldMain100P" ></asp:TextBox></td>
					
					
					<td align="left" valign="top" width="50%" rowspan="3">
					<asp:CheckBox id="CheckboxAllowEdit" runat="server"></asp:CheckBox><font face="Verdana" size="1">Allow Invitee(s) to Edit Carousel<br><br></font>
					<asp:Button cssclass="submitButton" ID="btnsendmail"   Runat=server Text="Email Carousel"></asp:Button>
						
						<br><br>
						</font>

<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">
 <font size="1" face="Arial">
						<span ><font face="Verdana">Email a carousel to a user by entering the recipient's email address.  If you wish to enter multiple email addresses, please seperate them with a semi colon (;).<br><br>To allow the invited to edit your carousel, please select the 'Allow Edit' link above before sending the email.</font></span></div>
					</td>
				</tr>
            
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<b>
						<font face="Verdana" size="1">Subject:</font></b></td>
					<td class="PageContent" align="left" valign="top"  width="50%">
 <font size="1" face="Arial">
            
						<asp:TextBox id="EmailSubject" runat="server" style="height:25px" Height="25px" CssClass="InputFieldMain100P" ></asp:TextBox>

     
</font>    </td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<b>
						<font face="Verdana" size="1">Message:</font></b></td>
					<td class="PageContent" align="left" valign="top"  width="50%">
 <font size="1" face="Arial">
            
						<asp:TextBox id="EmailMessage"  TextMode="MultiLine" runat="server" style="height:200px" Height="200px" CssClass="InputFieldMain100P" ></asp:TextBox>

     
</font>    </td>
				</tr>				

								
			</table>
</font>




















		</ComponentArt:PageView>



		
		
		

		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Carousel_Items">

<!--test-->
<div style="vertical-align:top;">


		
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">List of available assets within this carousel.  Note:  If you do not have permission to view the asset, it will not show in the carousel.</div><br>
			


				

			<img src="images/spacer.gif" width=1 height=10><br>
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
<div id="contactsheet" style="display:none;">

<!--Start Rounded Box-->
<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%">
<div class="sidemodule">
    <div class="modulecontainer sidebar">
        <div class="first">
            <span class="first"></span><span class="last"></span>
        </div>
        <div>
            <table border="0" width="100%">
				<tr>
					<td width="4">&nbsp;</td>
					<td valign="top"><!--End Top Rounded Box-->

            <!--<form method="GET" target="NewWindow" id="contactsheetForm" action="contact_sheet.asp">-->
              <p><b><font face="Verdana" size="1">Contact Sheet Settings<br>
				<br>
				</font></b><font face="arial"  size="1">Heading (Optional - Max 3 lines)</font><font face="Arial" size="1" color=""><font face="Arial" size="1" color="#000000"><img border="0" src="images/spacer.gif" height="1" width="82"><br>
				</font><b>
				&nbsp;<textarea rows="3" name="Heading" cols="62"></textarea><br>
				</b></font><br>
					<font face="arial" size="1">layout</font><font face="Arial" size="1" color="#000000"><img border="0" src="images/spacer.gif" height="1" width="82"><br>
				</font><font face="Arial" size="1" color="">
				<b>
<select   size="1" name="columns" class="more_projects">
                <option  value=2>2 X 2</option>
                <option  value=3>3 X 4</option>
                <option  value=4>4 X 5</option>
              </select></b></font><br>
              <br>
				</font></font>
				<font face="arial" size="1" color="">Selection</font><font face="Arial" size="1" color=""><font face="Arial" size="1" color="#000000"><img border="0" src="images/spacer.gif" height="1" width="82"><br>
				</font><font face="Arial" size="1" color="">
				<b>
<select   size="1" name="selection" class="more_projects">
                <option  value=2>Only Selected Items</option>
              </select></b></font></p>
				<table border="0" width="100%" id="table1">
					<tr>
						<td valign="top"><font face="Arial" size="1" color=""><font face="Arial" size="1" color="#000000">
						Optional
				Fields<br>
				</font>
              <font color="#808080" size="1" face="Arial">
              <input class="Preferences" type="checkbox" name="ffiletype" value="1" checked >FileType 
				Icon</font><font face="Arial" size="1" color="#000000"><br>
				</font>
              <font color="#808080" size="1" face="Arial">
              <input class="Preferences" type="checkbox" name="ffilename" value="1" checked >Filename<br>
              <input class="Preferences" type="checkbox" name="ffilesize" value="1" checked >Size<br>
              <input class="Preferences" type="checkbox" name="fdate" value="1" checked >Date<br>
              <input class="Preferences" type="checkbox" name="fdimensions" value="1" checked >Dimensions<br>
              <input class="Preferences" type="checkbox" name="fprojectname" value="1" checked >Project 
						Name<br></td>
					<td>&nbsp;</td>
					<td valign="top">
					

              </td>
					</tr>
				</table>
					<br><font size="1">Note:&nbsp; </font>
				<font face="Arial" size="1" color="">
				Selecting too many fields may prevent the page from printing 
				properly.  Please use your Print Preview function to 
				ensure that the contact sheet page breaks are aligned properly 
				before printing.<br>
				</b></font><font face="Arial" size="1" color="#000000"><br>
				</font><font face="Arial" size="1" color=""><br>
				<br>
              <input type="button" onclick="javascript:submitContactSheet();" value="View" name="Add"></p></p>
              <input type="hidden" name="actionType" value="edit">
              <input type="hidden" name="carr" value="<%=request.querystring("Carousel_id")%>" >
              <input type="hidden" name="asset_array" value="" >
            <!--</form>-->
            <p>
            
            


<!--Close Rounded Box-->
					</td>
					<td width="4">&nbsp;</td>
				</tr>
			</table>
			</div>
        <div class="last">
        	<span class="first"></span><span class="last"></span>
        </div>
    </div>
</div>
<!--End Rounded Box-->
<!--Start Close Rounded Box Table-->
								</td>
								<td><img border="0" src="images/spacer.gif" height="2" width="11"></td>
							</tr>
</table>
<!--End Close Rounded Box Table-->
</div>

			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			<asp:Literal id="literalNoAssets" Text="No assets added yet." visible=false runat="server" />
			
			
			

											<style>
											div.asset_name      { width:164px; overflow: hidden;padding-left:5px;
																text-overflow-mode:ellipsis;font-size:8px }
											div.asset_date      { width:120px; overflow: hidden;padding-left:5px;
																text-overflow-mode:ellipsis;font-size:8px }  
											div.asset_filetype  { width:80px; overflow: hidden;
																text-overflow-mode:ellipsis;font-size:8px }                  
											div.asset_filesize  { width:60px; overflow: hidden;
																text-overflow-mode:ellipsis;font-size:8px }    
											div.asset_owner     { width:120px; overflow: hidden;padding-left:5px;
																text-overflow-mode:ellipsis;font-size:8px }    
											div.asset_links    {width:100%;padding-left:2px; height:4px;overflow:hidden;background-image: url(images/dottedline.gif);background-repeat:repeat-x;}                                                                           
											span.nowrap       { white-space : nowrap; }
											div.attributed-to { position: relative;left:8px }

											</style>

											<COMPONENTART:CALLBACK id="ThumbnailCallBack" runat="server" CacheContent="true">
												<CONTENT>
													<!--hidden Thumbs-->
													<asp:Literal id="ltrlPager" Text="" runat="server" />
													
													<asp:Repeater id=Thumbnails runat="server" Visible="False">
														<ItemTemplate>
															<div valign="Top" style="float:left!important; width:220px!important; width:180px; height:320px!important; height:220px;"> 
																<div style='width:170px;height:220px;'><!--Asset Thumbnails-->
																	<div align="left"><!--PADDING LEFT X1-->
																		<div align="left"><!--PADDING LEFT X2-->
																			<table  class="bluelightlight" border="0" cellspacing="0" width="15">
																				<tr>
																				<td  class="bluelightlight" valign="top">
																					<div align="left" id="assethighlight_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" style="border:1px white solid;margin:5px;">
																						<table border="0"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse"><tr><td>
																						<table border="0"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse" height="120" width="160">
																						<tr class="bluelightlight" width="100%" align="center" height="120" width="160"><td class="bluelightlight" width="100%" height="120" width="160" align="center" valign="bottom" bordercolor="#808080" bordercolorlight="#808080" nowrap><img src="images\spacer.gif" height="0" width="159"><br><a id="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" href="javascript:highlightAssetToggleII(document.getElementById('aselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>'),<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);"><img alt="" border="0" src="
																						<%#DataBinder.Eval(Container.DataItem, "timage")%>"></a></td><td><img src="images\spacer.gif" width="0" height="140"></td>

																						</tr>
																						</table></td></tr></table>
																					</div>
																				</td>
																				</tr>
																				<tr>
																				<td>      
																					<!--PADDING-->
																					<div style="padding:5px;">             
																						<table width="100%" cellspacing="0" cellpadding="0" >
																							<tr>
																								<td nowrap width="5"><img src="<%#DataBinder.Eval(Container.DataItem, "imagesource")%>"></td>
																								<td nowrap><div class="asset_name"><span class="nowrap"><b><font face="Verdana" size="1"><%#DataBinder.Eval(Container.DataItem, "name")%></font></b></span></div></td>
																								<td align="right" width="20"><font color="#808080" face="Arial" size="1"><input onclick="highlightAssetToggle(this,<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);" class="preferences" type="checkbox" name="aselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" id="aselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" value="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"><input type=hidden id=selectassethidden value="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"></font></td>
																							</tr>
																						</table>
																						<table width="100%"  cellspacing="0" cellpadding="0" >
																							<tr>
																								<td nowrap width="100%" align="left"><div class="asset_filetype"><span class="nowrap"><font face="Verdana" size="1"><%# GetAssetFileTypeName(CType(DataBinder.Eval(Container.DataItem, "media_type"), String))%></font></span></div></td>
																							<td align="right"><div class="asset_filesize"><span class="nowrap"><font face="Verdana" size="1"><%# FormatSize(CType(DataBinder.Eval(Container.DataItem, "filesize"), Long))%></font></span></div></td>
																							</tr>
																						</table>
																						<table width="100%" cellspacing="0" cellpadding="0" >
																							<tr>
																								<td nowrap><font face="Verdana" size="1">Updated</font></td>
																								<td align="left" width="868"><div class="asset_date"><span class="nowrap"><font face="Verdana" size="1">

																							<%# String.Format("{0:MM/dd/yy hh:mm tt}", (DataBinder.Eval(Container.DataItem, "update_date")))%></font></span></div></td>

																							</tr>
																							<tr>
																								<td nowrap><font face="Verdana" size="1">Created By</font></td>
																								<td align="left" width="868"><div class="asset_owner"><span class="nowrap"><font face="Verdana" size="1"><%# GetAssetOwner(CType(DataBinder.Eval(Container.DataItem, "asset_id"), String))%></font></span></div></td>
																							</tr>
																							<tr>
																								<td nowrap><font face="Verdana" size="1">Project</font></td>
																								<td align="left" width="868"><div class="asset_owner"><span class="nowrap"><font face="Verdana" size="1"><%# GetAssetProjectName(CType(DataBinder.Eval(Container.DataItem, "projectid"), String))%></font></span></div></td>
																							</tr>
																						</table>
																						<div style="padding-top:3px;">
																							<div class="asset_links"></div>
																						</div>
																						<b><font face="Verdana" size="1"><a href="javascript:window.parent.NavigateToAsset('<%#DataBinder.Eval(Container.DataItem, "asset_id")%>');">View Details</a></font><br>
																						<!--<font face="Verdana" size="1"><a href="javascript:DownloadAssetsThumbs('0 <%#DataBinder.Eval(Container.DataItem, "asset_id")%>');">Download</a></font><br><br>-->
																						<font face="Verdana" size="1"><a href="javascript:window.parent.RemoveItemFromCarousel(<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);ThumbnailCallBackPage('<%#DataBinder.Eval(Container.DataItem, "asset_id")%>,remove');">Remove</a></font><br>
																						         
																					</div>                        
																					<!--END PADDING-->
														                        
																				</td>
																				</tr>
																			</table>
																		</div><!--END PADDING LEFT X2-->
																		<img border="0" src="images/spacer.gif" width="135" height="8">
																	</div><!--END PADDING LEFT X1-->
																</div><!--end Asset Thumbnail span-->
															</div><!--end inner span-->
														</ItemTemplate>
													
														<HeaderTemplate>
														<div style=" height: 455px; overflow: auto;">
															<!--Start Thumbnail Float-->
															<table width=100%><tr><td>
																<div valign="Bottom" style="float:left!important;">       
														</HeaderTemplate>
														
																
														<FooterTemplate>
															</td></tr></table></div>
														</FooterTemplate> 
															
													</asp:Repeater>
													
													<asp:Literal id="ltrlPagingBottom" Text="" runat="server" />
												</CONTENT>
												<LOADINGPANELCLIENTTEMPLATE>
													<TABLE height="300" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD align="center">
																<TABLE cellSpacing="0" cellPadding="0" border="0">
																	<TR>
																		<TD style="FONT-SIZE: 10px">Loading...
																		</TD>
																		<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
													</TABLE>
												</LOADINGPANELCLIENTTEMPLATE>
											</COMPONENTART:CALLBACK>
			
			
			
											<COMPONENTART:CALLBACK id="carousellist_callback" runat="server" CacheContent="true">
												<CONTENT>
													
												</CONTENT>
												<LOADINGPANELCLIENTTEMPLATE>

												</LOADINGPANELCLIENTTEMPLATE>
											</COMPONENTART:CALLBACK>
			
		

			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			</div><!--test-->
			
			
		</ComponentArt:PageView>		



		
	</ComponentArt:MultiPage>
	<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
					<div style="text-align:right;"><br>
			<asp:Button id="btnCarouselSave" runat="server" Text="Save"></asp:Button><img src="images/spacer.gif" width=5 height=1><input type="button" onclick="<% Response.Write(carousellist_callback.ClientID) %>.Callback('0,setdefault');window.setTimeout('window.parent.CloseDialogWindowXCarousel()',500);" value="Close" />
			</div>
	</div>
</div>

			
			
			
			

			</div>
			
			
			
			
			
			
			
			
			</div>
			

			
			</div>
			</div>
			
		
			
			
		</form>








<script language="javascript">

  
    function ThumbnailCallBackPage(page)
  {
    <% Response.Write(ThumbnailCallBack.ClientID) %>.Callback(page);
  }
  
  
  function highlightAssetToggle(checkbox,asset_id)
  {
  var highlightbox;
  highlightbox = document.getElementById('assethighlight_' + asset_id);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  
  
    function highlightAssetToggleII(checkbox,asset_id)
  {
  var highlightbox;
  highlightbox = document.getElementById('assethighlight_' + asset_id);
  if (checkbox.checked) {
  	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
	checkbox.checked = false;

  } else
  {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  checkbox.checked = true;
  }}
  
  
    
  function RemoveItemFromCarousel(id)
  {
  
  <% Response.Write(carousellist_callback.ClientID) %>.Callback(id + ',remove');
  //return true;
  
  }	
  
    <%if request.querystring("tab") = "view" then%>
    <% Response.Write(TabStripCarousel.ClientID) %>.SelectTabById('Carousel_Items');
  
  <%end if%>

  function deleteCarousel()
  {
		if (confirm("Delete this carousel?"))
			 {
				window.parent.RemoveCarousel('<%=Request.QueryString("Carousel_id")%>');
				window.parent.CloseDialogWindowXCarousel();
				//self.close();
				return false;
			}
			else
			{

			}	
			return true;
  }
  
  
  
//setup form switch for delete or multi asset zip download
function DownloadAssetsThumbs(ids){
	var arraylist;
	var i;
	var sAssets;
	var downloadtype;
	arraylist = '';
	ids = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
	if (arraylist.split(',').length > 2) {
			 if (confirm("Download selected assets?"))
			 {
				arraylist= arraylist.substring(1, arraylist.length);
				downloadtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				downloadtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	downloadtype = 'single';
	}
	if (downloadtype == 'multi') {
		if (arraylist != ''){
		window.open('<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLLocationIDAMDownloadGetMulti & "?assetids="%>' + arraylist + '<%="&instance=" & IDAMInstance%>','ConvertingImage','width=500,height=75,location=no');
		}
	}
	if (downloadtype == 'single') {
	/*assume single*/
	window.location = '<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLLocationIDAMDownload & "?dtype=assetdownload&assetid="%>' + ids + '<%="&instance=" & IDAMInstance%>&size=0';
	return true;
	}

}
  

function showContactSheet() {
	if (contactsheet.style.display == "block") {
		contactsheet.style.display = "none";
	} else {
	if (contactsheet.style.display == "none") {
		contactsheet.style.display = "block";
	}
	}
}


function cloneCarousel()
{
	if (confirm("Clone this carousel?"))
		{
		var cname = prompt(" Enter the name for the new carousel. ","");
			if (cname != '' && cname != null )
			{
				 <% Response.Write(ThumbnailCallBack.ClientID) %>.Callback('CLONE,' + cname);
				 alert('Carousel cloned.  Please close this window and select the cloned carousel from the carousel listing on the left panel.');
			}
		}
}

<%if request.querystring("tab") = "email" then%>
TabStripCarousel.SelectTabById('Carousel_Email');
<%end if%>

function view_email_history(ID)
{
window.open('CarouselemailHistory.aspx?ID=' + ID + '<%="&instance=" & IDAMInstance%>','EmailHistory','width=700,height=500,location=yes,resizable=yes');
}
  

</script>	
















		
	</body>
</HTML>








