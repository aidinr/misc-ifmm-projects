<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.ContactSheet" EnableViewStateMac="False" CodeBehind="ContactSheet.aspx.vb" %>
<%@ Import Namespace="System.Data" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>Contact Sheet</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <style>div.break {page-break-before:always}</style> 
    <style media="print">
	.print {
			display: none;
		}
	</style>
  </head>
  <body topmargin="0" style="font-size: 9px;" leftmargin="0" onload="<%if request.form("buttonused") = "print" then response.write("window.print();")%>">

    <form id="Form1" method="post" runat="server">
<font face="Verdana" >
<%

'Check Login - Put this in and inc file
    Dim sThumbAssetoidID, txtFileContents, tmpAssetInformation, bfdimensions, bfdate, bfassetid, bfprojectname, bffilesize, bffilename, xWidth, xpWidth, iProjectID, sqltemp, sLineImage, ThumbnailImageHeight, ThumbnailImageWidth, aAsset_Array, aCarouselID, aSortID, sHeadingVal, bffiletype As String
sLineImage = "images/line_som.jpg"
'set vars
    txtFileContents = ""
	aAsset_Array = Session("contactsheetitems")
	sHeadingVal = request.form("Heading")
	bffiletype = request.form("ffiletype")
	bffilename = request.form("ffilename")
	bffilesize = request.form("ffilesize")
	bfprojectname = request.form("fprojectname")
	bfassetid = request.form("fassetid")
	bfdate = request.form("fdate")
	bfdimensions = request.form("fdimensions")


aCarouselID = request.form("carr")
aSortID = request.form("sort_id")
dim rsAssetsUDF 

xWidth = 3
xpWidth = 3


dim i as integer
dim iudf as integer
dim sThumbLocationID,sThumbAssetID,sThumbDescription ,sThumbName,sThumbProjectName
dim sThumbRepo as string
dim iInput,iText,irowcount,pagebreak,xHeight,acount,iout,iin,sPage,sType,strSubLocationTmp,strSubLocation,strSubLocation1,strSubLocation2 




'handle remaining images.
	%>
	<div style="padding-top:10px">
	
	
	
	
	
	<table   align = center><tr><td><b><font face="Verdana" size="1">
           
         	<%	    Response.Write(Replace(sHeadingVal, vbCrLf, "<br>"))
         	       If sHeadingVal.Trim <> "" Then
         	           txtFileContents += sHeadingVal & vbCrLf & vbCrLf & vbCrLf & vbCrLf
         	       End If
         	   %><br>

         	</font></b>
             
             
             <img border="0" src="images/line_som.gif"><img border="0" src="images/line_som.gif"><img border="0" src="images/line_som.gif"><img border="0" width=30 height=0 src="images/spacer.gif"><br>
             </td></tr><tr><td align=left  class="print">
             <b><font face="Verdana" size="1"><A HREF="javascript:window.print()">print</a></font></b><br></td></tr></table>   

	
	</div><%

if aCarouselID = "" then

	              sqltemp = "select a.oid,a.asset_id,a.location_id,a.description,a.name,0 page from ipm_asset a,  ipm_filetype_lookup c where a.media_type = c.media_type and a.asset_id in (" & aAsset_Array & ")"

else
	              sqltemp = "select a.oid,a.asset_id,a.location_id,a.description,a.name,isnull(b.page,0) page from ipm_asset a, ipm_carrousel_item b, ipm_filetype_lookup c, ipm_user d where a.userid = d.userid and a.media_type = c.media_type and a.asset_id = b.asset_id and b.carrousel_id = " & aCarouselID & " and b.carouselitem_id in (" & aAsset_Array & ")"

			'add sort ordering
			if Session("sCAROrderBy") <> "" then
				'case
				'media_type_name
				'date_added
				'update_date
				select Session("sCAROrderBy")
					case "date_added"
						sqltemp = sqltemp + " order by b.available_date " & Session("sCARSort")
					case "media_type_name"
						sqltemp = sqltemp + " order by c.name " & Session("sCARSort")
					case "FullName"
						sqltemp = sqltemp + " order by d.lastname " & Session("sCARSort")		
					case "s_value"
						If Session("CUSTOMSORTORDER") <> "" And Session("CUSTOMSORTORDER") <> "0" Then
							sqltemp = "select cast(isnull(a.sort_value,0) as int) s_value,b.* from (" & sqltemp & ") b left outer join IPM_CARROUSEL_ITEM_SORT a on a.item_id = b.asset_id and a.sort_id = " & Session("CUSTOMSORTORDER")  & " order by a.sort_value " & Session("sCARSort")
						End If
					case "imagesource"
						sqltemp = sqltemp + " order by b.available_date " & Session("sCARSort")
					case else
						sqltemp = sqltemp + " order by a." & Session("sCAROrderBy") & " " & Session("sCARSort")
				end select	
				else
					sqltemp = sqltemp + " order by b.available_date  desc "
					
			end if

end if
rsAssets = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable(sqltemp)
if rsAssets.Rows.Count > 0 then
irowcount = 0
xWidth = request.form("columns")
select case xWidth 
	case 1
		ThumbnailImageHeight =700
		ThumbnailImageWidth = 598
		pagebreak = 1
	case 2
		ThumbnailImageHeight =299
		ThumbnailImageWidth = 299
		pagebreak = 2
	case 3
		ThumbnailImageHeight =200
		ThumbnailImageWidth = 200
		pagebreak = 3
	case 4
		ThumbnailImageHeight =120
		ThumbnailImageWidth = 120
		pagebreak = 4
end select
xHeight = rsAssets.Rows.Count / xWidth  + 1
aCount = 0

i = 0
%>
         <%if rsAssets.Rows.Count >= 1 then%>
         <%for iout = 1 to xHeight
         %>  
              <table align=center border="0" cellspacing="1" width="5" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                <tr>
                	<%for iin = 1 to cint(xWidth) %>
                	<%if not i = rsAssets.Rows.Count then %>
<%'get asset information

    sqltemp = "select ipm_asset.oid,wpixel,hpixel,ipm_asset.description,media_type,asset_id,ipm_asset.name,filesize,repository_id,location_id,ipm_asset.projectid,ipm_asset.update_date,ipm_asset.upload_date, ipm_project.name projectname from ipm_asset, ipm_project where ipm_asset.projectid = ipm_project.projectid and asset_id = " & rsAssets.Rows(i)("asset_id")
sPage = "asset_view.asp"
sType = "asset"

rsAssetInfo = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable(sqltemp)
if rsAssetInfo.Rows.Count = 0 then
sThumbRepo = 0
        sThumbAssetID = rsAssets.Rows(i)("asset_id")
        sThumbAssetoidID = WebArchives.iDAM.Web.Core.IDAMFunctions.checkNulltoStringAndTrim(rsAssets.Rows(i)("oid"))
sThumbLocationID = rsAssets.Rows(i)("location_ID")
sThumbDescription = rsAssets.Rows(i)("description")
sThumbName = rsAssets.Rows(i)("name")
sThumbProjectName = "N/A"
else
sThumbRepo = rsAssetInfo.Rows(0)("repository_id")
        sThumbAssetID = rsAssetInfo.Rows(0)("asset_id")
        sThumbAssetoidID = WebArchives.iDAM.Web.Core.IDAMFunctions.checkNulltoStringAndTrim(rsAssetInfo.Rows(0)("oid"))
sThumbLocationID = rsAssetInfo.Rows(0)("location_id")
sThumbDescription = rsAssetInfo.Rows(0)("description")
        sThumbName = rsAssetInfo.Rows(0)("name")

'if rsAssetInfoProject.state then rsAssetInfoProject.close
'sqltemp = "select ltrim(rtrim(ipm_project.name)) name from ipm_asset, ipm_project where ipm_asset.projectid = ipm_project.projectid and asset_id = " & rsAssetInfo("asset_id")
'rsAssetInfoProject.Open (sqltemp, Conn, 1, 4)
Try
	sThumbProjectName = rsAssetInfo.Rows(0)("projectname")
Catch ex As Exception
	sThumbProjectName = "N/A"
End Try

    End If
    tmpAssetInformation = "# " & i + 1 & " of " & rsAssets.Rows.Count & " IDAM #: " & sThumbAssetID & "<br>"
    txtFileContents += "# " & i + 1 & " of " & rsAssets.Rows.Count & vbCrLf
    txtFileContents += "IDAM #: " & sThumbAssetID & vbCrLf
    If sThumbAssetoidID <> "" Then
        txtFileContents += "Reference #: " & sThumbAssetoidID & vbCrLf
    End If
    
%>                	
                  <td width="50" valign="top">
                    <div align="left">
                      <table class="bluelightlight" border="0" cellspacing="0" width="<%if xWidth > 1 then 
                      response.write ("15") 
                      else 
                      response.write (ThumbnailImageWidth) 
                      end if%>">
                      <%if request.form("fimagepreview") = "1" then %>
                        <tr>
                          <td class="bluelightlight" valign="top">
                          <div align="left">
                            <table class="bluelightlight" border="0" cellspacing="0" width="5" cellpadding="0" >
                              <tr class="bluelightlight">
				<%
				select case sThumbRepo
					case "0"
						strSubLocationTmp = strSubLocation 
					case "1"
						strSubLocationTmp = strSubLocation1
					case "2"
						strSubLocationTmp = strSubLocation2
				end select%>
	
       <td class="bluelightlight" height="<%if xWidth > 1 then response.write (ThumbnailImageHeight)%>" width="<%=ThumbnailImageWidth%>" valign="middle" align="center"><img border="0" src="images/spacer.gif" width="<%=ThumbnailImageWidth%>" height="0"><img alt="" border="0" src="
  
  		<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?id=<%=sThumbAssetID%>&instance=<%response.write(IDAMInstance)%>&type=asset&size=1&width=<%=ThumbnailImageWidth%>&height=<%=ThumbnailImageHeight%>&page=<%=rsAssets.Rows(i)("page")%>">

                                                                </td>
                                <td class="bluelightlight" width="5" valign="top">


                				</td>
                				<td class="bluelightlight" width="0" valign="top"><img border="0" src="images/spacer.gif" width="0" height="<%if xWidth > 1 then response.write (ThumbnailImageHeight)%>">
                				</td>
                              </tr>
                            </table>
                          </div>
                          </td>
                        </tr>
                        <%end if %>
                        <tr>
                          <td >                          
                            <table border="0" width="100%" bordercolor="#C0C0C0" style="border-collapse: collapse">
							<tr>
								<td colspan="3">
								<table border="0" width="100%" cellspacing="1" cellpadding="0">
								<tr>
								<td valign=top width=1>
								<%if bffiletype <> "" then%><img border="0" src="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveIconURLBase%>?id=<%=rsAssetInfo.Rows(0)("media_type")%>&instance=<%response.write(IDAMInstance)%>&size=0"><%end if%><br><img border="0" src="images/spacer.gif" width="20" height="3"></td>
								<td >
								<%If bfassetid <> "" then %><font face="Verdana" size="1"><B><%=tmpAssetInformation%></B></font><%End If%>
								<font color="#808080" face="arial" size="1"><%if bfprojectname <> "" then%><img border="0" src="dash.jpg" width="4" height="4"><img border="0" src="images/spacer.gif" width="3" height="3"><b>Project:</b> <%response.write (sThumbProjectName & "<br>")%><%txtFileContents += "Collection: " & sThumbProjectName & vbCrLf%><%end if%></font>
								<%if bffilename <> "" then%><img border="0" src="dash.jpg" width="4" height="4"><img border="0" src="images/spacer.gif" width="3" height="3"><font color="#808080" face="arial" size="1"><b>Name:</b> <%=left(sThumbName,25)%></font><br><%txtFileContents += "Name: " & sThumbName & vbCrLf%><%end if%>
								<%if bffilesize <> "" then%><%iInput = ""
                             iText = ""
                       	     iInput = trim(rsAssetInfo.Rows(0)("Filesize"))
                        	    if iInput <> "" then
	                        	    itext = webarchives.iDAM.Web.Core.IDAMFunctions.formatSize(iInput)
	                       	  end if
	                       	  if iText <> "" then%><img border="0" src="dash.jpg" width="4" height="4"><font color="#808080" face="Arial" size="1"><img border="0" src="images/spacer.gif" width="3" height="3"><%=iText%></font><%end if%><img border="0" src="images/spacer.gif" width="3" height="3"><%end if%><%if bfdate <> "" then%><img border="0" src="dash.jpg" width="4" height="4"><font color="#808080" face="Arial" size="1"><img border="0" src="images/spacer.gif" width="3" height="3">Upload Date: <%=FormatDateTime(rsAssetInfo.Rows(0)("upload_date"),2)%></font><br><%end if%>
	                       	  <%if bfdimensions <> "" then%><%if cint(rsAssetInfo.Rows(0)("wpixel")) <> 0 and cint(rsAssetInfo.Rows(0)("wpixel")) <> 32  then%><img border="0" src="dash.jpg" width="4" height="4"><font color="#808080" face="Arial" size="1"><img border="0" src="images/spacer.gif" width="3" height="3"><%=rsAssetInfo.Rows(0)("wpixel")%> x <%=rsAssetInfo.Rows(0)("hpixel")%></font><%end if%><br><%end if%>


<%'add any user defined fields
    If Not Request.Form("UDF") Is Nothing Then

        rsAssetsUDF = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select a.item_type,b.item_value, a.item_name from ipm_asset_field_desc a, ipm_asset_field_value b where a.item_id = b.item_id and b.asset_id = " & sThumbAssetID & " and b.item_id in (" & Request.Form("UDF").Trim() & ") order by item_group,item_name")
        For Each row As datarow In rsAssetsUDF.rows
            'get value for this asset
            'rsAssetsUDF = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select a.item_type,b.item_value, a.item_name from ipm_asset_field_desc a, ipm_asset_field_value b where a.item_id = b.item_id and b.asset_id = " & sThumbAssetID & " and b.item_id = " & Request.Form("UDF").Split(",")(iudf).Trim())
            If row("item_value").trim <> "" Then
                txtFileContents += row("item_name") & ": " & row("item_value") & vbCrLf & vbCrLf%>
	                       	  	
	                       	  	<table><tr><td valign="top"><font color="#808080" face="Verdana" size="1"><b><%=row("item_name")%>:</b></font></td><td valign="top"><font color="#808080" face="Verdana" size="1">
	                       	  	
	                       	  	 <%
	                       	  	     If row("item_type") = 10 Then
	                       	  	         If row("item_value") = 1 Then
	                       	  	             Response.Write("Yes")
	                       	  	         Else
	                       	  	             Response.Write("No")
	                       	  	         End If
	                       	  	     Else
	                       	  	         
	                       	  	         Response.Write(row("item_value").replace(vbCrLf, "<br>"))
	                       	  	     End If%></font></td></tr></table><%end if
							  next
end if							  
	                       	  %>

	                       	  
	                       	  
	                       	  </td>
								</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td>
							
	                       	  </td>
								<td></td>
								
							</tr>
							</table>
							</td>
                        </tr>
                      </table>
                    </div>
                   <img border="0" src="images/spacer.gif" width="135" height="8"></td>
                  <%
                  i = i + 1
                  end if
                  txtFileContents += vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf
                  next%>
                  </tr>
              </table>
           <%if (iout mod (pagebreak) = 0) and not i = rsAssets.Rows.Count  then 
			response.write ("<DIV style=""page-break-after: always;""></DIV>") 
				%>
	<div style="padding:0px">

	</div><%
           end if
	      %>
         	<%next%>
         	<%else%>
              
         	<%end if%>
<!--</td></tr></table>-->
<%
irowcount = irowcount + 1
%>
<%end if%>      











<%
    If Request.Form("buttonused") = "exporttxt" Then
        'check for pipe to txt
        Response.Clear()
        Response.ClearContent()
        Response.ClearHeaders()
        Response.AddHeader("content-disposition", "attachment;filename=data.txt")
        Response.ContentType = "text/csv"
        Response.Write(txtFileContents)
        Response.End()
    End If

    
    %>





































</div>










    </form>

  </body>
  


</html>
