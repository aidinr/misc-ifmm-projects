<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.Keywords" CodeBehind="Keywords.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Keywords</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
<link href="treeStyle.css" type="text/css" rel="stylesheet">
<link href="navStyle.css" type="text/css" rel="stylesheet" >
<link href="tabStripStyle.css" type="text/css" rel="stylesheet" />    
<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
										<style>BODY { MARGIN: 0px }
	</style>
	<body onload="this.focus">
		<form id="Form" method="post" runat="server">
			<script type="text/javascript">
  
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridKeywords.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    GridKeywords.Edit(GridKeywords.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    GridKeywords.EditComplete();     
  }

  function insertRow()
  {
    GridKeywords.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    GridKeywords.Delete(GridKeywords.GetRowFromClientId(rowId)); 
  }

      
			</script>
			
			<div style="padding:20px;position:relative;">
			<div style="">
			<div style="padding:10px;border:1px solid;background-color:white;position:relative;">
			
			<div style=";padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Modify keywords by clicking the edit or delete links.  To add a new keyword, click the "Add Keyword" button.  Note:  You cannot permanently delete a keyword from the system.</div><br>
  <div style="font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">
  <div style="width:100%;">
			<ComponentArt:Grid id="GridKeywords" RunningMode="Callback" CssClass="Grid" GroupByTextCssClass="GroupByText"
				GroupingNotificationTextCssClass="GridHeaderText" ShowFooter="true" PageSize="20" ImagesBaseUrl="images/"
				EditOnClickSelectedItem="true" AllowEditing="true" Sort="KeyName" ScrollBar="Off" ScrollTopBottomImagesEnabled="true"
				ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/"
				ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip"
				ScrollBarWidth="16" ScrollPopupClientTemplateId="ScrollPopupTemplate" FillWidth="true" Width="100%" Height="207" AutoCallBackOnDelete="true"  
 AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true"  runat="server" PagerStyle="Numbered"
        PagerTextCssClass="PagerText" 
        >
				<Levels>
					<ComponentArt:GridLevel DataKeyField="Keyid" ShowTableHeading="false" ShowSelectorCells="false" HeadingCellCssClass="HeadingCell"
						HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive" HeadingTextCssClass="HeadingCellText"
						DataCellCssClass="DataCell" RowCssClass="Row" SelectedRowCssClass="SelectedRow" SortAscendingImageUrl="asc.gif"
						SortDescendingImageUrl="desc.gif" ColumnReorderIndicatorImageUrl="reorder.gif" SortedDataCellCssClass="SortedDataCell"
						SortImageWidth="14" SortImageHeight="14" EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField"
						EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate">
						<Columns>
						<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" DataCellClientTemplateId="Column1Template"  DataField="imagesource" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" Width="20" FixedWidth="True"/>
							<ComponentArt:GridColumn DataField="KeyName" HeadingText="Name" Width="120" FixedWidth="True"/>
							<ComponentArt:GridColumn DataField="Keydescription" HeadingText="Description" Width="120" FixedWidth="True"/>
							<ComponentArt:GridColumn DataField="KeyUse" HeadingText="Visible" Width="50" FixedWidth="True"/>
							
							<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Edit Command" AllowSorting="False" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand"
								Width="100" Align="Center" FixedWidth="True"/>
							<ComponentArt:GridColumn DataField="KeyId" Visible="false" />
						</Columns>
					</ComponentArt:GridLevel>
				</Levels>
				<ClientTemplates>
					<ComponentArt:ClientTemplate Id="EditTemplate">
            <a href="javascript:editGrid('## DataItem.ClientId ##');">Edit</a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> | <a href="javascript:GridKeywords.EditCancel();">Cancel</a>
          </ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> | <a href="javascript:GridKeywords.EditCancel();">Cancel</a>
          </ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="LoadingFeedbackTemplate">
						<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td style="font-size:10px;">Loading...&nbsp;</td>
								<td><img src="images/spinner.gif" width="16" height="16" border="0"></td>
							</tr>
						</table>
					</ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="Column1Template">
						<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td><img src="images/## DataItem.GetMember("imagesource").Value ##" width="16" height="16" border="0"></td>
	
							</tr>
						</table>
					</ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="Column1TemplateEdit">
						<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td><img src="images/9.gif" width="16" height="16" border="0"></td>
								<td style="padding-left:2px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;"><nobr>## 
											DataItem.GetMember("KeyName").Value ##</nobr></div>
								</td>
							</tr>
						</table>
					</ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="ScrollPopupTemplate">
						<table cellspacing="0" cellpadding="2" border="0" class="ScrollPopup">
							<tr>
								<td style="width:20px;"><img src="images/9.gif" width="16" height="16" border="0"></td>
								<td style="width:130px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;width:130px;"><nobr>## 
											DataItem.GetMember("KeyName").Value ##</nobr></div>
								</td>
							</tr>
						</table>
					</ComponentArt:ClientTemplate>
				</ClientTemplates>
				<ServerTemplates>
          <ComponentArt:GridServerTemplate Id="PickerTemplate">
            <Template>

 	   <table cellspacing="0" cellpadding="0" border="0">
	    <tr>
	      <td onmouseup="Button_OnMouseUp()"><ComponentArt:Calendar id="Picker1" 
	          runat="server" 
	          PickerFormat="Custom" 
	          PickerCustomFormat="MMMM d yyyy" 
	          ControlType="Picker" 
	          SelectedDate="2005-9-13"
	          ClientSideOnSelectionChanged="Picker1_OnDateChange"
	          PickerCssClass="picker" /></td>
	      <td style="font-size:10px;">&nbsp;</td>
	      <td><img id="calendar_from_button" alt="" onclick="Button_OnClick(this)" onmouseup="Button_OnMouseUp()" class="calendar_button" src="images/btn_calendar.gif" /></td>
	    </tr>
	    </table>

		<ComponentArt:Calendar runat="server"
		      id="Calendar1" 
		      AllowMultipleSelection="false"
		      AllowWeekSelection="false"
		      AllowMonthSelection="false"
		      ControlType="Calendar"
		      PopUp="Custom"
		      PopUpExpandControlId="calendar_from_button"
		      CalendarTitleCssClass="title" 
		      SelectedDate="2005-9-13"
		      VisibleDate="2005-9-13"
		      ClientSideOnSelectionChanged="Calendar1_OnChange" 
		      DayHeaderCssClass="dayheader" 
		      DayCssClass="day" 
		      DayHoverCssClass="dayhover" 
		      OtherMonthDayCssClass="othermonthday" 
		      SelectedDayCssClass="selectedday" 
		      CalendarCssClass="calendar" 
		      NextPrevCssClass="nextprev" 
		      MonthCssClass="month"
		      SwapSlide="Linear"
		      SwapDuration="300"
		      DayNameFormat="FirstTwoLetters"
		      PrevImageUrl="images/cal_prevMonth.gif" 
  		      NextImageUrl="images/cal_nextMonth.gif"
    			/>

            </Template>
          </ComponentArt:GridServerTemplate>
        </ServerTemplates>
			</ComponentArt:Grid>
			</div>
			<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("ADD_DELETE_KEYWORDS") Then%>
			<br>
			<input type="button" onclick="GridKeywords.Page(GridKeywords.PageCount-1);GridKeywords.Table.AddRow();" value="Add Keyword" />
			<%end if%>
			</div>
			
			
			
			
			
			
			
			
			</div>
			
			<div style="text-align:right;"><br>
			<input type="button" onclick="try{<%
			if request.querystring("Noreturn") = "" then
				select case request.querystring("Keyword")
					case "Discipline"
						response.write ("window.parent.RefreshKeywordDiscipline();")
					case "Keyword"
						response.write ("window.parent.RefreshKeywordKeyword();")
					case "Office"
						response.write ("window.parent.RefreshKeywordOffice();")
					case "Services"
						response.write ("window.parent.RefreshKeywordServices();")
					case "MediaType"
						response.write ("window.parent.RefreshKeywordMediaType();")
					case "IllustType"
						response.write ("window.parent.RefreshKeywordIllustType();")																		
				end select
			end if%>}catch(err){}try{<%
			if request.querystring("Noreturn") = "" then
				select case request.querystring("Keyword")
					case "Discipline"
						response.write ("window.opener.RefreshKeywordDiscipline();")
					case "Keyword"
						response.write ("window.opener.RefreshKeywordKeyword();")
					case "Office"
						response.write ("window.opener.RefreshKeywordOffice();")
					case "Services"
						response.write ("window.opener.RefreshKeywordServices();")
					case "MediaType"
						response.write ("window.opener.RefreshKeywordMediaType();")
					case "IllustType"
						response.write ("window.opener.RefreshKeywordIllustType();")																		
				end select
			end if%>}catch(err){}closewindow();" value="Ok" /><img src="images/spacer.gif" width=5 height=1><input type="button" onclick="closewindow();" value="Cancel" />
			</div>
			
			</div>
			</div>
			
		<script language=javascript type="text/javascript">


		    function closewindow() {
		      
		        try {
		            window.close();

		        } catch (err) {
		    }
		    try { window.parent.CloseDialogWindowX(); } catch (err) { }
		    
		    }
		</script>
			
			
		</form>
	</body>
</HTML>
