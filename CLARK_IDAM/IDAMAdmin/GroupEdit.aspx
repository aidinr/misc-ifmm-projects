<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.GroupEdit" ValidateRequest="False" CodeBehind="GroupEdit.aspx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>Group</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
		<link href="treeStyle.css" type="text/css" rel="stylesheet">
		<link href="navStyle.css" type="text/css" rel="stylesheet">
		<link href="tabStripStyle.css" type="text/css" rel="stylesheet">
		<link href="navBarStyle.css" type="text/css" rel="stylesheet">
		<style>BODY { MARGIN: 0px }
	</style>
</HEAD>
	<body onload="this.focus">
		<form id="Form" method="post" runat="server">
			<script type="text/javascript">
   
   
   function setForderPreferredFolder(_val)
  {
  <%=CallBackGroupEdit.ClientID %>.Callback('ForderPreferredFolder,'+_val);
  }
   function setCreateFolder(_val)
  {
  <%=CallBackGroupEdit.ClientID %>.Callback('CreateFolder,'+_val);
  }   
   function addNewFolder()
  {
  var foldername;
  foldername = document.getElementById('preferredfolder').value;
  <%=CallBackGroupEdit.ClientID %>.Callback('AddNewFolder,'+foldername);
  }      
   function removeFolder(_val)
  {
  <%=CallBackGroupEdit.ClientID %>.Callback('removeFolder,'+_val);
  }  
     
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdateGridUploadFolders(sender, eventArgs)
{

  var item = eventArgs.get_item();
  //alert('UpdateFolder,'+item.GetMember('id').Value+','+item.GetMember('CreateFolder').Value+','+item.GetMember('Priority').Value);
  <%=CallBackGroupEdit.ClientID %>.Callback('UpdateFolder,'+item.GetMember('id').Value+','+item.GetMember('CreateFolder').Value);
  return true;
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridKeywords.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function deleteRowUploadfolders(rowId)
  {
   <%=CallBackGroupEdit.ClientID %>.Callback('DeleteFolder,'+rowId);
  }
  
  function editGrid(rowId)
  {
    GridKeywords.Edit(GridKeywords.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    GridKeywords.EditComplete();     
  }

  function insertRow()
  {
    GridKeywords.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    GridKeywords.Delete(GridKeywords.GetRowFromClientId(rowId)); 
  }

<%if request.querystring("newid") <> "" then%>
//window.opener.NavigateToPage('User');
//window.close();
<%end if%>


			</script>
			<%'response.write (request.querystring('newid'))%>
			<input type="hidden" name="catid" id="catid" value="<%'response.write (replace(request.querystring('p'),'CAT',''))%>">
			<div style="PADDING-RIGHT:20px;PADDING-LEFT:20px;PADDING-BOTTOM:20px;PADDING-TOP:20px;POSITION:relative">
				<div>
					<div style="BORDER-RIGHT:1px solid; PADDING-RIGHT:10px; BORDER-TOP:1px solid; PADDING-LEFT:10px; PADDING-BOTTOM:10px; BORDER-LEFT:1px solid; PADDING-TOP:10px; BORDER-BOTTOM:1px solid; POSITION:relative; BACKGROUND-COLOR:white">
						<table>
							<tr>
								<td><img src="images/group16.gif"></td>
								<td>Create/Edit Group</td>
							</tr>
						</table>
						<div style="BORDER-RIGHT:#b7b4b4 1px solid; PADDING-RIGHT:5px; BORDER-TOP:#b7b4b4 1px solid; PADDING-LEFT:5px; FONT-WEIGHT:normal; FONT-SIZE:10px; PADDING-BOTTOM:5px; BORDER-LEFT:#b7b4b4 1px solid; WIDTH:100%; COLOR:#3f3f3f; PADDING-TOP:5px; BORDER-BOTTOM:#b7b4b4 1px solid; FONT-FAMILY:verdana; BACKGROUND-COLOR:#e4e4e4">Create 
							a group by entering a name and description.</div>
						<br>
						<div style="WIDTH: 100%">
							<COMPONENTART:TABSTRIP id="TabStripCategory" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataGroup.xml"
								CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
								DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
								<ItemLooks>
									<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif" HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
									<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif" LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
								</ItemLooks>
							</COMPONENTART:TABSTRIP>
							<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageCategoryPage" runat="server">
							
								<ComponentArt:PageView CssClass="PageContent2" runat="server" ID="User_General">
<DIV style="BORDER-RIGHT: #b7b4b4 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #b7b4b4 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: normal; FONT-SIZE: 10px; PADDING-BOTTOM: 5px; BORDER-LEFT: #b7b4b4 1px solid; COLOR: #3f3f3f; PADDING-TOP: 5px; BORDER-BOTTOM: #b7b4b4 1px solid; FONT-FAMILY: verdana; BACKGROUND-COLOR: #e4e4e4">Enter 
										a name, description and security level for this group.</DIV>
									<BR>
									<TABLE id="Table1" cellSpacing="0" cellPadding="3" width="100%" border="0">
									<TR>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Group Name
											</FONT>
											</TD>
											<TD vAlign="top" align="left" width="215">
												<asp:TextBox id="name" runat="server" CssClass="InputFieldMain" Height=25px></asp:TextBox></TD>
											<TD vAlign="top" align="right" width="400">
												<FONT face="Verdana" size="1">Administrator</FONT>
											</TD>
											<TD vAlign="top" align="left" width="100%">
												<asp:CheckBox id="administrator" CssClass="InputFieldMainCheckbox" runat="server"></asp:CheckBox></TD>
										</TR>
										<TR>
											<TD class="PageContent" vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Security Level</FONT></TD>
											<TD class="PageContent" vAlign="top" align="left" width="100%" colspan="3">
												<asp:DropDownList id="SecurityLevel" Runat="server" CssClass="InputFieldMain" Height=25px>
													<asp:ListItem Value="0">Administrator</asp:ListItem>
													<asp:ListItem Value="1">Internal Use</asp:ListItem>
													<asp:ListItem Value="2">Client Use</asp:ListItem>
													<asp:ListItem Value="3">Public</asp:ListItem>
												</asp:DropDownList></TD>
										</TR>
										
										<TR>
											<TD class="PageContent" vAlign="top" noWrap align="left" width="120">
												<FONT face="Verdana" size="1">Description</FONT></TD>
											<TD class="PageContent" vAlign="top" align="left" width="180%" colspan="3">
												<asp:TextBox id="Description" runat="server" CssClass="InputFieldMain100P" style="height:200px" TextMode="MultiLine"></asp:TextBox></TD>
										</TR>									
										</table>
																
									
								</ComponentArt:PageView>
							
								
								
								
								
								
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="User_Membership">
									<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
									font-size: 10px;
									font-weight: normal;">Modify the user assignments to this group.</div><br>
									
								
								
								
								
						
																				
		
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td class="PageContent" width="360" valign=top>
<!--available list-->
<b>Available Users</b><br><br>



<COMPONENTART:GRID id="GridPermissionAvailable" runat="server" pagerposition="2" ScrollBar="Off" Sort="lastname asc" Height="10" Width="250" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategory" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" AllowPaging="true" cachecontent="false" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategory">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategory">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                            
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategory" InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataCellClientTemplateId="TypeIconTemplateCategory" DataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Firstname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="firstname" Width="70"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Lastname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="lastname" Width="100"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" AllowEditing="false" AllowGrouping="False" Width="120" SortedDataCellCssClass="SortedDataCell" DataField="description"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<div style=  "text-align:right;BORDER-RIGHT: 1px solid; PADDING-RIGHT: 4px; BORDER-TOP: 1px solid; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: 1px solid; PADDING-TOP: 4px; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: gainsboro">
<b>Search for user/group:</b> <input Class="InputFieldMain" type=text value="" id=search_filter  style="width:120px" onkeyup="javascript:<%response.write( GridPermissionAvailable.ClientId)%>.Filter('firstname LIKE \'%' + this.value + '%\' or lastname LIKE \'%' + this.value + '%\'');">
</div>
					</td>
					<td class="PageContent" width="120" valign= middle>
		
					<asp:Button id="btnAddPermissions" runat="server" Text="     Add >> "></asp:Button><br><br>
					<asp:Button id="btnRemovePermissions" runat="server" Text="<< remove"></asp:Button>
					</td>
					<td class="PageContent" width="100%" valign=top>
					
					
					
					
<!--active list-->

<b>Active Users</b><br><br>


<COMPONENTART:GRID id="GridPermissionActive" runat="server" autoPostBackonDelete="true" pagerposition="2" ScrollBar="Off" Sort="typeofobject asc" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategoryActive" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="false" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="false" CssClass="Grid" RunningMode="callback" AllowPaging="false" cachecontent="false" PagerStyle="Numbered" PageSize="500" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategoryActive">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateCategoryActive">
          <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategoryActive">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                                
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategoryActive" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataField="imagesource" DataCellClientTemplateId="TypeIconTemplateCategoryActive" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True"  />
<componentart:GridColumn AllowEditing="false" HeadingText="Firstname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="firstname" FixedWidth="false"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Lastname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="lastname" FixedWidth="false"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Description" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="description" width="100" FixedWidth="true"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
					</td>					
				</tr>
			</table>													
									

								</ComponentArt:PageView>
								
								
								
								
								
								
								
								
								
								
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="User_Assign_Roles">
									<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
									font-size: 10px;
									font-weight: normal;">Modify the group's assigned roles.</div><br>
									
Choose a Role Set<br>	
<table width=100%><tr>
<td width=300px><asp:DropDownList DataTextField="name" DataValueField="role_id" id="Roles" style="width:250px" runat="server"></asp:DropDownList></td>
<td width=100px><asp:Button id="Refresh" runat="server" Text="Refresh"></asp:Button></td>
<td width=100%><asp:Button id="Manage_Role_Sets" runat="server" Text="Manage Role Sets"></asp:Button></td>
</tr></table>				
<br>

<script language=javascript>
function changerole()
{
if (confirm('Are you sure you want to change this groups role?  This will erase any previous role overrides as well.')) {
__doPostBack('Roles','');
}
}
</script>

<table width=100%><tr>
<td width=100% style="FONT-SIZE: 11px; ">Role List</td>
<td width=200px align=right><asp:Button   id="roleOverrides" runat="server" Text="Clear Role Overrides"></asp:Button></td>
</tr></table>	
		<ComponentArt:TreeView id="TreeView_Roles" Height="380" Width="100%" 
		AutoPostBackOnNodeCheckChanged="true"
		DragAndDropEnabled="false" 
		NodeEditingEnabled="false" 
		KeyboardEnabled="true" 
		CssClass="TreeView" 
		NodeCssClass="TreeNode" 
		SelectedNodeCssClass="SelectedTreeNode" 
		HoverNodeCssClass="HoverTreeNode" 
		NodeEditCssClass="NodeEdit" 
		LineImageWidth="19" 
		LineImageHeight="20" 
		DefaultImageWidth="16" DefaultImageHeight="16" ItemSpacing="0" NodeLabelPadding="3" 
		ParentNodeImageUrl="images/folders.gif" LeafNodeImageUrl="images/folder.gif" ShowLines="true" 
		LineImagesFolderUrl="images/lines/" 
		EnableViewState="true"  
		runat="server" >
        </ComponentArt:TreeView>
									
									
								</ComponentArt:PageView>
								
								
								
								
								
								
								
								
								
								
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="PageView1">
									<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
									font-size: 10px;
									font-weight: normal;">Modify the group's preferred upload folder(s).</div><br>
									
								
								
								
								
						
																				
		
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td class="PageContent" width="360" valign=top>




<div style=  "text-align:left; width:360px;PADDING-RIGHT: 4px; PADDING-LEFT: 4px; 
    PADDING-BOTTOM: 4px; PADDING-TOP: 4px; ">
    <!--available list-->
<font face="Verdana" size="1"><b>Add Folder Path</b></font><br>
<input type="text" id="preferredfolder" style="width:300px;"/><input type="button"  onclick="addNewFolder();" id="addpreferredfolder" value=" + "/>
(example: "\My Folder\Here")<br /><br />
</div>
<table border="0" width="100%" id="table2">
<tr>
	<td><b><font face="Verdana" size="1">Global Options</font></b></td>
</tr>
</table>
<table style="font-family:Verdana; font-size:10;">
<tr>
<td valign=top><input disabled type=checkbox id=ForderPreferredFolder/></td>
<td valign=top><b>Require Upload Folder</b> <br />(If does'nt exist then do not allow upload.) <br /></td></tr>
<tr>
<td valign=top><input disabled type=checkbox id=CreateFolder/></td>
<td valign=top><b>Create Folder</b> <br />(If none of the upload folders are available then system will create the first folder in the list and begin upload.)</td></tr>
</table>

 

					</td>
					<td class="PageContent" width="120" valign= middle>
					
					</td>
					<td class="PageContent" width="100%" valign=top>
					
					
					
					
<!--active list-->
<!--<font face="Verdana" size="1"><b>Upload Folders</b></font><br>-->
<componentart:CallBack ID="CallBackGroupEdit" Debug="false" runat="server">
                                    <Content>
                                        <componentart:Grid ID="GridUploadFolders" EmptyGridText="No Folders Defined" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ClientSideOnInsert="onInsert" ClientSideOnUpdate="onUpdate" ClientSideOnDelete="onDelete" ScrollPopupClientTemplateId="ScrollPopupTemplate" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplate" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="20" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="True" GroupingPageSize="5" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="20" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="false" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="False" >
                                            <ClientEvents>
                                                <ItemCheckChange EventHandler="onUpdateGridUploadFolders"/>
                                            </ClientEvents>
                                            <ClientTemplates>
                                                <componentart:ClientTemplate ID="ClientTemplate3">
                                                    <div>
                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                            <tr>
                                                                <td style="font-size: 10px; font-family: Verdana;">
                                                                    <img src="images/spacer.gif" height="25" width="1"><br>
                                                                    <img src="images/spinner.gif" width="16" height="16" border="0">
                                                                    Loading...
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </componentart:ClientTemplate>
                                                <componentart:ClientTemplate ID="ClientTemplateDeleteUploadFolders">
                                                    <a href="javascript:deleteRowUploadfolders('## DataItem.ClientId ##')">Delete</a>
                                                </componentart:ClientTemplate>
                                            </ClientTemplates>
                                            <Levels>
                                                <componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategoryActive" DataKeyField="id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" ColumnReorderIndicatorImageUrl="reorder.gif" SortImageWidth="10" SortDescendingImageUrl="desc.gif" SortImageHeight="19" RowCssClass="Row">
                                                    <Columns>
                                                        <componentart:GridColumn AllowHtmlContent="True" AllowSorting="false" DataCellCssClass="FirstDataCellPostings" AllowEditing="True" HeadingText="Folder Path" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="path" FixedWidth="True" Width="250"></componentart:GridColumn>
                                                        <componentart:GridColumn AllowEditing="True" AllowSorting="false" Align="Center" HeadingText="Create" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="CreateFolder" FixedWidth="True" Width="50"></componentart:GridColumn>
                                                        <componentart:GridColumn AllowEditing="True" AllowSorting="false" Align="Center" Visible=false HeadingText="Priority" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="Priority" FixedWidth="True" Width="50"></componentart:GridColumn>
                                                        <componentart:GridColumn DataCellCssClass="LastDataCellPostings" AllowSorting="False" AllowEditing="false" DataCellClientTemplateId="ClientTemplateDeleteUploadFolders" HeadingText="Action" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" FixedWidth="True" Width="60"></componentart:GridColumn>
                                                        <componentart:GridColumn Visible="false" AllowEditing="false" HeadingText="id" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="id" FixedWidth="false"></componentart:GridColumn>
                                                    </Columns>
                                                </componentart:GridLevel>
                                            </Levels>
                                        </componentart:Grid>




                                    </Content>
                                </componentart:CallBack>   
					</td>					
				</tr>
			</table>													
									

								</ComponentArt:PageView>
                                
                            </ComponentArt:MultiPage>
                         
							<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
								<div style="TEXT-ALIGN:right"><br>
									<asp:Button id="btnProjectSave" runat="server" Text="Save"></asp:Button><img src="images/spacer.gif" width="5" height="1"><input type="button" onclick="window.parent.NavigateToPage('Group');window.parent.CloseDialogWindowX();" value="Close">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
<script language=javascript >
TabStripCategory.SelectTabById('<%response.write (request.querystring("tab"))%>');
</script>

<script language=javascript>
function Object_PopUp(strUrl, popupName, intHeight, intWidth)
{


	if (strUrl == null || strUrl.Length <= 0)
		return;

	var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=no,resizable=yes,toolbar=no";
	//var strFeatures = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;
	//var strFeatures += "'"
	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures , false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
}


function Object_PopUp_Roles()
{
Object_PopUp('RolesEdit.aspx','Edit_Role',700,700);

}
</script>
		</form>
	</body>
</HTML>
