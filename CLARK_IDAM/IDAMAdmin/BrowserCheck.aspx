<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.BrowserCheck" CodeBehind="BrowserCheck.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>BrowserCheck</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
		<div style="TEXT-ALIGN: center"><br><br><br><br><br><br><br>
			<img src="images/splash70.jpg" align="middle" ></div>
			<script type="text/javascript" src="includes/CookieScripts.js"></script>
			    <script type="text/javascript" src="js/Silverlight.js"></script>					
			
			<script type="text/javascript">

//DELETE ALL COOKIES
function delete_cookie (name, path)
{
	if (name.indexOf('MAINPAGE')!=-1||name.indexOf('MYPROJECTS')!=-1){
	// Build expiration date string:
	var expiration_date = new Date ();
	expiration_date . setYear (expiration_date . getYear () - 1);
	expiration_date = expiration_date . toGMTString ();

	// Build set-cookie string:
	var cookie_string = escape (name) + "=; expires=" + expiration_date;
	if (path != null)
		cookie_string += "; path=" + path;

	// Delete the cookie:
	document . cookie = cookie_string;
	}
}
function delete_all_cookies (path)
{
	// Get cookie string and separate into individual cookie phrases:
	var cookie_string = "" + document . cookie;
	var cookie_array = cookie_string . split ("; ");

	// Try to delete each cookie:
	for (var i = 0; i < cookie_array . length; ++ i)
	{
		var single_cookie = cookie_array [i] . split ("=");
		if (single_cookie . length != 2)
			continue;
		var name = unescape (single_cookie [0]);
		delete_cookie (name, path);
	}
}
try
{
//delete_all_cookies();
}
catch(ex)
{

}



			
try
{

	if(isSilverlightInstalled()==true) {
		SetCookie('BrowserCheckSilverlight', 'True', exp);
	}else{
		SetCookie('BrowserCheckSilverlight', 'False', exp);
	}
}
catch(ex)
{
	SetCookie('BrowserCheckSilverlight', 'False', exp);
}

var BrowserCheckJava = GetCookie('BrowserCheckJava');
var BrowserCheckJavaVersion = GetCookie('BrowserCheckJavaVersion');
var BrowserCheckJavaVendor = GetCookie('BrowserCheckJavaVendor');



function CheckCookieForRedirect(BrowserCheckJava,BrowserCheckJavaVersion,BrowserCheckJavaVendor)
{
	if ((BrowserCheckJava == "True") && (BrowserCheckJavaVersion != "") && (BrowserCheckJavaVendor != "")) {
		location.href="IDAM.aspx?version=" + BrowserCheckJavaVendor + "&vendor=" + BrowserCheckJavaVendor + "&r=<%=server.urlencode(request.querystring("r"))%>"
		return;
	}		
		
	if (navigator.javaEnabled()) { 
		SetCookie('BrowserCheckJava', 'True', exp);
		location.href="BrowserCheckJava.aspx?r=<%=server.urlencode(request.querystring("r"))%>" 
	} else {
		SetCookie('BrowserCheckJava', 'False', exp);
		location.href="IDAM.aspx?Java=False&r=<%=server.urlencode(request.querystring("r"))%>" 
	}
}



function isSilverlightInstalled()

{

    var isSilverlightInstalled = false;

    

    try

    {

        //check on IE

        try

        {

            var slControl = new ActiveXObject('AgControl.AgControl');

            isSilverlightInstalled = true;

        }

        catch (e)

        {

            //either not installed or not IE. Check Firefox

            if ( navigator.plugins["Silverlight Plug-In"] )

            {

                isSilverlightInstalled = true;

            }

        }

    }

    catch (e)

    {

        //we don't want to leak exceptions. However, you may want

        //to add exception tracking code here.

    }

    return isSilverlightInstalled;

}











window.setTimeout('go()',1500); 

function go(){
CheckCookieForRedirect(BrowserCheckJava,BrowserCheckJavaVersion,BrowserCheckJavaVendor);
}

			</script>
		</form>
	</body>
</HTML>
