<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.Explore" CodeBehind="Explore.ascx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<div class="ExploreDiv" >
</div>
<COMPONENTART:CALLBACK id="CallBackExplore" runat="server" CacheContent="True">
<CONTENT>
</CONTENT>
<ClientEvents>
<CallbackError EventHandler="GenericCallback_onCallbackError" />
</ClientEvents>
</COMPONENTART:CALLBACK>
<ComponentArt:TreeView id="TreeView1" ExpandCollapseInFront="true" OnContextMenu="treeContextMenu" Height="400px"
Width="100%" AutoCallBackOnNodeMove="False" DragAndDropEnabled="true" NodeEditingEnabled="false" KeyboardEnabled="false"
ExpandCollapseImageWidth="16" ExpandCollapseImageHeight="16" NodeIndent="15" ExpandSinglePath="True"
ItemSpacing="0" ExpandImageUrl="images/colarrow.gif" CollapseImageUrl="images/exparrow.gif" CssClass="TreeViewFullLine"
NodeCssClass="TreeNodeFullLine" NodeRowCssClass="TreeNodeRowFullLine" SelectedNodeCssClass="SelectedTreeNodeRowFullLine"
SelectedNodeRowCssClass="SelectedTreeNodeRowFullLine" HoverNodeCssClass="HoverTreeNodeRowFullLine"
HoverNodeRowCssClass="HoverTreeNodeRowFullLine" LineImageWidth="19" LineImageHeight="20" DefaultImageWidth="16"
DefaultImageHeight="16" NodeLabelPadding="3" ParentNodeImageUrl="images/folders.gif" LeafNodeImageUrl="images/folder.gif"
LineImagesFolderUrl="images/lines/" EnableViewState="false" runat="server" ClientSideOnNodeMove="nodeMove"
ExtendNodeCells="false" LoadingFeedbackCssClass="TreeNodeFullLine" ClientSideOnNodeExpand="nodeExpand">
</ComponentArt:TreeView>
<script language="javascript">
if (<% Response.Write(TreeView1.ClientID) %>.SelectedNode == null) {
var treeviewselectednodeindex = 0;
}else{
var treeviewselectednodeindex = <% Response.Write(TreeView1.ClientID) %>.SelectedNode.StorageIndex;
}
//set scroll appropriately
var objDiv = document.getElementById('<% Response.Write(TreeView1.ClientID) %>');
function settreeviewscroll()
{
//if (TreeViewExplore_Explore_TreeView1_loaded)
//{
if (treeviewselectednodeindex>20)
{
objDiv.scrollTop = 0;
objDiv.scrollTop = treeviewselectednodeindex*16.8;
}
//}
}
function setTreeViewHeight()
{
if(document.getElementById('<%response.write(TreeView1.ClientID)%>').style.height=='100%')
{document.getElementById('<%response.write(TreeView1.ClientID)%>').style.height='400px';
scroll(0,0);
SetCookie ('IDAMTreeViewHeight', 'max', exp);
}
else
{document.getElementById('<%response.write(TreeView1.ClientID)%>').style.height='100%';
SetCookie ('IDAMTreeViewHeight', 'min', exp);}
<%response.write(TreeView1.ClientID)%>.render(); 
}
</script>
<div style="PADDING-RIGHT:4px;HEIGHT:18px;TEXT-ALIGN:right"><a href="javascript:setTreeViewHeight();<%response.write(TreeView1.ClientID)%>.set_expandSinglePath(false);">[+/-]</a></div>
