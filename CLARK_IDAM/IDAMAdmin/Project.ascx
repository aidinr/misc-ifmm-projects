<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.Project" CodeBehind="Project.ascx.vb" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<div class="previewpaneMyProjects ui-helper-reset ui-widget-content" id="toolbarheading" style="z-index:99;border-top:0px;border-bottom:0px;">
	<!--Project cookie and top menu-->
    
		<table cellspacing="0" cellpadding="0" id="table2" width="100%" >
			<tr ><td valign="top" width="67" style="padding:10px;">
			<img style="background-color:#CBCBCB;color:#CBCBCB;border:2px solid #CBCBCB;" src="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?id=<%response.write(spProjectID)%>&instance=<%response.write(IDAMInstance)%>&type=project&crop=1&size=2&height=48&width=100&cache=1">
			</td>
			<td valign="top" style="padding-top:10px;padding-bottom:10px;">
			<font face="Verdana" size="3px" ><b><%response.write(spProjectName)%>&nbsp;</b><%if trim(spProjectClientName) <> "" then 
			response.write("for") 
			end if%><b>&nbsp;<%if trim(spProjectClientName) <> "" then 
			response.write(spProjectClientName) 
			end if%></b></font><br>
				<%if trim(spProjectDescription) <> "" then%><div style="font-weight: 400; max-height:50px; overflow: auto;"><%Response.Write(spProjectDescription.Replace(vbCrLf, "<br>"))%><br></div><%end if%>
				<b>Security Level:</b> <span style="font-weight: 400"><%response.write(spProjectSecurityLevel)%></span><br>
				<div><asp:Literal id="LiteralCookie" Text="N/A" visible="true" runat="server" /></div>
				</font>
			</td>
			<td id="Test" valign="top" align="right">
			<div style="padding-top:2px;">
			<!--[ <a href="#">help</a> ]-->
				</div>
			</td></tr>
			<tr><td valign="top" colspan="3">
			</td>
			</tr>
		</table>
    
</div> <!--end preview pane-->
<div id="subtabsetbottomlinefix" style="border-top:1px solid silver;z-index:2;position:relative;top:-1px;"></div>



<div class="previewpaneSubResults" style="z-index:1">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle" width="100%" style="HEIGHT: 20px"></td>
			<td align="left"  width="200" style="HEIGHT: 16px" nowrap >

			</td>
		</tr>
	</table>
</div>


<div id="subtabset" style="position:relative;top:-24px;z-index:2; font-size:10px;">
<input type="radio" id="radio1" name="subtab" onclick="gotopagelink('ProjectGeneral');"/><label for="radio1">General</label><img src="images/spacer.gif" width="2" alt="" /> 
<%if WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("PROJECT_GENERAL_VIEW","project",spProjectID) then%><input type="radio" id="radio2" name="subtab"  onclick="gotopagelink('ProjectDetails');"/><label for="radio2">Project Information</label><img src="images/spacer.gif" width="2" alt="" /> <%End If%>
<%if WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("PROJECT_PERMISSIONS_VIEW","project",spProjectID) then%><input type="radio" id="radio3" name="subtab"  onclick="gotopagelink('ProjectPermissions');"/><label for="radio3">Permissions</label><img src="images/spacer.gif" width="2" alt="" /> <%End If%>
<%if WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("IDAM_PROPERTYMODULE","project",spProjectID) then%><input type="radio" id="radio4" name="subtab"  onclick="gotopagelink('ProjectProperty');"/><label for="radio4">Property Data</label><img src="images/spacer.gif" width="2" alt="" /> <%End If%>
</div>





<div class="previewpaneSubResults" style="z-index:999;position:relative;top:-42px;margin-left:500px;">
<table  style="WIDTH:100%;" cellpadding="0" cellspacing="0" >
	<tr>
		<td align="right" style="FONT-SIZE: 10px; FONT-FAMILY: Verdana"><div style="padding-right:10px;">[ <a href="javascript:closealltags();closepinfo();">close 
				all</a> ] [ <a href="javascript:openalltags();openpinfo();">open all</a> ]</div></td>
	</tr>
</table>
</div>
<script>
    $(function () {
        $("#subtab").button();
        $("#subtabset").buttonset();
    });
	</script>

    <div class="previewpaneProjects" id="toolbar" style="position:relative;top:-29px;">

	<!--END Project cookie and top menu-->
	<!--projectcentermain-->
	<div id="projectcentermain"  class="projectcentermainpdxx" >
		<ComponentArt:Snap  ClientSideCookieName="PROJECTPAGESNAPProjectInfoMainx" id="SNAPProjectInfoMainx" runat="server" FillWidth="True" FillHeight="True" Height="100%" Visible=false
			MustBeDocked="true" DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy" CurrentDockingContainer="projectcentermain"
			DockingContainers="projectcentermain" 
			MinimizeDirectionElement="Test" MinimizeDuration="300" MinimizeSlide="Linear" ClientSideCookieEnabled="True">
			<Header>
				<div style="CURSOR: move; width: 100%;">
					<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
							<td align=left onmousedown="<% Response.Write(SNAPProjectInfoMainx.ClientID) %>.StartDragging(event);"><b>Project 
								Summary</b>
								<%'response.write(spProjectName)%>
								</td>
							<td width="15" style="cursor: hand" align="right"></td>
							<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfoMainx.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(0);" src="images/closebutton.gif" border="0"></td>
							<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfoMainx.ClientID)%>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
						</tr>
					</table>
				</div>
			</Header>
			<CollapsedHeader>
				<div style="CURSOR: move; width: 100%;">
					<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
							<td align=left onmousedown="<% Response.Write(SNAPProjectInfoMainx.ClientID) %>.StartDragging(event);"><b>Project 
								Summary</b>
								<%'response.write(spProjectName)%>
								</td>
							<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfoMainx.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(0);" src="images/closebutton.gif" border="0"></td>
							<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfoMainx.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
															border="0"></td>
						</tr>
					</table>
				</div>
				<img src="images/spacer.gif" width="188" height="0"><br>
			</CollapsedHeader>
			<Content>
				
                        

        <div class="SnapProjectsWrapper">
					<div class="SnapProjects">
					<!--<asp:PlaceHolder id="PageLoaderProjectSummary" runat="server"></asp:PlaceHolder>-->
					</div>
				</div> <!--SnapProjectsWrapper-->


			</Content>
		</ComponentArt:Snap>


       

                  
<script>
    $(function () {
        $("#JQPSx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQPSx', $("#JQPSx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
     if ($.cookie('JQPSx') == "false") {
            $("#JQPSx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQPSx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Project Summary</a></h3> 
        
        <div class="SnapProjectsWrapperxx">
					<div class="SnapProjectsxx">
                   <table border="0" width="100%" id="">
	<tr>
		<td width="1" align="left" valign="top">
			<asp:Literal ID="LiteralProjectSummaryImage" Runat="server"></asp:Literal></div>
		</td>
		<td align="left" valign="top">
			<div style="padding-left:10px;width:95%;">
				<asp:Literal ID="LiteralProjectSummary" Runat="server"></asp:Literal></div>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="left" valign="top"></td>
	</tr>
</table>
<img src="images/spacer.gif" width="188" height="1">
					</div>
				</div> <!--SnapProjectsWrapper-->
 </div>













	</div>
	<!--END projectcentermain-->
	<div  id=projectcentermainsubpages class="projectcentermainsubpagesxxx" style="border:1px solid white;"><asp:PlaceHolder id="PageLoaderProject" runat="server"></asp:PlaceHolder>
	</div>
</div> <!--end preview pane-->


<script language="javascript">
  function closepinfo()
{
 $("#JQPSx").accordion("option", "active", false);
}
function openpinfo()
{
    $("#JQPSx").accordion("option", "active", 0);
}   
<%
Select Case Request.QueryString("subtype")
    Case "ProjectGeneral"
        %>document.getElementById('radio1').checked = true;<%
    Case "ProjectPostings"
        %>//document.getElementById('link_Edit_div').className = "projecttabshighlight";<%
    Case "ProjectDetails"
        %>document.getElementById('radio2').checked = true;<%
    Case "ProjectPermissions"
        %>document.getElementById('radio3').checked = true;<%
    Case "ProjectProperty"
        %>document.getElementById('radio4').checked = true;<%        
    Case Else
        %>document.getElementById('radio1').checked = true;<%
End Select
%>
</script>
