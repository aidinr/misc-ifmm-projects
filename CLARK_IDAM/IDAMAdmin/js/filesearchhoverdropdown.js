/*
Simple Image Trail script- By JavaScriptKit.com
Visit http://www.javascriptkit.com for this script and more
This notice must stay intact
*/

//var offsetfrommouse=[xoffsetpopup,yoffsetpopup]; //image x,y offsets from cursor position in pixels. Enter 0,0 for no offset
//var offsetfrommouse=[-150,-500]; //image x,y offsets from cursor position in pixels. Enter 0,0 for no offset
//var displayduration=0; //duration in seconds image should remain visible. 0 for always.
//var currentimageheight = 400;	// maximum image size.



function gettrailobjdd(){
if (document.getElementById)
return document.getElementById("projectlookpupdropdown").style
else if (document.all)
return document.all.projectlookpupdropdown.style
}

function gettrailobjnostyledd(){
if (document.getElementById)
return document.getElementById("projectlookpupdropdown")
else if (document.all)
return document.all.projectlookpupdropdown
}


function truebodydd(){
    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    var is_safari = navigator.userAgent.toLowerCase().indexOf('safari') > -1;
    var is_opera = navigator.userAgent.toLowerCase().indexOf('opera') > -1; 
    if (is_chrome || is_safari || is_opera) {
        return document.body;
    }
    else {
        return document.documentElement;
    }
}

function showtraildd(){
	followmousedd(document.getElementById("projectlookpupdropdown"));
	gettrailobjdd().display="inline";
}


function hidetraildd(){
	//gettrailobj().innerHTML = " ";
	gettrailobjdd().display="none"
	//document.onmousemove=""
	gettrailobjdd().left="-1000px"

}

function followmousedd(e){

	var xcoord=offsetfrommouse[0]
	var ycoord=offsetfrommouse[1]

	var docwidth=document.all? truebodydd().scrollLeft+truebodydd().clientWidth : pageXOffset+window.innerWidth-15
	var docheight=document.all? Math.min(truebodydd().scrollHeight, truebodydd().clientHeight) : Math.min(window.innerHeight)

	//if (document.all){
	//	gettrailobjnostyle().innerHTML = 'A = ' + truebodydd().scrollHeight + '<br>B = ' + truebodydd().clientHeight;
	//} else {
	//	gettrailobjnostyle().innerHTML = 'C = ' + document.body.offsetHeight + '<br>D = ' + window.innerHeight;
	//}

	if (typeof e != "undefined"){
		if (docwidth - e.pageX < 380){
			xcoord = e.pageX - xcoord - 1000; // Move to the left side of the cursor
		} else {
			xcoord += e.pageX;
		}
		if (docheight - e.pageY < (currentimageheight + 260)){
			ycoord += e.pageY - Math.max(0,(260 + currentimageheight + e.pageY - docheight - truebodydd().scrollTop));
		} else {
			ycoord += e.pageY;
		}

	} else if (typeof window.event != "undefined"){
		if (docwidth - event.clientX < 380){
			xcoord = event.clientX + truebodydd().scrollLeft - xcoord - 1000; // Move to the left side of the cursor
		} else {
			xcoord += truebodydd().scrollLeft+event.clientX
		}
		if (docheight - event.clientY < (currentimageheight + 260)){
			ycoord += event.clientY + truebodydd().scrollTop - Math.max(0,(260 + currentimageheight + event.clientY - docheight));
		} else {
			ycoord += truebodydd().scrollTop + event.clientY;
		}
	}

	var docwidth=document.all? truebodydd().scrollLeft+truebodydd().clientWidth : pageXOffset+window.innerWidth-15
	var docheight=document.all? Math.max(truebodydd().scrollHeight, truebodydd().clientHeight) : Math.max(document.body.offsetHeight, window.innerHeight)
		if(ycoord < 0) { ycoord = ycoord*-1; }
	gettrailobjdd().left=xcoord+"px"
	gettrailobjdd().top=ycoord+"px"

}

