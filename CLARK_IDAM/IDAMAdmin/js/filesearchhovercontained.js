/*
Simple Image Trail script- By JavaScriptKit.com
Visit http://www.javascriptkit.com for this script and more
This notice must stay intact
*/

var offsetfrommouse=[xoffsetpopup,yoffsetpopup]; //image x,y offsets from cursor position in pixels. Enter 0,0 for no offset
//var offsetfrommouse=[-150,-500]; //image x,y offsets from cursor position in pixels. Enter 0,0 for no offset
var displayduration=0; //duration in seconds image should remain visible. 0 for always.
var currentimageheight = 400;	// maximum image size.


function gettrailobj(){
if (document.getElementById)
return document.getElementById("trailimageid").style
else if (document.all)
return document.all.trailimageid.style
}

function gettrailobjnostyle(){
if (document.getElementById)
return document.getElementById("trailimageid")
else if (document.all)
return document.all.trailimagid
}


function truebody() {
    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    var is_safari = navigator.userAgent.toLowerCase().indexOf('safari') > -1;
    var is_opera = navigator.userAgent.toLowerCase().indexOf('opera') > -1;
    if (is_chrome || is_safari || is_opera) {
        return document.body;
    }
    else {
        return document.documentElement;
    }
}





function showtrail(imagename,title,description,ratingaverage,ratingnumber,showthumb,height,filetype){

	if (height > 0){
		currentimageheight = height;
	}

	document.onmousemove=followmouse;

	cameraHTML = '';

	if ( !ratingnumber ){
		ratingnumber = 0;
		ratingaverage = 0;
	}

	for(x = 1; x <= 5; x++){

		if (ratingaverage >= 1){
			cameraHTML = cameraHTML + '<img src="/images/camera_1.gif">';
		} else if (ratingaverage >= 0.5){
			cameraHTML = cameraHTML + '<img src="/images/camera_05.gif">';
		} else {
			cameraHTML = cameraHTML + '<img src="/images/camera_0.gif">';
		}
	
		ratingaverage = ratingaverage - 1;
	}

	cameraHTML = cameraHTML + ' (' + ratingnumber + ' Review';
	if ( ratingnumber > 1 ) cameraHTML += 's';
	cameraHTML = cameraHTML + ')';

	newHTML = '<div style="padding: 5px; background-color: #FFF; border: 1px solid #888;">';
	newHTML = newHTML + '<h1>' + title + '</h1>';
	//newHTML = newHTML + 'Rating: ' + cameraHTML + '<br/>';
	newHTML = newHTML + description + '<br/>';

	if (showthumb > 0){
		newHTML = newHTML + '<div align="center" style="padding: 8px 2px 2px 2px;">';
		if(filetype == 8) { // Video
			newHTML = newHTML +	'<object width="380" height="285" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0">';
			newHTML = newHTML + '<param name="movie" value="video_loupe.swf">';
			newHTML = newHTML + '<param name="quality" value="best">';
			newHTML = newHTML + '<param name="loop" value="true">';

			newHTML = newHTML + '<param name="FlashVars" value="videoLocation=' + imagename + '">';
			newHTML = newHTML + '<EMBED SRC="video_loupe.swf" LOOP="true" QUALITY="best" FlashVars="videoLocation=' + imagename + '" WIDTH="380" HEIGHT="285">';
			newHTML = newHTML + '</object></div>';
		} else {
			newHTML = newHTML + '<img src="' + imagename + '" border="0"></div>';
		}
	}

	newHTML = newHTML + '</div>';
	gettrailobjnostyle().innerHTML = newHTML;
	gettrailobj().display="inline";
}


function hidetrail(){
	gettrailobj().innerHTML = " ";
	gettrailobj().display="none"
	document.onmousemove=""
	gettrailobj().left="-1000px"

}

function getCookieValcc (offset) {  
var endstr = document.cookie.indexOf (";", offset);  
if (endstr == -1)    
endstr = document.cookie.length;  
return unescape(document.cookie.substring(offset, endstr));
}

function GetCookiecc (name) {  
var arg = name + "=";  
var alen = arg.length;  
var clen = document.cookie.length;  
var i = 0;  
while (i < clen) {    
var j = i + alen;    
if (document.cookie.substring(i, j) == arg)      
return getCookieValcc (j);    
i = document.cookie.indexOf(" ", i) + 1;    
if (i == 0) break;   
}  
return null;
}

function followmouse(e){

	var xcoord=offsetfrommouse[0]
	var ycoord=offsetfrommouse[1]
	var xsplitteradd=GetCookiecc('PaneSize');
	
	var docwidth=document.all? truebody().scrollLeft+truebody().clientWidth : pageXOffset+window.innerWidth-15-xsplitteradd
	var docheight=document.all? Math.min(truebody().scrollHeight, truebody().clientHeight) : Math.min(window.innerHeight)

	//if (document.all){
	//	gettrailobjnostyle().innerHTML = 'A = ' + truebody().scrollHeight + '<br>B = ' + truebody().clientHeight;
	//} else {
	//	gettrailobjnostyle().innerHTML = 'C = ' + document.body.offsetHeight + '<br>D = ' + window.innerHeight;
	//}
	if (typeof e != "undefined"){
		//alert(xsplitteradd+':'+docwidth+':'+e.pageX);
		if (docwidth - e.pageX < (100)){
			//xcoord = e.pageX - xcoord - 800 - parseInt(xsplitteradd); // + parseInt(xsplitteradd); // Move to the left side of the cursor
			//alert(xcoord);
			xcoord = e.pageX - parseInt(xsplitteradd) - 650 ; // Move to the left side of the cursor
			//alert(xcoord);
			
		} else {
			xcoord += e.pageX - parseInt(xsplitteradd) + 200;
		}
		if (docheight - e.pageY < (currentimageheight + 260)){
			ycoord += e.pageY - Math.max(0,(260 + currentimageheight + e.pageY - docheight - truebody().scrollTop));
		} else {
			ycoord += e.pageY;
		}

	} else if (typeof window.event != "undefined"){
		//alert(xsplitteradd+':'+docwidth+':'+event.clientX);
	//was 380
		if (docwidth - event.clientX < (260)){
			xcoord = (event.clientX + truebody().scrollLeft) - parseInt(xsplitteradd) - 650; // Move to the left side of the cursor
			//alert(xcoord+'3');
		} else {
			xcoord += truebody().scrollLeft+event.clientX-parseInt(xsplitteradd)+200;
		}
	//was 260
		if (docheight - event.clientY < (currentimageheight + 260)){
			ycoord += event.clientY + truebody().scrollTop - Math.max(0,(260 + currentimageheight + event.clientY - docheight));
		} else {
			ycoord += truebody().scrollTop + event.clientY;
		}
	}

	//var docwidth=document.all? truebody().scrollLeft+truebody().clientWidth : pageXOffset+window.innerWidth-15
	//var docheight=document.all? Math.max(truebody().scrollHeight, truebody().clientHeight) : Math.max(document.body.offsetHeight, window.innerHeight)
		if(ycoord < 0) { ycoord = ycoord*-1; }
	gettrailobj().left=xcoord+"px"
	gettrailobj().top=ycoord+"px"

}
