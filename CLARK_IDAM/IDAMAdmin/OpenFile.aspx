<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.OpenFile" CodeBehind="OpenFile.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>OpenFile</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head> 
  <body  onload="redirect()">
	<SCRIPT language="JavaScript"><!--
function redirect () { setTimeout("go_now()",20000); }


		 function go_now ()   { window.close(); } 
//--></SCRIPT> 
		<form id="Form1" method="post" runat="server">
			<TABLE width="100%">
				<TR>
					<TD></TD>
				</TR>
				<TR>
					<TD align="center"><font face="Verdana" size="1">
							<asp:Label id="lblFileName" runat="server">FileName</asp:Label>&nbsp;is 
							currently being opened directly from iDAM Live location.<br>Path: <b>[</b> <%=path.replace("\\","\")%> <b>]</b><br><br>
							<IMG src="images/loading.gif"><BR>
						</font>
					</TD>
				</TR>
			</TABLE><br><br><br><br><br><br><br><br><br>
			<script language="JavaScript">

    var _info = navigator.userAgent;
        var ie = (_info.indexOf("MSIE") > 0);
        var win = (_info.indexOf("Win") > 0);
        if(win)
        {
          if(ie)
            {

		    document.writeln('<object classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93"');
		    document.writeln('      width= "5" height= "5" id="rup"');
		    document.writeln('      codebase="http://java.sun.com/update/1.5.0/jinstall-1_5-windows-i586.cab#version=1,4,1">');
		    document.writeln('<param name="archive" value="https://download.webarchives.com/IdamLiveApplet.jar">');
		    document.writeln('<param name="code" value="com.webarchives.idam.applet.IdamLiveApplet">');
		    document.writeln('<param name="name" value="iDAM Live">');
           }
            else
            {
                document.writeln('<object type="application/x-java-applet;version=1.4.1"');
                document.writeln('width= "5" height= "5"  id="rup">');
                document.writeln('<param name="archive" value="https://download.webarchives.com/IdamLiveApplet.jar">');
                document.writeln('<param name="code" value="com.webarchives.idam.applet.IdamLiveApplet">');
                document.writeln('<param name="name" value="iDAM Live">');
                document.writeln('<param name="MAYSCRIPT" value="yes">');
            }
        }
        else
        {
            /* mac and linux */
            document.writeln('<applet ');
            document.writeln('              archive  = "https://download.webarchives.com/IdamLiveApplet.jar"');
            document.writeln('                      code     = "com.webarchives.idam.applet.IdamLiveApplet"');
            document.writeln('                      name     = "iDAM Live"');
            document.writeln('                      hspace   = "0"');
            document.writeln('                      vspace   = "0" VIEWASTEXT');
            document.writeln('                      width = "1"');
            document.writeln('                      height = "1"');
            document.writeln('                      align    = "middle" id="IdamLiveApplet">');
        }

/******    BEGIN APPLET CONFIGURATION PARAMETERS   ******/
	 
    document.writeln('<param name="path" value="<%=path.replace("'", "\'")%>">');
    document.writeln('<param name="mountname" value="<%=mountname%>">');
    document.writeln('<param name="platform" value="<%=platform%>">');
    document.writeln('<param name="networkPath" value="<%=networkPath%>">');
    document.writeln('<param name="isFolder" value="<%=isFolder%>">');
     
/******    END APPLET CONFIGURATION PARAMETERS     ******/
       if(win)
	   {		 
		  document.writeln('</object>');
	   }
	   else

	   {

		  document.writeln('</applet>');
	   }
    

    
    
    
			</script>
		</form>

  </body>
</html>
