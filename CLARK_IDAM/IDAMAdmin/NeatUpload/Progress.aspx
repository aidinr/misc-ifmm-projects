<%@ Register TagPrefix="Upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>
<%@ Page language="vb" enableSessionstate="True"  AutoEventWireup="false" Inherits="Brettle.Web.NeatUpload.ProgressPage"  %>
<HTML>
	<HEAD>
		<title>Upload Progress</title>
		<%--
NeatUpload - an HttpModule and User Controls for uploading large files
Copyright (C) 2005  Dean Brettle

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
--%>
		<link rel="stylesheet" type="text/css" title="default" href="default.css">
			<style type="text/css">
BODY { BORDER-RIGHT: 0px; PADDING-RIGHT: 0px; BORDER-TOP: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-LEFT: 0px; PADDING-TOP: 0px; BORDER-BOTTOM: 0px }
FORM { BORDER-RIGHT: 0px; PADDING-RIGHT: 0px; BORDER-TOP: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-LEFT: 0px; PADDING-TOP: 0px; BORDER-BOTTOM: 0px }
TABLE { BORDER-RIGHT: 0px; PADDING-RIGHT: 0px; BORDER-TOP: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-LEFT: 0px; PADDING-TOP: 0px; BORDER-BOTTOM: 0px }
TR { BORDER-RIGHT: 0px; PADDING-RIGHT: 0px; BORDER-TOP: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-LEFT: 0px; PADDING-TOP: 0px; BORDER-BOTTOM: 0px }
TD { BORDER-RIGHT: 0px; PADDING-RIGHT: 0px; BORDER-TOP: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-LEFT: 0px; PADDING-TOP: 0px; BORDER-BOTTOM: 0px }
#progressDisplayCenterer { VERTICAL-ALIGN: middle; WIDTH: 100%; HEIGHT: 100% }
#progressDisplay { VERTICAL-ALIGN: middle; WIDTH: 100% }
#barTd { WIDTH: 100% }
#statusDiv { BORDER-RIGHT: 1px solid; PADDING-RIGHT: 0px; BORDER-TOP: 1px solid; PADDING-LEFT: 0px; Z-INDEX: 1; PADDING-BOTTOM: 0px; BORDER-LEFT: 1px solid; WIDTH: 100%; PADDING-TOP: 0px; BORDER-BOTTOM: 1px solid; POSITION: relative; TEXT-ALIGN: center }
#barDiv { BORDER-RIGHT: 0px; PADDING-RIGHT: 0px; BORDER-TOP: 0px; PADDING-LEFT: 0px; Z-INDEX: -1; LEFT: 0pt; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-LEFT: 0px; WIDTH: 75%; PADDING-TOP: 0px; BORDER-BOTTOM: 0px; POSITION: absolute; TOP: 0pt; HEIGHT: 100% }
#barDetailsDiv { BORDER-RIGHT: 0px; PADDING-RIGHT: 0px; BORDER-TOP: 0px; PADDING-LEFT: 0px; Z-INDEX: -1; LEFT: 0pt; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-LEFT: 0px; WIDTH: 75%; PADDING-TOP: 0px; BORDER-BOTTOM: 0px; POSITION: absolute; TOP: 0pt; HEIGHT: 100% }
</style>
	</HEAD>
	<body>
		<form id="dummyForm" runat="server">
			<table id="progressDisplayCenterer">
				<tr>
					<td>
						<table id="progressDisplay" class="ProgressDisplay">
							<tr>
								<td>
									<span id="label" runat="server" class="Label">Uploading:</span>
								</td>
								<td id="barTd">
									<div id="statusDiv" runat="server" class="StatusMessage">&nbsp;
										<Upload:DetailsSpan id="normalInProgress" runat="server" WhenStatus="NormalInProgress" style="FONT-WEIGHT: normal; WHITE-SPACE: nowrap"><%# FormatCount(BytesRead) %>/<%# FormatCount(BytesTotal) %> 
<%# CountUnits %>(<%# String.Format("{0:0%}", FractionComplete) %>) 
            at <%# FormatRate(BytesPerSec) %>- <%# FormatTimeSpan(TimeRemaining) %> left
				</Upload:DetailsSpan>
										<Upload:DetailsSpan id="chunkedInProgress" runat="server" WhenStatus="ChunkedInProgress" style="FONT-WEIGHT: normal; WHITE-SPACE: nowrap"><%# FormatCount(BytesRead) %><%# CountUnits %>at 
<%# FormatRate(BytesPerSec) %>- 
            <%# FormatTimeSpan(TimeElapsed) %> elapsed
				</Upload:DetailsSpan>
										<Upload:DetailsSpan id="processing" runat="server" WhenStatus="ProcessingInProgress ProcessingCompleted"
											style="FONT-WEIGHT: normal; WHITE-SPACE: nowrap">
											<%# ProcessingHtml %>
										</Upload:DetailsSpan>
										<Upload:DetailsSpan id="completed" runat="server" WhenStatus="Completed">
					Completed: <%# FormatCount(BytesRead) %><%# CountUnits %> at <%# FormatRate(BytesPerSec) %> total time: <%# FormatTimeSpan(TimeElapsed) %>
				</Upload:DetailsSpan>
										<Upload:DetailsSpan id="cancelled" runat="server" WhenStatus="Cancelled">
					Cancelled!
				</Upload:DetailsSpan>
										
										<Upload:DetailsDiv id="barDetailsDiv" runat="server" UseHtml4="true"					 Width='<%# Unit.Percentage(Math.Floor(100*FractionComplete)) %>' class="ProgressBar">
										</Upload:DetailsDiv>
									</div>
								</td>
								<td>
									<asp:HyperLink id="cancel" runat="server" Visible='<%# CancelVisible %>' NavigateUrl='<%# CancelUrl %>' title="Cancel Upload" class="ImageButton" >
										<img id="cancelImage" src="cancel.png" alt="Cancel Upload" /></asp:HyperLink>
									<asp:HyperLink id="refresh" runat="server" Visible='<%# StartRefreshVisible %>' NavigateUrl='<%# StartRefreshUrl %>' title="Refresh" class="ImageButton" >
										<img id="refreshImage" src="refresh.png" alt="Refresh" /></asp:HyperLink>
									<asp:HyperLink id="stopRefresh" runat="server" Visible='<%# StopRefreshVisible %>' NavigateUrl='<%# StopRefreshUrl %>' title="Stop Refreshing" class="ImageButton">
										<img id="stopRefreshImage" src="stop_refresh.png" alt="Stop Refreshing" /></asp:HyperLink>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
