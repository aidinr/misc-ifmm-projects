<%@ Page language="vb" AutoEventWireup="false" Inherits="IdamAdmin.Error413" CodeBehind="Error413.aspx.vb" %>
<Html>
	<Head runat="server">
		<Title>Upload Too Large</Title>
	</Head>
	<Body>
		<h1>Upload Too Large</h1>
		<p>
		You are attempting an upload which is too large.  
		Please use your browser's Back button to go back and try a smaller upload.
		</p>
	</Body>
</Html>
