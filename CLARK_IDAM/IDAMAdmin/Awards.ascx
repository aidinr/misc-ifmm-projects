<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.Awards" CodeBehind="Awards.ascx.vb" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<div class="previewpaneMyProjects ui-accordion-header ui-helper-reset ui-state-active " id="toolbarheading" style="z-index:99;border-top:0px;border-bottom:0px;">
	<!--Project cookie and top menu-->
			<!--Project cookie and top menu-->
		<table cellspacing="0"  id="table2" width="100%">
			<tr>
			<td width="1" nowrap valign="top">
			<img src="images/ui/news_ico_bg.gif" height=42 width=42>
			
			
			</td>
			
			<td valign="top">
			<div style="PADDING-RIGHT:5px;PADDING-LEFT:5px;FLOAT:left;PADDING-BOTTOM:5px;PADDING-TOP:5px"><font face="Verdana" size="3" ><b>Awards and Publications</b></font><br>Modify/add awards and publications below.  Manage the viewing of the items by setting the post and pull dates appropriately.</div>
			</td>
			<td id="Test" valign="top" align="right">
			<font face="Verdana" size="1"><script>writeDate();</script></font>
			</td></tr>
			<tr><td valign="top" colspan="2">
			</td>
			</tr>
		</table>
</div> <!--end preview pane-->




<div id="subtabset" style="position:relative;top:-4px;z-index:2; font-size:10px;">
<input type="radio" id="radio1" name="subtab" checked="checked" /><label for="radio1">General</label><img src="images/spacer.gif" width="2" alt="" />
</div>
<div style="border-top:1px solid silver;position:relative;top:-25px;z-index:1;"></div>

<script>
    $(function () {
        $("#subtab").button();
        $("#subtabset").buttonset();
    });
	</script>









<div class="previewpaneProjects" id="toolbar"   style="padding:10px;">
	<div style="BORDER-RIGHT:0px solid; PADDING-RIGHT:5px; BORDER-TOP:0px solid; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:0px solid; PADDING-TOP:15px; BORDER-BOTTOM:0px solid" id="browsecenter">

		<div style="PADDING-RIGHT:10px;PADDING-BOTTOM:10px">
<b>Award Items</b>
</div>		


<div style="PADDING-TOP:5px"><div style="PADDING-RIGHT:3px;PADDING-LEFT:3px;PADDING-BOTTOM:3px;PADDING-TOP:3px"></div></div>
[ <a href="javascript:Object_PopUp('AwardsEdit.aspx?action=new','Edit_Awards',880,650);">create award item</a> ] 
<div style="PADDING-TOP:5px"><div style="PADDING-RIGHT:3px;PADDING-LEFT:3px;PADDING-BOTTOM:3px;PADDING-TOP:3px"></div></div>						
<asp:Literal id="LiteralNews" Text="No items available" visible=false runat="server" />
							
<script language=javascript type=text/javascript>

  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <%response.write(GridNews.ClientID)%>.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }


  function editGridNews(rowId)
  {
	Object_PopUpScrollbars('AwardsEdit.aspx?ID=' + rowId ,'Edit_Awards',920,650);
  }
  
  function editRow()
  {
    <%response.write(GridNews.ClientID)%>.EditComplete();     
  }

  function insertRow()
  {
    <%response.write(GridNews.ClientID)%>.EditComplete(); 
  }

  function deleteRowNews(rowId)
  {
  if (confirm("Delete record?"))
  {
        <%response.write(GridNews.ClientID)%>.Delete(<%response.write(GridNews.ClientID)%>.GetRowFromClientId(rowId)); 
  }
    
  }
  
  
  function getValue()
  {
    var selectedDate = <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_3_Picker1.GetSelectedDate();
    var formattedDate = <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_3_Picker1.FormatDate(selectedDate, 'MMM dd yyyy');
    return [selectedDate, formattedDate];
  }

  function setValue(DataItem)
  {
   var selectedDate = DataItem.GetMember('post_date_new').Object;
   <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_3_Picker1.SetSelectedDate(selectedDate);
  }

      function Picker1_OnDateChange()
      {
        var fromDate = <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_3_Picker1.GetSelectedDate();
        <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_3_Calendar1.SetSelectedDate(fromDate);
      }
      
      function Calendar1_OnChange()
      {
        var fromDate = <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_3_Calendar1.GetSelectedDate();
        <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_3_Picker1.SetSelectedDate(fromDate);
      }

      function Button_OnClick(button)
      {
        if (<%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_3_Calendar1.PopUpObjectShowing)
        {
          <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_3_Calendar1.Hide();
        }
        else
        {
          <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_3_Calendar1.SetSelectedDate(<%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_3_Picker1.GetSelectedDate());
          <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_3_Calendar1.Show(button);
        }
      }
      
      function Button_OnMouseUp()
      {
        if (<%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_3_Picker1.PopUpObjectShowing)
        {
          event.cancelBubble=true;
          event.returnValue=false;
          return false;
        }
        else
        {
          return true;
        }
      }
      
      
      
      function RefreshNewsItems()
      {

      <%response.write(GridNews.ClientID)%>.Filter("headline LIKE '%%'");
 
      }
      
      
      
      
  function getValue2()
  {
  
    var selectedDate = <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_4_Picker2.GetSelectedDate();
    var formattedDate = <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_4_Picker2.FormatDate(selectedDate, 'MMM dd yyyy');
    return [selectedDate, formattedDate];
  }

  function setValue2(DataItem)
  {
   var selectedDate = DataItem.GetMember('pull_date_new').Object;
   <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_4_Picker2.SetSelectedDate(selectedDate);
  }

      function Picker1_OnDateChange2()
      {
        var fromDate = <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_4_Picker2.GetSelectedDate();
        <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_4_Calendar2.SetSelectedDate(fromDate);
      }
      
      function Calendar1_OnChange2()
      {
        var fromDate = <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_4_Calendar2.GetSelectedDate();
        <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_4_Picker2.SetSelectedDate(fromDate);
      }

      function Button_OnClick2(button)
      {
        if (<%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_4_Calendar2.PopUpObjectShowing)
        {
          <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_4_Calendar2.Hide();
        }
        else
        {
          <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_4_Calendar2.SetSelectedDate(<%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_4_Picker2.GetSelectedDate());
          <%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_4_Calendar2.Show(button);
        }
      }
      
      function Button_OnMouseUp2()
      {
        if (<%=ctype(Page,IdamAdmin.IDAM).RootClientID%>_<%=GridNews.ClientID%>_EditTemplate_0_4_Calendar2.PopUpObjectShowing)
        {
          event.cancelBubble=true;
          event.returnValue=false;
          return false;
        }
        else
        {
          return true;
        }
      }



</script>	




		
<COMPONENTART:GRID 
id="GridNews" 
runat="server" 
AutoFocusSearchBox="false"
AutoCallBackOnInsert="true"
AutoCallBackOnUpdate="true"
AutoCallBackOnDelete="true"
EditOnClickSelectedItem="true"
pagerposition="2"
ScrollBar="Off"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ScrollPopupClientTemplateId="ScrollPopupTemplate" 
Sort="creation_date desc"
Height="10" Width="99%" 
LoadingPanelPosition="MiddleCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateNews" 
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRowCategory" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="true" 
GroupingPageSize="1" 
PagerTextCssClass="GridFooterTextCategory" 
PagerStyle="Numbered" 
PageSize="10" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeader" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="true" 
AllowSorting="False"
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
ScrollBarWidth="15" 
AllowPaging="true" 
EnableViewState="true"
>
<ClientTemplates>
<ComponentArt:ClientTemplate Id="TypeIconTemplateNews">
<img src="## DataItem.GetMember("imagelink").Value ##" border="0" >
            
          </ComponentArt:ClientTemplate>     
          <ComponentArt:ClientTemplate Id="EditTemplateNews">
          <a href="javascript:editGridNews('## DataItem.GetMember("awards_id").Text ##');"><img src="images/i_rate.gif" alt="Edit" border="0" > </a> | <a href="javascript:deleteRowNews('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
            
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateNews">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateNews">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>            
			<ComponentArt:ClientTemplate Id="NewsTemplate">
			<table width="100%" cellspacing="0" cellpadding="1" border="0">
			<tr>
				<td class="CellText" align="left"><a style="color:#595959;" href="#"><b><nobr>## DataItem.GetMember("Headline").Value ##</nobr></a></b></td>
			</tr>
			<tr>
				<td class="CellText" align="left"><font color="#595959">## DataItem.GetMember("content_new").Text ##</font></td>
			</tr>
			</table>
			</ComponentArt:ClientTemplate>    
					        
			<componentart:ClientTemplate ID="LoadingFeedbackTemplateNews">
			<table cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
			</tr>
			</table>
			</componentart:ClientTemplate>     
			                      
</ClientTemplates>

<ServerTemplates>
<ComponentArt:GridServerTemplate ID="yoyoyo">
            <Template>

 	   <table cellspacing="0" cellpadding="0" border="0">
	    <tr>
	      <td onmouseup="Button_OnMouseUp()">
<ComponentArt:Calendar id="Picker1" runat="server" PickerFormat="Custom" PickerCustomFormat="MMM dd yyyy" ControlType="Picker" SelectedDate="2005-12-24" ClientSideOnSelectionChanged="Picker1_OnDateChange" PickerCssClass="picker" /></td>
	      <td style="font-size:10px;">&nbsp;</td>
	      <td><img id="calendar_from_button" alt="" onclick="Button_OnClick(this)" onmouseup="Button_OnMouseUp()" class="calendar_button" src="images/btn_calendar.gif" /></td>
	    </tr>
	    </table>

		<ComponentArt:Calendar runat="server" id="Calendar1" AllowMultipleSelection="false" AllowWeekSelection="false" AllowMonthSelection="false" ControlType="Calendar" PopUp="Custom" PopUpExpandControlId="calendar_from_button" CalendarTitleCssClass="title" SelectedDate="2005-12-24" VisibleDate="2005-12-24" ClientSideOnSelectionChanged="Calendar1_OnChange" DayHeaderCssClass="dayheader" DayCssClass="day" DayHoverCssClass="dayhover" OtherMonthDayCssClass="othermonthday" SelectedDayCssClass="selectedday" CalendarCssClass="calendar" NextPrevCssClass="nextprev" MonthCssClass="month" SwapSlide="Linear" SwapDuration="300" DayNameFormat="FirstTwoLetters" PrevImageUrl="images/cal_prevMonth.gif" NextImageUrl="images/cal_nextMonth.gif" />
    		
    			

            </Template>
</ComponentArt:GridServerTemplate>


<ComponentArt:GridServerTemplate ID="yoyoyo2">
            <Template>

 	   <table cellspacing="0" cellpadding="0" border="0">
	    <tr>
	      <td onmouseup="Button_OnMouseUp2()">
<ComponentArt:Calendar id="Picker2" runat="server" PickerFormat="Custom" PickerCustomFormat="MMM dd yyyy" ControlType="Picker" SelectedDate="2005-12-24" ClientSideOnSelectionChanged="Picker1_OnDateChange2" PickerCssClass="picker" /></td>
	      <td style="font-size:10px;">&nbsp;</td>
	      <td><img id="calendar_from_button2" alt="" onclick="Button_OnClick2(this)" onmouseup="Button_OnMouseUp2()" class="calendar_button" src="images/btn_calendar.gif" /></td>
	    </tr>
	    </table>

		<ComponentArt:Calendar runat="server" id="Calendar2" AllowMultipleSelection="false" AllowWeekSelection="false" AllowMonthSelection="false" ControlType="Calendar" PopUp="Custom" PopUpExpandControlId="calendar_from_button2" CalendarTitleCssClass="title" SelectedDate="2005-12-24" VisibleDate="2005-12-24" ClientSideOnSelectionChanged="Calendar1_OnChange2" DayHeaderCssClass="dayheader" DayCssClass="day" DayHoverCssClass="dayhover" OtherMonthDayCssClass="othermonthday" SelectedDayCssClass="selectedday" CalendarCssClass="calendar" NextPrevCssClass="nextprev" MonthCssClass="month" SwapSlide="Linear" SwapDuration="300" DayNameFormat="FirstTwoLetters" PrevImageUrl="images/cal_prevMonth.gif" NextImageUrl="images/cal_nextMonth.gif" />

            </Template>
</ComponentArt:GridServerTemplate>
</ServerTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" ShowTableHeading="false" ShowSelectorCells="false" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateNews" InsertCommandClientTemplateId="InsertCommandTemplateNews" DataKeyField="awards_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="asc.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
           
<Columns>
<ComponentArt:GridColumn Align="center" AllowEditing="false" DataCellClientTemplateId="TypeIconTemplateNews" dataField="imagelink" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="50" FixedWidth="True" />
<componentart:GridColumn HeadingText="Type" DataCellCssClass="DataCell" SortedDataCellCssClass="SortedDataCell" DataField="type" ForeignTable="TypeTable" ForeignDataKeyField="Type_Id" ForeignDisplayField="Type_Value" Width="180" FixedWidth="True"></componentart:GridColumn>
<ComponentArt:GridColumn DataCellCssClass="DataCell" AllowSorting="True" HeadingText="Award Name" DataCellClientTemplateId="NewsTemplate" AllowEditing="false" />
<ComponentArt:GridColumn DataField="post_date_new" HeadingText="Award Date" SortedDataCellCssClass="SortedDataCell" FormatString="MMM dd yyyy" EditControlType="Custom" EditCellServerTemplateId="yoyoyo" CustomEditSetExpression="setValue(DataItem)" CustomEditGetExpression="getValue()" width="200" FixedWidth="True"/>
<ComponentArt:GridColumn DataField="pull_date_new" HeadingText="Pull" SortedDataCellCssClass="SortedDataCell" FormatString="MMM dd yyyy" EditControlType="Custom" EditCellServerTemplateId="yoyoyo2" CustomEditSetExpression="setValue2(DataItem)" CustomEditGetExpression="getValue2()" width="200" FixedWidth="True"/>
<ComponentArt:GridColumn DataField="creation_date_new" AllowEditing="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Created" FormatString="MMM dd yyyy" AllowGrouping="False" Width="80" />
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" AllowSorting="false" HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplateNews" EditControlType="EditCommand" Align="Center" Width="80" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="content_new" SortedDataCellCssClass="SortedDataCell" DataField="content_new"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="Headline" SortedDataCellCssClass="SortedDataCell" DataField="Headline"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="post_date_new" SortedDataCellCssClass="SortedDataCell" DataField="post_date_new"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="pull_date_new" SortedDataCellCssClass="SortedDataCell" DataField="pull_date_new"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="pdflink" SortedDataCellCssClass="SortedDataCell" DataField="pdflink"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="imagelink" SortedDataCellCssClass="SortedDataCell" DataField="imagelink"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="awards_id" SortedDataCellCssClass="SortedDataCell" DataField="awards_id"></componentart:GridColumn>

</Columns>
</componentart:GridLevel>
</Levels>


</COMPONENTART:GRID>

























										

		

      </div><!--END browsecenter-->
</div><!--END MAIN-->

<script language=javascript type=text/javascript>


function gotopagelink(subtype)
{
	var newurl = 'IDAM.aspx?Page=' + subtype;
	window.location.href = newurl;
	return true;
}
  

</script>

