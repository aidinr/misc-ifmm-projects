Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient





Partial Public Class SlideShowNew
    Inherits System.Web.UI.Page
    Public spimageSrcArray, spimageNameArray, spimageDescriptionArray, spimageUpdateDateArray, spimageHeightSrcArray, spimageWidthSrcArray, sprsAssetsTotal As String

    Public Shared ProjectImageLocation As String = ConfigurationSettings.AppSettings("IDAM.WSLocation")
    Public UseWSSearch As String = ConfigurationSettings.AppSettings("IDAM.UseWSSearch")
    Public Shared IDAMInstance As String = ConfigurationSettings.AppSettings("IDAM.Instance")
    Public Shared sAssetNumResults As String = "0"
    Public Shared sProjectNumResults As String = "0"
    Public WSLocationClientDownload As String = ConfigurationSettings.AppSettings("IDAM.WSLocationClientDownload")
    Public WSLocationClient As String = ConfigurationSettings.AppSettings("IDAM.WSLocationClient")
    Public MaxResults As String = ConfigurationSettings.AppSettings("IDAM.MaxResults")


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim iCarr, sqltemp As String
        iCarr = Request.QueryString("car")
        Dim rsAssets As DataSet = returnAssetDataSource(False, "")
        Dim assetlist As String = ""
        Dim i As Integer = 1
        Try

        
        For Each item As DataRow In rsAssets.Tables(0).Rows
            assetlist += "{image: '" & WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase & "?id=" & item("asset_id") & "&instance=" & IDAMInstance & "&type=asset&size=1&crop=1&height=1000&width=1300', thumb: '" & WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase & "?id=" & item("asset_id") & "&instance=" & IDAMInstance & "&type=asset&size=1&crop=1&height=50&width=50', title:'" & item("media_type_name").replace("'", "") & "', description: '" & item("name").replace("'", "") & "', link: 'javascript:gotoAsset(" & item("asset_id") & ")'}"
            If i < rsAssets.Tables(0).Rows.Count Then
                assetlist += ","
            End If
            i = i + 1


        Next
        ltrslideshowimages.text = assetlist
        Catch ex As Exception

        End Try
    End Sub

    Public Function addOrdering(ByVal sql As String, ByVal ids As String, ByVal field As String) As String
        Dim sqlordering As String = ""
        Dim ixx As Integer = 1
        For Each id As String In ids.Split(",")

            sqlordering = sqlordering + "WHEN " & field & " = " & id & " then " & ixx & " "
            ixx = ixx + 1
        Next
        sql = sql & " ORDER BY CASE " & sqlordering & " END"
        Return sql
    End Function


    Public Function returnAssetDataSource(ByVal isThumb As Boolean, Optional ByVal SearchFilter As String = "") As DataSet

        Dim sqluser, sqlbase, sSortOrder, sOrderBy, sIllustTypeFilter, sMediaTypeFilter, sServicesFilter, sfiletypefilter, scategoryfilter, sprojectfilter, sadtanddate, sqltemp, sSearchType, sCategory, sProject, sA_mediatype, sadtcreatemodify, sadt, sadtmonth, sadtdays, sadtbetweendate As String
        Dim sKeyword As String
        Dim blnRating As Boolean
        Dim strkeywordservicesArray, strkeywordmediatypeArray, strkeywordillusttypeArray As String
        Dim callbackCheck, assetFilterCallBackCheck As String
        Dim KeywordSearchType, KeywordMediaTypeType As String
        Dim KeywordIllustTypeSearchType, KeywordIllustTypeType As String
        Dim asizeleastmost As String

        If Trim(SearchFilter) <> "" Then
            sKeyword += " " + SearchFilter
        End If


        Dim KeywordServicesSearchType, KeywordMediaTypeSearchType As String

        Try
            callbackCheck = Request.Form("Cart__ctl0_CallbackProjectAssetOverlay_Callback_Param").ToString()
        Catch ex As Exception
        End Try
        Try
            assetFilterCallBackCheck = Request.Form("Cart__ctl0_GridAssets_Callback_Level").ToString()
        Catch ex As Exception
        End Try
        Try
            callbackCheck = Request.Form("Cart__ctl0_GridProjects_Callback_Level").ToString()
        Catch ex As Exception
        End Try


        ''need to fix this...session based sql for initial form submittal...everything then works off that as applied filters
        ''accomodates text filter as well as menu filters



        If callbackCheck Is Nothing Then
            'override using assetids
            'no check for securty...assume this was covered in the source page
            Dim sql As String

            Try

                Dim i As Integer = 1
                
                If Not Request.QueryString("usesessionsql") Is Nothing Then
                    Select Case Request.QueryString("usesessionsql")
                        Case "usesessionsqlcarousel"
                            sql = Session("SQL_Original_Carousel")
                        Case "usesessionsqlprojectasset", "usesessionsqlresult"
                            'sql = Session("SQL_Original")
                            sql = "select top 250 a.asset_id,replace(replace(replace(a.description,'""',''),')',''),'(','') description,a.category_id,a.location_id,a.filesize,a.available,a.projectid,a.name,a.update_date,c.name media_type_name,a.update_date available_date,'0' carrousel_id from ipm_asset a, ipm_filetype_lookup c, ipm_user d where a.userid = d.userid and a.media_type = c.media_type  and a.asset_id in (select asset_id from (" & Session("SQL_Original") & ") a) order by " & IDAMWebSession.Session("sPGOrderBy").ToLower & " " & IDAMWebSession.Session("sPGSort").ToLower
                    End Select
                Else



                    If Request.QueryString("type") <> "carousel" Then
                        sql = "select a.asset_id,replace(replace(replace(a.description,'""',''),')',''),'(','') description,a.category_id,a.location_id,a.filesize,a.available,a.projectid,a.name,a.update_date,c.name media_type_name,a.update_date available_date,'0' carrousel_id from ipm_asset a, ipm_filetype_lookup c, ipm_user d where a.userid = d.userid and a.media_type = c.media_type  and a.asset_id in (" & Session("IDAMCurrentSlideshowIds") & ")"
                        sql = addOrdering(sql, Session("IDAMCurrentSlideshowIds"), "asset_id")
                    Else

                        Dim theIDs As String
                        If Session("manuallogin") Is Nothing Then
                            Dim carouselitems As New Hashtable
                            If Not Session("carouselitems") Is Nothing Then
                                carouselitems = Session("carouselitems")
                                theIDs = ""
                                For Each key In carouselitems.Keys
                                    theIDs += key & ","
                                Next
                            End If




                            'if theIDs is nothing then assume a simple slideshow from selected list
                            If theIDs Is Nothing Then
                                If Request.QueryString("assetids") <> "all" Then

                                    'convert ids to assets with paging.
                                    sql = "select a.asset_id,e.page,e.carouselitem_id,replace(replace(replace(a.description,'""',''),')',''),'(','') description,a.category_id,a.location_id,a.filesize,a.available,a.projectid,a.name,a.update_date,c.name media_type_name,a.update_date available_date,'0' carrousel_id from ipm_asset a, ipm_filetype_lookup c, ipm_user d,ipm_carrousel_item e where a.userid = d.userid and a.media_type = c.media_type  and a.asset_id = e.asset_id and e.carouselItem_id in (" & Session("IDAMCurrentSlideshowIds") & ")"
                                    sql = addOrdering(sql, Session("IDAMCurrentSlideshowIds"), "carouselitem_id")
                                Else
                                    sql = "select a.asset_id,replace(replace(replace(a.description,'""',''),')',''),'(','') description,a.category_id,a.location_id,a.filesize,a.available,a.projectid,a.name,a.update_date,c.name media_type_name,a.update_date available_date,'0' carrousel_id from ipm_asset a, ipm_carrousel_item b, ipm_filetype_lookup c, ipm_user d where a.userid = d.userid and a.media_type = c.media_type and a.asset_id = b.asset_id and b.carrousel_id = " & Session("defaultcarousel").ToString
                                    'sql = addOrdering(sql, Session("IDAMCurrentSlideshowIds"), "carouselitem_id")
                                End If
                            Else
                                'theIDs = theIDs.Substring(0, theIDs.ToString.Length - 1)
                                sql = "select a.asset_id,e.page,replace(replace(replace(a.description,'""',''),')',''),'(','') description,a.category_id,a.location_id,a.filesize,a.available,a.projectid,a.name,a.update_date,c.name media_type_name,a.update_date available_date,'0' carrousel_id from ipm_asset a, ipm_filetype_lookup c, ipm_user d,ipm_carrousel_item e where a.userid = d.userid and a.media_type = c.media_type  and a.asset_id = e.asset_id and e.carouselItem_id in (" & Session("IDAMCurrentSlideshowIds") & ")"

                            End If
                        Else
                            sql = "select a.asset_id,replace(replace(replace(a.description,'""',''),')',''),'(','') description,a.category_id,a.location_id,a.filesize,a.available,a.projectid,a.name,a.update_date,c.name media_type_name,a.update_date available_date,'0' carrousel_id from ipm_asset a, ipm_carrousel_item b, ipm_filetype_lookup c, ipm_user d where a.userid = d.userid and a.media_type = c.media_type and a.asset_id = b.asset_id and b.carrousel_id = " & Session("defaultcarousel").ToString
                        End If

                    End If




                End If





                Dim ConnectionString As String = WebArchives.iDAM.Web.Core.IDAMWebSession.Config.ConnectionString
                Dim connectionMain As OleDbConnection = New OleDbConnection(ConnectionString)
                Dim adapterSrc As New OleDbDataAdapter
                adapterSrc.SelectCommand = New OleDbCommand(sql, connectionMain)
                Dim oTable1 As New DataTable("Main")
                'Dim oTable2 As New DataTable("SecurityLevel")
                'Dim oTable3 As New DataTable("Users")

                'Dim oTable5 As New DataTable("Projects")
                connectionMain.Open()
                Try
                    adapterSrc.Fill(oTable1)
                   


                Finally
                    connectionMain.Close()
                End Try

                Dim dsSrc As New DataSet
                dsSrc.Tables.Add(oTable1)
                

                Return dsSrc

            Catch ex As Exception

            End Try

        End If
    End Function



End Class


