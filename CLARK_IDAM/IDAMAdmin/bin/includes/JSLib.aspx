<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.JSLib" CodeBehind="JSLib.aspx.vb" %>

function detectIE() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (userAgent.indexOf('msie')!=-1) {
  return true;
  }
}

function xtrim(str) {
        return str.replace(/^\s+|\s+$/g,"");
    }

function ClearFilters()
{
SetCookie("tFilter", "");
SetCookie("iFilter", "");
SetCookie("cFilter", "");
SetCookie("mFilter", "");
SetCookie("pFilter", "");
SetCookie("sFilter", "");
SetCookie("tagFilter", "");
SetCookie("carFilter", "");
var new_date = new Date()
new_date = new_date.toGMTString()
var thecookie = document.cookie.split(";")
for (var i = 0;i < thecookie.length;i++) 
{   
var tmp = xtrim(thecookie[i]).substring(0,3);
    if (tmp == 'dyn') {
            var cname = xtrim(thecookie[i]);
            if (cname.split("=").length > 0) {
                cname = cname.split("=")[0];
            }
            document.cookie = cname + "; expires ="+ new_date
            SetCookie(cname, "");
            }
   }
}


function switchToThumbnail() {
if (getLastView() == 'thumb') {
//alert(getLastView() + 'settinglist');
setLastView ('list');
} else {
//alert(getLastView() + 'settingthumb');
setLastView ('thumb');
}
//alert('set' + getLastView());

window.location.href = '<%response.write (Request.Url.PathAndQuery.Replace("'",""))%>';
//thumbnailview.style.display = "block";
//listview.style.display = "none";
}
function switchToThumbnailSubmit() {
if (getLastView() == 'thumb') {
//alert(getLastView() + 'settinglist');
setLastView ('list');
} else {
//alert(getLastView() + 'settingthumb');
setLastView ('thumb');
}
//alert('set' + getLastView());

__doPostBack('__Page', '');
//thumbnailview.style.display = "block";
//listview.style.display = "none";
}


function getIDAMGridSizeFormat(filesize)
{	

return size_format(filesize);
}


function size_format (filesize) {	if (filesize >= 1073741824) {	     filesize = number_format(filesize / 1073741824, 2, '.', '') + ' Gb';	} else { 		if (filesize >= 1048576) {     		filesize = number_format(filesize / 1048576, 2, '.', '') + ' Mb';   	} else { 			if (filesize >= 1024) {    		filesize = number_format(filesize / 1024, 0) + ' Kb';  		} else {    		filesize = number_format(filesize, 0) + ' bytes';			}; 		};	};  return filesize;};
function number_format( number, decimals, dec_point, thousands_sep ) 
{   
var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;    
var d = dec_point == undefined ? "," : dec_point;    
var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";    
var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;     
return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function getRatingGraphicNum(rating)
{
if (rating>=500){return 5;}
else if (rating>=250&&rating<=499){return 4;}
else if (rating>=100&&rating<=249){return 3;}
else if (rating>=50&&rating<=99){return 2;}
else if (rating>=0&&rating<=49){return 1;}
}
function getALink()
{
return 'IDAM.aspx?page=Asset&type=asset&c=<%response.write (Request.QueryString("c"))%>&Id=';
}

// Forces the treeview to adjust to the new size of its container          
function resizeTree(DomElementId, NewPaneHeight, NewPaneWidth)
{
setPaneSize(NewPaneWidth);
}

function onquicksearchkeydown(val,eventObj)
{
if (eventObj.keyCode==13){
document.PageForm.idamaction.value='true';
window.location = 'IDAM.aspx?page=Results&Type=QuickSearch&keyword=' + document.PageForm.keyword.value;
return false;
}
}
// Forces the grid to adjust to the new size of its container          
function resizeGrid(DomElementId, NewPaneHeight, NewPaneWidth)
{
} 

function explorecarousel()
{
}


function makeArray(len) 
{for (var i = 0; i < len; i++)
{this[i] = null
}
this.length = len
}



// array of day names

var daynames = new makeArray(7)
daynames[0] = "Sunday"
daynames[1] = "Monday"
daynames[2] = "Tuesday"
daynames[3] = "Wednesday"
daynames[4] = "Thursday"
daynames[5] = "Friday"
daynames[6] = "Saturday"


// array of month names

var monthnames = new makeArray(12)
monthnames[0] = "January"
monthnames[1] = "February"
monthnames[2] = "March"
monthnames[3] = "April"
monthnames[4] = "May"
monthnames[5] = "June"
monthnames[6] = "July"
monthnames[7] = "August"
monthnames[8] = "September"
monthnames[9] = "October"
monthnames[10] = "November"
monthnames[11] = "December"


// define date variables

var now = new Date()
var day = now.getDay()
var month = now.getMonth()
var year = now.getFullYear()
var date = now.getDate()
var hour=now.getHours()
var minutes=now.getMinutes()

// write date

function writeDate()

{
document.write("<FONT FACE='GENEVA, ARIAL, HELVETICA' SIZE='1'>" + daynames[day] + ", " + monthnames[month] + " " + date + ","+ year +"  "      +"</FONT>")
}

var contextMenuX = 0;
var contextMenuY = 0;

function loadContextMenuThumbsLib(evt, id,Menu1ClientID)
{   
var sAssets;
var downloadtype;  
var arraylist;
var i;
arraylist = '';
id = id.replace('0 ','');
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden');
for (i=0; i<checks.length; i++) {
if (MM_findObj('aselect_' + checks[i].value).checked) {
if (getParameter2('page')=='Carousel'){
arraylist = arraylist + ',' +  MM_findObj('asset_id')[i].value;
}else{
arraylist = arraylist + ',' +  checks[i].value;
}
}
}
}
if (arraylist.split(',').length > 2) {
arraylist= arraylist.substring(1, arraylist.length);
downloadtype = 'multi';
}
else {
/*download single*/
arraylist = id;
downloadtype = 'single';
}
<%if (useContextMenu) then %>
evt = (evt == null) ? window.event : evt;
var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
var scrollLeft;
var scrollTop;
if(is_chrome) {
	scrollLeft = document.body.scrollLeft;
	scrollTop = document.body.scrollTop;
	contextMenuX = evt.clientX + scrollLeft;
    contextMenuY = evt.clientY + scrollTop;
}
else {
    scrollLeft = document.documentElement && document.documentElement.scrollLeft? document.documentElement.scrollLeft : document.body.scrollLeft;
    scrollTop =  document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollTop  : document.body.scrollTop;
    contextMenuX = evt.x ? evt.clientX : evt.clientX + scrollLeft;
    contextMenuY = evt.y ? evt.clientY : evt.clientY + scrollTop;
}
var callbackX = evt.x ? contextMenuX + scrollLeft : contextMenuX;
var callbackY = evt.y ? contextMenuY + scrollTop  : contextMenuY;
var callBack1Element = Menu1ClientID.element;
callBack1Element.style.left = callbackX + 'px';
callBack1Element.style.top = callbackY  + 'px';
callBack1Element.style.zIndex = 2000;
Menu1ClientID.Callback(id, arraylist);


evt.cancelBubble = true;
evt.returnValue = false;
return false;
<% else %>      
if (downloadtype == 'multi') {
if (arraylist != ''){
if (confirm("Download selected assets?")) {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('multi,'+arraylist);
} else {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('single,'+id);
}
}
}
if (downloadtype == 'single') {
/*assume single*/
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('single,'+id);
}
<%end if%>
}




function loadContextMenuLib(evt, id, grid, Menu1CallBackClientID)
{   
//alert('document.body');
var sAssets; 
sAssets = grid.GetSelectedItems();
var downloadtype;
id = id.replace('0 ','');
var arraylist;
var i;
arraylist = '';
if (sAssets.length > 1) { 
/*download multi*/
for (i = 0; i < sAssets.length; i++) {
if (arraylist == ''){
arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
} else {
if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
}
}
}
arraylist= arraylist.substring(1, arraylist.length);
downloadtype = 'multi';

} else {
arraylist = id;
downloadtype = 'single';
}

<%if (useContextMenu) then %>
evt = (evt == null) ? window.event : evt;
var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
var scrollLeft;
var scrollTop;
if(is_chrome) {
	scrollLeft = document.body.scrollLeft;
	scrollTop = document.body.scrollTop;
	contextMenuX = evt.clientX + scrollLeft;
    contextMenuY = evt.clientY + scrollTop;
}
else {
    scrollLeft = document.documentElement && document.documentElement.scrollLeft? document.documentElement.scrollLeft : document.body.scrollLeft;
    scrollTop =  document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollTop  : document.body.scrollTop;
    contextMenuX = evt.x ? evt.clientX : evt.clientX + scrollLeft;
    contextMenuY = evt.y ? evt.clientY : evt.clientY + scrollTop;
}

var callbackX = evt.x ? contextMenuX + scrollLeft : contextMenuX;
var callbackY = evt.y ? contextMenuY + scrollTop  : contextMenuY;
var callBack1Element = Menu1CallBackClientID.element;
callBack1Element.style.left = callbackX + 'px';
callBack1Element.style.top = callbackY  + 'px';
callBack1Element.style.zIndex = 2000;
Menu1CallBackClientID.Callback(id, arraylist);

evt.cancelBubble = true;
evt.returnValue = false;
return false;
<% else %>      
if (downloadtype == 'multi') {
if (arraylist != ''){
if (confirm("Download selected assets?")) { 
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('multi,'+arraylist, '<%response.write(WebArchives.iDAM.Web.Core.IDAMWebSession.MultiDownloadFileName)%>');
} else { 
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('single,'+id);
}
}
}
if (downloadtype == 'single') {
/*assume single*/

<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('single,'+id);
}
<%end if%>
}



function ShowSlideShow(ids,type)
{ 	

if(GetCookie('IDAMSlideShowCoolIris')!='1')
{
if(isSilverlightInstalled()==true)
{
/*new slideshow silverlight ver*/
var t = 'iDAM SlideShow Enhanced';
var a = 'Lightbox.htm?page='+getParameter2("page")+'&ids='+ids+'&s=na&sort=name&sortdir=asc&silverlight=1&height=88&width=90&bwidth=' + screen.width + '&bheight=' + screen.height + '&KeepThis=true&TB_iframe=true&guid='+urlhelper();
var g = false;
try
{tb_show(t,a,g);
} catch(e) {}
/*this.blur();
return false;*/
}else{
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('setslideshowids',ids);
var strFeatures = "width=1024,height=800,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes";

window.open("./slideshow/slideshownew.aspx?assetids=" + '' + "&type=" + type + "&uid=<%response.write(webarchives.idam.web.core.idamwebsession.IDAMUser.userid)%>&usl=<%response.write(webarchives.idam.web.core.idamwebsession.idamuser.useraccess)%>","",strFeatures);

}
} else {
PicLensLite.start({feedUrl:'rss.aspx?page='+getParameter2("page")+'&assetids=' + ids + '&type=' + type});
}
}

function showslideshownewwin(fullscreen)
{
var strFeatures = "width=1024,height=800,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes";
if (fullscreen) {
window.open("./slideshow/slideshownew.aspx?assetids=" + '' + "&type=&uid=<%response.write(webarchives.idam.web.core.idamwebsession.IDAMUser.userid)%>&usl=<%response.write(webarchives.idam.web.core.idamwebsession.idamuser.useraccess)%>","",strFeatures + ",fullscreen=yes");
} else {
window.open("./slideshow/slideshownew.aspx?assetids=" + '' + "&type=&uid=<%response.write(webarchives.idam.web.core.idamwebsession.IDAMUser.userid)%>&usl=<%response.write(webarchives.idam.web.core.idamwebsession.idamuser.useraccess)%>","",strFeatures);
}
}

function ShowSlideShowUseSessionSql(val)
{ 
if(GetCookie('IDAMSlideShowCoolIris')!='1')
{
if(isSilverlightInstalled()==true) 
{
var urlhelperval = urlhelper();
/*new slideshow silverlight ver*/
var t = 'iDAM SlideShow Enhanced';
var a = 'Lightbox.htm?page='+getParameter2("page")+'&usesessionsql='+val+'_'+urlhelperval+'&s=na&sort=name&sortdir=asc&silverlight=1&height=88&width=90&bwidth=' + screen.width + '&bheight=' + screen.height + '&KeepThis=true&TB_iframe=true';
var g = false;
try 
{tb_show(t,a,g);
} catch(e) {}
/*this.blur();
return false;*/
}else{
/*add basic slideshow*/

window.open("./slideshow/slideshownew.aspx?usesessionsql=" + val + "&uid=<%response.write(webarchives.idam.web.core.idamwebsession.IDAMUser.userid)%>&usl=<%response.write(webarchives.idam.web.core.idamwebsession.idamuser.useraccess)%>","","width=1024,height=800,location=no,");

}
} else {
PicLensLite.start({feedUrl:'rss.aspx?page='+getParameter2("page")+'&usesessionsql='+val+'&guid='+urlhelper()});
}
}


function ShowSlideShowCarousel(id)
{ 
if(GetCookie('IDAMSlideShowCoolIris')!='1')
{
if(isSilverlightInstalled()==true) 
{
var urlhelperval = urlhelper();
/*new slideshow silverlight ver*/
var t = 'iDAM SlideShow Enhanced';
var a = 'Lightbox.htm?page='+getParameter2("page")+'&car='+id+'&s=na&sort=name&sortdir=asc&silverlight=1&height=88&width=90&bwidth=' + screen.width + '&bheight=' + screen.height + '&KeepThis=true&TB_iframe=true';
var g = false;
try
{tb_show(t,a,g);
} catch(e) {}
/*this.blur();
return false;*/
}else{
/*add basic slideshow*/
var strFeatures = "width=1024,height=800,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes";

window.open("./slideshow/slideshownew.aspx?assetids=all" + '' + "&type=carousel&uid=<%response.write(webarchives.idam.web.core.idamwebsession.IDAMUser.userid)%>&usl=<%response.write(webarchives.idam.web.core.idamwebsession.idamuser.useraccess)%>","",strFeatures);

}
} else {
PicLensLite.start({feedUrl:'rss.aspx?id=' + id});
}
}





function loadTreeAssetContextMenuLib(evt, id,Menu1CallBackClientID)
{   
//alert('document.body');
evt = (evt == null) ? window.event : evt;
var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
var scrollLeft;
var scrollTop;
if(is_chrome) {
	scrollLeft = document.body.scrollLeft;
	scrollTop = document.body.scrollTop;
	contextMenuX = evt.clientX + scrollLeft;
    contextMenuY = evt.clientY + scrollTop;
}
else {
    scrollLeft = document.documentElement && document.documentElement.scrollLeft? document.documentElement.scrollLeft : document.body.scrollLeft;
    scrollTop =  document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollTop  : document.body.scrollTop;
    contextMenuX = evt.x ? evt.clientX : evt.clientX + scrollLeft;
    contextMenuY = evt.y ? evt.clientY : evt.clientY + scrollTop;
}
var callbackX = evt.x ? contextMenuX + scrollLeft : contextMenuX;
var callbackY = evt.y ? contextMenuY + scrollTop  : contextMenuY;
var callBack1Element = Menu1CallBackClientID.element;
callBack1Element.style.left = callbackX + 'px';
callBack1Element.style.top = callbackY  + 'px';
callBack1Element.style.zIndex = 2000;
Menu1CallBackClientID.Callback(id, id);      
evt.cancelBubble = true;
evt.returnValue = false;
return false;
}




function yy_menuItemSelect(sender, eventArgs)
{

var item = eventArgs.get_item();

if (item.ID.indexOf('custom') > -1) {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('custom',item.get_value(),item.ID,item.ToolTip);
return true;
}
if (item.ID=='0') {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('single,'+item.get_value());
return true;
}
if (item.ID=='3')  {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('openfile,'+item.get_value());
return true;
}
if (item.ID=='4')  {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('openloc,'+item.get_value());
return true;
}
if (item.ID=='4b')  {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('openlocfid,'+item.get_value());
return true;
}
if (item.ID=='0dwa') {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('associated',item.get_value(),item.ToolTip);
return true;
}
var itemID; 
itemID = item.ID;
if (itemID>10) {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('downloadtype',item.get_value(),item.ID,item.ToolTip);
return true;
}
if (item.ID=='5') {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('downloadpdf',item.get_value(),item.ToolTip);
return true;
}
if (item.ID=='6') {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('multi,'+item.get_value(), item.ToolTip);
return true;
}
if (item.ID=='7') {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback("ppt",item.ToolTip,item.get_value(),"");
return true;
}
if (item.ID=='8') {
Object_PopUp_IDAMExpressAssets(item.get_value());
return true;
}
if (item.ID=='9') {
ShowSlideShow(item.get_value());
return true;
}
if (item.ID=='10') {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback("reconvert",item.ToolTip,item.get_value(),"");
alert("Reconvert request sent to the queue.");
return true; 
}
if (item.ID=='rotateleft') {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback("rotate",item.ToolTip,item.get_value(),"-90");
return true; 
}
if (item.ID=='rotateright') {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback("rotate",item.ToolTip,item.get_value(),"90");
return true; 
}

if (item.ID=='makeprojectimage') {
if (confirm("This will replace the project image with this asset image.  Are you sure you would like to continue?")){
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback("makeprojectimage",item.ToolTip,item.get_value(),"");
}return true; 
}

if (item.ID=='deleteshortcut') {
if (confirm("Are you sure you would like to delete this shortcut?")){
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback("deleteshortcut",item.ToolTip,item.get_value(),"");
}return true; 
}

if (item.ID=='orderselected') {
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback("addassettoorder",item.ToolTip,item.get_value(),"");
alert("Items added to order.");
return true; 
}
if (item.ID=='tagselected') {
opentagwindow(item.get_value());
return true; 
}
if (item.ID=='QP') {
openQuickPrintWindow(item.get_value());
return true; 
}
if (item.ID=='DELETEPROJECT') {
if (confirm(rightTrim(item.ToolTip) + "?"))
{if (confirm("Deleting this item will delete all items underneath it.  Do you want to continue?  Note: This cannot be undone.")){
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('deleteproject',item.get_value());}}return true; 
}
if (item.ID=='DELETECATEGORY') {
if (confirm(rightTrim(item.ToolTip) + "?"))
{if (confirm("Deleting this item will delete all items underneath it.  Do you want to continue?  Note: This cannot be undone.")){
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('deletecategory',item.get_value());}}return true; 
}
if (item.ID=='DELETEFOLDER') {
if (confirm(rightTrim(item.ToolTip) + "?"))
{if (confirm("Deleting this item will delete all items underneath it.  Do you want to continue?  Note: This cannot be undone.")){
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('deletefolder',item.get_value());}}return true; 
}
if (item.ID=='DELETEASSET') {
if (confirm(rightTrim(item.ToolTip) + "?"))
{if (confirm("Deleting this item will delete all items underneath it.  Do you want to continue?  Note: This cannot be undone.")){
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('deleteasset',item.get_value());}}return true; 
}
if (item.ID=='NEWPROJECT') {
ShowDialogWindowX(900,900,800,'ProjectNew.aspx?parent_id=' + item.get_value() + '&x=' + urlhelper(),'New Project','contentCssProject','iFrameCss');
return true; 
}
if (item.ID=='NEWQUICKCATEGORY') {
ShowDialogWindowX(850,850,800,'CategoryEdit.aspx?parent_id=' + item.get_value() + '&action=new' + '&x=' + urlhelper(),'New Category','contentCssFolder','iFrameCss');
return true; 
}
if (item.ID=='NEWPROJECTFOLDER') {
ShowDialogWindowX(850,850,800,'ProjectFolderEdit.aspx?parent_id=' + item.get_value() + '&action=new' + '&x=' + urlhelper(),'New Project Folder','contentCssFolder','iFrameCss');
return true; 
}
if (item.ID=='NEWPROJECTFOLDERFROMFOLDER') {
ShowDialogWindowX(850,850,800,'ProjectFolderEdit.aspx?parent_id=' + item.get_value() + '&action=new' + '&x=' + urlhelper(),'New Project Folder','contentCssFolder','iFrameCss');
return true; 
}
if (item.ID=='EDITQUICKPROJECT') {
ShowDialogWindowX(900,900,800,'ProjectNew.aspx?id=' + item.get_value() + '&action=edit' + '&x=' + urlhelper(),'Edit Project','contentCssProject','iFrameCss');
return true; 
}
if (item.ID=='EDITQUICKCATEGORY') {
ShowDialogWindowX(850,850,800,'CategoryEdit.aspx?id=' + item.get_value() + '&action=edit' + '&x=' + urlhelper(),'Edit Category','contentCssFolder','iFrameCss');
return true; 
}
if (item.ID=='EDITQUICKFOLDER') {
ShowDialogWindowX(850,850,800,'ProjectFolderEdit.aspx?id=' + item.get_value() + '&action=edit' + '&x=' + urlhelper(),'Edit Project Folder','contentCssFolder','iFrameCss');
return true; 
}
if (item.ID=='EDITFULLPROJECT') {
window.location="IDAM.aspx?page=Project&id=" + item.get_value() + "&type=Project&subtype=ProjectDetails";
return true; 
}
if (item.ID=='UPLOAD') {
Object_PopUp_UploadExplore(item.get_value());
return true; 
}
if (item.ID=='NEWPORTAL') {
Object_PopUp('PortalEdit.aspx?action=new','New_Portal',650,850);
return true; 
}
if (item.ID=='EDITPORTAL') {
Object_PopUp('Portaledit.aspx?id=' + item.get_value() + '&action=edit','Edit_Portal',650,850);
return true; 
}
if (item.ID=='NEWPORTALPAGE') {
Object_PopUp('PortalPageEdit.aspx?parent_id=' + item.get_value() + '&action=new','New_Portal_Page',650,850);
return true; 
}
if (item.ID=='EDITPORTALPAGE') {
Object_PopUp('PortalPageEdit.aspx?id=' + item.get_value() + '&action=edit','Edit_Portal_Page',650,850);
return true; 
}
if (item.ID=='NEWPORTLET') {
Object_PopUp('PortletEdit.aspx?parent_id=' + item.get_value() + '&action=new','New_Portlet',650,950);
return true; 
}
if (item.ID=='EDITPORTLET') {

Object_PopUp('PortletEdit.aspx?id=' + item.get_value() + '&action=edit','Edit_Portlet',650,950);	
return true; 
}
if (item.ID=='UPLOADEXPRESS') {

ShowDialogWindowX(624,624,1000,'UploadExpress.aspx?objectid=' + item.get_value() + '&action=upload' + '&x=' + urlhelper(),'IDAMExpress Upload','contentCssUEXPRESS','iFrameCss');			
return true; 
}
//end
}

















//explore
function nodeExpand(sourceNode)
{
return true;
}

function nodeMove(sourceNode, targetNode)
{

<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("IDAM_MOVEASSETS") Then%>
var doMove = false;  
var srouceNodeText;
var srouceNodeID;
var targetNodeID;
var comboID;
//possible combos  CATCAT PRJCAT ACTACT ACTPRJ ASSACT ASSPRJ
srouceNodeID=sourceNode.ID.substr(0,3).toLocaleUpperCase();
targetNodeID=targetNode.ID.substr(0,3).toLocaleUpperCase();
comboID=srouceNodeID+targetNodeID
if(comboID=='CATCAT'||comboID=='PRJCAT'||comboID=='ACTACT'||comboID=='ASSACT'||comboID=='ASSPRJ'||comboID=='ACTPRJ')
{
if(targetNode)
{
doMove = confirm("Move '" + rightTrim(sourceNode.Text) + "' to '" + targetNode.Text + "'?"); 
} else {
doMove = false; 
}
if (doMove)
{
<%response.write(idamadmin.IDAM.MenuCallBack1Clientid)%>.Callback("Move",sourceNode.ID,targetNode.ID);

//if (navigator.userAgent.indexOf("MSIE")!=-1){
//alert('Move completed.');
//}
}
}
else
{
doMove = false; 
}
return doMove; 
<%end if%>

}

function GridItemMove(sourceNode, targetNode)
{
return GridItemMoveSelected(sourceNode, targetNode);
}

function GridItemMoveSelected(sourceNodes, targetNode)
{

<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("IDAM_MOVEASSETS") Then%>
var doMove = false; 
var doShortcut = false; 
var doCopy = false;
var srouceNodeText;
var srouceNodeID;
var targetNodeID;
var comboID;
//possible combos  CATCAT PRJCAT ACTACT ACTPRJ ASSACT ASSPRJ
srouceNodeID='ASS';
targetNodeID=targetNode.ID.substr(0,3).toLocaleUpperCase();
comboID=srouceNodeID+targetNodeID
if(comboID=='CATCAT'||comboID=='PRJCAT'||comboID=='ACTACT'||comboID=='ASSACT'||comboID=='ASSPRJ')
{
if(targetNode)
{
var gAgent=navigator.userAgent.toLowerCase();
var gbFirefox=(gAgent.indexOf("firefox")!=-1);
e=window.event||window.Event;
if(e.ctrlKey){
doShortcut = confirm("Create a shortcut of asset(s) to '" + targetNode.Text + "'?"); 
}else{
if(e.shiftKey){
doCopy = confirm("Create a copy of asset(s) to '" + targetNode.Text + "'?"); 
}else{
doMove = confirm("Move asset(s) to '" + targetNode.Text + "'?"); 
}
}
} else {
doMove = false; 
}
if (doMove)
{
<%response.write(idamadmin.IDAM.MenuCallBack1Clientid)%>.Callback("MoveSelected","ASS" + sourceNodes,targetNode.ID);
}
if (doShortcut)
{
<%response.write(idamadmin.IDAM.MenuCallBack1Clientid)%>.Callback("ShortcutSelected","ASS" + sourceNodes,targetNode.ID);
}
if (doCopy)
{
<%response.write(idamadmin.IDAM.MenuCallBack1Clientid)%>.Callback("CopySelected","ASS" + sourceNodes,targetNode.ID);
}
}

else
{
doMove = false; 
}
return doMove; 
<%end if%>
}
function rightTrim(sString) 
{
while (sString.substring(sString.length-1, sString.length) == ' ')
{
sString = sString.substring(0,sString.length-1);
}
return sString;
}


function treeContextMenu(treeNode, evt)
{
//alert('document.body');

evt = (evt == null) ? window.event : evt;
var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
var scrollLeft;
var scrollTop;
if(is_chrome) {
	scrollLeft = document.body.scrollLeft;
	scrollTop = document.body.scrollTop;
	contextMenuX = evt.clientX + scrollLeft;
    contextMenuY = evt.clientY + scrollTop;
}
else {
    scrollLeft = document.documentElement && document.documentElement.scrollLeft? document.documentElement.scrollLeft : document.body.scrollLeft;
    scrollTop =  document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollTop  : document.body.scrollTop;
    contextMenuX = evt.x ? evt.clientX : evt.clientX + scrollLeft;
    contextMenuY = evt.y ? evt.clientY : evt.clientY + scrollTop;
}
var callbackX = evt.x ? contextMenuX + scrollLeft : contextMenuX;
var callbackY = evt.y ? contextMenuY + scrollTop  : contextMenuY;
var callBack1Element = <%response.write(idamadmin.IDAM.MenuCallBack1Clientid)%>.element;
callBack1Element.style.left = callbackX + 'px';
callBack1Element.style.top = callbackY  + 'px';
callBack1Element.style.zIndex = 2000;    
document.getElementById('ExploreTreeNewselectednode').value = treeNode.ID;
document.getElementById('idamaction').value = 'editcategories';
Menu2.ShowContextMenu(contextMenuX,contextMenuY,treeNode); 
<%response.write(idamadmin.IDAM.MenuCallBack1Clientid)%>.Callback('treeview',treeNode.ID,treeNode.Value,treeNode.getProperty("ToolTip"));
}

function NavigateToProject(redirectloc)
{
window.location = 'IDAM.aspx?Page=Project&ID=' + redirectloc + '&type=Project&subtype=ProjectDetails';
}
function NavigateToRepository()
{
window.location = 'IDAM.aspx?Page=Repository';
}
function NavigateToPage(Page)
{
window.location = 'IDAM.aspx?Page=' + Page;
}
function NavigateToCategory(redirectloc)
{
window.location = 'IDAM.aspx?Page=Category&ID=' + redirectloc + '&type=category&action=edit';
}
function NavigateToBrowseCategory(id)
{
window.location = 'IDAM.aspx?Page=Browse&id=' + id + '&type=category';
}
function NavigateToProjectFolder(pid,redirectloc)
{
window.location = 'IDAM.aspx?Page=Project&ID=' + pid + '&type=Project&c=' + redirectloc;
}
function NavigateToProjectFolder2(pid,redirectloc)
{
window.location = 'IDAM.aspx?Page=Project&ID=' + pid + '&type=project&c=' + redirectloc;
}

function NavigateToProjectFolder3(pid,redirectloc)
{
window.location = window.location.href.match(/^[^\#\?]+/)[0] + '?Page=Project&ID=' + pid + '&type=project&c=' + redirectloc;
}
function NavigateToAsset2(pid)
{
window.location = window.location.href.match(/^[^\#\?]+/)[0] + '?Page=Asset&ID=' + pid + '&type=asset';
}
function Object_PopUp(strUrl, popupName, intHeight, intWidth)
{
if (strUrl == null || strUrl.Length <= 0)
return;

var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=yes,resizable=yes,toolbar=no";
//var strFeatures = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes";
if (intHeight != null)
strFeatures += ",height="+intHeight;
if (intWidth != null)
strFeatures += ",width=" + intWidth;
//var strFeatures += "'"

if (popupName == null || popupName.Length <= 0)
{
var theWindow = window.open( strUrl, "PopUpWindow", strFeatures , false );
}
else
{
var theWindow = window.open( strUrl, popupName, strFeatures, false );
}
theWindow.focus();
}


function Object_PopUpNoScrollbars(strUrl, popupName, intHeight, intWidth)
{
if (strUrl == null || strUrl.Length <= 0)
return;
var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=no,resizable=yes,toolbar=no";
if (intHeight != null)
strFeatures += ",height="+intHeight;
if (intWidth != null)
strFeatures += ",width=" + intWidth;
if (popupName == null || popupName.Length <= 0)
{
var theWindow = window.open( strUrl, "PopUpWindow", strFeatures , false );
}
else
{
var theWindow = window.open( strUrl, popupName, strFeatures, false );
}
theWindow.focus();
}

function Object_PopUpScrollbars(strUrl, popupName, intHeight, intWidth)
{
if (strUrl == null || strUrl.Length <= 0)
return;
var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=yes,resizable=yes,toolbar=no";
//var strFeatures = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes";
if (intHeight != null)
strFeatures += ",height="+intHeight;
if (intWidth != null)
strFeatures += ",width=" + intWidth;
//var strFeatures += "'"
if (popupName == null || popupName.Length <= 0)
{
var theWindow = window.open( strUrl, "PopUpWindow", strFeatures , false );
}
else
{
var theWindow = window.open( strUrl, popupName, strFeatures, false );
}
theWindow.focus();
}







//IDAM Bottom
function Object_PopUp_Carousel(tab)
{
var dropdown = document.getElementById('carousel_select');
var myindex  = dropdown.selectedIndex;
var SelValue = dropdown.options[myindex].value;
if (tab == 'view')
{
ShowDialogWindowX(905,905,840,'CarouselNew.aspx?Carousel_id=' + SelValue + '&tab=view' + '&x=' + urlhelper(),'Carousel','contentCssCarousel','iFrameCss');
} else {
ShowDialogWindowX(905,905,840,'CarouselNew.aspx?Carousel_id=' + SelValue + '&x=' + urlhelper(),'Carousel','contentCssCarousel','iFrameCss');
}
}

function Object_PopUp_IDAMExpress()
{
var dropdown = document.getElementById('carousel_select');
var myindex  = dropdown.selectedIndex;
var SelValue = dropdown.options[myindex].value;
ShowDialogWindowX(1340,1340,972,'IDAMExpress.aspx?Carousel_id=' + SelValue + '&x=' + urlhelper(),'IDAMExpress Carousel','contentCssEXPRESS','iFrameCss');
}

function Object_gotoCarousel()
{
var dropdown = document.getElementById('carousel_select');
var myindex  = dropdown.selectedIndex;
var SelValue = dropdown.options[myindex].value;
if (SelValue != '')
{
window.location='IDAM.aspx?page=Carousel&id=' + SelValue;
}
}
function Object_PopUp_Upload(projectid,categoryid)
{
<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("IDAM_USEUPLOADFOLDERS") Then%>
ShowDialogWindowX(730,730,700,'UploadFolders.aspx?projectid=' + projectid + '&categoryid=' + categoryid + '&x=' + urlhelper,'Select Upload Folder','contentCssUploadFolders','iFrameCss',true);
<%Else%>
Object_PopUp('includes/Upload/UploadAsync.aspx?projectid=' + projectid + '&categoryid=' + categoryid + '&uid=<%response.write(webarchives.idam.web.core.idamwebsession.idamuser.userid)%>','Upload',640,777);
<%End If%>
}
function Object_PopUp_UploadExplore(objectid)
{
<%  If WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("IDAM_USEUPLOADFOLDERS") Then%>
ShowDialogWindowX(730,730,700,'UploadFolders.aspx?id=' + objectid + '&x=' + urlhelper,'Select Upload Folder','contentCssUploadFolders','iFrameCss',true);
<%Else%>
Object_PopUp('includes/Upload/UploadAsync.aspx?objectid=' + objectid + '&uid=<%response.write(webarchives.idam.web.core.idamwebsession.idamuser.userid)%>','Upload',640,777);
<%End If%>
}
function Object_PopUp_Repository(objectid)
{
Object_PopUp('RepositoryEdit.aspx?id=' + objectid,'Repository',550,480);
}
function Object_PopUp_User(objectid)
{
ShowDialogWindowX(905,905,840,'UserEdit.aspx?id=' + objectid + '&x=' + urlhelper,'Edit User Contact','contentCssUser','iFrameCss');
}
function Object_PopUp_Tenant(objectid)
{
ShowDialogWindowX(905,905,840,'Tenant.aspx?id=' + objectid + '&x=' + urlhelper(),'Edit Tenant Information','contentCssUser','iFrameCss');
}
function Object_PopUp_Space(id,pid)
{
ShowDialogWindowX(905,905,840,'Space.aspx?id=' + id + '&pid='+pid+'&x=' + urlhelper(),'Edit Space Information','contentCssUser','iFrameCss');
}
function Object_PopUp_UserNew()
{
ShowDialogWindowX(905,905,840,'UserEdit.aspx' + '?x=' + urlhelper,'Edit User Contact','contentCssUser','iFrameCss');
}
function Object_PopUp_TenantNew()
{
ShowDialogWindowX(905,905,840,'Tenant.aspx' + '?x=' + urlhelper(),'Edit Tenant Information','contentCssUser','iFrameCss');
}
function Object_PopUp_SpaceNew(pid)
{
ShowDialogWindowX(905,905,840,'Space.aspx' + '?pid='+pid+'&x=' + urlhelper(),'Edit Space Information','contentCssUser','iFrameCss');
}

function Object_PopUp_Contact(objectid)
{
ShowDialogWindowX(905,905,840,'UserEdit.aspx?id=' + objectid + '&type=contact' + '&x=' + urlhelper,'Edit User Contact','contentCssUser','iFrameCss');
}
function Object_PopUp_ContactNew()
{
ShowDialogWindowX(905,905,840,'UserEdit.aspx?type=contact' + '&x=' + urlhelper,'Edit User Contact','contentCssUser','iFrameCss');
}
function Object_PopUp_Client(objectid)
{
ShowDialogWindowX(905,905,840,'ClientEdit.aspx?id=' + objectid + '&x=' + urlhelper,'Edit Client','contentCssUser','iFrameCss');
}
function Object_PopUp_ClientNew()
{
ShowDialogWindowX(905,905,840,'ClientEdit.aspx?x=' + urlhelper,'Create Client','contentCssUser','iFrameCss');
}
function Object_PopUp_Keyword(URL)
{
ShowDialogWindowX(700,700,700,URL + '&x=' + urlhelper,'Keywords','contentCssKeyword','iFrameCss');
}
function Object_PopUp_UDF(URL)
{
ShowDialogWindowX(1124,1124,1000,URL + '&x=' + urlhelper(),'UDF','contentCssUDF','iFrameCss');
}
function Object_PopUp_Roles(URL)
{
Object_PopUp(URL,'Roles',800,800);
}
function Object_PopUp_Splash(URL)
{
Object_PopUp(URL,'Splash',295,485);
}
function Object_PopUp_Recover(URL)
{
Object_PopUp(URL,'Roles',800,1124);
}
function Object_PopUp_Vendor(objectid)
{
ShowDialogWindowX(905,905,840,'VendorEdit.aspx?id=' + objectid + '&x=' + urlhelper,'Edit Vendor','contentCssUser','iFrameCss');
}
function Object_PopUp_VendorNew()
{
ShowDialogWindowX(905,905,840,'VendorEdit.aspx?x=' + urlhelper,'Create Vendor','contentCssUser','iFrameCss');
}
function Object_PopUp_Group(objectid)
{
ShowDialogWindowX(905,905,840,'GroupEdit.aspx?id=' + objectid + '&x=' + urlhelper,'Edit Group','contentCssGroup','iFrameCss');
}
function Object_PopUp_GroupNew()
{
ShowDialogWindowX(905,905,840,'GroupEdit.aspx?x=' + urlhelper,'Create Group','contentCssGroup','iFrameCss');
}
function Object_PopUp_RepositoryNew()
{
Object_PopUp('RepositoryEdit.aspx','Repository',550,480);
}
function Object_PopUp_ContactSheetConfig(carid,sortid)
{
ShowDialogWindowX(700,700,700,'ContactSheetConfig.aspx?carid=' + carid + '&sort_id=' + sortid + '&x=' + urlhelper(),'Contact Sheet Options','contentCssKeyword','iFrameCss');
}

function BrowseOnSingleClick(item)
{


var itemvaluetmp;
itemvaluetmp = item.GetMember('uniqueid').Value;
itemvaluenametmp = item.GetMember('name').Value;
if (itemvaluetmp.substring(0,1) == 'P') 

{
itemvaluetmp = itemvaluetmp.replace('P_','');


}
return true;
}  



function BrowseOnSingleClickAssets(item)
{

var itemvaluetmp;
var itemvaluenametmp;
var itemvaluefiletypetmp;
itemvaluetmp = item.GetMember('asset_id').Value;
itemvaluenametmp = item.GetMember('name').Value;
//itemvaluefiletypetmp = item.GetMember('timage').Value;

return true;
}  


function BrowseOnSingleClickAssetsLegacy(item)
{

var itemvaluetmp;
var itemvaluenametmp;
var itemvaluefiletypetmp;
itemvaluetmp = item.GetMember('asset_id').Value;
itemvaluenametmp = item.GetMember('name').Value;
itemvaluefiletypetmp = item.GetMember('imagesource').Value;


return true;
}  

function DownloadCarouselUsingWS()
{ 
var myindex  = document.getElementById('carousel_select').selectedIndex;
var SelValue = document.getElementById('carousel_select').options[myindex].value;
var SelName = document.getElementById('carousel_select').options[myindex].text;
var answer = false
<%  If WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("DOWNLOAD_ASSOCIATED_CAROUSEL") Then %>
answer =   confirm ("Do you want to include associated assets?")
<%end if%>

<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('carousel',SelValue, answer, SelName)

}

function openCarouselNew()
{
ShowDialogWindowX(905,905,840,'CarouselNew.aspx?x=' + urlhelper(),'Create Carousel','contentCssCarousel','iFrameCss');
}

function downloadticket(ticketId, name) {

<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('ticket',ticketId, name);
}

function NavigateToCarousel()
{

ChangeDefaultCarousel(document.getElementById('carousel_select'));
}

function NavigateToAsset(id)
{
window.location = 'IDAM.aspx?page=Asset&Id=' + id + '&type=Asset';
}
function onCallbackError()
{
//window.reload;
//alert('Callback Error');
}
function opentagwindow(ids)
{
ShowDialogWindowX(1340,1340,1372,'AssetTagging.aspx?ids=' + ids + '&x=' + urlhelper(),'Asset Tagging','contentCssTagging','iFrameCss');
}
function openQuickPrintWindow(ids)
{
<%response.write(idamadmin.IDAM.downloadcheck_callback_clientID)%>.Callback('setcontactsheetitems',ids);
Object_PopUp_ContactSheetConfig('','');
//ShowDialogWindowX(905,905,840,'ContactSheet.aspx?QP=True&Heading=&columns=1&ffilename=1&ffilesize=1&fdimensions=1&fdate=1&ffiletype=1&asset_array=' + ids + '&x=' + urlhelper(),'Quick Print','contentCssUser','iFrameCss');
}


function ShowDialogWindowXLib(width,hct,fct,loc,heading,contentCss,iframeCss,bmodal,DlgMainClientID)
{
//alert('document.body');
if (bmodal=='undefined'){bmodal=false}
if(DlgMainClientID.get_isShowing())
{
DlgMainClientID.Close();
}
else
{
document.getElementById('DlgMainTitle').innerHTML = heading;
document.getElementById('dialogheadertable').width = width;
document.getElementById('dialogfootertable').width = width;
document.getElementById('dialogheadercontenttable').width = hct;
document.getElementById('dialogfootercontenttable').width = fct;
var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
var scrollLeft;
var scrollTop;
if(is_chrome) {
	scrollLeft = document.body.scrollLeft;
	scrollTop = document.body.scrollTop;
}
else {
    scrollLeft = document.documentElement && document.documentElement.scrollLeft? document.documentElement.scrollLeft : document.body.scrollLeft;
    scrollTop =  document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollTop  : document.body.scrollTop;
}
var winW = 50;
var winH = 50;
var switchbacktopopup = false;
if (parseInt(navigator.appVersion)>3) {
if (navigator.appName=="Netscape") {
winW = window.innerWidth;
winH = window.innerHeight;
}
if (navigator.appName.indexOf("Microsoft")!=-1) {
winW = document.body.offsetWidth;
winH = document.body.offsetHeight;
}
}
DlgMainClientID.set_x((winW-width)/2);
DlgMainClientID.set_y(scrollTop+15);
DlgMainClientID.set_modal(bmodal);
DlgMainClientID.set_width(width);
DlgMainClientID.set_contentCssClass(contentCss);
DlgMainClientID.set_iFrameCssClass(iframeCss); 
DlgMainClientID.set_contentUrl(loc); 
DlgMainClientID.set_alignment('TopCentre');
switch (contentCss) {
case 'contentCssTagging': width = 1330;hct = 800; switchbacktopopup=true;break;
case 'contentCssEXPRESS': width = 1330;hct = 740; switchbacktopopup=true;break;
case 'contentCssUDF': width = 1114;hct = 750; break;
case 'contentCssUEXPRESS': width = 614;hct = 750; switchbacktopopup=true;break;
case 'contentCssKeyword': width = 690;hct = 650; break;
case 'contentCssFolder': width = 840;hct = 600; break;
case 'contentCssProject': width = 890;hct = 700; break;
case 'contentCssGroup': width = 895;hct = 700; break;
case 'contentCssUser': width = 895;hct = 800; switchbacktopopup=true;break;
case 'contentCssCarousel': width = 895;hct = 720; break;
case 'contentCssUploadFolders': width = 720;hct = 450; break;
default: width = 895;hct = 400; 
}

heading=heading.replace(/ /g,"_");
if (switchbacktopopup){
Object_PopUp(loc,heading,hct,width);
}else{
DlgMainClientID.show();
DlgMainClientID.focus();
}
}
}
function urlhelper()
{
var thisdate = new Date();
return thisdate.getTime();
}
function gotoprojectnum(ID)
{
window.location.href = 'IDAM.aspx?page=Project&type=project&Id='+ID;
}

//Project
function gotopage(tab)
{

// Get names
var queryString = window.location.href.substring((window.location.href.indexOf('?') + 1));
var array1 = queryString.split('&'); // Each array element is in format "fieldname=value"
var IDValue;
for(var i = 0; i < array1.length; i++){
var tempArray = array1[i].split('='); // Separate fieldname and value
if (tempArray[0]=='ID') {
IDValue = tempArray[1]
}
if (tempArray[0]=='id') {
IDValue = tempArray[1]
}
if (tempArray[0]=='Id') {
IDValue = tempArray[1]
}				
//eval(tempArray[0] + " = \"" + tempArray[1] + "\"");
}


var newurl = 'IDAM.aspx?Page=Project&ID=' + IDValue + '&type=Project&subtype=' + tab.ID;
//alert(newurl);
window.location.href = newurl;
return true;
}

function gotopagelink(subtype)
{

// Get names
var queryString = window.location.href.substring((window.location.href.indexOf('?') + 1));
var array1 = queryString.split('&'); // Each array element is in format "fieldname=value"
var IDValue;
for(var i = 0; i < array1.length; i++){
var tempArray = array1[i].split('='); // Separate fieldname and value
if (tempArray[0]=='ID') {
IDValue = tempArray[1]
}
if (tempArray[0]=='id') {
IDValue = tempArray[1]
}
if (tempArray[0]=='Id') {
IDValue = tempArray[1]
}				
//eval(tempArray[0] + " = \"" + tempArray[1] + "\"");
}


var newurl = 'IDAM.aspx?Page=Project&ID=' + IDValue + '&type=Project&subtype=' + subtype;
//alert(newurl);
window.location.href = newurl;
return true;
}


function loadjsfile(filename)
{

  //var fileref=document.createElement('script')
  //fileref.setAttribute("type","text/javascript")
  //fileref.setAttribute("src", filename)
  //if (typeof fileref!="undefined")
  //document.getElementsByTagName("head")[0].appendChild(fileref)

}

function ClearAllItemsThumb()
{
var bcheck,checks;
var highlightbox;
var checks = MM_findObj('selectassethidden');
bcheck = false;
for (i=0; i<checks.length; i++) {
highlightbox = document.getElementById('assethighlight_' + checks[i].value);
highlightbox.style.border = '1px solid transparent';
highlightbox.style.backgroundColor = '';
highlightboxcheckbox = document.getElementById('aselect_' + checks[i].value);
highlightboxcheckbox.checked = false;
}
}

function getParameter2 (  parameterName ) {
try
{
var queryString = window.location.href;
var parameterName = parameterName + "=";
if ( queryString.length > 0 ) {
begin = queryString.indexOf ( parameterName );
if ( begin != -1 ) {
begin += parameterName.length;
end = queryString.indexOf ( "&" , begin );
if ( end == -1 ) {
end = queryString.length
}
return unescape ( queryString.substring ( begin, end ) );
}
return "null";
}
}
catch (err)
{
alert(err.description);
}
}





function isSilverlightInstalled()

{
   
    var isSilverlightInstalled = false;

    

    try

    {

        //check on IE

        try

        {

            var slControl = new ActiveXObject('AgControl.AgControl');

            isSilverlightInstalled = true;

        }

        catch (e)

        {

            //either not installed or not IE. Check Firefox

            if ( navigator.plugins["Silverlight Plug-In"] )

            {

                isSilverlightInstalled = true;

            }

        }

    }

    catch (e)

    {

        //we don't want to leak exceptions. However, you may want

        //to add exception tracking code here.

    }

    return false;

}




