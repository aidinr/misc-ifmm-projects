
Imports WebArchives.iDAM.Services.ConfigurationService



Partial Public Class JSLib
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected useContextMenu As Boolean = False

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        useContextMenu = WebArchives.iDAM.Web.Core.IDAMWebSession.Config.ConfigurationService.GetConfigurationSetting(WebAdminConfigKey.IDAMEnableDownloadContextMenu).ToLower.Equals("true")
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetExpires(DateTime.Today.AddMonths(2))
        Response.AppendHeader("Cache-Control", "post-check=3600,pre-check=43200")
        Response.ContentType = "application/x-javascript"
    End Sub

End Class

