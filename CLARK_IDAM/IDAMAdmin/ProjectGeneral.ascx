<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.ProjectGeneral" enableViewState="False" CodeBehind="ProjectGeneral.ascx.vb" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<script language="javascript">
    var xoffsetpopup
    xoffsetpopup = -150
    var yoffsetpopup
    yoffsetpopup = -500
</script>
<!--ipt language="javascript" src="js/filesearchhover.js" />-->
<script type="text/javascript">
//<![CDATA[    ]

//]]>
</script>
<script type="text/javascript"  src="js/jquery.tabSlideOut.v1.3.js"></script>
<script type="text/javascript" language="javascript">

$(document).ready(function() {
$(function(){
        $('.slide-out-div').tabSlideOut({
            tabHandle: '.handle',                     
            pathToTabImage: '/images/side_tab_filters.jpg', 
            imageHeight: '122px',                     
            imageWidth: '30px',                       
            tabLocation: 'right',                     
            speed: 300,                              
            action: 'click',                        
            topPos: '250px',                        
            leftPos: '20px',                        
            fixedPosition: true
            
        });

        

    });

    function overlayclickclose() {
        if (closedialog) {
            $('#dialogAssetFiltersNew').dialog('close');
        }
        //set to one because click on dialog box sets to zero
        closedialog = 1;
    }

    $('#dialogAssetFiltersNew').dialog({ autoOpen: false,
        open: function() { closedialog = 0; $(document).bind('click', overlayclickclose); },
        focus: function() { closedialog = 0; },
        close: function() { $(document).unbind('click'); }
    });

    $('#assetfilternewaddfilter_clear').click(function() {
        ClearFilters();
        DoSearchFilter('filter', document.getElementById('ctl14_ctl00_search_filter_asset').value);
        return false;
    });
    
    $(window).resize(function() {
        try
        {
            DoSearchFilter('filter', document.getElementById('ctl14_ctl00_search_filter_asset').value);
            ReSizeassetfilterscrollbars();
        } catch(x) {}

    });
         
    ReSizeassetfilterscrollbars();
    
});

function ReSizeassetfilterscrollbars(){     
var p = $('.slide-out-div');
var position = p.position();
var tagpos = ($(window).height() - position.top - 120);
$('#assetfilterscrollbars').height(tagpos);
}

function LoadFiltersCallback(){
var p = $('.slide-out-div');
var position = p.position();
var tagpos = ($(window).width() - position.left);
if (tagpos == 0) {
<%response.write(CallbackAssetFiltersNew.ClientID)%>.Callback('forcebuild');
}
}


function loadgridthumbnail(imageobj,asset_id)
{
var urlstring;
urlstring = '<%response.write(WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase)%>?id=' + asset_id + '&instance=<%response.write (IDAMInstance)%>&type=asset&size=2&height=65&width=65';
//imageobj.src=urlstring;
//return urlstring;
}
function ToggleSnapMinimize(SnapObject, MenuItemIndex)
{
   
}    
 function AssetFiltersCallback_onCallbackComplete(sender, eventArgs)
{
//setTimeout("showCustomFooter();",timeoutDelay);
}
function GridAssets_onCallbackComplete(sender, eventArgs)
{
sizeCenterPane();
showCustomFooter();
var p = $('.slide-out-div');
var position = p.position();
var tagpos = ($(window).width() - position.left);
try
{
    if ( tagpos > 50) {
        
        CallbackAssetFiltersNew.Callback('forcebuild'); 
        try {
            $(dialogAssetFiltersNew).dialog('close');
        } catch (ex)
{ } 
    }
    if ((sender.get_parameter(0) == 'projectpagefilter') && tagpos > 50) {
        CallbackFilterCrumbs.Callback();
        CallbackAssetFiltersNew.Callback('forcebuild');
        try {
            $(dialogAssetFiltersNew).dialog('close');
        } catch (ex)
{ } 
    }
}catch(ex){}
return true;
}
  
function ToggleItemCheckedState(MenuItemIndex)
{
var item = <%response.write(ctype(page, IdamAdmin.IDAM).RootClientID)%>_MenuControl.Items(0).Items(MenuItemIndex); 
if (item.GetProperty('Look-LeftIconUrl') == 'check.gif')
{
item.SetProperty('Look-LeftIconUrl','clear.gif');
//set cookie to false
SetCookie ('WindowProjectAssets' + MenuItemIndex, 0, exp);

}
else
{
item.SetProperty('Look-LeftIconUrl','check.gif');
//set cookie to true
SetCookie ('WindowProjectAssets' + MenuItemIndex, 1, exp);
}
item.ParentMenu.Render();

}

//setup form switch for delete or multi asset zip download
function DownloadAssets(ids){
var sAssets;
var downloadtype;
sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
ids = ids.replace('0 ','');
var arraylist;
var i;
arraylist = '';
if (sAssets.length > 1) {
 if (confirm("Download selected assets?"))
 {
/*download multi*/
for (i = 0; i < sAssets.length; i++) {
if (arraylist == ''){
arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
} else {
if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
}
}
}

arraylist= arraylist.substring(1, arraylist.length);
downloadtype = 'multi';
 }
 else
 {
/*download single*/
downloadtype = 'single';

 }
}
else {
/*download single*/
downloadtype = 'single';
}
if (downloadtype == 'multi') {
if (arraylist != ''){
<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('multi,'+arraylist);
}
}
if (downloadtype == 'single') {
/*assume single*/
<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('single,'+ids);
}

}

function PreviewOverlayOnSingleClickAssets(item)
  {
var itemvaluetmp;
var itemvaluenametmp;
var itemvaluefiletypetmp;
itemvaluetmp = item.GetMember('asset_id').Value;
itemvaluenametmp = item.GetMember('name').Value;
itemvaluefiletypetmp = getURLPreviewIcon() + itemRow.GetMember('imagesource').Value;

return true;
  }  
  
  
  function RotateAsset(asset_id,degrees,aname,afiletype)
  {

  }
  
  function PreviewOverlayOnThumbSingleClickAssets(aid,aname,afiletype,eventObject)
  {

//return true;
  } 
  
function PreviewOverlayOnSingleClickAssetsFromIcon(RowId,eventObject)
  {
try
{
var itemvaluetmp;
var itemvaluenametmp;
var itemvaluefiletypetmp;
var item 
itemRow = <% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(RowId);
itemvaluetmp = itemRow.GetMember('asset_id').Value;
itemvaluenametmp = itemRow.GetMember('name').Value;
itemvaluefiletypetmp = getURLPreviewIcon() + itemRow.GetMember('imagesource').Value;


}  //end try  
catch(err)
{
txt="There was an error on this page.\n\n"
txt+="Error description: " + err.description
alert(txt);
}
//return true;
}

function AssetPreview(Asset_ID,cid)
{
}

//setup goto for grid nav
function GotoLocation(ctype,cid)
{
window.location = 'IDAM.aspx?page=Project&Id=<%response.write (request.querystring("Id"))%>&type=project&c=' + cid;
return true;
}

  function onUpdate(item)
  {
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
  }
  function onInsert(item)
  {
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
  }
  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridAssets.ClientID) %>.Page(1); 
  }
  function onDelete(item)
  {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
  }
function editRow()
  {
    <% Response.Write(GridAssets.ClientID) %>.EditComplete();     
  }
function editGrid(RowId)
  {
    itemRow = <% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(RowId);
itemvaluetmp = itemRow.GetMember('asset_id').Value;
    window.location = 'IDAM.aspx?page=Asset&ID=' + itemvaluetmp + '&type=asset'
  }

function CheckAllItems()
{

if(<% Response.Write(GridAssets.ClientID) %>.get_pageSize()<<% Response.Write(GridAssets.ClientID) %>.get_recordCount()) itemcnt=<% Response.Write(GridAssets.ClientID) %>.get_pageSize();
if(<% Response.Write(GridAssets.ClientID) %>.get_pageSize()><% Response.Write(GridAssets.ClientID) %>.get_recordCount()) itemcnt=<% Response.Write(GridAssets.ClientID) %>.get_recordCount();
var itemIndex = 0;
for (var x = 1; x <= itemcnt; x++)
{
<% Response.Write(GridAssets.ClientID) %>.Select(<% Response.Write(GridAssets.ClientID) %>.Table.GetRow(x-1),true);
}
<% Response.Write(GridAssets.ClientID) %>.Render();
}

function MM_findObj(n, d) { //v3.0
var p,i,x; if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}
function CheckAllItemsThumbs() { //Set All checkboxes, if un-checked, check them.
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
var dummyobjectvalue = checks[0].value;
var firstobjectcheckedstate = document.getElementById('aselect_' + dummyobjectvalue);
var bcheck = firstobjectcheckedstate.checked
if (bcheck) {
bcheck = true;
} else {
bcheck = false;
}
for (i=0; i<checks.length; i++) {
MM_findObj('aselect_' + checks[i].value).checked = bcheck ;
highlightAssetToggleII(document.getElementById('aselect_' + checks[i].value),checks[i].value,true); 
}
}
}
//setup form switch for delete or multi asset zip download
function DownloadAssetsThumbs(ids){
var arraylist;
var i;
var sAssets;
var downloadtype;
arraylist = '';
ids = ids.replace('0 ','');
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
for (i=0; i<checks.length; i++) {
if (MM_findObj('aselect_' + checks[i].value).checked) {
arraylist = arraylist + ',' +  checks[i].value;
}
}
}
//alert(arraylist + arraylist.split(',').length)
if (arraylist.split(',').length > 2) {
 if (confirm("Download selected assets?"))
 {
arraylist= arraylist.substring(1, arraylist.length);
downloadtype = 'multi';
 }
 else
 {
/*download single*/
downloadtype = 'single';

 }
}
else {
/*download single*/
downloadtype = 'single';
}
if (downloadtype == 'multi') {
if (arraylist != ''){
<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('multi,'+arraylist);
}
}
if (downloadtype == 'single') {
/*assume single*/
<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('single,'+ids);
}
}



function CallbackTHUMBLIST_onCallbackComplete()
{
try
{
var p = $('.slide-out-div');
var position = p.position();
sizeCenterPane();
var tagpos = ($(window).width() - position.left);
if (tagpos > 50) {
<%response.write(CallbackAssetFiltersNew.ClientID)%>.Callback('forcebuild');
}
} catch(x) {}
}


  function onDeleteAsset(item)
  {

      //if (confirm("Delete this asset?"))
        return true; 
      //else
      //  return false; 

  }
  function deleteRow(ids)
  {
  <% if blnLiveProject then %>
alert('You are not allowed to delete from a Live Location using IDAM.  Please open the location using the menuitem for a folder or asset and then delete directly from the source.  Deletion(s) occuring at the source will be reflected on the next scheduled refresh.  Usually within 5 minutes.');;
return;
<%end if %>
  //check to see if multi select
var sAssets;
var addtype;
<%If bThumbnailView Then%>
    var arraylist;
var i;
arraylist = '';
idstmp = ids.replace('0 ','');
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
for (i=0; i<checks.length; i++) {
if (MM_findObj('aselect_' + checks[i].value).checked) {
arraylist = arraylist + ',' +  checks[i].value;
}
}
}
//alert(arraylist + arraylist.split(',').length)
if (arraylist.split(',').length > 2) {
 if (confirm("Add selected assets to carousel?"))
 {
arraylist= arraylist.substring(1, arraylist.length);
addtype = 'multi';
 }
 else
 {
/*download single*/
addtype = 'single';

 }
}
else {
/*download single*/
addtype = 'single';
}
    <%else%>
    sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
var arraylist;
var i;
arraylist = '';
if (sAssets.length > 1) {
 if (confirm("Delete selected assets?"))
 {
/*download multi*/
for (i = 0; i < sAssets.length; i++) {
if (arraylist == ''){
if (sAssets[i]!=null){
arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
}
} else {
if (sAssets[i]!=null){
if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
}
}
}
}
arraylist= arraylist.substring(1, arraylist.length);
addtype = 'multi';
 }
 else
 {
/*download single*/
addtype = 'single';

 }
}
else {
/*download single*/
addtype = 'single';
}
    <%end if%>
//alert(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(ids));
if (addtype == 'multi') {
if (arraylist != ''){


//override filter call
<%response.write( GridAssets.ClientId)%>.Filter("DELETE " + arraylist);
<%response.write( GridAssets.ClientId)%>.Page(0);
//return false;
}
}
if (addtype == 'single') {
/*assume single*/
      if (confirm("Delete this asset?")){
       <% Response.Write(GridAssets.ClientID) %>.Delete(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(ids));

        }
      else {
        }
} 
  }
function getNameValue()
  {

    return 'New Name';
  }

  function setNameValue(DataItem)
  {
    var tb = document.getElementById("<% Response.Write(GridAssets.ClientID) %>_EditTemplate_0_3_txtImageUrl");
   tb.value = DataItem;
 return true;
  }
</script>

<script>
  function isdefined(variable)
  {
    return (typeof (window[variable]) == "undefined") ? false : true;
  }
  var timeoutDelay = 1400;
  var nb = "&nbsp;"; // non-breaking space
  function buildPager(grid, pagerSpan, First, Last)
  {
    var pager = "";
    var mid = Math.floor(pagerSpan / 2);
    var startPage = grid.PageCount <= pagerSpan ? 0 : Math.max(0, grid.CurrentPageIndex - mid);
    // adjust range for last few pages
    if (grid.PageCount > pagerSpan)
    {
      startPage = grid.CurrentPageIndex < (grid.PageCount - mid) ? startPage : grid.PageCount - pagerSpan;
    }

    var endPage = grid.PageCount <= pagerSpan ? grid.PageCount : Math.min(startPage + pagerSpan, grid.PageCount);

    if (grid.PageCount > pagerSpan && grid.CurrentPageIndex > mid)
    {
      pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(0);return false;\">&laquo; First</a>" + nb + "..." + nb;
      pager += "<a href=\"#\" onclick=\"" + grid.Id + ".PreviousPage();return false;\">&lt;</a>" + nb;
    }

    for (var page = startPage; page < endPage; page++)
    {
      var showPage = page + 1;
      if (page == grid.CurrentPageIndex)
      {
        pager += showPage + nb;
      }
      else 
      {
        pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(" + page + ");return false;\">" + showPage + "</a>" + nb;
      }
    }

    if (grid.PageCount > pagerSpan && grid.CurrentPageIndex < grid.PageCount - mid)
    {
      pager += "<a href=\"#\" onclick=\"" + grid.Id + ".NextPage();return false;\">></a>" + nb + "..." + nb;
      pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(" + (grid.PageCount - 1) + ");return false;\">Last &raquo;</a>" + nb;
    }

    return pager;
  }
  function buildPageXofY(grid, Page, of, items)
  {
    // Note: You can trap an empty Grid here to change the default "Page 1 of 0 (0 items)" text
    var pageXofY = Page + nb + "<b>" + (grid.CurrentPageIndex + 1) + "</b>" + nb + of + nb + "<b>" + (grid.PageCount) + "</b>" + nb + "(" + grid.RecordCount + nb + items + ")";

    return pageXofY;
  }
  function showCustomFooter()
  {
    var gridId = "<%response.write( GridAssets.ClientId)%>";
    if (isdefined(gridId))
    {
      var grid = <%response.write( GridAssets.ClientId)%>;

      var Page = "Page";
      var of = "of";
      var items = "items";

      var pagerSpan = 5; // should be at least 2
      var First = "First";
      var Last = "Last";
      var cssClass = "GridFooterText";

      var footer = buildPager(grid, pagerSpan, First, Last);
      document.getElementById("tdPager").innerHTML = "<div style='white-space:nowrap;display:inline;' class=\"" + cssClass + "\">" + footer + "</div>";

      footer = buildPageXofY(grid, Page, of, items);
      document.getElementById("tdIndex").innerHTML = "<div class=\"" + cssClass + "\">" + footer + "</div>";
    }
    else 
    {
      setTimeout("showCustomFooter();", timeoutDelay);
    }
  }
  function onPage(newPage)
  {
    // delay call so that Grid's client properties have their new values
    setTimeout("showCustomFooter();",timeoutDelay);
    return true;
  }
  function onLoad()
  {
    showCustomFooter();
  }

function Project_GridAssets_onSelect(){
  var sAssets;
  if (getLastView() == 'thumb')
  {
		var arraylist;
		var i;
		arraylist = '';
		if (MM_findObj('selectassethidden').length > 1) { 
			var checks = MM_findObj('selectassethidden')
			for (i=0; i<checks.length; i++) {
				if (MM_findObj('aselect_' + checks[i].value).checked) {
					arraylist = arraylist + ',' +  checks[i].value;
				}
			}
		}
  		if (arraylist.split(',').length > 1) {
			arraylist= arraylist.substring(1, arraylist.length);
			IDAMTagids=arraylist;
			 if (DialogTagging.get_isShowing()==true){
            SNAPTAGGINGCallbackTags.Callback('View|'+arraylist);SNAPTAGGINGCallbackServices.Callback('View|'+arraylist);;SNAPTAGGINGCallbackMedia.Callback('View|'+arraylist);SNAPTAGGINGCallbackIllust.Callback('View|'+arraylist);}
        } else {
        return true;
        }
  }else{
    sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
    var arraylist;
    var i;
    arraylist = '';
    if (sAssets.length > 0) {
		    /*download multi*/
		    for (i = 0; i < sAssets.length; i++) {
			    if (sAssets[i] != null) {
				    if (arraylist == ''){
					    arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
				    } else {
					    if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
						    arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					    }
				    }
			    }
		    }
		    arraylist= arraylist.substring(1, arraylist.length);
		    IDAMTagids=arraylist;
		     if (DialogTagging.get_isShowing()==true){
		    SNAPTAGGINGCallbackTags.Callback('View|'+arraylist);SNAPTAGGINGCallbackServices.Callback('View|'+arraylist);;SNAPTAGGINGCallbackMedia.Callback('View|'+arraylist);SNAPTAGGINGCallbackIllust.Callback('View|'+arraylist);}
    } else {
        return true;
    }
  
  }
}
  
  
  
  
  
  
</script>
<style type="text/css">
      
      .slide-out-div {
          padding: 20px;
          width: 250px;
          background: #fffffb;
          border: 1px solid #DFDFDF;
          z-index:1;
          height:100%;
      }   
      

      </style>
<!--TABLE JUNK-->
<table width=100% cellpadding=0 cellspacing=0>
    <tr>
        <td width=100%>
            <div id="assetpopuplocation">
            </div>
        </td>
        <td>
            <div id="testmenucontrollocation">
            </div>
        </td>
    </tr>
</table>
<!--END TABLE JUNK-->
<!--TABLE TOP-->
<table width=100% cellpadding=0 cellspacing=0>
    <tr>
        <td width=100% align=left valign=top>
            <div id="projectcenter" class="projectcentertestpgxx">
                <div class="projectcentertestpgxx" >
                    <!--SNAPProjectAssets-->
                   



                          
                                    
                                    
                          
<div class="slide-out-div">
<a class="handle" onclick="javascript:LoadFiltersCallback();"></a>
            <h3></h3>
            <p>
            </p>
            
    <div id="filter_assets">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td width="120" align="left" nowrap>
                    <div style="padding-left: 4px; padding-bottom: 10px;">
                        <b>Search Within Results:</b></div>
                    <div id="search_input_filter">
                        <asp:TextBox ID="search_filter_asset" runat="server" onkeydown="javascript:if(event.keyCode==13) {DoSearchFilter('filter',this.value); return false}"
                            type="text" value="" name="search_filter_asset"></asp:TextBox>
                        <a href="#" style="border:0px;"><img style="border:0px;" height="19" src="images/search_input_button.jpg" width="35" onclick="javascript:DoSearchFilter('filter',document.getElementById('ctl04_search_filter_asset').value); return false;"></a></div>
                </td>
            </tr>
        </table>
        <br />
        <div style="padding-bottom: 5px;">
            <table width="100%">
                <tr>
                    <td>
                        <b>iDAM Filters:</b>
                    </td>
                    <td align="right" class="assetfilternewaddfilter">
                        <div id="assetfilternewaddfilter_clear">
                            [ <a href="#">clear</a> ]</div>
                    </td>
                </tr>
            </table>
        </div>
       
       
       
            
       <div id="assetfilterscrollbars" style="overflow-x: hidden;overflow-y:auto;height:300px;">
       
       
       
                                    <ComponentArt:CallBack ID="CallbackAssetFiltersNew" runat="server" CacheContent="false" >
                                        <ClientEvents>
                                        </ClientEvents>
                                        <Content><br />
                                        &nbsp;&nbsp;&nbsp;No filters available.  
                                            <asp:Literal ID="LiteralAssetFiltersNew" Text="" runat="server" />
                                        </Content>
                                        
                                    </ComponentArt:CallBack>
                                    <div id="dialogAssetFiltersNew" title="Dialog Title" >

                                        <ComponentArt:CallBack ID="CallbackAssetFiltersContent" runat="server" CacheContent="false">
                                            <ClientEvents>
                                            </ClientEvents>
                                            <Content>
                                            <asp:PlaceHolder runat="server" ID="PLFiltersContentHeading" >
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div id="search_input_dialog_filter">
                                                          
                                                                <asp:Literal ID="LiteralAssetFiltersContentTextObject" Text="" runat="server"></asp:Literal>
                                                          
                                                                <input type="text" id="assetfiltertextfilter" onkeydown="javascript:if(event.keyCode==13) {CallbackAssetFiltersContent.Callback(filterobjecttype,'textfilter',this.value); return false}"
                                                                    name="assetfiltertextfilter" />
                                                                <a href="#" style="border:0px;">
                                                                    <img style="border:0px;" height="19" src="images/search_input_button.jpg" width="35" onclick="javascript:CallbackAssetFiltersContent.Callback(filterobjecttype,'textfilter',document.getElementById('assetfiltertextfilter').value); return false;"></a></div>
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:PlaceHolder>
                                                                               
                                            
                                                <asp:Literal ID="LiteralAssetFiltersContent" Text="" runat="server"></asp:Literal>
                                            </Content>
                                           
                                        </ComponentArt:CallBack>
                                    </div>
                                    
                                    
                                    </div> <!--scrollbars-->

    </div>
</div>
   

<script>
function executeViewCallback(view)
{
if (view=='list') {
document.getElementById('assetGirdPagingHeading').style.display='block';
}else{
document.getElementById('assetGirdPagingHeading').style.display='none';
}
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(view);
}
</script>

<style>
div.thumbnailbox {float:left!important; width:220px!important; width:180px; height:320px!important; height:220px;}
div.thumbnailboxsc {width:170px;height:220px;}
div.asset_name      { width:164px; overflow: hidden;padding-left:5px;
text-overflow-mode:ellipsis;font-size:8px }
div.asset_date      { width:120px; overflow: hidden;padding-left:5px;
text-overflow-mode:ellipsis;font-size:8px }  
div.asset_filetype  { width:80px; overflow: hidden;
text-overflow-mode:ellipsis;font-size:8px }                  
div.asset_filesize  { width:60px; overflow: hidden;
text-overflow-mode:ellipsis;font-size:8px }    
div.asset_owner     { width:120px; overflow: hidden;padding-left:5px;
text-overflow-mode:ellipsis;font-size:8px }    
div.asset_links    {width:100%;padding-left:2px; height:4px;overflow:hidden;
                    
                    border-top: 1px dashed ;/*the border*/
height: 1px;/*whatever the total width of the border-top and border-bottom equal*/
                    }                                                                           
span.nowrap       { white-space : nowrap; }
div.attributed-to { position: relative;left:8px }
</style>


                  
<script>
    $(function () {
        $("#JQPAx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQPAx', $("#JQPAx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQPAx') == "false") {
            $("#JQPAx").accordion("option", "active", false);
        }
    });
    

function loadProjectContextMenu(evt, id) {
loadContextMenu(evt, id, <%=GridAssets.ClientID%>);
}
</script>

 
<div id="JQPAx" style="padding-bottom:5px;"> 
	<h3><a href="#section1">Project Assets</a></h3> 
                            <div class="SnapProjectsWrapperxx">
                                <div class="SnapProjectsxx">
                                    <img src="images/spacer.gif" width="188" height="<%if Request.Browser.Browser = "IE" then
response.write ("1")
else
response.write ("3")
end if%>">
                                    <div class="assetOptionsx" style="padding-top: 2px; padding-bottom: 2px;margin-top:4px;">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <b>Current Folder:</b>&nbsp;&nbsp;<%response.write (spFolderName)%>&nbsp;&nbsp;&nbsp;
                                                </td>
                                                <td>
                                                    <input type=hidden name=chksearchsubfoldersaction id=chksearchsubfoldersaction>[
                                                </td>
                                                <td>
                                                    <input type=checkbox name=chksearchsubfolders id=chksearchsubfolders onclick="javascript:setSearchWithinCheckValueFromCheckbox();document.PageForm.submit();" <%if blnpchksearchsubfolders then response.write ("CHECKED")%>>
                                                </td>
                                                <td valign="middle">
                                                    <a onclick="javascript:setSearchWithinCheckValue();document.PageForm.submit();" href="#">search subfolders</a> ]<%If WebArchives.iDAM.Web.Core.IDAMQueryService.CheckProjectAndFolderOverride(Request.QueryString("id"), WebArchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.userID, "UPLOAD", WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("UPLOAD"), Request.QueryString("c")) Or WebArchives.iDAM.Web.Core.IDAMQueryService.CheckProjectAndFolderOverride(Request.QueryString("id"), WebArchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.userID, "IDAM_ADDASSETS", WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("IDAM_ADDASSETS"), Request.QueryString("c")) Then%>&nbsp;[ <a href="javascript:Object_PopUp_Upload('<%response.write (request.querystring("id"))%>','<%response.write (request.querystring("c"))%>');"><b>upload here!</b></a> ]<%end if%>
                                                    <%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("CREATE_NEW_PROJECT", spWOType, spWOID, spProjectID) And Not blnLiveProject Then%>&nbsp;[ <a href="javascript:ShowDialogWindowX(850,850,800,'ProjectFolderEdit.aspx?parent_id=<%response.write (spFolderParentID)%>&action=new' + '&x=' + urlhelper(),'Edit/Create Project Folder','contentCssFolder','iFrameCss');">create project folder</a> ]<%end if%><%if WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("CREATE_NEW_PROJECT",spWOType,spWOID,spProjectID) and request.querystring("c") <> "" then%>&nbsp;[ <a href="javascript:ShowDialogWindowX(850,850,800,'ProjectFolderEdit.aspx?id=<%response.write (spFolderParentID)%>&action=edit' + '&x=' + urlhelper(),'Edit/Create Project Folder','contentCssFolder','iFrameCss');">edit project folder</a> ]<%end if%>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <img src="images/spacer.gif" width=1 height=10><br>
                                    <asp:Literal ID="LiteralNoFilters" Text="No filters available" Visible=false runat="server" />
                                    <!--SNAPProjectAssetsFiltersDock-->
                                    <!-- removed-->
                                    
                                    
                                    
                                    
                                    <!--SNAPProjectAssetsFiltersDock-->
                                    <div class="SnapHeaderViewIcons" style="display: block;height:27px; padding-top:4px; vertical-align:middle;">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr valign=top>
                                                <td width=14>
                                                    <div style="padding: 4px;">
                                                        View</div>
                                                </td>
                                                <td width=14>
                                                    <div style="padding-top: 4px; padding-bottom: 4px;">
                                                        <a href="#">
                                                            <asp:Image onclick="javascript:switchView('list');" ID="list_icon_GridAssets" runat="server"></asp:Image></a></div>
                                                </td>
                                                <td width=25>
                                                    <div style="padding: 4px;">
                                                        |</div>
                                                </td>
                                                <td width=14>
                                                    <div style="padding-top: 4px; padding-bottom: 4px;">
                                                        <a href="#">
                                                            <asp:Image onclick="javascript:switchView('thumb');" ID="thumb_icon_GridAssets" runat="server"></asp:Image></a></div>
                                                </td>
                                                <td width=100% nowrap align="right" valign="middle" style="padding-right: 8px;">
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                [ page size <a id="pagesize20" href="javascript:setPageSize(20);">20</a> <a id="pagesize50" href="javascript:setPageSize(50);">50</a> <a id="pagesize100" href="javascript:setPageSize(100);">100</a> ] [
                                                                <asp:HyperLink ID=CheckAllItemsJavascriptFunction runat="server">select all</asp:HyperLink>
                                                                ]<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("EDIT_ASSETS",spWOType,spWOID,spProjectID)   Then%>&nbsp;[ <a href="javascript:TagAssets();">tag assets</a>&nbsp;or&nbsp;<a href="javascript:showqtagging();">Qtag</a> ]<%else %><%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("EDIT_QTAGS") Then%>[ <a href="javascript:showqtagging();">Qtag</a> ]<%End If %><%end if%>&nbsp;[ <a href="javascript:ShowSlideShowResults();">slideshow</a> ]
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                   
                                    <div class="assetGirdPagingHeading" id="assetGirdPagingHeading" style="display: block;">
                                        <div id="CustomFooter" style="width: 100%; border-top: none;">
                                            <table width="100%">
                                                <tr>
                                                    <td id="tdPager" align="left">
                                                    </td>
                                                    <td id="tdIndex" align="right">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                  

                                    <ComponentArt:CallBack ID="CallbackTHUMBLIST" runat="server" CacheContent="false">
                                        <ClientEvents>
                                            <CallbackError EventHandler="GenericCallback_onCallbackError" />
                                            <CallbackComplete EventHandler="CallbackTHUMBLIST_onCallbackComplete" />
                                        </ClientEvents>
                                        <Content>
                                            <COMPONENTART:GRID 
id="GridAssets" 
runat="server" 
ItemDraggingEnabled="True"
externalDropTargets="ctl11_TreeView1"
ItemDraggingClientTemplateId="ItemDraggingTemplate"
AutoFocusSearchBox="false"
AutoCallBackOnInsert="true"
AutoCallBackOnUpdate="true"
AutoCallBackOnDelete="true"
ClientSideOnPage="onPage"
ClientSideOnLoad="onLoad"
pagerposition="2"
ScrollBar="Off"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ClientSideOnInsert="onInsert"
ClientSideOnUpdate="onUpdate"
ClientSideOnDelete="onDeleteAsset"
ClientSideOnCallbackError="onCallbackError"
ScrollPopupClientTemplateId="ScrollPopupTemplate" 
Height="10" Width="100%"
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
LoadingPanelFadeDuration="200"
LoadingPanelFadeMaximumOpacity="95"
LoadingPanelEnabled="true"
EnableViewState="true"
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRowCategory" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="20" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="True" 
GroupingPageSize="5" 
PagerTextCssClass="GridFooterTextCategory" 
PagerStyle="Numbered" 
PageSize="20" 
ManualPaging="true"
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeader" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="true" 
AllowSorting="False"
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
ScrollBarWidth="15" 
allowtextselection="false"
AllowPaging="true" >
<ClientEvents>
    <ItemExternalDrop EventHandler="GridAssets_onItemExternalDrop" />
    <CallbackComplete EventHandler="GridAssets_onCallbackComplete" />
    <ItemSelect EventHandler="Project_GridAssets_onSelect" />
    <ItemUnSelect EventHandler="Project_GridAssets_onSelect" /> 
  </ClientEvents>
<ClientTemplates>
<ComponentArt:ClientTemplate Id="ItemDraggingTemplate">
        <table width=10><tr><td valign=middle><img src="images/assetdragicon.gif" border=0 ></td><td valign=middle nowrap  ><div style="width:100px;height:20px;border:single solid black; font-family: Verdana; font-weight:bold;font-size:8pt; text-align :center;vertical-align:middle;">## ctl14_ctl00_GridAssets.getSelectedItems().length < 2 ? 'One item' : ctl14_ctl00_GridAssets.getSelectedItems().length + ' items' ## selected. </div></td></tr></table>
      </ComponentArt:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplate">
<img src="images/13Save2.gif" border=0  onClick="loadProjectContextMenu(event, '## DataItem.ClientId ##');"> | <a href="javascript:AddToCarouselFromPG('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border=0></a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
</ComponentArt:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateNoDownload">
<img src="images/13Save2.gif" border=0  onClick="loadProjectContextMenu(event, '## DataItem.ClientId ##');"> | <a href="javascript:AddToCarouselFromPG('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border=0></a>
</ComponentArt:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditCommandTemplate">
<a href="javascript:editRow();">Update</a> 
</ComponentArt:ClientTemplate>
<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
<a href="javascript:insertRow();">Insert</a> 
</ComponentArt:ClientTemplate>    
<ComponentArt:ClientTemplate Id="TypeIconTemplate">
<img src="## getURLPreviewIcon() ####DataItem.GetMember("imagesource").Value ##" border="0"  > 
</ComponentArt:ClientTemplate>  
<ComponentArt:ClientTemplate Id="TypeFolderTemplate">
<a href="IDAM.aspx?page=Project&id=## DataItem.GetMember("projectid").Value ##&type=project&c=## DataItem.GetMember("category_id").Value ##">## DataItem.GetMember("categoryname").Value ##</a> 
</ComponentArt:ClientTemplate>  
<ComponentArt:ClientTemplate Id="FileSizeTemplate">
## getIDAMGridSizeFormat(DataItem.GetMember("filesize").Value) ##
</ComponentArt:ClientTemplate>  
<ComponentArt:ClientTemplate Id="LookupProjectTemplate">
 <div id="## DataItem.GetMember("asset_id").Value ##" class="asset_image">
<table border=0 cellpadding=0 cellspacing=0 height="## getIDAMGridThumbnailHeight() ##"><tr><td valign=middle align=center height=## getIDAMGridThumbnailHeight() ##>
<A href="## getALink() #### DataItem.GetMember("asset_id").Value ##"><img border=0 src="## getURLPreview() #### DataItem.GetMember("asset_id").Value ##"></a></td></tr></table>
</div>
</ComponentArt:ClientTemplate> 
<ComponentArt:ClientTemplate Id="LookupAssetPopupTemplate">
<A href="#" onclick="javascript:PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.ClientId ##',event);"><img border=0 src="images/15printPreview.gif"></a>
</ComponentArt:ClientTemplate> 
<ComponentArt:ClientTemplate Id="LookupProjectTemplatePreview">
<A href="javascript:AssetPreview('## DataItem.ClientId ##');"><img border=0 src="images/15printPreview.gif"></a>
</ComponentArt:ClientTemplate>           
<ComponentArt:ClientTemplate Id="LookupCategoryTemplate">
<A href="javascript:GotoLocation('## DataItem.GetMember("ispost").Value ##','## DataItem.GetMember("category_id").Value ##');">## DataItem.GetMember("categoryname").Value ##</a>
</ComponentArt:ClientTemplate>    
<ComponentArt:ClientTemplate Id="LookupCategoryIconTemplate">
<A href="javascript:GotoLocation('## DataItem.GetMember("ispost").Value ##','## DataItem.GetMember("category_id").Value ##');"><img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif"></a>
</ComponentArt:ClientTemplate>    
<componentart:ClientTemplate ID="LoadingFeedbackTemplate"> 
<table height="1px" width="100%" ><tr><td valign="center" align="center">
<table cellspacing="0" cellpadding="0" border="0"  background="images/loadingback.gif" height="70px" width="300px">
<tr>
<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
<td style="font-size:10px;font-family:Verdana;" width="60px" align="right">Loading...&nbsp;</td>
<td><img src="images/spinner2.gif"  border="0"></td>
<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
</tr>
</table>
</td></tr></table>
</componentart:ClientTemplate>                                 
</ClientTemplates>


<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
EditFieldCssClass="EditDataField"
EditCommandClientTemplateId="EditCommandTemplate"
InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asch10.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="desch10.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="10" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>


<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" Width="65" Align="Center" TextWrap="true" AllowSorting="false" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplate" HeadingCellCssClass="FirstHeadingCell" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" FixedWidth="True" />
<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" AllowSorting="false" dataField="imagesource"  DataCellCssClass="DataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="19" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="True" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false"  DataCellClientTemplateId="FileSizeTemplate" Width="70" HeadingText="Filesize" SortedDataCellCssClass="SortedDataCell" DataField="filesize"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Type" AllowEditing="false" Width="40" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Updated" AllowEditing="false" Width="110" DataType="System.DateTime" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Security Level" AllowEditing="true" Width="80"  SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="User" AllowEditing="false"  SortedDataCellCssClass="SortedDataCell" Width="90"  DataField="FullName"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="False" DataCellClientTemplateId="LookupCategoryIconTemplate" HeadingText=" " HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" Align="Right" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="ispost" Width="19" FixedWidth="True"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="False" HeadingText="Folder" DataCellClientTemplateId="TypeFolderTemplate"  AllowGrouping="True" SortedDataCellCssClass="SortedDataCell" Width="144"  DataField="categoryname" ></componentart:GridColumn>
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowEditing="false" AllowSorting="false" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Width="150"  Align="Center" />
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" Visible="False" HeadingText="Action" AllowSorting="false" AllowEditing="false" DataCellClientTemplateId="EditTemplateNoDownload" EditControlType="EditCommand" Width="150"  Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="category_id" SortedDataCellCssClass="SortedDataCell" DataField="category_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="ispost" SortedDataCellCssClass="SortedDataCell" DataField="ispost"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="timage" SortedDataCellCssClass="SortedDataCell" DataField="timage"></componentart:GridColumn>

</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>




<div style="padding:10px;"><asp:Literal id="literalNoAssets" Text="No assets available" visible=false runat="server" /></div>



<COMPONENTART:CALLBACK id="ThumbnailCallBack" runat="server" CacheContent="false">
<ClientEvents>
<CallbackError EventHandler="GenericCallback_onCallbackError" />
</ClientEvents>
<CONTENT>
<!--hidden Thumbs-->
<asp:Literal id="ltrlPager" Text="" runat="server" />
<asp:Repeater id=Thumbnails runat="server" Visible="True">
<ItemTemplate>
<div valign="Top"  class="thumbnailbox" > 
<div class="thumbnailboxsc"><!--Asset Thumbnails-->
<div align="left"><!--PADDING LEFT X1-->
<div align="left"><!--PADDING LEFT X2-->
<table  class="bluelightlight" border="0" cellspacing="0" width="15">
<tr>
<td  class="bluelightlight" valign="top">
<div align="left" id="assethighlight_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" >
<table border="0" width="100%"   style="border-collapse: collapse"><tr><td>
<table border="0" width="100%"   style="border-collapse: collapse" height="120" width="160">
<tr class="bluelightlight" width="100%" align="center" height="120" width="160"><td class="bluelightlight" width="100%" height="120" width="160" align="center" valign="bottom"  nowrap><img src="images\spacer.gif" height="0" width="159"><br><a id="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" class="asset_image" onclick="javascript:highlightAssetToggleII(document.getElementById('aselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>'),<%#DataBinder.Eval(Container.DataItem, "asset_id")%>,false);"><img alt="" border="0" src="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?instance=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMInstance%>&type=asset&size=2&height=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.ConfigurationService.GetConfigurationSetting(WebArchives.iDAM.Services.ConfigurationService.WebAdminConfigKey.IDAMThumbnailHeight)%>&width=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.ConfigurationService.GetConfigurationSetting(WebArchives.iDAM.Services.ConfigurationService.WebAdminConfigKey.IDAMThumbnailWidth)%>&qfactor=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMQfactor%>&id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" ></a></td><td><img src="images\spacer.gif" width="0" height="140"></td>
</tr>
</table></td></tr></table>
</div>
</td>
</tr>
<tr>
<td>      
<!--PADDING-->
<div style="padding:5px;">             
<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
<td nowrap width="5"><img src="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveIconURLBase%>?instance=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMInstance%>&size=0&id=<%#DataBinder.Eval(Container.DataItem, "imagesource")%>"></td>
<td nowrap><div class="asset_name"><span class="nowrap"><b><font face="Verdana" size="1"><%#DataBinder.Eval(Container.DataItem, "name")%></font></b></span></div></td>
<td align="right" width="20"><font color="#808080" face="Arial" size="1"><input onclick="highlightAssetToggle(this,<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);" class="preferences" type="checkbox" id="aselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" value="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"><input type=hidden id=selectassethidden value="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"></font></td>
</tr>
</table>
<table width="100%"  cellspacing="0" cellpadding="0" >
<tr>
<td nowrap width="100%" align="left"><div class="asset_filetype"><span class="nowrap"><font face="Verdana" size="1"><%# DataBinder.Eval(Container.DataItem, "media_type_name")%></font></span></div></td>
<td align="right"><div class="asset_filesize"><span class="nowrap"><font face="Verdana" size="1"><%# FormatSize(CType(DataBinder.Eval(Container.DataItem, "filesize"), Long))%></font></span></div></td>
</tr>
</table>
<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
<td nowrap><font face="Verdana" size="1">Updated</font></td>
<td align="left" width="868"><div class="asset_date"><span class="nowrap"><font face="Verdana" size="1">

<%# String.Format("{0:MM/dd/yy hh:mm tt}", (DataBinder.Eval(Container.DataItem, "update_date")))%></font></span></div></td>

</tr>
<tr>
<td nowrap><font face="Verdana" size="1">Created By</font></td>
<td align="left" width="868"><div class="asset_owner"><span class="nowrap"><font face="Verdana" size="1"><%# DataBinder.Eval(Container.DataItem, "fullname")%></font></span></div></td>
</tr>
<tr>
<td nowrap><font face="Verdana" size="1">Project</font></td>
<td align="left" width="868"><div class="asset_owner"><span class="nowrap"><font face="Verdana" size="1"><%# DataBinder.Eval(Container.DataItem, "projectname")%></font></span></div></td>
</tr>
</table>
<div style="padding-top:3px;">
<div class="asset_links"></div>
</div>
<table width=100%><tr><td>
<b><font face="Verdana" size="1"><a href="IDAM.aspx?page=Asset&ID=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=asset">View Details</a></font><br>
<!--<font face="Verdana" size="1"><a href="#" onclick="javascript:PreviewOverlayOnThumbSingleClickAssets('<%#DataBinder.Eval(Container.DataItem, "asset_id")%>','<%# GetAssetProjectName(CType(DataBinder.Eval(Container.DataItem, "projectid"), String))%>','<%#DataBinder.Eval(Container.DataItem, "imagesource")%>',event);">Preview</a></font><br>-->
<font face="Verdana" size="1"><a onclick="loadContextMenuThumbs(event, '0 <%#DataBinder.Eval(Container.DataItem, "asset_id")%>');">Download</a></font><br>
<font face="Verdana" size="1"><a href="javascript:AddToCarouselFromPG('0 <%#DataBinder.Eval(Container.DataItem, "asset_id")%>');">Add to Carousel</a></font><br></b>    
</td><td align=right valign=top>
<b><font face="Verdana" size="1"><a href="javascript:DeleteSelected('0 <%#DataBinder.Eval(Container.DataItem, "asset_id")%>');">Delete</a></font></b>
</td></tr></table>
       
</div>                        
<!--END PADDING-->
                        
</td>
</tr>
</table>
</div><!--END PADDING LEFT X2-->
<img border="0" src="images/spacer.gif" width="135" height="8">
</div><!--END PADDING LEFT X1-->
</div><!--end Asset Thumbnail span-->
</div><!--end inner span-->
</ItemTemplate>

<HeaderTemplate>
<!--Start Thumbnail Float-->
<table width=100%><tr><td>
<div valign="Bottom" style="float:left!important;">       
</HeaderTemplate>

<FooterTemplate>
</td></tr></table>
</FooterTemplate> 

</asp:Repeater>
<asp:Literal id="ltrlPagingBottom" Text="" runat="server" />
</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
<TABLE height="300" cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD align="center">
<TABLE cellSpacing="0" cellPadding="0" border="0">
<TR>
<TD style="FONT-SIZE: 10px">Loading...
</TD>
<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
</TR>
</TABLE>
</TD>
</TR>
</TABLE>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>







</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
<TABLE height="300" cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD align="center">
<TABLE cellSpacing="0" cellPadding="0" border="0">
<TR>
<TD style="FONT-SIZE: 10px">Loading...
</TD>
<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
</TR>
</TABLE>
</TD>
</TR>
</TABLE>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>








</div><!--SnapProjects-->
</div><!--padding-->
</div><!--END-->


<!--SNAP OVERLAY WAS HERE-->

<!--END SnapProjectAssetOverlay-->




















<script type="text/javascript">

</script>

<div class="SnapHeaderProjectsFiller" ></div>
</div><!--bottom padding-->
</div><!--projectcenter-->


<script>
    $(function () {
        $("#JQPCx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQPCx', $("#JQPCx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQPCx') == "false") {
            $("#JQPCx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQPCx" style="padding-bottom:5px;"> 
	<h3><a href="#section1">Project Contact</a></h3> 
<div >
<div class="SnapProjectsxxx" >
<img src="images/spacer.gif" width=188 height=1>
<table cellpadding=0 cellspacing=0 ><tr><td align=right>
<a href="#" onclick="deleteContacts();">delete</a> | <a href="#" onclick="addContacts();">add</a><br><br>
</td></tr></table>
<table><tr><td>
<asp:Literal id="LiteralNoContacts" Text="No contacts added." visible=false runat="server" />
</td></tr>
</table>
<table cellpadding=0 cellspacing=0 ><tr><td>
<COMPONENTART:CALLBACK id="CallbackTreeViewContacts" runat="server" CacheContent="false" >
<ClientEvents>
<CallbackError EventHandler="GenericCallback_onCallbackError" />
</ClientEvents>
<CONTENT>

<asp:Literal id="LiteralAddContacts" Text="" visible=true runat="server" />


</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
<TABLE cellSpacing="0" cellPadding="0" border="0">
<TR>
<TD align="center">
<TABLE cellSpacing="0" cellPadding="0" border="0">
<TR>
<TD style="FONT-SIZE: 10px">Loading...
</TD>
<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
</TR>
</TABLE>
</TD>
</TR>
</TABLE>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>
</td></tr>
</table>

<asp:Literal id="LiteralTVCStart" Text="" visible=false runat="server" />
<table cellpadding=0 cellspacing=0 ><tr><td>
<ComponentArt:TreeView id="TreeviewContacts" Height="100%" Width="188px" AutoCallBackOnNodeMove="True" DragAndDropEnabled="false"
NodeEditingEnabled="false" KeyboardEnabled="false" CssClass="TreeView" NodeCssClass="TreeNode" SelectedNodeCssClass="SelectedTreeNode"
HoverNodeCssClass="HoverTreeNodeContacts" NodeEditCssClass="NodeEdit" LineImageWidth="19" LineImageHeight="20"
DefaultImageWidth="16" DefaultImageHeight="16" ExpandSinglePath="true"  NodeLabelPadding="3" ParentNodeImageUrl="images/folders.gif"
LeafNodeImageUrl="images/folder.gif" ShowLines="false" LineImagesFolderUrl="images/lines/" EnableViewState="true"         ExpandCollapseImageWidth="15" 
ExpandCollapseImageHeight="15"          NodeIndent="16"         SpacerImageUrl="images/spacer.gif"         CollapseImageUrl="images/exp.gif"         ExpandImageUrl="images/col.gif"         ItemSpacing="3" 
runat="server">
<ServerTemplates>
         
<ComponentArt:NavigationCustomTemplate id="ContactInfoTemplate">
<Template>
<table width=188><tr><td>
First Name: <%# Container.Attributes("FirstName") %><br>
Last Name: <%# Container.Attributes("LastName") %><br>
Email: <a href="mailto:<%# Container.Attributes("Email") %>"><%# Container.Attributes("Email") %></a><br>
Agency: <%# Container.Attributes("Agency") %><br>
Phone: <%# Container.Attributes("Phone") %><br>
Address: <%# Container.Attributes("workAddress") %><br>
City: <%# Container.Attributes("workCity") %><br>
State: <%# Container.Attributes("workState") %><br>
Zip: <%# Container.Attributes("workZip") %><br>
Cell: <%# Container.Attributes("Cell") %><br>
Department: <%# Container.Attributes("Department") %><br>
</td></tr></table>

</Template>
</ComponentArt:NavigationCustomTemplate>
          
</ServerTemplates>  

</ComponentArt:TreeView>
</td></tr>
</table><br>
<asp:Literal id="LiteralTVCEnd" Text="" visible=false runat="server" />

</div><!--SnapProjects-->
</div><!--padding-->
</div><!--END SNAPProjectContacts-->

<!--SNAPProjectVendors-->






<script>
    $(function () {
        $("#JQPVx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQPVx', $("#JQPVx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQPVx') == "false") {
            $("#JQPVx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQPVx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Project Vendors</a></h3> 
<div >
<div class="SnapProjectsxx" >
<img src="images/spacer.gif" width=188 height=1>
<table cellpadding=0 cellspacing=0 ><tr><td align=right>
<a href="#" onclick="deleteVendors();">delete</a> | <a href="#" onclick="addVendors();">add</a><br><br>
</td></tr></table>
<table><tr><td>
<asp:Literal id="LiteralNoVendors" Text="No vendors added." visible=false runat="server" />
</td></tr></table>
<table cellpadding=0 cellspacing=0 ><tr><td>
<COMPONENTART:CALLBACK id="CallbackTreeViewVendors" runat="server" CacheContent="false" >
<ClientEvents>
<CallbackError EventHandler="GenericCallback_onCallbackError" />
</ClientEvents>
<CONTENT>

<asp:Literal id="LiteralAddVendors" Text="" visible=true runat="server" />


</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
<TABLE cellSpacing="0" cellPadding="0" border="0">
<TR>
<TD align="center">
<TABLE cellSpacing="0" cellPadding="0" border="0">
<TR>
<TD style="FONT-SIZE: 10px">Loading...
</TD>
<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
</TR>
</TABLE>
</TD>
</TR>
</TABLE>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>
</td></tr></table>


<asp:Literal id="LiteralTVVStart" Text="" visible=false runat="server" />
<table cellpadding=0 cellspacing=0 ><tr><td>
<ComponentArt:TreeView id="TreeviewVendors" Height="100%" Width="188px" AutoCallBackOnNodeMove="True" DragAndDropEnabled="false"
NodeEditingEnabled="false" KeyboardEnabled="false" CssClass="TreeView" NodeCssClass="TreeNode" SelectedNodeCssClass="SelectedTreeNode"
HoverNodeCssClass="HoverTreeNodeContacts" NodeEditCssClass="NodeEdit" LineImageWidth="19" LineImageHeight="20"
DefaultImageWidth="16" DefaultImageHeight="16" ExpandSinglePath="true"  NodeLabelPadding="3" ParentNodeImageUrl="images/folders.gif"
LeafNodeImageUrl="images/folder.gif" ShowLines="false" LineImagesFolderUrl="images/lines/" EnableViewState="true"         ExpandCollapseImageWidth="15" 
ExpandCollapseImageHeight="15"          NodeIndent="16"         SpacerImageUrl="images/spacer.gif"         CollapseImageUrl="images/exp.gif"         ExpandImageUrl="images/col.gif"         ItemSpacing="3" 
runat="server">
<ServerTemplates>
         
<ComponentArt:NavigationCustomTemplate id="VendorInfoTemplate">
<Template>
<table width=188><tr><td>
Name: <%# Container.Attributes("Name") %><br>
           
Email: <a href="mailto:<%# Container.Attributes("Email") %>"><%# Container.Attributes("Email") %></a><br>
           
Phone: <%# Container.Attributes("Phone") %><br>
Address: <%# Container.Attributes("Address1") %><br>
Address: <%# Container.Attributes("Address2") %><br>
Address: <%# Container.Attributes("Address3") %><br>
Address: <%# Container.Attributes("Address4") %><br>
City: <%# Container.Attributes("City") %><br>
State: <%# Container.Attributes("State") %><br>
Zip: <%# Container.Attributes("Zip") %><br>
</td></tr></table>

</Template>
</ComponentArt:NavigationCustomTemplate>
          
</ServerTemplates>  

</ComponentArt:TreeView>
</td></tr></table><br>
<asp:Literal id="LiteralTVVEnd" Text="" visible=false runat="server" />
</div>
</div><!--padding-->
</div><!--END SNAPProjectVendors-->



























<div class="SnapHeaderProjectsFiller"></div>
</div><!--bottom padding-->
</div><!--projectright-->
</td></tr></table>
<script language="javascript">
 
  function deleteContacts()
  {
  var treeViewNodes;
  treeViewNodes = '';
  for (i=0;i<40;i++)
  {
  if (<% Response.Write(TreeviewContacts.ClientID) %>.Nodes(i) != null){
  
if (<% Response.Write(TreeviewContacts.ClientID) %>.Nodes(i).Checked != undefined){
if (<% Response.Write(TreeviewContacts.ClientID) %>.Nodes(i).Checked) 
{
treeViewNodes += <% Response.Write(TreeviewContacts.ClientID) %>.Nodes(i).ID + ","
}
}
}
  }
if (treeViewNodes != '') {
var nodelist=treeViewNodes.split(",")
for (i=0;i<nodelist.length-1;i++)
{
    <% Response.Write(TreeviewContacts.ClientID) %>.FindNodeById(nodelist[i]).Remove();
}
<% Response.Write(CallbackTreeViewContacts.ClientID) %>.Callback(treeViewNodes);
}
<% Response.Write(TreeviewContacts.ClientID) %>.Render();
  }
  
  function addContacts()
  {
  <% Response.Write(CallbackTreeViewContacts.ClientID) %>.Callback('addcontacts');
  }
  
  function cancelAddContacts()
  {
  <% Response.Write(CallbackTreeViewContacts.ClientID) %>.Callback('canceladdcontacts');
  }
  
  function deleteVendors()
  {
  var treeViewNodes;
  treeViewNodes = '';
  for (i=0;i<40;i++)
  {
  if (<% Response.Write(TreeviewVendors.ClientID) %>.Nodes(i) != null){
  
if (<% Response.Write(TreeviewVendors.ClientID) %>.Nodes(i).Checked != undefined){
if (<% Response.Write(TreeviewVendors.ClientID) %>.Nodes(i).Checked) 
{
treeViewNodes += <% Response.Write(TreeviewVendors.ClientID) %>.Nodes(i).ID + ","
}
}
}
  }
if (treeViewNodes != '') {
var nodelist=treeViewNodes.split(",")
for (i=0;i<nodelist.length-1;i++)
{
    <% Response.Write(TreeviewVendors.ClientID) %>.FindNodeById(nodelist[i]).Remove();
}
<% Response.Write(CallbackTreeViewVendors.ClientID) %>.Callback(treeViewNodes);
}
<% Response.Write(TreeviewVendors.ClientID) %>.Render();
  }
  
  function addVendors()
  {

  <% Response.Write(CallbackTreeViewVendors.ClientID) %>.Callback('addvendors');
  }
  
  function cancelAddVendors()
  {
  <% Response.Write(CallbackTreeViewVendors.ClientID) %>.Callback('canceladdvendors');
  }

    function ThumbnailCallBackPage(page)
  {
    <% Response.Write(ThumbnailCallBack.ClientID) %>.Callback(page);
  }

  function highlightAssetToggle(checkbox,asset_id)
  {
  var highlightbox;
  highlightbox = document.getElementById('assethighlight_' + asset_id);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }
  if (DialogTagging.get_isShowing()==true){
  Project_GridAssets_onSelect();}}
  
  
  
  
  
  
function highlightAssetToggleII(checkbox,asset_id,keyoverride)
  {
var highlightbox;
highlightbox = document.getElementById('assethighlight_' + asset_id);
if (checkbox.checked) {
highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
checkbox.checked = false;

} else
{
var gAgent=navigator.userAgent.toLowerCase();
var gbFirefox=(gAgent.indexOf("firefox")!=-1);
if (!gbFirefox){
e=window.event||window.Event;

if (e) {
if((!e.ctrlKey)&&(!keyoverride)){
ClearAllItemsThumb();
}
}
}
highlightbox.style.border = '1px solid black';
highlightbox.style.backgroundColor = '#FFEEC2';
checkbox.checked = true;
}

if (DialogTagging.get_isShowing()==true) {
    Project_GridAssets_onSelect();
} 
}

  

</script>
<script language=javascript>
function setSearchWithinCheckValue()
{
if (document.getElementById('chksearchsubfolders').checked) 
{
document.getElementById('chksearchsubfolders').checked = false;
document.getElementById('chksearchsubfoldersaction').value = '0';
} else
{
document.getElementById('chksearchsubfolders').checked = true;
document.getElementById('chksearchsubfoldersaction').value = '1';
}
}


function setSearchWithinCheckValueFromCheckbox()
{
if (document.getElementById('chksearchsubfolders').checked) 
{
document.getElementById('chksearchsubfolders').checked = true;
document.getElementById('chksearchsubfoldersaction').value = '1';
} else
{
document.getElementById('chksearchsubfolders').checked = false;
document.getElementById('chksearchsubfoldersaction').value = '0';
}
}


function setSearchWithinCheckValueTrue()
{

document.getElementById('chksearchsubfolders').checked = true;
document.getElementById('chksearchsubfoldersaction').value = '1';

}



function AddToCarouselFromPG(ids)
  {
  //check to see if multi select
var sAssets;
var addtype;
if (getLastView() == 'thumb')
        {
    
    var arraylist;
var i;
arraylist = '';
ids = ids.replace('0 ','');
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
for (i=0; i<checks.length; i++) {
if (MM_findObj('aselect_' + checks[i].value).checked) {
arraylist = arraylist + ',' +  checks[i].value;
}
}
}
//alert(arraylist + arraylist.split(',').length)
if (arraylist.split(',').length > 2) {
 if (confirm("Add selected assets to carousel?"))
 {
arraylist= arraylist.substring(1, arraylist.length);
addtype = 'multi';
 }
 else
 {
/*download single*/
addtype = 'single';

 }
}
else {
/*download single*/
addtype = 'single';
}
       
    
    } else {
        
        
    sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
ids = ids.replace('0 ','');
var arraylist;
var i;
arraylist = '';
if (sAssets.length > 1) {
 if (confirm("Add selected assets to carousel?"))
 {
/*download multi*/
for (i = 0; i < sAssets.length; i++) {
  if (sAssets[i] != null) {
if (arraylist == ''){

arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
} else {
if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
}
}
  }
}

arraylist= arraylist.substring(1, arraylist.length);
addtype = 'multi';
 }
 else
 {
/*download single*/
addtype = 'single';

 }
}
else {
/*download single*/
addtype = 'single';
}
        

    }


if (addtype == 'multi') {
if (arraylist != ''){

carousel_callback.Callback('1,' + arraylist + ',AddMultiToCarousel');
}
}

if (addtype == 'single') {
/*assume single*/
carousel_callback.Callback('1,' + ids + ',AddToCarousel');
} 
  }
  

  
  
function TagAssets()
  {
  //check to see if multi select
	var sAssets;
	var addtype;
	if (getLastView() == 'thumb')
        {
    
		var arraylist;
		var i;
		arraylist = '';
		if (MM_findObj('selectassethidden').length > 1) { 
			var checks = MM_findObj('selectassethidden')
			for (i=0; i<checks.length; i++) {
				if (MM_findObj('aselect_' + checks[i].value).checked) {
					arraylist = arraylist + ',' +  checks[i].value;
				}
			}
		}
		if (arraylist.split(',').length > 1) {

			arraylist= arraylist.substring(1, arraylist.length);
			addtype = 'multi';

		} else {
			alert('Select an asset to tag.');
			return;
		}

    } else {

		sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
		var arraylist;
		var i;
		arraylist = '';
		if (sAssets.length > 0) {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (sAssets[i] != null) {
						if (arraylist == ''){
							arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
						} else {
							if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
								arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
							}
						}
					}
				}
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
		} else {
			alert('Select an asset to tag.');
			return;
		}
    }
	opentagwindow(arraylist);
  }
	  

  
  
  
  
function ShowSlideShowResults()
  {

  //check to see if multi select
	var sAssets;
	var addtype;
	if (getLastView() == 'thumb')
        {
    
		var arraylist;
		var i;
		arraylist = '';
		if (MM_findObj('selectassethidden').length > 1) { 
			var checks = MM_findObj('selectassethidden')
			for (i=0; i<checks.length; i++) {
				if (MM_findObj('aselect_' + checks[i].value).checked) {
					arraylist = arraylist + ',' +  checks[i].value;
				}
			}
		}
		if (arraylist.split(',').length > 1) {

			arraylist= arraylist.substring(1, arraylist.length);
			addtype = 'multi';

		} else {
		    if(GetCookie('IDAMSlideShowCoolIris')!='1'&&GetCookie('BrowserCheckSilverlight')!='True')
		    {
			    alert('Select asset(s) to preview in the slideshow.');
			} else {
			    ShowSlideShowUseSessionSql('usesessionsqlprojectasset');
			}
		    return;
		}

    } else {

		sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
		var arraylist;
		var i;
		arraylist = '';
		if (sAssets.length > 0) {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (sAssets[i] != null) {
						if (arraylist == ''){
							arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
						} else {
							if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
								arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
							}
						}
					}
				}
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
		} else {
			if(GetCookie('IDAMSlideShowCoolIris')!='1'&&GetCookie('BrowserCheckSilverlight')!='True')
		    {
		        alert('Select asset(s) to preview in the slideshow.');
		    } else {
		        ShowSlideShowUseSessionSql('usesessionsqlprojectasset');
		    }
		    return;
		}
    }
    
	ShowSlideShow(arraylist);
  }

  
  
  

function DeleteSelected(ids)
  {
  //check to see if multi select
var sAssets;
var addtype;
<% if blnLiveProject then %>
alert('You are not allowed to delete from a Live Location using IDAM.  Please open the location using the menuitem for a folder or asset and then delete directly from the source.  Deletion(s) occuring at the source will be reflected on the next scheduled refresh.  Usually within 5 minutes.');;
return;
<%end if %>
    
    var arraylist;
var i;
arraylist = '';
ids = ids.replace('0 ','');
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
for (i=0; i<checks.length; i++) {
if (MM_findObj('aselect_' + checks[i].value).checked) {
arraylist = arraylist + ',' +  checks[i].value;
}
}
}
//alert(arraylist + arraylist.split(',').length)
if (arraylist.split(',').length > 2) {
 if (confirm("Delete selected assets?"))
 {
arraylist= arraylist.substring(1, arraylist.length);
addtype = 'multi';
 }
 else
 {
    if (confirm("Delete individual asset?"))
    {
/*delete single*/
addtype = 'single';
} else {
/*delete nothing*/
addtype = 'donothing';
}
 }
}
else {
if (confirm("Delete this asset?"))
    {
/*delete single*/
addtype = 'single';
} else {
/*delete nothing*/
addtype = 'donothing';
}
}

if (addtype == 'multi') {
if (arraylist != ''){
<%response.write(ThumbnailCallBack.ClientID)%>.Callback('DeleteMulti,' + arraylist );
}
}

if (addtype == 'single') {
/*assume single*/
<%response.write(ThumbnailCallBack.ClientID)%>.Callback('DeleteSingle,' + ids );
} 
  }












function DoSearchFilter(stype,searchval)
{
if (stype=='search'){
ClearFilters();
SetCookie("sFilter", "");
SetCookie ('SearchValueProjectAssets', document.getElementById('quicksearchkeyword').value, exp);
firsttabfilterclick = true;
}
if (stype=='filter'){
SetCookie ('SearchValueProjectAssets', searchval, exp);
}
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView(),stype,searchval);
}



//function DoSearchFilter(searchvalue)
//{
//SetCookie ('SearchValueProjectAssets', searchvalue, exp);
//if (getLastView() == 'thumb')
        //{
        //<%response.write(ThumbnailCallBack.ClientID)%>.Callback('Search,' + searchvalue);
        //
        //}else{
        //<%response.write(GridAssets.ClientID)%>.Filter(searchvalue);
        //
        //}
//}


function CheckAllItemsAnyMode()
{
if (getLastView() == 'thumb')
        {
        CheckAllItemsThumbs();
        }else{
       CheckAllItems();
        }
}

</script>
<script language=javascript type="text/javascript">
function showtrailpreload(imageid,title,description,ratingaverage,ratingnumber,showthumb,height,filetype)
{
showtrail('<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?id=' + imageid + '&amp;instance=<%response.write (IDAMInstance)%>&amp;type=asset&amp;size=2&amp;height=399&amp;width=370',title,description,'5','1',270,7);
}

function closealltags()
{
    $("#JQPAx").accordion("option", "active", false);
    $("#JQPCx").accordion("option", "active", false);
    $("#JQPVx").accordion("option", "active", false);



    
}
function openalltags()
{
    $("#JQPAx").accordion("option", "active", 0);
    $("#JQPCx").accordion("option", "active", 0);
    $("#JQPVx").accordion("option", "active", 0);
} 


//initialize the callback controls - clear vars and execute initial callback
SetCookie ('SearchValueProjectAssets', '', exp);
SetCookie ('tFilter', '', exp);
SetCookie ('sFilter', '', exp);
SetCookie ('mFilter', '', exp);
SetCookie ('iFilter', '', exp);
SetCookie ('cFilter', '', exp);
SetCookie ('tagFilter', '', exp);
SetCookie ('pFilter', '', exp);

if (getLastView()!='thumb') {
document.getElementById('assetGirdPagingHeading').style.display='block';
}else{
document.getElementById('assetGirdPagingHeading').style.display='none';
}

if (navigator.userAgent.indexOf("Firefox")!=-1){
window.setTimeout('<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView()+","+"initialize")',500); 
}else{
//<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView()+","+"initialize");
window.setTimeout('<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView()+","+"initialize")',500);  
}
try
{

} catch(x) {}

function GridAssets_onItemExternalDrop(sender, eventArgs)
{

//check for multi move
  var draggedItem = eventArgs.get_item();
  var targetControl = eventArgs.get_targetControl();
  var target = eventArgs.get_target();
  sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();


var arraylist;
var i;
arraylist = '';
if (sAssets.length >= 1) {

/*download multi*/
for (i = 0; i < sAssets.length; i++) {
if (arraylist == ''){
try
{
arraylist = 'x' + sAssets[i].GetMember("asset_id").Value;
}
catch (err)
{}

} else {
try
{
if (arraylist.indexOf('x' +  sAssets[i].GetMember("asset_id").Value) == -1) {
arraylist = arraylist + 'x' +  sAssets[i].GetMember("asset_id").Value;
}
}
catch (err)
{}
}
}

arraylist= arraylist.substring(1, arraylist.length);
addtype = 'multi';

}
else {
/*download single*/
addtype = 'single';
}
if (addtype=='multi')
{
GridItemMoveSelected(arraylist,target);
}
else
{
  GridItemMove('ASS'+draggedItem.Id,target);
 }
  <%=GridAssets.ClientID%>.callback();
} 
  


function setPageSize(ps)
{
	SetCookie ('IDAMassetgridPageSize',ps, exp);
	<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
	document.getElementById('pagesize20').style.fontWeight='normal';
	document.getElementById('pagesize50').style.fontWeight='normal';
	document.getElementById('pagesize100').style.fontWeight='normal';
	if (ps==20){
	document.getElementById('pagesize20').style.fontWeight='bold';
	}
	if (ps==50){
	document.getElementById('pagesize50').style.fontWeight='bold';
	}
	if (ps==100){
	document.getElementById('pagesize100').style.fontWeight='bold';
	}
}
switch(GetCookie('IDAMassetgridPageSize'))
{
case '20': 
document.getElementById('pagesize20').style.fontWeight='bold';
break; 
case '50': 
document.getElementById('pagesize50').style.fontWeight='bold';
break; 
case '100': 
document.getElementById('pagesize100').style.fontWeight='bold';
break; 											
default: 
document.getElementById('pagesize20').style.fontWeight='bold';
break; 
}  















function FilterSecOpCl(section,status)
{
try{
CallbackAssetFiltersNew.Callback('showhidepreview',section,status);
}catch(ex)
{}
}


function FilterProject_Del(pId)
{
try{
SetCookie ('pFilter','', exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterPorjectFilterByID(pIndexID)
{
try{
CallbackAssetFiltersContent.Callback('project','index',pIndexID);
}catch(ex)
{}
}


function FilterProject_Add(pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('pFilter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}




function FilterFolder_Del(pId)
{
try{
SetCookie ('cFilter','', exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterFolderFilterByID(pIndexID)
{
try{
CallbackAssetFiltersContent.Callback('folder','index',pIndexID);
}catch(ex)
{}
}


function FilterFolder_Add(pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('cFilter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterTags_Del(pId)
{
try{
var newCookie;
newCookie = '';//GetCookie('tagFilter').replace('@'+pId,'')
SetCookie ('tagFilter',newCookie, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterTagsFilterByID(pIndexID)
{
try{
CallbackAssetFiltersContent.Callback('tags','index',pIndexID);
}catch(ex)
{}
}


function FilterTags_Add(pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('tagFilter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}








function FilterFileType_Del(pId)
{
try{
SetCookie ('tFilter','', exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterFileTypeFilterByID(pIndexID)
{
try{
CallbackAssetFiltersContent.Callback('filetype','index',pIndexID);
}catch(ex)
{}
}


function FilterFileType_Add(pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('tFilter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}








function FilterService_Del(pId)
{
try{
var keyArr = GetCookie('sFilter').split(',');
for (var i=0; i<keyArr.length; i+=1) {
if (pId==keyArr[i]) {
keyArr.splice(i,1);
}
}
SetCookie ('sFilter',keyArr, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterServiceFilterByID(pIndexID)
{
try{
CallbackAssetFiltersContent.Callback('service','index',pIndexID);
}catch(ex)
{}
}


function FilterService_Add(pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('sFilter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}






function FilterMedia_Del(pId)
{
try{
var keyArr = GetCookie('mFilter').split(',');
for (var i=0; i<keyArr.length; i+=1) {
if (pId==keyArr[i]) {
keyArr.splice(i,1);
}
}
SetCookie ('mFilter',keyArr, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterMediaFilterByID(pIndexID)
{
try{
CallbackAssetFiltersContent.Callback('mediatype','index',pIndexID);
}catch(ex)
{}
}


function FilterMedia_Add(pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('mFilter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}






function FilterIllusttype_Del(pId)
{
try{
var keyArr = GetCookie('iFilter').split(',');
for (var i=0; i<keyArr.length; i+=1) {
if (pId==keyArr[i]) {
keyArr.splice(i,1);
}
}
SetCookie ('iFilter',keyArr, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterIllusttypeFilterByID(pIndexID)
{
try{
CallbackAssetFiltersContent.Callback('illusttype','index',pIndexID);
}catch(ex)
{}
}


function FilterIllusttype_Add(pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('iFilter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}



function FilterDyn_Del(tag,type,pId)
{
try{
var newCookie;
newCookie = '';
SetCookie ('dyn'+tag+'Filter',newCookie, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}


function FilterDynFilterByID(tag,type,pIndexID)
{
try{
CallbackAssetFiltersContent.Callback(tag,'index',pIndexID);
}catch(ex)
{}
}


function FilterDyn_Add(tag,type,pId) 
{
try{
$('#dialogAssetFiltersNew').dialog('close');
SetCookie ('dyn'+tag+'Filter',pId, exp);
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
}catch(ex)
{}
}




</script>