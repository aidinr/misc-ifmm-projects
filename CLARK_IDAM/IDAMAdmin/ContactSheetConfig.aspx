<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ContactSheetConfig.aspx.vb" Inherits="IdamAdmin.ContactSheetConfig" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>Contact Sheet Options</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
		<link href="treeStyle.css" type="text/css" rel="stylesheet">
		<link href="navStyle.css" type="text/css" rel="stylesheet">
		<link href="tabStripStyle.css" type="text/css" rel="stylesheet">
		<link href="navBarStyle.css" type="text/css" rel="stylesheet">
		<style>BODY { MARGIN: 0px }
	</style>
</HEAD>
	<body onload="this.focus">
    <form id="myForm" runat="server">



    
   
<div  style="PADDING-RIGHT:20px;PADDING-LEFT:20px;PADDING-BOTTOM:20px;PADDING-TOP:20px;POSITION:relative">
			    <div>
				    <div style="BORDER-RIGHT:1px solid; PADDING-RIGHT:10px; BORDER-TOP:1px solid; PADDING-LEFT:10px; PADDING-BOTTOM:10px; BORDER-LEFT:1px solid; PADDING-TOP:10px; BORDER-BOTTOM:1px solid; POSITION:relative; BACKGROUND-COLOR:white">
					    <table>
						    <tr>
							    <td><img src="images/user16.gif"></td>
							    <td style="font-family:Verdana;">Contact Sheet Options</td>
						    </tr>
					    </table>
					    <div style="BORDER-RIGHT:#b7b4b4 1px solid; PADDING-RIGHT:5px; BORDER-TOP:#b7b4b4 1px solid; PADDING-LEFT:5px; FONT-WEIGHT:normal; FONT-SIZE:10px; PADDING-BOTTOM:5px; BORDER-LEFT:#b7b4b4 1px solid; COLOR:#3f3f3f; PADDING-TOP:5px; BORDER-BOTTOM:#b7b4b4 1px solid; FONT-FAMILY:verdana; BACKGROUND-COLOR:#e4e4e4">
					    Modify options for generating a contact sheet.</div>
					    <br />
  <COMPONENTART:TABSTRIP id="TabStripTenant" runat="server" ImagesBaseUrl="images/" 
							    CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" FillWidth="True" Width="100%" DefaultItemLookId="DefaultTabLook"
							     EnableViewState=false DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="Multipage1">
							    <ItemLooks>
								    <ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
									    LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif"
									    HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
									    LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
								    <ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
									    LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif"
									    LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
							    </ItemLooks>
						    </COMPONENTART:TABSTRIP>
    <ComponentArt:MultiPage id="Multipage1" CssClass=""   runat="server">
	    <ComponentArt:PageView CssClass="" runat="server" ID="Pageview1">
	    
	     
	    
	      
    
    	
<div id="contactsheet" style="display:block;">

<!--Start Rounded Box-->
<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%">
<div class="sidemodule">
    <div class="modulecontainer sidebar">
        <div class="first">
            <span class="first"></span><span class="last"></span>
        </div>
        <div>
            <table border="0" width="100%">
				<tr>
					<td width="4">&nbsp;</td>
					<td valign="top"><!--End Top Rounded Box-->

            <!--<form method="GET" target="NewWindow" id="contactsheetForm" action="contact_sheet.asp">-->
              <p><b><font face="Verdana" size="1">Contact Sheet Settings<br>
				<br>
				</font></b><font face="arial"  size="1">Heading (Optional - Max 3 lines)</font><font face="Arial" size="1" color=""><font face="Arial" size="1" color="#000000"><img border="0" src="images/spacer.gif" height="1" width="82"><br>
				</font><b>
				&nbsp;<textarea rows="3" name="Heading" cols="62"></textarea><br><br />
				</b></font>
				
              <font face="Verdana" size="1"><input class="Preferences" type="checkbox" name="fimagepreview" value="1" checked >Include Image Preview<br></font><br />
					<font face="arial" size="1">layout</font><font face="Arial" size="1" color="#000000"><img border="0" src="images/spacer.gif" height="1" width="82"><br>
				</font>
						
				
				<font face="Arial" size="1" color="">
				<b>
<select   size="1" name="columns" class="more_projects">
				<option  value=1>1 per page</option>
                <option  selected value=2>4 per page</option>
                <option  value=3>12 per page</option>
                <option  value=4>20 per page</option>
              </select></b></font><br>
              
              <br>
				</font></font>
				<!--<font face="arial" size="1" color="">Selection</font><font face="Arial" size="1" color=""><font face="Arial" size="1" color="#000000"><img border="0" src="images/spacer.gif" height="1" width="82"><br>
				</font><font face="Arial" size="1" color="">
				<b>
<select    size="1" name="selection" class="more_projects">
                <option  value=2>Only Selected Items</option>
              </select></b></font>--></p>
				<table border="0" width="100%" id="table1">
					<tr>
						<td valign="top"><font face="Arial" size="1" color=""><font face="Arial" size="1" color="#000000">
						Optional
				Fields<br>
				</font>
              <font color="#808080" size="1" face="Arial">
              <input class="Preferences" type="checkbox" id="fassetname" name="fassetname" value="1" checked >Asset Name<br>
              <input class="Preferences" type="checkbox" id="fassetid" name="fassetid" value="1" checked >Asset ID<br>
              <input class="Preferences" type="checkbox" id="fassetdescription" name="fassetdescription" value="1" checked >Asset Description<br>
              <input class="Preferences" type="checkbox" id="ffiletype" name="ffiletype" value="1" checked >FileType Icon<br>
              <input class="Preferences" type="checkbox" id="ffilename" name="ffilename" value="1" checked >Filename<br>
              <input class="Preferences" type="checkbox" id="ffilesize" name="ffilesize" value="1" checked >Size<br>
              <input class="Preferences" type="checkbox" id="fdate" name="fdate" value="1" checked >Date<br>
              <input class="Preferences" type="checkbox" id="fdimensions" name="fdimensions" value="1" checked >Dimensions<br>
              <input class="Preferences" type="checkbox" id="fprojectname" name="fprojectname" value="1" checked >Project Name<br><br />
               <font face="Arial" size="1" color="#000000"><a href="javascript:unselectalloptions();">unselect all</a></font>
              </td>
					<td>&nbsp;</td>
					<td valign="top">
<div style="width: 250px; height: 200px; overflow: auto;" >			
<asp:Repeater id="RepeaterCSUDF" runat="server" Visible="True">
<ItemTemplate>
	<font color="#808080" size="1" face="Arial"><input class="Preferences" type="checkbox" name="udf" value="<%#DataBinder.Eval(Container.DataItem, "item_id")%>"><%#DataBinder.Eval(Container.DataItem, "item_name")%></font><br>
</ItemTemplate>

<HeaderTemplate>
		<font face="Arial" size="1" color="#000000">Additional User Defined Fields<br></font>
</HeaderTemplate>
		
<FooterTemplate>

</FooterTemplate> 
	
</asp:Repeater>
</div>					
		<font face="Arial" size="1" color="#000000"><a href="javascript:selectalludfs();">select all</a></font> 
					
					
					
					
              </td>
					</tr>
				</table>
					<br><font size="1">Note:&nbsp; </font>
				<font face="Arial" size="1" color="">
				Selecting too many fields may prevent the page from printing 
				properly.  Please use your Print Preview function to 
				ensure that the contact sheet page breaks are aligned properly 
				before printing.<br>
				</b></font><font face="Arial" size="1" color="#000000"><br>
				</font><font face="Arial" size="1" color="">
				<br>
              <input type="button" onclick="javascript:submitContactSheet('preview');" value="Preview" name="Preview">&nbsp;&nbsp;<input type="button" onclick="javascript:submitContactSheet('print');" value="Print" name="Print">&nbsp;&nbsp;<input type="button" onclick="javascript:submitContactSheet('exporttxt');" value="Export Text" name="Exporttxt">&nbsp;&nbsp;<input type="button" onclick="javascript:submitContactSheet('cancel');" value="Cancel" name="Cancel">
              <input type="hidden" name="actionType" value="edit">
              <input type="hidden" name="carr" value="<%=request.querystring("carid")%>" >
              <input type="hidden" name="sort_id" value="<%=request.querystring("sort_id")%>" >
              <input type="hidden" name="asset_array" value="" >
              <input type="hidden" name="buttonused" value="" >
              
            <!--</form>-->
     
            
            


<!--Close Rounded Box-->
					</td>
					<td width="4">&nbsp;</td>
				</tr>
			</table>
			</div>
        <div class="last">
        	<span class="first"></span><span class="last"></span>
        </div>
    </div>
</div>
<!--End Rounded Box-->
<!--Start Close Rounded Box Table-->
								</td>
								<td><img border="0" src="images/spacer.gif" height="2" width="11"></td>
							</tr>
</table>
<!--End Close Rounded Box Table-->
</div>

		
		
		
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
    
	    </ComponentArt:PageView>
	    </ComponentArt:MultiPage>
		
	    </div>
	    </div>
	    </div> 
    </div>
    
    
    
    



<script>
function unselectalloptions() {
document.getElementById('fassetname').checked = false;
document.getElementById('fassetid').checked = false;
document.getElementById('fassetdescription').checked = false;
document.getElementById('ffiletype').checked = false;
document.getElementById('ffilename').checked = false;
document.getElementById('fassetname').checked = false;
document.getElementById('ffilesize').checked = false;
document.getElementById('fdate').checked = false;
document.getElementById('fdimensions').checked = false;
document.getElementById('fprojectname').checked = false;
}
    function selectalludfs() {
        CheckAllItems();
    }



    function CheckAllItems() {
        var bcheck, checks;
        var highlightbox;
        var checks = document.getElementsByName('udf');
   
        bcheck = false;
        for (i = 0; i < checks.length; i++) {
            checks[i].checked = true;
        }
    }  
    
//submit contact sheet

    function submitContactSheet(buttonused) {

        if (buttonused == 'cancel') {
            window.parent.CloseDialogWindowX();
            return true;
        }
        
        myForm.buttonused.value = buttonused;
        myForm.action = 'ContactSheet.aspx';
		myForm.target = '_Blank';
		myForm.submit();
		myForm.action = '';
		myForm.target = '';
		return true;
	}
    
    
    
    
    
    
    
    
    
    
    
		
		
		
		
		
		
		</script>
		
		
		
		
		
		
		
		

    </form>
</body>
</html>
