<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.Orders" CodeBehind="Orders.ascx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<div class="previewpaneMyProjects ui-accordion-header ui-helper-reset ui-state-active " id="toolbarheading" style="z-index:99;border-top:0px;border-bottom:0px;">
	<!--Project cookie and top menu this is a test compiler-->
		<table cellspacing="0"  id="table2" width="100%">
			<tr>
			<td width=1 nowrap >
			<img  style="padding-top:5px;" src="images/ui/users_ico_bg.gif" height=42 width=42 >
			</td><td valign=top>
			<div style="float:left;padding:5px;">
						<font face="Verdana" size="1"><b>Order Information</b><br>
				<span style="font-weight: 400">Modify/add Order information.<br></span>
				
				</font>
			</div>
			</td>
			<td id="Test" valign="top" align="right">

			</td></tr>
			<tr><td valign="top" colspan="2">
			</td>
			</tr>
		</table>
</div> <!--end preview pane-->


      
<script language=javascript>

// Forces the grid to adjust to the new size of its container          
function resizeGrid2(DomElementId, NewPaneHeight, NewPaneWidth)
{
<% Response.Write(GridOrders.ClientID) %>.Render();
//alert('here');
} 

function editGridPopup(rowId)
{
var rowdata;
var orderid;
rowdata = <% Response.Write(GridOrders.ClientID) %>.GetRowFromClientId(rowId);
orderid = rowdata.GetMember('orderid').Value;
window.location='IDAM.aspx?page=OrdersDetails&id=' + orderid;
}
function editRow(item)
{

 <% Response.Write(GridOrders.ClientID) %>.EditComplete();  

  }
  
  
  function onInsert(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
    function ShowContextMenu(item, column, evt) 
  {
    <% Response.Write(GridOrders.ClientID) %>.Select(item); 
   
    return false; 
  }
  
  function onUpdate(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridOrders.ClientID) %>.Page(1); 
  }

  function onDelete(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
 
  function insertRow()
  {
    <% Response.Write(GridOrders.ClientID) %>.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    <% Response.Write(GridOrders.ClientID) %>.Delete(<% Response.Write(GridOrders.ClientID) %>.GetRowFromClientId(rowId)); 
  }



  function onInsertCategory(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  

  
  function onUpdateCategory(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }



  function onDeleteCategory(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
  
function isdefined(variable)
{
return (typeof (window[variable]) == "undefined") ? false : true;
}
var timeoutDelay = 1400;
var nb = "&nbsp;"; // non-breaking space
function buildPager(grid, pagerSpan, First, Last)
{
var pager = "";
var mid = Math.floor(pagerSpan / 2);
var startPage = grid.PageCount <= pagerSpan ? 0 : Math.max(0, grid.CurrentPageIndex - mid);
// adjust range for last few pages
if (grid.PageCount > pagerSpan)
{
startPage = grid.CurrentPageIndex < (grid.PageCount - mid) ? startPage : grid.PageCount - pagerSpan;
}

var endPage = grid.PageCount <= pagerSpan ? grid.PageCount : Math.min(startPage + pagerSpan, grid.PageCount);

if (grid.PageCount > pagerSpan && grid.CurrentPageIndex > mid)
{
pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(0);return false;\">&laquo; First</a>" + nb + "..." + nb;
pager += "<a href=\"#\" onclick=\"" + grid.Id + ".PreviousPage();return false;\">&lt;</a>" + nb;
}

for (var page = startPage; page < endPage; page++)
{
var showPage = page + 1;
if (page == grid.CurrentPageIndex)
{
pager += showPage + nb;
}
else 
{
pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(" + page + ");return false;\">" + showPage + "</a>" + nb;
}
}

if (grid.PageCount > pagerSpan && grid.CurrentPageIndex < grid.PageCount - mid)
{
pager += "<a href=\"#\" onclick=\"" + grid.Id + ".NextPage();return false;\">></a>" + nb + "..." + nb;
pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(" + (grid.PageCount - 1) + ");return false;\">Last &raquo;</a>" + nb;
}

return pager;
}
function buildPageXofY(grid, Page, of, items)
{
// Note: You can trap an empty Grid here to change the default "Page 1 of 0 (0 items)" text
var pageXofY = Page + nb + "<b>" + (grid.CurrentPageIndex + 1) + "</b>" + nb + of + nb + "<b>" + (grid.PageCount) + "</b>" + nb + "(" + grid.RecordCount + nb + items + ")";

return pageXofY;
}
function showCustomFooter()
{
var gridId = "<%response.write( GridOrders.ClientId)%>";
if (isdefined(gridId))
{
var grid = <%response.write( GridOrders.ClientId)%>;

var Page = "Page";
var of = "of";
var items = "items";

var pagerSpan = 5; // should be at least 2
var First = "First";
var Last = "Last";
var cssClass = "GridFooterText";

var footer = buildPager(grid, pagerSpan, First, Last);
document.getElementById("tdPager").innerHTML = "<div style='white-space:nowrap;display:inline;' class=\"" + cssClass + "\">" + footer + "</div>";

footer = buildPageXofY(grid, Page, of, items);
document.getElementById("tdIndex").innerHTML = "<div class=\"" + cssClass + "\">" + footer + "</div>";
}
else 
{
setTimeout("showCustomFooter();", timeoutDelay);
}
}

function onPage(newPage)
{
// delay call so that Grid's client properties have their new values
setTimeout("showCustomFooter();",timeoutDelay);
return true;
}
function onLoad()
{
showCustomFooter();
}


function formatCurrency(num) {
num = num.toString().replace(/\$|\,/g,'');
if(isNaN(num))
num = "0";
sign = (num == (num = Math.abs(num)));
num = Math.floor(num*100+0.50000000001);
cents = num%100;
num = Math.floor(num/100).toString();
if(cents<10)
cents = "0" + cents;
for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
num = num.substring(0,num.length-(4*i+3))+','+
num.substring(num.length-(4*i+3));
return (((sign)?'':'-') + '$' + num + '.' + cents);
}



</script>

<div id="subtabset" style="position:relative;top:-4px;z-index:2; font-size:10px;">
<input type="radio" id="radio1" name="subtab"  checked="checked"  /><label for="radio1">General</label><img src="images/spacer.gif" width="2" alt="" /></div>
<div style="border-top:1px solid silver;position:relative;top:-25px;z-index:1;margin-left:0px;"></div>

<script>
    $(function () {
        $("#subtab").button();
        $("#subtabset").buttonset();
    });
	</script>





<!--<div style="padding:10px;border-left:1px #969698 solid; border-right:1px #969698 solid;border-bottom:1px #969698 solid;border-top:1px #969698 solid;">-->



									<div class="SnapProjectsWrapper">
										<div class="SnapProjectsAssets" >



<div style="padding-top:10px;"><div style="padding:3px;"></div></div>

<table cellpadding=0 cellspacing=0 ><tr><td width="80" nowrap><b>Filter Status:</b></td><td width="220" nowrap  ><asp:DropDownList ID="ddstatusfilter" width="180" DataTextField="name" DataValueField="status"  Runat="server"></asp:DropDownList></td><td align=right width=100% nowrap ><b><font size=1>Search Orders:</font></b><img src="images/spacer.gif" width=5 height=1></td><td align=right width=50 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:70px"  onkeyup="javascript:if(event.keyCode==13) {DoSearchFilter(this.value); return false}"></td></tr></table> 

<div style="padding-top:10px;"><div style="padding:3px;"></div></div>


<div class="assetGirdPagingHeading" id="assetGirdPagingHeading" style="display:block;" >
<div id="CustomFooter" style="width:100%;border-top:none;">
<table width="100%">
<tr>
<td id="tdPager" align="left"></td>
<td id="tdIndex" align="right"></td>
</tr>
</table>
</div>
</div>




<COMPONENTART:GRID 
id="GridOrders" 
runat="server" 
AutoFocusSearchBox="false"
AutoCallBackOnInsert="true"
AutoCallBackOnUpdate="true"
AutoCallBackOnDelete="true"
pagerposition="2"
ScrollBar="Off"
ClientSideOnPage="onPage"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ClientSideOnInsert="onInsert"
ClientSideOnUpdate="onUpdate"
ClientSideOnDelete="onDelete"
ClientSideOnLoad="onLoad"
ClientSideOnCallbackError="onCallbackError"
Height="10" Width="100%"
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
EnableViewState="true"
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRowCategory" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="20" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="True" 
GroupingPageSize="5" 
PagerTextCssClass="GridFooterTextCategory" 
PagerStyle="Numbered" 
PageSize="50" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeader" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="true" 
AllowSorting="False"
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
ScrollBarWidth="15" 
AllowPaging="true">
<ClientEvents>
<CallbackComplete EventHandler="GridAssets_onCallbackComplete" />
</ClientEvents>
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
<table height="1px" width="100%" ><tr><td valign="center" align="center">
<table cellspacing="0" cellpadding="0" border="0"  background="images/loadingback.gif" height="70px" width="300px">
<tr>
	<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
	<td style="font-size:10px;font-family:Verdana;" width="60px" align="right">Loading...&nbsp;</td>
	<td><img src="images/spinner2.gif"  border="0"></td>
	<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
</tr>
</table>
</td></tr></table>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplate">
            <a href="javascript:editGridPopup('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border="0" > </a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplate">
            <img src="images/flag_blue.gif" border="0" > 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateGoto">
            <img src="images/i_rate.gif" border="0" > 
          </ComponentArt:ClientTemplate>    
          <ComponentArt:ClientTemplate Id="TypeCurrency"><div style="text-align:right;">$## DataItem.GetMember("ItemTotal").Value ##</div></ComponentArt:ClientTemplate>                                              
</ClientTemplates>
<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
EditFieldCssClass="EditDataField"
EditCommandClientTemplateId="EditCommandTemplate"
InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="orderid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asch10.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desch10.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="10" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center" AllowSorting="False" AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Invoice"   AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="60"  DataField="orderid" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Agency"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="120"   DataField="agency" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Firstname"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="120"  DataField="firstname" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Lastname"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="120"  DataField="lastname" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Order Date" AllowEditing="false" FormatString="MMM dd yyyy, hh:mm tt"  AllowGrouping="False" Width="80" SortedDataCellCssClass="SortedDataCell"  DataField="orderdate"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Completed Date" AllowEditing="false"  AllowGrouping="False" Width="80" SortedDataCellCssClass="SortedDataCell"  DataField="completed_date"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false"  HeadingText="Items"  align="center" SortedDataCellCssClass="SortedDataCell" Width="50" DataField="Items"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false"  HeadingText="Total Cost"  FormatString="c" align="right" width="80"  SortedDataCellCssClass="SortedDataCell" DataField="ItemTotal"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" align="right" HeadingText="  Status" AllowEditing="true"  SortedDataCellCssClass="SortedDataCell" Width="120"  DataField="status" ForeignTable="StatusTable" ForeignDataKeyField="status" ForeignDisplayField="Name" ></componentart:GridColumn>
<ComponentArt:GridColumn AllowSorting="False" HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand"  Width="80" Align="Center" DataCellCssClass="LastDataCellPostings" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="orderid" SortedDataCellCssClass="SortedDataCell" DataField="orderid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>




























      <script type=text/javascript>
  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

  
  function BrowseOnDoubleClick(SelectedGridRow)
  {
  
  	var itemvaluetmp;
	itemvaluetmp = SelectedGridRow.GetMember('uniqueid').Value;
	if (itemvaluetmp.substring(0,1) == 'P') 
	{
		itemvaluetmp = itemvaluetmp.replace('P_','');
		window.location.href = 'IDAM.aspx?page=Project&id=' + itemvaluetmp + '&type=project'
    
	} else {
		itemvaluetmp = itemvaluetmp.replace('C_','');
		window.location.href = 'IDAM.aspx?page=Browse&id=' + itemvaluetmp + '&type=category';
	}
	
	return true;
  
  
  
	
  }
  
    function highlightProjectToggle(checkbox,projectid)
  {
  var highlightbox;
  highlightbox = document.getElementById('projecthighlight_' + projectid);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  
  
  
if (GetCookie('SearchValueOrders')!=null){window.document.getElementById('search_filter').value=GetCookie('SearchValueOrders')};
ddstatusfiltertmp=window.document.getElementById('_ctl2_ddstatusfilter')
if (GetCookie('StatusFilterValueOrders')!=null){ddstatusfiltertmp.value=GetCookie('StatusFilterValueOrders')};

//SetCookie ('SearchValueOrders', '', exp);
//alert(GetCookie('SearchValueOrders'));
function DoSearchFilter(searchvalue)
{
SetCookie ('SearchValueOrders', searchvalue, exp);
<%response.write (GridOrders.ClientID)%>.Filter(searchvalue);
}
function GridAssets_onCallbackComplete(sender, eventArgs)
{
showCustomFooter();
}

function statusfilterchange(dropdown)
{
SetCookie ('StatusFilterValueOrders', dropdown.value, exp);
<%response.write (GridOrders.ClientID)%>.Filter('STATUSFILTER'+dropdown.value);
}

  </script>



      
      
      
      </div><!--END browsecenter-->
      </div><!--END MAIN-->
      
      
      
      
      
      
      
      
<div style="padding-left:10px;"><div style="padding:3px;"></div></div>