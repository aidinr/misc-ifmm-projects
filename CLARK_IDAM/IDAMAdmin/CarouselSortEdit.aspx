<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.CarouselSortEdit" CodeBehind="CarouselSortEdit.aspx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Create/Edit Sort Order</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
			<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
			<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
				<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
					<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
						<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
							<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
								<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
									<link href="treeStyle.css" type="text/css" rel="stylesheet">
										<link href="navStyle.css" type="text/css" rel="stylesheet" >
										    <link href="tabStripStyle.css" type="text/css" rel="stylesheet" />    
												<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
										<style>BODY { MARGIN: 0px }
	</style>
	<body onload="this.focus">
		<form id="Form" method="post" runat="server">
		
		
		
		<script language=javascript>

  

  

  function onInsertCategory(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }

  function onUpdateCategory(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }


  


</script>
		
<script type="text/javascript">
  

<%if request.querystring("newid") <> "" then%>
//window.opener.NavigateToCategory('<%response.write (request.querystring("newid"))%>');
//window.close();
<%end if%>

<%if instr(request.querystring("id"),"CAT") = 0  then%>
<%if request.querystring("parent_id") = ""  then%>
//window.close();
<%end if%>
<%end if%>

</script>

			
			<input type="hidden" name="catid" id="catid" value="<%response.write (replace(request.querystring("id"),"CAT",""))%>">
			<input type="hidden" name="parent_id" id="parent_id" value="<%response.write (request.querystring("parent_id"))%>">
			<div style="padding:20px;position:relative;">
			<div style="">
			<div style="padding:10px;border:1px solid;background-color:white;position:relative;">
			<table><tr><td></td><td>Create/Edit Sort Order</td></tr></table><br>
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Create/Modify a Sort Order.  </div><br>
  <div style="">

			
			
			
	
			
			
		
							<COMPONENTART:TABSTRIP id="TabStripCategory" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataEvents.xml"
								CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" FillWidth="True" Width="100%" DefaultItemLookId="DefaultTabLook"
								DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
								<ItemLooks>
									<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
									<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
								</ItemLooks>
							</COMPONENTART:TABSTRIP>
							<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageCarouselSortPage" runat="server">
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="User_General">
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Enter a name, description and security level for this sort order.</div>
			<br>
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">Name </font>
					</td>
					<td align="left" valign="top" width=100%>
						<asp:TextBox id="categoryName" runat="server"  Height=25px Width="100%" CssClass="InputFieldMain100P"></asp:TextBox></td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						&nbsp;</td>
					<td class="PageContent"  width="180%" align="left" valign="top">
	
	
						<asp:CheckBox  Runat=server Checked=true id="chkactive"></asp:checkbox>Active</td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">Description</font></td>
					<td class="PageContent" align="left" valign="top"  width=100%>
						<asp:TextBox id="categoryDescription" TextMode="MultiLine" runat="server" Width="100%" CssClass="InputFieldMain100P" Height="100px" ></asp:TextBox></td>
				</tr>
			</table>
		</ComponentArt:PageView>
		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Category_Members">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Change permissions for this object by selecting a group or user from the list and adding to the active list.</div><br>
			Coming soon.
		</ComponentArt:PageView>
	</ComponentArt:MultiPage>
	<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
					<div style="text-align:right;"><br>
			<asp:Button id="btnCategorySave" runat="server" Text="Save"></asp:Button><img src="images/spacer.gif" width=5 height=1><input type="button" onclick="window.opener.RefreshCarousel('<%response.write (request.querystring("carousel_id"))%>');window.close();" value="Close" />
			</div>
	</div>
</div>

			
			
			
			

			</div>
			
			
			
			
			
			
			
			
			</div>
			

			
			</div>
			</div>
			
		
			
			
		</form>
	</body>
</HTML>
