<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.AssetTagging" EnableViewstate="True" ValidateRequest="False" CodeBehind="AssetTagging.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>Project</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
    <link href="common/baseStyle.css" type="text/css" rel="stylesheet" />
    <link href="gridStyle.css" type="text/css" rel="stylesheet" />
    <link href="snapStyle.css" type="text/css" rel="stylesheet" />
    <link href="tabStyle.css" type="text/css" rel="stylesheet" />
    <link href="menuStyle.css" type="text/css" rel="stylesheet" />
    <link href="multipageStyle.css" type="text/css" rel="stylesheet" />
    <link href="treeStyle.css" type="text/css" rel="stylesheet" />
    <link href="navStyle.css" type="text/css" rel="stylesheet" />
    <link href="tabStripStyle.css" type="text/css" rel="stylesheet" />
    <link href="navBarStyle.css" type="text/css" rel="stylesheet" />
    <link href="calendarStyle.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="thickbox.css" type="text/css" />
    <link rel="stylesheet" href="combobox.css" type="text/css" />
    <link href="css/custom/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css"  media="all"/>
		<style type="text/css">BODY { MARGIN: 0px }
		    
		    .paneliconoff
		    {
		    	background-position: bottom left;
		    }
            .paneliconon
		    {
		    	background-image: url(images/i_openclose.gif);background-repeat:no-repeat;
		    	cursor:pointer;
		    }		    
	</style>
<script type="text/javascript" src="includes/CookieScripts.js"></script>
<script type="text/javascript" src="js/piclens_optimized.js"></script>
<script type="text/javascript" src="includes/JSLib.aspx"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery.effects.slide.js"></script>
<script type="text/javascript" src="http://cdn.jquerytools.org/1.2.3/all/jquery.tools.min.js"></script>
<script src="js/jquery.multiSelect.js" type="text/javascript"></script>
<script src="js/jquery.cookie.js" type="text/javascript"></script>
<script type="text/javascript" src="js/splitter.js"></script>
	
	
<style type="text/css" media="all">
html, body
{
	margin: 0;			/* Remove body margin/padding */
	padding: 0;
	overflow: hidden;	/* Remove scroll bars on browser window */
}
#header { padding: 1em; }

#MySplitter {
 
	min-width: 500px;	/* Splitter can't be too thin ... */
	min-height: 300px;	/* ... or too flat */
	height: 600px;

}
#LeftPane {

	width: 500px;
	min-width: 100px;
	max-width: 500px;
	overflow: auto;		/* Scroll bars appear as needed */
}
#TopPane {				/* Top nested in right pane */

	height: 185px;		/* Initial height */
	min-height: 25px;	/* Minimum height */
	overflow: hidden;	
}
#BottomPane {			/* Bottom nested in right pane */

	min-height: 100px;
	overflow: auto;
}

/* Splitbar styles; these are the default class names */

.vsplitbar {
	width: 4px;
	background: #e4e4e4;
	border-left:#b7b4b4 1px solid;
	border-right:#b7b4b4 1px solid;
	
}
.vsplitbar:hover, .vsplitbar.active {
	opacity: 0.7;
	filter: alpha(opacity=70); /* IE */
	background: #b7b4b4;
	border-left:#b7b4b4 1px solid;
	border-right:#b7b4b4 1px solid;
}
.hsplitbar {
	height: 4px;
	background: #e4e4e4;
		border-top:#b7b4b4 1px solid;
	border-bottom:#b7b4b4 1px solid;
		border-right:#b7b4b4 1px solid;
}
.hsplitbar.active, .hsplitbar:hover {
	background: #b7b4b4;
			border-top:#b7b4b4 1px solid;
	border-bottom:#b7b4b4 1px solid;
			border-right:#b7b4b4 1px solid;
}
</style>
<script type="text/javascript">
    $(document).ready(function() {
        // Vertical splitter. Set min/max/starting sizes for the left pane.
        $("#MySplitter").splitter({
            splitVertical: true,
            outline: true,
            sizeLeft: true,
            anchorToWindow: false,
            accessKey: "I"
        });
        // Horizontal splitter, nested in the right pane of the vertical splitter.
        $("#RightPane").splitter({
            splitHorizontal: true,
            sizeTop: true,
            accessKey: "H"
        });
    });
    
  
   $(window).bind("resize", function(){ 
	  $("#MySplitter").trigger("resize");
   });

    
    
</script>
</HEAD>
	<body >
    <script language="javascript" type="text/javascript">
    <!--
      // Forces the treeview to adjust to the new size of its container          


      // Forces the grid to adjust to the new size of its container          
      function resizeGrid(DomElementId, NewPaneHeight, NewPaneWidth)
      {
        GridAssets.Render();
      }  
      //-->
    </script>	
        <script type="text/javascript">




$(document).ready(function() {

 $('#TagInformation_Icon').click(function() {
        $("#TagInformation").slideToggle("slow");
		$(this).toggleClass("paneliconoff"); 
    });
 $('#TagSecurity_Icon').click(function() {
        $("#TagSecurity").slideToggle("slow");
		$(this).toggleClass("paneliconoff"); 
    });
 $('#TagKeywordMediaType_Icon').click(function() {
        $("#TagKeywordMediaType").slideToggle("slow");
		$(this).toggleClass("paneliconoff"); 
    });
 $('#TagKeywordIllustType_Icon').click(function() {
        $("#TagKeywordIllustType").slideToggle("slow");
		$(this).toggleClass("paneliconoff"); 
    });
 $('#TagKeywordServices_Icon').click(function() {
        $("#TagKeywordServices").slideToggle("slow");
		$(this).toggleClass("paneliconoff"); 
    });
 $('#TagUserDefinedFields_Icon').click(function() {
        $("#TagUserDefinedFields").slideToggle("slow");
		$(this).toggleClass("paneliconoff"); 
    });                    


});


    // Ensure that folders are grouped together 
    function SortHandler(column, desc)
    {
      // multiple sort, giving the top priority to IsFolder
      GridAssets.SortMulti([4,!desc,column.ColumnNumber,desc]);

      // re-draw the grid
      GridAssets.Render();

      // cancel default sort
      return false;
    }
    function getURLPreviewIcon()
{
return '<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveIconURLBase%>?instance=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMInstance%>&size=0&id=';
}
    
    
 
    
    
    
    function closealltags()
    {
$("#TagInformation").slideUp("slow");
$("#TagUserDefinedFields").slideUp("slow");
$("#TagKeywordServices").slideUp("slow");
$("#TagKeywordMediaType").slideUp("slow");
$("#TagKeywordIllustType").slideUp("slow");
$("#TagSecurity").slideUp("slow");
    }
    
    function openalltags()
    {
$("#TagInformation").slideDown("slow");
$("#TagUserDefinedFields").slideDown("slow");
$("#TagKeywordServices").slideDown("slow");
$("#TagKeywordMediaType").slideDown("slow");
$("#TagKeywordIllustType").slideDown("slow");
$("#TagSecurity").slideDown("slow");
    }    
    
      
function GridAssets_onLoad(sender, eventArgs) 
{
  CheckAllItems();
}

function CheckAllItems()
    {
    
    
    var itemIndex = 0;


for (var x = 1; x <= <% Response.Write(GridAssets.ClientID) %>.get_recordCount(); x++)
{
<% Response.Write(GridAssets.ClientID) %>.Select(<% Response.Write(GridAssets.ClientID) %>.Table.GetRow(x-1),true);
}


<% Response.Write(GridAssets.ClientID) %>.Render();
}

    
    
function unCheckAllItems()
    {
    <% Response.Write(GridAssets.ClientID) %>.unSelectAll();
    //<% Response.Write(GridAssets.ClientID) %>.updated();
    //<% Response.Write(GridAssets.ClientID) %>.Render();
    }
    
    
function CopyTags(item)
{
doCopy = confirm("Copy tags?"); 
if (doCopy)
    {
		<% Response.Write(CALLBACKKeywordsServices.ClientID) %>.Callback(item);
		<% Response.Write(CALLBACKKeywordsIllustType.ClientID) %>.Callback(item);
		<% Response.Write(CALLBACKKeywordsMediaType.ClientID) %>.Callback(item);
		<% Response.Write(CALLBACKInformation.ClientID) %>.Callback(item);
		<% Response.Write(CALLBACKUDFMain.ClientID) %>.Callback(item);
		<% Response.Write(CALLBACKSecurityTag.ClientID) %>.Callback(item);
		
	}
	

}    
    </script>
 <SCRIPT LANGUAGE="JavaScript" SRC="js/CalendarPopup.js"></SCRIPT>
		<form id="Form" method="post" runat="server">
			<div style="PADDING-RIGHT:20px;PADDING-LEFT:20px;PADDING-BOTTOM:1px;PADDING-TOP:20px;POSITION:relative;">
				<div>
					<div style="BORDER-RIGHT:1px solid; PADDING-RIGHT:10px; BORDER-TOP:1px solid; PADDING-LEFT:10px; PADDING-BOTTOM:1px; BORDER-LEFT:1px solid; PADDING-TOP:10px; BORDER-BOTTOM:1px solid; POSITION:relative; BACKGROUND-COLOR:white;">
						<div style="BORDER-RIGHT:#b7b4b4 1px solid; PADDING-RIGHT:5px; BORDER-TOP:#b7b4b4 1px solid; PADDING-LEFT:5px; FONT-WEIGHT:normal; FONT-SIZE:10px; PADDING-BOTTOM:5px; BORDER-LEFT:#b7b4b4 1px solid; COLOR:#3f3f3f; PADDING-TOP:5px; BORDER-BOTTOM:#b7b4b4 1px solid; FONT-FAMILY:verdana; BACKGROUND-COLOR:#e4e4e4;position:relative;">Tag 
							assets by selecting each asset from the listing below and setting the proper 
							attributes in the information section.&nbsp; Appending the tags means to keep 
							any existing information about the selected assets and adding any new 
							tags.&nbsp; Overwriting the tags means to remove any existing information about 
							the asset and replace with the new tags.&nbsp; Use the splitter window to 
							switch from full asset grid view to preview.&nbsp; You can edit asset 
							information, user defined fields, keywords and security settings from here.</div>
						<br>
						
						
		
		
		
<div id="MySplitter">
		
		
		<div id="LeftPane">
		
		
            <table class="HeadingCell" style="width:100%;height:25px;" cellpadding="0" cellspacing="0">
              <tr>
                <td class="HeadingCellText">Tags</td>
                <td  align=right style="FONT-SIZE: 10px; FONT-FAMILY: Verdana">[<a href="javascript:closealltags();">close panels</a>] [<a href="javascript:openalltags();">open panels</a>] &nbsp;&nbsp;</td>
              </tr>
            </table>
           
            
            <div style="padding:5px;padding-right:10px;">
            <div id="LeftColumn" >


<table id="TagInformation_Table" class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
<tr>
<td id="TagInformation_Icon" width="15" style="cursor: hand" align="right" class="paneliconon"></td>
<td >Information</td>
</tr>
</table>
<div id="TagInformation">
								
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">



<COMPONENTART:CALLBACK id="CALLBACKInformation" runat="server" CacheContent="false">
											<CONTENT>
<asp:PlaceHolder ID=PlaceholderTagInformation runat=server >
Name:<br>
<asp:TextBox ID="name" TextMode=SingleLine CssClass="InputFieldMain" style="width:90%" runat="server" value=""></asp:TextBox>
<!--<input id="nametmp" type=text class="InputFieldMain" style="width:90%" value="">-->
Description:<br>
<asp:TextBox id="description" TextMode=MultiLine Rows=4 cssclass="InputFieldMain" runat="server" style="width:90%;height:90px"></asp:TextBox>
<!--<textarea id="description" class="InputFieldMain"  style="width:90%;height:90px"></textarea>-->
</asp:PlaceHolder>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div><!--carouselshortlist-->
</div><!--SnapTreeview-->
</div><!--panel-->						
								
								
								
								
								
								
<div  class="SnapHeaderProjectsFiller"></div>	
<table id="Table1" class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
<tr>
<td id="TagSecurity_Icon" width="15" style="cursor: hand" align="right" class="paneliconon"></td>
<td >Security</td>
</tr>
</table>
<DIV id="TagSecurity" >
								

									<div class="SnapTreeviewTag" style="background-color:white;">	
									<div class="carouselshortlist">
									
										<COMPONENTART:CALLBACK id="CALLBACKSecurityTag" runat="server" CacheContent="false">
											<CONTENT>
												<asp:PlaceHolder ID="PlaceholderSecurityTag" runat="server" >
													Security Level:<br>
													<asp:Literal ID="LiteralSecurityLevelDropdown" Runat=server></asp:Literal><br>
													<asp:CheckBox id="CheckBoxSecurityTag" runat="server" Checked="False"></asp:CheckBox>Force security level change
												</asp:PlaceHolder>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>

									</div><!--carouselshortlist-->
									</div><!--SnapTreeview-->
</DIV>








	
<div  class="SnapHeaderProjectsFiller"></div>
<table id="Table2" class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
<tr>
<td id="TagUserDefinedFields_Icon" width="15" style="cursor: hand" align="right" class="paneliconon"></td>
<td >User Defined Fields</td>
</tr>
</table>
<DIV id="TagUserDefinedFields">
									
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">


<COMPONENTART:CALLBACK id="CALLBACKUDFMain" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="Literal_UDFMAIN" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>


</div><!--carouselshortlist-->
</div><!--SnapTreeview-->
</DIV>















<div  class="SnapHeaderProjectsFiller"></div>	
<table id="Table3" class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
<tr>
<td id="TagKeywordServices_Icon" width="15" style="cursor: hand" align="right" class="paneliconon"></td>
<td ><asp:Literal ID=LiteralKeywords1aTitle runat=server></asp:Literal></td>
</tr>
</table>
<DIV id="TagKeywordServices">
								
									
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">
<img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=Services','Keywords_Services','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsServices" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="LiteralKeywords1" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div><!--carouselshortlist-->
</div><!--SnapTreeview-->
</DIV>








<div  class="SnapHeaderProjectsFiller"></div>	


<table id="Table4" class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
<tr>
<td id="TagKeywordMediaType_Icon" width="15" style="cursor: hand" align="right" class="paneliconon"></td>
<td ><asp:Literal ID=LiteralKeywords2aTitle runat=server></asp:Literal></td>
</tr>
</table>
<DIV  id="TagKeywordMediaType">
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">
<img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=MediaType','Keywords_MediaType','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsMediaType" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="LiteralKeywords2" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div><!--carouselshortlist-->
</div><!--SnapTreeview-->
</DIV>











		
<div  class="SnapHeaderProjectsFiller"></div>	





<table id="Table5" class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
<tr>
<td id="TagKeywordIllustType_Icon" width="15" style="cursor: hand" align="right" class="paneliconon"></td>
<td ><asp:Literal ID=LiteralKeywords3aTitle runat=server></asp:Literal></td>
</tr>
</table>
<DIV id="TagKeywordIllustType">
								
									
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">
<img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=IllustType','Keywords_IllustType','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsIllustType" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="LiteralKeywords3" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>

</div><!--carouselshortlist-->
</div><!--SnapTreeview-->
</DIV>
							
		
						
						
						
						
							
		
<div  class="SnapHeaderProjectsFiller"></div>	

            </div><!--leftcolumn-->
            </div><!--padding-->
          
          
          
          
     </div><!--left pane-->     
     <div id="RightPane">
          <div id="TopPane">
          
          
              
          
          
          
          
          
          
          
 
                         <table class="HeadingCell" style="width:100%;height:25px;" cellpadding="0" cellspacing="0">
              <tr>
                <td class="HeadingCellText">Image Preview</td>
              </tr>
            </table>
									<div style="padding:0px;overflow:hidden;padding:5px;background-color:white;">
										<COMPONENTART:CALLBACK id="AssetImage" runat="server" CacheContent="false">
											<CONTENT>
											<asp:Placeholder ID=assetpreview Runat=server>
												<TABLE height="100" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px" valign=top width=10>
																	<!--<asp:Literal ID="CurrentImageName" Runat=server></asp:Literal><br>-->
																	<asp:Image id="CurrentImage" runat="server" ImageUrl="images/spacer.gif"></asp:Image></TD>
																	<td width=100% valign=top >
																	<div style= "PADDING-LEFT: 5px; FONT-SIZE: 8pt; FONT-FAMILY: Verdana">
<asp:Literal ID="LiteralAssetInfoSummary" Runat=server></asp:Literal></div>
																	</td>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
												</asp:PlaceHolder>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px">Loading...
																	</TD>
																	<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
									</div>
        
             
      
      
      
      
      
      </div>
	
	<div id="BottomPane">
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      

<div  style="padding:5px;">      
      
<font style="FONT-SIZE: 8pt; FONT-FAMILY: Verdana">[ <a href="javascript:CheckAllItems();">select all</a> ] <!--[ <a href="javascript:unCheckAllItems();">unselect all</a> ]-->  </font><br>     											
<img src="images/spacer.gif" width=1 height=4><br>
<asp:Literal id="literalNoAssets" Text="No assets available" visible=false runat="server" />

<!--GroupBy="categoryname ASC"-->
											<COMPONENTART:GRID 
												id="GridAssets" 
												ClientSideOnSelect="BrowseOnSingleClickAssets" 
												runat="server" 
												AutoFocusSearchBox="false"
												AutoCallBackOnInsert="true"
												AutoCallBackOnUpdate="true"
												AutoCallBackOnDelete="true"
												pagerposition="TopRight"
												ScrollBar="Off"
												ScrollTopBottomImagesEnabled="false"
												ScrollTopBottomImageHeight="2"
												ScrollTopBottomImageWidth="16"
												ScrollImagesFolderUrl="images/scroller/"
												ScrollButtonWidth="16"
												ScrollButtonHeight="17"
												ScrollBarCssClass="ScrollBar"
												ScrollGripCssClass="ScrollGrip"
AllowHtmlContent="true" 
												
												
												Height="10" Width="97%"
												LoadingPanelPosition="TopCenter" 
												LoadingPanelClientTemplateId="LoadingFeedbackTemplateAssets" 
												EnableViewState="true"
												GroupBySortImageHeight="10" 
												GroupBySortImageWidth="10" 
												GroupBySortDescendingImageUrl="group_desc.gif" 
												GroupBySortAscendingImageUrl="group_asc.gif" 
												GroupingNotificationTextCssClass="GridHeaderText" 
												AlternatingRowCssClass="AlternatingRowCategory" 
												IndentCellWidth="22" 
												TreeLineImageHeight="19" 
												TreeLineImageWidth="20" 
												TreeLineImagesFolderUrl="images/lines/" 
												PagerImagesFolderUrl="images/pager/" 
												ImagesBaseUrl="images/" 
												PreExpandOnGroup="True" 
												GroupingPageSize="5" 
												PagerTextCssClass="GridFooterTextCategory" 
												PagerStyle="Numbered" 
												PageSize="1000" 
												GroupByTextCssClass="GroupByText" 
												GroupByCssClass="GroupByCell" 
												FooterCssClass="GridFooter" 
												HeaderCssClass="GridHeader" 
												SearchOnKeyPress="true" 
												SearchTextCssClass="GridHeaderText" 
												AllowEditing="true" 
												AllowSorting="False"
												ShowSearchBox="false" 
												ShowHeader="false" 
												ShowFooter="true" 
												CssClass="Grid" 
												RunningMode="callback" 
												ScrollBarWidth="15" 
												AllowPaging="true" >
<ClientEvents>
    <Load EventHandler="GridAssets_onLoad" />
  </ClientEvents>

<ClientTemplates>
<ComponentArt:ClientTemplate Id="EditTemplateAssets">
           <a href="javascript:DownloadAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> | <a href="javascript:AddToCarousel('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/3Drafts.gif" alt="Edit" border=0></a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateAssets">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateAssets">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>   
          <ComponentArt:ClientTemplate Id="TypeImageTemplateAssets">
            <div style= "WIDTH: 50px; HEIGHT: 50px; "><img src="## DataItem.GetMember("mainimagesource").Value ##" border="0" ></div>
          </ComponentArt:ClientTemplate>    
          <ComponentArt:ClientTemplate Id="TypeIconTemplateAssets">
          <div style="TEXT-ALIGN: left"><img src="## getURLPreviewIcon() ####DataItem.GetMember("media_type").Value ##" border="0"  > 
            &nbsp;## DataItem.GetMember("media_type_name").Value ##</div>
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplateAssets">
            <A href="javascript:CopyTags('## DataItem.ClientId ##');"><img border=0 src="images/copytags.gif"></a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="LookupCategoryTemplateAssets">
            <A href="javascript:GotoLocation('0','## DataItem.GetMember("projectid2").Value ##');">## DataItem.GetMember("projectname2").Value ##</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="LookupKeywordsTemplateAssets">
            <table style="font-size:10px;font-family:Verdana;vertical-align:text-top;" ><tr><td valign=top ><img border=0 src="images/moreinfo.gif" style="CURSOR: hand" alt="## DataItem.GetMember("search_keyword_new").Value ## ## DataItem.GetMember("search_ukeyword_new").Value ##"></td><Td><div style="overflow:hidden;height:25px;vertical-align:text-top;">## DataItem.GetMember("search_keyword_new").Value ##<br>## DataItem.GetMember("search_ukeyword_new").Value ##</div></td></tr></table>
  </ComponentArt:ClientTemplate>                         
          <ComponentArt:ClientTemplate Id="LookupCategoryIconTemplateAssets">
            <img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif">
          </ComponentArt:ClientTemplate>                    
<componentart:ClientTemplate ID="LoadingFeedbackTemplateAssets">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>                                 
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField"  DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeImageTemplateAssets" dataField="mainimagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="52" FixedWidth="True" />
<ComponentArt:GridColumn DataCellCssClass="DataCell" Align="Center" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplateAssets" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="16" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="false" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" visible="false" AllowEditing="false" HeadingText="UDF's" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="search_ukeyword_new" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="false" DataCellClientTemplateId="LookupKeywordsTemplateAssets" HeadingText="Keywords" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="search_keyword_new" ></componentart:GridColumn>
<ComponentArt:GridColumn AllowEditing="false" DataCellClientTemplateId="TypeIconTemplateAssets" dataField="imagesource" DataCellCssClass="DataCell" HeadingText="Type" AllowGrouping="false" Width="110" />
<componentart:GridColumn DataCellCssClass="DataCell" Visible="False" HeadingText="Type" AllowEditing="false" Width="110" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Updated" AllowEditing="false" Width="110" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Security Level" AllowEditing="false" Width="80" SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid2"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="mainimagesource"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="media_type"></componentart:GridColumn>

</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
      
      
      
      
      
      
      
      
      

</div>      
      
      

      
      
      
      </div> <!-- #BottomPane -->
      
      
       </div> <!-- #RightPane -->
  
</div> <!-- #MySplitter -->
      
      
   
      
<div style="height:4px; PADDING-RIGHT:5px; BORDER-TOP:#b7b4b4 1px solid; PADDING-LEFT:5px; FONT-WEIGHT:normal; FONT-SIZE:10px;   BORDER-BOTTOM:#b7b4b4 1px solid; FONT-FAMILY:verdana; BACKGROUND-COLOR:#e4e4e4;position:relative;"></div>
													
						
<br>
<div style="WIDTH: 100%;TEXT-ALIGN: left">
<asp:Button Runat=server ID=Button1 Text="Append to Selected"></asp:Button>&nbsp;<asp:Button Runat=server ID="Button2" Text="Overwrite Selected"></asp:Button></div><br><br>

						
						
						
						
						
						
						
						
						
						
						
						
					</div>
				</div>
				<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
					<div style="TEXT-ALIGN:right;display:none;"><br>
					<input type="button" onclick="window.close();" value="Close" >
					</div>
				</div>
				
				
<input type=hidden id="typemod">				
				
<script>							
function BrowseOnSingleClickAssets(item)
  {
	var itemvaluetmp;
	var itemvaluenametmp;
	var itemvaluefiletypetmp;
	itemvaluetmp = item.GetMember('asset_id').Value;
	itemvaluenametmp = item.GetMember('name').Value;
	itemvaluefiletypetmp = item.GetMember('imagesource').Value;
		<% Response.Write(AssetImage.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);

	return true;
  }  

function Keyword_PopUp(strUrl, popupName, intHeight, intWidth)
{
	if (strUrl == null || strUrl.Length <= 0)
		return;

	var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=no,resizable=yes,toolbar=no";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;

	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures, false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
}



  
function RefreshKeywordServices()
{


		<% Response.Write(CALLBACKKeywordsServices.ClientID) %>.Callback('Refresh');

	
}

function RefreshKeywordMediaType()
{
	
		<% Response.Write(CALLBACKKeywordsMediaType.ClientID) %>.Callback('Refresh');


	
}

function RefreshKeywordIllustType()
{
	
	<% Response.Write(CALLBACKKeywordsIllustType.ClientID) %>.Callback('Refresh');


}

  
</script>	
				
				
				
				
				
				
			</div>
		</form>
	</body>
</HTML>
