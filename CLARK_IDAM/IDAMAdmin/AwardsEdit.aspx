<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.AwardsEdit" ValidateRequest="False" CodeBehind="AwardsEdit.aspx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Awards</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
		<link href="treeStyle.css" type="text/css" rel="stylesheet">
		<link href="navStyle.css" type="text/css" rel="stylesheet">
		<link href="tabStripStyle.css" type="text/css" rel="stylesheet">
		<link href="navBarStyle.css" type="text/css" rel="stylesheet">
		<link href="calendarStyle.css" type="text/css" rel="stylesheet" />
		<style>BODY { MARGIN: 0px }
		</style>
	</HEAD>
	<body onload="this.focus">
		<form id="Form" method="post" runat="server">
			<script type="text/javascript">
  
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridKeywords.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    GridKeywords.Edit(GridKeywords.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    GridKeywords.EditComplete();     
  }

  function insertRow()
  {
    GridKeywords.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    GridKeywords.Delete(GridKeywords.GetRowFromClientId(rowId)); 
  }

<%if request.querystring("newid") <> "" then%>
//window.opener.NavigateToPage('User');
//window.close();
<%end if%>


function onPickerChange(picker)
      {
        <% Response.Write(Calendar1.ClientID) %>.SetSelectedDate(picker.GetSelectedDate());
      }
      function onPickerMouseUp()
      {
        if (<% Response.Write(Calendar1.ClientID) %>.PopUpObjectShowing)
        {
          event.cancelBubble=true;
          event.returnValue=false;
          return false;
        }
        else
        {
          return true;
        }
      }
      function OkClick()
      {
      
        <% Response.Write(Picker1.ClientID) %>.SetSelectedDate(<% Response.Write(Calendar1.ClientID) %>.GetSelectedDate());
        <% Response.Write(Calendar1.ClientID) %>.Hide();
      }
      function CancelClick()
      {
        <% Response.Write(Calendar1.ClientID) %>.Hide();
      }



function onPicker2Change(picker)
      {
        <% Response.Write(Calendar2.ClientID) %>.SetSelectedDate(picker2.GetSelectedDate());
      }
      function onPicker2MouseUp()
      {
        if (<% Response.Write(Calendar2.ClientID) %>.PopUpObjectShowing)
        {
          event.cancelBubble=true;
          event.returnValue=false;
          return false;
        }
        else
        {
          return true;
        }
      }
      function OkClick2()
      {
        <% Response.Write(Picker2.ClientID) %>.SetSelectedDate(<% Response.Write(Calendar2.ClientID) %>.GetSelectedDate());
        <% Response.Write(Calendar2.ClientID) %>.Hide();
      }
      function CancelClick2()
      {
        <% Response.Write(Calendar2.ClientID) %>.Hide();
      }


			</script>

			<input type="hidden" name="awardsid" id="awardsid" value="<%response.write (request.querystring("ID"))%>">
			
			
			
			<div style="padding:20px;position:relative;">
			
			<div style="">
			<div style="padding:10px;border:1px solid;background-color:white;">
			<table><tr><td><img src="images/31normal.gif"></td><td>Create/Edit Awards Items</td></tr></table>
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Create an item by entering the information below.</div><br>
  <div style="">
  
  
  
  <COMPONENTART:TABSTRIP id="TabStripCategory" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataAwards.xml"
								CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" FillWidth="True" Width="100%" DefaultItemLookId="DefaultTabLook"
								DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
								<ItemLooks>
									<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
									<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
								</ItemLooks>
							</COMPONENTART:TABSTRIP>

							<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageEventPage" runat="server">
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="User_General">
									<DIV style="BORDER-RIGHT: #b7b4b4 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #b7b4b4 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: normal; FONT-SIZE: 10px; PADDING-BOTTOM: 5px; BORDER-LEFT: #b7b4b4 1px solid; COLOR: #3f3f3f; PADDING-TOP: 5px; BORDER-BOTTOM: #b7b4b4 1px solid; FONT-FAMILY: verdana; BACKGROUND-COLOR: #e4e4e4">Enter 
										information for this item.</DIV>
									<BR>
				
									
								
<table border="0" width="100%" id="table1" style="FONT-SIZE: 8pt; FONT-FAMILY: verdana;height=25px;"	>
<tr>
		<td class="PageContent" width="100" align="left" valign="top" nowrap>
			<font face="Verdana" size="1">Type</font></td>
			<td class="PageContent"  align="left" valign="top" valign="top">
				<asp:DropDownList style="border:1px dotted;padding:5px;"  DataTextField="type_value" DataValueField="type_id"  ID="DropDown_News_Type"  Runat="server"></asp:DropDownList>
			</td>
		</tr>
		<tr>
			<td width="100" align="left" valign="top">Name
</td>
			<td align="left" valign="top">
<asp:TextBox  id="Textbox_Headline" Width="100%" CssClass="InputFieldMain100P" Height=25px runat="server"></asp:TextBox>
			</td>
		</tr>
		
		
		
		<tr>
			<td width="100" align="left" valign="top">Category
</td>
			<td align="left" valign="top">
<asp:TextBox  id="Textbox_Publication_Title" Width="100%" CssClass="InputFieldMain100P" Height=25px runat="server"></asp:TextBox>
			</td>
		</tr>
		
	
		
		
		
		
		
		
	
		<tr>
			<td width="100" align="left" valign="top">Description</td>
			<td align="left" valign="top">
<asp:TextBox style="height:120px;" TextMode="MultiLine" id="Textbox_Content" Width="100%" CssClass="InputFieldMain100P" runat="server"></asp:TextBox>
			</td>
		</tr>
		
		
		
		
		
		
			
		<tr>
			<td width="100" align="left" valign="top">Additional Information</td>
			<td align="left" valign="top">
<asp:TextBox style="height:80px;" TextMode="MultiLine" id="Textbox_Contact_Info" Width="100%" CssClass="InputFieldMain100P" runat="server"></asp:TextBox>
			</td>
		</tr>
		
		
		
		
		
		
		<tr>
			<td width="100" align="left" valign="top">Award Date</td>
			<td align="left" valign="top">

    <table onmouseup="onPickerMouseUp()" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td><ComponentArt:Calendar id="Picker1" 
          ControlType="Picker" 
          ClientSideOnSelectionChanged="onPickerChange" 
          PickerFormat="Custom" 
          PickerCustomFormat="MMMM d yyyy" 
          PickerCssClass="pickerForm" 
          runat="server" 
          /></td>
      <td style="font-size:10px;">&nbsp;</td>
      <td><img id="calendar_button" alt="" onclick="<% Response.Write(Calendar1.ClientID) %>.SetSelectedDate(<% Response.Write(Picker1.ClientID) %>.GetSelectedDate());<% Response.Write(Calendar1.ClientID) %>.Show();" class="calendar_button" src="images/btn_calendar.gif" width="25" height="22" /></td>
    </tr>
    </table>

    <ComponentArt:Calendar runat="server"
      id="Calendar1"
      AllowMonthSelection="false"
      AllowMultipleSelection="false"
      AllowWeekSelection="false"
      CalendarCssClass="calendar"
      CollapseOnSelect="false"
      ControlType="Calendar"
      DayCssClass="day"
      DayHeaderCssClass="dayheader"
      DayHoverCssClass="dayhover"
      DayNameFormat="FirstTwoLetters"
      OtherMonthDayCssClass="othermonthday"
      OtherMonthDayHoverCssClass="othermonthdayhover"
      PopUp="Custom"
      PopUpExpandControlId="calendar_button"
      SelectedDayCssClass="selectedday" 
      SelectedDayHoverCssClass="selecteddayhover"
      ShowCalendarTitle="false"
      SwapDuration="300"
      SwapSlide="Linear"      
      Width="200"
      >
      <HeaderClientTemplate>
        <div class="header">
          <span style="float:left;">
            <span class="headerbutton" onclick="Calendar1.GoPrevYear()"><img src="images/i_closed.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoPrevMonth()"><img src="images/cal_prevMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoToday()"><img src="images/bullet.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoNextMonth()"><img src="images/cal_nextMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoNextYear()"><img src="images/i_open.gif"></span>
          </span>
          <span style="float:right;">##Calendar1.FormatDate(Calendar1.VisibleDate,"MMMM yyyy")##</span>
        </div>
      </HeaderClientTemplate>
      <FooterClientTemplate>
        <div class="footer">
          <span style="float:right;">
            <input type="button" value="&nbsp; Ok &nbsp;" onclick="OkClick()" class="button" />
            <input type="button" value="Cancel" onclick="CancelClick()" class="button" />
          </span>
        </div>
      </FooterClientTemplate>
    </ComponentArt:Calendar>


			</td>
		</tr>
		
		
		
		
		
		
		
		
		<tr>
			<td width="100" align="left" valign="top">Pull Date</td>
			<td align="left" valign="top">

    <table onmouseup="onPicker2MouseUp()" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td><ComponentArt:Calendar id="Picker2" 
          ControlType="Picker" 
          ClientSideOnSelectionChanged="onPicker2Change" 
          PickerFormat="Custom" 
          PickerCustomFormat="MMMM d yyyy" 
          PickerCssClass="pickerForm" 
          runat="server" 
          /></td>
      <td style="font-size:10px;">&nbsp;</td>
      <td><img id="calendar_button" alt="" onclick="<% Response.Write(Calendar2.ClientID) %>.SetSelectedDate(<% Response.Write(Picker2.ClientID) %>.GetSelectedDate());<% Response.Write(Calendar2.ClientID) %>.Show();" class="calendar_button" src="images/btn_calendar.gif" width="25" height="22" /></td>
    </tr>
    </table>

    <ComponentArt:Calendar runat="server"
      id="Calendar2"
      AllowMonthSelection="false"
      AllowMultipleSelection="false"
      AllowWeekSelection="false"
      CalendarCssClass="calendar"
      CollapseOnSelect="false"
      ControlType="Calendar"
      DayCssClass="day"
      DayHeaderCssClass="dayheader"
      DayHoverCssClass="dayhover"
      DayNameFormat="FirstTwoLetters"
      OtherMonthDayCssClass="othermonthday"
      OtherMonthDayHoverCssClass="othermonthdayhover"
      PopUp="Custom"
      PopUpExpandControlId="calendar_button"
      SelectedDayCssClass="selectedday" 
      SelectedDayHoverCssClass="selecteddayhover"
      ShowCalendarTitle="false"
      SwapDuration="300"
      SwapSlide="Linear"      
      Width="200"
      >
      <HeaderClientTemplate>
        <div class="header">
          <span style="float:left;">
            <span class="headerbutton" onclick="Calendar2.GoPrevYear()"><img src="images/i_closed.gif"></span>
            <span class="headerbutton" onclick="Calendar2.GoPrevMonth()"><img src="images/cal_prevMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar2.GoToday()"><img src="images/bullet.gif"></span>
            <span class="headerbutton" onclick="Calendar2.GoNextMonth()"><img src="images/cal_nextMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar2.GoNextYear()"><img src="images/i_open.gif"></span>
          </span>
          <span style="float:right;">##Calendar2.FormatDate(Calendar2.VisibleDate,"MMMM yyyy")##</span>
        </div>
      </HeaderClientTemplate>
      <FooterClientTemplate>
        <div class="footer">
          <span style="float:right;">
            <input type="button" value="&nbsp; Ok &nbsp;" onclick="OkClick2()" class="button" />
            <input type="button" value="Cancel" onclick="CancelClick2()" class="button" />
          </span>
        </div>
      </FooterClientTemplate>
    </ComponentArt:Calendar>



			</td>
		</tr>
		
				
			
		<tr>
			<td width="100" align="left" valign="top"><b>Image</b></td>
			<td align="left" valign="top"><hr noshade width=100% size=1
		</td>
		</tr>
				
		
				
			
		<tr>
			<td width="100" align="left" valign="top">Main Image</td>
			<td align="left" valign="top">



	
	
				
<table border="0" width="100%" id="" style="FONT-SIZE: 8pt; FONT-FAMILY: verdana">
	<tr>
		<td width=200 valign="top">
		<%if request.querystring("Id") <> "" and  reqPicture = "1" then%>
		<div style="padding:5px;border:1px dotted #E0DCDC;">
		<img border="0" src="<%response.write(WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase)%>?id=<%response.write(request.querystring("Id"))%>&instance=<%response.write(IDAMInstance)%>&type=awards&size=1&width=200&height=90&cache=1" ></div>
		<br>
		<asp:Button id="Button_RemoveImage" runat="server" Text="Remove Image"></asp:Button><br><br>

		<%end if%>

		Add/Modify Image<br>
        </font>
 <font face="Arial" size="2">
 <Upload:InputFile id="thefile" Class="InputFieldFileMain" name="thefile" size="14" runat="server" />
		</font>
		
		</td>
	</tr>
</table>


			</td>
		</tr>
		
		
				
		
		
		
		
		
		
	</table>
	
	
	

	
<br>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
			</ComponentArt:PageView>
			
		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Awards_UDF">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Edit User Defined Fields.</div><br>
<asp:Literal ID="Literal_UDFMAIN" Runat=server></asp:Literal>
			
<br>
																
																				
																				
					
			
		</ComponentArt:PageView>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Related_Projects">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Add related projects to this news item.</div><br>

			
<br>
																
				<table cellspacing="0" cellpadding="3" border="0" >
				<tr>
					<td class="PageContent" width="200" valign=top>
<!--available list-->
<b>Available Projects</b><br>


<table style="FONT-SIZE: 8pt; FONT-FAMILY: verdana;height=25px;"><tr><td  align=left >Search Filter:</font></b><img src="images/spacer.gif" width=5 height=1></td><td align=left width=50 nowrap ><input Class="InputFieldMain100P" type=text value="" id=search_filter  style="width:70px" onkeyup="javascript:<%response.write( GridProjectsAvailable.ClientId)%>.Filter('name LIKE \'%' + this.value + '%\'');"></td></tr></table>
<COMPONENTART:GRID 
id="GridProjectsAvailable" runat="server" 
pagerposition="2"

ScrollBar="Off"
Sort="Name asc"
Height="10" Width="150" 
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategory" 
AlternatingRowCssClass="AlternatingRow" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PagerTextCssClass="GridFooterText" 
PagerStyle="Numbered" 
PageSize="10" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeaderCategory" 
SearchTextCssClass="GridHeaderText"  
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
AllowPaging="true"
AutoFocusSearchBox="false">
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategory">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategory">
            <img src="images/projcat16.jpg" border="0" > 
          </ComponentArt:ClientTemplate>                            
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
            EditFieldCssClass="EditDataField"
            EditCommandClientTemplateId="EditCommandTemplateCategory"
            InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="projectid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataCellClientTemplateId="TypeIconTemplateCategory" DataField="projectid" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Name"   DataCellCssClass="LastDataCellPostings" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="Name" fixedWidth="True" Width="200" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>

					</td>
					<td class="PageContent" width="120" valign= middle>
		
					<asp:Button id="btnAddRelatedProject" runat="server" Text=" >> "></asp:Button><br><br><br><br>
					<asp:Button id="btnRemoveRelatedProject" runat="server" Text=" << "></asp:Button>
					</td>
					<td class="PageContent" width="200" valign=top>
					
					
					
					
<!--active list-->
<b>Related Projects</b><br>

<table style="FONT-SIZE: 8pt; FONT-FAMILY: verdana;height=25px;"><tr><td width="100%" align=left >Search Filter:</font></b><img src="images/spacer.gif" width=5 height=1></td><td align=left width=50 nowrap ><input Class="InputFieldMain100P" type=text value="" id=search_filter  style="width:70px" onkeyup="javascript:<%response.write( GridProjectsRelated.ClientId)%>.Filter('name LIKE \'%' + this.value + '%\'');"></td></tr></table>
<COMPONENTART:GRID id="GridProjectsRelated" runat="server" 
autoPostBackonDelete="true"
pagerposition="2"
ScrollBar="Off"
Sort="Name asc"
Height="10" Width="200" 
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategoryActive" 
AlternatingRowCssClass="AlternatingRow" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PagerTextCssClass="GridFooterText" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeaderCategory" 
SearchOnKeyPress="false" 
SearchTextCssClass="GridHeaderText"  
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
AllowPaging="false"
cachecontent="false" 
PagerStyle="Numbered" 
PageSize="500"  >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategoryActive">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateCategoryActive">
          <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TypeIconTemplateActive">
            <img src="images/projcat16.jpg" border="0" > 
          </ComponentArt:ClientTemplate>                                
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" 
 EditFieldCssClass="EditDataField" 
 EditCommandClientTemplateId="EditCommandTemplateCategoryActive" 
 DataKeyField="projectid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataCellClientTemplateId="TypeIconTemplateActive" DataField="projectid" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Name"  DataCellCssClass="LastDataCellPostings" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" FixedWidth="True" Width="150"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>

					</td>					
				</tr>
			</table>																	
											

		</ComponentArt:PageView>		
	</ComponentArt:MultiPage>
						<div style="PADDING-BOTTOM:10px;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
										<div style="text-align:right;"><br>
								<asp:Button id="btnProjectSave" runat="server" Text="Save"></asp:Button><img src="images/spacer.gif" width=5 height=1><input type="button" onclick="window.opener.RefreshNewsItems();window.close();" value="Close" />
								</div>
						</div>
			</div>
			</div>
			</div>

			<script language="javascript">
TabStripCategory.SelectTabById('<%response.write (request.querystring("tab"))%>');
			</script>
		</form>
	</body>
</HTML>
