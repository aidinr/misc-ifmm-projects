<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.ProjectDetails"  CodeBehind="ProjectDetails.ascx.vb" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="Upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>
<SCRIPT LANGUAGE="JavaScript" SRC="js/CalendarPopup.js"></SCRIPT>


<script language="javascript">

function closealltags()
{
 $("#JQPDPRx").accordion("option", "active", false);
 $("#JQPDPCSx").accordion("option", "active", false);
 $("#JQPDAIx").accordion("option", "active", false);
 $("#JQPDIx").accordion("option", "active", false);
 $("#JQPDUDFx").accordion("option", "active", false);
 $("#JQPDPDx").accordion("option", "active", false);
 $("#JQPDPIx").accordion("option", "active", false);
 $("#JQPDPKx").accordion("option", "active", false);
}
function openalltags()
{
 $("#JQPDPRx").accordion("option", "active", 0);
 $("#JQPDPCSx").accordion("option", "active", 0);
 $("#JQPDAIx").accordion("option", "active", 0);
 $("#JQPDIx").accordion("option", "active", 0);
 $("#JQPDUDFx").accordion("option", "active", 0);
 $("#JQPDPDx").accordion("option", "active", 0);
 $("#JQPDPIx").accordion("option", "active", 0);
 $("#JQPDPKx").accordion("option", "active", 0);
}    


 
function onPickerChange(picker)
      {
        <% Response.Write(Calendar1.ClientID) %>.SetSelectedDate(picker.GetSelectedDate());
      }
      function onPickerMouseUp()
      {
        if (<% Response.Write(Calendar1.ClientID) %>.PopUpObjectShowing)
        {
          event.cancelBubble=true;
          event.returnValue=false;
          return false;
        }
        else
        {
          return true;
        }
      }
      function OkClick()
      {
        <% Response.Write(Picker1.ClientID) %>.SetSelectedDate(<% Response.Write(Calendar1.ClientID) %>.GetSelectedDate());
        <% Response.Write(Calendar1.ClientID) %>.Hide();
      }
      function CancelClick()
      {
        <% Response.Write(Calendar1.ClientID) %>.Hide();
      }
</script>

<style>
.projectcentertest
{
position:relative;
width:100%;
padding-left:5px;padding-right:5px;
/*overflow: -moz-scrollbars-vertical; */
}
</style>

<table width=100% cellpadding=0 cellspacing=0><tr><td width=100% align=left valign=top >
<div id="projectcenter" class="projectcentertestxx">
<div class="projectcentertestxx" >
      
<script>
    $(function () {
        $("#JQPDIx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQPDIx', $("#JQPDIx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQPDIx') == "false") {
            $("#JQPDIx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQPDIx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Project Information</a></h3> 
								<div class="SnapProjectsWrapperxx">
								<div class="SnapProjectsxx" >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px">								

<table border="0" width="100%" id="table1">
		<tr>
			<td width="100" align="left" valign="top">Name
</td>
			<td align="left" valign="top">
<asp:TextBox  id="Textbox_Name" Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox>
			</td>
		</tr>
		
		
		<tr>
		<td class="PageContent" width="100" align="left" valign="top" nowrap>
			<font face="Verdana" size="1">Security Level</font></td>
			<td class="PageContent"  align="left" valign="top" valign="top">
				<asp:DropDownList ID="SecurityLevel" width="180" Runat="server">
					<asp:ListItem Value="0">Administrator</asp:ListItem>
					<asp:ListItem Value="1">Internal Use</asp:ListItem>
					<asp:ListItem Value="2">Client Use</asp:ListItem>
					<asp:ListItem Value="3">Public</asp:ListItem>
				</asp:DropDownList>
			</td>
		</tr>
		
		
		<tr>
			<td width="100" align="left" valign="top">Client</td>
			<td align="left" valign="top">
			<asp:DropDownList style="border:1px dotted;padding:5px;" id="DropdownClient" DataTextField="name" DataValueField="name" runat="server"></asp:DropDownList>
			</td>
		</tr>
		<tr>
			<td width="100" align="left" valign="top">Project Number</td>
			<td align="left" valign="top">
<asp:TextBox  id="Textbox_Projectnumber" Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td width="100" align="left" valign="top">Project Address</td>
			<td align="left" valign="top">
<asp:TextBox height="80px" TextMode="MultiLine" id="Textbox_Address" Width="150px" CssClass="InputFieldMain100P" runat="server"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td width="100" align="left" valign="top">City</td>
			<td align="left" valign="top">
<asp:TextBox id="Textbox_City" Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td width="100" align="left" valign="top">State</td>
			<td align="left" valign="top">
			<asp:DropDownList style="border:1px dotted;padding:5px;" id=DropDownList_State DataTextField="name" DataValueField="state_id" runat="server"></asp:DropDownList>
			</td>
		</tr>
		<tr>
			<td width="100" align="left" valign="top">Zip</td>
			<td align="left" valign="top">
<asp:TextBox id="Textbox_Zip"  Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox>
			</td>
		</tr>
</table>
<!--BEGIN inserted system udf country-->
<asp:Literal ID="IDAM_COUNTRY" Runat=server Visible=True ></asp:Literal>
<!--END inserted system udf country-->
<table border="0" width="100%" id="table1">
		<tr>
			<td width="100" align="left" valign="top">Start Date</td>
			<td align="left" valign="top">

    <table onmouseup="onPickerMouseUp()" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td><ComponentArt:Calendar id="Picker1" 
          ControlType="Picker" 
          ClientSideOnSelectionChanged="onPickerChange" 
          PickerFormat="Custom" 
          PickerCustomFormat="MMMM d yyyy" 
          PickerCssClass="picker" 
          runat="server" /></td>
      <td style="font-size:10px;">&nbsp;</td>
      <td><img id="calendar_button" alt="" onclick="<% Response.Write(Calendar1.ClientID) %>.SetSelectedDate(<% Response.Write(Picker1.ClientID) %>.GetSelectedDate());<% Response.Write(Calendar1.ClientID) %>.Show();" class="calendar_button" src="images/btn_calendar.gif" width="25" height="22" /></td>
    </tr>
    </table>

    <ComponentArt:Calendar runat="server"
      id="Calendar1"
      AllowMonthSelection="false"
      AllowMultipleSelection="false"
      AllowWeekSelection="false"
      CalendarCssClass="calendar"
      CollapseOnSelect="false"
      ControlType="Calendar"
      DayCssClass="day"
      DayHeaderCssClass="dayheader"
      DayHoverCssClass="dayhover"
      DayNameFormat="FirstTwoLetters"
      OtherMonthDayCssClass="othermonthday"
      OtherMonthDayHoverCssClass="othermonthdayhover"
      PopUp="Custom"
      PopUpExpandControlId="calendar_button"

      SelectedDayCssClass="selectedday" 
      SelectedDayHoverCssClass="selecteddayhover"
      ShowCalendarTitle="false"
      SwapDuration="300"
      SwapSlide="Linear"      
      Width="200"
      >
      <HeaderClientTemplate>
        <div class="header">
          <span style="float:left;">
            <span class="headerbutton" onclick="<%=ctype(Page,IdamAdmin.IDAM).RootClientID%>__ctl0_SNAPProjectInfo_Calendar1.GoPrevYear()"><img src="images/i_closed.gif"></span>
            <span class="headerbutton" onclick="<%=ctype(Page,IdamAdmin.IDAM).RootClientID%>__ctl0_SNAPProjectInfo_Calendar1.GoPrevMonth()"><img src="images/cal_prevMonth.gif"></span>
            <span class="headerbutton" onclick="<%=ctype(Page,IdamAdmin.IDAM).RootClientID%>__ctl0_SNAPProjectInfo_Calendar1.GoToday()"><img src="images/bullet.gif"></span>
            <span class="headerbutton" onclick="<%=ctype(Page,IdamAdmin.IDAM).RootClientID%>__ctl0_SNAPProjectInfo_Calendar1.GoNextMonth()"><img src="images/cal_nextMonth.gif"></span>
            <span class="headerbutton" onclick="<%=ctype(Page,IdamAdmin.IDAM).RootClientID%>__ctl0_SNAPProjectInfo_Calendar1.GoNextYear()"><img src="images/i_open.gif"></span>
          </span>
          <span style="float:right;">##ctl04_ctl00_SNAPProjectInfo_Calendar1.FormatDate(ctl04_ctl00_SNAPProjectInfo_Calendar1.VisibleDate,"MMMM yyyy")##</span>
        </div>
      </HeaderClientTemplate>
      <FooterClientTemplate>
        <div class="footer">
          <span style="float:right;">
            <input type="button" value="&nbsp; Ok &nbsp;" onclick="OkClick()" class="button" />
            <input type="button" value="Cancel" onclick="CancelClick()" class="button" />
          </span>
        </div>
      </FooterClientTemplate>
    </ComponentArt:Calendar>


			</td>
		</tr>
		
				<tr>
			<td width="100" align="left" valign="top">Status</td>
			<td align="left" valign="top">
<asp:DropDownList style="border:1px dotted;padding:5px;" id="Dropdownlist_Phase" DataTextField="keyname" DataValueField="keyid" runat="server"></asp:DropDownList>
			</td>
		</tr>		
	</table>
<asp:Literal ID="ltlIDAM_UDF_GENERAL_INFO" Runat=server Visible=True ></asp:Literal>
	
	<br><br>
	

	<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("PROJECT_GENERAL_EDIT",spWOType,spWOID,spProjectID) Then%>
	<input type='button' Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:idamaction.value='editProjectDetails';PageForm.submit();return true;"  width=40 value="Save" id="btnsaveSNAPProjectInfo" name="btnsaveSNAPProjectInfo">
	<%end if%>
</div><!--padding-->

								</div><!--SnapProjects-->
								</div><!--SnapProjectsWrapper-->
								</div>
							
					
							
							
							
							
							
							
							

   
<script>
    $(function () {
        $("#JQPDAIx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQPDAIx', $("#JQPDAIx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQPDAIx') == "false") {
            $("#JQPDAIx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQPDAIx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Additional Project Information</a></h3> 
								<div class="SnapProjectsWrapperxx">
								<div class="SnapProjectsxx" >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px">
Project Data<br>
<asp:TextBox  id="TextboxProjectData"  Wrap=False  style="height:220px;overflow: scroll;overflow-y: scroll;overflow-x: scroll; overflow:moz-scrollbars-both;" height="224px" TextMode="MultiLine" CssClass="InputFieldMain100P" runat="server"></asp:TextBox>														
<br>
Awards<br>
<asp:TextBox id="TextboxAwards" Wrap=False  style="height:220px;overflow: scroll;overflow-y: scroll;overflow-x: scroll; overflow:moz-scrollbars-both;" height="224px" TextMode="MultiLine" CssClass="InputFieldMain100P" runat="server"></asp:TextBox>
<br><br>
<b>Project URL</b><br><br>
URL<br>
<asp:TextBox id="TextboxURL"  height="15px" TextMode=SingleLine  CssClass="InputFieldMain" runat="server"></asp:TextBox>
<br>
URL Caption<br>
<asp:TextBox id="TextboxCaption"  height="15px" TextMode=SingleLine  CssClass="InputFieldMain" runat="server"></asp:TextBox>
<br><br>
<b>View Options</b><br><br>
<asp:CheckBox Runat="server" ID="CheckboxPublic" ></asp:CheckBox>Publish
<br><br>
<asp:CheckBox Runat="server" ID="CheckboxHomepage"></asp:CheckBox>Favorite
<br><br>
<asp:CheckBox Runat="server" ID="CheckboxCaseStudy"></asp:CheckBox>Case Study
<br><br>

<asp:Literal ID="ltlIDAM_UDF_ProjectInfoAdditional" Runat=server Visible=True ></asp:Literal>

<% If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("PROJECT_VIEWOPTIONS_EDIT",spWOType,spWOID,spProjectID) Then%>
<br><br>
<input type='button' Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:idamaction.value='editProjectDetails';PageForm.submit();return true;"  width=40 value="Save" id="upload" name="upload">
<% end if%>
</div>


								</div><!--SnapProjects-->
								</div><!--SnapProjectsWrapper-->
								</div>













						

   
<script>
    $(function () {
        $("#JQPDUDFx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQPDUDFx', $("#JQPDUDFx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQPDUDFx') == "false") {
            $("#JQPDUDFx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQPDUDFx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">User Defined Fields</a></h3> 
								<div class="SnapProjectsWrapperxx">
								<div class="SnapProjectsxx" >
<img src="images/spacer.gif" width=188 height=1><br>


 <div style=" padding-bottom:20px;font-family: verdana;
                        color: #3F3F3F; font-size: 10px; font-weight: normal;">
                        <div style="float:left;" class="ui-state-default ui-corner-all" title=".ui-icon-notice"><span class="ui-icon ui-icon-notice"></span></div><div style="float:left;position:relative;top:3px;padding-left:5px;"> Modify the user defined fields. Note; if tabs are available, please be sure to save your work in between navigating to new tabs.</div>
                        </div>
                    <br />
                   
                   <div id="UDFTabStrip" style="border:0px;"><ul>

                   <asp:Literal ID="ltrlUDFTabStrip" runat=server></asp:Literal>

                   </ul>                  
                   <div id="tabs-1"> 
                        
                        
                           
                                <ComponentArt:CallBack ID="CallbackUDFTab" runat="server" CacheContent="false">
                                    <Content>
                                        <asp:Literal ID="Literal_UDFMAIN" runat="server"></asp:Literal>
                                    </Content>
                                    <LoadingPanelClientTemplate>
                                        <img height="16" src="images/spinner.gif" width="16" border="0">
                                        Rendering information...
                                    </LoadingPanelClientTemplate>
                                </ComponentArt:CallBack>
                           
                   </div>

                    </div>


                    <br /><br /><br />



	<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("PROJECT_GENERAL_EDIT",spWOType,spWOID,spProjectID) Then%>            
	<br><br>
	<input type='button' Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:idamaction.value='editProjectDetails';PageForm.submit();return true;"  width=40 value="Save" id="upload" name="upload">
	<%end if%>
</div><!--padding-->

								</div><!--SnapProjects-->
								</div><!--SnapProjectsWrapper-->
								</div>

							
<script>
    $(function () {
        $("#JQPDPIx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQPDPIx', $("#JQPDPIx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQPDPIx') == "false") {
            $("#JQPDPIx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQPDPIx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Project Image</a></h3> 
								<div class="SnapProjectsWrapperxx">
								<div class="SnapProjectsxx" >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px"><!--padding-->							
<table border="0" width="100%" id="">
	<tr>
		<td width=200 align="left" valign="top">
		<div style="padding:5px;border:1px dotted #E0DCDC;">
		<img border="0" src="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?id=<%response.write(request.querystring("Id"))%>&instance=<%response.write(IDAMInstance)%>&type=project&size=1&width=400&height=400&cache=1"></div>
		<br>
		
		        Upload Image:<br>
        </font>
 <font face="Arial" size="2">
 <% If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("PROJECT_GENERAL_EDIT", spWOType, spWOID, spProjectID) Then%>  
    <Upload:InputFile id="thefile" Class="InputFieldFileMain" name="thefile" size="14" runat="server" />
		<!--<input type="file" Class="InputFieldFileMain" size="14" name="thefileold" id="thefileold" runat="server" ></font>-->
<%end if%>

		
		
		
		
		</td>
		<td align="left" valign="top"></td>
	</tr>
	<tr>
		<td colspan="2" align="left" valign="top"></td>
	</tr>
</table>


	

	<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("PROJECT_GENERAL_EDIT",spWOType,spWOID,spProjectID) Then%>    
<br>
<input type='button' Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:idamaction.value='editProjectDetails';PageForm.submit();return true;"  width=40 value="Save" id="upload" name="upload">
<%end if%>
</div><!--padding-->
								</div><!--SnapProjects-->
								</div><!--SnapProjectsWrapper-->
								</div>
												
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
				
							
					
							
<script>
    $(function () {
        $("#JQPDPDx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQPDPDx', $("#JQPDPDx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQPDPDx') == "false") {
            $("#JQPDPDx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQPDPDx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Project Descriptions</a></h3> 
								<div >
								<div  >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px">

<table border="0" width="100%" id="tableudfs"><tr><td width="100" align="left" valign="top"><img src="images/spacer.gif" width="100" height="1"><br><div class="UDF_Title">Short Description</div></td><td width="100%" align="left" valign="top"><asp:TextBox  id="shortdescription"   MaxLength="255" height="80px" TextMode="MultiLine"  CssClass="InputFieldMain100P" runat="server"></asp:TextBox></td></tr></table>
<table border="0" width="100%" id="tableudfs"><tr><td width="100" align="left" valign="top"><img src="images/spacer.gif" width="100" height="1"><br><div class="UDF_Title">Long Description</div></td><td width="100%" align="left" valign="top"><asp:TextBox id="longdescription"  height="224px" TextMode="MultiLine" CssClass="InputFieldMain100P"	runat="server"></asp:TextBox></td></tr></table>
<asp:Literal ID="ltlIDAM_UDF_Description" Runat=server Visible=True ></asp:Literal>
<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("PROJECT_GENERAL_EDIT",spWOType,spWOID,spProjectID) Then%>   	
<Br><br><input type='button' Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:idamaction.value='editProjectDetails';PageForm.submit();return true;"  width=40 value="Save" id="upload" name="upload">
<%end if%>
</div></div>	
								
												
								</div><!--padding-->
								
								
								
								
							</div>
							
								
					
							
<script>
    $(function () {
        $("#JQPDPKx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQPDPKx', $("#JQPDPKx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQPDPKx') == "false") {
            $("#JQPDPKx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQPDPKx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Project Keywords</a></h3> 
								
								
								
								<div >
								<div  >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px">
<div style="width: 100%;float:left;">
<br><br>
<asp:Literal ID="LiteralKeywords1Title" Runat=server></asp:Literal>:<br><img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=Discipline','Keywords_Discipline','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsDiscipline" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="LiteralKeywords1" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div>
<div style="width: 100%;float:left;">
<br><br>
<asp:Literal ID="LiteralKeywords2Title" Runat=server></asp:Literal>:<br><img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=Keyword','Keywords_Keyword','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsKeyword" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="LiteralKeywords2" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div>									
<div style="width: 100%;float:left;">
<br><br>
<asp:Literal ID="LiteralKeywords3Title" Runat=server></asp:Literal>:<br><img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=Office','Keywords_Office','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsOffice" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="LiteralKeywords3" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div>
<div style="width: 100%;float:left;">




<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("PROJECT_ATTRIBUTES_EDIT",spWOType,spWOID,spProjectID) Then%>
<br><br>
<input type='button' Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:idamaction.value='editProjectDetails';PageForm.submit();return true;"  width=40 value="Save" id="upload" name="upload">
<%end if%>
</div>

													</div>
													</div>	

								</div><!--padding-->
								</div><!--End Keywords-->
							
											
<script>
    $(function () {
        $("#JQPDPCSx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQPDPCSx', $("#JQPDPCSx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQPDPCSx') == "false") {
            $("#JQPDPCSx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQPDPCSx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Case Study</a></h3> 
								<div >
								<div >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px">
Challenge
													<asp:TextBox  id="Textbox_Challenge"  height="124px" TextMode="MultiLine" Width=97% CssClass="InputFieldMain" runat="server"></asp:TextBox>
														
														<br>
														 
														Approach
													<asp:TextBox id="Textbox_Approach"  height="124px" TextMode="MultiLine" Width=97% CssClass="InputFieldMain"	runat="server"></asp:TextBox>
																	
														<Br>
														
														
														Solution
													<asp:TextBox id="Textbox_Solution"  height="124px" TextMode="MultiLine" Width=97% CssClass="InputFieldMain"	runat="server"></asp:TextBox>
																	
														<Br>
														
														

														
														<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("PROJECT_CASESTUDY_EDIT",spWOType,spWOID,spProjectID) Then%>
														<br><input type='button' Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:idamaction.value='editProjectDetails';PageForm.submit();return true;"  width=40 value="Save" id="upload" name="upload">
														<%end if%>
													</div></div>	
								
												
								</div><!--padding-->
								</div>
																
<script>
    $(function () {
        $("#JQPDPRx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQPDPRx', $("#JQPDPRx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQPDPRx') == "false") {
            $("#JQPDPRx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQPDPRx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Related Projects</a></h3> 
								<div >
								<div >
								<img src="images/spacer.gif" width=188 height=1>
																			
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Change related projects by selecting a project from the list and adding to the active list.</div><br>

<div style="padding:5px">

			<table cellspacing="0" cellpadding="3" border="0" width="250">
				<tr>
					<td class="PageContentx" width="250" valign=top>
<!--available list-->
<b>Available Projects</b><br>



<COMPONENTART:GRID 
id="GridProjectsAvailable" runat="server" 
pagerposition="2"
ScrollBar="Off"
Sort="Name asc"
Height="10" Width="250" 
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategory" 
AlternatingRowCssClass="AlternatingRow" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PagerTextCssClass="GridFooterText" 
PagerStyle="Numbered" 
PageSize="10" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeaderCategory" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText"  
ShowSearchBox="true" 
ShowHeader="true" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
AllowPaging="true"
AutoFocusSearchBox="false">
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategory">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategory">
            <img src="images/projcat16.jpg" border="0" > 
          </ComponentArt:ClientTemplate>                            
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
            EditFieldCssClass="EditDataField"
            EditCommandClientTemplateId="EditCommandTemplateCategory"
            InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="projectid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataCellClientTemplateId="TypeIconTemplateCategory" DataField="projectid" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Name"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="Name" Width="150" DataCellCssClass="LastDataCellPostings"></componentart:GridColumn>
<componentart:GridColumn Visible="false" AllowEditing="false" HeadingText="Description"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="Description" Width="50" DataCellCssClass="LastDataCellPostings"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
					</td>
					<td class="PageContent" width="120" valign= middle>
					
					
		
					<asp:Button id="btnAddRelatedProject" runat="server" 

					Text="     Add >> "></asp:Button><br><br><br><br>
					<asp:Button id="btnRemoveRelatedProject" runat="server" 

					Text="<< remove"></asp:Button>
					</td>
					<td class="PageContent" width="100%" valign=top>
					
					
					
					
<!--active list-->
<b>Related Projects</b><br><br><br>


<COMPONENTART:GRID id="GridProjectsRelated" runat="server" 
autoPostBackonDelete="true"
ClientSideOnUpdate="onUpdateRP"
 AutoCallBackOnUpdate="true"
pagerposition="2"
ScrollBar="Off"
Sort="ordering asc"
Height="10" Width="300" 
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategoryActive" 
AlternatingRowCssClass="AlternatingRow" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PagerTextCssClass="GridFooterText" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeaderCategory" 
SearchOnKeyPress="false" 
SearchTextCssClass="GridHeaderText"  
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
AllowPaging="false"
cachecontent="false" 
PagerStyle="Numbered" 
PageSize="500"  >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategoryActive">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateCategoryActive">
          <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TypeIconTemplateActive">
            <img src="images/projcat16.jpg" border="0" > 
          </ComponentArt:ClientTemplate>                                
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" 
 EditFieldCssClass="EditDataField" 
 EditCommandClientTemplateId="EditCommandTemplateCategoryActive" 
 DataKeyField="projectid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataCellClientTemplateId="TypeIconTemplateActive" DataField="projectid" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Name" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" FixedWidth="false" Width="150" ></componentart:GridColumn>
<componentart:GridColumn Visible="false" AllowEditing="false" HeadingText="Description" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="description" width="30" FixedWidth="true"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="true" HeadingText="Order" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="ordering" width="50" FixedWidth="true" DataCellCssClass="LastDataCellPostings"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
					</td>					
				</tr>
			</table>									
						
								</div>												
								</div>
								</div><!--padding-->
								</div>
											
							
</div>						
</div><!--bottom padding-->		
</td></tr></table>




<script language="javascript">

<!--

function Keyword_PopUp(strUrl, popupName, intHeight, intWidth)
{
	if (strUrl == null || strUrl.Length <= 0)
		return;

	var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=no,resizable=yes,toolbar=no";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;

	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures, false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
}

function RefreshKeywordDiscipline()
{
		<% Response.Write(CALLBACKKeywordsDiscipline.ClientID) %>.Callback('Refresh');
    alert('Keywords refreshed.');
}

function RefreshKeywordKeyword()
{
		<% Response.Write(CALLBACKKeywordsKeyword.ClientID) %>.Callback('Refresh');
		alert('Keywords refreshed.');
		return false;
    
}

function RefreshKeywordOffice()
{
		<% Response.Write(CALLBACKKeywordsOffice.ClientID) %>.Callback('Refresh');
    alert('Keywords refreshed.');
}



//-->
function UDFTabCallback(id)
{
<%Response.Write(CallbackUDFTab.ClientID)%>.Callback(id);
}

   



  function onUpdateRP(item)
  {
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
  }
  
</script>	





		