﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="IDAM.aspx.vb" ValidateRequest="False" Inherits="IdamAdmin.IDAM" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
    <title>IDAM Version 7</title>

    <link href="splitterStyle.css" type="text/css" rel="stylesheet" />
    <link href="common/baseStyle.css" type="text/css" rel="stylesheet" />
    <link href="gridStyle.css" type="text/css" rel="stylesheet" />
    <link href="snapStyle.css" type="text/css" rel="stylesheet" />
    <link href="tabStyle.css" type="text/css" rel="stylesheet" />
    <link href="menuStyle.css" type="text/css" rel="stylesheet" />
    <link href="multipageStyle.css" type="text/css" rel="stylesheet" />
    <link href="treeStyle.css" type="text/css" rel="stylesheet" />
    <link href="navStyle.css" type="text/css" rel="stylesheet" />
    <link href="tabStripStyle.css" type="text/css" rel="stylesheet" />
    <link href="navBarStyle.css" type="text/css" rel="stylesheet" />
    <link href="calendarStyle.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="thickbox.css" type="text/css" />
    <link rel="stylesheet" href="combobox.css" type="text/css" />
    <link href="css/custom/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css"  media="all"/>
    <link href="colorbox.css" type="text/css" rel="stylesheet" />

    
<style type="text/css">
BODY
{
margin: 0px 0px 0px 0px;
}
.ui-layout-pane {
overflow: hidden !important;
}
.hidden { display: none; }
#idamcontent { position: relative; overflow: hidden; min-height:1600px; }
</style>
</head>
<body  style="background-color:White;border:0px;" ><!--class="ui-widget-content"-->
<script type="text/javascript" src="includes/CookieScripts.js"></script>
<script type="text/javascript" src="js/piclens_optimized.js"></script>
<script type="text/javascript" src="includes/JSLib.aspx"></script>
<script type="text/javascript" src="includes/JSLibNoCache.aspx"></script>
<script type="text/javascript" src="js/jquery.1.6.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.1.8.13.min.js"></script>
<script type="text/javascript" src="js/jquery.tools.min.scrollable142.js"></script>
<script type="text/javascript" src="js/jquery.layout.min.js" ></script>
<script type="text/javascript" src="js/jquery.effects.slide.js"></script>
<script type="text/javascript" src="js/jquery.multiSelect.js" ></script>
<script type="text/javascript" src="js/jquery.cookie.js" ></script>
<script type="text/javascript" src="js/jquery.colorbox-min.js" ></script>

<script>
    if ($.cookie('jqueryuithemeloaded') != null) {
        document.writeln('<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/' + $.cookie("jqueryuithemeloaded").replace(" ", "-").toLowerCase() + '/jquery-ui.css" rel="stylesheet" type="text/css"  media="all"/>');
    } else {

    $(document).ready(function () {
        $("#toolbarheading").removeClass("ui-widget-content").addClass("ui-accordion-header").addClass("ui-state-active");
        $("#logoheading").removeClass("ui-widget-content").addClass("ui-accordion-header").addClass("ui-state-active");
            
    });
    }
</script>

    
<script type="text/javascript" language="javascript">
function getURLPreview()
{
return '<asp:Literal ID="RetreiveAssetBrowseURL" runat=server Text=""></asp:Literal>';
}
function getProjectURLPreview()
{
return '<asp:Literal ID="RetreiveProjectBrowseURL" runat=server Text=""></asp:Literal>';
}
function center(object){
if (DialogTagging.get_isShowing()==true){
var winW, winH;
object.beginUpdate();
winW=object.get_x();
winH=document.documentElement.scrollTop+200;
object.set_x(null);
object.set_y(null);
object.set_x(winW);
object.set_y(winH);
object.endUpdate();
}
}

function Callback_DownloadBufferCheck_onCallbackError(sender, eventArgs)
{
return true;
}
function GenericCallback_onCallbackError(sender, eventArgs)
{
return true;
}

function showqtagging(){
<%response.write(CALLBACKComboBoxTagging.ClientID)%>.Callback();
<%response.write(CALLBACKComboBoxServices.ClientID)%>.Callback();
<%response.write(CALLBACKComboBoxMedia.ClientID)%>.Callback();
<%response.write(CALLBACKComboBoxIllust.ClientID)%>.Callback();
DialogTagging.show();

$('#dialogAssetFiltersNew').dialog({ autoOpen: false });
$('#assetfilternewaddfilter_project').click(function() { 
$('#dialogAssetFiltersNew').dialog("option","title",'" & _ProjectHeading & "');
$('#dialogAssetFiltersNew').dialog( "option", "position", [$(this).offset().left+0,$(this).offset().top-$(document).scrollTop()+0] );
$('#dialogAssetFiltersNew').dialog( "option", "modal", false );
$('#dialogAssetFiltersNew').dialog( "option", "height", 500 );
$('#dialogAssetFiltersNew').dialog( "option", "width", 250 );
$('#dialogAssetFiltersNew').dialog( "option", "zIndex", 9999 );
$('#dialogAssetFiltersNew').dialog('open');
CallbackAssetFiltersContent.Callback('project','init'); });

Project_GridAssets_onSelect();
}

window.onscroll = function(){center(DialogTagging)};

function getURLPreviewSml()
{
return '<asp:Literal ID="RetreiveAssetBrowseURLsml" runat=server Text=""></asp:Literal>';
}
function getURLPreviewIcon()
{
return '<asp:Literal ID="RetreiveAssetBrowseURLIcon" runat=server Text=""></asp:Literal>';
}
function getURLPreFixPreview()
{
return '<%response.write(WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLPRefix)%>';
}
function getIDAMGridThumbnailHeight()
{
return '<%response.write(WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMGridThumbnailHeight)%>';
}
function loadContextMenuThumbs(evt, id)
{   
loadContextMenuThumbsLib(evt, id,<%response.write(MenuCallBack1.clientid)%>); 
}  
function loadContextMenu(evt, id, grid)
{   
loadContextMenuLib(evt, id, grid,<%response.write(MenuCallBack1.clientid)%>)
}
function loadTreeAssetContextMenu(evt, id)
{   
loadTreeAssetContextMenuLib(evt, id,<%response.write(MenuCallBack1.clientid)%>)
}
function yy_callbackComplete(sender, eventArgs)
{
<%response.write(Menu1.clientid)%>.showContextMenu(contextMenuX, contextMenuY);
}

    </script>

<script type="text/javascript">


    var previewCurrentID;
    var myLayout;

    function sizeCenterPane() {
        var $Container = $('#container')
        var $Pane = $('.ui-layout-center');
        var $Content = $('#idamcontent');
        var outerHeight = $Pane.outerHeight();
        // use a Layout utility to calc total height of padding+borders (also handles IE6)
        var panePadding = outerHeight - myLayout.cssHeight($Pane, outerHeight);
        // update the container height - *just* tall enough to accommodate #Content without scrolling
        $Container.height($Pane.position().top + $Content.outerHeight() + panePadding);
        // resize panes to fit new container size
        myLayout.resizeAll();
        $("#quick_info").colorbox({ width: 550, inline: true, href: "#inline_preview" });
        $("#quick_info").live({ click: function () {
            showtrailpreload(previewCurrentID, '', '', '5', '1', 270, 7);
        }
        });
     }


     $(document).ready(function () {

         var mouseoverquick_info = 0;
         $('.asset_image').live({ mousemove: function () {

             offset = $(this).offset();
             width = $(this).width();
             height = $(this).height();

             previewCurrentID = $(this).attr("id");
             if (detectIE()) {
                 offsetLeft = offset.left + 0;
                 offsetTop = offset.top + ($(this).height()) - 18;
             }
             else {
                 offsetLeft = offset.left + 0;
                 offsetTop = offset.top + ($(this).height()) - 18;
             }
             //minwidth
             if (width < 65) {
                 width = 65;
                 offsetLeft = offsetLeft - (width / 2) * .5;
             }
             $("#quick_info").css("width", width - 1 + "px");
             $("#quick_info").css("left", offsetLeft + "px");
             $("#quick_info").css("top", offsetTop + 1 + "px");
             $("#quick_info").css("display", "block");
             mouseoverquick_info = 1;
         }
     });

     $('.asset_image').live({ mouseleave: function () {
         mouseoverquick_info = 0;
     }
     });


         $("#quick_info").live({ click: function () {
             showtrailpreload(previewCurrentID, '', '', '5', '1', 270, 7);
         }
         });



         $("#GridContent").live({ mousemove: function () {
             if (mouseoverquick_info == 0) {
                 $("#quick_info").css("display", "none");
             }
         }
         });


         // first set a 'fixed height' on the container so it does not collapse...
         var $Container = $('#container')
         $Container.height($(window).height() - $Container.offset().top);

         // NOW create the layout
         myLayout = $('#container').layout({
             applyDefaultStyles: true, north__spacing_open: 0, north__applyDefaultStyles: false, center__applyDefaultStyles: false, west__applyDefaultStyles: false, center__spacing_open: 0, west__size: 300
         });

         // now RESIZE the container to be a perfect fit        
         window.setTimeout('sizeCenterPane()', 2000);
     });


</script>




    <form id="PageForm" style="margin-top: 0;" method="post" runat="server" >

    <ComponentArt:Dialog Title="Template" AllowDrag="True" PreloadContentUrl="False" IFrameCssClass="iFrameCss" ContentUrl="AssetTagging.aspx" ContentCssClass="contentCss" FooterCssClass="footerCss" HeaderCssClass="headerCss" CssClass="dialogCss" ID="DlgMain" runat="server">
        <Header>
            <table id="dialogheadertable" cellpadding="0" cellspacing="0" border="0" width="905" height="35" onmousedown="<% Response.Write(DlgMain.ClientID) %>.StartDrag(event);">
                <tr>
                    <td width="9" height="35" style="background-image: url(images/UI/dialog/images/top-left.png);">
                    </td>
                    <td height="35" style="background-image: url(images/UI/dialog/images/top-mid.png); height: 35px !important;" valign="middle" id="dialogheadercontenttable" width="857">
                        <span style="color: White; font-size: 15px; font-family: Arial; font-weight: bold;" id="DlgMainTitle"></span>
                    </td>
                    <td width="40" height="35" valign="top" style="background-image: url(images/UI/dialog/images/top-right.png);">
                        <img src="images/UI/dialog/images/close.png" style="cursor: default; padding-top: 4px;" width="32" height="25" style="margin-top: 4px;" onmousedown="this.src='images/UI/dialog/images/close-down.png';" onmouseup="this.src='images/UI/dialog/images/close-hover.png';" onclick="<% Response.Write(DlgMain.ClientID) %>.Close('Close click');" onmouseover="this.src='images/UI/dialog/images/close-hover.png';" onmouseout="this.src='images/UI/dialog/images/close.png';" />
                    </td>
                </tr>
            </table>
        </Header>
        <Footer>
            <table id="dialogfootertable" cellpadding="0" cellspacing="0" width="905" height="7">
                <tr>
                    <td width="9" height="7">
                        <img style="display: block;" src="images/UI/dialog/images/bottom-left.png" />
                    </td>
                    <td style="background-image: url(images/UI/dialog/images/bottom-mid.png);" id="dialogfootercontenttable" width="888">
                    </td>
                    <td width="9" height="7">
                        <img style="display: block;" src="images/UI/dialog/images/bottom-right.png" />
                    </td>
                </tr>
            </table>
        </Footer>
    </ComponentArt:Dialog>
    <div id="container">


<!-- Header  ----------------------------------------------------------------------->
  <div class="ui-layout-north hidden" >
    <div style="border-bottom: 1px solid #D8D7DA;" id="logoheading" class=" ui-accordion-content ui-helper-reset ui-widget-content ui-accordion-content-active" >
        <table id="table1" cellpadding=0 cellspacing=0 width="100%"  border="0" >
            <tr>
                <td width="210">
                    <asp:Image ID="Image2" runat="server" ImageUrl="images/LOGO_template.png" DescriptionUrl="logo"></asp:Image>
                </td>
                <td width="100%" align=right>
                    <div style="padding: 10px">
                        <%if  Session("login_name") = "Anonymous" then%>
                        Version
                        <%response.write(CurrentVersion)%><br>
                        You are not logged in. [
                        <asp:HyperLink ID="Hyperlink7" runat="server" BackColor="Transparent" BorderColor="Transparent" ForeColor="#EB6601" NavigateUrl="Login.aspx?Logout=True">login</asp:HyperLink>
                        ]
                        <%if trim(webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.userAccess.ToString) = "0" then%>[
                        <asp:HyperLink ID="Hyperlink2" Enabled=True runat="server" BackColor="Transparent" BorderColor="Transparent" ForeColor="#EB6601" NavigateUrl="IDAM.aspx?page=VersionInfo">check for update</asp:HyperLink>
                        ]<%end if%>
                        <%if trim(webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.userAccess.ToString) = "0" then%>[
                        <asp:HyperLink ID="Hyperlink5" Enabled=True runat="server" BackColor="Transparent" BorderColor="Transparent" ForeColor="#EB6601" NavigateUrl="IDAM.aspx?page=QManager">queue manager</asp:HyperLink>
                        ]<%end if%>
                        <%else%>
                        <font size="1">Welcome, <strong>
                            <%response.write(WebArchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.loginName)%></strong>&nbsp;&nbsp;Version
                            <%response.write(CurrentVersion)%><br>
                            [
                            <asp:HyperLink ID="HyperLink3" runat="server" BackColor="Transparent" BorderColor="Transparent" ForeColor="#EB6601" NavigateUrl="Login.aspx?Logout=True">log out</asp:HyperLink>
                            ]
                           
                            <%if trim(WebArchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.userAccess) = "0" then%>[
                            <asp:HyperLink ID="HyperLink1" Enabled=True runat="server" BackColor="Transparent" BorderColor="Transparent" ForeColor="#EB6601" NavigateUrl="IDAM.aspx?page=QManager">queue manager</asp:HyperLink>
                            ]<%end if%>
                            
                            [
                            <asp:HyperLink ID="Hyperlink4" Enabled=True runat="server" BackColor="Transparent" BorderColor="Transparent" ForeColor="#EB6601" NavigateUrl="IDAM.aspx?page=preferences">my preferences</asp:HyperLink>
                            ]
                            
                             <%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("EDIT_INFO") Then%>[
                            <asp:HyperLink ID="HyperLink6" Enabled=True runat="server" BackColor="Transparent" BorderColor="Transparent" ForeColor="#EB6601" NavigateUrl="javascript:Object_PopUp_GenericDialog('MyInfo');">edit profile</asp:HyperLink>
                            ]<%end if%>
                            
                            </STRONG></font>
                        <%end if%>
                        
                    </div>
                </td>
            </tr>
            <tr>
                <td class="ui-helper-reset ui-state-active" style="border-right:0px;border-left:0px;border-bottom:0px;border-top:0px;" colspan="2">
                    <ComponentArt:Menu ID="MenuMAIN" runat="server" Orientation="Horizontal" CssClass="TopGroup" DefaultGroupCssClass="MenuGroup" SiteMapXmlFile="menuData.xml" DefaultItemLookId="DefaultItemLook" DefaultGroupItemSpacing="1" ImagesBaseUrl="images/" EnableViewState="true" ExpandDelay="100" ExpandOnClick="true">
                        <ItemLooks>
                            <ComponentArt:ItemLook HoverCssClass="TopMenuItemHoverMainPage" LabelPaddingTop="2px" LabelPaddingRight="10px" LabelPaddingBottom="2px" ExpandedCssClass="TopMenuItemExpandedMainPage" LabelPaddingLeft="10px" LookId="TopItemLook" CssClass="TopMenuItemMainPage"></ComponentArt:ItemLook>
                            <ComponentArt:ItemLook HoverCssClass="MenuItemHover" LabelPaddingTop="3px" LeftIconHeight="18px" LabelPaddingRight="10px" LabelPaddingBottom="4px" ExpandedCssClass="MenuItemHover" LabelPaddingLeft="10px"  LookId="DefaultItemLook" CssClass="MenuItem"></ComponentArt:ItemLook>
                            <ComponentArt:ItemLook LookId="BreakItem" CssClass="MenuBreak"></ComponentArt:ItemLook>
                        </ItemLooks>
                    </ComponentArt:Menu>
                </td>
            </tr>
        </table>
    </div>
    <asp:Image ID="Image3" runat="server" ImageUrl="images/spacer.gif" Width="1" Height="10"></asp:Image><br>
   </div>
    


<div id="TreeViewContent" class="ui-layout-west hidden">
                <div  class="SplitterPaneleftsidetest">
                    <!--SplitterPaneleftsidetest-->

                    
<script  type="text/javascript" language="javascript">
    $(function () {
        $("#JQQSx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQQSx', $("#JQQSx").accordion("option", "active"));
               
            }
        });
    });
    $(document).ready(function () {
     if ($.cookie('JQQSx') == "false") {
            $("#JQQSx").accordion("option", "active", false);
        }
    });
    function setDocSearchVal(chkbx)
    {
    <%response.write(DownloadBatchJobsCallback.ClientID)%>.Callback('setDocuSearchVal', chkbx.checked);
    }
</script> 





 
<div id="JQQSx" style="padding-bottom:5px;"> 
	<h3><a href="#section1">Quick Search</a></h3> 
                      <div style="padding-left:10px;padding-right:10px;">
                        <table cellpadding="0" cellspacing=0 >
                            <tr>
                                <td align="left" width="5">
                                </td>
                                <td align="left" width="100%">
                                    <font size="1" face="Verdana"><b>Keyword</b></font>
                                </td>
                                <td align="right" width="5" nowrap>
                                    <%if WebArchives.iDAM.Web.Core.IDAMWebSession.Config.EnableIDAMDocSearch then%>
                                    <font size="1" face="Verdana">iDAMDocSearch
                                </td>
                                <td>
                                    <input type=checkbox <%if session("DocuSearch") = "true" then 
response.write("Checked") 
end if%> id="DocuSearch" onclick="javascript:setDocSearchVal(this);"></font>
                                    <%end if%>
                                </td>
                            </tr>
                        </table>
                        <img src="images/spacer.gif" width="1" height="3"><br>
                        <style>
                            .quicksearchkeywordstyle
                            {
                                 border: 0px #CCCCCC solid; !importantborder:0px#CCCCCCsolid;
                            }</style>

                        

                       
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                      <td width="100%">
                                        <div class="quicksearchkeywordstyle">
                                            <asp:TextBox ID="quicksearchkeyword" Width=100% Style="border-left: 1px solid #969698; border-top: 1px solid #969698; border-bottom: 1px solid #969698; height: 16px; width: 100%; background-color: white; z-index: 1;" runat="server"></asp:TextBox></div>
                                    </td>
                                    <td width="3">
                                       <img style="margin-top: 0px; !important margin-top: 0px;" border="0" id=lquicksearchbutton src="images/search_butt.gif" runat=server onclick="javascript:$('#idamaction').val('true');var param=encodeURIComponent($('#quicksearchkeyword').val()); window.location = 'IDAM.aspx?page=Results&Type=QuickSearch&keyword='+param;">
                                    </td>
                                </tr>
                            </table>
                     
                        <asp:TextBox ID="idamactionserver" name="idamactionserver" Visible="False" runat="server"></asp:TextBox>
                        <input type="hidden" name="idamaction" id="idamaction" value="">
                        <input type="hidden" name="ExploreTreeNewselectednode" id="ExploreTreeNewselectednode" value="">
                        <input type="hidden" name="ExploreTreeNewselectednodeaction" id="ExploreTreeNewselectednodeaction" value="">
                        <input type="hidden" name="ProjectExploreTreeNewselectednode" id="ProjectExploreTreeNewselectednode" value="">
                        <input type="hidden" name="ProjectExploreTreeNewselectednodeaction" id="ProjectExploreTreeNewselectednodeaction" value="">
                        <input type="hidden" name="ProjectAssetselectednodeaction" id="ProjectAssetselectednodeaction" value="">
                        </div>
 </div>
                    







                						

<script  type="text/javascript" language="javascript">
    $(function () {
        $("#JQBrowsex").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQBrowsex', $("#JQBrowsex").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQBrowsex') == "false") {
            $("#JQBrowsex").accordion("option", "active", false);
        }
    });
</script> 


 
<div id="JQBrowsex" style="padding-bottom:5px;"> 
	<h3><a href="#section1">Browse</a></h3> 
                            <div class="SnapTreeview" style="height: 100%; padding: 0px; padding-left: 4px; padding-top: 4px; ">
                             <asp:PlaceHolder ID="ExploreTreeLoader" runat="server"></asp:PlaceHolder>
                            </div>
</div>





<%if  Session("login_name") <> "Anonymous" then%>

                 
<script type="text/javascript" language="javascript">
    $(function () {
        $("#JQCarouselx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQCarouselx', $("#JQCarouselx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQCarouselx') == "false") {
            $("#JQCarouselx").accordion("option", "active", false);
        }
    });
</script> 


 
<div id="JQCarouselx" style="padding-bottom:5px;"> 
	<h3><a href="#section1">Carousel</a></h3> 
                            <div class="SnapTreeviewxxx" style="overflow:hidden;" >
                                <div class="carouselshortlistxxxx" style="font-size:10px;">
                                    <div style="padding-top: 5px; padding-bottom: 10px; text-align: left;">
                                        [ <a href="IDAM.aspx?Page=CarouselPublic">
                                            <asp:Literal ID="LiteralPublicCarouselLink" runat="server">View Public</asp:Literal></a> ]</div>
                                           
                                    <ComponentArt:CallBack ID="carousel_callback" runat="server" CacheContent="False" EnableViewState="False" PostState="False">
                                        <ClientEvents>
                                            <CallbackError EventHandler="GenericCallback_onCallbackError" />
                                        </ClientEvents>
                                        <Content>
                                            <asp:Literal ID="carousel_title_literal" runat="server">My carousels</asp:Literal>
                                            <asp:DropDownList DataTextField="name" DataValueField="carrousel_id" ID="carousel_select" Style="width: 100%; visibility: inherit;" runat="server">
                                            </asp:DropDownList>
                                            <asp:Literal ID="carousel_literal" runat="server"></asp:Literal>
                                            <div id="side_results" name="side_results" style="width: 100%; height: 100%; overflow: auto;">
                                            <asp:Repeater ID="repeater_carousel_shortlist" runat="server">
                                                <ItemTemplate>
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr valign="top">
                                                            <td width="70" align=left>
                                                            <div id="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" class="asset_image">
                                                                <div class="<%#CheckHighlightonCarouselImage(CType(DataBinder.Eval(Container.DataItem, "asset_id"), Long))%>">
                                                                    <a href="IDAM.aspx?page=Asset&Id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=asset">
                                                                        <img alt="" border="0" src="<%#getURLPreview()%><%#DataBinder.Eval(Container.DataItem, "asset_id")%>&page=<%#DataBinder.Eval(Container.DataItem, "page")%>"></a>
                                                                </div>
                                                                </div>
                                                            </td>
                                                            <td align=left>
                                                                <div>
                                                                    <a href="IDAM.aspx?page=Asset&Id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=asset">
                                                                        <%# getPageIndicatorMain(CType(DataBinder.Eval(Container.DataItem, "page"), Long))%><%#DataBinder.Eval(Container.DataItem, "name")%></a></div>
                                                                <div>
                                                                    Filesize:
                                                                    <%#FormatSize(CType(DataBinder.Eval(Container.DataItem, "filesize"), Long))%><br>
                                                                </div>
                                                                <div>
                                                                    <a href="javascript:RemoveItemFromCarousel('<%#DataBinder.Eval(Container.DataItem, "asset_id")%>'+'_'+'<%#DataBinder.Eval(Container.DataItem, "page")%>');">remove</a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="padding: 5px;">
                                                        <div class="asset_links">
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    <div style="padding-top: 15px;">
                                                    </div>
                                                    <!--Carousel Header-->
                                                    <!---->
                                                </HeaderTemplate>
                                                <FooterTemplate>
                                                    <!--Carousel Header-->
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            </div>
                                <div>
                                    <table width="235" cellpadding="0" cellspacing="0" border="0">
                                        <tr align="center" valign="top">
                                            <td>
                                                <asp:Literal ID="carousel_footer_literal" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                        </Content>
                        <loadingpanelclienttemplate>
                            <TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
                            <TR>
                            <TD align="center">
                            <TABLE cellSpacing="0" cellPadding="0" border="0">
                            <TR>
                            <TD style="FONT-SIZE: 10px">Loading...
                            </TD>
                            <TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
                            </TR>
                            </TABLE>
                            </TD>
                            </TR>
                            </TABLE>
                         </loadingpanelclienttemplate>
                   </ComponentArt:CallBack>
                </div>
                <!--carouselshortlist-->
                </div><!--SnapTreeview-->
      </div><!--IDAMCarousel-->
        <%end if%>









        
<!--TreeViewExplore-->
<% if enableDownloadHistory then %>
                                						

<script type="text/javascript" language="javascript">
    $(function () {
        $("#JQDownloadx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQDownloadx', $("#JQDownloadx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
     if ($.cookie('JQDownloadx') == "false") {
            $("#JQDownloadx").accordion("option", "active", false);
        }
    });

function StartRefresh() {  
alert('Download request sent to the queue.');
<%response.write(Callback_DownloadBufferCheck.ClientID)%>.Callback();
<%response.write(downloadbatchjob_clientID)%>.Callback();
}

function StopRefresh() {
setInterval('<%response.write(downloadbatchjob_clientID)%>.Callback(<%response.write(downloadbatchjob_clientID)%>.Parameter)', 2000);
}

function RemoveDownloadJob(ticketId) {
<%response.write(DownloadBatchJobsCallback.ClientID)%>.Callback('delete', ticketId);
}
function retry(ticketId) {
<%response.write(DownloadBatchJobsCallback.ClientID)%>.Callback('retry', ticketId);
<%response.write(Callback_DownloadBufferCheck.ClientID)%>.Callback();
}
 </script>
<div id="JQDownloadx" style="padding-bottom:5px;"> 
	<h3><a href="#section1">Download</a></h3> 

<div class="SnapTreeviewxx">
 [<a style="cursor:default" onclick="javascript:Callback_DownloadBufferCheck.Callback();">refresh</a>]
                                <div class="carouselshortlistxx" style="font-size:10px;">
                                    <div style="height: 300px; padding-top: 5px; padding-bottom: 10px; text-align: left; overflow: auto;">
                                        <ComponentArt:CallBack ID="DownloadBatchJobsCallback" runat="server" CacheContent="False" EnableViewState="False" PostState="False">
                                            <ClientEvents>
                                                <CallbackError EventHandler="GenericCallback_onCallbackError" />
                                            </ClientEvents>
                                            <Content>
                                                <asp:Repeater ID="DownloadBatchJobsRepeater" runat="server">
                                                    <ItemTemplate>
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr valign="top">
                                                                <td width="33" align="left">
                                                                    <div style="padding: 4px;">
                                                                        <img src="<%#DataBinder.Eval(Container.DataItem, "IconImage")%>" />
                                                                    </div>
                                                                </td>
                                                                <td width="100%" align="left">
                                                                    <div style="overflow: hidden;">
                                                                        <b>
                                                                            <%#Server.HTMLEncode(DataBinder.Eval(Container.DataItem, "name"))%></b>
                                                                        <br>
                                                                        <%#DataBinder.Eval(Container.DataItem, "DownloadTypeName")%>
                                                                        <br>
                                                                        <%#DataBinder.Eval(Container.DataItem, "State")%>
                                                                    </div>
                                                                </td>
                                                                <td align="right">
                                                                    <div style="padding-right: 18px;">
                                                                        <%#DataBinder.Eval(Container.DataItem, "Download").tolower%>
                                                                        [<a href="javascript:RemoveDownloadJob('<%#DataBinder.Eval(Container.DataItem, "TicketID")%>')">remove</a>]</div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div style="padding: 5px;">
                                                            <div class="asset_links">
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        <div style="padding-top: 15px;">
                                                        </div>
                                                        <!--Carousel Header-->
                                                        <div id="side_results" name="side_results" style="width: 93%; overflow: visible;">
                                                    </HeaderTemplate>
                                                    <FooterTemplate>
                                                        <!--Carousel Header-->
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                <asp:Literal ID="DownloadLinkURLLiteral" runat=server Text="No Downloads"></asp:Literal>
                                            </Content>
                                            <LoadingPanelClientTemplate>
                                            </LoadingPanelClientTemplate>
                                        </ComponentArt:CallBack>
                                        <ComponentArt:CallBack ID="Callback_DownloadBufferCheck" runat="server" RefreshInterval="10000"  EnableViewState="False" PostState="False" CacheContent="True">
                                            <ClientEvents>
                                                <CallbackError EventHandler="Callback_DownloadBufferCheck_onCallbackError" />
                                            </ClientEvents>
                                            <Content>
                                                <asp:Literal ID="DownloadcheckLinkURLLiteral" runat=server></asp:Literal></Content>
                                            <LoadingPanelClientTemplate>
                                            </LoadingPanelClientTemplate>
                                        </ComponentArt:CallBack>
                                    </div>
                                    <!--carouselshortlist-->
                                </div>
                                <!--SnapTreeview-->
                               
        </div>
</div>
<%end if%>

<script type="text/javascript" language="javascript">
    var bDownloadBuffer = GetCookie('bDownloadBuffer');
    function setDownloadBuffer() {
        if (bDownloadBuffer == 1) {
            bDownloadBuffer = 0
            SetCookie('bDownloadBuffer', '0', exp);
        } else {
            bDownloadBuffer = 1
            SetCookie('bDownloadBuffer', '1', exp);
        }
    }
</script>








        </DIV>
        <!--Dock-->
        </div>
        <div id="GridContent" class="ui-layout-center hidden">
         	<div id="idamcontent">
           
           
            <style>
                .rightsideplaceholderdiv
                {
                    padding-right: 10px !important;
                    margin-left: 1px !important; <%if Request.Browser.Browser="IE" then 
                    response.write("margin-top: -.2em !important;") 
                    else 
                    response.write("margin-top: -.4em !important;") 
                    end if%>padding-right:0px;
                margin-left:1px;
                margin-top:-2px;
                }
                .rightsideplaceholderdivheading
                {
                    border: #959595 1px solid;
                    height: 10px;
                    background-image: url(images/headingtop.gif);
                }
                .topTabsset
                {
                    <%if Request.Browser.Browser="IE" then 
                    response.write("position:relative;top:1px;z-index:2;left:1px; font-size:12px;") 
                    else 
                    response.write("position:relative;top:1px;z-index:2;left:1px; font-size:12px;") 
                    end if%>
                    
                }
            </style>


            <div style="font-size:14px; white-space:nowrap;">
            <asp:Literal ID="jqTabStripMain" runat="server"></asp:Literal>
            </div>

  <%if Request.Browser.Browser="IE" then%> 
  <div style="border-top:1px solid silver;position:relative;top:-3px;z-index:1;margin-left:11px;left:-10px;"></div>
                    <%else %>
<div style="border-top:4px solid silver;position:relative;top:-2px;z-index:1;margin-left:11px;left:-10px;"></div>
                    <%end if%>          

<script>

    $(function () {
        $("#radio2").button();
        $("#radioset2").buttonset();
    });

	</script>

            <div class="rightsideplaceholderdiv">
                <table width=100% cellpadding=0 cellspacing=0>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="PageLoader" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
       
       </div>


      









      </div>



<div style="height:500px;" >
<div style="top:470px; vertical-align:bottom;position:relative;padding:20px;color:Silver;">
Page Load (Internal Code Execution):
<%response.write(ElapsePageLoadTime)%>
ms | Client Page Load (Client Browser Execution):
<%response.write(System.DateTime.Now.Subtract(ClientPageLoadTime).Milliseconds)%>
ms
</div>
</div>
   
<!--</div>--><!--end-->























    <ComponentArt:CallBack ID="CallbackDownloadCheck" runat="server" CacheContent="false">
        <ClientEvents>
            <CallbackError EventHandler="GenericCallback_onCallbackError" />
        </ClientEvents>
        <Content>
            <asp:Literal ID="LiteralDownloadCheck" Text="" Visible=true runat="server" />
        </Content>
        <LoadingPanelClientTemplate>
        </LoadingPanelClientTemplate>
    </ComponentArt:CallBack>
   

<script language="javascript">
function Object_PopUp_IDAMExpressAssets(assets)
{
<%if request.querystring("Page") = "OrdersDetails" then%>
ShowDialogWindowX(1340,1340,972,'IDAMExpress.aspx?assetids=' + assets + '&orderid=<%response.write(request.querystring("ID"))%>&x=' + urlhelper(),'IDAMExpress Assets','contentCssEXPRESS','iFrameCss');
<%else%>
ShowDialogWindowX(1340,1340,972,'IDAMExpress.aspx?assetids=' + assets + '&x=' + urlhelper(),'IDAMExpress Assets','contentCssEXPRESS','iFrameCss');
<%end if%>
}

function ChangeDefaultCarousel(dropdown)
{
var myindex  = dropdown.selectedIndex
var SelValue = dropdown.options[myindex].value
<% Response.Write(carousel_callback.ClientID) %>.Callback(SelValue);
return true;
}

function RemoveItemFromCarousel(id)
{
<% Response.Write(carousel_callback.ClientID) %>.Callback(id + ',remove');
}

function RemoveCarousel(id)
{
encodeURIComponent
<% Response.Write(carousel_callback.ClientID) %>.Callback(id + ',removecarousel');
return true;
}

function AddToCarouseldeletthis(rowId)
{
<% Response.Write(carousel_callback.ClientID) %>.Callback('1,' + rowId + ',AddToCarousel');
}

function AddDownloadJob(JobID)
{ 
<% Response.Write(DownloadBatchJobsCallback.ClientID) %>.Callback(JobID);
}

function DownloadCarousel2()
{ 
var myindex  = document.getElementById('carousel_select').selectedIndex;
var SelValue = document.getElementById('carousel_select').options[myindex].value;
<% Response.Write(DownloadBatchJobsCallback.ClientID) %>.Callback('carousel',SelValue);
}


function ShowDialogWindowX(width,hct,fct,loc,heading,contentCss,iframeCss,bmodal)
{
ShowDialogWindowXLib(width,hct,fct,loc,heading,contentCss,iframeCss,bmodal,<% Response.Write(DlgMain.ClientID) %>);
}
function CloseDialogWindowX(){
<% Response.Write(DlgMain.ClientID) %>.Close('Close click');
}
function CloseDialogWindowXCarousel(){
<% Response.Write(DlgMain.ClientID) %>.Close('Close click');
<% Response.Write(carousel_callback.ClientID) %>.Callback("callbackinit");
}



var IDAMTagids;


function IDAMServiceAction(param){
SNAPTAGGINGCallbackServices.Callback(param,IDAMTagids);
}
function IDAMServiceSearch(cvalue)
{
window.location='IDAM.aspx?page=Results&Type=QuickSearch&keyword=' + cvalue;
}

function IDAMMediaAction(param){
SNAPTAGGINGCallbackMedia.Callback(param,IDAMTagids);
}
function IDAMMediaSearch(cvalue)
{
window.location='IDAM.aspx?page=Results&Type=QuickSearch&keyword=' + cvalue;
}

function IDAMIllustAction(param){
SNAPTAGGINGCallbackIllust.Callback(param,IDAMTagids);
}
function IDAMIllustSearch(cvalue)
{
window.location='IDAM.aspx?page=Results&Type=QuickSearch&keyword=' + cvalue;
}

function IDAMTagAction(param){
SNAPTAGGINGCallbackTags.Callback(param,IDAMTagids);
}
function IDAMTagSearch(cvalue)
{
window.location='IDAM.aspx?page=Results&Type=QuickSearch&keyword=' + cvalue;
}
<% Response.Write(carousel_callback.ClientID) %>.Callback("callbackinit")
window.setTimeout('settreeviewscroll()',5000); 
<% if enableDownloadHistory then %>
<%response.write(downloadbatchjob_clientID)%>.Callback()
<% end if %>

    </script>




<ComponentArt:ComboBox ID="SNAPTAGGINGComboBoxTagging" runat="server" 
RunningMode="CallBack" AutoHighlight="false" AutoComplete="true" AutoFilter="true" CssClass="comboBox" HoverCssClass="comboBoxHover" 
FocusedCssClass="comboBoxHover" 
TextBoxCssClass="comboTextBox" TextBoxHoverCssClass="comboBoxHover" DropDownCssClass="comboDropDown" 
ItemCssClass="comboItem" ItemHoverCssClass="comboItemHover" SelectedItemCssClass="comboItemHover" 
DropHoverImageUrl="images/drop_hover.gif" DropImageUrl="images/drop.gif" DropDownPageSize="13" Width="265" Visible="False">
</ComponentArt:ComboBox>
<ComponentArt:ComboBox ID="SNAPTAGGINGComboBoxServices" runat="server" 
RunningMode="CallBack" AutoHighlight="false" AutoComplete="true" AutoFilter="true" CssClass="comboBox" HoverCssClass="comboBoxHover" 
FocusedCssClass="comboBoxHover" 
TextBoxCssClass="comboTextBox" TextBoxHoverCssClass="comboBoxHover" DropDownCssClass="comboDropDown" 
ItemCssClass="comboItem" ItemHoverCssClass="comboItemHover" SelectedItemCssClass="comboItemHover" 
DropHoverImageUrl="images/drop_hover.gif" DropImageUrl="images/drop.gif" DropDownPageSize="13" Width="265" Visible="False">
</ComponentArt:ComboBox>
<ComponentArt:ComboBox ID="SNAPTAGGINGComboBoxIllust" runat="server" 
RunningMode="CallBack" AutoHighlight="false" AutoComplete="true" AutoFilter="true" CssClass="comboBox" HoverCssClass="comboBoxHover" 
FocusedCssClass="comboBoxHover" 
TextBoxCssClass="comboTextBox" TextBoxHoverCssClass="comboBoxHover" DropDownCssClass="comboDropDown" 
ItemCssClass="comboItem" ItemHoverCssClass="comboItemHover" SelectedItemCssClass="comboItemHover" 
DropHoverImageUrl="images/drop_hover.gif" DropImageUrl="images/drop.gif" DropDownPageSize="13" Width="265" Visible="False">
</ComponentArt:ComboBox>
<ComponentArt:ComboBox ID="SNAPTAGGINGComboBoxMedia" runat="server" 
RunningMode="CallBack" AutoHighlight="false" AutoComplete="true" AutoFilter="true" CssClass="comboBox" HoverCssClass="comboBoxHover" 
FocusedCssClass="comboBoxHover" 
TextBoxCssClass="comboTextBox" TextBoxHoverCssClass="comboBoxHover" DropDownCssClass="comboDropDown" 
ItemCssClass="comboItem" ItemHoverCssClass="comboItemHover" SelectedItemCssClass="comboItemHover" 
DropHoverImageUrl="images/drop_hover.gif" DropImageUrl="images/drop.gif" DropDownPageSize="13" Width="265" Visible="False">
</ComponentArt:ComboBox>

<ComponentArt:Dialog ID="DialogTagging" ContentCssClass="contentCssQtag" FooterCssClass="footerCss" HeaderCssClass="headerCss" CssClass="dialogCss" runat="server" AllowDrag="true" Alignment="MiddleCentre">
        <Header>
            <table id="Table2" cellpadding="0" cellspacing="0" border="0" width="305" height="35" onmousedown="<% Response.Write(DialogTagging.ClientID) %>.StartDrag(event);">
                <tr>
                    <td width="9" height="35" style="background-image: url(images/UI/dialog/images/top-left.png);">
                    </td>
                    <td height="35" style="background-image: url(images/UI/dialog/images/top-mid.png); height: 35px !important;" valign="middle" id="Td1" width="257">
                        <span style="color: White; font-size: 15px; font-family: Arial; font-weight: bold;" id="Span1">Quick Tagging</span>
                    </td>
                    <td width="40" height="35" valign="top" style="background-image: url(images/UI/dialog/images/top-right.png);">
                        <img src="images/UI/dialog/images/close.png" style="cursor: default; padding-top: 4px;" width="32" height="25" style="margin-top: 4px;" onmousedown="this.src='images/UI/dialog/images/close-down.png';" onmouseup="this.src='images/UI/dialog/images/close-hover.png';" onclick="<% Response.Write(DialogTagging.ClientID) %>.Close('Close click');" onmouseover="this.src='images/UI/dialog/images/close-hover.png';" onmouseout="this.src='images/UI/dialog/images/close.png';" />
                    </td>
                </tr>
            </table>
        </Header>
        <Footer>
            <table id="Table4" cellpadding="0" cellspacing="0" width="305" height="7">
                <tr>
                    <td width="9" height="7">
                        <img style="display: block;" src="images/UI/dialog/images/bottom-left.png" />
                    </td>
                    <td style="background-image: url(images/UI/dialog/images/bottom-mid.png);" id="Td2" width="288">
                    </td>
                    <td width="9" height="7">
                        <img style="display: block;" src="images/UI/dialog/images/bottom-right.png" />
                    </td>
                </tr>
            </table>
        </Footer>
        <Content>
        
        
        <!-- tabs work as navigator for scrollable -->
<div class="navi"></div>

        
        <!-- tab panes -->
<div id="flowpanes">

	<!-- wrapper for scrollable items -->
	<div class="scrollableitems">

		<!-- the items -->
		<div >   
            <div style="padding-bottom:4px;"><b>Qtag</b> - Add a Tag</div>
                    <ComponentArt:CallBack ID="CALLBACKComboBoxTagging" runat="server" CacheContent="false" 
                    LoadingPanelFadeMaximumOpacity="20" LoadingPanelFadeDuration="20">
                    <Content>
                     </Content>
                      <LoadingPanelClientTemplate>
                      <img height="16" src="images/spinner.gif" width="16" border="0">                  
                    </LoadingPanelClientTemplate>
                     </ComponentArt:CallBack>
                    <div style="padding-top:4px;padding-bottom:10px;">
                            [ <a href="javascript:IDAMTagAction('AddTag|'+SNAPTAGGINGComboBoxTagging.get_text());">add</a> ]
                    </div>
            <br />
            <div style="height: 230px;width:270px; margin-bottom: -10px; overflow: auto">
                <ComponentArt:CallBack ID="SNAPTAGGINGCallbackTags" runat="server" CacheContent="false" LoadingPanelFadeMaximumOpacity="20" LoadingPanelFadeDuration="20">
                    <Content>
                        <asp:Literal ID="SNAPTAGGINGLtrlTagListing" runat=server Text="No tags available for selected assets." Visible=True></asp:Literal>
                        <asp:Repeater ID="SNAPTAGGINGRptrTagListing" runat=server>
                            <ItemTemplate>
                                <a border="0" href="javascript:IDAMTagAction('Modify|<%#DataBinder.Eval(Container.DataItem, "tagstatus")%>|<%#DataBinder.Eval(Container.DataItem, "tagid")%>|<%#DataBinder.Eval(Container.DataItem, "ids")%>');">
                                    <img border="0" src="images/<%#DataBinder.Eval(Container.DataItem, "tagstatus")%>.gif" /></a>&nbsp;&nbsp;<a href="javascript:IDAMTagSearch('<%#DataBinder.Eval(Container.DataItem, "tagname")%>');"><%#DataBinder.Eval(Container.DataItem, "tagname")%></a><br>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:Literal ID="SNAPTAGGINGLtrlTagSearch" runat=server Text="" Visible=True></asp:Literal>
                        
                    </Content>
                    <LoadingPanelClientTemplate>
                     <img height="16" src="images/spinner.gif" width="16" border="0">
                    </LoadingPanelClientTemplate>
                </ComponentArt:CallBack>
            </div>
        </div>
        
        
        
        
        
        
        
		<div>  		
             <ComponentArt:CallBack ID="CALLBACKComboBoxServices" runat="server" CacheContent="false" 
                    LoadingPanelFadeMaximumOpacity="20" LoadingPanelFadeDuration="20">
                    <Content>
                    <asp:Literal ID="LiteralCALLBACKComboBoxServicesTitle" runat=server Text="Services" Visible=True></asp:Literal>
                     </Content>
                      <LoadingPanelClientTemplate>
                      <img height="16" src="images/spinner.gif" width="16" border="0">                  
                    </LoadingPanelClientTemplate>
                     </ComponentArt:CallBack>
                    <div style="padding-top:4px;padding-bottom:10px;">
                            [ <a href="javascript:IDAMServiceAction('AddService|'+SNAPTAGGINGComboBoxServices.get_text());">add</a> ]
                    </div>
            <br />
            <div style="height: 230px;width:270px; margin-bottom: -10px; overflow: auto">
                <ComponentArt:CallBack ID="SNAPTAGGINGCallbackServices" runat="server" CacheContent="false" LoadingPanelFadeMaximumOpacity="20" LoadingPanelFadeDuration="20">
                    <Content>
                        <asp:Literal ID="SNAPTAGGINGLtrlServiceListing" runat=server Text="No keywords available for selected assets." Visible=True></asp:Literal>
                        <asp:Repeater ID="SNAPTAGGINGRptrServiceListing" runat=server>
                            <ItemTemplate>
                                <a border="0" href="javascript:IDAMServiceAction('Modify|<%#DataBinder.Eval(Container.DataItem, "tagstatus")%>|<%#DataBinder.Eval(Container.DataItem, "keyid")%>|<%#DataBinder.Eval(Container.DataItem, "ids")%>');">
                                    <img border="0" src="images/<%#DataBinder.Eval(Container.DataItem, "tagstatus")%>.gif" /></a>&nbsp;&nbsp;<a href="javascript:IDAMServiceSearch('<%#DataBinder.Eval(Container.DataItem, "keyname")%>');"><%#DataBinder.Eval(Container.DataItem, "keyname")%></a><br>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:Repeater>
                    </Content>
                    <LoadingPanelClientTemplate>
                     <img height="16" src="images/spinner.gif" width="16" border="0">
                    </LoadingPanelClientTemplate>
                </ComponentArt:CallBack>
            </div>         
		</div>
		
		
		
		
		
		
		<div>
		 <ComponentArt:CallBack ID="CALLBACKComboBoxMedia" runat="server" CacheContent="false" 
                    LoadingPanelFadeMaximumOpacity="20" LoadingPanelFadeDuration="20">
                    <Content>
                    <asp:Literal ID="LiteralCALLBACKComboBoxMediaTitle" runat=server Text="Media" Visible=True></asp:Literal>
                     </Content>
                      <LoadingPanelClientTemplate>
                      <img height="16" src="images/spinner.gif" width="16" border="0">                  
                    </LoadingPanelClientTemplate>
                     </ComponentArt:CallBack>
                    <div style="padding-top:4px;padding-bottom:10px;">
                            [ <a href="javascript:IDAMMediaAction('AddService|'+SNAPTAGGINGComboBoxMedia.get_text());">add</a> ]
                    </div>
            <br />
            <div style="height: 230px;width:270px; margin-bottom: -10px; overflow: auto">
                <ComponentArt:CallBack ID="SNAPTAGGINGCallbackMedia" runat="server" CacheContent="false" LoadingPanelFadeMaximumOpacity="20" LoadingPanelFadeDuration="20">
                    <Content>
                        <asp:Literal ID="SNAPTAGGINGLtrlMediaListing" runat=server Text="No keywords available for selected assets." Visible=True></asp:Literal>
                        <asp:Repeater ID="SNAPTAGGINGRptrMediaListing" runat=server>
                            <ItemTemplate>
                                <a border="0" href="javascript:IDAMMediaAction('Modify|<%#DataBinder.Eval(Container.DataItem, "tagstatus")%>|<%#DataBinder.Eval(Container.DataItem, "keyid")%>|<%#DataBinder.Eval(Container.DataItem, "ids")%>');">
                                    <img border="0" src="images/<%#DataBinder.Eval(Container.DataItem, "tagstatus")%>.gif" /></a>&nbsp;&nbsp;<a href="javascript:IDAMMediaSearch('<%#DataBinder.Eval(Container.DataItem, "keyname")%>');"><%#DataBinder.Eval(Container.DataItem, "keyname")%></a><br>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:Repeater>
                    </Content>
                    <LoadingPanelClientTemplate>
                     <img height="16" src="images/spinner.gif" width="16" border="0">
                    </LoadingPanelClientTemplate>
                </ComponentArt:CallBack>
            </div>         
		</div>
		
		
		
		
		
		
		
		
		
		<div>
		 <ComponentArt:CallBack ID="CALLBACKComboBoxIllust" runat="server" CacheContent="false" 
                    LoadingPanelFadeMaximumOpacity="20" LoadingPanelFadeDuration="20">
                    <Content>
                    <asp:Literal ID="LiteralCALLBACKComboBoxIllustTitle" runat=server Text="Illustration" Visible=True></asp:Literal>
                     </Content>
                      <LoadingPanelClientTemplate>
                      <img height="16" src="images/spinner.gif" width="16" border="0">                  
                    </LoadingPanelClientTemplate>
                     </ComponentArt:CallBack>
                    <div style="padding-top:4px;padding-bottom:10px;">
                            [ <a href="javascript:IDAMIllustAction('AddService|'+SNAPTAGGINGComboBoxIllust.get_text());">add</a> ]
                    </div>
            <br />
            <div style="height: 230px;width:270px; margin-bottom: -10px; overflow: auto">
                <ComponentArt:CallBack ID="SNAPTAGGINGCallbackIllust" runat="server" CacheContent="false" LoadingPanelFadeMaximumOpacity="20" LoadingPanelFadeDuration="20">
                    <Content>
                        <asp:Literal ID="SNAPTAGGINGLtrlIllustListing" runat=server Text="No Keywords available for selected assets." Visible=True></asp:Literal>
                        <asp:Repeater ID="SNAPTAGGINGRptrIllustListing" runat=server>
                            <ItemTemplate>
                                <a border="0" href="javascript:IDAMIllustAction('Modify|<%#DataBinder.Eval(Container.DataItem, "tagstatus")%>|<%#DataBinder.Eval(Container.DataItem, "keyid")%>|<%#DataBinder.Eval(Container.DataItem, "ids")%>');">
                                    <img border="0" src="images/<%#DataBinder.Eval(Container.DataItem, "tagstatus")%>.gif" /></a>&nbsp;&nbsp;<a href="javascript:IDAMIllustSearch('<%#DataBinder.Eval(Container.DataItem, "keyname")%>');"><%#DataBinder.Eval(Container.DataItem, "keyname")%></a><br>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:Repeater>
                    </Content>
                    <LoadingPanelClientTemplate>
                     <img height="16" src="images/spinner.gif" width="16" border="0">
                    </LoadingPanelClientTemplate>
                </ComponentArt:CallBack>
            </div>         
		</div>




	</div>
</div>
        
        
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
        </Content>
    </ComponentArt:Dialog>


    
    
    <ComponentArt:CallBack ID="MenuCallBack1" CssClass="CallBack" Width="100" Height="21" runat="server">
        <ClientEvents>
            <CallbackComplete EventHandler="yy_callbackComplete" />
            <CallbackError EventHandler="GenericCallback_onCallbackError" />
        </ClientEvents>
        <Content>
            <ComponentArt:Menu ID="Menu1" Orientation="Vertical" DefaultGroupCssClass="MenuGroup" DefaultItemLookId="DefaultItemLook" DefaultGroupItemSpacing="1" ImagesBaseUrl="images/" EnableViewState="false" ContextMenu="Simple" CollapseSlide="None" CollapseTransition="Fade" CollapseDuration="0" CollapseDelay="0" ExpandDelay="0" ExpandDuration="0" ExpandSlide="None" ExpandTransition="Fade" runat="server">
                <ClientEvents>
                    <ItemSelect EventHandler="yy_menuItemSelect" />
                </ClientEvents>
                <ItemLooks>
                    <ComponentArt:ItemLook HoverCssClass="TopMenuItemHoverMainPage" LabelPaddingTop="2px" LabelPaddingRight="10px" LabelPaddingBottom="2px" ExpandedCssClass="TopMenuItemExpandedMainPage" LabelPaddingLeft="10px" LookId="TopItemLook" CssClass="TopMenuItemMainPage"></ComponentArt:ItemLook>
                    <ComponentArt:ItemLook HoverCssClass="MenuItemHover" LabelPaddingTop="3px" LeftIconHeight="18px" LabelPaddingRight="10px" LabelPaddingBottom="4px" ExpandedCssClass="MenuItemHover" LabelPaddingLeft="10px" LeftIconWidth="20px" LookId="DefaultItemLook" CssClass="MenuItem"></ComponentArt:ItemLook>
                    <ComponentArt:ItemLook LookId="BreakItem" CssClass="MenuBreak"></ComponentArt:ItemLook>
                </ItemLooks>
            </ComponentArt:Menu>
        </Content>
        <LoadingPanelClientTemplate>
            <table width="100" height="21" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td style="font-size: 10px;">
                                    Loading...&nbsp;
                                </td>
                                <td>
                                    <img src="images/spinner.gif" width="16" height="16" border="0">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </LoadingPanelClientTemplate>
    </ComponentArt:CallBack>
    <ComponentArt:Menu ID="Menu2" EnableViewState="false" ContextMenu="Simple" runat="server">
        <ClientEvents>
        </ClientEvents>
        <ItemLooks>
        </ItemLooks>
    </ComponentArt:Menu>

    

<script language="javascript">
<%response.write(IdamAdmin.Explore.CallBackExploreClientID)%>.Callback("treeviewcallback");

$(function() {
try
{

	// select #flowplanes and make it scrollable. use circular and navigator plugins
	$("#flowpanes").scrollable({ mousewheel: true }).navigator({
		// make browser's back button work
		history: true
	});
} catch(e){}	
	
	
	
	
});


function showtrailpreload(imageid,title,description,ratingaverage,ratingnumber,showthumb,height,filetype) {

<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(imageid +',Asset'+','+'test'+',');
}

ClearFilters();
getPaneSize();
 
</script>

<div id="quick_info" class="ui-state-highlight ui-corner-bottom" title="quick view" style="display:none;position:absolute;z-index:8000; text-align:center;cursor:pointer;white-space:nowrap;height:17px;padding-top:1px;">preview</div>

<div id=trailimageid>	
</div>


<!-- This contains the hidden content for inline calls -->
<div style='display:none'>
		<div id='inline_preview' style='padding:10px; background:#fff;height:500px;'>
<COMPONENTART:CALLBACK id="CallbackProjectAssetOverlay" runat="server" CacheContent="false">
<ClientEvents>
<CallbackError EventHandler="GenericCallback_onCallbackError" />
</ClientEvents>
<CONTENT>
<asp:PlaceHolder ID="PlaceHolderAssetPreviewPage" runat="server">
<div id="quick_preview_window">             
<table id="table3">
<tr>
<td>
<font size="1"><b>Image:</b></font><font size="1">
</font></td>
<td align="left" >
<font size="1"><asp:Literal ID="ProjectAssetOverlayCurrentImageName" Runat=server></asp:Literal></font></td>
</tr>
<tr >
<td nowrap  valign=top><font size="1"><b>Location(s):</b></font></span><font size="1">
</font></td>
<td align="left" >
<font size="1"><asp:Literal ID="ProjectAssetOverlayCurrentProjectName" Runat=server></asp:Literal></font></a><font size="1">
</font></td>
</tr>
</table>
<br>
<asp:Image id="ProjectAssetOverlayCurrentImage" runat="server" ImageUrl="~/common/images/spacer.gif"></asp:Image>
<!--<br>
[ <asp:Literal ID="LiteralRotateRight" Runat=server></asp:Literal>rotate right</a> ] [ <asp:Literal ID="LiteralRotateLeft" Runat=server></asp:Literal>rotate left</a> ]-->
<br>
<table id="table3">
<tr>
<td valign=top nowrap>
<font size="1"><b>Description:</b></font><font size="1">
</font></td>
<td align="left" width="80%">
<font size="1"><asp:Literal ID="LiteralAssetDescription" Runat=server></asp:Literal></font></td>
</tr>
<tr >
<td valign=top><font size="1"><b>Owner</b></font><font size="1">:</font><font size="1">
</font></td>
<td align="left" width="80%">
<font size="1"><asp:Literal ID="LiteralAssetOwner" Runat=server></asp:Literal></font></a><font size="1">
</font></td>
</tr>
<tr >
<td noWrap valign=top>
<font size="1"><b>Date Created:</b></font><font size="1">
</font></td>
<td>
<font size="1"><asp:Literal ID="LiteralAssetDateCreated" Runat=server></asp:Literal></font><font size="1">
</font></td>
</tr>
<tr>
<td noWrap valign=top><font size="1"><b>Type</b></font><font size="1">:</font><font size="1">
</font></td>
<td>
<font size="1"><asp:Literal ID="LiteralAssetType" Runat=server></asp:Literal></font></td>
</tr>
<tr>
<td noWrap valign=top><font size="1"><b>File Size</b></font><font size="1">:</font><font size="1">
</font></td>
<td>
<font size="1"> 
<asp:Literal ID="LiteralAssetSize" Runat=server></asp:Literal></a> </font>
</td>
</tr>
<tr>
<td noWrap  valign=top>
<font size="1"><b>Keywords:</b></font><font size="1">
</font></td>
<td>
<font size="1"><asp:Literal ID="LiteralAssetKeywords" Runat=server></asp:Literal></font></td>
</tr>
</table>
</div>	
</asp:PlaceHolder>
</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
<div class="SnapProjectsOverlay" >
<TABLE cellSpacing="0" cellPadding="0" border="0" height="500" width=100%>
<TR>
<TD align="center">
<TABLE cellSpacing="0" cellPadding="0" border="0">
<TR>
<TD style="FONT-SIZE: 10px">Loading...
</TD>
<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
</TR>
</TABLE>
</TD>
<td><IMG height="16" src="images/spacer.gif" width="16" height="500" border="0">
</td>
</TR>
</TABLE>
</div>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>
</div>
</div>







    <div id="GenericDialog" title="Dialog Title" >
    <ComponentArt:CallBack ID="CallbackGenericDialog" runat="server" CacheContent="false" LoadingPanelFadeMaximumOpacity="20" LoadingPanelFadeDuration="20">
    <ClientEvents>
    </ClientEvents>
    <Content>
    <asp:PlaceHolder runat="server" ID="PlaceHolderGenericDialog" >
    </asp:PlaceHolder>  
    </Content>     
     <loadingpanelclienttemplate>
                            <TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
                            <TR>
                            <TD align="center">
                            <TABLE cellSpacing="0" cellPadding="0" border="0">
                            <TR>
                            <TD style="FONT-SIZE: 10px">Loading...
                            </TD>
                            <TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
                            </TR>
                            </TABLE>
                            </TD>
                            </TR>
                            </TABLE>
                         </loadingpanelclienttemplate>                               
    </ComponentArt:CallBack>
    </div>


    <script language="javascript" type="text/javascript">
        $('#GenericDialog').dialog({ autoOpen: false
        });
        function Object_PopUp_GenericDialog(id) {
            $('#GenericDialog').dialog("option", "title", id);
            $('#GenericDialog').dialog("option", "modal", false);
            $('#GenericDialog').dialog("option", "height", 600);
            $('#GenericDialog').dialog("option", "width", 550);
            $('#GenericDialog').dialog("option", "zIndex", 9999);
            $('#GenericDialog').dialog('open');
            CallbackGenericDialog.Callback(id);

        }


    </script>





<input type="hidden" name="pptURL" id="pptURL" value="" />
<input type="hidden" name="browseURL" id="browseURL" value="" />
<input type="hidden" name="instance" id="instance" value="" />
<input type="hidden" name="id" id="id" value="" />
<input type="hidden" name="ids" id="ids" value="" />
</form>
    
     
</body>
</html>