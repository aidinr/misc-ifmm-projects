<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.AssetGeneral" enableViewState="True" CodeBehind="AssetGeneral.ascx.vb" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>



<style>
.projectcentertest
{
width:100%;
height:16000px;
z-index:100;
}
	</style>

<style>
div.asset_name      { width:164px; overflow: hidden;padding-left:5px;
                    text-overflow-mode:ellipsis;font-size:8px }
div.asset_date      { width:120px; overflow: hidden;padding-left:5px;
                    text-overflow-mode:ellipsis;font-size:8px }  
div.asset_filetype  { width:80px; overflow: hidden;
                    text-overflow-mode:ellipsis;font-size:8px }                  
div.asset_filesize  { width:60px; overflow: hidden;
                    text-overflow-mode:ellipsis;font-size:8px }    
div.asset_owner     { width:120px; overflow: hidden;padding-left:5px;
                    text-overflow-mode:ellipsis;font-size:8px }    
div.asset_links    {width:100%;padding-left:2px; height:4px;overflow:hidden;background-image: url(images/dottedline.gif);background-repeat:repeat-x;}                                                                           
span.nowrap       { white-space : nowrap; }
div.attributed-to { position: relative;left:8px }
	</style>

<script type=text/javascript>
//<![CDATA[    ]
function closealltags()
{
 $("#JQAMainx").accordion("option", "active", false);
 $("#JQAKeywordx").accordion("option", "active", false);
 $("#JQAUDFx").accordion("option", "active", false);
 $("#JQAAAx").accordion("option", "active", false);
 $("#JQACommentsx").accordion("option", "active", false);
 $("#JQAPermx").accordion("option", "active", false);
}
function openalltags()
{
 $("#JQAMainx").accordion("option", "active", 0);
 $("#JQAKeywordx").accordion("option", "active", 0);
 $("#JQAUDFx").accordion("option", "active", 0);
 $("#JQAAAx").accordion("option", "active", 0);
 $("#JQACommentsx").accordion("option", "active", 0);
 $("#JQAPermx").accordion("option", "active", 0);
}  

function ToggleSnapMinimizeByType(SnapName, MenuItemIndex)
{
	
}    
   
function ToggleItemCheckedState(MenuItemIndex)
{

  
}
//]]>
</script>
<!--<SCRIPT LANGUAGE="JavaScript" SRC="js/CalendarPopup.js"></SCRIPT>-->
<table width=100% cellpadding=0 cellspacing=0><tr><td width=100% align=left valign=top >

<div class="projectcentertest">

<script language=javascript>
			function actiontype(item)
			{
			if (item.ID=='1') {
				<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('downloadpdf','<%=spAsset_Id%>','<%=spFileName%>');
				//window.open('<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLLocationIDAMDownloadPDF%>?assetid=<%=spAsset_Id%>&filename=<%=spFileName%>&instance=<%=IDAMInstance%>','ConvertingImage','width=500,height=75,location=no');
				return true;
			}
			if (item.ID=='0dwa') {
				<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('associated','<%=spAsset_Id%>','<%=spFileName%>');
			//window.open('<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLLocationIDAMDownloadGetMulti%>?assetid=<%=spAsset_Id%>&parentAssetID=<%=spAsset_Id%>&filename=<%=spProjectName%>&instance=<%=IDAMInstance%>&dtype=associated','ConvertingImage','width=500,height=75,location=no');
				return true;
			}
			if (item.ID=='0') {
				//window.location = '<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLLocationIDAMDownload%>?dType=assetdownload&assetid=<%=spAsset_Id%>&Instance=<%=IDAMInstance%>&size=0'
				<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('single,<%=spAsset_Id%>');
				return true;
			}
			if (item.ID=='2')  {
				<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback("reconvert","",<%=spAsset_Id%>,"");
				alert("Reconvert request sent to the queue.");
				return true;
			}
			
			if (item.ID=='3')  {
				<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('openfile,<%=spAsset_Id%>');
				//window.open('OpenFile.aspx?id=<%=spAsset_Id%>','DownloadTypeAsset','width=500,height=75,location=no');
				//return false;
			}
			var itemID; 
			itemID = item.ID;
			if (itemID>10) {
				<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback('downloadtype','<%=spAsset_Id%>',item.ID,'<%=spFileName%>');
				//window.open('<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLLocationIDAMDownloadType%>?type=' + item.ID + '&assetid=<%=spAsset_Id%>&filename=<%=spFileName%>&instance=<%=IDAMInstance%>','DownloadTypeAsset','width=500,height=75,location=no');
				return true;
			}
			if (itemID=='DeleteYes') {
				/*send callback for deletion then go to project general*/
				if (confirm("Delete this asset?"))
				{
					<%=IdamAdmin.idam.downloadcheck_callback_clientID%>.Callback("delete","",<%=spAsset_Id%>,"");
					window.location = 'IDAM.aspx?page=Project&id=<%=spProjectID%>&type=project';				
					return true;
				}
				
			}		
			if (itemID=='DeleteNo') {
				/*send callback for deletion then go to project general*/			
				return true;
			}
			if (itemID=='CreateVersion') {
				document.getElementById('tagcreateversion').style.display='block';
			return true;
			}						
			if (itemID=='Checkout') {
				document.getElementById('checkindiv').style.display='block';
			return true;
			}		
			if (itemID=='Links') {
				document.getElementById('linkstag').style.display='block';
			return true;
			}				
			return false;
			}
			
			
			
			
			
			
			
  function deleteRow(ids)
  {

   

  //check to see if multi select
	var sAssets;
	var addtype;
	
        
        
    sAssets = <%response.write( GridAssoc.ClientId)%>.GetSelectedItems();
	idstmp = ids.replace('0 ','');
	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Delete selected assets?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (arraylist == ''){
					arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
					} else {
					if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
					arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					}
					}
				}
				
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				addtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	addtype = 'single';
	}
        


	if (addtype == 'multi') {
		if (arraylist != ''){
		

		<% Response.Write(GridAssoc.ClientID) %>.Delete(<% Response.Write(GridAssoc.ClientID) %>.GetRowFromClientId(ids));

		}
	}
	
	if (addtype == 'single') {
	/*assume single*/
	if (confirm("Delete this asset?")) {
	<% Response.Write(GridAssoc.ClientID) %>.Delete(<% Response.Write(GridAssoc.ClientID) %>.GetRowFromClientId(ids));
	}

	} 



  }


function TagAddItem(cvalue)
{
	<%response.write(CallbackTags.ClientID)%>.Callback('Add'+','+cvalue.replace(',','|||'));
}
function TagDeleteItem(cvalue)
{
	<%response.write(CallbackTags.ClientID)%>.Callback('Delete'+','+cvalue);
}
function TagSearch(cvalue)
{
	window.location='IDAM.aspx?page=Results&Type=QuickSearch&keyword=' + cvalue;
}

function TagAddItemServices(cvalue)
{
	<%response.write(CallbackTagsServices.ClientID)%>.Callback('Add'+','+cvalue.replace(',','|||'));
}
function TagDeleteItemServices(cvalue)
{
	<%response.write(CallbackTagsServices.ClientID)%>.Callback('Delete'+','+cvalue);
}
function TagSearchServices(cvalue)
{
	window.location='IDAM.aspx?page=Results&Type=QuickSearch&keyword=' + cvalue;
}

	function TagAddItemMedia(cvalue)
{
	<%response.write(CallbackTagsMedia.ClientID)%>.Callback('Add'+','+cvalue.replace(',','|||'));
}
function TagDeleteItemMedia(cvalue)
{
	<%response.write(CallbackTagsMedia.ClientID)%>.Callback('Delete'+','+cvalue);
}
function TagSearchMedia(cvalue)
{
	window.location='IDAM.aspx?page=Results&Type=QuickSearch&keyword=' + cvalue;
}

function TagAddItemIllust(cvalue)
{
	<%response.write(CallbackTagsIllust.ClientID)%>.Callback('Add'+','+cvalue.replace(',','|||'));
}
function TagDeleteItemIllust(cvalue)
{
	<%response.write(CallbackTagsIllust.ClientID)%>.Callback('Delete'+','+cvalue);
}
function TagSearchIllust(cvalue)
{
	window.location='IDAM.aspx?page=Results&Type=QuickSearch&keyword=' + cvalue;
}
</script>


			
<script>
    $(function () {
        $("#JQAMainx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQAMainx', $("#JQAMainx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQAMainx') == "false") {
            $("#JQAMainx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQAMainx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Asset Information</a></h3> 
								<div class="SnapProjectsWrapperxx">
								<div class="SnapProjectsxx" >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px">								

<table border="0" width="100%" id="table1">
		<tr>
			<td width="100" align="left" valign="top">Name
</td>
			<td align="left" valign="top">
<asp:TextBox id="Textbox_Name" Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td width="100" align="left" valign="top">Description</td>
			<td align="left" valign="top">
<asp:TextBox id="Textbox_Description" TextMode=MultiLine Rows=5 Width="90%" CssClass="InputFieldMain" runat="server"></asp:TextBox>
			</td>
		</tr>	
		<tr>
			<td class="PageContent" width="120" align="left" valign="top" nowrap>
			<font face="Verdana" size="1">Security Level</font></td>
			<td class="PageContent"  width=100% align="left" valign="top">
				<asp:DropDownList ID="SecurityLevel" width="180" Runat="server" >
					<asp:ListItem Value="0">Administrator</asp:ListItem>
					<asp:ListItem Value="1">Internal Use</asp:ListItem>
					<asp:ListItem Value="2">Client Use</asp:ListItem>
					<asp:ListItem Value="3">Public</asp:ListItem>
				</asp:DropDownList>
			</td>
		</tr>	
	</table>
	      
	<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("EDIT_ASSETS","asset",spAsset_ID) Then%>
	<br><br>
	<input type='button' Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:idamaction.value='editAssetDetails';PageForm.submit();return true;"  width=40 value="Save" id="upload" name="upload">
	<%end if%>
</div><!--padding-->

								</div><!--SnapProjects-->
								</div><!--SnapProjectsWrapper-->
								</div>
							
							
						
			
<script>
    $(function () {
        $("#JQAUDFx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQAUDFx', $("#JQAUDFx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQAUDFx') == "false") {
            $("#JQAUDFx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQAUDFx" style="padding-bottom: 15px;">
        <h3>
            <a href="#section1">User Defined Fields</a></h3>
       
        <div class="SnapProjectsWrapperx" style="padding-top:25px;">
             
            <div class="SnapProjectsx">
                <img src="images/spacer.gif" width="188" height="1"><br />
                
                    <div style=" padding-bottom:20px;font-family: verdana;
                        color: #3F3F3F; font-size: 10px; font-weight: normal;">
                        <div style="float:left;" class="ui-state-default ui-corner-all" title=".ui-icon-notice"><span class="ui-icon ui-icon-notice"></span></div><div style="float:left;position:relative;top:3px;padding-left:5px;"> Modify the user defined fields. Note; if tabs are available, please be sure to save your work in between navigating to new tabs.</div>
                        </div>
                    <br />
                   
                   <div id="UDFTabStrip" style="border:0px;"><ul>

                   <asp:Literal ID="ltrlUDFTabStrip" runat=server></asp:Literal>

                   </ul>                  
                   <div id="tabs-1"> 
                        
                        
                           
                                <ComponentArt:CallBack ID="CallbackUDFTab" runat="server" CacheContent="false">
                                    <Content>
                                        <asp:Literal ID="Literal_UDFMAIN" runat="server"></asp:Literal>
                                    </Content>
                                    <LoadingPanelClientTemplate>
                                        <img height="16" src="images/spinner.gif" width="16" border="0">
                                        Rendering information...
                                    </LoadingPanelClientTemplate>
                                </ComponentArt:CallBack>
                           
                   </div>

                    </div>


                    <br /><br /><br />

	<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("EDIT_ASSETS", "asset", spAsset_ID) Then%>
                    <br><br>
                    <input type='button' class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all"
                        onclick="javascript:idamaction.value='editAssetDetails';PageForm.submit();return true;"
                        width="40" value="Save" id="upload" name="upload">
                    <%End If%>
                </div>
                <!--padding-->
            </div>
            <!--SnapProjects-->
        </div>
        <!--SnapProjectsWrapper-->
  
							
							
							
<script>
    $(function () {
        $("#JQAKeywordx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQAKeywordx', $("#JQAKeywordx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQAKeywordx') == "false") {
            $("#JQAKeywordx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQAKeywordx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Keywords</a></h3> 
								
								
								
								<div class="SnapProjectsWrapperx">
								<div class="SnapProjectsx" >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px">

<b>Tags:</b><br><br>
<COMPONENTART:CALLBACK id="CallbackTags" runat="server" CacheContent="false">
<CONTENT>
<asp:Literal ID="LtrlTagListing" Runat=server Text="No tags added." Visible=True  ></asp:Literal>
<asp:Repeater ID="RptrTagListing" Runat=server >
<ItemTemplate>
[<a href="javascript:TagDeleteItem(<%#DataBinder.Eval(Container.DataItem, "tagid")%>);">x</a>]&nbsp;&nbsp;<a href="javascript:TagSearch('<%#DataBinder.Eval(Container.DataItem, "tagname")%>');"><%#DataBinder.Eval(Container.DataItem, "tagname")%></a><br>
</ItemTemplate>
<HeaderTemplate>

</HeaderTemplate>
<FooterTemplate>

</FooterTemplate>
</asp:Repeater>
</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>
<br>
<table cellpadding=0 cellspacing=0><tr>
<td><ComponentArt:ComboBox id="ComboBoxTagging" runat="server" RunningMode="Callback"
        AutoHighlight="false"
        AutoComplete="true"
        AutoFilter="true"
        DataTextField="tagname"
        DataValueField="tagid"
        CssClass="comboBox"
        HoverCssClass="comboBoxHover"
        FocusedCssClass="comboBoxHover"
        TextBoxCssClass="comboTextBox"
        TextBoxHoverCssClass="comboBoxHover"
        DropDownCssClass="comboDropDown"
        ItemCssClass="comboItem"
        ItemHoverCssClass="comboItemHover"
        SelectedItemCssClass="comboItemHover"
        DropHoverImageUrl="images/drop_hover.gif"
        DropImageUrl="images/drop.gif"
        DropDownPageSize="10"
        Width="200">
      </ComponentArt:ComboBox></td>
<td><div style=padding-left:10px;><%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("ADD_DELETE_KEYWORDS","asset",spAsset_ID) Then%>[ <a href="javascript:TagAddItem(<%response.write(ComboBoxTagging.ClientID)%>.get_text());">add</a> ]<%end if%></div></td></tr></table>
<br>(To add a tag, choose one from the dropdown or simply<br>type a new tag and then click the add link to the right.)
<div style="width: 100%;float:left;">
<br><br>




<b><asp:Literal ID="LiteralKeywords1Title" Runat=server></asp:Literal>:</b><br><img src="images/spacer.gif" width=1 height=4 border="0"><br><%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("ADD_DELETE_KEYWORDS","asset",spAsset_ID) Then%><img onclick="javascript:Object_PopUp_Keyword('Keywords.aspx?Keyword=Services');" src="images/editbutton.gif"  border="0"><%end if%>
<div id="DivKeywordServicesHighVolume" style="display:none;">
<COMPONENTART:CALLBACK id="CallbackTagsServices" runat="server" CacheContent="false">
<CONTENT>
<asp:Literal ID="LtrlTagListingServices" Runat=server Text="No keywords added." Visible=True  ></asp:Literal>
<asp:Repeater ID="RptrTagListingServices" Runat=server >
<ItemTemplate>
[<a href="javascript:TagDeleteItemServices(<%#DataBinder.Eval(Container.DataItem, "tagid")%>);">x</a>]&nbsp;&nbsp;<a href="javascript:TagSearchServices('<%#DataBinder.Eval(Container.DataItem, "tagname")%>');"><%#DataBinder.Eval(Container.DataItem, "tagname")%></a><br>
</ItemTemplate>
<HeaderTemplate>

</HeaderTemplate>
<FooterTemplate>

</FooterTemplate>
</asp:Repeater>
</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>
<br>
<table cellpadding=0 cellspacing=0><tr>
<td><ComponentArt:ComboBox id="ComboBoxTaggingServices" runat="server" RunningMode="Callback"
        AutoHighlight="false"
        AutoComplete="true"
        AutoFilter="true"
        DataTextField="tagname"
        DataValueField="tagid"
        CssClass="comboBox"
        HoverCssClass="comboBoxHover"
        FocusedCssClass="comboBoxHover"
        TextBoxCssClass="comboTextBox"
        TextBoxHoverCssClass="comboBoxHover"
        DropDownCssClass="comboDropDown"
        ItemCssClass="comboItem"
        ItemHoverCssClass="comboItemHover"
        SelectedItemCssClass="comboItemHover"
        DropHoverImageUrl="images/drop_hover.gif"
        DropImageUrl="images/drop.gif"
        DropDownPageSize="10"
        Width="200">
      </ComponentArt:ComboBox></td>
<td><div style=padding-left:10px;><%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("ADD_DELETE_KEYWORDS","asset",spAsset_ID) Then%>[ <a href="javascript:TagAddItemServices(<%response.write(ComboBoxTaggingServices.ClientID)%>.get_text());">add</a> ]<%end if%></div></td></tr></table>
<br>(To add a keyword, choose one from the dropdown or simply<br>type a new keyword and then click the add link to the right.)
</div>
<COMPONENTART:CALLBACK id="CALLBACKKeywordsServices" runat="server" CacheContent="false">
	<CONTENT>

<asp:Literal ID="LiteralKeywords1" Runat=server></asp:Literal>

	</CONTENT>
	<LOADINGPANELCLIENTTEMPLATE>
		<IMG height="16" src="images/spinner.gif" width="16" border="0">
	</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>
</div>
<div style="width: 100%;float:left;">
<br><br>










<b><asp:Literal ID="LiteralKeywords2Title" Runat=server></asp:Literal>:</b><br><img src="images/spacer.gif" width=1 height=4 border="0"><br><%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("ADD_DELETE_KEYWORDS","asset",spAsset_ID) Then%><img onclick="javascript:Object_PopUp_Keyword('Keywords.aspx?Keyword=MediaType');" src="images/editbutton.gif"  border="0"><%end if%>
<div id="DivKeywordMediaHighVolume" style="display:none;">
<COMPONENTART:CALLBACK id="CallbackTagsMedia" runat="server" CacheContent="false">
<CONTENT>
<asp:Literal ID="LtrlTagListingMedia" Runat=server Text="No keywords added." Visible=True  ></asp:Literal>
<asp:Repeater ID="RptrTagListingMedia" Runat=server >
<ItemTemplate>
[<a href="javascript:TagDeleteItemMedia(<%#DataBinder.Eval(Container.DataItem, "tagid")%>);">x</a>]&nbsp;&nbsp;<a href="javascript:TagSearchMedia('<%#DataBinder.Eval(Container.DataItem, "tagname")%>');"><%#DataBinder.Eval(Container.DataItem, "tagname")%></a><br>
</ItemTemplate>
<HeaderTemplate>

</HeaderTemplate>
<FooterTemplate>

</FooterTemplate>
</asp:Repeater>
</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>
<br>
<table cellpadding=0 cellspacing=0><tr>
<td><ComponentArt:ComboBox id="ComboBoxTaggingMedia" runat="server" RunningMode="Callback"
        AutoHighlight="false"
        AutoComplete="true"
        AutoFilter="true"
        DataTextField="tagname"
        DataValueField="tagid"
        CssClass="comboBox"
        HoverCssClass="comboBoxHover"
        FocusedCssClass="comboBoxHover"
        TextBoxCssClass="comboTextBox"
        TextBoxHoverCssClass="comboBoxHover"
        DropDownCssClass="comboDropDown"
        ItemCssClass="comboItem"
        ItemHoverCssClass="comboItemHover"
        SelectedItemCssClass="comboItemHover"
        DropHoverImageUrl="images/drop_hover.gif"
        DropImageUrl="images/drop.gif"
        DropDownPageSize="10"
        Width="200">
      </ComponentArt:ComboBox></td>
<td><div style=padding-left:10px;><%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("ADD_DELETE_KEYWORDS","asset",spAsset_ID) Then%>[ <a href="javascript:TagAddItemMedia(<%response.write(ComboBoxTaggingMedia.ClientID)%>.get_text());">add</a> ]<%end if%></div></td></tr></table>
<br>(To add a keyword, choose one from the dropdown or simply<br>type a new keyword and then click the add link to the right.)
</div>
<COMPONENTART:CALLBACK id="CALLBACKKeywordsMediatype" runat="server" CacheContent="false">
	<CONTENT>

<asp:Literal ID="LiteralKeywords2" Runat=server></asp:Literal>

	</CONTENT>
	<LOADINGPANELCLIENTTEMPLATE>
		<IMG height="16" src="images/spinner.gif" width="16" border="0">
	</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>
</div>					
<div style="width: 100%;float:left;">
<br><br>





<b><asp:Literal ID="LiteralKeywords3Title" Runat=server></asp:Literal>:</b><br><img src="images/spacer.gif" width=1 height=4 border="0"><br><%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("ADD_DELETE_KEYWORDS","asset",spAsset_ID) Then%><img onclick="javascript:Object_PopUp_Keyword('Keywords.aspx?Keyword=IllustType');" src="images/editbutton.gif"  border="0"><%end if%>
<div id="DivKeywordIllustHighVolume" style="display:none;">
<COMPONENTART:CALLBACK id="CallbackTagsIllust" runat="server" CacheContent="false">
<CONTENT>
<asp:Literal ID="LtrlTagListingIllust" Runat=server Text="No keywords added." Visible=True  ></asp:Literal>
<asp:Repeater ID="RptrTagListingIllust" Runat=server >
<ItemTemplate>
[<a href="javascript:TagDeleteItemIllust(<%#DataBinder.Eval(Container.DataItem, "tagid")%>);">x</a>]&nbsp;&nbsp;<a href="javascript:TagSearchIllust('<%#DataBinder.Eval(Container.DataItem, "tagname")%>');"><%#DataBinder.Eval(Container.DataItem, "tagname")%></a><br>
</ItemTemplate>
<HeaderTemplate>

</HeaderTemplate>
<FooterTemplate>

</FooterTemplate>
</asp:Repeater>
</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>
<br>
<table cellpadding=0 cellspacing=0><tr>
<td><ComponentArt:ComboBox id="ComboBoxTaggingIllust" runat="server" RunningMode="Callback"
        AutoHighlight="false"
        AutoComplete="true"
        AutoFilter="true"
        DataTextField="tagname"
        DataValueField="tagid"
        CssClass="comboBox"
        HoverCssClass="comboBoxHover"
        FocusedCssClass="comboBoxHover"
        TextBoxCssClass="comboTextBox"
        TextBoxHoverCssClass="comboBoxHover"
        DropDownCssClass="comboDropDown"
        ItemCssClass="comboItem"
        ItemHoverCssClass="comboItemHover"
        SelectedItemCssClass="comboItemHover"
        DropHoverImageUrl="images/drop_hover.gif"
        DropImageUrl="images/drop.gif"
        DropDownPageSize="10"
        Width="200">
      </ComponentArt:ComboBox></td>
<td><div style=padding-left:10px;><%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("ADD_DELETE_KEYWORDS","asset",spAsset_ID) Then%>[ <a href="javascript:TagAddItemIllust(<%response.write(ComboBoxTaggingIllust.ClientID)%>.get_text());">add</a> ]<%end if%></div></td></tr></table>
<br>(To add a keyword, choose one from the dropdown or simply<br>type a new keyword and then click the add link to the right.)
</div>
<COMPONENTART:CALLBACK id="CALLBACKKeywordsIllusttype" runat="server" CacheContent="false">
	<CONTENT>

<asp:Literal ID="LiteralKeywords3" Runat=server></asp:Literal>

	</CONTENT>
	<LOADINGPANELCLIENTTEMPLATE>
		<IMG height="16" src="images/spinner.gif" width="16" border="0">
	</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>

</div>
<div style="width: 100%;float:left;">
<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("EDIT_ASSETS","asset",spAsset_ID) Then%>
<br><br><br><br><br><br><br><br><br><br>
<br><br><input type='button' Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="javascript:idamaction.value='editAssetDetails';PageForm.submit();return true;"  width=40 value="Save" id="upload" name="upload">
<%end if%>

</div>

													</div>
													</div>
													

								</div><!--padding-->
								</div>
							
							
											
<script>
    $(function () {
        $("#JQAAAx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQAAAx', $("#JQAAAx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQAAAx') == "false") {
            $("#JQAAAx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQAAAx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Associated Assets</a></h3> 
								<div class="SnapProjectsWrapperx">
								<div class="SnapProjectsx" >
		
		
		
		
		

											<img src="images/spacer.gif" width="188" height="1">
											
<div style=" padding-bottom:20px;font-family: verdana;
color: #3F3F3F; font-size: 10px; font-weight: normal;">
<div style="float:left;" class="ui-state-default ui-corner-all" title=".ui-icon-notice"><span class="ui-icon ui-icon-notice"></span></div>
<div style="float:left;position:relative;top:3px;padding-left:5px;"> 
This section lists all associated assets.</div>
</div>
												
												
												<br>
												
												
												
												
											<%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("ADD_ASSOCIATED","asset",spAsset_ID) Then%>
											<table><tr><td width=250><img src=images/spacer.gif width=200 height=1><br>[ <a href="javascript:Object_PopUp_Upload_Assoc();">add</a> ] </td><td width="100%" align=right>Search Filter:</font></b><img src="images/spacer.gif" width=5 height=1></td><td align=right width=50 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:70px"  onkeyup="javascript:<%response.write( GridAssoc.ClientId)%>.Filter('name LIKE \'%' + this.value + '%\' OR media_type_name LIKE \'%' + this.value + '%\'');"></td></tr></table>
											<%end if%>
											
											
											
											
											
											
											
											<img src="images/spacer.gif" width=1 height=4><br>
<asp:Literal id="LiteralNoAssocAssets" Text="No associated assets available" visible=false runat="server" />

<script language="javascript">
function loadAssociatedContextMenu(evt, id) {
loadContextMenu(evt, id, <%=GridAssoc.ClientID%>);
}
</script>
<COMPONENTART:GRID 
id="GridAssoc" 
runat="server" 
AutoFocusSearchBox="false"
AutoCallBackOnInsert="true"
AutoCallBackOnUpdate="true"
AutoCallBackOnDelete="true"
pagerposition="2"
ScrollBar="Off"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ScrollPopupClientTemplateId="ScrollPopupTemplate" 
Sort="update_date desc"
Height="10" Width="100%"
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateRecentAssets" 
EnableViewState="true"
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRowCategory" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="true" 
GroupingPageSize="1" 
PagerTextCssClass="GridFooterTextCategory" 
PagerStyle="Numbered" 
PageSize="10" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeader" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="true" 
AllowSorting="False"
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
ScrollBarWidth="15" 
AllowPaging="true" >
<ClientTemplates>
<ComponentArt:ClientTemplate Id="EditTemplateRecentAssets">
            <img src="images/13Save2.gif" border=0 alt="Download" onClick="loadAssociatedContextMenu(event, '## DataItem.ClientId ##');"> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateRecentAssets">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateRecentAssets">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateRecentAssets">
            <img src="## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>   
          <ComponentArt:ClientTemplate Id="LookupProjectTemplateRecentAssets">
            <A href="## DataItem.GetMember("alink").Value ##"><img border=0 src="images/goto.gif"></a>
          </ComponentArt:ClientTemplate>                                       
<componentart:ClientTemplate ID="LoadingFeedbackTemplateRecentAssets">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>                                 
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
            EditFieldCssClass="EditDataField"
            EditCommandClientTemplateId="EditCommandTemplateRecentAssets"
            InsertCommandClientTemplateId="InsertCommandTemplateRecentAssets" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplateRecentAssets" dataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="50" FixedWidth="True" />
<ComponentArt:GridColumn DataCellCssClass="DataCell" Align="Center" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplateRecentAssets" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="16" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="True" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Type" AllowEditing="false" Width="110" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Updated" AllowEditing="false" Width="110" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="True" HeadingText="User" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="username" ></componentart:GridColumn>
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplateRecentAssets" EditControlType="EditCommand" Width="100" Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="URLPREFIXPREVIEW" SortedDataCellCssClass="SortedDataCell" DataField="URLPREFIXPREVIEW"></componentart:GridColumn>

</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
											

		

								</div>
								</div><!--SnapProjectsWrapper-->
								</div>
							
																	
<script>
    $(function () {
        $("#JQACommentsx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQACommentsx', $("#JQACommentsx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQACommentsx') == "false") {
            $("#JQACommentsx").accordion("option", "active", false);
        }
    });
    
</script> 
<asp:literal ID="ltrl_JQACommentsx" runat=server></asp:literal>

 
<div id="JQACommentsx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Comments</a></h3> 
								<div class="SnapProjectsWrapperx">
								<div class="SnapProjectsx" >
								<div style="padding:5px;">
		
		
		
		
<b><font size=2 face=arial,verdana color=#000000>Comments</font></b><br>
            <%If WebArchives.iDAM.Web.Core.IDAMFunctions.getRoleWO("CREATE_COMMENTS","asset",spAsset_ID) Then%>
            [ <a href="javascript:showComment();"> 
            new</a> ]
            <%end if%><br>
<div id="commenttag" style="display:none;">
                          
            
              <p><font face="Arial" size="1" color="#000000">Comment</font><br>
              <textarea rows="3" name="Comment" cols="34"></textarea></p>
              <p><font size="1" face="Arial">Priority</font><br>
              <input class="preferences" type="radio" value="1" name="P" checked><font color="#808080" size="1" face="Arial">High</font> <input class="preferences" type="radio" name="P" value="2"><font color="#808080" size="1" face="Arial">Medium</font>
              <input class="preferences" type="radio" name="P" value="3"><font color="#808080" size="1" face="Arial">Low<br>
              <br>
              </font><font color="#000000" size="1" face="Arial">
              Notify Users</font><font color="#808080" size="1" face="Arial"><br>
              <input class="preferences" type="radio" name="N" value="N" checked>None&nbsp;
              <input class="preferences" type="radio" name="N" value="A">All&nbsp; <input class="preferences" type="radio" name="N" value="O">Owner</font></p>
              <p><input type="button" value="Add Comment" name="Add" id="button_add_comment" runat="server"></p>
           
            <p>
</div>
            <br>
            
            
	        <asp:Repeater ID=Repeater_comments Runat=server>
            
            <ItemTemplate>
          
		         
					<font color="#808080" size="1" face="Arial"><%#DataBinder.Eval(Container.DataItem, "date_posted")%> </font><font color="#666666" size="1" face="Arial"><img border="0" src="dash.jpg" width="4" height="4">
								</font>
		         
					<font size="1" face="Arial" color="#808080"> Priority <%#ReturnPriority(CType(DataBinder.Eval(Container.DataItem, "Priority"), String))%>
					<img border="0" src="dash.jpg" width="4" height="4"> Posted By <%#DataBinder.Eval(Container.DataItem, "firstname")%>&nbsp;<%#DataBinder.Eval(Container.DataItem, "lastname")%> </font><font color="#666666" size="1" face="Arial"><br>
								</font>
		         
					<font color="#000000" size="1" face="Arial"><img border="0" src="images/spacer.gif" height="2" width="11"><%#DataBinder.Eval(Container.DataItem, "description")%><br /></font>
					</font>
		         
					<div style="WIDTH: 100%; TEXT-ALIGN: right">[ <a href="javascript:showComment();">reply</a> ]</div><br><img border="0" src="images/spacer.gif" height="4" width="11"><br>
		            
			<div style="padding:5px;">
			<div class="asset_links">
			</div></div>
			</ItemTemplate>
			<HeaderTemplate>

			</HeaderTemplate>
			<FooterTemplate>

			</FooterTemplate> 
			</asp:Repeater> 
			
			<asp:Literal ID=Literal_nocomments runat=server Text="No comments" Visible=false></asp:Literal>

			<script>
			    document.getElementById("commenttag").style.display = "none";
			    function showComment() {
			        if (document.getElementById("commenttag").style.display == "block") {
			            document.getElementById("commenttag").style.display = "none";
			        } else {
			            document.getElementById("commenttag").style.display = "block";
			        }
			    }
			</script>
		
		
		
		
		
		
		
		
		
								</div><!--end padding-->
								</div>
								</div><!--SnapProjectsWrapper-->
								</div>
							
													
																	
<script>
    $(function () {
        $("#JQAPermx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQAPermx', $("#JQAPermx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQAPermx') == "false") {
            $("#JQAPermx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQAPermx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Permissions</a></h3> 
								<div class="SnapProjectsWrapperx">
								<div class="SnapProjectsx" >
								<div style="padding:5px;">
		
		
		
						
 
  <div style=" padding-bottom:20px;font-family: verdana;
color: #3F3F3F; font-size: 10px; font-weight: normal;">
<div style="float:left;" class="ui-state-default ui-corner-all" title=".ui-icon-notice"><span class="ui-icon ui-icon-notice"></span></div>
<div style="float:left;position:relative;top:3px;padding-left:5px;"> 
Change permissions for this object by selecting a group or user from the list and adding to the active list.</div>
</div>
  
  
  <br>
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td class="PageContent" width="360" valign=top>
<!--available list-->
<b>Available Permissions</b><br><br>



<COMPONENTART:GRID id="GridPermissionAvailable" runat="server" pagerposition="2" 
ScrollBar="Off" 
Sort="lastname asc" 
Height="10" Width="250" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategory" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" AllowPaging="true" 
cachecontent="false" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategory">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategory">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                            
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" 
 EditFieldCssClass="EditDataField" 
 EditCommandClientTemplateId="EditCommandTemplateCategory" 
 InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataCellClientTemplateId="TypeIconTemplateCategory" DataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Firstname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="firstname" Width="70"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Lastname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="lastname" Width="100"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" AllowEditing="false" AllowGrouping="False" Width="120" SortedDataCellCssClass="SortedDataCell" DataCellCssClass="LastDataCell" DataField="description"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<div style=  "text-align:right;BORDER-RIGHT: 1px solid; PADDING-RIGHT: 4px; BORDER-TOP: 1px solid; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: 1px solid; PADDING-TOP: 4px; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: gainsboro">
<b>Search for user/group:</b> <input Class="InputFieldMain" type=text value="" id=search_filter  style="width:120px" onkeyup="javascript:<%response.write( GridPermissionAvailable.ClientId)%>.Filter('firstname LIKE \'%' + this.value + '%\' or lastname LIKE \'%' + this.value + '%\'');">
</div>
					</td>
					<td class="PageContent" width="120" valign= middle>
		
					<asp:Button id="btnAddPermissions" runat="server" Text="     Add >> "></asp:Button><br><br>
					<asp:Button id="btnRemovePermissions" runat="server" Text="<< remove"></asp:Button>
					</td>
					<td class="PageContent" width="100%" valign=top>
					
					
					
					
<!--active list-->
<b>Active Permissions</b><br><br>


<COMPONENTART:GRID id="GridPermissionActive" runat="server" autoPostBackonDelete="true" 
pagerposition="2" 
ScrollBar="Off" 
Sort="typeofobject asc" 
Height="10" Width="350" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategoryActive" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="false" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="false" CssClass="Grid" RunningMode="callback" AllowPaging="false" 
cachecontent="false" PagerStyle="Numbered" PageSize="500" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategoryActive">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateCategoryActive">
          <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategoryActive">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                                
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategoryActive" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataField="imagesource" DataCellClientTemplateId="TypeIconTemplateCategoryActive" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True"  />
<componentart:GridColumn AllowEditing="false" HeadingText="Name" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" FixedWidth="false"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Description" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataCellCssClass="LastDataCell" DataField="description" width="100" FixedWidth="true"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
					</td>					
				</tr>
			</table>													
									
		
		
		
		
		
		
		
		
		
		
		
		
		
		
								</div><!--end padding-->
								</div>
								</div><!--SnapProjectsWrapper-->
								</div>
		
</div><!--bottom padding-->							
	

</td></tr></table>



<script language="javascript">



function Keyword_PopUp(strUrl, popupName, intHeight, intWidth)
{
	if (strUrl == null || strUrl.Length <= 0)
		return;

	var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=no,resizable=yes,toolbar=no";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;

	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures, false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
}




  
 function highlightAssetToggle(checkbox,asset_id)
  {
  var highlightbox;
  highlightbox = document.getElementById('assethighlight_' + asset_id);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }
}
  
  
  
function RefreshKeywordServices()
{

	<%if not WebArchives.iDAM.Web.Core.IDAMWebSession.Config.UseHighVolumeKeywords then%>
		<% Response.Write(CALLBACKKeywordsServices.ClientID) %>.Callback('Refresh');
		alert('Keywords refreshed.');
	<%end if%>		
}

function RefreshKeywordMediaType()
{
		<%if not WebArchives.iDAM.Web.Core.IDAMWebSession.Config.UseHighVolumeKeywords then%>
		<% Response.Write(CALLBACKKeywordsMediaType.ClientID) %>.Callback('Refresh');
		alert('Keywords refreshed.');
		return false;
    <%end if%>	
}

function RefreshKeywordIllustType()
{
	<%if not WebArchives.iDAM.Web.Core.IDAMWebSession.Config.UseHighVolumeKeywords then%>
	<% Response.Write(CALLBACKKeywordsIllustType.ClientID) %>.Callback('Refresh');
    alert('Keywords refreshed.');
    <%end if%>	
}

  
  
  
function Object_PopUp_Upload_Assoc()
{
  //add something here to redirect to java or form based upload	
  Object_PopUp('includes/Upload/UploadAsync.aspx?projectid=' + <%=spProjectID%> + '&categoryid=' + <%=spCategory_ID%> + '&pa=' + '<%=spAsset_ID%>' + '&uid=<%=webarchives.iDAM.Web.Core.IDAMWebSession.IDAMUser.UserID.tostring%>','Upload',640,777);
}	

  
  
  
  //Load keywords on entry not page load for performance
  <%if not WebArchives.iDAM.Web.Core.IDAMWebSession.Config.UseHighVolumeKeywords then%>
  <% Response.Write(CALLBACKKeywordsServices.ClientID) %>.Callback('Refresh');
  <% Response.Write(CALLBACKKeywordsMediaType.ClientID) %>.Callback('Refresh');
  <% Response.Write(CALLBACKKeywordsIllustType.ClientID) %>.Callback('Refresh');
  <%else%>
  document.getElementById('DivKeywordMediaHighVolume').style.display='block';
  document.getElementById('DivKeywordIllustHighVolume').style.display='block';
  document.getElementById('DivKeywordServicesHighVolume').style.display='block';
  <%end if%>
function UDFTabCallback(id)
{
<%Response.Write(CallbackUDFTab.ClientID)%>.Callback(id);
}
  
  </script>

