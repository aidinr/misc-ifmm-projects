<%@ Reference Control="~/PreviewPane.ascx" %>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.Configuration" CodeBehind="Configuration.ascx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<div class="previewpaneMyProjects ui-accordion-header ui-helper-reset ui-state-active " id="toolbarheading" style="z-index:99;border-top:0px;border-bottom:0px;">
	<!--Project cookie and top menu-->
		<table cellspacing="0" cellpadding="4" id="table2" width="100%">
			<tr><td valign="top" width="67">
			<img  src="images/ControlPanel.gif">
			</td>
			<td valign="top">
			<font face="Verdana" size="1"><b>Configuration Information</b><br>
				<span style="FONT-WEIGHT: 400">Manage IDAM system configuration parameters.<br></span>
				
				</font>
			</td>
			<td id="Test" valign="top" align="right">
			
			<!--<div style="padding-top:2px;">
			[ <a href="#">help</a> ]
				</div>-->
			</td></tr>
			<tr><td valign="top" colspan="3">
			</td>
			</tr>
		</table>
</div> <!--end preview pane-->
<div id="subtabset" style="position:relative;top:-4px;z-index:2; font-size:10px;">
<input type="radio" id="radio1" name="subtab" checked="checked" /><label for="radio1">General</label><img src="images/spacer.gif" width="2" alt="" />
</div>
<div style="border-top:1px solid silver;position:relative;top:-25px;z-index:1;"></div>

<script>
    $(function () {
        $("#subtab").button();
        $("#subtabset").buttonset();
    });
	</script>



<div class="previewpaneProjectsx" id="toolbar">

<script language=javascript>
  
        // Forces the grid to adjust to the new size of its container          
      function resizeGridFileType(DomElementId, NewPaneHeight, NewPaneWidth)
      {
        <% Response.Write(GridFileType.ClientID) %>.Render();
        //alert('here');
      } 
  function editGrid(rowId)
  {

    <% Response.Write(GridFileType.ClientID) %>.Edit(<% Response.Write(GridFileType.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  

  function onInsert(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
   function editGridDT(rowId)
  {

    <% Response.Write(GridDownloadType.ClientID) %>.Edit(<% Response.Write(GridDownloadType.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  

  function onInsertDT(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
  
  
   function editGridIT(rowId)
  {

    <% Response.Write(GridImageType.ClientID) %>.Edit(<% Response.Write(GridImageType.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  

  function onInsertIT(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
    function ShowContextMenu(item, column, evt) 
  {
    <% Response.Write(GridFileType.ClientID) %>.Select(item); 
   
    return false; 
  }
  
  function onUpdate(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }
  
    
  function onUpdateDT(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }
  
  
    
  function onUpdateIT(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }


  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridFileType.ClientID) %>.Page(1); 
  }

  function onDelete(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
  
  function editRow()
  {
    <% Response.Write(GridFileType.ClientID) %>.EditComplete();     
  }

  function insertRow()
  {
    <% Response.Write(GridFileType.ClientID) %>.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    <% Response.Write(GridFileType.ClientID) %>.Delete(<% Response.Write(GridFileType.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  
    function onDeleteDT(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
  
  function editRowDT()
  {
    <% Response.Write(GridDownloadType.ClientID) %>.EditComplete();     
  }

  function insertRowDT()
  {
    <% Response.Write(GridDownloadType.ClientID) %>.EditComplete(); 
  }

  function deleteRowDT(rowId)
  {
    <% Response.Write(GridDownloadType.ClientID) %>.Delete(<% Response.Write(GridDownloadType.ClientID) %>.GetRowFromClientId(rowId)); 
  }




  function onDeleteIT(item)
  {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
  }
  
  function editRowIT()
  {
    <% Response.Write(GridImageType.ClientID) %>.EditComplete();     
  }

  function insertRowIT()
  {
    <% Response.Write(GridImageType.ClientID) %>.EditComplete(); 
  }

  function deleteRowIT(rowId)
  {
    <% Response.Write(GridImageType.ClientID) %>.Delete(<% Response.Write(GridImageType.ClientID) %>.GetRowFromClientId(rowId)); 
  }

  

</script>



<div  id="browsecenter">



<img src="images/spacer.gif"  height="15" width="5"><br>
<!--<div style="padding:10px;border-left:1px #969698 solid; border-right:1px #969698 solid;border-bottom:1px #969698 solid;border-top:1px #969698 solid;">-->






					
<script>
    $(function () {
        $("#JQCFTx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQCFTx', $("#JQCFTx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQCFTx') == "false") {
            $("#JQCFTx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQCFTx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">File Types</a></h3> 
									<div class="SnapProjectsWrapperxx">
										<div class="SnapProjectsxx">




<div style= "padding:5px;background-color:   #f9f9f9;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; font-size: 10px;font-weight: normal;">
IDAM automatically populates new filetypes when they are uploaded into the system.  Use this editable grid to change how
IDAM handles the filetypes.  Note:  At the time the filetype was first entered into the system, IDAM tried to determine the 
best capabilities of that filetype.  Only change a filetypes capability if new funcationality has been added to the 
IDAM conversion engine.  
<br><br>
Administrators only.  Modifying these values improperly can negatively impact the IDAM environment and or performance.
</div>


<div style="padding-right:10px;padding-bottom:10px;">
<table><tr><td width="100%" align=right>Search File Types:</font></b><img src="images/spacer.gif" width=5 height=1></td><td align=right width=50 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:70px" onkeyup="javascript:<%response.write( GridFileType.ClientId)%>.Filter('name LIKE \'%' + this.value + '%\' or extension LIKE \'%' + this.value + '%\'');"></td></tr></table>
</div>
<COMPONENTART:GRID id="GridFileType" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ClientSideOnInsert="onInsert" ClientSideOnUpdate="onUpdate" ClientSideOnDelete="onDelete" ClientSideOnCallbackError="onCallbackError" ScrollPopupClientTemplateId="ScrollPopupTemplate" Sort="name asc" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplate" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="20" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="True" GroupingPageSize="5" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="20" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=125 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplate">
            <a href="javascript:editGrid('## DataItem.ClientId ##');">Edit</a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplate">
          <img src="## DataItem.GetMember("imagesource").Value ##" border="0"  > 
          </ComponentArt:ClientTemplate>                              
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="media_type" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate"  HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="True" HeadingText="Name" AllowGrouping="False" width="120" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="False" HeadingText="Extension" AllowGrouping="False" width="80" SortedDataCellCssClass="SortedDataCell" DataField="extension" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" AllowEditing="true" AllowGrouping="False" width="120" SortedDataCellCssClass="SortedDataCell" DataField="description"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Use Icon" AllowEditing="true" AllowGrouping="False" width="40" SortedDataCellCssClass="SortedDataCell" DataField="useicon"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Use Document Converter" AllowEditing="true" width="40" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="useprintdriver"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Use Image Converter" AllowEditing="true" width="40" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="useimageconvert"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Allow Export to PDF" AllowEditing="true" width="40" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="exportpdf"></componentart:GridColumn>
<componentart:GridColumn HeadingText="System File" AllowEditing="false" width="40" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="system"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Converter Type" AllowEditing="true" width="60" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="convertertype"></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Align="Center" Width="80" FixedWidth="True"  DataCellCssClass="LastDataCell" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="media_type" SortedDataCellCssClass="SortedDataCell" DataField="media_type"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="imagesource" SortedDataCellCssClass="SortedDataCell" DataField="imagesource"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<!--<componentart:GridColumn AllowEditing="false" HeadingText="Assets" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid" ForeignTable="Assets" ForeignDataKeyField="uniqueid" ForeignDisplayField="acount"></componentart:GridColumn>-->

















										</div><!--SnapProjects-->
									</div><!--padding-->												
								</div><!--END SNAPFileType-->	
							
							
							
							
							
							
							
							
							
										
<script>
    $(function () {
        $("#JQDTx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQDTx', $("#JQDTx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQDTx') == "false") {
            $("#JQDTx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQDTx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Download Types</a></h3> 
									<div class="SnapProjectsWrapperx">
										<div class="SnapProjectsx">






<div style= "padding:5px;background-color:   #f9f9f9;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; font-size: 10px;font-weight: normal;">
System administrators can add/modify/delete global download capabilities.  Defining a download type enables appropriate file types to
convert to new formats on the fly.  Once the download has been defined and set active, IDAM users will be able to choose this file setting 
for download on the appropriate asset detail page.
<br><br>
Administrators only.  Modifying these values improperly can negatively impact the IDAM environment and or performance.
</div>

<div style="padding-right:10px;padding-bottom:10px;">
<table><tr><td width="100%" align=right>Search Download Types:</font></b><img src="images/spacer.gif" width=5 height=1></td><td align=right width=50 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:70px" onkeyup="javascript:<%response.write( GridDownloadType.ClientId)%>.Filter('name LIKE \'%' + this.value + '%\' or description LIKE \'%' + this.value + '%\'');"></td></tr></table>
</div>
<COMPONENTART:GRID id="GridDownloadType" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ClientSideOnInsert="onInsertDT" ClientSideOnUpdate="onUpdateDT" ClientSideOnDelete="onDeleteDT" ClientSideOnCallbackError="onCallbackError" ScrollPopupClientTemplateId="ScrollPopupTemplate" Sort="name asc" Height="10" Width="99%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplatedt" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="20" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="True" GroupingPageSize="5" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="20" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplatedt">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=125 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplatedt">
            <a href="javascript:editGridDT('## DataItem.ClientId ##');">Edit</a> | <a href="javascript:deleteRowDT('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplatedt">
            <a href="javascript:editRowDT();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplatedt">
            <a href="javascript:insertRowDT();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplatedt">
          <img src="images/icon_posting.gif" border="0"  > 
          </ComponentArt:ClientTemplate>                              
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplatedt" InsertCommandClientTemplateId="InsertCommandTemplatedt" DataKeyField="download_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplatedt"  HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="True" HeadingText="Name" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="80" FixedWidth="True" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="True" HeadingText="Description" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="80" FixedWidth="True" DataField="description" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Format" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="format" Width="80" FixedWidth="True" ForeignTable="FormatTable" ForeignDataKeyField="format_id" ForeignDisplayField="format_name"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Width" AllowEditing="true" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="60" FixedWidth="True" DataField="width"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Height" AllowEditing="true" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="60" FixedWidth="True" DataField="height"></componentart:GridColumn>
<componentart:GridColumn HeadingText="DPI" AllowEditing="true" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="40" FixedWidth="True" DataField="dpi"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Bits" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="bits" Width="40" FixedWidth="True" ForeignTable="BitsTable" ForeignDataKeyField="bit_id" ForeignDisplayField="bits_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Quality" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="quality" Width="80" FixedWidth="True" ForeignTable="QualityTable" ForeignDataKeyField="quality_id" ForeignDisplayField="quality_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Active" AllowEditing="true" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="40" FixedWidth="True" DataField="active"></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplatedt" EditControlType="EditCommand" Align="Center" Width="80" FixedWidth="True"  DataCellCssClass="LastDataCell" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="download_id" SortedDataCellCssClass="SortedDataCell" DataField="download_id"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<!--<componentart:GridColumn AllowEditing="false" HeadingText="Assets" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid" ForeignTable="Assets" ForeignDataKeyField="uniqueid" ForeignDisplayField="acount"></componentart:GridColumn>-->



			<br>
			<input type="button" onclick="<%response.write (GridDownloadType.ClientID)%>.Page(<%response.write (GridDownloadType.ClientID)%>.PageCount-1);<%response.write (GridDownloadType.ClientID)%>.Table.AddRow();" value="Add Download Type" />
			














										</div><!--SnapProjects-->
									</div><!--padding-->												
								</div><!--END SNAPDownloadType-->	
							
							
							
							
							
							
							
										
<script>
    $(function () {
        $("#JQCIIx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQCIIx', $("#JQCIIx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQCIIx') == "false") {
            $("#JQCIIx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQCIIx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">iDAM Images</a></h3> 
									<div class="SnapProjectsWrapperx">
										<div class="SnapProjectsx">





<div style= "padding:5px;background-color:   #f9f9f9;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; font-size: 10px;font-weight: normal;">
Modifying the IDAM image settings will change how IDAM stores screen and thumbnail images within the working repository.  These values 
should be set at the time of installation and only in rare situations should they require modification.  
<br><br>
Administrators only.  Modifying these values improperly can negatively impact the IDAM environment and or performance.
</div>


<div style="padding-right:10px;padding-bottom:10px;">
<table><tr><td width="100%" align=right>Search Image Types:</font></b><img src="images/spacer.gif" width=5 height=1></td><td align=right width=50 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:70px" onkeyup="javascript:<%response.write( GridImageType.ClientId)%>.Filter('name LIKE \'%' + this.value + '%\' or description LIKE \'%' + this.value + '%\'');"></td></tr></table>
</div>
<COMPONENTART:GRID id="GridImageType" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ClientSideOnInsert="onInsertIT" ClientSideOnUpdate="onUpdateIT" ClientSideOnDelete="onDeleteIT" ClientSideOnCallbackError="onCallbackError" ScrollPopupClientTemplateId="ScrollPopupTemplate" Sort="name asc" Height="10" Width="99%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplatedi" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="20" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="True" GroupingPageSize="5" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="20" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplatedi">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=125 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplatedi">
            <a href="javascript:editGridIT('## DataItem.ClientId ##');">Edit</a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplatedi">
            <a href="javascript:editRowIT();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplatedi">
            <a href="javascript:insertRowIT();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplatedi">
          <img src="images/image_thumb.gif" border="0"  > 
          </ComponentArt:ClientTemplate>                              
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplatedi" InsertCommandClientTemplateId="InsertCommandTemplatedi" DataKeyField="image_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplatedi"  HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="True" HeadingText="Name" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="80" FixedWidth="True" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="True" HeadingText="Description" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="80" FixedWidth="True" DataField="description" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Screen Height" AllowEditing="true" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="sheight" Width="40" FixedWidth="True"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Screen Width" AllowEditing="true" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="swidth" Width="40" FixedWidth="True"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Screen Crop" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="scrop" ForeignTable="CropTable" Width="60" FixedWidth="True" ForeignDataKeyField="crop_id" ForeignDisplayField="crop_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Screen Crop Vertical Align" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="scropwalign" Width="60" FixedWidth="True" ForeignTable="AlignTable" ForeignDataKeyField="align_id" ForeignDisplayField="align_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Screen Crop Horizontal Align" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="scrophalign" Width="60" FixedWidth="True" ForeignTable="AlignTable" ForeignDataKeyField="align_id" ForeignDisplayField="align_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Screen Crop Color" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="scropcolor" Width="60" FixedWidth="True" ForeignTable="ColorTable" ForeignDataKeyField="color_id" ForeignDisplayField="color_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Thumbnail Height" AllowEditing="true" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="40" FixedWidth="True" DataField="theight"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Thumbnail Width" AllowEditing="true" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="40" FixedWidth="True" DataField="twidth"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Thumbnail Crop" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="tcrop" Width="60" FixedWidth="True" ForeignTable="CropTable" ForeignDataKeyField="crop_id" ForeignDisplayField="crop_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Thumbnail Crop Vertical Align" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" Width="60" FixedWidth="True" DataField="tcropwalign" ForeignTable="AlignTable" ForeignDataKeyField="align_id" ForeignDisplayField="align_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Thumbnail Crop Horizontal Align" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" Width="60" FixedWidth="True" DataField="tcrophalign" ForeignTable="AlignTable" ForeignDataKeyField="align_id" ForeignDisplayField="align_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Thumbnail Crop Color" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" Width="60" FixedWidth="True" DataField="tcropcolor" ForeignTable="ColorTable" ForeignDataKeyField="color_id" ForeignDisplayField="color_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="System" AllowEditing="true" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="40" FixedWidth="True" DataField="system"></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplatedi" EditControlType="EditCommand" Align="Center" Width="40" FixedWidth="True" DataCellCssClass="LastDataCell" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="image_id" SortedDataCellCssClass="SortedDataCell" DataField="image_id"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<!--<componentart:GridColumn AllowEditing="false" HeadingText="Assets" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid" ForeignTable="Assets" ForeignDataKeyField="uniqueid" ForeignDisplayField="acount"></componentart:GridColumn>-->



			<br>
			<input type="button" onclick="<%response.write (GridImageType.ClientID)%>.Page(<%response.write (GridImageType.ClientID)%>.PageCount-1);<%response.write (GridImageType.ClientID)%>.Table.AddRow();" value="Add Image Type" />
			














										</div><!--SnapProjects-->
									</div><!--padding-->												
								</div><!--END SNAPImageType-->	
							
							
							
							
							
							
							
							
							


      </div><!--END browsecenter-->
</div><!--END MAIN-->
      
      
      
      
      


      <script type=text/javascript>
  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

  
  function BrowseOnDoubleClick(SelectedGridRow)
  {
  
  	var itemvaluetmp;
	itemvaluetmp = SelectedGridRow.GetMember('uniqueid').Value;
	if (itemvaluetmp.substring(0,1) == 'P') 
	{
		itemvaluetmp = itemvaluetmp.replace('P_','');
		window.location.href = 'IDAM.aspx?page=Project&id=' + itemvaluetmp + '&type=project'
    
	} else {
		itemvaluetmp = itemvaluetmp.replace('C_','');
		window.location.href = 'IDAM.aspx?page=Browse&id=' + itemvaluetmp + '&type=category';
	}
	
	return true;
  
  
  
	
  }
  
    function highlightProjectToggle(checkbox,projectid)
  {
  var highlightbox;
  highlightbox = document.getElementById('projecthighlight_' + projectid);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  

  </script>
      
<div style="PADDING-LEFT:10px"><div style="PADDING-RIGHT:3px;PADDING-LEFT:3px;PADDING-BOTTOM:3px;PADDING-TOP:3px"></div></div>






<script language=javascript>
//thumbnailsarea.style.display = 'none';
</script>

