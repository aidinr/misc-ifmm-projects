﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Tenant.aspx.vb" Inherits="IdamAdmin.Tenant" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Property Module</title>
    <style type="text/css">
    @import url("tabStyle.css");
    @import url("gridStyle.css");
    @import url("multipageStyle.css");
    @import url("snapStyle.css");
    BODY { MARGIN: 0px; background-color:#F7F7F7; }
	
    </style>
</head>
<body>
    <form id="form1" runat="server">
    
    
    
    
    
    
    <script type="text/javascript">

        function onCallbackError(excString) {
            //if (confirm('Invalid data has been entered. View details?')) alert(excString);
        }

function getURLPreview()
{
return '<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?instance=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMInstance%>&type=asset&size=2&crop=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMGridThumbnailCrop%>&height=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMGridThumbnailHeight%>&width=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMGridThumbnailWidth%>&qfactor=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMQfactor%>&cache=1&id=';
}

function getURLPreviewIcon()
{
    return '<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveIconURLBase%>?instance=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMInstance%>&size=0&cache=1&id=';
}




			</script>
    
    
    
    
    
    
    
    
    
    
    
    <div>
<div  style="PADDING-RIGHT:20px;PADDING-LEFT:20px;PADDING-BOTTOM:20px;PADDING-TOP:20px;POSITION:relative">
			    <div>
				    <div style="BORDER-RIGHT:1px solid; PADDING-RIGHT:10px; BORDER-TOP:1px solid; PADDING-LEFT:10px; PADDING-BOTTOM:10px; BORDER-LEFT:1px solid; PADDING-TOP:10px; BORDER-BOTTOM:1px solid; POSITION:relative; BACKGROUND-COLOR:white">
					    <table>
						    <tr>
							    <td><img src="images/user16.gif"></td>
							    <td style="font-family:Verdana;">Create/Edit Tenant</td>
						    </tr>
					    </table>
					    <div style="BORDER-RIGHT:#b7b4b4 1px solid; PADDING-RIGHT:5px; BORDER-TOP:#b7b4b4 1px solid; PADDING-LEFT:5px; FONT-WEIGHT:normal; FONT-SIZE:10px; PADDING-BOTTOM:5px; BORDER-LEFT:#b7b4b4 1px solid; COLOR:#3f3f3f; PADDING-TOP:5px; BORDER-BOTTOM:#b7b4b4 1px solid; FONT-FAMILY:verdana; BACKGROUND-COLOR:#e4e4e4">Create 
						    a new tenant or edit an existing tenant.</div>
					    <br />
  <COMPONENTART:TABSTRIP id="TabStripTenant" runat="server" ImagesBaseUrl="images/" 
							    CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" FillWidth="True" Width="100%" DefaultItemLookId="DefaultTabLook"
							     EnableViewState=false DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="Multipage1">
							    <ItemLooks>
								    <ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
									    LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif"
									    HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
									    LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
								    <ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
									    LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif"
									    LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
							    </ItemLooks>
						    </COMPONENTART:TABSTRIP>
    <ComponentArt:MultiPage id="Multipage1" CssClass="subMultiPageUserPage"   runat="server">
	    <ComponentArt:PageView CssClass="PageContent" runat="server" ID="Pageview1">
	    <table cellpadding="3" cellspacing="5">
        <tr>
            <td>Tenant Short Name</td>
            <td><asp:TextBox id="ShortName"  Width="200px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Tenant Long Name</td>
            <td><asp:TextBox id="LongName"  Width="200px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
        </tr>
            <tr>
                <td>Contact</td>
                <td><asp:TextBox id="Contact"  Width="200px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Address</td>
                <td><asp:TextBox id="Address"  Width="200px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
            </tr>   
            <tr>
                <td>City</td>
                <td><asp:TextBox id="City"  Width="200px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>State</td>
                <td><asp:TextBox id="State"  Width="200px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>ZIP</td>
                <td><asp:TextBox id="Zip"  Width="200px" CssClass="InputFieldMain" runat="server"></asp:TextBox></td>
            </tr>                                            
            <tr>
            <td>Tenant Description</td>
            <td>
            <asp:TextBox style="height:120px;width:200px;" TextMode="MultiLine" id="Description" Width="100%" CssClass="InputFieldMain100P" runat="server"></asp:TextBox>
            </td>
        </tr>
            <td>Major Tenant</td>
            <td>
            <asp:CheckBox ID="Major" runat="server" />
            </td>
        </tr>                                     
        <tr>
            <td>Image(s)</td>
            <td><hr /></td>
        </tr>
       <tr>
            <td></td>
            <td>
                <div style="padding:5px;border:1px dotted #E0DCDC;">
                    <table border="0" width="100%" id="">
	<tr>
		<td width=400 align="left" valign="top">

		
	    <div id="tagallowupload" style="display:none;">
	    [ <a href="javascript:showEventAssetAdd()">add tenant asset</a> ]
	    </div>
    <div id="tagcreateversion" style="padding:10px;display:none;">

    <b>Add tenant assets</b> to this event by browsing to each file and selecting the 'add asset' button below.</font><br>

     <br>Upload Assets
            <br>
            <Upload:InputFile Class="InputFieldMain100P" id="InputFile1" runat="server"></Upload:InputFile>
		     <Upload:InputFile Class="InputFieldMain100P" id="Inputfile2" runat="server"></Upload:InputFile>
		      <Upload:InputFile Class="InputFieldMain100P" id="Inputfile3" runat="server"></Upload:InputFile>
		       <Upload:InputFile Class="InputFieldMain100P" id="Inputfile4" runat="server"></Upload:InputFile>
		        <Upload:InputFile Class="InputFieldMain100P" id="Inputfile5" runat="server"></Upload:InputFile>
		         <Upload:InputFile Class="InputFieldMain100P" id="Inputfile6" runat="server"></Upload:InputFile>
		          <Upload:InputFile Class="InputFieldMain100P" id="Inputfile7" runat="server"></Upload:InputFile>
		           <Upload:InputFile Class="InputFieldMain100P" id="Inputfile8" runat="server"></Upload:InputFile>
		            <Upload:InputFile Class="InputFieldMain100P" id="Inputfile9" runat="server"></Upload:InputFile>
		             <Upload:InputFile Class="InputFieldMain100P" id="Inputfile10" runat="server"></Upload:InputFile>
		    <br><br>
    <asp:Button id="btnUpload" Class="InputButtonMain" runat="server" Text="Add Assets"></asp:Button><input type="button" Class="InputButtonMain" onclick="document.getElementById('tagcreateversion').style.display='none';document.getElementById('tageventassets').style.display='block';" value="Cancel" id="submit1" name="submit1"></p>
    						
    </div>	
    <div id="tageventassets" style="display:block;">
	    <!--GroupBy="categoryname ASC"--><br><br><b>Tenant Assets</b><br><Br>
    <COMPONENTART:GRID 
    id="GridAssets" 

    runat="server" 
    AutoFocusSearchBox="false"
    AutoCallBackOnDelete="true"
    pagerposition="2"
    ScrollBar="Off"
    ScrollTopBottomImagesEnabled="true"
    ScrollTopBottomImageHeight="2"
    ScrollTopBottomImageWidth="16"
    ScrollImagesFolderUrl="images/scroller/"
    ScrollButtonWidth="16"
    ScrollButtonHeight="17"
    ScrollBarCssClass="ScrollBar"
    ScrollGripCssClass="ScrollGrip"
    ClientSideOnDelete="onDeleteAsset"
    ClientSideOnCallbackError="onCallbackError"
    ScrollPopupClientTemplateId="ScrollPopupTemplate" 
    Sort="porder,name asc"
    Height="10" Width="100%"
    LoadingPanelPosition="TopCenter" 
    LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
    EnableViewState="False"
    GroupBySortImageHeight="10" 
    GroupBySortImageWidth="10" 
    GroupBySortDescendingImageUrl="group_desc.gif" 
    GroupBySortAscendingImageUrl="group_asc.gif" 
    GroupingNotificationTextCssClass="GridHeaderText" 
    AlternatingRowCssClass="AlternatingRowCategory" 
    IndentCellWidth="22" 
    TreeLineImageHeight="19" 
    TreeLineImageWidth="20" 
    TreeLineImagesFolderUrl="images/lines/" 
    PagerImagesFolderUrl="images/pager/" 
    ImagesBaseUrl="images/" 
    PreExpandOnGroup="True" 
    GroupingPageSize="5" 
    PagerTextCssClass="GridFooterTextCategory" 
    PagerStyle="Numbered" 
    PageSize="5" 
    GroupByTextCssClass="GroupByText" 
    GroupByCssClass="GroupByCell" 
    FooterCssClass="GridFooter" 
    HeaderCssClass="GridHeader" 
    SearchOnKeyPress="true" 
    SearchTextCssClass="GridHeaderText" 
    AllowEditing="true" 
    AllowSorting="False"
    ShowSearchBox="false" 
    ShowHeader="false" 
    ShowFooter="true" 
    CssClass="Grid" 
    RunningMode="callback" 
    ScrollBarWidth="15" 
    AllowPaging="true" >
    <ClientTemplates>
    <ComponentArt:ClientTemplate Id="EditTemplate">
    <a href="javascript:DownloadAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
    </ComponentArt:ClientTemplate>
    <ComponentArt:ClientTemplate Id="EditCommandTemplate">
    <a href="javascript:editRow();">Update</a> 
    </ComponentArt:ClientTemplate>
    <ComponentArt:ClientTemplate Id="InsertCommandTemplate">
    <a href="javascript:insertRow();">Insert</a> 
    </ComponentArt:ClientTemplate>    
    <ComponentArt:ClientTemplate Id="TypeIconTemplate">
    <img src="## getURLPreviewIcon() ####DataItem.GetMember("media_type").Value ##" border="0"  > 
    </ComponentArt:ClientTemplate>        
    <ComponentArt:ClientTemplate Id="LookupProjectTemplate">
    <div style="border:0px solid;"><img src="images/spacer.gif" width=1 height=45><A href="## DataItem.GetMember("alink").Value ##"><img border=0 src="## getURLPreview() #### DataItem.GetMember("asset_id").Value ##"  ></a></div>
    </ComponentArt:ClientTemplate> 
    <ComponentArt:ClientTemplate Id="LookupAssetPopupTemplate">
    <A href="#" onclick="javascript:PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.ClientId ##',event);"><img border=0 src="images/15printPreview.gif"></a>
    </ComponentArt:ClientTemplate> 
    <ComponentArt:ClientTemplate Id="LookupProjectTemplatePreview">
    <A href="javascript:AssetPreview('## DataItem.ClientId ##');"><img border=0 src="images/15printPreview.gif"></a>
    </ComponentArt:ClientTemplate>           
    <ComponentArt:ClientTemplate Id="LookupCategoryTemplate">
    <A href="javascript:GotoLocation('## DataItem.GetMember("ispost").Value ##','## DataItem.GetMember("category_id").Value ##');">## DataItem.GetMember("categoryname").Value ##</a>
    </ComponentArt:ClientTemplate>    
    <ComponentArt:ClientTemplate Id="LookupCategoryIconTemplate">
    <A href="javascript:GotoLocation('## DataItem.GetMember("ispost").Value ##','## DataItem.GetMember("category_id").Value ##');"><img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif"></a>
    </ComponentArt:ClientTemplate>    
    <componentart:ClientTemplate ID="LoadingFeedbackTemplate">
    <table cellspacing="0" cellpadding="0" border="0">
    <tr>
    <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
    </tr>
    </table>
    </componentart:ClientTemplate>                                 
    </ClientTemplates>


    <Levels>
    <componentart:GridLevel EditCellCssClass="EditDataCell"
    EditFieldCssClass="EditDataField"
    EditCommandClientTemplateId="EditCommandTemplate"
    InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
    <Columns>

    <ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" Align="Center" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplate" HeadingCellCssClass="FirstHeadingCell" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="45" FixedWidth="True" />
    <ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" dataField="imagesource"  DataCellCssClass="DataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="19" FixedWidth="True" />
    <componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="false" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
    <componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Type" AllowEditing="false" Width="40" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
    <componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Uploaded" AllowEditing="false" Width="110"  FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  DataField="update_date"></componentart:GridColumn>
    <ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Width="150"  Align="Center" />
    <componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
    <componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
    <componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="category_id" SortedDataCellCssClass="SortedDataCell" DataField="category_id"></componentart:GridColumn>
    <componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="ispost" SortedDataCellCssClass="SortedDataCell" DataField="ispost"></componentart:GridColumn>
    <componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
    <componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="media_type"></componentart:GridColumn>
    <componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="timage" SortedDataCellCssClass="SortedDataCell" DataField="timage"></componentart:GridColumn>

    </Columns>
    </componentart:GridLevel>
    </Levels>
    <ServerTemplates>
    <ComponentArt:GridServerTemplate Id="PickerTemplate">
    <Template>

    <input id="txtImageUrl" type="text" value="<%# Container.DataItem("name") %>"/>


    </Template>
    </ComponentArt:GridServerTemplate>




</ServerTemplates>
</COMPONENTART:GRID>
	</div>
	
		
	<div style="padding:10px;"><asp:Literal id="literalNoAssets" Text="No tenant assets available" visible=false runat="server" /></div>


		 

		
		
		
		
		</td>
		<td align="left" valign="top"></td>
	</tr>
	<tr>
		<td colspan="2" align="left" valign="top"></td>
	</tr>
</table>
                </div>
            </td>
        </tr>
        <tr>
        <td></td>
        <td><asp:Button ID="saveTenant" CssClass="InputButtonMain" Text="Edit Tenant" runat="server"/></td>
        </tr>                    
    </table>
    
    <br /><br />
    
    
	    </ComponentArt:PageView>
	    <ComponentArt:PageView CssClass="PageContent" runat="server" ID="Pageview2">
<COMPONENTART:GRID 
id="GridTenantUsage" 
runat="server" 
visible="true"
width="100%"
 >
<ClientEvents>
  </ClientEvents>
<ClientTemplates>
<ComponentArt:ClientTemplate Id="TenantImageTemplate">
</ComponentArt:ClientTemplate>
</ClientTemplates>


<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
EditFieldCssClass="EditDataField"
 DataKeyField="projectid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asch10.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="desch10.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="10" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" Align="Center" AllowSorting="false" AllowEditing="false" DataCellClientTemplateId="TenantImageTemplate" HeadingCellCssClass="FirstHeadingCell" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" width="20" AllowGrouping="false" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Property Name" AllowEditing="false" Width="240" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Project ID" AllowEditing="false"  SortedDataCellCssClass="SortedDataCell" DataField="projectid" visible="false"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>		
	    </ComponentArt:PageView>
	    </ComponentArt:MultiPage>
		
	    </div>
	    </div>
	    </div> 
    </div>
    
    
    
    
    
    
    <script language="javascript">
TabStripTenant.SelectTabById('<%response.write (request.querystring("tab"))%>');

function showEventAssetAdd()
{
document.getElementById('tagcreateversion').style.display='block';
document.getElementById('tageventassets').style.display='none';
}


//setup form switch for delete or multi asset zip download
function DownloadAssets(ids){
	var sAssets;
	var downloadtype;
	sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
	ids = ids.replace('0 ','');
	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Download selected assets?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (arraylist == ''){
					arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
					} else {
					if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
					arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					}
					}
				}
				
				arraylist= arraylist.substring(1, arraylist.length);
				downloadtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				downloadtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	downloadtype = 'single';
	}
	if (downloadtype == 'multi') {
		if (arraylist != ''){
		window.open('<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLLocationIDAMDownloadGetMulti & "?assetids="%>' + arraylist + '<%="&instance=" & IDAMInstance%>','ConvertingImage','width=500,height=75,location=no');
		}
	}
	if (downloadtype == 'single') {
	/*assume single*/
	window.location = '<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLLocationIDAMDownload & "?dtype=assetdownload&assetid="%>' + ids + '<%="&instance=" & IDAMInstance%>&size=0';
	return true;
	}

}
	

  function onDeleteAsset(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }


  function deleteRow(ids)
  {
  
   
	/*assume single*/
	<% Response.Write(GridAssets.ClientID) %>.Delete(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(ids));


  }
  var isCreate;
  isCreate = '<%response.write(request.querystring("ID"))%>';
  if (isCreate!='')
  {
  document.getElementById('tagallowupload').style.display='block';  
  }

function refreshAssets()
{
<% Response.Write(GridAssets.ClientID) %>.Filter('name LIKE \'%' + '' + '%\'');
}
 setTimeout("refreshAssets();",200);

	</script>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    </form>
</body>
</html>