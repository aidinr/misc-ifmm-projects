﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Asset.ascx.vb" Inherits="IdamAdmin.Asset" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>
<%@Import Namespace="System.Data" %>
<div class="previewpaneMyProjects ui-accordion-content ui-helper-reset ui-widget-content ui-accordion-content-active" id="toolbarheading" style="z-index:99;border-top:0px;border-bottom:0px;">
	<!--Project cookie and top menu-->
    
		<table cellspacing="0" cellpadding="0" id="table2" width="100%" >
			<tr ><td valign="top" width="67" style="padding:10px;">
			<img style="background-color:#CBCBCB;color:#CBCBCB;border:2px solid #CBCBCB;" src="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?id=<%response.write(spProjectID)%>&instance=<%response.write(IDAMInstance)%>&type=project&crop=1&size=2&height=48&width=100&cache=1">
			</td>
			<td valign="top" style="padding-top:10px;padding-bottom:10px;">
			<font face="Verdana" size="3" ><b><%response.write(spProjectName)%>&nbsp;</b><%if trim(spProjectClientName) <> "" then 
			response.write("for") 
			end if%><b>&nbsp;<%if trim(spProjectClientName) <> "" then 
			response.write(spProjectClientName) 
			end if%></b></font><br><font face="Verdana" size="1" >
				<%if trim(spProjectDescription) <> "" then%><div style=" max-height:50px; overflow: auto;"><%Response.Write(spProjectDescription.Replace(vbCrLf, "<br>"))%><br></div><%end if%>
				<b>Security Level:</b> <%response.write(spProjectSecurityLevel)%><br>
				<asp:Literal id="LiteralCookie" Text="N/A" visible="true" runat="server" />
				</font>
			</td>
			<td id="Test" valign="top" align="right">
			<div style="padding-top:2px;">
			<!--[ <a href="#">help</a> ]-->
				</div>
			</td></tr>
			<tr><td valign="top" colspan="3">
			</td>
			</tr>
		</table>
    
</div> <!--end preview pane-->
<div id="subtabsetbottomlinefix" style="border-top:1px solid silver;z-index:2;position:relative;top:-1px;"></div>
<!--end preview pane-->
<div class="previewpaneSubResults">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td align="left" valign="middle" width="100%" style="height: 20px">
            </td>
            <td align="left" width="200" style="height: 16px" nowrap>
                
            </td>
        </tr>
    </table>
</div>

   

<div id="subtabset" style="position:relative;top:-24px;z-index:2; font-size:10px;">
<input type="radio" id="radio1" name="subtab" checked="checked" /><label for="radio1">General</label><img src="images/spacer.gif" width="2" alt="" />
</div>



<div class="previewpaneSubResults" style="z-index:999;position:relative;top:-42px;margin-left:500px;">
<table  style="WIDTH:100%;" cellpadding="0" cellspacing="0" >
	<tr>
		<td align="right" style="FONT-SIZE: 10px; FONT-FAMILY: Verdana"><div style="padding-right:10px;">[ <a href="javascript:closealltags();closeainfo();">close 
				all</a> ] [ <a href="javascript:openalltags();openainfo();">open all</a> ]</div></td>
	</tr>
</table>
</div>
<script>
    $(function () {
        $("#subtab").button();
        $("#subtabset").buttonset();
    });
	</script>
    <div class="previewpaneProjects" id="toolbar" style="position:relative;top:-19px;">
    <div id="projectcentermain" >
       

   

					
<script>
    $(function () {
        $("#JQAPSx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQAPSx', $("#JQAPSx").accordion("option", "active"));
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQAPSx') == "false") {
            $("#JQAPSx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQAPSx" style="padding-bottom:15px;margin-top:-10px;"> 
	<h3><a href="#section1">Project Summary</a></h3> 


                <div class="SnapProjectsWrapperx">
                    <div class="SnapProjectsx">
                        <asp:PlaceHolder ID="PageLoaderProjectSummary" runat="server"></asp:PlaceHolder>
                    </div>
                </div>
                <!--SnapProjectsWrapper-->
            </div>






					
<script>
    $(function () {
        $("#JQAASummaryx").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true,
            change: function (event, ui) {
                $.cookie('JQAASummaryx', $("#JQAASummaryx").accordion("option", "active"), { expires: 365 });
            }
        });
    });
    $(document).ready(function () {
        if ($.cookie('JQAASummaryx') == "false") {
            $("#JQAASummaryx").accordion("option", "active", false);
        }
    });
    
</script> 


 
<div id="JQAASummaryx" style="padding-bottom:15px;"> 
	<h3><a href="#section1">Asset Summary</a></h3> 
                <div class="SnapProjectsWrapperxx">
                    <div class="SnapProjectsxx" >
                        <ComponentArt:Menu ID="MenuDownload" runat="server" Orientation="Horizontal" CssClass="TopGroupExp" DefaultGroupCssClass="MenuGroup" DefaultItemLookId="DefaultItemLook" ClientSideOnItemSelect="actiontype" DefaultGroupItemSpacing="1" ImagesBaseUrl="images/" EnableViewState="true" ExpandDelay="100" ExpandOnClick="true">
                            <ItemLooks>
                                <ComponentArt:ItemLook HoverCssClass="TopMenuItemHoverExp" LabelPaddingTop="2px" LabelPaddingRight="10px" LabelPaddingBottom="2px" ExpandedCssClass="TopMenuItemHoverExp" LabelPaddingLeft="10px" LookId="TopItemLook" CssClass="TopMenuItemExp"></ComponentArt:ItemLook>
                                <ComponentArt:ItemLook HoverCssClass="MenuItemHoverExp" LabelPaddingTop="3px" LeftIconHeight="18px" LabelPaddingRight="10px" LabelPaddingBottom="4px" ExpandedCssClass="MenuItemHoverExp" LabelPaddingLeft="10px" LeftIconWidth="20px" LookId="DefaultItemLook" CssClass="MenuItemExp"></ComponentArt:ItemLook>
                                <ComponentArt:ItemLook LookId="BreakItem" CssClass="MenuBreak"></ComponentArt:ItemLook>
                            </ItemLooks>
                        </ComponentArt:Menu>
                        <asp:Literal ID="LiteralCheckoutStatusTop" runat="server"></asp:Literal>
                        <div id="checkindiv" style="display: none;">
                            <!--Start Rounded Box-->
                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="99%">
                                        <div class="sidemodule">
                                            <div class="modulecontainer sidebar">
                                                <div class="first">
                                                    <span class="first"></span><span class="last"></span>
                                                </div>
                                                <div>
                                                    <table border="0" width="100%">
                                                        <tr>
                                                            <td width="4">
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <!--End Top Rounded Box-->
                                                                <b><font size="1" face="verdana" color="#000000">Checkin / Checkout</font></b><br>
                                                                <br>
                                                                <asp:Literal ID="LiteralCheckoutHistory" runat="server"></asp:Literal>
                                                                <b>Status:</b>
                                                                <asp:Literal ID="LiteralCheckoutStatus" runat="server"></asp:Literal>
                                                                <asp:Literal ID="LiteralCheckoutNote" runat="server"></asp:Literal>
                                                                <br>
                                                                <br>
                                                                <b>Add a note (Optional):</b>
                                                                <asp:TextBox ID="Textbox_CheckoutDescription" CssClass="InputFieldMain100P" TextMode="MultiLine" Height="100px" Style="height: 100px;" runat="server"></asp:TextBox>
                                                                <p>
                                                                    <asp:Button ID="btnCheckout" CssClass="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" runat="server" Text="Checkout"></asp:Button>
                                                                    &nbsp;<input type="button" class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="document.getElementById('checkindiv').style.display='none';" value="Cancel">
                                                                </p>
                                                                <!--Close Rounded Box-->
                                                            </td>
                                                            <td width="4">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="last">
                                                    <span class="first"></span><span class="last"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--End Rounded Box-->
                                        <!--Start Close Rounded Box Table-->
                                    </td>
                                    <td>
                                        <img border="0" src="images/spacer.gif" height="2" width="11">
                                    </td>
                                </tr>
                            </table>
                            <!--End Close Rounded Box Table-->
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        

<%
    'INDESIGN MODULE VIEW

dim iAssetID as string
dim i as integer
iAssetID = request.querystring("ID")
dim bLinkRequireInput,bLinkRecommendations  as boolean
dim sqltemp as string
bLinkRequireInput = FALSE
'get links
    Dim rsLinks 
    Dim rsLinkRecommendations
    Dim rsLink

sqltemp = "SELECT * from ipm_asset_links where status >= 0 and active=0 and parent_asset_id = " & iAssetID
    rsLinks = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable(sqltemp)
    If rsLinks.rows.count > 0 Then
        For Each rsLink In rsLinks.rows
            If rsLink("status") = 0 Then
                bLinkRequireInput = True
            End If
        Next
    End If


    If rsLinks.rows.count > 0 Then
        %>

<div id="linkstag">








<!--Start Rounded Box-->
<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%">
<div class="sidemodule">
    <div class="modulecontainer sidebar">
        <div class="first">
            <span class="first"></span><span class="last"></span>
        </div>
        <div>
            <table border="0" width="100%">
				<tr>
					<td width="4">&nbsp;</td>
					<td><!--End Top Rounded Box-->
					</font>

<font face="Verdana" size="1" color="<%%>">

					
					
				
		
<b>Link Editor</b></font><br>
<font face="arial" size="1"><br>
</font><table border="0" width="100%" cellspacing="0">
	<tr>
		<td class="intrabackhazedarker"></td>
		<td class="intrabackhazedarker"><font face="arial" size="1" color="#808080">Filename</font></td>
		<td class="intrabackhazedarker"></td>
		<td class="intrabackhazedarker"><font face="arial" size="1" color="#808080">Location</font></td>
		<td class="intrabackhazedarker"></td>
		<td class="intrabackhazedarker"><font face="arial" size="1" color="#808080">Status</font><br><img border="0" src="images/spacer.gif" height="0" width="60"></td>
		<td class="intrabackhazedarker"></td>
		<td class="intrabackhazedarker"><font face="arial" size="1" color="#808080">Recommendations</font><br><img border="0" src="images/spacer.gif" height="0" width="93"></td>
		<td class="intrabackhazedarker"></td>
		<td class="intrabackhazedarker"><font face="arial" size="1" color="#808080">Action</font></td>
		<td class="intrabackhazedarker"></td>
	</tr>
	<%
	i = 0
	    For Each rsLink In rsLinks.rows%>
	<tr>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>><font face="arial" size="1" color="#000000"><%=i + 1%>.</font></td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>><font face="arial" size="1" color="#000000"><%=rsLink("file_name")%></font></td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>><input type=hidden name=link_id value=<%=rsLink("link_id")%>><input type=hidden name=filename_<%=rsLink("link_id")%> value="<%=rsLink("file_name")%>"></td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>><font face="arial" size="1" color="#000000"><%=Left(rsLink("network_location"), 100)%></font></td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>>&nbsp;</td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>><font face="arial" size="1" color="#000000">
		<%
		    If rsLink("status") <> 0 Then
		        Select Case rsLink("status")
		            Case 1
		                Response.Write("Added")
		            Case 2
		                Response.Write("Added")
		            Case 3
		                Response.Write("Added")
		            Case -1
		                Response.Write("Removed")
		        End Select
		    Else
		        Select Case rsLink("found_on_network")
		            Case -1, 0
		                Response.Write("<font face='arial' size='1' color='#FF0000'>Not Found</font>")
		            Case Else
		                Response.Write("<font face='arial' size='1' color='#FF0000'>Found</font>")
		        End Select
		    End If%></font></td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>>&nbsp;</td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>>
<font face="Arial" size="1" color="<%%>"><%
bLinkRecommendations = FALSE
                                             rsLinkRecommendations = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select asset_id, filename,2 priority from ipm_asset where available = 'Y' and filename = '" & rsLink("file_name").replace("'","''") & "' union all select asset_id, filename,1 priority  from ipm_asset where available = 'Y' and filename = replace('" & rsLink("file_name").replace("'","''") & "','jpg','tif')  order by priority asc, asset_id desc ")
                                             Dim iLinkRecommendationlist As string = ""
                                             Dim sOverridetext As string = ""
                                             
                                             
                                             

                                             If rsLinkRecommendations.rows.count > 0 Then
                                                                                                  
                                                 bLinkRecommendations = True
	%><select size="1" class="maindropdown" onChange="" id="recommendation_<%=rsLink("link_id")%>" name="recommendation_<%=rsLink("link_id")%>"><%
For Each rsLinkRecommendation In rsLinkRecommendations.rows
sOverridetext = ""
If rsLinkRecommendation("priority") = 1 then
sOverridetext = "(Tif Override)"
End If
If instr(iLinkRecommendationlist,rsLinkRecommendation("asset_id").tostring,CompareMethod.Text) = 0 then
Response.Write("<option value='" & rsLinkRecommendation("asset_id") & "'>" & rsLinkRecommendation("asset_id") & " :: " & rsLinkRecommendation("filename") & " " & sOverridetext & "</option>")
end if
iLinkRecommendationlist = iLinkRecommendationlist + "," + rsLinkRecommendation("asset_id").tostring
Next
	%></select><%
else
	response.write ("None<Br>")	
end if
%>
<%if bLinkRecommendations then
        Response.Write("<br><a href=""" & "javascript:gotoactiveRecommendedAsset('recommendation_" & Trim(rsLink("link_id")) & "');" & """>view</a>")
end if%></font>
</td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>>&nbsp;</td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>>
<font face="Arial" size="1" color="<%%>">
<select size="1" class="maindropdown" onChange='SetLinkAction(this);' id="linkaction_<%=rsLink("link_id")%>" name="linkaction_<%=rsLink("link_id")%>">
<option value='nothing'>Nothing</option>
<option value='remove'>Remove Link</option>
<%if bLinkRecommendations then%><option <%if bLinkRecommendations and rsLink("status") = 0  then response.write ("selected ")%> value='recommendation'>Use Recommendation</option><%end if%>
<!--<%'if rsLink("status") = 1 then%><option <%'if rsLink("status") = 1 then response.write "selected "%> value='network'>Use Network Location</option><%'end if%>-->
<option value='upload'>Upload From Network</option>
<option value='existing'>Use Existing Asset</option>
</select></font>
<div id="filetag_<%=rsLink("link_id")%>"><br>
<font face="Arial" size="1" color="<%%>">Upload File<br>
<input type=file  size="23" name="thefile_<%=rsLink("link_id")%>" id="thefile_<%=rsLink("link_id")%>"></font><br><br>
</div>
<div id="assetid_<%=rsLink("link_id")%>"><br>
<font face="Arial" size="1" color="<%%>">Use Asset ID<br>
<input type="text" size="23" name="useassetid_<%=rsLink("link_id")%>"></font><br><br>
</div>
<script>
document.getElementById("filetag_<%=rsLink("link_id")%>").style.display = "none";
document.getElementById("assetid_<%=rsLink("link_id")%>").style.display = "none";
</script>


</td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>>&nbsp;</td>
	</tr>
	<%
	i = i + 1
	Next%>

</table>
<p><font face="arial" size="1"><br>

<input type="button" onclick="javascript:savelinksintercept('IDAM.aspx?NeatUpload_PostBackID=<%response.write (spPostBackID)%>&page=Asset&Id=<%response.write (request.querystring("ID"))%>&type=asset&c=<%response.write (request.querystring("c"))%>');" value="Save" id="submitlink" name="submitlink">&nbsp;<input type="button"  onclick="javascript:document.getElementById('linkstag').style.display = 'none';" value="Cancel" id="" name="">



<!--Close Rounded Box-->
					</td>
					<td width="4">&nbsp;</td>
				</tr>
			</table>
			</div>
        <div class="last">
        	<span class="first" style="position: absolute; top: 0px; width: 6px; height: 18px; margin-left: -6px"></span><span class="last"></span>
        </div>
    </div>
</div>
<!--End Rounded Box-->
<!--Start Close Rounded Box Table-->
								</td>
								<td><img border="0" src="images/spacer.gif" height="2" width="11"></td>
							</tr>
</table>
<!--End Close Rounded Box Table-->
</div>
<br>

<SCRIPT LANGUAGE=javascript>
<!--

function SetLinkAction(dropdown){

	var myindex  = dropdown.selectedIndex
	var SelValue = dropdown.options[myindex].value
	var dropdownname = dropdown.id;
	var linkid = (dropdownname.substring(11,50));
	var filetagname = 'filetag_' + linkid
	var elm = document.getElementById(filetagname);
	var filetagname = 'assetid_' + linkid
	var elm_existing = document.getElementById(filetagname);	
	if (SelValue == 'upload'){
		elm.style.display = "block";
	} else {
		elm.style.display = "none";
	}
	if (SelValue == 'existing'){
		elm_existing.style.display = "block";
	} else {
		elm_existing.style.display = "none";
	}	
}


function gotoactiveRecommendedAsset(recommendationid)
{
	var elm = document.getElementById(recommendationid);
	var elmvalue = elm.options[elm.selectedIndex].value;
	var linkURL = '<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?id=' + elmvalue + '&instance=<%response.write(IDAMInstance)%>&type=asset&size=1&width=640&height=480'
	winpops=window.open(linkURL,"","width=" + 660 + ",height=" + 500 + ",location=no,")
	//return true;
}
//-->
</script>

<script>
<%if bLinkRequireInput then%>
document.getElementById("linkstag").style.display = "block";
<%else%>
document.getElementById("linkstag").style.display = "none";
<%end if%>
</script>



<%end if%>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div id="tagcreateversion" style="padding: 10px; display: none;">
                            <b>Create a version</b> of this asset by selecting a file using the 'Browse' button and choosing the appropriate version action.</font><br>
                            <upload:progressbar id="progressBar" runat="server" triggers="btnUpload linkButton commandButton htmlInputButtonButton htmlInputButtonSubmit"></upload:progressbar>
                            <br>
                            Upload Version File
                            <br>
                            <upload:inputfile class="InputFieldMain" id="InputFile1" name="InputFile1" runat="server"></upload:inputfile>
                             
                            <br>
                            Name (Optional)
                            <br>
                            <input name="versionname" class="InputFieldMain" size="36"><br>
                            Description (Optional)
                            <br>
                            <textarea class="InputFieldMain" name="versiondescription" rows="4" style="width: 97%"></textarea></font>
                            <br>
                            <br>
                            Version Action<br>
                            <input class="InputFieldMainCheckbox" type="radio" value="TRUE" name="txtIsOverwrite">
                            Overwrite Existing File<br>
                            <input class="InputFieldMainCheckbox" type="radio" name="txtIsOverwrite" value="FALSE" checked>
                            Create New Version<br>
                            <br>
                            <asp:Button ID="btnUpload" Class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all"  OnClientClick="return checkversionextension();" runat="server" Text="Create Version"></asp:Button>&nbsp;<input type="button" class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all" onclick="document.getElementById('tagcreateversion').style.display='none';" value="Cancel" id="submit1" name="submit1"></p>
                        </div>
                        <asp:PlaceHolder ID="PreviewTemplateLoader" runat="server"></asp:PlaceHolder>
                        <asp:Literal ID="LiteralStatus" Text="" Visible="true" runat="server" />
                        <img src="images/spacer.gif" width="188" height="1">
                    </div>
                    
                </div>
                <!--SnapProjectsWrapper-->
                
            </div>
        <asp:PlaceHolder ID="PageLoaderAsset" runat="server"></asp:PlaceHolder>
    </div>
    <!--end main div-->
</div>

<script language="javascript" type="text/javascript">

 
  

function savelinksintercept(sNewFormAction)
{
    document.forms[0].action = sNewFormAction;
    document.forms[0].__VIEWSTATE.name = 'NOVIEWSTATE';
    document.forms[0].submit();
}

function showversions(ID)
{
	window.open('AssetVersion.aspx?ID=' + ID,'Versions','width=880,height=800,location=no,scrollbars=yes');
}	

function closeainfo()
{
try{
    $("#JQAPSx").accordion("option", "active", false);
    $("#JQAASummaryx").accordion("option", "active", false);
}catch(err){}
try{

}catch(err){}
}
function openainfo()
{
try{
    $("#JQAPSx").accordion("option", "active", 0);
    $("#JQAASummaryx").accordion("option", "active", 0);
}catch(err){}
try{

}catch(err){}
}   



function checkversionextension()
{
var assetfilename = '<%=spAssetFilename%>';

if ((document.getElementsByName('txtIsOverwrite')[0].checked) &&(assetfilename.toLowerCase().replace('.','')!=getext(document.getElementById('InputFile1').value)))
{
alert('You cannot upload a file version using the overwrite option when the new file is of a different extension.  Please create a new version of the file when using a new file extension.');
return false;
}else{
return true;
}
}


function getext(filename) 
    { 
    return typeof filename != "undefined" ? filename.substring(filename.lastIndexOf(".")+1, filename.length).toLowerCase() : false; 
    } 


</script>


<div id="AssetDialog" title="Dialog Title" >
<ComponentArt:CallBack ID="CallbackAssetDialog" runat="server" CacheContent="false">
<ClientEvents>
</ClientEvents>
<Content>
<asp:PlaceHolder runat="server" ID="AssetDialogHeading" >
<table width="100%"><tr><td><b>Firstname</b></td><td><b>Lastname</b></td><td><b>Event Time</b></td><td><b>Event</b></td><td><b>Details</b></td></tr>
<asp:Literal ID="LiteralAssetDialog" Text="" runat="server"></asp:Literal><a ti
</table>
</asp:PlaceHolder>  
</Content>
                                           
</ComponentArt:CallBack>
</div>

<script language="javascript" type="text/javascript">
    $('#AssetDialog').dialog({ autoOpen: false,
        open: function () { closedialog = 0; },
        focus: function () { closedialog = 0; },
        close: function () { $(document).unbind('click'); }
    });


    function Object_PopUp_AssetActivity(id) {
       
            $('#AssetDialog').dialog("option", "title", 'Activity');
            $('#AssetDialog').dialog("option", "modal", false);
            $('#AssetDialog').dialog("option", "height", 500);
            $('#AssetDialog').dialog("option", "width", 550);
            $('#AssetDialog').dialog("option", "zIndex", 9999);
            $('#AssetDialog').dialog('open');
            CallbackAssetDialog.Callback(id);
       
    }


</script>
