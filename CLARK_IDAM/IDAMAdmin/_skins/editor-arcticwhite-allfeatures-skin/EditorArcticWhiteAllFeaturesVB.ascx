<%@ Control Language="VB" Src="EditorArcticWhiteAllFeaturesVB.ascx.vb" Inherits="EditorArcticWhiteAllFeatures" AutoEventWireup="false" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Literal ID="SkinPathClientOutput" runat="server" />

<link rel="stylesheet" type="text/css" href="<%= Me.SkinPath %>/css/style.css" />
<script type="text/javascript" src="<%= Me.SkinPath %>/js/scripts.js"></script>
<script type="text/javascript">
function debug() { }

</script>

<ComponentArt:Editor
  ID="editor1"
  RunAt="server"
  SourceCssClass="SourceCssClass"
  DesignCssClass="DesignCssClass"
  HighlightElementCssClass="HighlightCssClass"
  CssFileURL="css/iframe.css"
  ContentHTML="<h1>ComponentArt Editor for ASP.NET AJAX</h1><h2>The New Lightweight, Feature-packed and Easy to Configure Web Editing Control </h2><div>We are excited to announce the imminent release of ComponentArt Editor for ASP.NET AJAX. The new control overcomes browser inconsistencies commonly found in web-based ASP.NET editing controls as well as major online document editing services like Google Docs. Major features include:</div><ul><li>Browser-consistent, XHTML-compliant output</li><li>Smooth, responsive and highly customizable UI</li><li>Integrated AJAX SpellChecker</li><li>Powerful client-side API</li><li>And much more...</li></ul><div>For more information check <a title='http://www.componentart.com/' href='http://www.componentart.com/'>ComponentArt.com</a> or subscribe to the ComponentArt <a title='http://www.componentart.com/publish/news.xml' href='http://www.componentart.com/publish/news.xml'>news RSS feed</a>.</div>"
  ModeTabsCssClass="tabs"
  ModeTabCssClass="tab"
  BreadcrumbClientTemplateId="breadcrumbTemplate"
  BreadcrumbCssClass="f-l"
  BreadcrumbSeparator="<div class='sep'></div>"
  Width="730"
  Height="400"
>

  <ClientTemplates>
    <ComponentArt:ClientTemplate Id="breadcrumbTemplate"><a onclick="this.blur();return false;" href="javascript:void(0);" class="btn"><span>&lt;##DataItem.breadcrumb##&gt;</span></a></ComponentArt:ClientTemplate>
  </ClientTemplates>

  <Styles>
    <ComponentArt:EditorStyle Name="Normal" StyleString="font-weight:400;font-style:normal;font-size:12px;font-family:Arial;" />
    <ComponentArt:EditorStyle Name="Heading 1" Element="H1" ReplaceParagraphTag="true" DropDownStyleString="font-family:'Arial Black',Arial;font-size:20px;color:#333;font-weight:700;" />
    <ComponentArt:EditorStyle Name="Heading 2" Element="H2" ReplaceParagraphTag="true" DropDownStyleString="font-family:Arial;font-size:17px;color:#3c7e9d;font-weight:700;" />
    <ComponentArt:EditorStyle Name="Heading 3" Element="H3" ReplaceParagraphTag="true" DropDownStyleString="font-family:Arial;font-size:12px;color:#999;" />
    <ComponentArt:EditorStyle Name="Bold" StyleString="font-weight:700;" DropDownStyleString="font-weight:700;" />
    <ComponentArt:EditorStyle Name="Italic" StyleString="font-style:italic;" DropDownStyleString="font-style:italic;" />
    <ComponentArt:EditorStyle Name="Bold + Italic" StyleString="font-weight:700;font-style:italic;" DropDownStyleString="font-weight:700;font-style:italic;" />
  </Styles>

  <Template>
<%--
  Editor wrapper template (CSS class = .wrapper): This specifically-sized DIV contains the ToolBar, Editor Area and Status Bar DIVs as follows:

  *********************************
  * ToolBar (.tb)
    - Left (.tb-l): Left edge
    - Middle (.tb-m): The ToolBar goes here
    - Right (.tb-r): Right edge

  * Editor (.e)
    - Top (.et)
      > Left (.et-l): Top-left corner
      > Middle (.et-m): Top-middle horizontally-tiling edge
        - Toolbar containers (.tb-c): These containe the individual ToolBars; they're assigned an inline width for pixel-perfection
      > Right (.et-r): Top-right corner
    - Middle (.em)
      > Left (.em-l): Left vertically-tiling edge
      > Middle (.em-m): The Editor Area goes here
      > Right (.em-r): Right vertically-tiling edge
    - Bottom (.eb)
      > Left (.eb-l): Bottom-left corner
      > Middle (.eb-m): Bottom-middle horizontally-tiling edge
      > Right (.eb-r): Bottom-right corner

  * Status Bar (.sb)
    - Left (.sb-l): Left edge
    - Middle (.sb-m): The mode-switching buttons [design / source view], breadcrumbs and wordcount area go here
    - Right (.sb-r): Right edge
  *********************************
--%>
<div class="wrapper">
  <div class="tb">
    <div class="tb-l"></div>
    <div class="tb-m">
      <div class="tb-c" style="width:auto;">$$FormattingToolBar$$</div>
      <div class="tb-c" style="width:auto;">$$StandardToolBar$$</div>
      <div class="tb-c" style="width:auto;">$$ElementsToolBar$$</div>
    </div>
    <div class="tb-r"></div>
  </div>

  <div class="e">
    <div class="et">
      <div class="et-l"><span></span></div>
      <div class="et-m"><span></span></div>
      <div class="et-r"><span></span></div>
    </div>

    <div class="em">
      <div class="em-l"></div>
      <div class="em-m">$$editorarea$$</div>
      <div class="em-r"></div>
    </div>

    <div class="eb">
      <div class="eb-l"><span></span></div>
      <div class="eb-m"><span></span></div>
      <div class="eb-r"><span></span></div>
    </div>
  </div>

  <div class="sb">
    <div class="sb-l"></div>
    <div class="sb-m">
      <div class="mode">
        <a onclick="toggle_radio(this,'btn-sel','btn');editor1.DesignMode();this.blur();return false;" href="javascript:void(0);" class="btn-sel" title="Design Mode"><span><img src="<%= Me.SkinPath %>/images/_blank.png" width="16" height="16" alt="Design Mode" class="icon design" />Design</span></a>
        <a onclick="toggle_radio(this,'btn-sel','btn');editor1.SourceMode();this.blur();return false;" href="javascript:void(0);" class="btn" title="HTML Mode"><span><img src="<%= Me.SkinPath %>/images/_blank.png" width="16" height="16" alt="HTML Mode" class="icon html" />HTML</span></a>
      </div>

      <div class="path">
        <div class="path-l">Path:</div>
        <div class="path-m">$$breadcrumbs$$</div>
        <div class="path-r"></div>
      </div>

            <div class="count">
        <div class="words" title="Word Count">
          <div class="lbl">Words:</div>
          <div class="val">$$wordcount$$</div>
        </div>
        <div class="chars" title="Character Count">
          <div class="lbl">Chars:</div>
          <div class="val">$$charactercount$$</div>
        </div>
            </div>
    </div>
    <div class="sb-r"></div>
  </div>
</div>
<%-- /Wrapper --%>
  </Template>



  <ToolBars>
    <ComponentArt:ToolBar
      ID="FormattingToolBar"
      RunAt="server"
      ImagesBaseUrl="images"
      CssClass="toolbar"
      Width="100"
      Height="25"
      DefaultItemImageWidth="25"
      DefaultItemImageHeight="25"
      UseFadeEffect="false"
    >

      <ClientEvents>
        <ItemMouseUp EventHandler="formatting_toolbar_mouseup" />
      </ClientEvents>

      <Items>
        <ComponentArt:ToolBarItem
          EditorCommandType="FormatStyle"
          ChildControlId="FontStyles"
          CustomContentId="FontStylesContent"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatFontFace"
          ChildControlId="FontFamily"
          CustomContentId="FontFamilyContent"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatFontSize"
          ChildControlId="FontSize"
          CustomContentId="FontSizeContent"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatFontColor"
          ItemType="SplitDropDown"
          ToolTip="Font Color"
          ImageUrl="_blank.png"
          ImageWidth="24"
          CssClass="item split font-color"
          HoverCssClass="item split-h font-color font-color-h"
          ActiveCssClass="item split-a font-color font-color-a"
          CheckedCssClass="item split-c font-color font-color-c"
          CheckedHoverCssClass="item split-c-h font-color font-color-c-h"
          CheckedActiveCssClass="item split-c-a font-color font-color-c-a"
          DisabledCssClass="item split-d font-color font-color-d"
          DisabledCheckedCssClass="item split-d-c font-color font-color-d-c"
          ExpandedCssClass="item split-a font-color font-color-a"
          DropDownImageWidth="15"
          DropDownImageHeight="25"
          DropDownImageUrl="_blank.png"
          DropDownId="FontColorMenu"
          Id="FontColorItem"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatHighlight"
          ItemType="SplitDropDown"
          ToolTip="Background Color"
          ImageUrl="_blank.png"
          ImageWidth="24"
          CssClass="item split bg-color"
          HoverCssClass="item split-h bg-color bg-color-h"
          ActiveCssClass="item split-a bg-color bg-color-a"
          CheckedCssClass="item split-c bg-color bg-color-c"
          CheckedHoverCssClass="item split-c-h bg-color bg-color-c-h"
          CheckedActiveCssClass="item split-c-a bg-color bg-color-c-a"
          DisabledCssClass="item split-d bg-color bg-color-d"
          DisabledCheckedCssClass="item split-d-c bg-color bg-color-d-c"
          ExpandedCssClass="item split-a bg-color bg-color-a"
          DropDownImageWidth="15"
          DropDownImageHeight="25"
          DropDownImageUrl="_blank.png"
          DropDownId="FontColorMenu"
          Id="BackgroundColorItem"
        />

        <ComponentArt:ToolBarItem ItemType="Separator" ImageWidth="10" ImageUrl="_blank.png" CssClass="sep" />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatBold"
          ItemType="ToggleCheck"
          ToolTip="Bold"
          ImageUrl="_blank.png"
          CssClass="item bold"
          HoverCssClass="item bold-h"
          ActiveCssClass="item bold-a"
          CheckedCssClass="item bold-c"
          CheckedHoverCssClass="item bold-c-h"
          CheckedActiveCssClass="item bold-c-a"
          DisabledCssClass="item bold-d"
          DisabledCheckedCssClass="item bold-d-c"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatItalic"
          ItemType="ToggleCheck"
          ToolTip="Italic"
          ImageUrl="_blank.png"
          CssClass="item italic"
          HoverCssClass="item italic-h"
          ActiveCssClass="item italic-a"
          CheckedCssClass="item italic-c"
          CheckedHoverCssClass="item italic-c-h"
          CheckedActiveCssClass="item italic-c-a"
          DisabledCssClass="item italic-d"
          DisabledCheckedCssClass="item italic-d-c"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatUnderline"
          ItemType="ToggleCheck"
          ToolTip="Underline"
          ImageUrl="_blank.png"
          CssClass="item underline"
          HoverCssClass="item underline-h"
          ActiveCssClass="item underline-a"
          CheckedCssClass="item underline-c"
          CheckedHoverCssClass="item underline-c-h"
          CheckedActiveCssClass="item underline-c-a"
          DisabledCssClass="item underline-d"
          DisabledCheckedCssClass="item underline-d-c"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatStrikethrough"
          ItemType="ToggleCheck"
          ToolTip="Strikethrough"
          ImageUrl="_blank.png"
          CssClass="item strikethrough"
          HoverCssClass="item strikethrough-h"
          ActiveCssClass="item strikethrough-a"
          CheckedCssClass="item strikethrough-c"
          CheckedHoverCssClass="item strikethrough-c-h"
          CheckedActiveCssClass="item strikethrough-c-a"
          DisabledCssClass="item strikethrough-d"
          DisabledCheckedCssClass="item strikethrough-d-c"
        />

        <ComponentArt:ToolBarItem ItemType="Separator" ImageWidth="10" ImageUrl="_blank.png" CssClass="sep" />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatAlignLeft"
          ItemType="ToggleRadio"
          ToggleGroupId="TextAlign"
          ToolTip="Align Left"
          ImageUrl="_blank.png"
          CssClass="item align-left"
          HoverCssClass="item align-left-h"
          ActiveCssClass="item align-left-a"
          CheckedCssClass="item align-left-c"
          CheckedHoverCssClass="item align-left-c-h"
          CheckedActiveCssClass="item align-left-c-a"
          DisabledCssClass="item align-left-d"
          DisabledCheckedCssClass="item align-left-d-c"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatAlignCenter"
          ItemType="ToggleRadio"
          ToggleGroupId="TextAlign"
          ToolTip="Center"
          ImageUrl="_blank.png"
          CssClass="item align-center"
          HoverCssClass="item align-center-h"
          ActiveCssClass="item align-center-a"
          CheckedCssClass="item align-center-c"
          CheckedHoverCssClass="item align-center-c-h"
          CheckedActiveCssClass="item align-center-c-a"
          DisabledCssClass="item align-center-d"
          DisabledCheckedCssClass="item align-center-d-c"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatAlignRight"
          ItemType="ToggleRadio"
          ToggleGroupId="TextAlign"
          ToolTip="Align Right"
          ImageUrl="_blank.png"
          CssClass="item align-right"
          HoverCssClass="item align-right-h"
          ActiveCssClass="item align-right-a"
          CheckedCssClass="item align-right-c"
          CheckedHoverCssClass="item align-right-c-h"
          CheckedActiveCssClass="item align-right-c-a"
          DisabledCssClass="item align-right-d"
          DisabledCheckedCssClass="item align-right-d-c"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatAlignJustify"
          ItemType="ToggleRadio"
          ToggleGroupId="TextAlign"
          ToolTip="Justify"
          ImageUrl="_blank.png"
          CssClass="item align-justify"
          HoverCssClass="item align-justify-h"
          ActiveCssClass="item align-justify-a"
          CheckedCssClass="item align-justify-c"
          CheckedHoverCssClass="item align-justify-c-h"
          CheckedActiveCssClass="item align-justify-c-a"
          DisabledCssClass="item align-justify-d"
          DisabledCheckedCssClass="item align-justify-d-c"
        />

        <ComponentArt:ToolBarItem ItemType="Separator" ImageWidth="10" ImageUrl="_blank.png" CssClass="sep" />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatBullets"
          ItemType="SplitDropDown"
          ToolTip="Bullets"
          ImageUrl="_blank.png"
          ImageWidth="24"
          CssClass="item split bullets"
          HoverCssClass="item split-h bullets-h"
          ActiveCssClass="item split-a bullets-a"
          CheckedCssClass="item split-c bullets-c"
          CheckedHoverCssClass="item split-c-h bullets-c-h"
          CheckedActiveCssClass="item split-c-a bullets-c-a"
          DisabledCssClass="item split-d bullets-d"
          DisabledCheckedCssClass="item split-d-c bullets-d-c"
          ExpandedCssClass="item split-a bullets-a"
          DropDownImageWidth="15"
          DropDownImageHeight="25"
          DropDownImageUrl="_blank.png"
          DropDownId="BulletsMenu"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatNumbering"
          ItemType="SplitDropDown"
          ToolTip="Numbering"
          ImageUrl="_blank.png"
          ImageWidth="24"
          CssClass="item split numbering"
          HoverCssClass="item split-h numbering-h"
          ActiveCssClass="item split-a numbering-a"
          CheckedCssClass="item split-c numbering-c"
          CheckedHoverCssClass="item split-c-h numbering-c-h"
          CheckedActiveCssClass="item split-c-a numbering-c-a"
          DisabledCssClass="item split-d numbering-d"
          DisabledCheckedCssClass="item split-d-c numbering-d-c"
          ExpandedCssClass="item split-a numbering-a"
          DropDownImageWidth="15"
          DropDownImageHeight="25"
          DropDownImageUrl="_blank.png"
          DropDownId="NumberingMenu"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatIncreaseIndent"
          ToolTip="Increase Indent"
          ImageUrl="_blank.png"
          CssClass="item increase-indent"
          HoverCssClass="item increase-indent-h"
          ActiveCssClass="item increase-indent-a"
          DisabledCssClass="item increase-indent-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="FormatDecreaseIndent"
          ToolTip="Decrease Indent"
          ImageUrl="_blank.png"
          CssClass="item decrease-indent"
          HoverCssClass="item decrease-indent-h"
          ActiveCssClass="item decrease-indent-a"
          DisabledCssClass="item decrease-indent-d"
        />

        <ComponentArt:ToolBarItem ItemType="Separator" ImageWidth="10" ImageUrl="_blank.png" CssClass="sep" />

        <ComponentArt:ToolBarItem
          EditorCommandType="InsertHorizontalRule"
          ToolTip="Insert horizontal line"
          ImageUrl="_blank.png"
          CssClass="item insert-hr"
          HoverCssClass="item insert-hr-h"
          ActiveCssClass="item insert-hr-a"
          DisabledCssClass="item insert-hr-d"
        />
      </Items>

      <Content>
        <ComponentArt:ToolBarItemContent ID="FontStylesContent">
          <ComponentArt:ComboBox
            ID="FontStyles"
            RunAt="server"
            AutoFilter="false"
            Width="83"
            Height="24"
            ItemCssClass="style-item"
            ItemHoverCssClass="style-item-hover"
            CssClass="tb-cmb"
            TextBoxCssClass="tb-txt"
            DropImageUrl="images/tb-ddn.png"
            DropHoverImageUrl="images/tb-ddn-hover.png"
            DropDownResizingMode="bottom"
            DropDownWidth="190"
            DropDownHeight="160"
            DropDownCssClass="ddn"
            DropDownContentCssClass="ddn-con"
            SelectedIndex="6"
          >
            <DropDownFooter><div class="ddn-ftr"><span></span></div></DropDownFooter>

            <Items>
              <ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Normal" Value="Normal" />
              <ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Heading 1" Value="Heading 1" />
              <ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Heading 2" Value="Heading 2" />
              <ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Heading 3" Value="Heading 3" />
              <ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Bold" Value="Bold" />
              <ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Italic" Value="Italic" />
              <ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Bold + Italic" Value="Bold + Italic" />
            </Items>

            <ClientTemplates>
              <ComponentArt:ClientTemplate ID="StyleItemTemplate">## style_html(Parent.ParentEditor,DataItem) ##</ComponentArt:ClientTemplate>
            </ClientTemplates>

          </ComponentArt:ComboBox>
        </ComponentArt:ToolBarItemContent>

        <ComponentArt:ToolBarItemContent ID="FontFamilyContent">
          <ComponentArt:ComboBox
            ID="FontFamily"
            RunAt="server"
                    Width="120"
                      Height="24"
            ItemCssClass="ddn-item"
            ItemHoverCssClass="ddn-item-hover"
            CssClass="tb-cmb"
            TextBoxCssClass="tb-txt"
            DropImageUrl="images/tb-ddn.png"
            DropHoverImageUrl="images/tb-ddn-hover.png"
            DropDownResizingMode="bottom"
            DropDownWidth="190"
            DropDownHeight="160"
            DropDownCssClass="ddn"
            DropDownContentCssClass="ddn-con"
            SelectedIndex="6"
            ItemClientTemplateId="FontFamilyItem"
          >
            <DropDownFooter><div class="ddn-ftr"><span></span></div></DropDownFooter>

            <Items>
              <ComponentArt:ComboBoxItem Text="Arial" Value="arial" />
              <ComponentArt:ComboBoxItem Text="Arial Black" Value="'arial black'" />
              <ComponentArt:ComboBoxItem Text="Comic Sans MS" Value="'comic sans ms'" />
              <ComponentArt:ComboBoxItem Text="Courier New" Value="courier new" />
              <ComponentArt:ComboBoxItem Text="Garamond" Value="garamond" />
              <ComponentArt:ComboBoxItem Text="Georgia" Value="georgia" />
              <ComponentArt:ComboBoxItem Text="Tahoma" Value="tahoma" />
              <ComponentArt:ComboBoxItem Text="Times New Roman" Value="'times new roman'" />
              <ComponentArt:ComboBoxItem Text="Trebuchet MS" Value="'trebuchet ms'" />
              <ComponentArt:ComboBoxItem Text="Verdana" Value="verdana" />
            </Items>

            <ClientTemplates>
              <ComponentArt:ClientTemplate ID="FontFamilyItem">
                <div class="font"><img src="## eval(Parent.ParentEditor.ClientControlId+'_SkinPath') ##/images/_blank.png" width="16" height="16" /><span style="font-family:## DataItem.get_value() ##;font-size:11px;vertical-align:middle;line-height:22px;font-weight:regular;">## DataItem.get_text() ##</span></div>
              </ComponentArt:ClientTemplate>
            </ClientTemplates>
          </ComponentArt:ComboBox>
        </ComponentArt:ToolBarItemContent>

        <ComponentArt:ToolBarItemContent ID="FontSizeContent">
          <ComponentArt:ComboBox
            ID="FontSize"
            RunAt="server"
            Width="46"
            Height="24"
            ItemCssClass="size-item"
            ItemHoverCssClass="size-item-hover"
            CssClass="tb-cmb"
            TextBoxCssClass="tb-txt"
            DropImageUrl="images/tb-ddn.png"
            DropHoverImageUrl="images/tb-ddn-hover.png"
            DropDownResizingMode="bottom"
            DropDownWidth="64"
            DropDownHeight="160"
            DropDownCssClass="ddn"
            DropDownContentCssClass="size-con"
            SelectedIndex="6"
          >
            <DropDownFooter><div class="ddn-ftr"><span></span></div></DropDownFooter>
            <Items>
              <ComponentArt:ComboBoxItem Text="8" Value="8px" />
              <ComponentArt:ComboBoxItem Text="9" Value="9px" />
              <ComponentArt:ComboBoxItem Text="10" Value="10px" />
              <ComponentArt:ComboBoxItem Text="11" Value="11px" />
              <ComponentArt:ComboBoxItem Text="12" Value="12px" />
              <ComponentArt:ComboBoxItem Text="14" Value="14px" />
              <ComponentArt:ComboBoxItem Text="16" Value="16px" />
              <ComponentArt:ComboBoxItem Text="18" Value="18px" />
              <ComponentArt:ComboBoxItem Text="20" Value="20px" />
              <ComponentArt:ComboBoxItem Text="22" Value="22px" />
              <ComponentArt:ComboBoxItem Text="24" Value="24px" />
              <ComponentArt:ComboBoxItem Text="26" Value="26px" />
              <ComponentArt:ComboBoxItem Text="28" Value="28px" />
              <ComponentArt:ComboBoxItem Text="36" Value="36px" />
              <ComponentArt:ComboBoxItem Text="48" Value="48px" />
              <ComponentArt:ComboBoxItem Text="72" Value="72px" />
            </Items>
          </ComponentArt:ComboBox>
        </ComponentArt:ToolBarItemContent>
      </Content>

    </ComponentArt:ToolBar>

    <ComponentArt:ToolBar
      ID="StandardToolBar"
      RunAt="server"
      ImagesBaseUrl="images"
      CssClass="toolbar"
      Width="100"
      Height="25"
      DefaultItemImageWidth="25"
      DefaultItemImageHeight="25"
      UseFadeEffect="false"
    >

      <Items>
        <ComponentArt:ToolBarItem
          EditorCommandType="New"
          ToolTip="New document"
          ImageUrl="_blank.png"
          CssClass="item new"
          HoverCssClass="item new-h"
          ActiveCssClass="item new-a"
          DisabledCssClass="item new-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="Save"
          ToolTip="FileSave"
          ImageUrl="_blank.png"
          CssClass="item save"
          HoverCssClass="item save-h"
          ActiveCssClass="item save-a"
          DisabledCssClass="item save-d"
        />

        <ComponentArt:ToolBarItem ItemType="Separator" ImageWidth="10" ImageUrl="_blank.png" CssClass="sep" />

        <ComponentArt:ToolBarItem
          EditorCommandType="EditCut"
          ToolTip="Cut"
          ImageUrl="_blank.png"
          CssClass="item cut"
          HoverCssClass="item cut-h"
          ActiveCssClass="item cut-a"
          DisabledCssClass="item cut-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="EditCopy"
          ToolTip="Copy"
          ImageUrl="_blank.png"
          CssClass="item copy"
          HoverCssClass="item copy-h"
          ActiveCssClass="item copy-a"
          DisabledCssClass="item copy-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="EditPaste"
          ToolTip="Paste"
          ImageUrl="_blank.png"
          CssClass="item paste"
          HoverCssClass="item paste-h"
          ActiveCssClass="item paste-a"
          DisabledCssClass="item paste-d"
        />

        <ComponentArt:ToolBarItem
          ClientSideCommand="editor1.PasteHTML(true);"
          ToolTip="Paste from Word"
          ImageUrl="_blank.png"
          CssClass="item word-paste"
          HoverCssClass="item word-paste-h"
          ActiveCssClass="item word-paste-a"
          DisabledCssClass="item word-paste-d"
        />

        <ComponentArt:ToolBarItem ItemType="Separator" ImageWidth="10" ImageUrl="_blank.png" CssClass="sep" />

        <ComponentArt:ToolBarItem
          EditorCommandType="Print"
          ToolTip="Print"
          ImageUrl="_blank.png"
          CssClass="item print"
          HoverCssClass="item print-h"
          ActiveCssClass="item print-a"
          DisabledCssClass="item print-d"
        />

        <ComponentArt:ToolBarItem ItemType="Separator" ImageWidth="10" ImageUrl="_blank.png" CssClass="sep" />

        <ComponentArt:ToolBarItem
          ClientSideCommand="FindDialog.Show();"
          ToolTip="Find &amp; Replace"
          ImageUrl="_blank.png"
          CssClass="item find-replace"
          HoverCssClass="item find-replace-h"
          ActiveCssClass="item find-replace-a"
          DisabledCssClass="item find-replace-d"
          ChildControlId="FindDialog"
        />

        <ComponentArt:ToolBarItem
          ClientSideCommand="Spell1.Check(null,spellcheck_handler);"
          ToolTip="Check spelling"
          ImageUrl="_blank.png"
          CssClass="item spellcheck"
          HoverCssClass="item spellcheck-h"
          ActiveCssClass="item spellcheck-a"
          DisabledCssClass="item spellcheck-d"
          ChildControlId="SpellCheckDialog"
        />


        <ComponentArt:ToolBarItem ItemType="Separator" ImageWidth="10" ImageUrl="_blank.png" CssClass="sep" />

        <ComponentArt:ToolBarItem
          EditorCommandType="EditUndoExtended"
          ItemType="SplitDropDown"
          DropDownId="UndoMenu"
          ChildControlId="UndoMenu"
          ToolTip="Undo"
          ImageUrl="_blank.png"
          ImageWidth="24"
          CssClass="item split undo"
          HoverCssClass="item split-h undo-h"
          ActiveCssClass="item split-a undo-a"
          CheckedCssClass="item split-c undo-c"
          CheckedHoverCssClass="item split-c-h undo-c-h"
          CheckedActiveCssClass="item split-c-a undo-c-a"
          DisabledCssClass="item split-d undo-d"
          DisabledCheckedCssClass="item split-d-c undo-d-c"
          ExpandedCssClass="item split-a undo-a"
          DropDownImageWidth="15"
          DropDownImageHeight="25"
          DropDownImageUrl="_blank.png"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="EditRedoExtended"
          ItemType="SplitDropDown"
          DropDownId="RedoMenu"
          ChildControlId="RedoMenu"
          ToolTip="redo"
          ImageUrl="_blank.png"
          ImageWidth="24"
          CssClass="item split redo"
          HoverCssClass="item split-h redo-h"
          ActiveCssClass="item split-a redo-a"
          CheckedCssClass="item split-c redo-c"
          CheckedHoverCssClass="item split-c-h redo-c-h"
          CheckedActiveCssClass="item split-c-a redo-c-a"
          DisabledCssClass="item split-d redo-d"
          DisabledCheckedCssClass="item split-d-c redo-d-c"
          ExpandedCssClass="item split-a redo-a"
          DropDownImageWidth="15"
          DropDownImageHeight="25"
          DropDownImageUrl="_blank.png"
        />

        <ComponentArt:ToolBarItem ItemType="Separator" ImageWidth="10" ImageUrl="_blank.png" CssClass="sep" />

        <ComponentArt:ToolBarItem
          ClientSideCommand="HyperlinkDialog.Show();"
          ToolTip="Insert / edit hyperlink"
          ImageUrl="_blank.png"
          CssClass="item hyperlink"
          HoverCssClass="item hyperlink-h"
          ActiveCssClass="item hyperlink-a"
          DisabledCssClass="item hyperlink-d"
          ChildControlId="HyperlinkDialog"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="RemoveHyperlink"
          ToolTip="Remove hyperlink"
          ImageUrl="_blank.png"
          CssClass="item remove-hyperlink"
          HoverCssClass="item remove-hyperlink-h"
          ActiveCssClass="item remove-hyperlink-a"
          DisabledCssClass="item remove-hyperlink-d"
          Enabled="false"
        />

        <ComponentArt:ToolBarItem
          ClientSideCommand="MediaDialog.Show();"
          ToolTip="Insert / edit media"
          ImageUrl="_blank.png"
          CssClass="item media"
          HoverCssClass="item media-h"
          ActiveCssClass="item media-a"
          DisabledCssClass="item media-d"
          ChildControlId="MediaDialog"
        />



        <ComponentArt:ToolBarItem ItemType="Separator" ImageWidth="10" ImageUrl="_blank.png" CssClass="sep" />


      </Items>

    </ComponentArt:ToolBar>

    <ComponentArt:ToolBar
      ID="ElementsToolBar"
      RunAt="server"
      ImagesBaseUrl="images"
      CssClass="toolbar"
      Width="100"
      Height="25"
      DefaultItemImageWidth="25"
      DefaultItemImageHeight="25"
      UseFadeEffect="false"
    >

      <Items>
        <ComponentArt:ToolBarItem
          ItemType="DropDown"
          ToolTip="Insert symbol"
          ImageUrl="_blank.png"
          ImageWidth="32"
          CssClass="item insert-symbol"
          HoverCssClass="item insert-symbol-h"
          ActiveCssClass="item insert-symbol-a"
          CheckedCssClass="item insert-symbol-c"
          CheckedHoverCssClass="item insert-symbol-c-h"
          CheckedActiveCssClass="item insert-symbol-c-a"
          DisabledCssClass="item insert-symbol-d"
          DisabledCheckedCssClass="item insert-symbol-d-c"
          ExpandedCssClass="item insert-symbol-a"
          ChildControlId="InsertSymbolMenu"
          DropDownId="InsertSymbolMenu"
        />

        <ComponentArt:ToolBarItem ItemType="Separator" ImageWidth="10" ImageUrl="_blank.png" CssClass="sep" />

        <ComponentArt:ToolBarItem
          ItemType="DropDown"
          ToolTip="Insert table"
          ImageUrl="_blank.png"
          ImageWidth="32"
          CssClass="item insert-table"
          HoverCssClass="item insert-table-h"
          ActiveCssClass="item insert-table-a"
          CheckedCssClass="item insert-table-c"
          CheckedHoverCssClass="item insert-table-c-h"
          CheckedActiveCssClass="item insert-table-c-a"
          DisabledCssClass="item insert-table-d"
          DisabledCheckedCssClass="item insert-table-d-c"
          ExpandedCssClass="item insert-table-a"
          ChildControlId="InsertTableMenu"
          DropDownId="InsertTableMenu"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="TableInsertColumnLeft"
          ToolTip="Insert column to the left"
          ImageUrl="_blank.png"
          CssClass="item insert-col-left"
          HoverCssClass="item insert-col-left-h"
          ActiveCssClass="item insert-col-left-a"
          DisabledCssClass="item insert-col-left-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="TableInsertColumnRight"
          ToolTip="Insert column to the right"
          ImageUrl="_blank.png"
          CssClass="item insert-col-right"
          HoverCssClass="item insert-col-right-h"
          ActiveCssClass="item insert-col-right-a"
          DisabledCssClass="item insert-col-right-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="TableInsertRowAbove"
          ToolTip="Insert row above"
          ImageUrl="_blank.png"
          CssClass="item insert-row-above"
          HoverCssClass="item insert-row-above-h"
          ActiveCssClass="item insert-row-above-a"
          DisabledCssClass="item insert-row-above-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="TableInsertRowBelow"
          ToolTip="Insert row below"
          ImageUrl="_blank.png"
          CssClass="item insert-row-below"
          HoverCssClass="item insert-row-below-h"
          ActiveCssClass="item insert-row-below-a"
          DisabledCssClass="item insert-row-below-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="TableDeleteRow"
          ToolTip="Delete row"
          ImageUrl="_blank.png"
          CssClass="item del-row"
          HoverCssClass="item del-row-h"
          ActiveCssClass="item del-row-a"
          DisabledCssClass="item del-row-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="TableDeleteColumn"
          ToolTip="Delete column"
          ImageUrl="_blank.png"
          CssClass="item del-col"
          HoverCssClass="item del-col-h"
          ActiveCssClass="item del-col-a"
          DisabledCssClass="item del-col-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="TableDeleteCell"
          ToolTip="Delete cell"
          ImageUrl="_blank.png"
          CssClass="item del-cell"
          HoverCssClass="item del-cell-h"
          ActiveCssClass="item del-cell-a"
          DisabledCssClass="item del-cell-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="TableMergeCellDown"
          ToolTip="Merge cell down"
          ImageUrl="_blank.png"
          CssClass="item merge-cell-down"
          HoverCssClass="item merge-cell-down-h"
          ActiveCssClass="item merge-cell-down-a"
          DisabledCssClass="item merge-cell-down-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="TableMergeCellRight"
          ToolTip="Merge cell right"
          ImageUrl="_blank.png"
          CssClass="item merge-cell-right"
          HoverCssClass="item merge-cell-right-h"
          ActiveCssClass="item merge-cell-right-a"
          DisabledCssClass="item merge-cell-right-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="TableSplitCellVertical"
          ToolTip="Split cell vertically"
          ImageUrl="_blank.png"
          CssClass="item split-cell-vert"
          HoverCssClass="item split-cell-vert-h"
          ActiveCssClass="item split-cell-vert-a"
          DisabledCssClass="item split-cell-vert-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="TableSplitCellHorizontal"
          ToolTip="Split cell horizontally"
          ImageUrl="_blank.png"
          CssClass="item split-cell-horizontal"
          HoverCssClass="item split-cell-horizontal-h"
          ActiveCssClass="item split-cell-horizontal-a"
          DisabledCssClass="item split-cell-horizontal-d"
        />

        <ComponentArt:ToolBarItem ItemType="Separator" ImageWidth="10" ImageUrl="_blank.png" CssClass="sep" />

        <ComponentArt:ToolBarItem
          EditorCommandType="InsertForm"
          ToolTip="Insert new form"
          ImageUrl="_blank.png"
          CssClass="item form"
          HoverCssClass="item form-h"
          ActiveCssClass="item form-a"
          DisabledCssClass="item form-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="InsertTextbox"
          ToolTip="Insert textbox"
          ImageUrl="_blank.png"
          CssClass="item form-textbox"
          HoverCssClass="item form-textbox-h"
          ActiveCssClass="item form-textbox-a"
          DisabledCssClass="item form-textbox-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="InsertPassword"
          ToolTip="Insert password field"
          ImageUrl="_blank.png"
          CssClass="item form-password"
          HoverCssClass="item form-password-h"
          ActiveCssClass="item form-password-a"
          DisabledCssClass="item form-password-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="InsertHidden"
          ToolTip="Insert hidden field"
          ImageUrl="_blank.png"
          CssClass="item form-hidden"
          HoverCssClass="item form-hidden-h"
          ActiveCssClass="item form-hidden-a"
          DisabledCssClass="item form-hidden-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="InsertButton"
          ToolTip="Insert button"
          ImageUrl="_blank.png"
          CssClass="item form-button"
          HoverCssClass="item form-button-h"
          ActiveCssClass="item form-button-a"
          DisabledCssClass="item form-button-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="InsertReset"
          ToolTip="Insert reset button"
          ImageUrl="_blank.png"
          CssClass="item form-reset"
          HoverCssClass="item form-reset-h"
          ActiveCssClass="item form-reset-a"
          DisabledCssClass="item form-reset-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="InsertSubmit"
          ToolTip="Insert submit button"
          ImageUrl="_blank.png"
          CssClass="item form-submit"
          HoverCssClass="item form-submit-h"
          ActiveCssClass="item form-submit-a"
          DisabledCssClass="item form-submit-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="InsertTextarea"
          ToolTip="Insert textarea"
          ImageUrl="_blank.png"
          CssClass="item form-textarea"
          HoverCssClass="item form-textarea-h"
          ActiveCssClass="item form-textarea-a"
          DisabledCssClass="item form-textarea-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="InsertSelect"
          ToolTip="Insert select box"
          ImageUrl="_blank.png"
          CssClass="item form-select"
          HoverCssClass="item form-select-h"
          ActiveCssClass="item form-select-a"
          DisabledCssClass="item form-select-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="InsertCheckbox"
          ToolTip="Insert checkbox"
          ImageUrl="_blank.png"
          CssClass="item form-checkbox"
          HoverCssClass="item form-checkbox-h"
          ActiveCssClass="item form-checkbox-a"
          DisabledCssClass="item form-checkbox-d"
        />

        <ComponentArt:ToolBarItem
          EditorCommandType="InsertRadio"
          ToolTip="Insert radio button"
          ImageUrl="_blank.png"
          CssClass="item form-radio"
          HoverCssClass="item form-radio-h"
          ActiveCssClass="item form-radio-a"
          DisabledCssClass="item form-radio-d"
        />

        <ComponentArt:ToolBarItem ItemType="Separator" ImageWidth="10" ImageUrl="_blank.png" CssClass="sep" />
      </Items>

    </ComponentArt:ToolBar>
  </ToolBars>

</ComponentArt:Editor>



<%--
  Dialogue box definitions
--%>
<%-- Hyperlink Dialogue --%>
<ComponentArt:Dialog
  ID="HyperlinkDialog"
  RunAt="server"
  AllowDrag="true"
  AllowResize="false"
  Modal="false"
  Alignment="MiddleCentre"
  Width="442"
  Height="155"
  ContentCssClass="dlg-link tabbed"
>

  <ClientEvents>
    <OnShow EventHandler="hyperlinkdialog_show" />
    <OnClose EventHandler="hyperlinkdialog_close" />
  </ClientEvents>

  <Content>
    <div class="ttl" onmousedown="HyperlinkDialog.StartDrag(event);">
      <div class="ttlt">
        <div class="ttlt-l"></div>
        <div class="ttlt-m">
          <a class="close" href="javascript:void(0);" onclick="HyperlinkDialog.close();this.blur();return false;"></a>
          <span>Insert / Edit Hyperlink</span>
        </div>
        <div class="ttlt-r"></div>
      </div>
      <div class="ttlb">
        <div class="ttlb-l"><span></span></div>
        <div class="ttlb-m"><span></span></div>
        <div class="ttlb-r"><span></span></div>
      </div>
    </div>

    <div class="ts">
      <div class="ts-l"></div>
      <div class="ts-m">
        <a href="javascript:void(0);" class="tab-sel" onclick="toggle_dialog_tab(this,HyperlinkDialog,0);" title="Edit link details"><img src="<%= Me.SkinPath %>/images/_blank.png" width="16" height="16" alt="Link icon" class="icon link" /><span class="link">Link</span></a>
        <a href="javascript:void(0);" class="tab" onclick="toggle_dialog_tab(this,HyperlinkDialog,1);this.blur();" title="Edit anchor details"><img src="<%= Me.SkinPath %>/images/_blank.png" width="16" height="16" alt="Anchor icon" class="icon anchor" /><span class="anchor">Anchor</span></a>
        <a href="javascript:void(0);" class="tab" onclick="toggle_dialog_tab(this,HyperlinkDialog,2);this.blur();" title="Edit email details"><img src="<%= Me.SkinPath %>/images/_blank.png" width="16" height="16" alt="Email icon" class="icon email" /><span class="email">Email</span></a>
      </div>
      <div class="ts-r"></div>
    </div>

    <div class="con">
      <div class="con-l link-basic"></div>
      <div class="con-m link-basic">

<ComponentArt:MultiPage ID="HyperlinkPages" RunAt="server">
  <ComponentArt:PageView RunAt="server">
    <div class="row top" title="Required">
      <span class="label">Link URL:</span><input id="link_url" type="text" class="link-url" /><a href="javascript:void(0);" onclick="check_url(this);this.blur();" class="preview-url" title="Preview URL in new window"></a>
    </div>

    <div class="row">
      <span class="label">Link Text:</span><input type="text" class="link-text" />
      <span class="label-narrow" title="Optional">Target:</span><input id="link_target" type="text" class="link-target f-l" title="Optional" />
    </div>

    <div class="hr"><span style="display:none;">.</span></div>

    <div class="row">
      <span class="label" title="Optional">Tooltip:</span><input id="link_tooltip" type="text" class="link-tooltip" disabled="true" />
      <span class="label-narrow">Anchor:</span>
      <span class="combo">
        <ComponentArt:ComboBox
          ID="LinkAnchor"
          RunAt="server"
          Width="121"
          Height="18"
          ItemCssClass="ddn-item"
          ItemHoverCssClass="ddn-item-hover"
          CssClass="dlg-combobox"
          TextBoxCssClass="txt"
          DropImageUrl="images/ddn.png"
          DropHoverImageUrl="images/ddn-hover.png"
          DropDownResizingMode="bottom"
          DropDownWidth="190"
          DropDownHeight="160"
          DropDownCssClass="ddn"
          DropDownContentCssClass="ddn-con"
          SelectedIndex="6"
          Enabled="false"
        >
          <DropDownFooter><div class="ddn-ftr"><span></span></div></DropDownFooter>
          <Items>
            <ComponentArt:ComboBoxItem Text="anchor #1" />
          </Items>
        </ComponentArt:ComboBox>
      </span>
    </div>

    <div class="row">
      <span class="label" title="Select a protocol for the hyperlink">Type:</span>
      <span class="combo">
        <ComponentArt:ComboBox
          ID="LinkType"
          RunAt="server"
          Width="121"
          Height="18"
          ItemCssClass="ddn-item"
          ItemHoverCssClass="ddn-item-hover"
          CssClass="dlg-combobox"
          TextBoxCssClass="txt cur"
          DropImageUrl="images/ddn.png"
          DropHoverImageUrl="images/ddn-hover.png"
          KeyboardEnabled="false"
          TextBoxEnabled="false"
          DropDownResizingMode="bottom"
          DropDownWidth="190"
          DropDownHeight="160"
          DropDownCssClass="ddn"
          DropDownContentCssClass="ddn-con"
          ItemClientTemplateId="LinkTypeItemTemplate"
          SelectedIndex="3"
          Enabled="false"
        >
          <ClientEvents>
            <Change EventHandler="link_type_change" />
          </ClientEvents>

        <DropDownFooter><div class="ddn-ftr"><span></span></div></DropDownFooter>

        <Items>
          <ComponentArt:ComboBoxItem Text="(Other)" Value="" />
          <ComponentArt:ComboBoxItem Text="Local File" Value="file://" />
          <ComponentArt:ComboBoxItem Text="FTP" Value="ftp://" />
          <ComponentArt:ComboBoxItem Text="HTTP" Value="http://" />
          <ComponentArt:ComboBoxItem Text="HTTP (Secure)" Value="https://" />
          <ComponentArt:ComboBoxItem Text="Javascript" Value="javascript:" />
          <ComponentArt:ComboBoxItem Text="Telnet" Value="telnet:" />
          <ComponentArt:ComboBoxItem Text="Usenet" Value="news:" />
        </Items>

        <ClientTemplates>
          <ComponentArt:ClientTemplate ID="LinkTypeItemTemplate">
            <span title="## (DataItem.get_text() == "(Other)") ? "Other: Please specify" : DataItem.get_text() + " (" + DataItem.get_value() + ")" ##">## DataItem.get_text() ##</span>
          </ComponentArt:ClientTemplate>
        </ClientTemplates>
      </ComponentArt:ComboBox>
      </span>
      <input type="text" class="link-type-other f-l disabled" disabled="true" />
    </div>

    <div class="row" title="Optional">
      <span class="label">CSS Class:</span>
      <span class="combo">
        <ComponentArt:ComboBox
        ID="LinkCssClass"
        RunAt="server"
        Width="121"
        Height="18"
        ItemCssClass="ddn-item"
        ItemHoverCssClass="ddn-item-hover"
        CssClass="dlg-combobox"
        TextBoxCssClass="txt cur"
        DropImageUrl="images/ddn.png"
        DropHoverImageUrl="images/ddn-hover.png"
        KeyboardEnabled="false"
        TextBoxEnabled="false"
        DropDownResizingMode="bottom"
        DropDownWidth="190"
        DropDownHeight="160"
        DropDownCssClass="ddn"
        DropDownContentCssClass="ddn-con"
        >
          <DropDownFooter><div class="ddn-ftr"><span></span></div></DropDownFooter>

          <Items>
            <ComponentArt:ComboBoxItem Text="(None)" Value="" />
          </Items>
        </ComponentArt:ComboBox>
      </span>
    </div>
  </ComponentArt:PageView>

  <ComponentArt:PageView RunAt="server">
    <div class="row top"><span class="label">Name:</span><input type="text" class="anchor-name" /></div>
    <div class="row"><span class="label">Text:</span><input type="text" class="anchor-text" /></div>
  </ComponentArt:PageView>


  <ComponentArt:PageView  id="Email" RunAt="server">
    <div class="row top"><span class="label">Address:</span><input type="text" class="email-address" /></div>
    <div class="row"><span class="label">Subject:</span><input type="text" class="email-subject" /></div>
    <div class="row" title="Optional">
      <span class="label">Link Text:</span><input type="text" class="email-text" />
      <span class="label-narrow">Class:</span>
      <span class="combo">
        <ComponentArt:ComboBox
          ID="EmailCssClass"
          RunAt="server"
          Width="121"
          Height="18"
          ItemCssClass="ddn-item"
          ItemHoverCssClass="ddn-item-hover"
          CssClass="dlg-combobox"
          TextBoxCssClass="txt cur"
          DropImageUrl="images/ddn.png"
          DropHoverImageUrl="images/ddn-hover.png"
          KeyboardEnabled="false"
          TextBoxEnabled="false"
          DropDownResizingMode="bottom"
          DropDownWidth="190"
          DropDownHeight="160"
          DropDownCssClass="ddn"
          DropDownContentCssClass="ddn-con"
        >
          <DropDownFooter><div class="ddn-ftr"></div></DropDownFooter>

          <Items>
            <ComponentArt:ComboBoxItem Text="(None)" Value="" />
          </Items>
        </ComponentArt:ComboBox>
      </span>
    </div>
  </ComponentArt:PageView>
</ComponentArt:MultiPage>
      </div>
      <div class="con-r link-basic"></div>
    </div>

    <div class="ftr">
      <div class="ftr-l"></div>
      <div class="ftr-m">
        <a onclick="toggle_hyperlink_options(this,HyperlinkDialog);this.blur();return false;" href="javascript:void(0);" id="hyperlink-options" class="btn70 f-l"><span class="advanced">Advanced</span></a>
        <a onclick="HyperlinkDialog.close(true);this.blur();return false;" href="javascript:void(0);" class="btn70 f-r"><span>Insert</span></a>
        <a onclick="HyperlinkDialog.close();this.blur();return false;" href="javascript:void(0);" class="btn70 f-r first"><span>Close</span></a>
      </div>
      <div class="ftr-r"></div>
    </div>
  </Content>
</ComponentArt:Dialog>
<%-- Hyperlink Dialogue --%>



<%-- Media Dialogue --%>
<ComponentArt:Dialog
  ID="MediaDialog"
  RunAt="server"
  AllowDrag="true"
  AllowResize="false"
  Modal="false"
  Alignment="MiddleCentre"
  Width="600"
  Height="298"
  ContentCssClass="dlg-media tabbed"
>
  <ClientEvents>
    <%--
    <OnShow EventHandler="mediadialog_show" />
    <OnClose EventHandler="mediadialog_close" />
    --%>
  </ClientEvents>

  <Content>
    <div class="ttl" onmousedown="MediaDialog.StartDrag(event);">
      <div class="ttlt">
        <div class="ttlt-l"></div>
        <div class="ttlt-m">
          <a class="close" href="javascript:void(0);" onclick="MediaDialog.close();this.blur();return false;"></a>
          <span>Insert / Edit Media</span>
        </div>
        <div class="ttlt-r"></div>
      </div>
      <div class="ttlb">
        <div class="ttlb-l"><span></span></div>
        <div class="ttlb-m"><span></span></div>
        <div class="ttlb-r"><span></span></div>
      </div>
    </div>

    <div class="ts">
      <div class="ts-l"></div>
      <div class="ts-m">
        <a href="javascript:void(0);" class="tab-sel" onclick="toggle_dialog_tab(this,MediaDialog,0);this.blur();" title="Edit image details"><img src="<%= Me.SkinPath %>/images/_blank.png" width="16" height="16" alt="Image icon" class="icon image" /><span class="image">Image</span></a>
      </div>
      <div class="ts-r"></div>
    </div>

    <div class="con">
      <div class="con-l"></div>
      <div class="con-m">

<ComponentArt:MultiPage Id="MediaPages" RunAt="server">
  <ComponentArt:PageView RunAt="server">
    <div class="fields">
      <div class="row top" title="Required"><span class="label">Image URL:</span><input type="text" class="image-url" /><a href="javascript:void(0);" onclick="load_image(this,MediaDialog);this.blur();" class="preview-url" title="Preview this image"></a></div>

      <div class="row" title="Required"><span class="label">Image Description:</span><input type="text" class="image-desc" /></div>

      <div class="row" title="Required">
      <span class="label">Width:</span><input type="text" class="image-width" />
      <span class="label-narrow">Height:</span><input type="text" class="image-height float-left" />
      </div>

      <div class="row" title="Optional">
        <span class="label">Alignment:</span>
        <span class="combo">
          <ComponentArt:ComboBox
            ID="ImageAlignment"
            RunAt="server"
            Width="104"
            Height="18"
            ItemCssClass="ddn-item"
            ItemHoverCssClass="ddn-item-hover"
            CssClass="dlg-combobox"
            TextBoxCssClass="txt cur"
            DropImageUrl="images/ddn.png"
            DropHoverImageUrl="images/ddn-hover.png"
            KeyboardEnabled="false"
            TextBoxEnabled="false"
            DropDownResizingMode="bottom"
            DropDownWidth="190"
            DropDownHeight="160"
            DropDownCssClass="ddn"
            DropDownContentCssClass="ddn-con"
            SelectedIndex="0"
          >

            <DropDownFooter><div class="ddn-ftr"></div></DropDownFooter>

            <Items>
              <ComponentArt:ComboBoxItem Text="(Default)" Value="" />
              <ComponentArt:ComboBoxItem Text="Baseline" Value="baseline" />
              <ComponentArt:ComboBoxItem Text="Top" Value="top" />
              <ComponentArt:ComboBoxItem Text="Middle" Value="middle" />
              <ComponentArt:ComboBoxItem Text="Bottom" Value="bottom" />
              <ComponentArt:ComboBoxItem Text="TextTop" Value="texttop" />
              <ComponentArt:ComboBoxItem Text="Absolute middle" Value="absmiddle" />
              <ComponentArt:ComboBoxItem Text="Absolute bottom" Value="absbottom" />
              <ComponentArt:ComboBoxItem Text="Left" Value="left" />
              <ComponentArt:ComboBoxItem Text="Right" Value="right" />
            </Items>
          </ComponentArt:ComboBox>

        </span>
        <span class="label-narrow">Border:</span><input type="text" class="image-border float-left" />
      </div>

      <div class="hr"><span></span></div>

      <div class="row" title="Optional">
        <span class="label">Horizontal Spacing:</span><input type="text" class="image-hspace" />
        <span class="label">Vertical Spacing:</span><input type="text" class="image-vspace float-left" />
      </div>

      <div class="row" title="Optional"><span class="label">CSS Class:</span><input type="text" class="image-class" /></div>

      <div class="row" title="Optional"><span class="label">Inline CSS:</span><input type="text" class="image-inline" /></div>
    </div>

    <div class="image-preview">
      <div class="status">Preview</div>
      <div class="thumbnail"></div>
    </div>
  </ComponentArt:PageView>

  <ComponentArt:PageView RunAt="server">
    <div style="width:582px;line-height:216px;text-align:center;">Flash: Coming soon</div>
  </componentArt:PageView>

  <ComponentArt:PageView RunAt="server">
    <div style="width:582px;line-height:216px;text-align:center;">Video: Coming soon</div>
  </ComponentArt:PageView>
</ComponentArt:MultiPage>

      </div>
      <div class="con-r"></div>
    </div>

    <div class="ftr">
      <div class="ftr-l"></div>
      <div class="ftr-m">
        <a onclick="MediaDialog.close(true);this.blur();return false;" href="javascript:void(0);" class="btn70 f-r"><span>Insert</span></a>
        <a onclick="MediaDialog.close();this.blur();return false;" href="javascript:void(0);" class="btn70 f-r first"><span>Close</span></a>
      </div>
      <div class="ftr-r"></div>
    </div>
  </Content>
</ComponentArt:Dialog>
<%-- /Media Dialogue --%>


<%-- SpellCheck Dialogue & Control --%>
<ComponentArt:Dialog
  ID="SpellCheckDialog"
  RunAt="server"
  AllowDrag="true"
  AllowResize="false"
  Modal="false"
  Alignment="MiddleCentre"
  Width="456"
  Height="307"
  ContentCssClass="dlg-spell"
>
  <Content>
    <div class="ttl" onmousedown="SpellCheckDialog.StartDrag(event);">
      <div class="ttlt">
        <div class="ttlt-l"></div>
        <div class="ttlt-m">
          <a class="close" href="javascript:void(0);" onclick="SpellCheckDialog.close();this.blur();return false;"></a>
          <span>Check Spelling</span>
        </div>
        <div class="ttlt-r"></div>
      </div>

      <div class="ttlb">
        <div class="ttlb-l"><span></span></div>
        <div class="ttlb-m"><span></span></div>
        <div class="ttlb-r"><span></span></div>
      </div>
    </div>

    <div class="con">
      <div class="con-l"></div>
      <div class="con-m">

        <div class="highlight">
          <div class="label">Not in Dictionary:</div>
          <div class="text" id="SpellCheckDialog_Html"></div>
          <div class="f-r">
            <a onclick="Spell1.dialogIgnore();this.blur();return false;" href="javascript:void(0);" class="btn105"><span>Ignore Once</span></a>
            <a onclick="Spell1.dialogIgnoreAll();this.blur();return false;" href="javascript:void(0);" class="btn105"><span>Ignore All</span></a>
            <a onclick="Spell1.dialogAdd();this.blur();return false;" href="javascript:void(0);" class="btn105"><span>Add to Dictionary</span></a>
          </div>
        </div>

        <div class="suggest">
          <div class="label">Suggestions:</div>
          <div class="wordlist" id="SpellCheckDialog_Suggestions"></div>
          <div class="f-r">
            <a onclick="Spell1.dialogChange(document.getElementById('SpellCheckDialog_Replacement').value);this.blur();return false;" href="javascript:void(0);" class="btn105"><span>Change</span></a>
            <a onclick="Spell1.dialogChangeAll(document.getElementById('SpellCheckDialog_Replacement').value);this.blur();return false;" href="javascript:void(0);" class="btn105"><span>Change All</span></a>
          </div>
          <div class="row"><span class="label">Change to:</span><input type="text" id="SpellCheckDialog_Replacement" /></div>
        </div>

      </div>
      <div class="con-r"></div>
    </div>

    <div class="ftr">
      <div class="ftr-l"></div>
      <div class="ftr-m">
        <a onclick="SpellCheckDialog.close();this.blur();return false;" href="javascript:void(0);" class="btn70 f-r"><span>Close</span></a>
      </div>
      <div class="ftr-r"></div>
    </div>
  </Content>
</ComponentArt:Dialog>

<ComponentArt:SpellCheck RunAt="server" ID="Spell1" ControlToCheck="editor1" ShowDialog="false" ErrorCssClass="error">
  <ClientEvents>
    <DialogComplete EventHandler="spellcheck_complete" />
    <DialogUpdateNeeded EventHandler="spellcheck_update" />
  </ClientEvents>
</ComponentArt:SpellCheck>
<%-- /SpellCheck Dialogue & Control --%>

<%-- Find and Replace Dialogue --%>
<ComponentArt:Dialog
  ID="FindDialog"
  RunAt="server"
  AllowDrag="true"
  AllowResize="false"
  Modal="false"
  Alignment="MiddleCentre"
  Width="430"
  Height="126"
  ContentCssClass="dlg-find tabbed"
>
  <ClientEvents>
    <OnShow EventHandler="finddialog_show" />
    <OnClose EventHandler="finddialog_close" />
  </ClientEvents>
  <Content>
    <div class="ttl" onmousedown="FindDialog.StartDrag(event);">
      <div class="ttlt">
        <div class="ttlt-l"></div>
        <div class="ttlt-m">
          <a class="close" href="javascript:void(0);" onclick="FindDialog.close();this.blur();return false;"></a>
          <span>Find &amp; Replace</span>
        </div>
        <div class="ttlt-r"></div>
      </div>
      <div class="ttlb">
        <div class="ttlb-l"><span></span></div>
        <div class="ttlb-m"><span></span></div>
        <div class="ttlb-r"><span></span></div>
      </div>
    </div>

    <div class="ts">
      <div class="ts-l"></div>
      <div class="ts-m">
        <a href="javascript:void(0);" class="tab-sel" onclick="toggle_dialog_tab(this,FindDialog,0);this.blur();" title="Find"><img src="<%= Me.SkinPath %>/images/_blank.png" width="16" height="16" alt="Find icon" class="icon find" /><span class="image">Find</span></a>
        <a href="javascript:void(0);" class="tab" onclick="toggle_dialog_tab(this,FindDialog,1);this.blur();" title="Replace"><img src="<%= Me.SkinPath %>/images/_blank.png" width="16" height="16" alt="Replace icon" class="icon replace" /><span class="image">Replace</span></a>
      </div>
      <div class="ts-r"></div>
    </div>

    <div class="con">
      <div class="con-l find"></div>
      <div class="con-m find">

<ComponentArt:MultiPage ID="FindPages" RunAt="server">
  <ComponentArt:PageView RunAt="server">
    <div class="fields">
      <div class="row top"><span class="label">Find what:</span><input type="text" class="find-text" NAME="find_text"/></div>

      <div class="row">
        <span class="label dir">Direction:</span>
        <a class="rad find-up" href="javascript:void(0);" onclick="toggle_radio(this,'rad-sel','rad');this.blur();return false;">Up</a>
        <a class="rad-sel find-down" href="javascript:void(0);" onclick="toggle_radio(this,'rad-sel','rad');this.blur();return false;">Down</a>
      </div>

      <div class="row widgets"><a id="find_matchwholeword" class="chk find-match-word" href="javascript:void(0);" onclick="toggle_checkbox(this,'chk-sel','chk');this.blur();return false;">Match whole word only</a></div>
      <div class="row widgets"><a id="find_matchcase" class="chk find-match-case" href="javascript:void(0);" onclick="toggle_checkbox(this,'chk-sel','chk');this.blur();return false;">Match case</a></div>
    </div>

    <div class="btns">
      <a onclick="FindDialog_Find();this.blur();return false;" href="javascript:void(0);" class="btn105 float-right"><span>Find Next</span></a>
    </div>
  </ComponentArt:PageView>

  <ComponentArt:PageView RunAt="server">
  <div class="fields">
    <div class="row top"><span class="label">Find what:</span><input id="replace_text" type="text" class="replace-find-text" NAME="replace_text"/></div>
    <div class="row"><span class="label">Replace with:</span><input id="replace_replacetext" type="text" class="replace-text" NAME="replace_replacetext"/></div>

    <div class="row">
      <span class="label" style="">Direction:</span>
      <a class="rad replace-up" href="javascript:void(0);" onclick="toggle_radio(this,'rad-sel','rad');this.blur();return false;">Up</a>
      <a class="rad-sel replace-down" href="javascript:void(0);" onclick="toggle_radio(this,'rad-sel','rad');this.blur();return false;">Down</a>
    </div>

    <div class="row widgets"><a id="replace_matchwholeword" class="chk replace-match-word" href="javascript:void(0);" onclick="toggle_checkbox(this,'chk-sel','chk');this.blur();return false;">Match whole word only</a></div>
    <div class="row widgets"><a id="replace_matchcase" class="chk replace-match-case" href="javascript:void(0);" onclick="toggle_checkbox(this,'chk-sel','chk');this.blur();return false;">Match case</a></div>
  </div>

  <div class="btns">
    <a onclick="FindDialog_FindReplace();this.blur();return false;" href="javascript:void(0);" class="btn105"><span>Find Next</span></a>
    <a onclick="FindDialog_Replace();this.blur();return false;" href="javascript:void(0);" class="btn105"><span>Replace</span></a>
    <a onclick="FindDialog_ReplaceAll();this.blur();return false;" href="javascript:void(0);" class="btn105"><span>Replace All</span></a>
  </div>
  </ComponentArt:PageView>
</ComponentArt:MultiPage>

      </div>
      <div class="con-r find"></div>
    </div>

    <div class="ftr">
      <div class="ftr-l"></div>
      <div class="ftr-m">
        <a onclick="FindDialog.close();this.blur();return false;" href="javascript:void(0);" class="btn70 f-r"><span>Close</span></a>
      </div>
      <div class="ftr-r"></div>
    </div>
  </Content>
</ComponentArt:Dialog>
<%-- /Find and Replace Dialogue --%>

















<%-- Menus --%>
<%-- Undo & Redo Menus --%>
<ComponentArt:Menu
  ID="UndoMenu"
  RunAt="server"
  ContextMenu="custom"
  Orientation="Vertical"
  CssClass="ddn"
  TopGroupExpandDirection="BelowLeft"
  TopGroupExpandOffsetY="-3"
  ShadowEnabled="false"
>
  <Items>
    <ComponentArt:MenuItem ClientTemplateId="UndoTemplate" />
  </Items>
  <ClientTemplates>
    <ComponentArt:ClientTemplate ID="UndoTemplate">
      <div class="ddn-con undo-list">
        <span>Undo</span>
        <div class="list">## undo_html(Parent); ##</div>
      </div>
    </ComponentArt:ClientTemplate>
  </ClientTemplates>
</ComponentArt:Menu>

<ComponentArt:Menu
  ID="RedoMenu"
  RunAt="server"
  ContextMenu="custom"
  Orientation="Vertical"
  CssClass="ddn"
  TopGroupExpandDirection="BelowLeft"
  TopGroupExpandOffsetY="-3"
  ShadowEnabled="false"
>
  <Items>
    <ComponentArt:MenuItem ClientTemplateId="RedoTemplate" />
  </Items>
  <ClientTemplates>
    <ComponentArt:ClientTemplate ID="RedoTemplate">
      <div class="ddn-con undo-list">
        <span>Redo</span>
        <div class="list">## redo_html(Parent); ##</div>
      </div>
    </ComponentArt:ClientTemplate>
  </ClientTemplates>
</ComponentArt:Menu>
<%-- /Undo & Redo Menus --%>

<%-- Bullets & Numbering Menus --%>
<ComponentArt:Menu
  ID="BulletsMenu"
  RunAt="server"
  ContextMenu="custom"
  Orientation="Vertical"
  CssClass="ddn ddn-con"
  TopGroupExpandDirection="BelowLeft"
  TopGroupItemSpacing="2"
  ShadowEnabled="false"
>
  <Items>
    <ComponentArt:MenuItem ClientTemplateId="BulletsTemplate" ListCssClass="ul1" ListCssClassDepth="3" />
    <ComponentArt:MenuItem Enabled="false" Look-CssClass="bullets-break" />
    <ComponentArt:MenuItem ClientTemplateId="BulletsTemplate" ListCssClass="ul2" ListCssClassDepth="3" />
    <ComponentArt:MenuItem Enabled="false" Look-CssClass="bullets-break" />
    <ComponentArt:MenuItem ClientTemplateId="BulletsTemplate" ListCssClass="ul3" ListCssClassDepth="3" />
  </Items>
  <ClientTemplates>
    <ComponentArt:ClientTemplate ID="BulletsTemplate">## ListMenuItemHtml('ul', DataItem.getProperty("ListCssClass"), DataItem.getProperty("ListCssClassDepth")) ##</ComponentArt:ClientTemplate>
  </ClientTemplates>
  <ClientEvents>
    <ItemSelect EventHandler="handleBulletsMenuItemSelect" />
  </ClientEvents>
</ComponentArt:Menu>

<ComponentArt:Menu
  ID="NumberingMenu"
  RunAt="server"
  ContextMenu="custom"
  Orientation="Vertical"
  CssClass="ddn ddn-con"
  TopGroupExpandDirection="BelowLeft"
  TopGroupItemSpacing="2"
  ShadowEnabled="false"
>
  <Items>
    <ComponentArt:MenuItem ClientTemplateId="NumberingTemplate" ListCssClass="ol1" ListCssClassDepth="3" />
    <ComponentArt:MenuItem Enabled="false" Look-CssClass="bullets-break" />
    <ComponentArt:MenuItem ClientTemplateId="NumberingTemplate" ListCssClass="ol2" ListCssClassDepth="2" />
    <ComponentArt:MenuItem Enabled="false" Look-CssClass="bullets-break" />
    <ComponentArt:MenuItem ClientTemplateId="NumberingTemplate" ListCssClass="ol3" ListCssClassDepth="5" />
    <ComponentArt:MenuItem Enabled="false" Look-CssClass="bullets-break" />
    <ComponentArt:MenuItem ClientTemplateId="NumberingTemplate" ListCssClass="ol4" ListCssClassDepth="5" />
    <ComponentArt:MenuItem Enabled="false" Look-CssClass="bullets-break" />
    <ComponentArt:MenuItem ClientTemplateId="NumberingTemplate" ListCssClass="ol5" ListCssClassDepth="3" />
  </Items>
  <ClientTemplates>
    <ComponentArt:ClientTemplate ID="NumberingTemplate">## ListMenuItemHtml('ol', DataItem.getProperty("ListCssClass"), DataItem.getProperty("ListCssClassDepth")) ##</ComponentArt:ClientTemplate>
  </ClientTemplates>
  <ClientEvents>
    <ItemSelect EventHandler="handleNumberingMenuItemSelect" />
  </ClientEvents>
</ComponentArt:Menu>
<%-- /Bullets & Numbering Menus --%>


<%-- Insert Symbol Menu --%>
 <ComponentArt:Menu
      ID="InsertSymbolMenu"
      RunAt="server"
      ContextMenu="custom"
      CssClass="symbol-menu"
      CollapseDuration="0"
      ShadowEnabled="false"
      >
      <ClientEvents>
        <ContextMenuShow EventHandler="reset_symbol_title" />
        <ItemMouseOut EventHandler="reset_symbol_title" />
      </ClientEvents>

      <Items>
        <ComponentArt:MenuItem ClientTemplateId="InsertSymbolTemplate" />
      </Items>

      <ClientTemplates>
        <ComponentArt:ClientTemplate ID="InsertSymbolTemplate">
          <div id="symbol-title"><span>Insert Symbol</span></div>
          <div id="insert-symbol">
            <div class="symbol-row"><a  href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Euro">&euro;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Pound">&pound;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Yen">&yen;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Copyright">&copy;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Registered trademark">&reg;</a></div>
            <div class="symbol-row"><a  href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Trademark">&#153;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Plus or minus">&plusmn;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Not equal to">&ne;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Less-than or equal to">&le;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Greater-than or equal to">&ge;</a></div>
            <div class="symbol-row"><a  href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Division">&divide;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Multiplication">&times;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Infinity">&infin;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Summation">&sum;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Integral">&int;</a></div>
            <div class="symbol-row"><a  href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Lower-case Mu">&mu;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Lower-case Alpha">&alpha;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Lower-case Beta">&beta;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Lower-case Pi">&pi;</a><a
                                        href="javascript:void(0);" onclick="insert_symbol(##Parent.ClientControlId##,this);" onmouseover="update_symbol_title(this);" title="Upper-case Omega">&Omega;</a></div>
          </div>
          <div class="symbol-footer"></div>
        </ComponentArt:ClientTemplate>
      </ClientTemplates>

    </ComponentArt:Menu>
<%-- /Insert Symbol Menu --%>


<%-- Insert Table Menu --%>
<ComponentArt:Menu
      ID="InsertTableMenu"
      RunAt="server"
      ContextMenu="custom"
      CssClass="insert-table-menu"
      CollapseDuration="0"
      ShadowEnabled="false"
      >
      <ClientEvents>
        <ContextMenuShow EventHandler="insert_table_menu_show" />
      </ClientEvents>

      <Items>
        <ComponentArt:MenuItem ClientTemplateId="InsertTableTemplate" />
      </Items>

      <ClientTemplates>
        <ComponentArt:ClientTemplate ID="InsertTableTemplate">
          <div id="table-title"><span>Insert Table</span></div>
          <div id="insert-table">
            <div id="table-cells-highlight"></div>
            <div id="table-cells"></div>
            <div id="table-cells-events" onclick="insert_table(##Parent.ClientControlId##);"></div>
          </div>
          <div class="insert-table-footer"></div>
        </ComponentArt:ClientTemplate>
      </ClientTemplates>

    </ComponentArt:Menu>
<%-- /Insert Table Menu --%>











<%--
  Colour menu
--%>
   <ComponentArt:Menu
      ID="FontColorMenu"
      RunAt="server"
      ContextMenu="Custom"
      Orientation="Vertical"
      TopGroupExpandDirection="BelowLeft"
      CssClass="color-menu"
      ImagesBaseUrl="images/menus/"
      CollapseDuration="0"
      ShadowEnabled="false"
  >
      <ItemLooks>
        <ComponentArt:ItemLook LookId="ColorMenuItem" CssClass="color-menu-item" HoverCssClass="color-menu-item-hover" />
      </ItemLooks>

      <Items>
        <ComponentArt:MenuItem ClientTemplateId="SwatchesTemplate" />
        <ComponentArt:MenuItem ID="AddCustomColor" Text="Add Custom Color..." Look-LeftIconUrl="icon-palette.png" LookId="ColorMenuItem" />
      </Items>

      <ClientEvents>
        <ContextMenuShow EventHandler="color_menu_show" />
        <ContextMenuHide EventHandler="color_menu_hide" />
        <ItemSelect EventHandler="color_menu_select" />
      </ClientEvents>

      <ClientTemplates>
        <ComponentArt:ClientTemplate Id="SwatchesTemplate">
          <div class="swatch-title-preset"><span>Preset Colors</span></div>
          <div><img src="## eval(Parent.ParentEditor.ClientControlId+'_SkinPath') ##/images/toolbar/_blank.png" width="176" height="92" class="preset-swatches" border="0" /></div>

          <div class="swatch-title"><span>Standard Colors</span></div>
          <div><img src="## eval(Parent.ParentEditor.ClientControlId+'_SkinPath') ##/images/toolbar/_blank.png" width="176" height="21" class="standard-swatches" border="0" /></div>

          <div class="swatch-title"><span>Custom Colors</span></div>
          <div class="custom-swatches">
            <div class="swatches-container">
              <div id="custom-0" class="swatch"><img src="## eval(Parent.ParentEditor.ClientControlId+'_SkinPath') ##/images/toolbar/_blank.png" width="13" height="13" /></div>
              <div id="custom-1" class="swatch"><img src="## eval(Parent.ParentEditor.ClientControlId+'_SkinPath') ##/images/toolbar/_blank.png" width="13" height="13" /></div>
              <div id="custom-2" class="swatch"><img src="## eval(Parent.ParentEditor.ClientControlId+'_SkinPath') ##/images/toolbar/_blank.png" width="13" height="13" /></div>
              <div id="custom-3" class="swatch"><img src="## eval(Parent.ParentEditor.ClientControlId+'_SkinPath') ##/images/toolbar/_blank.png" width="13" height="13" /></div>
              <div id="custom-4" class="swatch"><img src="## eval(Parent.ParentEditor.ClientControlId+'_SkinPath') ##/images/toolbar/_blank.png" width="13" height="13" /></div>
              <div id="custom-5" class="swatch"><img src="## eval(Parent.ParentEditor.ClientControlId+'_SkinPath') ##/images/toolbar/_blank.png" width="13" height="13" /></div>
              <div id="custom-6" class="swatch"><img src="## eval(Parent.ParentEditor.ClientControlId+'_SkinPath') ##/images/toolbar/_blank.png" width="13" height="13" /></div>
              <div id="custom-7" class="swatch"><img src="## eval(Parent.ParentEditor.ClientControlId+'_SkinPath') ##/images/toolbar/_blank.png" width="13" height="13" /></div>
              <div id="custom-8" class="swatch"><img src="## eval(Parent.ParentEditor.ClientControlId+'_SkinPath') ##/images/toolbar/_blank.png" width="13" height="13" /></div>
              <div id="custom-9" class="swatch"><img src="## eval(Parent.ParentEditor.ClientControlId+'_SkinPath') ##/images/toolbar/_blank.png" width="13" height="13" /></div>
            </div>
          </div>

          <div class="color-selected" id="color-selected">
            <div class="color-highlight" onmousemove="move_swatch_highlight(this,event);" onclick="select_swatch(this,##DataItem.get_parentMenu().get_clientControlId()##)"></div>
          </div>

        </ComponentArt:ClientTemplate>
      </ClientTemplates>

    </ComponentArt:Menu>