
public  class EditorArcticWhiteAllFeatures : System.Web.UI.UserControl 
{
  protected ComponentArt.Web.UI.Editor editor1;
  protected ComponentArt.Web.UI.ToolBar FormattingToolBar;
  protected ComponentArt.Web.UI.ToolBar StandardToolBar;
  protected ComponentArt.Web.UI.ToolBar ElementsToolBar;

  protected ComponentArt.Web.UI.ComboBox FontStyles;
  protected ComponentArt.Web.UI.ComboBox FontFamily;
  protected ComponentArt.Web.UI.ComboBox FontSize;

  protected ComponentArt.Web.UI.ComboBox LinkAnchor;
  protected ComponentArt.Web.UI.ComboBox LinkType;
  protected ComponentArt.Web.UI.ComboBox LinkCssClass;
  protected ComponentArt.Web.UI.ComboBox EmailCssClass;
  protected ComponentArt.Web.UI.ComboBox ImageAlignment;

  protected System.Web.UI.WebControls.Literal SkinPathClientOutput;

  public ComponentArt.Web.UI.Editor Editor
  {
    get
    {
      return this.editor1;
    }
  }

  private string _defaultSkinPath;
  private string _skinPath = null;
  public string SkinPath 
  {
    get
    {
      return (this._skinPath != null) ? this._skinPath : this._defaultSkinPath;
    }
    set
    {
      this._skinPath = value;
    }
  }

  private void Page_Init(object sender,System.EventArgs e) 
  {
    this._defaultSkinPath = this.TemplateSourceDirectory;
  }

  private void Page_PreRender(object sender,System.EventArgs e) 
  {
    ApplySkinPath();
  }

  private void ApplySkinPath() 
  {
    //  Editor
    editor1.CssFileURL = ApplySkinPath(editor1.CssFileURL);

    //  ToolBar
    FormattingToolBar.ImagesBaseUrl = ApplySkinPath(FormattingToolBar.ImagesBaseUrl);
    StandardToolBar.ImagesBaseUrl = ApplySkinPath(StandardToolBar.ImagesBaseUrl);
    ElementsToolBar.ImagesBaseUrl = ApplySkinPath(ElementsToolBar.ImagesBaseUrl);

    //  ComboBoxes
    //  Formatting ToolBar
    FontStyles.DropImageUrl = ApplySkinPath(FontStyles.DropImageUrl);
    FontStyles.DropHoverImageUrl = ApplySkinPath(FontStyles.DropHoverImageUrl);
    FontFamily.DropImageUrl = ApplySkinPath(FontFamily.DropImageUrl);
    FontFamily.DropHoverImageUrl = ApplySkinPath(FontFamily.DropHoverImageUrl);
    FontSize.DropImageUrl = ApplySkinPath(FontSize.DropImageUrl);
    FontSize.DropHoverImageUrl = ApplySkinPath(FontSize.DropHoverImageUrl);

    //  Hyperlink Dialogue
    LinkAnchor.DropImageUrl = ApplySkinPath(LinkAnchor.DropImageUrl);
    LinkAnchor.DropHoverImageUrl = ApplySkinPath(LinkAnchor.DropHoverImageUrl);
    LinkType.DropImageUrl = ApplySkinPath(LinkType.DropImageUrl);
    LinkType.DropHoverImageUrl = ApplySkinPath(LinkType.DropHoverImageUrl);
    LinkCssClass.DropImageUrl = ApplySkinPath(LinkCssClass.DropImageUrl);
    LinkCssClass.DropHoverImageUrl = ApplySkinPath(LinkCssClass.DropHoverImageUrl);
    EmailCssClass.DropImageUrl = ApplySkinPath(EmailCssClass.DropImageUrl);
    EmailCssClass.DropHoverImageUrl = ApplySkinPath(EmailCssClass.DropHoverImageUrl);

    //  Media Dialogue
    ImageAlignment.DropImageUrl = ApplySkinPath(ImageAlignment.DropImageUrl);
    ImageAlignment.DropHoverImageUrl = ApplySkinPath(ImageAlignment.DropHoverImageUrl);


    //  Literal
    SkinPathClientOutput.Text = SkinPathClientOutputJS();
  }

  private string ApplySkinPath(string localPath) 
  {
    if (localPath != null && !localPath.StartsWith("/") && !localPath.StartsWith("~")) return System.String.Concat(this.SkinPath, "/", localPath);
    else return localPath;
  }

  private string SkinPathClientOutputJS() 
  {
    System.Text.StringBuilder result = new System.Text.StringBuilder();
    result.Append("<script type=\"text/javascript\">");
    result.Append("window.");
    result.Append(editor1.ClientObjectId);
    result.Append("_SkinPath = '");
    result.Append(this.SkinPath);
    result.Append("';");
    result.Append("</script>");
    return result.ToString();
  }

  override protected void OnInit(System.EventArgs e) 
  {
    InitializeComponent();
    base.OnInit(e);
  }

  private void InitializeComponent() 
  {
    this.Page.Init += new System.EventHandler(this.Page_Init);
    this.Page.PreRender += new System.EventHandler(this.Page_PreRender);
  }

  protected void Page_Load(object sender,System.EventArgs e) { }
}
