function checkHandler()
{
  if(Spell1.DialogBegin())
  {
    // show the dialog  
    Dialog1.Show();
  }
}

function dialogComplete()
{
  alert('Spell check is complete!');
  Dialog1.Close();
}

function updateDialog()
{
  // update html
  var domObj = document.getElementById('Dialog1_Html');
  if(domObj)
  {
    domObj.innerHTML = Spell1.DialogGetErrorsHtml();

    var currentError = document.getElementById('Spell1_CurrentError');
    if(currentError && currentError.scrollIntoView)
    {
      currentError.scrollIntoView();
    }
  }
  
  // update suggestions
  Spell1.DialogGetSuggestions(suggestHandler);
}

function suggestHandler(result)
{
  var suggestionSelect = document.getElementById('Dialog1_Suggestions');
  if(suggestionSelect)
  {
    suggestionSelect.innerHTML = '';

    for(var i = 0; i < result.length; i++)
    {
      var newOpt = document.createElement('a');
      newOpt.innerHTML = result[i];
      newOpt.id = 'Dialog1_Word_' + i;
      newOpt.onclick = new Function('wordlistSelect(' + i + ')');
      newOpt.href = "javascript:void(0);";

      suggestionSelect.appendChild(newOpt);
    }
  }

  wordlistSelect(0);
}

function wordlistSelect(num)
{
  var el = document.getElementById('Dialog1_Word_' + num);
  var word = '';

  if(el)
  {
    word = el.innerHTML;
    if (window.selected_word != null) document.getElementById(window.selected_word).className = "";

    el.className = "selected";
    window.selected_word = el.id;
  }

  document.getElementById("Dialog1_Replacement").value = word;
}
