/********************************************
  Common functions
********************************************/
// Find the position of elements relative to the viewable screen area.
// Returns [x_coord,y_coord];
function get_position(el) {
  var el_left = 0;
  var el_top = 0;

  if (el.offsetParent) {
    el_left = el.offsetLeft;
    el_top = el.offsetTop;
    while (el = el.offsetParent) { el_left += el.offsetLeft;el_top += el.offsetTop; }
  }

  return [el_left,el_top];
}

//  Makes <a> within a container act as radio buttons -- only one can ever be in a selected state.
function toggle_radio(el,selected_css,default_css) {
  if (el.className == selected_css) return;

  var p = el.parentNode;
  var a= p.getElementsByTagName("a");
  for (var i = 0;i < a.length;i++) a[i].className = default_css;
  el.className = selected_css;
}

//  Makes <a> act as checkbox.
function toggle_checkbox(el,selected_css,default_css) {
  el.className = (el.className.indexOf(selected_css) != -1) ? el.className.replace(selected_css,default_css) : el.className.replace(default_css,selected_css);
}



//  Returns the first DOM element of type "el_type" and of class containing the string "css" found within "container"
//  If none are found, returns null;
function get_element(container,el_type,css) {
  var arr = container.getElementsByTagName(el_type);

  for (var i = 0; i < arr.length; i++) {
    if (arr[i].className.indexOf(css) != -1) return arr[i];
  }

  return null;
}

//  Returns all DOM elements of type "el_type" and of class containing the string "css" found within "container"
//  If none are found, returns an empty array
function get_all_elements(container,el_type,css) {
  var arr = container.getElementsByTagName(el_type);
  var els = [];

  for (var i = 0; i < arr.length; i++) {
    if (arr[i].className.indexOf(css) != -1) els.push(arr[i]);
  }

  return els;
}


/********************************************
  ToolBar & related menu functions
********************************************/
//  Passes the toolbar item to the Swatches object
//  Used to determine which button -- font or background -- has invoked the colour picker
function formatting_toolbar_mouseup(sender,args)
{
  if (!window.Swatches) init_swatches();
  window.Swatches.caller = args.get_item();
}

/* Set the individual item styles for the document style menu items */
function style_html(editor,dropdownitem) {
  var styleName = dropdownitem.get_value();
  var style = editor.Styles[styleName];
  var result = "";
  result += "<span style=\"float:left;";
  result += style.DropDownStyleString ? style.DropDownStyleString : "";
  result += "\">";
  result += DataItem.get_text();
  result += "</span>";
  return result;
}

/* Build the undo & redo menus */
function undo_html(menu) {
  var html = [];
  var editor = menu.ParentEditor;
  var count = editor.UndoStates.length;
  for (var i = 1; i <= count; i++) {
    html[html.length] = "<a href=\"javascript:void(0);\" onclick=\"";
    html[html.length] = editor.ClientControlId;
    html[html.length] = ".Undo(";
    html[html.length] = i;
    html[html.length] = ");";
    html[html.length] = menu.ClientControlId;
    html[html.length] = ".hide();\" onmouseover=\"undo_menu_over(this);\" ";
    html[html.length] = "onmouseout=\"undo_menu_out(this);\">";
    html[html.length] = editor.UndoStates[count - i].action;
    html[html.length] = "</a>";
  }
  return html.join("\n");
}

function redo_html(menu) {
  var html = [];
  var editor = menu.ParentEditor;
  var count = editor.RedoStates.length;
  for (var i = 1; i <= count; i++) {
    html[html.length] = "<a href=\"javascript:void(0);\" onclick=\"";
    html[html.length] = editor.ClientControlId;
    html[html.length] = ".Redo(";
    html[html.length] = i;
    html[html.length] = ");";
    html[html.length] = menu.ClientControlId;
    html[html.length] = ".hide();\" onmouseover=\"undo_menu_over(this);\" ";
    html[html.length] = "onmouseout=\"undo_menu_out(this);\">";
    html[html.length] = editor.RedoStates[count - i].action;
    html[html.length] = "</a>";
  }
  return html.join("\n");
}


function undo_menu_over(el) {
  var p = el.parentNode;
  var a = p.getElementsByTagName("a");

  var css = "selected";
  var c = null;
  var s = "";
  for (var i = 0; i < a.length; i++) {
    if (a[i] == el) {
      css = "";
      if (c == null) c = i;
    }
    a[i].className = css;
  }

  el.className = "hover";
}

function undo_menu_out(el) {
  var p = el.parentNode;
  var a = p.getElementsByTagName("a");

  for (var i = 0; i < a.length; i++) {
    if (a[i] == el) break;
    else a[i].className = "";
  }
}

// Bullets/Numbering Menus
function ListMenuItemHtml(ol_or_ul, cssClass, cssClassDepth)
{
  cssClassDepth -= 0; // just to make sure it's treated as a number
  var html = '<div class="bullets-item" onmouseover="this.className=\'bullets-item-hover\';" onmouseout="this.className=\'bullets-item\';">';
  for (var i=1; i<=cssClassDepth; i++)
  {
    html += '<' + ol_or_ul;
    if (i == 1) {html += ' class="' + cssClass + '"';} // only output the cssClass with the first OL tag
    html += '><li><span style="color:#CCCCCC">-------------</span></li>';
  }
  for (var i=1; i<=cssClassDepth; i++)
  {
    html += '</' + ol_or_ul + '>';
  }
  html += '</div>';
  return html;
}

function handleBulletsMenuItemSelect(sender, eventArgs)
{
  var selectedClass = eventArgs.get_item().getProperty("ListCssClass");
  var editor = sender.ParentEditor;
  if (!editor.getTopUnorderedList())
  {
    editor.toggleUnorderedList();
  }
  editor.setUnorderedListCssClass(selectedClass);
}

function handleNumberingMenuItemSelect(sender, eventArgs)
{
  var selectedClass = eventArgs.get_item().getProperty("ListCssClass");
  var editor = sender.ParentEditor;
  if (!editor.getTopOrderedList())
  {
    editor.toggleOrderedList();
  }
  editor.setOrderedListCssClass(selectedClass);
}


//  Insert Symbol functions
//  TODO: Remove getElementById() calls
function update_symbol_title(el) { document.getElementById("symbol-title").firstChild.innerHTML = el.getAttribute("title"); }
function reset_symbol_title() { document.getElementById("symbol-title").firstChild.innerHTML = "Insert Symbol"; }
function insert_symbol(menu, element) { var editor = menu.ParentEditor; ComponentArt_InsertDomElement(editor.element,editor.document.createTextNode(element.innerHTML)); menu.Hide(); }


//  Insert Table functions
//  TODO: Remove getElementById() calls
function insert_table_menu_show() {
  document.getElementById("table-title").firstChild.innerHTML = "Insert Table";

  if (!window.insert_table_dimensions) window.insert_table_dimensions = [1,1];

  document.getElementById("table-cells-events").onmousemove = track_table_cells;
  document.getElementById("table-cells-events").onmouseover = mouse_over_table_cells;
  document.getElementById("table-cells-events").onmouseout = mouse_out_table_cells;
}

function insert_table_menu_hide() { document.getElementById("table-title").firstChild.innerHTML = "Insert Table"; }

function mouse_over_table_cells(e) { window.table_cells_mouse_over = true;document.getElementById("table-cells-highlight").style.visibility = "visible"; }
function mouse_out_table_cells(e) { window.table_cells_mouse_over = false;document.getElementById("table-cells-highlight").style.visibility = "hidden";insert_table_menu_hide(); }

function track_table_cells(e) {
  if (window.table_cells_mouse_over) {
    var mouse_x = (document.all) ? event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - 2 : e.pageX;
    var mouse_y = (document.all) ? event.clientY + document.body.scrollTop + document.documentElement.scrollTop - 2 : e.pageY;

    var image = document.getElementById("table-cells-events");
    var image_x = get_position(image)[0];
    var image_y = get_position(image)[1];

    var highlight_w = (Math.floor((mouse_x - image_x) / 19) + 1) * 19;
    var highlight_h = (Math.floor((mouse_y - image_y) / 19) + 1) * 19;

    document.getElementById("table-title").firstChild.innerHTML = (Math.floor((mouse_x - image_x) / 19) + 1) + "x" + (Math.floor((mouse_y - image_y) / 19) + 1) + " Table";
    document.getElementById("table-cells-highlight").style.width = highlight_w + "px";
    document.getElementById("table-cells-highlight").style.height = highlight_h + "px";

    window.insert_table_dimensions[0] = (Math.floor((mouse_x - image_x) / 19) + 1);
    window.insert_table_dimensions[1] = (Math.floor((mouse_y - image_y) / 19) + 1);
  }
}

function insert_table(menu) {
  if (!window.insert_table_dimensions) window.insert_table_dimensions = [1,1];

  var editor = menu.ParentEditor;

  // Phil's insert table method
  var newTable = editor.document.createElement('table');
  newTable.border = '1';
  newTable.style.borderCollapse = 'collapse';
  newTable.style.width = '100%';
  var newTableBody = editor.document.createElement('tbody');

  var firstCell = null;
  for(var i=0;i<window.insert_table_dimensions[1];i++)
  {
    var newRow = editor.document.createElement('tr');
    for(var x=0;x<window.insert_table_dimensions[0];x++)
    {
      var newCell = editor.document.createElement('td');
      newCell.innerHTML = '&nbsp;';
      if(x == 0 && i == 0) firstCell = newCell;
      newRow.appendChild(newCell);
    }
    newTableBody.appendChild(newRow);
  }
  newTable.appendChild(newTableBody);
  ComponentArt_InsertDomElement(editor.element,newTable,firstCell);
  editor.Focus();

  menu.Hide();
}







/********************************************
  Common dialogue functions
********************************************/
//  Find the input by classname in the specified dialog
function get_dialog_input(dialog,css) {
  var dlg = document.getElementById(dialog.DomElementId);
  var input = dlg.getElementsByTagName("input");
  for (var i = 0; i < input.length; i++) {
    if (input[i].className.indexOf(css) != -1) { return input[i]; }
  }
}

//  Find the input by classname in the specified dialog
function get_dialog_anchor(dialog,css) {
  var dlg = document.getElementById(dialog.DomElementId);
  var a = dlg.getElementsByTagName("a");
  for (var i = 0; i < a.length; i++) {
    if (a[i].className.indexOf(css) != -1) { return a[i]; }
  }
}

function change_dialog_classname(css,dialog) {
  var dlg = document.getElementById(dialog.ClientControlId);
  var d = dlg.getElementsByTagName("div");
  var c,l,m,r;

  for (var i = 0; i < d.length; i++) {
    if (d[i].className == "con") {
      c = d[i].getElementsByTagName("div");
      for (var j = 0; j < c.length; j++) {
        if (c[j].className.indexOf("con-m") != -1) m = c[j];
        else if (c[j].className.indexOf("con-l") != -1) l = c[j];
        else if (c[j].className.indexOf("con-r") != -1) r = c[j];
      }
      break;
    }
  }

  m.className = "con-m " + css;
  l.className = "con-l " + css;
  r.className = "con-r " + css;

  //  Are we switching to the link page?
  if (css == "link-basic") {
    //  Disable the advanced fields
    var t = m.getElementsByTagName("input");
    for (var i =0; i < t.length; i++) {
      if (t[i].className == "link-tooltip" || t[i].className.indexOf("link-anchor") != -1 || t[i].className.indexOf("link-type") != -1 || t[i].className == "link-css") t[i].disabled = true;
    }

    //  Disable the Comboboxes
    window.LinkAnchor.disable();
    window.LinkCssClass.disable();
    window.LinkType.disable();

  } else if (css == "link-advanced") {
    //  Enable the advanced fields
    var t = m.getElementsByTagName("input");
    for (var i =0; i < t.length; i++) {
      if (t[i].className == "link-tooltip" || t[i].className.indexOf("link-anchor") != -1 || t[i].className == ("link-type") || t[i].className == "link-css" || t[i].className == "dlg-textfield") t[i].disabled = false;
    }

    //  Enable the comboboxes
    window.LinkAnchor.enable();
    window.LinkCssClass.enable();
    window.LinkType.enable();

    //  Enable the "Link Type - Other" textfield if need be
    if (window.LinkType.get_selectedIndex() == 0) {
      var t = get_dialog_input(dialog,"link-type-other");
      t.disabled = false;
      t.className = "link-type-other f-l";
    }
  }
}


function toggle_dialog_tab(el,dialog,page_index) {
  if (el.className != "tab") return false;
  else {
    toggle_radio(el,'tab-sel','tab');

    var t = el.getElementsByTagName("span")[0].innerHTML;

    switch(t) {
      //  Hyperlink dialogue
      case "Link":
        change_dialog_classname("link-basic",dialog);
        get_options_button(dialog).style.display = "block";
        get_options_button(dialog).firstChild.className = "advanced";
        get_options_button(dialog).firstChild.innerHTML = "Advanced";
        break;
      case "Anchor":
        change_dialog_classname("anchor",dialog);
        get_options_button(dialog).style.display = "none";
        break;
      case "Email":
        change_dialog_classname("email",dialog);
        get_options_button(dialog).style.display = "none";
        break;

      //  Media dialogue
      case "Image":
        change_dialog_classname("image",dialog);
        break;

      case "Flash":
        change_dialog_classname("flash",dialog);
        break;

      case "Video":
        change_dialog_classname("video",dialog);
        break;

      //  Find dialogue
      case "Find":
        change_dialog_classname("find",dialog);
        break;

      case "Replace":
        change_dialog_classname("replace",dialog);
        break;
    }

    var mp = window[dialog.ClientControlId.replace("Dialog","Pages")];
    mp.setPageIndex(page_index);
  }
}


function  open_dialog_page(dialog,page_index,show) {
  var dlg,tabs,tab,anchors;

  dlg = document.getElementById(dialog.ClientControlId);

  for (var i = 0; i < dlg.getElementsByTagName("div").length; i++) {
    if (dlg.getElementsByTagName("div")[i].className == "ts-m") { tabs = dlg.getElementsByTagName("div")[i].getElementsByTagName("a");break; }
  }

  var tab = tabs[page_index];
  toggle_dialog_tab(tab,dialog,page_index);

  if (show) dialog.Show();
}





/********************************************
  Hyperlink dialogue functions
********************************************/
function get_options_button(dialog) {
  var dlg = document.getElementById(dialog.ClientControlId);
  var d = dlg.getElementsByTagName("div");
  var f,btn;

  for (var i = 0; i < d.length; i++) {
    if (d[i].className == "ftr-m") {
      f = d[i].getElementsByTagName("a");
      for (var j = 0; j < f.length; j++) {
        if (f[j].className.indexOf("f-l") != -1) { btn = f[j];break; }
      }
      break;
    }
  }

  return btn;
}

function link_type_change(sender,args) {
  var p = document.getElementById(sender.DomElementId).parentNode.parentNode;
  var input = p.getElementsByTagName("input");
  var t;

  for (var i = 0; i < input.length; i++) {
    if (input[i].className.indexOf("link-type-other") != -1) { t = input[i];break; }
  }

  if (sender.get_selectedIndex() == 0) {
    if (t.disabled == true) { t.disabled = false;t.className = "link-type-other float-left";t.focus(); }
  } else if (t.disabled == false) { t.disabled = true;t.className = "link-type-other float-left disabled"; }
}


function toggle_hyperlink_options(el,dialog) {
  var t = el.firstChild.innerHTML.toLowerCase();

  switch(t) {
    case "advanced":
      el.firstChild.className = "basic";
      el.firstChild.innerHTML = "Basic";
      change_dialog_classname("link-advanced",dialog);
      break;

    case "basic":
      el.firstChild.className = "advanced";
      el.firstChild.innerHTML = "Advanced";
      change_dialog_classname("link-basic",dialog);
      break;
  }
}

function check_url(el) {
  var txt = el.previousSibling;
  if (txt.value) { window.open(txt.value,"check_url"); }
}

function hyperlinkdialog_reset(sender) {
  //  Link page
  get_dialog_input(sender,"link-url").value = "";
  get_dialog_input(sender,"link-text").value = "";
  get_dialog_input(sender,"link-target").value = "";
  get_dialog_input(sender,"link-tooltip").value = "";
  get_dialog_input(sender,"link-type-other").value = "";

  //  Anchor page
  get_dialog_input(sender,"anchor-name").value = "";
  get_dialog_input(sender,"anchor-text").value = "";

  //  Email page
  get_dialog_input(sender,"email-address").value = "";
  get_dialog_input(sender,"email-subject").value = "";
  get_dialog_input(sender,"email-text").value = "";
}

function hyperlinkdialog_show(sender,args) {
  hyperlinkdialog_reset(sender);

  var a = sender.ParentEditor.HasParent('A');
  var txt = (sender.ParentEditor.get_selectionText() == "") ? a.innerHTML : sender.ParentEditor.get_selectionText();


  if (a) {
    var url = (a.getAttribute("href") == null) ? "" : a.getAttribute("href");
    if (url.indexOf("://") != -1) {         //  Assume it's a standard link
      get_dialog_input(sender,"link-url").value = url;
      get_dialog_input(sender,"link-text").value = txt;
      get_dialog_input(sender,"link-target").value = a.getAttribute("target");
      open_dialog_page(sender,0);
    } else if (url.indexOf("mailto:") != -1) {    //  Assume it's an email link
      var mailto = url.replace("mailto:","")
      get_dialog_input(sender,"email-address").value = mailto.split("?subject=")[0];
      get_dialog_input(sender,"email-subject").value = mailto.split("?subject=")[1];
      get_dialog_input(sender,"email-text").value = txt;
      open_dialog_page(sender,2);
    } else {                    //  Assume it's an anchor
      get_dialog_input(sender,"anchor-name").value = a.getAttribute("name");
      get_dialog_input(sender,"anchor-text").value = txt;
      open_dialog_page(sender,1);
    }
  }
}

function hyperlinkdialog_close(sender,args) {
  var dialog = sender;
  if (dialog.get_result() != true) return;

  var editor = dialog.ParentEditor;

  // InsertHyperlink (url,text,target,cssclass,id,name,tooltip)
  // still needs advanced options
  var mp = window[dialog.ClientControlId.replace("Dialog","Pages")];

  switch (mp.getPageIndex()) {
    case 0:
      editor.InsertHyperlink(get_dialog_input(dialog,"link-url").value,get_dialog_input(dialog,"link-text").value);
      break;

    case 1:
      editor.InsertHyperlink("","",null,null,get_dialog_input(sender,"anchor-name").value,get_dialog_input(dialog,"anchor-name").value);
      break;

    case 2:
      var link = "mailto:" + get_dialog_input(dialog,"email-address").value + "?subject=" + get_dialog_input(dialog,"email-subject").value;
      editor.InsertHyperlink(link,get_dialog_input(dialog,"email-text").value);
      break;

    default:break;
  }
}



/********************************************
  Media dialogue functions
********************************************/
function mediadialog_show(sender,args) {  }

function mediadialog_close(sender,args) {
  var dialog = sender;
  if(dialog.get_result() != true) return;

  var editor = dialog.ParentEditor;

  // InsertImage (url,alt,width,height,alignment,border,hspace,vspace,cssclass,style)
  // still needs alignment, border, cssclass, style, hspace, vspace

  var mp = window[dialog.ClientControlId.replace("Dialog","Pages")];

  switch (mp.getPageIndex()) {
    case 'Image':
      editor.InsertImage(get_dialog_input(dialog,"image-url").value,get_dialog_input(dialog,"image-desc").value,get_dialog_input(dialog,"image-wide").value,get_dialog_input(dialog,"image-height").value);
      break;
  }
}

function load_image(el,dialog) {
  var url,img;

  //  Find the input field containing the URL
  for (var i = 0; i < el.parentNode.getElementsByTagName("input").length; i++) {
    if (el.parentNode.getElementsByTagName("input")[i].className == "image-url") { url = el.parentNode.getElementsByTagName("input")[i].value;break; }
  }

  //  Error: no URL
  if (!url) { alert("Error: The URL entered is invalid. Preview loading will not continue."); return false; }

  // Update the preview panel to indicate its loading status
  var dlg,p,s,t;
  dlg = el.parentNode.parentNode.parentNode;    //  Find the main 'container'

  //  Find both the preview DIV
  for (var i = 0; i < dlg.getElementsByTagName("div").length; i++) {
    if (dlg.getElementsByTagName("div")[i].className == "image-preview") { p = dlg.getElementsByTagName("div")[i];break; }
  }

  //  Error: No preview DIV
  if (!p) { alert("Error: Preview container cannot be found. Preview loading will not continue.");return false; }


  //  Find the status DIV
  for (var i = 0; i < p.getElementsByTagName("div").length; i++) {
    if (p.getElementsByTagName("div")[i].className.indexOf("status") != -1) { s = p.getElementsByTagName("div")[i];break; }
  }

  // Does the loader already exist? If so, remove it
  if (document.getElementById("image-loader")) { img = document.getElementById("image-loader");document.getElementsByTagName("body")[0].removeChild(img); }

  // Create the image-loading element
  img = document.createElement("img");
  img.id = "image-loader";
  img.style.position = "absolute";
  img.style.left = "-999999px";
  img.style.top = "-999999px";
  img.style.visibility = "hidden";
  var imgLoadHandler = new Function('get_image_dimensions(' + dialog.ClientControlId + ');');
  if (cart_browser_ie)
  {
    img.onresize = imgLoadHandler;
  }
  else
  {
    img.onload   = imgLoadHandler;
  }
  img.onerror = load_image_error;
  img.src = url;

  //  Append it to the body
  document.getElementsByTagName("body")[0].appendChild(img);

  //  Update the status to reflect the loading of the image
  if (s.className.indexOf("loading") == -1) { s.className += " loading";s.innerHTML = ""; }
}

function load_image_error(e) {
  var img = document.getElementById("image-loader");
  img.className = "error";
  //alert('error!');
}

function get_image_dimensions(dialog)
{
  var img = document.getElementById("image-loader");
  if (img.readyState == 'uninitialized')
  {
    return;
  }

  //  Get the dialog's main container
  var dlg = document.getElementById(dialog.ClientControlId);

  var p,s,t;
  //  Find the dialog's preview container
  for (var i = 0; i < dlg.getElementsByTagName("div").length; i++) {
    if (dlg.getElementsByTagName("div")[i].className == "image-preview") { p = dlg.getElementsByTagName("div")[i]; }
  }

  //  Error: Preview container not found
  if (!p) { alert("Error: Preview container cannot be found. Preview loading will not continue.");return false; }

  //  Find the status & thumbnail DIVs
  for (var i = 0; i < p.getElementsByTagName("div").length; i++) {
    if (p.getElementsByTagName("div")[i].className.indexOf("status") != -1) s = p.getElementsByTagName("div")[i];
    else if (p.getElementsByTagName("div")[i].className == "thumbnail") t = p.getElementsByTagName("div")[i];
  }

  //  Check if the thumbnail already contains an IMG node; if so, remove it
  var thumb;
  if (t.getElementsByTagName("img").length == 1) { thumb = t.getElementsByTagName("img")[0];t.removeChild(thumb); }

  //  Examine the newly-loaded image

  if (img.className != "error") {
    var w = img.offsetWidth;
    var h = img.offsetHeight;
    var ratio = w / h;

    //  Set the value of the width & height fields
    for (var i = 0; i < dlg.getElementsByTagName("input").length; i++) {
      if (dlg.getElementsByTagName("input")[i].className.indexOf("image-width") != -1) dlg.getElementsByTagName("input")[i].value = String(w);
      else if (dlg.getElementsByTagName("input")[i].className.indexOf("image-height") != -1) dlg.getElementsByTagName("input")[i].value = String(h);
    }

    //  Create the thumbnail IMG node
    thumb = document.createElement("img");
    thumb.src = img.src;
    thumb.style.position = "absolute";

    //thumb.setAttribute("alt","Preview of " + thumb.src);
    //thumb.setAttribute("title","Preview of " + thumb.src);

    //  Resize, position and append the thumbnail IMG
    var pw,ph;
    if (w > t.offsetWidth || h > t.offsetHeight) {
      if (w > h) {
        //  landscape
        pw = t.offsetWidth;
        ph = Math.floor(pw / ratio);
        thumb.style.left = "0";
        thumb.style.top = Math.floor((t.offsetHeight - ph) * 0.5) + "px";
      } else if (w < h) {
        //  portrait
        ph = t.offsetHeight;
        pw = Math.floor(ratio * ph);
        thumb.style.left = Math.floor((t.offsetWidth - pw) * 0.5) + "px";
        thumb.style.top = "0";
      } else {
        //  square
        pw = ph = container.offsetHeight;
        thumb.style.left = Math.floor((t.offsetWidth - pw) * 0.5) + "px";
        thumb.style.top = "0";
      }

      thumb.style.width = pw + "px";
      thumb.style.height = ph + "px";
    } else {
      thumb.style.left = Math.floor((t.offsetWidth - w) * 0.5) + "px";
      thumb.style.top = Math.floor((t.offsetHeight - h) * 0.5) + "px";
    }

    t.appendChild(thumb);

    if (w == 0 || h == 0) { s.innerHTML = "<span class='error'>Error: Failed to load image</span>"; }
    //else { s.innerHTML = "Preview"; }

  } else {
    //  An error occurred!
    s.innerHTML = "<span class='error'>Error: Failed to load image</span>";
  }

  //  Set the status
  s.className = "status";

//  //  Remove the image loader
  document.getElementsByTagName("body")[0].removeChild(img);
}



/********************************************
  Find dialogue functions
********************************************/
function finddialog_show(dialog) {
  window.findIndex = 0;
  window.replaceIndex = 0;
}

function finddialog_close(dialog) {
  window.findIndex = 0;
  window.replaceIndex = 0;
}

function FindDialog_Find() {
  var dialog = FindDialog;
  var editor = FindDialog.ParentEditor;


  var result = editor.Find(get_dialog_input(dialog,"find-text").value,(get_dialog_anchor(dialog,"find-match-case").className.indexOf("-sel") > -1) ? false : true,(get_dialog_anchor(dialog,"find-match-word").className.indexOf("-sel") > -1) ? true : false,window.findIndex);

  if (!result) { window.findIndex = 0;alert("Find is complete!"); }
  else window.findIndex++;
}

function FindDialog_FindReplace() {
  var dialog = FindDialog;
  var editor = FindDialog.ParentEditor;
  var result = editor.Find(get_dialog_input(dialog,"replace-find-text").value,(get_dialog_anchor(dialog,"replace-match-case").className.indexOf("-sel") > -1) ? false : true,(get_dialog_anchor(dialog,"replace-match-word").className.indexOf("-sel") > -1) ? true : false,window.replaceIndex);

  if (!result) { window.replaceIndex = 0;alert("Find is complete!"); }
  else window.replaceIndex++;
}

function FindDialog_Replace() {
  var dialog = FindDialog;
  var editor = FindDialog.ParentEditor;
  var index = (window.replaceIndex > 0) ? window.replaceIndex - 1 : 0;
  var result = editor.Replace(get_dialog_input(dialog,"replace-find-text").value,(get_dialog_anchor(dialog,"replace-match-case").className.indexOf("-sel") > -1) ? false : true,(get_dialog_anchor(dialog,"replace-match-word").className.indexOf("-sel") > -1) ? true : false,get_dialog_input(dialog,"replace-text").value,index);

  if (!result) { window.replaceIndex = 0;alert("Replace is complete!"); }
  else window.replaceIndex--;
}

function FindDialog_ReplaceAll() {
  var dialog = FindDialog;
  var editor = FindDialog.ParentEditor;
  var result = editor.ReplaceAll(get_dialog_input(dialog,"replace-find-text").value,(get_dialog_anchor(dialog,"replace-match-case").className.indexOf("-sel") > -1) ? false : true,(get_dialog_anchor(dialog,"replace-match-word").className.indexOf("-sel") > -1) ? true : false,get_dialog_input(dialog,"replace-text").value);
  alert("Replace is complete!");
}












/********************************************
  Colour menu functions
********************************************/
// The Swatches object
window.Swatches_Collection = function() {
  //  Since we're using one picker for both foreground and background colors, we'll need to identify
  //  which item has invoked the color-picker dropdown menu
  this.caller = null;

  //  Swatch menu 'bounding box'
  //  These are the min_x, min_y, max_x, and max_y coords used for the 'snapping' of the swatch highlight frame
  this.bounds = [5,28,158,187];

  //  Swatch Y coordinates relative to the top of the menu
  this.preset_top = 28;
  this.standard_top = 143;
  this.custom_top = 187;

  //  Custom offsets (for borders, padding, etc.)
  this.offset_x = 0;
  this.offset_y = 0;

  //  Swatch dimensions
  this.width = 13;
  this.height = 13;

  //  Selected & highlighted swatches
  //  These are pre-loaded with a default color; in this case, rust-orange.
  this.selected = new Swatch(5,187,"#ff5010",true);
  this.highlighted = new Swatch(5,187,"#ff5010",true);

  this.foreground = this.selected;
  this.background = new Swatch(56,143,"#ffff00",true);

  //  Create the preset swatches array
  this.preset = [
    new Swatch(5,28,"#ffffff"),
    new Swatch(22,28,"#000000"),
    new Swatch(39,28,"#eeece1"),
    new Swatch(56,28,"#1f497d"),
    new Swatch(73,28,"#4f81bd"),
    new Swatch(90,28,"#c0504d"),
    new Swatch(107,28,"#9bbb59"),
    new Swatch(124,28,"#8064a2"),
    new Swatch(141,28,"#4bacc6"),
    new Swatch(158,28,"#f79646"),
    new Swatch(5,46,"#f2f2f2"),
    new Swatch(22,46,"#7f7f7f"),
    new Swatch(39,46,"#ddd9c3"),
    new Swatch(56,46,"#c6d9f0"),
    new Swatch(73,46,"#dbe5f1"),
    new Swatch(90,46,"#f2dcdb"),
    new Swatch(107,46,"#ebf1dd"),
    new Swatch(124,46,"#e5e0ec"),
    new Swatch(141,46,"#dbeef3"),
    new Swatch(158,46,"#fdeada"),
    new Swatch(5,59,"#d8d8d8"),
    new Swatch(22,59,"#595959"),
    new Swatch(39,59,"#c4bd97"),
    new Swatch(56,59,"#8db3e2"),
    new Swatch(73,59,"#b8cce4"),
    new Swatch(90,59,"#e5b9b7"),
    new Swatch(107,59,"#d7e3bc"),
    new Swatch(124,59,"#ccc1d9"),
    new Swatch(141,59,"#b7dde8"),
    new Swatch(158,59,"#fbd5b5"),
    new Swatch(5,72,"#bfbfbf"),
    new Swatch(22,72,"#3f3f3f"),
    new Swatch(39,72,"#938953"),
    new Swatch(56,72,"#548dd4"),
    new Swatch(73,72,"#95b3d7"),
    new Swatch(90,72,"#d99694"),
    new Swatch(107,72,"#c3d69b"),
    new Swatch(124,72,"#b2a2c7"),
    new Swatch(141,72,"#92cddc"),
    new Swatch(158,72,"#fac08f"),
    new Swatch(5,85,"#a5a5a5"),
    new Swatch(22,85,"#262626"),
    new Swatch(39,85,"#494429"),
    new Swatch(56,85,"#17365d"),
    new Swatch(73,85,"#366092"),
    new Swatch(90,85,"#953734"),
    new Swatch(107,85,"#76923c"),
    new Swatch(124,85,"#5f497a"),
    new Swatch(141,85,"#31859b"),
    new Swatch(158,85,"#e36c09"),
    new Swatch(5,98,"#7f7f7f"),
    new Swatch(22,98,"#0c0c0c"),
    new Swatch(39,98,"#1d1b10"),
    new Swatch(56,98,"#0f243e"),
    new Swatch(73,98,"#244061"),
    new Swatch(90,98,"#632423"),
    new Swatch(107,98,"#4f6128"),
    new Swatch(124,98,"#3f3151"),
    new Swatch(141,98,"#205867"),
    new Swatch(158,98,"#974806")
  ];

  //  Create the standard swatches array
  this.standard = [
    new Swatch(5,143,"#c00000"),
    new Swatch(22,143,"#ff0000"),
    new Swatch(39,143,"#ffc000"),
    new Swatch(56,143,"#ffff00"),
    new Swatch(73,143,"#92d050"),
    new Swatch(90,143,"#00b050"),
    new Swatch(107,143,"#00b0f0"),
    new Swatch(124,143,"#0070c0"),
    new Swatch(141,143,"#002060"),
    new Swatch(158,143,"#7030a0")
  ];

  //  Create the custom swatches array
  this.custom = [
    new Swatch(5,187,"#ff5010",true),
    new Swatch(22,187,"",true),
    new Swatch(39,187,"",true),
    new Swatch(56,187,"",true),
    new Swatch(73,187,"",true),
    new Swatch(90,187,"",true),
    new Swatch(107,187,"",true),
    new Swatch(124,187,"",true),
    new Swatch(141,187,"",true),
    new Swatch(158,187,"",true)
  ];
}

window.Swatch = function(coord_x,coord_y,swatch_color,custom_swatch) {
  this.X = coord_x;
  this.Y = coord_y;
  this.Color = swatch_color;
  this.Custom = (custom_swatch) ? true : false;
}

function init_swatches() { window.Swatches = new Swatches_Collection(); }

function color_menu_show(sender,arg) {
  if (!window.Swatches) init_swatches();

  //  'Hide' the selected frame
  document.getElementById("color-selected").style.backgroundPosition = "-1000px -1000px";
  // Draw the selected frame, if a colour is indeed selected, after 200 ms to prevent ghosting
  setTimeout("show_swatch_selected();",200);

  //  Due to the nature of the client template, the custom swatches are redrawn everytime the
  //  context menu is shown. As such, the swatches lose any custom colours they've been assigned.
  //  This repopulates the custom swatches with the 'correct' colours values
  for (var i = 0; i < window.Swatches.custom.length; i++) {
    if (window.Swatches.custom[i].Color != "") {
      var swatch = document.getElementById("custom-" + String(i));
      swatch.style.backgroundColor = window.Swatches.custom[i].Color;
      swatch.style.backgroundImage = "none";
    }
  }
}

function show_swatch_selected()
{
  if (window.Swatches.caller)
  {
    switch (window.Swatches.caller.getProperty('EditorCommandType'))
    {
      case 'FormatFontColor':
        document.getElementById("color-selected").style.backgroundPosition = (window.Swatches.foreground.X + window.Swatches.offset_x) + "px " + (window.Swatches.foreground.Y + window.Swatches.offset_y) + "px";
      break;
      case 'FormatHighlight':
        document.getElementById("color-selected").style.backgroundPosition = (window.Swatches.background.X + window.Swatches.offset_x) + "px " + (window.Swatches.background.Y + window.Swatches.offset_y) + "px";
      break;
    }
  }
}

function color_menu_hide(sender,arg) { }

function color_menu_select(sender,eventArgs) {
  if (eventArgs.get_item().get_id() != "AddCustomColor") return;
  open_color_picker(sender.ParentEditor);
}

function get_swatch_coordinates(swatch,pos_x,pos_y) {
  var arr = [];

  for (var i = 0; i < swatch.length; i++) {
    if (pos_x >= (swatch[i].X + window.Swatches.offset_x) && pos_x <= (swatch[i].X + window.Swatches.offset_x) + window.Swatches.width && pos_y >= (swatch[i].Y + window.Swatches.offset_y) && pos_y <= (swatch[i].Y + window.Swatches.offset_y) + window.Swatches.height) {
      arr[0] = swatch[i].X + window.Swatches.offset_x;
      arr[1] = swatch[i].Y + window.Swatches.offset_y;
      window.Swatches.highlighted = swatch[i];
    }
  }

  return arr;
}

function move_swatch_highlight(el,e) {
  var el_x = get_position(el)[0];
  var el_y = get_position(el)[1];

  if (!e) e = window.event;
  mouse_x = e.clientX;
  mouse_y = e.clientY;

  // If the page has been scrolled, account for the scroll offsets
  var scroll_x = document.documentElement.scrollLeft;
  var scroll_y = document.documentElement.scrollTop;

  var pos_x = (mouse_x - el_x) + window.Swatches.offset_x + scroll_x;
  var pos_y = (mouse_y - el_y) + window.Swatches.offset_y + scroll_y;

  var min_x = window.Swatches.bounds[0] + window.Swatches.offset_x;
  var min_y = window.Swatches.bounds[1] + window.Swatches.offset_x;
  var max_x = window.Swatches.bounds[2] + window.Swatches.offset_y;
  var max_y = window.Swatches.bounds[3] + window.Swatches.offset_y;

  if (pos_x < min_x) pos_x = min_x;
  else if (pos_x > max_x) pos_x = max_x;

  if (pos_y < min_y) pos_y = min_y;
  else if (pos_y > max_y) pos_y = max_y;

  if (pos_y >= (window.Swatches.preset_top + window.Swatches.offset_y) && pos_y < (window.Swatches.standard_top  + window.Swatches.offset_y)) var highlight = get_swatch_coordinates(window.Swatches.preset,pos_x,pos_y);
  else if (pos_y >= (window.Swatches.standard_top  + window.Swatches.offset_y) && pos_y < (window.Swatches.custom_top + window.Swatches.offset_y)) var highlight = get_swatch_coordinates(window.Swatches.standard,pos_x,pos_y);
  else if (pos_y >= (window.Swatches.custom_top + window.Swatches.offset_y) && pos_y <= max_y) var highlight = get_swatch_coordinates(window.Swatches.custom,pos_x,pos_y);

  if (highlight.length == 2) el.style.backgroundPosition = String(highlight[0]) + "px " + String(highlight[1]) + "px";
}


function select_swatch(element,menu)
{
  if (window.Swatches.highlighted.Color != "")
  {
    window.Swatches.selected = window.Swatches.highlighted;
    if (window.Swatches.caller)
    {
      switch (window.Swatches.caller.getProperty('EditorCommandType'))
      {
        case 'FormatFontColor':
          window.Swatches.foreground = window.Swatches.selected;
        break;
        case 'FormatHighlight':
          window.Swatches.background = window.Swatches.selected;
        break;
      }
    }
  }
  change_color(menu.ParentEditor);
  menu.hide();
}

function change_color(editor)
{
  if (!window.Swatches) { init_swatches(); }

  //  There's a colour assigned to this swatch
  window.Swatches.selected = window.Swatches.highlighted;

  if (window.Swatches.caller)
  {
    switch (window.Swatches.caller.getProperty('EditorCommandType'))
    {
      case 'FormatFontColor':
        get_toolbar_item_cell(editor,"font-color").style.backgroundColor = window.Swatches.foreground.Color;
        editor.setStyle('color',window.Swatches.foreground.Color);
      break;
      case 'FormatHighlight':
        get_toolbar_item_cell(editor,"bg-color").style.backgroundColor = window.Swatches.background.Color;
        editor.setStyle('background-color',window.Swatches.background.Color);
      break;
    }
  }
}


function get_empty_swatch() {
  var empty_swatch = 0;
  for (var i = 0; i < window.Swatches.custom.length; i++) {
    if (window.Swatches.custom[i].Color == "") { empty_swatch = i;break; }
  }

  return empty_swatch;
}


function open_color_picker(editor)
{
  var empty_swatch = get_empty_swatch();
  var color_swatch = "";

  color_swatch = window.prompt ('Please enter a hexidecimal color value:', '#000000');

  //  TODO: Check the validity of the hex value
  window.Swatches.custom[empty_swatch].Color = color_swatch;
  window.Swatches.selected = window.Swatches.custom[empty_swatch];

  if (window.Swatches.caller)
  {
    switch (window.Swatches.caller.getProperty('EditorCommandType'))
    {
      case 'FormatFontColor':
        window.Swatches.foreground = window.Swatches.custom[empty_swatch];
      break;
      case 'FormatHighlight':
        window.Swatches.background = window.Swatches.custom[empty_swatch];
      break;
    }
  }

  change_color(editor);
}


//  This returns the <td> containing a toolbar item's image
//  Use primarily to modify the background colours of the foreground and background colour toolbar items.
function get_toolbar_item_cell(editor,css) {
  var id = editor.ClientControlId;
  var p = document.getElementById(editor.ClientControlId + "_Placeholder");

  var td = p.getElementsByTagName("td");
  for (var i = 0; i < td.length; i++) {
    if (td[i].className.indexOf(css) != -1) {
      var inner_td = td[i].getElementsByTagName("td");
      for (var j = 0; j < inner_td.length; j++) {
        if (inner_td[j].className.indexOf("ca_tb_img") != -1) {
          return inner_td[j].getElementsByTagName("img")[0];
        }
      }

      break;
    }
  }
}









/********************************************
  SpellCheck functions
********************************************/
function spellcheck_handler() {
  if (Spell1.DialogBegin()) {
  // show the dialog
    SpellCheckDialog.Show();
  }
}

function spellcheck_complete() {
  alert("Spell check is complete!");
  SpellCheckDialog.Close();
}

function spellcheck_update() {
  // update html
  var domObj = document.getElementById("SpellCheckDialog_Html");
  if (domObj) {
    domObj.innerHTML = Spell1.DialogGetErrorsHtml();

    var currentError = document.getElementById('Spell1_CurrentError');
    if(currentError && currentError.scrollIntoView) {
        currentError.scrollIntoView();
    }
  }

  // update suggestions
  Spell1.DialogGetSuggestions(spellcheck_suggest);
}

function spellcheck_suggest(result) {
  var suggestionSelect = document.getElementById('SpellCheckDialog_Suggestions');
  if(suggestionSelect) {
    suggestionSelect.innerHTML = '';

    for (var i = 0; i < result.length; i++) {
      var newOpt = document.createElement('a');
      newOpt.innerHTML = result[i];
      newOpt.id = 'SpellCheckDialog_Word_' + i;
      newOpt.onclick = new Function('wordlistSelect(' + i + ')');
      newOpt.href = "javascript:void(0);";

      suggestionSelect.appendChild(newOpt);
    }
  }

  wordlistSelect(0);
}

function wordlistSelect(num) {
  var el = document.getElementById('SpellCheckDialog_Word_' + num);
  var word = '';

  if(el) {
    word = el.innerHTML;
    if (window.selected_word != null) document.getElementById(window.selected_word).className = "";

    el.className = "selected";
    window.selected_word = el.id;
  }

  document.getElementById("SpellCheckDialog_Replacement").value = word;
}

