<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.Data" CodeBehind="Data.aspx.vb" %><?xml version="1.0" encoding="utf-8" ?>
<data transition="CrossFadeTransition">
  <album title="Album" description="Description" image="images/s23.jpg">
    <asp:Repeater id="assetList" runat="server">
    <ItemTemplate>
    <slide title="name: <%#server.HTMLEncode(replace(Container.DataItem("name"),"""",""))%>" description="description: <%#server.HTMLEncode(replace(replace(Container.DataItem("description"),VbCrLf,""),"""",""))%>" image="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?instance=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMInstance%>&amp;type=asset&amp;size=1&amp;width=<%=bWidth%>&amp;height=<%=bHeight%>&amp;id=<%#Container.DataItem("asset_id")%>&amp;page=<%#Container.DataItem("page")%>" thumbnail="<%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?instance=<%=WebArchives.iDAM.Web.Core.IDAMWebSession.Config.IDAMInstance%>&amp;type=asset&amp;size=1&amp;width=320&amp;height=240&amp;id=<%#Container.DataItem("asset_id")%>&amp;page=<%#Container.DataItem("page")%>" />
    </ItemTemplate>
    </asp:Repeater>
  </album>
</data>
