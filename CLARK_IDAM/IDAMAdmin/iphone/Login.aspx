<%@ Reference Page="~/Login.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.LoginIPhone" CodeBehind="Login.aspx.vb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
         "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<HTML>
	<HEAD>
		<title>iDAM</title><meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
		<style type="text/css" media="screen">@import "iui/iui.css"; 
	</style>
		<script type="application/x-javascript" src="iui/iui.js"></script>
	</HEAD>
	
	
	<body>
	
<style type="text/css">

body > ul > li {
    font-size: 14px;
}

li .assetlist {
    padding-left: 50px;
    padding-right: 40px;
    min-height: 34px;
}

li .thumbnail {
    display: block;
    position: absolute;
    margin: 0;
    left: 6px;
    top: 6px;
    text-align: center;
    font-size: 110%;
    letter-spacing: -0.07em;
    color: #93883F;
    font-weight: bold;
    text-decoration: none;
    width: 30px;
    height: 30px;
    padding: 7px 0 0 0;
    background: url(shade-compact.gif) no-repeat;
}

li .icon {
    display: block;
    position: absolute;
    margin: 0;
    left: 15px;
    top: 4px;
    text-align: center;
    font-size: 110%;
    letter-spacing: -0.07em;
    color: #93883F;
    font-weight: bold;
    text-decoration: none;
    width: 20px;
    height: 20px;
    padding: 7px 0 0 0;
    background: url(shade-compact.gif) no-repeat;
}

h2 {
    margin: 10px;
    color: slateblue;
}

p {
    margin: 10px;
}
</style>
<script language="javascript">
function checkRememberMe(myDiv)
			{
			
			if (myDiv) {
			var myID = myDiv.getAttribute('id');
			var myState = myDiv.getAttribute('toggled');
			if (myID) {
				var myCheckbox = document.getElementById(myID.replace('-toggle', ''));
				if (myCheckbox) {
					if (myCheckbox.getAttribute('type') == 'hidden') {
						if (myState == 'true')
							myCheckbox.value = "on";
						else
							myCheckbox.value = "off";
					}
				}
			}
		}
			alert(document.getElementById('toggleRememberMe').checked);
	document.getElementById('RememberMe').checked = true; 
				 
			}

</script>
	
		<div class="toolbar">
			<h1 id="pageTitle"></h1>
			<a id="backButton" class="button" href="#"></a><a id="search" class="button" href="#searchForm">
				Search</a>
		</div>
		<form title="Login" id="Form1" class="panel" method="post" action="login.aspx" selected="true">
			<h2>Login To iDAM</h2>
			<fieldset>
				<div class="row">
					<label>Login</label>
					<input type="text" name="Login" ID="Text1" value="<%=_username%>">
				</div>
				<div class="row">
					<label>Password</label>
					<input type="password" name="Password" ID="Text2" value="<%=_password%>">
				</div>
				<div class="row">
				<label>Instance</label>
					<div id="RememberMe-toggle" >
					<select name="configId" ID="configId">
						<%For Each cfg As IdamDAO.DAO.IdamConfiguration In instancesList%>
						<option value="<%=cfg.ConfigId%>"><%=cfg.ConfigName%></option>
						<%next%>
					</select>
					</div>
				</div>
				<div class="row">
					<label>Remember Me</label>
					<div id="RememberMe-toggle" class="toggle" onclick="javascript:checkRememberMe(this)" toggled="true"><br>
					 <span class="thumb"></span><span class="toggleOn">ON</span><span class="toggleOff">OFF</span></div>
 
				</div>
			</fieldset>
			<span style="visible:false">
			<input type="hidden" name="RememberMe" ID="RememberMe" value="on"></span>
			<input type="submit"  value="Loginsubmit" name="Loginsubmit"/>
			
			
		</form>
		
		
	</body>
</HTML>
