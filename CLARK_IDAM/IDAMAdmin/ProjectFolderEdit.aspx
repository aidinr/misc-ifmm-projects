<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.ProjectFolderEdit" ValidateRequest="False" CodeBehind="ProjectFolderEdit.aspx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Quick Create/Edit Category</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
			<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
			<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
				<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
					<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
						<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
							<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
								<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
									<link href="treeStyle.css" type="text/css" rel="stylesheet">
										<link href="navStyle.css" type="text/css" rel="stylesheet" >
										    <link href="tabStripStyle.css" type="text/css" rel="stylesheet" />    
												<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
										<style>BODY { MARGIN: 0px }
	</style>
	<body onload="this.focus">
		<form id="Form" method="post" runat="server">
		
		
		
		<script language=javascript>

  


  function onInsertCategory(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }

  function onUpdateCategory(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }


  


</script>
		
<script type="text/javascript">
  
  
  
  <%if not WebArchives.iDAM.Web.Core.IDAMFunctions.getRole("CREATE_NEW_PROJECT") then%>
	alert ('You do not have permissions to add a project folder.  Please contact your system administrator to add this capability.');
	window.parent.CloseDialogWindowX();
<%end if%>

  
  

<%if request.querystring("newid") <> "" then%>
window.parent.NavigateToProjectFolder('<%response.write (request.querystring("pid"))%>','<%response.write (request.querystring("newid"))%>');
window.parent.CloseDialogWindowX();
<%end if%>

<%if instr(request.querystring("id"),"ACT") = 0  then%>
<%if request.querystring("parent_id") = ""  then%>
window.parent.CloseDialogWindowX();
<%end if%>
<%end if%>

</script>

			<%response.write (request.querystring("newid"))%>
			<input type="hidden" name="catid" id="catid" value="<%response.write (replace(request.querystring("id"),"CAT",""))%>">
			<input type="hidden" name="parent_id" id="parent_id" value="<%response.write (request.querystring("parent_id"))%>">
			<div style="padding:20px;position:relative;">
			<div style="width:100%">
			<div style="padding:10px;border:1px solid;background-color:white;position:relative;">
			<table><tr><td><img src="images/categorytype0.gif"></td><td>Create/Edit Project Folder</td></tr></table><br>
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Create/Modify a project folder by entering a folder name and optional description.  </div><br>
  <div style="width=100%">

			
	<COMPONENTART:TABSTRIP id="TabStripCategory" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataProjectFolder.xml"
		CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
		DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
		<ItemLooks>
			<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10" 
 LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif" 
 HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23" 
 LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
			<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10" 
 LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif" 
 LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
		</ItemLooks>
	</COMPONENTART:TABSTRIP>
	
	<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageCategoryPage" runat="server">
		<ComponentArt:PageView CssClass="PageContent2" runat="server" ID="Pageview1">
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Enter a name, description and security level for this project folder.</div>
			<br>
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">Name </font>
					</td>
					<td colspan="2" align="left" valign="top" width=100%>
						<asp:TextBox id="CategoryName" runat="server" CssClass="InputFieldMain"  Height=25px></asp:TextBox></td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">Security Level</font></td>
					<td class="PageContent"  width=100% align="left" valign="top">
						<asp:DropDownList ID="SecurityLevel" CssClass="InputFieldMain"  Height=25px Runat="server">
							<asp:ListItem Value="0">Administrator</asp:ListItem>
							<asp:ListItem Value="1">Internal Use</asp:ListItem>
							<asp:ListItem Value="2">Client Use</asp:ListItem>
							<asp:ListItem Value="3">Public</asp:ListItem>
						</asp:DropDownList>
					</td>
					<td class="PageContent" width="80%" align="left" valign="top">
						<input type="checkbox" name="chkactive" checked value="ON">Active</td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">Description</font></td>
					<td class="PageContent" colspan="2" align="left" valign="top"  width=100%>
						<asp:TextBox id="CategoryDescription" TextMode="MultiLine" runat="server" CssClass="InputFieldMain100P"  Style="height:200px" Height="200px" ></asp:TextBox></td>
				</tr>
			</table>
		</ComponentArt:PageView>
		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Category_Members">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Change permissions for this object by selecting a group or user from the available list and adding to the active list.</div><br>






								
								
								
						
																				
		
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td class="PageContent" width="360" valign=top>
<!--available list-->
<b>Available Groups/Users</b><br><br>



<COMPONENTART:GRID id="GridPermissionAvailable" runat="server" pagerposition="2" ScrollBar="Off" Sort="lastname asc" Height="10" Width="250" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategory" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" AllowPaging="true" cachecontent="false" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategory">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategory">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                            
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategory" InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataCellClientTemplateId="TypeIconTemplateCategory" DataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Firstname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="firstname" Width="70"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Lastname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="lastname" Width="100"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" DataCellCssClass="LastDataCell" AllowEditing="false" AllowGrouping="False" Width="120" SortedDataCellCssClass="SortedDataCell" DataField="description"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<div style=  "text-align:right;BORDER-RIGHT: 1px solid; PADDING-RIGHT: 4px; BORDER-TOP: 1px solid; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: 1px solid; PADDING-TOP: 4px; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: gainsboro">
<b>Search for user/group:</b> <input Class="InputFieldMain" type=text value="" id=search_filter  style="width:120px" onkeyup="javascript:<%response.write( GridPermissionAvailable.ClientId)%>.Filter('firstname LIKE \'%' + this.value + '%\' or lastname LIKE \'%' + this.value + '%\'');">
</div>
					</td>
					<td class="PageContent" width="120" valign= middle>

		<asp:ImageButton ImageUrl="images/bt_u_fwd.gif" Runat=server id="imgbtnAddPermissionsnew"></asp:ImageButton><br><br>
		<asp:ImageButton ImageUrl="images/bt_u_rwd.gif" Runat=server id="imgbtnRemovePermissions"></asp:ImageButton> 
					
					</td>
					<td class="PageContent" width="100%" valign=top>
					
					
					
					
<!--active list-->
<b>Active Groups/Users</b><br><br>


<COMPONENTART:GRID id="GridPermissionActive" runat="server" autoPostBackonDelete="true" PageSize="10" pagerposition="2" ScrollBar="Off" Sort="typeofobject asc" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategoryActive" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="false" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" AllowPaging="true" cachecontent="false" PagerStyle="Numbered"  >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategoryActive">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateCategoryActive">
          <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategoryActive">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                                
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategoryActive" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataField="imagesource" DataCellClientTemplateId="TypeIconTemplateCategoryActive" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True"  />
<componentart:GridColumn AllowEditing="false" HeadingText="Name" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" FixedWidth="false"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Description" DataCellCssClass="LastDataCell" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="description" width="100" FixedWidth="true"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<br>
<asp:Button Runat=server ID=btnApplyPermissions Text="Apply permissions to all descendants"></asp:Button>
<br><asp:CheckBox Runat=server Checked ID=chkapplyoverrides></asp:CheckBox>Include overrides<br>
<asp:CheckBox Runat=server Checked ID="chkapplytofolders"></asp:CheckBox>Include folders
<div id="applypermissionsspinner" style="display:none;  font-family: tahoma, verdana;font-size: 9px;font-weight: bold;"><br><table><tr valign=top ><td><IMG SRC="images/spinner.gif"></td><td style="font-family: tahoma, verdana;font-size: 9px;font-weight: bold;"> Applying permissions...  Please note this can take a couple minutes to complete.</td></tr></table> </div>
					</td>					
				</tr>
			</table>													
							




















		</ComponentArt:PageView>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Role_Override">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Change permissions for this object by selecting a group or user from the available list and adding to the active list.</div><br>
															
		
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td class="PageContent" width="360" valign=top>
<!--available list-->
<b>Active Groups/Users</b><br>
<img src="images/spacer.gif" width="300" height="1"><br>




<COMPONENTART:GRID id="GridRoleActivePermissions" AllowMultipleSelect="False" ClientSideOnSelect="AJAXRefreshRoles" runat="server" pagerposition="2" ScrollBar="Off" Sort="name asc" Height="100" Width="300" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateRoleOActive" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" AllowPaging="true" cachecontent="false" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateRoleOActive">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateroleOActive">
          <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TypeIconTemplateRoleOActive">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                                
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategory" InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataField="imagesource" DataCellClientTemplateId="TypeIconTemplateRoleOActive" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True"  />
<componentart:GridColumn AllowEditing="false" HeadingText="Name" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" FixedWidth="false"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="Description" DataCellCssClass="LastDataCell" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="description" width="100" FixedWidth="true"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="True" DataCellCssClass="LastDataCell" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  width="1" FixedWidth="true"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<div style=  "text-align:right;BORDER-RIGHT: 1px solid; PADDING-RIGHT: 4px; BORDER-TOP: 1px solid; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: 1px solid; PADDING-TOP: 4px; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: gainsboro">
<b>Search for user/group:</b> <input Class="InputFieldMain" type=text value="" id=search_filter  style="width:120px" onkeyup="javascript:<%response.write( GridRoleActivePermissions.ClientId)%>.Filter('name LIKE \'%' + this.value + '%\'');">
</div>
					</td>
					<td class="PageContent" width="120" valign= middle>


					
					</td>
					<td class="PageContent" width="100%" valign=top>
					
					
					
					
<!--active list-->
<b>Role Overrides</b><br><br>










<script language=javascript>
function AJAXRefreshRoles(item)
  {
	var itemvaluetmp;
	itemvaluetmp = item.GetMember('uniqueid').Value;
	<%=TreeView_RolesCallback.ClientID%>.Callback(itemvaluetmp);
	return true;
  }  
  


function Object_PopUp(strUrl, popupName, intHeight, intWidth)
{


	if (strUrl == null || strUrl.Length <= 0)
		return;

	var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=no,resizable=yes,toolbar=no";
	//var strFeatures = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;
	//var strFeatures += "'"
	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures , false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
}


function Object_PopUp_Roles()
{
Object_PopUp('RolesEdit.aspx','Edit_Role',700,700);

}

function changerole()
{
if (confirm('Are you sure you want to change this users role?  This will erase any previous role overrides as well.')) {
__doPostBack('Roles','');
}
}

</script>
<table width=100%><tr>
<td width=100% style="FONT-SIZE: 11px; ">Role List</td>
<td width=200px align=right><asp:Button   id="roleOverrides" runat="server" Text="Clear Role Overrides"></asp:Button></td>
</tr></table>	


<COMPONENTART:CALLBACK id="TreeView_RolesCallback" runat="server" CacheContent="false">
			<CONTENT>
				<ComponentArt:TreeView id="TreeView_Roles" Height="280" Width="420px" 
				AutoPostBackOnNodeCheckChanged="true"
				DragAndDropEnabled="false" 
				NodeEditingEnabled="false" 
				KeyboardEnabled="true" 
				CssClass="TreeView" 
				NodeCssClass="TreeNode" 
				SelectedNodeCssClass="SelectedTreeNode" 
				HoverNodeCssClass="HoverTreeNode" 
				NodeEditCssClass="NodeEdit" 
				LineImageWidth="19" 
				LineImageHeight="20" 
				DefaultImageWidth="16" DefaultImageHeight="16" ItemSpacing="0" NodeLabelPadding="3" 
				ParentNodeImageUrl="images/folders.gif" LeafNodeImageUrl="images/folder.gif" ShowLines="true" 
				LineImagesFolderUrl="images/lines/" 
				EnableViewState="true"  
				runat="server" >
				</ComponentArt:TreeView>
			</CONTENT>
	<LOADINGPANELCLIENTTEMPLATE>
		<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
			<TR>
				<TD align="center">
					<TABLE cellSpacing="0" cellPadding="0" border="0">
						<TR>
							<TD style="FONT-SIZE: 10px">Processing Download Request...
							</TD>
							<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
	</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>
		
									









					</td>					
				</tr>
			</table>													
		</ComponentArt:PageView>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	</ComponentArt:MultiPage>
	<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
					<div style="text-align:right;"><br>
			<asp:Button id="btnCategorySave" runat="server" cssclass="button" Text="Save"></asp:Button><img src="images/spacer.gif" width=5 height=1><input type="button" cssclass="button" onclick="<%if request.querystring("ID") <> "" then response.write ("window.parent.NavigateToProjectFolder('" & sProjectID & "','" & replace(request.querystring("id"),"ACT","") & "')") %>;window.parent.CloseDialogWindowX();" value="Close" />
			</div>
	</div>
</div>

			
			
			
			

			</div>
			
			
			
			
			
			
			
			
			</div>
			

			
			</div>
			</div>
			
			
		<script language=javascript >
TabStripCategory.SelectTabById('<%response.write (request.querystring("tab"))%>');

function confirmApplyPermissions()
{
if (confirm('Are you sure you want to overwrite the permissions for all descendants?'))
	{
		document.getElementById("applypermissionsspinner").style.display="block";
		return true;
	}else{
		return false;
	}
}

</script>	
				
			
			
		</form>
	</body>
</HTML>
