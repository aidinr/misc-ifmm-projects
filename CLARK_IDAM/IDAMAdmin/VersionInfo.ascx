<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.VersionInfo" CodeBehind="VersionInfo.ascx.vb" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<body>
	<div class="previewpaneMyProjects ui-accordion-header ui-helper-reset ui-state-active " id="toolbarheading" style="z-index:99;border-top:0px;border-bottom:0px;">
		<!--Project cookie and top menu-->
		<table cellspacing="0" id="table2" width="100%">
			<tr>
				<td>
					<div style="PADDING-RIGHT:5px;PADDING-LEFT:5px;FLOAT:left;PADDING-BOTTOM:5px;PADDING-TOP:5px"><img src="images/idampref_ID_ico.gif"></div>
					<div style="PADDING-RIGHT:5px;PADDING-LEFT:5px;FLOAT:left;PADDING-BOTTOM:5px;PADDING-TOP:5px">
						<font face="Verdana" size="1"><b>iDAM Updater</b><br>
							<span style="FONT-WEIGHT: 400">Update iDAM Components:  Please contact WebArchives <a  href="mailto:support@webarchives.com">support</a> before updating any component.<br></span>
						</font>
					</div>
				</td>
				<td id="Test" valign="top" align="right">
				</td>
			</tr>
			<tr>
				<td valign="top" colspan="2">
				</td>
			</tr>
		</table>
	</div> <!--end preview pane-->
		
		
		<div style="PADDING-RIGHT:10px;PADDING-LEFT:10px;PADDING-BOTTOM:10px;PADDING-TOP:10px;BACKGROUND-COLOR:white;border:1px solid;">
			<br>
			<asp:label id="versionLabel" runat="server"></asp:label>
			<p></p>
			<asp:Literal ID="CheckVersionLiteral" Runat="server"></asp:Literal>
<br><br><br>
			<COMPONENTART:CALLBACK id="VersionCheckCallback" runat="server" CacheContent="false">
				<CONTENT>
					
											<asp:Literal ID="VersionTextLiteral" Runat="server"></asp:Literal><br>
											
				</CONTENT>
				<LOADINGPANELCLIENTTEMPLATE>
					<TABLE height="100" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD align="center">
								<TABLE cellSpacing="0" cellPadding="0" border="0">
									<TR>
										<TD style="FONT-SIZE: 10px">Checking For Versions.. Please Wait.
										</TD>
										<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
									</TR>
								</TABLE>
							</TD>
						</TR>
					</TABLE>
				</LOADINGPANELCLIENTTEMPLATE>
			</COMPONENTART:CALLBACK>
			
			<COMPONENTART:CALLBACK id="UpdateCallBack" runat="server" CacheContent="false">
				<CONTENT>
					<TABLE height="100" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD align="center">
								<TABLE cellSpacing="0" cellPadding="0" border="0">
									<TR>
										<TD style="FONT-SIZE: 10px"><br>
											<asp:Literal ID="LiteralUpdateMessage" Runat="server"></asp:Literal><br>
											<br>
										</TD>
									</TR>
								</TABLE>
							</TD>
						</TR>
					</TABLE>
				</CONTENT>
				<LOADINGPANELCLIENTTEMPLATE>
					<TABLE height="100" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD align="center">
								<TABLE cellSpacing="0" cellPadding="0" border="0">
									<TR>
										<TD style="FONT-SIZE: 10px">Updating component.. Please Wait.
										</TD>
										<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
									</TR>
								</TABLE>
							</TD>
						</TR>
					</TABLE>
				</LOADINGPANELCLIENTTEMPLATE>
			</COMPONENTART:CALLBACK>
			Click <a href="javascript:InstallSearchPlugin();">Here</a> to install Search 
			Plugin (This requires either Firefox or IE).
		</div>
	</div>
	<script language="javascript">
		function CheckVersion()
  {  
  <% Response.Write(VersionCheckCallback.ClientID) %>.Callback();
  }	
  function UpdateVersion(type)
  {  
  <% Response.Write(UpdateCallBack.ClientID) %>.Callback(type);
  }
  function InstallSearchPlugin() {
  window.external.AddSearchProvider(location.href.replace('version_info.aspx','searchplugin.aspx') );
  }
	</script>
</body>
