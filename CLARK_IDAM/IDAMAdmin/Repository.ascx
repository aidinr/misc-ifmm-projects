﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Repository.ascx.vb" Inherits="IdamAdmin.RepositoryNew" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<div class="previewpaneMyProjects ui-accordion-header ui-helper-reset ui-state-active " id="toolbarheading" style="z-index:99;border-top:0px;border-bottom:0px;">
    <!--Project cookie and top menu-->
    <table cellspacing="0" id="table2" width="100%">
        <tr>
            <td width="1" nowrap>
                <img src="images/ui/home_ico_bg.gif" width="42" height="42">
            </td>
            <td valign="top">
                <div style="float: left; padding: 5px;">
                    <font face="Verdana" size="1"><b>Repository Information</b><br>
                        <span style="font-weight: 400">Modify repository information (Note: Please have an IDAM administrator make any changes).<br>
                        </span></font>
                </div>
            </td>
            <td id="Test" valign="top" align="right">
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
            </td>
        </tr>
    </table>
</div>

<div id="subtabset" style="position:relative;top:-4px;z-index:2; font-size:10px;">
<input type="radio" id="radio1" name="subtab" checked="checked" /><label for="radio1">General</label><img src="images/spacer.gif" width="2" alt="" />
</div>
<div style="border-top:1px solid silver;position:relative;top:-25px;z-index:1;"></div>

<script>
    $(function () {
        $("#subtab").button();
        $("#subtabset").buttonset();
    });
	</script>
<!--end preview pane-->
<div class="previewpaneProjects"  id="toolbar2" style="padding:10px;">

    <script language="javascript">
  
        // Forces the grid to adjust to the new size of its container          
      function resizeGrid2(DomElementId, NewPaneHeight, NewPaneWidth)
      {
        <% Response.Write(GridRepository.ClientID) %>.Render();
        //alert('here');
      } 
  
    function editGridPopup(rowId)
  {
	var rowdata;
	var repo_id;
	rowdata = <% Response.Write(GridRepository.ClientID) %>.GetRowFromClientId(rowId);
	repo_id = rowdata.GetMember('repository_id').Value;
    Object_PopUp_Repository(repo_id); 
  }
  
function editRow(item)
  {

 <% Response.Write(GridRepository.ClientID) %>.EditComplete();  

  }

  function onInsert(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
    function ShowContextMenu(item, column, evt) 
  {
    <% Response.Write(GridRepository.ClientID) %>.Select(item); 
   
    return false; 
  }
  
  function onUpdate(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridRepository.ClientID) %>.Page(1); 
  }

  function onDelete(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
 
  function insertRow()
  {
    <% Response.Write(GridRepository.ClientID) %>.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    <% Response.Write(GridRepository.ClientID) %>.Delete(<% Response.Write(GridRepository.ClientID) %>.GetRowFromClientId(rowId)); 
  }



  function onInsertCategory(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  

  
  function onUpdateCategory(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }



  function onDeleteCategory(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
  

  
  
    function ThumbnailCallBackPage(page)
  {
    <% Response.Write(ThumbnailCallBack.ClientID) %>.Callback(page);
  }
  

  
  

    </script>

    <div  id="browsecenter">
        <!--<div style="padding:10px;border-left:1px #969698 solid; border-right:1px #969698 solid;border-bottom:1px #969698 solid;border-top:1px #969698 solid;">-->
        <div class="SnapProjectsWrapperx">
            <div class="SnapProjectsAssetsx">
                <div style="padding-top: 0px;">
                    <div style="padding: 3px;">
                    </div>
                </div>
                [ <a href="javascript:Object_PopUp_RepositoryNew();">create new</a> ]<br>
                <div style="padding-top: 10px;">
                    <div style="padding: 3px;">
                    </div>
                </div>
                <componentart:Grid ID="GridRepository" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" PagerPosition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ClientSideOnInsert="onInsert" ClientSideOnUpdate="onUpdate" ClientSideOnDelete="onDelete" ClientSideOnCallbackError="onCallbackError" ScrollPopupClientTemplateId="ScrollPopupTemplate" Sort="repository_id asc" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplate" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="20" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="True" GroupingPageSize="5" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true">
                    <ClientTemplates>
                        <componentart:ClientTemplate ID="LoadingFeedbackTemplate">
                            <div>
                                <table cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td style="font-size: 10px; font-family: Verdana;">
                                            <img src="images/spacer.gif" height="25" width="1"><br>
                                            <img src="images/spinner.gif" width="16" height="16" border="0">
                                            Loading...
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </componentart:ClientTemplate>
                        <componentart:ClientTemplate ID="EditTemplate">
                            <a href="javascript:editGridPopup('## DataItem.ClientId ##');">
                                <img src="images/repository16edit.gif" alt="Edit" border="0">
                            </a>| <a href="javascript:deleteRow('## DataItem.ClientId ##')">
                                <img src="images/2DeletedItems.gif" alt="Delete" border="0"></a>
                        </componentart:ClientTemplate>
                        <componentart:ClientTemplate ID="EditCommandTemplate">
                            <a href="javascript:editRow('## DataItem.ClientId ##');">Update</a>
                        </componentart:ClientTemplate>
                        <componentart:ClientTemplate ID="InsertCommandTemplate">
                            <a href="javascript:insertRow();">Insert</a>
                        </componentart:ClientTemplate>
                        <componentart:ClientTemplate ID="TypeIconTemplate">
                            <img src="images/repository16b.jpg" border="0">
                        </componentart:ClientTemplate>
                        <componentart:ClientTemplate ID="TypeIconTemplateGoto">
                            <img src="images/repository16edit.gif" border="0">
                        </componentart:ClientTemplate>
                    </ClientTemplates>
                    <Levels>
                        <componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="repository_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
                            <Columns>
                                <componentart:GridColumn Align="Center" AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
                                <componentart:GridColumn AllowEditing="False" HeadingText="ID" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="repository_id"></componentart:GridColumn>
                                <componentart:GridColumn AllowEditing="True" HeadingText="Name" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name"></componentart:GridColumn>
                                <componentart:GridColumn HeadingText="Description" AllowEditing="true" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="description"></componentart:GridColumn>
                                <componentart:GridColumn HeadingText="Updated" AllowEditing="false" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="modifieddate"></componentart:GridColumn>
                                <componentart:GridColumn HeadingText="Created By" AllowEditing="false" SortedDataCellCssClass="SortedDataCell" DataField="userid" ForeignTable="Users" ForeignDataKeyField="userid" ForeignDisplayField="FullName"></componentart:GridColumn>
                                <componentart:GridColumn HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Align="Center" DataCellCssClass="LastDataCellPostings" />
                                <componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="repository_id" SortedDataCellCssClass="SortedDataCell" DataField="repository_id"></componentart:GridColumn>
                            </Columns>
                        </componentart:GridLevel>
                    </Levels>
                </componentart:Grid>
                <componentart:CallBack ID="ThumbnailCallBack" runat="server" CacheContent="true">
                    <Content>
                        <!--hidden Thumbs-->
                        <asp:Literal ID="ltrlPager" Text="" runat="server" />
                        <asp:Repeater ID="Thumbnails" runat="server" Visible="False">
                            <ItemTemplate>
                                <div valign="Top" style="float: left!important; width: 220px!important; width: 180px; height: 220px!important; height: 220px;">
                                    <!--Project Thumbnails-->
                                    <div style='width: 170px; height: 220px;'>
                                        <div align="left">
                                            <div align="left">
                                                <table class="bluelightlight" border="0" cellspacing="0" width="15">
                                                    <tr>
                                                        <td class="bluelightlight" valign="top">
                                                            <div align="left" id="projecthighlight_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" style="border: 1px white solid;">
                                                                <table border="0" width="100%" background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse">
                                                                    <tr>
                                                                        <td>
                                                                            <table border="0" width="100%" background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse" height="120" width="160">
                                                                                <tr class="bluelightlight" width="100%" align="center" height="120" width="160">
                                                                                    <td class="bluelightlight" width="100%" height="120" width="160" align="center" valign="bottom" bordercolor="#808080" bordercolorlight="#808080" nowrap>
                                                                                        <img src="images\spacer.gif" height="0" width="159"><br>
                                                                                        <a id="25257" href="IDAM.aspx?page=Project&Id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=project">
                                                                                            <img alt="" border="0" src="
  <%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&instance=<%#DataBinder.Eval(Container.DataItem, "instance")%>&type=project&size=2&height=120&width=160"></a>
                                                                                    </td>
                                                                                    <td>
                                                                                        <img src="images\spacer.gif" width="0" height="140">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div style="padding: 5px;">
                                                                <table width="100%" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td nowrap width="5">
                                                                            <img src="images/projcat16.jpg">
                                                                        </td>
                                                                        <td nowrap>
                                                                            <div class="asset_name">
                                                                                <span class="nowrap"><b><font face="Verdana" size="1">
                                                                                    <%#DataBinder.Eval(Container.DataItem, "name")%></font></b></span></div>
                                                                        </td>
                                                                        <td align="right" width="20">
                                                                            <font color="#808080" face="Arial" size="1">
                                                                                <input onclick="highlightProjectToggle(this,<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);" class="preferences" type="checkbox" name="pselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" value="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"></font>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table width="100%" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td nowrap>
                                                                            <font face="Verdana" size="1">Updated</font>
                                                                        </td>
                                                                        <td align="left" width="868">
                                                                            <div class="asset_date">
                                                                                <span class="nowrap"><font face="Verdana" size="1">
                                                                                    <%# String.Format("{0:MM/dd/yy hh:mm tt}", (DataBinder.Eval(Container.DataItem, "update_date")))%></font></span></div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <div style="padding-top: 3px;">
                                                                    <div class="asset_links">
                                                                    </div>
                                                                </div>
                                                                <b><font face="Verdana" size="1"><a id="25257" href="IDAM.aspx?page=Project&Id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=project">View Details</a></font><br>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <img border="0" src="images/spacer.gif" width="135" height="8">
                                        </div>
                                    </div>
                                    <!--end Project Thumbnail span-->
                                </div>
                                <!--end inner span-->
                            </ItemTemplate>
                            <HeaderTemplate>
                                <!--Start Thumbnail Float-->
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <div valign="Bottom" style="float: left!important;">
                            </HeaderTemplate>
                            <FooterTemplate>
                                </td></tr></table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:Literal ID="ltrlPagingBottom" Text="" runat="server" />
            </div>
            <!--End padding 10px Main window frame-->
            <!--hidden Thumbs-->
            </CONTENT>
            <loadingpanelclienttemplate>
												<TABLE height="300" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px">Loading...
																	</TD>
																	<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</loadingpanelclienttemplate>
            </COMPONENTART:CALLBACK>
        </div>
        <!--SnapProjects-->
    </div>
    <!--padding-->

    <script type="text/javascript">
  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

  
  function BrowseOnDoubleClick(SelectedGridRow)
  {
  
  	var itemvaluetmp;
	itemvaluetmp = SelectedGridRow.GetMember('uniqueid').Value;
	if (itemvaluetmp.substring(0,1) == 'P') 
	{
		itemvaluetmp = itemvaluetmp.replace('P_','');
		window.location.href = 'IDAM.aspx?page=Project&id=' + itemvaluetmp + '&type=project'
    
	} else {
		itemvaluetmp = itemvaluetmp.replace('C_','');
		window.location.href = 'IDAM.aspx?page=Browse&id=' + itemvaluetmp + '&type=category';
	}
	
	return true;
  
  
  
	
  }
  
    function highlightProjectToggle(checkbox,projectid)
  {
  var highlightbox;
  highlightbox = document.getElementById('projecthighlight_' + projectid);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  

    </script>

</div>
<!--END browsecenter-->
</div><!--END MAIN-->
<div style="padding-left: 10px;">
    <div style="padding: 3px;">
    </div>
</div>
