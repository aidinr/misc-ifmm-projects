<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.ClientNameLookup" CodeBehind="ClientNameLookup.ascx.vb" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<script language="javascript">
var clientlookupclickval;
clientlookupclickval = 1;
function OnItemSelectClientName(item)
{
if (item.ID == 'clientnamelookup') {

}

}
</script>
<div onclick="ClientNameLookup();">
<componentart:Menu id="MenuClientNameLookup" AutoPostBackOnSelect="False" runat="server" ExpandOnClick="true" ClientSideOnItemSelect="OnItemSelectClientName" >
<ItemLooks>
<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="ItemFilters" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
<ComponentArt:ItemLook LookID="BoldItemLook" CssClass="Itembold" HoverCssClass="ItemHbold" ExpandedCssClass="ItemExpbold" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
<ComponentArt:ItemLook LookID="ScrollUpItemLook" ImageUrl="scroll_up.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
<ComponentArt:ItemLook LookID="ScrollDownItemLook" ImageUrl="scroll_down.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
</ItemLooks></componentart:Menu>
</div>