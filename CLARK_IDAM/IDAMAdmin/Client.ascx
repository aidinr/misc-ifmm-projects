<%@ Reference Control="~/PreviewPane.ascx" %>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.Client" CodeBehind="Client.ascx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<div class="previewpaneMyProjects ui-accordion-header ui-helper-reset ui-state-active " id="toolbarheading" style="z-index:99;border-top:0px;border-bottom:0px;">
	<!--Project cookie and top menu-->
		<table cellspacing="0"  id="table2" width="100%">
			<tr>
			<td width=1 nowrap >
			<img  style="padding-top:5px;" src="images/ui/users_ico_bg.gif" height=42 width=42 >
			</td><td valign=top>
			<div style="float:left;padding:5px;">
						<font face="Verdana" size="1"><b>Client Information</b><br>
				<span style="font-weight: 400">Modify/add Client information.<br></span>
				
				</font>
			</div>
			</td>
			<td id="Test" valign="top" align="right">
			
			</td></tr>
			<tr><td valign="top" colspan="2">
			</td>
			</tr>
		</table>
</div> <!--end preview pane-->
<div id="subtabset" style="position:relative;top:-4px;z-index:2; font-size:10px;">
<input type="radio" id="radio1" name="subtab" checked="checked" /><label for="radio1">General</label><img src="images/spacer.gif" width="2" alt="" />
</div>
<div style="border-top:1px solid silver;position:relative;top:-25px;z-index:1;"></div>

<script>
    $(function () {
        $("#subtab").button();
        $("#subtabset").buttonset();
    });
	</script>
 
<script language=javascript>
  
        // Forces the grid to adjust to the new size of its container          
      function resizeGrid2(DomElementId, NewPaneHeight, NewPaneWidth)
      {
        <% Response.Write(GridClient.ClientID) %>.Render();
        //alert('here');
      } 
  
    function editGridPopup(rowId)
  {
	var rowdata;
	var userid;
	rowdata = <% Response.Write(GridClient.ClientID) %>.GetRowFromClientId(rowId);
	userid = rowdata.GetMember('clientid').Value;
    Object_PopUp_Client(userid); 
  }
    function editRow(item)
  {

 <% Response.Write(GridClient.ClientID) %>.EditComplete();  

  }
    
  function onInsert(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
    function ShowContextMenu(item, column, evt) 
  {
    <% Response.Write(GridClient.ClientID) %>.Select(item); 
   
    return false; 
  }
  
  function onUpdate(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridClient.ClientID) %>.Page(1); 
  }

  function onDelete(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
 
  function insertRow()
  {
    <% Response.Write(GridClient.ClientID) %>.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    <% Response.Write(GridClient.ClientID) %>.Delete(<% Response.Write(GridClient.ClientID) %>.GetRowFromClientId(rowId)); 
  }



  function onInsertCategory(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  

  
  function onUpdateCategory(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }



  function onDeleteCategory(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
  

  
  
    function ThumbnailCallBackPage(page)
  {
    <% Response.Write(ThumbnailCallBack.ClientID) %>.Callback(page);
  }
  

  
  

</script>

<div class=previewpaneProjects id=toolbar   style="padding:10px;">
      
      
      
      
      	<table width="100%">
		<tr>
			<td align="left" valign="top" width="100%"><div style="PADDING-TOP:0px"></div>
			</td>
			<td align="right" valign="top" width="300" nowrap>
				<table>
					<tr>
						<td id="Test"></td>
						<td>
							<div style="MARGIN-TOP: 1px"></div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
	</table>
      
      



<div  id="browsecenter">



<img src="images/spacer.gif"  height="15" width="5"><br>
<!--<div style="padding:10px;border-left:1px #969698 solid; border-right:1px #969698 solid;border-bottom:1px #969698 solid;border-top:1px #969698 solid;">-->





<table cellpadding=0 cellspacing=0 ><tr><td width="220" nowrap  >[ <a href="javascript:Object_PopUp_ClientNew();">create client</a> ]</td><td align=right width=100% nowrap ><b><font size=1>Search Clients:</font></b><img src="images/spacer.gif" width=5 height=1></td><td align=right width=50 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:70px"  onkeyup="javascript:DoSearchFilter(this.value);"></td></tr></table> 

<div style="padding-top:10px;"><div style="padding:3px;"></div></div>







<COMPONENTART:GRID 
id="GridClient" 
runat="server" 
AutoFocusSearchBox="false"
AutoCallBackOnInsert="true"
AutoCallBackOnUpdate="true"
AutoCallBackOnDelete="true"
pagerposition="2"
ScrollBar="Off"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ClientSideOnInsert="onInsert"
ClientSideOnUpdate="onUpdate"
ClientSideOnDelete="onDelete"
ClientSideOnCallbackError="onCallbackError"
ScrollPopupClientTemplateId="ScrollPopupTemplate" 
Sort="name asc"
Height="10" Width="100%"
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
EnableViewState="true"
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRowCategory" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="20" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="True" 
GroupingPageSize="5" 
PagerTextCssClass="GridFooterTextCategory" 
PagerStyle="Numbered" 
PageSize="25" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeader" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="true" 
AllowSorting="False"
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
ScrollBarWidth="15" 
AllowPaging="true">
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
<table height="1px" width="100%" ><tr><td valign="center" align="center">
<table cellspacing="0" cellpadding="0" border="0"  background="images/loadingback.gif" height="70px" width="300px">
<tr>
	<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
	<td style="font-size:10px;font-family:Verdana;" width="60px" align="right">Loading...&nbsp;</td>
	<td><img src="images/spinner2.gif"  border="0"></td>
	<td style="font-size:10px;font-family:Verdana;" width="80px" >&nbsp;</td>
</tr>
</table>
</td></tr></table>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplate">
            <a href="javascript:editGridPopup('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border="0" > </a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplate">
            <img src="images/user16.jpg" border="0" > 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateGoto">
            <img src="images/i_rate.gif" border="0" > 
          </ComponentArt:ClientTemplate>                                      
</ClientTemplates>
<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
EditFieldCssClass="EditDataField"
EditCommandClientTemplateId="EditCommandTemplate"
InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="clientid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asch10.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desch10.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="10" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="True" width="120" HeadingText="Name"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name"  ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="True" width="120"  HeadingText="Short Name"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="shortname" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="True" width="120"  HeadingText="Email"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="email" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="True" width="80"  HeadingText="Phone"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="phone" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Modified Date" width="80"  AllowEditing="false" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Created By" width="80"  AllowEditing="false" SortedDataCellCssClass="SortedDataCell"  DataField="createdby" ForeignTable="Users" ForeignDataKeyField="userid" ForeignDisplayField="FullName"></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" width="80"  AllowEditing="false" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand"  Align="Center" DataCellCssClass="LastDataCellPostings" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="clientid" SortedDataCellCssClass="SortedDataCell" DataField="clientid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>






















<COMPONENTART:CALLBACK id="ThumbnailCallBack" runat="server" CacheContent="true">
<CONTENT>











<!--hidden Thumbs-->

 <asp:Literal id="ltrlPager" Text="" runat="server" />

<asp:Repeater id=Thumbnails runat="server" Visible="False">
<ItemTemplate>



<div valign="Top" style="float:left!important; width:220px!important; width:180px; height:220px!important; height:220px;">
<!--Project Thumbnails-->
<div style='width:170px;height:220px;'>
        <div align="left">
                    <div align="left">
                      <table  class="bluelightlight" border="0" cellspacing="0" width="15">
                        <tr>
                          <td  class="bluelightlight" valign="top">
                          <div align="left" id="projecthighlight_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" style="border:1px white solid;">
                          <table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse"><tr><td>
                            <table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse" height="120" width="160">
                              <tr class="bluelightlight" width="100%" align="center" height="120" width="160"><td class="bluelightlight" width="100%" height="120" width="160" align="center" valign="bottom" bordercolor="#808080" bordercolorlight="#808080" nowrap><img src="images\spacer.gif" height="0" width="159"><br><a id="25257" href="IDAM.aspx?page=Project&Id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=project"><img alt="" border="0" src="
  <%=WebArchives.iDAM.Web.Core.IDAMWebSession.BrowseService.GetRetrieveAssetURLBase%>?id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&instance=<%#DataBinder.Eval(Container.DataItem, "instance")%>&type=project&size=2&height=120&width=160"></a></td><td><img src="images\spacer.gif" width="0" height="140"></td>

                              </tr>
                            </table></td></tr></table>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td >      
                          
                          
                          
                          
                          
                          
<div style="padding:5px;">             
<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
<td nowrap width="5"><img src="images/projcat16.jpg"></td>
	<td nowrap><div class="asset_name"><span class="nowrap"><b><font face="Verdana" size="1"><%#DataBinder.Eval(Container.DataItem, "name")%></font></b></span></div></td>
	<td align="right" width="20"><font color="#808080" face="Arial" size="1"><input onclick="highlightProjectToggle(this,<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);" class="preferences" type="checkbox" name="pselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" value="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"></font></td>
</tr>
</table>

<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
	<td nowrap><font face="Verdana" size="1">Updated</font></td>
	<td align="left" width="868"><div class="asset_date"><span class="nowrap"><font face="Verdana" size="1">

<%# String.Format("{0:MM/dd/yy hh:mm tt}", (DataBinder.Eval(Container.DataItem, "update_date")))%></font></span></div></td>

</tr>
</table>
<div style="padding-top:3px;">
<div class="asset_links"></div></div>
<b><font face="Verdana" size="1"><a id="25257" href="IDAM.aspx?page=Project&Id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=project">View Details</a></font><br>       
</div>                        
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
							</td>
                        </tr>
                      </table>
                    </div>
                    
                   <img border="0" src="images/spacer.gif" width="135" height="8">
                   </div>
                   </div><!--end Project Thumbnail span-->

                   </div><!--end inner span-->









</ItemTemplate>
<HeaderTemplate>
<!--Start Thumbnail Float-->
<table width=100%><tr><td>
<div valign="Bottom" style="float:left!important;">       
</HeaderTemplate>
<FooterTemplate>
</td></tr></table>
</FooterTemplate> 
</asp:Repeater>

<asp:Literal id="ltrlPagingBottom" Text="" runat="server" />
</div><!--End padding 10px Main window frame-->
<!--hidden Thumbs-->



















											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<TABLE height="300" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px">Loading...
																	</TD>
																	<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>









      <script type=text/javascript>
  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

  
  function BrowseOnDoubleClick(SelectedGridRow)
  {
  
  	var itemvaluetmp;
	itemvaluetmp = SelectedGridRow.GetMember('uniqueid').Value;
	if (itemvaluetmp.substring(0,1) == 'P') 
	{
		itemvaluetmp = itemvaluetmp.replace('P_','');
		window.location.href = 'IDAM.aspx?page=Project&id=' + itemvaluetmp + '&type=project'
    
	} else {
		itemvaluetmp = itemvaluetmp.replace('C_','');
		window.location.href = 'IDAM.aspx?page=Browse&id=' + itemvaluetmp + '&type=category';
	}
	
	return true;
  
  
  
	
  }
  
    function highlightProjectToggle(checkbox,projectid)
  {
  var highlightbox;
  highlightbox = document.getElementById('projecthighlight_' + projectid);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  
  
  
  


function DoSearchFilter(searchvalue)
{

<%response.write (GridClient.ClientID)%>.Filter("name LIKE '%" + searchvalue + "%' OR shortname LIKE '%" + searchvalue + "%' OR email LIKE '%" + searchvalue + "%'");
<%response.write (GridClient.ClientID)%>.Render();

}

  </script>



      
      
      
      </div><!--END browsecenter-->
      </div><!--END MAIN-->
      
      
      
      
      
      
      
      
<div style="padding-left:10px;"><div style="padding:3px;"></div></div>