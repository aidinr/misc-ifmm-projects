	<!--#include file="connection.asp" -->
<%
	orderID = prep_sql(request.querystring("oid"))
	
	if(orderID <> "") then
	
	set rsOrder=server.createobject("adodb.recordset")
	sql="select * from ipm_order where orderID = " & orderID
	rsOrder.Open sql, Conn, 1, 4

	set rsOrderSummary=server.createobject("adodb.recordset")
	sql="select b.name Format, a.quantity Quantity, price FeePerUnit, additionalcost AdditionalFee, c.item_value Worksheet from ipm_downloadtype b, ipm_order_details a LEFT JOIN (select t.asset_id, t.item_value from ipm_asset_field_value t, ipm_asset_field_desc u where t.item_id = u.item_id and u.item_tag = 'IDAM_WORKSHEET') c ON a.asset_id = c.asset_id where a.download_id = b.download_id and orderid = " & orderID & " order by c.item_value"
	rsOrderSummary.Open sql, Conn, 1, 4

	set rsUserFeesSource = server.createobject("adodb.recordset")
	sql = "select distinct sourcename,itemname  from ipm_order_details where orderid = " & orderID & "order by sourcename"
	rsUserFeesSource.Open sql, Conn, 1, 4	
	
	set rsUserFees=server.createobject("adodb.recordset")
	'sql="select a.item_value source,c.item_value worksheet,e.price price FROM ipm_asset_field_value a, ipm_asset_field_desc b, ipm_asset_field_value c, ipm_asset_field_desc d, ipm_order_details e WHERE e.orderid = " & orderID & " and a.item_id = b.item_id and b.item_tag = 'IDAM_SOURCE' and a.asset_id = e.asset_id and c.item_id = d.item_id and d.item_tag = 'IDAM_WORKSHEET' and c.asset_id = e.asset_id"
	'rsUserFees.Open sql, Conn, 1, 4		
%>	
<html>
<head>
<title>INVOICE - ITEMIZED REPRODUCTION FEES AND USER FEES</title>
<style type="text/css">
	body {
	}td {
		font-family: serif;
		font-size:11px;
	}
	hr.single {
		border: 0;
		border-top: 1px solid black;
		height: 1px;
		width: 100%;
		color: black;
		background-color: black;
	}
	hr.double {
		border: 0;
		border-top: 4px double black;
		height: 4px;
		width: 100%;
		color: white;
		background-color: white;
	}	
</style>
<style media="print">
	.print {
		display: none;
	}
</style>
</head>
<body>
	<table align="center" cellpadding="3" cellspacing="3" width="100%">
		<tr>
			<td align="center"><b><a href="javascript:window.print();" class="print">PRINT</a>&nbsp; &nbsp;  <a href="emailpage.aspx" class="print">EMAIL</a></b></td>
		</tr>
	</table>
	<table align="center" cellpadding="3" cellspacing="3" width="100%">
		<tr>
			<td><big><b>INVOICE</b></big></td>
			<td align="right"><b>INVOICE#:</b> <%=orderID%></td>
		</tr>
	</table>
	<table align="center" cellpadding="3" cellspacing="3">
		<tr>
			<td>
					<b>United States Holocaust Memorial Museum<br />
					PHOTO ARCHIVES<br />
					100 RAOUL WALLENBERG PLACE, S.W.<br />
					WASHINGTON DC &nbsp;&nbsp; 20024-2126<br />
					FAX:(202) 479-9726</b>
			</td>
		</tr>
	</table>
	<br />
	<table align="center" cellpadding="3" cellspacing="3" width="100%">
		<tr>
			<td valign="top"><b>CONTACT:</b></td>
			<td><%=rsOrder("FirstName")%>&nbsp;<%=rsOrder("LastName")%></td>
			<td align="right"><b>DATE OF BILLING:</b> &nbsp; <%=rsOrder("OrderDate")%> </td>
		</tr>
		<tr>
			<td valign="top"><b>INSTITUTION:</b></td>
			<td><%=rsOrder("Agency")%></td>
		</tr>
		<tr>
			<td valign="top"><b>BILLING ADDRESS:</b></td>
			<td>
				<%=rsOrder("baddress1")%><br />
				<%if (rsOrder("baddress2") <> "") then%><%=rsOrder("baddress2")%><br /><%end if%>
				<%=rsOrder("bcity")%> <%if (rsOrder("bstate") <> "") then%>, <%=rsOrder("bstate")%><%end if%> &nbsp; <%=rsOrder("bzip")%><br />
				<%if (rsOrder("bcountry") <> "") then%><%=rsOrder("bcountry")%> <br /><%end if%>
				<%if (rsOrder("bphone1") <> "") then%> Phone#:  <%=rsOrder("bphone1")%> <br /><%end if%>
				<%if (rsOrder("bphone2") <> "") then%> Phone#: <%=rsOrder("bphone2")%><br /><%end if%>
				<%if (rsOrder("fax") <> "") then%>Fax#: <%=rsOrder("fax")%><br /><%end if%>
				<%if (rsOrder("email") <> "") then%>E-mail Address: <%=rsOrder("email")%><br /><%end if%>
			
			</td>
		</tr>		
	</table>
<br />
	<table cellpadding="3" cellspacing="3" width="100%">
			<tr>
			<td colspan="6"><hr class="double" /></td>
		</tr>		
		<tr>
			<td colspan="6"><b>REPRODUCTION FEES (PAYABLE TO United States Holocaust Memorial Museum)</b><br /></td>
		</tr>
			<tr>
			<td colspan="6"><hr class="double" /></td>
		</tr>			
		<tr>
			<td><b>WORKSHEET #</b></td>
			<td><b>FORMAT</b></td>
			<td><b>QTY</b></td>
			<td><b>FEE/UNIT</b></td>
			<td><b>+FEE</b></td>
			<td><b>SUBTOTAL</b></td>
		</tr>
		<%
		theTotal = 0
		do while not rsOrderSummary.eof
		subTotal = rsOrderSummary("Quantity") * rsOrderSummary("FeePerUnit") + rsOrderSummary("AdditionalFee")
		%>
		<tr>
			<td><%=rsOrderSummary("Worksheet")%></td>
			<td><%=rsOrderSummary("Format")%></td>
			<td><%=rsOrderSummary("Quantity")%></td>
			<td><%=formatCurrency(rsOrderSummary("FeePerUnit"))%></td>
			<td><%=formatCurrency(rsOrderSummary("AdditionalFee"))%></td>
			<td><%=formatCurrency(subTotal)%></td>
		</tr>
		<%
		theTotal = theTotal + subTotal
		rsOrderSummary.moveNext
		loop
		%>
		<tr>	<td colspan="5" align="right"><b>Total:</b></td>
			<td ><b><%=formatCurrency(theTotal)%></b></td>
		</tr>
		<tr>
			<td colspan="6"><hr class="double" /></td>
		</tr>		
	</table>
<br />	
	<table cellpadding="3" cellspacing="3" width="100%">
		<tr>
			<td colspan="5"><hr class="double" /></td>
		</tr>			
		<tr>
			<td colspan="5"><b>USER FEES (PAYABLE TO INDIVIDUAL SOURCES. PLEASE ENCLOSE SEPARATE CHECKS IN THE AMOUNTS LISTED AT THE FAR RIGHT. WE WILL FORWARD CHECKS.)</td>
		</tr>		
		<tr>
			<td width="55%" valign="top"><b>SOURCE</b></td>
			
			<td width="15%" valign="top"><b>CD#</b></td>
<td width="15%" valign="top"><b>WORKSHEET#</b></td>
			<td width="15%" valign="top"><b>USER FEE</b></td>
			<td width="15%" valign="top"></td>
		</tr>
		<tr>
			<td colspan="5"><hr class="double" /></td>
		</tr>
		<%
		'Outer loop
		theTotal = 0
		do while not rsUserFeesSource.eof
		
			'sql = "SELECT sourcename,b.item_value,shippingcost FROM ipm_order_details a LEFT JOIN (select t.asset_id, t.item_value from ipm_asset_field_value t, ipm_asset_field_desc u where t.item_id = u.item_id and u.item_tag = 'IDAM_WORKSHEET') b ON b.asset_id = a.asset_id WHERE sourcename = '" & rsUserFeesSource("sourcename") &"' and orderid = " & orderID & " order by b.item_value"
			sql = "SELECT sourcename,b.item_value, c.item_value CDNUM, shippingcost FROM ipm_order_details a LEFT JOIN (select t.asset_id, t.item_value from ipm_asset_field_value t, ipm_asset_field_desc u where t.item_id = u.item_id and u.item_tag = 'IDAM_WORKSHEET') b ON b.asset_id = a.asset_id LEFT JOIN (select v.asset_id, v.item_value from ipm_asset_field_value v, ipm_asset_field_desc w where v.item_id = w.item_id and w.item_tag = 'IDAM_CDNUM') c ON c.asset_id = a.asset_id LEFT JOIN (select download_id,name from ipm_downloadtype) d ON d.download_id = a.download_id WHERE itemname = '" & rsUserFeesSource("itemname") &"' and a.orderid = " & orderID & "  order by b.item_value"
			if(rsUserFees.state = 1) then
				rsUserFees.close
			end if
			
			rsUserFees.open sql, Conn, 1, 4
			
			'Inner loop
			subTotal = 0
			do while not rsUserFees.eof
		%>
		<%
		if(rsUserFees("shippingcost") <> 0) then
		%>
		<tr>
			<td><%=rsUserFees("sourcename")%></td>

			<td><%=rsUserFees("CDNUM")%></td>
			<td><%=rsUserFees("item_value")%></td>
			<td><%=formatCurrency(rsUserFees("shippingcost"))%></td>
			<td></td>
		</tr>
		<%
		end if
		%>
		<%
		'Inner loop
		subTotal = subTotal + rsUserFees("shippingcost")
		rsUserFees.moveNext
		loop
		
		%>
		<%
		if(subTotal > 0) then
		%>
		<tr>	<td colspan="4" align="right"><b>Subtotal:</b></td>
			<td ><b><%=formatCurrency(subTotal)%></b></td>
		</tr>
		<tr>
			<td colspan="5"><hr class="single" /></td>
		</tr>
		<%end if%>
		<%
		'Outer loop
		theTotal = theTotal + subTotal
		rsUserFeesSource.moveNext
		loop%>

		<tr>	<td colspan="4" align="right"><b>Total:</b></td>
			<td ><b><%=formatCurrency(theTotal)%></b></td>
		</tr>
		<tr>
			<td colspan="5"><hr class="double" /></td>
		</tr>			
	</table>
</body>
</html>
<%
end if
%>
