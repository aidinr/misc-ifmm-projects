	<!--#include file="connection.asp" -->
<%
	orderID = prep_sql(request.querystring("oid"))
	
	if(orderID <> "") then
	
	set rsOrder=server.createobject("adodb.recordset")
	'sql="select * from ipm_order a left join (select c.orderid, c.item_value TaskOrder from ipm_order_field_value c, ipm_order_field_desc d where c.item_id = d.item_id and d.item_tag = 'IDAM_89') b on a.orderid = b.orderid left join (select e.orderid, e.item_value LabDate from ipm_order_field_value e, ipm_order_field_desc f where e.item_id = f.item_id and f.item_tag = 'IDAM_ORDER_LAB_SENT_DATE') c on a.orderid = b.orderid where a.orderid = " & orderID
	sql="select * from ipm_order a  where a.orderid = " & orderID
	rsOrder.Open sql, Conn, 1, 4

	set rsOrderTask=server.createobject("adodb.recordset")
	sql = "select c.orderid, c.item_value TaskOrder from ipm_order_field_value c, ipm_order_field_desc d where c.item_id = d.item_id and d.item_tag = 'IDAM_89' and c.orderid = " & orderID
	rsOrderTask.Open sql, Conn, 1, 4
	
	set rsOrderDate=server.createobject("adodb.recordset")
	sql = "select e.orderid, e.item_value LabDate from ipm_order_field_value e, ipm_order_field_desc f where e.item_id = f.item_id and f.item_tag = 'IDAM_ORDER_LAB_SENT_DATE' and e.orderid = " & orderID
	rsOrderDate.Open sql, Conn, 1, 4
	
	
	set rsUserFeesSource = server.createobject("adodb.recordset")
	sql = " select distinct itemname,c.item_value  from ipm_order_details a LEFT JOIN (select v.asset_id, v.item_value from ipm_asset_field_value v, ipm_asset_field_desc w where v.item_id = w.item_id and w.item_tag = 'IDAM_CDNUM') c ON c.asset_id = a.asset_id   where a.orderid = " & orderID & " order by c.item_value,itemname"
	rsUserFeesSource.Open sql, Conn, 1, 4	
	
	set rsUserFees=server.createobject("adodb.recordset")	
%>
<html>
<head>
<title>LAB ORDER</title>
<style type="text/css">
	td, th {
		font-size: 11px;
	}
	th {
		border-top: 4px double black;
		text-align: left;
		font-weight: bold;
	}
	#details {
		border-collapse: collapse;
	}
	#details td {
		text-align: left;
		border-top: 1px solid black;
	}
	#details_end td {
		border-top: 4px double black;
	}	
</style>
<style media="print">
	.print {
		display: none;
	}
</style>
</head>
<body>
	<table align="center" cellpadding="3" cellspacing="3" width="100%">
		<tr>
			<td align="center"><b><a href="javascript:window.print();" class="print">PRINT</a>&nbsp; &nbsp;  <a href="emailpage.aspx" class="print">EMAIL</a></b></td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td align="left" style="border-top:4px double black;border-bottom:4px double black;"><big><b>Lab Order</b></big></td>
		</tr>
	</table>
	<br />
	<table cellpadding="3" cellspacing="3">
		<tr>
			<td align="left" valign="top"><b>SHIP TO: </b></td>
			<td align="left" valign="top">
				<!--<%=rsOrder("baddress1")%><br />
				<%if (rsOrder("baddress2") <> "") then%><%=rsOrder("baddress2")%><br /><%end if%>
				<%=rsOrder("bcity")%> <%if (rsOrder("bstate") <> "") then%>, <%=rsOrder("bstate")%><%end if%> &nbsp; <%=rsOrder("bzip")%><br />
				<%if (rsOrder("bcountry") <> "") then%><%=rsOrder("bcountry")%> <br /><%end if%>
				-->
				Dodge Chrome,Inc<br />
                11941-L Bournefield Way<br />
                Silver Spring, MD 20904-7821<br />
                USA<br />
			</td>
		</tr>
	</table>
	<br />
	<table cellpadding="5" cellspacing="5">
		<tr>
			<td align="right" valign="top"><b>DATE ORDER SENT TO LAB: </b></td>
			<td align="left" valign="top">
				<%if(rsOrderDate.recordCount > 0) then %>
					<%=rsOrderDate("LabDate")%>
				<%else%>
				&nbsp;&nbsp;&nbsp; / &nbsp;&nbsp;&nbsp; /
				<%end if%>
			</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>CONTACT NAME: </b></td>
			<td align="left" valign="top">
				<%=rsOrder("FirstName")%>&nbsp;<%=rsOrder("LastName")%>
			</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>INSTITUTION: </b></td>
			<td align="left" valign="top">
				<%=rsOrder("Agency")%>
			</td>
		</tr>		
	</table>
	<br />
	<table cellpadding="3" cellspacing="3" width="100%" id="details">
		<tr>
		
		<th width="20%">CD NUMBER</th>
<th width="20%">WORKSHEET#</th>
		<th width="50%">FORMAT</th>
		<th width="10%">QTY</th>
		</tr>
		
		<%
		do while not rsUserFeesSource.eof
		
		'sql = "SELECT c.item_value Worksheet, e.item_value CDNUM, quantity, f.name Format FROM ipm_order_details a,ipm_asset_field_desc b, ipm_asset_field_value c,ipm_asset_field_desc d, ipm_asset_field_value e, ipm_downloadtype f WHERE itemname = '" & rsUserFeesSource("itemname") &"' and orderid = " & orderID & " and b.item_id = c.item_id and b.item_tag = 'IDAM_WORKSHEET' and c.asset_id = a.asset_id  and d.item_id = e.item_id and d.item_tag = 'IDAM_CDNUM' and e.asset_id = a.asset_id  and f.download_id = a.download_id order by c.item_value"
		sql = "SELECT b.item_value Worksheet, c.item_value CDNUM, quantity, d.name Format FROM ipm_order_details a LEFT JOIN (select t.asset_id, t.item_value from ipm_asset_field_value t, ipm_asset_field_desc u where t.item_id = u.item_id and u.item_tag = 'IDAM_WORKSHEET') b ON b.asset_id = a.asset_id LEFT JOIN (select v.asset_id, v.item_value from ipm_asset_field_value v, ipm_asset_field_desc w where v.item_id = w.item_id and w.item_tag = 'IDAM_CDNUM') c ON c.asset_id = a.asset_id LEFT JOIN (select download_id,name from ipm_downloadtype) d ON d.download_id = a.download_id WHERE itemname = '" & rsUserFeesSource("itemname") &"' and a.orderid = " & orderID & "  order by b.item_value"

		
			if(rsUserFees.state = 1) then
				rsUserFees.close
			end if
			
			rsUserFees.open sql, Conn, 1, 4
			
			'Inner loop
			subTotal = 0
			do while not rsUserFees.eof
		%>
		<tr>

			<td><%=rsUserFees("CDNum")%></td>
			<td><%=rsUserFees("Worksheet")%></td>
			<td><%=rsUserFees("Format")%></td>
			<td><%=rsUserFees("Quantity")%></td>
		</tr>
		<%
		rsUserFees.moveNext
		loop
		
		rsUserFeesSource.moveNext
		loop
		%>
		<tr id="details_end">
			<td colspan="4"></td>
		</tr>
	</table>
	<br />
	<table id="footer" cellspacing="10" cellpadding="10" width="100%">
		<%if (rsOrderTask.recordcount > 0) then %>
		<tr>
			<td width="35%"><b>USHMM TASK ORDER#:</b></td>
			<td>
				<%=rsOrderTask("TaskOrder")%>
			</td>
		</tr>
		<%end if%>
		<tr>
			<td width="35%"><b>SPECIAL INSTRUCTIONS:</b></td>
			<td style="border-bottom: 1px solid black;">&nbsp;</td>
		</tr>
		<tr>
			<td width="35%"></td>
			<td style="border-bottom: 1px solid black;">&nbsp;</td>
		</tr>		
	</table>
</body>
</html>
<%
end if
%>
