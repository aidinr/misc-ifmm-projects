web<%@ Page Language="vb" AutoEventWireup="false" Inherits="IdamAdmin.CarouselEmailHistory" CodeBehind="CarouselEmailHistory.aspx.vb" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Keywords</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
			<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
			<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
				<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
					<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
						<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
							<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
								<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
									<link href="treeStyle.css" type="text/css" rel="stylesheet">
										<link href="navStyle.css" type="text/css" rel="stylesheet" >
										    <link href="tabStripStyle.css" type="text/css" rel="stylesheet" />    
												<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
										<style>BODY { MARGIN: 0px }
	</style>
	<body onload="this.focus">
		<form id="Form" method="post" runat="server">
			<script type="text/javascript">
  
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridKeywords.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    GridKeywords.Edit(GridKeywords.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    GridKeywords.EditComplete();     
  }

  function insertRow()
  {
    GridKeywords.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    GridKeywords.Delete(GridKeywords.GetRowFromClientId(rowId)); 
  }

      
			</script>
			
			<div style="padding:20px;position:relative;">
			<div style="">
			<div style="padding:10px;border:1px solid;background-color:white;position:relative;">
			
			<div style=";padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Email history for this carousel listed below.</div><br>
  <div style="font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">
			<ComponentArt:Grid id="GridKeywords" RunningMode="Callback" CssClass="Grid" GroupByTextCssClass="GroupByText"
				GroupingNotificationTextCssClass="GridHeaderText" ShowFooter="true" PageSize="20" ImagesBaseUrl="images/"
				EditOnClickSelectedItem="true" AllowEditing="false" Sort="sent_date desc" ScrollBar="Off" ScrollTopBottomImagesEnabled="true"
				ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/"
				ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip"
				ScrollBarWidth="16" ScrollPopupClientTemplateId="ScrollPopupTemplate" FillWidth="true" Width="620px" Height="207" AutoCallBackOnDelete="true"  
 AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true"  runat="server" PagerStyle="Numbered"
        PagerTextCssClass="PagerText" 
        >
				<Levels>
					<ComponentArt:GridLevel DataKeyField="sent_date" ShowTableHeading="false" ShowSelectorCells="false" HeadingCellCssClass="HeadingCell"
						HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive" HeadingTextCssClass="HeadingCellText"
						DataCellCssClass="DataCell" RowCssClass="Row" SelectedRowCssClass="SelectedRow" SortAscendingImageUrl="asc.gif"
						SortDescendingImageUrl="desc.gif" ColumnReorderIndicatorImageUrl="reorder.gif" SortedDataCellCssClass="SortedDataCell"
						SortImageWidth="14" SortImageHeight="14" EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField"
						EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate">
						<Columns>
						<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings"  DataField="sent_date" HeadingImageUrl="icon_icon.gif"  FormatString="MMM dd yyyy HH:mm:ss"  HeadingImageWidth="14" HeadingImageHeight="16" Width="120"/>
						<ComponentArt:GridColumn DataField="sent_to" HeadingText="Sent To" Width="150" />
							<ComponentArt:GridColumn DataField="subject" HeadingText="Subject" Width="120" />
							<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" DataField="body" HeadingText="Body" Width="120" />
							
						</Columns>
					</ComponentArt:GridLevel>
				</Levels>
				<ClientTemplates>
					<ComponentArt:ClientTemplate Id="EditTemplate">
            <a href="javascript:editGrid('## DataItem.ClientId ##');">Edit</a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> | <a href="javascript:GridKeywords.EditCancel();">Cancel</a>
          </ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> | <a href="javascript:GridKeywords.EditCancel();">Cancel</a>
          </ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="LoadingFeedbackTemplate">
						<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td style="font-size:10px;">Loading...&nbsp;</td>
								<td><img src="images/spinner.gif" width="16" height="16" border="0"></td>
							</tr>
						</table>
					</ComponentArt:ClientTemplate>
					
				</ClientTemplates>
			</ComponentArt:Grid>
			
			
			</div>
			
			
			
			
			
			
			
			
			</div>
			
			<div style="text-align:right;"><br>
			<input type="button" onclick="window.close();" value="Close" />
			</div>
			
			</div>
			</div>
			
		
			
			
		</form>
	</body>
</HTML>
