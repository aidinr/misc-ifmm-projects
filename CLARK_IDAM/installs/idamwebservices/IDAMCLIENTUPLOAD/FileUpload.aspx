<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FileUpload.aspx.vb" EnableViewState="false"  Inherits="WebArchives.iDAM.WebClientUpload.FileUpload"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>FileUpload</title>
		<SCRIPT Language="JavaScript">
/* 
This small function makes sure that the progress indicator and server-side
processing page receive the new progress ID we just created in the CodeBehind class.
Also, it pops up the progress window.
*/
function startupload() 
{		
		winstyle="height=200,width=500,status=no,scrollbars=no,toolbar=no,menubar=no,location=no";
		if(Form1.thefile.value != "")
			window.open("ProgressIndicator.aspx?progressid=<%=progressid%>",null,winstyle);
		Form1.action = Form1.action + "?progressid=" + <%=progressid%>;
}
		</SCRIPT>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form onSubmit="startupload();" id="Form1" method="post" runat="server">
			<P>file:<BR>
				<INPUT id="thefile" type="file" name="thefile" runat="server"><BR>
				repositoryId:<BR>
				<asp:textbox id="repository_id" runat="server">1</asp:textbox><BR>
				projectid:<BR>
				<asp:textbox id="project_id" runat="server">21192852</asp:textbox><BR>
				securityId:<BR>
				<asp:textbox id="security_id" runat="server">3</asp:textbox><BR>
				userId<BR>
				<asp:textbox id="user_id" runat="server">1</asp:textbox><BR>
				fileName<BR>
				<asp:textbox id="name" runat="server">test.tif</asp:textbox><BR>
				description<BR>
				<asp:textbox id="description" runat="server"></asp:textbox><BR>
				categoryId<BR>
				<asp:textbox id="category_id" runat="server">0</asp:textbox><BR>
				instanceId:<BR>
				<asp:textbox id="instance_id" runat="server">IDAM_RIM</asp:textbox><BR>
				redirect to:<BR>
				<asp:textbox id="txtRedirect" runat="server">http://localhost/IDAMFileUpload/FileUpload.aspx</asp:textbox><BR>
				type<BR>
				<asp:dropdownlist id="asset_type" runat="server">
					<asp:ListItem>mainasset</asp:ListItem>
					<asp:ListItem>userasset</asp:ListItem>
					<asp:ListItem>resume</asp:ListItem>
					<asp:ListItem>approvalreq</asp:ListItem>
				</asp:dropdownlist></P>
			<P>priority<BR>
				<asp:TextBox id="txtPriority" runat="server">0</asp:TextBox></P>
			<P><asp:button id="Button1" runat="server" Text="Upload"></asp:button></P>
			<P><asp:label id="Label1" runat="server">Label</asp:label></P>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
		</form>
	</body>
</HTML>
