<%@ Page Language="VB" Inherits="FileUpSESamples.ProgressIndicator" Src="ProgressIndicator.uplx.vb" Debug="True" %>

<%
'-----------------------------------------------------------------------
'--- .NET FileUp QueryString Progress Indicator Sample
'--- 
'--- FileUp's progress indicator lets you know how much of the entire upload
'--- has arrived at the server. 
'---
'--- Note: Rename ASPX pages to UPLX for production
'---
'--- Copyright (c) 2003 SoftArtisans, Inc.
'--- Mail: info@softartisans.com   http://www.softartisans.com
'-----------------------------------------------------------------------
%>

<html>
<head>
</head>
<body>
  
      <h3>Upload Progress:</h3>
      <!-- 
      This table holds cells that are used to display
      the upload progress for the progress indicator
      -->
      <asp:Table id="UploadTable" 
           GridLines="Both" 
           HorizontalAlign="Center" 
           Font-Name="Verdana" 
           Font-Size="8pt" 
           CellPadding="15" 
           CellSpacing="0" 
           runat="server">
	
	<asp:tablerow>
		<asp:tablecell><B>Progress ID2</B></asp:tablecell>
		<asp:tablecell width="300"><B>Graphic</B></asp:tablecell>
		<asp:tablecell><B>Transferred Bytes</B></asp:tablecell>
		<asp:tablecell><B>Total Bytes</B></asp:tablecell>
		<asp:tablecell><B>Percentage</B></asp:tablecell>
	</asp:tablerow>
	<asp:tablerow>
		<asp:tablecell runat="server" id="cellProgressID" text="0" />
		<asp:tablecell runat="server" id="cellGraphic">
			<HR align="left" style="color:blue" size="10" width="<%=strPercent%>">
		</asp:tablecell>
		<asp:tablecell runat="server" id="cellTransferredBytes" />
		<asp:tablecell runat="server" id="cellTotalBytes" />
		<asp:tablecell runat="server" id="cellPercentage" />
	</asp:tablerow>
	</asp:table>
 
</body>
</html>