<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Download_PDF.aspx.vb" Inherits="WebArchives.iDAM.WebServices.Download_PDF" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Download_PDF</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<asp:literal id="metarefresh" runat="server"></asp:literal>
		<SCRIPT language="JavaScript"><!--
function redirect () { setTimeout("go_now()",20000); }
//--></SCRIPT>
		<asp:literal id="RedirectScript" runat="server"></asp:literal>
	</HEAD>
	<body onload="redirect()">
		<form id="Form1" method="post" runat="server">
			<asp:panel id="ProgressPanel" Visible="True" runat="server">
				<TABLE width="100%">
					<TR>
						<TD></TD>
					</TR>
					<TR>
						<TD align="center"><FONT face="Arial" size="1">
								<asp:Label id="lblFileName" runat="server">FileName</asp:Label>&nbsp;is is 
								currently being converted to pdf.
								<BR>
								<IMG src="images/loading.gif"><BR>
								Note: This process should take no more than 400 seconds. Time to complete the 
								conversion will depend greatly on the type and size of the document being 
								converted. </FONT>
						</TD>
					</TR>
				</TABLE>
			</asp:panel><asp:Label id="lblError" Visible="False" runat="server">Label</asp:Label></form>
	</body>
</HTML>
