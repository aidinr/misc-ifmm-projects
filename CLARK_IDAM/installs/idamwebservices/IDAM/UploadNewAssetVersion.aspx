<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UploadNewAssetVersion.aspx.vb" EnableViewState="false" Inherits="WebArchives.iDAM.WebServices.UploadNewAssetVersion" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>FileUpload</title>
		<SCRIPT language="JavaScript">
/* 
This small function makes sure that the progress indicator and server-side
processing page receive the new progress ID we just created in the CodeBehind class.
Also, it pops up the progress window.
*/
function startupload() 
{		
		winstyle="height=200,width=500,status=no,scrollbars=no,toolbar=no,menubar=no,location=no";
		if(Form1.thefile.value != "")
			window.open("ProgressIndicator.aspx?progressid=<%=progressid%>",null,winstyle);
		Form1.action = Form1.action + "?progressid=" + <%=progressid%>;
}
		</SCRIPT>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form id="Form1" onsubmit="startupload();" method="post" runat="server">
			<P>file:<BR>
				<INPUT id="thefile" type="file" name="thefile" runat="server"><BR>
			</P>
			<P><asp:button id="Button1" runat="server" Text="Upload"></asp:button></P>
			<P><asp:label id="Label1" runat="server">Label</asp:label></P>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
		</form>
	</body>
</HTML>
