<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Download_PowerPoint.aspx.vb" Inherits="WebArchives.iDAM.WebServices.Download_PowerPoint" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Download_Type</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="metarefresh" runat="server"></asp:literal>
		<SCRIPT language="JavaScript"><!--
function redirect () { setTimeout("go_now()",20000); }
//--></SCRIPT>
		<asp:literal id="RedirectScript" runat="server"></asp:literal>
	</HEAD>
	<body onload="redirect()">
		<form id="Form1" method="post" runat="server">
			<asp:panel id="ProgressPanel" Visible="True" runat="server">
				<TABLE width="100%">
					<TR>
						<TD></TD>
					</TR>
					<TR>
						<TD align="middle"><font face="Arial" size="1">
								Creating powerpoint file.
								<BR>
								<IMG src="images/loading.gif"><BR>
								Note: This should take about a second for every&nbsp;Meg&nbsp;asset file selected</font>
						</TD>
					</TR>
				</TABLE>
			</asp:panel>
			<FONT face="Arial" size="1" color="red"><asp:Label id="lblError" Visible="False" runat="server">Label</asp:Label></font></form>
	</body>
</HTML>

