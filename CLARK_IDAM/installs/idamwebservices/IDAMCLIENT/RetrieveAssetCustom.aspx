<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RetrieveAssetCustom.aspx.vb" Inherits="WebArchives.iDAM.WebClient.RetrieveAssetCustom"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>RetrieveAssetSize</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
					<table>
				<tr>
					<td>parameter names</td>
					<td>parameter description</td>
				</tr>
				<tr>
					<td>id</td>
					<td>asset id</td>
				</tr>
				<tr>
					<td>instance</td>
					<td>&nbsp;instance id</td>
				</tr>
				<tr>
					<td>type</td>
					<td>asset type</td>
				</tr>
				<tr>
					<td>width</td>
					<td></td>
				</tr>
				<tr>
					<td>height</td>
					<td></td>
				</tr>
				<tr>
					<td>crop</td>
					<td></td>
				</tr>
				<tr>
					<td>crop_color</td>
					<td></td>
				</tr>
				<tr>
					<td>crop_h_align</td>
					<td></td>
				</tr>
				<tr>
					<td>crop_w_align</td>
					<td></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
