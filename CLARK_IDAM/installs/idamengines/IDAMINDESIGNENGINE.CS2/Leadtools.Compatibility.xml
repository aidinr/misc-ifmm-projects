<?xml version="1.0"?>
<doc>
    <assembly>
        <name>Leadtools.Compatibility</name>
    </assembly>
    <members>
        <member name="T:Leadtools.Compatibility.RasterImageConverter">
            <summary>
            The RasterImageConverter class provides support for converting <see cref="T:Leadtools.IRasterImage"/> to a format
            compatible with LEADTOOLS Raster COM Objects and LEADTOOLS ActiveX Controls.
            </summary>
            <remarks>
            Use this class if you want to combine LEADTOOLS Raster Imaging COM Objects or ActiveX Controls 
            with your LEADTOOLS .Net Class Library applications.
            </remarks>
        </member>
        <member name="M:Leadtools.Compatibility.RasterImageConverter.FromLeadBitmap(System.IntPtr,System.String)">
            <summary>
            Converts the specified image from a LEADTOOLS COM Object or ActiveX Control to an <see cref="T:Leadtools.IRasterImage"/>.
            </summary>
            <param name="leadBitmap">The input image (from LEADTOOLS COM Object or ActiveX Control's <b>Bitmap</b> property.</param>
            <param name="kernelName">A string containing the file name of the LEADTOOLS Kernel being used by the COM Object or ActiveX control from which <b>leadBitmap</b> is being supplied.</param>
            <returns>The <see cref="T:Leadtools.IRasterImage"/> that this method creates.</returns>
            <remarks>
            The <b>kernelName</b> will be the name of the core dll for the COM Object or ActiveX control from which the <b>leadBitmap</b> is
            being supplied.  Possible values are as follows:
            <list type="table">
               <listheader>
                  <term>Toolkit</term>
                  <description>Kernel DLL name</description>
               </listheader>
               <item>
                  <term>LEADTOOLS Raster COM Object</term>
                  <description>"LTR14N.DLL"</description>
               </item>
               <item>
                  <term>LEADTOOLS ActiveX Control</term>
                  <description>"LTKRN14N.DLL"</description>
               </item>
            </list>
            The <b>kernelName</b> can be a filename or a fully qualified path.
            </remarks>
            <example>
            <para><b>VB.NET</b></para>
            <code>
            Imports Leadtools.Compatibility
            Imports LTRASTERVIEWLib
            Private Sub FromLeadBitmapTest(ByVal rasterImageViewer1 As RasterImageViewer)
                ' Assume that axLEADRasterView1 is an instance of ILEADRasterView COM Object.
                ' Assume that rasterImageViewer1 is an instance of RasterImageViewer.
                Dim ocxBitmap As IntPtr = New IntPtr(axLEADRasterView1.Raster.Bitmap)
                Dim rasterImageViewer1.Image = RasterImageConverter.FromLeadBitmap(ocxBitmap, "LTR14N.DLL")
            End Sub
            </code>
            <para><b>C#</b></para>
            <code>
            using Leadtools.Compatibility;
            using  LTRASTERVIEWLib;
            private void FromLeadBitmapTest(RasterImageViewer rasterImageViewer1)
            {
               // Assume that axLEADRasterView1 is an instance of ILEADRasterView COM Object.
               // Assume that rasterImageViewer1 is an instance of RasterImageViewer.
               IntPtr ocxBitmap = new IntPtr(axLEADRasterView1.Raster.Bitmap);
               rasterImageViewer1.Image = RasterImageConverter.FromLeadBitmap(ocxBitmap, "LTR14N.DLL");
            }
            </code>
            </example>
        </member>
        <member name="M:Leadtools.Compatibility.RasterImageConverter.FromLeadBitmapList(System.IntPtr,System.String)">
            <summary>
            Converts the specified BitmapList from a LEADTOOLS COM Object or ActiveX Control to an <see cref="T:Leadtools.IRasterImage"/>.
            </summary>
            <param name="leadBitmapList">The input BitmapList (from LEADTOOLS COM Object or ActiveX Control's <b>BitmapList</b>
            property.  All items from the BitmapList are copied to the <see cref="T:Leadtools.IRasterImage"/>.</param>
            <param name="kernelName">A string containing the file name of the LEADTOOLS Kernel being used by the COM Object or ActiveX control from which <b>leadBitmapList</b> is being supplied.</param>
            <returns>The <see cref="T:Leadtools.IRasterImage"/> that this method creates.</returns>
            <remarks>
            The <b>kernelName</b> will be the name of the core dll for the COM Object or ActiveX control from which the <b>leadBitmapList</b> is
            being supplied.  Possible values are as follows:
            <list type="table">
               <listheader>
                  <term>Toolkit</term>
                  <description>Kernel DLL name</description>
               </listheader>
               <item>
                  <term>LEADTOOLS Raster COM Object</term>
                  <description>"LTR14N.DLL"</description>
               </item>
               <item>
                  <term>LEADTOOLS ActiveX Control</term>
                  <description>"LTKRN14N.DLL"</description>
               </item>
            </list>
            The <b>kernelName</b> can be a filename or a fully qualified path.
            </remarks>
            <example>
            <para><b>VB.NET</b></para>
            <code>
            Imports Leadtools.Compatibility
            Imports LTRASTERVIEWLib
            Private Sub FromLeadBitmapListTest(ByVal rasterImageViewer1 As RasterImageViewer)
                ' Assume that axLEADRasterView1 is an instance of ILEADRasterView COM Object.
                ' Assume that rasterImageViewer1 is an instance of RasterImageViewer.
                Dim ocxBitmapList As IntPtr = New IntPtr(axLEADRasterView1.Raster.BitmapList)
                rasterImageViewer1.Image = RasterImageConverter.FromLeadBitmapList(ocxBitmapList, "LTR14N.DLL")
            End Sub
            </code>
            <para><b>C#</b></para>
            <code>
            using Leadtools.Compatibility;
            using  LTRASTERVIEWLib;
            private void FromLeadBitmapListTest(RasterImageViewer rasterImageViewer1)
            {
               // Assume that axLEADRasterView1 is an instance of ILEADRasterView COM Object.
               // Assume that rasterImageViewer1 is an instance of RasterImageViewer.
               IntPtr ocxBitmapList = new IntPtr(axLEADRasterView1.Raster.BitmapList);
               rasterImageViewer1.Image = RasterImageConverter.FromLeadBitmapList(ocxBitmapList, "LTR14N.DLL");
            }
            </code>
            </example>
        </member>
        <member name="M:Leadtools.Compatibility.RasterImageConverter.ToLeadBitmap(Leadtools.IRasterImage,System.String)">
            <summary>
            Converts the specified <see cref="T:Leadtools.IRasterImage"/> to a format compatible with the LEADTOOLS COM Object or 
            ActiveX Control.
            </summary>
            <param name="image">The input <see cref="T:Leadtools.IRasterImage"/>.</param>
            <param name="kernelName">A string containing the file name of the LEADTOOLS Kernel being used by the COM Object or ActiveX control from which <b>leadBitmap</b> is being supplied.</param>
            <returns>A pointer to the bitmap that this method creates.</returns>
            <remarks>
            This method allocates new memory for the converted bitmap.  You must free that memory when it is no longer needed.<br/>
            For example, after you assign the returned <see cref="T:System.IntPtr"/> to the COM Object's <b>Bitmap</b> property, you can call the <b>Marshal.FreeHGlobal</b> method.<br/>
            If you wish to assign the bitmap to the COM Object without making a copy of the data, you can set the COM Object's <b>RefBitmap</b> property
            to true before making the assignment, and then there is no need to free the <see cref="T:System.IntPtr"/>.
            <br/>
            <br/>
            The <b>kernelName</b> will be the name of the core dll for the COM Object or ActiveX control from which the <b>leadBitmap</b> is
            being supplied.  Possible values are as follows:
            <list type="table">
               <listheader>
                  <term>Toolkit</term>
                  <description>Kernel DLL name</description>
               </listheader>
               <item>
                  <term>LEADTOOLS Raster COM Object</term>
                  <description>"LTR14N.DLL"</description>
               </item>
               <item>
                  <term>LEADTOOLS ActiveX Control</term>
                  <description>"LTKRN14N.DLL"</description>
               </item>
            </list>
            The <b>kernelName</b> can be a filename or a fully qualified path.
            </remarks>
            <example>
            <para><b>VB.NET</b></para>
            <code>
            Imports Leadtools.Compatibility
            Imports LTRASTERVIEWLib
            Private Sub ToLeadBitmapTest(ByVal rasterImageViewer1 As RasterImageViewer)
                ' Assume that axLEADRasterView1 is an instance of ILEADRasterView COM Object.
                ' Assume that rasterImageViewer1 is an instance of RasterImageViewer
                Dim ocxBitmap As IntPtr = RasterImageConverter.ToLeadBitmap(rasterImageViewer1.Image, "LTR14N.DLL")
                axLEADRasterView1.Raster.RefBitmap = True
                axLEADRasterView1.Raster.Bitmap = ocxBitmap.ToInt32()
                axLEADRasterView1.Raster.RefBitmap = False
            End Sub
            </code>
            <para><b>C#</b></para>
            <code>
            using Leadtools.Compatibility;
            using  LTRASTERVIEWLib;
            private void ToLeadBitmapTest(RasterImageViewer rasterImageViewer1)
            {
               // Assume that axLEADRasterView1 is an instance of ILEADRasterView COM Object.
               // Assume that rasterImageViewer1 is an instance of RasterImageViewer
               IntPtr ocxBitmap = RasterImageConverter.ToLeadBitmap(rasterImageViewer1.Image, "LTR14N.DLL");
               axLEADRasterView1.Raster.RefBitmap = true;
               axLEADRasterView1.Raster.Bitmap = ocxBitmap.ToInt32();
               axLEADRasterView1.Raster.RefBitmap = false;
            }
            </code>
            </example>
        </member>
        <member name="M:Leadtools.Compatibility.RasterImageConverter.ToLeadBitmapList(Leadtools.IRasterImage,System.String)">
            <summary>
            Converts the specified <see cref="T:Leadtools.IRasterImage"/> to a format compatible with the LEADTOOLS COM Object or 
            ActiveX Control.
            </summary>
            <param name="image">The input <see cref="T:Leadtools.IRasterImage"/>.  All pages from <b>image</b> will be copied to the LEAD BitmapList returned by this method.</param>
            <param name="kernelName">A string containing the file name of the LEADTOOLS Kernel being used by the COM Object or ActiveX control from which <b>leadBitmap</b> is being supplied.</param>
            <returns>A pointer to the LEAD BitmapList that this method creates.</returns>
            <remarks>
            This method allocates new memory for a LEAD BitmapList.  You must free that memory when it is no longer needed.<br/>
            For example, after you assign the returned <see cref="T:System.IntPtr"/> to the COM Object's <b>BitmapList</b> property, you can
            call the <see cref="M:Leadtools.Compatibility.RasterImageConverter.FreeLeadBitmapList(System.IntPtr,System.String)"/> method.<br/>
            If you wish to assign the bitmap list to the COM Object without making a copy of the data, you can set the COM Object's <b>RefBitmapList</b> property
            to true before making the assignment, but you still <b>MUST</b> call <see cref="M:Leadtools.Compatibility.RasterImageConverter.FreeLeadBitmapList(System.IntPtr,System.String)"/> to free the <see cref="T:System.IntPtr"/>.
            <br/>
            <br/>
            The <b>kernelName</b> will be the name of the core dll for the COM Object or ActiveX control from which the <b>leadBitmap</b> is
            being supplied.  Possible values are as follows:
            <list type="table">
               <listheader>
                  <term>Toolkit</term>
                  <description>Kernel DLL name</description>
               </listheader>
               <item>
                  <term>LEADTOOLS Raster COM Object</term>
                  <description>"LTR14N.DLL"</description>
               </item>
               <item>
                  <term>LEADTOOLS ActiveX Control</term>
                  <description>"LTKRN14N.DLL"</description>
               </item>
            </list>
            The <b>kernelName</b> can be a filename or a fully qualified path.
            </remarks>
            <example>
            <para><b>VB.NET</b></para>
            <code>
            Imports Leadtools.Compatibility
            Imports LTRASTERVIEWLib
            Private Sub ToLeadBitmapListTest(ByVal rasterImageViewer1 As RasterImageViewer)
               ' Assume that axLEADRasterView1 is an instance of ILEADRasterView COM Object.
               ' Assume that rasterImageViewer1 is an instance of RasterImageViewer
               Dim ocxBitmapList As IntPtr = RasterImageConverter.ToLeadBitmapList(rasterImageViewer1.Image, "LTR14N.DLL")
               axLEADRasterView1.Raster.RefBitmapList = True
               axLEADRasterView1.Raster.BitmapList = ocxBitmapList.ToInt32()
               axLEADRasterView1.Raster.RefBitmapList = False
               axLEADRasterView1.Raster.BitmapListIndex = 0
               RasterImageConverter.FreeLeadBitmapList(ocxBitmapList, "LTR14N.DLL")
            End Sub
            </code>
            <para><b>C#</b></para>
            <code>
            using Leadtools.Compatibility;
            using  LTRASTERVIEWLib;
            private void FromLeadBitmapLiarTest(RasterImageViewer rasterImageViewer1)
            {
               // Assume that axLEADRasterView1 is an instance of ILEADRasterView COM Object.
               // Assume that rasterImageViewer1 is an instance of RasterImageViewer
               IntPtr ocxBitmapList = RasterImageConverter.ToLeadBitmapList(rasterImageViewer1.Image, "LTR14N.DLL");
               axLEADRasterView1.Raster.RefBitmapList = true;
               axLEADRasterView1.Raster.BitmapList = ocxBitmapList.ToInt32();
               axLEADRasterView1.Raster.RefBitmapList = false;
               axLEADRasterView1.Raster.BitmapListIndex = 0;
               RasterImageConverter.FreeLeadBitmapList(ocxBitmapList, "LTR14N.DLL");
            }
            </code>
            </example>
        </member>
        <member name="M:Leadtools.Compatibility.RasterImageConverter.FreeLeadBitmap(System.IntPtr,System.String)">
            <summary>
            Frees storage allocated for a specified LEADTOOLS COM Object or ActiveX control bitmap.
            </summary>
            <param name="leadBitmap">The input image (from LEADTOOLS COM Object or ActiveX Control's <b>Bitmap</b> property.</param>
            <param name="kernelName">A string containing the file name of the LEADTOOLS Kernel being used by the COM Object or ActiveX control from which <b>leadBitmap</b> is being supplied.</param>
            <remarks>
            The <b>kernelName</b> will be the name of the core dll for the COM Object or ActiveX control from which the leadBitmap is
            being supplied.  Possible values are as follows:
            <list type="table">
               <listheader>
                  <term>Toolkit</term>
                  <description>Kernel DLL name</description>
               </listheader>
               <item>
                  <term>LEADTOOLS Raster COM Object</term>
                  <description>"LTR14N.DLL"</description>
               </item>
               <item>
                  <term>LEADTOOLS ActiveX Control</term>
                  <description>"LTKRN14N.DLL"</description>
               </item>
            </list>
            The <b>kernelName</b> can be a filename or a fully qualified path.
            </remarks>
            <example>
            <para><b>VB.NET</b></para>
            <code>
            Imports Leadtools.Compatibility
            Private Sub ToLeadTest(ByVal viewer As RasterImageViewer)
               Dim ocxBitmap As IntPtr = RasterImageConverter.ToLeadBitmap(viewer.Image, "LTR14N.DLL")
               ' Do something with ocxBitmap, for example, use LEADTOOLS COM objects to save it to a file.
               RasterImageConverter.FreeLeadBitmap(ocxBitmap, "LTR14N.DLL")
               System.Runtime.InteropServices.Marshal.FreeHGlobal(ocxBitmap)
            End Sub
            </code>
            <para><b>C#</b></para>
            <code>
            using Leadtools.Compatibility;
            private void ToLeadBitmapTest(RasterImageViewer viewer)
            {
               IntPtr ocxBitmap = RasterImageConverter.ToLeadBitmap(viewer.Image, "LTR14N.DLL");
               // Do something with ocxBitmap, for example, use LEADTOOLS COM objects to save it to a file.
               RasterImageConverter.FreeLeadBitmap(ocxBitmap, "LTR14N.DLL");
               System.Runtime.InteropServices.Marshal.FreeHGlobal(ocxBitmap);
            }
            </code>
            </example>
        </member>
        <member name="M:Leadtools.Compatibility.RasterImageConverter.FreeLeadBitmapList(System.IntPtr,System.String)">
            <summary>
            Frees storage allocated for a specified LEADTOOLS COM Object or ActiveX control bitmap list.
            </summary>
            <param name="leadBitmapList">The input image (from LEADTOOLS COM Object or ActiveX Control's <b>BitmapList</b> property.</param>
            <param name="kernelName">A string containing the file name of the LEADTOOLS Kernel being used by the COM Object or ActiveX control from which <b>leadBitmapList</b> is being supplied.</param>
            <remarks>
            The <b>kernelName</b> will be the name of the core dll for the COM Object or ActiveX control from which the <b>leadBitmapList</b> is
            being supplied.  Possible values are as follows:
            <list type="table">
               <listheader>
                  <term>Toolkit</term>
                  <description>Kernel DLL name</description>
               </listheader>
               <item>
                  <term>LEADTOOLS Raster COM Object</term>
                  <description>"LTR14N.DLL"</description>
               </item>
               <item>
                  <term>LEADTOOLS ActiveX Control</term>
                  <description>"LTKRN14N.DLL"</description>
               </item>
            </list>
            The <b>kernelName</b> can be a filename or a fully qualified path.
            </remarks>
            <example>
            Refer to <see cref="M:Leadtools.Compatibility.RasterImageConverter.ToLeadBitmapList(Leadtools.IRasterImage,System.String)"/> example.
            </example>
        </member>
    </members>
</doc>
