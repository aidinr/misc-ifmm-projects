<%@ Control Language="vb" AutoEventWireup="false" Codebehind="Jobs.ascx.vb" Inherits="IDAM5.Jobs" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<div class="previewpaneMyProjects" id="toolbar">
	<!--Project cookie and top menu-->
		<table cellspacing="0"  id="table2" width="100%">
			<tr>
						<td width="1" nowrap valign="top">
			<img src="images/ui/profiles_ico_bg.gif" height=42 width=42>
			
			
			</td>
			
			<td valign="top">
			<div style="float:left;padding:5px;"><font face="Verdana" size="3" color=gray><b>Job Listings</b></font><br>Modify/add job listings below.  Manage the viewing of the items by setting the post and pull dates appropriately.</div>
			</td>
			<td id="Test" valign="top" align="right">
			<font face="Verdana" size="1" color=white><script>writeDate();</script></font>
			</td></tr>
			<tr><td valign="top" colspan="2">
			</td>
			</tr>
		</table>
</div> <!--end preview pane-->

<div class="previewpaneSub">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle" width="100%">
			
			<div style="PADDING-RIGHT:0px;PADDING-LEFT:0px;PADDING-BOTTOM:0px;PADDING-TOP:0px;align:right">
								
									<b><asp:HyperLink CssClass="projecttabs" id="Hyperlink1" NavigateUrl="javascript:gotopagelink('Events');"
									runat="server">General</asp:HyperLink></b>
							</div>
			</td>
			<td align="right"  width="100%" >
				<img src="images/spacer.gif" height=16 width=1>
			</td>
		</tr>
	</table>
</div>











<div class="previewpaneProjects" id="toolbar">
	<div style="BORDER-RIGHT:0px solid; PADDING-RIGHT:5px; BORDER-TOP:0px solid; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:0px solid; PADDING-TOP:5px; BORDER-BOTTOM:0px solid" id="browsecenter">
	<img src="images/spacer.gif"  height="15" width="5"><br>
		<div style="PADDING-RIGHT:10px;PADDING-BOTTOM:10px">
<b>Job Items</b>
</div>		


<div style="PADDING-TOP:5px"><div style="PADDING-RIGHT:3px;PADDING-LEFT:3px;PADDING-BOTTOM:3px;PADDING-TOP:3px"></div></div>
[ <a href="javascript:Object_PopUp('JobEdit.aspx?action=new','Job_Item',880,650);">create job listing</a> ] 
<div style="PADDING-TOP:5px"><div style="PADDING-RIGHT:3px;PADDING-LEFT:3px;PADDING-BOTTOM:3px;PADDING-TOP:3px"></div></div>						
<asp:Literal id="LiteralNews" Text="No jobs available" visible=false runat="server" />
							
<script language=javascript type=text/javascript>

  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <%response.write(GridNews.ClientID)%>.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }


  function editGridJobs(rowId)
  {
	Object_PopUp('JobEdit.aspx?ID=' + rowId ,'Edit_Job',920,650);
  }
  
  function editRow()
  {
    <%response.write(GridNews.ClientID)%>.EditComplete();     
  }

  function insertRow()
  {
    <%response.write(GridNews.ClientID)%>.EditComplete(); 
  }

  function deleteRowNews(rowId)
  {
  if (confirm("Delete record?"))
  {
        <%response.write(GridNews.ClientID)%>.Delete(<%response.write(GridNews.ClientID)%>.GetRowFromClientId(rowId)); 
}
    
  }

  function getValue()
  {

    var selectedDate = _ctl0__ctl0_GridNews_EditTemplate_0_2_Picker1.GetSelectedDate();
    var formattedDate = _ctl0__ctl0_GridNews_EditTemplate_0_2_Picker1.FormatDate(selectedDate, 'MMM dd yyyy');
    return [selectedDate, formattedDate];
  }

  function setValue(DataItem)
  {
   var selectedDate = DataItem.GetMember('post_date_new').Object;
   _ctl0__ctl0_GridNews_EditTemplate_0_2_Picker1.SetSelectedDate(selectedDate);
  }

      function Picker1_OnDateChange()
      {
        var fromDate = _ctl0__ctl0_GridNews_EditTemplate_0_2_Picker1.GetSelectedDate();
        _ctl0__ctl0_GridNews_EditTemplate_0_2_Calendar1.SetSelectedDate(fromDate);
      }
      
      function Calendar1_OnChange()
      {
        var fromDate = _ctl0__ctl0_GridNews_EditTemplate_0_2_Calendar1.GetSelectedDate();
        _ctl0__ctl0_GridNews_EditTemplate_0_2_Picker1.SetSelectedDate(fromDate);
      }

      function Button_OnClick(button)
      {
        if (_ctl0__ctl0_GridNews_EditTemplate_0_2_Calendar1.PopUpObjectShowing)
        {
          _ctl0__ctl0_GridNews_EditTemplate_0_2_Calendar1.Hide();
        }
        else
        {
          _ctl0__ctl0_GridNews_EditTemplate_0_2_Calendar1.SetSelectedDate(_ctl0__ctl0_GridNews_EditTemplate_0_2_Picker1.GetSelectedDate());
          _ctl0__ctl0_GridNews_EditTemplate_0_2_Calendar1.Show(button);
        }
      }
      
      function Button_OnMouseUp()
      {
        if (_ctl0__ctl0_GridNews_EditTemplate_0_2_Calendar1.PopUpObjectShowing)
        {
          event.cancelBubble=true;
          event.returnValue=false;
          return false;
        }
        else
        {
          return true;
        }
      }
      
      
      
      function RefreshJobItems()
      {

      <%response.write(GridNews.ClientID)%>.Filter("job_title_new LIKE '%%'");
 
      }
      
      
      
      
  function getValue2()
  {
  
    var selectedDate = _ctl0__ctl0_GridNews_EditTemplate_0_3_Picker2.GetSelectedDate();
    var formattedDate = _ctl0__ctl0_GridNews_EditTemplate_0_3_Picker2.FormatDate(selectedDate, 'MMM dd yyyy');
    return [selectedDate, formattedDate];
  }

  function setValue2(DataItem)
  {
   var selectedDate = DataItem.GetMember('pull_date_new').Object;
   _ctl0__ctl0_GridNews_EditTemplate_0_3_Picker2.SetSelectedDate(selectedDate);
  }

      function Picker1_OnDateChange2()
      {
        var fromDate = _ctl0__ctl0_GridNews_EditTemplate_0_3_Picker2.GetSelectedDate();
        _ctl0__ctl0_GridNews_EditTemplate_0_3_Calendar2.SetSelectedDate(fromDate);
      }
      
      function Calendar1_OnChange2()
      {
        var fromDate = _ctl0__ctl0_GridNews_EditTemplate_0_3_Calendar2.GetSelectedDate();
        _ctl0__ctl0_GridNews_EditTemplate_0_3_Picker2.SetSelectedDate(fromDate);
      }

      function Button_OnClick2(button)
      {
        if (_ctl0__ctl0_GridNews_EditTemplate_0_3_Calendar2.PopUpObjectShowing)
        {
          _ctl0__ctl0_GridNews_EditTemplate_0_3_Calendar2.Hide();
        }
        else
        {
          _ctl0__ctl0_GridNews_EditTemplate_0_3_Calendar2.SetSelectedDate(_ctl0__ctl0_GridNews_EditTemplate_0_3_Picker2.GetSelectedDate());
          _ctl0__ctl0_GridNews_EditTemplate_0_3_Calendar2.Show(button);
        }
      }
      
      function Button_OnMouseUp2()
      {
        if (_ctl0__ctl0_GridNews_EditTemplate_0_3_Calendar2.PopUpObjectShowing)
        {
          event.cancelBubble=true;
          event.returnValue=false;
          return false;
        }
        else
        {
          return true;
        }
      }



</script>	




		
<COMPONENTART:GRID 
id="GridNews" 
runat="server" 
AutoFocusSearchBox="false"
AutoCallBackOnInsert="true"
AutoCallBackOnUpdate="true"
AutoCallBackOnDelete="true"
EditOnClickSelectedItem="true"
pagerposition="2"
ScrollBar="Off"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ScrollPopupClientTemplateId="ScrollPopupTemplate" 
Sort="creation_date desc"
Height="10" Width="99%" 
LoadingPanelPosition="MiddleCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateNews" 
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRowCategory" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="true" 
GroupingPageSize="1" 
PagerTextCssClass="GridFooterTextCategory" 
PagerStyle="Numbered" 
PageSize="10" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeader" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="true" 
AllowSorting="False"
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
ScrollBarWidth="15" 
AllowPaging="true" 
EnableViewState="true"
>
<ClientTemplates>
<ComponentArt:ClientTemplate Id="TypeIconTemplateNews">

            
          </ComponentArt:ClientTemplate>     
          <ComponentArt:ClientTemplate Id="EditTemplateNews">
          <a href="javascript:editGridJobs('## DataItem.GetMember("job_id").Text ##');"><img src="images/i_rate.gif" alt="Edit" border="0" > </a> | <a href="javascript:deleteRowNews('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
            
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateNews">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateNews">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>            
			<ComponentArt:ClientTemplate Id="NewsTemplate">
			<table width="100%" cellspacing="0" cellpadding="1" border="0">
			<tr>
				<td class="CellText" align="left"><a style="color:#595959;" href="#"><b><nobr>## DataItem.GetMember("job_title_new").Value ##</nobr></a></b></td>
			</tr>
			<tr>
				<td class="CellText" align="left"><font color="#595959">## DataItem.GetMember("description_new").Text ##</font></td>
			</tr>
			</table>
			</ComponentArt:ClientTemplate>    
					        
			<componentart:ClientTemplate ID="LoadingFeedbackTemplateNews">
			<table cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
			</tr>
			</table>
			</componentart:ClientTemplate>     
			                      
</ClientTemplates>

<ServerTemplates>
<ComponentArt:GridServerTemplate ID="yoyoyo">
            <Template>

 	   <table cellspacing="0" cellpadding="0" border="0">
	    <tr>
	      <td onmouseup="Button_OnMouseUp()">
<ComponentArt:Calendar id="Picker1" runat="server" PickerFormat="Custom" PickerCustomFormat="MMM dd yyyy" ControlType="Picker" SelectedDate="2005-12-24" ClientSideOnSelectionChanged="Picker1_OnDateChange" PickerCssClass="picker" /></td>
	      <td style="font-size:10px;">&nbsp;</td>
	      <td><img id="calendar_from_button" alt="" onclick="Button_OnClick(this)" onmouseup="Button_OnMouseUp()" class="calendar_button" src="images/btn_calendar.gif" /></td>
	    </tr>
	    </table>

		<ComponentArt:Calendar runat="server" id="Calendar1" AllowMultipleSelection="false" AllowWeekSelection="false" AllowMonthSelection="false" ControlType="Calendar" PopUp="Custom" PopUpExpandControlId="calendar_from_button" CalendarTitleCssClass="title" SelectedDate="2005-12-24" VisibleDate="2005-12-24" ClientSideOnSelectionChanged="Calendar1_OnChange" DayHeaderCssClass="dayheader" DayCssClass="day" DayHoverCssClass="dayhover" OtherMonthDayCssClass="othermonthday" SelectedDayCssClass="selectedday" CalendarCssClass="calendar" NextPrevCssClass="nextprev" MonthCssClass="month" SwapSlide="Linear" SwapDuration="300" DayNameFormat="FirstTwoLetters" PrevImageUrl="images/cal_prevMonth.gif" NextImageUrl="images/cal_nextMonth.gif" />
    		
    			

            </Template>
</ComponentArt:GridServerTemplate>


<ComponentArt:GridServerTemplate ID="yoyoyo2">
            <Template>

 	   <table cellspacing="0" cellpadding="0" border="0">
	    <tr>
	      <td onmouseup="Button_OnMouseUp2()">
<ComponentArt:Calendar id="Picker2" runat="server" PickerFormat="Custom" PickerCustomFormat="MMM dd yyyy" ControlType="Picker" SelectedDate="2005-12-24" ClientSideOnSelectionChanged="Picker1_OnDateChange2" PickerCssClass="picker" /></td>
	      <td style="font-size:10px;">&nbsp;</td>
	      <td><img id="calendar_from_button2" alt="" onclick="Button_OnClick2(this)" onmouseup="Button_OnMouseUp2()" class="calendar_button" src="images/btn_calendar.gif" /></td>
	    </tr>
	    </table>

		<ComponentArt:Calendar runat="server" id="Calendar2" AllowMultipleSelection="false" AllowWeekSelection="false" AllowMonthSelection="false" ControlType="Calendar" PopUp="Custom" PopUpExpandControlId="calendar_from_button2" CalendarTitleCssClass="title" SelectedDate="2005-12-24" VisibleDate="2005-12-24" ClientSideOnSelectionChanged="Calendar1_OnChange2" DayHeaderCssClass="dayheader" DayCssClass="day" DayHoverCssClass="dayhover" OtherMonthDayCssClass="othermonthday" SelectedDayCssClass="selectedday" CalendarCssClass="calendar" NextPrevCssClass="nextprev" MonthCssClass="month" SwapSlide="Linear" SwapDuration="300" DayNameFormat="FirstTwoLetters" PrevImageUrl="images/cal_prevMonth.gif" NextImageUrl="images/cal_nextMonth.gif" />

            </Template>
</ComponentArt:GridServerTemplate>











</ServerTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" ShowTableHeading="false" ShowSelectorCells="true" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateNews" InsertCommandClientTemplateId="InsertCommandTemplateNews" DataKeyField="job_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="asc.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
           
<Columns>
<componentart:GridColumn DataCellCssClass="FirstDataCell" HeadingText="Type" SortedDataCellCssClass="SortedDataCell" DataField="department_new" HeadingCellCssClass="FirstHeadingCell" ForeignTable="TypeTable" ForeignDataKeyField="Type_Id" ForeignDisplayField="Type_Value" Width="150" FixedWidth="True"></componentart:GridColumn>
<ComponentArt:GridColumn DataCellCssClass="DataCell" AllowSorting="false" HeadingText="Title" DataCellClientTemplateId="NewsTemplate" AllowEditing="false" Width="150" />
<ComponentArt:GridColumn DataField="post_date_new" HeadingText="Post" SortedDataCellCssClass="SortedDataCell" FormatString="MMM dd yyyy" EditControlType="Custom" EditCellServerTemplateId="yoyoyo" CustomEditSetExpression="setValue(DataItem)" CustomEditGetExpression="getValue()" width="200" FixedWidth="True"/>
<ComponentArt:GridColumn DataField="pull_date_new" HeadingText="Pull" SortedDataCellCssClass="SortedDataCell" FormatString="MMM dd yyyy" EditControlType="Custom" EditCellServerTemplateId="yoyoyo2" CustomEditSetExpression="setValue2(DataItem)" CustomEditGetExpression="getValue2()" width="200" FixedWidth="True"/>
<ComponentArt:GridColumn DataField="creation_date_new" AllowEditing="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Created" FormatString="MMM dd yyyy" AllowGrouping="False" Width="80" />
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" AllowSorting="false" HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplateNews" EditControlType="EditCommand" Align="Center"  />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="description_new" SortedDataCellCssClass="SortedDataCell" DataField="description_new"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="title" SortedDataCellCssClass="SortedDataCell" DataField="job_title_new"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="post_date_new" SortedDataCellCssClass="SortedDataCell" DataField="post_date_new"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="pull_date_new" SortedDataCellCssClass="SortedDataCell" DataField="pull_date_new"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="job_id" SortedDataCellCssClass="SortedDataCell" DataField="job_id"></componentart:GridColumn>

</Columns>
</componentart:GridLevel>
</Levels>


</COMPONENTART:GRID>

























										

		

      </div><!--END browsecenter-->
</div><!--END MAIN-->

<script language=javascript type=text/javascript>


function gotopagelink(subtype)
{
	var newurl = 'IDAM.aspx?Page=' + subtype;
	window.location.href = newurl;
	return true;
}
  

</script>

