<%@ Control Language="vb" AutoEventWireup="false" Codebehind="Category.ascx.vb" Inherits="IDAM5.Category" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<div class="previewpaneAll" id="toolbar">
	<!--Project cookie and top menu-->
	<table cellspacing="0" cellpadding="4" id="table2" width="100%">
		<tr>
			<td valign="top" width="67">
				<img style="BORDER-RIGHT:#cbcbcb 4px solid;BORDER-TOP:#cbcbcb 4px solid;BORDER-LEFT:#cbcbcb 4px solid;COLOR:#cbcbcb;BORDER-BOTTOM:#cbcbcb 4px solid;BACKGROUND-COLOR:#cbcbcb" src="<%=idam5.BaseIDAMSession.Config.URLLocationPreview%>?id=<%response.write(spCategoryImageID)%>&amp;instance=<%response.write(IDAMInstance)%>&amp;type=asset&amp;size=2&amp;height=90&amp;width=120">
			</td>
			<td valign="top">
				<font face="Verdana" size="1"><b>
						<%response.write(sCategoryName)%>
					</b>
					<br>
					<%if trim(spCategoryDescription) <> "" then%>
					<span style="FONT-WEIGHT: 400">
						<%response.write(spCategoryDescription)%>
						<br>
					</span>
					<%end if%>
					<b>Security Level:</b> <span style="FONT-WEIGHT: 400">
						<%response.write(spCategorySecurityLevel)%>
					</span></font>
			</td>
			<td valign="top" align="right">
				<div style="PADDING-TOP:2px">
				
				</div>
			</td>
		</tr>
		<tr>
			<td valign="top" colspan="3">
			</td>
		</tr>
	</table>
</div> <!--end preview pane-->
<div class="previewpaneSub">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle" width="100%">
				<div style="PADDING-RIGHT:0px;PADDING-LEFT:0px;PADDING-BOTTOM:0px;PADDING-TOP:0px;align:right">
					<asp:HyperLink CssClass="projecttabs" id="link_ProjectGeneral" NavigateUrl="javascript:gotopagelink('Browse');"
						runat="server">General</asp:HyperLink>&nbsp;|&nbsp;<!--&nbsp;&nbsp;<asp:HyperLink CssClass="projecttabs" id="link_ProjectPostings" NavigateUrl="javascript:gotopagelink('ProjectPostings');"
									runat="server">postings</asp:HyperLink>--><b><asp:HyperLink CssClass="projecttabs" id="link_ProjectDetails" NavigateUrl="javascript:gotopagelink('CategoryEdit');"
							runat="server">Edit</asp:HyperLink></b>&nbsp;|&nbsp;<asp:HyperLink id="link_ProjectPermissions" CssClass="projecttabs" NavigateUrl="javascript:gotopagelink('CategoryPermissions');"
						runat="server">Permissions</asp:HyperLink>
				</div>
			</td>
			<td id="Test" align="right" width="100%">
				<!--<asp:Image onclick="javascript:switchToThumbnail();" id="list_icon"   runat="server"></asp:Image><asp:Image onclick="javascript:switchToThumbnail();"  id="thumb_icon" runat="server"></asp:Image>-->
				<img src="images/spacer.gif" height="16">
			</td>
		</tr>
	</table>
</div>
<div class="previewpaneProjects" id="toolbar">
	<script language="javascript">

  
  function editGridCategory(rowId)
  {
    <% Response.Write(GridCategoryActive.ClientID) %>.Edit(<% Response.Write(GridCategoryActive.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  

  function onInsertCategory(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
    function ShowContextMenuCategory(item, column, evt) 
  {
    <% Response.Write(GridCategoryActive.ClientID) %>.Select(item);  
    return false; 
  }
  
  function onUpdateCategory(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }

  function onCallbackErrorCategory(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridCategoryActive.ClientID) %>.Page(1); 
  }

  function onDeleteCategory(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
  
  function editRowCategory()
  {
    <% Response.Write(GridCategoryActive.ClientID) %>.EditComplete();     
  }

  function insertRowCategory()
  {
    <% Response.Write(GridCategoryActive.ClientID) %>.EditComplete(); 
  }

  function deleteRowCategory(rowId)
  {
    <% Response.Write(GridCategoryActive.ClientID) %>.Delete(<% Response.Write(GridCategoryActive.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  
  


	</script>
	<div style="BORDER-RIGHT:0px solid;PADDING-RIGHT:5px;BORDER-TOP:0px solid;PADDING-LEFT:5px;PADDING-BOTTOM:5px;BORDER-LEFT:0px solid;PADDING-TOP:15px;BORDER-BOTTOM:0px solid"
		id="browsecenter">
		<img src="images/spacer.gif" height="15" width="5"><br>
		<div style="BORDER-RIGHT:#b7b4b4 1px solid;PADDING-RIGHT:5px;BORDER-TOP:#b7b4b4 1px solid;PADDING-LEFT:5px;FONT-WEIGHT:normal;FONT-SIZE:10px;PADDING-BOTTOM:5px;BORDER-LEFT:#b7b4b4 1px solid;COLOR:#3f3f3f;PADDING-TOP:5px;BORDER-BOTTOM:#b7b4b4 1px solid;FONT-FAMILY:verdana;BACKGROUND-COLOR:#e4e4e4">Enter 
			a name, description and security level to this category.</div>
		<br>
		<table cellspacing="0" cellpadding="3" border="0" width="100%">
			<tr>
				<td width="120" align="left" valign="top" nowrap>
					<font face="Verdana" size="1">Name: </font>
				</td>
				<td colspan="2" align="left" valign="top" width="100%">
					<asp:TextBox id="categoryName" runat="server" Width="99%"></asp:TextBox></td>
			</tr>
			<tr>
				<td class="PageContent" width="120" align="left" valign="top" nowrap>
					<font face="Verdana" size="1">Security Level</font></td>
				<td class="PageContent" width="100%" align="left" valign="top">
					<asp:DropDownList ID="SecurityLevel" width="180" Runat="server">
						<asp:ListItem Value="0">Administrator</asp:ListItem>
						<asp:ListItem Value="1">Internal Use</asp:ListItem>
						<asp:ListItem Value="2">Client Use</asp:ListItem>
						<asp:ListItem Value="3">Public</asp:ListItem>
					</asp:DropDownList>
				</td>
				<td class="PageContent" width="80%" align="left" valign="top">
					<input type="checkbox" name="chkactive" checked value="ON">Active</td>
			</tr>
			<tr>
				<td class="PageContent" width="120" align="left" valign="top" nowrap>
					<font face="Verdana" size="1">Description</font></td>
				<td class="PageContent" colspan="2" align="left" valign="top" width="100%">
					<asp:TextBox id="categoryDescription" TextMode="MultiLine" runat="server" Height="300px" Width="99%"></asp:TextBox></td>
			</tr>
		</table>
		<!--
		
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Change permissions for this object by selecting a group or user from the list and adding to the active list.</div><br>
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td class="PageContent" width="250" valign=top>

<b>Available Permissions</b><br>



<COMPONENTART:GRID 
id="GridCategoryAvailable" runat="server" 
pagerposition="2"
ScrollBar="Off"
Sort="lastname asc"
Height="10" Width="250" 
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategory" 
AlternatingRowCssClass="AlternatingRow" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PagerTextCssClass="GridFooterText" 
PagerStyle="Numbered" 
PageSize="10" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeaderCategory" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText"  
ShowSearchBox="true" 
ShowHeader="true" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
AllowPaging="true"
cachecontent="false" 
>
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategory">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategory">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                            
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
            EditFieldCssClass="EditDataField"
            EditCommandClientTemplateId="EditCommandTemplateCategory"
            InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataCellClientTemplateId="TypeIconTemplateCategory" DataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Firstname"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="firstname" Width="70"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Lastname"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="lastname" Width="100"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" AllowEditing="false" AllowGrouping="False"  Width="120" SortedDataCellCssClass="SortedDataCell" DataField="description"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
					</td>
					<td class="PageContent" width="120" valign= middle>
		
					<asp:Button id="btnAddPermissions" runat="server" Text="     Add >> "></asp:Button><br><br><br><br>
					<asp:Button id="btnRemovePermissions" runat="server" Text="<< remove"></asp:Button>
					</td>
					<td class="PageContent" width="100%" valign=top>
					
					
					
					

<b>Active Permissions</b><br>


<COMPONENTART:GRID id="GridCategoryActive" runat="server" 
autoPostBackonDelete="true"
pagerposition="2"
ScrollBar="Off"
Sort="typeofobject asc"
Height="10" Width="100%" 
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategoryActive" 
AlternatingRowCssClass="AlternatingRow" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PagerTextCssClass="GridFooterText" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeaderCategory" 
SearchOnKeyPress="false" 
SearchTextCssClass="GridHeaderText"  
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
AllowPaging="false"
cachecontent="false" 
PagerStyle="Numbered" 
PageSize="500"  >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategoryActive">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateCategoryActive">
          <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategoryActive">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                                
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" 
 EditFieldCssClass="EditDataField" 
 EditCommandClientTemplateId="EditCommandTemplateCategoryActive" 
 DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataField="imagesource" DataCellClientTemplateId="TypeIconTemplateCategoryActive" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True"  />
<componentart:GridColumn AllowEditing="false" HeadingText="Name" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" FixedWidth="false"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Description" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="description" width="100" FixedWidth="true"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
					</td>					
				</tr>
			</table>
		-->
		<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
			<asp:Button id="btnCategorySave" runat="server" Text="Save"></asp:Button>&nbsp;&nbsp;
		</div>
	</div>
</div>
<DIV></DIV>
<DIV></DIV>
