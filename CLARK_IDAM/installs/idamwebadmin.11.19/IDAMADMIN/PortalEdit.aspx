<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PortalEdit.aspx.vb" Inherits="IDAM5.PortalEdit" ValidateRequest="False"%>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Quick Create/Edit Category</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
			<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
				<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
					<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
						<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
							<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
								<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
									<link href="treeStyle.css" type="text/css" rel="stylesheet">
										<link href="navStyle.css" type="text/css" rel="stylesheet" >
										    <link href="tabStripStyle.css" type="text/css" rel="stylesheet" />    
												<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
	<style>BODY { MARGIN: 0px }
	</style></HEAD>
			
										
	<body MS_POSITIONING="GridLayout" onload="this.focus">
		<form id="Form" method="post" runat="server">
		
		
		
		<script language=javascript>

  


  function onInsertCategory(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }

  function onUpdateCategory(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }


  


</script>
		
<script type="text/javascript">
  

<%if request.querystring("newid") <> "" then%>
window.close();
<%end if%>

</script>

			<%response.write (request.querystring("newid"))%>
			<input type="hidden" name="catid" id="catid" value="<%response.write (replace(request.querystring("id"),"POR",""))%>">
			<input type="hidden" name="parent_id" id="parent_id" value="<%response.write (request.querystring("parent_id"))%>">
			<div style="padding:20px;position:relative;">
			<div style="width:100%">
			<div style="padding:10px;border:1px solid;background-color:white;position:relative;">
			<table><tr><td><img src="images/top_folder.gif"></td><td>Create/Edit Portal</td></tr></table><br>
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Create/Modify a portal by entering a portal name and optional description.  </div><br>
  <div style="width=100%">

			
			
			
	
			
			
			
			
			
	<COMPONENTART:TABSTRIP id="TabStripCategory" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataPortal.xml"
		CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
		DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
		<ItemLooks>
			<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10" 
 LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif" 
 HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23" 
 LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
			<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10" 
 LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif" 
 LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
		</ItemLooks>
	</COMPONENTART:TABSTRIP>
	
	<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageCategoryPage" runat="server">
		<ComponentArt:PageView CssClass="PageContent2" runat="server" ID="Pageview1">
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Enter a name, description and security level for this portal.</div>
			<br>
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">Name </font>
					</td>
					<td colspan="2" align="left" valign="top" width=100%>
						<asp:TextBox id="categoryName" runat="server" CssClass="InputFieldMain"  Height=25px></asp:TextBox><font style="font-family: verdana; color: red;font-size: 10px;font-weight: bold;"><asp:RequiredFieldValidator  id="RequiredFieldValidator2" runat="server" ErrorMessage="[!] Please fill in." ControlToValidate="categoryName"></asp:RequiredFieldValidator></font>
						</td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">Security Level</font></td>
					<td class="PageContent"  width=100% align="left" valign="top">
						<asp:DropDownList ID="SecurityLevel" CssClass="InputFieldMain"  Height=25px Runat="server">
							<asp:ListItem Value="0">Administrator</asp:ListItem>
							<asp:ListItem Value="1">Internal Use</asp:ListItem>
							<asp:ListItem Value="2">Client Use</asp:ListItem>
							<asp:ListItem Value="3">Public</asp:ListItem>
						</asp:DropDownList>
					</td>
					<td class="PageContent" width="80%" align="left" valign="top">
						<asp:CheckBox  runat=server id="chkactive" Checked=True></asp:CheckBox>Active</td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">Description</font></td>
					<td class="PageContent" colspan="2" align="left" valign="top"  width=100%>
						<asp:TextBox id="categoryDescription" TextMode="MultiLine" runat="server" CssClass="InputFieldMain100P"  Style="height:200px" Height="200px" ></asp:TextBox>
						<font style="font-family: verdana; color: red;font-size: 10px;font-weight: bold;"><asp:RequiredFieldValidator  id="Requiredfieldvalidator3" runat="server" ErrorMessage="[!] Please fill in." ControlToValidate="categoryDescription"></asp:RequiredFieldValidator></font></td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1"><b>Options</b></font></td>
					<td class="PageContent" colspan="2" align="left" valign="top"  width=100%>
						<hr noshade ></td>
				</tr>				
				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">Heading</font></td>
					<td class="PageContent" colspan="2" align="left" valign="top"  width=100%>
						<asp:TextBox id="categoryHeading" TextMode="SingleLine" runat="server" CssClass="InputFieldMain"  Height=25px></asp:TextBox>
						<font style="font-family: verdana; color: red;font-size: 10px;font-weight: bold;"><asp:RequiredFieldValidator  id="Requiredfieldvalidator4" runat="server" ErrorMessage="[!] Please fill in." ControlToValidate="categoryHeading"></asp:RequiredFieldValidator></font></td>
				</tr>
				
				
				
			</table>
		</ComponentArt:PageView>
		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Category_Members">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Change permissions for this object by selecting a group or user from the available list and adding to the active list.</div><br>






								
								
								
						
																				
		
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td class="PageContent" width="360" valign=top>
<!--available list-->
<b>Available Groups/Users</b><br><br>



<COMPONENTART:GRID id="GridPermissionAvailable" runat="server" pagerposition="2" ScrollBar="Off" Sort="lastname asc" Height="10" Width="250" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategory" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" AllowPaging="true" cachecontent="false" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategory">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategory">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                            
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategory" InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataCellClientTemplateId="TypeIconTemplateCategory" DataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Firstname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="firstname" Width="70"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Lastname" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="lastname" Width="100"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" DataCellCssClass="LastDataCell" AllowEditing="false" AllowGrouping="False" Width="120" SortedDataCellCssClass="SortedDataCell" DataField="description"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<div style=  "text-align:right;BORDER-RIGHT: 1px solid; PADDING-RIGHT: 4px; BORDER-TOP: 1px solid; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: 1px solid; PADDING-TOP: 4px; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: gainsboro">
<b>Search for user/group:</b> <input Class="InputFieldMain" type=text value="" id=search_filter  style="width:120px" onkeyup="javascript:<%response.write( GridPermissionAvailable.ClientId)%>.Filter('firstname LIKE \'%' + this.value + '%\' or lastname LIKE \'%' + this.value + '%\'');">
</div>
					</td>
					<td class="PageContent" width="120" valign= middle>

		<asp:ImageButton ImageUrl="images/bt_u_fwd.gif" Runat=server id="imgbtnAddPermissionsnew"></asp:ImageButton><br><br>
		<asp:ImageButton ImageUrl="images/bt_u_rwd.gif" Runat=server id="imgbtnRemovePermissions"></asp:ImageButton> 
					
					</td>
					<td class="PageContent" width="100%" valign=top>
					
					
					
					
<!--active list-->
<b>Active Groups/Users</b><br><br>


<COMPONENTART:GRID id="GridPermissionActive" runat="server" autoPostBackonDelete="true" PageSize="10" pagerposition="2" ScrollBar="Off" Sort="typeofobject asc" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategoryActive" AlternatingRowCssClass="AlternatingRow" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PagerTextCssClass="GridFooterText" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="false" SearchTextCssClass="GridHeaderText" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" AllowPaging="true" cachecontent="false" PagerStyle="Numbered"  >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategoryActive">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateCategoryActive">
          <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategoryActive">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>                                
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategoryActive" DataKeyField="uniqueid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataField="imagesource" DataCellClientTemplateId="TypeIconTemplateCategoryActive" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True"  />
<componentart:GridColumn AllowEditing="false" HeadingText="Name" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" FixedWidth="false"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Description" DataCellCssClass="LastDataCell" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="description" width="100" FixedWidth="true"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="type" SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
					</td>					
				</tr>
			</table>													
							




















		</ComponentArt:PageView>
	</ComponentArt:MultiPage>
	<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
					<div style="text-align:right;"><br>
			<asp:Button id="btnCategorySave" runat="server" cssclass="button" Text="Save"></asp:Button><img src="images/spacer.gif" width=5 height=1><input type="button" cssclass="button" onclick="window.close();" value="Close" />
			</div>
	</div>
</div>

			
			
			
			

			</div>
			
			
			
			
			
			
			
			
			</div>
			

			
			</div>
			</div>
			
	<script language=javascript >
TabStripCategory.SelectTabById('<%response.write (request.querystring("tab"))%>');


<%if bCheckNewProject then%>

<%end if%>


</script>	
			
			
		</form>
	</body>
</HTML>
