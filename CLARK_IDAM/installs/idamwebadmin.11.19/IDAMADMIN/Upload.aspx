<%@ Register TagPrefix="radu" Namespace="Telerik.WebControls" Assembly="RadUpload" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Upload.aspx.vb" Inherits="IDAM5.Upload"%>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Upload</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
		<link href="treeStyle.css" type="text/css" rel="stylesheet">
		<link href="navStyle.css" type="text/css" rel="stylesheet">
		<link href="tabStripStyle.css" type="text/css" rel="stylesheet">
		<link href="navBarStyle.css" type="text/css" rel="stylesheet">
		<style>
		</style>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onload="this.focus">
	
	<script type="text/javascript">
		<%if request.querystring("closewindow") <> "" then%>
		window.opener.NavigateToProjectFolder('<%response.write (request.querystring("projectid"))%>','<%response.write (request.querystring("categoryid"))%>');
		window.close();
		<%end if%>
	</script>
	


			<div style="PADDING-RIGHT:10px;PADDING-LEFT:10px;PADDING-BOTTOM:10px;PADDING-TOP:10px">
				<div style="WIDTH:100%">
					<div style="BORDER-RIGHT:1px solid; PADDING-RIGHT:10px; BORDER-TOP:1px solid; PADDING-LEFT:10px; PADDING-BOTTOM:10px; BORDER-LEFT:1px solid; PADDING-TOP:10px; BORDER-BOTTOM:1px solid; POSITION:relative; BACKGROUND-COLOR:white">
						<div style="BORDER-RIGHT:#b7b4b4 1px solid; PADDING-RIGHT:5px; BORDER-TOP:#b7b4b4 1px solid; PADDING-LEFT:5px; FONT-WEIGHT:normal; FONT-SIZE:10px; PADDING-BOTTOM:5px; BORDER-LEFT:#b7b4b4 1px solid; WIDTH:100%; COLOR:#3f3f3f; PADDING-TOP:5px; BORDER-BOTTOM:#b7b4b4 1px solid; FONT-FAMILY:verdana; BACKGROUND-COLOR:#e4e4e4"> 
							Upload assets to the following location.</div>
						<br>
						<table><tr><td width=400>
						<div style="WIDTH: 100%">
							<br>
							Upload asset to project: [ <b>
								<asp:Label id="LabelProjectName" runat="server">Label</asp:Label></b> ]<br>
							Category: [ <b>
								<asp:Label id="LabelCategoryName" runat="server">Label</asp:Label></b> ]<br><br>
							<asp:PlaceHolder id="PlaceHolderUpload" runat="server"></asp:PlaceHolder>
						</div>
						</td ><td valign=top>
						<img src=images/spacer.gif height=75 width=1><br>
						<div style="BORDER-RIGHT:#b7b4b4 1px solid; PADDING:8px; BORDER-TOP:#b7b4b4 1px solid;  FONT-WEIGHT:normal; FONT-SIZE:10px; BORDER-LEFT:#b7b4b4 1px solid; WIDTH:100%; COLOR:#3f3f3f; BORDER-BOTTOM:#b7b4b4 1px solid; FONT-FAMILY:verdana; BACKGROUND-COLOR:#e4e4e4"> 
							Upload an asset by clicking on the select button and choosing a file from the browse dialog box.  You may upload more than one asset by repeating the acting as many times as needed.  To begin the upload process, simply click the 'upload' button at the bottom of the list.  A progress indicator will show once you begin the upload progress.  Note:  Maximum file size is 2Gb.  The time it takes to upload the asset(s) depends on your network connectivity.  Remote access may greatly reduce upload performance. </div>
						
						</td></tr></table>

					</div>
				</div>
				
				
				
	<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
					<div style="text-align:right;"><br>
			<img src="images/spacer.gif" width=5 height=1><input type="button" onclick="window.close();" value="Close" />
			</div>
	</div>					
			</div>

	</body>
</HTML>
