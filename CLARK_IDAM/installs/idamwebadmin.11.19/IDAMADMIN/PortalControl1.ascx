<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="PortalControl1.ascx.vb" Inherits="IDAM5.PortalControl1" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script language="javascript">
var xoffsetpopup
xoffsetpopup = 50
var yoffsetpopup 
yoffsetpopup = -300
</script>
<script language="javascript"  src="js/filesearchhover.js"/>
<style>
.projectcentertest
{
width:100%;
height:100%;
/*z-index:100;*/
/*padding-left:3px;
padding-right:3px;*/
}

</style>

<script type="text/javascript">
//<![CDATA[    ]

//]]>
</script>
<script type="text/javascript">
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridPortlet.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    <%response.write(GridPortlet.ClientID)%>.Edit(<%response.write(GridPortlet.ClientID)%>.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    <%response.write(GridPortlet.ClientID)%>.EditComplete();     
  }

  function insertRow()
  {
    <%response.write(GridPortlet.ClientID)%>.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    <%response.write(GridPortlet.ClientID)%>.Delete(<%response.write(GridPortlet.ClientID)%>.GetRowFromClientId(rowId)); 
  }
</script>
<div style="padding-top:2px;padding-bottom:8px;position:relative;"><!--top-->
<div style="padding:1px;border:0px solid ;background-color:white;position:relative;"><!--inner-->
<div id="<%response.write(GridPortlet.ClientID)%>_toptitlebar" style=";padding:5px;background-color: #E4E4E4;border:1px solid silver;  font-family: verdana; color: #3F3F3F; font-size: 10px;  font-weight: bold;"><%=sHeadingVal%></div>
  <div style="padding:5px;border:0px solid silver;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;"><%=sDescriptionVal%></div>
  <%=sContentVal%>

<asp:repeater ID="repeater_rssfeed" runat="server" Visible=False>
    <ItemTemplate>
        <b><a href="<%# DataBinder.Eval(Container.DataItem, "link") %>"><%# DataBinder.Eval(Container.DataItem, "title") %></a></b>
        <%# DataBinder.Eval(Container.DataItem, "description") %><br /><br />
    </ItemTemplate>
</asp:repeater>
   
<div style="font-family: verdana; color: #3F3F3F; font-size: 10px;  font-weight: normal;"><!--Grid-->
	<asp:Literal Runat=server ID=searchportletinput></asp:Literal>
			<ComponentArt:Grid id="GridPortlet" RunningMode="Callback" CssClass="Grid" GroupByTextCssClass="GroupByText"
				GroupingNotificationTextCssClass="GridHeaderText" ShowFooter="true" PageSize="20" ImagesBaseUrl="images/"
				EditOnClickSelectedItem="true" AllowEditing="false" ScrollBar="Off" ScrollTopBottomImagesEnabled="true"
				ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/"
				ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip"
				ScrollBarWidth="16" ScrollPopupClientTemplateId="ScrollPopupTemplate" FillWidth="true" Width="100%" Height="207" AutoCallBackOnDelete="true"  
				AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true"  runat="server" PagerStyle="Numbered"
				PagerTextCssClass="PagerText">
				<Levels>
					<ComponentArt:GridLevel DataKeyField="id" ShowTableHeading="false" ShowSelectorCells="false" HeadingCellCssClass="HeadingCell"
						HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive" HeadingTextCssClass="HeadingCellText"
						DataCellCssClass="DataCell" RowCssClass="Row" SelectedRowCssClass="SelectedRow" SortAscendingImageUrl="asc.gif"
						SortDescendingImageUrl="desc.gif" ColumnReorderIndicatorImageUrl="reorder.gif" SortedDataCellCssClass="SortedDataCell"
						SortImageWidth="14" SortImageHeight="14" EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField"
						EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate">
						<Columns>
							<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings"  DataField="id" HeadingImageUrl="icon_icon.gif" FixedWidth="True" Width="50"/>
							<ComponentArt:GridColumn DataField="title" HeadingText="Name" Width="100"/>
							<ComponentArt:GridColumn DataField="description" HeadingText="Details" Width="180" />
							<ComponentArt:GridColumn DataField="id" HeadingText="Actions" Width="18" FixedWidth="True" Visible="False" />
							<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" FixedWidth="True" Width="10" />
							<ComponentArt:GridColumn  DataField="type" HeadingText="type" Width="20" visible="False"/>
							<ComponentArt:GridColumn  DataField="imageurl" Visible="False" HeadingText="image" Width="50" />
							<ComponentArt:GridColumn  DataField="link" Visible="False" HeadingText="link" Width="50" />
							<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" FormatString="MMM dd yyyy, hh:mm tt" DataField="date" HeadingText="date" visible="False"  />
						</Columns>
					</ComponentArt:GridLevel>
				</Levels>
				<ClientTemplates>
			
					<ComponentArt:ClientTemplate Id="TypeIconTemplateComments">
						<a href="#" onclick="GoToObjectLocation('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("type").Value ##');"><img src="## DataItem.GetMember("imageurl").Value ##&width=45&height=45" border="0"></a>
					</ComponentArt:ClientTemplate> 
						    
					<ComponentArt:ClientTemplate Id="CommentTemplate">

					</ComponentArt:ClientTemplate>  
						
					<ComponentArt:ClientTemplate Id="CTThumbPT1">
						<a href="#" onclick="GoToObjectLocation('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("type").Value ##');"><img src="## DataItem.GetMember("imageurl").Value ##&width=45&height=45" border="0" onmouseover="javascript:showtrailpreload('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("title").Value ##');PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("title").Value ##','## DataItem.GetMember("imageurl").Value ##',event);" onmouseout="javascript:hidetrail();" ></a>
					</ComponentArt:ClientTemplate>																																		
					
					<ComponentArt:ClientTemplate Id="CTNamePT1">
						
					</ComponentArt:ClientTemplate> 					
						    
					<ComponentArt:ClientTemplate Id="CTDescriptionPT1">
					<b></b> <a href="#" onclick="GoToObjectLocation('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("type").Value ##');">## DataItem.GetMember("title").Value ##</a>
						<table width="100%" cellspacing="0" cellpadding="1" border="0">
						
						<tr>
							<td class="CellText" align="left"><b></b> <font color="#595959"><nobr>## DataItem.GetMember("date").Text ##</nobr></font></td>
						</tr>
						<tr>
							<td class="CellText" align="left"><b></b> <a style="color:#595959;" href="#"><b><nobr>## DataItem.GetMember("description").Value ##</nobr></a></b></td>
						</tr>
						</table>
					</ComponentArt:ClientTemplate>  
					
					<ComponentArt:ClientTemplate Id="CTActionsPT1">
						<a href="javascript:DownloadAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> 
					</ComponentArt:ClientTemplate>
										
					<ComponentArt:ClientTemplate Id="CTThumbPT2">
						<a href="#" onclick="GoToObjectLocation('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("type").Value ##');"><img src="## DataItem.GetMember("imageurl").Value ##&width=100&height=100" border="0" ></a>
					</ComponentArt:ClientTemplate> 
					
					<ComponentArt:ClientTemplate Id="CTNamePT2">
						<b>Name:</b> <a href="#" onclick="GoToObjectLocation('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("type").Value ##');">## DataItem.GetMember("title").Value ##</a>
					</ComponentArt:ClientTemplate> 
						    
					<ComponentArt:ClientTemplate Id="CTDescriptionPT2">
						<b></b> <a href="#" onclick="GoToObjectLocation('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("type").Value ##');">## DataItem.GetMember("title").Value ##</a>
						<table width="100%" cellspacing="0" cellpadding="1" border="0">
						
						<tr>
							<td class="CellText" align="left"><font color="#595959"><nobr>## DataItem.GetMember("date").Text ##</nobr></font></td>
						</tr>
						<tr>
							<td class="CellText" align="left"><a style="color:#595959;" href="#"><b><nobr>## DataItem.GetMember("description").Value ##</nobr></a></b></td>
						</tr>
						</table>
					</ComponentArt:ClientTemplate>  
					
					<ComponentArt:ClientTemplate Id="CTThumbPT5">
						<a href="#" onclick="GoToObjectLocation('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("type").Value ##');"><img src="## DataItem.GetMember("imageurl").Value ##&width=200&height=160" border="0"  onmouseover="javascript:showtrailpreload('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("title").Value ##');PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("title").Value ##','## DataItem.GetMember("imageurl").Value ##',event);" onmouseout="javascript:hidetrail();"></a>
						<br><b></b> <a href="#" onclick="GoToObjectLocation('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("type").Value ##');">## DataItem.GetMember("title").Value ##</a>
						
						<br><b></b> ## DataItem.GetMember("date").Value ##
						<br><b></b> ## DataItem.GetMember("description").Value ##
					</ComponentArt:ClientTemplate> 
					
					<ComponentArt:ClientTemplate Id="CTThumbPT6">
						<img src="images/ui/carousel_ico_bg.gif" border="0">
					</ComponentArt:ClientTemplate> 
					
					<ComponentArt:ClientTemplate Id="CTNamePT6">
						<b></b> <a href="#" onclick="GoToObjectLocation('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("type").Value ##');">## DataItem.GetMember("title").Value ##</a>
						<br><b></b> ## DataItem.GetMember("description").Value ##
					</ComponentArt:ClientTemplate> 
						    
					<ComponentArt:ClientTemplate Id="CTThumbPT7">
						<a href="#" onclick="GoToObjectLocation('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("type").Value ##');"><img src="## DataItem.GetMember("imageurl").Value ##&width=45&height=45" border="0"></a>
					</ComponentArt:ClientTemplate> 
					
					<ComponentArt:ClientTemplate Id="CTNamePT7">
						<b></b> <a href="#" onclick="GoToObjectLocation('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("type").Value ##');">## DataItem.GetMember("title").Value ##</a>
						
						<br><b></b> ## DataItem.GetMember("date").Value ##
						<br><b></b> ## DataItem.GetMember("description").Value ##
					</ComponentArt:ClientTemplate> 
					
					<ComponentArt:ClientTemplate Id="CTGeneric100">
					    <a href="#" onclick="GoToObjectLocation('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("type").Value ##');"><img src="## DataItem.GetMember("imageurl").Value ##&width=620&height=540" border="0" onmouseover="javascript:showtrailpreload('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("title").Value ##');PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("title").Value ##','## DataItem.GetMember("imageurl").Value ##',event);" onmouseout="javascript:hidetrail();" ></a>
						<br><b></b> <a href="#" onclick="GoToObjectLocation('## DataItem.GetMember("id").Value ##','## DataItem.GetMember("type").Value ##');">## DataItem.GetMember("title").Value ##</a>
						<br><b></b> ## DataItem.GetMember("date").Value ##
						<br><b></b> ## DataItem.GetMember("description").Value ##
					</ComponentArt:ClientTemplate> 
					
					
					
					<ComponentArt:ClientTemplate Id="LoadingFeedbackTemplate">
						<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td style="font-size:10px;">Loading...&nbsp;</td>
								<td><img src="images/spinner.gif" width="16" height="16" border="0"></td>
							</tr>
						</table>
					</ComponentArt:ClientTemplate>
					
				</ClientTemplates>
			</ComponentArt:Grid>
			</div> <!--Grid-->
			</div> <!--inner-->
			</div> <!--top-->
<script type="text/javascript">
<% select case ObjectType
	case 2,3,4
	%>
	document.getElementById('<%response.write(GridPortlet.ClientID)%>_toptitlebar').style.display='none';
	<%
end select%>

//setup form switch for delete or multi asset zip download
function DownloadAssets(id){
	window.location = '<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownload & "?dtype=assetdownload&assetid="%>' + id.replace('0 ','') + '<%="&instance=" & idam5.BaseIDAMSession.Config.IDAMInstance%>&size=0';
	return true;

}
function GoToObjectLocation (id,type)
{
var URL;
URL='<%=Request.Url.Scheme + Request.Url.SchemeDelimiter + Request.Url.Authority + Request.Url.AbsolutePath%>'
switch(type)
 { 
 case 'asset': URL+='?Page=Asset&id=' + id;break;
 case 'project': URL+='?Page=Project&id=' + id;break;
 }
window.location=URL;
return true;
}
</script>
							
							