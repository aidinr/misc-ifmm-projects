<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="MyProjects.ascx.vb" Inherits="IDAM5.MyProjects" EnableViewState="False" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div class="previewpaneMyProjects" id="toolbar">
	<!--Project cookie and top menu-->
		<table cellspacing="0" cellpadding="4" id="table2" width="100%">
			<tr>
			<td width="1" nowrap valign="top">
			<img src="images/ui/home_ico_bg.gif" height=42 width=42>
			
			
			</td>
			
			<td valign="top">
			<font face="Verdana" size="3" ><b>Welcome, <%=Session("user_name")%>!</b></font><font face="Verdana" size="1" > [ <a href="login.aspx?Logout=True">log out</a> ]<br>
				<font face="Verdana" size="1">Your homepage contains information relevant to you.  
				
				</font></font>
				
				
				
				
				
			</td>
			<td id="Test" valign="top" align="right">
			<font face="Verdana" size="1" ><script>writeDate();</script></font>
			<ComponentArt:Menu id="MenuControl" CssClass="TopGroup" DefaultGroupCssClass="MenuGroup" SiteMapXmlFile="menuDataMenuControlMyProjects.xml"
								DefaultItemLookID="DefaultItemLook" TopGroupItemSpacing="1" DefaultGroupItemSpacing="2" ImagesBaseUrl="images/"
								ExpandDelay="100" runat="server">
								<ItemLooks>
									<ComponentArt:ItemLook LookID="EmptyLook" />
									<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" LeftIconUrl="check.gif" LeftIconWidth="15" LeftIconHeight="10" LabelPaddingLeft="8" LabelPaddingRight="12" LabelPaddingTop="3" LabelPaddingBottom="4" />
								</ItemLooks>
							</ComponentArt:Menu>
							
			<!--<div style="padding-top:2px;">
			[ <a href="#">help</a> ]
				</div>-->
			</td></tr>
			<tr><td valign="top" colspan="2">
			</td>
			</tr>
		</table>
</div> <!--end preview pane-->


<div class="previewpaneSub">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle" width="100%" style="HEIGHT: 20px">
			
			<div style="PADDING-RIGHT:0px;PADDING-LEFT:0px;PADDING-BOTTOM:0px;PADDING-TOP:0px;align:right">
								<b><asp:HyperLink CssClass="projecttabs" id="link_ProjectGeneral" NavigateUrl="javascript:gotopagelink('MyProjects');"
									runat="server">My Dashboard</asp:HyperLink></b>
							</div>
			</td>
			<td align="right"  width="100%" style="HEIGHT: 20px">
				<!--<asp:Image onclick="javascript:switchToThumbnail();" id="list_icon"   runat="server"></asp:Image><asp:Image onclick="javascript:switchToThumbnail();"  id="thumb_icon" runat="server"></asp:Image>-->
				<img src="images/spacer.gif" height=16 width=1>
			</td>
		</tr>
	</table>
</div>
<style>
.projectcentertest { Z-INDEX: 100; WIDTH: 100%; HEIGHT: 100% }
</style>
<script language="javascript">
var xoffsetpopup
xoffsetpopup = 150
var yoffsetpopup 
yoffsetpopup = -200
</script>

<script language="javascript" type="text/javascript">

  function onUpdate(item)
  {
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
  }
  function onInsert(item)
  {
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
  }
  function onCallbackErrorProject(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridProjects.ClientID) %>.Page(1); 
  }
  function onDelete(item)
  {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }	
function editRowProject()
  {
    <% Response.Write(GridProjects.ClientID) %>.EditComplete();     
  }
  
function editRowRecent()
  {
    <% Response.Write(GridRecent.ClientID) %>.EditComplete();     
  }
  
  function editGridAssets(RowId)
  {
    itemRow = <% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(RowId);
	itemvaluetmp = itemRow.GetMember('asset_id').Value;
    window.location = 'IDAM.aspx?page=Asset&ID=' + itemvaluetmp + '&type=asset'
  }
  
    function editGridRecent(RowId)
  {
    itemRow = <% Response.Write(Gridrecent.ClientID) %>.GetRowFromClientId(RowId);
	itemvaluetmp = itemRow.GetMember('asset_id').Value;
    window.location = 'IDAM.aspx?page=Asset&ID=' + itemvaluetmp + '&type=asset'
  }


  
function editRowMyAssets()
  {
    <% Response.Write(GridAssets.ClientID) %>.EditComplete();     
  }
 
 
function editGridProject(rowId)
{
<% Response.Write(GridProjects.ClientID) %>.Edit(<% Response.Write(GridProjects.ClientID) %>.GetRowFromClientId(rowId)); 
}
 
function deleteRowProject(rowId)
  {
    <% Response.Write(GridProjects.ClientID) %>.Delete(<% Response.Write(GridProjects.ClientID) %>.GetRowFromClientId(rowId)); 
  } 
  
  
function deleteRowRecent(rowId)
  {
  
  if (confirm("Delete asset?"))
  {
        <% Response.Write(GridRecent.ClientID) %>.Delete(<% Response.Write(GridRecent.ClientID) %>.GetRowFromClientId(rowId)); 
        }
      else {

    }
  } 
  
function deleteRowMyAssets(rowId)
  {
    if (confirm("Delete asset?"))
        <% Response.Write(GridAssets.ClientID) %>.Delete(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(rowId)); 
      else
        return false; 
    
  } 
  
  
function ToggleSnapMinimize(SnapObject, MenuItemIndex)
{
    SnapObject.ToggleMinimize();
    ToggleItemCheckedState(MenuItemIndex); 
}    
 
  
function ToggleItemCheckedState(MenuItemIndex)
{

    var item = _ctl1_MenuControl.Items(0).Items(MenuItemIndex); 
	if (item.GetProperty('Look-LeftIconUrl') == 'check.gif')
	{
		item.SetProperty('Look-LeftIconUrl','clear.gif');
		//set cookie to false
		SetCookie ('WindowMyProjects' + MenuItemIndex, 0, exp);
		
	}
	else
	{
		item.SetProperty('Look-LeftIconUrl','check.gif');
		//set cookie to true
		SetCookie ('WindowMyProjects' + MenuItemIndex, 1, exp);
	}
	item.ParentMenu.Render();
	
}





 
function showMenu(id, eventObj) {

    var elmobj = document.getElementById(id);
    var xoffsettype = 260;
   	if (id!='projectlookpupdropdown')
	{
	xoffsettype += 35;
	//hide others
	document.getElementById('projectlookpupdropdown').style.display='none';
	} else
	{
	document.getElementById('categorylookpupdropdown').style.display='none';
	}
	ypos=this.<%response.write (SNAPUploadWizard.ClientID)%>.Frame.offsetTop+xoffsettype;
	xpos=this.Splitter1.Panes[0].Width+28+ truebody().scrollLeft;
    elmobj.style.posLeft=xpos+"px";
    elmobj.style.posTop=ypos+"px";
    elmobj.style.left=xpos+"px";
    elmobj.style.top=ypos+"px";   
    
    
 
    
    if (elmobj.style.display=='block')
		{
		if (eventObj.type!='keyup'){
		    elmobj.style.display='none';
		    }
		} else {
			elmobj.style.display='block';
		}
}



  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

function showcategorylookup(){
var elmprojectlookpupdropdown = document.getElementById('categorylookpupdropdown')
if (elmprojectlookpupdropdown.style.display=='block') {
elmprojectlookpupdropdown.style.display='none';
} else {
elmprojectlookpupdropdown.style.display='block';
}
}

function showprojectlookupforce(){
var elmprojectlookpupdropdown = document.getElementById('projectlookpupdropdown')
//elmprojectlookpupdropdown.style.display='block';
//document.getElementById('_ctl0_SNAPUploadWizard_project_id').value='';

if (document.getElementById('<%response.write (SNAPUploadWizard.ClientID)%>_txtProjectNameDropDown').value.length!=1) {
<%response.write (GridProjectsUpload.ClientID)%>.Filter('name LIKE \'%' + document.getElementById('<%response.write (SNAPUploadWizard.ClientID)%>_txtProjectNameDropDown').value + '%\'');
}
}


function showcategorylookupforce(){
var elmprojectlookpupdropdown = document.getElementById('categorylookpupdropdown')
elmprojectlookpupdropdown.style.display='block';
document.getElementById('<%response.write (SNAPUploadWizard.ClientID)%>_category_id').value='';

}



	
function selectprojectfromdropdown(item)
  {
	var itemvaluetmp;
	itemvaluetmp = item.GetMember('projectid').Value;
	itemvaluenametmp = item.GetMember('name').Value;
	document.getElementById('<%response.write (SNAPUploadWizard.ClientID)%>_txtProjectNameDropDown').value=itemvaluenametmp;
	document.getElementById('<%response.write (SNAPUploadWizard.ClientID)%>_project_id').value=itemvaluetmp;
	
	document.getElementById('<%response.write (SNAPUploadWizard.ClientID)%>_txtCategoryNameDropDown').value="General";
	document.getElementById('<%response.write (SNAPUploadWizard.ClientID)%>_category_id').value="0";
	
	
	//showprojectlookup();
	document.getElementById('projectlookpupdropdown').style.display='none';
	<%response.write(CallBackTreeViewUpload.ClientID)%>.Callback(itemvaluetmp)
	return true;
  }  
  
function selectprojectcategory(item)
{
	var itemvaluetmp;
	//check for general select.
	itemvaluetmp = item.ID.replace('ACT','');
	if (itemvaluetmp.indexOf('PRJ')==0) {
	itemvaluetmp='0';
	}
	itemvaluenametmp = item.Text
	document.getElementById('<%response.write (SNAPUploadWizard.ClientID)%>_txtCategoryNameDropDown').value=itemvaluenametmp;
	document.getElementById('<%response.write (SNAPUploadWizard.ClientID)%>_category_id').value=itemvaluetmp;
	showcategorylookup();
	return true;
}

function uploadasset()
{
	if (document.getElementById('<%response.write (SNAPUploadWizard.ClientID)%>_project_id').value=='')
	{
		//alert('Please select a project in step 1.');
		return false;
	}
	if (document.getElementById('<%response.write (SNAPUploadWizard.ClientID)%>_category_id').value=='')
	{
		//alert('Please select a project folder in step 2.');
		return false;
	}
	Object_PopUp('includes/java/UploadJava.aspx?projectid=' + document.getElementById('<%response.write (SNAPUploadWizard.ClientID)%>_project_id').value + '&categoryid=' + document.getElementById('<%response.write (SNAPUploadWizard.ClientID)%>_category_id').value,'Upload',550,780);
	//showprogressbar();
	//return true;
}

function showprogressbar()
{
	ypos=this.<%response.write (SNAPUploadWizard.ClientID)%>.Frame.offsetTop+100;
	xpos=this.Splitter1.Panes[0].Width+128+ truebody().scrollLeft;
	//alert(ypos+':'+xpos);

var ProgressBarWindow 
ProgressBarWindow = document.getElementById('ProgressBarWindow')
ProgressBarWindow.style.display='block';


}


  
  function BrowseOnDoubleClick(SelectedGridRow)
  {
  
  	var itemvaluetmp;
	itemvaluetmp = SelectedGridRow.GetMember('uniqueid').Value;
	if (itemvaluetmp.substring(0,1) == 'P') 
	{
		itemvaluetmp = itemvaluetmp.replace('P_','');
		window.location.href = 'IDAM.aspx?page=Project&id=' + itemvaluetmp + '&type=project'
    
	} else {
		itemvaluetmp = itemvaluetmp.replace('C_','');
		window.location.href = 'IDAM.aspx?page=Browse&id=' + itemvaluetmp + '&type=category';
	}
	
	return true;
  
  
  
	
  }
  
    function highlightProjectToggle(checkbox,projectid)
  {
  var highlightbox;
  highlightbox = document.getElementById('projecthighlight_' + projectid);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  
  
//setup form switch for delete or multi asset zip download
function DownloadAssets(ids){
	var sAssets;
	var downloadtype;
	sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
	ids = ids.replace('0 ','');
	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Download selected assets?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (arraylist == ''){
					arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
					} else {
					if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
					arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					}
					}
				}
				
				arraylist= arraylist.substring(1, arraylist.length);
				downloadtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				downloadtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	downloadtype = 'single';
	}
	if (downloadtype == 'multi') {
		if (arraylist != ''){
		<%=idam5.idam.downloadcheck_callback_clientID%>.Callback('multi,'+arraylist);		
		}
	}
	if (downloadtype == 'single') {
	/*assume single*/
	<%=idam5.idam.downloadcheck_callback_clientID%>.Callback('single,'+ids);
	}

}

//setup goto for grid nav
function GotoLocation(ctype,cid)
	{
	if (ctype == '1') {
		window.location = 'IDAM.aspx?page=Project&Id=<%response.write (request.querystring("Id"))%>&type=project&c=' + cid;
	} else{
		window.location = 'IDAM.aspx?page=Project&Id=' + cid + '&type=project';
	}
	return true;
	}
	
	
	
</script>
		
					<div id="trailimageid">
									<div class="SnapProjectsWrapperPopup" style="WIDTH:599px">					
										<COMPONENTART:CALLBACK id="CallbackProjectAssetOverlay" runat="server" CacheContent="false">
											<CONTENT>
												<asp:PlaceHolder ID="PlaceHolderAssetPreviewPage" runat="server">
													
													<!--SnapProjectsOverlay-->
													<div class="SnapProjectsOverlay" >
														<font face="Verdana" size="1">
														<table id="table3">
															<tr>

																<td>
														
																<font size="1"><b>Image:</b></font></span><font size="1">
																</font></td>

																<td align="left" >

																<font size="1">
<asp:Literal ID="ProjectAssetOverlayCurrentImageName" Runat=server></asp:Literal></font></td>
												
  															</tr>
															<tr >

																<td><font size="1"><b>Location(s):</b></font></span><font size="1">
																</font></td>

																<td align="left" >
																
																<font size="1">
<asp:Literal ID="ProjectAssetOverlayCurrentProjectName" Runat=server></asp:Literal></font></a><font size="1">
																</font></td>

															</tr>
															</table>
														<br>
														<asp:Image id="ProjectAssetOverlayCurrentImage" runat="server" ImageUrl="~/common/images/spacer.gif"></asp:Image>
														
														<br><!--
[ <asp:Literal ID="LiteralRotateRight" Runat=server></asp:Literal>rotate right</a> ] [ <asp:Literal ID="LiteralRotateLeft" Runat=server></asp:Literal>rotate left</a> ]
													<br>-->
														<table id="table2">
															<tr>

																<td>
														
																<font size="1">Description:</font></span><font size="1">
																</font></td>
										
																<td align="left" width="80%">

																<font size="1">
<asp:Literal ID="LiteralAssetDescription" Runat=server></asp:Literal></font></td>
												
  															</tr>
															<tr >
																<td><font size="1">Owner</font><font size="1">:</font></span><font size="1">
																</font></td>
																<td align="left" width="80%">
																
																<font size="1">
<asp:Literal ID="LiteralAssetOwner" Runat=server></asp:Literal></font></a><font size="1">
																</font></td>
															</tr>
															<tr >
																<td noWrap>
																
																<font size="1">Date Created:</font></span><font size="1">
																</font></td>
																<td>
																<font size="1">
<asp:Literal ID="LiteralAssetDateCreated" Runat=server></asp:Literal></font></span><font size="1">
																</font></td>
															</tr>
															<tr>


																<td noWrap><font size="1">Type</font><font size="1">:</font></span><font size="1">
																</font></td>
												
																<td>
																<font size="1">
<asp:Literal ID="LiteralAssetType" Runat=server></asp:Literal></font></td>
											

  															</tr>
															<tr>

																<td noWrap><font size="1">File Size</font><font size="1">:</font></span><font size="1">
																</font></td>

																<td>
																<font size="1"> 
																
																<asp:Literal ID="LiteralAssetSize" Runat=server></asp:Literal></a> </font>
																</td>
									
  															</tr>
															<tr>

																<td noWrap>
										
																<font size="1">Keywords:</font></span><font size="1">
																</font></td>
										
																<td>
																<font size="1">
<asp:Literal ID="LiteralAssetKeywords" Runat=server></asp:Literal></font></td>
											
  															</tr>
															
														</table>
													</div><!--END SnapProjectsOverlay-->
												</asp:PlaceHolder>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<div class="SnapProjectsOverlay" >
													<TABLE cellSpacing="0" cellPadding="0" border="0" height="500" width=100%>
														<TR>
															<TD align="center">
																<TABLE cellSpacing="0" cellPadding="0" border="0">
																	<TR>
																		<TD style="FONT-SIZE: 10px">Loading...
																		</TD>
																		<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																	</TR>
																</TABLE>
															</TD>
															<td><IMG height="16" src="images/spacer.gif" width="16" height="500" border="0">
															</td>
														</TR>
													</TABLE>
												</div>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
									</div><!--padding-->
								</div>
							<!--END SnapProjectAssetOverlay-->
							
							
							
							
							
							
							
							
<script language=javascript type=text/javascript>

	
	
	
	
	
	/*
Simple Image Trail script- By JavaScriptKit.com
Visit http://www.javascriptkit.com for this script and more
This notice must stay intact
*/

var offsetfrommouse=[xoffsetpopup,yoffsetpopup]; //image x,y offsets from cursor position in pixels. Enter 0,0 for no offset
//var offsetfrommouse=[-150,-500]; //image x,y offsets from cursor position in pixels. Enter 0,0 for no offset
var displayduration=0; //duration in seconds image should remain visible. 0 for always.
var currentimageheight = 400;	// maximum image size.


function gettrailobj(){
if (document.getElementById)
return document.getElementById("trailimageid").style
else if (document.all)
return document.all.trailimagid.style
}

function gettrailobjnostyle(){
if (document.getElementById)
return document.getElementById("trailimageid")
else if (document.all)
return document.all.trailimagid
}


function truebody(){
return (!window.opera && document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}


function showtrail(imagename,title,description,ratingaverage,ratingnumber,showthumb,height,filetype){

	if (height > 0){
		currentimageheight = height;
	}

	document.onmousemove=followmouse;

	cameraHTML = '';

	if ( !ratingnumber ){
		ratingnumber = 0;
		ratingaverage = 0;
	}

	for(x = 1; x <= 5; x++){

		if (ratingaverage >= 1){
			cameraHTML = cameraHTML + '<img src="/images/camera_1.gif">';
		} else if (ratingaverage >= 0.5){
			cameraHTML = cameraHTML + '<img src="/images/camera_05.gif">';
		} else {
			cameraHTML = cameraHTML + '<img src="/images/camera_0.gif">';
		}
	
		ratingaverage = ratingaverage - 1;
	}

	cameraHTML = cameraHTML + ' (' + ratingnumber + ' Review';
	if ( ratingnumber > 1 ) cameraHTML += 's';
	cameraHTML = cameraHTML + ')';

	newHTML = '<div style="padding: 5px; background-color: #FFF; border: 1px solid #888;">';
	newHTML = newHTML + '<h1>' + title + '</h1>';
	//newHTML = newHTML + 'Rating: ' + cameraHTML + '<br/>';
	newHTML = newHTML + description + '<br/>';

	if (showthumb > 0){
		newHTML = newHTML + '<div align="center" style="padding: 8px 2px 2px 2px;">';
		if(filetype == 8) { // Video
			newHTML = newHTML +	'<object width="380" height="285" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0">';
			newHTML = newHTML + '<param name="movie" value="video_loupe.swf">';
			newHTML = newHTML + '<param name="quality" value="best">';
			newHTML = newHTML + '<param name="loop" value="true">';

			newHTML = newHTML + '<param name="FlashVars" value="videoLocation=' + imagename + '">';
			newHTML = newHTML + '<EMBED SRC="video_loupe.swf" LOOP="true" QUALITY="best" FlashVars="videoLocation=' + imagename + '" WIDTH="380" HEIGHT="285">';
			newHTML = newHTML + '</object></div>';
		} else {
			newHTML = newHTML + '<img src="' + imagename + '" border="0"></div>';
		}
	}

	newHTML = newHTML + '</div>';
	//gettrailobjnostyle().innerHTML = newHTML;
	gettrailobj().display="inline";
}


function hidetrail(){
	gettrailobj().innerHTML = " ";
	gettrailobj().display="none"
	document.onmousemove=""
	gettrailobj().left="-1000px"

}

function followmouse(e){

	var xcoord=offsetfrommouse[0]
	var ycoord=offsetfrommouse[1]

	var docwidth=document.all? truebody().scrollLeft+truebody().clientWidth : pageXOffset+window.innerWidth-15
	var docheight=document.all? Math.min(truebody().scrollHeight, truebody().clientHeight) : Math.min(window.innerHeight)

	//if (document.all){
	//	gettrailobjnostyle().innerHTML = 'A = ' + truebody().scrollHeight + '<br>B = ' + truebody().clientHeight;
	//} else {
	//	gettrailobjnostyle().innerHTML = 'C = ' + document.body.offsetHeight + '<br>D = ' + window.innerHeight;
	//}

	if (typeof e != "undefined"){
		if (docwidth - e.pageX < 380){
			xcoord = e.pageX - xcoord - 1000; // Move to the left side of the cursor
		} else {
			xcoord += e.pageX;
		}
		if (e.pageY - truebody().scrollTop  > (currentimageheight + 360)){
			ycoord += e.pageY - Math.max(0,(360 + currentimageheight + e.pageY - docheight - truebody().scrollTop));
		} else {
			if (e.pageY - truebody().scrollTop > 175) {
				ycoord += e.pageY + 100;
				} else {
				ycoord += e.pageY + 175;
				}
		}

	} else if (typeof window.event != "undefined"){

		if (docwidth - event.clientX < 380){
			xcoord = event.clientX + truebody().scrollLeft - xcoord - 1000; // Move to the left side of the cursor
		} else {
			xcoord += truebody().scrollLeft+event.clientX
		}
		if (docheight - event.clientY < (currentimageheight + 360)){
			ycoord += event.clientY + truebody().scrollTop  - Math.max(0,(360 + currentimageheight + event.clientY - docheight));
		} else {
		if (event.clientY > 175) {
			ycoord += truebody().scrollTop + event.clientY + 100;
			} else {
			ycoord += truebody().scrollTop + event.clientY + 175;
			}
		}
	}

	var docwidth=document.all? truebody().scrollLeft+truebody().clientWidth : pageXOffset+window.innerWidth-15
	var docheight=document.all? Math.max(truebody().scrollHeight, truebody().clientHeight) : Math.max(document.body.offsetHeight, window.innerHeight)
		if(ycoord < 0) { ycoord = ycoord*-1; }
	gettrailobj().left=xcoord+"px"
	gettrailobj().top=ycoord+"px"

}
</script>

							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							


<div id=projectlookpupdropdown style="BORDER-RIGHT:1px solid; BORDER-TOP:1px solid; DISPLAY:none; Z-INDEX:1; BORDER-LEFT:1px solid; WIDTH:350px; PADDING-TOP:1px; BORDER-BOTTOM:1px solid; POSITION:absolute; HEIGHT:230px; BACKGROUND-COLOR:white"> 
	<COMPONENTART:GRID id="GridProjectsUpload" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true"
	ClientSideOnSelect="selectprojectfromdropdown" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true"
	ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/"
	ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip"
	ScrollPopupClientTemplateId="ScrollPopupTemplate" Height="230px" Width="300px" LoadingPanelPosition="TopCenter"
	LoadingPanelClientTemplateId="LoadingFeedbackTemplateUpload" EnableViewState="true" GroupBySortImageHeight="10"
	GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif"
	GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategoryDropdown"
	IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/"
	PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="true" GroupingPageSize="1"
	PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText"
	GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderDropdown" SearchOnKeyPress="true"
	SearchTextCssClass="GridHeaderText" AllowEditing="False" AllowSorting="False" ShowSearchBox="false"
	ShowHeader="false" ShowFooter="true" CssClass="GridDropdown" RunningMode="callback" ScrollBarWidth="15"
	AllowPaging="true">
	<ClientTemplates>
		<componentart:ClientTemplate ID="LoadingFeedbackTemplateUpload">
			<div style="height:150px;"><table cellspacing="0" height=150 cellpadding="0" border="0">
					<tr>
						<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height="25" width="1"><br>
							<img src="images/spinner.gif" width="16" height="16" border="0"> Loading...
						</td>
					</tr>
				</table>
			</div>
		</componentart:ClientTemplate>
		<ComponentArt:ClientTemplate Id="EditTemplateUpload">
	<a href="javascript:editGrid('## DataItem.ClientId ##');">Edit</a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')">Delete</a>
	</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="EditCommandTemplateUpload">
			<a href="javascript:editRow();">Update</a>
		</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="InsertCommandTemplateUpload">
			<a href="javascript:insertRow();">Insert</a>
		</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="TypeIconTemplateUpload">
			<img src="images/projcat16.gif" border="0">
		</ComponentArt:ClientTemplate>
		<ComponentArt:ClientTemplate Id="LookupProjectTemplateUpload">
			<A href="IDAM.aspx?page=Project&ID=## DataItem.GetMember("projectid").Value ##&type=project"><img border="0" src="images/goto.gif"></A>
		</ComponentArt:ClientTemplate>
	</ClientTemplates>
	<Levels>
		<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateUpload" InsertCommandClientTemplateId="InsertCommandTemplateUpload" DataKeyField="projectid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCellDropdown" HeadingTextCssClass="HeadingCellTextDropdown" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssetsDropdown" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssetsDropdown" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssetsDropdown" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActiveDropdown">
			<Columns>
				<ComponentArt:GridColumn Align="Center" AllowEditing="false" DataCellClientTemplateId="TypeIconTemplateUpload"
					dataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_icon.gif"
					HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
				<componentart:GridColumn AllowEditing="False" HeadingText="Name" Width="250" FixedWidth="True" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" DataCellCssClass="LastDataCellPostings"></componentart:GridColumn>
				<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
				<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="page" SortedDataCellCssClass="SortedDataCell" DataField="page"></componentart:GridColumn>
			</Columns>
		</componentart:GridLevel>
	</Levels>
	</COMPONENTART:GRID>
</div>


<div id=categorylookpupdropdown style="BORDER-RIGHT:1px solid; BORDER-TOP:1px solid; DISPLAY:none; Z-INDEX:1; BORDER-LEFT:1px solid; WIDTH:300px; BORDER-BOTTOM:1px solid; POSITION:absolute; HEIGHT:300px; BACKGROUND-COLOR:white">	
					<COMPONENTART:CALLBACK id="CallBackTreeViewUpload" runat="server" CacheContent="false" Height="300px">
						<CONTENT>
							<ComponentArt:TreeView id="TreeViewUpload" Height="300px" Width="100%" AutoCallBackOnNodeMove="false" DragAndDropEnabled="false" NodeEditingEnabled="false" KeyboardEnabled="false" CssClass="TreeView" NodeCssClass="TreeNode" SelectedNodeCssClass="SelectedTreeNode" HoverNodeCssClass="HoverTreeNode" NodeEditCssClass="NodeEdit" LineImageWidth="19" LineImageHeight="20" DefaultImageWidth="16" DefaultImageHeight="16" NodeLabelPadding="3" ParentNodeImageUrl="images/folders.gif" LeafNodeImageUrl="images/folder.gif" ShowLines="true" LineImagesFolderUrl="images/lines/" EnableViewState="true" runat="server" ClientSideOnNodeSelect="selectprojectcategory" ></ComponentArt:TreeView>		
						</CONTENT>
						<LOADINGPANELCLIENTTEMPLATE>
							<TABLE cellSpacing="0" cellPadding="0" border="0">
								<TR>
									<TD align="center">
										<TABLE cellSpacing="0" cellPadding="0" border="0">
											<TR>
												<TD style="FONT-SIZE: 10px">Loading...
												</TD>
												<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</LOADINGPANELCLIENTTEMPLATE>
					</COMPONENTART:CALLBACK>										
				 </div>





<div class="previewpaneProjects" style="PADDING-TOP:10px" id="toolbar2">
<!--<img src="images/spacer.gif" width="5" height="1">-->
<!--<div class="MultiPageProjectsx">-->
	<div style="WIDTH:100%;PADDING-TOP:0px">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" align="left" valign="top">
					<div id="projectcenter" class="projectcentertest">
						<div class="projectcentertest">
						



				
						
							<!--margin-left: -200px; margin-top: -20px;-->
							

							<!--Upload Wizard-->
							
						
							
							<ComponentArt:Snap id="SNAPUploadWizard" runat="server" FillWidth="True" Height="100%" MustBeDocked="true"
								DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter"
								DockingContainers="projectcenter" AutoCallBackOnCollapse="true" AutoCallBackOnExpand="true"	AutoCallBackOnDock="true">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" style=" cursor: pointer;" onmousedown="<% Response.Write(SNAPUploadWizard.ClientID) %>.StartDragging(event);"><b>Upload Assets</b></td>
												<td width="15" style="cursor: hand" align="right"></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPUploadWizard.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(0);" src="images/closebutton.gif" border="0"></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPUploadWizard.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" style=" cursor: pointer;" onmousedown="<% Response.Write(SNAPUploadWizard.ClientID) %>.StartDragging(event);"><b>Upload Assets</b></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPUploadWizard.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(0);" src="images/closebutton.gif" border="0"></td>
												<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPUploadWizard.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									<div class="SnapProjectsWrapper">
										<div class="SnapProjects" style="padding-top:5px;">
											
											
											
											
											
<style>
.uploadleft
{
float:left!important; width:300px!important; width:300px; 
}

.uploadright
{
float:left!important; width:400px!important; width:400px; padding-left:10px;
}

</style>			
<div class= "uploadleft">	
STEP 1: (Select a Project)<div id=projectlookpupdropdownlocation style="position:absolute;display:none;"></div>
	<table cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td width=200>
<asp:TextBox id="txtProjectNameDropDown" runat="server" style="width:200px;"/></td>
      <td align=left ><img src="images/dropdown.gif" id="projectdropdownbutton" onclick="return !showMenu('projectlookpupdropdown', event);">
<asp:RequiredFieldValidator id="RequiredFieldValidatortxtProjectNameDropDown" runat="server" ErrorMessage="[!]" ControlToValidate="txtProjectNameDropDown"></asp:RequiredFieldValidator></td>
	</tr>
	
    </table>									
	
	STEP 2: (Select a Folder - optional)										
	<table cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td width=200>
<asp:TextBox id="txtCategoryNameDropDown" ReadOnly=true runat="server" value="General" style="width:200px;"/></td>
      <td  align=left ><img src="images/dropdown.gif" id="categorydropdownbutton" onclick="return !showMenu('categorylookpupdropdown', event);"></td>
	</tr>
    </table>	
	<table cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td >
						
				
				STEP 3: (Select File(s) to upload)<br>
				<input type=button  id="btnUpload"  onclick="uploadasset();"  value="upload">
          <div style="display:none;">
			<asp:textbox id="repository_id" runat="server">1</asp:textbox>
			
			<asp:textbox id="project_id" runat="server"></asp:textbox>
			
			<asp:textbox id="security_id" runat="server">3</asp:textbox>
			
			<asp:textbox id="user_id" runat="server"></asp:textbox>
			
			<asp:textbox id="name" runat="server"></asp:textbox>
			
			<asp:textbox id="description" runat="server"></asp:textbox>
		
			<asp:textbox id="category_id" runat="server">0</asp:textbox>
		
			<asp:textbox id="instance_id" runat="server"></asp:textbox>
			</div>
			
         </td>
    </tr>
    </table>	
</div>								
<div class="uploadright"><b>Upload Instructions:</b><br>Use the quick upload feature to upload an asset directly into a project from your homepage.  First select a project in step 1, then select a project folder in step 2 (Please note this step is optional.  If you do not select a project folder, then the asset will be uploaded into the root of the project selected in step 1.).  Choose your file in step 3 (Please note you can use the multi upload utility if your browser is java compatible.  This will allow you to drag file(s) from your desktop into the IDAM browser to begin the upload.  To start the upload process, simply press the upload button.</div>						
<div style="align:left;"></div>
								
											
										</div>
									</div> 
								</Content>
							</ComponentArt:Snap>
							
							
							<!--Upload Wizard-->
							
							
							
						
							
							
							<ComponentArt:Snap id="SNAPMyProjects" runat="server" FillWidth="True" Height="100%" MustBeDocked="true"
								DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter"
								DockingContainers="projectcenter" AutoCallBackOnCollapse="true" 
													AutoCallBackOnExpand="true" 	AutoCallBackOnDock="true">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%"  onmousedown="<% Response.Write(SNAPMyProjects.ClientID) %>.StartDragging(event);"><b>My 
													Projects (Top 100)</b></td>
												<td width="15" style="cursor: hand" align="right"></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPMyProjects.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(2);" src="images/closebutton.gif" border="0"></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPMyProjects.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" onmousedown="<% Response.Write(SNAPMyProjects.ClientID) %>.StartDragging(event);"><b>My 
													Projects (Top 100)</b></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPMyProjects.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(2);" src="images/closebutton.gif" border="0"></td>
												<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPMyProjects.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									<div class="SnapProjectsWrapper">
										<div class="SnapProjects" style="">
											<img src="images/spacer.gif" width="188" height="1">
											<div class="headingexplaination">Lists top 100 projects that you have access to within 
												the current instance.</div>
											<br>
											<div class="headingexplaination">
<asp:Literal id="literalNoProjects" Text="No projects available" visible=false runat="server" /></div>
											<table><tr><td width="100%" align=right>Search Filter:</font></b><img src="images/spacer.gif" width=5 height=1></td><td align=right width=50 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:70px" onkeyup="javascript:<%response.write( GridProjects.ClientId)%>.Filter('name LIKE \'%' + this.value + '%\'');"></td></tr></table>
											<COMPONENTART:GRID id="GridProjects" ClientSideOnInsert="onInsert" ClientSideOnUpdate="onUpdate" ClientSideOnDelete="onDelete" ClientSideOnCallbackError="onCallbackErrorProject" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ScrollPopupClientTemplateId="ScrollPopupTemplate" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplate" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="true" GroupingPageSize="1" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true">
												<ClientTemplates>
													<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
														<div><table cellspacing="0" cellpadding="0" border="0">
																<tr>
																	<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height="25" width="1"><br>
																		<img src="images/spinner.gif" width="16" height="16" border="0"> Loading...
																	</td>
																</tr>
															</table>
														</div>
													</componentart:ClientTemplate>
													<ComponentArt:ClientTemplate Id="EditTemplate">
														<a href="javascript:editGridProject('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border="0" > </a> | <a href="javascript:deleteRowProject('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
													</ComponentArt:ClientTemplate>
													<ComponentArt:ClientTemplate Id="EditCommandTemplate">
														<a href="javascript:editRowProject();">Update</a>
													</ComponentArt:ClientTemplate>
													<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
														<a href="javascript:insertRow();">Insert</a>
													</ComponentArt:ClientTemplate>
													<ComponentArt:ClientTemplate Id="TypeIconTemplate">
														<img src="images/projcat16.gif" border="0">
													</ComponentArt:ClientTemplate>
													<ComponentArt:ClientTemplate Id="LookupProjectTemplate">
														<A href="IDAM.aspx?page=Project&ID=## DataItem.GetMember("projectid").Value ##&type=project"><img border="0" src="images/goto.gif"></A>
													</ComponentArt:ClientTemplate>
												</ClientTemplates>
												<Levels>
													<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="projectid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
														<Columns>
															<ComponentArt:GridColumn Align="Center" AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate"
																dataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif"
																HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
															<ComponentArt:GridColumn Align="Center" AllowSorting="False" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplate"
																HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false"
																Width="20" FixedWidth="True" />
															<componentart:GridColumn AllowEditing="True" HeadingText="Name" Width="100" FixedWidth="True" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name"></componentart:GridColumn>
															<componentart:GridColumn HeadingText="Description" AllowEditing="true" Width="100" FixedWidth="True" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="description"></componentart:GridColumn>
															<componentart:GridColumn HeadingText="Updated" AllowEditing="false" Width="80" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="update_date"></componentart:GridColumn>
															<componentart:GridColumn HeadingText="Security Level" AllowEditing="true" Width="100" SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname"></componentart:GridColumn>
															<componentart:GridColumn HeadingText="Project Date" AllowEditing="false" Width="100" FormatString="MMM yyyy" SortedDataCellCssClass="SortedDataCell" DataField="projectdate"></componentart:GridColumn>
															<ComponentArt:GridColumn HeadingText="Action" AllowSorting="False" AllowEditing="false" Width="60" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Align="Center" />
															<ComponentArt:GridColumn HeadingText=" " AllowSorting="False" AllowEditing="false" Width="5" Align="Center" DataCellCssClass="LastDataCellPostings"/>
															<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
															<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="page" SortedDataCellCssClass="SortedDataCell" DataField="page"></componentart:GridColumn>
														</Columns>
													</componentart:GridLevel>
												</Levels>
											</COMPONENTART:GRID>
										</div> <!--SnapProjects-->
									</div> <!--padding-->
								</Content>
							</ComponentArt:Snap>
							<ComponentArt:Snap id="SNAPMyAssets" runat="server" FillWidth="True" Height="200px" MustBeDocked="true"
								DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter"
								DockingContainers="projectcenter" AutoCallBackOnCollapse="true" 	AutoCallBackOnDock="true"
													AutoCallBackOnExpand="true">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" style="PADDING-LEFT: 4px"  onmousedown="<% Response.Write(SNAPMyAssets.ClientID) %>.StartDragging(event);"><b>My 
													Assets (Top 100)</b></td>
												<td width="15" style="cursor: hand" align="right"></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPMyAssets.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(1);" src="images/closebutton.gif" border="0"></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPMyAssets.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" style="PADDING-LEFT: 4px"  onmousedown="<% Response.Write(SNAPMyAssets.ClientID) %>.StartDragging(event);"><b>My 
													Assets (Top 100)</b></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPMyAssets.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(1);" src="images/closebutton.gif" border="0"></td>
												<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPMyAssets.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									<div class="SnapProjectsWrapper">
										<div class="SnapProjects" style="">
											<img src="images/spacer.gif" width="188" height="1">
											<div class="headingexplaination">Lists all assets that you have created within the 
												current instance.</div>
											<br>
											<div class="headingexplaination">
<asp:Literal id="literalNoAssets" Text="No assets available" visible=false runat="server" /></div>

											
											<table><tr><td width="100%" align=right>Search Filter:</font></b><img src="images/spacer.gif" width=5 height=1></td><td align=right width=50 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:70px"  onkeyup="javascript:<%response.write( GridAssets.ClientId)%>.Filter('name LIKE \'%' + this.value + '%\' OR media_type_name LIKE \'%' + this.value + '%\'');"></td></tr></table>
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
<img src="images/spacer.gif" width=1 height=4><br>



<COMPONENTART:GRID id="GridAssets" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ClientSideOnSelect="BrowseOnSingleClickAssets" ScrollPopupClientTemplateId="ScrollPopupTemplate" Sort="update_date desc" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateAssets" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="true" GroupingPageSize="1" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true" >
<ClientTemplates>
<ComponentArt:ClientTemplate Id="EditTemplateAssets">
           <a href="javascript:DownloadAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> | <a href="javascript:AddToCarousel('## DataItem.ClientId ##','GridAssets');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> | <a href="javascript:editGridAssets('## DataItem.ClientId ##');"><img src="images/3Drafts.gif" alt="Edit" border=0></a> | <a href="javascript:deleteRowMyAssets('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
          </ComponentArt:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateAssetsNoDownload">
           <a href="javascript:AddToCarousel('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> 
          </ComponentArt:ClientTemplate>          
          <ComponentArt:ClientTemplate Id="EditCommandTemplateAssets">
            <a href="javascript:editRowMyAssets();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateAssets">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateAssets">
            <A href="## DataItem.GetMember("alink").Value ##"><img src="## DataItem.GetMember("timage").Value ##" border="0" onmouseover="javascript:showtrailpreload('## DataItem.GetMember("asset_id").Value ##','## DataItem.GetMember("name").Value ##','','5','1',270,7);PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.ClientId ##',event);" onmouseout="javascript:hidetrail();"></a>
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplateAssets">
            <A href="## DataItem.GetMember("alink").Value ##"><img border=0 src="images/goto.gif"></a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="LookupCategoryTemplateAssets">
            <A href="javascript:GotoLocation('0','## DataItem.GetMember("projectid2").Value ##');">## DataItem.GetMember("projectname2").Value ##</a>
          </ComponentArt:ClientTemplate>    
          <ComponentArt:ClientTemplate Id="LookupCategoryIconTemplateAssets">
            <img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif">
          </ComponentArt:ClientTemplate>                    
<componentart:ClientTemplate ID="LoadingFeedbackTemplateAssets">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>                                 
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateAssets" InsertCommandClientTemplateId="InsertCommandTemplateAssets" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplateAssets" dataField="timage" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="50" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="True" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="False" DataCellClientTemplateId="LookupCategoryTemplateAssets" HeadingText="Project" Width="80" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="projectname2" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Type" AllowEditing="false" Width="110" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Updated" AllowEditing="false" Width="110" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Security Level" AllowEditing="true" Width="80" SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" AllowSorting="False" HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplateAssets" EditControlType="EditCommand" Width="110" Align="Center" />
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" Visible="False" AllowSorting="False" HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplateAssetsNoDownload" EditControlType="EditCommand" Width="110" Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid2"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="imagesource" SortedDataCellCssClass="SortedDataCell" DataField="imagesource"></componentart:GridColumn>

</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>


										</div> <!--SnapProjects-->
									</div> <!--padding-->
								</Content>
							</ComponentArt:Snap>
							<ComponentArt:Snap id="SNAPRecentAdditions" MinWidth="200" runat="server" FillWidth="True" FillHeight="True"
								Height="100%" MustBeDocked="true" DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy"
								CurrentDockingContainer="projectcenter" DockingContainers="projectcenter" AutoCallBackOnCollapse="true" 
													AutoCallBackOnExpand="true" 	AutoCallBackOnDock="true">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" align=left onmousedown="<% Response.Write(SNAPRecentAdditions.ClientID) %>.StartDragging(event);"><b>Most Recent 
													Additions (Top 100)</b></td>
												<td width="15" style="cursor: hand" align="right"></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPRecentAdditions.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(4);" src="images/closebutton.gif" border="0"></td>
												<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPRecentAdditions.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" align=left onmousedown="<% Response.Write(SNAPRecentAdditions.ClientID) %>.StartDragging(event);"><b>Most Recent 
													Additions (Top 100)</b></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="" src="images/closebutton.gif" border="0"></td>
												<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPRecentAdditions.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									<div class="SnapProjectsWrapper">
										<div class="SnapProjects" style="">
											<img src="images/spacer.gif" width="188" height="1">
											<div class="headingexplaination">Shows the 100 most recent assets added to the iDAM 
												system.</div>
												
												
												<br>
												
												
												
												
												
												
											
											<table><tr><td width="100%" align=right>Search Filter:</font></b><img src="images/spacer.gif" width=5 height=1></td><td align=right width=50 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:70px"  onkeyup="javascript:<%response.write( GridRecent.ClientId)%>.Filter('name LIKE \'%' + this.value + '%\' OR media_type_name LIKE \'%' + this.value + '%\'');"></td></tr></table>
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											<img src="images/spacer.gif" width=1 height=4><br>
<asp:Literal id="LiteralNoRecentAssets" Text="No assets available" visible=false runat="server" />


<COMPONENTART:GRID id="GridRecent" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" 
AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ClientSideOnSelect="BrowseOnSingleClickAssets" ScrollPopupClientTemplateId="ScrollPopupTemplate" Sort="update_date desc" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateRecentAssets" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="true" GroupingPageSize="1" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true" >
<ClientTemplates>
<ComponentArt:ClientTemplate Id="EditTemplateRecentAssets">
           <a href="javascript:DownloadAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> | <a href="javascript:AddToCarousel('## DataItem.ClientId ##','GridRecent');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> | <a href="javascript:editGridRecent('## DataItem.ClientId ##');"><img src="images/3Drafts.gif" alt="Edit" border=0></a> | <a href="javascript:deleteRowRecent('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
          </ComponentArt:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateRecentAssetsNoDownload">
           <a href="javascript:AddToCarousel('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a>
          </ComponentArt:ClientTemplate>          
          <ComponentArt:ClientTemplate Id="EditCommandTemplateRecentAssets">
            <a href="javascript:editRowRecent();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateRecentAssets">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateRecentAssets">
            <A href="## DataItem.GetMember("alink").Value ##"><img src="## DataItem.GetMember("timage").Value ##" border="0" onmouseover="javascript:showtrailpreload('## DataItem.GetMember("asset_id").Value ##','## DataItem.GetMember("name").Value ##','','5','1',270,7);PreviewOverlayOnSingleClickAssetsFromIconRecent('## DataItem.ClientId ##',event);" onmouseout="javascript:hidetrail();"></a>
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplateRecentAssets">
            <A href="## DataItem.GetMember("alink").Value ##"><img border=0 src="images/goto.gif"></a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="LookupCategoryTemplateRecentAssets">
            <A href="javascript:GotoLocation('0','## DataItem.GetMember("projectid2").Value ##');">## DataItem.GetMember("projectname2").Value ##</a>
          </ComponentArt:ClientTemplate>    
          <ComponentArt:ClientTemplate Id="LookupCategoryIconTemplateRecentAssets">
            <img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif">
          </ComponentArt:ClientTemplate>                    
<componentart:ClientTemplate ID="LoadingFeedbackTemplateRecentAssets">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>                                 
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateRecentAssets" InsertCommandClientTemplateId="InsertCommandTemplateRecentAssets" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplateRecentAssets" dataField="timage" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="50" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="True" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="False" DataCellClientTemplateId="LookupCategoryTemplateRecentAssets" HeadingText="Project" Width="80" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="projectname2" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Type" AllowEditing="false" Width="110" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Updated" AllowEditing="false" Width="110" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Security Level" AllowEditing="true" Width="80" SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="User" AllowEditing="false" Width="110" SortedDataCellCssClass="SortedDataCell" DataField="userid" ForeignTable="Users" ForeignDataKeyField="userid" ForeignDisplayField="FullName"></componentart:GridColumn>
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowSorting="False" AllowEditing="false" DataCellClientTemplateId="EditTemplateRecentAssets" EditControlType="EditCommand" Width="110" Align="Center" />
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" Visible="False" AllowSorting="False" HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplateRecentAssetsNoDownload" EditControlType="EditCommand" Width="110" Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid2"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="imagesource" SortedDataCellCssClass="SortedDataCell" DataField="imagesource"></componentart:GridColumn>

</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
											
								
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
	
												
												
												
										</div>
									</div> <!--padding-->
								</Content>
							</ComponentArt:Snap>
							<ComponentArt:Snap id="SNAPMyPostings" MinWidth="200" runat="server" FillWidth="True" FillHeight="True"
								Height="100%" MustBeDocked="true" DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy"
								CurrentDockingContainer="projectcenter" DockingContainers="projectcenter" AutoCallBackOnCollapse="true" 
													AutoCallBackOnExpand="true" 	AutoCallBackOnDock="true">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" align=left onmousedown="<% Response.Write(SNAPMyPostings.ClientID) %>.StartDragging(event);"><b>Most Recent 
													Postings (Top 25)</b></td>
												<td width="15" style="cursor: hand" align="right"></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPMyPostings.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(3);" src="images/closebutton.gif" border="0"></td>
												<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPMyPostings.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" align=left onmousedown="<% Response.Write(SNAPMyPostings.ClientID) %>.StartDragging(event);"><b>Most Recent 
													Postings (Top 25)</b></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPMyPostings.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(3);" src="images/closebutton.gif" border="0"></td>
												<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPMyPostings.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									<div class="SnapProjectsWrapper">
										<div class="SnapProjects" style="">
											<img src="images/spacer.gif" width="188" height="1">
											<div class="headingexplaination">Listing of 25 latest postings you are able to see within 
												the iDAM system. These may not be postings you have created.</div>
												<br>
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
<div class="headingexplaination">																	
<!--No Postings warning-->
<div style="TEXT-ALIGN: left">
<asp:Literal id="LiteralNoPostings" Text="No postings available" visible=false runat="server" /></div></div>
													
																	
																	
																	
																	
																	
<COMPONENTART:GRID id="GridPosting" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ClientSideOnSelect="BrowseOnSingleClickAssetsLegacy" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ScrollPopupClientTemplateId="ScrollPopupTemplate" Sort="available_date desc" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategory" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" IndentCellCssClass="IndentCell" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/postinglines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="true" GroupingPageSize="1" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeaderCategory" SearchOnKeyPress="false" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="GridCategory" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true" >
<ClientTemplates>
		<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategory">
		<div><table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
		</tr>
		</table></div>
		</componentart:ClientTemplate>
		  <ComponentArt:ClientTemplate Id="EditTemplateCategory">
            <a href="javascript:editGridCategory('## DataItem.ClientId ##');"><img src="images/3Drafts.gif" alt="Edit" border=0></a> <!--| <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>-->
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateCategory">
            <a href="javascript:editRowCategory();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateCategory">
            <a href="javascript:insertRowCategory();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategory">
            <img border=0 src="images/tasks.gif">
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplateCategory">
            <A href="IDAM.aspx?page=Project&id=## DataItem.GetMember("category_id").Value ##&type=project"><img border=0 src="images/goto.gif"></a>
          </ComponentArt:ClientTemplate>     
          <ComponentArt:ClientTemplate Id="EditTemplateAssetPostings">
           <a href="javascript:DownloadPostingAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> | <a href="javascript:AddToCarousel('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/3Drafts.gif" alt="Edit" border=0></a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateAssetPostings">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateAssetPostings">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateAssetPostings">
            <img src="http://## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplateAssetPostings">
			<A href="## DataItem.GetMember("alink").Value ##"><img border=0 src="images/goto.gif"></a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="LookupCategoryTemplateAssetPostings">
            <A href="javascript:GotoLocation('## DataItem.GetMember("ispost").Value ##','## DataItem.GetMember("category_id").Value ##');">## DataItem.GetMember("categoryname").Value ##</a>
          </ComponentArt:ClientTemplate>    
          <ComponentArt:ClientTemplate Id="LookupCategoryIconTemplateAssetPostings">
            <img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif">
          </ComponentArt:ClientTemplate>                    


          
                              
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" DataMember="Postings" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateCategory" InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="category_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCellCategory" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellCategory" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverCategory" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowCategory" SortImageHeight="19" RowCssClass="RowCategory" HeadingCellActiveCssClass="HeadingCellActiveCategory">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowSorting="False" AllowEditing="false"   DataCellClientTemplateId="TypeIconTemplateCategory" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings"  HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn HeadingText="Posted" AllowEditing="false" Width="50" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" DataCellCssClass="DataCell" SortedDataCellCssClass="SortedDataCell" DataField="available_date"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="True" AllowSorting="False" HeadingText="Name" Width="200" AllowGrouping="False" DataCellCssClass="DataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" AllowEditing="true" AllowGrouping="False" Width="80" DataCellCssClass="DataCell" DataField="description"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Security Level" AllowEditing="true" DataCellCssClass="DataCell" Width="50" SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" AllowSorting="False" HeadingText="Posted By" Width="80" DataCellCssClass="DataCell" AllowGrouping="False" DataField="fullname" ></componentart:GridColumn>
<componentart:GridColumn Align="Center"  AllowEditing="false" AllowSorting="False" Width="25" HeadingText="Assets" DataCellCssClass="DataCell" AllowGrouping="False"  DataField="numassets" ></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" AllowEditing="false" AllowSorting="False" DataCellClientTemplateId="EditTemplateCategory" Width="80" DataCellCssClass="LastDataCellPostings" EditControlType="EditCommand" Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="category_id" SortedDataCellCssClass="SortedDataCell" DataField="category_id"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>


<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate" DataMember="Assets" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCellCategory" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellCategory" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverCategory" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowCategory" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActiveCategory">
<Columns>



<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplateAssetPostings" dataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="16" FixedWidth="True" />
<ComponentArt:GridColumn DataCellCssClass="DataCell" Align="Center" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplateAssetPostings" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="16" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="True" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Type" AllowEditing="false" Width="110" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Updated" AllowEditing="false" Width="110" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="update_date"></componentart:GridColumn>
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplateAssetPostings" EditControlType="EditCommand" Width="140" Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="category_id" SortedDataCellCssClass="SortedDataCell" DataField="category_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="ispost" SortedDataCellCssClass="SortedDataCell" DataField="ispost"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>


</Columns>

</componentart:GridLevel>








</Levels>
</COMPONENTART:GRID>
																	
																	
																	
											
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
										</div>
									</div> <!--padding-->
								</Content>
							</ComponentArt:Snap>
							
							
							
							
							
							
							
							
							
							
							
							
							
							<ComponentArt:Snap id="SNAPComments" MinWidth="200" runat="server" FillWidth="True" Height="100%"
								MustBeDocked="true" DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter"
								DockingContainers="projectcenter" AutoCallBackOnCollapse="true" 
													AutoCallBackOnExpand="true" 	AutoCallBackOnDock="true">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" align=left onmousedown="<% Response.Write(SNAPComments.ClientID) %>.StartDragging(event);"><b>Comments</b>
													</td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPComments.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(5);" src="images/closebutton.gif" border="0"></td>
												<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPComments.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td  width="100%" align=left onmousedown="<% Response.Write(SNAPComments.ClientID) %>.StartDragging(event);"><b>Comments</b></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPComments.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(5);" src="images/closebutton.gif" border="0"></td>
												<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPComments.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									<div class="SnapProjectsWrapper">
										<div class="SnapProjects" style="">
											<img src="images/spacer.gif" width="188" height="1">
											
												
<div class="headingexplaination">																	
<!--No Postings warning-->
<div style="TEXT-ALIGN: left">
<asp:Literal id="LiteralComments" Text="No comments available" visible=false runat="server" /></div></div>
		



<COMPONENTART:GRID id="GridComments" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ClientSideOnSelect="BrowseOnSingleClickAssetsLegacy" ScrollPopupClientTemplateId="ScrollPopupTemplate" Sort="date_posted desc" Height="10" Width="100%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplateComments" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="true" GroupingPageSize="1" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="10" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true" >
<ClientTemplates>
<ComponentArt:ClientTemplate Id="EditTemplateComments">
            <A href="## DataItem.GetMember("alink").Value ##"><img src="images/search_ID_ico.gif" alt="View Comment" border=0></a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="PriorityTemplate">
           <img src="images/priority## DataItem.GetMember("priority_id").Value ##.gif" border=0>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateComments">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateComments">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateComments">
            <img src="## DataItem.GetMember("imagesource").Value ##" border="0" onmouseover="javascript:showtrailpreload('## DataItem.GetMember("asset_id").Value ##','## DataItem.GetMember("asset_name").Value ##','','5','1',270,7);PreviewOverlayOnSingleClickCommentsFromIconComments('## DataItem.ClientId ##',event);" onmouseout="javascript:hidetrail();"> 
          </ComponentArt:ClientTemplate>        
			<ComponentArt:ClientTemplate Id="CommentTemplate">
			<table width="100%" cellspacing="0" cellpadding="1" border="0">
			<tr>
				<td class="CellText" align="left">comment: <a style="color:#595959;" href="#"><b><nobr>## DataItem.GetMember("description").Value ##</nobr></a></b></td>
			</tr>
			<tr>
				<td class="CellText" align="left">by: <font color="#595959"><nobr>## DataItem.GetMember("name").Text ##</nobr></font></td>
			</tr>
			</table>
			</ComponentArt:ClientTemplate>    
			<ComponentArt:ClientTemplate Id="CommentLocationTemplate">
			<table width="100%" cellspacing="0" cellpadding="1" border="0">
			<tr>
				<td class="CellText" align="left">asset: <a style="color:#595959;" href="#"><b><nobr>## DataItem.GetMember("asset_name").Value ##</nobr></a></b></td>
			</tr>
			<tr>
				<td class="CellText" align="left">project: <font color="#595959"><nobr><A href="javascript:GotoLocation('0','## DataItem.GetMember("projectid").Value ##');">## DataItem.GetMember("project_name").Text ##</a></nobr></font></td>
			</tr>
			</table>
			</ComponentArt:ClientTemplate>  			        
			<componentart:ClientTemplate ID="LoadingFeedbackTemplateComments">
			<table cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
			</tr>
			</table>
			</componentart:ClientTemplate>                                 
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplateComments" InsertCommandClientTemplateId="InsertCommandTemplateComments" DataKeyField="commentid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" AlternatingRowCssClass="AlternatingRowComments" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center" AllowEditing="false" DataCellClientTemplateId="TypeIconTemplateComments" dataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="50" FixedWidth="True" />
<ComponentArt:GridColumn DataCellCssClass="DataCell" HeadingText="Comment" Align="left" AllowSorting="false" DataCellClientTemplateId="CommentTemplate" AllowEditing="false"  Width="250"  />
<ComponentArt:GridColumn DataCellCssClass="DataCell" HeadingText="Priority" Align="left" AllowSorting="false" DataCellClientTemplateId="PriorityTemplate" AllowEditing="false"  Width="40"  />
<ComponentArt:GridColumn DataCellCssClass="DataCell" HeadingText="Location" Align="left" AllowSorting="false" DataCellClientTemplateId="CommentLocationTemplate" AllowEditing="false"  Width="150"  />
<ComponentArt:GridColumn AllowEditing="false" DataField="date_posted" HeadingText="Posted" FormatString="MMM dd yyyy, hh:mm tt" DefaultSortDirection="Descending" Align="Right" Width="150" />
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowSorting="false" AllowEditing="false" DataCellClientTemplateId="EditTemplateComments" EditControlType="EditCommand" Width="50" Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_name" SortedDataCellCssClass="SortedDataCell" DataField="asset_name"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="project_name" SortedDataCellCssClass="SortedDataCell" DataField="project_name"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="priority" SortedDataCellCssClass="SortedDataCell" DataField="priority_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="description" SortedDataCellCssClass="SortedDataCell" DataField="description"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="date_posted" SortedDataCellCssClass="SortedDataCell" DataField="date_posted"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="name" SortedDataCellCssClass="SortedDataCell" DataField="name"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="comment_id" SortedDataCellCssClass="SortedDataCell" DataField="commentid"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="imagesource" SortedDataCellCssClass="SortedDataCell" DataField="imagesource"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="timage" SortedDataCellCssClass="SortedDataCell" DataField="timage"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
			
											
										</div>
									</div> <!--padding-->
								</Content>
							</ComponentArt:Snap>
							
							
							
							
							
							
							
							<ComponentArt:Snap id="SNAPAnnouncements" MinWidth="200" runat="server" FillWidth="True" FillHeight="True"
								Height="100%" MustBeDocked="true" DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy"
								CurrentDockingContainer="projectcenter" DockingContainers="projectcenter" AutoCallBackOnCollapse="true" 
													AutoCallBackOnExpand="true" 	AutoCallBackOnDock="true">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" onmousedown="<% Response.Write(SNAPAnnouncements.ClientID) %>.StartDragging(event);"><b>Alerts</b>
													</td>
												<td width="15" style="cursor: hand" align="right"></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPAnnouncements.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(6);" src="images/closebutton.gif" border="0"></td>
												<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPAnnouncements.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" onmousedown="<% Response.Write(SNAPAnnouncements.ClientID) %>.StartDragging(event);"><b>Alerts</b></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPAnnouncements.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(6);" src="images/closebutton.gif" border="0"></td>
												<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPAnnouncements.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									<div style="padding-bottom:5px;border:1px;">
										<div class="SnapProjects" style="height:100%;width=100%padding:0px;padding-left:5px;padding-right:5px;background-color:white;">
											<img src="images/spacer.gif" width="188" height="1">
											

											
											
											
											
										</div>
									</div> <!--padding-->
								</Content>
							</ComponentArt:Snap>
							
							
							<ComponentArt:Snap id="SNAPLinks" MinWidth="200" runat="server" FillWidth="True" FillHeight="True"
								Height="100%" MustBeDocked="true" DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy"
								CurrentDockingContainer="projectcenter" DockingContainers="projectcenter" AutoCallBackOnCollapse="true" 
													AutoCallBackOnExpand="true"	AutoCallBackOnDock="true">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" onmousedown="<% Response.Write(SNAPAnnouncements.ClientID) %>.StartDragging(event);"><b>My Links</b></td>
												<td width="15" style="cursor: hand" align="right"></td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPLinks.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(7);" src="images/closebutton.gif" border="0"></td>
												<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPLinks.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" onmousedown="<% Response.Write(SNAPAnnouncements.ClientID) %>.StartDragging(event);"><b>My Links</b>
													</td>
												<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPLinks.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(7);" src="images/closebutton.gif" border="0"></td>
												<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPLinks.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									<div style="padding-bottom:5px;border:1px;">
										<div class="SnapProjects" style="height:100%;width=100%padding:0px;padding-left:5px;padding-right:5px;background-color:white;">
											<img src="images/spacer.gif" width="188" height="1">
											
											
											
											
											
											
											
											
											
											
											
											
											
											
										</div>
									</div> <!--padding-->
								</Content>
							</ComponentArt:Snap>
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
						</div> <!--bottom padding-->
					</div> <!--projectcenter-->
					<!--split here-->
				</td>
				<!--
				<td width="1px" align="left" valign="top">
					<div id="projectright" style="BORDER-RIGHT:0px solid; BORDER-TOP:0px solid; PADDING-LEFT:5px; BORDER-LEFT:0px solid; BORDER-BOTTOM:0px solid">
						<div>
							
						</div>
					</div>
				</td>
				-->
			</tr>
		</table>
		<div style="PADDING-RIGHT:8px;PADDING-LEFT:8px;PADDING-BOTTOM:8px;PADDING-TOP:8px"></div>
	</div> <!--End Multipage-->
</div>
<script type="text/javascript">
  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

  
  function BrowseOnDoubleClick(SelectedGridRow)
  {
  
  	var itemvaluetmp;
	itemvaluetmp = SelectedGridRow.GetMember('asset_id').Value;
	if (itemvaluetmp.substring(0,1) == 'P') 
	{
		itemvaluetmp = itemvaluetmp.replace('P_','');
		window.location.href = 'IDAM.aspx?page=Project&id=' + itemvaluetmp
    
	} else {
		itemvaluetmp = itemvaluetmp.replace('C_','');
		window.location.href = 'IDAM.aspx?page=Browse&id=' + itemvaluetmp + '&type=category';
	}
	
	return true;
  
  
  
	
  }
  
  
  
  
function PreviewOverlayOnSingleClickCommentsFromIconComments(RowId,eventObject)
  {
	try
	{

		var itemvaluetmp;
		var itemvaluenametmp;
		var itemvaluefiletypetmp;
		var item 
		itemRow = <% Response.Write(GridComments.ClientID) %>.GetRowFromClientId(RowId);
		itemvaluetmp = itemRow.GetMember('asset_id').Value;
		itemvaluenametmp = itemRow.GetMember('asset_name').Value.replace(',','');
		itemvaluefiletypetmp = itemRow.GetMember('timage').Value;
		//alert(itemRow.GetMember('timage').Value);
		//alert(itemRow+itemvaluetmp+itemvaluenametmp+itemvaluefiletypetmp);
		
		
		<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);
		
		 
	}  //end try  
	catch(err)
	{
	txt="There was an error on this page.\n\n"
	txt+="Error description: " + err.description
	//alert(txt);
	}
	
	//return true;
}

function PreviewOverlayOnSingleClickAssetsFromIcon(RowId,eventObject)
  {
	try
	{

		var itemvaluetmp;
		var itemvaluenametmp;
		var itemvaluefiletypetmp;
		var item 
		itemRow = <% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(RowId);
		itemvaluetmp = itemRow.GetMember('asset_id').Value;
		itemvaluenametmp = itemRow.GetMember('name').Value;
		itemvaluefiletypetmp = itemRow.GetMember('imagesource').Value;
		<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);
	}  //end try  
	catch(err)
	{
	txt="There was an error on this page.\n\n"
	txt+="Error description: " + err.description
	//alert(txt);
	}
	//return true;
}


function PreviewOverlayOnSingleClickAssetsFromIconRecent(RowId,eventObject)
  {
	try
	{

		var itemvaluetmp;
		var itemvaluenametmp;
		var itemvaluefiletypetmp;
		var item 
		
		itemRow = <% Response.Write(GridRecent.ClientID) %>.GetRowFromClientId(RowId);
		itemvaluetmp = itemRow.GetMember('asset_id').Value;
		itemvaluenametmp = itemRow.GetMember('name').Value;
		itemvaluefiletypetmp = itemRow.GetMember('imagesource').Value;
		<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);
	}  //end try  
	catch(err)
	{
	txt="There was an error on this page.\n\n"
	txt+="Error description: " + err.description
	//alert(txt);
	}
	//return true;
}

		
function PreviewOverlayOnSingleClickCommentsFromIcon(RowId,eventObject)
  {
	try
	
	{

		var itemvaluetmp;
		var itemvaluenametmp;
		var itemvaluefiletypetmp;
		var item 
		itemRow = <% Response.Write(GridComments.ClientID) %>.GetRowFromClientId(RowId);
		alert(itemRow.GetMember('imagesource').Value);
		itemvaluetmp = itemRow.GetMember('asset_id').Value;
		itemvaluenametmp = itemRow.GetMember('asset_name').Value;
		itemvaluefiletypetmp = itemRow.GetMember('imagesource').Value;
		
		<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);
		 
	}  //end try  
	catch(err)
	{
	txt="There was an error on this page.\n\n"
	txt+="Error description: " + err.description
	//alert(txt);
	}
}
  
  
  
  
  
  function highlightAssetToggle(checkbox,asset_id)
  {
  var highlightbox;
  highlightbox = document.getElementById('assethighlight_' + asset_id);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  
  
  function highlightProjectToggle(checkbox,projectid)
  {
  var highlightbox;
  highlightbox = document.getElementById('projecthighlight_' + projectid);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  
  
  
  
  
  
		
function AddToCarousel(ids,_source)
  {
  //check to see if multi select
	var sAssets;
	var addtype;
	if (getLastView() == 'thumb')
        {
    
    var arraylist;
	var i;
	arraylist = '';
	ids = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
	if (arraylist.split(',').length > 2) {
			 if (confirm("Add selected assets to carousel?"))
			 {
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				addtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	addtype = 'single';
	}
       
    
    } else {
        
    if (_source=='GridAssets')    
    {
    sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
    } else {
    sAssets = <%response.write( GridRecent.ClientId)%>.GetSelectedItems();
    }
	ids = ids.replace('0 ','');
	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Add selected assets to carousel?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
				  if (sAssets[i] != null) {
					if (arraylist == ''){
					
					arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
					} else {
					if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
					arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					}
					}
				  }
				}
				
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				addtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	addtype = 'single';
	}
        

    }
	
	
	if (addtype == 'multi') {
		if (arraylist != ''){
		
		IDAMCarousel_carousel_callback.Callback('1,' + arraylist + ',AddMultiToCarousel');
		}
	}
	
	if (addtype == 'single') {
	/*assume single*/
	IDAMCarousel_carousel_callback.Callback('1,' + ids + ',AddToCarousel');
	} 
  }	
  
  
  

</script>

<script language=javascript type="text/javascript">
function showtrailpreload(imageid,title,description,ratingaverage,ratingnumber,showthumb,height,filetype)
{
	showtrail('<%=idam5.BaseIDAMSession.Config.URLLocationPreview%>?id=' + imageid + '&amp;instance=<%response.write (IDAMInstance)%>&amp;type=asset&amp;size=2&amp;height=399&amp;width=370',title,description,'5','1',270,7);
}

function gotopagelink(subtype)
{
	var newurl = 'IDAM.aspx?Page=' + subtype;
	window.location.href = newurl;
	return true;
}

</script>

