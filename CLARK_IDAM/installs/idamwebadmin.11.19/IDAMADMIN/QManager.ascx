<%@ Control Language="vb" AutoEventWireup="false" Codebehind="QManager.ascx.vb" Inherits="IDAM5.QManager" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<div class="previewpaneAll" id="toolbar">
	<!--Project cookie and top menu-->
		
		<table cellspacing="0"  id="table2" width="100%">
			<tr>
			<td >
			<div style="float:left;padding:5px;"><img src="images/idampref_ID_ico.gif" ></div>
			<div style="float:left;padding:5px;">
						<font face="Verdana" size="1"><b>Queue Manager</b><br>
				<span style="font-weight: 400">View and manage the IDAM queue.<br></span>
				
				</font>
			</div>
			</td>
			<td id="Test" valign="top" align="right">
			
			</td></tr>
			<tr><td valign="top" colspan="2">
			</td>
			</tr>
		</table>
		
</div> <!--end preview pane-->

<div class="previewpaneSub">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle" width="100%">
			
			<div style="PADDING-RIGHT:0px;PADDING-LEFT:0px;PADDING-BOTTOM:0px;PADDING-TOP:0px;align:right">
								<b><asp:HyperLink CssClass="projecttabs" id="link_ProjectGeneral" NavigateUrl="javascript:gotopagelink('ProjectGeneral');"
									runat="server">General</asp:HyperLink></b>
							</div>
			</td>
			<td align="right"  width="100%" >
				<!--<asp:Image onclick="javascript:switchToThumbnail();" id="list_icon"   runat="server"></asp:Image><asp:Image onclick="javascript:switchToThumbnail();"  id="thumb_icon" runat="server"></asp:Image>-->
				<img src="images/spacer.gif" height=16 width=1>
			</td>
		</tr>
	</table>
</div>


<div class="previewpaneProjects" id="toolbar">

<script language=javascript>
  
        // Forces the grid to adjust to the new size of its container          
      function resizeGridSuccess(DomElementId, NewPaneHeight, NewPaneWidth)
      {
        <% Response.Write(GridSuccess.ClientID) %>.Render();
        //alert('here');
      } 
  function editGrid(rowId)
  {

    <% Response.Write(GridSuccess.ClientID) %>.Edit(<% Response.Write(GridSuccess.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  

  function onInsert(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
   function editGridDT(rowId)
  {

    <% Response.Write(GridProcessing.ClientID) %>.Edit(<% Response.Write(GridProcessing.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  

  function onInsertDT(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
  
  
   function editGridIT(rowId)
  {

    <% Response.Write(GridFailed.ClientID) %>.Edit(<% Response.Write(GridFailed.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  

  function onInsertIT(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
    function ShowContextMenu(item, column, evt) 
  {
    <% Response.Write(GridSuccess.ClientID) %>.Select(item); 
   
    return false; 
  }
  
  function onUpdate(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }
  
    
  function onUpdateDT(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }
  
  
    
  function onUpdateIT(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }


  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridSuccess.ClientID) %>.Page(1); 
  }

  function onDelete(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
  
  function editRow()
  {
    <% Response.Write(GridSuccess.ClientID) %>.EditComplete();     
  }

  function insertRow()
  {
    <% Response.Write(GridSuccess.ClientID) %>.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    <% Response.Write(GridSuccess.ClientID) %>.Delete(<% Response.Write(GridSuccess.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  
    function onDeleteDT(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
  
  function editRowDT()
  {
    <% Response.Write(GridProcessing.ClientID) %>.EditComplete();     
  }

  function insertRowDT()
  {
    <% Response.Write(GridProcessing.ClientID) %>.EditComplete(); 
  }

  function deleteRowDT(rowId)
  {
    <% Response.Write(GridProcessing.ClientID) %>.Delete(<% Response.Write(GridProcessing.ClientID) %>.GetRowFromClientId(rowId)); 
  }




  function onDeleteIT(item)
  {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
  }
  
  function editRowIT()
  {
    <% Response.Write(GridFailed.ClientID) %>.EditComplete();     
  }

  function insertRowIT()
  {
    <% Response.Write(GridFailed.ClientID) %>.EditComplete(); 
  }

  function deleteRowIT(rowId)
  {
    <% Response.Write(GridFailed.ClientID) %>.Delete(<% Response.Write(GridFailed.ClientID) %>.GetRowFromClientId(rowId)); 
  }

  

</script>



<div style="BORDER-RIGHT:0px solid; PADDING-RIGHT:5px; BORDER-TOP:0px solid; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:0px solid; PADDING-TOP:15px; BORDER-BOTTOM:0px solid" id="browsecenter">



<img src="images/spacer.gif"  height="15" width="5"><br>
<!--<div style="padding:10px;border-left:1px #969698 solid; border-right:1px #969698 solid;border-bottom:1px #969698 solid;border-top:1px #969698 solid;">-->







<div style="padding-right:10px;padding-bottom:10px;">
<b>Recently Completed List (Top 1000)</b>
</div>
<COMPONENTART:GRID id="GridSuccess" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ClientSideOnInsert="onInsert" ClientSideOnUpdate="onUpdate" ClientSideOnDelete="onDelete" ClientSideOnCallbackError="onCallbackError" ScrollPopupClientTemplateId="ScrollPopupTemplate" Sort="date_complete desc" Height="10" Width="99%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplate" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="20" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="True" GroupingPageSize="5" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="20" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=125 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplate">
            <a href="javascript:editGrid('## DataItem.ClientId ##');">Edit</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplate">
          <img src="## DataItem.GetMember("imagesource").Value ##" border="0"  > 
          </ComponentArt:ClientTemplate>           
          <ComponentArt:ClientTemplate Id="TemplocationTemplate">
          <a href="## DataItem.GetMember("src").Value ##" target=_blank>test</a>
          </ComponentArt:ClientTemplate> 
          <ComponentArt:ClientTemplate Id="AssetLinkTemplate">
          <a href="## DataItem.GetMember("alink").Value ##">## DataItem.GetMember("asset_name").Value ##</a>
          </ComponentArt:ClientTemplate>  
                   
          
                             
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="ID" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate"  HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="False" HeadingText="Asset" DataCellClientTemplateId="AssetLinkTemplate"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="asset_name" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="False" HeadingText="Instance" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="instance_id" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Source" AllowEditing="false" DataCellClientTemplateId="TemplocationTemplate"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="src"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Handler" AllowEditing="False" SortedDataCellCssClass="SortedDataCell" DataField="asset_handler" Width="80" FixedWidth="True" ForeignTable="HandlerTable" ForeignDataKeyField="handler_id" ForeignDisplayField="handler_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Completed" AllowEditing="false" FormatString="MM/dd/yyyy hh:mm:ss" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="date_complete"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Engine" AllowEditing="False" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="engine_id"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Process Time" AllowEditing="false" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="elapse"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Priority" AllowEditing="False" SortedDataCellCssClass="SortedDataCell" DataField="priority" Width="80" FixedWidth="True" ForeignTable="PriorityTable" ForeignDataKeyField="priority_id" ForeignDisplayField="priority_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Status" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="status" Width="80" FixedWidth="True" ForeignTable="StatusTable" ForeignDataKeyField="status_id" ForeignDisplayField="status_value"></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" AllowEditing="false" AllowSorting="False" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Align="Center" Width="80" FixedWidth="True"  DataCellCssClass="LastDataCell" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="imagesource" SortedDataCellCssClass="SortedDataCell" DataField="imagesource"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="ID" SortedDataCellCssClass="SortedDataCell" DataField="ID"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<!--<componentart:GridColumn AllowEditing="false" HeadingText="Assets" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid" ForeignTable="Assets" ForeignDataKeyField="uniqueid" ForeignDisplayField="acount"></componentart:GridColumn>-->
<input type="button" onclick="<%response.write( GridSuccess.ClientId)%>.Filter('asset_name LIKE \'%%\' ');" value="Refresh" />










<div style="padding-top:10px;"><div style="padding:2px;"></div></div>

<div style="padding-right:10px;padding-bottom:10px;">
<b>Active Items in Queue</b>
</div>


<COMPONENTART:GRID id="GridProcessing" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ClientSideOnInsert="onInsertDT" ClientSideOnUpdate="onUpdateDT" ClientSideOnDelete="onDeleteDT" ClientSideOnCallbackError="onCallbackError" ScrollPopupClientTemplateId="ScrollPopupTemplate" Sort="date_placed desc" Height="10" Width="99%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplatep" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="20" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="True" GroupingPageSize="5" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="20" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplatep">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=125 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplatep">
            <a href="javascript:editGridDT('## DataItem.ClientId ##');">Edit</a> | <a href="javascript:deleteRowDT('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplatep">
            <a href="javascript:editRowDT();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplatep">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplatep">
          <img src="## DataItem.GetMember("imagesource").Value ##" border="0"  > 
          </ComponentArt:ClientTemplate>           
          <ComponentArt:ClientTemplate Id="TemplocationTemplatep">
          <a href="## DataItem.GetMember("src").Value ##" target=_blank>test</a>
          </ComponentArt:ClientTemplate> 
          <ComponentArt:ClientTemplate Id="AssetLinkTemplatep">
          ## DataItem.GetMember("asset_name").Value ##
          </ComponentArt:ClientTemplate>  
                   
          
                             
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplatep" InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="ID" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplatep"  HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="False" HeadingText="Asset" DataCellClientTemplateId="AssetLinkTemplatep"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="asset_name" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="False" HeadingText="Instance" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="instance_id" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Source" AllowEditing="false" DataCellClientTemplateId="TemplocationTemplatep"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="src"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Handler" AllowEditing="False" SortedDataCellCssClass="SortedDataCell" DataField="asset_handler" Width="80" FixedWidth="True" ForeignTable="HandlerTable" ForeignDataKeyField="handler_id" ForeignDisplayField="handler_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Added" AllowEditing="false" FormatString="MM/dd/yyyy hh:mm:ss" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="date_complete"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Engine" AllowEditing="true" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="engine_id"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Priority" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="priority" Width="80" FixedWidth="True" ForeignTable="PriorityTable" ForeignDataKeyField="priority_id" ForeignDisplayField="priority_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Status" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="status" Width="80" FixedWidth="True" ForeignTable="StatusTable" ForeignDataKeyField="status_id" ForeignDisplayField="status_value"></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" AllowEditing="false"  AllowSorting="False" DataCellClientTemplateId="EditTemplatep" EditControlType="EditCommand" Align="Center" Width="80" FixedWidth="True"  DataCellCssClass="LastDataCell" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="imagesource" SortedDataCellCssClass="SortedDataCell" DataField="imagesource"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="ID" SortedDataCellCssClass="SortedDataCell" DataField="ID"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<!--<componentart:GridColumn AllowEditing="false" HeadingText="Assets" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid" ForeignTable="Assets" ForeignDataKeyField="uniqueid" ForeignDisplayField="acount"></componentart:GridColumn>-->
<input type="button" onclick="<%response.write( GridProcessing.ClientId)%>.Filter('asset_name LIKE \'%%\' ');" value="Refresh" />









<div style="padding-top:10px;"><div style="padding:2px;"></div></div>

<div style="padding-right:10px;padding-bottom:10px;">
<b>Failed Items in Queue</b>
</div>


<COMPONENTART:GRID id="GridFailed" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ClientSideOnInsert="onInsertIT" ClientSideOnUpdate="onUpdateIT" ClientSideOnDelete="onDeleteIT" ClientSideOnCallbackError="onCallbackError" ScrollPopupClientTemplateId="ScrollPopupTemplate" Sort="date_processed desc" Height="10" Width="99%" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplatef" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="20" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="True" GroupingPageSize="5" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="20" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplatef">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=125 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplatef">
            <a href="javascript:editGridIT('## DataItem.ClientId ##');">Edit</a> | <a href="javascript:deleteRowIT('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplatef">
            <a href="javascript:editRowIT();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplatef">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplatef">
          <img src="## DataItem.GetMember("imagesource").Value ##" border="0"  > 
          </ComponentArt:ClientTemplate>           
          <ComponentArt:ClientTemplate Id="TemplocationTemplatef">
          <a href="## DataItem.GetMember("src").Value ##" target=_blank>test</a>
          </ComponentArt:ClientTemplate> 
          <ComponentArt:ClientTemplate Id="AssetLinkTemplatef">
          ## DataItem.GetMember("asset_name").Value ##
          </ComponentArt:ClientTemplate>  
                   
          
                             
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplatef" InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="ID" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplatef"  HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="False" HeadingText="Asset" DataCellClientTemplateId="AssetLinkTemplatef"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="asset_name" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="False" HeadingText="Instance" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="instance_id" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Source" AllowEditing="false" DataCellClientTemplateId="TemplocationTemplatef"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="src"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Handler" AllowEditing="False" SortedDataCellCssClass="SortedDataCell" DataField="asset_handler" Width="80" FixedWidth="True" ForeignTable="HandlerTable" ForeignDataKeyField="handler_id" ForeignDisplayField="handler_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Date Processed" AllowEditing="false" FormatString="MM/dd/yyyy hh:mm:ss" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="date_complete"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Engine" AllowEditing="true" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="engine_id"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Priority" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="priority" Width="80" FixedWidth="True" ForeignTable="PriorityTable" ForeignDataKeyField="priority_id" ForeignDisplayField="priority_value"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Status" AllowEditing="True" SortedDataCellCssClass="SortedDataCell" DataField="status" Width="80" FixedWidth="True" ForeignTable="StatusTable" ForeignDataKeyField="status_id" ForeignDisplayField="status_value"></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" AllowEditing="false"  AllowSorting="False" DataCellClientTemplateId="EditTemplatef" EditControlType="EditCommand" Align="Center" Width="80" FixedWidth="True"  DataCellCssClass="LastDataCell" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="imagesource" SortedDataCellCssClass="SortedDataCell" DataField="imagesource"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="ID" SortedDataCellCssClass="SortedDataCell" DataField="ID"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<!--<componentart:GridColumn AllowEditing="false" HeadingText="Assets" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid" ForeignTable="Assets" ForeignDataKeyField="uniqueid" ForeignDisplayField="acount"></componentart:GridColumn>-->
<input type="button" onclick="<%response.write( GridFailed.ClientId)%>.Filter('asset_name LIKE \'%%\' ');" value="Refresh" />












      </div><!--END browsecenter-->
</div><!--END MAIN-->
      
      
      
      
      


      <script type=text/javascript>
  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

  
  function BrowseOnDoubleClick(SelectedGridRow)
  {
  
  	var itemvaluetmp;
	itemvaluetmp = SelectedGridRow.GetMember('uniqueid').Value;
	if (itemvaluetmp.substring(0,1) == 'P') 
	{
		itemvaluetmp = itemvaluetmp.replace('P_','');
		window.location.href = 'IDAM.aspx?page=Project&id=' + itemvaluetmp + '&type=project'
    
	} else {
		itemvaluetmp = itemvaluetmp.replace('C_','');
		window.location.href = 'IDAM.aspx?page=Browse&id=' + itemvaluetmp + '&type=category';
	}
	
	return true;
  
  
  
	
  }
  
    function highlightProjectToggle(checkbox,projectid)
  {
  var highlightbox;
  highlightbox = document.getElementById('projecthighlight_' + projectid);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  

  </script>
      
<div style="PADDING-LEFT:10px"><div style="PADDING-RIGHT:3px;PADDING-LEFT:3px;PADDING-BOTTOM:3px;PADDING-TOP:3px"></div></div>






<script language=javascript>
//thumbnailsarea.style.display = 'none';
</script>

