<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FileUpload_Multi.aspx.vb" EnableViewState="false" Inherits="IDAM5.FileUpload_Multi" %>
<%@ Register TagPrefix="upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>FileUpload_Multi</title>
		<SCRIPT Language="JavaScript">
/* 
This small function makes sure that the progress indicator and server-side
processing page receive the new progress ID we just created in the CodeBehind class.
Also, it pops up the progress window.
*/
function startupload() 
{		
		winstyle="height=200,width=500,status=no,scrollbars=no,toolbar=no,menubar=no,location=no";
		if(Form1.thefile1.value != "")
			window.open("ProgressIndicator.aspx?progressid=<%=progressid%>",null,winstyle);
		Form1.action = Form1.action + "?progressid=" + <%=progressid%>;
}
		</SCRIPT>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table>
				<tr>
					<td>
						<P>file:<BR>
							<Upload:InputFile id="thefile1" runat="server"></Upload:InputFile>
							Name<BR>
							<asp:textbox id="name1" runat="server">asset 1</asp:textbox><BR>
							description<BR>
							<asp:textbox id="description1" runat="server"></asp:textbox><BR>
						</P>
					</td>
					<td>
						<P>file:<BR>
							<upload:InputFile id="thefile2" runat="server"></upload:InputFile><BR>
							Name<BR>
							<asp:textbox id="name2" runat="server">asset 2</asp:textbox><BR>
							description<BR>
							<asp:textbox id="description2" runat="server"></asp:textbox><BR>
						</P>
					</td>
					<td>
						<P>file:<BR>
							<upload:InputFile id="thefile3" runat="server"></upload:InputFile><BR>
							Name<BR>
							<asp:textbox id="name3" runat="server">asset 3</asp:textbox><BR>
							description<BR>
							<asp:textbox id="description3" runat="server"></asp:textbox><BR>
						</P>
					</td>
				</tr>
				<tr>
					<td>
						<P>file:<BR>
							<upload:InputFile id="thefile4" runat="server"></upload:InputFile><BR>
							Name<BR>
							<asp:textbox id="name4" runat="server">asset 4</asp:textbox><BR>
							description<BR>
							<asp:textbox id="description4" runat="server"></asp:textbox><BR>
						</P>
					</td>
					<td>
						<P>file:<BR>
							<upload:InputFile id="thefile5" runat="server"></upload:InputFile><BR>
							Name<BR>
							<asp:textbox id="name5" runat="server">asset 5</asp:textbox><BR>
							description<BR>
							<asp:textbox id="description5" runat="server"></asp:textbox><BR>
						</P>
					</td>
					<td>
						<P>file:<BR>
							<upload:InputFile id="thefile6" runat="server"></upload:InputFile><BR>
							Name<BR>
							<asp:textbox id="name6" runat="server">asset 6</asp:textbox><BR>
							description<BR>
							<asp:textbox id="description6" runat="server"></asp:textbox><BR>
						</P>
					</td>
				</tr>
				<tr>
					<td>
						<P>file:<BR>
							<upload:InputFile id="thefile7" runat="server"></upload:InputFile><BR>
							Name<BR>
							<asp:textbox id="name7" runat="server">asset 7</asp:textbox><BR>
							description<BR>
							<asp:textbox id="description7" runat="server"></asp:textbox><BR>
						</P>
					</td>
					<td>
						<P>file:<BR>
							<upload:InputFile id="thefile8" runat="server"></upload:InputFile><BR>
							Name<BR>
							<asp:textbox id="name8" runat="server">asset 8</asp:textbox><BR>
							description<BR>
							<asp:textbox id="description8" runat="server"></asp:textbox><BR>
						</P>
					</td>
					<td>
						<P>file:<BR>
							<upload:InputFile id="thefile9" runat="server"></upload:InputFile><BR>
							Name<BR>
							<asp:textbox id="name9" runat="server">asset 9</asp:textbox><BR>
							description<BR>
							<asp:textbox id="description9" runat="server"></asp:textbox><BR>
						</P>
					</td>
				</tr>
				<tr>
					<td>
						<P>file:<BR>
							<upload:InputFile id="thefile10" runat="server"></upload:InputFile>
							<BR>
							Name<BR>
							<asp:textbox id="name10" runat="server">asset 10</asp:textbox><BR>
							description<BR>
							<asp:textbox id="description10" runat="server"></asp:textbox><BR>
						</P>
					</td>
				</tr>
			</table>
			<P>
				<upload:ProgressBar id="ProgressBar1" runat="server" Inline="True"></upload:ProgressBar>
				<BR>
				repositoryId:<BR>
				<asp:textbox id="repository_id" runat="server">1</asp:textbox><BR>
				projectid:<BR>
				<asp:textbox id="project_id" runat="server">21725714</asp:textbox><BR>
				securityId:<BR>
				<asp:textbox id="security_id" runat="server">3</asp:textbox><BR>
				userId<BR>
				<asp:textbox id="user_id" runat="server">1</asp:textbox><BR>
				categoryId<BR>
				<asp:textbox id="category_id" runat="server">0</asp:textbox><BR>
				instanceId:<BR>
				<asp:textbox id="instance_id" runat="server">fce</asp:textbox><BR>
				redirect to:<BR>
				<asp:textbox id="txtRedirect" runat="server">http://localhost/IDAMClient/Download.aspx</asp:textbox><BR>
				parentAssetID:<BR>
				<asp:TextBox id="parent_asset_id" runat="server"></asp:TextBox><BR>
				asset_type<BR>
				<asp:TextBox id="asset_type" runat="server"></asp:TextBox>
			</P>
			<P><BR>
			</P>
			<P></P>
			<P><asp:button id="button1" runat="server" Text="Create Posting"></asp:button></P>
			<P><asp:label id="Label1" runat="server">Label</asp:label></P>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
		</form>
	</body>
</HTML>
