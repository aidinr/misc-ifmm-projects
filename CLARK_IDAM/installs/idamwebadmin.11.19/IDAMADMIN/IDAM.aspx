<%@ Register TagPrefix="uc1" TagName="Explore" Src="Explore.ascx" %>
<%@ Register TagPrefix="uc3" TagName="CookieScripts" Src="includes/CookieScripts.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IDAM.aspx.vb" Inherits="IDAM5.IDAM"  ValidateRequest="False"%>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<%
ClientPageLoadTime = System.DateTime.Now
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<HTML lang="EN">
	<HEAD>
		<title>IDAM Version 5.01</title>
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
			<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
				<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
					<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
						<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
							<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
								<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
									<link href="treeStyle.css" type="text/css" rel="stylesheet">
										<link href="navStyle.css" type="text/css" rel="stylesheet" >
										    <link href="tabStripStyle.css" type="text/css" rel="stylesheet" />    
												<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
													<link href="calendarStyle.css" type="text/css" rel="stylesheet" />
<style>BODY { MARGIN: 0px 0px 0px 0px}</style>
<uc3:cookiescripts id="CookieScripts" runat="server"></uc3:cookiescripts>
<script language="javascript" type="text/javascript">
    <!--
      // Forces the treeview to adjust to the new size of its container          
      function resizeTree(DomElementId, NewPaneHeight, NewPaneWidth)
      {
      
       // alert(NewPaneWidth);
         setPaneSize(NewPaneWidth);
         //alert('set to:' + NewPaneWidth);

         
      }




  function onquicksearchkeydown(val,eventObj)
  {
  if (eventObj.keyCode==13){
  document.PageForm.idamaction.value='true';
  window.location = 'IDAM.aspx?page=Results&Type=QuickSearch&keyword=' + document.PageForm.keyword.value;
  return false;
  }
  
  }
        // Forces the grid to adjust to the new size of its container          
      function resizeGrid(DomElementId, NewPaneHeight, NewPaneWidth)
      {
		 //setHPaneSize(NewPaneHeight);
         //resizeGrid2(DomElementId, NewPaneHeight, NewPaneWidth);
         //set cookie for pane size
         //TreeViewExplore.Render();
         //alert('hey');


        // for(var prop in)
		//{
			//alert(prop);
		//}
        //alert('hey2');
      } 
      function explorecarousel()
      {
         //for(var prop in <% Response.Write(IDAMCarousel.ClientID) %>)
		//{
			//alert(prop);
		//}
		//alert(<% Response.Write(IDAMCarousel.ClientID) %>.GetProperty("X"));

		}
		

      //-->
      
      
	function makeArray(len) 
	{	for (var i = 0; i < len; i++)
		{	this[i] = null
		}
		this.length = len
	}



// array of day names

	var daynames = new makeArray(7)
	daynames[0] = "Sunday"
	daynames[1] = "Monday"
	daynames[2] = "Tuesday"
	daynames[3] = "Wednesday"
	daynames[4] = "Thursday"
	daynames[5] = "Friday"
	daynames[6] = "Saturday"
	

// array of month names

	var monthnames = new makeArray(12)
	monthnames[0] = "January"
	monthnames[1] = "February"
	monthnames[2] = "March"
	monthnames[3] = "April"
	monthnames[4] = "May"
	monthnames[5] = "June"
	monthnames[6] = "July"
	monthnames[7] = "August"
	monthnames[8] = "September"
	monthnames[9] = "October"
	monthnames[10] = "November"
	monthnames[11] = "December"


// define date variables

	var now = new Date()
	var day = now.getDay()
	var month = now.getMonth()
	var year = now.getFullYear()
	var date = now.getDate()
	var hour=now.getHours()
	var minutes=now.getMinutes()

// write date

	function writeDate()
	
	{	
	document.write("<FONT FACE='GENEVA, ARIAL, HELVETICA' SIZE='1'>" + daynames[day] + ", " + monthnames[month] + " " + date + ","+ year +"  "      +"</FONT>")
	}

</script>
	</HEAD>
	<body >
		<form id="PageForm" style="margin-top:0;" method="post" runat="server">
			<!-- Header go ----------------------------------------------------------------------->
			<div style="border-bottom:1px solid #D8D7DA;">
				<table id="table1" cellpadding=0 cellspacing=0 width="100%" bgColor="#f4f3f5" border="0">
					<tr>
						<td width="180"><asp:image id="Image1" runat="server" ImageUrl="images/spacer.gif" Width="210" height="1"></asp:image><br>
							<asp:image id="Image2" runat="server" ImageUrl="images/LOGO_template.gif" DescriptionUrl="logo"></asp:image></td>
						<td width="100%" align=right><div style="padding:10px">
						
						<%if  Session("login_name") = "Anonymous" then%>
						Version <%=CurrentVersion%><BR>
						You are not logged in.  [ <asp:hyperlink id="Hyperlink7" runat="server" BackColor="Transparent" BorderColor="Transparent"
									ForeColor="#EB6601" NavigateUrl="Login.aspx?Logout=True">login</asp:hyperlink> ]
						<%if trim(session("sUserAccess")) = "0" then%>[ <asp:hyperlink id="Hyperlink5"  Enabled=True runat="server" BackColor="Transparent" BorderColor="Transparent"
									ForeColor="#EB6601"  NavigateUrl="IDAM.aspx?page=QManager">queue manager</asp:hyperlink> ]<%end if%>
						<%else%>
						
						
						
						<FONT size="1">Welcome, <STRONG><%=Session("user_name")%></STRONG>&nbsp;&nbsp;Version <%=CurrentVersion%><BR>
								[ <asp:hyperlink id="HyperLink3" runat="server" BackColor="Transparent" BorderColor="Transparent"
									ForeColor="#EB6601" NavigateUrl="Login.aspx?Logout=True">log out</asp:hyperlink> ]
								<!--[ <asp:hyperlink id="HyperLink2" Enabled=False runat="server" BackColor="Transparent" BorderColor="Transparent"
									ForeColor="#EB6601" NavigateUrl="#">my info</asp:hyperlink> ]-->
								<%if trim(session("sUserAccess")) = "0" then%>[ <asp:hyperlink id="HyperLink1"  Enabled=True runat="server" BackColor="Transparent" BorderColor="Transparent"
									ForeColor="#EB6601"  NavigateUrl="IDAM.aspx?page=QManager">queue manager</asp:hyperlink> ]<%end if%>
								[ <asp:hyperlink id="Hyperlink4" Enabled=True  runat="server" BackColor="Transparent" BorderColor="Transparent"
									ForeColor="#EB6601"  NavigateUrl="IDAM.aspx?page=preferences">my preferences</asp:hyperlink> ]</STRONG></FONT>
									
						<%end if%>		
									
									
									
									
									</div></td>
									
					</tr>
					
					<tr>
						<td background="images/header_bg.gif" colSpan="2">
						<COMPONENTART:MENU id="MenuMAIN" runat="server" Orientation="Horizontal" CssClass="TopGroup" DefaultGroupCssClass="MenuGroup"
								SiteMapXmlFile="menuData.xml" DefaultItemLookID="DefaultItemLook" DefaultGroupItemSpacing="1" ImagesBaseUrl="images/" EnableViewState="true"
								ExpandDelay="100" ExpandOnClick="true">
								<ItemLooks>
									<componentart:ItemLook HoverCssClass="TopMenuItemHoverMainPage" LabelPaddingTop="2px" LabelPaddingRight="10px"
										LabelPaddingBottom="2px" ExpandedCssClass="TopMenuItemExpandedMainPage" LabelPaddingLeft="10px" LookId="TopItemLook"
										CssClass="TopMenuItemMainPage"></componentart:ItemLook>
									<componentart:ItemLook HoverCssClass="MenuItemHover" LabelPaddingTop="3px" LeftIconHeight="18px" LabelPaddingRight="10px"
										LabelPaddingBottom="4px" ExpandedCssClass="MenuItemHover" LabelPaddingLeft="10px" LeftIconWidth="20px"
										LookId="DefaultItemLook" CssClass="MenuItem"></componentart:ItemLook>
									<componentart:ItemLook LookId="BreakItem" CssClass="MenuBreak"></componentart:ItemLook>
								</ItemLooks>
							</COMPONENTART:MENU>

						</td>
					</tr>
					
				</table>
			</div>
			<asp:image id="Image3" runat="server" ImageUrl="images/spacer.gif" Width="1" height="10"></asp:image><br>
			<!-- Header no ----------------------------------------------------------------------->

			<COMPONENTART:SPLITTER id="Splitter1" runat="server" ImagesBaseUrl="images/" Height="16000px" FillHeight="false" FillWidth="true">
				<LAYOUTS>
					<ComponentArt:SplitterLayout id="SplitterListLayout">
						<PANES Orientation="Horizontal" SplitterBarWidth="5" SplitterBarActiveCssClass="ActiveSplitterBar"
							SplitterBarCollapsedCssClass="CollapsedHorizontalSplitterBar" SplitterBarCssClass="HorizontalSplitterBar"
							SplitterBarCollapseImageHeight="116" SplitterBarCollapseImageWidth="10" SplitterBarExpandHoverImageUrl="spacer.gif"
							SplitterBarExpandImageUrl="spacer.gif" SplitterBarCollapseHoverImageUrl="spacer.gif"
							SplitterBarCollapseImageUrl="spacer.gif" >
							<ComponentArt:SplitterPane Width="220"  Height="100%"  CssClass="SplitterPane" ClientSideOnResize="resizeTree" MaxWidth="350" 
								MinWidth="100" PaneContentId="TreeViewContent"></ComponentArt:SplitterPane>
							<ComponentArt:SplitterPane Width="100%"  Height="100%"  CssClass="SplitterPaneRightNew" ClientSideOnResize="resizeGrid" MinWidth="100"
								PaneContentId="GridContent"></ComponentArt:SplitterPane>
						</PANES>
					</ComponentArt:SplitterLayout>
					<ComponentArt:SplitterLayout id="SplitterPortalLayout">
						<PANES Orientation="Horizontal" SplitterBarWidth="5" SplitterBarActiveCssClass="ActiveSplitterBar"
							SplitterBarCollapsedCssClass="CollapsedHorizontalSplitterBar" SplitterBarCssClass="HorizontalSplitterBar"
							SplitterBarCollapseImageHeight="116" SplitterBarCollapseImageWidth="10" SplitterBarExpandHoverImageUrl="spacer.gif"
							SplitterBarExpandImageUrl="spacer.gif" SplitterBarCollapseHoverImageUrl="spacer.gif"
							SplitterBarCollapseImageUrl="spacer.gif" >
							<ComponentArt:SplitterPane Width="0"  Height="100%"  CssClass="SplitterPane" MaxWidth="0" 
								MinWidth="0" PaneContentId="TreeViewContent"></ComponentArt:SplitterPane>
							<ComponentArt:SplitterPane Width="100%"  Height="100%"  CssClass="SplitterPaneRightNew" ClientSideOnResize="resizeGrid" MinWidth="100"
								PaneContentId="GridContent"></ComponentArt:SplitterPane>
						</PANES>
					</ComponentArt:SplitterLayout>					
				</LAYOUTS>
				<CONTENT>
					<ComponentArt:SplitterPaneContent id="TreeViewContent">
						<DIV class="SplitterPaneleftsidetest" ><!--SplitterPaneleftsidetest-->
							<DIV class="SplitterPaneleftsidetestinner" ><!--SplitterPaneleftsidetestinner-->
								<table cellpadding="0"><tr><td align="left" width="5"></td><td align="left" width="100%"><font size="1" face="Verdana"><b>Quick Search</b></font></td><td align="right" width="5"><!--<img src="images/optionsbutton.gif">--></td></tr></table>
								<img src="images/spacer.gif" width="1" height="3"><br>
								<style>
								.quicksearchkeywordstyle{
								border:0px #CCCCCC solid;!important border:0px #CCCCCC solid;
								}
								</style>
								<div style="border:0px solid;width:100%;">
									<table cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td width="100%"><div class="quicksearchkeywordstyle"><asp:TextBox id="quicksearchkeyword" Width=100% style="border-left:1px solid #969698;border-top:1px solid #969698;border-bottom:1px solid #969698;height:16px;width=100%;background-color:white;z-index:1;" runat="server"></asp:TextBox></div></td>
											<td width="3"><img style="margin-top:1px;!important margin-top:0px; " border="0" id=lquicksearchbutton src="images/search_butt.gif" runat=server onclick="javascript:document.PageForm.idamaction.value='true';window.location = 'IDAM.aspx?page=Results&Type=QuickSearch&keyword=' + document.PageForm.quicksearchkeyword.value;"></td>
										</tr>
									</table>
								</div>
								<asp:TextBox id="idamactionserver" name="idamactionserver" Visible="False" runat="server"></asp:TextBox>
								<input type="hidden" name="idamaction" id="idamaction" value="">
								<input type="hidden" name="ExploreTreeNewselectednode" id="ExploreTreeNewselectednode" value="">
								<input type="hidden" name="ExploreTreeNewselectednodeaction" id="ExploreTreeNewselectednodeaction" value="">
								<input type="hidden" name="ProjectExploreTreeNewselectednode" id="ProjectExploreTreeNewselectednode" value="">
								<input type="hidden" name="ProjectExploreTreeNewselectednodeaction" id="ProjectExploreTreeNewselectednodeaction" value="">
								<input type="hidden" name="ProjectAssetselectednodeaction" id="ProjectAssetselectednodeaction" value="">								
							</DIV><!--SplitterPaneleftsidetestinner-->
						</DIV><!--SplitterPaneleftsidetest-->
						
						
						
						<DIV style="PADDING-LEFT: 10px;border:1px;">
							<DIV style="PADDING-RIGHT: 3px; PADDING-LEFT: 3px; PADDING-BOTTOM: 3px; PADDING-TOP: 3px"></DIV>
						</DIV>
						
						
						
						<DIV class="Dock" id="LeftColumn">
							<ComponentArt:Snap id="TreeViewExplore" runat="server" Width="100%" Height="100%" MustBeDocked="true" CurrentDockingIndex="0"
								DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn"
								DockingContainers="LeftColumn" AutoCallBackOnDock="true" AutoCallBackOnCollapse="true" AutoCallBackOnExpand="true">
								<Header>
									<div style="CURSOR: move; width: 100%;"><!--Header Div-->
										<table class="SnapHeader" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" onmousedown="TreeViewExplore.StartDragging(event);">Browse</td>
												<td width="5" style="cursor: hand" align="right">
												
																	<ComponentArt:Menu id="MenuExploreTreeNewContextCategory" 
																		Orientation="Vertical"
																		DefaultGroupCssClass="MenuGroup"
																		SiteMapXmlFile="menuDataExploreTreeNewContextCategory.xml"
																		DefaultItemLookID="DefaultItemLook"
																		DefaultGroupItemSpacing="1"
																		ImagesBaseUrl="images/"
																		EnableViewState="false"
																		ContextMenu="Custom"
																		ClientSideOnItemSelect="contextMenuClickHandler"
																		runat="server">
																		<ItemLooks>
																		<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
																		<ComponentArt:ItemLook LookID="BreakItem" CssClass="MenuBreak" />
																		</ItemLooks>
																	</ComponentArt:Menu>

																	<ComponentArt:Menu id="MenuExploreTreeNewContextProject" 
																		Orientation="Vertical"
																		DefaultGroupCssClass="MenuGroup"
																		SiteMapXmlFile="menuDataExploreTreeNewContextProject.xml"
																		DefaultItemLookID="DefaultItemLook"
																		DefaultGroupItemSpacing="1"
																		ImagesBaseUrl="images/"
																		EnableViewState="false"
																		ContextMenu="Custom"
																		ClientSideOnItemSelect="contextMenuClickHandler"
																		runat="server">
																		<ItemLooks>
																		<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
																		<ComponentArt:ItemLook LookID="BreakItem" CssClass="MenuBreak" />
																		</ItemLooks>
																	</ComponentArt:Menu>	
																	
																	<ComponentArt:Menu id="MenuExploreTreeNewContextProjectFolder" 
																		Orientation="Vertical"
																		DefaultGroupCssClass="MenuGroup"
																		SiteMapXmlFile="menuDataExploreTreeNewContextProjectFolder.xml"
																		DefaultItemLookID="DefaultItemLook"
																		DefaultGroupItemSpacing="1"
																		ImagesBaseUrl="images/"
																		EnableViewState="false"
																		ContextMenu="Custom"
																		ClientSideOnItemSelect="contextMenuClickHandler"
																		runat="server">
																		<ItemLooks>
																		<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
																		<ComponentArt:ItemLook LookID="BreakItem" CssClass="MenuBreak" />
																		</ItemLooks>
																	</ComponentArt:Menu>			
																	
																	<ComponentArt:Menu id="MenuExploreTreeNewContextAsset" 
																		Orientation="Vertical"
																		DefaultGroupCssClass="MenuGroup"
																		SiteMapXmlFile="menuDataExploreTreeNewContextAsset.xml"
																		DefaultItemLookID="DefaultItemLook"
																		DefaultGroupItemSpacing="1"
																		ImagesBaseUrl="images/"
																		EnableViewState="false"
																		ContextMenu="Custom"
																		ClientSideOnItemSelect="contextMenuClickHandler"
																		runat="server">
																		<ItemLooks>
																		<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
																		<ComponentArt:ItemLook LookID="BreakItem" CssClass="MenuBreak" />
																		</ItemLooks>
																	</ComponentArt:Menu>		
																	
																	<ComponentArt:Menu id="MenuExploreTreeNewContextPortalMain" 
																		Orientation="Vertical"
																		DefaultGroupCssClass="MenuGroup"
																		SiteMapXmlFile="menuDataExploreTreeNewContextPortalMain.xml"
																		DefaultItemLookID="DefaultItemLook"
																		DefaultGroupItemSpacing="1"
																		ImagesBaseUrl="images/"
																		EnableViewState="false"
																		ContextMenu="Custom"
																		ClientSideOnItemSelect="contextMenuClickHandler"
																		runat="server">
																		<ItemLooks>
																		<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
																		<ComponentArt:ItemLook LookID="BreakItem" CssClass="MenuBreak" />
																		</ItemLooks>
																	</ComponentArt:Menu>	
																	
																	<ComponentArt:Menu id="MenuExploreTreeNewContextPortal" 
																		Orientation="Vertical"
																		DefaultGroupCssClass="MenuGroup"
																		SiteMapXmlFile="menuDataExploreTreeNewContextPortal.xml"
																		DefaultItemLookID="DefaultItemLook"
																		DefaultGroupItemSpacing="1"
																		ImagesBaseUrl="images/"
																		EnableViewState="false"
																		ContextMenu="Custom"
																		ClientSideOnItemSelect="contextMenuClickHandler"
																		runat="server">
																		<ItemLooks>
																		<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
																		<ComponentArt:ItemLook LookID="BreakItem" CssClass="MenuBreak" />
																		</ItemLooks>
																	</ComponentArt:Menu>	
																	
																	<ComponentArt:Menu id="MenuExploreTreeNewContextPortalPage" 
																		Orientation="Vertical"
																		DefaultGroupCssClass="MenuGroup"
																		SiteMapXmlFile="menuDataExploreTreeNewContextPortalPage.xml"
																		DefaultItemLookID="DefaultItemLook"
																		DefaultGroupItemSpacing="1"
																		ImagesBaseUrl="images/"
																		EnableViewState="false"
																		ContextMenu="Custom"
																		ClientSideOnItemSelect="contextMenuClickHandler"
																		runat="server">
																		<ItemLooks>
																		<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
																		<ComponentArt:ItemLook LookID="BreakItem" CssClass="MenuBreak" />
																		</ItemLooks>
																	</ComponentArt:Menu>	
																	
																	<ComponentArt:Menu id="MenuExploreTreeNewContextPortalPortlet" 
																		Orientation="Vertical"
																		DefaultGroupCssClass="MenuGroup"
																		SiteMapXmlFile="menuDataExploreTreeNewContextPortalPortlet.xml"
																		DefaultItemLookID="DefaultItemLook"
																		DefaultGroupItemSpacing="1"
																		ImagesBaseUrl="images/"
																		EnableViewState="false"
																		ContextMenu="Custom"
																		ClientSideOnItemSelect="contextMenuClickHandler"
																		runat="server">
																		<ItemLooks>
																		<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
																		<ComponentArt:ItemLook LookID="BreakItem" CssClass="MenuBreak" />
																		</ItemLooks>
																	</ComponentArt:Menu>																																																	
				
												</td>
												<td width="5" style="cursor: hand" align="right"><img onclick="TreeViewExplore.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0">
												</td>
											</tr>
										</table>
									</div><!--Header Div-->
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeader" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td onmousedown="TreeViewExplore.StartDragging(event);">Browse</td>
												<td width="10" style="cursor: hand" align="right"><img onclick="TreeViewExplore.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									<div class="SnapTreeview" style="height:100%;padding:0px;background-color:#F8F8F8;"><uc1:Explore id="Explore" runat="server"></uc1:Explore></div><div style="padding-top:0px"></div>
								</Content>
							</ComponentArt:Snap><!--TreeViewExplore-->
							
							
							
							
							<!--
							<ComponentArt:Snap id="IDAMPreview" runat="server" Width="100%" Height="200px" MustBeDocked="true" DockingStyle="TransparentRectangle" CurrentDockingIndex="1"
								DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" AutoCallBackOnDock="true" AutoCallBackOnCollapse="true" AutoCallBackOnExpand="true">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeader" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td onmousedown="IDAMPreview.StartDragging(event);">Image Preview</td>
												<td width="10" style="cursor: hand" align="right"><img onclick="IDAMPreview.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeader" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td onmousedown="IDAMPreview.StartDragging(event);">Image Preview</td>
												<td width="10" style="cursor: hand" align="right"><img onclick="IDAMPreview.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									<div class="SnapTreeview" >
										<COMPONENTART:CALLBACK id="ProjectImage" runat="server" CacheContent="false">
											<CONTENT>
												<TABLE height="100" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px">
																	<asp:Literal ID="CurrentImageName" Runat=server></asp:Literal><br>
																		<asp:Image id="CurrentImage" runat="server" ImageUrl="~/common/images/spacer.gif"></asp:Image></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px">Loading...
																	</TD>
																	<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
									</div>
									<div style="padding-top:4px"></div>
								</Content>
							</ComponentArt:Snap>
							-->
							
						<ComponentArt:Snap id="DownloadBatchJobs" runat="server" Visible="false" Fillcontainer="true" Height="200px" MustBeDocked="false" DockingStyle="TransparentRectangle" CurrentDockingIndex="1"
								DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" AutoCallBackOnDock="true" AutoCallBackOnCollapse="true" AutoCallBackOnExpand="true">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeader" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td onmousedown="DownloadBatchJobs.StartDragging(event);" >Download History</td>
												<td width="10" style="cursor: hand" align="right"><img onclick="DownloadBatchJobs.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeader" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td onmousedown="DownloadBatchJobs.StartDragging(event);">Download Buffer</td>
												<td width="10" style="cursor: hand" align="right"><img onclick="DownloadBatchJobs.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>								
									<div class="SnapTreeview" >	
										<div class="carouselshortlist">
											<COMPONENTART:CALLBACK id="DownloadBatchJobsCallback" runat="server" CacheContent="false">
													<CONTENT>
												<asp:Literal ID="DownloadLinkURLLiteral" Runat=server></asp:Literal>  -	<asp:HyperLink visible="False"  id="LastDownloadRequestLink" runat="server">download</asp:HyperLink>														
													</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px">Processing Download Request...
																	</TD>
																	<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
									</div><!--carouselshortlist-->
								</div><!--SnapTreeview-->
								<div style="padding-top:4px"></div>
							</Content>
						</ComponentArt:Snap>
							
							<ComponentArt:Snap id="IDAMDownloadBuffer" runat="server" Visible="false" Fillcontainer="true" Height="200px" MustBeDocked="false" DockingStyle="TransparentRectangle" CurrentDockingIndex="1"
								DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" AutoCallBackOnDock="true" AutoCallBackOnCollapse="true" AutoCallBackOnExpand="true">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeader" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td onmousedown="IDAMDownloadBuffer.StartDragging(event);" >Download Buffer</td>
												<td width="10" style="cursor: hand" align="right"><img onclick="IDAMDownloadBuffer.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeader" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td onmousedown="IDAMDownloadBuffer.StartDragging(event);">Download Buffer</td>
												<td width="10" style="cursor: hand" align="right"><img onclick="IDAMDownloadBuffer.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>								
									<div class="SnapTreeview" >	
										<div class="carouselshortlist">
											<COMPONENTART:CALLBACK id="DownloadBufferCallback" runat="server" CacheContent="false">
													<CONTENT>
														<div style="padding-bottom:4px;"><asp:CheckBox id="CheckBoxUseDownloadBuffer" onclick="setDownloadBuffer();" runat="server"></asp:CheckBox>&nbsp;Turn On</div>
														<asp:Literal ID="LiteralDownloadBuffer" Runat=server></asp:Literal>
														<asp:Repeater id="RepeaterDownloadBuffer" Runat="server" >
														<ItemTemplate>
															<table width="100%" cellpadding="0" cellspacing="0"  border="0">
																<tr valign="top">
																	<td width="70" align=left >
																		<div class="<%#CheckHighlightonCarouselImage(CType(DataBinder.Eval(Container.DataItem, "asset_id"), Long))%>">
																		<a href="IDAM.aspx?page=Asset&Id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=asset"><img alt="" border="0" src="<%=IDAM5.BaseIDAMSession.Config.URLLocationPreview%>?id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&instance=<%#DataBinder.Eval(Container.DataItem, "instance")%>&type=asset&size=2&height=65&width=65"></a>
																		</div></td>
																		
																	<td align=left >
																		<div ><a href="IDAM.aspx?page=Asset&Id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=asset"><%#DataBinder.Eval(Container.DataItem, "name")%></a></div>
																		
																		<div >
																			Filesize: <%#FormatSize(CType(DataBinder.Eval(Container.DataItem, "filesize"), Long))%><br>
																		</div>
																		<div >
																			<a href="javascript:RemoveItemFromCarousel(<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);">remove</a>
																		</div>				
																	</td>
																</tr>
															</table>
														<div style="padding:5px;">
															<div class="asset_links">
															</div>
														</div>
														</ItemTemplate>
														<HeaderTemplate>
														<div style="padding-top:15px;"></div>
														<!--Carousel Header--><div id="side_results" name="side_results" style="width: 100%; height: 100%; overflow: auto;" > 
														</HeaderTemplate>
														<FooterTemplate>
																<div >
																	<table width="235" cellpadding="0" cellspacing="0" border="0">
																		<tr align="center" valign="top">
																			<td><br></td>
																		</tr>
																	</table>
																</div>
														</div><!--Carousel Header-->
														</FooterTemplate> 
														</asp:Repeater>
													</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px">Loading...
																	</TD>
																	<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
									</div><!--carouselshortlist-->
								</div><!--SnapTreeview-->
								<div style="padding-top:4px"></div>
							</Content>
						</ComponentArt:Snap><!--IDAMCarousel-->
							
<script language="javascript">
var bDownloadBuffer = GetCookie('bDownloadBuffer');
function setDownloadBuffer(){
if (bDownloadBuffer==1) {
	bDownloadBuffer=0
	SetCookie('bDownloadBuffer', '0', exp);
	}else	{
	bDownloadBuffer = 1
	SetCookie('bDownloadBuffer', '1', exp);
	}
}
</script>

							
							
							
							
							
							
							<%if  Session("login_name") <> "Anonymous" then%>
							
							<ComponentArt:Snap id="IDAMCarousel" runat="server" Fillcontainer="true" Height="200px" MustBeDocked="false" DockingStyle="TransparentRectangle" CurrentDockingIndex="1"
								DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" AutoCallBackOnDock="true" AutoCallBackOnCollapse="true" AutoCallBackOnExpand="true">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeader" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td onmousedown="IDAMCarousel.StartDragging(event);" onmouseup="explorecarousel();">Carousels</td>
												<td width="10" style="cursor: hand" align="right"><img onclick="IDAMCarousel.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeader" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td onmousedown="IDAMCarousel.StartDragging(event);">Carousels</td>
												<td width="10" style="cursor: hand" align="right"><img onclick="IDAMCarousel.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>								
									<div class="SnapTreeview" >	
										<div class="carouselshortlist">
										<div style="padding-top:5px;padding-bottom:10px;text-align:left;">[ <a href="IDAM.aspx?Page=CarouselPublic"><asp:Literal ID="LiteralPublicCarouselLink" Runat=server>View Public Carousels</asp:Literal></a> ]</div>
											<COMPONENTART:CALLBACK id="carousel_callback" runat="server" CacheContent="false">
													<CONTENT>
														<asp:Literal ID="carousel_title_literal" Runat=server>My carousels</asp:Literal>
														<asp:DropDownList   DataTextField="name" DataValueField="carrousel_id" id="carousel_select" style="width:100%;visibility: inherit;" runat="server"></asp:DropDownList>
														<asp:Literal ID="carousel_literal" Runat=server></asp:Literal>
														<asp:Repeater id="repeater_carousel_shortlist" Runat="server" >
															<ItemTemplate>
																<table width="100%" cellpadding="0" cellspacing="0"  border="0">
																	<tr valign="top">
																		<td width="70" align=left >
																			
																			<div class="<%#CheckHighlightonCarouselImage(CType(DataBinder.Eval(Container.DataItem, "asset_id"), Long))%>">
																			<a href="IDAM.aspx?page=Asset&Id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=asset"><img alt="" border="0" src="<%=IDAM5.BaseIDAMSession.Config.URLLocationPreview%>?id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&instance=<%#DataBinder.Eval(Container.DataItem, "instance")%>&type=asset&size=2&height=65&width=65"></a>
																			</div></td>
																			
																		<td align=left >
																			<div ><a href="IDAM.aspx?page=Asset&Id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=asset"><%#DataBinder.Eval(Container.DataItem, "name")%></a></div>
																			
																			<div >
																				Filesize: <%#FormatSize(CType(DataBinder.Eval(Container.DataItem, "filesize"), Long))%><br>
																			</div>
																			<div >
																				<a href="javascript:RemoveItemFromCarousel(<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);">remove</a>
																			</div>				
																		</td>
																	</tr>
																</table>
															<div style="padding:5px;">
																<div class="asset_links">
																</div>
															</div>
															</ItemTemplate>
															<HeaderTemplate>
															<div style="padding-top:15px;"></div>
															<!--Carousel Header--><div id="side_results" name="side_results" style="width: 100%; height: 100%; overflow: auto;" > 
															</HeaderTemplate>
															<FooterTemplate>
															<!--Carousel Header-->
															</FooterTemplate> 
														</asp:Repeater>
													</div>
																	<div >
																		<table width="235" cellpadding="0" cellspacing="0" border="0">
																			<tr align="center" valign="top">
																				<td> 		<asp:Literal ID="carousel_footer_literal" Runat="server"></asp:Literal>
																				</td>
																				
																			</tr>
																		</table>
																	</div>
																</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px">Loading...
																	</TD>
																	<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
										
									</div><!--carouselshortlist-->
								</div><!--SnapTreeview-->
								<div style="padding-top:4px"></div>
							</Content>
						</ComponentArt:Snap><!--IDAMCarousel-->
						<%end if%>
					</DIV> <!--Dock-->
				</ComponentArt:SplitterPaneContent>
					<ComponentArt:SplitterPaneContent id="GridContent">
					     <COMPONENTART:TABSTRIP id="TabStrip1" runat="server" ImagesBaseUrl="images/" 
								CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
								DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1">
								<ItemLooks>
									<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />									
									<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
									<ComponentArt:ItemLook LookId="SelectedTabLookHome" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon_home.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />			
									<ComponentArt:ItemLook LookId="SelectedTabLookBrowse" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon_browse.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />	
									<ComponentArt:ItemLook LookId="SelectedTabLookAS" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon_as.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />																												
									<ComponentArt:ItemLook LookId="SelectedTabLookProject" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon_project.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />	
									<ComponentArt:ItemLook LookId="SelectedTabLookAsset" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon_asset.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />		
									<ComponentArt:ItemLook LookId="SelectedTabLookApprovalManager" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon_approval.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />	
									<ComponentArt:ItemLook LookId="SelectedTabLookCarousel" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon_carousel.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />																											
									<ComponentArt:ItemLook LookId="TabLookBrowse" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_browse.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon_browse.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />	
									<ComponentArt:ItemLook LookId="TabLookHome" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_home.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon_home.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />		
									<ComponentArt:ItemLook LookId="TabLookProject" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_project.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon_project.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />	
									<ComponentArt:ItemLook LookId="TabLookAS" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_as.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon_as.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />		
									<ComponentArt:ItemLook LookId="TabLookAsset" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_asset.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon_asset.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />			
									<ComponentArt:ItemLook LookId="TabLookConfiguration" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_config.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon_config.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />	
									<ComponentArt:ItemLook LookId="TabLookApprovalManager" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_approval.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon_approval.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />	
									<ComponentArt:ItemLook LookId="TabLookCarousel" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_carousel.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon_carousel.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />											
									<ComponentArt:ItemLook LookId="SelectedTabLookConfiguration" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_config.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />																																																											
								</ItemLooks>
							</COMPONENTART:TABSTRIP>
							<style>
							.rightsideplaceholderdiv
							{
							padding-right: 10px !important;
							margin-left: 1px !important;
							<%if Request.Browser.Browser = "IE" then
								response.write ("margin-top: -.2em !important;")
							  else
								response.write ("margin-top: -.4em !important;")
							  end if%>
							padding-right:0px;
							margin-left:1px;
							margin-top:-2px;
							}
							.rightsideplaceholderdivheading
							{
							border:#959595 1px solid;
							height:10px;
							 background-image: url(images/headingtop.gif);
							}							
							</style>
						<div class="rightsideplaceholderdiv">	
							<div class="rightsideplaceholderdivheading">
							</div>
							<table width=100% cellpadding=0 cellspacing=0><tr><td>
							<asp:PlaceHolder id="PageLoader" runat="server"></asp:PlaceHolder></td></tr></table>
						</div>
						</ComponentArt:SplitterPaneContent>
						<ComponentArt:SplitterPaneContent id="DetailsContent"></ComponentArt:SplitterPaneContent>
					</CONTENT>
				</COMPONENTART:SPLITTER>
				
<COMPONENTART:CALLBACK id="CallbackDownloadCheck" runat="server" CacheContent="false" >
<CONTENT>	
<asp:Literal id="LiteralDownloadCheck" Text="" visible=true runat="server" />											
</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>	
Page Load (Internal Code Execution): <%=ElapsePageLoadTime%> ms | Client Page Load (Client Browser Execution): <%=System.DateTime.Now.Subtract(ClientPageLoadTime).Milliseconds%> ms 
		</form>
		
<script language="javascript">
function Object_PopUp_Carousel(tab)
{
  var dropdown = document.getElementById('IDAMCarousel_carousel_select');
  var myindex  = dropdown.selectedIndex;
  var SelValue = dropdown.options[myindex].value;
  if (tab == 'view')
  {
  Object_PopUp('CarouselNew.aspx?Carousel_id=' + SelValue + '&tab=view','New_Carousel',700,780);
  } else {
  Object_PopUp('CarouselNew.aspx?Carousel_id=' + SelValue,'New_Carousel',700,780);
  }
}	

function Object_PopUp_IDAMExpress()
{
  var dropdown = document.getElementById('IDAMCarousel_carousel_select');
  var myindex  = dropdown.selectedIndex;
  var SelValue = dropdown.options[myindex].value;
  Object_PopUp('IDAMExpress.aspx?Carousel_id=' + SelValue,'IDAMExpress',700,1200);
}	

function Object_PopUp_IDAMExpressAssets(assets)
{
  Object_PopUp('IDAMExpress.aspx?assetids=' + assets,'IDAMExpress',700,1200);
}	



function Object_gotoCarousel()
{
  var dropdown = document.getElementById('IDAMCarousel_carousel_select');
  var myindex  = dropdown.selectedIndex;
  var SelValue = dropdown.options[myindex].value;
  if (SelValue != '')
  {
    window.location='IDAM.aspx?page=Carousel&id=' + SelValue;
  }
}	




function Object_PopUp_Upload(projectid,categoryid)
{

  //add something here to redirect to java or form based upload	

  Object_PopUp('includes/java/UploadJava.aspx?projectid=' + projectid + '&categoryid=' + categoryid,'Upload',550,780);
}	


function Object_PopUp_UploadExplore(objectid)
{
  Object_PopUp('includes/java/UploadJava.aspx?objectid=' + objectid,'Upload',550,780);
  //Object_PopUp('Upload.aspx?objectid=' + objectid,'Upload',700,780);
}		

function Object_PopUp_Repository(objectid)
{
  Object_PopUp('RepositoryEdit.aspx?id=' + objectid,'Repository',550,480);
  //Object_PopUp('Upload.aspx?objectid=' + objectid,'Upload',700,780);
}	

function Object_PopUp_User(objectid)
{
  Object_PopUp('UserEdit.aspx?id=' + objectid,'user',850,840);
}	

function Object_PopUp_UserNew()
{
  Object_PopUp('UserEdit.aspx','user',850,840);
}

function Object_PopUp_Contact(objectid)
{
  Object_PopUp('UserEdit.aspx?id=' + objectid + '&type=contact','user',750,840);
}	

function Object_PopUp_ContactNew()
{
  Object_PopUp('UserEdit.aspx?type=contact','user',750,840);
}

function Object_PopUp_Client(objectid)
{
  Object_PopUp('ClientEdit.aspx?id=' + objectid,'user',750,840);
}	

function Object_PopUp_ClientNew()
{
  Object_PopUp('ClientEdit.aspx','user',750,840);
}

function Object_PopUp_Keyword(URL)
{
  Object_PopUp(URL,'Keyword',600,700);
}

function Object_PopUp_UDF(URL)
{
  Object_PopUp(URL,'UDF',500,1124);
}

function Object_PopUp_Roles(URL)
{
  Object_PopUp(URL,'Roles',800,800);
}

function Object_PopUp_Splash(URL)
{
  Object_PopUp(URL,'Splash',295,485);
}

function Object_PopUp_Recover(URL)
{
  Object_PopUp(URL,'Roles',800,1124);
}




function Object_PopUp_Vendor(objectid)
{
  Object_PopUp('VendorEdit.aspx?id=' + objectid,'user',750,840);
}	

function Object_PopUp_VendorNew()
{
  Object_PopUp('VendorEdit.aspx','user',750,840);
}

function Object_PopUp_Group(objectid)
{
  Object_PopUp('GroupEdit.aspx?id=' + objectid,'user',750,840);
}	

function Object_PopUp_GroupNew()
{
  Object_PopUp('GroupEdit.aspx','user',750,840);
}



function Object_PopUp_RepositoryNew()
{
  Object_PopUp('RepositoryEdit.aspx','Repository',550,480);
  //Object_PopUp('Upload.aspx?objectid=' + objectid,'Upload',700,780);
}	
			
			
function BrowseOnSingleClick(item)
  {
	//<% 'Response.Write(ProjectInformation.ClientID) %>.Callback(item.GetMember('asset_id').Value);

	var itemvaluetmp;
	itemvaluetmp = item.GetMember('uniqueid').Value;
	itemvaluenametmp = item.GetMember('name').Value;
	if (itemvaluetmp.substring(0,1) == 'P') 
//alert(itemvaluetmp+',Project'+','+itemvaluenametmp);
	{
		itemvaluetmp = itemvaluetmp.replace('P_','');
		//<% Response.Write(ProjectImage.ClientID) %>.Callback(itemvaluetmp+',Project'+','+itemvaluenametmp);
    
	}
	return true;
  }  
  
  
  			
function BrowseOnSingleClickAssets(item)
  {
	//<% 'Response.Write(ProjectInformation.ClientID) %>.Callback(item.GetMember('asset_id').Value);

	var itemvaluetmp;
	var itemvaluenametmp;
	var itemvaluefiletypetmp;
	itemvaluetmp = item.GetMember('asset_id').Value;
	itemvaluenametmp = item.GetMember('name').Value;
	itemvaluefiletypetmp = item.GetMember('timage').Value;


		//<% Response.Write(ProjectImage.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);

	return true;
  }  
  
  	
function BrowseOnSingleClickAssetsLegacy(item)
  {
	//<% 'Response.Write(ProjectInformation.ClientID) %>.Callback(item.GetMember('asset_id').Value);

	var itemvaluetmp;
	var itemvaluenametmp;
	var itemvaluefiletypetmp;
	itemvaluetmp = item.GetMember('asset_id').Value;
	itemvaluenametmp = item.GetMember('name').Value;
	itemvaluefiletypetmp = item.GetMember('imagesource').Value;


		//<% Response.Write(ProjectImage.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);

	return true;
  }  
		
		
		
function ChangeDefaultCarousel(dropdown)
  {
  var myindex  = dropdown.selectedIndex
  var SelValue = dropdown.options[myindex].value

  
  <% Response.Write(carousel_callback.ClientID) %>.Callback(SelValue);
  return true;
  
  }			
  
  
  function RemoveItemFromCarousel(id)
  {
  
  <% Response.Write(carousel_callback.ClientID) %>.Callback(id + ',remove');
  //return true;
  
  }	
  
    function RemoveCarousel(id)
  {
  
  <% Response.Write(carousel_callback.ClientID) %>.Callback(id + ',removecarousel');
  return true;
  
  }	
	
function AddToCarouseldeletthis(rowId)
  {
  //check to see if multi select
 
  <% Response.Write(carousel_callback.ClientID) %>.Callback('1,' + rowId + ',AddToCarousel');
  }	

function AddDownloadJob(JobID)
  { 
  <% Response.Write(DownloadBatchJobsCallback.ClientID) %>.Callback(JobID);
  }	

function DownloadCarousel2()
  { 
    var myindex  = document.getElementById('IDAMCarousel_carousel_select').selectedIndex
  var SelValue = document.getElementById('IDAMCarousel_carousel_select').options[myindex].value
  <% Response.Write(DownloadBatchJobsCallback.ClientID) %>.Callback('carousel',SelValue );
  }	
  function DownloadCarouselUsingWS()
  { 
    var myindex  = document.getElementById('IDAMCarousel_carousel_select').selectedIndex
  var SelValue = document.getElementById('IDAMCarousel_carousel_select').options[myindex].value
  var answer = false
<%  If IDAM5.Functions.getRole("DOWNLOAD_ASSOCIATED_CAROUSEL", Session("Roles")) Then %>
answer =   confirm ("Do you want to include associated assets?")
<%end if%>
if (answer)
  window.open('<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownloadGetMulti & "?carouselid="%>' + SelValue + '<%="&instance=" & idam5.BaseIDAMSession.Config.IDAMInstance%>&uid=<%=Session("sUserID")%>&associated=true','ZippingFiles','width=500,height=75,location=no');
  else
  window.open('<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownloadGetMulti & "?carouselid="%>' + SelValue + '<%="&instance=" & idam5.BaseIDAMSession.Config.IDAMInstance%>&uid=<%=Session("sUserID")%>','ZippingFiles','width=500,height=75,location=no');
  
  }	
function openCarouselNew()
{
	window.open('CarouselNew.aspx','New_Carousel','width=700,height=700,location=no');
	return true;
}		

function NavigateToCarousel()
{

	ChangeDefaultCarousel(document.getElementById('IDAMCarousel_carousel_select'));
}	

function NavigateToAsset(id)
{
	window.location = 'IDAM.aspx?page=Asset&Id=' + id + '&type=Asset';
}	

//var callbackparamtemp = '2050,Project,10th Stret,NW'
//setInterval('<% Response.Write(ProjectImage.ClientID) %>.Callback(callbackparamtemp)', 2000);

 <% Response.Write(carousel_callback.ClientID) %>.Callback('callbackinit');

</script>		
	</body>
</HTML>
