<%@ Control Language="vb" AutoEventWireup="false" Codebehind="MyAssets.ascx.vb" Inherits="IDAM5.MyAssets" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>







<script language=javascript>
  
  
  
  
  
  
  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridAssets.ClientID) %>.Page(1); 
  }

  
  
  
   function editGrid(rowId)
  {

    <% Response.Write(GridAssets.ClientID) %>.Edit(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  

  

  function onInsert(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
    function ShowContextMenu(item, column, evt) 
  {
    <% Response.Write(GridAssets.ClientID) %>.Select(item); 
   
    return false; 
  }
  
  function onUpdate(item)
  {
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridAssets.ClientID) %>.Page(1); 
  }

  function onDelete(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
  
  function editRow()
  {
    <% Response.Write(GridAssets.ClientID) %>.EditComplete();     
  }

  function insertRow()
  {
    <% Response.Write(GridAssets.ClientID) %>.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    <% Response.Write(GridAssets.ClientID) %>.Delete(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(rowId)); 
  }

  
  
    
    function ThumbnailCallBackPage(page)
  {
    <% Response.Write(ThumbnailCallBack.ClientID) %>.Callback(page);
  }
  
  

</script>


<div class="previewpaneProjects" id="toolbar">
	<table width="100%">
		<tr>
			<td align="left" width="20" valign="top">
				<img src="images/search.gif"><img src="images/spacer.gif" width="5" height="1"></td>
			<td align="left" valign="top" width="70%"><div style="PADDING-TOP:2px"><asp:Literal id="LiteralCookie" Text="N/A" visible="true" runat="server" /></div>
			</td>
			<td align="right" valign="top" width="30%"><div style="PADDING-TOP:2px"><asp:hyperlink id="Hyperlink2" BackColor="Transparent" BorderColor="Transparent" ForeColor="Gray"
						Font-Names="Verdana" Font-Size="7pt" NavigateUrl="javascript:switchToThumbnail()" runat="server">View Toggle</asp:hyperlink></div>
			</td>
		</tr>
	</table>
</div>
<img src="images/spacer.gif" width="5" height="5"><br>
<style>
.projectcentertest { WIDTH: 100%; POSITION: relative }
</style>
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td width="100%" align="left" valign="top">
			<div id="myprojectleft" class="projectcentertest">
				<div class="projectcentertest" style="PADDING-BOTTOM:5px">
					<ComponentArt:Snap id="SNAPProjectInfo" runat="server" Width="100%" Height="400px" MustBeDocked="true"
						DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy" CurrentDockingContainer="myprojectleft"
						DockingContainers="myprojectleft">
						<Header>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="5"><img src="images/projcat16.jpg"></td>
										<td align=left onmousedown="<% Response.Write(SNAPProjectInfo.ClientID) %>.StartDragging(event);">My Assets<br>
											<img src="images/spacer.gif" width="116" height="1"></td>
										<td width="15" style="cursor: hand" align="right"><img onclick="" src="images/editbutton.gif" border="0"></td>
										<td width="15" style="cursor: hand" align="right"><img onclick="" src="images/closebutton.gif" border="0"></td>
										<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfo.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
									</tr>
								</table>
							</div>
						</Header>
						<CollapsedHeader>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="5"><img src="images/projcat16.jpg"></td>
										<td align=left onmousedown="<% Response.Write(SNAPProjectInfo.ClientID) %>.StartDragging(event);">My Assets<br>
											<img src="images/spacer.gif" width="151" height="1"></td>
										<td width="15" style="cursor: hand" align="right"><img onclick="" src="images/closebutton.gif" border="0"></td>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfo.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
									</tr>
								</table>
							</div>
						</CollapsedHeader>
						<Content>
							<div class="SnapProjectsWrapper">
								<div class="SnapProjects">
									<img src="images/spacer.gif" width="188" height="1"><br>
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
					

 <asp:Literal id="LiteralNoFilters" Text="No filters available" visible=false runat="server" />
  
 

<table cellpadding=0 cellspacing=0><tr><td width="75" nowrap ><b><asp:Literal id="ltrlProjectFilter" Text="" runat="server" /></b></td>
<td width=100%><ComponentArt:Menu id="MenuFilterProjects" 
      ScrollingEnabled="true"
      Orientation="horizontal"
      CssClass="TopGroup"
      DefaultGroupCssClass="Group"
      DefaultItemLookId="DefaultItemLook"
      ScrollUpLookId="ScrollUpItemLook"
      ScrollDownLookId="ScrollDownItemLook"
      DefaultGroupItemSpacing="1"
      Visible="False"
      ExpandDelay="100"
      ImagesBaseUrl="images/"
      EnableViewState="false"
      runat="server">
    <ItemLooks>
      <ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="ItemFilters" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
      <ComponentArt:ItemLook LookID="BoldItemLook" CssClass="Itembold" HoverCssClass="ItemHbold" ExpandedCssClass="ItemExpbold" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
      <ComponentArt:ItemLook LookID="ScrollUpItemLook" ImageUrl="scroll_up.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
      <ComponentArt:ItemLook LookID="ScrollDownItemLook" ImageUrl="scroll_down.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
    </ItemLooks>
    </ComponentArt:Menu></td></tr></table><table cellpadding=0 cellspacing=0><tr><td width="75" nowrap ><b><asp:Literal id="ltrlFileTypeFilter" Text="" runat="server" /></b></td>
<td width=100%><ComponentArt:Menu id="MenuFilterFileType" 
      ScrollingEnabled="true"
      Orientation="horizontal"
      CssClass="TopGroup"
      DefaultGroupCssClass="Group"
      DefaultItemLookId="DefaultItemLook"
      ScrollUpLookId="ScrollUpItemLook"
      ScrollDownLookId="ScrollDownItemLook"
      DefaultGroupItemSpacing="1"
      Visible="False"
      ExpandDelay="100"
      ImagesBaseUrl="images/"
      EnableViewState="false"
      runat="server">
    <ItemLooks>
      <ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="ItemFilters" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
      <ComponentArt:ItemLook LookID="BoldItemLook" CssClass="Itembold" HoverCssClass="ItemHbold" ExpandedCssClass="ItemExpbold" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
      <ComponentArt:ItemLook LookID="ScrollUpItemLook" ImageUrl="scroll_up.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
      <ComponentArt:ItemLook LookID="ScrollDownItemLook" ImageUrl="scroll_down.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
    </ItemLooks>
    </ComponentArt:Menu></td></tr></table>    
    
<table cellpadding=0 cellspacing=0><tr><td width="75" nowrap><b><asp:Literal id="ltrlMediaTypeFilter" Text="" runat="server" /></b></td>
<td width=100%><ComponentArt:Menu id="MenuFilterMediaType" 
      ScrollingEnabled="true"
      Orientation="horizontal"
      CssClass="TopGroup"
      DefaultGroupCssClass="Group"
      DefaultItemLookId="DefaultItemLook"
      ScrollUpLookId="ScrollUpItemLook"
      ScrollDownLookId="ScrollDownItemLook"
      DefaultGroupItemSpacing="1"
      Visible="False"
      ExpandDelay="100"
      ImagesBaseUrl="images/"
      EnableViewState="false"
      runat="server">
    <ItemLooks>
      <ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="ItemFilters" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
      <ComponentArt:ItemLook LookID="BoldItemLook" CssClass="Itembold" HoverCssClass="ItemHbold" ExpandedCssClass="ItemExpbold" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
      <ComponentArt:ItemLook LookID="ScrollUpItemLook" ImageUrl="scroll_up.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
      <ComponentArt:ItemLook LookID="ScrollDownItemLook" ImageUrl="scroll_down.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
    </ItemLooks>
    </ComponentArt:Menu></td></tr></table>    






<!--No Assets warning-->
<asp:Literal id="literalNoAssets" Text="No assets available" visible=false runat="server" />

		
<table width="100%">
        <tr>
          <td align=left width="30%"></td>
<td align=right width="70%"><font face="verdana" size=1><b>Keyword Filter</b></font>
</td></tr></table>
<COMPONENTART:GRID 
id="GridAssets" 
runat="server" 
AutoCallBackOnInsert="true"
AutoCallBackOnUpdate="true"
AutoCallBackOnDelete="true"
AutoFocusSearchBox="false"
pagerposition="2"
ScrollBar="Off"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ScrollPopupClientTemplateId="ScrollPopupTemplate" 
ClientSideOnSelect="BrowseOnSingleClickAssets" 
Sort="name asc"
ClientSideOnInsert="onInsert"
ClientSideOnUpdate="onUpdate"
ClientSideOnDelete="onDelete"
ClientSideOnCallbackError="onCallbackError"
ClientSideOnDoubleClick="BrowseOnDoubleClick" 
Height="100" Width="100%" 
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
EnableViewState="true"
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRow" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="true" 
GroupingPageSize="1" 
PagerTextCssClass="GridFooterText" 
PagerStyle="Numbered" 
PageSize="20" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeader" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="true" 
ShowSearchBox="true" 
ShowHeader="true" 
CssClass="Grid" 
RunningMode="Callback" 
ScrollBarWidth="15" 
AllowPaging="True" >
<ClientTemplates>
<ComponentArt:ClientTemplate Id="EditTemplate">
            <a href="javascript:DownloadAsset('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> | <a href="javascript:AddToCarousel('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/3Drafts.gif" alt="Edit" border=0></a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplate">
            <img src="## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplate">
            <A href="IDAM.aspx?page=Asset&ID=## DataItem.GetMember("asset_id").Value ##&type=asset"><img border=0 src="images/goto.gif"></a>
          </ComponentArt:ClientTemplate>  
<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>                                 
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
            EditFieldCssClass="EditDataField"
            EditCommandClientTemplateId="EditCommandTemplate"
            InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" dataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<ComponentArt:GridColumn Align="Center" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplate" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="True" HeadingText="Name"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Project" AllowEditing="false" SortedDataCellCssClass="SortedDataCell" DataField="projectname"  ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Type" AllowEditing="false" SortedDataCellCssClass="SortedDataCell" DataField="media_type" ForeignTable="FileType" ForeignDataKeyField="media_type" ForeignDisplayField="filetypevalue" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Updated" AllowEditing="false" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Security Level" AllowEditing="true" SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="User" AllowEditing="false" SortedDataCellCssClass="SortedDataCell"  DataField="userid" ForeignTable="Users" ForeignDataKeyField="userid" ForeignDisplayField="FullName"></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Width="100" Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>

</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>

<style>
div.asset_name      { width:164px; overflow: hidden;padding-left:5px;
                    text-overflow-mode:ellipsis;font-size:8px }
div.asset_date      { width:120px; overflow: hidden;padding-left:5px;
                    text-overflow-mode:ellipsis;font-size:8px }  
div.asset_filetype  { width:80px; overflow: hidden;
                    text-overflow-mode:ellipsis;font-size:8px }                  
div.asset_filesize  { width:60px; overflow: hidden;
                    text-overflow-mode:ellipsis;font-size:8px }    
div.asset_owner     { width:120px; overflow: hidden;padding-left:5px;
                    text-overflow-mode:ellipsis;font-size:8px }    
div.asset_links    {width:100%;padding-left:2px; height:4px;overflow:hidden;background-image: url(images/dottedline.gif);background-repeat:repeat-x;}                                                                           
span.nowrap       { white-space : nowrap; }
div.attributed-to { position: relative;left:8px }

</style>

<COMPONENTART:CALLBACK id="ThumbnailCallBack" runat="server" CacheContent="true">
<CONTENT>




<!--hidden Thumbs-->

 <asp:Literal id="ltrlPager" Text="" runat="server" />

<asp:Repeater id=Thumbnails runat="server" Visible="False">
<ItemTemplate>
<div valign="Top" style="float:left!important; width:220px!important; width:180px; height:320px!important; height:220px;"> 
<!--Asset Thumbnails-->
<div style='width:170px;height:220px;'>
        <div align="left">
                    <div align="left">
                      <table  class="bluelightlight" border="0" cellspacing="0" width="15">
                        <tr>
                          <td  class="bluelightlight" valign="top">
                          <div align="left" id="assethighlight_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" style="border:1px white solid;">
                          <table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse"><tr><td>
                            <table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse" height="120" width="160">
                              <tr class="bluelightlight" width="100%" align="center" height="120" width="160"><td class="bluelightlight" width="100%" height="120" width="160" align="center" valign="bottom" bordercolor="#808080" bordercolorlight="#808080" nowrap><img src="images\spacer.gif" height="0" width="159"><br><a id="25257" href="cd_asset.asp?p=50155&a=25257&pc=&pcc=&c=&post="><img alt="" border="0" src="
  <%=idam5.BaseIDAMSession.Config.URLLocationPreview%>?id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&instance=<%#DataBinder.Eval(Container.DataItem, "instance")%>&type=asset&size=2&height=120&width=160"></a></td><td><img src="images\spacer.gif" width="0" height="140"></td>

                              </tr>
                            </table></td></tr></table>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td >      
                          
                          
                          
                          
                          
                          
<div style="padding:5px;">             
<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
	<td nowrap width="5"><img src="<%#DataBinder.Eval(Container.DataItem, "imagesource")%>"></td>
	<td nowrap><div class="asset_name"><span class="nowrap"><b><font face="Verdana" size="1"><%#DataBinder.Eval(Container.DataItem, "name")%></font></b></span></div></td>
	<td align="right" width="20"><font color="#808080" face="Arial" size="1"><input onclick="highlightAssetToggle(this,<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);" class="preferences" type="checkbox" name="aselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" value="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"></font></td>
</tr>
</table>
<table width="100%"  cellspacing="0" cellpadding="0" >
<tr>
	<td nowrap width="100%" align="left"><div class="asset_filetype"><span class="nowrap"><font face="Verdana" size="1"><%# GetAssetFileTypeName(CType(DataBinder.Eval(Container.DataItem, "media_type"), String))%></font></span></div></td>
<td align="right"><div class="asset_filesize"><span class="nowrap"><font face="Verdana" size="1"><%# FormatSize(CType(DataBinder.Eval(Container.DataItem, "filesize"), Long))%></font></span></div></td>
</tr>
</table>
<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
	<td nowrap><font face="Verdana" size="1">Updated</font></td>
	<td align="left" width="868"><div class="asset_date"><span class="nowrap"><font face="Verdana" size="1">

<%# String.Format("{0:MM/dd/yy hh:mm tt}", (DataBinder.Eval(Container.DataItem, "update_date")))%></font></span></div></td>

</tr>
<tr>
	<td nowrap><font face="Verdana" size="1">Created By</font></td>
	<td align="left" width="868"><div class="asset_owner"><span class="nowrap"><font face="Verdana" size="1"><%# GetAssetOwner(CType(DataBinder.Eval(Container.DataItem, "asset_id"), String))%></font></span></div></td>
</tr>
<tr>
	<td nowrap><font face="Verdana" size="1">Project</font></td>
	<td align="left" width="868"><div class="asset_owner"><span class="nowrap"><font face="Verdana" size="1"><%# GetAssetProjectName(CType(DataBinder.Eval(Container.DataItem, "projectid"), String))%></font></span></div></td>
</tr>
</table>
<div style="padding-top:3px;">
<div class="asset_links"></div></div>
<b><font face="Verdana" size="1"><a href="#">View Details</a></font><br>
<font face="Verdana" size="1"><a href="#">Preview</a></font><br>
<font face="Verdana" size="1"><a href="#">Download</a></font><br>
<font face="Verdana" size="1"><a href="javascript:AddToCarousel('0 <%#DataBinder.Eval(Container.DataItem, "asset_id")%>');">Add to Carousel</a></font><br></b>           
</div>                        
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
							</td>
                        </tr>
                      </table>
                    </div>
                    
                   <img border="0" src="images/spacer.gif" width="135" height="8">
                   </div>
                   </div><!--end Asset Thumbnail span-->

                   </div><!--end inner span-->
</ItemTemplate>
<HeaderTemplate>
<!--Start Thumbnail Float-->
<table width=100%><tr><td>
<div valign="Bottom" style="float:left!important;">       
</HeaderTemplate>
<FooterTemplate>
</td></tr></table>
</FooterTemplate> 
</asp:Repeater>
<asp:Literal id="ltrlPagingBottom" Text="" runat="server" />
</div><!--End padding 10px Main window frame-->
<!--hidden Thumbs-->



											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<TABLE height="300" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px">Loading...
																	</TD>
																	<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>

									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
								</div> <!--SnapProjects-->
							</div> <!--SnapProjectsWrapper-->
						</Content>
					</ComponentArt:Snap>
					
					
					
					
					
					
					
					
					


      <script type=text/javascript>
  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

  
  function BrowseOnDoubleClick(SelectedGridRow)
  {
  
  	var itemvaluetmp;
	itemvaluetmp = SelectedGridRow.GetMember('asset_id').Value;
	if (itemvaluetmp.substring(0,1) == 'P') 
	{
		itemvaluetmp = itemvaluetmp.replace('P_','');
		window.location.href = 'IDAM.aspx?page=Project&id=' + itemvaluetmp
    
	} else {
		itemvaluetmp = itemvaluetmp.replace('C_','');
		window.location.href = 'IDAM.aspx?page=Browse&id=' + itemvaluetmp + '&type=category';
	}
	
	return true;
  
  
  
	
  }
  
  function highlightAssetToggle(checkbox,asset_id)
  {
  var highlightbox;
  highlightbox = document.getElementById('assethighlight_' + asset_id);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  
  
  function highlightProjectToggle(checkbox,projectid)
  {
  var highlightbox;
  highlightbox = document.getElementById('projecthighlight_' + projectid);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  

  </script>



      
					
					
					
					
					
					
					
					
				</div>
			</div> <!--bottom padding-->
		</td>
	</tr>
</table>


