<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="JobEdit.aspx.vb" Inherits="IDAM5.JobEdit" ValidateRequest="False"%>
<%@ Register TagPrefix="upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Job</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
		<link href="treeStyle.css" type="text/css" rel="stylesheet">
		<link href="navStyle.css" type="text/css" rel="stylesheet">
		<link href="tabStripStyle.css" type="text/css" rel="stylesheet">
		<link href="navBarStyle.css" type="text/css" rel="stylesheet">
		<link href="calendarStyle.css" type="text/css" rel="stylesheet" />
		<style>BODY { MARGIN: 0px }
		</style>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onload="this.focus">
		<form id="Form" method="post" runat="server">
			<script type="text/javascript">
  
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridKeywords.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    GridKeywords.Edit(GridKeywords.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    GridKeywords.EditComplete();     
  }

  function insertRow()
  {
    GridKeywords.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    GridKeywords.Delete(GridKeywords.GetRowFromClientId(rowId)); 
  }

<%if request.querystring("newid") <> "" then%>
//window.opener.NavigateToPage('User');
//window.close();
<%end if%>


function onPickerChange(picker)
      {
        <% Response.Write(Calendar1.ClientID) %>.SetSelectedDate(picker.GetSelectedDate());
      }
      function onPickerMouseUp()
      {
        if (<% Response.Write(Calendar1.ClientID) %>.PopUpObjectShowing)
        {
          event.cancelBubble=true;
          event.returnValue=false;
          return false;
        }
        else
        {
          return true;
        }
      }
      function OkClick()
      {
        <% Response.Write(Picker1.ClientID) %>.SetSelectedDate(<% Response.Write(Calendar1.ClientID) %>.GetSelectedDate());
        <% Response.Write(Calendar1.ClientID) %>.Hide();
      }
      function CancelClick()
      {
        <% Response.Write(Calendar1.ClientID) %>.Hide();
      }



function onPicker2Change(picker)
      {
        <% Response.Write(Calendar2.ClientID) %>.SetSelectedDate(picker2.GetSelectedDate());
      }
      function onPicker2MouseUp()
      {
        if (<% Response.Write(Calendar2.ClientID) %>.PopUpObjectShowing)
        {
          event.cancelBubble=true;
          event.returnValue=false;
          return false;
        }
        else
        {
          return true;
        }
      }
      function OkClick2()
      {
        <% Response.Write(Picker2.ClientID) %>.SetSelectedDate(<% Response.Write(Calendar2.ClientID) %>.GetSelectedDate());
        <% Response.Write(Calendar2.ClientID) %>.Hide();
      }
      function CancelClick2()
      {
        <% Response.Write(Calendar2.ClientID) %>.Hide();
      }






			</script>

			<input type="hidden" name="newsid" id="newsid" value="<%response.write (request.querystring("ID"))%>">
			
			
			
			<div style="padding:20px;position:relative;">
			
			<div style="">
			<div style="padding:10px;border:1px solid;background-color:white;position:relative;">
			<table><tr><td><img src="images/image_thumb.gif"></td><td>Create/Edit Job Item</td></tr></table>
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Create a job item by entering the information below.</div><br>
  <div style="">

							<COMPONENTART:TABSTRIP id="TabStripCategory" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataEvents.xml"
								CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" FillWidth="True" Width="100%" DefaultItemLookId="DefaultTabLook"
								DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
								<ItemLooks>
									<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
									<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
								</ItemLooks>
							</COMPONENTART:TABSTRIP>
							<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageEventPage" runat="server">
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="User_General">
									<DIV style="BORDER-RIGHT: #b7b4b4 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #b7b4b4 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: normal; FONT-SIZE: 10px; PADDING-BOTTOM: 5px; BORDER-LEFT: #b7b4b4 1px solid; COLOR: #3f3f3f; PADDING-TOP: 5px; BORDER-BOTTOM: #b7b4b4 1px solid; FONT-FAMILY: verdana; BACKGROUND-COLOR: #e4e4e4">Enter 
										information for this job item.</DIV>
									<BR>
				
									
								
<table border="0" width="100%" id="table1" style="FONT-SIZE: 8pt; FONT-FAMILY: verdana;height=25px;"	>

		<tr>
			<td width="100" align="left" valign="top">Title
</td>
			<td align="left" valign="top">
<asp:TextBox  id="Textbox_title" Width="100%" CssClass="InputFieldMain100P" Height=25px runat="server"></asp:TextBox>
			</td>
		</tr>
		
		
		
		
		
		
		<tr>
		<td class="PageContent" width="100" align="left" valign="top" nowrap>
			<font face="Verdana" size="1">Department</font></td>
			<td class="PageContent"  align="left" valign="top" valign="top">
				<asp:DropDownList style="border:1px dotted;padding:5px;"  DataTextField="type_value" DataValueField="type_id"  ID="DropDown_News_Type" width="180" Runat="server"></asp:DropDownList>
			</td>
		</tr>
		
		
				<tr>
			<td width="100" align="left" valign="top">Description</td>
			<td align="left" valign="top">
<asp:TextBox  style="height:150px;" TextMode="MultiLine" id="Textbox_Description" Width="100%" CssClass="InputFieldMain100P" runat="server"></asp:TextBox>
			</td>
		</tr>
		
		
		
		
	
		<tr>
			<td width="100" align="left" valign="top">Experience</td>
			<td align="left" valign="top">
<asp:TextBox style="height:150px;" TextMode="MultiLine" id="Textbox_Experience" Width="100%" CssClass="InputFieldMain100P" runat="server"></asp:TextBox>
			</td>
		</tr>
		
		
		
		
		
		
			
		<tr>
			<td width="100" align="left" valign="top">Contact Info</td>
			<td align="left" valign="top">
<asp:TextBox  style="height:150px;"  TextMode="MultiLine" id="Textbox_Contact_Info" Width="100%" CssClass="InputFieldMain100P" runat="server"></asp:TextBox>
			</td>
		</tr>
		
		
		
		
		
		
		<tr>
			<td width="100" align="left" valign="top">Post Date</td>
			<td align="left" valign="top">

    <table onmouseup="onPickerMouseUp()" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td><ComponentArt:Calendar id="Picker1" 
          ControlType="Picker" 
          ClientSideOnSelectionChanged="onPickerChange" 
          PickerFormat="Custom" 
          PickerCustomFormat="MMMM d yyyy" 
          PickerCssClass="pickerForm" 
          runat="server" 
          /></td>
      <td style="font-size:10px;">&nbsp;</td>
      <td><img id="calendar_button" alt="" onclick="<% Response.Write(Calendar1.ClientID) %>.SetSelectedDate(<% Response.Write(Picker1.ClientID) %>.GetSelectedDate());<% Response.Write(Calendar1.ClientID) %>.Show();" class="calendar_button" src="images/btn_calendar.gif" width="25" height="22" /></td>
    </tr>
    </table>

    <ComponentArt:Calendar runat="server"
      id="Calendar1"
      AllowMonthSelection="false"
      AllowMultipleSelection="false"
      AllowWeekSelection="false"
      CalendarCssClass="calendar"
      CollapseOnSelect="false"
      ControlType="Calendar"
      DayCssClass="day"
      DayHeaderCssClass="dayheader"
      DayHoverCssClass="dayhover"
      DayNameFormat="FirstTwoLetters"
      OtherMonthDayCssClass="othermonthday"
      OtherMonthDayHoverCssClass="othermonthdayhover"
      PopUp="Custom"
      PopUpExpandControlId="calendar_button"
      SelectedDayCssClass="selectedday" 
      SelectedDayHoverCssClass="selecteddayhover"
      ShowCalendarTitle="false"
      SwapDuration="300"
      SwapSlide="Linear"      
      Width="200"
      >
      <HeaderClientTemplate>
        <div class="header">
          <span style="float:left;">
            <span class="headerbutton" onclick="Calendar1.GoPrevYear()"><img src="images/i_closed.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoPrevMonth()"><img src="images/cal_prevMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoToday()"><img src="images/bullet.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoNextMonth()"><img src="images/cal_nextMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoNextYear()"><img src="images/i_open.gif"></span>
          </span>
          <span style="float:right;">##Calendar1.FormatDate(Calendar1.VisibleDate,"MMMM yyyy")##</span>
        </div>
      </HeaderClientTemplate>
      <FooterClientTemplate>
        <div class="footer">
          <span style="float:right;">
            <input type="button" value="&nbsp; Ok &nbsp;" onclick="OkClick()" class="button" />
            <input type="button" value="Cancel" onclick="CancelClick()" class="button" />
          </span>
        </div>
      </FooterClientTemplate>
    </ComponentArt:Calendar>


			</td>
		</tr>
		
		
		
		
		
		
		
		
		<tr>
			<td width="100" align="left" valign="top">Pull Date</td>
			<td align="left" valign="top">

    <table onmouseup="onPicker2MouseUp()" cellspacing="0" cellpadding="0" border="0" style="font-size:10px;Height:25px">
    <tr style="font-size:10px;Height:25px" Height=25px>
      <td style="font-size:10px;Height:25px" Height=25px ><ComponentArt:Calendar id="Picker2" 
          ControlType="Picker" 
          ClientSideOnSelectionChanged="onPicker2Change" 
          PickerFormat="Custom" 
          PickerCustomFormat="MMMM d yyyy" 
          PickerCssClass="pickerForm" 
          runat="server" Height="25px"/></td>
      <td style="font-size:10px;Height:25px">&nbsp;</td>
      <td><img id="calendar_button" alt="" onclick="<% Response.Write(Calendar2.ClientID) %>.SetSelectedDate(<% Response.Write(Picker2.ClientID) %>.GetSelectedDate());<% Response.Write(Calendar2.ClientID) %>.Show();" class="calendar_button" src="images/btn_calendar.gif" width="25" height="22" /></td>
    </tr>
    </table>

    <ComponentArt:Calendar runat="server"
      id="Calendar2"
      AllowMonthSelection="false"
      AllowMultipleSelection="false"
      AllowWeekSelection="false"
      CalendarCssClass="calendar"
      CollapseOnSelect="false"
      ControlType="Calendar"
      DayCssClass="day"
      DayHeaderCssClass="dayheader"
      DayHoverCssClass="dayhover"
      DayNameFormat="FirstTwoLetters"
      OtherMonthDayCssClass="othermonthday"
      OtherMonthDayHoverCssClass="othermonthdayhover"
      PopUp="Custom"
      PopUpExpandControlId="calendar_button"
      SelectedDayCssClass="selectedday" 
      SelectedDayHoverCssClass="selecteddayhover"
      ShowCalendarTitle="false"
      SwapDuration="300"
      SwapSlide="Linear"      
      Width="200"
      >
      <HeaderClientTemplate>
        <div class="header">
          <span style="float:left;">
            <span class="headerbutton" onclick="Calendar2.GoPrevYear()"><img src="images/i_closed.gif"></span>
            <span class="headerbutton" onclick="Calendar2.GoPrevMonth()"><img src="images/cal_prevMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar2.GoToday()"><img src="images/bullet.gif"></span>
            <span class="headerbutton" onclick="Calendar2.GoNextMonth()"><img src="images/cal_nextMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar2.GoNextYear()"><img src="images/i_open.gif"></span>
          </span>
          <span style="float:right;">##Calendar2.FormatDate(Calendar2.VisibleDate,"MMMM yyyy")##</span>
        </div>
      </HeaderClientTemplate>
      <FooterClientTemplate>
        <div class="footer">
          <span style="float:right;">
            <input type="button" value="&nbsp; Ok &nbsp;" onclick="OkClick2()" class="button" />
            <input type="button" value="Cancel" onclick="CancelClick2()" class="button" />
          </span>
        </div>
      </FooterClientTemplate>
    </ComponentArt:Calendar>




			</td>
		</tr>
		
		
		
		
		
		
		
		
		
		

				
		
		
		
		
		
		
	</table>
	
	
	
	
			</ComponentArt:PageView>
		
	</ComponentArt:MultiPage>
	<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
					<div style="text-align:right;"><br>
			<asp:Button id="btnProjectSave" runat="server" Text="Save"></asp:Button><img src="images/spacer.gif" width=5 height=1><input type="button" onclick="window.opener.RefreshJobItems();window.close();" value="Close" />
			</div>
	</div>
				</div>
			</div>
			</div>
			</div>
			</div>
			<script language="javascript">
TabStripCategory.SelectTabById('<%response.write (request.querystring("tab"))%>');


  function onDeleteAsset(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }



	</script>
		</form>
	</body>
</HTML>
