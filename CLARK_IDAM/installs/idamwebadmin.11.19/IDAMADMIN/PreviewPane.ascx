<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="PreviewPane.ascx.vb" Inherits="IDAM5.PreviewPane" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div class="previewpane" id="previewpane" style="HEIGHT: 44px">Preview Pane
</div>
<div style="PADDING-RIGHT: 6px; 
  PADDING-LEFT: 6px; 
  FONT-SIZE: 11px; 
  PADDING-BOTTOM: 6px; 
  CURSOR: default; 
  PADDING-TOP: 6px; 
  FONT-FAMILY: Tahoma; 
  BACKGROUND-COLOR: white">
	<COMPONENTART:CALLBACK id="ProjectImage" runat="server" CacheContent="true">
		<CONTENT>
			<asp:Image id="CurrentImage" runat="server" ImageUrl="~/common/images/spacer.gif"></asp:Image>
		</CONTENT>
		<LOADINGPANELCLIENTTEMPLATE>
			<TABLE height="222" cellSpacing="0" cellPadding="0" width="330" border="0">
				<TR>
					<TD align="center">
						<TABLE cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD style="FONT-SIZE: 10px">Loading...
								</TD>
								<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</LOADINGPANELCLIENTTEMPLATE>
	</COMPONENTART:CALLBACK>
	<asp:Image id="Image1" runat="server" ImageUrl="~/common/images/spacer.gif"></asp:Image>
</div>
