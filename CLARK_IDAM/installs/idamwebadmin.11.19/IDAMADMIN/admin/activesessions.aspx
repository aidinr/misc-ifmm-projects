<%@ Page Language="vb" AutoEventWireup="false" Codebehind="activesessions.aspx.vb" Inherits="IDAM5.sessions"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>sessions</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:datagrid id="SessionGrid" runat="server" autogeneratecolumns="false" forecolor="#000000"
				backcolor="#ffffff" cellpadding="3" gridlines="none" width="100%" font-name="tahoma,arial,sans-serif">
				<AlternatingItemStyle Font-Names="tahoma,arial,sans-serif" BackColor="Lavender"></AlternatingItemStyle>
				<ItemStyle Font-Names="tahoma,arial,sans-serif"></ItemStyle>
				<HeaderStyle Font-Size="12px" Font-Names="tahoma,arial,sans-serif" Font-Bold="True" Wrap="False"
					ForeColor="White" BackColor="Gray"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="SessionID" HeaderText="Session ID" DataFormatString="&lt;font size='4'&gt;{0}&lt;/font&gt;"></asp:BoundColumn>
					<asp:BoundColumn DataField="UserName" HeaderText="UserName" DataFormatString="&lt;font size='3'&gt;{0:f2}&lt;/font&gt;"></asp:BoundColumn>
					<asp:BoundColumn DataField="InstanceID" HeaderText="InstanceID" DataFormatString="&lt;font size='3'&gt;{0:f2}&lt;/font&gt;"></asp:BoundColumn>
					<asp:BoundColumn DataField="UserHostAddress" HeaderText="UserHostAddress" DataFormatString="&lt;font size='3'&gt;{0:f2}&lt;/font&gt;"></asp:BoundColumn>
					<asp:BoundColumn DataField="ActiveTime" HeaderText="ActiveTime" DataFormatString="&lt;font size='3'&gt;{0:f2}&lt;/font&gt;"></asp:BoundColumn>
					<asp:BoundColumn DataField="UserAgent" HeaderText="UserAgent"  DataFormatString="&lt;font size='2'&gt;{0:f2}&lt;/font&gt;"></asp:BoundColumn>		
		          <asp:HyperLinkColumn DataNavigateUrlField="ExpireURL" Text="Expire"  HeaderText=""></asp:HyperLinkColumn> 
				</Columns>
			</asp:datagrid>
		</form>
	</body>
</HTML>
