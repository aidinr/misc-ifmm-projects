<%@ Page Language="vb" AutoEventWireup="false" Codebehind="version_info.aspx.vb" Inherits="IDAM5.version_info"%>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>version_info</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
		<LINK href="treeStyle.css" type="text/css" rel="stylesheet">
		<LINK href="navStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStripStyle.css" type="text/css" rel="stylesheet">
		<LINK href="navBarStyle.css" type="text/css" rel="stylesheet">
		<style>BODY { MARGIN: 0px }
		</style>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div style="PADDING-RIGHT: 20px; PADDING-LEFT: 20px; PADDING-BOTTOM: 20px; PADDING-TOP: 20px; POSITION: relative">
				<div style="WIDTH: 100%">
					<div style="BORDER-RIGHT: 1px solid; PADDING-RIGHT: 10px; BORDER-TOP: 1px solid; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; BORDER-LEFT: 1px solid; PADDING-TOP: 10px; BORDER-BOTTOM: 1px solid; POSITION: relative; BACKGROUND-COLOR: white">
						<table>
							<tr>
								<td>
								</td>
								<td>IDAM</td>
							</tr>
						</table>
						<br>
						IDAM WebAdmin Version:
						<asp:label id="versionLabel" runat="server">5</asp:label>
						<p></p>
						<asp:Button ID="checkforUpdatesBtn" Text="Check For Update" Runat="server"></asp:Button>
		</form>
		<div style="WIDTH: 100%">
		</div>
		</DIV></DIV></DIV>
	</body>
</HTML>
