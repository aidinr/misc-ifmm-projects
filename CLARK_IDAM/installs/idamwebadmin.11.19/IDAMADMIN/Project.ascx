<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="Project.ascx.vb" Inherits="IDAM5.Project" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div class="previewpaneHeadingProjects" id="toolbar">
	<!--Project cookie and top menu-->
		<table cellspacing="0" cellpadding="4" id="table2" width="100%">
			<tr><td valign="top" width="67">
			<img style="background-color:#CBCBCB;color:#CBCBCB;border:4px solid #CBCBCB;" src="<%=idam5.BaseIDAMSession.Config.URLLocationPreview%>?id=<%response.write(spProjectID)%>&instance=<%response.write(IDAMInstance)%>&type=project&size=2&height=90&width=120">
			</td>
			<td valign="top">
			<font face="Verdana" size="1"><b><%response.write(spProjectName)%>&nbsp;</b><%if trim(spProjectClientName) <> "" then 
			response.write("for") 
			end if%><b>&nbsp;<%if trim(spProjectClientName) <> "" then 
			response.write(spProjectClientName) 
			end if%></b><br>
				<%if trim(spProjectDescription) <> "" then%><span style="font-weight: 400"><%response.write(spProjectDescription)%><br></span><%end if%>
				<b>Security Level:</b> <span style="font-weight: 400"><%response.write(spProjectSecurityLevel)%></span><br>
				<asp:Literal id="LiteralCookie" Text="N/A" visible="true" runat="server" />
				</font>
			</td>
			<td id="Test" valign="top" align="right">
			<div style="padding-top:2px;">
			<ComponentArt:Menu id="MenuControl" CssClass="TopGroup" DefaultGroupCssClass="MenuGroup" SiteMapXmlFile="menuDataMenuControlProjects.xml"
						DefaultItemLookID="DefaultItemLook" TopGroupItemSpacing="1" DefaultGroupItemSpacing="2" ImagesBaseUrl="images/"
						ExpandDelay="100" runat="server">
						<ItemLooks>
							<ComponentArt:ItemLook LookID="EmptyLook" />
							<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover"
								LeftIconUrl="check.gif" LeftIconWidth="15" LeftIconHeight="10" LabelPaddingLeft="8" LabelPaddingRight="12"
								LabelPaddingTop="3" LabelPaddingBottom="4" />
						</ItemLooks>
					</ComponentArt:Menu><!--[ <a href="#">help</a> ]-->
				</div>
			</td></tr>
			<tr><td valign="top" colspan="3">
			</td>
			</tr>
		</table>
</div> <!--end preview pane-->


<div class="previewpaneSub">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle" width="100%">
			
			<div style="align:right;padding:0px;">
								<asp:HyperLink CssClass="projecttabs" id="link_ProjectGeneral" NavigateUrl="javascript:gotopagelink('ProjectGeneral');"
									runat="server">General</asp:HyperLink>&nbsp;<%if IDAM5.Functions.getRole("PROJECT_GENERAL_VIEW",session("Roles")) then%>|&nbsp;<!--&nbsp;&nbsp;<asp:HyperLink CssClass="projecttabs" id="link_ProjectPostings" NavigateUrl="javascript:gotopagelink('ProjectPostings');"
									runat="server">postings</asp:HyperLink>--><asp:HyperLink CssClass="projecttabs" id="link_ProjectDetails" NavigateUrl="javascript:gotopagelink('ProjectDetails');"
									runat="server">Project Information</asp:HyperLink>&nbsp;<%end if%><%if IDAM5.Functions.getRole("PROJECT_PERMISSIONS_VIEW",session("Roles")) then%>|&nbsp;<asp:HyperLink id="link_ProjectPermissions" CssClass="projecttabs" NavigateUrl="javascript:gotopagelink('ProjectPermissions');"
									runat="server">Permissions</asp:HyperLink><%end if%>
							</div>
			</td>
			<td align="right"  width="100%" >
				<!--<asp:Image onclick="javascript:switchToThumbnail();" id="list_icon"   runat="server"></asp:Image><asp:Image onclick="javascript:switchToThumbnail();"  id="thumb_icon" runat="server"></asp:Image>-->
				<img src=images/spacer.gif height=16 width=1>
			</td>
		</tr>
	</table>
</div>


<div class="previewpaneProjects" id="toolbar">
	
	<!--END Project cookie and top menu-->
	<!--projectcentermain-->
	<div id="projectcentermain"  class="projectcentermain" >
		<ComponentArt:Snap id="SNAPProjectInfoMain" runat="server" FillWidth="True" FillHeight="True" Height="100%"
			MustBeDocked="true" DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy" CurrentDockingContainer="projectcentermain"
			DockingContainers="projectcentermain" 
			MinimizeDirectionElement="Test" MinimizeDuration="300" MinimizeSlide="Linear">
			<Header>
				<div style="CURSOR: move; width: 100%;">
					<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
							<td align=left onmousedown="<% Response.Write(SNAPProjectInfoMain.ClientID) %>.StartDragging(event);"><b>Project 
								Summary</b>
								<%'response.write(spProjectName)%>
								</td>
							<td width="15" style="cursor: hand" align="right"></td>
							<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfoMain.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(0);" src="images/closebutton.gif" border="0"></td>
							<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfoMain.ClientID)%>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
						</tr>
					</table>
				</div>
			</Header>
			<CollapsedHeader>
				<div style="CURSOR: move; width: 100%;">
					<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
							<td align=left onmousedown="<% Response.Write(SNAPProjectInfoMain.ClientID) %>.StartDragging(event);"><b>Project 
								Summary</b>
								<%'response.write(spProjectName)%>
								</td>
							<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfoMain.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(0);" src="images/closebutton.gif" border="0"></td>
							<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfoMain.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
															border="0"></td>
						</tr>
					</table>
				</div>
				<img src="images/spacer.gif" width="188" height="0"><br>
			</CollapsedHeader>
			<Content>
				<div class="SnapProjectsWrapper">
					<div class="SnapProjects">
						<table border="0" width="100%" id="">
							<tr>
								<td width="1" align="left" valign="top">
									<img border="0" src="<%=idam5.BaseIDAMSession.Config.URLLocationPreview%>?id=<%response.write(spProjectID)%>&instance=<%response.write(IDAMInstance)%>&type=project&size=1&height=206&width=248">
								</td>
								<td align="left" valign="top">
									<div style="padding-left:10px;width:95%;">
										<asp:Literal ID="LiteralProjectSummary" Runat="server"></asp:Literal></div>
								</td>
							</tr>
							<tr>
								<td colspan="2" align="left" valign="top"></td>
							</tr>
						</table>
						<img src="images/spacer.gif" width="188" height="1">
					</div>
				</div> <!--SnapProjectsWrapper-->
			</Content>
		</ComponentArt:Snap>
	</div>
	<!--END projectcentermain-->
	<div  id=projectcentermainsubpages class="projectcentermainsubpages" style="">
		<asp:PlaceHolder id="PageLoaderProject" runat="server"></asp:PlaceHolder>
	</div>
</div> <!--end preview pane-->
<script language="javascript">

  function gotopage(tab)
  {

	// Get names
	var queryString = window.location.href.substring((window.location.href.indexOf('?') + 1));
	var array1 = queryString.split('&'); // Each array element is in format "fieldname=value"
	var IDValue;
	for(var i = 0; i < array1.length; i++){
		var tempArray = array1[i].split('='); // Separate fieldname and value
		if (tempArray[0]=='ID') {
			IDValue = tempArray[1]
		}
		if (tempArray[0]=='id') {
			IDValue = tempArray[1]
		}
		if (tempArray[0]=='Id') {
			IDValue = tempArray[1]
		}				
		//eval(tempArray[0] + " = \"" + tempArray[1] + "\"");
	}
  
  
  var newurl = 'IDAM.aspx?Page=Project&ID=' + IDValue + '&type=Project&subtype=' + tab.ID;
  //alert(newurl);
  window.location.href = newurl;
  return true;
  }
  
  function gotopagelink(subtype)
  {

	// Get names
	var queryString = window.location.href.substring((window.location.href.indexOf('?') + 1));
	var array1 = queryString.split('&'); // Each array element is in format "fieldname=value"
	var IDValue;
	for(var i = 0; i < array1.length; i++){
		var tempArray = array1[i].split('='); // Separate fieldname and value
		if (tempArray[0]=='ID') {
			IDValue = tempArray[1]
		}
		if (tempArray[0]=='id') {
			IDValue = tempArray[1]
		}
		if (tempArray[0]=='Id') {
			IDValue = tempArray[1]
		}				
		//eval(tempArray[0] + " = \"" + tempArray[1] + "\"");
	}
  
  
  var newurl = 'IDAM.aspx?Page=Project&ID=' + IDValue + '&type=Project&subtype=' + subtype;
  //alert(newurl);
  window.location.href = newurl;
  return true;
  }
  

  
</script>
