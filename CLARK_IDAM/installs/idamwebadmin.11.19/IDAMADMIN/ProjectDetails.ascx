<%@ Control Language="vb" AutoEventWireup="false" Codebehind="ProjectDetails.ascx.vb" Inherits="IDAM5.ProjectDetails" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"  %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<SCRIPT LANGUAGE="JavaScript" SRC="js/CalendarPopup.js"></SCRIPT>
<script language="javascript">


function ToggleSnapMinimize(SnapObject, MenuItemIndex)
{
	//Force reload of page if trying to open a snap.  This is for performance reasons.
	//if (SnapObject.IsMinimized)
	//{
	//force submit here
	//document.getElementById('PageForm').submit();
	//} else
	//{
    SnapObject.ToggleMinimize();
    ToggleItemCheckedState(MenuItemIndex); 
    //}
   
    
}    
 
  
function ToggleItemCheckedState(MenuItemIndex)
{

    var item = _ctl1_MenuControl.Items(0).Items(MenuItemIndex); 
	if (item.GetProperty('Look-LeftIconUrl') == 'check.gif')
	{
		item.SetProperty('Look-LeftIconUrl','clear.gif');
		//set cookie to false
		SetCookie ('WindowProjectAssets' + MenuItemIndex, 0, exp);
		
	}
	else
	{
		item.SetProperty('Look-LeftIconUrl','check.gif');
		//set cookie to true
		SetCookie ('WindowProjectAssets' + MenuItemIndex, 1, exp);
	}
	item.ParentMenu.Render();
	
}
function onPickerChange(picker)
      {
        <% Response.Write(Calendar1.ClientID) %>.SetSelectedDate(picker.GetSelectedDate());
      }
      function onPickerMouseUp()
      {
        if (<% Response.Write(Calendar1.ClientID) %>.PopUpObjectShowing)
        {
          event.cancelBubble=true;
          event.returnValue=false;
          return false;
        }
        else
        {
          return true;
        }
      }
      function OkClick()
      {
        <% Response.Write(Picker1.ClientID) %>.SetSelectedDate(<% Response.Write(Calendar1.ClientID) %>.GetSelectedDate());
        <% Response.Write(Calendar1.ClientID) %>.Hide();
      }
      function CancelClick()
      {
        <% Response.Write(Calendar1.ClientID) %>.Hide();
      }
</script>

<style>
.projectcentertest
{
position:relative;
width:100%;
padding-left:5px;padding-right:5px;
/*overflow: -moz-scrollbars-vertical; */
}
</style>

<table width=100% cellpadding=0 cellspacing=0><tr><td width=100% align=left valign=top >
<div id="projectcenter" class="projectcentertest">
<div class="projectcentertest" style="padding-bottom:5px;">


<ComponentArt:Snap id="SNAPProjectInfo" runat="server" Width="100%" Height="400px" MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter"  DockingContainers="projectcenter" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=100% onmousedown="<% Response.Write(SNAPProjectInfo.ClientID) %>.StartDragging(event);"><b>General Information</b></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectInfo.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=100% onmousedown="<% Response.Write(SNAPProjectInfo.ClientID) %>.StartDragging(event);"><b>General Information</b></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfo.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
								<div class="SnapProjectsWrapper">
								<div class="SnapProjects" >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px">								

<table border="0" width="100%" id="table1">
		<tr>
			<td width="100" align="left" valign="top">Name
</td>
			<td align="left" valign="top">
<asp:TextBox  id="Textbox_Name" Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox>
			</td>
		</tr>
		
		
		<tr>
		<td class="PageContent" width="100" align="left" valign="top" nowrap>
			<font face="Verdana" size="1">Security Level</font></td>
			<td class="PageContent"  align="left" valign="top" valign="top">
				<asp:DropDownList ID="SecurityLevel" width="180" Runat="server">
					<asp:ListItem Value="0">Administrator</asp:ListItem>
					<asp:ListItem Value="1">Internal Use</asp:ListItem>
					<asp:ListItem Value="2">Client Use</asp:ListItem>
					<asp:ListItem Value="3">Public</asp:ListItem>
				</asp:DropDownList>
			</td>
		</tr>
		
		
		<tr>
			<td width="100" align="left" valign="top">Client</td>
			<td align="left" valign="top">
			<asp:DropDownList style="border:1px dotted;padding:5px;" id="DropdownClient" DataTextField="name" DataValueField="name" runat="server"></asp:DropDownList>
			</td>
		</tr>
		<tr>
			<td width="100" align="left" valign="top">Project Number</td>
			<td align="left" valign="top">
<asp:TextBox  id="Textbox_Projectnumber" Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td width="100" align="left" valign="top">Project Address</td>
			<td align="left" valign="top">
<asp:TextBox height="80px" TextMode="MultiLine" id="Textbox_Address" Width="150px" CssClass="InputFieldMain100P" runat="server"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td width="100" align="left" valign="top">City</td>
			<td align="left" valign="top">
<asp:TextBox id="Textbox_City" Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td width="100" align="left" valign="top">State</td>
			<td align="left" valign="top">
			<asp:DropDownList style="border:1px dotted;padding:5px;" id=DropDownList_State DataTextField="name" DataValueField="state_id" runat="server"></asp:DropDownList>
			</td>
		</tr>
		<tr>
			<td width="100" align="left" valign="top">Zip</td>
			<td align="left" valign="top">
<asp:TextBox id="Textbox_Zip" Width="150px" CssClass="InputFieldMain" runat="server"></asp:TextBox>
			</td>
		</tr>
		
		<tr>
			<td width="100" align="left" valign="top">Start Date</td>
			<td align="left" valign="top">

    <table onmouseup="onPickerMouseUp()" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td><ComponentArt:Calendar id="Picker1" 
          ControlType="Picker" 
          ClientSideOnSelectionChanged="onPickerChange" 
          PickerFormat="Custom" 
          PickerCustomFormat="MMMM d yyyy" 
          PickerCssClass="picker" 
          runat="server" /></td>
      <td style="font-size:10px;">&nbsp;</td>
      <td><img id="calendar_button" alt="" onclick="<% Response.Write(Calendar1.ClientID) %>.SetSelectedDate(<% Response.Write(Picker1.ClientID) %>.GetSelectedDate());<% Response.Write(Calendar1.ClientID) %>.Show();" class="calendar_button" src="images/btn_calendar.gif" width="25" height="22" /></td>
    </tr>
    </table>

    <ComponentArt:Calendar runat="server"
      id="Calendar1"
      AllowMonthSelection="false"
      AllowMultipleSelection="false"
      AllowWeekSelection="false"
      CalendarCssClass="calendar"
      CollapseOnSelect="false"
      ControlType="Calendar"
      DayCssClass="day"
      DayHeaderCssClass="dayheader"
      DayHoverCssClass="dayhover"
      DayNameFormat="FirstTwoLetters"
      OtherMonthDayCssClass="othermonthday"
      OtherMonthDayHoverCssClass="othermonthdayhover"
      PopUp="Custom"
      PopUpExpandControlId="calendar_button"

      SelectedDayCssClass="selectedday" 
      SelectedDayHoverCssClass="selecteddayhover"
      ShowCalendarTitle="false"
      SwapDuration="300"
      SwapSlide="Linear"      
      Width="200"
      >
      <HeaderClientTemplate>
        <div class="header">
          <span style="float:left;">
            <span class="headerbutton" onclick="_ctl1__ctl0_SNAPProjectInfo_Calendar1.GoPrevYear()"><img src="images/i_closed.gif"></span>
            <span class="headerbutton" onclick="_ctl1__ctl0_SNAPProjectInfo_Calendar1.GoPrevMonth()"><img src="images/cal_prevMonth.gif"></span>
            <span class="headerbutton" onclick="_ctl1__ctl0_SNAPProjectInfo_Calendar1.GoToday()"><img src="images/bullet.gif"></span>
            <span class="headerbutton" onclick="_ctl1__ctl0_SNAPProjectInfo_Calendar1.GoNextMonth()"><img src="images/cal_nextMonth.gif"></span>
            <span class="headerbutton" onclick="_ctl1__ctl0_SNAPProjectInfo_Calendar1.GoNextYear()"><img src="images/i_open.gif"></span>
          </span>
          <span style="float:right;">##_ctl1__ctl0_SNAPProjectInfo_Calendar1.FormatDate(_ctl1__ctl0_SNAPProjectInfo_Calendar1.VisibleDate,"MMMM yyyy")##</span>
        </div>
      </HeaderClientTemplate>
      <FooterClientTemplate>
        <div class="footer">
          <span style="float:right;">
            <input type="button" value="&nbsp; Ok &nbsp;" onclick="OkClick()" class="button" />
            <input type="button" value="Cancel" onclick="CancelClick()" class="button" />
          </span>
        </div>
      </FooterClientTemplate>
    </ComponentArt:Calendar>


			</td>
		</tr>
		
				<tr>
			<td width="100" align="left" valign="top">Status</td>
			<td align="left" valign="top">
<asp:DropDownList style="border:1px dotted;padding:5px;" id="Dropdownlist_Phase" DataTextField="keyname" DataValueField="keyid" runat="server"></asp:DropDownList>
			</td>
		</tr>		
	</table>
	      
	
	<br><br>
	

	<%If IDAM5.Functions.getRole("PROJECT_GENERAL_EDIT", Session("Roles")) Then%>
	<input type='button' Class="InputButtonMain" onclick="javascript:idamaction.value='editProjectDetails';PageForm.submit();return true;"  width=40 value="Save" id="btnsaveSNAPProjectInfo" name="btnsaveSNAPProjectInfo">
	<%end if%>
</div><!--padding-->

								</div><!--SnapProjects-->
								</div><!--SnapProjectsWrapper-->
								</Content>
							</ComponentArt:Snap>
							
					
							
							
							
							
							
							
							





<ComponentArt:Snap id="SnapProjectInfoAdditional" runat="server" Width="100%" Height="400px" MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter" DockingContainers="projectcenter" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=100% onmousedown="<% Response.Write(SnapProjectInfoAdditional.ClientID) %>.StartDragging(event);" nowrap><b>Additional Project Information</b></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SnapProjectInfoAdditional.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=100% onmousedown="<% Response.Write(SnapProjectInfoAdditional.ClientID) %>.StartDragging(event);" nowrap><b>Additional Project Information</b></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SnapProjectInfoAdditional.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
								<div class="SnapProjectsWrapper">
								<div class="SnapProjects" >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px">
Project Data
<asp:TextBox  id="TextboxProjectData"  height="224px" TextMode="MultiLine" Width=97% CssClass="InputFieldMain" runat="server"></asp:TextBox>														
<br>
Awards
<asp:TextBox id="TextboxAwards"  height="224px" TextMode="MultiLine" Width=97% CssClass="InputFieldMain" runat="server"></asp:TextBox>
<br><br>
<b>Project URL</b><br><br>
URL<br>
<asp:TextBox id="TextboxURL"  height="15px" TextMode=SingleLine  CssClass="InputFieldMain" runat="server"></asp:TextBox>
<br>
URL Caption<br>
<asp:TextBox id="TextboxCaption"  height="15px" TextMode=SingleLine  CssClass="InputFieldMain" runat="server"></asp:TextBox>
<br><br>
<b>View Options</b><br><br>
<asp:CheckBox Runat=server ID=CheckboxPublic ></asp:CheckBox>Publish
<br><br>
<asp:CheckBox Runat=server ID="CheckboxHomepage"></asp:CheckBox>Favorite
<br><br>
<asp:CheckBox Runat=server ID="CheckboxCaseStudy"></asp:CheckBox>Case Study
<br><br>


<% If IDAM5.Functions.getRole("PROJECT_VIEWOPTIONS_EDIT", Session("Roles")) Then%>
<br><br>
<input type='button' Class="InputButtonMain" onclick="javascript:idamaction.value='editProjectDetails';PageForm.submit();return true;"  width=40 value="Save" id="upload" name="upload">
<% end if%>
</div>


								</div><!--SnapProjects-->
								</div><!--SnapProjectsWrapper-->
								</Content>
							</ComponentArt:Snap>













							
							
							
							
							
							
							
							
							
							
							
<ComponentArt:Snap id="SNAPProjectUDF" runat="server" Width="100%" Height="200px"  MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter"  DockingContainers="projectcenter" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=100% onmousedown="<% Response.Write(SNAPProjectUDF.ClientID) %>.StartDragging(event);"><b>Custom Fields</b></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectUDF.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=100% onmousedown="<% Response.Write(SNAPProjectUDF.ClientID) %>.StartDragging(event);"><b>Custom Fields</b></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectUDF.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
								<div class="SnapProjectsWrapper">
								<div class="SnapProjects" >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px">	


							
<asp:Literal ID="Literal_UDFMAIN" Runat=server></asp:Literal>

	      

	<%If IDAM5.Functions.getRole("PROJECT_GENERAL_EDIT", Session("Roles")) Then%>            
	<br><br>
	<input type='button' Class="InputButtonMain" onclick="javascript:idamaction.value='editProjectDetails';PageForm.submit();return true;"  width=40 value="Save" id="upload" name="upload">
	<%end if%>
</div><!--padding-->

								</div><!--SnapProjects-->
								</div><!--SnapProjectsWrapper-->
								</Content>
							</ComponentArt:Snap>
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
		

<ComponentArt:Snap id="SNAPProjectImage" runat="server" Width="100%" Height="200px"  MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter"  DockingContainers="projectcenter" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=100% onmousedown="<% Response.Write(SNAPProjectImage.ClientID) %>.StartDragging(event);"><b>Project Image</b></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectImage.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=100% onmousedown="<% Response.Write(SNAPProjectImage.ClientID) %>.StartDragging(event);"><b>Project Image</b></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectImage.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
								<div class="SnapProjectsWrapper">
								<div class="SnapProjects" >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px"><!--padding-->							
<table border="0" width="100%" id="">
	<tr>
		<td width=200 align="left" valign="top">
		<div style="padding:5px;border:1px dotted #E0DCDC;">
		<img border="0" src="<%=idam5.BaseIDAMSession.Config.URLLocationPreview%>?id=<%response.write(request.querystring("Id"))%>&instance=<%response.write(IDAMInstance)%>&type=project&size=1&cache=1"></div>
		<br>
		
		        Upload Image:<br>
        </font>
 <font face="Arial" size="2">
		<input type="file" Class="InputFieldFileMain" size="14" name="thefile" id="thefile" runat="server" ></font>
		

		
		
		
		
		</td>
		<td align="left" valign="top"></td>
	</tr>
	<tr>
		<td colspan="2" align="left" valign="top"></td>
	</tr>
</table>


	

	<%If IDAM5.Functions.getRole("PROJECT_GENERAL_EDIT", Session("Roles")) Then%>    
<br>
<input type='button' Class="InputButtonMain" onclick="javascript:idamaction.value='editProjectDetails';PageForm.submit();return true;"  width=40 value="Save" id="upload" name="upload">
<%end if%>
</div><!--padding-->
								</div><!--SnapProjects-->
								</div><!--SnapProjectsWrapper-->
								</Content>
							</ComponentArt:Snap>
												
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
				
							
							
							
								
							
<ComponentArt:Snap id="SNAPProjectDescription" runat="server" Width="100%" Height="200px"  MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter"  DockingContainers="projectcenter" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=100% onmousedown="<% Response.Write(SNAPProjectDescription.ClientID) %>.StartDragging(event);"><b>Description</b></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectDescription.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=100% onmousedown="<% Response.Write(SNAPProjectDescription.ClientID) %>.StartDragging(event);"><b>Description</b></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectDescription.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
								<div class="SnapProjectsWrapper">
								<div class="SnapProjects" >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px">
Short Description 
													<asp:TextBox  id="shortdescription"   MaxLength="255" height="80px" TextMode="MultiLine" Width=97% CssClass="InputFieldMain" runat="server"></asp:TextBox>
														
														<br>
														 
														Long Description
													<asp:TextBox id="longdescription"  height="224px" TextMode="MultiLine" Width=97% CssClass="InputFieldMain"	runat="server"></asp:TextBox>
															
															
															

														<%If IDAM5.Functions.getRole("PROJECT_GENERAL_EDIT", Session("Roles")) Then%>   	
														<Br><br><input type='button' Class="InputButtonMain" onclick="javascript:idamaction.value='editProjectDetails';PageForm.submit();return true;"  width=40 value="Save" id="upload" name="upload">
														<%end if%>
													</div></div>	
								
												
								</div><!--padding-->
								
								
								
								
								</Content>
							</ComponentArt:Snap>
							
											
							
							
							
							
							


							
							
							
							
							
							
							
							
							






						
								
							
<ComponentArt:Snap id="SNAPProjectAttributes" runat="server" Width="100%" Height="200px"  MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter" DockingContainers="projectcenter" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=100% onmousedown="<% Response.Write(SNAPProjectAttributes.ClientID) %>.StartDragging(event);"><b>Keywords</b></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectAttributes.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=100% onmousedown="<% Response.Write(SNAPProjectAttributes.ClientID) %>.StartDragging(event);"><b>Keywords</b></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectAttributes.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
								
								
								
								<div class="SnapProjectsWrapper">
								<div class="SnapProjects" >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px">
<div style="width: 100%;float:left;">
<br><br>
<asp:Literal ID="LiteralKeywords1Title" Runat=server></asp:Literal>:<br><img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=Discipline','Keywords_Discipline','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsDiscipline" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="LiteralKeywords1" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div>
<div style="width: 100%;float:left;">
<br><br>
<asp:Literal ID="LiteralKeywords2Title" Runat=server></asp:Literal>:<br><img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=Keyword','Keywords_Keyword','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsKeyword" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="LiteralKeywords2" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div>									
<div style="width: 100%;float:left;">
<br><br>
<asp:Literal ID="LiteralKeywords3Title" Runat=server></asp:Literal>:<br><img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=Office','Keywords_Office','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsOffice" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="LiteralKeywords3" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div>
<div style="width: 100%;float:left;">




<%If IDAM5.Functions.getRole("PROJECT_ATTRIBUTES_EDIT", Session("Roles")) Then%>
<br><br>
<input type='button' Class="InputButtonMain" onclick="javascript:idamaction.value='editProjectDetails';PageForm.submit();return true;"  width=40 value="Save" id="upload" name="upload">
<%end if%>
</div>

													</div>
													</div>	

								</div><!--padding-->
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								</Content>
							</ComponentArt:Snap>
							
											
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
													
								
							
<ComponentArt:Snap id="SNAPProjectCaseStudyInformation" runat="server" Width="100%" Height="200px"  MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter"  DockingContainers="projectcenter" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=100% onmousedown="<% Response.Write(SNAPProjectCaseStudyInformation.ClientID) %>.StartDragging(event);"><b>Case Study Information</b></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectCaseStudyInformation.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=100% onmousedown="<% Response.Write(SNAPProjectCaseStudyInformation.ClientID) %>.StartDragging(event);"><b>Case Study Information</b></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectCaseStudyInformation) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
								<div class="SnapProjectsWrapper">
								<div class="SnapProjects" >
<img src="images/spacer.gif" width=188 height=1><br>
<div style="padding:5px">
Challenge
													<asp:TextBox  id="Textbox_Challenge"  height="124px" TextMode="MultiLine" Width=97% CssClass="InputFieldMain" runat="server"></asp:TextBox>
														
														<br>
														 
														Approach
													<asp:TextBox id="Textbox_Approach"  height="124px" TextMode="MultiLine" Width=97% CssClass="InputFieldMain"	runat="server"></asp:TextBox>
																	
														<Br>
														
														
														Solution
													<asp:TextBox id="Textbox_Solution"  height="124px" TextMode="MultiLine" Width=97% CssClass="InputFieldMain"	runat="server"></asp:TextBox>
																	
														<Br>
														
														

														
														<%If IDAM5.Functions.getRole("PROJECT_CASESTUDY_EDIT", Session("Roles")) Then%>
														<br><input type='button' Class="InputButtonMain" onclick="javascript:idamaction.value='editProjectDetails';PageForm.submit();return true;"  width=40 value="Save" id="upload" name="upload">
														<%end if%>
													</div></div>	
								
												
								</div><!--padding-->
								
								
								</Content>
							</ComponentArt:Snap>
							
											
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
													
								
							
<ComponentArt:Snap id="SNAPProjectRelatedObjects" runat="server" Width="100%" Height="200px"  MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter"  DockingContainers="projectcenter" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td onmousedown="<% Response.Write(SNAPProjectRelatedObjects.ClientID) %>.StartDragging(event);" width=100%><b>Related Objects</b></td><td align=left onmousedown="<% Response.Write(SNAPProjectRelatedObjects.ClientID) %>.StartDragging(event);"></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectRelatedObjects.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td onmousedown="<% Response.Write(SNAPProjectRelatedObjects.ClientID) %>.StartDragging(event);" width=100%><b>Related Objects</b></td><td align=left onmousedown="<% Response.Write(SNAPProjectRelatedObjects.ClientID) %>.StartDragging(event);"></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectRelatedObjects.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
								<div class="SnapProjectsWrapper">
								<div class="SnapProjects" style="">
								<img src="images/spacer.gif" width=188 height=1>
																					
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Change related projects by selecting a project from the list and adding to the active list.</div><br>
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td class="PageContent" width="100%" valign=top>
<!--available list-->
<b>Available Projects</b><br>



<COMPONENTART:GRID 
id="GridProjectsAvailable" runat="server" 
pagerposition="2"

ScrollBar="Off"
Sort="Name asc"
Height="10" Width="250" 
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategory" 
AlternatingRowCssClass="AlternatingRow" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PagerTextCssClass="GridFooterText" 
PagerStyle="Numbered" 
PageSize="10" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeaderCategory" 
SearchOnKeyPress="true" 
SearchTextCssClass="GridHeaderText"  
ShowSearchBox="true" 
ShowHeader="true" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
AllowPaging="true"
AutoFocusSearchBox="false">
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategory">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategory">
            <img src="images/projcat16.jpg" border="0" > 
          </ComponentArt:ClientTemplate>                            
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
            EditFieldCssClass="EditDataField"
            EditCommandClientTemplateId="EditCommandTemplateCategory"
            InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="projectid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataCellClientTemplateId="TypeIconTemplateCategory" DataField="projectid" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Name"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="Name" Width="70"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Description"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="Description" Width="100"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="id" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
					</td>
					<td class="PageContent" width="120" valign= middle>
					
					
		
					<asp:Button id="btnAddRelatedProject" runat="server" 

					Text="     Add >> "></asp:Button><br><br><br><br>
					<asp:Button id="btnRemoveRelatedProject" runat="server" 

					Text="<< remove"></asp:Button>
					</td>
					<td class="PageContent" width="100%" valign=top>
					
					
					
					
<!--active list-->
<b>Related Projects</b><br><br><br>


<COMPONENTART:GRID id="GridProjectsRelated" runat="server" 
autoPostBackonDelete="true"
pagerposition="2"
ScrollBar="Off"
Sort="Name asc"
Height="10" Width="100%" 
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategoryActive" 
AlternatingRowCssClass="AlternatingRow" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PagerTextCssClass="GridFooterText" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeaderCategory" 
SearchOnKeyPress="false" 
SearchTextCssClass="GridHeaderText"  
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="Grid" 
RunningMode="callback" 
AllowPaging="false"
cachecontent="false" 
PagerStyle="Numbered" 
PageSize="500"  >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategoryActive">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateCategoryActive">
          <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TypeIconTemplateActive">
            <img src="images/projcat16.jpg" border="0" > 
          </ComponentArt:ClientTemplate>                                
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" 
 EditFieldCssClass="EditDataField" 
 EditCommandClientTemplateId="EditCommandTemplateCategoryActive" 
 DataKeyField="projectid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="reorder.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center"  AllowEditing="false"  DataCellClientTemplateId="TypeIconTemplateActive" DataField="projectid" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="false" HeadingText="Name" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" FixedWidth="false"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" HeadingText="Description" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="description" width="100" FixedWidth="true"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
					</td>					
				</tr>
			</table>									
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
												</div>
								</div><!--padding-->
								</Content>
							</ComponentArt:Snap>
							
											
							
							
							








































							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
								



							
							
							
							
			
							
							
</div>						
</div><!--bottom padding-->		
</td></tr></table>

























<script language="javascript">

<!--

function Keyword_PopUp(strUrl, popupName, intHeight, intWidth)
{
	if (strUrl == null || strUrl.Length <= 0)
		return;

	var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=no,resizable=yes,toolbar=no";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;

	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures, false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
}

function RefreshKeywordDiscipline()
{
		<% Response.Write(CALLBACKKeywordsDiscipline.ClientID) %>.Callback('Refresh');
    alert('Keywords refreshed.');
}

function RefreshKeywordKeyword()
{
		<% Response.Write(CALLBACKKeywordsKeyword.ClientID) %>.Callback('Refresh');
		alert('Keywords refreshed.');
		return false;
    
}

function RefreshKeywordOffice()
{
		<% Response.Write(CALLBACKKeywordsOffice.ClientID) %>.Callback('Refresh');
    alert('Keywords refreshed.');
}



//-->

  
</script>	





		