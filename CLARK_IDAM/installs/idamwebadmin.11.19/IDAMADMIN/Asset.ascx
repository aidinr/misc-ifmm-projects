<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="Asset.ascx.vb" Inherits="IDAM5.Asset" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"  %>
<div class="previewpaneAll" id="toolbar">
	<!--Project cookie and top menu-->
		<table cellspacing="0" cellpadding="4" id="table2" width="100%">
			<tr><td valign="top" width="67">
			<img style="background-color:#CBCBCB;color:#CBCBCB;border:4px solid #CBCBCB;" src="<%=IDAM5.BaseIDAMSession.Config.URLLocationPreview%>?id=<%response.write(spProjectID)%>&instance=<%response.write(IDAMInstance)%>&type=project&size=2&height=90&width=120">
			</td>
			<td valign="top">
			<font face="Verdana" size="1"><b><%response.write(spProjectName)%>&nbsp;</b><%if trim(spProjectClientName) <> "" then 
			response.write("for") 
			end if%><b>&nbsp;<%if trim(spProjectClientName) <> "" then 
			response.write(spProjectClientName) 
			end if%></b><br>
				<%if trim(spProjectDescription) <> "" then%><span style="font-weight: 400"><%response.write(spProjectDescription)%><br></span><%end if%>
				<b>Security Level:</b> <span style="font-weight: 400"><%response.write(spProjectSecurityLevel)%></span><br>
				<asp:Literal id="LiteralCookie" Text="N/A" visible="true" runat="server" />
				</font>
			</td>
			<td id="Test" valign="top" align="right">
			<ComponentArt:Menu id="MenuControl" CssClass="TopGroup" DefaultGroupCssClass="MenuGroup" SiteMapXmlFile="menuDataMenuControlAssets.xml"
								DefaultItemLookID="DefaultItemLook" TopGroupItemSpacing="1" DefaultGroupItemSpacing="2" ImagesBaseUrl="images/"
								ExpandDelay="100" runat="server">
								<ItemLooks>
							<ComponentArt:ItemLook LookID="EmptyLook" />
							<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover"
								LeftIconUrl="check.gif" LeftIconWidth="15" LeftIconHeight="10" LabelPaddingLeft="8" LabelPaddingRight="12"
								LabelPaddingTop="3" LabelPaddingBottom="4" />
								</ItemLooks>
							</ComponentArt:Menu>
				<!--<div style="padding-top:2px;">
			[ <a href="#">help</a> ]</div>-->
				
			</td></tr>
			<tr><td valign="top" colspan="3">
			</td>
			</tr>
		</table>
</div> <!--end preview pane-->




<div class="previewpaneSub">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle" width="100%">
			
			<div style="align:right;padding:0px;">
								<b><asp:HyperLink CssClass="projecttabs" id="link_ProjectGeneral" NavigateUrl="#"
									runat="server">General</asp:HyperLink></b>
							</div>
			</td>
			<td align="right"  width="100%" >
				<!--<asp:Image onclick="javascript:switchToThumbnail();" id="list_icon"   runat="server"></asp:Image><asp:Image onclick="javascript:switchToThumbnail();"  id="thumb_icon" runat="server"></asp:Image>-->
				<img src=images/spacer.gif height=16 width=1>
			</td>
		</tr>
	</table>
</div>




<div class="previewpaneProjects" id="toolbar">
<img src="images/spacer.gif" width="5" height="10">
<div id="projectcentermain" style="PADDING-RIGHT: 3px;PADDING-LEFT: 3px;PADDING-TOP: 5px">
	<ComponentArt:Snap id="SNAPProjectInfoMain" runat="server" FillWidth="True" FillHeight="True" Height="100%"
		MustBeDocked="true" DockingStyle="TransparentRectangle" DraggingStyle="GhostCopy" CurrentDockingContainer="projectcentermain"
		DockingContainers="projectcentermain" AutoCallBackOnDock="true" CurrentDockingIndex="0" AutoCallBackOnCollapse="true" AutoCallBackOnExpand="true">
		<Header>
			<div style="CURSOR: move; width: 100%;">
				<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
					<tr>
						<td width="100%" nowrap onmousedown="<% Response.Write(SNAPProjectInfoMain.ClientID) %>.StartDragging(event);"><b>Project Summary:</b>&nbsp;<%response.write(spProjectName)%></td>
						<td width="15" style="cursor: hand" align="right"></td>
						<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfoMain.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(0);" src="images/closebutton.gif" border="0"></td>
						<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfoMain.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
					</tr>
				</table>
			</div>
		</Header>
		<CollapsedHeader>
			<div style="CURSOR: move; width: 100%;">
				<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
					<tr>
						<td width="100%" nowrap onmousedown="<% Response.Write(SNAPProjectInfoMain.ClientID) %>.StartDragging(event);"><b>Project Summary:</b>&nbsp;<%response.write(spProjectName)%></td>
						<td width="15" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfoMain.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(0);" src="images/closebutton.gif" border="0"></td>
						<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfoMain.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
					</tr>
				</table>
			</div><img src="images/spacer.gif" width="188" height="4"><br>
		</CollapsedHeader>
		<Content>
			<div class="SnapProjectsWrapper">
				<div class="SnapProjects">
					<table border="0" width="100%" id="">
						<tr>
							<td width="1" align="left" valign="top">
								<img border="0" src="<%=IDAM5.BaseIDAMSession.Config.URLLocationPreview%>?id=<%response.write(spProjectID)%>&instance=<%response.write(IDAMInstance)%>&type=project&size=2&height=90&width=120"></td>
							<td align="left" valign="top">
								<asp:Literal ID="LiteralProjectSummary" Runat="server"></asp:Literal>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="left" valign="top"></td>
						</tr>
					</table>
					<img src="images/spacer.gif" width="188" height="1">
				</div>
			</div> <!--SnapProjectsWrapper-->
		</Content>
	</ComponentArt:Snap>
	
	
	

<ComponentArt:Snap id="SNAPProjectInfo" runat="server" FillWidth="True" FillHeight="True" Height="100%"  MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingIndex="1" CurrentDockingContainer="projectcentermain" DockingContainers="projectcentermain" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" nowrap onmousedown="<% Response.Write(SNAPProjectInfo.ClientID) %>.StartDragging(event);"><b>Asset Summary:</b>&nbsp;<%response.write(spAssetName)%></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectInfo.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width="100%" nowrap onmousedown="<% Response.Write(SNAPProjectInfo.ClientID) %>.StartDragging(event);"><b>Asset Summary:</b>&nbsp;<%response.write(spAssetName)%></td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectInfo.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
								<div class="SnapProjectsWrapper">
								<div class="SnapProjects" style="padding-top:5px;" >


<COMPONENTART:MENU id="MenuDownload" runat="server" Orientation="Horizontal" CssClass="TopGroupExp"
					DefaultGroupCssClass="MenuGroup"  DefaultItemLookID="DefaultItemLook" ClientSideOnItemSelect="actiontype"
					
					DefaultGroupItemSpacing="1" ImagesBaseUrl="images/" EnableViewState="true" ExpandDelay="100" ExpandOnClick="true">
					<ItemLooks>
						<componentart:ItemLook HoverCssClass="TopMenuItemHoverExp" LabelPaddingTop="2px" LabelPaddingRight="10px"
							LabelPaddingBottom="2px" ExpandedCssClass="TopMenuItemHoverExp" LabelPaddingLeft="10px" LookId="TopItemLook"
							CssClass="TopMenuItemExp"></componentart:ItemLook>
						<componentart:ItemLook HoverCssClass="MenuItemHoverExp" LabelPaddingTop="3px" LeftIconHeight="18px" LabelPaddingRight="10px"
							LabelPaddingBottom="4px" ExpandedCssClass="MenuItemHoverExp" LabelPaddingLeft="10px" LeftIconWidth="20px"
							LookId="DefaultItemLook" CssClass="MenuItemExp"></componentart:ItemLook>
						<componentart:ItemLook LookId="BreakItem" CssClass="MenuBreak"></componentart:ItemLook>
					</ItemLooks></COMPONENTART:MENU>








































<asp:Literal ID="LiteralCheckoutStatusTop" Runat=server></asp:Literal>






































<div id="checkindiv" style="display:none;">

<!--Start Rounded Box-->
<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%">
<div class="sidemodule">
    <div class="modulecontainer sidebar">
        <div class="first">
            <span class="first"></span><span class="last"></span>
        </div>
        <div>
            <table border="0" width="100%">
				<tr>
					<td width="4">&nbsp;</td>
					<td><!--End Top Rounded Box-->

<b><font size=1 face=verdana color=#000000>Checkin / Checkout</font></b><br><br>
<asp:Literal ID="LiteralCheckoutHistory" Runat=server></asp:Literal>
<b>Status:</b> <asp:Literal ID="LiteralCheckoutStatus" Runat=server></asp:Literal>
<asp:Literal ID="LiteralCheckoutNote" Runat=server></asp:Literal>
<br><br>
<b>Add a note (Optional):</b>
<asp:TextBox  id="Textbox_CheckoutDescription" CssClass="InputFieldMain100P"  TextMode=MultiLine Height=100px style="height:100px;" runat="server"></asp:TextBox>
<p>
<asp:Button id="btnCheckout" CssClass="InputButtonMain" runat="server" Text="Checkout"></asp:Button>
&nbsp;<input type="button" Class="InputButtonMain" onclick="document.getElementById('checkindiv').style.display='none';" value="Cancel" >
</p>


<!--Close Rounded Box-->
					</td>
					<td width="4">&nbsp;</td>
				</tr>
			</table>
			</div>
        <div class="last">
        	<span class="first"></span><span class="last"></span>
        </div>
    </div>
</div>
<!--End Rounded Box-->
<!--Start Close Rounded Box Table-->
								</td>
								<td><img border="0" src="images/spacer.gif" height="2" width="11"></td>
							</tr>
</table>
<!--End Close Rounded Box Table-->
</div>     
 





















<%
'INDESIGN MODULE VIEW
dim iAssetID as string
dim i as integer
iAssetID = request.querystring("ID")
Dim Conn As New ADODB.Connection
Conn.Open(ConnectionString)
dim bLinkRequireInput,bLinkRecommendations  as boolean
dim sqltemp as string
bLinkRequireInput = FALSE
'get links
Dim rsLinks,rsLinkRecommendations As ADODB.Recordset
 rsLinks = Server.CreateObject("adodb.recordset") 
sqltemp = "SELECT * from ipm_asset_links where status >= 0 and active=0 and parent_asset_id = " & iAssetID
rsLinks.Open (sqltemp, Conn, 1, 4)
if rsLinks.recordcount > 0 then
	do while not rsLinks.eof
		if rsLinks("status").Value = 0 then
			bLinkRequireInput = TRUE
		end if
		rsLinks.movenext
	loop
	rsLinks.movefirst
end if


if rsLinks.recordcount > 0 then
rsLinkRecommendations=server.createobject("adodb.recordset")%>

<div id="linkstag">
<!--Start Rounded Box-->
<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%">
<div class="sidemodule">
    <div class="modulecontainer sidebar">
        <div class="first">
            <span class="first"></span><span class="last"></span>
        </div>
        <div>
            <table border="0" width="100%">
				<tr>
					<td width="4">&nbsp;</td>
					<td><!--End Top Rounded Box-->
					</font>

<font face="Verdana" size="1" color="<%%>">

					
					
				
		
<b>Link Editor</b></font><br>
<font face="arial" size="1"><br>
</font><table border="0" width="100%" cellspacing="0">
	<tr>
		<td class="intrabackhazedarker"></td>
		<td class="intrabackhazedarker"><font face="arial" size="1" color="#808080">Filename</font></td>
		<td class="intrabackhazedarker"></td>
		<td class="intrabackhazedarker"><font face="arial" size="1" color="#808080">Location</font></td>
		<td class="intrabackhazedarker"></td>
		<td class="intrabackhazedarker"><font face="arial" size="1" color="#808080">Status</font><br><img border="0" src="images/spacer.gif" height="0" width="60"></td>
		<td class="intrabackhazedarker"></td>
		<td class="intrabackhazedarker"><font face="arial" size="1" color="#808080">Recommendations</font><br><img border="0" src="images/spacer.gif" height="0" width="93"></td>
		<td class="intrabackhazedarker"></td>
		<td class="intrabackhazedarker"><font face="arial" size="1" color="#808080">Action</font></td>
		<td class="intrabackhazedarker"></td>
	</tr>
	<%
	i = 0
	do while not rsLinks.eof%>
	<tr>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>><font face="arial" size="1" color="#000000"><%=i + 1%>.</font></td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>><font face="arial" size="1" color="#000000"><%=rsLinks("file_name").Value%></font></td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>><input type=hidden name=link_id value=<%=rsLinks("link_id").Value%>><input type=hidden name=filename_<%=rsLinks("link_id")%> value="<%=rsLinks("file_name")%>"></td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>><font face="arial" size="1" color="#000000"><%=left(rsLinks("network_location").Value,100)%></font></td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>>&nbsp;</td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>><font face="arial" size="1" color="#000000">
		<%
		if rsLinks("status").Value <> 0 then
			select case rsLinks("status").Value
				case 1
					response.write ("Added")
				case 2
					response.write ("Added")
				case 3
					response.write ("Added")
				case -1
					response.write ("Removed")					
			end select
		else
			select case rsLinks("found_on_network").Value
			case -1,0 
			response.write ("<font face='arial' size='1' color='#FF0000'>Not Found</font>")
			case else
			response.write ("<font face='arial' size='1' color='#FF0000'>Found</font>")
			end select
		end if%></font></td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>>&nbsp;</td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>>
<font face="Arial" size="1" color="<%%>"><%
bLinkRecommendations = FALSE
if rsLinkRecommendations.state then rsLinkRecommendations.close
rsLinkRecommendations.open ("select asset_id, filename from ipm_asset where available = 'Y' and filename = '" & rsLinks("file_name").Value & "' order by update_date desc", Conn, 1, 4)
if rsLinkRecommendations.recordcount > 0 then
	bLinkRecommendations = TRUE
	%><select size="1" class="maindropdown" onChange="" id="recommendation_<%=rsLinks("link_id").Value%>" name="recommendation_<%=rsLinks("link_id").Value%>"><%
	do while not rsLinkRecommendations.EOF
	   response.write ("<option value='" & rsLinkRecommendations("asset_id").Value & "'>" & rsLinkRecommendations("asset_id").Value & " :: " & rsLinkRecommendations("filename").Value & "</option>")
	   rsLinkRecommendations.movenext
	loop	
	%></select><%
else
	response.write ("None<Br>")	
end if
%>
<%if bLinkRecommendations then
response.write ("<br><a href=""" & "javascript:gotoactiveRecommendedAsset('recommendation_" & trim(rsLinks("link_id").Value) & "');" & """>view</a>")
end if%></font>
</td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>>&nbsp;</td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>>
<font face="Arial" size="1" color="<%%>">
<select size="1" class="maindropdown" onChange='SetLinkAction(this);' id="linkaction_<%=rsLinks("link_id").Value%>" name="linkaction_<%=rsLinks("link_id").Value%>">
<option value='nothing'>Nothing</option>
<option value='remove'>Remove Link</option>
<%if bLinkRecommendations then%><option <%if bLinkRecommendations and rsLinks("status").Value = 0  then response.write ("selected ")%> value='recommendation'>Use Recommendation</option><%end if%>
<!--<%'if rsLinks("status") = 1 then%><option <%'if rsLinks("status") = 1 then response.write "selected "%> value='network'>Use Network Location</option><%'end if%>-->
<option value='upload'>Upload From Network</option>
<option value='existing'>Use Existing Asset</option>
</select></font>
<div id="filetag_<%=rsLinks("link_id").Value%>"><br>
<font face="Arial" size="1" color="<%%>">Upload File<br>
<input type=file  size="23" name="thefile_<%=rsLinks("link_id").Value%>" id="thefile_<%=rsLinks("link_id").Value%>"></font><br><br>
</div>
<div id="assetid_<%=rsLinks("link_id").Value%>"><br>
<font face="Arial" size="1" color="<%%>">Use Asset ID<br>
<input type="text" size="23" name="useassetid_<%=rsLinks("link_id").Value%>"></font><br><br>
</div>
<script>
document.getElementById("filetag_<%=rsLinks("link_id").Value%>").style.display = "none";
document.getElementById("assetid_<%=rsLinks("link_id").Value%>").style.display = "none";
</script>


</td>
		<td valign="top" <%if i mod 2 = 1 then response.write ("class='intrabackhaze'")%>>&nbsp;</td>
	</tr>
	<%rsLinks.movenext
	i = i + 1
	loop%>

</table>
<p><font face="arial" size="1"><br>

<input type="button" onclick="javascript:savelinksintercept('IDAM.aspx?NeatUpload_PostBackID=<%=spPostBackID%>&page=Asset&Id=<%=request.querystring("ID")%>&type=asset&c=<%=request.querystring("c")%>');" value="Save" id="submitlink" name="submitlink">&nbsp;<input type="button"  onclick="javascript:document.getElementById('linkstag').style.display = 'none';" value="Cancel" id="" name="">



<!--Close Rounded Box-->
					</td>
					<td width="4">&nbsp;</td>
				</tr>
			</table>
			</div>
        <div class="last">
        	<span class="first" style="position: absolute; top: 0px; width: 6px; height: 18px; margin-left: -6px"></span><span class="last"></span>
        </div>
    </div>
</div>
<!--End Rounded Box-->
<!--Start Close Rounded Box Table-->
								</td>
								<td><img border="0" src="images/spacer.gif" height="2" width="11"></td>
							</tr>
</table>
<!--End Close Rounded Box Table-->
</div>
<br>

<SCRIPT LANGUAGE=javascript>
<!--

function SetLinkAction(dropdown){

	var myindex  = dropdown.selectedIndex
	var SelValue = dropdown.options[myindex].value
	var dropdownname = dropdown.id;
	var linkid = (dropdownname.substring(11,50));
	var filetagname = 'filetag_' + linkid
	var elm = document.getElementById(filetagname);
	var filetagname = 'assetid_' + linkid
	var elm_existing = document.getElementById(filetagname);	
	if (SelValue == 'upload'){
		elm.style.display = "block";
	} else {
		elm.style.display = "none";
	}
	if (SelValue == 'existing'){
		elm_existing.style.display = "block";
	} else {
		elm_existing.style.display = "none";
	}	
}


function gotoactiveRecommendedAsset(recommendationid)
{
	var elm = document.getElementById(recommendationid);
	var elmvalue = elm.options[elm.selectedIndex].value;
	var linkURL = '<%=IDAM5.BaseIDAMSession.Config.URLLocationPreview%>?id=' + elmvalue + '&instance=<%response.write(IDAMInstance)%>&type=asset&size=1&width=640&height=480'
	winpops=window.open(linkURL,"","width=" + 660 + ",height=" + 500 + ",location=no,")
	//return true;
}
//-->
</script>

<script>
<%if bLinkRequireInput then%>
document.getElementById("linkstag").style.display = "block";
<%else%>
document.getElementById("linkstag").style.display = "none";
<%end if%>
</script>



<%end if%>















































<div id="tagcreateversion" style="padding:10px;display:none;">

<b>Create a version</b> of this asset by selecting a file using the 'Browse' button and choosing the appropriate version action.</font><br>

 <br>Upload Version File
        <br>
        <Upload:InputFile Class="InputFieldMain" id="InputFile1" runat="server"></Upload:InputFile>
		<br>
        Name (Optional)
        <br>
        <input name="versionname" Class="InputFieldMain"  size="36" ><br>
        Description (Optional)
        <br>
        <TEXTAREA Class="InputFieldMain"  name=versiondescription rows=4 style="Width:97%" ></TEXTAREA></font> <br>
		<br>
		Version Action<br>
		<input Class="InputFieldMainCheckbox"  type="radio" value="TRUE" name="txtIsOverwrite"> Overwrite Existing File<br>
		<input Class="InputFieldMainCheckbox"  type="radio" name="txtIsOverwrite" value="FALSE" checked> Create New Version<br><br>
<asp:Button id="btnUpload" Class="InputButtonMain" runat="server" Text="Create Version"></asp:Button>&nbsp;<input type="button" Class="InputButtonMain" onclick="document.getElementById('tagcreateversion').style.display='none';" value="Cancel" id="submit1" name="submit1"></p>
						
</div>	


<asp:PlaceHolder id="PreviewTemplateLoader" runat="server"></asp:PlaceHolder>

<img src="images/spacer.gif" width=188 height=1>

												</div>
								</div><!--SnapProjectsWrapper-->
								</Content>
							</ComponentArt:Snap>
	
		
		<asp:PlaceHolder id="PageLoaderAsset" runat="server"></asp:PlaceHolder>

</div>
<!--end main div-->
</div>
<script language="javascript">

 
  

function savelinksintercept(sNewFormAction)
{
    document.forms[0].action = sNewFormAction;
    document.forms[0].__VIEWSTATE.name = 'NOVIEWSTATE';
    document.forms[0].submit();
}

function showversions(ID)
{
	window.open('AssetVersion.aspx?ID=' + ID,'Versions','width=880,height=800,location=no');
}	

	
</script>
  

</script>

