<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ContactSheet.aspx.vb" Inherits="IDAM5.ContactSheet" EnableViewStateMac="False"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>Contact Sheet</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <style>div.break {page-break-before:always}</style> 
  </head>
  <body MS_POSITIONING="GridLayout" topmargin="0" style="font-size: 9px;" leftmargin="0">

    <form id="Form1" method="post" runat="server">
<font face="Verdana" >
<%

'Check Login - Put this in and inc file
dim xWidth,xpWidth,iProjectID,sqltemp,sLineImage,ThumbnailImageHeight,ThumbnailImageWidth,aAsset_Array as string
sLineImage = "images/line_som.jpg"
aAsset_Array = request.querystring("asset_array")
aAsset_Array = "0" + aAsset_Array

xWidth = 3
xpWidth = 3


Dim Conn As New ADODB.Connection
Conn.Open(ConnectionString)

Dim rsAssets,rsAssetsUDF,rsAssetInfoProject,rsAssetInfo As ADODB.Recordset

rsAssets=server.createobject("adodb.recordset") 
rsAssetsUDF=server.createobject("adodb.recordset")
rsAssetInfoProject=server.createobject("adodb.recordset")



dim sThumbLocationID,sThumbAssetID,sThumbDescription ,sThumbName,sThumbProjectName
dim sThumbRepo as string
dim iInput,iText,irowcount,pagebreak,xHeight,acount,iout,iin,sPage,sType,strSubLocationTmp,strSubLocation,strSubLocation1,strSubLocation2 

rsAssetInfo=server.createobject("adodb.recordset")


'handle remaining images.
	%>
	<div style="padding-top:10px">
	
	
	
	
	
	<table align = center><tr><td><b><font face="Verdana" size="1">
           
         	<%=replace(request.form("Heading"),vbcrlf,"<br>")%><br>

         	</font></b>
             
             
             <img border="0" src="images/line_som.gif"><img border="0" src="images/line_som.gif"><img border="0" src="images/line_som.gif"><img border="0" width=30 height=0 src="images/spacer.gif"><br>
             </td></tr><tr><td align=left >
             <b><font face="Verdana" size="1"><A HREF="javascript:window.print()">print sheet</a></font></b><br></td></tr></table>   

	
	</div><%

sqltemp = "select asset_id,location_id,description,name from ipm_asset where asset_id in (" & aAsset_Array & ")"
rsAssets.Open (sqltemp, Conn, 1, 4)
if rsAssets.recordcount > 0 then
irowcount = 0
xWidth = request.querystring("columns")
select case xWidth 
	case 2
		ThumbnailImageHeight =299
		ThumbnailImageWidth = 299
		pagebreak = 2
	case 3
		ThumbnailImageHeight =200
		ThumbnailImageWidth = 200
		pagebreak = 3
	case 4
		ThumbnailImageHeight =120
		ThumbnailImageWidth = 120
		pagebreak = 4
end select
xHeight = rsAssets.recordcount / xWidth  + 1
aCount = 0
%>
         <%if rsAssets.recordcount >= 1 then%>
         <%for iout = 1 to xHeight%>  
              <table align=center border="0" cellspacing="1" width="5" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                <tr>
                	<%for iin = 1 to xWidth %>
                	<%if not rsAssets.EOF then %>
<%'get asset information
if rsAssetInfo.state then rsAssetInfo.close
sqltemp = "select wpixel,hpixel,description,media_type,asset_id,name,filesize,repository_id,location_id,projectid,update_date from ipm_asset where asset_id = " & rsAssets("asset_id").Value
sPage = "asset_view.asp"
sType = "asset"

rsAssetInfo.Open (sqltemp, Conn, 1, 4)
if rsAssetInfo.eof then
sThumbRepo = 0
sThumbAssetID = rsAssets("asset_id").Value
sThumbLocationID = rsAssets("location_ID").Value
sThumbDescription = rsAssets("description").Value
sThumbName = rsAssets("name").Value
sThumbProjectName = "N/A"
else
sThumbRepo = rsAssetInfo("repository_id").Value
sThumbAssetID = rsAssetInfo("asset_id").Value
sThumbLocationID = rsAssetInfo("location_id").Value
sThumbDescription = rsAssetInfo("description").Value
sThumbName = rsAssetInfo("name").Value
if rsAssetInfoProject.state then rsAssetInfoProject.close
sqltemp = "select ltrim(rtrim(ipm_project.name)) name from ipm_asset, ipm_project where ipm_asset.projectid = ipm_project.projectid and asset_id = " & rsAssetInfo("asset_id").Value
rsAssetInfoProject.Open (sqltemp, Conn, 1, 4)
if rsAssetInfoProject.EOF then
sThumbProjectName = "N/A"
else
sThumbProjectName = rsAssetInfoProject("name").Value
end if
end if
%>                	
                  <td width="50" valign="top">
                    <div align="left">
                      <table class="bluelightlight" border="0" cellspacing="0" width="15">
                        <tr>
                          <td class="bluelightlight" valign="top">
                          <div align="left">
                            <table class="bluelightlight" border="0" cellspacing="0" width="5" cellpadding="0">
                              <tr class="bluelightlight">
				<%
				select case sThumbRepo
					case "0"
						strSubLocationTmp = strSubLocation 
					case "1"
						strSubLocationTmp = strSubLocation1
					case "2"
						strSubLocationTmp = strSubLocation2
				end select%>
	
       <td class="bluelightlight" height="<%=ThumbnailImageHeight%>" width="<%=ThumbnailImageWidth%>" valign="middle" align="center"><img border="0" src="images/spacer.gif" width="<%=ThumbnailImageWidth%>" height="0"><img alt="" border="0" src="
  
  		<%=idam5.BaseIDAMSession.Config.URLLocationPreview%>?id=<%=sThumbAssetID%>&instance=<%response.write(IDAMInstance)%>&type=asset&size=1&width=<%=ThumbnailImageWidth%>&height=<%=ThumbnailImageHeight%>">

                                                                </td>
                                <td class="bluelightlight" width="5" valign="top">


                				</td>
                				<td class="bluelightlight" width="0" valign="top"><img border="0" src="images/spacer.gif" width="0" height="<%=ThumbnailImageHeight%>">
                				</td>
                              </tr>
                            </table>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td >                          
                            <table border="0" width="100%" bordercolor="#C0C0C0" style="border-collapse: collapse">
							<tr>
								<td colspan="3">
								<table border="0" width="100%" cellspacing="1" cellpadding="0">
								<tr>
								<td valign=top width=1>
								
								<%if request.form("ffiletype") <> "" then%><img border="0" src="<%=idam5.BaseIDAMSession.Config.URLLocationPreviewIcon%>?id=<%=rsAssetInfo("media_type").value%>&instance=<%response.write(IDAMInstance)%>&size=0"><%end if%><br><img border="0" src="images/spacer.gif" width="20" height="3"></td>
								<td >
								<font color="#808080" face="arial" size="1"><%if request.form("fprojectname") <> "" then%><img border="0" src="dash.jpg" width="4" height="4"><img border="0" src="images/spacer.gif" width="3" height="3"><%response.write (sThumbProjectName & "<br>")%><%end if%></font>
								<%if request.form("ffilename") <> "" then%><img border="0" src="dash.jpg" width="4" height="4"><img border="0" src="images/spacer.gif" width="3" height="3"><font color="#808080" face="arial" size="1"><%=left(sThumbName,25)%></font><br><%end if%>
								<%if request.form("ffilesize") <> "" then%><%iInput = ""
                             iText = ""
                       	     iInput = trim(rsAssetInfo("Filesize").value)
                        	    if iInput <> "" then
	                        	    itext = idam5.Functions.FormatSize(iInput)
	                       	  end if
	                       	  if iText <> "" then%><img border="0" src="dash.jpg" width="4" height="4"><font color="#808080" face="Arial" size="1"><img border="0" src="images/spacer.gif" width="3" height="3"><%=iText%></font><%end if%><img border="0" src="images/spacer.gif" width="3" height="3"><%end if%><%if request.form("fdate") <> "" then%><img border="0" src="dash.jpg" width="4" height="4"><font color="#808080" face="Arial" size="1"><img border="0" src="images/spacer.gif" width="3" height="3"><%=FormatDateTime(rsAssetInfo("update_date").value,2)%></font><br><%end if%>
	                       	  <%if request.form("fdimensions") <> "" then%><%if cint(rsAssetInfo("wpixel").value) <> 0 and cint(rsAssetInfo("wpixel").value) <> 32  then%><img border="0" src="dash.jpg" width="4" height="4"><font color="#808080" face="Arial" size="1"><img border="0" src="images/spacer.gif" width="3" height="3"><%=rsAssetInfo("wpixel").value%> x <%=rsAssetInfo("hpixel").value%></font><%end if%><br><%end if%>


<%'add any user defined fields
if not Request.Form("UDF") is nothing then
dim i as integer
	                       	  for i=0 to Request.Form("UDF").Split(",").Length-1
	                       	  	'get value for this asset
	                       	  	if rsAssetsUDF.state then rsAssetsUDF.close
	                       	  	rsAssetsUDF.Open ("select a.item_type,b.item_value, a.item_name from ipm_asset_field_desc a, ipm_asset_field_value b where a.item_id = b.item_id and b.asset_id = " & sThumbAssetID & " and b.item_id = " & Request.Form("UDF").Split(",")(i).Trim(), Conn, 1, 4)
	                       	  	if rsAssetsUDF.recordcount > 0 then%><img border="0" src="dash.jpg" width="4" height="4"><font color="#808080" face="Arial" size="1"><img border="0" src="images/spacer.gif" width="3" height="3"><%=rsAssetsUDF("item_name").value%>: <%
if rsAssetsUDF("item_type").Value = 10 then
 if rsAssetsUDF("item_value").Value = 1 then
 	response.write ("Yes")
 else
 	response.write ("No")
 end if                    	  	
else
 response.write (rsAssetsUDF("item_value").Value)
end if%></font><img border="0" src="images/spacer.gif" width="3" height="3"><br><%end if
							  next
end if							  
	                       	  %>

	                       	  
	                       	  
	                       	  </td>
								</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td>
							
	                       	  </td>
								<td></td>
								
							</tr>
							</table>
							</td>
                        </tr>
                      </table>
                    </div>
                   <img border="0" src="images/spacer.gif" width="135" height="8"></td>
                  <%rsAssets.movenext
                  end if
                  next%>
                  </tr>
              </table>
           <%if (iout mod (pagebreak) = 0) and not rsAssets.eof then 
			response.write ("<DIV style=""page-break-after: always;""></DIV>") 
				%>
	<div style="padding:0px">

	</div><%
           end if%>
         	<%next%>
         	<%else%>
              
         	<%end if%>
<!--</td></tr></table>-->
<%
irowcount = irowcount + 1
%>
<%end if%>      

















































</div>










    </form>

  </body>
  


</html>
