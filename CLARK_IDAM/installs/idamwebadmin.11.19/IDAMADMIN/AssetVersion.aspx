<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AssetVersion.aspx.vb" Inherits="IDAM5.AssetVersion"%>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>View Versions</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
			<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
			<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
				<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
					<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
						<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
							<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
								<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
									<link href="treeStyle.css" type="text/css" rel="stylesheet">
										<link href="navStyle.css" type="text/css" rel="stylesheet" >
										    <link href="tabStripStyle.css" type="text/css" rel="stylesheet" />    
												<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
										<style>BODY { MARGIN: 0px }
	</style>
	<body MS_POSITIONING="GridLayout" onload="this.focus">
		<form id="Form" method="post" runat="server">
		
		
		
		<script language=javascript>
function RefreshRoles(id)
{
if (id!='')
{
window.location.href = 'RolesEdit.aspx?id=' + id 
} else
{
window.location.href = 'RolesEdit.aspx?id=1';
}
}



  function onUpdate(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }


  function onInsert(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }



  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridAssets.ClientID) %>.Page(1); 
  }

  function onDeleteAsset(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }	
	
	
function editRow()
  {
    <% Response.Write(GridAssets.ClientID) %>.EditComplete();     
  }


function editGrid(RowId)
  {
    itemRow = <% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(RowId);
	itemvaluetmp = itemRow.GetMember('asset_id').Value;
    window.location = 'IDAM.aspx?page=Asset&ID=' + itemvaluetmp + '&type=asset'
  }

function Object_PopUp(strUrl, popupName, intHeight, intWidth)
{


	if (strUrl == null || strUrl.Length <= 0)
		return;

	var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=no,resizable=yes,toolbar=no";
	//var strFeatures = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;
	//var strFeatures += "'"
	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures , false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
}


  function onInsertCategory(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }

  function onUpdateCategory(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }


  


</script>
		
<script type="text/javascript">
  
  
  
  //setup form switch for delete or multi asset zip download
function DownloadAssets(ids){
	var sAssets;
	var downloadtype;
	sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
	ids = ids.replace('0 ','');
	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Download selected assets?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (arraylist == ''){
					arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
					} else {
					if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
					arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					}
					}
				}
				
				arraylist= arraylist.substring(1, arraylist.length);
				downloadtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				downloadtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	downloadtype = 'single';
	}
	if (downloadtype == 'multi') {
		if (arraylist != ''){
		window.open('<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownloadGetMulti & "?assetids="%>' + arraylist + '<%="&instance=" & IDAMInstance%>','ConvertingImage','width=500,height=75,location=no');
		}
	}
	if (downloadtype == 'single') {
	/*assume single*/
	window.location = '<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownload & "?dtype=assetdownload&assetid="%>' + ids + '<%="&instance=" & IDAMInstance%>&size=0';
	return true;
	}

}

  
  

<%if request.querystring("newid") <> "" then%>
window.opener.NavigateToCategory('<%response.write (request.querystring("newid"))%>');
//window.close();
<%end if%>

<%if instr(request.querystring("id"),"CAT") = 0  then%>
<%if request.querystring("parent_id") = ""  then%>
//window.close();
<%end if%>
<%end if%>

</script>

			<%response.write (request.querystring("newid"))%>
			<input type="hidden" name="catid" id="catid" value="<%response.write (replace(request.querystring("id"),"CAT",""))%>">
			<input type="hidden" name="parent_id" id="parent_id" value="<%response.write (request.querystring("parent_id"))%>">
			<div style="padding:20px;position:relative;">
			<div style="width:100%">
			<div style="padding:10px;border:1px solid;background-color:white;position:relative;">
			<table><tr><td><img src="images/ui/carousel_ico_bg.gif"></td><td>Versions</td></tr></table><br>
			
			
			  <div style="width=100%">
			
			
	<COMPONENTART:TABSTRIP id="TabStripCategory" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataRoles.xml"
		CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
		DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
		<ItemLooks>
			<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10" 
 LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif" 
 HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23" 
 LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
			<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10" 
 LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif" 
 LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
		</ItemLooks>
	</COMPONENTART:TABSTRIP>
	
	<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageCategoryPage" runat="server">
		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Pageview1">
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Master Version.</div>
			<br>

			<table cellspacing="0" cellpadding="3" border="0" width="100%">



				<tr>
					<td class="PageContent" width="100%" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">
						
						
						
						<!--place master here-->
						
						
						<table border="0" width="100%" id="">
							<tr>
								<td width=100 align="left" valign="top">
								<img border="0" src="<%=IDAM5.BaseIDAMSession.Config.URLLocationPreview%>?id=<%response.write(spAsset_ID)%>&instance=<%response.write(IDAMInstance)%>&type=asset&size=1&width=120&height=90&cache=1"></td>
								<td align="left" valign="top">
								<div style="padding:5px;font-size:8pt;">
								<asp:Literal ID="LiteralAssetSummary" Runat=server></asp:Literal>
								</div><!--padding 5px on all-->				
								</td>
							</tr>
							<tr>
								<td colspan="2" align="left" valign="top"></td>
							</tr>
						</table>
						
						</font>
						</td>
					<td colspan="2" align="left" valign="top" width=100%>

				
<script language=javascript>
function changerole()
{
//if (confirm('Are you sure you want to change this role?')) {
__doPostBack('Roles','');
//}
}




</script>


				</td>
					<td class="PageContent" width="80%" align="left" valign="top">
						</td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left"  nowrap>
						<font face="Verdana" size="1"></font></td>
					<td class="PageContent" colspan="3" align="left"  width=100%></td>
				</tr>

				
				
				
				
				<tr>
					<td class="PageContent" width="120" align="left"  nowrap>
						<font face="Verdana" size="1"></font></td>
					<td class="PageContent" colspan="3" align="left" width=100%><br><br>
						</td>
				</tr>

			</table>
			
			
			
			
			
			
			
			
			<table border="0" width="100%" id="table2">
										<tr>
											<td width="88"><b><font face="Verdana" size="1">Versions</font></b></td>
											<td><hr>
											</td>
										</tr>
									</table>
			<div style="padding:10px;width=100%;"><!--sub tabs-->
			
			
			
			
			<COMPONENTART:TABSTRIP id="Tabstrip1" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataVersions.xml"
		CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
		DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultipageRoleItems">
		<ItemLooks>
			<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10" 
 LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif" 
 HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23" 
 LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
			<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10" 
 LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif" 
 LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
		</ItemLooks>
	</COMPONENTART:TABSTRIP>
	
	<ComponentArt:MultiPage id="MultipageRoleItems" CssClass="MultiPageCategoryPage" runat="server">
		<ComponentArt:PageView CssClass="PageContent2" runat="server" ID="Pageviewx1">

<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">View Versions</div><br>













											<!--GroupBy="categoryname ASC"-->
											<COMPONENTART:GRID 
												id="GridAssets" 

												runat="server" 
												AutoFocusSearchBox="false"
												AutoCallBackOnInsert="true"
												AutoCallBackOnUpdate="true"
												AutoCallBackOnDelete="true"
													
												
												pagerposition="2"
												ScrollBar="On"
												ScrollTopBottomImagesEnabled="true"
												ScrollTopBottomImageHeight="2"
												ScrollTopBottomImageWidth="16"
												ScrollImagesFolderUrl="images/scroller/"
												ScrollButtonWidth="16"
												ScrollButtonHeight="17"
												ScrollBarCssClass="ScrollBar"
												ScrollGripCssClass="ScrollGrip"
												ClientSideOnInsert="onInsert"
												ClientSideOnUpdate="onUpdate"
												ClientSideOnDelete="onDeleteAsset"
												ClientSideOnCallbackError="onCallbackError"
												ScrollPopupClientTemplateId="ScrollPopupTemplate" 
												Sort="update_date desc"
												Height="10" Width="100%"
												LoadingPanelPosition="TopCenter" 
												LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
												EnableViewState="true"
												GroupBySortImageHeight="10" 
												GroupBySortImageWidth="10" 
												GroupBySortDescendingImageUrl="group_desc.gif" 
												GroupBySortAscendingImageUrl="group_asc.gif" 
												GroupingNotificationTextCssClass="GridHeaderText" 
												AlternatingRowCssClass="AlternatingRowCategory" 
												IndentCellWidth="22" 
												TreeLineImageHeight="19" 
												TreeLineImageWidth="20" 
												TreeLineImagesFolderUrl="images/lines/" 
												PagerImagesFolderUrl="images/pager/" 
												ImagesBaseUrl="images/" 
												PreExpandOnGroup="True" 
												GroupingPageSize="5" 
												PagerTextCssClass="GridFooterTextCategory" 
												PagerStyle="Numbered" 
												PageSize="5" 
												GroupByTextCssClass="GroupByText" 
												GroupByCssClass="GroupByCell" 
												FooterCssClass="GridFooter" 
												HeaderCssClass="GridHeader" 
												SearchOnKeyPress="true" 
												SearchTextCssClass="GridHeaderText" 
												AllowEditing="False" 
												AllowSorting="False"
												ShowSearchBox="false" 
												ShowHeader="false" 
												ShowFooter="true" 
												CssClass="Grid" 
												RunningMode="callback" 
												ScrollBarWidth="15" 
												AllowPaging="true" >
												<ClientTemplates>
												<ComponentArt:ClientTemplate Id="EditTemplate">
														<a href="javascript:DownloadAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> 
														</ComponentArt:ClientTemplate>
												<ComponentArt:ClientTemplate Id="EditTemplateNoDownload">
														<a href="javascript:AddToCarouselFromPG('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a>
														</ComponentArt:ClientTemplate>														
														<ComponentArt:ClientTemplate Id="EditCommandTemplate">
															<a href="javascript:editRow();">Update</a> 
														</ComponentArt:ClientTemplate>
														<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
															<a href="javascript:insertRow();">Insert</a> 
														</ComponentArt:ClientTemplate>    
														<ComponentArt:ClientTemplate Id="TypeIconTemplate">
															<img src="## DataItem.GetMember("imagesource").Value ##" border="0"  > 
														</ComponentArt:ClientTemplate>        
														<ComponentArt:ClientTemplate Id="LookupProjectTemplate">
														<div style="height:45;width=45;border:0px solid;"><img src="images/spacer.gif" width=1 height=45><A href="## DataItem.GetMember("alink").Value ##"><img border=0 src="## DataItem.GetMember("timage").Value ##" onmouseover="javascript:showtrailpreload('## DataItem.GetMember("asset_id").Value ##','## DataItem.GetMember("name").Value ##','','5','1',270,7);PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.ClientId ##',event);" onmouseout="javascript:hidetrail();" ></a></div>
														</ComponentArt:ClientTemplate> 
														<ComponentArt:ClientTemplate Id="LookupAssetPopupTemplate">
															<A href="#" onclick="javascript:PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.ClientId ##',event);"><img border=0 src="images/15printPreview.gif"></a>
														</ComponentArt:ClientTemplate> 
														<ComponentArt:ClientTemplate Id="LookupProjectTemplatePreview">
															<A href="javascript:AssetPreview('## DataItem.ClientId ##');"><img border=0 src="images/15printPreview.gif"></a>
														</ComponentArt:ClientTemplate>           
 
														<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
														<table cellspacing="0" cellpadding="0" border="0">
														<tr>
															<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
														</tr>
														</table>
														</componentart:ClientTemplate>                                 
												</ClientTemplates>
												

												<Levels>
												<componentart:GridLevel EditCellCssClass="EditDataCell"
															EditFieldCssClass="EditDataField"
															EditCommandClientTemplateId="EditCommandTemplate"
															InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
												<Columns>
												
												<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" Align="Center" AllowSorting="false" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplate" HeadingCellCssClass="FirstHeadingCell" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="45" FixedWidth="True" />
												<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" AllowSorting="false" dataField="imagesource"  DataCellCssClass="DataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="19" FixedWidth="True" />
												<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="False" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
												<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Type" AllowEditing="false" Width="40" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
												<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Updated" AllowEditing="false" Width="110"  FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  DataField="update_date"></componentart:GridColumn>
												<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Security Level" AllowEditing="False" Width="80"  SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
												<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="User" AllowEditing="false" Width="110"  SortedDataCellCssClass="SortedDataCell"  DataField="userid" ForeignTable="Users" ForeignDataKeyField="userid" ForeignDisplayField="FullName"></componentart:GridColumn>
												<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowEditing="false" AllowSorting="false" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Width="150"  Align="Center" />
												<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" Visible="False" HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplateNoDownload" EditControlType="EditCommand" Width="150"  Align="Center" />
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
												
												
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="timage" SortedDataCellCssClass="SortedDataCell" DataField="timage"></componentart:GridColumn>

												</Columns>
												</componentart:GridLevel>
												</Levels>
												<ServerTemplates>
												<ComponentArt:GridServerTemplate Id="PickerTemplate">
            <Template>

 	   <input id="txtImageUrl" type="text" value="<%# Container.DataItem("name") %>"/>


            </Template>
          </ComponentArt:GridServerTemplate>
												

												
												
												</ServerTemplates>
											</COMPONENTART:GRID>

<div style="padding:10px;"><asp:Literal id="literalNoAssets" Text="No assets available" visible=false runat="server" /></div>


											































<table width=100%><tr>
<td width=100% style="FONT-SIZE: 11px; "></td>
<td width=200px align=right></td>
</tr></table>	
		

















		</ComponentArt:PageView>
		
		
		
		
		
		
	</ComponentArt:MultiPage>
			
			
			
			
			
			
			
			
			
			</div><!--sub tabs-->
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		</ComponentArt:PageView>
		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Category_Members">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Change permissions for this object by selecting a group or user from the list and adding to the active list.</div><br>
			Coming soon.
		</ComponentArt:PageView>
	</ComponentArt:MultiPage>
	<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
			<div style="text-align:right;"><br>
				<img src="images/spacer.gif" width=5 height=1><input type="button" onclick="window.close();" value="Close" />
			</div>
	</div>
	
	</div>
	</div>
	</div>		
			
		</form>
	</body>
</HTML>
