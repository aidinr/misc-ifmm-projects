<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ClientEdit.aspx.vb" Inherits="IDAM5.ClientEdit"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Client</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
		<link href="treeStyle.css" type="text/css" rel="stylesheet">
		<link href="navStyle.css" type="text/css" rel="stylesheet">
		<link href="tabStripStyle.css" type="text/css" rel="stylesheet">
		<link href="navBarStyle.css" type="text/css" rel="stylesheet">
		<style>BODY { MARGIN: 0px }
		</style>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onload="this.focus">
		<form id="Form" method="post" runat="server">
			<script type="text/javascript">
  
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridKeywords.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    GridKeywords.Edit(GridKeywords.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    GridKeywords.EditComplete();     
  }

  function insertRow()
  {
    GridKeywords.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    GridKeywords.Delete(GridKeywords.GetRowFromClientId(rowId)); 
  }

<%if request.querystring("newid") <> "" then%>
//window.opener.NavigateToPage('User');
//window.close();
<%end if%>


			</script>
			<%'response.write (request.querystring('newid'))%>
			<input type="hidden" name="catid" id="catid" value="<%'response.write (replace(request.querystring('p'),'CAT',''))%>">
			<div style="PADDING-RIGHT:20px;PADDING-LEFT:20px;PADDING-BOTTOM:20px;PADDING-TOP:20px;POSITION:relative">
					<div style="BORDER-RIGHT:1px solid; PADDING-RIGHT:10px; BORDER-TOP:1px solid; PADDING-LEFT:10px; PADDING-BOTTOM:10px; BORDER-LEFT:1px solid; PADDING-TOP:10px; BORDER-BOTTOM:1px solid; POSITION:relative; BACKGROUND-COLOR:white">
						<table>
							<tr>
								<td><img src="images/user16.jpg"></td>
								<td>Create/Edit client</td>
							</tr>
						</table>
						<DIV style="BORDER-RIGHT: #b7b4b4 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #b7b4b4 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: normal; FONT-SIZE: 10px; PADDING-BOTTOM: 5px; BORDER-LEFT: #b7b4b4 1px solid; COLOR: #3f3f3f; PADDING-TOP: 5px; BORDER-BOTTOM: #b7b4b4 1px solid; FONT-FAMILY: verdana; BACKGROUND-COLOR: #e4e4e4">
							Create a client by entering a client name and description.
						</div>
						<br>
						<div style="WIDTH: 100%">
							<COMPONENTART:TABSTRIP id="TabStripCategory" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataClient.xml"
								CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
								DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
								<ItemLooks>
									<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
									<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
								</ItemLooks>
							</COMPONENTART:TABSTRIP>
							<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageCategoryPage" runat="server">
								<ComponentArt:PageView CssClass="PageContent2" runat="server" ID="User_General">
									<DIV style="BORDER-RIGHT: #b7b4b4 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #b7b4b4 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: normal; FONT-SIZE: 10px; PADDING-BOTTOM: 5px; BORDER-LEFT: #b7b4b4 1px solid; COLOR: #3f3f3f; PADDING-TOP: 5px; BORDER-BOTTOM: #b7b4b4 1px solid; FONT-FAMILY: verdana; BACKGROUND-COLOR: #e4e4e4">Enter 
										a name and description for this client.</DIV>
									<BR>
									<TABLE id="Table1" cellSpacing="0" cellPadding="3" width="100%" border="0">
										<TR>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Client 
													Name
													<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage="[!]" ControlToValidate="name"></asp:RequiredFieldValidator>
												</FONT>
											</TD>
											<TD vAlign="top" align="left" width="215">
												<asp:TextBox id="name" runat="server" CssClass="InputFieldMain" Height=25px></asp:TextBox></TD>
											<TD vAlign="top" align="right" width="400">
											</TD>
											<TD vAlign="top" align="left" width="100%">
											</TD>
										</TR>
										<TR>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Short 
													Name </FONT>
											</TD>
											<TD vAlign="top" align="left" width="100%" colspan="3">
												<asp:TextBox id="short_name" runat="server" CssClass="InputFieldMain" Height=25px></asp:TextBox></TD>
										</TR>
										<TR>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Contact 
													Person </FONT>
											</TD>
											<TD vAlign="top" align="left" width="215">
												<asp:TextBox id="contact_person" runat="server" CssClass="InputFieldMain" Height=25px></asp:TextBox></TD>
											<TD vAlign="top" align="right" width="400">
												<FONT face="Verdana" size="1">Show Public</FONT>
											</TD>
											<TD vAlign="top" align="left" width="100%">
												<asp:CheckBox id="show_public" CssClass="InputFieldMainCheckbox" runat="server"></asp:CheckBox></TD>
										</TR>
										<TR>
											<TD class="PageContent" vAlign="top" noWrap align="left" width="120">
												<FONT face="Verdana" size="1">Comments</FONT></TD>
											<TD class="PageContent" vAlign="top" align="left" width="180%" colspan="3">
												<asp:TextBox id="comments" runat="server" CssClass="InputFieldMain100P" Height=180px TextMode="MultiLine"></asp:TextBox></TD>
										</TR>
									</TABLE>
									<table border="0" width="100%" id="table2">
										<tr>
											<td width="88"><b><font face="Verdana" size="1">Client Info</font></b></td>
											<td><hr>
											</td>
										</tr>
									</table>
									<TABLE id="Table1" cellSpacing="0" cellPadding="3" width="100%" border="0">
										<TR>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Email </FONT>
											</TD>
											<TD vAlign="top" align="left" width="100%" colspan="3">
												<asp:TextBox id="email" runat="server" CssClass="InputFieldMain" Height=25px></asp:TextBox></TD>
										</TR>
										<TR>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Phone </FONT>
											</TD>
											<TD vAlign="top" align="left" width="100%" colspan="3">
												<asp:TextBox id="phone" runat="server" CssClass="InputFieldMain" Height=25px></asp:TextBox></TD>
										</TR>
										<tr>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Address1 </FONT>
											</TD>
											<TD vAlign="top" align="left" width="13%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px ID="Address1"></asp:TextBox>
											</TD>
											<TD vAlign="top" align="right" width="17%">
												<FONT face="Verdana" size="1">Address2 </FONT>
											</TD>
											<TD vAlign="top" align="left" width="61%">
												<asp:TextBox id="Address2" runat="server" CssClass="InputFieldMain" Height=25px></asp:TextBox></TD>
										</tr>
										<tr>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Address3 </FONT>
											</TD>
											<TD vAlign="top" align="left" width="13%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px ID="Address3"></asp:TextBox>
											</TD>
											<TD vAlign="top" align="right" width="17%">
												<FONT face="Verdana" size="1">Address4 </FONT>
											</TD>
											<TD vAlign="top" align="left" width="61%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px ID="Address4"></asp:TextBox>
											</TD>
										</tr>
										<tr>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">City </FONT>
											</TD>
											<TD vAlign="top" align="left" width="13%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px ID="City"></asp:TextBox>
											</TD>
											<TD vAlign="top" noWrap align="right" width="120"><FONT face="Verdana" size="1">State </FONT>
											</TD>
											<TD vAlign="top" align="left" width="13%">
												<asp:DropDownList DataTextField="name" DataValueField="state_id" id="state" CssClass="InputFieldMain" Height=25px runat="server"></asp:DropDownList>
											</TD>
										</tr>
										<tr>
											<TD vAlign="top" noWrap align="left" width="120"><FONT face="Verdana" size="1">Zip </FONT>
											</TD>
											<TD vAlign="top" align="left" width="13%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px ID="zip"></asp:TextBox>
											</TD>
											<TD vAlign="top" align="right" width="17%">
												<FONT face="Verdana" size="1">Country </FONT>
											</TD>
											<TD vAlign="top" align="left" width="61%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px ID="Country"></asp:TextBox>
											</TD>
										</tr>
										<TR>
											<TD vAlign="top" align="left" width="17%">
												<FONT face="Verdana" size="1">Fax </FONT>
											</TD>
											<TD vAlign="top" align="left" width="61%">
												<asp:TextBox runat="server" CssClass="InputFieldMain" Height=25px ID="Fax"></asp:TextBox>
											</TD>
										</TR>
									</TABLE>
								</ComponentArt:PageView>
							</ComponentArt:MultiPage>
							<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
								<div style="TEXT-ALIGN:right"><br>
									<asp:Button id="btnProjectSave" runat="server" Text="Save"></asp:Button><img src="images/spacer.gif" width="5" height="1"><input type="button" onclick="window.opener.NavigateToPage('Client');window.close();" value="Close">
								</div>
							</div>
						</div>
				</div>
			</div>
			<script language="javascript">
TabStripCategory.SelectTabById('<%response.write (request.querystring("tab"))%>');
			</script>
		</form>
	</body>
</HTML>
