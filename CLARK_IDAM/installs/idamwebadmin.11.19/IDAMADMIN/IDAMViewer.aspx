<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="IDAM.aspx.vb" Inherits="IDAM5.IDAMViewer"  ValidateRequest="False"%>
<%@ Register TagPrefix="uc3" TagName="CookieScripts" Src="includes/CookieScripts.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<HTML lang="EN">
	<HEAD>
		<title>IDAM Version 5.01</title>
		<%
ClientPageLoadTime = System.DateTime.Now
%>
			<LINK href="common/ViewerbaseStyle.css" type="text/css" rel="stylesheet">
				<LINK href="ViewergridStyle.css" type="text/css" rel="stylesheet">
					<LINK href="ViewersnapStyle.css" type="text/css" rel="stylesheet">
						<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
							<LINK href="ViewermenuStyle.css" type="text/css" rel="stylesheet">
								<LINK href="ViewermultipageStyle.css" type="text/css" rel="stylesheet">
									<link href="ViewertreeStyle.css" type="text/css" rel="stylesheet">
										<link href="ViewernavStyle.css" type="text/css" rel="stylesheet">
											<link href="ViewertabStripStyle.css" type="text/css" rel="stylesheet">
												<link href="ViewernavBarStyle.css" type="text/css" rel="stylesheet">
													<link href="ViewercalendarStyle.css" type="text/css" rel="stylesheet">
														<style>BODY { MARGIN: 0px }
	</style>
														<uc3:cookiescripts id="CookieScripts" runat="server"></uc3:cookiescripts>
														<script language="javascript" type="text/javascript">
    <!--
      // Forces the treeview to adjust to the new size of its container          
      function resizeTree(DomElementId, NewPaneHeight, NewPaneWidth)
      {
      
       // alert(NewPaneWidth);
         setPaneSize(NewPaneWidth);
         //alert('set to:' + NewPaneWidth);

         
      }




  function onquicksearchkeydown(val,eventObj)
  {
  if (eventObj.keyCode==13){
  document.PageForm.idamaction.value='true';
  window.location = 'IDAM.aspx?page=Results&Type=QuickSearch&keyword=' + document.PageForm.keyword.value;
  return false;
  }
  
  }
        // Forces the grid to adjust to the new size of its container          
      function resizeGrid(DomElementId, NewPaneHeight, NewPaneWidth)
      {
		 //setHPaneSize(NewPaneHeight);
         //resizeGrid2(DomElementId, NewPaneHeight, NewPaneWidth);
         //set cookie for pane size
         //TreeViewExplore.Render();
         //alert('hey');


        // for(var prop in)
		//{
			//alert(prop);
		//}
        //alert('hey2');
      } 
      function explorecarousel()
      {

		}
		

      //-->
      
      
	function makeArray(len) 
	{	for (var i = 0; i < len; i++)
		{	this[i] = null
		}
		this.length = len
	}



// array of day names

	var daynames = new makeArray(7)
	daynames[0] = "Sunday"
	daynames[1] = "Monday"
	daynames[2] = "Tuesday"
	daynames[3] = "Wednesday"
	daynames[4] = "Thursday"
	daynames[5] = "Friday"
	daynames[6] = "Saturday"
	

// array of month names

	var monthnames = new makeArray(12)
	monthnames[0] = "January"
	monthnames[1] = "February"
	monthnames[2] = "March"
	monthnames[3] = "April"
	monthnames[4] = "May"
	monthnames[5] = "June"
	monthnames[6] = "July"
	monthnames[7] = "August"
	monthnames[8] = "September"
	monthnames[9] = "October"
	monthnames[10] = "November"
	monthnames[11] = "December"


// define date variables

	var now = new Date()
	var day = now.getDay()
	var month = now.getMonth()
	var year = now.getFullYear()
	var date = now.getDate()
	var hour=now.getHours()
	var minutes=now.getMinutes()

// write date

	function writeDate()
	
	{	
	document.write("<FONT FACE='GENEVA, ARIAL, HELVETICA' SIZE='1'>" + daynames[day] + ", " + monthnames[month] + " " + date + ","+ year +"  "      +"</FONT>")
	}

														</script>
	</HEAD>
	<body bgcolor="white">
		<form id="PageForm" style="MARGIN-TOP:0px" method="post" runat="server">
			<!-- Header go ----------------------------------------------------------------------->
			<div>
				<table id="table1" cellpadding="0" cellspacing="0" width="100%" bgColor="white" border="0">
					<tr>
						<td width="180"><asp:image id="Image1" runat="server" ImageUrl="images/spacer.gif" Width="210" height="1"></asp:image><br>
							<asp:image id="Image2" runat="server" ImageUrl="images/LOGO_template.gif" DescriptionUrl="logo"></asp:image></td>
						<td width="100%" align="right"><div style="PADDING-RIGHT:10px; PADDING-LEFT:10px; PADDING-BOTTOM:10px; PADDING-TOP:10px">
								<%if  Session("login_name") = "Anonymous" then%>
								Version
								<%=CurrentVersion%>
								<BR>
								You are not logged in. [
								<asp:hyperlink id="Hyperlink7" runat="server" BackColor="Transparent" BorderColor="Transparent"
									ForeColor="#EB6601" NavigateUrl="Login.aspx?Logout=True">login</asp:hyperlink>]
								<%if trim(session("sUserAccess")) = "0" then%>
								[
								<asp:hyperlink id="Hyperlink5" Enabled="True" runat="server" BackColor="Transparent" BorderColor="Transparent"
									ForeColor="#EB6601" NavigateUrl="IDAM.aspx?page=QManager">queue manager</asp:hyperlink>]<%end if%>
								<%else%>
								<FONT size="1">Welcome, <STRONG>
										<%=Session("user_name")%>
									</STRONG>&nbsp;&nbsp;Version
									<%=CurrentVersion%>
									<BR>
									[
									<asp:hyperlink id="HyperLink3" runat="server" BackColor="Transparent" BorderColor="Transparent"
										ForeColor="#EB6601" NavigateUrl="Login.aspx?Logout=True">log out</asp:hyperlink>] 
									<!--[ <asp:hyperlink id="HyperLink2" Enabled=False runat="server" BackColor="Transparent" BorderColor="Transparent"
									ForeColor="#EB6601" NavigateUrl="#">my info</asp:hyperlink> ]-->
									<%if trim(session("sUserAccess")) = "0" then%>
									[
									<asp:hyperlink id="HyperLink1" Enabled="True" runat="server" BackColor="Transparent" BorderColor="Transparent"
										ForeColor="#EB6601" NavigateUrl="IDAM.aspx?page=QManager">queue manager</asp:hyperlink>]<%end if%>
									[
									<asp:hyperlink id="Hyperlink4" Enabled="True" runat="server" BackColor="Transparent" BorderColor="Transparent"
										ForeColor="#EB6601" NavigateUrl="IDAM.aspx?page=preferences">my preferences</asp:hyperlink>]</STRONG></FONT>
								<%end if%>
							</div>
						</td>
					</tr>
					<tr>
						<td colSpan="2">
						</td>
					</tr>
				</table>
			</div>
			<asp:image id="Image3" runat="server" ImageUrl="images/spacer.gif" Width="1" height="10"></asp:image><br>
			<!-- Header no ----------------------------------------------------------------------->
			<COMPONENTART:TABSTRIP id="TabStrip1" runat="server" ImagesBaseUrl="images/" CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook"
				DefaultItemLookId="DefaultTabLook" DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1">
				<ItemLooks>
					<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
						LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif"
						HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
						LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
						LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="SelectedTabLookHome" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
						LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon_home.gif" RightIconUrl="selected_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="SelectedTabLookBrowse" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
						LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon_browse.gif" RightIconUrl="selected_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="SelectedTabLookAS" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
						LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon_as.gif" RightIconUrl="selected_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="SelectedTabLookProject" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
						LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon_project.gif" RightIconUrl="selected_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="SelectedTabLookAsset" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
						LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon_asset.gif" RightIconUrl="selected_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="SelectedTabLookApprovalManager" CssClass="SelectedTab" LabelPaddingLeft="10"
						LabelPaddingRight="10" LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon_approval.gif"
						RightIconUrl="selected_tab_right_icon.gif" LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5"
						RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="SelectedTabLookCarousel" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
						LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon_carousel.gif" RightIconUrl="selected_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="TabLookBrowse" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
						LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_browse.gif"
						RightIconUrl="tab_right_icon.gif" HoverLeftIconUrl="hover_tab_left_icon_browse.gif" HoverRightIconUrl="hover_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="TabLookHome" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
						LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_home.gif"
						RightIconUrl="tab_right_icon.gif" HoverLeftIconUrl="hover_tab_left_icon_home.gif" HoverRightIconUrl="hover_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="TabLookProject" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
						LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_project.gif"
						RightIconUrl="tab_right_icon.gif" HoverLeftIconUrl="hover_tab_left_icon_project.gif" HoverRightIconUrl="hover_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="TabLookAS" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
						LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_as.gif"
						RightIconUrl="tab_right_icon.gif" HoverLeftIconUrl="hover_tab_left_icon_as.gif" HoverRightIconUrl="hover_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="TabLookAsset" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
						LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_asset.gif"
						RightIconUrl="tab_right_icon.gif" HoverLeftIconUrl="hover_tab_left_icon_asset.gif" HoverRightIconUrl="hover_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="TabLookConfiguration" CssClass="DefaultTab" HoverCssClass="DefaultTabHover"
						LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_config.gif"
						RightIconUrl="tab_right_icon.gif" HoverLeftIconUrl="hover_tab_left_icon_config.gif" HoverRightIconUrl="hover_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="TabLookApprovalManager" CssClass="DefaultTab" HoverCssClass="DefaultTabHover"
						LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_approval.gif"
						RightIconUrl="tab_right_icon.gif" HoverLeftIconUrl="hover_tab_left_icon_approval.gif" HoverRightIconUrl="hover_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="TabLookCarousel" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
						LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_carousel.gif"
						RightIconUrl="tab_right_icon.gif" HoverLeftIconUrl="hover_tab_left_icon_carousel.gif" HoverRightIconUrl="hover_tab_right_icon.gif"
						LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5" RightIconHeight="23" />
					<ComponentArt:ItemLook LookId="SelectedTabLookConfiguration" CssClass="SelectedTab" LabelPaddingLeft="10"
						LabelPaddingRight="10" LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon_config.gif"
						RightIconUrl="selected_tab_right_icon.gif" LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="5"
						RightIconHeight="23" />
				</ItemLooks>
			</COMPONENTART:TABSTRIP>
			<style>
							.rightsideplaceholderdiv { PADDING-RIGHT: 10px! important; MARGIN-TOP: -2px; MARGIN-LEFT: 1px! important }
							.rightsideplaceholderdivheading { padding-left:5px;padding-top:15px;BORDER-RIGHT: #959595 0px solid; BORDER-TOP: #959595 1px solid; BORDER-LEFT: #959595 0px solid; BORDER-BOTTOM: #959595 0px solid; HEIGHT: 50px;
								background-image:url(images/viewerheadingback.jpg);
								background-repeat: repeat-x; 	
							
							
							 }
							</style>
			<div class="rightsideplaceholderdiv">
				<div class="rightsideplaceholderdivheading">

<INPUT type=hidden value=search name=f> <INPUT class=query name=txt> 
<INPUT type=image src="images/viewer/btn_search.gif"><A 
href="#" ><IMG src="images/viewer/btn_advanced.gif" border=0></A>

				</div>
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<asp:PlaceHolder id="PageLoader" runat="server"></asp:PlaceHolder></td>
					</tr>
				</table>
			</div>
			<COMPONENTART:CALLBACK id="CallbackDownloadCheck" runat="server" CacheContent="false">
				<CONTENT>
					<asp:Literal id="LiteralDownloadCheck" Text="" visible="true" runat="server" />
				</CONTENT>
				<LOADINGPANELCLIENTTEMPLATE></LOADINGPANELCLIENTTEMPLATE>
			</COMPONENTART:CALLBACK>Page Load (Internal Code Execution):
			<%=ElapsePageLoadTime%>
			ms | Client Page Load (Client Browser Execution):
			<%=System.DateTime.Now.Subtract(ClientPageLoadTime).Milliseconds%>
			ms
		</form>
		<script language="javascript">
function Object_PopUp_Carousel(tab)
{
  var dropdown = document.getElementById('IDAMCarousel_carousel_select');
  var myindex  = dropdown.selectedIndex;
  var SelValue = dropdown.options[myindex].value;
  if (tab == 'view')
  {
  Object_PopUp('CarouselNew.aspx?Carousel_id=' + SelValue + '&tab=view','New_Carousel',700,780);
  } else {
  Object_PopUp('CarouselNew.aspx?Carousel_id=' + SelValue,'New_Carousel',700,780);
  }
}	


function Object_gotoCarousel()
{
  var dropdown = document.getElementById('IDAMCarousel_carousel_select');
  var myindex  = dropdown.selectedIndex;
  var SelValue = dropdown.options[myindex].value;
  if (SelValue != '')
  {
    window.location='IDAM.aspx?page=Carousel&id=' + SelValue;
  }
}	


function Object_PopUp_Upload(projectid,categoryid)
{

  //add something here to redirect to java or form based upload	

  Object_PopUp('includes/java/UploadJava.aspx?projectid=' + projectid + '&categoryid=' + categoryid,'Upload',550,780);
}	


function Object_PopUp_UploadExplore(objectid)
{
  Object_PopUp('includes/java/UploadJava.aspx?objectid=' + objectid,'Upload',550,780);
  //Object_PopUp('Upload.aspx?objectid=' + objectid,'Upload',700,780);
}		

function Object_PopUp_Repository(objectid)
{
  Object_PopUp('RepositoryEdit.aspx?id=' + objectid,'Repository',550,480);
  //Object_PopUp('Upload.aspx?objectid=' + objectid,'Upload',700,780);
}	

function Object_PopUp_User(objectid)
{
  Object_PopUp('UserEdit.aspx?id=' + objectid,'user',850,840);
}	

function Object_PopUp_UserNew()
{
  Object_PopUp('UserEdit.aspx','user',850,840);
}

function Object_PopUp_Contact(objectid)
{
  Object_PopUp('UserEdit.aspx?id=' + objectid + '&type=contact','user',750,840);
}	

function Object_PopUp_ContactNew()
{
  Object_PopUp('UserEdit.aspx?type=contact','user',750,840);
}

function Object_PopUp_Client(objectid)
{
  Object_PopUp('ClientEdit.aspx?id=' + objectid,'user',750,840);
}	

function Object_PopUp_ClientNew()
{
  Object_PopUp('ClientEdit.aspx','user',750,840);
}

function Object_PopUp_Keyword(URL)
{
  Object_PopUp(URL,'Keyword',600,700);
}

function Object_PopUp_UDF(URL)
{
  Object_PopUp(URL,'UDF',500,1124);
}

function Object_PopUp_Roles(URL)
{
  Object_PopUp(URL,'Roles',800,800);
}

function Object_PopUp_Splash(URL)
{
  Object_PopUp(URL,'Splash',295,485);
}

function Object_PopUp_Recover(URL)
{
  Object_PopUp(URL,'Roles',800,1124);
}




function Object_PopUp_Vendor(objectid)
{
  Object_PopUp('VendorEdit.aspx?id=' + objectid,'user',750,840);
}	

function Object_PopUp_VendorNew()
{
  Object_PopUp('VendorEdit.aspx','user',750,840);
}

function Object_PopUp_Group(objectid)
{
  Object_PopUp('GroupEdit.aspx?id=' + objectid,'user',750,840);
}	

function Object_PopUp_GroupNew()
{
  Object_PopUp('GroupEdit.aspx','user',750,840);
}



function Object_PopUp_RepositoryNew()
{
  Object_PopUp('RepositoryEdit.aspx','Repository',550,480);
  //Object_PopUp('Upload.aspx?objectid=' + objectid,'Upload',700,780);
}	
			
			
function BrowseOnSingleClick(item)
  {

	var itemvaluetmp;
	itemvaluetmp = item.GetMember('uniqueid').Value;
	itemvaluenametmp = item.GetMember('name').Value;
	if (itemvaluetmp.substring(0,1) == 'P') 
//alert(itemvaluetmp+',Project'+','+itemvaluenametmp);
	{
		itemvaluetmp = itemvaluetmp.replace('P_','');

	}
	return true;
  }  
  
  
  			
function BrowseOnSingleClickAssets(item)
  {

	var itemvaluetmp;
	var itemvaluenametmp;
	var itemvaluefiletypetmp;
	itemvaluetmp = item.GetMember('asset_id').Value;
	itemvaluenametmp = item.GetMember('name').Value;
	itemvaluefiletypetmp = item.GetMember('timage').Value;

	return true;
  }  
  
  	
function BrowseOnSingleClickAssetsLegacy(item)
  {

	var itemvaluetmp;
	var itemvaluenametmp;
	var itemvaluefiletypetmp;
	itemvaluetmp = item.GetMember('asset_id').Value;
	itemvaluenametmp = item.GetMember('name').Value;
	itemvaluefiletypetmp = item.GetMember('imagesource').Value;

	return true;
  }  
		
		
		
function ChangeDefaultCarousel(dropdown)
  {
  var myindex  = dropdown.selectedIndex
  var SelValue = dropdown.options[myindex].value

  return true;
  
  }			
  
  
  function RemoveItemFromCarousel(id)
  {
  
  
  //return true;
  
  }	
  
    function RemoveCarousel(id)
  {
  

  return true;
  
  }	
	
function AddToCarouseldeletthis(rowId)
  {
  //check to see if multi select
 

  }	

function AddDownloadJob(JobID)
  { 

  }	

function DownloadCarousel2()
  { 
    var myindex  = document.getElementById('IDAMCarousel_carousel_select').selectedIndex
  var SelValue = document.getElementById('IDAMCarousel_carousel_select').options[myindex].value

  }	
  function DownloadCarouselUsingWS()
  { 
    var myindex  = document.getElementById('IDAMCarousel_carousel_select').selectedIndex
  var SelValue = document.getElementById('IDAMCarousel_carousel_select').options[myindex].value
  var answer = false
<%  If IDAM5.Functions.getRole("DOWNLOAD_ASSOCIATED_CAROUSEL", Session("Roles")) Then %>
answer =   confirm ("Do you want to include associated assets?")
<%end if%>
if (answer)
  window.open('<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownloadGetMulti & "?carouselid="%>' + SelValue + '<%="&instance=" & idam5.BaseIDAMSession.Config.IDAMInstance%>&uid=<%=Session("sUserID")%>&associated=true','ZippingFiles','width=500,height=75,location=no');
  else
  window.open('<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownloadGetMulti & "?carouselid="%>' + SelValue + '<%="&instance=" & idam5.BaseIDAMSession.Config.IDAMInstance%>&uid=<%=Session("sUserID")%>','ZippingFiles','width=500,height=75,location=no');
  
  }	
function openCarouselNew()
{
	window.open('CarouselNew.aspx','New_Carousel','width=700,height=700,location=no');
	return true;
}		

function NavigateToCarousel()
{

	ChangeDefaultCarousel(document.getElementById('IDAMCarousel_carousel_select'));
}	

function NavigateToAsset(id)
{
	window.location = 'IDAM.aspx?page=Asset&Id=' + id + '&type=Asset';
}	

//var callbackparamtemp = '2050,Project,10th Stret,NW'


		</script>
	</body>
</HTML>
