<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="Browse.ascx.vb" Inherits="IDAM5.Browse" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<div class="previewpaneAll" id="toolbar">
	<!--Project cookie and top menu-->
		<table cellspacing="0" cellpadding="4" id="table2" width="100%">
			<tr><td valign="top" width="1" nowrap >
			<img src="images/ui/browse_ico_bg.gif" height=42 width=42>
			</td>
			<td valign="top">
			<font face="Verdana" size="1"><b><%response.write(sCategoryName)%></b><br>
				<%if trim(spCategoryDescription) <> "" then%><span style="font-weight: 400"><%response.write(spCategoryDescription)%><br></span><%end if%>
				<b>Security Level:</b> <span style="font-weight: 400"><%response.write(spCategorySecurityLevel)%></span>
				</font>
			</td>
			<td valign="top" align="right">
			<div style="padding-top:2px;">
			
				</div>
			</td></tr>
			<tr><td valign="top" colspan="3">
			</td>
			</tr>
		</table>
</div> <!--end preview pane-->


<div class="previewpaneSub">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle" width="100%">
			
			<div style="align:right;padding:0px;">
								<b><asp:HyperLink CssClass="projecttabs" id="link_ProjectGeneral" NavigateUrl="javascript:gotopagelink('Browse');"
									runat="server">General</asp:HyperLink></b>
							</div>
			</td>
			<td id="Test" align="right"  width="100%" >
				<!--<asp:Image onclick="javascript:switchToThumbnail();" id="list_icon"   runat="server"></asp:Image><asp:Image onclick="javascript:switchToThumbnail();"  id="thumb_icon" runat="server"></asp:Image>-->
			<img src="images/spacer.gif" height=16 width=1>
			</td>
		</tr>
	</table>
</div>


<div class="previewpaneProjects" id="toolbar">

<script language=javascript>
  
        // Forces the grid to adjust to the new size of its container          
      function resizeGrid2(DomElementId, NewPaneHeight, NewPaneWidth)
      {
        <% Response.Write(Grid2.ClientID) %>.Render();
        //alert('here');
      } 
  function editGrid(rowId)
  {

    <% Response.Write(Grid2.ClientID) %>.Edit(<% Response.Write(Grid2.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  

  function onInsert(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
    function ShowContextMenu(item, column, evt) 
  {
    <% Response.Write(Grid2.ClientID) %>.Select(item); 
   
    return false; 
  }
  
  function onUpdate(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(Grid2.ClientID) %>.Page(1); 
  }

  function onDelete(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
  
  function editRow()
  {
    <% Response.Write(Grid2.ClientID) %>.EditComplete();     
  }

  function insertRow()
  {
    <% Response.Write(Grid2.ClientID) %>.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    <% Response.Write(Grid2.ClientID) %>.Delete(<% Response.Write(Grid2.ClientID) %>.GetRowFromClientId(rowId)); 
  }

  
  function editGridCategory(rowId)
  {
    <% Response.Write(GridCategory.ClientID) %>.Edit(<% Response.Write(GridCategory.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  

  function onInsertCategory(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
    function ShowContextMenuCategory(item, column, evt) 
  {
    <% Response.Write(GridCategory.ClientID) %>.Select(item);   
    return false; 
  }
  
  function onUpdateCategory(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }

  function onCallbackErrorCategory(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridCategory.ClientID) %>.Page(1); 
  }

  function onDeleteCategory(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }
  
  function editRowCategory()
  {
    <% Response.Write(GridCategory.ClientID) %>.EditComplete();     
  }

  function insertRowCategory()
  {
    <% Response.Write(GridCategory.ClientID) %>.EditComplete(); 
  }

  function deleteRowCategory(rowId)
  {
    <% Response.Write(GridCategory.ClientID) %>.Delete(<% Response.Write(GridCategory.ClientID) %>.GetRowFromClientId(rowId)); 
  }
  
  
  
    function ThumbnailCallBackPage(page)
  {
    <% Response.Write(ThumbnailCallBack.ClientID) %>.Callback(page);
  }
  

  
  
  function gotopagelink(subtype)
  {

	// Get names
	var queryString = window.location.href.substring((window.location.href.indexOf('?') + 1));
	var array1 = queryString.split('&'); // Each array element is in format "fieldname=value"
	var IDValue;
	for(var i = 0; i < array1.length; i++){
		var tempArray = array1[i].split('='); // Separate fieldname and value
		if (tempArray[0]=='ID') {
			IDValue = tempArray[1]
		}
		if (tempArray[0]=='id') {
			IDValue = tempArray[1]
		}
		if (tempArray[0]=='Id') {
			IDValue = tempArray[1]
		}				
		//eval(tempArray[0] + " = \"" + tempArray[1] + "\"");
	}
  
  
  var newurl = 'IDAM.aspx?Page=Category&ID=' + IDValue 
  //alert(newurl);
  window.location.href = newurl;
  return true;
  }

</script>



<div style="padding:5px;padding-top:15px;border:0px solid;" id="browsecenter">



<img src="images/spacer.gif"  height="15" width="5"><br>
<!--<div style="padding:10px;border-left:1px #969698 solid; border-right:1px #969698 solid;border-bottom:1px #969698 solid;border-top:1px #969698 solid;">-->





							<!--SNAPBrowseCategoryEdit-->	
							<ComponentArt:Snap id="SNAPBrowseCategoryEdit" MinWidth="200" runat="server" FillWidth="True" CurrentDockingIndex="0"  Height="200px"  MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="browsecenter"  DockingContainers="browsecenter" MinimizeDirectionElement="testmenucontrollocation" MinimizeDuration="300">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=5><img src="images/snap_icon.gif"></td><td align=left onmousedown="<% Response.Write(SNAPBrowseCategoryEdit.ClientID) %>.StartDragging(event);">Category Information (Quick Edit)</td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPBrowseCategoryEdit.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=5><img src="images/snap_icon.gif"></td><td align=left onmousedown="<% Response.Write(SNAPBrowseCategoryEdit.ClientID) %>.StartDragging(event);">Category Information (Quick Edit)</td>
												<td width="15" style="cursor: hand" align="right" ></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPBrowseCategoryEdit.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									<div style="padding-bottom:5px;border:1px;">
										<div class="SnapProjects" style="height:100%;padding:0px;overflow:hidden;padding-left:5px;padding-right:5px;background-color:white;">
											


<div style="padding-top:10px;"><div style="padding:3px;"></div></div>





<COMPONENTART:GRID 
id="GridCategory" 
runat="server" 
AutoFocusSearchBox="false"
AutoCallBackOnInsert="true"
AutoCallBackOnUpdate="true"
AutoCallBackOnDelete="true"
pagerposition="2"
ScrollBar="Off"
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ScrollPopupClientTemplateId="ScrollPopupTemplate" 
Sort="name asc"
ClientSideOnInsert="onInsertCategory"
ClientSideOnUpdate="onUpdateCategory"
ClientSideOnDelete="onDeleteCategory"
ClientSideOnCallbackError="onCallbackErrorCategory"
Height="10" Width="100%" 
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategory" 
EnableViewState="true"
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRowCategory" 
IndentCellWidth="22" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/lines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="true" 
GroupingPageSize="1" 
PagerTextCssClass="GridFooterTextCategory" 
PagerStyle="Numbered" 
PageSize="5" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 

FooterCssClass="GridFooter" 
HeaderCssClass="GridHeaderCategory" 
SearchOnKeyPress="false" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="true" 
AllowSorting="False"
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="false" 
CssClass="Grid" 
RunningMode="callback" 
ScrollBarWidth="15" 
AllowPaging="false" >
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategory">
<div><table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table></div>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplateCategory">
            <a href="javascript:editGridCategory('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border="0" ></a> <!--| <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>-->
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateCategory">
            <a href="javascript:editRowCategory();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateCategory">
            <a href="javascript:insertRowCategory();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategory">
            <img border=0 src="images/folder_open.gif">
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplateCategory">
            <A href="IDAM.aspx?page=Project&id=## DataItem.GetMember("category_id").Value ##&type=project"><img border=0 src="images/goto.gif"></a>
          </ComponentArt:ClientTemplate>                         
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
            EditFieldCssClass="EditDataField"
            EditCommandClientTemplateId="EditCommandTemplateCategory"
            InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="category_id" SelectedRowCssClass="SelectedRowCategory" DataCellCssClass="DataCellCategory" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellCategory" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverCategory" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowCategory" SortImageHeight="19" RowCssClass="RowCategory" HeadingCellActiveCssClass="HeadingCellActiveCategory">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowSorting="False" AllowEditing="false"   DataCellClientTemplateId="TypeIconTemplateCategory" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell"  HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="True" AllowSorting="False" HeadingText="Name"  AllowGrouping="False"  DataField="name" Width="180"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" AllowEditing="true" AllowGrouping="False"  Width="120" DataField="description"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Updated" AllowEditing="false" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" Width="120" DataField="available_date"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Security Level" AllowEditing="true" SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" width="120"></componentart:GridColumn>
<componentart:GridColumn HeadingText="User" AllowEditing="false" SortedDataCellCssClass="SortedDataCell" Width="150" DataField="userid" ForeignTable="Users" ForeignDataKeyField="userid" ForeignDisplayField="FullName"></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplateCategory" EditControlType="EditCommand" Width="100" Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="category_id" SortedDataCellCssClass="SortedDataCell" DataField="category_id"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>




<div style="padding-top:10px;"><div style="padding:3px;"></div></div>







										</div><!--SnapProjects-->
									</div><!--padding-->												
								</Content>
							</ComponentArt:Snap><!--END SNAPBrowseCategoryEdit-->	

















									<div class="SnapProjectsWrapper">
										<div class="SnapProjectsv">




<br><br>
<table  cellpadding=0 cellspacing =0 width=100%><tr><td width=300 nowrap >[ <a href="javascript:Object_PopUp('CategoryEdit.aspx?parent_id=' + <%response.write(request.querystring("id"))%> + '&action=new','Edit_Category',600,700);">create new category</a> ] 
[ <a href="javascript:Object_PopUp('ProjectNew.aspx?parent_id=' + <%response.write(request.querystring("id"))%>,'New_Project',600,650);">create new project</a> ]</td><td width="60%" align=right nowrap><b>Filter List:</b><img src="images/spacer.gif" width=5 height=1><input Class="InputFieldMain" type=text value="" id=search_filter  onkeydown="javascript:if(event.keyCode==13) {<%response.write( Grid2.ClientId)%>.Filter('name LIKE \'%' + this.value + '%\''); return false}"></td></tr></table>
<br>
<COMPONENTART:GRID 
id="Grid2" 

												runat="server" 
												AutoFocusSearchBox="false"
												AutoCallBackOnInsert="true"
												AutoCallBackOnUpdate="true"
												AutoCallBackOnDelete="true"
												pagerposition="2"
												ScrollBar="Off"
												ScrollTopBottomImagesEnabled="true"
												ScrollTopBottomImageHeight="2"
												ScrollTopBottomImageWidth="16"
												ScrollImagesFolderUrl="images/scroller/"
												ScrollButtonWidth="16"
												ScrollButtonHeight="17"
												ScrollBarCssClass="ScrollBar"
												ScrollGripCssClass="ScrollGrip"
												ClientSideOnInsert="onInsert"
												ClientSideOnUpdate="onUpdate"
												ClientSideOnDelete="onDelete"
												ClientSideOnCallbackError="onCallbackError"
												ScrollPopupClientTemplateId="ScrollPopupTemplate" 
												Sort="imagesource asc"
												Height="10" Width="99%"
												LoadingPanelPosition="TopCenter" 
												LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
												EnableViewState="true"
												GroupBySortImageHeight="10" 
												GroupBySortImageWidth="10" 
												GroupBySortDescendingImageUrl="group_desc.gif" 
												GroupBySortAscendingImageUrl="group_asc.gif" 
												GroupingNotificationTextCssClass="GridHeaderText" 
												AlternatingRowCssClass="AlternatingRowCategory" 
												IndentCellWidth="22" 
												TreeLineImageHeight="19" 
												TreeLineImageWidth="20" 
												TreeLineImagesFolderUrl="images/lines/" 
												PagerImagesFolderUrl="images/pager/" 
												ImagesBaseUrl="images/" 
												PreExpandOnGroup="True" 
												GroupingPageSize="5" 
												PagerTextCssClass="GridFooterTextCategory" 
												PagerStyle="Numbered" 
												PageSize="10" 
												GroupByTextCssClass="GroupByText" 
												GroupByCssClass="GroupByCell" 
												FooterCssClass="GridFooter" 
												HeaderCssClass="GridHeader" 
												SearchOnKeyPress="true" 
												SearchTextCssClass="GridHeaderText" 
												AllowEditing="true" 
												AllowSorting="False"
												ShowSearchBox="false" 
												ShowHeader="false" 
												ShowFooter="true" 
												CssClass="Grid" 
												RunningMode="callback" 
												ScrollBarWidth="15" 
												AllowPaging="true" 
>
<ClientTemplates>
<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=65 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>
<ComponentArt:ClientTemplate Id="EditTemplate">
            <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border="0" ></a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplate">
            <img src="images/## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplate">
            <A href="IDAM.aspx?page=## DataItem.GetMember("page").Value ##&id=## DataItem.GetMember("projectid").Value ##&type=## DataItem.GetMember("typeofobject").Value ##"><img border=0 src="images/goto.gif"></a>
          </ComponentArt:ClientTemplate>                         
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell"
EditFieldCssClass="EditDataField"
EditCommandClientTemplateId="EditCommandTemplate"
InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="projectid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="asc.gif" HeadingCellCssClass="HeadingCell" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHover" SortImageWidth="10" SortDescendingImageUrl="desc.gif" HeadingRowCssClass="HeadingRow" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" dataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<ComponentArt:GridColumn Align="Center" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplate" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn AllowEditing="True" HeadingText="Name"  AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" AllowEditing="true" AllowGrouping="False"   SortedDataCellCssClass="SortedDataCell" DataField="description"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Type" AllowEditing="false" AllowGrouping="False"  SortedDataCellCssClass="SortedDataCell" DataField="typeofobject"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Updated" AllowEditing="false" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Security Level" AllowEditing="true" SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Project Date" AllowEditing="false" FormatString="MMM yyyy" SortedDataCellCssClass="SortedDataCell"  DataField="projectdate"></componentart:GridColumn>
<componentart:GridColumn HeadingText="User" AllowEditing="false" SortedDataCellCssClass="SortedDataCell"  DataField="userid" ForeignTable="Users" ForeignDataKeyField="userid" ForeignDisplayField="FullName"></componentart:GridColumn>

<ComponentArt:GridColumn HeadingText="Action" DataCellCssClass="LastDataCellPostings" AllowEditing="false" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand"  Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="category_id" SortedDataCellCssClass="SortedDataCell" DataField="category_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="uniqueid" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="page" SortedDataCellCssClass="SortedDataCell" DataField="page"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
<!--<componentart:GridColumn AllowEditing="false" HeadingText="Assets" SortedDataCellCssClass="SortedDataCell" DataField="uniqueid" ForeignTable="Assets" ForeignDataKeyField="uniqueid" ForeignDisplayField="acount"></componentart:GridColumn>-->
















<COMPONENTART:CALLBACK id="ThumbnailCallBack" runat="server" CacheContent="true">
<CONTENT>











<!--hidden Thumbs-->

 <asp:Literal id="ltrlPager" Text="" runat="server" />

<asp:Repeater id=Thumbnails runat="server" Visible="False">
<ItemTemplate>



<div valign="Top" style="float:left!important; width:220px!important; width:180px; height:220px!important; height:220px;">
<!--Project Thumbnails-->
<div style='width:170px;height:220px;'>
        <div align="left">
                    <div align="left">
                      <table  class="bluelightlight" border="0" cellspacing="0" width="15">
                        <tr>
                          <td  class="bluelightlight" valign="top">
                          <div align="left" id="projecthighlight_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" style="border:1px white solid;">
                          <table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse"><tr><td>
                            <table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse" height="120" width="160">
                              <tr class="bluelightlight" width="100%" align="center" height="120" width="160"><td class="bluelightlight" width="100%" height="120" width="160" align="center" valign="bottom" bordercolor="#808080" bordercolorlight="#808080" nowrap><img src="images\spacer.gif" height="0" width="159"><br><a id="25257" href="IDAM.aspx?page=Project&Id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=project"><img alt="" border="0" src="
  <%=idam5.BaseIDAMSession.Config.URLLocationPreview%>?id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&instance=<%#DataBinder.Eval(Container.DataItem, "instance")%>&type=project&size=2&height=120&width=160"></a></td><td><img src="images\spacer.gif" width="0" height="140"></td>

                              </tr>
                            </table></td></tr></table>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td >      
                          
                          
                          
                          
                          
                          
<div style="padding:5px;">             
<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
<td nowrap width="5"><img src="images/projcat16.jpg"></td>
	<td nowrap><div class="asset_name"><span class="nowrap"><b><font face="Verdana" size="1"><%#DataBinder.Eval(Container.DataItem, "name")%></font></b></span></div></td>
	<td align="right" width="20"><font color="#808080" face="Arial" size="1"><input onclick="highlightProjectToggle(this,<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);" class="preferences" type="checkbox" name="pselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" value="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"></font></td>
</tr>
</table>

<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
	<td nowrap><font face="Verdana" size="1">Updated</font></td>
	<td align="left" width="868"><div class="asset_date"><span class="nowrap"><font face="Verdana" size="1">

<%# String.Format("{0:MM/dd/yy hh:mm tt}", (DataBinder.Eval(Container.DataItem, "update_date")))%></font></span></div></td>

</tr>
</table>
<div style="padding-top:3px;">
<div class="asset_links"></div></div>
<b><font face="Verdana" size="1"><a id="25257" href="IDAM.aspx?page=Project&Id=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=project">View Details</a></font><br>       
</div>                        
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
							</td>
                        </tr>
                      </table>
                    </div>
                    
                   <img border="0" src="images/spacer.gif" width="135" height="8">
                   </div>
                   </div><!--end Project Thumbnail span-->

                   </div><!--end inner span-->









</ItemTemplate>
<HeaderTemplate>
<!--Start Thumbnail Float-->
<table width=100%><tr><td>
<div valign="Bottom" style="float:left!important;">       
</HeaderTemplate>
<FooterTemplate>
</td></tr></table>
</FooterTemplate> 
</asp:Repeater>

<asp:Literal id="ltrlPagingBottom" Text="" runat="server" />
</div><!--End padding 10px Main window frame-->
<!--hidden Thumbs-->



















											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<TABLE height="300" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px">Loading...
																	</TD>
																	<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>









										</div><!--SnapProjects-->
									</div><!--padding-->												
								
								
								
								
								
								
								
								
								
								
								
								


      </div><!--END browsecenter-->
</div><!--END MAIN-->
      
      
      
      
      


      <script type=text/javascript>
  // Image preloading
  var img1 = new Image();
  img1.src = 'images/header_hoverBg.gif';
  var img2 = new Image();
  img2.src = 'images/header_activeBg.gif';
  var img3 = new Image();
  img3.src = 'images/spinner.gif';
  

  
  function BrowseOnDoubleClick(SelectedGridRow)
  {
  
  	var itemvaluetmp;
	itemvaluetmp = SelectedGridRow.GetMember('uniqueid').Value;
	if (itemvaluetmp.substring(0,1) == 'P') 
	{
		itemvaluetmp = itemvaluetmp.replace('P_','');
		window.location.href = 'IDAM.aspx?page=Project&id=' + itemvaluetmp + '&type=project'
    
	} else {
		itemvaluetmp = itemvaluetmp.replace('C_','');
		window.location.href = 'IDAM.aspx?page=Browse&id=' + itemvaluetmp + '&type=category';
	}
	
	return true;
  }
  
    function highlightProjectToggle(checkbox,projectid)
  {
  var highlightbox;
  highlightbox = document.getElementById('projecthighlight_' + projectid);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
 
  </script>      
      
      
      
      
<div style="padding-left:10px;"><div style="padding:3px;"></div></div>






<script language=javascript>
//thumbnailsarea.style.display = 'none';
</script>
