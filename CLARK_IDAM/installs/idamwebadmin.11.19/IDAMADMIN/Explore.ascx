<%@ Control Language="vb" AutoEventWireup="false" Codebehind="Explore.ascx.vb" Inherits="IDAM5.Explore" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<script language="javascript">

function nodeExpand(sourceNode)
{
//<% Response.Write(TreeView1.ClientID) %>.Render();
return true;
}


function nodeMove(sourceNode, targetNode)
  {
    var doMove = false;  
    var srouceNodeText;
    var srouceNodeID;
    var targetNodeID;
    var comboID;
    //possible combos  CATCAT PRJCAT ACTACT ACTPRJ ASSACT ASSPRJ
    srouceNodeID=sourceNode.ID.substr(0,3).toLocaleUpperCase();
    targetNodeID=targetNode.ID.substr(0,3).toLocaleUpperCase();
    comboID=srouceNodeID+targetNodeID
	if(comboID=='CATCAT'||comboID=='PRJCAT'||comboID=='ACTACT'||comboID=='ASSACT'||comboID=='ASSPRJ')
    {
		if(targetNode)
		{
		doMove = confirm("Move '" + rightTrim(sourceNode.Text) + "' to '" + targetNode.Text + "'?"); 
		} else {
		doMove = false; 
		}
		if (doMove)
		{
			<% Response.Write(CallBackExplore.ClientID) %>.Callback("Move" + "," + sourceNode.ID + "," +  targetNode.ID);
		}
	}
	else
	{
	doMove = false; 
	}
    return doMove; 
  }
  
  function rightTrim(sString) 
	{
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
	sString = sString.substring(0,sString.length-1);
	}
	return sString;
	}
	
	
  function treeContextMenu(treeNode, e)
    {
    var contextMenuX = 0; 
    var contextMenuY = 0; 
    e = (e == null) ? window.event : e;
    contextMenuX = e.pageX ? e.pageX : e.x;
    contextMenuY = e.pageY ? e.pageY : e.y;
	<%if instr(Request.ServerVariables ("HTTP_USER_AGENT"),"MSIE") > 0 then%>
		var sheight = (!window.opera && document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body;
		contextMenuY = contextMenuY + 160 - sheight.scrollTop;
		contextMenuX = contextMenuX + 25;
	<%end if%>
	document.getElementById('ExploreTreeNewselectednode').value = treeNode.ID;
	document.getElementById('idamaction').value = 'editcategories';

	//cutoff all alpha structures
	//if (treeNode.ID.indexOf('ALP')&&treeNode.ID.indexOf('PRD'))
	//{
		switch(treeNode.ID.substr(0,3).toLocaleUpperCase())
		{
			case 'CAT': 
			TreeViewExplore_MenuExploreTreeNewContextCategory.ShowContextMenu(contextMenuX,contextMenuY,treeNode); 
			//TreeViewExplore_MenuExploreTreeNewContext.ShowContextMenu(e, treeNode); 
			break; 
			
			case 'PRJ': 
			TreeViewExplore_MenuExploreTreeNewContextProject.ShowContextMenu(contextMenuX,contextMenuY,treeNode); 
			//TreeViewExplore_MenuExploreTreeNewContext.ShowContextMenu(e, treeNode); 
			break; 

			case 'ACT': 
			TreeViewExplore_MenuExploreTreeNewContextProjectFolder.ShowContextMenu(contextMenuX,contextMenuY,treeNode); 
			//TreeViewExplore_MenuExploreTreeNewContext.ShowContextMenu(e, treeNode); 
			break; 			

			case 'ASS': 
			TreeViewExplore_MenuExploreTreeNewContextAsset.ShowContextMenu(contextMenuX,contextMenuY,treeNode); 
			//TreeViewExplore_MenuExploreTreeNewContext.ShowContextMenu(e, treeNode); 
			break; 	
			
			case 'POM': 
			TreeViewExplore_MenuExploreTreeNewContextPortalMain.ShowContextMenu(contextMenuX,contextMenuY,treeNode); 
			//TreeViewExplore_MenuExploreTreeNewContext.ShowContextMenu(e, treeNode); 
			break; 		
			
			case 'POR': 
			TreeViewExplore_MenuExploreTreeNewContextPortal.ShowContextMenu(contextMenuX,contextMenuY,treeNode); 
			//TreeViewExplore_MenuExploreTreeNewContext.ShowContextMenu(e, treeNode); 
			break; 	
			
			case 'POP': 
			TreeViewExplore_MenuExploreTreeNewContextPortalPage.ShowContextMenu(contextMenuX,contextMenuY,treeNode); 
			//TreeViewExplore_MenuExploreTreeNewContext.ShowContextMenu(e, treeNode); 
			break; 		
			
			case 'POL': 
			TreeViewExplore_MenuExploreTreeNewContextPortalPortlet.ShowContextMenu(contextMenuX,contextMenuY,treeNode); 
			//TreeViewExplore_MenuExploreTreeNewContext.ShowContextMenu(e, treeNode); 
			break; 											

			default: 
			//TreeViewExplore_MenuExploreTreeNewContext.ShowContextMenu(contextMenuX,contextMenuY,treeNode); 
			break; 
		}  
      //}
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    function contextMenuClickHandler(menuItem)
    {
	  document.getElementById('ExploreTreeNewselectednodeaction').value = menuItem.ID;
      var contextDataNode = menuItem.ParentMenu.ContextData; 
      var sType = contextDataNode.ID.substr(0,3).toLocaleUpperCase();
      document.getElementById('ExploreTreeNewselectednode').value = contextDataNode.ID;
      switch(menuItem.ID)
      {
		case 'NEWCATEGORY':
			document.getElementById('PageForm').submit();
			break;
			
		case 'NEWQUICKCATEGORY':
			if (sType == 'CAT') {
				Object_PopUp('CategoryEdit.aspx?parent_id=' + contextDataNode.ID + '&action=new','Edit_Category',800,700);
			}
			break;			

		case 'NEWPROJECTFOLDER':
			if ((sType == 'ACT') || (sType == 'PRJ')) {
				Object_PopUp('ProjectFolderEdit.aspx?parent_id=' + contextDataNode.ID + '&action=new','Edit_Project_Folder',600,700);
			}		
			break;						
			
		case 'DELETE':
			 if (confirm("Delete " + rightTrim(contextDataNode.Text) + "?"))
			 {
				if (confirm("Deleting this item will delete all items underneath it.  Do you want to continue?  Note: This cannot be undone."))
				{
					document.getElementById('PageForm').submit();
					break;
				}
				break;
			}
			else
			{
				break;
			}
			
		case 'EDITFULL':
			document.getElementById('PageForm').submit();
			break;
			
		case 'EDITQUICK':
			switch (sType)
			{
				case 'PRJ':
					Object_PopUp('ProjectNew.aspx?id=' + contextDataNode.ID + '&action=edit','New_Project',650,850);
					break;
					
				case 'CAT':
					Object_PopUp('CategoryEdit.aspx?id=' + contextDataNode.ID + '&action=edit','Edit_Category',600,700);
					break;			

				case 'ACT':
					Object_PopUp('ProjectFolderEdit.aspx?id=' + contextDataNode.ID + '&action=edit','Edit_Project_Folder',600,850);
					break;									
				
				default:
					break;
			
			}
			break;			

		case 'NEWPROJECT':
			if (sType == 'CAT')
				{
				Object_PopUp('ProjectNew.aspx?parent_id=' + contextDataNode.ID,'New_Project',650,850);
				}
			break;
			
		case 'UPLOAD':
			if ((sType == 'ACT') || (sType == 'PRJ')) {
				Object_PopUp_UploadExplore(contextDataNode.ID);
			}		
			break;			
			
		case 'NEWASSET':
			if ((sType == 'ACT') || (sType == 'PRJ')) {
				Object_PopUp_UploadExplore(contextDataNode.ID);
			}		
			break;	
		case 'NEWPORTAL':
			Object_PopUp('PortalEdit.aspx?action=new','New_Portal',650,850);	
			break;	
			
		case 'EDITPORTAL':
			Object_PopUp('Portaledit.aspx?id=' + contextDataNode.ID + '&action=edit','Edit_Portal',650,850);	
			break;		
			
		case 'NEWPORTALPAGE':
			Object_PopUp('PortalPageEdit.aspx?parent_id=' + contextDataNode.ID + '&action=new','New_Portal_Page',650,850);	
			break;	
			
		case 'EDITPORTALPAGE':
			Object_PopUp('PortalPageEdit.aspx?id=' + contextDataNode.ID + '&action=edit','Edit_Portal_Page',650,850);	
			break;		
			
		case 'NEWPORTLET':
			Object_PopUp('PortletEdit.aspx?parent_id=' + contextDataNode.ID + '&action=new','New_Portlet',650,950);	
			break;	
			
		case 'EDITPORTLET':
			Object_PopUp('PortletEdit.aspx?id=' + contextDataNode.ID + '&action=edit','Edit_Portlet',650,950);	
			break;			
			
		case 'DELETEPORTALNODES':
			 if (confirm("Delete " + rightTrim(contextDataNode.Text) + "?"))
			 {
					document.getElementById('PageForm').submit();
				break;
			}
			else
			{
				break;
			}		
											
													
			
		default:
			break;
      }
     
      return true; 
    }


function NavigateToProject(redirectloc)
{
	window.location = 'IDAM.aspx?Page=Project&ID=' + redirectloc + '&type=Project&subtype=ProjectDetails';
}

function NavigateToRepository()
{
	window.location = 'IDAM.aspx?Page=Repository';
}

function NavigateToPage(Page)
{
	window.location = 'IDAM.aspx?Page=' + Page;
}


function NavigateToCategory(redirectloc)
{
	window.location = 'IDAM.aspx?Page=Category&ID=' + redirectloc + '&type=category&action=edit';
	
}

function NavigateToProjectFolder(pid,redirectloc)
{
	window.location = 'IDAM.aspx?Page=Project&ID=' + pid + '&type=Project&c=' + redirectloc;
}


function NavigateToProjectFolder2(pid,redirectloc)
{
	window.location = '../../IDAM.aspx?Page=Project&ID=' + pid + '&type=project&c=' + redirectloc;
}

function NavigateToAsset2(pid)
{
	window.location = '../../IDAM.aspx?Page=Asset&ID=' + pid + '&type=asset';
}


function Object_PopUp(strUrl, popupName, intHeight, intWidth)
{


	if (strUrl == null || strUrl.Length <= 0)
		return;

	var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=yes,resizable=yes,toolbar=no";
	//var strFeatures = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;
	//var strFeatures += "'"
	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures , false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
}




function Object_PopUpScrollbars(strUrl, popupName, intHeight, intWidth)
{
	if (strUrl == null || strUrl.Length <= 0)
		return;
	var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=yes,resizable=yes,toolbar=no";
	//var strFeatures = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;
	//var strFeatures += "'"
	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures , false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
}
</script>
<div class="ExploreDiv" style="BACKGROUND-COLOR:#f8f8f8">
	<table width="100%" style="BACKGROUND-COLOR:white">
		<tr>
			<td align="left" width="10%"><asp:Image id="Image1" runat="server" ImageUrl="~/common/images/spacer.gif" Width="0" Height="0"></asp:Image></td>
			<td align="right" width="90%"><COMPONENTART:CALLBACK id="CallBackExplore" runat="server" CacheContent="false" Height="0">
					<CONTENT>
						<asp:Image id="dummy" runat="server" ImageUrl="~/common/images/spacer.gif" Width="0" Height="0"></asp:Image>
					</CONTENT>
					<LOADINGPANELCLIENTTEMPLATE>
						<TABLE cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD align="center">
									<TABLE cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD style="FONT-SIZE: 10px">Loading...
											</TD>
											<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</LOADINGPANELCLIENTTEMPLATE>
				</COMPONENTART:CALLBACK></td>
		</tr>
	</table>
</div>
<ComponentArt:TreeView id="TreeView1" ExpandCollapseInFront="true" OnContextMenu="treeContextMenu" Height="400px"
	Width="100%" AutoCallBackOnNodeMove="True" DragAndDropEnabled="true" NodeEditingEnabled="false" KeyboardEnabled="false"
	ExpandCollapseImageWidth="16" ExpandCollapseImageHeight="16" NodeIndent="15" ExpandSinglePath="True"
	ItemSpacing="0" ExpandImageUrl="images/colarrow.gif" CollapseImageUrl="images/exparrow.gif" CssClass="TreeViewFullLine"
	NodeCssClass="TreeNodeFullLine" NodeRowCssClass="TreeNodeRowFullLine" SelectedNodeCssClass="SelectedTreeNodeRowFullLine"
	SelectedNodeRowCssClass="SelectedTreeNodeRowFullLine" HoverNodeCssClass="HoverTreeNodeRowFullLine"
	HoverNodeRowCssClass="HoverTreeNodeRowFullLine" LineImageWidth="19" LineImageHeight="20" DefaultImageWidth="16"
	DefaultImageHeight="16" NodeLabelPadding="3" ParentNodeImageUrl="images/folders.gif" LeafNodeImageUrl="images/folder.gif"
	LineImagesFolderUrl="images/lines/" EnableViewState="false" runat="server" ClientSideOnNodeMove="nodeMove"
	ExtendNodeCells="false" LoadingFeedbackCssClass="TreeNodeFullLine" ClientSideOnNodeExpand="nodeExpand"></ComponentArt:TreeView>
<script>

if (<% Response.Write(TreeView1.ClientID) %>.SelectedNode == null) {
var treeviewselectednodeindex = 0;
}else{
var treeviewselectednodeindex = <% Response.Write(TreeView1.ClientID) %>.SelectedNode.StorageIndex;
}

//set scroll appropriately
var objDiv = document.getElementById('TreeViewExplore_Explore_TreeView1_div');
function settreeviewscroll()
{
if (TreeViewExplore_Explore_TreeView1_loaded)
{
if (treeviewselectednodeindex>20)
{
objDiv.scrollTop = 0;
objDiv.scrollTop = treeviewselectednodeindex*16.8;
}
}
}
window.setTimeout('settreeviewscroll()',2000); 
window.setTimeout('settreeviewscroll()',4000); 





//property finder debug
//for(var prop in <% Response.Write(TreeView1.ClientID) %>)
//{
//alert(prop);
//}
//<% Response.Write(TreeView1.ClientID) %>.scrollTo(500,500);
</script>
