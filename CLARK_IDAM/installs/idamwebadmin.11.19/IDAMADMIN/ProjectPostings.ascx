<%@ Control Language="vb" AutoEventWireup="false" Codebehind="ProjectPostings.ascx.vb" Inherits="IDAM5.ProjectPostings" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<style>
.projectcentertest
{
width:100%;
height:2000px;
z-index:100;
} 

</style>
<script language="javascript">

    function treeProjectContextMenu(treeNode, e)
    {


         //for(var prop in <% Response.Write(MenuProjectExploreTreeNew.ClientID) %>)
		//{
		//	alert(prop);
		//}
        //alert('hey2');
	var contextMenuX = 0; 
    var contextMenuY = 0; 
    e = (e == null) ? window.event : e;
    contextMenuX = e.pageX ? e.pageX : e.x;
    contextMenuY = e.pageY ? e.pageY : e.y;
	<%if instr(Request.ServerVariables ("HTTP_USER_AGENT"),"MSIE") > 0 then%>
		contextMenuY = contextMenuY + 170;
		contextMenuX = contextMenuX + document.body.offsetWidth -220;
	<%end if%>
  //winW = document.body.offsetWidth;
  //winH = document.body.offsetHeight
	document.getElementById('ProjectExploreTreeNewselectednode').value = treeNode.ID;
	document.getElementById('idamaction').value = 'editprojectcategories';
      switch(treeNode.Text)
      {
        case 'Calendar': 
          <%response.write( MenuProjectExploreTreeNewContext.ClientId)%>.ShowContextMenu(e, treeNode); 
          break; 

        default: 
         <%response.write( MenuProjectExploreTreeNewContext.ClientId)%>.ShowContextMenu(contextMenuX,contextMenuY,treeNode); 
          break; 
      }  
    }
    
    
    function contextProjectMenuClickHandler(menuItem)
    {
	  document.getElementById('ProjectExploreTreeNewselectednodeaction').value = menuItem.ID;
      var contextDataNode = menuItem.ParentMenu.ContextData; 
      document.getElementById('ProjectExploreTreeNewselectednode').value = contextDataNode.ID;
      
 switch(menuItem.ID)
      {
		case 'NEWCATEGORY':
			<%response.write(CallBackModifyCategory.ClientId)%>.Callback('NEW,' + contextDataNode.ID);
			break;
			return true;
			
		case 'DELETE':
			 if (confirm("Delete " + contextDataNode.Text + "?"))
			 {
				document.getElementById('idamaction').value = 'deleteprojectcategory';
				document.getElementById('PageForm').submit();
				break;
			}
			else
			{
				break;
			}
			
		case 'EDIT':
			<%response.write( CallBackModifyCategory.ClientId)%>.Callback('EDIT,' + contextDataNode.ID);
			//document.getElementById('PageForm').submit();
			break;
			
		default:
			break;
      }
      return true; 
    }

function hideEditCategory()
{
<%response.write( CallBackModifyCategory.ClientId)%>.Callback('HIDE,');
}







//setup form switch for delete or multi asset zip download
function DownloadPostingAssets(ids){
	var sAssets;
	var downloadtype;
	sAssets = <%response.write( GridPosting.ClientId)%>.GetSelectedItems();
	ids = ids.replace('1 ','');
	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Download selected assets?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (arraylist == ''){
					arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
					} else {
					if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
					arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					}
					}
				}
				
				arraylist= arraylist.substring(1, arraylist.length);
				downloadtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				downloadtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	downloadtype = 'single';
	}
	if (downloadtype == 'multi') {
		if (arraylist != ''){
		window.open('<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownloadGetMulti & "?assetids="%>' + arraylist + '<%="&instance=" & IDAMInstance%>','ConvertingImage','width=500,height=75,location=no');
		}
	}
	if (downloadtype == 'single') {
	/*assume single*/
	window.location = '<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownload & "?dtype=assetdownload&assetid="%>' + ids + '<%="&instance=" & IDAMInstance%>&size=0';
	return true;
	}

}





//setup goto for grid nav
function GotoLocation(ctype,cid)
	{
	if (ctype == '0') {
		window.location = 'IDAM.aspx?page=Project&Id=<%response.write (request.querystring("Id"))%>&type=project&c=' + cid;
	} else{
		window.location = 'IDAM.aspx?page=Project&Id=<%response.write (request.querystring("Id"))%>&type=project';
	}
	return true;
	}








</script>



<table width=100% cellpadding=0 cellspacing=0><tr><td width=100% align=left valign=top >
<div id="projectcenter" class="projectcentertest">
<div class="projectcentertest">

							
							
							
							
							
			
					
					
					
					
							
<ComponentArt:Snap id="SNAPProjectPostings" MinWidth="200" runat="server" FillWidth="True" FillHeight="True" Height="100%"  MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter" DockingContainers="projectcenter" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=5><img src="images/icon_posting.gif"></td><td align=left onmousedown="<% Response.Write(SNAPProjectPostings.ClientID) %>.StartDragging(event);">Postings <br><img src="images/spacer.gif" width=92 height=1></td>
												<td width="15" style="cursor: hand" align="right" ><img onclick="" src="images/editbutton.gif"  border="0"></td><td width="15" style="cursor: hand" align="right" ><img onclick="" src="images/closebutton.gif"  border="0"></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectPostings.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=5><img src="images/icon_posting.gif"></td><td align=left onmousedown="<% Response.Write(SNAPProjectPostings.ClientID) %>.StartDragging(event);">Postings <br><img src="images/spacer.gif" width=127 height=1></td>
												<td width="15" style="cursor: hand" align="right" ><img onclick="" src="images/closebutton.gif"  border="0"></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectPostings.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
								<div class="SnapProjectsWrapper">
								<div class="SnapProjects" style="">
								<img src="images/spacer.gif" width=188 height=1>

																	
																	
																	
																	
																	
																	
																	
																	
<!--No Postings warning-->
<div style="TEXT-ALIGN: left">
<asp:Literal id="LiteralNoPostings" Text="No postings available" visible=false runat="server" /></div>
													
																	
																	
																	
																	
																	
<COMPONENTART:GRID 
id="GridPosting" 
runat="server" 
AutoFocusSearchBox="false"
AutoCallBackOnInsert="true"
AutoCallBackOnUpdate="true"
AutoCallBackOnDelete="true"
pagerposition="2"
ScrollBar="Off"
ClientSideOnSelect="BrowseOnSingleClickAssets" 
ScrollTopBottomImagesEnabled="true"
ScrollTopBottomImageHeight="2"
ScrollTopBottomImageWidth="16"
ScrollImagesFolderUrl="images/scroller/"
ScrollButtonWidth="16"
ScrollButtonHeight="17"
ScrollBarCssClass="ScrollBar"
ScrollGripCssClass="ScrollGrip"
ScrollPopupClientTemplateId="ScrollPopupTemplate" 
Height="10" Width="100%"
LoadingPanelPosition="TopCenter" 
LoadingPanelClientTemplateId="LoadingFeedbackTemplateCategory" 
EnableViewState="true"
GroupBySortImageHeight="10" 
GroupBySortImageWidth="10" 
GroupBySortDescendingImageUrl="group_desc.gif" 
GroupBySortAscendingImageUrl="group_asc.gif" 
GroupingNotificationTextCssClass="GridHeaderText" 
AlternatingRowCssClass="AlternatingRowCategory" 
IndentCellWidth="22" 
IndentCellCssClass="IndentCell" 
TreeLineImageHeight="19" 
TreeLineImageWidth="22" 
TreeLineImagesFolderUrl="images/postinglines/" 
PagerImagesFolderUrl="images/pager/" 
ImagesBaseUrl="images/" 
PreExpandOnGroup="true" 
GroupingPageSize="1" 
PagerTextCssClass="GridFooterTextCategory" 
PagerStyle="Numbered" 
PageSize="25" 
GroupByTextCssClass="GroupByText" 
GroupByCssClass="GroupByCell" 
FooterCssClass="GridFooter" 
HeaderCssClass="GridHeaderCategory" 
SearchOnKeyPress="false" 
SearchTextCssClass="GridHeaderText" 
AllowEditing="true" 
AllowSorting="False"
ShowSearchBox="false" 
ShowHeader="false" 
ShowFooter="true" 
CssClass="GridCategory" 
RunningMode="callback" 
ScrollBarWidth="15" 
AllowPaging="true" >
<ClientTemplates>
		<componentart:ClientTemplate ID="LoadingFeedbackTemplateCategory">
		<div><table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
		</tr>
		</table></div>
		</componentart:ClientTemplate>
		  <ComponentArt:ClientTemplate Id="EditTemplateCategory">
            <a href="javascript:editGridCategory('## DataItem.ClientId ##');">Edit</a> <!--| <a href="javascript:deleteRowCategory('## DataItem.ClientId ##')">Delete</a>-->
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateCategory">
            <a href="javascript:editRowCategory();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateCategory">
            <a href="javascript:insertRowCategory();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateCategory">
            <img border=0 src="images/tasks.gif">
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplateCategory">
            <A href="IDAM.aspx?page=Project&id=## DataItem.GetMember("category_id").Value ##&type=project"><img border=0 src="images/goto.gif"></a>
          </ComponentArt:ClientTemplate>     
          <ComponentArt:ClientTemplate Id="EditTemplateAssetPostings">
           <a href="javascript:DownloadPostingAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> | <a href="javascript:AddToCarousel('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/3Drafts.gif" alt="Edit" border=0></a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateAssetPostings">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateAssetPostings">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>      
          <ComponentArt:ClientTemplate Id="TypeIconTemplateAssetPostings">
            <img src="http://## DataItem.GetMember("imagesource").Value ##" border="0" > 
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplateAssetPostings">
			<A href="## DataItem.GetMember("alink").Value ##"><img border=0 src="images/goto.gif"></a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="LookupCategoryTemplateAssetPostings">
            <A href="javascript:GotoLocation('## DataItem.GetMember("ispost").Value ##','## DataItem.GetMember("category_id").Value ##');">## DataItem.GetMember("categoryname").Value ##</a>
          </ComponentArt:ClientTemplate>    
          <ComponentArt:ClientTemplate Id="LookupCategoryIconTemplateAssetPostings">
            <img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif">
          </ComponentArt:ClientTemplate>                    


          
                              
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" DataMember="Postings" 
            EditFieldCssClass="EditDataField"
            EditCommandClientTemplateId="EditCommandTemplateCategory"
            InsertCommandClientTemplateId="InsertCommandTemplateCategory" DataKeyField="category_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCellCategory" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellCategory" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverCategory" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowCategory" SortImageHeight="19" RowCssClass="RowCategory" HeadingCellActiveCssClass="HeadingCellActiveCategory">
<Columns>

<ComponentArt:GridColumn Align="Center"  AllowSorting="False" AllowEditing="false"   DataCellClientTemplateId="TypeIconTemplateCategory" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings"  HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
<componentart:GridColumn HeadingText="Posted" AllowEditing="false" Width="100" FormatString="MMM dd yyyy, hh:mm" AllowGrouping="False" DataCellCssClass="DataCell" SortedDataCellCssClass="SortedDataCell"  DataField="available_date"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="True" AllowSorting="False" HeadingText="Name" Width="200" AllowGrouping="False" DataCellCssClass="DataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn HeadingText="Description" AllowEditing="true" AllowGrouping="False"  Width="80" DataCellCssClass="DataCell" DataField="description"></componentart:GridColumn>
<componentart:GridColumn HeadingText="Security Level" AllowEditing="true" DataCellCssClass="DataCell" Width="50" SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" AllowSorting="False" HeadingText="Posted By" Width="80" DataCellCssClass="DataCell" AllowGrouping="False"  DataField="fullname" ></componentart:GridColumn>
<componentart:GridColumn Align="Center"  AllowEditing="false" AllowSorting="False" Width="25" HeadingText="Assets" DataCellCssClass="DataCell" AllowGrouping="False"  DataField="numassets" ></componentart:GridColumn>
<ComponentArt:GridColumn HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplateCategory" Width="80" DataCellCssClass="LastDataCellPostings" EditControlType="EditCommand" Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="category_id" SortedDataCellCssClass="SortedDataCell" DataField="category_id"></componentart:GridColumn>
</Columns>
</componentart:GridLevel>


<componentart:GridLevel EditCellCssClass="EditDataCell"
            EditFieldCssClass="EditDataField"
            EditCommandClientTemplateId="EditCommandTemplate"
            InsertCommandClientTemplateId="InsertCommandTemplate" DataMember="Assets" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCellCategory" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellCategory" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverCategory" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowCategory" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActiveCategory">
<Columns>



<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplateAssetPostings" dataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="16" FixedWidth="True" />
<ComponentArt:GridColumn DataCellCssClass="DataCell" Align="Center" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplateAssetPostings" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="16" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="True" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Type" AllowEditing="false" Width="110" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Updated" AllowEditing="false" Width="110"  FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  DataField="update_date"></componentart:GridColumn>
<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplateAssetPostings" EditControlType="EditCommand" Width="140" Align="Center" />
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="category_id" SortedDataCellCssClass="SortedDataCell" DataField="category_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="ispost" SortedDataCellCssClass="SortedDataCell" DataField="ispost"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>


</Columns>

</componentart:GridLevel>








</Levels>
</COMPONENTART:GRID>
																	
																	
																	
																	
																	
																	
																	
																	
																	
																	
																	
																	
																	
																	
																	
										
								</div>
								</div><!--padding-->												
								</Content>
							</ComponentArt:Snap>			
							
							
							
									
					
					
					
					
					
					
							
							
							
							
							
</div><!--bottom padding-->							
</div><!--projectcenter-->	



	





<!--split here-->


</td><td width=200 align=left valign=top >
<div id="projectright" style="border:0px solid;padding-left:5px;">
<div style="">






































<ComponentArt:Snap id="SNAPProjectCategories" MinWidth="200" runat="server" FillWidth="True" FillHeight="True" Height="100%" MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectright"  CurrentDockingIndex="0" DockingContainers="projectright,projectcenter" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=5><img src="images/user16.jpg"></td><td align=left onmousedown="<% Response.Write(SNAPProjectCategories.ClientID) %>.StartDragging(event);">Project Folders<br><img src="images/spacer.gif" width=115 height=1></td>
												<td width="15" style="cursor: hand" align="right" ><COMPONENTART:MENU id="MenuProjectExploreTreeNew" runat="server" Orientation="Horizontal" CssClass="TopGroupExp"
					DefaultGroupCssClass="MenuGroup" SiteMapXmlFile="menuDataProjectExploreTreeNew.xml" DefaultItemLookID="DefaultItemLook"
					DefaultGroupItemSpacing="1" ImagesBaseUrl="images/" EnableViewState="true" ExpandDelay="100" ExpandOnClick="true">
					<ItemLooks>
						<componentart:ItemLook HoverCssClass="TopMenuItemHoverExp" LabelPaddingTop="2px" LabelPaddingRight="10px"
							LabelPaddingBottom="2px" ExpandedCssClass="TopMenuItemExpanded" LabelPaddingLeft="10px" LookId="TopItemLook"
							CssClass="TopMenuItemExp"></componentart:ItemLook>
						<componentart:ItemLook HoverCssClass="MenuItemHoverExp" LabelPaddingTop="3px" LeftIconHeight="18px" LabelPaddingRight="10px"
							LabelPaddingBottom="4px" ExpandedCssClass="MenuItemHoverExp" LabelPaddingLeft="10px" LeftIconWidth="20px"
							LookId="DefaultItemLook" CssClass="MenuItemExp"></componentart:ItemLook>
						<componentart:ItemLook LookId="BreakItem" CssClass="MenuBreak"></componentart:ItemLook>
					</ItemLooks>
				</COMPONENTART:MENU></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectCategories.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=5><img src="images/user16.jpg"></td><td align=left onmousedown="<% Response.Write(SNAPProjectCategories.ClientID) %>.StartDragging(event);">Project Folders<br><img src="images/spacer.gif" width=130 height=1></td>
												<td width="15" style="cursor: hand" align="right" ><img onclick="" src="images/closebutton.gif"  border="0"></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectCategories.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
								<div style="padding-bottom:5px;border:1px;">
								<div class="SnapProjects" style="height:100%;padding:0px;overflow:hidden;padding:5px;background-color:white;">
								<img src="images/spacer.gif" width=188 height=1>
<table class="" cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
		<td width=180 align=center >
		
		
		
		<!--
		 Upload Image:<br>
        </font>
		<font face="Arial" size="2">
		<input type="file" Class="InputFieldFileMain" size="14" name="thefile" id="thefile" runat="server" ></font>
		<input type="button" value="add asset" onclick="javascript:document.PageForm.idamaction='fileupload';document.PageForm.submit();">
	
		<asp:TextBox id="Textbox_Progressid" style="display:none;"  runat="server"></asp:TextBox>
		-->
		

		
		
		<img src="images/spacer.gif" width=188 height=1><br><asp:PlaceHolder id="JavaUploadAssets" runat="server"></asp:PlaceHolder>
		
		
		
	
		
		
		
		
		</td>
	</tr>
	<tr>
	<td width=100% align=center >
	
	</td>
	</tr>
</table>
								
<div>

		
<!--Category Editing-->
<table cellpadding=0 cellspacing=0 ><tr><td>	
<COMPONENTART:CALLBACK id="CallBackModifyCategory" runat="server" CacheContent="false">
	<CONTENT>
		<asp:Literal id="Literal_ModifyCategory" Text="" runat="server" />
	</CONTENT>
	<LOADINGPANELCLIENTTEMPLATE>
		<TABLE cellSpacing="0" cellPadding="0" border="0">
			<TR>
				<TD align="center">
					<TABLE cellSpacing="0" cellPadding="0" border="0">
						<TR>
							<TD style="FONT-SIZE: 10px">Loading...
							</TD>
							<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
	</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>
</td></tr></table>






<!--

<div style="padding:10px;">
<div class="asset_links"></div>
		<p>Name<br>
        <input Class="InputFieldMain" type="text" name="Name" ></font></p>
        <p>Description<br>
        <textarea Class="InputFieldMain" rows="3" name="Description" ></textarea></p>
        <p>Security Level<br>
        <input class="preferences" type="radio" name="SL" value="0" >Administrators<br>
        <input class="preferences" type="radio" name="SL" value="1">Internal Use<br>
        <input class="preferences" type="radio" name="SL" value="2">Clients<br>
        <input class="preferences" type="radio" name="SL" value="3">Public<br>
        <br></p>
        <input Class="InputButtonMain" width="50%" type="submit" value="Save Category" name="AddCategoryButton" id="AddCategoryButton">
        <input type="hidden" name="actionType" value="add">
        <input type="hidden" name="categoryId" value="">
        <input type="hidden" name="parent_cat_id" value="<%=request.querystring("c")%>">
</div>
<div style="padding:10px;">
<div class="asset_links"></div></div>

-->
<!--Category Editing-->

<ComponentArt:Menu id="MenuProjectExploreTreeNewContext" 
      Orientation="Vertical"
      DefaultGroupCssClass="MenuGroup"
      SiteMapXmlFile="menuDataProjectExploreTreeNewContext.xml"
      DefaultItemLookID="DefaultItemLook"
      DefaultGroupItemSpacing="1"
      ImagesBaseUrl="images/"
      EnableViewState="false"
      ContextMenu="Custom"
      ClientSideOnItemSelect="contextProjectMenuClickHandler"
      runat="server">
    <ItemLooks>
      <ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
      <ComponentArt:ItemLook LookID="BreakItem" CssClass="MenuBreak" />
    </ItemLooks>
    </ComponentArt:Menu>
<!--Category Editing-->





</div>						

<div style="OVERFLOW:hidden;HEIGHT:0px;BACKGROUND-COLOR:white">
	<table width="100%" style="BACKGROUND-COLOR:white">
		<tr>
			<td align="left" width="10%"><asp:Image id="Image2" runat="server" ImageUrl="~/common/images/spacer.gif" Width="0" Height="0"></asp:Image></td>
			<td align="right" width="90%"><COMPONENTART:CALLBACK id="CallBackExplorePC" runat="server" CacheContent="false" Height="0">
					<CONTENT>
						<asp:Image id="dummy" runat="server" ImageUrl="~/common/images/spacer.gif" Width="0" Height="0"></asp:Image>
					</CONTENT>
					<LOADINGPANELCLIENTTEMPLATE>
						<TABLE cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD align="center">
									<TABLE cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD style="FONT-SIZE: 10px">Loading...
											</TD>
											<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</LOADINGPANELCLIENTTEMPLATE>
				</COMPONENTART:CALLBACK></td>
		</tr>
	</table>
</div>
<table cellpadding=0 cellspacing=0><tr><td>
			<ComponentArt:TreeView id="TreeViewPC" OnContextMenu="treeProjectContextMenu" Height="200px" Width="188px" AutoCallBackOnNodeMove="True" DragAndDropEnabled="true"
	NodeEditingEnabled="false" KeyboardEnabled="false" CssClass="TreeView" NodeCssClass="TreeNode" SelectedNodeCssClass="SelectedTreeNode"
	HoverNodeCssClass="HoverTreeNode" NodeEditCssClass="NodeEdit" LineImageWidth="19" LineImageHeight="20"
	DefaultImageWidth="16" DefaultImageHeight="16" NodeLabelPadding="3" ParentNodeImageUrl="images/folders.gif"
	LeafNodeImageUrl="images/folder.gif" ShowLines="true" LineImagesFolderUrl="images/lines/" EnableViewState="true"
	runat="server" ClientSideOnNodeMove="nodeMovePC"></ComponentArt:TreeView>						
</td></tr></table>	
									
									
									
									
									
										
										
										
										
										
								</div>
								</div><!--padding-->												
								</Content>
							</ComponentArt:Snap>		





























<ComponentArt:Snap id="SNAPProjectContacts" MinWidth="200" runat="server" FillWidth="True" CurrentDockingIndex="1"  Height="200px"  MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectright"  DockingContainers="projectcenter,projectright" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=5><img src="images/user16.jpg"></td><td align=left onmousedown="<% Response.Write(SNAPProjectContacts.ClientID) %>.StartDragging(event);">Contacts <br><img src="images/spacer.gif" width=130 height=1></td>
												<td width="15" style="cursor: hand" align="right" ><img onclick="" src="images/closebutton.gif"  border="0"></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectContacts.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=5><img src="images/user16.jpg"></td><td align=left onmousedown="<% Response.Write(SNAPProjectContacts.ClientID) %>.StartDragging(event);">Contacts <br><img src="images/spacer.gif" width=130 height=1></td>
												<td width="15" style="cursor: hand" align="right" ><img onclick="" src="images/closebutton.gif"  border="0"></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectContacts.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
								<div style="padding-bottom:5px;border:1px;">
								<div class="SnapProjects" style="height:100%;padding:0px;overflow:hidden;padding-left:5px;padding-right:5px;background-color:white;">
								<img src="images/spacer.gif" width=188 height=1>
<table cellpadding=0 cellspacing=0 ><tr><td align=right>
<a href="#" onclick="deleteContacts();">delete</a> | <a href="#" onclick="addContacts();">add</a><br>	<br>
</td></tr></table>
<table><tr><td>										
<asp:Literal id="LiteralNoContacts" Text="No contacts added." visible=false runat="server" />
</td></tr></table>							
<table cellpadding=0 cellspacing=0 ><tr><td>						
<COMPONENTART:CALLBACK id="CallbackTreeViewContacts" runat="server" CacheContent="false" >
<CONTENT>

<asp:Literal id="LiteralAddContacts" Text="" visible=true runat="server" />

		
</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
	<TABLE cellSpacing="0" cellPadding="0" border="0">
		<TR>
			<TD align="center">
				<TABLE cellSpacing="0" cellPadding="0" border="0">
					<TR>
						<TD style="FONT-SIZE: 10px">Loading...
						</TD>
						<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
	</TABLE>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>										
</td></tr></table>								


<asp:Literal id="LiteralTVCStart" Text="" visible=false runat="server" />
<table cellpadding=0 cellspacing=0 ><tr><td>
<ComponentArt:TreeView id="TreeviewContacts" Height="100%" Width="188px" AutoCallBackOnNodeMove="True" DragAndDropEnabled="false"
	NodeEditingEnabled="false" KeyboardEnabled="false" CssClass="TreeView" NodeCssClass="TreeNode" SelectedNodeCssClass="SelectedTreeNode"
	HoverNodeCssClass="HoverTreeNodeContacts" NodeEditCssClass="NodeEdit" LineImageWidth="19" LineImageHeight="20"
	DefaultImageWidth="16" DefaultImageHeight="16" ExpandSinglePath="true"  NodeLabelPadding="3" ParentNodeImageUrl="images/folders.gif"
	LeafNodeImageUrl="images/folder.gif" ShowLines="false" LineImagesFolderUrl="images/lines/" EnableViewState="true"         ExpandCollapseImageWidth="15" 
    ExpandCollapseImageHeight="15"          NodeIndent="16"         SpacerImageUrl="images/spacer.gif"         CollapseImageUrl="images/exp.gif"         ExpandImageUrl="images/col.gif"         ItemSpacing="3" 
	runat="server">
		 <ServerTemplates>
         
          <ComponentArt:NavigationCustomTemplate id="ContactInfoTemplate">
            <Template>
            <table width=188><tr><td>
            First Name: <%# Container.Attributes("FirstName") %><br>
            Last Name: <%# Container.Attributes("LastName") %><br>
            Email: <a href="mailto:<%# Container.Attributes("Email") %>"><%# Container.Attributes("Email") %></a><br>
            Agency: <%# Container.Attributes("Agency") %><br>
            Phone: <%# Container.Attributes("Phone") %><br>
            Address: <%# Container.Attributes("workAddress") %><br>
            City: <%# Container.Attributes("workCity") %><br>
            State: <%# Container.Attributes("workState") %><br>
            Zip: <%# Container.Attributes("workZip") %><br>
            Cell: <%# Container.Attributes("Cell") %><br>
            Department: <%# Container.Attributes("Department") %><br>
            </td></tr></table>

            </Template>
          </ComponentArt:NavigationCustomTemplate>
          
        </ServerTemplates>  

	</ComponentArt:TreeView></td></tr></table><br>
	<asp:Literal id="LiteralTVCEnd" Text="" visible=false runat="server" />


					
		
										
										
				
										
										
										
										
								</div>
								</div><!--padding-->												
								</Content>
							</ComponentArt:Snap>
							
							
							
							
							
							
							
							
							
							
							
							









<ComponentArt:Snap id="SNAPProjectVendors" MinWidth="200" runat="server" FillWidth="True" FillHeight="True" CurrentDockingIndex="2" Height="100%" MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectright"  DockingContainers="projectcenter,projectright" >
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=5><img src="images/user16.jpg"></td><td align=left onmousedown="<% Response.Write(SNAPProjectVendors.ClientID) %>.StartDragging(event);">Vendors <br><img src="images/spacer.gif" width=95 height=1></td>
												<td width="15" style="cursor: hand" align="right" ><img onclick="" src="images/editbutton.gif"  border="0"></td><td width="15" style="cursor: hand" align="right" ><img onclick="" src="images/closebutton.gif"  border="0"></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectVendors.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=5><img src="images/user16.jpg"></td><td align=left onmousedown="<% Response.Write(SNAPProjectVendors.ClientID) %>.StartDragging(event);">Vendors <br><img src="images/spacer.gif" width=130 height=1></td>
												<td width="15" style="cursor: hand" align="right" ><img onclick="" src="images/closebutton.gif"  border="0"></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectVendors.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
								<div style="padding-bottom:5px;border:1px;">
								<div class="SnapProjects" style="height:100%;width=100%padding:0px;padding-left:5px;padding-right:5px;background-color:white;">
								<img src="images/spacer.gif" width=188 height=1>
<table cellpadding=0 cellspacing=0 ><tr><td align=right>
<a href="#" onclick="deleteVendors();">delete</a> | <a href="#" onclick="addVendors();">add</a><br>	<br>
</td></tr></table>
<table><tr><td>										
<asp:Literal id="LiteralNoVendors" Text="No vendors added." visible=false runat="server" />
</td></tr></table>							
<table cellpadding=0 cellspacing=0 ><tr><td>						
<COMPONENTART:CALLBACK id="CallbackTreeViewVendors" runat="server" CacheContent="false" >
<CONTENT>

<asp:Literal id="LiteralAddVendors" Text="" visible=true runat="server" />

		
</CONTENT>
<LOADINGPANELCLIENTTEMPLATE>
	<TABLE cellSpacing="0" cellPadding="0" border="0">
		<TR>
			<TD align="center">
				<TABLE cellSpacing="0" cellPadding="0" border="0">
					<TR>
						<TD style="FONT-SIZE: 10px">Loading...
						</TD>
						<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
	</TABLE>
</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>										
</td></tr></table>								


<asp:Literal id="LiteralTVVStart" Text="" visible=false runat="server" />
<table cellpadding=0 cellspacing=0 ><tr><td>
<ComponentArt:TreeView id="TreeviewVendors" Height="100%" Width="188px" AutoCallBackOnNodeMove="True" DragAndDropEnabled="false"
	NodeEditingEnabled="false" KeyboardEnabled="false" CssClass="TreeView" NodeCssClass="TreeNode" SelectedNodeCssClass="SelectedTreeNode"
	HoverNodeCssClass="HoverTreeNodeContacts" NodeEditCssClass="NodeEdit" LineImageWidth="19" LineImageHeight="20"
	DefaultImageWidth="16" DefaultImageHeight="16" ExpandSinglePath="true"  NodeLabelPadding="3" ParentNodeImageUrl="images/folders.gif"
	LeafNodeImageUrl="images/folder.gif" ShowLines="false" LineImagesFolderUrl="images/lines/" EnableViewState="true"         ExpandCollapseImageWidth="15" 
    ExpandCollapseImageHeight="15"          NodeIndent="16"         SpacerImageUrl="images/spacer.gif"         CollapseImageUrl="images/exp.gif"         ExpandImageUrl="images/col.gif"         ItemSpacing="3" 
	runat="server">
		 <ServerTemplates>
         
          <ComponentArt:NavigationCustomTemplate id="VendorInfoTemplate">
            <Template>
            <table width=188><tr><td>
            Name: <%# Container.Attributes("Name") %><br>
           
            Email: <a href="mailto:<%# Container.Attributes("Email") %>"><%# Container.Attributes("Email") %></a><br>
           
            Phone: <%# Container.Attributes("Phone") %><br>
            Address: <%# Container.Attributes("Address1") %><br>
            Address: <%# Container.Attributes("Address2") %><br>
            Address: <%# Container.Attributes("Address3") %><br>
            Address: <%# Container.Attributes("Address4") %><br>
            City: <%# Container.Attributes("City") %><br>
            State: <%# Container.Attributes("State") %><br>
            Zip: <%# Container.Attributes("Zip") %><br>
            </td></tr></table>

            </Template>
          </ComponentArt:NavigationCustomTemplate>
          
        </ServerTemplates>  

	</ComponentArt:TreeView></td></tr></table><br>
	<asp:Literal id="LiteralTVVEnd" Text="" visible=false runat="server" />


					
		
										
										
				
										
										
										
										
								</div>
								</div><!--padding-->												
								</Content>
							</ComponentArt:Snap>		




















</div><!--padding-->
</div><!--projectright-->
</td></tr></table>











































<script language="javascript">
function nodeMovePC(sourceNode, targetNode)
  {
    var doMove = false;  
    var srouceNodeText;
	if(targetNode)
	{
      doMove = confirm("Move '" + rightTrim(sourceNode.Text) + "' to '" + targetNode.Text + "'?"); 
    } else {
    doMove = false; 
    }
    if (doMove)
    {
		<% Response.Write(CallBackExplorePC.ClientID) %>.Callback("Move" + "," + sourceNode.ID + "," +  targetNode.ID);
	}
    return doMove; 
  }
  
  function deleteContacts()
  {
  var treeViewNodes;
  treeViewNodes = '';
  for (i=0;i<40;i++)
  {
  if (<% Response.Write(TreeviewContacts.ClientID) %>.Nodes(i) != null){
  
	if (<% Response.Write(TreeviewContacts.ClientID) %>.Nodes(i).Checked != undefined){
		if (<% Response.Write(TreeviewContacts.ClientID) %>.Nodes(i).Checked) 
		{
		treeViewNodes += <% Response.Write(TreeviewContacts.ClientID) %>.Nodes(i).ID + ","
		}
	}
	}
  }
	if (treeViewNodes != '') {
	var nodelist=treeViewNodes.split(",")
	for (i=0;i<nodelist.length-1;i++)
	{
	    <% Response.Write(TreeviewContacts.ClientID) %>.FindNodeById(nodelist[i]).Remove();
	}
	<% Response.Write(CallbackTreeViewContacts.ClientID) %>.Callback(treeViewNodes);
	}
	<% Response.Write(TreeviewContacts.ClientID) %>.Render();
  }
  
  function addContacts()
  {
  <% Response.Write(CallbackTreeViewContacts.ClientID) %>.Callback('addcontacts');
  }
  
  function cancelAddContacts()
  {
  <% Response.Write(CallbackTreeViewContacts.ClientID) %>.Callback('canceladdcontacts');
  }
  
  <% Response.Write(GridPosting.ClientID) %>.ExpandAll;
  


  
  
  
  
  
  
  function deleteVendors()
  {
  var treeViewNodes;
  treeViewNodes = '';
  for (i=0;i<40;i++)
  {
  if (<% Response.Write(TreeviewVendors.ClientID) %>.Nodes(i) != null){
  
	if (<% Response.Write(TreeviewVendors.ClientID) %>.Nodes(i).Checked != undefined){
		if (<% Response.Write(TreeviewVendors.ClientID) %>.Nodes(i).Checked) 
		{
		treeViewNodes += <% Response.Write(TreeviewVendors.ClientID) %>.Nodes(i).ID + ","
		}
	}
	}
  }
	if (treeViewNodes != '') {
	var nodelist=treeViewNodes.split(",")
	for (i=0;i<nodelist.length-1;i++)
	{
	    <% Response.Write(TreeviewVendors.ClientID) %>.FindNodeById(nodelist[i]).Remove();
	}
	<% Response.Write(CallbackTreeViewVendors.ClientID) %>.Callback(treeViewNodes);
	}
	<% Response.Write(TreeviewVendors.ClientID) %>.Render();
  }
  
  function addVendors()
  {
  <% Response.Write(CallbackTreeViewVendors.ClientID) %>.Callback('addvendors');
  }
  
  function cancelAddVendors()
  {
  <% Response.Write(CallbackTreeViewVendors.ClientID) %>.Callback('canceladdvendors');
  }
  
 
  
  
  
  function highlightAssetToggle(checkbox,asset_id)
  {
  var highlightbox;
  highlightbox = document.getElementById('assethighlight_' + asset_id);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  
  
  
  
  
  </script>	





