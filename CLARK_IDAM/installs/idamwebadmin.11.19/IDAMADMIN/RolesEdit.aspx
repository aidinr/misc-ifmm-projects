<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RolesEdit.aspx.vb" Inherits="IDAM5.RolesEdit"%>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Edit Roles</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
			<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
			<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
				<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
					<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
						<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
							<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
								<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
									<link href="treeStyle.css" type="text/css" rel="stylesheet">
										<link href="navStyle.css" type="text/css" rel="stylesheet" >
										    <link href="tabStripStyle.css" type="text/css" rel="stylesheet" />    
												<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
										<style>BODY { MARGIN: 0px }
	</style>
	<body MS_POSITIONING="GridLayout" onload="this.focus">
		<form id="Form" method="post" runat="server">
		
		
		
		<script language=javascript>
function RefreshRoles(id)
{
if (id!='')
{
window.location.href = 'RolesEdit.aspx?id=' + id 
} else
{
window.location.href = 'RolesEdit.aspx?id=1';
}
}




function Object_PopUp(strUrl, popupName, intHeight, intWidth)
{


	if (strUrl == null || strUrl.Length <= 0)
		return;

	var strFeatures = "directories=no,location=no,menubar=no,center=yes,scrollbars=no,resizable=yes,toolbar=no";
	//var strFeatures = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;
	//var strFeatures += "'"
	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures , false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
}


  function onInsertCategory(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }

  function onUpdateCategory(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }


  


</script>
		
<script type="text/javascript">
  

<%if request.querystring("newid") <> "" then%>
window.opener.NavigateToCategory('<%response.write (request.querystring("newid"))%>');
//window.close();
<%end if%>

<%if instr(request.querystring("id"),"CAT") = 0  then%>
<%if request.querystring("parent_id") = ""  then%>
//window.close();
<%end if%>
<%end if%>

</script>

			<%response.write (request.querystring("newid"))%>
			<input type="hidden" name="catid" id="catid" value="<%response.write (replace(request.querystring("id"),"CAT",""))%>">
			<input type="hidden" name="parent_id" id="parent_id" value="<%response.write (request.querystring("parent_id"))%>">
			<div style="padding:20px;position:relative;">
			<div style="width:100%">
			<div style="padding:10px;border:1px solid;background-color:white;position:relative;">
			<table><tr><td><img src="images/top_contacts.gif"></td><td>Create/Edit Roles</td></tr></table><br>
			
			
			  <div style="width=100%">
			
			
	<COMPONENTART:TABSTRIP id="TabStripCategory" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataRoles.xml"
		CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
		DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
		<ItemLooks>
			<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10" 
 LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif" 
 HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23" 
 LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
			<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10" 
 LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif" 
 LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
		</ItemLooks>
	</COMPONENTART:TABSTRIP>
	
	<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageCategoryPage" runat="server">
		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Pageview1">
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Edit/modify roles.</div>
			<br>

			<table cellspacing="0" cellpadding="3" border="0" width="100%">



				<tr>
					<td class="PageContent" width="120" align="left" valign="top" nowrap>
						<font face="Verdana" size="1">Role Set</font></td>
					<td colspan="2" align="left" valign="top" width=100%>

<table width=100%><tr>
<td width=300px><asp:DropDownList DataTextField="name"  DataValueField="role_id" id="Roles" style="width:250px"  runat="server"></asp:DropDownList></td>
<td width=200px  nowrap ><asp:Button  id="Button_Edit_Role"  Runat=server   text="Edit"></asp:Button> &nbsp;<input type=Button id="Button_New_Role"  onclick="newrole();"  value="Create New"></td>
<td width=100%></td>
</tr></table>				
<script language=javascript>
function changerole()
{
//if (confirm('Are you sure you want to change this role?')) {
__doPostBack('Roles','');
//}
}



function newrole()
{

Object_PopUp('RoleSetEdit.aspx','Edit_Role_Set',500,500);
}

function editrole()
{

  var dropdown = document.getElementById('Roles');
  var myindex  = dropdown.selectedIndex;
  var SelValue = dropdown.options[myindex].value;


Object_PopUp('RoleSetEdit.aspx?id=' + SelValue,'Edit_Role_Set',500,500);
}


function NewRoleItem()
{
Object_PopUp('RoleItemEdit.aspx','Edit_Role_Item',500,500);
}

function EditRoleItem()
{
//get treeview selected node
var TreeNode;
TreeNode = <%response.write (TreeView_Roles.ClientId)%>.SelectedNode;
Object_PopUp('RoleItemEdit.aspx?id=' + TreeNode.ID,'Edit_Role_Item',500,500);
}

function DeleteRoleItem()
{
var TreeNode;
var dropdown = document.getElementById('Roles');
var myindex  = dropdown.selectedIndex;
var SelValue = dropdown.options[myindex].value;
TreeNode = <%response.write (TreeView_Roles.ClientId)%>.SelectedNode;
if (confirm("Are you sure you want to delete the role item \'" + TreeNode.Value + "\'?"))
{
window.location.href = 'RolesEdit.aspx?id=' + SelValue + '&action=deleteroleitem&rid=' + TreeNode.ID;
return true; 
}
else
{
return false;
}
}


</script>


				</td>
					<td class="PageContent" width="80%" align="left" valign="top">
						</td>
				</tr>
				<tr>
					<td class="PageContent" width="120" align="left"  nowrap>
						<font face="Verdana" size="1"></font></td>
					<td class="PageContent" colspan="3" align="left"  width=100%><asp:CheckBox id="chkactive" runat=server></asp:CheckBox>Active
						</td>
				</tr>

				
				
				
				
				<tr>
					<td class="PageContent" width="120" align="left"  nowrap>
						<font face="Verdana" size="1">Description</font></td>
					<td class="PageContent" colspan="3" align="left" width=100%>&nbsp;<asp:Literal ID="Description" Runat=server ></asp:Literal><br><br>
						</td>
				</tr>

			</table>
			
			
			
			
			
			
			
			
			<table border="0" width="100%" id="table2">
										<tr>
											<td width="88"><b><font face="Verdana" size="1">Role Items</font></b></td>
											<td><hr>
											</td>
										</tr>
									</table>
			<div style="padding:10px;width=100%;"><!--sub tabs-->
			
			
			
			
			<COMPONENTART:TABSTRIP id="Tabstrip1" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataRoleItems.xml"
		CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" DefaultItemLookId="DefaultTabLook"
		DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultipageRoleItems">
		<ItemLooks>
			<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10" 
 LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif" 
 HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23" 
 LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
			<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10" 
 LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif" 
 LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
		</ItemLooks>
	</COMPONENTART:TABSTRIP>
	
	<ComponentArt:MultiPage id="MultipageRoleItems" CssClass="MultiPageCategoryPage" runat="server">
		<ComponentArt:PageView CssClass="PageContent2" runat="server" ID="Pageviewx1">

<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Modify Role Items</div><br>





<table width=100%><tr>
<td width=100% style="FONT-SIZE: 11px; ">[ <a href=# onClick="javascript:NewRoleItem();">create new</a> ] [ <a  href=# onclick="javascript:EditRoleItem();">edit selected</a> ] [ <a href=#  onclick="javascript:DeleteRoleItem();">delete selected</a> ] </td>
<td width=200px align=right></td>
</tr></table>	
		<ComponentArt:TreeView id="TreeView_Roles" Height="280" Width="100%" 
		AutoPostBackOnNodeCheckChanged="true"
		DragAndDropEnabled="false" 
		NodeEditingEnabled="false" 
		KeyboardEnabled="true" 
		CssClass="TreeView" 
		NodeCssClass="TreeNode" 
		SelectedNodeCssClass="SelectedTreeNode" 
		HoverNodeCssClass="HoverTreeNode" 
		NodeEditCssClass="NodeEdit" 
		LineImageWidth="19" 
		LineImageHeight="20" 
		DefaultImageWidth="16" DefaultImageHeight="16" ItemSpacing="0" NodeLabelPadding="3" 
		ParentNodeImageUrl="images/folders.gif" LeafNodeImageUrl="images/folder.gif" ShowLines="true" 
		LineImagesFolderUrl="images/lines/" 
		EnableViewState="true"  
		runat="server" >
        </ComponentArt:TreeView>

















		</ComponentArt:PageView>
		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Pageviewx2">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Modify Project Field Permissions</div><br>

<table width=100%><tr>
<td width=100% style="FONT-SIZE: 11px; ">Role-Based Project Field Permissions </td>
<td width=200px align=right></td>
</tr></table>	
		<ComponentArt:TreeView id="TreeviewProjectFields" Height="280" Width="100%" 
		AutoPostBackOnNodeCheckChanged="true"
		DragAndDropEnabled="false" 
		NodeEditingEnabled="false" 
		KeyboardEnabled="true" 
		CssClass="TreeView" 
		NodeCssClass="TreeNode" 
		SelectedNodeCssClass="SelectedTreeNode" 
		HoverNodeCssClass="HoverTreeNode" 
		NodeEditCssClass="NodeEdit" 
		LineImageWidth="19" 
		LineImageHeight="20" 
		DefaultImageWidth="16" DefaultImageHeight="16" ItemSpacing="0" NodeLabelPadding="3" 
		ParentNodeImageUrl="images/folders.gif" LeafNodeImageUrl="images/folder.gif" ShowLines="true" 
		LineImagesFolderUrl="images/lines/" 
		EnableViewState="true"  
		runat="server" >
        </ComponentArt:TreeView>
		</ComponentArt:PageView>
		
		
		
		
		
		
		
		
				<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Pageview2">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Modify Asset Field Permissions</div><br>

<table width=100%><tr>
<td width=100% style="FONT-SIZE: 11px; ">Role-Based Asset Field Permissions </td>
<td width=200px align=right></td>
</tr></table>	
		<ComponentArt:TreeView id="TreeviewAssetFields" Height="280" Width="100%" 
		AutoPostBackOnNodeCheckChanged="true"
		DragAndDropEnabled="false" 
		NodeEditingEnabled="false" 
		KeyboardEnabled="true" 
		CssClass="TreeView" 
		NodeCssClass="TreeNode" 
		SelectedNodeCssClass="SelectedTreeNode" 
		HoverNodeCssClass="HoverTreeNode" 
		NodeEditCssClass="NodeEdit" 
		LineImageWidth="19" 
		LineImageHeight="20" 
		DefaultImageWidth="16" DefaultImageHeight="16" ItemSpacing="0" NodeLabelPadding="3" 
		ParentNodeImageUrl="images/folders.gif" LeafNodeImageUrl="images/folder.gif" ShowLines="true" 
		LineImagesFolderUrl="images/lines/" 
		EnableViewState="true"  
		runat="server" >
        </ComponentArt:TreeView>
		</ComponentArt:PageView>
		
		
		
		
						<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Pageview3">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Modify User Field Permissions</div><br>

<table width=100%><tr>
<td width=100% style="FONT-SIZE: 11px; ">Role-Based User Field Permissions </td>
<td width=200px align=right></td>
</tr></table>	
		<ComponentArt:TreeView id="TreeviewUserFields" Height="280" Width="100%" 
		AutoPostBackOnNodeCheckChanged="true"
		DragAndDropEnabled="false" 
		NodeEditingEnabled="false" 
		KeyboardEnabled="true" 
		CssClass="TreeView" 
		NodeCssClass="TreeNode" 
		SelectedNodeCssClass="SelectedTreeNode" 
		HoverNodeCssClass="HoverTreeNode" 
		NodeEditCssClass="NodeEdit" 
		LineImageWidth="19" 
		LineImageHeight="20" 
		DefaultImageWidth="16" DefaultImageHeight="16" ItemSpacing="0" NodeLabelPadding="3" 
		ParentNodeImageUrl="images/folders.gif" LeafNodeImageUrl="images/folder.gif" ShowLines="true" 
		LineImagesFolderUrl="images/lines/" 
		EnableViewState="true"  
		runat="server" >
        </ComponentArt:TreeView>
		</ComponentArt:PageView>
		
		
		
		
		
		
		
		
	</ComponentArt:MultiPage>
			
			
			
			
			
			
			
			
			
			</div><!--sub tabs-->
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		</ComponentArt:PageView>
		<ComponentArt:PageView CssClass="PageContent" runat="server" ID="Category_Members">
<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Change permissions for this object by selecting a group or user from the list and adding to the active list.</div><br>
			Coming soon.
		</ComponentArt:PageView>
	</ComponentArt:MultiPage>
	<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
			<div style="text-align:right;"><br>
				<asp:Button id="btnSave" runat="server" Text="Save"></asp:Button><img src="images/spacer.gif" width=5 height=1><input type="button" onclick="window.close();" value="Close" />
			</div>
	</div>
	
	</div>
	</div>
	</div>		
			
		</form>
	</body>
</HTML>
