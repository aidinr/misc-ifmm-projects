<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AssetTagging.aspx.vb" Inherits="IDAM5.AssetTagging" ValidateRequest="False"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>Project</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
		<link href="treeStyle.css" type="text/css" rel="stylesheet">
		<link href="navStyle.css" type="text/css" rel="stylesheet">
		<link href="tabStripStyle.css" type="text/css" rel="stylesheet">
		<link href="navBarStyle.css" type="text/css" rel="stylesheet">
		<style>BODY { MARGIN: 0px }
	</style>
</HEAD>
	<body MS_POSITIONING="GridLayout" onload="this.focus">
    <script language="javascript" type="text/javascript">
    <!--
      // Forces the treeview to adjust to the new size of its container          


      // Forces the grid to adjust to the new size of its container          
      function resizeGrid(DomElementId, NewPaneHeight, NewPaneWidth)
      {
        GridAssets.Render();
      }  
      //-->
    </script>	
        <script type="text/javascript">

    // Ensure that folders are grouped together 
    function SortHandler(column, desc)
    {
      // multiple sort, giving the top priority to IsFolder
      GridAssets.SortMulti([4,!desc,column.ColumnNumber,desc]);

      // re-draw the grid
      GridAssets.Render();

      // cancel default sort
      return false;
    }
    
    
    function closealltags()
    {

	<% Response.Write(TagInformation.ClientID) %>.Collapse();
	<% Response.Write(TagUserDefinedFields.ClientID) %>.Collapse();
	<% Response.Write(TagKeywordServices.ClientID) %>.Collapse();
	<% Response.Write(TagKeywordMediaType.ClientID) %>.Collapse();
	<% Response.Write(TagKeywordIllustType.ClientID) %>.Collapse();
	<% Response.Write(TagSecurity.ClientID) %>.Collapse();

    }
    
    
    function openalltags()
    {

	<% Response.Write(TagInformation.ClientID) %>.Expand();
	<% Response.Write(TagUserDefinedFields.ClientID) %>.Expand();
	<% Response.Write(TagKeywordServices.ClientID) %>.Expand();
	<% Response.Write(TagKeywordMediaType.ClientID) %>.Expand();
	<% Response.Write(TagKeywordIllustType.ClientID) %>.Expand();
	<% Response.Write(TagSecurity.ClientID) %>.Expand();

    }    

function CheckAllItems()
    {
    var itemIndex = 0;
	for (var x = 0; x <= <% Response.Write(GridAssets.ClientID) %>.PageSize; x++)
		{
		 <% Response.Write(GridAssets.ClientID) %>.Select(<% Response.Write(GridAssets.ClientID) %>.Table.GetRow(x),true);
		}

      
      <% Response.Write(GridAssets.ClientID) %>.Render();
    }
    
function CopyTags(item)
{
doCopy = confirm("Copy tags?"); 
if (doCopy)
    {
		<% Response.Write(CALLBACKKeywordsServices.ClientID) %>.Callback(item);
		<% Response.Write(CALLBACKKeywordsIllustType.ClientID) %>.Callback(item);
		<% Response.Write(CALLBACKKeywordsMediaType.ClientID) %>.Callback(item);
		<% Response.Write(CALLBACKInformation.ClientID) %>.Callback(item);
		<% Response.Write(CALLBACKUDFMain.ClientID) %>.Callback(item);
		<% Response.Write(CALLBACKSecurityTag.ClientID) %>.Callback(item);
		
	}
	

}    
    </script>
 <SCRIPT LANGUAGE="JavaScript" SRC="js/CalendarPopup.js"></SCRIPT>
		<form id="Form" method="post" runat="server">
			<div style="PADDING-RIGHT:20px;PADDING-LEFT:20px;PADDING-BOTTOM:1px;PADDING-TOP:20px;POSITION:relative">
				<div>
					<div style="BORDER-RIGHT:1px solid; PADDING-RIGHT:10px; BORDER-TOP:1px solid; PADDING-LEFT:10px; PADDING-BOTTOM:1px; BORDER-LEFT:1px solid; PADDING-TOP:10px; BORDER-BOTTOM:1px solid; POSITION:relative; BACKGROUND-COLOR:white">
						<table>
							<tr>
								<td><img src="images/project34.gif"></td>
								<td>Tag Assets</td>
							</tr>
						</table>
						<div style="BORDER-RIGHT:#b7b4b4 1px solid; PADDING-RIGHT:5px; BORDER-TOP:#b7b4b4 1px solid; PADDING-LEFT:5px; FONT-WEIGHT:normal; FONT-SIZE:10px; PADDING-BOTTOM:5px; BORDER-LEFT:#b7b4b4 1px solid; COLOR:#3f3f3f; PADDING-TOP:5px; BORDER-BOTTOM:#b7b4b4 1px solid; FONT-FAMILY:verdana; BACKGROUND-COLOR:#e4e4e4">Tag 
							assets by selecting each asset from the listing below and setting the proper 
							attributes in the information section.&nbsp; Appending the tags means to keep 
							any existing information about the selected assets and adding any new 
							tags.&nbsp; Overwriting the tags means to remove any existing information about 
							the asset and replace with the new tags.&nbsp; Use the splitter window to 
							switch from full asset grid view to preview.&nbsp; You can edit asset 
							information, user defined fields, keywords and security settings from here.</div>
						<br>
						
						
						
						
						
						
						
						
						
						
						
						
		<ComponentArt:Splitter runat="server" id="Splitter1" fillheight="true" ImagesBaseUrl="images/" >
		
        <Layouts>
		   <ComponentArt:SplitterLayout>
            <Panes Orientation="Horizontal" SplitterBarCollapseImageUrl="splitter_horCol.gif" SplitterBarCollapseHoverImageUrl="splitter_horColHover.gif" SplitterBarExpandImageUrl="splitter_horExp.gif" SplitterBarExpandHoverImageUrl="splitter_horExpHover.gif" SplitterBarCollapseImageWidth="5" SplitterBarCollapseImageHeight="116" SplitterBarCssClass="HorizontalSplitterBarAssetTagging" SplitterBarCollapsedCssClass="CollapsedHorizontalSplitterBar" SplitterBarActiveCssClass="ActiveSplitterBar" SplitterBarWidth="5">
              <ComponentArt:SplitterPane PaneContentId="TreeViewContent" Width="300" MaxWidth="600" AllowScrolling="true" CssClass="SplitterPane" />
              <ComponentArt:SplitterPane Width="100%">
                <Panes Orientation="Vertical" SplitterBarCollapseImageUrl="splitter_verCol.gif" SplitterBarCollapseHoverImageUrl="splitter_verColHover.gif" SplitterBarExpandImageUrl="splitter_verExp.gif" SplitterBarExpandHoverImageUrl="splitter_verExpHover.gif" SplitterBarCollapseImageWidth="116" SplitterBarCollapseImageHeight="5" SplitterBarCssClass="VerticalSplitterBarAssetTagging" SplitterBarCollapsedCssClass="CollapsedVerticalSplitterBar" SplitterBarActiveCssClass="ActiveSplitterBar" SplitterBarWidth="5">
                  <ComponentArt:SplitterPane PaneContentId="ImagePreview" Height="200px" ClientSideOnResize="resizeGrid" CssClass="SplitterPane" /> 
                  <ComponentArt:SplitterPane PaneContentId="GridContent" Height="100%" MinHeight="150" ClientSideOnResize="resizeGrid" CssClass="DetailsPane" AllowScrolling="true" />             
                </Panes>
              </ComponentArt:SplitterPane>
            </Panes>
          </ComponentArt:SplitterLayout>  
        </Layouts>
        <Content>
          <ComponentArt:SplitterPaneContent id="TreeViewContent">
            <table class="HeadingCell" style="width:100%;height:25px;" cellpadding="0" cellspacing="0">
              <tr>
                <td class="HeadingCellText">Tags</td>
                <td  align=right style="FONT-SIZE: 10px; FONT-FAMILY: Verdana">[<a href="javascript:closealltags();">close all</a>] [<a href="javascript:openalltags();">open all</a>]</td>
              </tr>
            </table>
           
            
            <div style="padding:5px;padding-right:10px;">
            <div id="LeftColumn" >
							<ComponentArt:Snap id="TagInformation" runat="server" width="400px" MustBeDocked="false" DockingStyle="TransparentRectangle" CurrentDockingIndex="0" DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false" AutoCallBackOnExpand="false">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagInformation.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
												<td onmousedown="TagInformation.StartDragging(event);" onmouseup="explorecarousel();">Information</td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagInformation.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
												<td onmousedown="TagInformation.StartDragging(event);">Information</td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">




<COMPONENTART:CALLBACK id="CALLBACKInformation" runat="server" CacheContent="false">
											<CONTENT>
<asp:PlaceHolder ID=PlaceholderTagInformation runat=server >
Name:<br>
<asp:TextBox ID="name" TextMode=SingleLine CssClass="InputFieldMain" style="width:90%" runat="server" value=""></asp:TextBox>
<!--<input id="nametmp" type=text class="InputFieldMain" style="width:90%" value="">-->
Description:<br>
<asp:TextBox id="description" TextMode=MultiLine Rows=4 cssclass="InputFieldMain" runat="server" style="width:90%;height:90px"></asp:TextBox>
<!--<textarea id="description" class="InputFieldMain"  style="width:90%;height:90px"></textarea>-->
</asp:PlaceHolder>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div><!--carouselshortlist-->
</div><!--SnapTreeview-->
								</Content>
							</ComponentArt:Snap>
<div  class="SnapHeaderProjectsFiller"></div>	
							<ComponentArt:Snap id="TagSecurity" runat="server"  MustBeDocked="false" DockingStyle="TransparentRectangle" CurrentDockingIndex="1" DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false" AutoCallBackOnExpand="false">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagSecurity.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagSecurity.StartDragging(event);" onmouseup="explorecarousel();">Security</td></tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagSecurity.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagSecurity.StartDragging(event);">Security</td></tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>

									<div class="SnapTreeviewTag" style="background-color:white;">	
									<div class="carouselshortlist">
									
										<COMPONENTART:CALLBACK id="CALLBACKSecurityTag" runat="server" CacheContent="false">
											<CONTENT>
												<asp:PlaceHolder ID="PlaceholderSecurityTag" runat="server" >
													Security Level:<br>
													<asp:Literal ID="LiteralSecurityLevelDropdown" Runat=server></asp:Literal><br>
													<asp:CheckBox id="CheckBoxSecurityTag" runat="server" Checked="False"></asp:CheckBox>Force security level change
												</asp:PlaceHolder>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>

									</div><!--carouselshortlist-->
									</div><!--SnapTreeview-->


								</Content>
							</ComponentArt:Snap>		
<div  class="SnapHeaderProjectsFiller"></div>
<ComponentArt:Snap id="TagUserDefinedFields" runat="server"  MustBeDocked="false" DockingStyle="TransparentRectangle" CurrentDockingIndex="2" DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false" AutoCallBackOnExpand="false">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagUserDefinedFields.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagUserDefinedFields.StartDragging(event);" onmouseup="explorecarousel();">UDF</td></tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagUserDefinedFields.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagUserDefinedFields.StartDragging(event);">UDF</td></tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">


<COMPONENTART:CALLBACK id="CALLBACKUDFMain" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="Literal_UDFMAIN" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>


</div><!--carouselshortlist-->
</div><!--SnapTreeview-->



								</Content>
							</ComponentArt:Snap>
<div  class="SnapHeaderProjectsFiller"></div>	
							<ComponentArt:Snap id="TagKeywordServices" width="400px" runat="server"  MustBeDocked="false" DockingStyle="TransparentRectangle" CurrentDockingIndex="3" DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false" AutoCallBackOnExpand="false">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagKeywordServices.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagKeywordServices.StartDragging(event);" onmouseup="explorecarousel();">Keyword Services</td></tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagKeywordServices.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagKeywordServices.StartDragging(event);">Keyword Services</td></tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">
<asp:Literal ID="LiteralKeywords1Title" Runat=server></asp:Literal>:<br><img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=Services','Keywords_Services','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsServices" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="LiteralKeywords1" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div><!--carouselshortlist-->
</div><!--SnapTreeview-->



								</Content>
							</ComponentArt:Snap>
<div  class="SnapHeaderProjectsFiller"></div>	
							<ComponentArt:Snap id="TagKeywordMediaType" width="400px"  runat="server" MustBeDocked="false" DockingStyle="TransparentRectangle" CurrentDockingIndex="4" DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false" AutoCallBackOnExpand="false">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagKeywordMediaType.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagKeywordMediaType.StartDragging(event);" onmouseup="explorecarousel();">Keywords Media Type</td></tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagKeywordMediaType.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagKeywordMediaType.StartDragging(event);">Keywords Media Type</td></tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">
<asp:Literal ID="LiteralKeywords2Title" Runat=server></asp:Literal>:<br><img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=MediaType','Keywords_MediaType','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsMediaType" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="LiteralKeywords2" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
</div><!--carouselshortlist-->
</div><!--SnapTreeview-->



								</Content>
							</ComponentArt:Snap>
							
		
<div  class="SnapHeaderProjectsFiller"></div>	
							<ComponentArt:Snap id="TagKeywordIllustType"  width="400px" runat="server"  MustBeDocked="false" DockingStyle="TransparentRectangle" CurrentDockingIndex="5" DraggingStyle="GhostCopy" CurrentDockingContainer="LeftColumn" DockingContainers="LeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false" AutoCallBackOnExpand="false">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagKeywordIllustType.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagKeywordIllustType.StartDragging(event);" onmouseup="explorecarousel();">Keywords Illustration Type</td></tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												
												<td width="10" style="cursor: hand" align="right"><img onclick="TagKeywordIllustType.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
											<td onmousedown="TagKeywordIllustType.StartDragging(event);">Keywords Illustration Type</td></tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									
<div class="SnapTreeviewTag" style="background-color:white;">	
<div class="carouselshortlist">

<asp:Literal ID="LiteralKeywords3Title" Runat=server></asp:Literal>:<br><img src="images/spacer.gif" width=1 height=4 border="0"><br><img onclick="javascript:Keyword_PopUp('Keywords.aspx?Keyword=IllustType','Keywords_IllustType','650','570');" src="images/editbutton.gif"  border="0">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsIllustType" runat="server" CacheContent="false">
											<CONTENT>

<asp:Literal ID="LiteralKeywords3" Runat=server></asp:Literal>

											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>

</div><!--carouselshortlist-->
</div><!--SnapTreeview-->



								</Content>
							</ComponentArt:Snap>
							
		
							
		
<div  class="SnapHeaderProjectsFiller"></div>	

            </div><!--leftcolumn-->
            </div><!--padding-->
          </ComponentArt:SplitterPaneContent>
          
          
          <ComponentArt:SplitterPaneContent id="ImagePreview"> 
                         <table class="HeadingCell" style="width:100%;height:25px;" cellpadding="0" cellspacing="0">
              <tr>
                <td class="HeadingCellText">Image Preview</td>
              </tr>
            </table>
									<div style="padding:0px;overflow:hidden;padding:5px;background-color:white;">
										<COMPONENTART:CALLBACK id="AssetImage" runat="server" CacheContent="false">
											<CONTENT>
											<asp:Placeholder ID=assetpreview Runat=server>
												<TABLE height="100" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px" width=10>
																	<!--<asp:Literal ID="CurrentImageName" Runat=server></asp:Literal><br>-->
																	<asp:Image id="CurrentImage" runat="server" ImageUrl="images/spacer.gif"></asp:Image></TD>
																	<td width=100% valign=top >
																	<div style= "PADDING-LEFT: 5px; FONT-SIZE: 8pt; FONT-FAMILY: Verdana">
<asp:Literal ID="LiteralAssetInfoSummary" Runat=server></asp:Literal></div>
																	</td>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
												</asp:PlaceHolder>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px">Loading...
																	</TD>
																	<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
									</div>
            
          </ComponentArt:SplitterPaneContent>

          <ComponentArt:SplitterPaneContent id="GridContent"> 
             
      

<div  style="padding:5px;">      
      
<font style="FONT-SIZE: 8pt; FONT-FAMILY: Verdana">[ <a href="javascript:CheckAllItems();">select all</a> ] </font><br>     											
<img src="images/spacer.gif" width=1 height=4><br>
<asp:Literal id="literalNoAssets" Text="No assets available" visible=false runat="server" />


<COMPONENTART:GRID id="GridAssets" AllowPaging="true" PagerStyle="Numbered" PageSize="200"  ClientSideOnSort="SortHandler" AllowHorizontalScrolling="true" ClientSideOnSelect="BrowseOnSingleClickAssets" RunningMode="Client" CssClass="Grid" ShowHeader="true" ShowSearchBox="true" GroupByTextCssClass="GroupByText" GroupingNotificationTextCssClass="GridHeaderText" ShowFooter="true"  ImagesBaseUrl="images/" Sort="Name" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ScrollBarWidth="16" ScrollPopupClientTemplateId="ScrollPopupTemplate" Width="100%" Height="100%" fillheight="true" runat="server">
<ClientTemplates>
<ComponentArt:ClientTemplate Id="EditTemplateAssets">
           <a href="javascript:DownloadAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> | <a href="javascript:AddToCarousel('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/3Drafts.gif" alt="Edit" border=0></a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="EditCommandTemplateAssets">
            <a href="javascript:editRow();">Update</a> 
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate Id="InsertCommandTemplateAssets">
            <a href="javascript:insertRow();">Insert</a> 
          </ComponentArt:ClientTemplate>   
          <ComponentArt:ClientTemplate Id="TypeImageTemplateAssets">
            <div style= "WIDTH: 50px; HEIGHT: 50px; "><img src="## DataItem.GetMember("mainimagesource").Value ##" border="0" ></div>
          </ComponentArt:ClientTemplate>    
          <ComponentArt:ClientTemplate Id="TypeIconTemplateAssets">
            <div style="TEXT-ALIGN: left"><img src="## DataItem.GetMember("imagesource").Value ##" border="0" >&nbsp;## DataItem.GetMember("media_type_name").Value ##</div>
          </ComponentArt:ClientTemplate>        
          <ComponentArt:ClientTemplate Id="LookupProjectTemplateAssets">
            <A href="javascript:CopyTags('## DataItem.ClientId ##');"><img border=0 src="images/copytags.gif"></a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="LookupCategoryTemplateAssets">
            <A href="javascript:GotoLocation('0','## DataItem.GetMember("projectid2").Value ##');">## DataItem.GetMember("projectname2").Value ##</a>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="LookupKeywordsTemplateAssets">
            <img border=0 src="images/moreinfo.gif" style="CURSOR: hand" alt="## DataItem.GetMember("search_keyword_new").Value ##">## DataItem.GetMember("search_keyword_new").Value ##
          </ComponentArt:ClientTemplate>           
          <ComponentArt:ClientTemplate Id="LookupUDFsTemplateAssets">
            <img border=0 src="images/moreinfo.gif" style="CURSOR: hand" alt="## DataItem.GetMember("search_ukeyword_new").Value ##">## DataItem.GetMember("search_ukeyword_new").Value ##
          </ComponentArt:ClientTemplate>                
          <ComponentArt:ClientTemplate Id="LookupCategoryIconTemplateAssets">
            <img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif">
          </ComponentArt:ClientTemplate>                    
<componentart:ClientTemplate ID="LoadingFeedbackTemplateAssets">
<table cellspacing="0" cellpadding="0" border="0">
   <tr>
     <td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
   </tr>
   </table>
</componentart:ClientTemplate>                                 
</ClientTemplates>

<Levels>
<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
<Columns>
<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeImageTemplateAssets" dataField="mainimagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCellPostings" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="52" FixedWidth="True" />
<ComponentArt:GridColumn DataCellCssClass="DataCell" Align="Center" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplateAssets" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="16" FixedWidth="True" />
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="false" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="false" DataCellClientTemplateId="LookupUDFsTemplateAssets" HeadingText="UDF's" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="search_ukeyword_new" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="false" DataCellClientTemplateId="LookupKeywordsTemplateAssets" HeadingText="Keywords" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="search_keyword_new" ></componentart:GridColumn>
<ComponentArt:GridColumn AllowEditing="false" DataCellClientTemplateId="TypeIconTemplateAssets" dataField="imagesource" DataCellCssClass="DataCell" HeadingText="Type" AllowGrouping="false" Width="110" />
<componentart:GridColumn DataCellCssClass="DataCell" Visible="False" HeadingText="Type" AllowEditing="false" Width="110" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Updated" AllowEditing="false" Width="110" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="update_date"></componentart:GridColumn>
<componentart:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Security Level" AllowEditing="false" Width="80" SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid2"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="mainimagesource"></componentart:GridColumn>

</Columns>
</componentart:GridLevel>
</Levels>
</COMPONENTART:GRID>
      
      
      
      
      
      
      
      
      

</div>      
      
      

      
      
      
      
      
      
      
      
      
      
      
      
      
          </ComponentArt:SplitterPaneContent>

         
        </Content>          
      </ComponentArt:Splitter>
      
    <div class="HeadingCell" style="WIDTH:100%;HEIGHT:25px" cellpadding="0" cellspacing="0">
              
            </div>  						
						
<br>
<div style="WIDTH: 100%;TEXT-ALIGN: left">
					<input type="button"  value="Append to Selected" id=Button1 name=Button1 runat="server">&nbsp;<input type="button"  value="Overwrite Selected" id=Button2 name=Button2 runat="server"></div><br><br>

						
						
						
						
						
						
						
						
						
						
						
						
					</div>
				</div>
				<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
					<div style="TEXT-ALIGN:right"><br>
					<input type="button" onclick="window.close();" value="Close" >
					</div>
				</div>
				
				
<input type=hidden id="typemod">				
				
<script>							
function BrowseOnSingleClickAssets(item)
  {
	var itemvaluetmp;
	var itemvaluenametmp;
	var itemvaluefiletypetmp;
	itemvaluetmp = item.GetMember('asset_id').Value;
	itemvaluenametmp = item.GetMember('name').Value;
	itemvaluefiletypetmp = item.GetMember('imagesource').Value;
		<% Response.Write(AssetImage.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);

	return true;
  }  
</script>	
				
				
				
				
				
				
			</div>
		</form>
	</body>
</HTML>
