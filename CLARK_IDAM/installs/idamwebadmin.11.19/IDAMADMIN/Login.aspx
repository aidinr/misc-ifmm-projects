<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Login.aspx.vb" Inherits="IDAM5.Login" validateRequest="True" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<HTML lang="EN">
	<HEAD>
		<title>IDAM Version 5.01</title>
		<style type="text/CSS" media="screen">
BODY { PADDING-RIGHT: 0px;   background-color:#f4f3f5;PADDING-LEFT: 0px; FONT-SIZE: 12px; PADDING-BOTTOM: 0px; MARGIN: 0px; COLOR: #6a6559; PADDING-TOP: 0px; FONT-FAMILY: Arial, Helvetica, sans-serif! important }
A { COLOR: #397bac; TEXT-DECORATION: none }
A:hover { COLOR: #003366 }
A IMG { BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none }
H1 { CLEAR: both; PADDING-RIGHT: 0px; DISPLAY: block; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-TOP: 0px }
H1 B { DISPLAY: none }
IFRAME#cp { MARGIN-TOP: 2px; MARGIN-BOTTOM: 10px }
FORM { PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none }
FIELDSET { PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none }
TABLE.rteBack { BACKGROUND: #d0d293; MARGIN-BOTTOM: 2px }
.rteImageRaised { BORDER-RIGHT: 1px outset; BORDER-TOP: 1px outset; BORDER-LEFT: 1px outset; BORDER-BOTTOM: 1px outset }
.rteImage:hover { BORDER-RIGHT: 1px outset; BORDER-TOP: 1px outset; BORDER-LEFT: 1px outset; BORDER-BOTTOM: 1px outset }
.rteImageLowered { BORDER-RIGHT: 1px inset; BORDER-TOP: 1px inset; BORDER-LEFT: 1px inset; BORDER-BOTTOM: 1px inset }
.rteImage:active { BORDER-RIGHT: 1px inset; BORDER-TOP: 1px inset; BORDER-LEFT: 1px inset; BORDER-BOTTOM: 1px inset }
.rteImage { BORDER-RIGHT: #d0d293 1px solid; BORDER-TOP: #d0d293 1px solid; BORDER-LEFT: #d0d293 1px solid; BORDER-BOTTOM: #d0d293 1px solid }
DIV#signin { background-color:white; CLEAR: both; BORDER-RIGHT: #4e493b 1px solid; PADDING-RIGHT:25px; BORDER-TOP: #4e493b 1px solid; MARGIN-TOP: 5px; DISPLAY: block; PADDING-LEFT: 25px; PADDING-BOTTOM: 5px; MARGIN-LEFT: auto; BORDER-LEFT: #4e493b 1px solid; WIDTH: 270px; MARGIN-RIGHT: auto; PADDING-TOP: 25px; BORDER-BOTTOM: #4e493b 1px solid }
DIV#admintext { CLEAR: both; PADDING-RIGHT:25px; MARGIN-TOP: 5px; DISPLAY: block; PADDING-LEFT: 25px; PADDING-BOTTOM: 5px; MARGIN-LEFT: auto; WIDTH: 270px; MARGIN-RIGHT: auto; PADDING-TOP: 25px;  }
FORM { PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none }
FIELDSET { PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none }
INPUT.bigbtn { CLEAR: both; BORDER-RIGHT: #7d7b39 1px solid; BORDER-TOP: #7d7b39 1px solid; MARGIN-TOP: 10px; DISPLAY: block; BACKGROUND: #d9d79c; BORDER-LEFT: #7d7b39 1px solid; WIDTH: 472px; COLOR: #623527; BORDER-BOTTOM: #7d7b39 1px solid }
INPUT.username { CLEAR: both; BORDER-RIGHT: #7d7b39 1px solid; PADDING-RIGHT: 2px; BORDER-TOP: #7d7b39 1px solid; MARGIN-TOP: 2px; DISPLAY: block; PADDING-LEFT: 2px; BACKGROUND: url(../images/username_on.gif) #e8e7c3 no-repeat center center; MARGIN-BOTTOM: 10px; BORDER-LEFT: #7d7b39 1px solid; WIDTH: 246px; COLOR: #623527; BORDER-BOTTOM: #7d7b39 1px solid; FONT-FAMILY: Arial, Helvetica, sans-serif }
INPUT.password { CLEAR: both; BORDER-RIGHT: #7d7b39 1px solid; PADDING-RIGHT: 2px; BORDER-TOP: #7d7b39 1px solid; MARGIN-TOP: 2px; DISPLAY: block; PADDING-LEFT: 2px; BACKGROUND: url(../images/password_on.gif) #e8e7c3 no-repeat center center; MARGIN-BOTTOM: 10px; BORDER-LEFT: #7d7b39 1px solid; WIDTH: 246px; COLOR: #623527; BORDER-BOTTOM: #7d7b39 1px solid; FONT-FAMILY: Arial, Helvetica, sans-serif }
INPUT.signinbtn { CLEAR: both; BORDER-RIGHT: #7d7b39 1px solid; BORDER-TOP: #7d7b39 1px solid; DISPLAY: block; BACKGROUND: #d9d79c; MARGIN-LEFT: auto; BORDER-LEFT: #7d7b39 1px solid; WIDTH: 150px; COLOR: #623527; MARGIN-RIGHT: auto; BORDER-BOTTOM: #7d7b39 1px solid }
SPAN.warn { BORDER-RIGHT: #5a86ac 1px solid; PADDING-RIGHT: 3px; BORDER-TOP: #5a86ac 1px solid; MARGIN-TOP: -5px; DISPLAY: block; PADDING-LEFT: 23px; BACKGROUND: url(../images/warn.gif) #cfddea no-repeat 3px center; MARGIN-BOTTOM: 10px; PADDING-BOTTOM: 3px; BORDER-LEFT: #5a86ac 1px solid; WIDTH: 224px; COLOR: #003366; PADDING-TOP: 4px; BORDER-BOTTOM: #5a86ac 1px solid }
DIV#container { PADDING-RIGHT: 3px; DISPLAY: block; PADDING-LEFT: 3px; BACKGROUND: url(../images/bg/main_bg.gif) #766f5f repeat-y center center; MARGIN-LEFT: auto; WIDTH: 752px; MARGIN-RIGHT: auto }
DIV#container DIV#header { CLEAR: both; DISPLAY: block; BACKGROUND: #fff; WIDTH: 752px; HEIGHT: 133px }
DIV#container DIV#header DIV#logo { DISPLAY: block; BACKGROUND: #fff; FLOAT: left; WIDTH: 248px; HEIGHT: 133px }
DIV#container DIV#header DIV#nav UL#mainnav { PADDING-RIGHT: 0px; DISPLAY: inline; PADDING-LEFT: 0px; LIST-STYLE-POSITION: inside; PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-TOP: 0px; LIST-STYLE-TYPE: none }
DIV#container DIV#header DIV#nav UL#mainnav LI { DISPLAY: block; FLOAT: left; BORDER-LEFT: #fff 3px solid; WIDTH: 81px; HEIGHT: 133px }
DIV#container DIV#header DIV#nav UL#mainnav LI A { DISPLAY: block; WIDTH: 81px; HEIGHT: 133px }
DIV#container DIV#header DIV#nav UL#mainnav LI A#home { BACKGROUND: url(../../images/nav/btn1_0.gif) no-repeat }
DIV#container DIV#header DIV#nav UL#mainnav LI A.active#home { BACKGROUND: url(../../images/nav/btn1_1.gif) no-repeat }
DIV#container DIV#header DIV#nav UL#mainnav LI A#realestate { BACKGROUND: url(../../images/nav/btn2_0.gif) no-repeat }
DIV#container DIV#header DIV#nav UL#mainnav LI A.active#realestate { BACKGROUND: url(../../images/nav/btn2_1.gif) no-repeat }
DIV#container DIV#header DIV#nav UL#mainnav LI A#about { BACKGROUND: url(../../images/nav/btn3_0.gif) no-repeat }
DIV#container DIV#header DIV#nav UL#mainnav LI A.active#about { BACKGROUND: url(../../images/nav/btn3_1.gif) no-repeat }
DIV#container DIV#header DIV#nav UL#mainnav LI A#community { BACKGROUND: url(../../images/nav/btn4_0.gif) no-repeat }
DIV#container DIV#header DIV#nav UL#mainnav LI A.active#community { BACKGROUND: url(../../images/nav/btn4_1.gif) no-repeat }
DIV#container DIV#header DIV#nav UL#mainnav LI A#faqs { BACKGROUND: url(../../images/nav/btn5_0.gif) no-repeat }
DIV#container DIV#header DIV#nav UL#mainnav LI A.active#faqs { BACKGROUND: url(../../images/nav/btn5_1.gif) no-repeat }
DIV#container DIV#header DIV#nav UL#mainnav LI A#user { BACKGROUND: url(../../images/nav/btn7_0.gif) no-repeat }
DIV#container DIV#header DIV#nav UL#mainnav LI A.active#user { BACKGROUND: url(../../images/nav/btn7_1.gif) no-repeat }
DIV#container DIV#header DIV#nav UL#mainnav LI A DIV { DISPLAY: none }
DIV#container DIV#midbar { CLEAR: both; PADDING-RIGHT: 17px; BORDER-TOP: #fff 3px solid; DISPLAY: block; PADDING-LEFT: 17px; FONT-SIZE: 11px; BACKGROUND: #d0d293 no-repeat right center; PADDING-BOTTOM: 6px; WIDTH: 718px; PADDING-TOP: 6px; BORDER-BOTTOM: #fff 3px solid }
DIV#container DIV#midbar DIV.clearhack { CLEAR: both; DISPLAY: block; FONT-SIZE: 1px; VISIBILITY: hidden; WIDTH: 718px; HEIGHT: 1px }
DIV#container DIV#midbar B { COLOR: #623527 }
DIV#container DIV#midbar A { COLOR: #623527 }
DIV#container DIV#midbar SPAN.left { FLOAT: left; PADDING-TOP: 1px; HEIGHT: 14px }
DIV#container DIV#midbar SPAN.right { DISPLAY: block; PADDING-LEFT: 18px; BACKGROUND: url(../images/infoicon.gif) no-repeat left center; FLOAT: right; PADDING-TOP: 1px; HEIGHT: 14px }
DIV#container DIV#content { CLEAR: both; DISPLAY: block; BACKGROUND: url(../../images/bg/content_bg.gif) #fff repeat-y; WIDTH: 752px; BORDER-BOTTOM: #fff 3px solid }
DIV#container DIV#content DIV#left { BORDER-RIGHT: #fff 3px solid; DISPLAY: block; FLOAT: left; WIDTH: 248px }
DIV#container DIV#content DIV#left DIV#subnav { CLEAR: both; PADDING-RIGHT: 0px; DISPLAY: block; PADDING-LEFT: 17px; BACKGROUND: url(../../images/bg/subnav_mainbg.gif) #592a1c repeat-y left bottom; PADDING-BOTTOM: 3px; MARGIN: 0px; WIDTH: 231px; PADDING-TOP: 0px }
DIV#container DIV#content DIV#left DIV#subnav UL { BORDER-RIGHT: #d0d293 10px solid; PADDING-RIGHT: 0px; BORDER-TOP: #d0d293 10px solid; PADDING-LEFT: 0px; BACKGROUND: url(../../images/bg/subnav_bg.gif) #d0d293; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-LEFT: #d0d293 10px solid; PADDING-TOP: 1px; BORDER-BOTTOM: #d0d293 9px solid }
DIV#container DIV#content DIV#left DIV#subnav UL LI { PADDING-RIGHT: 0px; PADDING-LEFT: 0px; LIST-STYLE-POSITION: outside; FONT-SIZE: 11px; BACKGROUND: #d0d293; PADDING-BOTTOM: 0px; MARGIN: 0px 0px 1px; COLOR: #645f53; PADDING-TOP: 0px; FONT-FAMILY: Arial, Helvetica, sans-serif; LIST-STYLE-TYPE: none }
DIV#container DIV#content DIV#left DIV#subnav UL LI A { PADDING-RIGHT: 0px; DISPLAY: block; PADDING-LEFT: 2px; PADDING-BOTTOM: 1px; MARGIN: 0px; WIDTH: 209px; COLOR: #645f53; PADDING-TOP: 3px; HEIGHT: 16px; TEXT-DECORATION: none }
DIV#container DIV#content DIV#left DIV#subnav UL LI A:hover { PADDING-RIGHT: 0px; DISPLAY: block; PADDING-LEFT: 2px; BACKGROUND: url(../../images/subnav_arrow.gif) #e3e5b4 no-repeat right center; PADDING-BOTTOM: 1px; MARGIN: 0px; WIDTH: 209px; COLOR: #592a1c; PADDING-TOP: 3px; HEIGHT: 16px; TEXT-DECORATION: none }
DIV#container DIV#content DIV#left DIV#subnav UL LI A.active { PADDING-RIGHT: 0px; DISPLAY: block; PADDING-LEFT: 2px; BACKGROUND: url(../../images/subnav_arrow.gif) #e3e5b4 no-repeat right center; PADDING-BOTTOM: 1px; MARGIN: 0px; WIDTH: 209px; COLOR: #592a1c; PADDING-TOP: 3px; HEIGHT: 16px; TEXT-DECORATION: none }
DIV#container DIV#content DIV#right { DISPLAY: block; FLOAT: right; PADDING-BOTTOM: 10px; WIDTH: 501px }
DIV#container DIV.clearhack { CLEAR: both; DISPLAY: block; FONT-SIZE: 1px; WIDTH: 752px; HEIGHT: 1px }
DIV#container DIV#content DIV#right DIV#contentbody { CLEAR: both; MARGIN-TOP: 9px; DISPLAY: block; BACKGROUND: #fff; MARGIN-LEFT: 14px; WIDTH: 472px; MARGIN-RIGHT: 15px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.hr { PADDING-RIGHT: 0px; DISPLAY: block; PADDING-LEFT: 0px; FONT-SIZE: 1px; BACKGROUND: url(../../images/bg/subnav_bg.gif) repeat-x 50% top; PADDING-BOTTOM: 0px; MARGIN: 5px 0px 10px; WIDTH: 471px; PADDING-TOP: 0px; WHITE-SPACE: nowrap; HEIGHT: 1px }
DIV#container DIV#content DIV#right DIV#contentbody SPAN.success { BORDER-RIGHT: #82861c 1px solid; PADDING-RIGHT: 3px; BORDER-TOP: #82861c 1px solid; DISPLAY: block; PADDING-LEFT: 3px; FONT-WEIGHT: bold; BACKGROUND: #e7e8c9; MARGIN-BOTTOM: 10px; PADDING-BOTTOM: 3px; BORDER-LEFT: #82861c 1px solid; WIDTH: 464px; COLOR: #6c3725; PADDING-TOP: 3px; BORDER-BOTTOM: #82861c 1px solid; TEXT-ALIGN: center }
DIV#container DIV#content DIV#right DIV#contentbody SPAN.error { BORDER-RIGHT: tomato 1px solid; PADDING-RIGHT: 3px; BORDER-TOP: tomato 1px solid; DISPLAY: block; PADDING-LEFT: 3px; FONT-WEIGHT: bold; BACKGROUND: #ffbc8f; MARGIN-BOTTOM: 10px; PADDING-BOTTOM: 3px; BORDER-LEFT: tomato 1px solid; WIDTH: 464px; COLOR: #741e01; PADDING-TOP: 3px; BORDER-BOTTOM: tomato 1px solid; TEXT-ALIGN: center }
DIV#container DIV#content DIV#right DIV#contentbody DIV.row { CLEAR: both; PADDING-RIGHT: 3px; DISPLAY: block; PADDING-LEFT: 3px; BACKGROUND: #eeede2; MARGIN-BOTTOM: 8px; PADDING-BOTTOM: 3px; WIDTH: 466px; PADDING-TOP: 3px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.activate { CLEAR: both; PADDING-RIGHT: 3px; DISPLAY: block; PADDING-LEFT: 3px; BACKGROUND: #e8e8e8; MARGIN-BOTTOM: 8px; PADDING-BOTTOM: 3px; WIDTH: 466px; PADDING-TOP: 3px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.activate A { COLOR: #727272 }
DIV#container DIV#content DIV#right DIV#contentbody DIV.activate A:hover { COLOR: #524f4f }
DIV#container DIV#content DIV#right DIV#contentbody DIV.row DIV.deleteitem { DISPLAY: block; FLOAT: left; WIDTH: 16px; HEIGHT: 16px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.row DIV.deleteitem A { DISPLAY: block; BACKGROUND: url(../images/delete_off.gif) no-repeat center center; WIDTH: 16px; HEIGHT: 16px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.row DIV.deleteitem A:hover { BACKGROUND: url(../images/delete_on.gif) no-repeat center center }
DIV#container DIV#content DIV#right DIV#contentbody DIV.row DIV.activateitem { DISPLAY: block; FLOAT: left; WIDTH: 16px; HEIGHT: 16px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.row DIV.activateitem A { DISPLAY: block; BACKGROUND: url(../images/activate_off.gif) no-repeat center center; WIDTH: 16px; HEIGHT: 16px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.row DIV.activateitem A:hover { BACKGROUND: url(../images/activate_on.gif) no-repeat center center }
DIV#container DIV#content DIV#right DIV#contentbody DIV.row DIV.edititem { DISPLAY: block; FLOAT: right; WIDTH: 445px; PADDING-TOP: 1px; TEXT-ALIGN: left }
DIV#container DIV#content DIV#right DIV#contentbody DIV.editrow { DISPLAY: none }
DIV#container DIV#content DIV#right DIV#contentbody DIV.row DIV.clearhack { CLEAR: both; DISPLAY: block; FONT-SIZE: 1px; VISIBILITY: hidden; WIDTH: 466px; HEIGHT: 1px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.articleimages { CLEAR: both; MARGIN-TOP: 10px; DISPLAY: block; MARGIN-BOTTOM: 2px; WIDTH: 472px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.articleimages DIV.articleimg { MARGIN-TOP: 8px; DISPLAY: block; FLOAT: left; WIDTH: 157px; HEIGHT: 206px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.articleimages DIV.featureimg { MARGIN-TOP: 8px; DISPLAY: block; FLOAT: left; WIDTH: 157px; HEIGHT: 92px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.articleimages DIV.articleimg A.img { CLEAR: both; DISPLAY: block; MARGIN: 0px auto 2px; WIDTH: 143px; HEIGHT: 109px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.articleimages DIV.featureimg A.img { CLEAR: both; DISPLAY: block; MARGIN: 0px auto 2px; WIDTH: 143px; HEIGHT: 88px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.articleimages DIV.articleimg DIV.imgdeldiv { DISPLAY: block; Z-INDEX: 999; WIDTH: 143px; POSITION: absolute; HEIGHT: 15px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.articleimages DIV.featureimg DIV.imgdeldiv { DISPLAY: block; Z-INDEX: 999; WIDTH: 143px; POSITION: absolute; HEIGHT: 15px }
UNKNOWN { WIDTH: 150px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.articleimages DIV.articleimg DIV.imgdeldiv A.imgdelete { DISPLAY: block; BACKGROUND: url(../images/deletefile_off.gif) no-repeat center center; FILTER: Alpha (Opacity: 80); FLOAT: right; WIDTH: 16px; HEIGHT: 16px; opacity: 0.8 }
DIV#container DIV#content DIV#right DIV#contentbody DIV.articleimages DIV.featureimg DIV.imgdeldiv A.imgdelete { DISPLAY: block; BACKGROUND: url(../images/deletefile_off.gif) no-repeat center center; FILTER: Alpha (Opacity: 80); FLOAT: right; WIDTH: 16px; HEIGHT: 16px; opacity: 0.8 }
DIV#container DIV#content DIV#right DIV#contentbody DIV.articleimages DIV.articleimg DIV.imgdeldiv A.imgdelete:hover { BACKGROUND: url(../images/deletefile_on.gif) no-repeat center center; FILTER: Alpha (Opacity: 100); opacity: 1 }
DIV#container DIV#content DIV#right DIV#contentbody DIV.articleimages DIV.featureimg DIV.imgdeldiv A.imgdelete:hover { BACKGROUND: url(../images/deletefile_on.gif) no-repeat center center; FILTER: Alpha (Opacity: 100); opacity: 1 }
DIV#container DIV#content DIV#right DIV#contentbody DIV.articleimages DIV.clearhack { CLEAR: both; DISPLAY: block; FONT-SIZE: 1px; VISIBILITY: hidden; WIDTH: 472px; HEIGHT: 1px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.agentimage { CLEAR: both; MARGIN-TOP: 2px; DISPLAY: block; FLOAT: left; MARGIN-BOTTOM: 5px; WIDTH: 143px; HEIGHT: 152px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.agentimage A.agentimgdel { DISPLAY: block; BACKGROUND: url(../images/deletefile_off.gif) no-repeat center center; FILTER: Alpha (Opacity: 80); FLOAT: right; WIDTH: 16px; HEIGHT: 16px; opacity: 0.8 }
DIV#container DIV#content DIV#right DIV#contentbody DIV.agentimage A.agentimgdel:hover { BACKGROUND: url(../images/deletefile_on.gif) no-repeat center center; FILTER: Alpha (Opacity: 100); opacity: 1 }
DIV#container DIV#content DIV#right DIV#contentbody DIV.clearhack { CLEAR: both; DISPLAY: block; FONT-SIZE: 1px; VISIBILITY: hidden; WIDTH: 472px; HEIGHT: 1px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.featurehome { DISPLAY: block; FLOAT: left; WIDTH: 236px; HEIGHT: 200px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.featurehome A { DISPLAY: block; MARGIN-LEFT: auto; WIDTH: 224px; BORDER-TOP-STYLE: none; MARGIN-RIGHT: auto; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 177px; BORDER-BOTTOM-STYLE: none }
DIV#container DIV#content DIV#right DIV#contentbody DIV.featurehome A IMG { MARGIN: 0px; WIDTH: 224px; HEIGHT: 177px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.featurehome A.active { BORDER-RIGHT: #623527 5px solid; BORDER-TOP: #623527 5px solid; OVERFLOW: hidden; BORDER-LEFT: #623527 5px solid; WIDTH: 214px; BORDER-BOTTOM: #623527 5px solid; HEIGHT: 167px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.featurehome A.active IMG { MARGIN-TOP: -5px; MARGIN-LEFT: -5px }
DIV#container DIV#content DIV#right DIV#contentbody DIV.featurehome B { CLEAR: both; MARGIN-TOP: 3px; DISPLAY: block; WIDTH: 226px; TEXT-ALIGN: center }
DIV#container DIV#content DIV#right DIV#contentbody FORM.resourcenav { PADDING-RIGHT: 0px; DISPLAY: block; PADDING-LEFT: 0px; FLOAT: right; PADDING-BOTTOM: 0px; MARGIN: -3px 0px 0px; PADDING-TOP: 0px; HEIGHT: 20px }
DIV#container DIV#content DIV#right DIV#contentbody FORM.resourcenav SELECT { WIDTH: 170px }
INPUT.quicklinktext { CLEAR: both; PADDING-RIGHT: 0px; DISPLAY: block; PADDING-LEFT: 0px; FONT-SIZE: 12px; BACKGROUND: #fcfbf9; MARGIN-BOTTOM: 5px; PADDING-BOTTOM: 0px; WIDTH: 445px; COLOR: #766f5f; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; FONT-FAMILY: Arial, Helvetica, sans-serif; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none }
INPUT.selected { BACKGROUND: #f7f6ed }
INPUT.quicklinkbutton { CLEAR: both; DISPLAY: block; FONT-SIZE: 11px; BACKGROUND: #a38479; FLOAT: right; PADDING-BOTTOM: 1px; WIDTH: 100px; COLOR: #fff; BORDER-TOP-STYLE: none; FONT-FAMILY: Arial, Helvetica, Sans-serif; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none }
INPUT.quicklinkbutton:hover { BACKGROUND: #6c3725 }
INPUT.longtext { CLEAR: both; BORDER-RIGHT: #7d7b39 1px solid; PADDING-RIGHT: 2px; BORDER-TOP: #7d7b39 1px solid; MARGIN-TOP: 2px; DISPLAY: block; PADDING-LEFT: 2px; BACKGROUND: #e8e7c3; MARGIN-BOTTOM: 10px; BORDER-LEFT: #7d7b39 1px solid; WIDTH: 466px; COLOR: #623527; BORDER-BOTTOM: #7d7b39 1px solid; FONT-FAMILY: Arial, Helvetica, sans-serif }
INPUT.shorttext { CLEAR: both; BORDER-RIGHT: #7d7b39 1px solid; PADDING-RIGHT: 2px; BORDER-TOP: #7d7b39 1px solid; MARGIN-TOP: 2px; DISPLAY: block; PADDING-LEFT: 2px; BACKGROUND: #e8e7c3; MARGIN-BOTTOM: 10px; BORDER-LEFT: #7d7b39 1px solid; WIDTH: 150px; COLOR: #623527; BORDER-BOTTOM: #7d7b39 1px solid; FONT-FAMILY: Arial, Helvetica, sans-serif }
INPUT.selected { BACKGROUND: #eeede2 }
TEXTAREA.textareasmall { CLEAR: both; BORDER-RIGHT: #7d7b39 1px solid; PADDING-RIGHT: 2px; BORDER-TOP: #7d7b39 1px solid; MARGIN-TOP: 2px; DISPLAY: block; PADDING-LEFT: 2px; FONT-SIZE: 12px; BACKGROUND: #e8e7c3; MARGIN-BOTTOM: 10px; BORDER-LEFT: #7d7b39 1px solid; WIDTH: 466px; COLOR: #623527; BORDER-BOTTOM: #7d7b39 1px solid; FONT-FAMILY: Arial, Helvetica, sans-serif; HEIGHT: 60px }
TEXTAREA.textareamedium { CLEAR: both; BORDER-RIGHT: #7d7b39 1px solid; PADDING-RIGHT: 2px; BORDER-TOP: #7d7b39 1px solid; MARGIN-TOP: 2px; DISPLAY: block; PADDING-LEFT: 2px; FONT-SIZE: 12px; BACKGROUND: #e8e7c3; MARGIN-BOTTOM: 10px; BORDER-LEFT: #7d7b39 1px solid; WIDTH: 466px; COLOR: #623527; BORDER-BOTTOM: #7d7b39 1px solid; FONT-FAMILY: Arial, Helvetica, sans-serif; HEIGHT: 120px }
TEXTAREA.textareacaption { CLEAR: both; BORDER-RIGHT: #7d7b39 1px solid; PADDING-RIGHT: 2px; BORDER-TOP: #7d7b39 1px solid; DISPLAY: block; PADDING-LEFT: 2px; FONT-SIZE: 10px; BACKGROUND: #e8e7c3; MARGIN-LEFT: auto; BORDER-LEFT: #7d7b39 1px solid; WIDTH: 137px; COLOR: #623527; MARGIN-RIGHT: auto; BORDER-BOTTOM: #7d7b39 1px solid; FONT-FAMILY: Arial, Helvetica, sans-serif; HEIGHT: 90px }
INPUT.inputfile { CLEAR: both; BORDER-RIGHT: #7d7b39 1px solid; PADDING-RIGHT: 2px; BORDER-TOP: #7d7b39 1px solid; MARGIN-TOP: 2px; DISPLAY: block; PADDING-LEFT: 2px; BACKGROUND: #e8e7c3; MARGIN-BOTTOM: 4px; BORDER-LEFT: #7d7b39 1px solid; WIDTH: 476px; COLOR: #623527; BORDER-BOTTOM: #7d7b39 1px solid; FONT-FAMILY: Arial, Helvetica, sans-serif }
SELECT { CLEAR: both; BORDER-RIGHT: #7d7b39 1px solid; BORDER-TOP: #7d7b39 1px solid; MARGIN-TOP: 2px; DISPLAY: block; FONT-SIZE: 12px; BACKGROUND: #e8e7c3; MARGIN-BOTTOM: 10px; BORDER-LEFT: #7d7b39 1px solid; COLOR: #623527; BORDER-BOTTOM: #7d7b39 1px solid; FONT-FAMILY: Arial, Helvetica, sans-serif }
INPUT.disabled { BORDER-RIGHT: #aaa 1px solid; BORDER-TOP: #aaa 1px solid; BACKGROUND: #ccc; BORDER-LEFT: #aaa 1px solid; COLOR: #aaa; BORDER-BOTTOM: #aaa 1px solid }
LABEL.checkbox { CLEAR: both; MARGIN-TOP: 2px; DISPLAY: block; MARGIN-BOTTOM: 10px }
</style>
	</HEAD>
	<body>
		<form id="iDAMClientLoginForm" method="post" runat="server">
			<!-- Header go ----------------------------------------------------------------------->
			<div style="BORDER-BOTTOM:#d8d7da 0px solid">
				<table id="table1" cellpadding="0" cellspacing="0" width="100%" border="0">
					<tr>
						<td width="180"><asp:image id="Image1" runat="server" ImageUrl="images/spacer.gif" Width="210" height="1"></asp:image><br>
							<asp:image id="Image2" runat="server" ImageUrl="images/LOGO_template.gif" DescriptionUrl="logo"></asp:image></td>
						<td width="100%" align="right">Version 5.01&nbsp;&nbsp;
						</td>
					</tr>
					<tr>
						<td colSpan="2"></td>
					</tr>
				</table>
			</div>
			<br>
			<img src="images/spacer.gif" width=1 height=140><br>
			<table align=center ><tr><td valign=top>
			
			
			<asp:PlaceHolder id="LoginComments" runat="server"></asp:PlaceHolder>

			
			</td><td>
			
			
			
			
			<div id="signin">

    <ComponentArt:Rotator id="Rotator1" runat="server" 
      Width="276" Height="80"
      RotationType="RandomSlide"
      XmlContentFile="login_images.xml">
    <SlideTemplate>
      <img alt="" src="images/<%# DataBinder.Eval(Container.DataItem, "QuoteImage") %>" style="border:none;" /><br />
    </SlideTemplate>
    </ComponentArt:Rotator><br>
    
    
					<fieldset>
					<% if (request.querystring("Login") = "False") then%>
					<br>
					<font face="Arial" size="1" color="#FF0000">Your login and/or password was incorrect.  Please try again or contact your system administrator.</font><br><br>
					<%end if%>
			Username:<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ErrorMessage="[!]" ControlToValidate="Login_Name"></asp:RequiredFieldValidator>
			<asp:TextBox ID="Login_Name" Runat="server" TextMode="SingleLine" CssClass="username"></asp:TextBox>
			Password:
			<asp:TextBox ID="Login_Password" Runat="server" TextMode="Password" CssClass="username"></asp:TextBox>
			Instance:<br><select size="1" name="SERVER">
									<option value="<%response.write (IDAMInstance)%>" selected><%response.write (IDAMInstanceName)%></option>
								</select>
			<br>			
			<asp:Button ID="loginbutton" Text="Login" Runat="server" CssClass="signinbtn"></asp:Button><br>								
								

								<center><input class="preferences" type="checkbox" name="rememberme" value="ON"> <font face="Arial" size="1" color="#333333">
									remember me</font></center><br>
							
						<input type="hidden" name="Encrypted_Password">
						<input type="hidden" name="Redirect_Loc" size="40">
						<input type="hidden" name="txtPassword" />
						<input type="hidden" name="txtLogin" />
						
						

		</fieldset>
		</div>
		
			
			</td></tr></table>
			
			
			

					
</form>




<SCRIPT Language="JavaScript">

function copyAndSubmit() {
    document.getElementById('txtLogin').value = document.getElementById('Login_Name').value;
    document.getElementById('txtPassword').value = document.getElementById('Login_Password').value;
    document.getElementById('txtInstanceID').value = document.getElementById('SERVER').value;
    document.getElementById('iDAMClientLoginForm').submit();
}

var valid = '<%=request.querystring("Login")%>';
var checkpassthru = '<%=checkCookie("RememberMe")%>';
var login = '<%=checkCookie("Login")%>';
var encpassword = '<%=checkCookie("Password")%>';

if (valid == '') { 

	if (checkpassthru == 'ON') {
	document.getElementById('Login_Name').value = login;
	document.getElementById('Login_Password').value = encpassword;
	document.forms[0].elements[6].value = 'ON';
	document.forms[0].elements[6].checked = 1;
	document.forms[0].submit();
	//copyAndSubmit();
	}
}
</script>
	</body>

</HTML>
