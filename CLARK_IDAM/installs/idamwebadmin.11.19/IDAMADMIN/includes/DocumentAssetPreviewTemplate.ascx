<%@ Control Language="vb" AutoEventWireup="false" Codebehind="DocumentAssetPreviewTemplate.ascx.vb" Inherits="IDAM5.DocumentAssetPreviewTemplate" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<table border="0" width="100%" id="">
	<tr>
		<td width=640 height=480 align="left" valign="top" nowrap>
			<COMPONENTART:CALLBACK id="MainAssetImageCallBack" runat="server" CacheContent="false">		 
				<CONTENT>
				<asp:Image BorderWidth=0 ID=MainAssetImage Runat=server ></asp:Image>
				</CONTENT>
				<LOADINGPANELCLIENTTEMPLATE>
					<img border="0" src="<%=IDAM5.BaseIDAMSession.Config.URLLocationPreview%>?id=<%response.write(request.querystring("Id"))%>&instance=<%=IDAM5.BaseIDAMSession.Config.IDAMInstance%>&type=asset&size=1&width=640&height=480&cache=1">
				</LOADINGPANELCLIENTTEMPLATE>
			</COMPONENTART:CALLBACK>
			<asp:Literal ID="LiteralFLVPlayer" Runat=server Visible=false></asp:Literal>


		</td>
		<td align="left" valign="top">
		<div style="padding:5px;">
		<asp:Literal ID="LiteralAssetSummary" Runat=server></asp:Literal>
		</div><!--padding 5px on all-->				
		</td>
	</tr>
	<tr>
		<td colspan="2" align="left" valign="top">
		
			<COMPONENTART:CALLBACK id="MainAssetPageCallBack" runat="server" CacheContent="false">
				<CONTENT>
					<asp:Literal ID="LiteralAssetPaging" Runat=server></asp:Literal>
				</CONTENT>
				<LOADINGPANELCLIENTTEMPLATE>
				Available page(s) for preview:  Loading...
				</LOADINGPANELCLIENTTEMPLATE>
			</COMPONENTART:CALLBACK>
			<%If IDAM5.Functions.getRole("EDIT_ASSETS", Session("Roles")) Then%>
		<br>Rotate: <a href="javascript:RotateAsset('<%=request.querystring("ID")%>','-90');"><img src="images/rotate_left.gif" border=0> left</a> <a href="javascript:RotateAsset('<%=request.querystring("ID")%>','90');"><img src="images/rotate_right.gif" border=0> right</a>
		<%end if%>
		</td>
	</tr>
</table>
<script language="javascript">

  function gotopage(tab)
  {

	// Get names
	var queryString = window.location.href.substring((window.location.href.indexOf('?') + 1));
	var array1 = queryString.split('&'); // Each array element is in format "fieldname=value"
	var IDValue;
	for(var i = 0; i < array1.length; i++){
		var tempArray = array1[i].split('='); // Separate fieldname and value
		if (tempArray[0]=='ID') {
			IDValue = tempArray[1]
		}
		if (tempArray[0]=='id') {
			IDValue = tempArray[1]
		}
		if (tempArray[0]=='Id') {
			IDValue = tempArray[1]
		}				
		//eval(tempArray[0] + " = \"" + tempArray[1] + "\"");
	}
  
  
  var newurl = 'IDAM.aspx?Page=Project&ID=' + IDValue + '&type=Project&subtype=' + tab.ID;
  //alert(newurl);
  window.location.href = newurl;
  return true;
  }
  
function getassetpage(PAGEID)
{
    <% Response.Write(MainAssetImageCallBack.ClientID) %>.Callback(PAGEID);
    <% Response.Write(MainAssetPageCallBack.ClientID) %>.Callback(PAGEID);
}
function openFull()
	{
	  flvplayer = document.getElementById("flvplayer");
	  flvplayer.Rewind();
	  //alert('hi');
	  //var flvObject = flvplayer.getVariables("myFLVPlayback");
	  //flvObject.Stop();
	  var fs = window.open( "flvplayerfs.aspx?asset_id=<%=request.querystring("id")%>&idam=getSmil.aspx&skinswf=clearOverAll.swf" ,"FullScreenVideo", "toolbar=no,width=" + screen.availWidth  + ",height=" + screen.availHeight + ",status=no,resizable=yes,fullscreen=yes,scrollbars=no");
	  fs.focus();
	}	
	 
function RotateAsset(asset_id,degrees)
{
<% Response.Write(MainAssetImageCallBack.ClientID) %>.Callback('ROTATE'+','+asset_id+','+degrees);	
    <% Response.Write(MainAssetPageCallBack.ClientID) %>.Callback('1');														
}
</script>