<%@ Page CodeBehind="UploadJava.aspx.vb" Language="vb" AutoEventWireup="false" Inherits="IDAM5.UploadJava" %>
<%@ Register TagPrefix="upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Upload</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../multipageStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../treeStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../navStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../tabStripStyle.css" type="text/css" rel="stylesheet">
		<LINK href="../../navBarStyle.css" type="text/css" rel="stylesheet">
		<style>
		</style>
	</HEAD>
	<body onload="this.focus" MS_POSITIONING="GridLayout">
		<script>
	var windowopener=window.opener;
		</script>
		<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; PADDING-TOP: 10px">
			<div style="WIDTH: 100%">
				<div style="BORDER-RIGHT: 1px solid; PADDING-RIGHT: 10px; BORDER-TOP: 1px solid; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; BORDER-LEFT: 1px solid; PADDING-TOP: 10px; BORDER-BOTTOM: 1px solid; POSITION: relative; BACKGROUND-COLOR: white">
					<div style="BORDER-RIGHT: #b7b4b4 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #b7b4b4 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: normal; FONT-SIZE: 10px; PADDING-BOTTOM: 5px; BORDER-LEFT: #b7b4b4 1px solid; COLOR: #3f3f3f; PADDING-TOP: 5px; BORDER-BOTTOM: #b7b4b4 1px solid; FONT-FAMILY: verdana; BACKGROUND-COLOR: #e4e4e4">Upload 
						assets to the following location:</div>
					<table>
						<tr>
							<td width="400">
								<div style="FONT-SIZE: 10px; WIDTH: 100%"><br>
									Upload asset to project: [ <b>
										<asp:label id="LabelProjectName" runat="server">Label</asp:label></b>]<br>
									Category: [ <b>
										<asp:label id="LabelCategoryName" runat="server">Label</asp:label></b>]<br>
									<asp:literal id="PARENT_ASSET" Visible="False" Runat="server"></asp:literal><br>
									<script src="script.js" type="text/javascript"></script>
									<form id="Form1" runat="server">
										<%if request.querystring("pa") = "" and Session("UseJavaUpload") = "1" then%>
										<input id="createsubfolders" checked onclick="javascript:setcreatesubfolders();" type="checkbox"
											name="createsubfolders">&nbsp;Create subfolders (When uploading folders 
										with subfolders)
										<%end if%>
										<input id="createsubfoldershidden" type="hidden" value="true" name="createsubfoldershidden">
										<br>
										<br>
										<script language="JavaScript">
    
    
<%
If sCategoryid = ""  Then
	if not IDAM5.SQLFunctions.CheckProjectOverride(sProjectID, Session("sUserID"), "UPLOAD", IDAM5.Functions.getRole("UPLOAD", Session("Roles"))) then%>
		alert ('You do not have permissions to add assets to this location.  Please contact your system administrator to add this capability.');
		window.close();<%
	end if
else
    if not IDAM5.SQLFunctions.CheckProjectFolderOverride(sCategoryid, Session("sUserID"), "UPLOAD", IDAM5.Functions.getRole("UPLOAD", Session("Roles")),sProjectID) then%>
		alert ('You do not have permissions to add assets to this location.  Please contact your system administrator to add this capability.');
		window.close();<%
	end if
End If%>
			
										</script>
										<%if Session("UseJavaUpload") = "1" then%>
										<script language="JavaScript">

    var _info = navigator.userAgent;
        var ie = (_info.indexOf("MSIE") > 0);
        var win = (_info.indexOf("Win") > 0);
        if(win)
        {
        
            if(ie)
            {

		    document.writeln('<object classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93"');
		    document.writeln('      width= "290" height= "290" id="rup"');
		    document.writeln('      codebase="<%=idam5.BaseIDAMSession.Config.URLPRefix%>java.sun.com/update/1.5.0/jinstall-1_5-windows-i586.cab#version=1,4,1">');
		    document.writeln('<param name="archive" value="dndplus.jar">');
		    document.writeln('<param name="code" value="com.radinks.dnd.DNDAppletPlus">');
		    document.writeln('<param name="name" value="Rad Upload Plus">');
           }
            else
            {
                document.writeln('<object type="application/x-java-applet;version=1.4.1"');
                document.writeln('width= "290" height= "290"  id="rup">');
                document.writeln('<param name="archive" value="dndplus.jar">');
                document.writeln('<param name="code" value="com.radinks.dnd.DNDAppletPlus">');
                document.writeln('<param name="name" value="Rad Upload Plus">');
                document.writeln('<param name="MAYSCRIPT" value="yes">');
            }
        }
        else
        {
            /* mac and linux */
            document.writeln('<applet ');
            document.writeln('              archive  = "dndplus.jar"');
            document.writeln('                      code     = "com.radinks.dnd.DNDAppletPlus"');
            document.writeln('                      name     = "Rad Upload Plus"');
            document.writeln('                      hspace   = "0"');
            document.writeln('                      vspace   = "0" MAYSCRIPT="yes"');
            document.writeln('                      width = "290"');
            document.writeln('                      height = "290"');
            document.writeln('                      align    = "middle" id="rup">');
        }

/******    BEGIN APPLET CONFIGURATION PARAMETERS   ******/
	
    document.writeln('<param name="max_upload" value="2000000">');
    document.writeln('<param name="message" value="<%=idam5.BaseIDAMSession.Config.URLPRefix%><%response.write(sBaseURL)%>/imagejava.html">');
    document.writeln('<param name="url" value="<%=idam5.BaseIDAMSession.Config.URLPRefix%><%response.write(sBaseURL)%>/includes/java/UploadJava.aspx?NeatUpload_PostBackID=<%=spPostBackID%>&projectid=<%response.write(request.querystring("projectid"))%>&categoryid=<%response.write(request.querystring("categoryid"))%>&objectid=<%response.write(request.querystring("objectid"))%>&pa=<%response.write(request.querystring("pa"))%>&ap=<%response.write(IDAM5.Functions.getRole("REQUIRE_APPROVAL",session("Roles")))%>&uid=<%response.write(session("sUserID"))%>">');
    document.writeln('<param name="message" value="<%=idam5.BaseIDAMSession.Config.URLPRefix%><%response.write(sBaseURL)%>/imagejava.html">');
    document.writeln('<param name="full_path" value="yes">');
    document.writeln('<param name="send_button" value="1">');
    document.writeln('<param name="queue" value="yes">');
    document.writeln('<param name="jsnotify" value="yes">');
    document.writeln('<param name="browse" value="yes">');
    document.writeln('<param name="browse_button" value="yes">');
    document.writeln('<param name="show_thumb" value="yes">');
    document.writeln('<param name="permission_denied" value="<%=idam5.BaseIDAMSession.Config.URLPRefix%><%response.write(sBaseURL)%>/includes/java/denied.html">');
    document.writeln('<param name="external_redir" value="<%=idam5.BaseIDAMSession.Config.URLPRefix%><%response.write(sBaseURL)%>/imagejava.html">');
    document.writeln('<param name="external_target" value="_top">');
    document.writeln('<param name="redirect_delay" value="1000">');


/******    END APPLET CONFIGURATION PARAMETERS     ******/
       if(win)
	   {
		  document.writeln('</object>');
	   }
	   else

	   {

		  document.writeln('</applet>');
	   }
    

    
    
    
										</script>
										<%else%>
										<%'non java upload%>
										Upload File(s)
										<br>
										<Upload:ProgressBar id="progressBar" runat="server" Triggers="btnUpload linkButton commandButton htmlInputButtonButton htmlInputButtonSubmit"></Upload:ProgressBar>
										<Upload:InputFile Class="InputFieldMain" id="InputFile1" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile2" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile3" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile4" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile5" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile6" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile7" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile8" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile9" runat="server"></Upload:InputFile>
										<br>
										<Upload:InputFile Class="InputFieldMain" id="Inputfile10" runat="server"></Upload:InputFile>
										<br>
										<asp:Button id="btnUpload" CssClass="InputButtonMain" runat="server" Text="Upload Assets"></asp:Button>
										<%end if%>
								</div>
							</td>
							<td vAlign="top" align="right">
								<br>
								<asp:Button id="Switch_Simple" CssClass="InputButtonMain" runat="server" Text="Switch upload type"></asp:Button><br>
								<IMG height="25" src="images/spacer.gif" width="1"><br>
								<div style="BORDER-RIGHT:#b7b4b4 1px solid;PADDING-RIGHT:8px;BORDER-TOP:#b7b4b4 1px solid;PADDING-LEFT:8px;FONT-WEIGHT:normal;FONT-SIZE:10px;PADDING-BOTTOM:8px;BORDER-LEFT:#b7b4b4 1px solid;COLOR:#3f3f3f;PADDING-TOP:8px;BORDER-BOTTOM:#b7b4b4 1px solid;FONT-FAMILY:verdana;BACKGROUND-COLOR:#e4e4e4;TEXT-ALIGN:left">To 
									upload an asset, click on the "BROWSE" button and choosing a file from the 
									browse dialog box. You may upload more than one asset by repeating the act as 
									many times as needed.
									<br>
									<br>
									If Java is enabled, then you may also upload assets by simply dragging the 
									files from your desktop or folder onto the gray box on the left.
									<br>
									<br>
									To begin the upload process, simply click the "SEND" or "Upload Assets" button 
									at the bottom of the list. A progress indicator will show once you begin the 
									upload progress.
									<br>
									<br>
									If you would like to upload a folder and create the assets in the sub folders 
									of the source, simply click the "Create subfolders" checkbox (Java upload 
									only).
									<br>
									<br>
									Note: Maximum file(s) size is 2Gb. The time it takes to upload the asset(s) 
									depends on your network connectivity. Remote access may greatly reduce upload 
									performance.<br>
									<br>
									Contact your system administrator for any additional help.
								</div>
							</td>
						</tr>
					</table>
					</FORM>
				</div>
			</div>
			<div style="PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 10px; TEXT-ALIGN: left; align: right">
				<div style="TEXT-ALIGN: right"><br>
					<IMG height="1" src="images/spacer.gif" width="5"><input onclick="window.close();" type="button" value="Close">
				</div>
			</div>
		</div>
		<script language="javascript">
		function setcreatesubfolders()
		{
		//alert(document.getElementById('createsubfolders').checked);
			if (document.getElementById('createsubfolders').checked)
			{
				
				document.getElementById('createsubfoldershidden').value=true;
			}else{
				document.getElementById('createsubfoldershidden').value=false;
			}
			//alert(document.Form1.createsubfoldershidden.value);
		}
		//alert(document.getElementById('createsubfoldershidden').value);
		document.getElementById('createsubfoldershidden').value=true;
		
		
		
		<%if request.querystring("closewindow") = "true" then%>
		
		alert("The upload was successfull.");
		if (getParameter("pa") != "null") {
			//alert('a');
			window.opener.NavigateToAsset2(getParameter("pa"));
		} else {
			if (getParameter("id") != "null") {
				if (getParameter("c") != "null") {
					window.opener.NavigateToProjectFolder2(getParameter("id"),getParameter("c"));
				} else {
					window.opener.NavigateToProjectFolder2(getParameter("id"),'');
				}
			}
		}
		window.close();
		
		<%end if%>
		
		
		
		</script>
		
	</body>
</HTML>
