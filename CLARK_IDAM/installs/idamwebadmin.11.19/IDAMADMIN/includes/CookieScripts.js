
function switchToListView() {
    setLastView('list');
    thumbnailview.style.display = "none";
	listview.style.display = "block"; 
}
function switchToThumbnailp() {
    setLastView ('thumb');	
    thumbnailviewp.style.display = "block";
	listviewp.style.display = "none";
}
function switchToListViewp() {
    setLastView('list');
    thumbnailviewp.style.display = "none";
	listviewp.style.display = "block"; 
}	  
var expDays = 30;
var exp = new Date(); 
exp.setTime(exp.getTime() + (expDays*24*60*60*1000));
function getViewSetting(){
var lastView = GetCookie('lastView');
if (lastView == null) {
SetCookie('lastView', 'list', exp);
}
return lastView;
}
function getPaneSize(){
var PaneSize = GetCookie('PaneSize');
if (PaneSize == null) {
SetCookie('PaneSize', '200', exp);
}
return PaneSize;
}
function getHPaneSize(){
var HPaneSize = GetCookie('HPaneSize');
if (HPaneSize == null) {
SetCookie('HPaneSize', '300', exp);
}
return PaneSize;
}
function getResultsSetting(){
var lastView = GetCookie('ResultslastView');
if (lastView == null) {
SetCookie('ResultslastView', 'project', exp);
}
return lastView;
}
function getAssetViewCommentsSetting(){
var lastView = GetCookie('CommentslastView');
if (lastView == null) {
SetCookie('CommentslastView', 'show', exp);
}
return lastView;
}
function getAssetViewRelatedSetting(){
var lastView = GetCookie('RelatedlastView');
if (lastView == null) {
SetCookie('RelatedlastView', 'show', exp);
}
return lastView;
}

function getAssetViewAssociatedSetting(){
var lastView = GetCookie('AssociatedlastView');
if (lastView == null) {
SetCookie('AssociatedlastView', 'show', exp);
}
return lastView;
}
function getAssetViewDetailsSetting(){
var lastView = GetCookie('DetailslastView');
if (lastView == null) {
SetCookie('DetailslastView', 'show', exp);
}
return lastView;
}
function getAssetViewCheckinSetting(){
var lastView = GetCookie('CheckinlastView');
if (lastView == null) {
SetCookie('CheckinlastView', 'show', exp);
}
return lastView;
}
function getAssetViewUserDefinedSetting(){
var lastView = GetCookie('UserDefinedlastView');
if (lastView == null) {
SetCookie('UserDefinedlastView', 'show', exp);
}
return lastView;
}
function getAssetViewPermissionsSetting(){
var lastView = GetCookie('PermissionslastView');
if (lastView == null) {
SetCookie('PermissionslastView', 'show', exp);
}
return lastView;
}
function getAssetViewKeywordsSetting(){
var lastView = GetCookie('KeywordslastView');
if (lastView == null) {
SetCookie('KeywordslastView', 'show', exp);
}
return lastView;
}
function getAssetViewProjectsumSetting(){
var lastView = GetCookie('ProjectsumlastView');
if (lastView == null) {
SetCookie('ProjectsumlastView', 'show', exp);
}
return lastView;
}
function getAdvancedSearchFilterSetting(){
var lastView = GetCookie('AdvancedFilterlastView');
if (lastView == null) {
SetCookie('AdvancedFilterlastView', 'show', exp);
}
return lastView;
}
function getTagSetting(){
var lastView = GetCookie('TagView');
if (lastView == null) {
SetCookie('TagView', 'show', exp);
}
return lastView;
}
function getListColumns(){
var lastView = GetCookie('ColumnslastView');
if (lastView == null) {
SetCookie('ColumnslastView', '3', exp);
}
return lastView;
}
function getListRows(){
var lastView = GetCookie('RowslastView');
if (lastView == null) {
SetCookie('RowslastView', '5', exp);
}
return lastView;
}
function getDefaultOrder(){
var lastView = GetCookie('OrderbylastView');
if (lastView == null) {
SetCookie('OrderbylastView', 'Ratings', exp);
}
return lastView;
}
function getDefaultOrderDirection(){
var lastView = GetCookie('OrderDirectionlastView');
if (lastView == null) {
SetCookie('OrderDirectionlastView', 'DESC', exp);
}
return lastView;
}
function getRememberMe(){
var lastView = GetCookie('RememberMelastView');
if (lastView == null) {
SetCookie('RememberMelastView', 'No', exp);
}
return lastView;
}
function getSearchSetting(){
var SearchType = GetCookie('SearchType');
if (SearchType == null) {
SetCookie('SearchType', 'asset', exp);
}
return SearchType;
}

function getLastView(){
var getLastView = GetCookie('lastView');
return getLastView;
}
function setLastView(lastView){
SetCookie ('lastView', lastView);
}
function setLastViewResults(lastView){
SetCookie ('ResultslastView', lastView, exp);
}

function setLastViewAVComments(lastView){
SetCookie ('CommentslastView', lastView, exp);
}
function setLastViewAVRelated(lastView){
SetCookie ('RelatedlastView', lastView, exp);
}
function setLastViewAVAssociated(lastView){
SetCookie ('AssociatedlastView', lastView, exp);
}
function setLastViewAVDetails(lastView){
SetCookie ('DetailslastView', lastView, exp);
}
function setLastViewAVCheckin(lastView){
SetCookie ('CheckinlastView', lastView, exp);
}
function setLastViewAVUserDefined(lastView){
SetCookie ('UserDefinedlastView', lastView, exp);
}
function setLastViewAVPermissions(lastView){
SetCookie ('PermissionslastView', lastView, exp);
}
function setLastViewAVKeywords(lastView){
SetCookie ('KeywordslastView', lastView, exp);
}
function setLastViewAVProjectsum(lastView){
SetCookie ('ProjectsumlastView', lastView, exp);
}
function setLastViewPAAdvancedFilter(lastView){
SetCookie ('AdvancedFilterlastView', lastView, exp);
}
function setLastViewTagFilter(lastView){
SetCookie ('TagView', lastView, exp);
}
function setLastViewListColumns(lastView){
SetCookie ('ColumnslastView', lastView, exp);
}
function setLastViewListRows(lastView){
SetCookie ('RowslastView', lastView, exp);
}
function setLastViewDefaultOrder(lastView){
SetCookie ('OrderbylastView', lastView, exp);
}
function setLastViewDefaultOrderDirection(lastView){
SetCookie ('OrderDirectionlastView', lastView, exp);
}
function setRememberMe(lastView){
SetCookie ('RememberMelastView', lastView, exp);
}
function setSearchSetting(SearchType){
SetCookie ('SearchType', SearchType, exp);
}
function setPaneSize(Size){
SetCookie ('PaneSize', Size, exp);
}
function setHPaneSize(Size){
SetCookie ('HPaneSize', Size, exp);
}
function getCookieVal (offset) {  
var endstr = document.cookie.indexOf (";", offset);  
if (endstr == -1)    
endstr = document.cookie.length;  
return unescape(document.cookie.substring(offset, endstr));
}
function GetCookie (name) {  
var arg = name + "=";  
var alen = arg.length;  
var clen = document.cookie.length;  
var i = 0;  
while (i < clen) {    
var j = i + alen;    
if (document.cookie.substring(i, j) == arg)      
return getCookieVal (j);    
i = document.cookie.indexOf(" ", i) + 1;    
if (i == 0) break;   
}  
return null;
}
function SetCookie (name, value) {  
//alert('here2' + name + value);
var argv = SetCookie.arguments;  
var argc = SetCookie.arguments.length;  
var expires = (argc > 2) ? argv[2] : null;  
var path = (argc > 3) ? argv[3] : null;  
var domain = (argc > 4) ? argv[4] : null;  
var secure = (argc > 5) ? argv[5] : false;  
document.cookie = name + "=" + escape (value) + 
((expires == null) ? "" : ("; expires=" + expires.toGMTString())) + 
((path == null) ? "" : ("; path=" + path)) +  
((domain == null) ? "" : ("; domain=" + domain)) +    
((secure == true) ? "; secure" : "");
//alert('here3' + name + escape (value) + GetCookie('lastView') + document.cookie);
}
function DeleteCookie (name) {  
var exp = new Date();  
exp.setTime (exp.getTime() - 1);  
var cval = GetCookie (name);  
document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
}
