<%@ Control Language="vb" AutoEventWireup="false" Codebehind="CookieScripts.ascx.vb"  TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script type="text/javascript" src="includes/CookieScripts.js"></script>
<SCRIPT LANGUAGE = "JavaScript">
      function switchToThumbnail() {
		  if (getLastView() == 'thumb') {
			//alert(getLastView() + 'settinglist');
			setLastView ('list');
		  } else {
			//alert(getLastView() + 'settingthumb');
			setLastView ('thumb');
		  }
		  //alert('set' + getLastView());
		  
          window.location.href = '<%response.write (Request.Url.PathAndQuery)%>';
      	  //thumbnailview.style.display = "block";
	      //listview.style.display = "none";
	  }
	 function switchToThumbnailSubmit() {
		  if (getLastView() == 'thumb') {
			//alert(getLastView() + 'settinglist');
			setLastView ('list');
		  } else {
			//alert(getLastView() + 'settingthumb');
			setLastView ('thumb');
		  }
		  //alert('set' + getLastView());
		  
          __doPostBack('__Page', '');
      	  //thumbnailview.style.display = "block";
	      //listview.style.display = "none";
	  }
	  

	  function switchView(view) {
		if (view == 'list') {
			setLastView ('list');
			document.getElementById('_ctl1__ctl0_SNAPProjectAssets_thumb_icon_GridAssets').src="images/icon_thumb_on.gif";
			document.getElementById('_ctl1__ctl0_SNAPProjectAssets_list_icon_GridAssets').src="images/icon_list_off.gif";
			executeViewCallback(view);
		  } else {
			setLastView ('thumb');
			document.getElementById('_ctl1__ctl0_SNAPProjectAssets_thumb_icon_GridAssets').src="images/icon_thumb_off.gif";
			document.getElementById('_ctl1__ctl0_SNAPProjectAssets_list_icon_GridAssets').src="images/icon_list_on.gif";
			executeViewCallback(view);
		  }
	  }
	  
	  
	  
</SCRIPT>
