<%@ Control Language="vb" AutoEventWireup="false" Codebehind="ProjectGeneral.ascx.vb" Inherits="IDAM5.ProjectGeneral" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<script language="javascript">
var xoffsetpopup
xoffsetpopup = -150
var yoffsetpopup 
yoffsetpopup = -500
</script>
<script language="javascript"  src="js/filesearchhover.js"/>
<style>
.projectcentertest
{
width:100%;
height:100%;
/*z-index:100;*/
/*padding-left:3px;
padding-right:3px;*/
}

</style>

<script type="text/javascript">
//<![CDATA[    ]

//]]>
</script>
<script language="javascript">


function loadgridthumbnail(imageobj,asset_id)
{
alert(imageobj);
//property finder debug
//for(var prop in imageobj)
//{
//alert(prop);
//}
var urlstring;
urlstring = '<%=IDAM5.BaseIDAMSession.Config.URLLocationPreview%>?id=' + asset_id + '&instance=<%response.write (IDAMInstance)%>&type=asset&size=2&height=65&width=65';
//imageobj.src=urlstring;
//return urlstring;
}


function ToggleSnapMinimize(SnapObject, MenuItemIndex)
{
	//Force reload of page if trying to open a snap.  This is for performance reasons.
	//if (SnapObject.IsMinimized)
	//{
	//force submit here
	//document.getElementById('PageForm').submit();
	//} else
	//{
    SnapObject.ToggleMinimize();
    ToggleItemCheckedState(MenuItemIndex); 
    //}
   
    
}    
 
  
function ToggleItemCheckedState(MenuItemIndex)
{

    var item = _ctl1_MenuControl.Items(0).Items(MenuItemIndex); 
	if (item.GetProperty('Look-LeftIconUrl') == 'check.gif')
	{
		item.SetProperty('Look-LeftIconUrl','clear.gif');
		//set cookie to false
		SetCookie ('WindowProjectAssets' + MenuItemIndex, 0, exp);
		
	}
	else
	{
		item.SetProperty('Look-LeftIconUrl','check.gif');
		//set cookie to true
		SetCookie ('WindowProjectAssets' + MenuItemIndex, 1, exp);
	}
	item.ParentMenu.Render();
	
}


    

//setup form switch for delete or multi asset zip download
function DownloadAssets(ids){
	var sAssets;
	var downloadtype;
	sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
	ids = ids.replace('0 ','');
	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Download selected assets?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (arraylist == ''){
					arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
					} else {
					if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
					arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					}
					}
				}
				
				arraylist= arraylist.substring(1, arraylist.length);
				downloadtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				downloadtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	downloadtype = 'single';
	}
	if (downloadtype == 'multi') {
		if (arraylist != ''){
		<%=idam5.idam.downloadcheck_callback_clientID%>.Callback('multi,'+arraylist);		
		}
	}
	if (downloadtype == 'single') {
	/*assume single*/
	<%=idam5.idam.downloadcheck_callback_clientID%>.Callback('single,'+ids);
	}

}
	
function PreviewOverlayOnSingleClickAssets(item)
  {
	var itemvaluetmp;
	var itemvaluenametmp;
	var itemvaluefiletypetmp;
	itemvaluetmp = item.GetMember('asset_id').Value;
	itemvaluenametmp = item.GetMember('name').Value;
	itemvaluefiletypetmp = item.GetMember('imagesource').Value;
	
	//}
	//if ( Response.Write(SnapProjectAssetOverlay.ClientID) %>.IsMinimized)
	//{
	// Response.Write(SnapProjectAssetOverlay.ClientID) %>.UnMinimize();
	//}
	// Response.Write(SnapProjectAssetOverlay.ClientID) %>.Expand();
	
	<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);
	
	return true;
  }  
  
  
  function RotateAsset(asset_id,degrees,aname,afiletype)
  {
  <% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(asset_id +',Asset'+','+aname+','+afiletype+','+degrees+','+'AssetRotate');
																
  }
  
  function PreviewOverlayOnThumbSingleClickAssets(aid,aname,afiletype,eventObject)
  {

	//if ( Response.Write(SnapProjectAssetOverlay.ClientID) %>.IsMinimized)
	//{
	// Response.Write(SnapProjectAssetOverlay.ClientID) %>.UnMinimize();
	//}
	<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(aid + ',Asset' + ',' + aname + ',' + afiletype);
	// Response.Write(SnapProjectAssetOverlay.ClientID) %>.FloatTo(eventObject.x+50, eventObject.y-300);
	
	//if ( Response.Write(SnapProjectAssetOverlay.ClientID) %>.IsCollapsed)
	//{
	//	 Response.Write(SnapProjectAssetOverlay.ClientID) %>.Expand();
	//}
	
	
	//return true;
  } 
  
  
  //var beenundocked - can use later
  //beenundocked=false;
  
function PreviewOverlayOnSingleClickAssetsFromIcon(RowId,eventObject)
  {
	try
	{
		
		var itemvaluetmp;
		var itemvaluenametmp;
		var itemvaluefiletypetmp;
		var item 
		itemRow = <% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(RowId);
		itemvaluetmp = itemRow.GetMember('asset_id').Value;
		itemvaluenametmp = itemRow.GetMember('name').Value;
		itemvaluefiletypetmp = itemRow.GetMember('imagesource').Value;
		

		//if ( Response.Write(SnapProjectAssetOverlay.ClientID) %>.IsMinimized)
		//{
		// Response.Write(SnapProjectAssetOverlay.ClientID) %>.UnMinimize();
		//}
		

		<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);
	
		//if (!beenundocked) {
		// Response.Write(SnapProjectAssetOverlay.ClientID) %>.FloatTo(eventObject.x+224, eventObject.y);	
			//beenundocked=true;
		//} else {
			//***********add this Response.Write(SnapProjectAssetOverlay.ClientID) %>.FloatTo(eventObject.x+224, eventObject.y);
		//}
		//if ( Response.Write(SnapProjectAssetOverlay.ClientID) %>.IsCollapsed)
		//{
		//	 Response.Write(SnapProjectAssetOverlay.ClientID) %>.Expand();
		//}
		//window.scroll(0,eventObject.y); 
		 
	}  //end try  
	catch(err)
	{
	txt="There was an error on this page.\n\n"
	txt+="Error description: " + err.description
	alert(txt);
	}
	
	//return true;
}
	 



function AssetPreview(Asset_ID,cid)
{
	//get state
	
	// Response.Write(SnapProjectAssetOverlay.ClientID) %>.Expand();

	//if ( Response.Write(SnapProjectAssetOverlay.ClientID) %>.IsMinimized)
	//{
	// Response.Write(SnapProjectAssetOverlay.ClientID) %>.UnMinimize();
	//}
	//response.write( SnapProjectAssetOverlay.ClientId)%>.Callback(cid);
}

//setup goto for grid nav
function GotoLocation(ctype,cid)
	{
	//if (ctype == '0') {
		window.location = 'IDAM.aspx?page=Project&Id=<%response.write (request.querystring("Id"))%>&type=project&c=' + cid;
	//} else{
	//	window.location = 'IDAM.aspx?page=Project&Id=<%response.write (request.querystring("Id"))%>&type=project';
	//}
	return true;
	}
	
	
	
  function onUpdate(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }


  function onInsert(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }



  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridAssets.ClientID) %>.Page(1); 
  }

  function onDelete(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }	
	
	
function editRow()
  {
    <% Response.Write(GridAssets.ClientID) %>.EditComplete();     
  }


function editGrid(RowId)
  {
    itemRow = <% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(RowId);
	itemvaluetmp = itemRow.GetMember('asset_id').Value;
    window.location = 'IDAM.aspx?page=Asset&ID=' + itemvaluetmp + '&type=asset'
  }

function CheckAllItems()
    {
    var itemIndex = 0;
	for (var x = 0; x <= <% Response.Write(GridAssets.ClientID) %>.PageSize; x++)
		{
		 <% Response.Write(GridAssets.ClientID) %>.Select(<% Response.Write(GridAssets.ClientID) %>.Table.GetRow(x),true);
		}

      
      <% Response.Write(GridAssets.ClientID) %>.Render();
    }
    
function MM_findObj(n, d) { //v3.0
var p,i,x; if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function CheckAllItemsThumbs() { //Set All checkboxes, if un-checked, check them.
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
var dummyobjectvalue = checks[0].value;
var firstobjectcheckedstate = document.getElementById('aselect_' + dummyobjectvalue);
var bcheck = firstobjectcheckedstate.checked

if (bcheck) {
bcheck = true;
} else {
bcheck = false;
}

for (i=0; i<checks.length; i++) {
MM_findObj('aselect_' + checks[i].value).checked = bcheck ;
highlightAssetToggleII(document.getElementById('aselect_' + checks[i].value),checks[i].value); 
}

}
}


//setup form switch for delete or multi asset zip download
function DownloadAssetsThumbs(ids){
	var arraylist;
	var i;
	var sAssets;
	var downloadtype;
	arraylist = '';
	ids = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
	if (arraylist.split(',').length > 2) {
			 if (confirm("Download selected assets?"))
			 {
				arraylist= arraylist.substring(1, arraylist.length);
				downloadtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				downloadtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	downloadtype = 'single';
	}
	if (downloadtype == 'multi') {
		if (arraylist != ''){
		<%=idam5.idam.downloadcheck_callback_clientID%>.Callback('multi,'+arraylist);		
		}
	}
	if (downloadtype == 'single') {
	/*assume single*/
	<%=idam5.idam.downloadcheck_callback_clientID%>.Callback('single,'+ids);
	}
}

  function onDeleteAsset(item)
  {

      //if (confirm("Delete this asset?"))
        return true; 
      //else
      //  return false; 

  }


  function deleteRow(ids)
  {

  //check to see if multi select
	var sAssets;
	var addtype;
	<%If bThumbnailView Then%>
    var arraylist;
	var i;
	arraylist = '';
	idstmp = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
	if (arraylist.split(',').length > 2) {
			 if (confirm("Add selected assets to carousel?"))
			 {
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				addtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	addtype = 'single';
	}
    <%else%>
          
       
       
    sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();

	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Delete selected assets?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (arraylist == ''){
						if (sAssets[i]!=null){
							arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
						}
					} else {
						if (sAssets[i]!=null){
							if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
								arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
							}
						}
					}
				}
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				addtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	addtype = 'single';
	}
    <%end if%>
	//alert(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(ids));
	if (addtype == 'multi') {
		if (arraylist != ''){
		
		
		//override filter call
		<%response.write( GridAssets.ClientId)%>.Filter("DELETE " + arraylist);
		<%response.write( GridAssets.ClientId)%>.Page(0);
		//return false;
		}
	}
	if (addtype == 'single') {
	/*assume single*/
      if (confirm("Delete this asset?")){
       <% Response.Write(GridAssets.ClientID) %>.Delete(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(ids));

        }
      else {
        }
	} 
  }



function getNameValue()
  {

    return 'New Name';
  }

  function setNameValue(DataItem)
  {
    var tb = document.getElementById("<% Response.Write(GridAssets.ClientID) %>_EditTemplate_0_3_txtImageUrl");
   tb.value = DataItem;
 return true;

  }


</script>

<script>
  function isdefined(variable)
  {
    return (typeof (window[variable]) == "undefined") ? false : true;
  }

  var timeoutDelay = 100;
  var nb = "&nbsp;"; // non-breaking space

  function buildPager(grid, pagerSpan, First, Last)
  {
    var pager = "";
    var mid = Math.floor(pagerSpan / 2);
    var startPage = grid.PageCount <= pagerSpan ? 0 : Math.max(0, grid.CurrentPageIndex - mid);
    // adjust range for last few pages
    if (grid.PageCount > pagerSpan)
    {
      startPage = grid.CurrentPageIndex < (grid.PageCount - mid) ? startPage : grid.PageCount - pagerSpan;
    }

    var endPage = grid.PageCount <= pagerSpan ? grid.PageCount : Math.min(startPage + pagerSpan, grid.PageCount);

    if (grid.PageCount > pagerSpan && grid.CurrentPageIndex > mid)
    {
      pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(0);return false;\">&laquo; First</a>" + nb + "..." + nb;
      pager += "<a href=\"#\" onclick=\"" + grid.Id + ".PreviousPage();return false;\">&lt;</a>" + nb;
    }

    for (var page = startPage; page < endPage; page++)
    {
      var showPage = page + 1;
      if (page == grid.CurrentPageIndex)
      {
        pager += showPage + nb;
      }
      else 
      {
        pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(" + page + ");return false;\">" + showPage + "</a>" + nb;
      }
    }

    if (grid.PageCount > pagerSpan && grid.CurrentPageIndex < grid.PageCount - mid)
    {
      pager += "<a href=\"#\" onclick=\"" + grid.Id + ".NextPage();return false;\">></a>" + nb + "..." + nb;
      pager += "<a href=\"#\" onclick=\"" + grid.Id + ".Page(" + (grid.PageCount - 1) + ");return false;\">Last &raquo;</a>" + nb;
    }

    return pager;
  }
  
  
  function buildPageXofY(grid, Page, of, items)
  {
    // Note: You can trap an empty Grid here to change the default "Page 1 of 0 (0 items)" text
    var pageXofY = Page + nb + "<b>" + (grid.CurrentPageIndex + 1) + "</b>" + nb + of + nb + "<b>" + (grid.PageCount) + "</b>" + nb + "(" + grid.RecordCount + nb + items + ")";

    return pageXofY;
  }


  function showCustomFooter()
  {
    var gridId = "<%response.write( GridAssets.ClientId)%>";
    if (isdefined(gridId))
    {
      var grid = <%response.write( GridAssets.ClientId)%>;

      var Page = "Page";
      var of = "of";
      var items = "items";

      var pagerSpan = 5; // should be at least 2
      var First = "First";
      var Last = "Last";
      var cssClass = "GridFooterText";

      var footer = buildPager(grid, pagerSpan, First, Last);
      document.getElementById("tdPager").innerHTML = "<div style='white-space:nowrap;display:inline;' class=\"" + cssClass + "\">" + footer + "</div>";

      footer = buildPageXofY(grid, Page, of, items);
      document.getElementById("tdIndex").innerHTML = "<div class=\"" + cssClass + "\">" + footer + "</div>";
    }
    else 
    {
      setTimeout("showCustomFooter();", timeoutDelay);
    }
  }

  function onPage(newPage)
  {
    // delay call so that Grid's client properties have their new values
    setTimeout("showCustomFooter();",timeoutDelay);

    return true;
  }

  function onLoad()
  {
    showCustomFooter();
  }
</script> 

<!--TABLE JUNK-->
<table width=100% ><tr><td width=100%><div id="assetpopuplocation" ></div></td><td><div id="testmenucontrollocation" ></div></td></tr></table>
<!--END TABLE JUNK-->
				<!--TABLE TOP-->
				<table width=100% cellpadding=0 cellspacing=0><tr><td width=200 align=left valign=top >
					<div id="projectright" class="projectcentertest">
						<div class="projectcentertest" style="padding-right:5px;">
						
							
							



					
							<!--SNAPProjectAssets-->
							<ComponentArt:Snap id="SNAPProjectAssets" runat="server" FillWidth="True"  Height="200px"  MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter" DockingContainers="projectcenter" 
								MinimizeDirectionElement="testmenucontrollocation" MinimizeDuration="300"
								MinimizeSlide="Linear">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td onmousedown="<% Response.Write(SNAPProjectAssets.ClientID) %>.StartDragging(event);"><b>Assets</b></td>
												<td width="15" style="cursor: hand" align="right" ><COMPONENTART:MENU id="MenuProjectAssets" runat="server" Orientation="Horizontal" CssClass="TopGroupExp"
												DefaultGroupCssClass="MenuGroup" SiteMapXmlFile="menuDataProjectAssets.xml" DefaultItemLookID="DefaultItemLook"
												DefaultGroupItemSpacing="1" ImagesBaseUrl="images/" Visible="False" EnableViewState="true" ExpandDelay="100" ExpandOnClick="true" ClientSideOnItemSelect="contextProjectAssetsMenuClickHandler">
												<ItemLooks>
													<componentart:ItemLook HoverCssClass="TopMenuItemHoverExp" LabelPaddingTop="2px" LabelPaddingRight="10px"
														LabelPaddingBottom="2px" ExpandedCssClass="TopMenuItemExpanded" LabelPaddingLeft="10px" LookId="TopItemLook"
														CssClass="TopMenuItemExp"></componentart:ItemLook>
													<componentart:ItemLook HoverCssClass="MenuItemHoverExp" LabelPaddingTop="3px" LeftIconHeight="18px" LabelPaddingRight="10px"
														LabelPaddingBottom="4px" ExpandedCssClass="MenuItemHoverExp" LabelPaddingLeft="10px" LeftIconWidth="20px"
														LookId="DefaultItemLook" CssClass="MenuItemExp"></componentart:ItemLook>
													<componentart:ItemLook LookId="BreakItem" CssClass="MenuBreak"></componentart:ItemLook>
												</ItemLooks>
											</COMPONENTART:MENU>
												</td><td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectAssets.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(4);" src="images/closebutton.gif"  border="0"></td><td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectAssets.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
																		</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td onmousedown="<% Response.Write(SNAPProjectAssets.ClientID) %>.StartDragging(event);"><b>Assets</b></td>
												<td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectAssets.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(4);" src="images/closebutton.gif"  border="0"></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectAssets.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									<div class="SnapProjectsWrapper">
										<div class="SnapProjects" >
											<img src="images/spacer.gif" width=188 height=1>
											<div class="assetOptions">
												Current Folder:&nbsp;&nbsp;<b><%response.write (spFolderName)%></b><%if IDAM5.Functions.getRole("UPLOAD",session("Roles")) then%>&nbsp;&nbsp;[ <a href="javascript:Object_PopUp_Upload('<%response.write (request.querystring("id"))%>','<%response.write (request.querystring("c"))%>');">upload here!</a> ]<%end if%>
<br>
												<img src="images/spacer.gif" width=1 height=5><br>
												<table cellpadding=0 cellspacing=0 width=100%><tr width=1><td><input type=hidden name=chksearchsubfoldersaction id=chksearchsubfoldersaction><input type=checkbox name=chksearchsubfolders id=chksearchsubfolders onclick="javascript:setSearchWithinCheckValueFromCheckbox();document.PageForm.submit();" <%if blnpchksearchsubfolders then response.write ("CHECKED")%>></td><td width=150 nowrap ><a onclick="javascript:setSearchWithinCheckValue();document.PageForm.submit();" href="#">search subfolders</a></td><td width=100% align=right nowrap  >[ <asp:HyperLink id=CheckAllItemsJavascriptFunction runat="server">select all</asp:HyperLink> ]<%If IDAM5.Functions.getRole("EDIT_ASSETS", Session("Roles")) Then%>&nbsp;[ <a href="javascript:opentagwindow();">tag assets</a> ]<%end if%></td></tr></table>
											</div>
											<img src="images/spacer.gif" width=1 height=10><br>
											<asp:Literal id="LiteralNoFilters" Text="No filters available" visible=false runat="server" />
	

		
		

											<!--SNAPProjectAssetsFiltersDock-->
											<div id=SNAPProjectAssetsFiltersDock>
												<ComponentArt:Snap id="SNAPProjectAssetsFilters" runat="server" FillWidth="True" FillHeight="True" Height="100%"  AutoCallBackOnCollapse="true" 
													AutoCallBackOnExpand="true" MustBeDocked="true" DockingStyle="TransparentRectangle" 
													DraggingStyle="GhostCopy" CurrentDockingContainer="SNAPProjectAssetsFiltersDock" DockingContainers="projectcenter,SNAPProjectAssetsFiltersDock" 
													MinimizeDirectionElement="testmenucontrollocation" MinimizeDuration="300" MinimizeSlide="Linear" CollapseDuration="300" ExpandDuration="300">
													<Header>
														<div style="CURSOR: move; width: 100%;">
															<table class="SnapHeaderProjectsFilter" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<tr>
																	<td width=5><img src="images/24find.gif"></td><td align=left ><b>Asset Filters</b><br><img src="images/spacer.gif" width=116 height=1></td>
																	<td width="15" style="cursor: hand" align="right" ></td><td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectAssetsFilters.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(3);" src="images/closebutton.gif"  border="0"></td><td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectAssetsFilters.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
																</tr>
															</table>
														</div>
													</Header>
													<CollapsedHeader>
														<div style="CURSOR: move; width: 100%;">
															<table class="SnapHeaderProjectsFilter" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<tr>
																	<td width=5><img src="images/24find.gif"></td><td align=left ><b>Asset Filters</b><br><img src="images/spacer.gif" width=151  height=1></td>
																	<td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectAssetsFilters.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(3);" src="images/closebutton.gif"  border="0"></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectAssetsFilters.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
																			border="0"></td>
																</tr>
															</table>
														</div>
														<img src="images/spacer.gif" width=1 height=10><br>
													</CollapsedHeader>
													<Content>
														<div class="SnapProjectsWrapper">
															<div class="SnapProjectsFilter" >	
																<div style="text-align:left;">
																	<!--No Assets warning-->
																	
																	
																	
																	
																	
																	
																	
											<COMPONENTART:CALLBACK id="CallbackAssetFilters" runat="server" CacheContent="false">
												<CONTENT>

																		<asp:Literal id="ltrlCategoryFilterHeading" Text="" runat="server" />
																			<ComponentArt:Menu id="MenuCategoryType" 
																			ScrollingEnabled="true"
																			Orientation="horizontal"
																			CssClass="TopGroupPaddingBottom5"
																			DefaultGroupCssClass="Group"
																			DefaultItemLookId="DefaultItemLook"
																			ScrollUpLookId="ScrollUpItemLook"
																			ScrollDownLookId="ScrollDownItemLook"
																			DefaultGroupItemSpacing="0"
																			Visible="False"
																			ExpandDelay="100"
																			ImagesBaseUrl="images/"
																			EnableViewState="false"
																			runat="server">
																			<ItemLooks>
																			<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="ItemFiltersPG" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="DefaultItemLookPopup" CssClass="ItemFiltersPopupPG" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="BoldItemLook" CssClass="Itembold" HoverCssClass="ItemHbold" ExpandedCssClass="ItemExpbold" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="ScrollUpItemLook" ImageUrl="scroll_up.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
																			<ComponentArt:ItemLook LookID="ScrollDownItemLook" ImageUrl="scroll_down.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
																			</ItemLooks>
																			</ComponentArt:Menu>
																		<asp:Literal id="ltrlCategoryFilterFooter" Text="" runat="server" />

																		<asp:Literal id="ltrlFileTypeFilterHeading" Text="" runat="server" />
																			<ComponentArt:Menu id="MenuFilterFileType" 
																			ScrollingEnabled="true"
																			Orientation="horizontal"
																			CssClass="TopGroupPaddingBottom5"
																			DefaultGroupCssClass="Group"
																			DefaultItemLookId="DefaultItemLook"
																			ScrollUpLookId="ScrollUpItemLook"
																			ScrollDownLookId="ScrollDownItemLook"
																			DefaultGroupItemSpacing="0"
																			Visible="False"
																			ExpandDelay="100"
																			ImagesBaseUrl="images/"
																			EnableViewState="false"
																			runat="server"
																			ClientSideOnItemSelect="onClickMenuFilterFileType">
																			<ItemLooks>
																			<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="ItemFiltersPG" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="DefaultItemLookPopup" CssClass="ItemFiltersPopupPG" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="BoldItemLook" CssClass="Itembold" HoverCssClass="ItemHbold" ExpandedCssClass="ItemExpbold" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="ScrollUpItemLook" ImageUrl="scroll_up.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
																			<ComponentArt:ItemLook LookID="ScrollDownItemLook" ImageUrl="scroll_down.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
																			</ItemLooks>
																			</ComponentArt:Menu>
																		<asp:Literal id="ltrlFileTypeFilterFooter" Text="" runat="server" />

																		<asp:Literal id="ltrlKeywordFilterHeading" Text="" runat="server" />
																			<ComponentArt:Menu id="MenuFilterServices" 
																			ScrollingEnabled="true"
																			Orientation="horizontal"
																			CssClass="TopGroupPaddingBottom5"
																			DefaultGroupCssClass="Group"
																			DefaultItemLookId="DefaultItemLook"
																			ScrollUpLookId="ScrollUpItemLook"
																			ScrollDownLookId="ScrollDownItemLook"
																			DefaultGroupItemSpacing="0"
																			Visible="False"
																			ExpandDelay="100"
																			ImagesBaseUrl="images/"
																			EnableViewState="false"
																			runat="server"
																			ClientSideOnItemSelect="onClickMenuFilterKeyword">
																			<ItemLooks>
																			<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="ItemFiltersPG" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="DefaultItemLookPopup" CssClass="ItemFiltersPopupPG" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="BoldItemLook" CssClass="Itembold" HoverCssClass="ItemHbold" ExpandedCssClass="ItemExpbold" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="ScrollUpItemLook" ImageUrl="scroll_up.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
																			<ComponentArt:ItemLook LookID="ScrollDownItemLook" ImageUrl="scroll_down.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
																			</ItemLooks>
																			</ComponentArt:Menu>
																		<asp:Literal id="ltrlKeywordFilterFooter" Text="" runat="server" />
																	
																		<asp:Literal id="ltrlMediaTypeFilterHeading" Text="" runat="server" />
																			<ComponentArt:Menu id="MenuFilterMediaType" 
																			ScrollingEnabled="true"
																			Orientation="horizontal"
																			CssClass="TopGroupPaddingBottom5"
																			DefaultGroupCssClass="Group"
																			DefaultItemLookId="DefaultItemLook"
																			ScrollUpLookId="ScrollUpItemLook"
																			ScrollDownLookId="ScrollDownItemLook"
																			DefaultGroupItemSpacing="0"
																			Visible="False"
																			ExpandDelay="100"
																			ImagesBaseUrl="images/"
																			EnableViewState="false"
																			runat="server"
																			ClientSideOnItemSelect="onClickMenuFilterMediaType">
																			<ItemLooks>
																			<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="ItemFiltersPG" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="DefaultItemLookPopup" CssClass="ItemFiltersPopupPG" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="BoldItemLook" CssClass="Itembold" HoverCssClass="ItemHbold" ExpandedCssClass="ItemExpbold" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="ScrollUpItemLook" ImageUrl="scroll_up.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
																			<ComponentArt:ItemLook LookID="ScrollDownItemLook" ImageUrl="scroll_down.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
																			</ItemLooks>
																			</ComponentArt:Menu>
																		<asp:Literal id="ltrlMediaTypeFilterFooter" Text="" runat="server" />
			
																		<asp:Literal id="ltrlIllustTypeFilterHeading" Text="" runat="server" />
																			<ComponentArt:Menu id="MenuFilterIllustType" 
																			ScrollingEnabled="true"
																			Orientation="horizontal"
																			CssClass="TopGroupPaddingBottom5"
																			DefaultGroupCssClass="Group"
																			DefaultItemLookId="DefaultItemLook"
																			ScrollUpLookId="ScrollUpItemLook"
																			ScrollDownLookId="ScrollDownItemLook"
																			DefaultGroupItemSpacing="0"
																			Visible="False"
																			ExpandDelay="100"
																			ImagesBaseUrl="images/"
																			EnableViewState="false"
																			runat="server"
																			ClientSideOnItemSelect="onClickMenuFilterIllustType">
																			<ItemLooks>
																			<ComponentArt:ItemLook LookID="DefaultItemLook" CssClass="ItemFiltersPG" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="DefaultItemLookPopup" CssClass="ItemFiltersPopupPG" HoverCssClass="ItemH" ExpandedCssClass="ItemExp" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="BoldItemLook" CssClass="Itembold" HoverCssClass="ItemHbold" ExpandedCssClass="ItemExpbold" LabelPaddingLeft="5" LabelPaddingRight="15" LabelPaddingTop="2" LabelPaddingBottom="2" />
																			<ComponentArt:ItemLook LookID="ScrollUpItemLook" ImageUrl="scroll_up.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
																			<ComponentArt:ItemLook LookID="ScrollDownItemLook" ImageUrl="scroll_down.gif" ImageWidth="15" ImageHeight="13" CssClass="ScrollItem" HoverCssClass="ScrollItemH" ActiveCssClass="ScrollItemA" />
																			</ItemLooks>
																			</ComponentArt:Menu>
																		<asp:Literal id="ltrlIllustTypeFilterFooter" Text="" runat="server" />  
																
																	
																
															</CONTENT>
												<LOADINGPANELCLIENTTEMPLATE>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD align="center">
																<TABLE cellSpacing="0" cellPadding="0" border="0">
																	<TR>
																		<TD style="FONT-SIZE: 10px">Loading...
																		</TD>
																		<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
													</TABLE>
												</LOADINGPANELCLIENTTEMPLATE>
											</COMPONENTART:CALLBACK>	
																
																<table  cellpadding=0 cellspacing =0><tr><td width="125" align=left nowrap><b>Filter Results:</b><img src="images/spacer.gif" width=5 height=1></td><td align=left width=250 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:150px;height:15px;padding:1px;"  onkeydown="javascript:if(event.keyCode==13) {DoSearchFilter(this.value); return false}"></td></tr></table>
																
																
																
																
																
																
																
																
																
																
																
																</div>																		
															</div>
														</div><!--SnapProjectsWrapper-->
														<img src="images/spacer.gif" width=1 height=10><br>
													</Content>
												</ComponentArt:Snap>
												<!--SNAPProjectAssetsFiltersDock-->
											</div>
											<!--SNAPProjectAssetsFiltersDock-->











<div class="SnapHeaderViewIcons" style="display:block;" >
<table cellSpacing="0" cellPadding="0" width="100%" >
	<tr>
		<td width=14><div style="padding:4px;">View</div></td>
		<td width=14><a href="#"><asp:Image onclick="javascript:switchView('list');" id="list_icon_GridAssets" runat="server"></asp:Image></a></td>
		<td width=25><div style="padding:4px;">|</div></td>
		<td width=14><a href="#"><asp:Image onclick="javascript:switchView('thumb');"  id="thumb_icon_GridAssets" runat="server"></asp:Image></a></td>
		<td width=100% align=right style="padding-right:3px;" ></td>		
	</tr>
</table>
</div>


<script>

function executeViewCallback(view)
{
if (view=='list') {
document.getElementById('assetGirdPagingHeading').style.display='block';
}else{
document.getElementById('assetGirdPagingHeading').style.display='none';
}
<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(view);
}
</script>
<style>
											div.thumbnailbox {float:left!important; width:220px!important; width:180px; height:320px!important; height:220px;}
											div.thumbnailboxsc {width:170px;height:220px;}
											div.asset_name      { width:164px; overflow: hidden;padding-left:5px;
																text-overflow-mode:ellipsis;font-size:8px }
											div.asset_date      { width:120px; overflow: hidden;padding-left:5px;
																text-overflow-mode:ellipsis;font-size:8px }  
											div.asset_filetype  { width:80px; overflow: hidden;
																text-overflow-mode:ellipsis;font-size:8px }                  
											div.asset_filesize  { width:60px; overflow: hidden;
																text-overflow-mode:ellipsis;font-size:8px }    
											div.asset_owner     { width:120px; overflow: hidden;padding-left:5px;
																text-overflow-mode:ellipsis;font-size:8px }    
											div.asset_links    {width:100%;padding-left:2px; height:4px;overflow:hidden;background-image: url(images/dottedline.gif);background-repeat:repeat-x;}                                                                           
											span.nowrap       { white-space : nowrap; }
											div.attributed-to { position: relative;left:8px }
											</style>	
 <div class="assetGirdPagingHeading" id="assetGirdPagingHeading" style="display:block;" >
	<div id="CustomFooter" style="width:100%;border-top:none;">
    <table width="100%">
      <tr>
        <td id="tdPager" align="left"></td>
        <td id="tdIndex" align="right"></td>
      </tr>
    </table>
  </div>
</div>																			
<COMPONENTART:CALLBACK id="CallbackTHUMBLIST" runat="server" CacheContent="false">
	<CONTENT>
	
	

											<!--GroupBy="categoryname ASC"-->
											<COMPONENTART:GRID 
												id="GridAssets" 
												runat="server" 
												AutoFocusSearchBox="false"
												AutoCallBackOnInsert="true"
												AutoCallBackOnUpdate="true"
												AutoCallBackOnDelete="true"
													ClientSideOnPage="onPage"
												ClientSideOnLoad="onLoad"
												pagerposition="2"
												ScrollBar="Off"
												ScrollTopBottomImagesEnabled="true"
												ScrollTopBottomImageHeight="2"
												ScrollTopBottomImageWidth="16"
												ScrollImagesFolderUrl="images/scroller/"
												ScrollButtonWidth="16"
												ScrollButtonHeight="17"
												ScrollBarCssClass="ScrollBar"
												ScrollGripCssClass="ScrollGrip"
												ClientSideOnInsert="onInsert"
												ClientSideOnUpdate="onUpdate"
												ClientSideOnDelete="onDeleteAsset"
												ClientSideOnCallbackError="onCallbackError"
												ScrollPopupClientTemplateId="ScrollPopupTemplate" 
												Sort="update_date desc"
												Height="10" Width="100%"
												LoadingPanelPosition="TopCenter" 
												LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
												EnableViewState="true"
												GroupBySortImageHeight="10" 
												GroupBySortImageWidth="10" 
												GroupBySortDescendingImageUrl="group_desc.gif" 
												GroupBySortAscendingImageUrl="group_asc.gif" 
												GroupingNotificationTextCssClass="GridHeaderText" 
												AlternatingRowCssClass="AlternatingRowCategory" 
												IndentCellWidth="22" 
												TreeLineImageHeight="19" 
												TreeLineImageWidth="20" 
												TreeLineImagesFolderUrl="images/lines/" 
												PagerImagesFolderUrl="images/pager/" 
												ImagesBaseUrl="images/" 
												PreExpandOnGroup="True" 
												GroupingPageSize="5" 
												PagerTextCssClass="GridFooterTextCategory" 
												PagerStyle="Numbered" 
												PageSize="20" 
												GroupByTextCssClass="GroupByText" 
												GroupByCssClass="GroupByCell" 
												FooterCssClass="GridFooter" 
												HeaderCssClass="GridHeader" 
												SearchOnKeyPress="true" 
												SearchTextCssClass="GridHeaderText" 
												AllowEditing="true" 
												AllowSorting="False"
												ShowSearchBox="false" 
												ShowHeader="false" 
												ShowFooter="true" 
												CssClass="Grid" 
												RunningMode="callback" 
												ScrollBarWidth="15" 
												AllowPaging="true" >
												<ClientTemplates>
												<ComponentArt:ClientTemplate Id="EditTemplate">
														<a href="javascript:DownloadAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> | <a href="javascript:AddToCarouselFromPG('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a> | <a href="javascript:editGrid('## DataItem.ClientId ##');"><img src="images/i_rate.gif" alt="Edit" border=0></a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
														</ComponentArt:ClientTemplate>
												<ComponentArt:ClientTemplate Id="EditTemplateNoDownload">
														<a href="javascript:AddToCarouselFromPG('## DataItem.ClientId ##');"><img src="images/8.gif" border=0 alt="Add to Carousel"></a>
														</ComponentArt:ClientTemplate>														
														<ComponentArt:ClientTemplate Id="EditCommandTemplate">
															<a href="javascript:editRow();">Update</a> 
														</ComponentArt:ClientTemplate>
														<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
															<a href="javascript:insertRow();">Insert</a> 
														</ComponentArt:ClientTemplate>    
														<ComponentArt:ClientTemplate Id="TypeIconTemplate">
															<img src="## DataItem.GetMember("imagesource").Value ##" border="0"  > 
														</ComponentArt:ClientTemplate>        
														<ComponentArt:ClientTemplate Id="LookupProjectTemplate">
														<div style="height:45;width=45;border:0px solid;"><img src="images/spacer.gif" width=1 height=45><A href="## DataItem.GetMember("alink").Value ##"><img border=0 src="## DataItem.GetMember("timage").Value ##" onmouseover="javascript:showtrailpreload('## DataItem.GetMember("asset_id").Value ##','## DataItem.GetMember("name").Value ##','','5','1',270,7);PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.ClientId ##',event);" onmouseout="javascript:hidetrail();" ></a></div>
														</ComponentArt:ClientTemplate> 
														<ComponentArt:ClientTemplate Id="LookupAssetPopupTemplate">
															<A href="#" onclick="javascript:PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.ClientId ##',event);"><img border=0 src="images/15printPreview.gif"></a>
														</ComponentArt:ClientTemplate> 
														<ComponentArt:ClientTemplate Id="LookupProjectTemplatePreview">
															<A href="javascript:AssetPreview('## DataItem.ClientId ##');"><img border=0 src="images/15printPreview.gif"></a>
														</ComponentArt:ClientTemplate>           
														<ComponentArt:ClientTemplate Id="LookupCategoryTemplate">
															<A href="javascript:GotoLocation('## DataItem.GetMember("ispost").Value ##','## DataItem.GetMember("category_id").Value ##');">## DataItem.GetMember("categoryname").Value ##</a>
														</ComponentArt:ClientTemplate>    
														<ComponentArt:ClientTemplate Id="LookupCategoryIconTemplate">
															<A href="javascript:GotoLocation('## DataItem.GetMember("ispost").Value ##','## DataItem.GetMember("category_id").Value ##');"><img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif"></a>
														</ComponentArt:ClientTemplate>    
														<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
														<table cellspacing="0" cellpadding="0" border="0">
														<tr>
															<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
														</tr>
														</table>
														</componentart:ClientTemplate>                                 
												</ClientTemplates>
												

												<Levels>
												<componentart:GridLevel EditCellCssClass="EditDataCell"
															EditFieldCssClass="EditDataField"
															EditCommandClientTemplateId="EditCommandTemplate"
															InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
												<Columns>
												
												<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" Align="Center" AllowSorting="false" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplate" HeadingCellCssClass="FirstHeadingCell" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="45" FixedWidth="True" />
												<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" AllowSorting="false" dataField="imagesource"  DataCellCssClass="DataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="19" FixedWidth="True" />
												<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="True" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
												<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Type" AllowEditing="false" Width="40" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
												<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Updated" AllowEditing="false" Width="110"  FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  DataField="update_date"></componentart:GridColumn>
												<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Security Level" AllowEditing="true" Width="80"  SortedDataCellCssClass="SortedDataCell" DataField="securitylevel_id" ForeignTable="SecurityLevel" ForeignDataKeyField="securitylevel_id" ForeignDisplayField="securitylevelname" ></componentart:GridColumn>
												<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="User" AllowEditing="false" Width="110"  SortedDataCellCssClass="SortedDataCell"  DataField="userid" ForeignTable="Users" ForeignDataKeyField="userid" ForeignDisplayField="FullName"></componentart:GridColumn>
												<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="False" DataCellClientTemplateId="LookupCategoryIconTemplate" HeadingText=" " HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" Align="Right" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="ispost" Width="19" FixedWidth="True"></componentart:GridColumn>
												<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="True" HeadingText="Folder" Width="144" AllowGrouping="True" SortedDataCellCssClass="SortedDataCell" DataField="category_id" ForeignTable="Category" ForeignDataKeyField="category_id" ForeignDisplayField="categoryname" ></componentart:GridColumn>
												<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowEditing="false" AllowSorting="false" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Width="150"  Align="Center" />
												<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" Visible="False" HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplateNoDownload" EditControlType="EditCommand" Width="150"  Align="Center" />
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="category_id" SortedDataCellCssClass="SortedDataCell" DataField="category_id"></componentart:GridColumn>
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="ispost" SortedDataCellCssClass="SortedDataCell" DataField="ispost"></componentart:GridColumn>
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="timage" SortedDataCellCssClass="SortedDataCell" DataField="timage"></componentart:GridColumn>
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="URLPREFIXPREVIEW" SortedDataCellCssClass="SortedDataCell" DataField="URLPREFIXPREVIEW"></componentart:GridColumn>

												</Columns>
												</componentart:GridLevel>
												</Levels>
												<ServerTemplates>
												<ComponentArt:GridServerTemplate Id="PickerTemplate">
            <Template>

 	   <input id="txtImageUrl" type="text" value="<%# Container.DataItem("name") %>"/>


            </Template>
          </ComponentArt:GridServerTemplate>
												

												
												
												</ServerTemplates>
											</COMPONENTART:GRID>
											
											
											

<div style="padding:10px;"><asp:Literal id="literalNoAssets" Text="No assets available" visible=false runat="server" /></div>

											

											<COMPONENTART:CALLBACK id="ThumbnailCallBack" runat="server" CacheContent="false">
												<CONTENT>
													<!--hidden Thumbs-->
													<asp:Literal id="ltrlPager" Text="" runat="server" />
													<asp:Repeater id=Thumbnails runat="server" Visible="False">
														<ItemTemplate>
															<div valign="Top"  class="thumbnailbox" > 
																<div class="thumbnailboxsc"><!--Asset Thumbnails-->
																	<div align="left"><!--PADDING LEFT X1-->
																		<div align="left"><!--PADDING LEFT X2-->
																			<table  class="bluelightlight" border="0" cellspacing="0" width="15">
																				<tr>
																				<td  class="bluelightlight" valign="top">
																					<div align="left" id="assethighlight_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" style="border:1px white solid;">
																						<table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse"><tr><td>
																						<table border="0" width="100%"  background="#C0C0C0" bordercolor="#C0C0C0" style="border-collapse: collapse" height="120" width="160">
																						<tr class="bluelightlight" width="100%" align="center" height="120" width="160"><td class="bluelightlight" width="100%" height="120" width="160" align="center" valign="bottom" bordercolor="#808080" bordercolorlight="#808080" nowrap><img src="images\spacer.gif" height="0" width="159"><br><a id="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" href="javascript:highlightAssetToggleII(document.getElementById('aselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>'),<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);"><img alt="" border="0" src="<%#DataBinder.Eval(Container.DataItem, "timage")%>" onmouseover="javascript:showtrailpreload('<%#DataBinder.Eval(Container.DataItem, "asset_id")%>','<%#DataBinder.Eval(Container.DataItem, "name")%>','','5','1',270,7);PreviewOverlayOnThumbSingleClickAssets('<%#DataBinder.Eval(Container.DataItem, "asset_id")%>','<%# GetAssetProjectName(CType(DataBinder.Eval(Container.DataItem, "projectid"), String))%>','<%#DataBinder.Eval(Container.DataItem, "imagesource")%>',event);" onmouseout="javascript:hidetrail();" ></a></td><td><img src="images\spacer.gif" width="0" height="140"></td>
																						</tr>
																						</table></td></tr></table>
																					</div>
																				</td>
																				</tr>
																				<tr>
																				<td>      
																					<!--PADDING-->
																					<div style="padding:5px;">             
																						<table width="100%" cellspacing="0" cellpadding="0" >
																							<tr>
																								<td nowrap width="5"><img src="<%#DataBinder.Eval(Container.DataItem, "imagesource")%>"></td>
																								<td nowrap><div class="asset_name"><span class="nowrap"><b><font face="Verdana" size="1"><%#DataBinder.Eval(Container.DataItem, "name")%></font></b></span></div></td>
																								<td align="right" width="20"><font color="#808080" face="Arial" size="1"><input onclick="highlightAssetToggle(this,<%#DataBinder.Eval(Container.DataItem, "asset_id")%>);" class="preferences" type="checkbox" id="aselect_<%#DataBinder.Eval(Container.DataItem, "asset_id")%>" value="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"><input type=hidden id=selectassethidden value="<%#DataBinder.Eval(Container.DataItem, "asset_id")%>"></font></td>
																							</tr>
																						</table>
																						<table width="100%"  cellspacing="0" cellpadding="0" >
																							<tr>
																								<td nowrap width="100%" align="left"><div class="asset_filetype"><span class="nowrap"><font face="Verdana" size="1"><%# GetAssetFileTypeName(CType(DataBinder.Eval(Container.DataItem, "media_type"), String))%></font></span></div></td>
																							<td align="right"><div class="asset_filesize"><span class="nowrap"><font face="Verdana" size="1"><%# FormatSize(CType(DataBinder.Eval(Container.DataItem, "filesize"), Long))%></font></span></div></td>
																							</tr>
																						</table>
																						<table width="100%" cellspacing="0" cellpadding="0" >
																							<tr>
																								<td nowrap><font face="Verdana" size="1">Updated</font></td>
																								<td align="left" width="868"><div class="asset_date"><span class="nowrap"><font face="Verdana" size="1">

																							<%# String.Format("{0:MM/dd/yy hh:mm tt}", (DataBinder.Eval(Container.DataItem, "update_date")))%></font></span></div></td>

																							</tr>
																							<tr>
																								<td nowrap><font face="Verdana" size="1">Created By</font></td>
																								<td align="left" width="868"><div class="asset_owner"><span class="nowrap"><font face="Verdana" size="1"><%# GetAssetOwner(CType(DataBinder.Eval(Container.DataItem, "asset_id"), String))%></font></span></div></td>
																							</tr>
																							<tr>
																								<td nowrap><font face="Verdana" size="1">Project</font></td>
																								<td align="left" width="868"><div class="asset_owner"><span class="nowrap"><font face="Verdana" size="1"><%# GetAssetProjectName(CType(DataBinder.Eval(Container.DataItem, "projectid"), String))%></font></span></div></td>
																							</tr>
																						</table>
																						<div style="padding-top:3px;">
																							<div class="asset_links"></div>
																						</div>
																						<table width=100%><tr><td>
																						<b><font face="Verdana" size="1"><a href="IDAM.aspx?page=Asset&ID=<%#DataBinder.Eval(Container.DataItem, "asset_id")%>&type=asset">View Details</a></font><br>
																						<!--<font face="Verdana" size="1"><a href="#" onclick="javascript:PreviewOverlayOnThumbSingleClickAssets('<%#DataBinder.Eval(Container.DataItem, "asset_id")%>','<%# GetAssetProjectName(CType(DataBinder.Eval(Container.DataItem, "projectid"), String))%>','<%#DataBinder.Eval(Container.DataItem, "imagesource")%>',event);">Preview</a></font><br>-->
																						<font face="Verdana" size="1"><a href="javascript:DownloadAssetsThumbs('0 <%#DataBinder.Eval(Container.DataItem, "asset_id")%>');">Download</a></font><br>
																						<font face="Verdana" size="1"><a href="javascript:AddToCarouselFromPG('0 <%#DataBinder.Eval(Container.DataItem, "asset_id")%>');">Add to Carousel</a></font><br></b>    
																						</td><td align=right valign=top>
																						<b><font face="Verdana" size="1"><a href="javascript:DeleteSelected('0 <%#DataBinder.Eval(Container.DataItem, "asset_id")%>');">Delete</a></font></b>
																						</td></tr></table>
																						       
																					</div>                        
																					<!--END PADDING-->
														                        
																				</td>
																				</tr>
																			</table>
																		</div><!--END PADDING LEFT X2-->
																		<img border="0" src="images/spacer.gif" width="135" height="8">
																	</div><!--END PADDING LEFT X1-->
																</div><!--end Asset Thumbnail span-->
															</div><!--end inner span-->
														</ItemTemplate>
													
														<HeaderTemplate>
															<!--Start Thumbnail Float-->
															<table width=100%><tr><td>
																<div valign="Bottom" style="float:left!important;">       
														</HeaderTemplate>
																
														<FooterTemplate>
															</td></tr></table>
														</FooterTemplate> 
															
													</asp:Repeater>
													<asp:Literal id="ltrlPagingBottom" Text="" runat="server" />
												</CONTENT>
												<LOADINGPANELCLIENTTEMPLATE>
													<TABLE height="300" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD align="center">
																<TABLE cellSpacing="0" cellPadding="0" border="0">
																	<TR>
																		<TD style="FONT-SIZE: 10px">Loading...
																		</TD>
																		<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
													</TABLE>
												</LOADINGPANELCLIENTTEMPLATE>
											</COMPONENTART:CALLBACK>
											
											
											
											
											
											
											
	</CONTENT>
	<LOADINGPANELCLIENTTEMPLATE>
		<TABLE height="300" cellSpacing="0" cellPadding="0" width="100%" border="0">
			<TR>
				<TD align="center">
					<TABLE cellSpacing="0" cellPadding="0" border="0">
						<TR>
							<TD style="FONT-SIZE: 10px">Loading...
							</TD>
							<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
	</LOADINGPANELCLIENTTEMPLATE>
</COMPONENTART:CALLBACK>											
											
											
											
											
											
											
											
											
										</div><!--SnapProjects-->
									</div><!--padding-->
								</Content>
							</ComponentArt:Snap><!--END-->
							
							
							<!--SNAP OVERLAY WAS HERE-->
							
							
							
							
							
							
							
							
							
							
							
					<div id=trailimageid>
									<div class="SnapProjectsWrapperPopup" style="width:599px;">					
										<COMPONENTART:CALLBACK id="CallbackProjectAssetOverlay" runat="server" CacheContent="false">
											<CONTENT>
												<asp:PlaceHolder ID="PlaceHolderAssetPreviewPage" runat="server">
													<!--assetOptions-->
													<!--
													<div class='assetOptionsPopup'>
													
													
													
													
													
														<table border="0" width="94%" cellspacing="0" cellpadding="0" id="table1">
															<tr>
																<td >

																[<font size="1"><asp:Literal ID="LiteralAssetPopupLinksDownload" Runat=server></asp:Literal>
																download</a></font></td>
																

																
																																
																<td nowrap >

																<font size="1"> ] [ <a href="asfdas">export
																</a> </font>
															</td>
																<td nowrap >

																<font size="1"> 
															<select size="1" name="D1">
															<option>Select Type</option>
															<option>TIFF Large</option>
																</select></font></td>
																<td >

																 ] [ <a href="asd"><font size="1">reconvert</font></a><font size="1">
																</font>
									
  																</td>
																<td >

																<font size="1"> ] [ <asp:Literal ID="LiteralAssetPopupLinksCarousel" Runat=server></asp:Literal>add to 
																carrousel</a></font></td>
																<td ">

																<font size="1"> ] [ <asp:Literal ID="LiteralAssetPopupLinksView" Runat=server></asp:Literal>view 
																details</a></font></td>
																<td>

															 ] [ <a href="#ee"><font size="1">delete</font></a><font size="1"> ]
																</font>
												
  																</td>
															</tr>
														</table>
														
														
													</div>--><!--END assetOptions-->
													
													<!--SnapProjectsOverlay-->
													<div class="SnapProjectsOverlay" >
														<font face="Verdana" size="1">
														<table id="table3">
															<tr>

																<td>
														
																<font size="1"><b>Image:</b></font></span><font size="1">
																</font></td>

																<td align="left" >

																<font size="1"><asp:Literal ID="ProjectAssetOverlayCurrentImageName" Runat=server></asp:Literal></font></td>
												
  															</tr>
															<tr >

																<td><font size="1"><b>Location(s):</b></font></span><font size="1">
																</font></td>

																<td align="left" >
																
																<font size="1"><asp:Literal ID="ProjectAssetOverlayCurrentProjectName" Runat=server></asp:Literal></font></a><font size="1">
																</font></td>

															</tr>
															</table>
														<br>
														<asp:Image id="ProjectAssetOverlayCurrentImage" runat="server" ImageUrl="~/common/images/spacer.gif"></asp:Image>
														
														<!--<br>
													[ <asp:Literal ID="LiteralRotateRight" Runat=server></asp:Literal>rotate right</a> ] [ <asp:Literal ID="LiteralRotateLeft" Runat=server></asp:Literal>rotate left</a> ]-->
													<br>
														<table id="table2">
															<tr>

																<td>
														
																<font size="1">Description:</font></span><font size="1">
																</font></td>
										
																<td align="left" width="80%">

																<font size="1"><asp:Literal ID="LiteralAssetDescription" Runat=server></asp:Literal></font></td>
												
  															</tr>
															<tr >
																<td><font size="1">Owner</font><font size="1">:</font></span><font size="1">
																</font></td>
																<td align="left" width="80%">
																
																<font size="1"><asp:Literal ID="LiteralAssetOwner" Runat=server></asp:Literal></font></a><font size="1">
																</font></td>
															</tr>
															<tr >
																<td noWrap>
																
																<font size="1">Date Created:</font></span><font size="1">
																</font></td>
																<td>
																<font size="1"><asp:Literal ID="LiteralAssetDateCreated" Runat=server></asp:Literal></font></span><font size="1">
																</font></td>
															</tr>
															<tr>


																<td noWrap><font size="1">Type</font><font size="1">:</font></span><font size="1">
																</font></td>
												
																<td>
																<font size="1"><asp:Literal ID="LiteralAssetType" Runat=server></asp:Literal></font></td>
											

  															</tr>
															<tr>

																<td noWrap><font size="1">File Size</font><font size="1">:</font></span><font size="1">
																</font></td>

																<td>
																<font size="1"> 
																
																<asp:Literal ID="LiteralAssetSize" Runat=server></asp:Literal></a> </font>
																</td>
									
  															</tr>
															<tr>

																<td noWrap>
										
																<font size="1">Keywords:</font></span><font size="1">
																</font></td>
										
																<td>
																<font size="1"><asp:Literal ID="LiteralAssetKeywords" Runat=server></asp:Literal></font></td>
											
  															</tr>
															
														</table>
													</div><!--END SnapProjectsOverlay-->
												</asp:PlaceHolder>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<div class="SnapProjectsOverlay" >
													<TABLE cellSpacing="0" cellPadding="0" border="0" height="500" width=100%>
														<TR>
															<TD align="center">
																<TABLE cellSpacing="0" cellPadding="0" border="0">
																	<TR>
																		<TD style="FONT-SIZE: 10px">Loading...
																		</TD>
																		<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																	</TR>
																</TABLE>
															</TD>
															<td><IMG height="16" src="images/spacer.gif" width="16" height="500" border="0">
															</td>
														</TR>
													</TABLE>
												</div>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
									</div><!--padding-->
								
								
								</div>
							<!--END SnapProjectAssetOverlay-->
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							<script type="text/javascript">
								//<% 'Response.Write(SnapProjectAssetOverlay.ClientID)%>.FloatTo(20,20);
							</script>
					
							<div class="SnapHeaderProjectsFiller" ></div>								
						</div><!--bottom padding-->							
					</div><!--projectcenter-->	














				<!--split here ******************************************************************************************************-->
				</td><td width=100% align=left valign=top ><!--TABLE TOP-->
					<div id="projectcenter" style="border:0px solid;padding-left:5px;padding-right:3px;">
						<div class="projectcentertest">
				<!--split here ******************************************************************************************************-->
















							<!--SNAPProjectContacts-->	
							<ComponentArt:Snap id="SNAPProjectContacts" MinWidth="200" runat="server" FillWidth="True" CurrentDockingIndex="1"  Height="200px"  MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter"  DockingContainers="projectcenter" MinimizeDirectionElement="testmenucontrollocation" MinimizeDuration="300">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=5><b>Contacts</b></td><td align=left onmousedown="<% Response.Write(SNAPProjectContacts.ClientID) %>.StartDragging(event);"> <br><img src="images/spacer.gif" width=130 height=1></td>
												<td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectContacts.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(1);" src="images/closebutton.gif"  border="0"></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectContacts.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=5><b>Contacts</b></td><td align=left onmousedown="<% Response.Write(SNAPProjectContacts.ClientID) %>.StartDragging(event);"> <br><img src="images/spacer.gif" width=130 height=1></td>
												<td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectContacts.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(1);" src="images/closebutton.gif"  border="0"></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectContacts.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									<div style="padding-bottom:5px;border:1px;">
										<div class="SnapProjects" style="height:100%;padding:0px;overflow:hidden;padding-left:5px;padding-right:5px;background-color:white;">
											<img src="images/spacer.gif" width=188 height=1>
											<table cellpadding=0 cellspacing=0 ><tr><td align=right>
												<a href="#" onclick="deleteContacts();">delete</a> | <a href="#" onclick="addContacts();">add</a><br>	<br>
												</td></tr></table>
												<table><tr><td>										
												<asp:Literal id="LiteralNoContacts" Text="No contacts added." visible=false runat="server" />
												</td></tr>
											</table>							
											<table cellpadding=0 cellspacing=0 ><tr><td>						
												<COMPONENTART:CALLBACK id="CallbackTreeViewContacts" runat="server" CacheContent="false" >
													<CONTENT>

														<asp:Literal id="LiteralAddContacts" Text="" visible=true runat="server" />

																
													</CONTENT>
													<LOADINGPANELCLIENTTEMPLATE>
														<TABLE cellSpacing="0" cellPadding="0" border="0">
															<TR>
																<TD align="center">
																	<TABLE cellSpacing="0" cellPadding="0" border="0">
																		<TR>
																			<TD style="FONT-SIZE: 10px">Loading...
																			</TD>
																			<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</LOADINGPANELCLIENTTEMPLATE>
												</COMPONENTART:CALLBACK>										
												</td></tr>
											</table>								

											<asp:Literal id="LiteralTVCStart" Text="" visible=false runat="server" />
											<table cellpadding=0 cellspacing=0 ><tr><td>
												<ComponentArt:TreeView id="TreeviewContacts" Height="100%" Width="188px" AutoCallBackOnNodeMove="True" DragAndDropEnabled="false"
													NodeEditingEnabled="false" KeyboardEnabled="false" CssClass="TreeView" NodeCssClass="TreeNode" SelectedNodeCssClass="SelectedTreeNode"
													HoverNodeCssClass="HoverTreeNodeContacts" NodeEditCssClass="NodeEdit" LineImageWidth="19" LineImageHeight="20"
													DefaultImageWidth="16" DefaultImageHeight="16" ExpandSinglePath="true"  NodeLabelPadding="3" ParentNodeImageUrl="images/folders.gif"
													LeafNodeImageUrl="images/folder.gif" ShowLines="false" LineImagesFolderUrl="images/lines/" EnableViewState="true"         ExpandCollapseImageWidth="15" 
													ExpandCollapseImageHeight="15"          NodeIndent="16"         SpacerImageUrl="images/spacer.gif"         CollapseImageUrl="images/exp.gif"         ExpandImageUrl="images/col.gif"         ItemSpacing="3" 
													runat="server">
														<ServerTemplates>
												         
															<ComponentArt:NavigationCustomTemplate id="ContactInfoTemplate">
																<Template>
																<table width=188><tr><td>
																First Name: <%# Container.Attributes("FirstName") %><br>
																Last Name: <%# Container.Attributes("LastName") %><br>
																Email: <a href="mailto:<%# Container.Attributes("Email") %>"><%# Container.Attributes("Email") %></a><br>
																Agency: <%# Container.Attributes("Agency") %><br>
																Phone: <%# Container.Attributes("Phone") %><br>
																Address: <%# Container.Attributes("workAddress") %><br>
																City: <%# Container.Attributes("workCity") %><br>
																State: <%# Container.Attributes("workState") %><br>
																Zip: <%# Container.Attributes("workZip") %><br>
																Cell: <%# Container.Attributes("Cell") %><br>
																Department: <%# Container.Attributes("Department") %><br>
																</td></tr></table>

																</Template>
															</ComponentArt:NavigationCustomTemplate>
												          
														</ServerTemplates>  

												</ComponentArt:TreeView>
												</td></tr>
											</table><br>
											<asp:Literal id="LiteralTVCEnd" Text="" visible=false runat="server" />

										</div><!--SnapProjects-->
									</div><!--padding-->												
								</Content>
							</ComponentArt:Snap><!--END SNAPProjectContacts-->	
							
							<!--SNAPProjectVendors-->	
							<ComponentArt:Snap id="SNAPProjectVendors" MinWidth="200" runat="server" FillWidth="True" FillHeight="True" CurrentDockingIndex="2" Height="100%" MustBeDocked="true" DockingStyle="TransparentRectangle" 
								DraggingStyle="GhostCopy" CurrentDockingContainer="projectcenter"  DockingContainers="projectcenter" MinimizeDirectionElement="testmenucontrollocation" MinimizeDuration="300">
								<Header>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=5><b>Vendors</b></td><td align=left onmousedown="<% Response.Write(SNAPProjectVendors.ClientID) %>.StartDragging(event);"> <br><img src="images/spacer.gif" width=95 height=1></td>
												<td width="15" style="cursor: hand" align="right" ><img onclick="" src="images/editbutton.gif"  border="0"></td><td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectVendors.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(2);" src="images/closebutton.gif"  border="0"></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectVendors.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
											</tr>
										</table>
									</div>
								</Header>
								<CollapsedHeader>
									<div style="CURSOR: move; width: 100%;">
										<table class="SnapHeaderProjects" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td width=5><b>Vendors</b></td><td align=left onmousedown="<% Response.Write(SNAPProjectVendors.ClientID) %>.StartDragging(event);"> <br><img src="images/spacer.gif" width=130 height=1></td>
												<td width="15" style="cursor: hand" align="right" ><img onclick="<% Response.Write(SNAPProjectVendors.ClientID)%>.ToggleMinimize();ToggleItemCheckedState(2);" src="images/closebutton.gif"  border="0"></td><td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SNAPProjectVendors.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15"
														border="0"></td>
											</tr>
										</table>
									</div>
								</CollapsedHeader>
								<Content>
									<div style="padding-bottom:5px;border:1px;">
										<div class="SnapProjects" style="height:100%;width=100%padding:0px;padding-left:5px;padding-right:5px;background-color:white;">
											<img src="images/spacer.gif" width=188 height=1>
											<table cellpadding=0 cellspacing=0 ><tr><td align=right>
											<a href="#" onclick="deleteVendors();">delete</a> | <a href="#" onclick="addVendors();">add</a><br>	<br>
											</td></tr></table>
											<table><tr><td>										
											<asp:Literal id="LiteralNoVendors" Text="No vendors added." visible=false runat="server" />
											</td></tr></table>							
											<table cellpadding=0 cellspacing=0 ><tr><td>						
											<COMPONENTART:CALLBACK id="CallbackTreeViewVendors" runat="server" CacheContent="false" >
											<CONTENT>

											<asp:Literal id="LiteralAddVendors" Text="" visible=true runat="server" />

													
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<TABLE cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<TD align="center">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="FONT-SIZE: 10px">Loading...
																	</TD>
																	<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</LOADINGPANELCLIENTTEMPLATE>
											</COMPONENTART:CALLBACK>										
											</td></tr></table>								


											<asp:Literal id="LiteralTVVStart" Text="" visible=false runat="server" />
											<table cellpadding=0 cellspacing=0 ><tr><td>
												<ComponentArt:TreeView id="TreeviewVendors" Height="100%" Width="188px" AutoCallBackOnNodeMove="True" DragAndDropEnabled="false"
													NodeEditingEnabled="false" KeyboardEnabled="false" CssClass="TreeView" NodeCssClass="TreeNode" SelectedNodeCssClass="SelectedTreeNode"
													HoverNodeCssClass="HoverTreeNodeContacts" NodeEditCssClass="NodeEdit" LineImageWidth="19" LineImageHeight="20"
													DefaultImageWidth="16" DefaultImageHeight="16" ExpandSinglePath="true"  NodeLabelPadding="3" ParentNodeImageUrl="images/folders.gif"
													LeafNodeImageUrl="images/folder.gif" ShowLines="false" LineImagesFolderUrl="images/lines/" EnableViewState="true"         ExpandCollapseImageWidth="15" 
													ExpandCollapseImageHeight="15"          NodeIndent="16"         SpacerImageUrl="images/spacer.gif"         CollapseImageUrl="images/exp.gif"         ExpandImageUrl="images/col.gif"         ItemSpacing="3" 
													runat="server">
														<ServerTemplates>
												         
															<ComponentArt:NavigationCustomTemplate id="VendorInfoTemplate">
																<Template>
																<table width=188><tr><td>
																Name: <%# Container.Attributes("Name") %><br>
													           
																Email: <a href="mailto:<%# Container.Attributes("Email") %>"><%# Container.Attributes("Email") %></a><br>
													           
																Phone: <%# Container.Attributes("Phone") %><br>
																Address: <%# Container.Attributes("Address1") %><br>
																Address: <%# Container.Attributes("Address2") %><br>
																Address: <%# Container.Attributes("Address3") %><br>
																Address: <%# Container.Attributes("Address4") %><br>
																City: <%# Container.Attributes("City") %><br>
																State: <%# Container.Attributes("State") %><br>
																Zip: <%# Container.Attributes("Zip") %><br>
																</td></tr></table>

																</Template>
															</ComponentArt:NavigationCustomTemplate>
													          
														</ServerTemplates>  

												</ComponentArt:TreeView>
											</td></tr></table><br>
											<asp:Literal id="LiteralTVVEnd" Text="" visible=false runat="server" />
										</div>
									</div><!--padding-->												
								</Content>
							</ComponentArt:Snap><!--END SNAPProjectVendors-->		






							<div class="SnapHeaderProjectsFiller"></div>								
						</div><!--bottom padding-->				
					</div><!--projectright-->
				</td></tr></table>







<script language="javascript">

  
  function deleteContacts()
  {
  var treeViewNodes;
  treeViewNodes = '';
  for (i=0;i<40;i++)
  {
  if (<% Response.Write(TreeviewContacts.ClientID) %>.Nodes(i) != null){
  
	if (<% Response.Write(TreeviewContacts.ClientID) %>.Nodes(i).Checked != undefined){
		if (<% Response.Write(TreeviewContacts.ClientID) %>.Nodes(i).Checked) 
		{
		treeViewNodes += <% Response.Write(TreeviewContacts.ClientID) %>.Nodes(i).ID + ","
		}
	}
	}
  }
	if (treeViewNodes != '') {
	var nodelist=treeViewNodes.split(",")
	for (i=0;i<nodelist.length-1;i++)
	{
	    <% Response.Write(TreeviewContacts.ClientID) %>.FindNodeById(nodelist[i]).Remove();
	}
	<% Response.Write(CallbackTreeViewContacts.ClientID) %>.Callback(treeViewNodes);
	}
	<% Response.Write(TreeviewContacts.ClientID) %>.Render();
  }
  
  function addContacts()
  {
  <% Response.Write(CallbackTreeViewContacts.ClientID) %>.Callback('addcontacts');
  }
  
  function cancelAddContacts()
  {
  <% Response.Write(CallbackTreeViewContacts.ClientID) %>.Callback('canceladdcontacts');
  }
  
  function deleteVendors()
  {
  var treeViewNodes;
  treeViewNodes = '';
  for (i=0;i<40;i++)
  {
  if (<% Response.Write(TreeviewVendors.ClientID) %>.Nodes(i) != null){
  
	if (<% Response.Write(TreeviewVendors.ClientID) %>.Nodes(i).Checked != undefined){
		if (<% Response.Write(TreeviewVendors.ClientID) %>.Nodes(i).Checked) 
		{
		treeViewNodes += <% Response.Write(TreeviewVendors.ClientID) %>.Nodes(i).ID + ","
		}
	}
	}
  }
	if (treeViewNodes != '') {
	var nodelist=treeViewNodes.split(",")
	for (i=0;i<nodelist.length-1;i++)
	{
	    <% Response.Write(TreeviewVendors.ClientID) %>.FindNodeById(nodelist[i]).Remove();
	}
	<% Response.Write(CallbackTreeViewVendors.ClientID) %>.Callback(treeViewNodes);
	}
	<% Response.Write(TreeviewVendors.ClientID) %>.Render();
  }
  
  function addVendors()
  {
  <% Response.Write(CallbackTreeViewVendors.ClientID) %>.Callback('addvendors');
  }
  
  function cancelAddVendors()
  {
  <% Response.Write(CallbackTreeViewVendors.ClientID) %>.Callback('canceladdvendors');
  }
  
 
  
  
    function ThumbnailCallBackPage(page)
  {
    <% Response.Write(ThumbnailCallBack.ClientID) %>.Callback(page);
  }
  
  
  function highlightAssetToggle(checkbox,asset_id)
  {
  var highlightbox;
  highlightbox = document.getElementById('assethighlight_' + asset_id);
  if (checkbox.checked) {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  } else
  {
	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
  }}
  
  
    function highlightAssetToggleII(checkbox,asset_id)
  {
  var highlightbox;
  highlightbox = document.getElementById('assethighlight_' + asset_id);
  if (checkbox.checked) {
  	highlightbox.style.border = '1px solid white';
	highlightbox.style.backgroundColor = 'white';
	checkbox.checked = false;

  } else
  {
  highlightbox.style.border = '1px solid black';
  highlightbox.style.backgroundColor = '#FFEEC2';
  checkbox.checked = true;
  }}

  
</script>	
  
<script language=javascript>
	function setSearchWithinCheckValue()
	{
	if (document.getElementById('chksearchsubfolders').checked) 
	{
	document.getElementById('chksearchsubfolders').checked = false;
	document.getElementById('chksearchsubfoldersaction').value = '0';
	} else
	{
	document.getElementById('chksearchsubfolders').checked = true;
	document.getElementById('chksearchsubfoldersaction').value = '1';
	}
	}
	
	
	function setSearchWithinCheckValueFromCheckbox()
	{
	if (document.getElementById('chksearchsubfolders').checked) 
	{
	document.getElementById('chksearchsubfolders').checked = true;
	document.getElementById('chksearchsubfoldersaction').value = '1';
	} else
	{
	document.getElementById('chksearchsubfolders').checked = false;
	document.getElementById('chksearchsubfoldersaction').value = '0';
	}
	}
	
	
	function setSearchWithinCheckValueTrue()
	{
	
	document.getElementById('chksearchsubfolders').checked = true;
	document.getElementById('chksearchsubfoldersaction').value = '1';
	
	}
	
	function opentagwindow()
	{
		//alert('AssetTagging.aspx?id=' + '<%response.write(request.querystring("id"))%>' + '&category_id=' + '<%response.write(request.querystring("c"))%>' + '&pFilter=' + '<%response.write(request.querystring("pFilter"))%>' + '&tFilter=' + '<%response.write(request.querystring("tFilter"))%>' + '&sFilter=' + '<%response.write(request.querystring("sFilter"))%>' + '&mFilter=' + '<%response.write(request.querystring("mFilter"))%>' + '&iFilter=' + '<%response.write(request.querystring("iFilter"))%>');
	  Object_PopUp('AssetTagging.aspx?id=' + '<%response.write(request.querystring("id"))%>' + '&category_id=' + '<%response.write(request.querystring("c"))%>' + '&pFilter=' + '<%response.write(request.querystring("pFilter"))%>' + '&tFilter=' + '<%response.write(request.querystring("tFilter"))%>' + '&sFilter=' + '<%response.write(request.querystring("sFilter"))%>' + '&mFilter=' + '<%response.write(request.querystring("mFilter"))%>' + '&iFilter=' + '<%response.write(request.querystring("iFilter"))%>','Tag_Assets',768,1280);
	}
	
	
		
function AddToCarouselFromPG(ids)
  {
  //check to see if multi select
	var sAssets;
	var addtype;
	if (getLastView() == 'thumb')
        {
    
    var arraylist;
	var i;
	arraylist = '';
	ids = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
	if (arraylist.split(',').length > 2) {
			 if (confirm("Add selected assets to carousel?"))
			 {
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				addtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	addtype = 'single';
	}
       
    
    } else {
        
        
    sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
	ids = ids.replace('0 ','');
	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Add selected assets to carousel?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
				  if (sAssets[i] != null) {
					if (arraylist == ''){
					
					arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
					} else {
					if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
					arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					}
					}
				  }
				}
				
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				addtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	addtype = 'single';
	}
        

    }
	
	
	if (addtype == 'multi') {
		if (arraylist != ''){
		
		IDAMCarousel_carousel_callback.Callback('1,' + arraylist + ',AddMultiToCarousel');
		}
	}
	
	if (addtype == 'single') {
	/*assume single*/
	IDAMCarousel_carousel_callback.Callback('1,' + ids + ',AddToCarousel');
	} 
  }	
  
  
  
  
  
  	
function DeleteSelected(ids)
  {
  //check to see if multi select
	var sAssets;
	var addtype;
  
    
    var arraylist;
	var i;
	arraylist = '';
	ids = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
	if (arraylist.split(',').length > 2) {
			 if (confirm("Delete selected assets?"))
			 {
				arraylist= arraylist.substring(1, arraylist.length);
				addtype = 'multi';
			 }
			 else
			 {
			    if (confirm("Delete individual asset?"))
			    {
				/*delete single*/
				addtype = 'single';
				} else {
				/*delete nothing*/
				addtype = 'donothing';
				}
			 }
	}
	else {
				if (confirm("Delete this asset?"))
			    {
				/*delete single*/
				addtype = 'single';
				} else {
				/*delete nothing*/
				addtype = 'donothing';
				}
	}
       
    
	
	
	if (addtype == 'multi') {
		if (arraylist != ''){
		<%response.write(ThumbnailCallBack.ClientID)%>.Callback('DeleteMulti,' + arraylist );
		}
	}
	
	if (addtype == 'single') {
	/*assume single*/
	<%response.write(ThumbnailCallBack.ClientID)%>.Callback('DeleteSingle,' + ids );
	} 
  }	

  
  

function DoSearchFilter(searchvalue)
{
	SetCookie ('SearchValueProjectAssets', searchvalue, exp);
	if (getLastView() == 'thumb')
        {
        
        <%response.write(ThumbnailCallBack.ClientID)%>.Callback('Search,' + searchvalue);
        <%response.write(CallbackAssetFilters.ClientID)%>.Callback('forcebuild');
        }else{
        <%response.write(GridAssets.ClientID)%>.Filter("name LIKE '%" + searchvalue + "%' OR media_type_name LIKE '%" + searchvalue + "%'");
        <%response.write(GridAssets.ClientID)%>.Render();
        <%response.write(CallbackAssetFilters.ClientID)%>.Callback('forcebuild');
        }
}

function CheckAllItemsAnyMode()
{
	if (getLastView() == 'thumb')
        {
        CheckAllItemsThumbs();
        }else{
       CheckAllItems();
        }
}



</script>	


<script language=javascript type="text/javascript">

function showtrailpreload(imageid,title,description,ratingaverage,ratingnumber,showthumb,height,filetype)
{
	showtrail('<%=IDAM5.BaseIDAMSession.Config.URLLocationPreview%>?id=' + imageid + '&amp;instance=<%response.write (IDAMInstance)%>&amp;type=asset&amp;size=2&amp;height=399&amp;width=370',title,description,'5','1',270,7);

}
    
function onClickMenuFilterFileType(menuItem)
{
	if (menuItem.ID=='clear'){
		SetCookie ('tFilter','', exp);
	}else{
		SetCookie ('tFilter',menuItem.ID, exp);
	}	
	<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
	<% Response.write(CallbackAssetFilters.ClientID)%>.Callback('forcebuild');
	return true; 
}
function onClickMenuFilterKeyword(menuItem)
{
	if (menuItem.ID=='clear'){
		SetCookie ('sFilter','', exp);
	}else{
		SetCookie ('sFilter',menuItem.ID, exp);
	}	
	<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
	<% Response.write(CallbackAssetFilters.ClientID)%>.Callback('forcebuild');
	return true; 
}

function onClickMenuFilterMediaType(menuItem)
{
	if (menuItem.ID=='clear'){
		SetCookie ('mFilter','', exp);
	}else{
		SetCookie ('mFilter',menuItem.ID, exp);
	}	
	<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
	<% Response.write(CallbackAssetFilters.ClientID)%>.Callback('forcebuild');
	return true; 
}
function onClickMenuFilterIllustType(menuItem)
{
	if (menuItem.ID=='clear'){
		SetCookie ('iFilter','', exp);
	}else{
		SetCookie ('iFilter',menuItem.ID, exp);
	}	
	<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
	<% Response.write(CallbackAssetFilters.ClientID)%>.Callback('forcebuild');
	return true; 
}


//initialize the callback controls - clear vars and execute initial callback
SetCookie ('SearchValueProjectAssets', '', exp);
SetCookie ('tFilter', '', exp);
SetCookie ('sFilter', '', exp);
SetCookie ('mFilter', '', exp);
SetCookie ('iFilter', '', exp);
SetCookie ('cFilter', '', exp);

if (getLastView()=='list') {
document.getElementById('assetGirdPagingHeading').style.display='block';
}else{
document.getElementById('assetGirdPagingHeading').style.display='none';
}

<% Response.Write(CallbackTHUMBLIST.ClientID) %>.Callback(getLastView());
<% Response.write(CallbackAssetFilters.ClientID)%>.Callback('forcebuild');

</script>	