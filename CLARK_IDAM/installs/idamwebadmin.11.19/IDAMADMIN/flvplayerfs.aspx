<%@ Page Language="vb" AutoEventWireup="false" Codebehind="flvplayerfs.aspx.vb" Inherits="IDAM5.flvplayerfs"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!-- saved from url=(0014)about:internet -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SWFObject embed by Geoff Stearns (full page) @ deconcept</title>
<!-- SWFObject embed by Geoff Stearns geoff@deconcept.com http://blog.deconcept.com/ -->
<script type="text/javascript" src="js/swfobject.js"></script>
<script type="text/javascript">
<!-- 
function closeWindow() {
	window.close();
}
// -->
</script>
<style type="text/css">
	
	/* hide from ie on mac \*/
	html {
		height: 100%;
		overflow: hidden;
	}
	
	#flashcontent {
		height: 100%;
	}
	/* end hide */

	body {
		height: 100%;
		margin: 0;
		padding: 0;
		background-color: #000000;
	}

</style>
</head>
<body onLoad="window.document.flvplayer.focus();">

	<div id="flashcontent">
		<strong>You need to upgrade your Flash Player</strong>
		This is replaced by the Flash content. 
		Place your alternate content here and users without the Flash plugin or with 
		Javascript turned off will see this. Content here allows you to leave out <code>noscript</code> 
		tags. Include a link to <a href="fullpage.html?detectflash=false">bypass the detection</a> if you wish.
	</div>
	
	<script type="text/javascript">
		// <![CDATA[
		
		var so = new SWFObject("flvplayer.swf?idam=getSmil.aspx&asset_id=<%=request.querystring("asset_id")%>&skinswf=ClearOverAll.swf", "flvplayer", "100%", "100%", "8", "#000000");
		so.addVariable("flashVarText", "this is passed in via FlashVars for example only");
		so.addParam("scale", "exactfit");
		so.write("flashcontent");
		
		// ]]>
	</script>
	
</body>
</html>

