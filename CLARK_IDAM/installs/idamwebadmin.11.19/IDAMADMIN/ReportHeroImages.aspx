<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ReportHeroImages.aspx.vb" Inherits="IDAM5.ReportHeroImages"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Project Reporting Hero Images</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
			<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
			<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
				<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
					<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
						<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
							<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
								<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
									<link href="treeStyle.css" type="text/css" rel="stylesheet">
										<link href="navStyle.css" type="text/css" rel="stylesheet" >
										    <link href="tabStripStyle.css" type="text/css" rel="stylesheet" />    
												<link href="navBarStyle.css" type="text/css" rel="stylesheet" /> 
										<style>
										BODY { MARGIN: 0px }
					
	</style>
	<body MS_POSITIONING="GridLayout" onload="this.focus">
		<form id="Form" method="post" runat="server">
			<script type="text/javascript">
  
  function onInsert(item)
  {
   
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }
  
  function onUpdate(item)
  {
    
      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }
  
  

  function onCallbackErrorProject(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridProjects.ClientID) %>.Page(1); 
  }
  
function editRowProject()
  {
    <% Response.Write(GridProjects.ClientID) %>.EditComplete();     
  }
 
function editGridProject(rowId)
{
<% Response.Write(GridProjects.ClientID) %>.Edit(<% Response.Write(GridProjects.ClientID) %>.GetRowFromClientId(rowId)); 
}
 
  function deleteRowProject(rowId)
  {
    <% Response.Write(GridProjects.ClientID) %>.Delete(<% Response.Write(GridProjects.ClientID) %>.GetRowFromClientId(rowId)); 
  } 
  
    function ShowReport(rowId)
  {
    Object_PopUpScrollbars('ReportHeroImagesSheet.aspx?ID=' + rowId ,'Show_Report',920,750);
  } 
  
 
function Object_PopUpScrollbars(strUrl, popupName, intHeight, intWidth)
{
	if (strUrl == null || strUrl.Length <= 0)
		return;
	var strFeatures = "directories=yes,location=no,menubar=yes,center=yes,scrollbars=yes,resizable=yes,toolbar=yes";
	//var strFeatures = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes";
	if (intHeight != null)
		strFeatures += ",height="+intHeight;
	if (intWidth != null)
		strFeatures += ",width=" + intWidth;
	//var strFeatures += "'"
	if (popupName == null || popupName.Length <= 0)
	{
		var theWindow = window.open( strUrl, "PopUpWindow", strFeatures , false );
	}
	else
	{
		var theWindow = window.open( strUrl, popupName, strFeatures, false );
	}
	theWindow.focus();
} 
  
  

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridProjects.Page(1); 
  }

  function onDelete(item)
  {

    
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    
  }
  
  function editGrid(rowId)
  {
    GridProjects.Edit(GridProjects.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    GridProjects.EditComplete();     
  }

  function insertRow()
  {
    GridProjects.EditComplete(); 
  }

  function deleteRow(rowId)
  {

    GridProjects.Delete(GridProjects.GetRowFromClientId(rowId)); 
  }

      
			</script>
			

			<div  style="margin:20px 20px 20px 20px;padding:20px;border:1px solid;background-color:white;">		
			
			
			<br>
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Below lists all projects that are available within the system and have at least one HERO image declared for the SOM.COM website.</div><br>
<br>
<div class="headingexplaination">Search for projects by entering a keyword and hitting the enter button in the 'Search Filter' box.  You can also browse through the project list by clicking on the paging options listeds at the bottom of the page.  To view the contact sheet for that project, simply click on the 'show report' link at the right of the project listing.  Note:  To filter the list by office, simply type in the office name in the search filter box and hit the enter key.</div>
											<br>
											<div class="headingexplaination">
<asp:Literal id="literalNoProjects" Text="No projects available" visible=false runat="server" /></div>
											<B>Project Search Filter (Name or Office):</b><img src="images/spacer.gif" width=5 height=1><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:140px" onkeydown="javascript:if(event.keyCode==13) {<%response.write( GridProjects.ClientId)%>.Filter('name LIKE \'%' + this.value + '%\' OR keyname LIKE \'%' + this.value + '%\'');return false}">
											<br><br>
											<COMPONENTART:GRID id="GridProjects" ClientSideOnInsert="onInsert" ClientSideOnUpdate="onUpdate" ClientSideOnDelete="onDelete" ClientSideOnCallbackError="onCallbackErrorProject" runat="server" AutoFocusSearchBox="false" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" pagerposition="2" ScrollBar="Off" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ScrollPopupClientTemplateId="ScrollPopupTemplate" Height="10" LoadingPanelPosition="TopCenter" LoadingPanelClientTemplateId="LoadingFeedbackTemplate" EnableViewState="true" GroupBySortImageHeight="10" GroupBySortImageWidth="10" GroupBySortDescendingImageUrl="group_desc.gif" GroupBySortAscendingImageUrl="group_asc.gif" GroupingNotificationTextCssClass="GridHeaderText" AlternatingRowCssClass="AlternatingRowCategory" IndentCellWidth="22" TreeLineImageHeight="19" TreeLineImageWidth="22" TreeLineImagesFolderUrl="images/lines/" PagerImagesFolderUrl="images/pager/" ImagesBaseUrl="images/" PreExpandOnGroup="true" GroupingPageSize="1" PagerTextCssClass="GridFooterTextCategory" PagerStyle="Numbered" PageSize="20" GroupByTextCssClass="GroupByText" GroupByCssClass="GroupByCell" FooterCssClass="GridFooter" HeaderCssClass="GridHeader" SearchOnKeyPress="true" SearchTextCssClass="GridHeaderText" AllowEditing="true" AllowSorting="False" ShowSearchBox="false" ShowHeader="false" ShowFooter="true" CssClass="Grid" RunningMode="callback" ScrollBarWidth="15" AllowPaging="true">
												<ClientTemplates>
													<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
														<div><table cellspacing="0" cellpadding="0" border="0">
																<tr>
																	<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height="225" width="1"><br>
																		<img src="images/spinner.gif" width="16" height="16" border="0"> Loading...
																	</td>
																</tr>
															</table>
														</div>
													</componentart:ClientTemplate>
													<ComponentArt:ClientTemplate Id="EditTemplate">
														<a href="javascript:ShowReport('## DataItem.ClientId ##')">show report</a>
													</ComponentArt:ClientTemplate>
													<ComponentArt:ClientTemplate Id="EditCommandTemplate">
														<a href="javascript:editRowProject();">Update</a>
													</ComponentArt:ClientTemplate>
													<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
														<a href="javascript:insertRow();">Insert</a>
													</ComponentArt:ClientTemplate>
													<ComponentArt:ClientTemplate Id="TypeIconTemplate">
														<img src="images/projcat16.gif" border="0">
													</ComponentArt:ClientTemplate>
													<ComponentArt:ClientTemplate Id="LookupProjectTemplate">
														<A href="IDAM.aspx?page=Project&ID=## DataItem.GetMember("projectid").Value ##&type=project"><img border="0" src="images/goto.gif"></A>
													</ComponentArt:ClientTemplate>
												</ClientTemplates>
												<Levels>
													<componentart:GridLevel EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="projectid" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
														<Columns>
															<ComponentArt:GridColumn Align="Center" AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate"
																dataField="imagesource" HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" HeadingImageUrl="icon_priority.gif"
																HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="20" FixedWidth="True" />
															<componentart:GridColumn AllowEditing="False" HeadingText="Name" Width="450" FixedWidth="True" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name"></componentart:GridColumn>
															<componentart:GridColumn HeadingText="Updated" AllowEditing="false" Width="80" FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="update_date"></componentart:GridColumn>
															<componentart:GridColumn HeadingText="Office" AllowEditing="false" Width="100" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="keyname"></componentart:GridColumn>
															<ComponentArt:GridColumn HeadingText="Action" AllowSorting="False" AllowEditing="false" Width="100" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Align="Center" />
															<ComponentArt:GridColumn HeadingText=" " AllowSorting="False" AllowEditing="false" Width="5" Align="Center" DataCellCssClass="LastDataCellPostings"/>
															<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
															
															<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="page" SortedDataCellCssClass="SortedDataCell" DataField="page"></componentart:GridColumn>
														</Columns>
													</componentart:GridLevel>
												</Levels>
											</COMPONENTART:GRID>
			
			
			<br>
			

			
			
			
			
			
			</div>


			
		
			
			
		</form>
	</body>
</HTML>
