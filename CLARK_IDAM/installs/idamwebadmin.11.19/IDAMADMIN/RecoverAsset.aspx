<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RecoverAsset.aspx.vb" Inherits="IDAM5.RecoverAsset"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>RecoverAsset</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<meta http-equiv="pragma" content="NO-CACHE">
		<meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
		<LINK href="treeStyle.css" type="text/css" rel="stylesheet">
		<LINK href="navStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStripStyle.css" type="text/css" rel="stylesheet">
		<LINK href="navBarStyle.css" type="text/css" rel="stylesheet">
		


		
		
		
	</HEAD>
	<body onload="this.focus" MS_POSITIONING="GridLayout">
		<form id="Form" method="post" runat="server">
			<script type="text/javascript">
  
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridDeletedAssets.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    GridDeletedAssets.Edit(GridDeletedAssets.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    GridDeletedAssets.EditComplete();     
  }

  function insertRow()
  {
    GridDeletedAssets.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    GridDeletedAssets.Delete(GridDeletedAssets.GetRowFromClientId(rowId)); 
  }

 function CheckAllItems()
    {
      GridDeletedAssets.UnSelectAll();
      
    }     
			</script>
			<div class="assetOptions"><table  cellpadding=0 cellspacing =0><tr><td nowrap ></td><td width="125" align=left nowrap><b>Filter Assets:</b><img src="images/spacer.gif" width=5 height=1></td><td align=left width=250 nowrap ><input Class="InputFieldMain" type=text value="" id=search_filter  style="width:150px;height:15px;padding:1px;"  onkeydown="javascript:if(event.keyCode==13) {DoSearchFilter(this.value); return false}"></td><td width=100%></td><td align=right nowrap>[ <a href="javascript:CheckAllItems();">select all</a> ]</td></tr></table></div>
			<br>
			<div id="fullheightdiv" style="border:0px solid black;">
			<asp:Label id="lblMsg" style="TOP: 8px;padding:10px;" runat="server"
				Width="632px" Visible="False" ForeColor="Red">Label</asp:Label>
			<COMPONENTART:GRID id="GridDeletedAssets" PagerTextCssClass="PagerText" PagerStyle="Numbered" runat="server"
				AutoCallBackOnUpdate="false" AutoCallBackOnInsert="false" AutoCallBackOnDelete="false" 
				Width="95%" ScrollPopupClientTemplateId="ScrollPopupTemplate" ScrollBarWidth="16" ScrollGripCssClass="ScrollGrip"
				ScrollBarCssClass="ScrollBar" ScrollButtonHeight="17" ScrollButtonWidth="16" ScrollImagesFolderUrl="images/scroller/"
				ScrollTopBottomImageWidth="16" ScrollTopBottomImageHeight="2" ScrollTopBottomImagesEnabled="true"
				ScrollBar="Off" Sort="asset_id" AllowEditing="true" EditOnClickSelectedItem="false" ImagesBaseUrl="images/"
				ShowFooter="true" GroupingNotificationTextCssClass="GridHeaderText" GroupByTextCssClass="GroupByText"
				CssClass="Grid" RunningMode="Callback" AllowMultipleSelect="true" AllowTextSelection="false" AutoAdjustPageSize="True" FillContainer="True"
				pagerposition="2">
				<Levels>
					<ComponentArt:GridLevel DataKeyField="asset_id" ShowTableHeading="false" ShowSelectorCells="false" HeadingCellCssClass="HeadingCell"
						HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive" HeadingTextCssClass="HeadingCellText"
						DataCellCssClass="DataCell" RowCssClass="Row" SelectedRowCssClass="SelectedRow" SortAscendingImageUrl="asc.gif"
						SortDescendingImageUrl="desc.gif" ColumnReorderIndicatorImageUrl="reorder.gif" SortedDataCellCssClass="SortedDataCell"
						SortImageWidth="14" SortImageHeight="14" EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField"
						EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate">
						<Columns>
							<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" DataCellClientTemplateId="rowicon" DataField="remove" HeadingText="!" Width="20" Visible="true" />
							<ComponentArt:GridColumn  DataField="asset_id" HeadingText="Asset ID"  Visible="true" />
							<ComponentArt:GridColumn DataField="assetname" HeadingText="Asset Name"   />
							<ComponentArt:GridColumn DataField="username" HeadingText="Created By" />
							<ComponentArt:GridColumn DataField="projectname" HeadingText="Project Name"  />
							<ComponentArt:GridColumn DataField="update_date" HeadingText="Last Update Time"   />
							<ComponentArt:GridColumn DataField="Filesize" HeadingText="File Size"   />
							<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" DataField="status" HeadingText="Deletion Status" Width="150" FixedWidth="True" />
						</Columns>
					</ComponentArt:GridLevel>
				</Levels>
				<ClientTemplates>
					<ComponentArt:ClientTemplate Id="EditTemplate">
						<a href="javascript:editGrid('## DataItem.ClientId ##');">Edit</a>
					</ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="rowicon">
						<div style="padding:5px;"><img src="images/9.gif" width="16" height="16" border="0"></div>
					</ComponentArt:ClientTemplate>					
					<ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> | <a href="javascript:GridDeletedAssets.EditCancel();">Cancel</a>
          </ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> | <a href="javascript:GridDeletedAssets.EditCancel();">Cancel</a>
          </ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="LoadingFeedbackTemplate">
						<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td style="font-size:10px;">Loading...&nbsp;</td>
								<td><img src="images/spinner.gif" width="16" height="16" border="0"></td>
							</tr>
						</table>
					</ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="Column1TemplateEdit">
						<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td><img src="images/9.gif" width="16" height="16" border="0"></td>
								<td style="padding-left:2px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;"><nobr>## 
											DataItem.GetMember("Asset_id").Value ##</nobr></div>
								</td>
							</tr>
						</table>
					</ComponentArt:ClientTemplate>
					<ComponentArt:ClientTemplate Id="ScrollPopupTemplate">
						<table cellspacing="0" cellpadding="2" border="0" class="ScrollPopup">
							<tr>
								<td style="width:20px;"><img src="images/9.gif" width="16" height="16" border="0"></td>
								<td style="width:100px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;width:130px;">
										<nobr>## DataItem.GetMember("asset_id").Text ##</nobr></div>
								</td>
								<td style="width:200px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;width:130px;"><nobr>## 
											DataItem.GetMember("assetname").Text ##</nobr></div>
								</td>
								<td style="width:200px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;width:130px;"><nobr>## 
											DataItem.GetMember("username").Text ##</nobr></div>
								</td>
								<td style="width:200px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;width:130px;"><nobr>## 
											DataItem.GetMember("projectname").Text ##</nobr></div>
								</td>
								<td style="width:150px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;width:130px;"><nobr>## 
											DataItem.GetMember("update_date").Text ##</nobr></div>
								</td>
								<td style="width:100px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;width:130px;"><nobr>## 
											DataItem.GetMember("Filesize").Text ##</nobr></div>
								</td>
							</tr>
						</table>
					</ComponentArt:ClientTemplate>
				</ClientTemplates>
			</COMPONENTART:GRID>
			</div><br>
			
			<input id="btnDeleteAll" type="button" value="Delete All" name="btnDeleteAll" runat="server">
			<input id="btnDeleteSelected" type="button" value="Delete Selected" name="btnDeleteSelected"
				runat="server"> <input type="button" value="Recover All" id="btnRecoverAll" name="btnRecoverAll" runat="server">
			<input type="button" value="Recover Selected" id="btnRecoverSelected" name="btnRecoverSelected"
				runat="server">
				<div id="footer">
				</div>
<script type="text/javascript" language=javascript >
  
    

function DoSearchFilter(searchvalue)
{

var xstring = " assetname LIKE '%" + searchvalue + "%' OR username LIKE '%" + searchvalue + "%' OR projectname LIKE '%" + searchvalue + "%'"     
<%response.write( GridDeletedAssets.ClientId)%>.Filter(xstring);<%response.write( GridDeletedAssets.ClientId)%>.Render();
}



function CheckAllItems()
    {
    //three states
    //1 if all checked
    //2 if all unchecked
    //3 if partial - then turn all on.
    var itemIndex = 0;
	for (var x = 0; x <= <% Response.Write(GridDeletedAssets.ClientID) %>.PageSize; x++)
		{
			//mark all off
			if (<% Response.Write(GridDeletedAssets.ClientID) %>.Table.GetRow(x).Selected)
			{
			
			}else
			{
				<% Response.Write(GridDeletedAssets.ClientID) %>.Select(<% Response.Write(GridDeletedAssets.ClientID) %>.Table.GetRow(x),true);
			}
		}
		<% Response.Write(GridDeletedAssets.ClientID) %>.Render();		
 }

</script>


<script type="text/javascript">
<!--
function getWindowHeight() {
	var windowHeight = 0;
	if (typeof(window.innerHeight) == 'number') {
		windowHeight = window.innerHeight;
	}
	else {
		if (document.documentElement && document.documentElement.clientHeight) {
			windowHeight = document.documentElement.clientHeight;
		}
		else {
			if (document.body && document.body.clientHeight) {
				windowHeight = document.body.clientHeight;
			}
		}
	}
	return windowHeight;
}

function setFooter() {
	if (document.getElementById) {
		var windowHeight = getWindowHeight();
		if (windowHeight > 0) {
			var footerElement = document.getElementById('footer');
			var footerHeight  = footerElement.offsetHeight;
			var contentElement = document.getElementById('fullheightdiv');
			contentElement.style.height = (windowHeight - 120) + 'px';
			footerElement.style.position = 'static';
		}
	}
}
		
window.onload = function() { 
	setFooter();
}
window.onresize = function() {
	setFooter();
	<%response.write( GridDeletedAssets.ClientId)%>.Render();
}
//-->
</script>









				
		</form>
	</body>
</HTML>
