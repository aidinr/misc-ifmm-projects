<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="CarouselAccess.ascx.vb" Inherits="IDAM5.CarouselAccess" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div class="previewpaneHeadingProjects" id="toolbar">
	<!--Project cookie and top menu-->
	<table cellspacing="0" cellpadding="4" id="table2" width="100%">
		<tr>
			<td valign="top" width="67">
				<img style="BORDER-RIGHT:#cbcbcb 4px solid;BORDER-TOP:#cbcbcb 4px solid;BORDER-LEFT:#cbcbcb 4px solid;COLOR:#cbcbcb;BORDER-BOTTOM:#cbcbcb 4px solid;BACKGROUND-COLOR:#cbcbcb"
					src="images/8.gif">
			</td>
			<td valign="top">
				<font face="Verdana" size="1"><b>Carousel Validation</b><br>
					<asp:Literal id="LiteralInformation" Text="" visible="true" runat="server" />
				</font>
			</td>
			<td id="Test" valign="top" align="right">
				<!--<div style="padding-top:2px;">
			[ <a href="#">help</a> ]
				</div>-->
			</td>
		</tr>
		<tr>
			<td valign="top" colspan="2">
			</td>
		</tr>
	</table>
</div> <!--end preview pane-->
<div class="previewpaneSub">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle" width="100%">
				<div style="PADDING-RIGHT:0px;PADDING-LEFT:0px;PADDING-BOTTOM:0px;PADDING-TOP:0px;align:right">
					<b>
						<asp:HyperLink CssClass="projecttabs" id="link_Assets" NavigateUrl="javascript:showlist('assetlist');"
							runat="server">Authentication</asp:HyperLink></b><!-- | 
									<asp:HyperLink CssClass="projecttabs" id="link_Projects" NavigateUrl="javascript:showlist('projectlist');"
									runat="server">Projects</asp:HyperLink>-->
				</div>
			</td>
			<td align="right" width="100%">
				<!--<asp:Image onclick="javascript:switchToThumbnail();" id="list_icon"   runat="server"></asp:Image><asp:Image onclick="javascript:switchToThumbnail();"  id="thumb_icon" runat="server"></asp:Image>-->
				<img src="images/spacer.gif" height="16" width="1">
			</td>
		</tr>
	</table>
</div>
<script language="javascript">
var xoffsetpopup
xoffsetpopup = -100
var yoffsetpopup 
yoffsetpopup = -400
</script>
<script language="javascript" src="js/filesearchhover.js" />
<STYLE>.projectcentertest {
	Z-INDEX: 100; WIDTH: 100%; HEIGHT: 100%
}
</STYLE>
<SCRIPT type="text/javascript">
//<![CDATA[    ]
//]]>
</SCRIPT>
<div class="previewpaneProjects" id="toolbar">
	<!--END Project cookie and top menu-->
	<!--projectcentermain-->
	<div id="projectcentermain" style="PADDING-RIGHT: 10px;PADDING-LEFT: 10px;PADDING-TOP: 15px;align-text: left">
		<font color="red">
			<asp:Literal id="LiteralNoMatch" Text="" visible="True" runat="server" /><br>
			<br>
		</font>This carousel is password protected. Please enter the password in the 
		box below and click the submit button.
		<br>
		<br>
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td width="125" align="left" nowrap><b>Enter Password:</b><img src="images/spacer.gif" width="5" height="1"></td>
				<td align="left" width="250" nowrap><input Class="InputFieldMain" type="password" name="PasswordValue" id="PasswordValue" style="PADDING-RIGHT:1px;PADDING-LEFT:1px;PADDING-BOTTOM:1px;WIDTH:150px;PADDING-TOP:1px;HEIGHT:15px"></td>
			</tr>
		</table>
		<br>
		<asp:Button id="btnSubmit" runat="server" Text="Submit"></asp:Button>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
	</div>
</div>
