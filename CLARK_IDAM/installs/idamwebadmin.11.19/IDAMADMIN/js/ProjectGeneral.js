

//<![CDATA[    ]
function ToggleSnapMinimize(SnapObject, MenuItemIndex)
{
	
	//Force reload of page if trying to open a snap.  This is for performance reasons.
	//if (SnapObject.IsMinimized)
	//{
	//force submit here
	//document.getElementById('PageForm').submit();
	//} else
	//{
    SnapObject.ToggleMinimize();
    ToggleItemCheckedState(MenuItemIndex); 
    //}
   
    
}    
   
function ToggleItemCheckedState(MenuItemIndex)
{

    var item = _ctl1_MenuControl.Items(0).Items(MenuItemIndex); 
	if (item.GetProperty('Look-LeftIconUrl') == 'check.gif')
	{
		item.SetProperty('Look-LeftIconUrl','clear.gif');
		//set cookie to false
		SetCookie ('WindowProjectAssets' + MenuItemIndex, 0, exp);
		
	}
	else
	{
		item.SetProperty('Look-LeftIconUrl','check.gif');
		//set cookie to true
		SetCookie ('WindowProjectAssets' + MenuItemIndex, 1, exp);
	}
	item.ParentMenu.Render();
	
}
//]]>
</script>
<script language="javascript">

    function treeProjectContextMenu(treeNode, e)
    {


         //for(var prop in <% Response.Write(MenuProjectExploreTreeNew.ClientID) %>)
		//{
		//	alert(prop);
		//}
       //alert('hey2');
	var contextMenuX = 0; 
    var contextMenuY = 0; 
    e = (e == null) ? window.event : e;
    contextMenuX = e.pageX ? e.pageX : e.x;
    contextMenuY = e.pageY ? e.pageY : e.y;
	<%if instr(Request.ServerVariables ("HTTP_USER_AGENT"),"MSIE") > 0 then%>
		contextMenuY = contextMenuY + 170;
		//contextMenuX = contextMenuX + document.body.offsetWidth -220;
		contextMenuX = contextMenuX + 220;
	<%end if%>
  //winW = document.body.offsetWidth;
  //winH = document.body.offsetHeight
	document.getElementById('ProjectExploreTreeNewselectednode').value = treeNode.ID;
	document.getElementById('idamaction').value = 'editprojectcategories';
      switch(treeNode.Text)
      {
        case 'Calendar': 
          <%response.write( MenuProjectExploreTreeNewContext.ClientId)%>.ShowContextMenu(e, treeNode); 
          break; 

        default: 
         <%response.write( MenuProjectExploreTreeNewContext.ClientId)%>.ShowContextMenu(contextMenuX,contextMenuY,treeNode); 
          break; 
      }  
    }
    
    
    function contextProjectMenuClickHandler(menuItem)
    {
	  document.getElementById('ProjectExploreTreeNewselectednodeaction').value = menuItem.ID;
      var contextDataNode = menuItem.ParentMenu.ContextData; 
      document.getElementById('ProjectExploreTreeNewselectednode').value = contextDataNode.ID;
      
 switch(menuItem.ID)
      {
		case 'NEWCATEGORY':
			<%response.write(CallBackModifyCategory.ClientId)%>.Callback('NEW,' + contextDataNode.ID);
			break;
			return true;
			
		case 'DELETE':
			 if (confirm("Delete " + contextDataNode.Text + "?"))
			 {
				document.getElementById('idamaction').value = 'deleteprojectcategory';
				document.getElementById('PageForm').submit();
				break;
			}
			else
			{
				break;
			}
			
		case 'EDIT':
			<%response.write( CallBackModifyCategory.ClientId)%>.Callback('EDIT,' + contextDataNode.ID);
			//document.getElementById('PageForm').submit();
			break;
			
		default:
			break;
      }
      return true; 
    }

function hideEditCategory()
{
<%response.write( CallBackModifyCategory.ClientId)%>.Callback('HIDE,');
}

function contextProjectAssetsMenuClickHandler(menuItem)
{
var is_ColumnHandler=menuItem.ID.indexOf('ACTION');
if (is_ColumnHandler!=-1)
      {
			document.getElementById('ProjectAssetselectednodeaction').value = menuItem.ID; 
			document.getElementById('PageForm').submit();
			return true;
      }   
}


//setup form switch for delete or multi asset zip download
function DownloadAssets(ids){
	var sAssets;
	var downloadtype;
	sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
	ids = ids.replace('0 ','');
	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Download selected assets?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (arraylist == ''){
					arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
					} else {
					if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
					arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					}
					}
				}
				
				arraylist= arraylist.substring(1, arraylist.length);
				downloadtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				downloadtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	downloadtype = 'single';
	}
	if (downloadtype == 'multi') {
		if (arraylist != ''){
		window.open('<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownloadGetMulti & "?assetids="%>' + arraylist + '<%="&instance=" & IDAMInstance%>','ConvertingImage','width=500,height=75,location=no');
		}
	}
	if (downloadtype == 'single') {
	/*assume single*/
	window.location = '<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownload & "?dtype=assetdownload&assetid="%>' + ids + '<%="&instance=" & IDAMInstance%>&size=0';
	return true;
	}

}
	
function PreviewOverlayOnSingleClickAssets(item)
  {
	var itemvaluetmp;
	var itemvaluenametmp;
	var itemvaluefiletypetmp;
	itemvaluetmp = item.GetMember('asset_id').Value;
	itemvaluenametmp = item.GetMember('name').Value;
	itemvaluefiletypetmp = item.GetMember('imagesource').Value;
	
	//}
	if (<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.IsMinimized)
	{
	<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.UnMinimize();
	}
	<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.Expand();
	<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);
	
	return true;
  }  
  
  function PreviewOverlayOnSingleClickAssets(item)
  {
	var itemvaluetmp;
	var itemvaluenametmp;
	var itemvaluefiletypetmp;
	itemvaluetmp = item.GetMember('asset_id').Value;
	itemvaluenametmp = item.GetMember('name').Value;
	itemvaluefiletypetmp = item.GetMember('imagesource').Value;
	
	//}
	if (<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.IsMinimized)
	{
	<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.UnMinimize();
	}
	<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.Expand();
	<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);
	
	return true;
  } 
  
  function PreviewOverlayOnThumbSingleClickAssets(aid,aname,afiletype)
  {
	if (<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.IsMinimized)
	{
	<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.UnMinimize();
	}
	<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.Expand();
	<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(aid + ',Asset' + ',' + aname + ',' + afiletype);
	
	//return true;
  } 
  
  
  
function PreviewOverlayOnSingleClickAssetsFromIcon(RowId)
  {
	var itemvaluetmp;
	var itemvaluenametmp;
	var itemvaluefiletypetmp;
	var item 
	itemRow = <% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(RowId);
	itemvaluetmp = itemRow.GetMember('asset_id').Value;
	itemvaluenametmp = itemRow.GetMember('name').Value;
	itemvaluefiletypetmp = itemRow.GetMember('imagesource').Value;
	
	//}
	if (<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.IsMinimized)
	{
	<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.UnMinimize();
	}
	<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.Expand();
	<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);
	
	//return true;
  }    


function AssetPreview(Asset_ID,cid)
{
	//get state
	//if (<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.IsCollapsed) 
	//{
	<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.Expand();
	//}
	if (<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.IsMinimized)
	{
	<% Response.Write(SnapProjectAssetOverlay.ClientID) %>.UnMinimize();
	}
	<%response.write( SnapProjectAssetOverlay.ClientId)%>.Callback(cid);
}

//setup goto for grid nav
function GotoLocation(ctype,cid)
	{
	//if (ctype == '0') {
		window.location = 'IDAM.aspx?page=Project&Id=<%response.write (request.querystring("Id"))%>&type=project&c=' + cid;
	//} else{
	//	window.location = 'IDAM.aspx?page=Project&Id=<%response.write (request.querystring("Id"))%>&type=project';
	//}
	return true;
	}
	
	
	
  function onUpdate(item)
  {

      if (confirm("Update record?"))
        return true; 
      else
        return false; 

  }


  function onInsert(item)
  {

      if (confirm("Insert record?"))
        return true; 
      else
        return false; 

  }



  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <% Response.Write(GridAssets.ClientID) %>.Page(1); 
  }

  function onDelete(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }	
	
	
function editRow()
  {
    <% Response.Write(GridAssets.ClientID) %>.EditComplete();     
  }


function CheckAllItems()
    {
    var itemIndex = 0;
	for (var x = 0; x <= <% Response.Write(GridAssets.ClientID) %>.PageSize; x++)
		{
		 <% Response.Write(GridAssets.ClientID) %>.Select(<% Response.Write(GridAssets.ClientID) %>.Table.GetRow(x),true);
		}

      
      <% Response.Write(GridAssets.ClientID) %>.Render();
    }
    
function MM_findObj(n, d) { //v3.0
var p,i,x; if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function CheckAllItemsThumbs() { //Set All checkboxes, if un-checked, check them.
if (MM_findObj('selectassethidden').length > 1) { 
var checks = MM_findObj('selectassethidden')
var dummyobjectvalue = checks[0].value;
var firstobjectcheckedstate = document.getElementById('aselect_' + dummyobjectvalue);
var bcheck = firstobjectcheckedstate.checked

if (bcheck) {
bcheck = true;
} else {
bcheck = false;
}

for (i=0; i<checks.length; i++) {
MM_findObj('aselect_' + checks[i].value).checked = bcheck ;
highlightAssetToggleII(document.getElementById('aselect_' + checks[i].value),checks[i].value); 
}

}
}


//setup form switch for delete or multi asset zip download
function DownloadAssetsThumbs(ids){
	var arraylist;
	var i;
	var sAssets;
	var downloadtype;
	arraylist = '';
	ids = ids.replace('0 ','');
	if (MM_findObj('selectassethidden').length > 1) { 
		var checks = MM_findObj('selectassethidden')
		for (i=0; i<checks.length; i++) {
			if (MM_findObj('aselect_' + checks[i].value).checked) {
				arraylist = arraylist + ',' +  checks[i].value;
			}
		}
	}
	//alert(arraylist + arraylist.split(',').length)
	if (arraylist.split(',').length > 2) {
			 if (confirm("Download selected assets?"))
			 {
				arraylist= arraylist.substring(1, arraylist.length);
				downloadtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				downloadtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	downloadtype = 'single';
	}
	if (downloadtype == 'multi') {
		if (arraylist != ''){
		window.open('<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownloadGetMulti & "?assetids="%>' + arraylist + '<%="&instance=" & IDAMInstance%>','ConvertingImage','width=500,height=75,location=no');
		}
	}
	if (downloadtype == 'single') {
	/*assume single*/
	window.location = '<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownload & "?dtype=assetdownload&assetid="%>' + ids + '<%="&instance=" & IDAMInstance%>&size=0';
	return true;
	}

}
