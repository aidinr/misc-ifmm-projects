<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="EventEdit.aspx.vb" Inherits="IDAM5.EventEdit"   ValidateRequest="False"%>
<%@ Register TagPrefix="upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Event</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
		<link href="treeStyle.css" type="text/css" rel="stylesheet">
		<link href="navStyle.css" type="text/css" rel="stylesheet">
		<link href="tabStripStyle.css" type="text/css" rel="stylesheet">
		<link href="navBarStyle.css" type="text/css" rel="stylesheet">
		<link href="calendarStyle.css" type="text/css" rel="stylesheet" />
		<style>BODY { MARGIN: 0px }
		</style>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onload="this.focus">
		<form id="Form" method="post" runat="server">
			<script type="text/javascript">
  
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    GridKeywords.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    GridKeywords.Edit(GridKeywords.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    GridKeywords.EditComplete();     
  }

  function insertRow()
  {
    GridKeywords.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    GridKeywords.Delete(GridKeywords.GetRowFromClientId(rowId)); 
  }

<%if request.querystring("newid") <> "" then%>
//window.opener.NavigateToPage('User');
//window.close();
<%end if%>


function onPickerChange(picker)
      {
        <% Response.Write(Calendar1.ClientID) %>.SetSelectedDate(picker.GetSelectedDate());
      }
      function onPickerMouseUp()
      {
        if (<% Response.Write(Calendar1.ClientID) %>.PopUpObjectShowing)
        {
          event.cancelBubble=true;
          event.returnValue=false;
          return false;
        }
        else
        {
          return true;
        }
      }
      function OkClick()
      {
        <% Response.Write(Picker1.ClientID) %>.SetSelectedDate(<% Response.Write(Calendar1.ClientID) %>.GetSelectedDate());
        <% Response.Write(Calendar1.ClientID) %>.Hide();
      }
      function CancelClick()
      {
        <% Response.Write(Calendar1.ClientID) %>.Hide();
      }



function onPicker2Change(picker)
      {
        <% Response.Write(Calendar2.ClientID) %>.SetSelectedDate(picker2.GetSelectedDate());
      }
      function onPicker2MouseUp()
      {
        if (<% Response.Write(Calendar2.ClientID) %>.PopUpObjectShowing)
        {
          event.cancelBubble=true;
          event.returnValue=false;
          return false;
        }
        else
        {
          return true;
        }
      }
      function OkClick2()
      {
        <% Response.Write(Picker2.ClientID) %>.SetSelectedDate(<% Response.Write(Calendar2.ClientID) %>.GetSelectedDate());
        <% Response.Write(Calendar2.ClientID) %>.Hide();
      }
      function CancelClick2()
      {
        <% Response.Write(Calendar2.ClientID) %>.Hide();
      }







function onPicker3Change(picker)
      {
        <% Response.Write(Calendar3.ClientID) %>.SetSelectedDate(picker3.GetSelectedDate());
      }
      function onPicker3MouseUp()
      {
        if (<% Response.Write(Calendar3.ClientID) %>.PopUpObjectShowing)
        {
          event.cancelBubble=true;
          event.returnValue=false;
          return false;
        }
        else
        {
          return true;
        }
      }
      function OkClick3()
      {
        <% Response.Write(Picker3.ClientID) %>.SetSelectedDate(<% Response.Write(Calendar3.ClientID) %>.GetSelectedDate());
        <% Response.Write(Calendar3.ClientID) %>.Hide();
      }
      function CancelClick3()
      {
        <% Response.Write(Calendar3.ClientID) %>.Hide();
      }


			</script>

			<input type="hidden" name="newsid" id="newsid" value="<%response.write (request.querystring("ID"))%>">
			
			
			
			<div style="padding:20px;position:relative;">
			
			<div style="">
			<div style="padding:10px;border:1px solid;background-color:white;position:relative;">
			<table><tr><td><img src="images/calendar.gif"></td><td>Create/Edit Event Items</td></tr></table>
			<div style="padding:5px;background-color: #E4E4E4;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">Create a event item by entering the information below.</div><br>
  <div style="">

							<COMPONENTART:TABSTRIP id="TabStripCategory" runat="server" ImagesBaseUrl="images/" SiteMapXmlFile="tabDataEvents.xml"
								CssClass="TopGroup" DefaultDisabledItemLookId="DisabledTabLook" FillWidth="True" Width="100%" DefaultItemLookId="DefaultTabLook"
								DefaultSelectedItemLookId="SelectedTabLook" DefaultGroupTabSpacing="1" MultiPageId="MultiPageCategory">
								<ItemLooks>
									<ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover" LabelPaddingLeft="10"
										LabelPaddingRight="10" LabelPaddingTop="5" LabelPaddingBottom="4" LeftIconUrl="tab_left_icon.gif" RightIconUrl="tab_right_icon.gif"
										HoverLeftIconUrl="hover_tab_left_icon.gif" HoverRightIconUrl="hover_tab_right_icon.gif" LeftIconWidth="23"
										LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
									<ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="10" LabelPaddingRight="10"
										LabelPaddingTop="4" LabelPaddingBottom="4" LeftIconUrl="selected_tab_left_icon.gif" RightIconUrl="selected_tab_right_icon.gif"
										LeftIconWidth="23" LeftIconHeight="23" RightIconWidth="6" RightIconHeight="23" />
								</ItemLooks>
							</COMPONENTART:TABSTRIP>
							<ComponentArt:MultiPage id="MultiPageCategory" CssClass="MultiPageEventPage" runat="server">
								<ComponentArt:PageView CssClass="PageContent" runat="server" ID="User_General">
									<DIV style="BORDER-RIGHT: #b7b4b4 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #b7b4b4 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: normal; FONT-SIZE: 10px; PADDING-BOTTOM: 5px; BORDER-LEFT: #b7b4b4 1px solid; COLOR: #3f3f3f; PADDING-TOP: 5px; BORDER-BOTTOM: #b7b4b4 1px solid; FONT-FAMILY: verdana; BACKGROUND-COLOR: #e4e4e4">Enter 
										information for this event item.</DIV>
									<BR>
				
									
								
<table border="0" width="100%" id="table1" style="FONT-SIZE: 8pt; FONT-FAMILY: verdana;height=25px;"	>

		<tr>
			<td width="100" align="left" valign="top">Headline
</td>
			<td align="left" valign="top">
<asp:TextBox  id="Textbox_Headline" Width="100%" CssClass="InputFieldMain100P" Height=25px runat="server"></asp:TextBox>
			</td>
		</tr>
		
		
		
		
		
		
		<tr>
		<td class="PageContent" width="100" align="left" valign="top" nowrap>
			<font face="Verdana" size="1">News Type</font></td>
			<td class="PageContent"  align="left" valign="top" valign="top">
				<asp:DropDownList style="border:1px dotted;padding:5px;"  DataTextField="type_value" DataValueField="type_id"  ID="DropDown_News_Type" width="180" Runat="server"></asp:DropDownList>
			</td>
		</tr>
		
		
		
		
		
	
		<tr>
			<td width="100" align="left" valign="top">Content</td>
			<td align="left" valign="top">
<asp:TextBox  style="height:150px;" TextMode="MultiLine" id="Textbox_Content" Width="100%" CssClass="InputFieldMain100P" runat="server"></asp:TextBox>
			</td>
		</tr>
		
		
		
		
		
		
			
		<tr>
			<td width="100" align="left" valign="top">Contact Info</td>
			<td align="left" valign="top">
<asp:TextBox style="height:150px;" TextMode="MultiLine" id="Textbox_Contact_Info" Width="100%" CssClass="InputFieldMain100P" runat="server"></asp:TextBox>
			</td>
		</tr>
		
		
		
		
		
		
		<tr>
			<td width="100" align="left" valign="top">Post Date</td>
			<td align="left" valign="top">

    <table onmouseup="onPickerMouseUp()" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td><ComponentArt:Calendar id="Picker1" 
          ControlType="Picker" 
          ClientSideOnSelectionChanged="onPickerChange" 
          PickerFormat="Custom" 
          PickerCustomFormat="MMMM d yyyy" 
          PickerCssClass="pickerForm" 
          runat="server" 
          /></td>
      <td style="font-size:10px;">&nbsp;</td>
      <td><img id="calendar_button" alt="" onclick="<% Response.Write(Calendar1.ClientID) %>.SetSelectedDate(<% Response.Write(Picker1.ClientID) %>.GetSelectedDate());<% Response.Write(Calendar1.ClientID) %>.Show();" class="calendar_button" src="images/btn_calendar.gif" width="25" height="22" /></td>
    </tr>
    </table>

    <ComponentArt:Calendar runat="server"
      id="Calendar1"
      AllowMonthSelection="false"
      AllowMultipleSelection="false"
      AllowWeekSelection="false"
      CalendarCssClass="calendar"
      CollapseOnSelect="false"
      ControlType="Calendar"
      DayCssClass="day"
      DayHeaderCssClass="dayheader"
      DayHoverCssClass="dayhover"
      DayNameFormat="FirstTwoLetters"
      OtherMonthDayCssClass="othermonthday"
      OtherMonthDayHoverCssClass="othermonthdayhover"
      PopUp="Custom"
      PopUpExpandControlId="calendar_button"
      SelectedDayCssClass="selectedday" 
      SelectedDayHoverCssClass="selecteddayhover"
      ShowCalendarTitle="false"
      SwapDuration="300"
      SwapSlide="Linear"      
      Width="200"
      >
      <HeaderClientTemplate>
        <div class="header">
          <span style="float:left;">
            <span class="headerbutton" onclick="Calendar1.GoPrevYear()"><img src="images/i_closed.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoPrevMonth()"><img src="images/cal_prevMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoToday()"><img src="images/bullet.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoNextMonth()"><img src="images/cal_nextMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar1.GoNextYear()"><img src="images/i_open.gif"></span>
          </span>
          <span style="float:right;">##Calendar1.FormatDate(Calendar1.VisibleDate,"MMMM yyyy")##</span>
        </div>
      </HeaderClientTemplate>
      <FooterClientTemplate>
        <div class="footer">
          <span style="float:right;">
            <input type="button" value="&nbsp; Ok &nbsp;" onclick="OkClick()" class="button" />
            <input type="button" value="Cancel" onclick="CancelClick()" class="button" />
          </span>
        </div>
      </FooterClientTemplate>
    </ComponentArt:Calendar>


			</td>
		</tr>
		
		
		
		
		
		
		
		
		<tr>
			<td width="100" align="left" valign="top">Pull Date</td>
			<td align="left" valign="top">

    <table onmouseup="onPicker2MouseUp()" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td><ComponentArt:Calendar id="Picker2" 
          ControlType="Picker" 
          ClientSideOnSelectionChanged="onPicker2Change" 
          PickerFormat="Custom" 
          PickerCustomFormat="MMMM d yyyy" 
          PickerCssClass="pickerForm" 
          runat="server" 
          /></td>
      <td style="font-size:10px;">&nbsp;</td>
      <td><img id="calendar_button" alt="" onclick="<% Response.Write(Calendar2.ClientID) %>.SetSelectedDate(<% Response.Write(Picker2.ClientID) %>.GetSelectedDate());<% Response.Write(Calendar2.ClientID) %>.Show();" class="calendar_button" src="images/btn_calendar.gif" width="25" height="22" /></td>
    </tr>
    </table>

    <ComponentArt:Calendar runat="server"
      id="Calendar2"
      AllowMonthSelection="false"
      AllowMultipleSelection="false"
      AllowWeekSelection="false"
      CalendarCssClass="calendar"
      CollapseOnSelect="false"
      ControlType="Calendar"
      DayCssClass="day"
      DayHeaderCssClass="dayheader"
      DayHoverCssClass="dayhover"
      DayNameFormat="FirstTwoLetters"
      OtherMonthDayCssClass="othermonthday"
      OtherMonthDayHoverCssClass="othermonthdayhover"
      PopUp="Custom"
      PopUpExpandControlId="calendar_button"
      SelectedDayCssClass="selectedday" 
      SelectedDayHoverCssClass="selecteddayhover"
      ShowCalendarTitle="false"
      SwapDuration="300"
      SwapSlide="Linear"      
      Width="200"
      >
      <HeaderClientTemplate>
        <div class="header">
          <span style="float:left;">
            <span class="headerbutton" onclick="Calendar2.GoPrevYear()"><img src="images/i_closed.gif"></span>
            <span class="headerbutton" onclick="Calendar2.GoPrevMonth()"><img src="images/cal_prevMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar2.GoToday()"><img src="images/bullet.gif"></span>
            <span class="headerbutton" onclick="Calendar2.GoNextMonth()"><img src="images/cal_nextMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar2.GoNextYear()"><img src="images/i_open.gif"></span>
          </span>
          <span style="float:right;">##Calendar2.FormatDate(Calendar2.VisibleDate,"MMMM yyyy")##</span>
        </div>
      </HeaderClientTemplate>
      <FooterClientTemplate>
        <div class="footer">
          <span style="float:right;">
            <input type="button" value="&nbsp; Ok &nbsp;" onclick="OkClick2()" class="button" />
            <input type="button" value="Cancel" onclick="CancelClick2()" class="button" />
          </span>
        </div>
      </FooterClientTemplate>
    </ComponentArt:Calendar>




			</td>
		</tr>
		
		
		
		
		
		
		<tr>
			<td width="100" align="left" valign="top">Event Date</td>
			<td align="left" valign="top">

    <table onmouseup="onPicker3MouseUp()" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td><ComponentArt:Calendar id="Picker3" 
          ControlType="Picker" 
          ClientSideOnSelectionChanged="onPicker3Change" 
          PickerFormat="Custom" 
          PickerCustomFormat="MMMM d yyyy" 
          PickerCssClass="pickerForm" 
          runat="server" 
          /></td>
      <td style="font-size:10px;">&nbsp;</td>
      <td><img id="calendar_button" alt="" onclick="<% Response.Write(Calendar3.ClientID) %>.SetSelectedDate(<% Response.Write(Picker3.ClientID) %>.GetSelectedDate());<% Response.Write(Calendar3.ClientID) %>.Show();" class="calendar_button" src="images/btn_calendar.gif" width="25" height="22" /></td>
    </tr>
    </table>

    <ComponentArt:Calendar runat="server"
      id="Calendar3"
      AllowMonthSelection="false"
      AllowMultipleSelection="false"
      AllowWeekSelection="false"
      CalendarCssClass="calendar"
      CollapseOnSelect="false"
      ControlType="Calendar"
      DayCssClass="day"
      DayHeaderCssClass="dayheader"
      DayHoverCssClass="dayhover"
      DayNameFormat="FirstTwoLetters"
      OtherMonthDayCssClass="othermonthday"
      OtherMonthDayHoverCssClass="othermonthdayhover"
      PopUp="Custom"
      PopUpExpandControlId="calendar_button"
      SelectedDayCssClass="selectedday" 
      SelectedDayHoverCssClass="selecteddayhover"
      ShowCalendarTitle="false"
      SwapDuration="300"
      SwapSlide="Linear"      
      Width="200"
      >
      <HeaderClientTemplate>
        <div class="header">
          <span style="float:left;">
            <span class="headerbutton" onclick="Calendar3.GoPrevYear()"><img src="images/i_closed.gif"></span>
            <span class="headerbutton" onclick="Calendar3.GoPrevMonth()"><img src="images/cal_prevMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar3.GoToday()"><img src="images/bullet.gif"></span>
            <span class="headerbutton" onclick="Calendar3.GoNextMonth()"><img src="images/cal_nextMonth.gif"></span>
            <span class="headerbutton" onclick="Calendar3.GoNextYear()"><img src="images/i_open.gif"></span>
          </span>
          <span style="float:right;">##Calendar3.FormatDate(Calendar3.VisibleDate,"MMMM yyyy")##</span>
        </div>
      </HeaderClientTemplate>
      <FooterClientTemplate>
        <div class="footer">
          <span style="float:right;">
            <input type="button" value="&nbsp; Ok &nbsp;" onclick="OkClick3()" class="button" />
            <input type="button" value="Cancel" onclick="CancelClick3()" class="button" />
          </span>
        </div>
      </FooterClientTemplate>
    </ComponentArt:Calendar>




			</td>
		</tr>
		
		
		
		
		<tr>
			<td width="100" align="left" valign="top">Event Time</td>
			<td align="left" valign="top">


                <select class="annoucementsdates" size="1" name="et_hour">
                  <option <%if (datepart("H",spEvent_date)=12 or datepart("H",spEvent_date)=00) then response.write ("selected")%>>
					12</option>
                  <option <%if datepart("H",spEvent_date)=1 or datepart("H",spEvent_date)=13 then response.write ("selected")%>>
					01</option>
                  <option <%if datepart("H",spEvent_date)=2 or datepart("H",spEvent_date)=14 then response.write ("selected")%>>
					02</option>
                  <option <%if datepart("H",spEvent_date)=3 or datepart("H",spEvent_date)=15 then response.write ("selected")%>>
					03</option>
                  <option <%if datepart("H",spEvent_date)=4  or datepart("H",spEvent_date)=16 then response.write ("selected")%>>
					04</option>
                  <option <%if datepart("H",spEvent_date)=5 or datepart("H",spEvent_date)=17 then response.write ("selected")%>>
					05</option>
                  <option <%if datepart("H",spEvent_date)=6 or datepart("H",spEvent_date)=18 then response.write ("selected")%>>
					06</option>
                  <option <%if datepart("H",spEvent_date)=7 or datepart("H",spEvent_date)=19 then response.write ("selected")%>>
					07</option>
                  <option <%if datepart("H",spEvent_date)=8 or datepart("H",spEvent_date)=20 then response.write ("selected")%>>
					08</option>
                  <option <%if datepart("H",spEvent_date)=9 or datepart("H",spEvent_date)=21 then response.write ("selected")%>>
					09</option>
                  <option <%if datepart("H",spEvent_date)=10 or datepart("H",spEvent_date)=22 then response.write ("selected")%>>
					10</option>
                  <option <%if datepart("H",spEvent_date)=11 or datepart("H",spEvent_date)=23 then response.write ("selected")%>>
					11</option>
                  
                </select>
                :
                <select class="annoucementsdates" size="1" name="et_min">
                  <option <%if datepart("n",spEvent_date)=0 then response.write ("selected")%>>
					00</option>
                  <option <%if datepart("n",spEvent_date)=5 then response.write ("selected")%>>
					05</option>
                  <option <%if datepart("n",spEvent_date)=10 then response.write ("selected")%>>
					10</option>
                  <option <%if datepart("n",spEvent_date)=15 then response.write ("selected")%>>
					15</option>
                  <option <%if datepart("n",spEvent_date)=20 then response.write ("selected")%>>
					20</option>
                  <option <%if datepart("n",spEvent_date)=25 then response.write ("selected")%>>
					25</option>
                  <option <%if datepart("n",spEvent_date)=30 then response.write ("selected")%>>
					30</option>
                  <option <%if datepart("n",spEvent_date)=35 then response.write ("selected")%>>
					35</option>
                  <option <%if datepart("n",spEvent_date)=40 then response.write ("selected")%>>
					40</option>
                  <option <%if datepart("n",spEvent_date)=45 then response.write ("selected")%>>
					45</option>
                  <option <%if datepart("n",spEvent_date)=50 then response.write ("selected")%>>
					50</option>
                  <option <%if datepart("n",spEvent_date)=55 then response.write ("selected")%>>
					55</option>

                </select>&nbsp;
                <select class="annoucementsdatesyear" size="1" name="et_ampm">
                  <option <%if datepart("H",spEvent_date)>=12 then response.write ("selected")%>>
					PM</option>
                  <option <%if datepart("H",spEvent_date)<12 then response.write ("selected")%>>
					AM</option>
                  
                </select>	
		
			</td>
		</tr>
		
		
				
		
		
		
		
		
		
	</table>
	
	

	
<br>
	<div id="tagallowupload" style="display:none;">
	[ <a href="javascript:showEventAssetAdd()">add event asset</a> ]
	</div>
<div id="tagcreateversion" style="padding:10px;display:none;">

<b>Add event assets</b> to this event by browsing to each file and selecting the 'add asset' button below.</font><br>

 <br>Upload Assets
        <br>
        <Upload:InputFile Class="InputFieldMain100P" id="InputFile1" runat="server"></Upload:InputFile>
		 <Upload:InputFile Class="InputFieldMain100P" id="Inputfile2" runat="server"></Upload:InputFile>
		  <Upload:InputFile Class="InputFieldMain100P" id="Inputfile3" runat="server"></Upload:InputFile>
		   <Upload:InputFile Class="InputFieldMain100P" id="Inputfile4" runat="server"></Upload:InputFile>
		    <Upload:InputFile Class="InputFieldMain100P" id="Inputfile5" runat="server"></Upload:InputFile>
		     <Upload:InputFile Class="InputFieldMain100P" id="Inputfile6" runat="server"></Upload:InputFile>
		      <Upload:InputFile Class="InputFieldMain100P" id="Inputfile7" runat="server"></Upload:InputFile>
		       <Upload:InputFile Class="InputFieldMain100P" id="Inputfile8" runat="server"></Upload:InputFile>
		        <Upload:InputFile Class="InputFieldMain100P" id="Inputfile9" runat="server"></Upload:InputFile>
		         <Upload:InputFile Class="InputFieldMain100P" id="Inputfile10" runat="server"></Upload:InputFile>
		<br><br>
<asp:Button id="btnUpload" Class="InputButtonMain" runat="server" Text="Add Assets"></asp:Button><input type="button" Class="InputButtonMain" onclick="document.getElementById('tagcreateversion').style.display='none';document.getElementById('tageventassets').style.display='block';" value="Cancel" id="submit1" name="submit1"></p>
						
</div>	
<div id="tageventassets" style="display:block;">
	<!--GroupBy="categoryname ASC"--><br><br><b>Event Assets</b><br><Br>
											<COMPONENTART:GRID 
												id="GridAssets" 

												runat="server" 
												AutoFocusSearchBox="false"
												AutoCallBackOnDelete="true"
												pagerposition="2"
												ScrollBar="Off"
												ScrollTopBottomImagesEnabled="true"
												ScrollTopBottomImageHeight="2"
												ScrollTopBottomImageWidth="16"
												ScrollImagesFolderUrl="images/scroller/"
												ScrollButtonWidth="16"
												ScrollButtonHeight="17"
												ScrollBarCssClass="ScrollBar"
												ScrollGripCssClass="ScrollGrip"
												ClientSideOnDelete="onDeleteAsset"
												ClientSideOnCallbackError="onCallbackError"
												ScrollPopupClientTemplateId="ScrollPopupTemplate" 
												Sort="porder,name asc"
												Height="10" Width="100%"
												LoadingPanelPosition="TopCenter" 
												LoadingPanelClientTemplateId="LoadingFeedbackTemplate" 
												EnableViewState="False"
												GroupBySortImageHeight="10" 
												GroupBySortImageWidth="10" 
												GroupBySortDescendingImageUrl="group_desc.gif" 
												GroupBySortAscendingImageUrl="group_asc.gif" 
												GroupingNotificationTextCssClass="GridHeaderText" 
												AlternatingRowCssClass="AlternatingRowCategory" 
												IndentCellWidth="22" 
												TreeLineImageHeight="19" 
												TreeLineImageWidth="20" 
												TreeLineImagesFolderUrl="images/lines/" 
												PagerImagesFolderUrl="images/pager/" 
												ImagesBaseUrl="images/" 
												PreExpandOnGroup="True" 
												GroupingPageSize="5" 
												PagerTextCssClass="GridFooterTextCategory" 
												PagerStyle="Numbered" 
												PageSize="5" 
												GroupByTextCssClass="GroupByText" 
												GroupByCssClass="GroupByCell" 
												FooterCssClass="GridFooter" 
												HeaderCssClass="GridHeader" 
												SearchOnKeyPress="true" 
												SearchTextCssClass="GridHeaderText" 
												AllowEditing="true" 
												AllowSorting="False"
												ShowSearchBox="false" 
												ShowHeader="false" 
												ShowFooter="true" 
												CssClass="Grid" 
												RunningMode="callback" 
												ScrollBarWidth="15" 
												AllowPaging="true" >
												<ClientTemplates>
												<ComponentArt:ClientTemplate Id="EditTemplate">
														<a href="javascript:DownloadAssets('## DataItem.ClientId ##');"><img src="images/13Save2.gif" border=0 alt="Download"></a> | <a href="javascript:deleteRow('## DataItem.ClientId ##')"><img src="images/2DeletedItems.gif" alt="Delete" border=0></a>
														</ComponentArt:ClientTemplate>
														<ComponentArt:ClientTemplate Id="EditCommandTemplate">
															<a href="javascript:editRow();">Update</a> 
														</ComponentArt:ClientTemplate>
														<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
															<a href="javascript:insertRow();">Insert</a> 
														</ComponentArt:ClientTemplate>    
														<ComponentArt:ClientTemplate Id="TypeIconTemplate">
															<img src="## DataItem.GetMember("imagesource").Value ##" border="0"  > 
														</ComponentArt:ClientTemplate>        
														<ComponentArt:ClientTemplate Id="LookupProjectTemplate">
														<div style="height:45;width=45;border:0px solid;"><img src="images/spacer.gif" width=1 height=45><A href="## DataItem.GetMember("alink").Value ##"><img border=0 src="## DataItem.GetMember("timage").Value ##"  ></a></div>
														</ComponentArt:ClientTemplate> 
														<ComponentArt:ClientTemplate Id="LookupAssetPopupTemplate">
															<A href="#" onclick="javascript:PreviewOverlayOnSingleClickAssetsFromIcon('## DataItem.ClientId ##',event);"><img border=0 src="images/15printPreview.gif"></a>
														</ComponentArt:ClientTemplate> 
														<ComponentArt:ClientTemplate Id="LookupProjectTemplatePreview">
															<A href="javascript:AssetPreview('## DataItem.ClientId ##');"><img border=0 src="images/15printPreview.gif"></a>
														</ComponentArt:ClientTemplate>           
														<ComponentArt:ClientTemplate Id="LookupCategoryTemplate">
															<A href="javascript:GotoLocation('## DataItem.GetMember("ispost").Value ##','## DataItem.GetMember("category_id").Value ##');">## DataItem.GetMember("categoryname").Value ##</a>
														</ComponentArt:ClientTemplate>    
														<ComponentArt:ClientTemplate Id="LookupCategoryIconTemplate">
															<A href="javascript:GotoLocation('## DataItem.GetMember("ispost").Value ##','## DataItem.GetMember("category_id").Value ##');"><img border=0 src="images/categorytype## DataItem.GetMember("ispost").Value ##.gif"></a>
														</ComponentArt:ClientTemplate>    
														<componentart:ClientTemplate ID="LoadingFeedbackTemplate">
														<table cellspacing="0" cellpadding="0" border="0">
														<tr>
															<td style="font-size:10px;font-family:Verdana;"><img src="images/spacer.gif" height=25 width=1><br><img src="images/spinner.gif" width="16" height="16" border="0"> Loading... </td>
														</tr>
														</table>
														</componentart:ClientTemplate>                                 
												</ClientTemplates>
												

												<Levels>
												<componentart:GridLevel EditCellCssClass="EditDataCell"
															EditFieldCssClass="EditDataField"
															EditCommandClientTemplateId="EditCommandTemplate"
															InsertCommandClientTemplateId="InsertCommandTemplate" DataKeyField="asset_id" SelectedRowCssClass="SelectedRow" DataCellCssClass="DataCell" HeadingTextCssClass="HeadingCellText" SortAscendingImageUrl="spacer.gif" HeadingCellCssClass="HeadingCellAssets" ColumnReorderIndicatorImageUrl="spacer.gif" GroupHeadingCssClass="GroupHeading" HeadingCellHoverCssClass="HeadingCellHoverAssets" SortImageWidth="10" SortDescendingImageUrl="spacer.gif" HeadingRowCssClass="HeadingRowAssets" SortImageHeight="19" RowCssClass="Row" HeadingCellActiveCssClass="HeadingCellActive">
												<Columns>
												
												<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" Align="Center" AllowEditing="false" DataCellClientTemplateId="LookupProjectTemplate" HeadingCellCssClass="FirstHeadingCell" HeadingImageUrl="icon_icon.gif" HeadingImageWidth="14" HeadingImageHeight="16" AllowGrouping="false" Width="45" FixedWidth="True" />
												<ComponentArt:GridColumn Align="Center"  AllowEditing="false" DataCellClientTemplateId="TypeIconTemplate" dataField="imagesource"  DataCellCssClass="DataCell" HeadingImageUrl="icon_priority.gif" HeadingImageWidth="9" HeadingImageHeight="14" AllowGrouping="false" Width="19" FixedWidth="True" />
												<componentart:GridColumn DataCellCssClass="DataCell" AllowEditing="false" HeadingText="Name" Width="110" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell" DataField="name" ></componentart:GridColumn>
												<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Type" AllowEditing="false" Width="40" SortedDataCellCssClass="SortedDataCell" DataField="media_type_name" ></componentart:GridColumn>
												<componentart:GridColumn DataCellCssClass="DataCell" HeadingText="Uploaded" AllowEditing="false" Width="110"  FormatString="MMM dd yyyy, hh:mm tt" AllowGrouping="False" SortedDataCellCssClass="SortedDataCell"  DataField="update_date"></componentart:GridColumn>
												<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowEditing="false" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Width="150"  Align="Center" />
												
												
												
												
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="asset_id" SortedDataCellCssClass="SortedDataCell" DataField="asset_id"></componentart:GridColumn>
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="projectid" SortedDataCellCssClass="SortedDataCell" DataField="projectid"></componentart:GridColumn>
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="category_id" SortedDataCellCssClass="SortedDataCell" DataField="category_id"></componentart:GridColumn>
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="ispost" SortedDataCellCssClass="SortedDataCell" DataField="ispost"></componentart:GridColumn>
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="alink" SortedDataCellCssClass="SortedDataCell" DataField="alink"></componentart:GridColumn>
												<componentart:GridColumn AllowEditing="false" Visible="False" HeadingText="timage" SortedDataCellCssClass="SortedDataCell" DataField="timage"></componentart:GridColumn>

												</Columns>
												</componentart:GridLevel>
												</Levels>
												<ServerTemplates>
												<ComponentArt:GridServerTemplate Id="PickerTemplate">
            <Template>

 	   <input id="txtImageUrl" type="text" value="<%# Container.DataItem("name") %>"/>


            </Template>
          </ComponentArt:GridServerTemplate>
												

												
												
												</ServerTemplates>
											</COMPONENTART:GRID>
	</div>
	
	
	
	<div style="padding:10px;"><asp:Literal id="literalNoAssets" Text="No event assets available" visible=false runat="server" /></div>



	
	
			</ComponentArt:PageView>
		
	</ComponentArt:MultiPage>
	<div style="PADDING-BOTTOM:10px;WIDTH:100%;PADDING-TOP:10px;TEXT-ALIGN:left;align:right">
					<div style="text-align:right;"><br>
			<asp:Button id="btnProjectSave" runat="server" Text="Save"></asp:Button><img src="images/spacer.gif" width=5 height=1><input type="button" onclick="window.opener.RefreshEventItems();window.close();" value="Close" />
			</div>
	</div>
				</div>
			</div>
			</div>
			</div>
			</div>
			<script language="javascript">
TabStripCategory.SelectTabById('<%response.write (request.querystring("tab"))%>');

function showEventAssetAdd()
{
document.getElementById('tagcreateversion').style.display='block';
document.getElementById('tageventassets').style.display='none';
}


//setup form switch for delete or multi asset zip download
function DownloadAssets(ids){
	var sAssets;
	var downloadtype;
	sAssets = <%response.write( GridAssets.ClientId)%>.GetSelectedItems();
	ids = ids.replace('0 ','');
	var arraylist;
	var i;
	arraylist = '';
	if (sAssets.length > 1) {
			 if (confirm("Download selected assets?"))
			 {
				/*download multi*/
				for (i = 0; i < sAssets.length; i++) {
					if (arraylist == ''){
					arraylist = ',' + sAssets[i].GetMember("asset_id").Value;
					} else {
					if (arraylist.indexOf(',' +  sAssets[i].GetMember("asset_id").Value) == -1) {
					arraylist = arraylist + ',' +  sAssets[i].GetMember("asset_id").Value;
					}
					}
				}
				
				arraylist= arraylist.substring(1, arraylist.length);
				downloadtype = 'multi';
			 }
			 else
			 {
				/*download single*/
				downloadtype = 'single';
				
			 }
	}
	else {
	/*download single*/
	downloadtype = 'single';
	}
	if (downloadtype == 'multi') {
		if (arraylist != ''){
		window.open('<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownloadGetMulti & "?assetids="%>' + arraylist + '<%="&instance=" & IDAMInstance%>','ConvertingImage','width=500,height=75,location=no');
		}
	}
	if (downloadtype == 'single') {
	/*assume single*/
	window.location = '<%=idam5.BaseIDAMSession.Config.URLLocationIDAMDownload & "?dtype=assetdownload&assetid="%>' + ids + '<%="&instance=" & IDAMInstance%>&size=0';
	return true;
	}

}
	

  function onDeleteAsset(item)
  {

      if (confirm("Delete record?"))
        return true; 
      else
        return false; 

  }


  function deleteRow(ids)
  {
  
   
	/*assume single*/
	<% Response.Write(GridAssets.ClientID) %>.Delete(<% Response.Write(GridAssets.ClientID) %>.GetRowFromClientId(ids));


  }
  var isCreate;
  isCreate = '<%response.write(request.querystring("ID"))%>';
  if (isCreate!='')
  {
  document.getElementById('tagallowupload').style.display='block';  
  }

function refreshAssets()
{
<% Response.Write(GridAssets.ClientID) %>.Filter('name LIKE \'%' + '' + '%\'');
}
 setTimeout("refreshAssets();",200);

	</script>
		</form>
	</body>
</HTML>
