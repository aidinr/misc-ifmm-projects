<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ImportLdapUsers.aspx.vb" Inherits="IDAM5.ImportLdapUsers" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Import LDAP Active Directory Users</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<meta http-equiv="pragma" content="NO-CACHE">
		<meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
		<LINK href="splitterStyle.css" type="text/css" rel="stylesheet">
		<LINK href="common/baseStyle.css" type="text/css" rel="stylesheet">
		<LINK href="gridStyle.css" type="text/css" rel="stylesheet">
		<LINK href="snapStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStyle.css" type="text/css" rel="stylesheet">
		<LINK href="menuStyle.css" type="text/css" rel="stylesheet">
		<LINK href="multipageStyle.css" type="text/css" rel="stylesheet">
		<LINK href="treeStyle.css" type="text/css" rel="stylesheet">
		<LINK href="navStyle.css" type="text/css" rel="stylesheet">
		<LINK href="tabStripStyle.css" type="text/css" rel="stylesheet">
		<LINK href="navBarStyle.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body onload="this.focus" MS_POSITIONING="GridLayout">
		<form id="Form" method="post" runat="server">
			<asp:panel id="PanelPage4" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server"
				Height="288px" Width="792px" Visible="False">
				<DIV style="PADDING-RIGHT: 20px; PADDING-LEFT: 20px; PADDING-BOTTOM: 20px; PADDING-TOP: 20px; POSITION: relative">
					<DIV>
						<DIV style="BORDER-RIGHT: 1px solid; PADDING-RIGHT: 10px; BORDER-TOP: 1px solid; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; BORDER-LEFT: 1px solid; PADDING-TOP: 10px; BORDER-BOTTOM: 1px solid; POSITION: relative; BACKGROUND-COLOR: white">
							<TABLE>
								<TR>
									<TD><IMG src="images/project34.gif"></TD>
									<TD>Import LDAP user wizard</TD>
								</TR>
							</TABLE>
							<DIV style="BORDER-RIGHT: #b7b4b4 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #b7b4b4 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: normal; FONT-SIZE: 10px; PADDING-BOTTOM: 5px; BORDER-LEFT: #b7b4b4 1px solid; WIDTH: 100%; COLOR: #3f3f3f; PADDING-TOP: 5px; BORDER-BOTTOM: #b7b4b4 1px solid; FONT-FAMILY: verdana; BACKGROUND-COLOR: #e4e4e4">Step 
								3. LDAP Import Complete.</DIV>
							<P></P>
							<asp:Label id="lblConfirm" runat="server"   Font-Name="Verdana">LDAP users successfully imported</asp:Label>
							<P></P>
							<P>
								<asp:ListBox id="ListBoxResult" runat="server" Width="360px" Height="504px" Enabled="False"></asp:ListBox></P>
							<P>
								<DIV align="right"><INPUT onclick="window.opener.NavigateToPage('Group');window.close();" type="button" value="Close"></DIV>
						</DIV>
					</DIV>
				</DIV>
				<P></P>
			</asp:panel><br>
			<br>
			<asp:panel id="PanelPage2" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server"
				Height="288px" Width="792px" Visible="False">
				<SCRIPT type="text/javascript">
  
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
   
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
   
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    LdapUsersGrid.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    LdapUsersGrid.Edit(LdapUsersGrid.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    LdapUsersGrid.EditComplete();     
  }

  function insertRow()
  {
    LdapUsersGrid.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    LdapUsersGrid.Delete(LdapUsersGrid.GetRowFromClientId(rowId)); 
  }

   
				</SCRIPT>
				<DIV style="PADDING-RIGHT: 20px; PADDING-LEFT: 20px; PADDING-BOTTOM: 20px; PADDING-TOP: 20px; POSITION: relative">
					<DIV>
						<DIV style="BORDER-RIGHT: 1px solid; PADDING-RIGHT: 10px; BORDER-TOP: 1px solid; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; BORDER-LEFT: 1px solid; PADDING-TOP: 10px; BORDER-BOTTOM: 1px solid; POSITION: relative; BACKGROUND-COLOR: white">
							<TABLE>
								<TR>
									<TD><IMG src="images/project34.gif"></TD>
									<TD>Import LDAP user wizard</TD>
								</TR>
							</TABLE>
							<DIV style="BORDER-RIGHT: #b7b4b4 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #b7b4b4 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: normal; FONT-SIZE: 10px; PADDING-BOTTOM: 5px; BORDER-LEFT: #b7b4b4 1px solid; WIDTH: 100%; COLOR: #3f3f3f; PADDING-TOP: 5px; BORDER-BOTTOM: #b7b4b4 1px solid; FONT-FAMILY: verdana; BACKGROUND-COLOR: #e4e4e4">Step 
								2. Select the users you wanted to import to iDAM user database.</DIV>
							<P></P>
							<COMPONENTART:GRID id="LdapUsersGrid" runat="server" Width="100%" Height="207" RunningMode="Callback"
								CssClass="Grid" GroupByTextCssClass="GroupByText" GroupingNotificationTextCssClass="GridHeaderText"
								ShowFooter="false" PageSize="20" ImagesBaseUrl="images/" EditOnClickSelectedItem="false" AllowEditing="true"
								Sort="login" ScrollBar="On" ScrollTopBottomImagesEnabled="true" ScrollTopBottomImageHeight="2"
								ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/" ScrollButtonWidth="16"
								ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip" ScrollBarWidth="16"
								ScrollPopupClientTemplateId="ScrollPopupTemplate" AutoCallBackOnDelete="false" AutoCallBackOnInsert="false"
								AutoCallBackOnUpdate="false" PagerStyle="Numbered" PagerTextCssClass="PagerText">
								<Levels>
									<ComponentArt:GridLevel DataKeyField="login" ShowTableHeading="false" ShowSelectorCells="false" HeadingCellCssClass="HeadingCell"
										HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive" HeadingTextCssClass="HeadingCellText"
										DataCellCssClass="DataCell" RowCssClass="Row" SelectedRowCssClass="SelectedRow" SortAscendingImageUrl="asc.gif"
										SortDescendingImageUrl="desc.gif" ColumnReorderIndicatorImageUrl="reorder.gif" SortedDataCellCssClass="SortedDataCell"
										SortImageWidth="14" SortImageHeight="14" EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField"
										EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate">
										<Columns>
											<ComponentArt:GridColumn DataField="Import" HeadingText="Import" ColumnType="CheckBox" Width="20" Visible="true" />
											<ComponentArt:GridColumn DataField="ImportStatus" HeadingText="Import Status" AllowEditing="false" Width="100"
												Visible="true" />
											<ComponentArt:GridColumn DataField="Login" HeadingText="User Name" Width="100" AllowEditing="false" Visible="true" />
											<ComponentArt:GridColumn DataField="SecurityLevelId" HeadingText="Security Level" ForeignTable="SecurityLevel"
												ForeignDataKeyField="SecuritylevelId" ForeignDisplayField="name" Width="100" Visible="true" />
											<ComponentArt:GridColumn HeadingText="Edit" AllowSorting="False" DataCellClientTemplateId="EditTemplate"
												EditControlType="EditCommand" Width="100" Align="Center" FixedWidth="True" />
											<ComponentArt:GridColumn DataField="FirstName" HeadingText="First Name" AllowEditing="false" Width="100"
												FixedWidth="True" />
											<ComponentArt:GridColumn DataField="LastName" HeadingText="Last Name" Width="100" FixedWidth="True" AllowEditing="false" />
											<ComponentArt:GridColumn DataField="Email" HeadingText="Email Address" Width="100" FixedWidth="True" AllowEditing="false" />
											<ComponentArt:GridColumn DataField="Position" HeadingText="Title" Width="50" FixedWidth="True" AllowEditing="false" />
											<ComponentArt:GridColumn DataField="Department" HeadingText="department" Width="50" FixedWidth="True" AllowEditing="false" />
											<ComponentArt:GridColumn DataField="Phone" HeadingText="phone" Width="50" FixedWidth="True" AllowEditing="false" />
											<ComponentArt:GridColumn DataField="Cell" HeadingText="cell" Width="50" FixedWidth="True" AllowEditing="false" />
											<ComponentArt:GridColumn DataField="WorkAddress" HeadingText="workaddress" Width="50" FixedWidth="True" AllowEditing="false" />
											<ComponentArt:GridColumn DataField="WorkCity" HeadingText="workcity" Width="50" FixedWidth="True" AllowEditing="false" />
											<ComponentArt:GridColumn DataField="WorkState" HeadingText="workstate" Width="50" FixedWidth="True" AllowEditing="false" />
											<ComponentArt:GridColumn DataField="WorkZip" HeadingText="workzip" Width="50" FixedWidth="True" AllowEditing="false" />
											<ComponentArt:GridColumn DataField="WorkCountry" HeadingText="workcountry" Width="50" FixedWidth="True" AllowEditing="false" />
										</Columns>
									</ComponentArt:GridLevel>
								</Levels>
								<ClientTemplates>
									<ComponentArt:ClientTemplate Id="EditTemplate">
										<a href="javascript:editGrid('## DataItem.ClientId ##');">Edit</a>
									</ComponentArt:ClientTemplate>
									<ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> | <a href="javascript:LdapUsersGrid.EditCancel();">Cancel</a>
          </ComponentArt:ClientTemplate>
									<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> | <a href="javascript:LdapUsersGrid.EditCancel();">Cancel</a>
          </ComponentArt:ClientTemplate>
									<ComponentArt:ClientTemplate Id="LoadingFeedbackTemplate">
										<table cellspacing="0" cellpadding="0" border="0">
											<tr>
												<td style="font-size:10px;">Loading...&nbsp;</td>
												<td><img src="images/spinner.gif" width="16" height="16" border="0"></td>
											</tr>
										</table>
									</ComponentArt:ClientTemplate>
									<ComponentArt:ClientTemplate Id="Column1TemplateEdit">
										<table cellspacing="0" cellpadding="0" border="0">
											<tr>
												<td><img src="images/9.gif" width="16" height="16" border="0"></td>
												<td style="padding-left:2px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;"><nobr>## 
															DataItem.GetMember("Asset_id").Value ##</nobr></div>
												</td>
											</tr>
										</table>
									</ComponentArt:ClientTemplate>
									<ComponentArt:ClientTemplate Id="ScrollPopupTemplate">
										<table cellspacing="0" cellpadding="2" border="0" class="ScrollPopup">
											<tr>
												<td style="width:20px;"><img src="images/9.gif" width="16" height="16" border="0"></td>
												<td style="width:100px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;width:130px;">
														<nobr>## DataItem.GetMember("login").Text ##</nobr></div>
												</td>
												<td style="width:200px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;width:130px;"><nobr>## 
															DataItem.GetMember("firstname").Text ##</nobr></div>
												</td>
												<td style="width:200px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;width:130px;"><nobr>## 
															DataItem.GetMember("lastname").Text ##</nobr></div>
												</td>
												<td style="width:200px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;width:130px;"><nobr>## 
															DataItem.GetMember("email").Text ##</nobr></div>
												</td>
											</tr>
										</table>
									</ComponentArt:ClientTemplate>
								</ClientTemplates>
							</COMPONENTART:GRID>
							<P></P>
							<asp:label id="lblMsg2" runat="server" Visible="False" Width="632px" Font-Name="Verdana" ForeColor="Red">Label</asp:label>
							<P>
								<asp:CheckBox id="checkRefreshLDAPUsers" runat="server" Font-Name="verdana" Text="Update IDAM user data with LDAP information"></asp:CheckBox><BR>
								<asp:CheckBox id="chkImportNewUsers" runat="server" Font-Name="verdana" Text="Import new users"></asp:CheckBox><BR>
								<asp:CheckBox id="chkForceLDAPAuthentication" runat="server" Font-Name="verdana"
									Text="Force LDAP Authentication for new imported users"></asp:CheckBox></P>
							<P><INPUT id="btnDeleteAll" onclick="CheckAllItems(LdapUsersGrid,0)" type="button" value="Select All"
									name="btnDeleteAll">&nbsp; <INPUT id="btnImportSelected" type="button" value="Import Selected" name="btnImportSelected"
									runat="server"></P>
						</DIV>
					</DIV>
				</DIV>
			</asp:panel><asp:panel id="PanelPage1" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server"
				Height="288px" Width="792px" Visible="False">
				<DIV style="PADDING-RIGHT: 20px; PADDING-LEFT: 20px; PADDING-BOTTOM: 20px; PADDING-TOP: 20px; POSITION: relative">
					<DIV>
						<DIV style="BORDER-RIGHT: 1px solid; PADDING-RIGHT: 10px; BORDER-TOP: 1px solid; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; BORDER-LEFT: 1px solid; PADDING-TOP: 10px; BORDER-BOTTOM: 1px solid; POSITION: relative; BACKGROUND-COLOR: white">
							<TABLE>
								<TR>
									<TD><IMG src="images/project34.gif"></TD>
									<TD>Import LDAP user wizard</TD>
								</TR>
							</TABLE>
							<DIV style="BORDER-RIGHT: #b7b4b4 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #b7b4b4 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: normal; FONT-SIZE: 10px; PADDING-BOTTOM: 5px; BORDER-LEFT: #b7b4b4 1px solid; WIDTH: 100%; COLOR: #3f3f3f; PADDING-TOP: 5px; BORDER-BOTTOM: #b7b4b4 1px solid; FONT-FAMILY: verdana; BACKGROUND-COLOR: #e4e4e4">Step 
								1. Retrieve LDAP users by entering a valid LDAP username and password.</DIV>
							<BR>
							<asp:label id="lblMsg" runat="server" Visible="False" Width="632px" Font-Name="Verdana" ForeColor="Red">Label</asp:label>
							<TABLE width="100%" border="0">
								<TR>
									<TD style="HEIGHT: 6px" align="right" width="200"><FONT face="Verdana" size="1">LDAP 
											Login</FONT></TD>
									<TD style="HEIGHT: 13px">
										<asp:TextBox id="txtLogin" runat="server"></asp:TextBox></TD>
								</TR>
								<TR>
									<TD align="right" width="200"><FONT face="Verdana" size="1">LDAP Password</FONT></TD>
									<TD>
										<asp:TextBox id="txtPassword" runat="server" TextMode="Password"></asp:TextBox></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 75px" align="right" width="200"><FONT face="Verdana" size="1">LDAP 
											Domain</FONT></TD>
									<TD style="HEIGHT: 75px">
										<asp:TextBox id="txtDomain" runat="server"></asp:TextBox></TD>
								</TR>
								<TR>
									<TD align="right" colSpan="2"><INPUT onclick="window.opener.NavigateToPage('Group');window.close();" type="button" value="Close">
										<asp:Button id="btnRetrieveLDAPUsers" runat="server" Width="64px" Text="Next"></asp:Button></TD>
								</TR>
							</TABLE>
						</DIV>
					</DIV>
				</DIV>
			</asp:panel></form>
	</body>
</HTML>
