<%@ Control Language="vb" AutoEventWireup="false" Codebehind="preferences.ascx.vb" Inherits="IDAM5.Preferences" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<div class="previewpaneAll" id="toolbar">
	<!--Project cookie and top menu-->
	<table cellspacing="0" cellpadding="4" id="table2" width="100%">
		<tr>
			<td valign="top" width="167">
				<font face="Verdana" size="1"><b>My Preferences</b><br>
				</font>
			</td>
			<td valign="top">
			</td>
			<td valign="top" align="right">
				<div style="PADDING-TOP:2px">
					
				</div>
			</td>
		</tr>
		<tr>
			<td valign="top" colspan="3">
			</td>
		</tr>
	</table>
</div> <!--end preview pane-->
<div class="previewpaneSub">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle" width="100%">
				<div style="PADDING-RIGHT:0px;PADDING-LEFT:0px;PADDING-BOTTOM:0px;PADDING-TOP:0px;align:right">
					<b>
						<asp:HyperLink CssClass="projecttabs" id="link_ProjectGeneral" runat="server">General</asp:HyperLink></b>
				</div>
			</td>
			<td id="Test" align="right" width="100%">
				<img src="images/spacer.gif" height="16" width="1">
			</td>
		</tr>
	</table>
</div>
<div class="previewpaneProjects" id="toolbar">
	<script type="text/javascript">
  
  function onInsert(item)
  {
    if (document.getElementById('chkConfirmInsert').checked)
      if (confirm("Insert record?"))
        return true; 
      else
        return false; 
    else
        return true; 
  }
  
  function onUpdate(item)
  {
    if (document.getElementById('chkConfirmUpdate').checked)
      if (confirm("Update record?"))
        return true; 
      else
        return false; 
    else
      return true; 
  }

  function onCallbackError(excString)
  {
    if (confirm('Invalid data has been entered. View details?')) alert(excString); 
    <%=GridPreferences.ClientID %>.Page(1); 
  }

  function onDelete(item)
  {
    if (document.getElementById('chkConfirmDelete').checked)
    {
      if (confirm("Delete record?"))
        return true; 
      else
        return false; 
    }
    else
      return true; 
  }
  
  function editGrid(rowId)
  {
    <%=GridPreferences.ClientID%>.Edit(<%=GridPreferences.ClientID %>.GetRowFromClientId(rowId)); 
  }
  
  function editRow()
  {
    <%=GridPreferences.ClientID %>.EditComplete();     
  }

  function insertRow()
  {
    <%=GridPreferences.ClientID %>.EditComplete(); 
  }

  function deleteRow(rowId)
  {
    <%=GridPreferences.ClientID %>.Delete(GridPreferences.GetRowFromClientId(rowId)); 
  }

      
	</script>
	<br>
	<br>
	<div style="BORDER-RIGHT:#b7b4b4 1px solid; PADDING-RIGHT:5px; BORDER-TOP:#b7b4b4 1px solid; PADDING-LEFT:5px; FONT-WEIGHT:normal; FONT-SIZE:10px; PADDING-BOTTOM:5px; BORDER-LEFT:#b7b4b4 1px solid; COLOR:#3f3f3f; PADDING-TOP:5px; BORDER-BOTTOM:#b7b4b4 1px solid; FONT-FAMILY:verdana; BACKGROUND-COLOR:#e4e4e4">Modify 
		Preferences by clicking the edit links.</div>
	<br>
	<div>
		<ComponentArt:Grid id="GridPreferences" RunningMode="Callback" CssClass="Grid" GroupByTextCssClass="GroupByText"
			GroupingNotificationTextCssClass="GridHeaderText" ShowFooter="false" PageSize="20" ImagesBaseUrl="images/"
			EditOnClickSelectedItem="true" AllowEditing="true" Sort="Preference" ScrollBar="Off" ScrollTopBottomImagesEnabled="true"
			ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="images/scroller/"
			ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" ScrollGripCssClass="ScrollGrip"
			ScrollBarWidth="16" ScrollPopupClientTemplateId="ScrollPopupTemplate" Width="100%" Height="207"
			AutoCallBackOnDelete="true" AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" runat="server"
			PagerStyle="Numbered" PagerTextCssClass="PagerText">
			<Levels>
				<ComponentArt:GridLevel DataKeyField="PreferenceKey" ShowTableHeading="false" ShowSelectorCells="false"
					HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive"
					HeadingTextCssClass="HeadingCellText" DataCellCssClass="DataCell" RowCssClass="Row" SelectedRowCssClass="SelectedRow"
					SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" ColumnReorderIndicatorImageUrl="reorder.gif"
					SortedDataCellCssClass="SortedDataCell" SortImageWidth="14" SortImageHeight="14" EditCellCssClass="EditDataCell"
					EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate">
					<Columns>
						<ComponentArt:GridColumn DataField="PreferenceKey" HeadingText="PreferenceKey" Visible="false" />
						<ComponentArt:GridColumn DataCellCssClass="FirstDataCellPostings" AllowEditing="False" DataField="Preference"
							HeadingText="Preference Name" />
						<ComponentArt:GridColumn DataField="Value" HeadingText="Value" Width="50" FixedWidth="True" />
						<ComponentArt:GridColumn DataCellCssClass="LastDataCellPostings" HeadingText="Action" AllowSorting="False"
							DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Width="100" Align="Center" FixedWidth="True" />
					</Columns>
				</ComponentArt:GridLevel>
			</Levels>
			<ClientTemplates>
				<ComponentArt:ClientTemplate Id="EditTemplate">
					<a href="javascript:editGrid('## DataItem.ClientId ##');">Edit</a>
				</ComponentArt:ClientTemplate>
				<ComponentArt:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();">Update</a> | <a href="javascript:_ctl0_GridPreferences.EditCancel();">Cancel</a>
          </ComponentArt:ClientTemplate>
				<ComponentArt:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();">Insert</a> | <a href="javascript:_ctl0_GridPreferences.EditCancel();">Cancel</a>
          </ComponentArt:ClientTemplate>
				<ComponentArt:ClientTemplate Id="LoadingFeedbackTemplate">
					<table cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td style="font-size:10px;">Loading...&nbsp;</td>
							<td><img src="images/spinner.gif" width="16" height="16" border="0"></td>
						</tr>
					</table>
				</ComponentArt:ClientTemplate>
				<ComponentArt:ClientTemplate Id="Column1TemplateEdit">
					<table cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td><img src="images/9.gif" width="16" height="16" border="0"></td>
							<td style="padding-left:2px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;"><nobr>## 
										DataItem.GetMember("Preference").Value ##</nobr></div>
							</td>
						</tr>
					</table>
				</ComponentArt:ClientTemplate>
				<ComponentArt:ClientTemplate Id="ScrollPopupTemplate">
					<table cellspacing="0" cellpadding="2" border="0" class="ScrollPopup">
						<tr>
							<td style="width:20px;"><img src="images/9.gif" width="16" height="16" border="0"></td>
							<td style="width:130px;"><div style="font-size:10px;font-family: MS Sans Serif;text-overflow:ellipsis;overflow:hidden;width:130px;"><nobr>## 
										DataItem.GetMember("Preference").Value ##</nobr></div>
							</td>
						</tr>
					</table>
				</ComponentArt:ClientTemplate>
			</ClientTemplates>
		</ComponentArt:Grid>
		<br>
	</div>
</div>
