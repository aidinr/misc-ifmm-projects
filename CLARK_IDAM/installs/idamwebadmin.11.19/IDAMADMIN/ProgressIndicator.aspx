<%@ Control Language="vb" AutoEventWireup="false" Codebehind="ProgressIndicator.uplx.vb" Inherits="IDAM5.ProgressIndicator" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>


<html>
<Head>
<STYLE type="text/css">
   body {font-family: Trebuchet,Arial,Helvetica; font-size: 10px}
 </STYLE>


<%
	'--- If the upload isn't complete, continue to refresh
	If intPercentComplete < 100 Then
		bDone = False
		Response.Write("<Meta HTTP-EQUIV=""Refresh"" CONTENT=1>")
	Else
		bDone = True
		
	End If
%>
<title>iDAM Upload Progress...</title>
</head>
<Body>
<%if bDone then%>
<SCRIPT LANGUAGE="JavaScript">
window.close();
</SCRIPT>
<%end if%>
<TABLE border=0 bordercolor="#3333CC" cellspacing=0 cellpadding=2 width="574">
<TR>	
<TD colspan=3><font size="1">Uploading to iDAM...</font></TD>
<TD colspan=2><B><font size="1">Status: <%If bDone Then Response.Write("Complete!") Else Response.Write("Sending") End If%></font></B><font size="2">
</font> 
</TR>
<TR><TD align=center bgColor="#C0C0C0" width="14"> <font size="1">ID </font> </TD>
	<TD align=left  align=left width=279 bgColor="#C0C0C0"><font size="1">Graphic Indicator</font></TD>
	<TD align=center  bgColor="#C0C0C0" width="93"><font size="1">Uploaded Bytes</font></TD>
	<TD  align=center  align=center bgColor="#C0C0C0" width="56"><font size="1">Total Bytes</font></TD>
	<TD bgColor="#C0C0C0" width="112"><font size="1">Upload Percentage</font></TD>
</TR>
<TR><TD align=left width="14"><font size="1"><%=oFileUpProgress.progressid%></font></TD>
	<TD>

		<TABLE bordercolor="black" border=0 cellspacing=0 ALIGN="left" WIDTH="<%=intPercentComplete%>%">
		<TR>
			<TD align=right width="100%" BGCOLOR="blue"><B><font size="1" color="#FFFFFF"><%=intPercentComplete%>%</font></B></TD>
		</TR>
		</TABLE>
<%
	Response.Write("</TD>")
	Response.Write "<TD align=center><font size='1'>" & intBytesTransferred & "</font></TD>"
	
	if oFileUpProgress.totalbytes > 0 then
		Response.Write("<TD align=center><font size='1'>" & intTotalBytes & "</TD>" & _
		"<TD align=center><font size='1'>" & intPercentComplete & "%</font></TD>" )
	else
		Response.Write ("<TD align=center><font size='1'>" & "N/A" & "</TD>" & _
		"<TD align=center><font size='1'>" & "N/A" & "</font></TD>")
	end if
	Response.Write("</TR>")
	
%>
      <h3>Upload Progress:</h3>
      <!-- 
      This table holds cells that are used to display
      the upload progress for the progress indicator
      -->
      <asp:Table id="UploadTable" 
           GridLines="Both" 
           HorizontalAlign="Center" 
           Font-Name="Verdana" 
           Font-Size="8pt" 
           CellPadding="15" 
           CellSpacing="0" 
           runat="server">
	
	<asp:tablerow>
		<asp:tablecell><B>Progress ID2</B></asp:tablecell>
		<asp:tablecell width="300"><B>Graphic</B></asp:tablecell>
		<asp:tablecell><B>Transferred Bytes</B></asp:tablecell>
		<asp:tablecell><B>Total Bytes</B></asp:tablecell>
		<asp:tablecell><B>Percentage</B></asp:tablecell>
	</asp:tablerow>
	<asp:tablerow>
		<asp:tablecell runat="server" id="cellProgressID" text="0" />
		<asp:tablecell runat="server" id="cellGraphic">
			<HR align="left" style="color:blue" size="10" width="<%=strPercent%>">
		</asp:tablecell>
		<asp:tablecell runat="server" id="cellTransferredBytes" />
		<asp:tablecell runat="server" id="cellTotalBytes" />
		<asp:tablecell runat="server" id="cellPercentage" />
	</asp:tablerow>
	</asp:table>
</Table>
</Body>
</Html>
  
