<%@ Control Language="vb" AutoEventWireup="false" Codebehind="Portal.ascx.vb" Inherits="IDAM5.Portal" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<div class="previewpaneMyProjects" id="toolbar">
	<!--Project cookie and top menu-->
		<table cellspacing="0" cellpadding="4" id="table2" width="100%">
			<tr>
			<td width="1" nowrap valign="top">
			<img src="images/ui/home_ico_bg.gif" height=42 width=42>
			
			
			</td>
			
			<td valign="top">
			<font face="Verdana" size="3" color=gray><b>Welcome, <%=Session("user_name")%>!</b></font><font face="Verdana" size="1" color=black> [ <a href="login.aspx?Logout=True">log out</a> ]<br>
				<font face="Verdana" size="1" color=black><asp:Literal ID=literalPortalHeading Runat=server ></asp:Literal></font></font>
			</td>
			<td id="Test" valign="top" align="right">
			<font face="Verdana" size="1" color=black><script>writeDate();</script></font>

							
			<!--<div style="padding-top:2px;">
			[ <a href="#">help</a> ]
				</div>-->
			</td></tr>
			<tr><td valign="top" colspan="2">
			</td>
			</tr>
		</table>
</div> <!--end preview pane-->




<div class="previewpaneSub">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left"  nowrap valign="middle" width="100%">
			
			<div style="align:right;padding:10px;font-size:14px;overflow:hidden;">
								<asp:Literal Runat=server ID=portalsubmenuitems></asp:Literal>								
								</div>
			</td>
			<td align="right"  width="100%" >
				<!--<asp:Image onclick="javascript:switchToThumbnail();" id="list_icon"   runat="server"></asp:Image><asp:Image onclick="javascript:switchToThumbnail();"  id="thumb_icon" runat="server"></asp:Image>-->
				<img src=images/spacer.gif height=16 width=1>
			</td>
		</tr>
	</table>
</div>

<style>
.projectcentertest
{
width:100%;
height:100%;
/*z-index:100;*/
/*padding-left:3px;
padding-right:3px;*/
}

</style>
<div class="previewpaneProjects" style="PADDING-TOP:10px" id="toolbar2">

				<!--TABLE TOP-->
				<table width=100% cellpadding=0 cellspacing=0><tr><td width=100% align=left valign=top >
					<div id="projectright" class="projectcentertest">
						<div class="projectcentertest" style="padding-right:5px;">
	
	<table width=100% cellpadding=0 cellspacing=0 border=0>
	<tr>
	<td width=50%  valign=top >
							<DIV class="Dock" id="PortalColumn1">
								<asp:PlaceHolder id="controlloadercolumn1" runat="server"></asp:PlaceHolder>
							</DIV> <!--Dock-->
	</td>
	<td width=100%  valign=top >
							<DIV class="Dock" id="PortalColumn2">
								<asp:PlaceHolder id="controlloadercolumn2" runat="server"></asp:PlaceHolder>	
							</DIV> <!--Dock-->
	</td>
	<td width=100%  valign=top >
							<DIV class="Dock" id="PortalColumn3">
								<asp:PlaceHolder id="controlloadercolumn3" runat="server"></asp:PlaceHolder>	
							</DIV> <!--Dock-->
	</td>
	</tr>
	</table>
					
					</div>
				</div>
			</td></tr></table>
		
</div>	
<script type="text/javascript">
//<![CDATA[    ]	
function showtrailpreload(imageid,title)
{
	showtrail('<%=IDAM5.BaseIDAMSession.Config.URLLocationPreview%>?id=' + imageid + '&amp;instance=<%response.write (IDAM5.BaseIDAMSession.Config.IDAMInstance)%>&amp;type=asset&amp;size=2&amp;height=399&amp;width=370',title,'','5','1',270,7);
}

function PreviewOverlayOnSingleClickAssetsFromIcon(itemvaluetmp,itemvaluenametmp,itemvaluefiletypetmp,eventObject)
  {
	try
	{
		var itemvaluetmp;
		var itemvaluenametmp;
		var itemvaluefiletypetmp;
		<% Response.Write(CallbackProjectAssetOverlay.ClientID) %>.Callback(itemvaluetmp +',Asset'+','+itemvaluenametmp+','+itemvaluefiletypetmp);		 
	}  //end try  
	catch(err)
	{
	txt="There was an error on this page.\n\n"
	txt+="Error description: " + err.description
	alert(txt);
	}
}
//]]>
</script>	



							
					<div id=trailimageid>
									<div class="SnapProjectsWrapperPopup" style="width:599px;">					
										<COMPONENTART:CALLBACK id="CallbackProjectAssetOverlay" runat="server" CacheContent="false">
											<CONTENT>
												<asp:PlaceHolder ID="PlaceHolderAssetPreviewPage" runat="server">
													<!--assetOptions-->
													<!--
													<div class='assetOptionsPopup'>
													
													
													
													
													
														<table border="0" width="94%" cellspacing="0" cellpadding="0" id="table1">
															<tr>
																<td >

																[<font size="1"><asp:Literal ID="LiteralAssetPopupLinksDownload" Runat=server></asp:Literal>
																download</a></font></td>
																

																
																																
																<td nowrap >

																<font size="1"> ] [ <a href="asfdas">export
																</a> </font>
															</td>
																<td nowrap >

																<font size="1"> 
															<select size="1" name="D1">
															<option>Select Type</option>
															<option>TIFF Large</option>
																</select></font></td>
																<td >

																 ] [ <a href="asd"><font size="1">reconvert</font></a><font size="1">
																</font>
									
  																</td>
																<td >

																<font size="1"> ] [ <asp:Literal ID="LiteralAssetPopupLinksCarousel" Runat=server></asp:Literal>add to 
																carrousel</a></font></td>
																<td ">

																<font size="1"> ] [ <asp:Literal ID="LiteralAssetPopupLinksView" Runat=server></asp:Literal>view 
																details</a></font></td>
																<td>

															 ] [ <a href="#ee"><font size="1">delete</font></a><font size="1"> ]
																</font>
												
  																</td>
															</tr>
														</table>
														
														
													</div>--><!--END assetOptions-->
													
													<!--SnapProjectsOverlay-->
													<div class="SnapProjectsOverlay" >
														<font face="Verdana" size="1">
														<table id="table3">
															<tr>

																<td>
														
																<font size="1"><b>Image:</b></font></span><font size="1">
																</font></td>

																<td align="left" >

																<font size="1"><asp:Literal ID="ProjectAssetOverlayCurrentImageName" Runat=server></asp:Literal></font></td>
												
  															</tr>
															<tr >

																<td><font size="1"><b>Location(s):</b></font></span><font size="1">
																</font></td>

																<td align="left" >
																
																<font size="1"><asp:Literal ID="ProjectAssetOverlayCurrentProjectName" Runat=server></asp:Literal></font></a><font size="1">
																</font></td>

															</tr>
															</table>
														<br>
														<asp:Image id="ProjectAssetOverlayCurrentImage" runat="server" ImageUrl="~/common/images/spacer.gif"></asp:Image>
														
														<!--<br>
													[ <asp:Literal ID="LiteralRotateRight" Runat=server></asp:Literal>rotate right</a> ] [ <asp:Literal ID="LiteralRotateLeft" Runat=server></asp:Literal>rotate left</a> ]-->
													<br>
														<table id="table2">
															<tr>

																<td>
														
																<font size="1">Description:</font></span><font size="1">
																</font></td>
										
																<td align="left" width="80%">

																<font size="1"><asp:Literal ID="LiteralAssetDescription" Runat=server></asp:Literal></font></td>
												
  															</tr>
															<tr >
																<td><font size="1">Owner</font><font size="1">:</font></span><font size="1">
																</font></td>
																<td align="left" width="80%">
																
																<font size="1"><asp:Literal ID="LiteralAssetOwner" Runat=server></asp:Literal></font></a><font size="1">
																</font></td>
															</tr>
															<tr >
																<td noWrap>
																
																<font size="1">Date Created:</font></span><font size="1">
																</font></td>
																<td>
																<font size="1"><asp:Literal ID="LiteralAssetDateCreated" Runat=server></asp:Literal></font></span><font size="1">
																</font></td>
															</tr>
															<tr>


																<td noWrap><font size="1">Type</font><font size="1">:</font></span><font size="1">
																</font></td>
												
																<td>
																<font size="1"><asp:Literal ID="LiteralAssetType" Runat=server></asp:Literal></font></td>
											

  															</tr>
															<tr>

																<td noWrap><font size="1">File Size</font><font size="1">:</font></span><font size="1">
																</font></td>

																<td>
																<font size="1"> 
																
																<asp:Literal ID="LiteralAssetSize" Runat=server></asp:Literal></a> </font>
																</td>
									
  															</tr>
															<tr>

																<td noWrap>
										
																<font size="1">Keywords:</font></span><font size="1">
																</font></td>
										
																<td>
																<font size="1"><asp:Literal ID="LiteralAssetKeywords" Runat=server></asp:Literal></font></td>
											
  															</tr>
															
														</table>
													</div><!--END SnapProjectsOverlay-->
												</asp:PlaceHolder>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<div class="SnapProjectsOverlay" >
													<TABLE cellSpacing="0" cellPadding="0" border="0" height="500" width=100%>
														<TR>
															<TD align="center">
																<TABLE cellSpacing="0" cellPadding="0" border="0">
																	<TR>
																		<TD style="FONT-SIZE: 10px">Loading...
																		</TD>
																		<TD><IMG height="16" src="images/spinner.gif" width="16" border="0"></TD>
																	</TR>
																</TABLE>
															</TD>
															<td><IMG height="16" src="images/spacer.gif" width="16" height="500" border="0">
															</td>
														</TR>
													</TABLE>
												</div>
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
									</div><!--padding-->
								
								
								</div>
							<!--END SnapProjectAssetOverlay-->