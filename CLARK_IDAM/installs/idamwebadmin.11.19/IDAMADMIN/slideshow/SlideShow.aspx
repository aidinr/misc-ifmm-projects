<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SlideShow.aspx.vb" Inherits="IDAM5.SlideShow1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>iDAM Slideshow</title>


	<!-- begin css -->
	<script language=javascript type=text/javascript src="yg_csstare.js"></script>
	<link rel="stylesheet" href="base.css" type="text/css">
	<link rel="stylesheet" href="yph2.css" type="text/css">
    <style type="text/css">
        .yphbox { background-color:#000000;border:1px solid #aaa;margin-bottom:20px; }
        .yphbox .yphsphdr { display:block;margin:10px 0 0 10px; }
        .yphbox ul { margin-top:5px;margin-bottom:10px;margin-left:27px;padding:0; }

        .yphne, .yphse { float:right; margin-right: -1px; }
        .yphnw, .yphsw { float:left; margin-left:-1px; }
        /* IE5-Win CSS \*/
        .yphne, .yphse { float:right; margin-right: -4px; }
        .yphnw, .yphsw { float:left; margin-left:-4px; }
        /* End hack */

        .yphne, .yphnw { margin-top: -1px; }
        .yphsw, .yphse { margin-top: -5px; }
        img[class=yphnw], img[class=yphsw] { margin-left:-1px; }
        img[class=yphne], img[class=yphse] { margin-right: -1px; }

		.yphshade2 { display:table;width:0;padding:0 2px 2px 0;border:0;background-image:url('gray_back.gif'); }

		@media all { button { display:inline; } }
    </style>
	<!-- end css -->

<script language="Javascript" src="dhtmllib.js"></script>
<script language="Javascript">

//var slideshow_end_url="javascript:window.close();";
var slideshow_end_url="<%="http://" & Request.ServerVariables("HTTP_HOST").ToString & Request.ServerVariables("URL").ToString & "?" & Request.ServerVariables("QUERY_STRING").ToString%>";

function swapLoop(){
if (slideshow_end_url == 'javascript:window.close();') {
	slideshow_end_url="<%="http://" & Request.ServerVariables("HTTP_HOST").ToString & Request.ServerVariables("URL").ToString & "?" & Request.ServerVariables("QUERY_STRING").ToString%>";
} else {
slideshow_end_url="javascript:window.close();";
}
//alert(slideshow_end_url);
}  
//alert(slideshow_end_url);
var imageSrcArray = new Array(<%=spimageSrcArray%>);

var imageNameArray = new Array(<%=spimageNameArray%>);

var imageDescArray = new Array(<%=spimageDescriptionArray%>);

var imageDateArray = new Array(<%=spimageUpdateDateArray%>);

var imageCreatorArray = new Array(<%=spimageUpdateDateArray%>);

var imageWidthArray = new Array(<%=spimageWidthSrcArray%>);

var imageHeightArray = new Array(<%=spimageHeightSrcArray%>);

var buttonImgPfx = "";
var buttonOnArray = new Array("bt_d_play.gif",
			      "bt_d_stop.gif",
			      "bt_d_rwd.gif",
			      "bt_d_fwd.gif"
			      );
var buttonOffArray = new Array("bt_u_play.gif",
			      "bt_u_stop.gif",
			      "bt_u_rwd.gif",
			      "bt_u_fwd.gif"
			       );

var numImgs = <%=sprsAssetsTotal%>;
var begImg  = 0;
var SHOWINGSTRING="Showing Slide %slideNum of <%=sprsAssetsTotal%>";
var POSTED1="Posted:";
var POSTED2="Posted:&nbsp;%date&nbsp;by&nbsp;%creator";
var RESOLUTION="Resolution:";
var arrPreload = new Array();
var SHOWCREATOR=false;

<%if request.querystring(".spd") <> "" then%>                 
var spd = <%=trim(request.querystring(".spd"))%>;
<%else%>
var spd = 2;
<%end if%>
function init()
{
    preloadRange(0,_PRELOADRANGE-1);
    window.onResize = function()
    {
		if (isMinNS4) {
			var mydir = "/280b";
			var urlstr = "http://test/slideshow?.dir=" + mydir + "&.view=";
			window.open(urlstr, "_top");
		}
    }

    curImg = begImg;

    if (curImg < 0 || curImg > numImgs - 1)
	curImg = numImgs - 1;

    changeSlide();

    interval = intervalm;


    play();

}
// -->
</script>
<script language="Javascript" src="slideshow.js"></script>
<script language="Javascript">
<% if request.querystring(".spd") <> "" then%>
changeSpeed(<%=request.querystring(".spd")%>);
set speedMenu.selectedIndex = <%=request.querystring(".spd")%>;
<%else%>
changeSpeed(2);
<%end if%>
</script> 
<style type="text/css">
#slideDiv {position:relative; color:#ffffff;}
#imgDiv {position:absolute; top:0; left:0; width:738; height:675; clip:rect(0,738,700,0); z-index:0; background-color:#000000; layer-background-color:#000000; color:#ffffff;}
#pnumBgDiv {position:relative; color:#ffffff;}
#pnumDiv {position:absolute; top:0; left:0; width:200; height:30; clip:rect(0,200,30,0); z-index:0 color:#ffffff;}
</style>
</head>

<body bgcolor=#000000 onLoad=init() >
<center>


<table border=0 cellpadding=0 cellspacing=5 width=750>
	<tr><td align=left style="padding-left:5px;"><a onClick="javascript:window.close();" href="#">
		<font color="#FFFFFF" face="Verdana" size="1">Close</font></a></td></tr>
</table>
<table border=0 cellpadding=0 cellspacing=0 width=750>
	<tr><td colspan=3 class=yphbrdr height=1><spacer type=block width=1 height=1></td></tr>
	<tr>
		<td width=1 class=yphbrdr><form><spacer type=block width=1 height=1></td>
		<td class=yphsectbr>
			<table border=0 cellpadding=0 cellspacing=0 width=748><tr><td height=4><spacer type=block width=1 height=1></td></tr></table>
			<table border=0 cellpadding=0 cellspacing=0 width=748>
				<tr>
					<td width=10><spacer type=block width=1 height=1></td>
					<td width=250>
<div id="pnumBgDiv"><div id="pnumDiv">
<em id="pem" style="font-style: normal; font-family:verdana color:white"></em>
</div></div>
					</td>
					<!-- replace these buttons with bt_d_(name).gif for down state --> 
					<td width=228 align=center>&nbsp;<a href="javascript:play()"><img src=bt_u_play.gif alt="Play" name="playbtn" border=0 align=absmiddle></a>&nbsp;<a href="javascript:stop()"><img src=bt_u_stop.gif alt="Stop" name="stopbtn" border=0 align=absmiddle></a>&nbsp;<a href="javascript:setButton(0); rewind();"><img src=bt_u_rwd.gif alt="Previous" name="prevbtn" border=0 align=absmiddle></a>&nbsp;<a href="javascript:setButton(1); forward();"><img src=bt_u_fwd.gif border=0 alt="Next" name="fowdbtn" align=absmiddle></a></td>
					<td width=250 align=right>
					<font color="#FFFFFF"><small>Loop Slideshow </small> 
					</font> 
					<input onclick="javascript:swapLoop();" type="checkbox" name="C1" value="ON" checked><font color="#FFFFFF"><small>&nbsp;&nbsp;&nbsp; Speed</small> 
					</font> <select name="speedMenu" onchange="changeSpeed(this.selectedIndex);"><option selected value=normal>Normal</option><option value=slow>Slow</option><option value=fast>Fast</option></select></td>
					<td width=10><spacer type=block width=1 height=1></td>
				</tr>
			</table>
			<table border=0 cellpadding=0 cellspacing=0 width=748><tr><td height=4><spacer type=block width=1 height=1></td></tr></table>
		</td>
		<td width=1 class=yphbrdr><spacer type=block width=1 height=1></td>
	</tr>
	<!-- gray shadow -->
	<tr><td colspan=3 class=height=1><spacer type=block width=1 height=1></td></tr>
	<tr><td colspan=3 bgcolor=000000 height=1><spacer type=block width=1 height=1></form></td></tr>
</table>
<table border=0 cellpadding=0 cellspacing=0 width=750 onClick="javascript:window.close();">
	<tr>
		<td class=yphbrdr width=1 rowspan=2><spacer type=block width=1 height=1></td><td width=748>

<div id="slideDiv">
    <div id="imgDiv">
    <div id="imgp" style="width:100%;"></div>
    </div>
</div>
</td>
		<td class=yphbrdr width=1 rowspan=2><spacer type=block width=1 height=1></td >
	</tr>
	<tr><td height=675 width=748>&nbsp; </td> </tr>
</table>
<table border=0 cellpadding=0 cellspacing=0 width=750>
	<tr valign=top>
		<td rowspan=2 width=1></td>
		<td width=738 height=5><spacer type=block width=1 height=1></td>
		<td rowspan=2 width=1></td>
	</tr>
	<tr><td height=1 class=yphbrdr><spacer type=block width=1 height=1></td></tr>
</table>
<table border=0 cellpadding=0 cellspacing=0><tr><td height=5><spacer type=block height=5></td></tr></table>

<p><b><font color="#FFFFFF" face="Verdana" size="1">Click the image to close 
the window.</font></b></p>

<table cellpadding=0 cellspacing=0 border=0>
	<tr><td height=15><spacer type=block height=15></td></tr>
</table>


<table width=750 cellpadding=0 cellspacing=0 border=0>
	<tr><td align=center></td></tr>
</table>
</center>

</body>
</html>