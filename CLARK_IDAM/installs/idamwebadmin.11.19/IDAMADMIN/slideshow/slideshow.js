var curImg = 0;
var timerId = -1;
var intervalf = 3000;
var intervalm = 10000;
var intervals = 15000;
var interval = intervalm;
var imgIsLoaded = false;
var arrPreload = new Array();
var _PRELOADRANGE = 5;

function replaceNum(myinput, token, newstr)
{
    var input = myinput;
    var output = input;
    var idx = output.indexOf(token);
    if (idx > -1) 
	{
		output = input.substring(0, idx);
		output += newstr;
		output += input.substr(idx+token.length);
    }
    return output;
}

function changeSpeed(sidx)
{
if (sidx == ''){
sidx = 1;
}
    switch (sidx) 
	{
		case 0: interval = intervalm; break;
		case 1: interval = intervals; break;
		default: interval = intervalf;
    }
    if (timerId != -1) 
	{
		window.clearInterval(timerId);
		timerId = window.setInterval("forward();", interval);
    }
}

function preloadRange(intPic,intRange) {
	for (var i=intPic; i<intPic+intRange; i++) {
		arrPreload[i] = new Image();
		arrPreload[i].src = imageSrcArray[i];
	} 
	return true;
}

function imgLoadNotify()
{
    imgIsLoaded = true;
}

function changeSlide()
{
    if (document.all)
	{
    	document.all.imgp.style.filter="blendTrans(duration=1)";
    	document.all.imgp.filters.blendTrans.Apply();
    }
    imgIsLoaded = false;
    var htmlCont = "<center>" +
	"<br><img src=\"" + imageSrcArray[curImg] + "\" alt=\"" + imageNameArray[curImg] + "\"";
    if (imageWidthArray[curImg] > 0 && imageHeightArray[curImg] > 0) 
	{
		htmlCont += (imageHeightArray[curImg]>600)?" height=600":" width=" + imageWidthArray[curImg] + " height=" + imageHeightArray[curImg];
    }
    htmlCont += " border=0 hspace=10 vspace=10 onload=\"imgLoadNotify();\">" + "<br>";
	htmlCont += "<br><span class=yphsmhdr>" + imageNameArray[curImg] + "</span><br><small>" + imageDescArray[curImg] + "</small></center>";

    var pnumLine = "<small>";
    pnumLine += replaceNum(SHOWINGSTRING, "%slideNum", eval(curImg+1));
    pnumLine += "</small>";
    document.getElementById("pem").innerHTML = pnumLine;
    document.getElementById("imgp").innerHTML = htmlCont;
    if (document.all) 
		document.all.imgp.filters.blendTrans.Play();
}

function forward()
{
	if (!arrPreload[curImg+1])
	{
		imgIsLoaded = false;
		imgIsLoaded = (curImg+_PRELOADRANGE<numImgs)?preloadRange(curImg+1,_PRELOADRANGE):preloadRange(curImg+1,numImgs-curImg-1);
		curImg++;
		if (curImg >= numImgs) { finish(); } else { setTimeout("changeSlide()",500); }
	} else {
		curImg++;
		if (curImg >= numImgs) { finish(); } else { changeSlide(); }
	}
}

function rewind()
{
	curImg--;
	if (curImg < 0)
//		curImg = numImgs - 1;
		finish();
	else
		changeSlide();
}

function stop()
{
    window.clearInterval(timerId);
    timerId = -1;
    document.playbtn.src = buttonImgPfx + buttonOffArray[0];
    document.stopbtn.src = buttonImgPfx + buttonOnArray[1];
    imgIsLoaded = true;
}

function play()
{
    if (timerId == -1) 
		timerId = window.setInterval('forward();', interval);
    document.playbtn.src = buttonImgPfx + buttonOnArray[0];
    document.stopbtn.src = buttonImgPfx + buttonOffArray[1];
}

function setButton(direction)
{
	if (timerId != -1) { window.clearInterval(timerId); timerId = window.setInterval("forward();", interval); }
    imgIsLoaded = true;
    if (direction == 0) 
	{
		document.prevbtn.src = buttonImgPfx + buttonOnArray[2];
		window.setTimeout("document.prevbtn.src = buttonImgPfx + buttonOffArray[2];", 300);
    }
	else 
	{
		document.fowdbtn.src = buttonImgPfx + buttonOnArray[3];
		window.setTimeout("document.fowdbtn.src = buttonImgPfx + buttonOffArray[3];", 300);
    }
}

function finish()
{
	stop();

	if (typeof(slideshow_end_url) != "undefined")
	{
		if (interval == intervals)
			slideshow_end_url += "&.spd=1";
		else if (interval == intervalf)
			slideshow_end_url += "&.spd=3";
		document.location=slideshow_end_url;
	}
}

