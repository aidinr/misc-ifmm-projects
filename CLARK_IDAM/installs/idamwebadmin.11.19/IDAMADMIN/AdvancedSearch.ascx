<%@ Control Language="vb" AutoEventWireup="false" Codebehind="AdvancedSearch.ascx.vb" Inherits="IDAM5.AdvancedSearch" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<div class="previewpaneHeadingProjects" id="toolbar">
	<!--Project cookie and top menu-->
	<table cellspacing="0" cellpadding="4" id="table2" width="100%">
		<tr>
			<td valign="top" width="1" nowrap >
				<!--<img style="BORDER-RIGHT:#cbcbcb 4px solid; BORDER-TOP:#cbcbcb 4px solid; BORDER-LEFT:#cbcbcb 4px solid; COLOR:#cbcbcb; BORDER-BOTTOM:#cbcbcb 4px solid; BACKGROUND-COLOR:#cbcbcb"
					src="images/search19rev.gif">-->
					<img src="images/ui/search_ico_bg.gif" height=42 width=42>
			</td>
			<td valign="top">
				<font face="Verdana" size="1"><b>
						<asp:Label id="lblheading" runat="server"></asp:Label></b><br>
						Advanced search enables you to access more detailed parameters for searching<br>for both assets and projects.  Choose the options below to filter your search results.
				</font>
			</td>
			<td id="Test" valign="top" align="right">
				<div style="PADDING-TOP:2px">
					<!--[ <a href="#">help</a> ]-->
				</div>
			</td>
		</tr>
		<tr>
			<td valign="top" colspan="3">
			</td>
		</tr>
	</table>
</div> <!--end preview pane-->
<div class="previewpaneSub">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle" width="100%">
				<div style="PADDING-RIGHT:0px;PADDING-LEFT:0px;PADDING-BOTTOM:0px;PADDING-TOP:0px;align:right">
					<b>
						<asp:HyperLink CssClass="projecttabs" id="link_ProjectGeneral" NavigateUrl="javascript:gotopagelink('AdvancedAssets');"
							runat="server">General</asp:HyperLink></b>
				</div>
			</td>
			<td align="right" width="100%">
				<!--<asp:Image onclick="javascript:switchToThumbnail();" id="list_icon"   runat="server"></asp:Image><asp:Image onclick="javascript:switchToThumbnail();"  id="thumb_icon" runat="server"></asp:Image>-->
				<img src="images/spacer.gif" height="16" width="1">
			</td>
		</tr>
	</table>
</div>
<script type="text/javascript">

    function ExecuteSubmit()
    {
    document.forms[0].action="IDAM.aspx?page=Results";
    document.forms[0].submit();

    }
    
    function closealltags()
    {


	<% Response.Write(TagUserDefinedFields.ClientID) %>.Collapse();

	<% Response.Write(TagKeywordServices.ClientID) %>.Collapse();

	<% Response.Write(TagKeywordMediaType.ClientID) %>.Collapse();

	<% Response.Write(TagKeywordIllustType.ClientID) %>.Collapse();
	<% Response.Write(TagAssetDate.ClientID) %>.Collapse();

	<% Response.Write(TagAssetSize.ClientID) %>.Collapse();

	<% Response.Write(SnapProjectAddress.ClientID) %>.Collapse();
	<% Response.Write(SnapProjectUDFS.ClientID) %>.Collapse();
	<% Response.Write(SnapProjectDisciplines.ClientID) %>.Collapse();
	<% Response.Write(SnapProjectKeywords.ClientID) %>.Collapse();
	<% Response.Write(SnapProjectOffices.ClientID) %>.Collapse();		
	<% Response.Write(SnapProjectDate.ClientID) %>.Collapse();		
	<% Response.Write(SnapProjectDate.ClientID) %>.Collapse();	
				
	
    }
    
    
    function openalltags()
    {


	<% Response.Write(TagUserDefinedFields.ClientID) %>.Expand();
	<% Response.Write(TagKeywordServices.ClientID) %>.Expand();
	<% Response.Write(TagKeywordMediaType.ClientID) %>.Expand();
	<% Response.Write(TagKeywordIllustType.ClientID) %>.Expand();
	<% Response.Write(TagAssetDate.ClientID) %>.Expand();
	<% Response.Write(TagAssetSize.ClientID) %>.Expand();
		
	<% Response.Write(SnapProjectAddress.ClientID) %>.Expand();
	<% Response.Write(SnapProjectUDFS.ClientID) %>.Expand();
	<% Response.Write(SnapProjectDisciplines.ClientID) %>.Expand();
	<% Response.Write(SnapProjectKeywords.ClientID) %>.Expand();
	<% Response.Write(SnapProjectOffices.ClientID) %>.Expand();	
	<% Response.Write(SnapProjectDate.ClientID) %>.Expand();	
	
	<% Response.Write(SnapProjectDate.ClientID) %>.Expand();	
    }    

</script>
<SCRIPT LANGUAGE="JavaScript" SRC="js/CalendarPopup.js"></SCRIPT>
<div class="previewpaneProjects" id="toolbar">
	<!--END Project cookie and top menu-->
	<!--projectcentermain-->
	<div id="projectcentermain" style="PADDING-RIGHT: 10px;PADDING-LEFT: 10px;PADDING-TOP: 20px">
		<br>
		<table class="HeadingCell2" style="WIDTH:100%;HEIGHT:25px" cellpadding="0" cellspacing="0">
			<tr>
				<td class="HeadingCellText2"><b>Advanced Search</b></td>
				<td align="right" style="FONT-SIZE: 10px; FONT-FAMILY: Verdana">[<a href="javascript:closealltags();">close 
						all</a>] [<a href="javascript:openalltags();">open all</a>]</td>
			</tr>
		</table>
		<div class="SnapTreeviewTag" style="HEIGHT:100%;BACKGROUND-COLOR:white">
			<div class="carouselshortlist">
				<COMPONENTART:CALLBACK id="CALLBACKInformation" runat="server" CacheContent="false">
					<CONTENT>
						<asp:PlaceHolder ID="PlaceholderTagInformation" runat="server">
<br>
Search String(s):<br>
<asp:TextBox id="keyword" TextMode="SingleLine" cssclass="InputFieldMain" runat="server" style="width:220px"></asp:TextBox><br><br>
<input class="preferences" type="radio" value="all" name="KeywordSearchType" checked>
with <B>all</B> of the words</FONT><br>
<input class="preferences" type="radio" value="exact" name="KeywordSearchType">
with the <B>exact phrase</B></FONT><br>
<input class="preferences" type="radio" value="atleastone" name="KeywordSearchType">
with <B>at least one</B> of the words</FONT><br><br>

<input type="button" class="button" onclick="javascript:ExecuteSubmit();" value="Search"><br>


<br>
<div style="padding:5px;background-color: white;border:1px solid #B7B4B4;  font-family: verdana; color: #3F3F3F; 
  font-size: 10px;
  font-weight: normal;">
								<input class="preferences" type="radio" value="asset" name="SearchType" checked onclick="javascript:setoptions('assetoptions');">
								Asset Options</FONT><br>
								<input class="preferences" type="radio" value="project" name="SearchType" onclick="javascript:setoptions('projectoptions');">
								Project Options</B></FONT><br>
								<!--<input class="preferences" type="radio" value="people" name="SearchType" onclick="javascript:setoptions('peopleoptionsxx');">
								People Options</FONT>-->
							</div>
<br>





</asp:PlaceHolder>
					</CONTENT>
					<LOADINGPANELCLIENTTEMPLATE>
						<IMG height="16" src="images/spinner.gif" width="16" border="0">
					</LOADINGPANELCLIENTTEMPLATE>
				</COMPONENTART:CALLBACK>
			</div> <!--carouselshortlist-->
		</div> <!--SnapTreeview-->
		<div id="assetoptions">
			<div class="preferences" style="PADDING-LEFT:25px"><b>Search Options (Assets)</b></div>
			<div style="PADDING-RIGHT:10px;PADDING-LEFT:35px;PADDING-BOTTOM:5px;PADDING-TOP:15px">
				<div id="AssetLeftColumn" style="VERTICAL-ALIGN:top;HEIGHT:2500px">
					<!--<ComponentArt:Snap id="TagSecurity" runat="server" Fillcontainer="false" Height="20px" MustBeDocked="true"
					DockingStyle="TransparentRectangle" CurrentDockingIndex="1" DraggingStyle="GhostCopy" CurrentDockingContainer="AssetLeftColumn"
					DockingContainers="AssetLeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false" AutoCallBackOnExpand="false">
					<Header>
						<div style="CURSOR: move; width: 100%;">
							<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagSecurity.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
									<td onmousedown="<% Response.Write(TagSecurity.ClientID) %>.StartDragging(event);" >Asset 
										Type</td>
								</tr>
							</table>
						</div>
					</Header>
					<CollapsedHeader>
						<div style="CURSOR: move; width: 100%;">
							<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagSecurity.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
									<td onmousedown="<% Response.Write(TagSecurity.ClientID) %>.StartDragging(event);">Asset 
										Type</td>
								</tr>
							</table>
						</div>
					</CollapsedHeader>
					<Content>
						<div class="SnapTreeviewTag" style="height:100%;background-color:white;">
							<div class="carouselshortlist">
								<asp:Literal ID="LiteralFileType" Runat="server"></asp:Literal>
								<br>
								<img src="images/spacer.gif" width="1" height="4" border="0"><br>
								<br>
								<br>
								<br>
								<input type="button" class="button" value="Search"><br>
								<br>
								<br>
							</div>
						</div> 
					</Content>
				</ComponentArt:Snap>
				<div class="SnapHeaderProjectsFiller"></div>-->
					<ComponentArt:Snap id="TagAssetDate" runat="server" Fillcontainer="false" Height="20px" MustBeDocked="true"
						DockingStyle="TransparentRectangle" CurrentDockingIndex="0" DraggingStyle="GhostCopy" CurrentDockingContainer="AssetLeftColumn"
						DockingContainers="AssetLeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false" AutoCallBackOnExpand="false">
						<Header>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagAssetDate.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(TagAssetDate.ClientID) %>.StartDragging(event);" >Asset 
											Date</td>
									</tr>
								</table>
							</div>
						</Header>
						<CollapsedHeader>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagAssetDate.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(TagAssetDate.ClientID) %>.StartDragging(event);">Asset 
											Date</td>
									</tr>
								</table>
							</div>
						</CollapsedHeader>
						<Content>
							<div class="SnapTreeviewTag" style="height:100%;background-color:white;">
								<div class="carouselshortlist">
									<br>
									<br>
									<div id="assetfilterdatetag">
										Date type:<br>
										<select class="preferences" id="adtcreatemodify" name="adtcreatemodify">
											<option value="1" selected>Last Modified Date</option>
											<option value="0">Uploaded Date</option>
											<option value="2">Created Date</option>
										</select><br>
										<br>
										<b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="#666666"><input class="preferences" type="radio" value="0" name="adt">in 
											the last <input type="text" size="9" name="adtmonth"> months<br>
										</font><b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="#666666"><input class="preferences" type="radio" value="1" name="adt">in 
											the last <input type="text" size="9" name="adtdays"> days<br>
										</font><b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="#666666"><input class="preferences" type="radio" value="2" name="adt">between 
											&nbsp; <input type="text" name="adtbetweendate" size="9"></font>
										<SCRIPT LANGUAGE="JavaScript">
var cal_1 = new CalendarPopup();
cal_1.showYearNavigation();
										</SCRIPT>
										<SCRIPT LANGUAGE="JavaScript">
var cal_2 = new CalendarPopup();
cal_2.showYearNavigation();
										</SCRIPT>
										<A HREF="#" onClick="cal_1.select(document.forms[0].adtbetweendate,'anchor1','MM/dd/yyyy'); return false;"
											NAME="anchor1" ID="anchor1"><font face="Arial" size="1">select a date</font></A><br>
										<b><img border="0" src="images/spacer.gif" height="7" width="63"></b><font face="Arial" size="1" color="#666666">and&nbsp;&nbsp;
											<input type="text" name="adtanddate" size="9"> </font><A HREF="#" onClick="cal_2.select(document.forms[0].adtanddate,'anchor2','MM/dd/yyyy'); return false;"
											NAME="anchor2" ID="anchor2"><font face="Arial" size="1">select a date</font></A><font face="Arial" size="1" color="#666666"><br>
										</font>
										<br>
										<br>
										<br>
										<input type="button" class="button" onclick="javascript:ExecuteSubmit();" value="Search"><br>
										<br>
										<br>
									</div> <!--carouselshortlist-->
								</div> <!--SnapTreeview-->
							</div>
						</Content>
					</ComponentArt:Snap>
					<div class="SnapHeaderProjectsFiller"></div>
					<ComponentArt:Snap id="TagUserDefinedFields" runat="server" Fillcontainer="false" Height="20px" MustBeDocked="true"
						DockingStyle="TransparentRectangle" CurrentDockingIndex="1" DraggingStyle="GhostCopy" CurrentDockingContainer="AssetLeftColumn"
						DockingContainers="AssetLeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false" AutoCallBackOnExpand="false">
						<Header>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagUserDefinedFields.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(TagUserDefinedFields.ClientID) %>.StartDragging(event);" >UDF</td>
									</tr>
								</table>
							</div>
						</Header>
						<CollapsedHeader>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagUserDefinedFields.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(TagUserDefinedFields.ClientID) %>.StartDragging(event);">UDF</td>
									</tr>
								</table>
							</div>
						</CollapsedHeader>
						<Content>
							<div class="SnapTreeviewTag" style="height:100%;background-color:white;">
								<div class="carouselshortlist">
									<COMPONENTART:CALLBACK id="CALLBACKUDFMain" runat="server" CacheContent="false">
										<CONTENT>
											<asp:Literal ID="Literal_UDFMAIN" Runat="server"></asp:Literal>
										</CONTENT>
										<LOADINGPANELCLIENTTEMPLATE>
											<IMG height="16" src="images/spinner.gif" width="16" border="0">
										</LOADINGPANELCLIENTTEMPLATE>
									</COMPONENTART:CALLBACK>
									<br>
									<input type="button" class="button" onclick="javascript:ExecuteSubmit();" value="Search"><br>
									<br>
									<br>
								</div> <!--carouselshortlist-->
							</div> <!--SnapTreeview-->
						</Content>
					</ComponentArt:Snap>
					<div class="SnapHeaderProjectsFiller"></div>
					<ComponentArt:Snap id="TagKeywordServices" runat="server" Fillcontainer="false" Height="20px" MustBeDocked="true"
						DockingStyle="TransparentRectangle" CurrentDockingIndex="2" DraggingStyle="GhostCopy" CurrentDockingContainer="AssetLeftColumn"
						DockingContainers="AssetLeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false" AutoCallBackOnExpand="false">
						<Header>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagKeywordServices.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(TagKeywordServices.ClientID) %>.StartDragging(event);" > 
											<asp:Literal  Runat=server ID="ltrlKeywordFilter"></asp:Literal></td>
									</tr>
								</table>
							</div>
						</Header>
						<CollapsedHeader>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagKeywordServices.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(TagKeywordServices.ClientID) %>.StartDragging(event);"> 
											<asp:Literal  Runat=server ID="ltrlKeywordFilter2"></asp:Literal></td>
									</tr>
								</table>
							</div>
						</CollapsedHeader>
						<Content>
							<div class="SnapTreeviewTag" style="height:100%;background-color:white;">
								<div class="SnapProjectsSearch">
									<asp:Literal ID="LiteralKeywords1Title" Runat="server"></asp:Literal><br>
									<img src="images/spacer.gif" width="1" height="4" border="0"><br>
									<input class="preferences" type="radio" value="and" name="KeywordServicesSearchType" checked>
									And (assets matching all selected keywords)<br>
									<input class="preferences" type="radio" value="or" name="KeywordServicesSearchType">
									Or (assets matching any selected keywords)
									<br>
									<br>
									<div style="width: 100%;float:left;">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsServices" runat="server" CacheContent="false">
											<CONTENT>
												<asp:Literal ID="LiteralKeywords1" Runat="server"></asp:Literal>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
									</div>
									<br>
									<br>
									<br>
									<div style="width: 100%;float:left;">
										<input type="button" class="button" onclick="javascript:ExecuteSubmit();" value="Search"><br>
									</div>
									<br>
									<br>
								</div> <!--carouselshortlist-->
							</div> <!--SnapTreeview-->
						</Content>
					</ComponentArt:Snap>
					<div class="SnapHeaderProjectsFiller"></div>
					<ComponentArt:Snap id="TagKeywordMediaType" runat="server" Fillcontainer="false" Height="20px" MustBeDocked="true"
						DockingStyle="TransparentRectangle" CurrentDockingIndex="3" DraggingStyle="GhostCopy" CurrentDockingContainer="AssetLeftColumn"
						DockingContainers="AssetLeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false" AutoCallBackOnExpand="false">
						<Header>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagKeywordMediaType.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(TagKeywordMediaType.ClientID) %>.StartDragging(event);" > 
											<asp:Literal  Runat=server ID=ltrlMediaTypeFilter></asp:Literal></td>
									</tr>
								</table>
							</div>
						</Header>
						<CollapsedHeader>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagKeywordMediaType.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(TagKeywordMediaType.ClientID) %>.StartDragging(event);"> 
											<asp:Literal Runat=server ID=ltrlMediaTypeFilter2></asp:Literal></td>
									</tr>
								</table>
							</div>
						</CollapsedHeader>
						<Content>
							<div class="SnapTreeviewTag" style="height:100%;background-color:white;">
								<div class="SnapProjectsSearch">
									<asp:Literal ID="LiteralKeywords2Title" Runat="server"></asp:Literal><br>
									<input class="preferences" type="radio" value="and" name="KeywordMediaTypeSearchType" checked>
									And (assets matching all selected keywords)<br>
									<input class="preferences" type="radio" value="or" name="KeywordMediaTypeSearchType">
									Or (assets matching any selected keywords)
									<br>
									<br>
									<div style="width: 100%;float:left;">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsMediaType" runat="server" CacheContent="false">
											<CONTENT>
												<asp:Literal ID="LiteralKeywords2" Runat="server"></asp:Literal>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
									</div>
									<br>
									<br>
									<br>
									<div style="width: 100%;float:left;">
										<input type="button" class="button" onclick="javascript:ExecuteSubmit();" value="Search"><br>
									</div>
									<br>
									<br>
								</div> <!--carouselshortlist-->
							</div> <!--SnapTreeview-->
						</Content>
					</ComponentArt:Snap>
					<div class="SnapHeaderProjectsFiller"></div>
					<ComponentArt:Snap id="TagKeywordIllustType" runat="server" Fillcontainer="false" Height="20px" MustBeDocked="true"
						DockingStyle="TransparentRectangle" CurrentDockingIndex="4" DraggingStyle="GhostCopy" CurrentDockingContainer="AssetLeftColumn"
						DockingContainers="AssetLeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false" AutoCallBackOnExpand="false">
						<Header>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagKeywordIllustType.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(TagKeywordIllustType.ClientID) %>.StartDragging(event);" > 
											<asp:Literal  Runat=server ID="ltrlIllustTypeFilter"></asp:Literal></td>
									</tr>
								</table>
							</div>
						</Header>
						<CollapsedHeader>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagKeywordIllustType.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(TagKeywordIllustType.ClientID) %>.StartDragging(event);"> 
											<asp:Literal  Runat=server ID="ltrlIllustTypeFilter2"></asp:Literal></td>
									</tr>
								</table>
							</div>
						</CollapsedHeader>
						<Content>
							<div class="SnapTreeviewTag" style="height:100%;background-color:white;">
								<div class="SnapProjectsSearch">
									<asp:Literal ID="LiteralKeywords3Title" Runat="server"></asp:Literal><br>
									<img src="images/spacer.gif" width="1" height="4" border="0"><br>
									<input class="preferences" type="radio" value="and" name="KeywordIllustTypeSearchType"
										checked> And (assets matching all selected keywords)<br>
									<input class="preferences" type="radio" value="or" name="KeywordIllustTypeSearchType">
									Or (assets matching any selected keywords)
									<br>
									<br>
									<div style="width: 100%;float:left;">
										<COMPONENTART:CALLBACK id="CALLBACKKeywordsIllustType" runat="server" CacheContent="false">
											<CONTENT>
												<asp:Literal ID="LiteralKeywords3" Runat="server"></asp:Literal>
											</CONTENT>
											<LOADINGPANELCLIENTTEMPLATE>
												<IMG height="16" src="images/spinner.gif" width="16" border="0">
											</LOADINGPANELCLIENTTEMPLATE>
										</COMPONENTART:CALLBACK>
									</div>
									<br>
									<br>
									<br>
									<div style="width: 100%;float:left;">
										<input type="button" class="button" onclick="javascript:ExecuteSubmit();" value="Search"><br>
									</div>
									<br>
									<br>
								</div> <!--carouselshortlist-->
							</div> <!--SnapTreeview-->
						</Content>
					</ComponentArt:Snap>
					<div class="SnapHeaderProjectsFiller"></div>
					<ComponentArt:Snap id="TagAssetSize" runat="server" Fillcontainer="false" Height="20px" MustBeDocked="true"
						DockingStyle="TransparentRectangle" CurrentDockingIndex="5" DraggingStyle="GhostCopy" CurrentDockingContainer="AssetLeftColumn"
						DockingContainers="AssetLeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false" AutoCallBackOnExpand="false">
						<Header>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagAssetSize.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(TagAssetSize.ClientID) %>.StartDragging(event);" >Asset 
											Size</td>
									</tr>
								</table>
							</div>
						</Header>
						<CollapsedHeader>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(TagAssetSize.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(TagAssetSize.ClientID) %>.StartDragging(event);">Asset 
											Size</td>
									</tr>
								</table>
							</div>
						</CollapsedHeader>
						<Content>
							<div class="SnapTreeviewTag" style="height:100%;background-color:white;">
								<div class="carouselshortlist">
									<div id="assetfiltersizetag">
										<font face="Arial" size="1" color="#666666">
											<br>
										</font><b><img border="0" src="images/spacer.gif" height="7" width="21"></b><select size="1" class="preferences" name="asizeleastmost">
											<option value="1">at least</option>
											<option value="0">at most</option>
										</select><font face="Arial" size="1" color="#666666"> <input type="text" size="9" name="asize">
											KB<br>
											<br>
										</font>
									</div>
									<br>
									<br>
									<input type="button" class="button" onclick="javascript:ExecuteSubmit();" value="Search"><br>
									<br>
									<br>
								</div> <!--carouselshortlist-->
							</div> <!--SnapTreeview-->
						</Content>
					</ComponentArt:Snap>
					<div class="SnapHeaderProjectsFiller"></div>
				</div>
			</div>
			<br>
			<img src="images/spacer.gif" width="1" height="1500"><br>
		</div> <!--asset options-->
		
		
		
		
		
		
		
		
		
		
		<div id="projectoptions">
			<div class="preferences" style="PADDING-LEFT:25px"><b>Search Options (Projects)</b></div>
			<div style="PADDING-RIGHT:10px;PADDING-LEFT:35px;PADDING-BOTTOM:5px;PADDING-TOP:15px">
				<div id="ProjectLeftColumn" style="VERTICAL-ALIGN:top;HEIGHT:2500px">
					<ComponentArt:Snap id="SnapProjectAddress" runat="server" Fillcontainer="false" Height="20px" MustBeDocked="true"
						DockingStyle="TransparentRectangle" CurrentDockingIndex="0" DraggingStyle="GhostCopy" CurrentDockingContainer="ProjectLeftColumn"
						DockingContainers="ProjectLeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false"
						AutoCallBackOnExpand="false">
						<Header>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SnapProjectAddress.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(SnapProjectAddress.ClientID) %>.StartDragging(event);" >Project 
											Address</td>
									</tr>
								</table>
							</div>
						</Header>
						<CollapsedHeader>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SnapProjectAddress.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(SnapProjectAddress.ClientID) %>.StartDragging(event);">Project 
											Address</td>
									</tr>
								</table>
							</div>
						</CollapsedHeader>
						<Content>
							<div class="SnapTreeviewTag" style="background-color:white;">
								<div class="carouselshortlist">
									<br>
									<br>
									<b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="">Address<u><br>
										</u></font><b><img border="0" src="images/spacer.gif" height="7" width="45"></b><font face="Arial" size="1" color="#666666"><input type="text" class="InputFieldMain" size="30" name="Address"></font><u><font face="Arial" size="1" color="#CC6600"><br>
										</font></u><b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="">City<u><br>
										</u></font><b><img border="0" src="images/spacer.gif" height="7" width="45"></b><font face="Arial" size="1" color="#666666"><input type="text" class="InputFieldMain" size="30" name="City"></font><u><font face="Arial" size="1" color="#CC6600"><br>
										</font></u><b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="">State<u><br>
										</u></font><b><img border="0" src="images/spacer.gif" height="7" width="45"></b>
									<asp:DropDownList style="border:1px dotted;padding:5px;" id="DropDownList_State" DataTextField="name"
										DataValueField="state_id" runat="server"><asp:ListItem Value=""  Selected=true >Select a State</asp:ListItem></asp:DropDownList>
									<u><font face="Arial" size="1" color="#CC6600">
											<br>
										</font></u><b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="">Zip<u><br>
										</u></font><b><img border="0" src="images/spacer.gif" height="7" width="45"></b><font face="Arial" size="1" color="#666666"><input type="text" class="InputFieldMain" size="9" name="Zip"><br>
									</font>
									<br>
									<input type="button" class="button" onclick="javascript:ExecuteSubmit();" value="Search"><br>
									<br>
								</div> <!--SnapTreeview-->
							</div>
						</Content>
					</ComponentArt:Snap>
					<div class="SnapHeaderProjectsFiller"></div>
					<ComponentArt:Snap id="SnapProjectDate" runat="server" Fillcontainer="false" Height="20px" MustBeDocked="true"
						DockingStyle="TransparentRectangle" CurrentDockingIndex="1" DraggingStyle="GhostCopy" CurrentDockingContainer="ProjectLeftColumn"
						DockingContainers="ProjectLeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false"
						AutoCallBackOnExpand="false">
						<Header>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SnapProjectDate.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(SnapProjectDate.ClientID) %>.StartDragging(event);" >Project 
											Date</td>
									</tr>
								</table>
							</div>
						</Header>
						<CollapsedHeader>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SnapProjectDate.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(SnapProjectDate.ClientID) %>.StartDragging(event);">Project 
											Date</td>
									</tr>
								</table>
							</div>
						</CollapsedHeader>
						<Content>
							<div class="SnapTreeviewTag" style="background-color:white;">
								<div class="carouselshortlist">
									<br>
									Date type:<br>
									<select class="preferences" id="pdtcreatemodify" name="pdtcreatemodify">
										<option value="1" selected>Last Modified Date</option>
										<option value="0">Uploaded Date</option>
										<option value="2">Created Date</option>
									</select><br>
									<br>
									<b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="#666666"><input class="preferences" type="radio" value="0" name="pdt">in 
										the last <input type="text" size="9" name="pdtmonth"> months<br>
									</font><b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="#666666"><input class="preferences" type="radio" value="1" name="adt">in 
										the last <input type="text" size="9" name="pdtdays"> days<br>
									</font><b><img border="0" src="images/spacer.gif" height="7" width="21"></b><font face="Arial" size="1" color="#666666"><input class="preferences" type="radio" value="2" name="adt">between 
										&nbsp; <input type="text" name="pdtbetweendate" size="9"></font>
									<SCRIPT LANGUAGE="JavaScript">
var cal_3 = new CalendarPopup();
cal_3.showYearNavigation();
									</SCRIPT>
									<SCRIPT LANGUAGE="JavaScript">
var cal_4 = new CalendarPopup();
cal_4.showYearNavigation();
									</SCRIPT>
									<A HREF="#" onClick="cal_3.select(document.forms[0].pdtbetweendate,'anchor3','MM/dd/yyyy'); return false;"
										NAME="anchor3" ID="anchor3"><font face="Arial" size="1">select a date</font></A><br>
									<b><img border="0" src="images/spacer.gif" height="7" width="63"></b><font face="Arial" size="1" color="#666666">and&nbsp;&nbsp;
										<input type="text" name="pdtanddate" size="9"> </font><A HREF="#" onClick="cal_4.select(document.forms[0].pdtanddate,'anchor4','MM/dd/yyyy'); return false;"
										NAME="anchor4" ID="anchor4"><font face="Arial" size="1">select a date</font></A><font face="Arial" size="1" color="#666666"><br>
									</font>
									<br>
									<br>
									<br>
									<input type="button" class="button" onclick="javascript:ExecuteSubmit();" value="Search"><br>
									<br>
									<br>
								</div> <!--SnapTreeview-->
							</div>
						</Content>
					</ComponentArt:Snap>
					<div class="SnapHeaderProjectsFiller"></div>
					<ComponentArt:Snap id="SnapProjectUDFs" runat="server" Fillcontainer="false" Height="20px" MustBeDocked="true"
						DockingStyle="TransparentRectangle" CurrentDockingIndex="2" DraggingStyle="GhostCopy" CurrentDockingContainer="ProjectLeftColumn"
						DockingContainers="ProjectLeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false"
						AutoCallBackOnExpand="false">
						<Header>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SnapProjectUDFs.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(SnapProjectUDFs.ClientID) %>.StartDragging(event);" >Project 
											User Defined Fields</td>
									</tr>
								</table>
							</div>
						</Header>
						<CollapsedHeader>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SnapProjectUDFs.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(SnapProjectUDFs.ClientID) %>.StartDragging(event);">Project 
											User Defined Fields</td>
									</tr>
								</table>
							</div>
						</CollapsedHeader>
						<Content>
							<div class="SnapTreeviewTag" style="background-color:white;">
								<div class="carouselshortlist">
									<COMPONENTART:CALLBACK id="Callback1" runat="server" CacheContent="false">
										<CONTENT>
											<asp:Literal ID="Literal_UDFPMAIN" Runat="server"></asp:Literal>
										</CONTENT>
										<LOADINGPANELCLIENTTEMPLATE>
											<IMG height="16" src="images/spinner.gif" width="16" border="0">
										</LOADINGPANELCLIENTTEMPLATE>
									</COMPONENTART:CALLBACK>
									<br>
									<input type="button" class="button" onclick="javascript:ExecuteSubmit();" value="Search"><br>
									<br>
									<br>
								</div> <!--carouselshortlist-->
							</div> <!--SnapTreeview-->
						</Content>
					</ComponentArt:Snap>
					<div class="SnapHeaderProjectsFiller"></div>
					<ComponentArt:Snap id="SnapProjectDisciplines" runat="server" Fillcontainer="false" Height="20px" MustBeDocked="true"
						DockingStyle="TransparentRectangle" CurrentDockingIndex="5" DraggingStyle="GhostCopy" CurrentDockingContainer="ProjectLeftColumn"
						DockingContainers="ProjectLeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false"
						AutoCallBackOnExpand="false">
						<Header>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SnapProjectDisciplines.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(SnapProjectDisciplines.ClientID) %>.StartDragging(event);" >Project
											<asp:Literal ID="LiteralKeywordsP1aTitle" Runat="server"></asp:Literal></td>
									</tr>
								</table>
							</div>
						</Header>
						<CollapsedHeader>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SnapProjectDisciplines.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(SnapProjectDisciplines.ClientID) %>.StartDragging(event);">Project
											<asp:Literal ID="LiteralKeywordsP1bTitle" Runat="server"></asp:Literal></td>
									</tr>
								</table>
							</div>
						</CollapsedHeader>
						<Content>
							<div class="SnapTreeviewTag" style="background-color:white;">
								<div class="SnapProjectsSearch">
									<img src="images/spacer.gif" width="188" height="1"><br>
									<div style="padding:5px">
										<div style="width: 100%;float:left;">
											<input class="preferences" type="radio" value="and" name="KeywordDisciplineSearchType"
												checked> And (projects matching all selected keywords)<br>
											<input class="preferences" type="radio" value="or" name="KeywordDisciplineSearchType">
											Or (projects matching any selected keywords)
											<br>
											<br>
											<div style="width: 100%;float:left;">
												<COMPONENTART:CALLBACK id="CALLBACKKeywordsDiscipline" runat="server" CacheContent="false">
													<CONTENT>
														<asp:Literal ID="LiteralKeywordsP1" Runat="server"></asp:Literal>
													</CONTENT>
													<LOADINGPANELCLIENTTEMPLATE>
														<IMG height="16" src="images/spinner.gif" width="16" border="0">
													</LOADINGPANELCLIENTTEMPLATE>
												</COMPONENTART:CALLBACK>
											</div>
											<div style="width: 100%;float:left;">
												<br>
												<br>
												<input type="button" class="button" onclick="javascript:ExecuteSubmit();" value="Search"><br>
											</div>
										</div>
									</div>
								</div> <!--padding-->
						</Content>
					</ComponentArt:Snap>
					<div class="SnapHeaderProjectsFiller"></div>
					<ComponentArt:Snap id="SnapProjectKeywords" runat="server" Fillcontainer="false" Height="20px" MustBeDocked="true"
						DockingStyle="TransparentRectangle" CurrentDockingIndex="4" DraggingStyle="GhostCopy" CurrentDockingContainer="ProjectLeftColumn"
						DockingContainers="ProjectLeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false"
						AutoCallBackOnExpand="false">
						<Header>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SnapProjectKeywords.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(SnapProjectKeywords.ClientID) %>.StartDragging(event);" >Project
											<asp:Literal ID="LiteralKeywordsP2aTitle" Runat="server"></asp:Literal></td>
									</tr>
								</table>
							</div>
						</Header>
						<CollapsedHeader>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SnapProjectKeywords.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(SnapProjectKeywords.ClientID) %>.StartDragging(event);">Project
											<asp:Literal ID="LiteralKeywordsP2bTitle" Runat="server"></asp:Literal></td>
									</tr>
								</table>
							</div>
						</CollapsedHeader>
						<Content>
							<div class="SnapTreeviewTag" style="background-color:white;">
								<div class="SnapProjectsSearch">
									<img src="images/spacer.gif" width="188" height="1"><br>
									<div style="padding:5px">
										<div style="width: 100%;float:left;">
											<input class="preferences" type="radio" value="and" name="KeywordKeywordsSearchType" checked>
											And (projects matching all selected keywords)<br>
											<input class="preferences" type="radio" value="or" name="KeywordKeywordsSearchType">
											Or (projects matching any selected keywords)
											<br>
											<br>
											<div style="width: 100%;float:left;">
												<COMPONENTART:CALLBACK id="Callback2" runat="server" CacheContent="false">
													<CONTENT>
														<asp:Literal ID="LiteralKeywordsP2" Runat="server"></asp:Literal>
													</CONTENT>
													<LOADINGPANELCLIENTTEMPLATE>
														<IMG height="16" src="images/spinner.gif" width="16" border="0">
													</LOADINGPANELCLIENTTEMPLATE>
												</COMPONENTART:CALLBACK>
											</div>
											<div style="width: 100%;float:left;">
												<br>
												<br>
												<input type="button" class="button" onclick="javascript:ExecuteSubmit();" value="Search"><br>
											</div>
										</div>
									</div>
								</div> <!--padding-->
						</Content>
					</ComponentArt:Snap>
					<div class="SnapHeaderProjectsFiller"></div>
					<ComponentArt:Snap id="SnapProjectOffices" runat="server" Fillcontainer="false" Height="20px" MustBeDocked="true"
						DockingStyle="TransparentRectangle" CurrentDockingIndex="3" DraggingStyle="GhostCopy" CurrentDockingContainer="ProjectLeftColumn"
						DockingContainers="ProjectLeftColumn" AutoCallBackOnDock="false" AutoCallBackOnCollapse="false"
						AutoCallBackOnExpand="false">
						<Header>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SnapProjectOffices.ClientID) %>.ToggleExpand();" src="images/i_open.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(SnapProjectOffices.ClientID) %>.StartDragging(event);" >Project
											<asp:Literal ID="LiteralKeywordsP3aTitle" Runat="server"></asp:Literal></td>
									</tr>
								</table>
							</div>
						</Header>
						<CollapsedHeader>
							<div style="CURSOR: move; width: 100%;">
								<table class="SnapHeaderTag" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td width="10" style="cursor: hand" align="right"><img onclick="<% Response.Write(SnapProjectOffices.ClientID) %>.ToggleExpand();" src="images/i_closed.gif" width="15" height="15" border="0"></td>
										<td onmousedown="<% Response.Write(SnapProjectOffices.ClientID) %>.StartDragging(event);">Project
											<asp:Literal ID="LiteralKeywordsP3bTitle" Runat="server"></asp:Literal></td>
									</tr>
								</table>
							</div>
						</CollapsedHeader>
						<Content>
							<div class="SnapTreeviewTag" style="background-color:white;">
								<div class="SnapProjectsSearch">
									<img src="images/spacer.gif" width="188" height="1"><br>
									<div style="padding:5px">
										<div style="width: 100%;float:left;">
											<input class="preferences" type="radio" value="and" name="KeywordOfficeSearchType" checked>
											And (projects matching all selected keywords)<br>
											<input class="preferences" type="radio" value="or" name="KeywordOfficeSearchType">
											Or (projects matching any selected keywords)
											<br>
											<br>
											<div style="width: 100%;float:left;">
												<COMPONENTART:CALLBACK id="Callback3" runat="server" CacheContent="false">
													<CONTENT>
														<asp:Literal ID="LiteralKeywordsP3" Runat="server"></asp:Literal>
													</CONTENT>
													<LOADINGPANELCLIENTTEMPLATE>
														<IMG height="16" src="images/spinner.gif" width="16" border="0">
													</LOADINGPANELCLIENTTEMPLATE>
												</COMPONENTART:CALLBACK>
											</div>
											<div style="width: 100%;float:left;">
												<br>
												<br>
												<input type="button" class="button" onclick="javascript:ExecuteSubmit();" value="Search"><br>
											</div>
										</div>
									</div>
								</div> <!--padding-->
						</Content>
					</ComponentArt:Snap>
					<div class="SnapHeaderProjectsFiller"></div>
				</div>
			</div>
			<br>

		</div> <!--Project options-->
		
		
		

		
		
		
		
		
		
		
		
		
		
		<br>
		<img src="images/spacer.gif" width="1" height="1500"><br>
	</div>
	<img src="images/spacer.gif" width="1" height="1500"><br>
</div>
<script language="javascript" type="text/javascript">
//closealltags();
function setoptions(page)
{
//turn off all others
document.getElementById('assetoptions').style.display='none';
document.getElementById('projectoptions').style.display='none';
//document.getElementById('peopleoptionsxx').style.display='none';
document.getElementById(page).style.display='block';
openalltags();
}

</script>
