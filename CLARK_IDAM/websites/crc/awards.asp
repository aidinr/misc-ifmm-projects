	<!--#include file="includes\config.asp" -->
<%
	set rsAwards=server.createobject("adodb.recordset")
	if request.querystring("aid") <> "" then	
		sql = "select * FROM ipm_awards where type = 0 AND SHOW = 1 and pull_date > getdate() and awards_id = " & request.querystring("aid") & " ORDER BY POST_DATE DESC"
	else
		sql = "select * FROM ipm_awards where type = 0 AND SHOW = 1 and pull_date > getdate() ORDER BY POST_DATE DESC"
	end if

	rsAwards.Open sql, Conn, 1, 4
	
	set rsAwardsUDF=server.createobject("adodb.recordset")
	set rsAwardsRelatedProjects=server.createobject("adodb.recordset")
	set rsAwardsRelatedProjectsType=server.createobject("adodb.recordset")

%>
<html>
<head>
<title><%=sPageTitle%> - Awards</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="news.asp">News</a> | Awards</small>
					<br /><br />
					<img src="images/awards.jpg" alt="overview" />
					<br /><br />
					As a testament to the quality of our work, Clark Realty Capital and our affiliated companies have garnered numerous accolades for innovative real estate projects across the country. 
					<br /><br />
					
					<table width="550" cellpadding="5" cellspacing="5">
					
					<%do while not rsAwards.eof

					if(rsAwardsUDF.state = 1) then
					rsAwardsUDF.close
					end if
					
					sql= "select * from ipm_awards_field_value where awards_id = " & rsAwards("awards_id")
										
					rsAwardsUDF.Open sql, Conn, 1, 4
					
					if(rsAwardsRelatedProjects.state = 1) then
					rsAwardsRelatedProjects.close
					end if
					
					sql= "select * from ipm_awards_related_projects a,ipm_project b where a.projectid = b.projectid and b.available = 'y' and awards_id = " & rsAwards("awards_id")
										
					rsAwardsRelatedProjects.Open sql, Conn, 1, 4
					
					
					sAward = ""
					sRecipient = ""
					sRecipientURL = ""
					sOrganization= ""
					sOrganizationURL = ""
					
					do while (rsAwardsUDF.EOF = false)
					
						'Reset the variables
						if (rsAwardsUDF("item_id") = "21764687") then
							sAward = rsAwardsUDF("item_value")
						elseif (rsAwardsUDF("item_id") = "21764690") then
							sRecipient = rsAwardsUDF("item_value")
						elseif (rsAwardsUDF("item_id") = "21764691") then
							sRecipientURL = rsAwardsUDF("item_value")
						elseif (rsAwardsUDF("item_id") = "21764688") then
							sOrganization = rsAwardsUDF("item_value")
						elseif (rsAwardsUDF("item_id") = "21764689") then
							sOrganizationURL = rsAwardsUDF("item_value")
						end if
						
					rsAwardsUDF.moveNext
					loop
					
					%>
					<tr>
					<td valign="top" align="center"><img width="109" height="115" src="<%=sAssetPath%><%=rsAwards("awards_id")%>&Instance=<%=siDAMInstance%>&type=awards&size=2" alt="<%=replace(rsAwards("Headline"),"""","'")%>" /></td>
					<td valign="top"><b><%=rsAwards("Headline")%></b>
					<%if not (sRecipientURL = "" or sRecipient = "") then%>
					<br /><b>Company:</b> <a href="<%=sRecipientURL%>"><%=sRecipient%></a> 
					<%end if%>
					<br />
					<%if not (sAward = "") then %>
					<b>Award: </b> <%=sAward%>
					<br />
					<%end if%>
					<%if not (trim(rsAwards("PublicationTitle")) = "") then %>
					<b>Type: </b> <%=rsAwards("PublicationTitle")%>
					<br />
					<%end if

					if(rsAwardsRelatedProjects.recordCount > 0) then
					%>
					<b>Projects: </b>
					<%
					do while (rsAwardsRelatedProjects.EOF = false) %>
					
					<%
					crc_link = 0
					cbg_link = 0
					
					if(rsAwardsRelatedProjectsType.state = 1) then
					rsAwardsRelatedProjectsType.close
					end if					
					sql = "select * from ipm_project_field_desc a, ipm_project_field_value b where (a.item_tag = 'IDAM_PROJECT_CR_WEBSITE' ) and a.item_id = b.item_id and b.item_value = '1' and b.projectid = " &  rsAwardsRelatedProjects("projectID")
					rsAwardsRelatedProjectsType.Open sql, Conn, 1, 4
					
					if (rsAwardsRelatedProjectsType.recordcount = 2) then
						crc_link = 1
						cbg_link = 1
					elseif(rsAwardsRelatedProjectsType.recordcount = 1) then
						if (trim(rsAwardsRelatedProjectsType("item_tag")) = "IDAM_PROJECT_CC_WEBSITE") then
						cbg_link = 1
                        
						elseif (trim(rsAwardsRelatedProjectsType("item_tag")) = "IDAM_PROJECT_CR_WEBSITE") then
						crc_link = 1
						end if
					end if
					
					%>
					<%
					if(crc_link = 1 and cbg_link = 1) then
					%>
					<a href="project.asp?pid=<%=rsAwardsRelatedProjects("projectID")%>"><%=rsAwardsRelatedProjects("name")%> - CRC</a>, <a href="http://www.clarkbuildersgroup.com/project.asp?pid=<%=rsAwardsRelatedProjects("projectID")%>"><%=rsAwardsRelatedProjects("name")%> - CBG</a>
					<%elseif(crc_link = 1) then%>
					<a href="project.asp?pid=<%=rsAwardsRelatedProjects("projectID")%>"><%=rsAwardsRelatedProjects("name")%></a>
					<%elseif(cbg_link = 1) then%>
					<a href="http://www.clarkbuildersgroup.com/project.asp?pid=<%=rsAwardsRelatedProjects("projectID")%>"><%=rsAwardsRelatedProjects("name")%></a>
					<%end if
					%>
					<%
					rsAwardsRelatedProjects.moveNext
					
					if rsAwardsRelatedProjects.EOF = false then
					response.write ", "
					end if
					
					loop
					
					elseif not (rsAwards("content") = "") then %>
					<b>Projects: </b> <%=replace(rsAwards("content"),VbCrLf,"<br />")%>
					<%
					end if
					
					if not (sOrganizationURL = "" or sOrganization = "") then
					%>
					<br />
					For more information, please visit the official site of <a target="_blank" href="<%=sOrganizationURL%>"><%=sOrganization%></a> 
					<%end if%>
					
					</td>
					</tr>					

					<%
					rsAwards.movenext
					loop					
					%>					
					</table>
					<%if request.querystring("aid") <> "" then%>
						<a href="awards.asp">See all</a>
					<%end if%>

					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/awards_side.jpg" alt="CRC's Fort Belvoir development has garnered national and international recognition for its innovative town center concept and sustainable design." />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
