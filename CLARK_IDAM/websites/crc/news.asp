	<!--#include file="includes\config.asp" -->
<%
	set rsNews=server.createobject("adodb.recordset")
	sql = "select TOP 3 * FROM ipm_news where type = 0 AND post_date < getdate() AND pull_date > getdate() AND SHOW = 1 ORDER BY POST_DATE DESC"
	rsNews.Open sql, Conn, 1, 4
	
	'newsletter script
	newLine = chr(13) & chr(10)
	if Request.form("submit") = "1" then
		emailFrom = Request.form("from")
		email_address = "enewsletter@clarkbuildersgroup.com" 
		
		Set Mail = Server.CreateObject("Persits.MailSender")
                MailText = MailText & "Please add the following address to your mailing list:" & newLine & newLine & newLine
		MailText = MailText & emailFrom
		
			Mail.Host = "localhost" ' Specify a valid SMTP server
		
                        Mail.From = "no-reply@clarkbuildersgroup.com"

                        Mail.FromName = "CBG Newsletter Subscription"

                        Mail.Subject = "New Newsletter Subscription Request"

                        Mail.Body = MailText
			
                        Mail.AddAddress email_address
			
                        on error resume next     
                        Mail.Send

                       

                        If Err <> 0 Then
                                    blnSent = "0"

                        else     
                                    blnSent = "1"

                        End If
                        set Mail = nothing			
			
	end if

%>
<html>
<head>
<title><%=sPageTitle%> - News</title>
	<!--#include file="includes\header.asp" -->
	
         <SCRIPT type="text/javascript">
          <!--
           function IsEmailValid(FormName,ElemName)
            {
             var EmailOk  = true
             var Temp     = document.forms[FormName].elements[ElemName]
             var AtSym    = Temp.value.indexOf('@')
             var Period   = Temp.value.lastIndexOf('.')
             var Space    = Temp.value.indexOf(' ')
             var Length   = Temp.value.length - 1   // Array is from 0 to length-1

             if ((AtSym < 1) ||                     // '@' cannot be in first position
                 (Period <= AtSym+1) ||             // Must be atleast one valid char btwn '@' and '.'
                 (Period == Length ) ||             // Must be atleast one valid char after '.'
                 (Space  != -1))                    // No empty spaces permitted
                {
                   alert('Please enter a valid e-mail address!')
                   Temp.focus()
                   EmailOk = false
                }
             return EmailOk
            }
          // -->
         </SCRIPT>
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small>News</small>
					<br /><br />
					<img src="images/news.jpg" alt="News" />
					<br /><br />
					Clark Realty Capital and our affiliates are involved in award-winning real estate projects from coast to coast. To keep up with our latest news, join our eNewsletter mailing list below.
					<br /><br />
					<%
					do while not rsNews.eof %>
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td><a href="view_news.asp?id=<%=rsNews("news_id")%>"><img width="180" height="86" src="<%=sAssetPath%><%=rsNews("news_ID")%>&Instance=<%=siDAMInstance%>&type=news&size=1&width=180&height=86&qfactor=<%=assetQFactor%>" alt="<%=rsNews("Headline")%>"/></a><img src="images/spacer.gif" height="1" width="13" alt="spacer" /></td>
							<td style="vertical-align:top;">
								<div style="margin-left:5px;margin-top:5px;">
								<b><a href="view_news.asp?id=<%=rsNews("news_id")%>"><%=MonthName(DatePart("m",rsNews("post_date")))%> <%=" " & DatePart("d",rsNews("post_date"))%>, <%=DatePart("yyyy",rsNews("post_date"))%></a></b><br />
								<%=rsNews("headline")%>
								</div>
							</td>
						</tr>
					
					</table>
					<br /><br />
					<%
					rsNews.movenext
					loop
					%>
					<b><a href="news_archives.asp">See All Press Releases &gt;&gt;</a></b>
					
					<br /><br />
					
					<div style="margin-bottom:5px;"><b>eNewsletter</b><br />
					<%if (blnSent = "1") then %>
					Your request has been submitted successfully. Thank you for your subscription!
					<%else %>
					<%if (blnSent = "0") then %>
					There was an error in processing your request. Please try again later.<br /><br />
					<%end if%>
Join our mailing list and receive Clark Realty Capital's latest news in your inbox. Emails containing press releases are distributed to subscribers four times a year. <b>Sign up now!</b></div>
					
																<FORM METHOD="POST" NAME="contact" ACTION="news.asp" ONSUBMIT="return IsEmailValid('contact','from');">
																<input type="hidden" name="submit" value="1" />
																<INPUT CLASS="mlisttype" TYPE="text" SIZE="48" NAME="from" value="Type Email Address Here" onfocus="if(this.value=='Type Email Address Here')this.value=''" />
																<INPUT TYPE="submit" VALUE="Subscribe">
																</FORM>
					<%end if%>
					</div>
					
					
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/news_side_bg.gif" alt="Media Relations - All media inquiries should be directed to Joy Lutes at 703.362.0150 or 1.877.846.7760" />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
