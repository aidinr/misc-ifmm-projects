<!--#include file="includes\config.asp" -->
<%

set rsProjectsStates=server.createobject("adodb.recordset")
sql = "select distinct state_id from ipm_project, ipm_project_field_desc e, ipm_project_field_value f where publish = 1 and available = 'y' and ipm_project.projectid = f.projectid and e.item_id = f.item_id and e.item_tag = 'IDAM_PROJECT_CR_WEBSITE' and f.item_value = '1' and state_id <> '' order by state_id"
rsProjectsStates.Open sql, Conn, 1, 4

%>

<html>
<head>
<title><%=sPageTitle%> - Advanced Search</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			<!--#include file="includes\menu.asp" -->
			
			
			<div id="content">

			<br />
			<table cellpadding="0" cellspacing="0" width="100%" id="main_table">
			<tr>
				<td class="left_col_overview" style="width:100%;">
					<div id="content_text">
					Portfolio | Advanced Search
					<br /><br />
					<img src="images/title_advanced_search.jpg" alt="Advanced Search" />
					<br /><br />
					<form method="get" action="portfolio_search.asp">
					<table cellpadding="5" cellspacing="5">
			<tr>
			<td valign="top"></td>
			<td valign="top">
				<table>
					<tr>
						<td><b>Conditions:</b></td>
						<td></td>
					</tr>				
					<tr>
						<td>Project Type : </td>
						<td><select name="type">
						<option value="">All</option>					
							<option value="21762217">Mixed-Use</option>
							<option value="21762215">Luxury Apartments</option>
							<option value="21762216">Military Housing</option>
							<option value="21763556">Senior Living</option>
							<option value="21762214">High-Density Apartments</option>
							<option value="21762213">Affordable Housing</option>
							<option value="21762980">Retail/Office/Hospitality</option>
							<option value="21763547">Condominium</option>
							<option value="21763550">For-Sale Housing</option>
							</select></td>
					</tr>						
					<tr>
						<td>Project Region : </td>
						<td><select name="region">
							<option value="">All</option>
							<option value="Northeast">Northeast</option>
							<option value="Northwest">Northwest</option>
							<option value="Southeast">Southeast</option>
							<option value="Southwest">Southwest</option>
						    </select>
						</td>
					</tr>
					<tr>
						<td>Project State : </td>
						<td><select name="state">
								<option value="">All</option>
								<%do while not rsProjectsStates.eof%>
								<option value="<%=trim(rsProjectsStates("state_id"))%>"><%=trim(rsProjectsStates("state_id"))%></option>
								<%
								rsProjectsStates.moveNext
								loop
								%>
							</select>
</td>
					</tr>
					<tr>
						<td>Keywords(s): </td>
						<td><input type="text" size="50" name="search" /></td>
					</tr>
					<tr>
						<td><br /><br />
						<input type="hidden" name="submit" value="1" />
						<input type="hidden" name="advanced" value="1" />
						<input type="submit" value="Search" />
						</td>
						<td></td>
					</tr>
	
				
					
				</table>
				</form>
			</td>
		</tr>
</table>

			<br /><br /><br />
			<!--#include file="includes\footer.asp" -->
			</div>
			
	
		</div>
			
	</div>
</body>
</html>
