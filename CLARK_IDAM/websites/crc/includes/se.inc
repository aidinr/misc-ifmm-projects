<b>REGIONAL HEADQUARTERS</b><br />
8431 Bayshore Boulevard<br />
Tampa, FL 33621<br />
Phone: 813.840.1700<br />
Fax: 813.840.1730<br />
<a target="_blank" href="http://maps.google.com/maps?saddr=&daddr=8431 Bayshore Boulevard Tampa FL 33621">Get Directions&gt;&gt;</a><br />
<br />
<b>CONTACTS</b><br />
<b>Business Development</b><br />
Chris Hirst<br />
Vice President of Construction<br />
813.840.1712<br />
<br />
<b>Recruiting</b><br />
Becky Fairchild<br />
Recruiting and Training Coordinator <br />
703.294.5200<br />
<br />
<b>LOCAL OFFICES</b><br />
<b>Fort Benning Office</b><br />
Building 280 Transportation Street<br />
Fort Benning, GA 31905<br />
Phone: 706.683.4201<br />
Fax: 706.683.4253<br />
<a target="_blank" href="http://maps.google.com/maps?saddr=&daddr=Building 280 Transportation Street Fort Benning CA 31905">Get Directions&gt;&gt;</a><br />
<br />
<b>Albany Office</b></br>
1210 Spencer Road<br />
Albany, GA 31705<br />
<a target="_blank" href="http://maps.google.com/maps?saddr=&daddr=1210 Spencer Road Albany GA 31705">Get Directions&gt;&gt;</a><br />
