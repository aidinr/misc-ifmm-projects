<%

	
	overviewID = "id=nav_overview"
	overviewState = "off"
	
	teamID = "id=nav_team"
	teamState = "off"
	
	newsID = "id=nav_news"
	newsState = "off"
	
	birdID = "id=nav_bird"
	birdState = "off"
	
	portfolioID = "id=nav_portfolio"
	portfolioState = "off"

SCRIPT_NAME = Request.ServerVariables("script_name")
	
	if(instr(SCRIPT_NAME,"overview")) then
	overviewID = ""
	overviewState = "on"
	elseif(instr(SCRIPT_NAME,"culture")) then
	teamID = ""
	teamState = "on"
	elseif(instr(SCRIPT_NAME,"portfolio")) then
	portfolioID = ""
	portfolioState = "on"
	elseif(instr(SCRIPT_NAME,"news") or instr(SCRIPT_NAME,"awards")) then
	newsID = ""
	newsState = "on"


	
	end if

%>


<form style="padding:0;margin:0;" method="get" action="portfolio_search.asp">
<div id="menu">




			<div id="nav">
			
			
			
			

			
				<div>
				<a href="overview.asp"><img src="images/menu/menu_gap.jpg" width="1" height="44" /><img <%=overviewID%> width="132" height="44" src="images/menu/menu_overview_<%=overviewState%>.jpg" /></a>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" class="navigation_left"></td>
						<td align="left" class="navigation">			    
							 <a href="overview_capabilities.asp">Capabilities</a>
							 <a href="overview_offices.asp">Offices</a>
							 <a href="overview_affiliates.asp">Affiliates</a>
							 
						</td>
						<td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
					</tr>
					<tr>
						<td width="10" height="10" class="navigation_bottom_left"></td>
						<td height="10" class="navigation_bottom"></td>
						<td width="8" height="10" class="navigation_bottom_right"></td>
					</tr>
				</table>				
				</div>
				
				<div>
				<a href="culture.asp"><img src="images/menu/menu_gap.jpg" width="1" height="44" /><img <%=teamID%> width="120" height="44" src="images/menu/menu_culture_<%=teamState%>.jpg" /></a>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" class="navigation_left"></td>
						<td align="left" class="navigation">			    
							 <a href="culture_careers.asp">Careers</a>							 
						</td>
						<td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
					</tr>
					<tr>
						<td width="10" height="10" class="navigation_bottom_left"></td>
						<td height="10" class="navigation_bottom"></td>
						<td width="8" height="10" class="navigation_bottom_right"></td>
					</tr>
				</table>				
				</div>
				
				<div>
				<a href="portfolio.asp"><img src="images/menu/menu_gap.jpg" width="1" height="44" /><img <%=portfolioID%> width="130" height="44" src="images/menu/menu_portfolio_<%=portfolioState%>.jpg" /></a>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" class="navigation_left"></td>
						<td align="left" class="navigation">			    
							 <a href="portfolio_cat.asp?cid=21763558">Pioneering Ideas</a>
							 <a href="portfolio_cat.asp?cid=21763555">Improving Lives</a>
							 <a href="portfolio_cat.asp?cid=21763426">Advancing Urbanism</a>
							 <a href="portfolio_cat.asp?cid=21763557">Building Community</a>
							 <a href="portfolio_cat.asp?cid=21763553">Changing Horizons</a>
							 <a href="portfolio_cat.asp?cid=21763554">Developing Responsibility</a>
							 
						</td>
						<td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
					</tr>
					<tr>
						<td width="10" height="10" class="navigation_bottom_left"></td>
						<td height="10" class="navigation_bottom"></td>
						<td width="8" height="10" class="navigation_bottom_right"></td>
					</tr>
				</table>
				</div>				
				
				
				
				<div>				
				<a href="news.asp"><img src="images/menu/menu_gap.jpg" width="1" height="44" /><img <%=newsID%> width="120" height="44" src="images/menu/menu_news_<%=newsState%>.jpg" /></a>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" class="navigation_left"></td>
						<td align="left" class="navigation">			    
							 
							 <a href="news_archives.asp">Press Releases</a>
							 <a href="awards.asp">Awards</a>							 
						</td>
						<td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
					</tr>
					<tr>
						<td width="10" height="10" class="navigation_bottom_left"></td>
						<td height="10" class="navigation_bottom"></td>
						<td width="8" height="10" class="navigation_bottom_right"></td>
					</tr>
				</table>
				</div>
				
				
				

  <div style="white-space: nowrap;">    
    <a id="BirdHyperLinkStyle" href="http://www.clarkcapitalventures.com"  border="0"><img src="images/menu/menu_gap.jpg" width="1" height="44"  border="0" /><img <%=birdID%> width="125" height="44"  border="0" src="images/menu/menu_bird_off.jpg" /><span id="birdanimEmbed"><object  border="0" width="31" height="19" onMouseOver="document.getElementById('nav_bird').src='images/menu/menu_bird_on.jpg';" ><param name="allowScriptAccess" value="sameDomain" /><param name="quality" value="high" /><embed src="images/menu/birdie_blue.swf?linkURL=http://www.clarkcapitalventures.com" width="80" height="32"  border="0" wmode="transparent" id="testb" type="application/x-shockwave-flash" onMouseOver="document.getElementById('nav_bird').src='images/menu/menu_bird_on.jpg'; " ></object></span></a>

    <table cellspacing="0" cellpadding="0" >
     <tr>
      <td width="10" class="navigation_left" ></td>
      
       <td align="left" class="navigation" onMouseOver="document.getElementById('nav_bird').src='images/menu/menu_bird_on.jpg'; " onMouseOut="document.getElementById('nav_bird').src='images/menu/menu_bird_off.jpg';">              
        <a style="color: #00529f; font-weight: bold; text-decoration: none;"  href="http://www.clarkcapitalventures.com">Check out our idea incubator at<br />www.clarkcapitalventures.com</a>
       
      </td>
      <td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
     </tr>
     <tr>
      <td width="10" height="10" class="navigation_bottom_left"></td>
      <td height="10" class="navigation_bottom"></td>
      <td width="8" height="10" class="navigation_bottom_right"></td>
     </tr>
    </table>
    
    </div>
    
    

				
			</div>


			
			<div class="menu_item_search"><form method="get" action="portfolio_search.asp"><input type="text" id="search_projects" name="search" value="SEARCH PROJECTS BY NAME" onfocus="this.select();this.value=''"/></div>
			
<span id="birdAnimOn" style="display: none;"  onMouseOver="BirdAnimOn(); document.getElementById('nav_bird').src='images/menu/menu_bird_on.jpg';" onMouseOut="BirdAnimOff(); document.getElementById('nav_bird').src='images/menu/menu_bird_off.jpg';">			
<object width="34" height="21" >      
   <param name="quality" value="high" />
   <embed src="images/menu/menu_bird_anim_on.swf?linkURL=http://www.clarkcapitalventures.com" id="animatedBird" quality="high" width="34" height="21" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" />
</object>
</span>




			</div>
</form>
