<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script type="text/javascript" src="js/jquery-1.2.1.pack.js"></script>
<script type="text/javascript" src="js/jquery.perciformes.js"></script>
<script type="text/javascript">
<!--
jQuery.preloadImages = function()
{
  for(var i = 0; i<arguments.length; i++)
  {
    jQuery("<img>").attr("src", arguments[i]);
  }
};
$.preloadImages("images/menu/menu_overview_on.jpg", "images/menu/menu_culture_on.jpg","images/menu/menu_news_on.jpg","images/menu/menu_portfolio_on.jpg", "images/menu/menu_bird_on.jpg", "images/menu/menu_bird_off.jpg" );

$(document).ready(function() { 
$('#nav_overview').hover(function() {
	$(this).attr("src","images/menu/menu_overview_on.jpg");
	activeSrc = $('#active').attr("src");
	if(activeSrc != null) {activeSrc = activeSrc.replace("_on","_off"); }
	$('#active').attr("src",activeSrc);
		}, function() {
	$(this).attr("src","images/menu/menu_overview_off.jpg");
	activeSrc = $('#active').attr("src");
	if(activeSrc != null) {activeSrc = activeSrc.replace("_off","_on");}
	$('#active').attr("src",activeSrc);
});
$('#nav_team').hover(function() {
	$(this).attr("src","images/menu/menu_culture_on.jpg");
	activeSrc = $('#active').attr("src");
	if(activeSrc != null) {activeSrc = activeSrc.replace("_on","_off"); }
	$('#active').attr("src",activeSrc);
		}, function() {
	$(this).attr("src","images/menu/menu_culture_off.jpg");
	activeSrc = $('#active').attr("src");
	if(activeSrc != null) {activeSrc = activeSrc.replace("_off","_on");}
	$('#active').attr("src",activeSrc);
});
$('#nav_portfolio').hover(function() {
	$(this).attr("src","images/menu/menu_portfolio_on.jpg");
	activeSrc = $('#active').attr("src");
	if(activeSrc != null) {activeSrc = activeSrc.replace("_on","_off"); }
	$('#active').attr("src",activeSrc);
		}, function() {
	$(this).attr("src","images/menu/menu_portfolio_off.jpg");
	activeSrc = $('#active').attr("src");
	if(activeSrc != null) {activeSrc = activeSrc.replace("_off","_on");}
	$('#active').attr("src",activeSrc);
});
$('#nav_news').hover(function() {
	$(this).attr("src","images/menu/menu_news_on.jpg");
	activeSrc = $('#active').attr("src");
	if(activeSrc != null) {activeSrc = activeSrc.replace("_on","_off"); }
	$('#active').attr("src",activeSrc);
		}, function() {
	$(this).attr("src","images/menu/menu_news_off.jpg");
	activeSrc = $('#active').attr("src");
	if(activeSrc != null) {activeSrc = activeSrc.replace("_off","_on");}
	$('#active').attr("src",activeSrc);
});


$('#nav_bird').hover(function() {
	$(this).attr("src","images/menu/menu_bird_on.jpg");
	activeSrc = $('#active').attr("src");
	if(activeSrc != null) {activeSrc = activeSrc.replace("_on","_off"); }
	$('#active').attr("src",activeSrc);
		}, function() {
	$(this).attr("src","images/menu/menu_bird_off.jpg");
	activeSrc = $('#active').attr("src");
	if(activeSrc != null) {activeSrc = activeSrc.replace("_off","_on");}
	$('#active').attr("src",activeSrc);
});


});
// -->
</script>
<link rel="stylesheet" type="text/css" href="style/crc.css" />


<!--[if lte IE 6]>
<style type="text/css">
img {
	behavior: url("pngbehavior.htc");
}
    #nav td.navigation { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/menu_bg.png", sizingMethod="scale");
}
    #nav td.navigation_right { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/menu_bg.png", sizingMethod="scale");
}
    #nav td.navigation_bottom_left { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/nav_bg_bottom_left.png", sizingMethod="scale");
}
    #nav td.navigation_bottom_right { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/nav_bg_bottom_right.png", sizingMethod="scale");
}

    #nav td.navigation_bottom { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/nav_bg_bottom.png", sizingMethod="scale");
}

    #nav td.navigation_left {
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/nav_bg_left.png", sizingMethod="scale");	
    }

</style>
<![endif]-->

<!--[if IE]>
  <script type="text/javascript">
  // <![CDATA[

  $(document).ready(function() {
    $('#nav div').sfHover();
    $('#search_projects').sfHover();
    $('#search_projects').sfFocus();

  });

  // ]]>
  </script>
<style type="text/css">
    #content {
    	height: 100%;
    }
    #home_content {
    	margin-top:-19px;
    	height: 100%;
    }
    #project_image_mask {
    	display: none;
    }
    #content_text {
    	width: 580px;
    }
</style>
<![endif]-->
