<!--#include file="includes\config.asp" -->

<%
	cid = prep_sql(request.querystring("cid"))
	sort = prep_sql(request.querystring("sort"))
	sort_dir = prep_sql(request.querystring("dir"))
	title = ""
	title_image = ""
	type_description = ""
	
	  parameters = "cid=" & cid
	
	title = ""
	title_image = ""
	type_description = ""
	
	th_name = "<a href=""portfolio_all.asp?" & parameters & "&sort=name&dir=asc"">NAME</a>"
	th_loc = "<a href=""portfolio_all.asp?" & parameters & "&sort=location&dir=asc"">LOCATION</a>"
	th_desc = "<a href=""portfolio_all.asp?" & parameters & "&sort=desc&dir=asc"">DESCRIPTION</a>"
	th_completion = "<a href=""portfolio_all.asp?" & parameters & "&sort=completion&dir=asc"">COMPLETION</a>"
	
	if(sort_dir = "asc") then
	next_dir = "desc"
	elseif(sort_dir = "desc") then
	next_dir = "asc"
	else
	next_dir = "asc"
	sort_dir = "asc"
	end if
	if(sort = "name") then
		sql_sort = "a.name " & sort_dir
		th_name = "<a href=""portfolio_all.asp?" & parameters & "&sort=name&dir=" & next_dir & """><b>NAME</b></a>"
	elseif (sort = "location") then
		sql_sort = "a.state_id " & sort_dir & ", a.city " & sort_dir
		th_loc = "<a href=""portfolio_all.asp?" & parameters & "&sort=location&dir=" & next_dir & """><b>LOCATION</b></a>"
	elseif (sort = "completion") then
		sql_sort = "completion " & sort_dir
		th_completion = "<a href=""portfolio_all.asp?" & parameters & "&sort=completion&dir=" & next_dir & """><b>COMPLETION</b></a>"
	elseif (sort = "desc") then
		sql_sort = "a.description " & sort_dir
		th_desc = "<a href=""portfolio_all.asp?" & parameters & "&sort=desc&dir=" & next_dir & """><b>DESCRIPTION</b></a>"		
	else 
		sql_sort = "a.name " & sort_dir
		th_name = "<a href=""portfolio_all.asp?" & parameters & "&sort=name&dir=desc""><b>NAME</b></a>"
	end if	
	
	
	if (cid = clng(DISC_PIO_IDE)) then 
	title = "Pioneering Ideas"
	title_image = "title_pioneering_ideas.jpg"
	type_description = ""
	elseif (cid = clng(DISC_IMP_LIV)) then
	title = "Improving Lives"
	title_image = "title_improving_lives.jpg"
	type_description = ""
	elseif (cid = clng(DISC_ADV_URB)) then
	title = "Advancing Urbanism"
	title_image = "title_advancing_urbanism.jpg"
	type_description = ""
	elseif (cid = clng(DISC_BUI_COM)) then
	title = "Building Community"
	title_image = "title_building_community.jpg"
	type_description = ""
	elseif (cid = clng(DISC_CHA_HOR)) then
	title = "Changing Horizons"
	title_image = "title_changing_horizons.jpg"
	type_description = ""
	elseif (cid = clng(DISC_DEV_RES)) then
	title = "Developing Responsibility"
	title_image = "title_developing_responsibility.jpg"
	type_description = ""	
	end if

	set rsProjectsByCat=server.createobject("adodb.recordset")
	sql="select a.*, convert(datetime,a.item_value) completion from (select distinct ipm_project.projectid, ipm_project.name, ipm_project.city, ipm_project.state_id, c.item_value roles, ipm_project.description, a.item_value from ipm_project, ipm_project_discipline,ipm_project_field_value a, ipm_project_field_desc b,ipm_project_field_value c, ipm_project_field_desc d, ipm_project_field_desc e, ipm_project_field_value f where ipm_project.projectid = f.projectid and e.item_id = f.item_id and e.item_tag = 'IDAM_PROJECT_CC_WEBSITE' and f.item_value = '1' and ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND keyid = " & cid & " AND a.projectid = ipm_project.projectID AND a.item_id = b.item_id AND b.item_tag = 'IDAM_PROJECT_COMPLETION' AND c.projectid = ipm_project.projectID AND c.item_id = d.item_id AND d.item_tag = 'IDAM_PROJECT_ROLE' and a.item_value <> '') a order by " & sql_sort
	rsProjectsByCat.Open sql, Conn, 1, 4
	

	


%>

<html>
<head>
<title><%=sPageTitle%> - Portfolio</title>
	<!--#include file="includes\header.asp" -->
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">

					<div id="content_text">
					<small><a href="portfolio.asp">Portfolio</a> | <%=title%></small>
					<br /><br />
					<img src="images/<%=title_image%>" alt="<%=title%>" />
					<br /><br />
					<%=type_description%>
					<br /><br />
					<table width="950">
						<tr>
							<th></th>
							<th><%=th_name%></th>
							<th><%=th_loc%></th>
							<th><%=th_desc%></th>
							<th><%=th_completion%></th>
						</tr>
						<%
						z = 1
						do while not rsProjectsByCat.eof

						
						if (z mod 2 = 0) then 
						lineStyle = "style=""background:#ffffff;"""
						lineStyle2 = "style=""background:#ffffff;"""
						else
						lineStyle =""
						lineStyle2 = ""
						end if
						
						if(rsProjectsByCat("completion") <> "") then
						lineDate = CDate(rsProjectsByCat("completion"))
						lineDate = MonthName(DatePart("m",lineDate)) & " " & DatePart("yyyy",lineDate)
						else
						lineDate = ""
						end if
						
						z = z + 1
						%>

							<tr>
								<td <%=lineStyle%> align="center"><div class="portfolio_bg" <%=lineStyle2%>><a href="project.asp?pid=<%=rsProjectsByCat("projectID")%>"><img class="portfolio_photo" src="<%=sAssetPath%><%=rsProjectsByCat("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=137&height=108&qfactor=<%=assetQFactor%>" alt="<%=trim(rsProjectsByCat("name"))%>"/></a></div></td>
								<td <%=lineStyle%> align="center"><%=trim(rsProjectsByCat("name"))%></td>
								<td <%=lineStyle%> align="center"><%=rsProjectsByCat("city")%>, <%=rsProjectsByCat("state_id")%></td>
								<td <%=lineStyle%> align="center"><%=rsProjectsByCat("description")%></td>
								<td <%=lineStyle%> align="center"><%=lineDate%></td>
							</tr>
						
						<%	
						rsProjectsByCat.moveNext
						
						loop						
						%>
					
					</table>
					
					</div>
					
					
					<!--#include file="includes\footer.asp" -->
				
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-2");
pageTracker._trackPageview();
</script></body>
</html>
