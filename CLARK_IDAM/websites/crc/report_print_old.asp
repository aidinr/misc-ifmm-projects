<!--#include file="includes\config.asp" -->
<%
	if request.form("submit") = "1" then
	
	'Pre-set parameters
	report_title = Request.Form("report_name")
	
	report_name = prep_sql(Request.Form("search_name"))
	report_client = prep_sql(Request.Form("search_client"))
	report_region = prep_sql(Request.Form("search_region"))
	report_state = prep_sql(Request.Form("search_state"))
	report_disc = prep_sql(Request.Form("search_discipline"))
	
	if(Request.Form("search_favorites") = "0") then
	report_favorite = " AND c.favorite = 0"
	elseif(Request.Form("search_favorites") = "1") then
	report_favorite = " AND c.favorite = 1"
	else
	report_favorite = ""
	end if
	
	display_number = Request.Form("number")
	display_client = Request.Form("client")
	display_address = Request.Form("address")
	display_city_state = Request.Form("city_state")
	display_zip = Request.Form("zip")
	display_desc = Request.Form("desc")
	
	'Big search filter
	set rsProjects = server.createobject("adodb.recordset")
	'sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description from ipm_project_field_value a, ipm_project_field_desc b, ipm_project c, ipm_project_discipline d, ipm_discipline e WHERE a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_REGION' AND a.projectid = c.projectid AND c.publish = 1AND item_value LIKE '%" & report_region & "%' AND d.projectid = c.projectid AND d.keyid = e.keyid AND c.name LIKE '%" & report_name & "%' AND c.clientname LIKE '%" & report_client & "%'AND c.state_id LIKE '%" & report_state & "%' AND c.projectID IN (select ipm_project.projectid from ipm_project, ipm_project_discipline where ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND keyid LIKE '%" & report_disc & "%') ORDER BY c.name"
	sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description from ipm_project_field_value a, ipm_project_field_desc b, ipm_project c, ipm_project_discipline d, ipm_discipline e WHERE a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_REGION' AND a.projectid = c.projectid AND c.publish = 1 " & report_favorite & "  AND item_value LIKE '%" & report_region & "%' AND d.projectid = c.projectid AND d.keyid = e.keyid AND c.name LIKE '%" & report_name & "%' AND c.clientname LIKE '%" & report_client & "%'AND c.state_id LIKE '%" & report_state & "%' AND c.projectID IN (select ipm_project.projectid from ipm_project, ipm_project_discipline where ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND keyid LIKE '%" & report_disc & "%') ORDER BY c.name"
	rsProjects.Open sql, Conn, 1, 4
	
	'Query for additional UDFs
	
	
	set rsUDF=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_desc where active = 1 order by item_name"
	rsUDF.Open sql, Conn, 1, 4
	
	'loop through and match checked inputs
	
	set rsClient = server.createobject("adodb.recordset")
	sql = "select * from ipm_client where active = 1 order by name"
	rsClient.Open sql, Conn, 1, 4
	
	set rsDisc = server.createobject("adodb.recordset")
	sql = "select * from ipm_discipline where keyuse = 1 order by keyname"
	rsDisc.Open sql, Conn, 1, 4

	set rsLineUDF=server.createobject("adodb.recordset")
	
	
	end if

%>
<html>
<head>
<title>
Report
</title>
</head>
<body>
	<h3><%=report_title%></h3>
	<table width="100%" border="1">
		<!--begin table heading-->
		<tr>
			<td>Project</td>
			<%
			if(display_number = 1) then
			response.write "<td>Number</td>"
			end if
			if (display_client = 1) then
			response.write "<td>Owner</td>"
			end if
			if (display_address = 1) then
			response.write "<td>Address</td>"
			end if
			if (display_city_state = 1) then
			response.write "<td>City, State</td>"
			end if
			if (display_desc = 1) then
			response.write "<td>Summary</td>"
			end if
			
			do while not rsUDF.eof
			itemTag = trim(rsUDF("item_tag"))
			
			if(Request.Form(itemTag) = "1") then
			response.write "<td>" & rsUDF("item_name") & "</td>"
			end if
			
			rsUDF.moveNext
			loop
			%>
		</tr>
		<!--end table heading-->
		<!--Begin results listing-->
		
			
			<%do while not rsProjects.eof%>
			<tr>
			<td><img src="<%=sAssetPath%><%=rsProjects("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=2" alt="<%=rsProjects("Name")%>" /><br /><%=rsProjects("Name")%></td>
			<%
			if(display_number = 1) then
			response.write "<td>"& rsProjects("projectNumber")  &"&nbsp;</td>"
			end if
			if (display_client = 1) then
			response.write "<td>"& rsProjects("clientName")  &"&nbsp;</td>"
			end if
			if (display_address = 1) then
			response.write "<td>"& rsProjects("Address")  &"&nbsp;</td>"
			end if
			if (display_city_state = 1) then
			response.write "<td>"& rsProjects("city") & ", " & rsProjects("state_id")  &"&nbsp;</td>"
			end if
			if (display_desc = 1) then
			response.write "<td>"& rsProjects("Description")  &"&nbsp;</td>"
			end if
			
			rsUDF.moveFirst
			
			do while not rsUDF.eof
			itemTag = trim(rsUDF("item_tag"))
			
			if(Request.Form(itemTag) = "1") then
				
				if(rsLineUDF.State = 1) then
				rsLineUDF.close
				end if
				sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = '" & rsUDF("item_tag") & "'"
				rsLineUDF.Open sql, Conn, 1, 4
			
			if(rsLineUDF.recordCount > 0) then
			response.write "<td>" & rsLineUDF("item_value") & "</td>"
			else
			response.write "<td>&nbsp;</td>"
			end if
			
			end if
			
			rsUDF.moveNext
			loop
			response.write "</tr>"
			
			rsProjects.moveNext
			loop
			%>
		<!--End results listing-->
	</table>
</body>
</html>
