	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Capabilities</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="overview.asp">Overview</a> | Capabilities</small>
					<br /><br />
					<img src="images/capabilities.jpg" alt="overview" />
					<br /><br />
					<b class="subheader">PIONEERING IDEAS</b><br />
					Our history is marked by firsts. Our team has consistently set goals beyond what seemed possible, and created opportunities where others didn't see any. From creating innovative public-private partnerships that transformed the scale and quality of military housing to developing a revolutionary model for affordable housing finance in Central America, Clark Realty Capital is always on the frontier of something new. &nbsp; <a href="portfolio_cat.asp?cid=21763558">See Projects &gt;&gt;</a>
					<br /><br />
					<b class="subheader">IMPROVING LIVES</b><br />
					We measure our success by way in which we impact the world around us. Our projects have helped individual families find a way into home ownership for the first time, created entirely new communities through innovative private-public partnerships, and changed the faces of inner-city cores with award-winning and enduring developments. &nbsp;  <a href="portfolio_cat.asp?cid=21763555">See Projects &gt;&gt;</a> 
					<br /><br />
					<b class="subheader">ADVANCING URBANISM</b><br />
					We see the advancement of existing cities and urban cores as both a sustainable and exciting way to develop.  Our approach to developing urban cores involves densifying these often transit-oriented neighborhoods - all within the context of the history, existing architecture, and current residents. Our mark on the urban fabric in cities across the country is a product of this careful balance and our drive to deliver the most desirable and sustainable neighborhoods. &nbsp;    <a href="portfolio_cat.asp?cid=21763426">See Projects &gt;&gt;</a> 
					<br /><br />
					<b class="subheader">BUILDING COMMUNITY</b><br />
					Ultimate success for us at the end of a project is to be considered by our neighbors as a member of their community.  We strive in our developments to unite and partner with communities in unique ways, inviting differing points of view to participate in the design process and creating developments that meet the needs of a variety of stakeholders. Our projects have benefited from our long-standing relationships with the communities in which we develop. &nbsp;    <a href="portfolio_cat.asp?cid=21763557">See Projects &gt;&gt;</a> 
					<br /><br />
					<b class="subheader">CHANGING HORIZONS</b><br />
					Our architectural achievements stand above the rest. Our signature buildings are iconic, often both physically and theoretically; representing architectural accomplishment and philosophical innovation. From redefining the skyline of Philadelphia with the first new residential highrise in a generation to repositioning an iconic Chicago skyscraper, we have developed landmarks in every sense. &nbsp;    <a href="portfolio_cat.asp?cid=21763553">See Projects &gt;&gt;</a>
					<br /><br />
					<b class="subheader">DEVELOPING RESPONSIBILITY</b><br />
					Our approach to conservation is progressive and takes on many forms, from energy efficiency to historic and ecological preservation. Our team has looked beyond the current trends to find ways to develop new solutions to drive adoption of renewable energy and repurpose some of the most challenging historic structures within a new modern design. The results of our commitment to developing responsibly can be seen in each and every one of our endeavors. &nbsp;    <a href="portfolio_cat.asp?cid=21763554">See Projects &gt;&gt;</a>
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/capabilities_side.jpg" alt="A privately held real estate company, CRC actively invests in, develops, finances, and manages award-winning real estate assets across the nation." />
				</td>

			</tr>
			<tr>
				<td class="left_col_overview">
					<!--#include file="includes\footer.asp" -->
					
				</td>
				
				<td class="right_col_overview">
				
					
				</td>

			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-2");
pageTracker._trackPageview();
</script></body>
</html>
