	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Affilitaes</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="overview.asp">Overview</a> | Affiliates</small>
					<br /><br />
					<img src="images/affiliates.jpg" alt="affiliates" />
					<br /><br />
					The Clark family of companies brings together multiple world-class companies under the singular umbrella of one diverse organization. Clark's culture of innovation and the passion to continually seek out new ideas has not only driven the growth of existing core businesses, but also spawned several new start-ups.
					<br /><br />

					<b class="subheader">DUXBURY FINANCIAL (DUX)</b><br />
					Duxbury Financial, a licensed FINRA broker-dealer and a member of SIPC, provides innovative financing solutions on large scale real estate transactions. The investment bank has been involved in the structuring of more than $6 billion of debt and equity. <br />  
					<a href="http://www.duxburyfinancial.com">View Website &gt;&gt;</a>
					<br /><br />
					<b class="subheader">CLARK BUILDERS GROUP (CBG)</b><br />
					Consistently ranked among the top ten multifamily builders in the nation, Clark Builders Group constructs high-quality residential communities throughout the U.S. Building more than 6,000 homes each year, CBG posted annual revenues of over $500 million in 2007. <br />  
					<a href="http://www.clarkbuildersgroup.com">View Website &gt;&gt;</a>
					<br /><br />
					<b class="subheader">CLARK CONSTRUCTION GROUP (CCG)</b><br />
					An affiliate of Clark Realty Capital, the Clark Construction Group boasts a 100-year history with over $4 billion in annual revenue and major projects. <br />  
					<a href="http://www.clarkconstruction.com">View Website &gt;&gt;</a>
					<br /><br />



					<b class="subheader">CLARK ENERGY GROUP (CEG)</b><br />
					A turn-key provider of energy management solutions for both the public and private sector, Clark Energy Group offers creative solutions to improve energy efficiency and reduce operations and maintenance costs of existing assets.<br />
					<a href="http://www.clarkenergygroup.com">View Website &gt;&gt;</a>
					<br /><br />


					<b class="subheader">INTER-MAC</b><br />
					Homeownership in Central America is now more achievable with the creation of Inter-Mac, a finance company specializing in affordable primary and secondary mortgages in emerging countries.<br />
					<a href="http://www.inter-mac.com/En.html">View Website &gt;&gt;</a>
					<br /><br />


					<b class="subheader">CENTERBOARD CAMPUS PARTNERS</b><br />
					Centerboard Campus Partners offers colleges and universities fresh, new solutions for implementing comprehensive master plans.  The firm�s expertise spans finance, development, construction, and operations. <br />
					<a href="http://www.centerboardcampus.com">View Website &gt;&gt;</a>
					<br /><br />




					<b class="subheader">CLEARVIEW VENTURES</b><br />
					An early stage venture capital company, Clearview Ventures helps entrepreneurs turn their ideas into reality. The company targets investments in promising start-ups in the Mid-Atlantic region.
					<br /><br />
					<b class="subheader">CLARK REALTY MANAGEMENT (CRM)</b><br />
					Clark Realty Management delivers superior resident services while preserving and enhancing the value of the real estate asset.
					<br /><br />
					<b class="subheader">CMAC FINANCIAL SERVICES</b><br />
					CMAC offers financial solutions to the construction industry.  Their QuickPay program helps subcontractors and suppliers grow and protect their businesses.
					<br /><br />
					<b class="subheader">GIC MADISON</b><br />
					By offering qualified contractors complementary insurance products, GIC Madison streamlines risk management on large-scale construction projects. 
					<br /><br />
					<b class="subheader">DEVELOPERS TITLE</b><br />
					Developers Title, a licensed title insurance agent in Maryland, Virginia, and the District of Columbia in an agency arrangement with a leading national title insurance company, provides title and settlement services for commercial real estate projects.
					<br /><br />
					<b class="subheader">CLARK CARES</b><br />
					Focused on community, advocacy, responsibility, education, and service, the Clark CARES Foundation is a 501(c)(3) public charity committed to improving local, national, and global communities by providing grants to deserving organizations.<br />
					<a href="http://www.clarkcares.org">View Website &gt;&gt;</a>
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/affiliates_side.jpg" alt="A privately held real estate company, CRC actively invests in, develops, finances, and manages award-winning real estate assets across the nation." />
				</td>

			</tr>
			<tr>
				<td class="left_col_overview">
					<!--#include file="includes\footer.asp" -->
					
				</td>
				
				<td class="right_col_overview">
				
					
				</td>

			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-2");
pageTracker._trackPageview();
</script></body>
</html>
