	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - History</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%"  id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="overview.asp">Overview</a> | History</small>
					<br /><br />
					<img src="images/history.jpg" alt="overview" />
					<br /><br />
					In 1906, George Hyman began his construction operations in Washington, D.C. Through competitiveness and a commitment to consistently meet the needs of its clients, The George Hyman Construction Company grew to become one of the nation�s most respected general contractors. In 1977 the firm created OMNI Construction to respond to the demand for merit shop contracting by the private development community in Washington, D.C. For the next 18 years, the two companies continued to grow and expand their services and capabilities. In 1996, Hyman and OMNI merged and now operate as The Clark Construction Group, Inc.  
					<br /><br />
					In 1992, the company formed OMNI Residential to meet the rising need for stick-built construction in the burgeoning DC real estate market. The company began its geographic expansion in 1996 as Clark Realty Builders. Over the years, the firm gained expertise across the nation taking on complex, higher-density projects that included concrete construction and underground parking, in addition to entering new markets like privatized military housing, which expanded the company�s repertoire into townhome, duplex, and single-family home construction.
					<br /><br />
					The company now operates as Clark Builders Group, which with a diverse portfolio of both wood-framed and concrete construction projects across a variety of product types and geographic areas, is today the third largest multifamily builder in the nation.  

					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/history_side.jpg" alt="Overview" />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
