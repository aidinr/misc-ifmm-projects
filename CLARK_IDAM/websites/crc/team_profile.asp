	<!--#include file="includes\config.asp" -->
<%	
	theID = prep_sql(request.querystring("id"))

	set rsProfile=server.createobject("adodb.recordset")
	sql="select a.active, userid,firstname,lastname,d.item_value office, position, e.item_value quote, f.item_value testimonial from ipm_user a, ipm_user_field_value c, ipm_user_field_value d, ipm_user_field_value e, ipm_user_field_value f where c.item_id = 21767014 and c.user_id = a.userid and c.item_value = '1' and d.user_id = a.userid and d.item_id = 21767007 and e.user_id = a.userid and e.item_id = 21766996 and f.user_id = a.userid and f.item_id = 21766997 and userid = " & theID
	rsProfile.Open sql, Conn, 1, 4	
	
	set rsRelated=server.createobject("adodb.recordset")
	'sql = "select * from ipm_project_contact a, ipm_project b, ipm_project_field_desc c,ipm_project_field_value d where available = 'y' and a.projectid = b.projectid and c.item_tag = 'IDAM_PROJECT_CC_WEBSITE' and c.item_id = d.item_id and d.projectid = a.projectid and d.item_value = '1' and a.userid = " & theID
	sql = "select top 5 * from ipm_project_contact a, ipm_project b, ipm_project_field_desc c,ipm_project_field_value d where available = 'y' and b.available = 'Y' and b.publish = 1 and a.projectid = b.projectid and c.item_tag = 'IDAM_PROJECT_CC_WEBSITE' and c.item_id = d.item_id and d.projectid = a.projectid  and d.item_value = '1' and a.userid = " & theID
	rsRelated.Open sql, Conn, 1, 4
%>	
	
	

<html>
<head>
<title><%=sPageTitle%> - Profile</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<% if UCase(rsProfile("active")) = "Y" then %>
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="culture.asp">Culture</a> | <%=rsProfile("FirstName")%></small>
					<br /><br />
					
					<big><b><%=rsProfile("FirstName")%></b></big><br /><b><%=ucase(rsProfile("position"))%>, <%=rsProfile("office")%></b>
					<br /><br />
					<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
					<td width="340" valign="top" align="left"><img src="<%=sAssetPath%><%=rsProfile("userID")%>&Instance=<%=siDAMInstance%>&type=contact&size=1&width=325&height=240&qfactor=<%=assetQFactor%>" alt="<%=rsProfile("FirstName") & " " & rsProfile("LastName")%>" /></td>
					<td align="left" valign="middle"><div class="profile_quote"><big><%=ucase(rsProfile("quote"))%></big></div></td>
					</tr>
					</table>
					<br /><br />
					<%=replace(rsProfile("testimonial"),VbCrLF,"<br />")%>
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/team_profile_side.jpg" alt="Related Projects" />
					<br /><br />
					<%do while not rsRelated.eof%>
						<table cellpadding="0" cellspacing="0">
							<tr>	<td valign="top" align="left"><div class="portfolio_bg"><a href="project.asp?pid=<%=rsRelated("projectID")%>"><img width="137" height="108" class="portfolio_photo" src="<%=sAssetPath%><%=rsRelated("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=137&height=108&qfactor=<%=assetQFactor%>" alt="<%=trim(rsRelated("name"))%>"/></a></div></td>
								<td valign="top" align="left"><b><%=trim(rsRelated("name"))%></b><br /><%=rsRelated("city")%>, <%=rsRelated("state_id")%></td>
							</tr>
						</table>
					<%
					rsRelated.moveNext
					loop
					%>
				</td>

			</tr>
			<tr>
				<td class="left_col_overview">
					<!--#include file="includes\footer.asp" -->
					
				</td>
				
				<td class="right_col_overview">
				
					
				</td>

			</tr>
			<% Else %>
		 	  <tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="culture.asp">Culture</a> | </small>
					<br /><br />
					
					<big><b>Record Archived</b></big><br /><b></b>
					<br /><br />
					<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
					
					</tr>
					</table>
					
					 This record has been archived. Please refer back to the <a href="culture.asp">Culture Page</a>.
					</div>
					
				</td>
			   </tr>

			<%End If%>			

			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
