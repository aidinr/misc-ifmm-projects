<!--#include file="includes\config.asp" -->
<%
	set rsUDF=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_desc where active = 1 order by item_name"
	rsUDF.Open sql, Conn, 1, 4
	
	set rsClient = server.createobject("adodb.recordset")
	sql = "select * from ipm_client where active = 1 order by name"
	rsClient.Open sql, Conn, 1, 4
	
	set rsDisc = server.createobject("adodb.recordset")
	sql = "select * from ipm_discipline where keyuse = 1 order by keyname"
	rsDisc.Open sql, Conn, 1, 4

%>

<html>
<head>
<title><%=sPageTitle%> Reports Tool</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body onload="if(document.getElementById('pdf_radio').checked == true) { document.getElementById('pdf_options').style.visibility='visible'; }">
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			
			<div id="content">

			<br />
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview" style="width:100%;">
					<div id="content_text">
					Marketing Tools | Generate Project Report
					<br /><br />
					<img src="images/title_project_reporting.jpg" alt="Project Reporting" />
					<h4>Step 1 - Select Projects</h4>
					<form method="post" action="report_print.asp">
	<table cellpadding="3" cellspacing="3">
			<tr>
				<td>Conditions: </td>
				<td></td>
			</tr>
			<tr>
			<td valign="top"></td>
			<td valign="top">
				<table>
					<tr>
						<td>Project Name Contains:</td>
						<td><input type="text" name="search_name" /></td>
					</tr>
					<tr>
						<td>Project Client:</td>
						<td>	<select name="search_client">
							<option value="">All</option>
							<%
							do while not rsClient.eof
							clientName = trim(rsClient("Name"))
							%>
							
							<option value="<%=clientName%>"><%=clientName%></option>
							
							<%rsClient.moveNext
							loop							
							
							%>
							</select>
						</td>
					</tr>					
					<tr>
						<td>Project Type:</td>
						<td><select name="search_discipline">
						<option value="">All</option>
							<%
							do while not rsDisc.eof
							keyName = trim(rsDisc("keyName"))
							%>
							
							<option value="<%=rsDisc("KeyID")%>"><%=keyName%></option>
							
							<%rsDisc.moveNext
							loop							
							
							%>
							</select></td>
					</tr>
<tr>
						<td>Favorite Projects:</td>
						<td>
						<input name="search_favorites" type="radio" checked="checked" value="" /> All  <input type="radio" name="search_favorites" value="1" /> Favorites  <input type="radio" name="search_favorites" value="0" />Non-Favorites
</td>
					</tr>
					<tr>
						<td>Published Projects:</td>
						<td>
						<input type="radio" name="search_published" checked="checked" value="" /> All  <input type="radio" name="search_published" value="1" /> Published  <input type="radio" name="search_published" value="0" />Non-Published
</td>
					</tr>						
					<tr>
						<td>Project Region:</td>
						<td><select name="search_region">
							<option value="">All</option>
							<option value="Northeast">Northeast</option>
							<option value="Northwest">Northwest</option>
							<option value="Southeast">Southeast</option>
							<option value="Southwest">Southwest</option>
						    </select>
						</td>
					</tr>
					<tr>
						<td>Project State:</td>
						<td><select name="search_state">
								<option value="">All</option>
								
								<option value="AK">AK</option>
								<option value="AL">AL</option>
								<option value="AR">AR</option>
								<option value="AZ">AZ</option>

								<option value="CA">CA</option>
								<option value="CO">CO</option>
								<option value="CT">CT</option>
								<option value="DC">DC</option>
								<option value="DE">DE</option>
								<option value="FL">FL</option>

								<option value="GA">GA</option>
								<option value="HI">HI</option>
								<option value="IA">IA</option>
								<option value="ID">ID</option>
								<option value="IL">IL</option>
								<option value="IN">IN</option>

								<option value="KS">KS</option>
								<option value="KY">KY</option>
								<option value="LA">LA</option>
								<option value="MA">MA</option>
								<option value="MD">MD</option>
								<option value="ME">ME</option>

								<option value="MI">MI</option>
								<option value="MN">MN</option>
								<option value="MO">MO</option>
								<option value="MS">MS</option>
								<option value="MT">MT</option>
								<option value="NC">NC</option>

								<option value="ND">ND</option>
								<option value="NE">NE</option>
								<option value="NH">NH</option>
								<option value="NJ">NJ</option>
								<option value="NM">NM</option>
								<option value="NV">NV</option>

								<option value="NY">NY</option>
								<option value="OH">OH</option>
								<option value="OK">OK</option>
								<option value="OR">OR</option>
								<option value="PA">PA</option>
								<option value="RI">RI</option>

								<option value="SC">SC</option>
								<option value="SD">SD</option>
								<option value="TN">TN</option>
								<option value="TX">TX</option>
								<option value="UT">UT</option>
								<option value="VA">VA</option>

								<option value="VT">VT</option>
								<option value="WA">WA</option>
								<option value="WI">WI</option>
								<option value="WV">WV</option>
								<option value="WY">WY</option>
							</select>
</td>
					</tr>
	
				
					
				</table>
			</td>
		</tr>
</table>

<h4>Step 2 - Select Formatting Options</h4>

<table cellpadding="3" cellspacing="3">
		<tr>
			<td valign="top">Report Name: </td>
			<td><input type="text" name="report_name" /></td>
		</tr>	
					<tr>
						<td valign="top">Output:</td>
						<td> <input name="search_output" type="radio" value="html" checked="checked" onclick="document.getElementById('pdf_options').style.visibility='hidden';" />HTML  <input id="pdf_radio" name="search_output" type="radio" value="pdf" onclick="document.getElementById('pdf_options').style.visibility='visible';" />PDF  <input name="search_output" type="radio" value="csv" onclick="document.getElementById('pdf_options').style.visibility='hidden';" />CSV
						     <div id="pdf_options" style="visibility:hidden;">Page break interval: <input type="text" size="2" name="pdf_page_break" id="pdf_page_break" value="3" /></div>
						</td>
					</tr>
					<tr>
						<td valign="top">Display Image:</td>
						<td> <input name="search_display_image" type="checkbox" checked="checked" value="1" />
						</td>
					</tr>			
		<tr>
			<td valign="top">Fields to display: </td>
			<td></td>
		</tr>
		<tr>
			<td valign="top">Standard Fields</td>

			<td>
			<input type="checkbox" class="udf" name="number" value="1" /> Project Number<br />
			<input type="checkbox" class="udf" name="client" value="1" /> Client<br />
			<input type="checkbox" class="udf" name="address" value="1" /> Address<br />
			<input type="checkbox" class="udf" name="city_state" value="1" /> Location (City, State)<br />
			<input type="checkbox" class="udf" name="zip" value="1" /> ZIP<br />
			<input type="checkbox" class="udf" name="desc" value="1" /> Brief Description<br />
			</td>
		</tr>
			<td valign="top">User-Defined Fields</td>
			<td>
			<%
			do while not rsUDF.eof
			itemTag = trim(rsUDF("item_tag"))
			%>
			
			<input type="checkbox" class="udf" name="<%=itemTag%>" value="1" /> <%=rsUDF("item_name")%> <br />
			
			<%rsUDF.moveNext
			loop
			
			%>
			</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" value="Generate Report" /></td>
		</tr>
<input type="hidden" value="1" name="submit" />
</form>
					
					
					
					
					
					</div>
					
				</td>
				


			</tr>
	
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
