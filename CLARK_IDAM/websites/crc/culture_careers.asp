	<!--#include file="includes\config.asp" -->
<%	
	set rsJobsNE=server.createobject("adodb.recordset")
	sql = "select * FROM ipm_jobs a, ipm_jobs_type b WHERE a.department = b.type_id AND SHOW = 1 AND POST_DATE < getdate() AND PULL_DATE > getdate() AND b.type_value like 'CRC NE%' ORDER BY b.type_value, Job_Title"
	rsJobsNE.Open sql, Conn, 1, 4

	set rsJobsSE=server.createobject("adodb.recordset")
	sql = "select * FROM ipm_jobs a, ipm_jobs_type b WHERE a.department = b.type_id AND SHOW = 1 AND POST_DATE < getdate() AND PULL_DATE > getdate() AND b.type_value like 'CRC SE%' ORDER BY b.type_value, Job_Title"
	rsJobsSE.Open sql, Conn, 1, 4	

	set rsJobsNW=server.createobject("adodb.recordset")
	sql = "select * FROM ipm_jobs a, ipm_jobs_type b WHERE a.department = b.type_id AND SHOW = 1 AND POST_DATE < getdate() AND PULL_DATE > getdate() AND b.type_value like 'CRC NW%' ORDER BY b.type_value, Job_Title"
	rsJobsNW.Open sql, Conn, 1, 4	

	set rsJobsSW=server.createobject("adodb.recordset")
	sql = "select * FROM ipm_jobs a, ipm_jobs_type b WHERE a.department = b.type_id AND SHOW = 1 AND POST_DATE < getdate() AND PULL_DATE > getdate() AND b.type_value like 'CRC SW%' ORDER BY b.type_value, Job_Title"
	rsJobsSW.Open sql, Conn, 1, 4		

%>
<html>
<head>
<title><%=sPageTitle%> - Careers</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="culture.asp">Culture</a> | Careers</small>
					<br /><br />
					<img src="images/title_careers.jpg" alt="Careers" />
					<br /><br />
					Clark Realty Capital seeks highly motivated and entrepreneurial individuals, undaunted by the responsibility that comes with the freedom to realize big ideas.  To promote a diversity of thinking, we place a great deal of emphasis on finding and hiring the brightest candidates with eclectic backgrounds. A relentless pursuit of new ideas and the camaraderie that forms from vetting these ideas provide employees with the opportunity to learn and grow.
					<br /><br />
					<%if (rsJobsNE.recordCount = 0 and rsJobsNW.recordCount = 0 and rsJobsSE.recordCount = 0 and rsJobsSW.recordCount = 0) then%>
					<b>No positions available.</b>
					<%end if%>
					<%if not (rsJobsNE.recordCount = 0) then %><b class="subheader">NORTHEAST</b><br /><br /><%end if%>
					<%
					i=0
					do while not (rsJobsNE.eof)%>
					<b><%=rsJobsNE("Job_Title") & " - " & replace(rsJobsNE("type_value"),"CRC NE","") & " Office"%></b><br />
					<%=rsJobsNE("Description")%> <a href="culture_form.asp?region=<%=rsJobsNE("type_id")%>&job=<%=rsJobsNE("Job_ID")%>">Apply online &gt;</a>
					<br /><br />
					<%
					i = i + 1
					rsJobsNE.movenext
					loop
					
					%>

					<%if not (rsJobsSE.recordCount = 0) then %><b class="subheader">SOUTHEAST</b><br /><br /><%end if%>
					<%
					i=0
					do while not (rsJobsSE.eof)%>
					<b><%=rsJobsSE("Job_Title") & " - " & replace(rsJobsSE("type_value"),"CRC SE","") & " Office"%></b><br />
					<%=rsJobsSE("Description")%> <a href="culture_form.asp?region=<%=rsJobsSE("type_id")%>&job=<%=rsJobsSE("Job_ID")%>">Apply online &gt;</a>
					<br /><br />
					<%
					i = i + 1
					rsJobsSE.movenext
					loop
					
					%>

					<%if not (rsJobsSW.recordCount = 0) then %><b class="subheader">SOUTHWEST</b><br /><br /><%end if%>
					<%
					i=0
					do while not (rsJobsSW.eof)%>
					<b><%=rsJobsSW("Job_Title") & " - " & replace(rsJobsSW("type_value"),"CRC SW","") & " Office"%></b><br />
					<%=rsJobsSW("Description")%> <a href="culture_form.asp?region=<%=rsJobsSW("type_id")%>&job=<%=rsJobsSW("Job_ID")%>">Apply online &gt;</a>
					<br /><br />
					<%
					i = i + 1
					rsJobsSW.movenext
					loop
					
					%>	
					
					<%if not (rsJobsNW.recordCount = 0) then %><b class="subheader">NORTHWEST</b><br /><br /><%end if%>
					<%
					i=0
					do while not (rsJobsNW.eof or i = 4)%>
					<b><%=rsJobsNW("Job_Title") & " - " & replace(rsJobsNW("type_value"),"CRC NW","") & " Office"%></b><br />
					<%=rsJobsNW("Description")%> &nbsp; <a href="culture_form.asp?region=<%=rsJobsNW("type_id")%>&job=<%=rsJobsNW("Job_ID")%>">Apply online &gt;</a>
					<br /><br />
					<%
					i = i + 1
					rsJobsNW.movenext
					loop
					
					%>					
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/careers_side.jpg" alt="Working for CRC" />
				</td>

			</tr>
			<tr>
				<td class="left_col_overview">
					<!--#include file="includes\footer.asp" -->
					
				</td>
				
				<td class="right_col_overview">
				
					
				</td>

			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-2");
pageTracker._trackPageview();
</script></body>
</html>
