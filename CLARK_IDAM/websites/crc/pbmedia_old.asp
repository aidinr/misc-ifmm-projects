	<!--#include file="includes\config.asp" -->
<html>
<head>
<title><%=sPageTitle%> - Pacific Beacon Ribbon-Cutting</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small> </small>
					<br /><br />



<font face="Century Gothic" color="#703412" size="5">PACIFIC BEACON RIBBON-CUTTING</font><br>
<br />
<br />

<font face="Century Gothic" color="#703412" size="3">PROJECT PHOTOGRAPHY</font><br>



					<br />

All photography is copyright of Clark Realty Capital, LLC.  For more information, please contact Joy Lutes at 877.846.7760


					<table width="550">
					<tr>
					<td valign="top" style="padding-top:1px;">





<p>

<br />
<ul>
	<li>
	<a href="images/Amy Fellows 2.jpg" target="_blank">Amy Fellows 2.jpg</a>
	</li>

	<li>
	<a href="images/Amy Fellows Photography.jpg" target="_blank">Amy Fellows Photography.jpg</a>
	</li>
	
	<li>
	<a href="images/David Hebble.jpg" target="_blank">David Hebble.jpg</a>
	</li>

	<li>
	<a href="images/David Hebble 1.jpg" target="_blank">David Hebble 1.jpg</a>
	</li>
	
	<li>
	<a href="images/David Hebble 2.jpg" target="_blank">David Hebble 2.jpg</a>
	</li>
	
	<li>
	<a href="images/David Hebble 3.jpg" target="_blank">David Hebble 3.jpg</a>
	</li>
	
	<li>
	<a href="images/David Hebble 4.jpg" target="_blank">David Hebble 4.jpg</a>
	</li>
	
	<li>
	<a href="images/David Hebble 5.jpg" target="_blank">David Hebble 5.jpg</a>
	</li>
	
	<li>
	<a href="images/David Hebble 6.jpg" target="_blank">David Hebble 6.jpg</a>
	</li>
	
	<li>
	<a href="images/David Hebble 7.jpg" target="_blank">David Hebble 7.jpg</a>
	</li>
	
	<li>
	<a href="images/David Hebble 8.jpg" target="_blank">David Hebble 8.jpg</a>
	</li>		
</ul>
<br>
<br>

<font face="Century Gothic" color="#703412" size="3">EVENT PHOTOGRAPHY</font><br></p>


Photos should be available after 2:00 PM EST on Thursday, March 26

					</td>
					</tr>
					</table>
					</div>
					
				</td>
				
				<td class="right_col_overview">
				<div>
				<img src="images/bg_contact.jpg" alt="CBG" />
				</div>
					
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-2");
pageTracker._trackPageview();
</script></body>
</html>
