<!--#include file="includes\config.asp" -->

<%
' returns string the can be written where you would like the reCAPTCHA challenged placed on your page
function recaptcha_challenge_writer(publickey)
recaptcha_challenge_writer = "<script type=""text/javascript"">" & "var RecaptchaOptions = {" & "   theme : 'white'," & "   tabindex : 0" & "};" & "</script>" & "<script type=""text/javascript"" src=""http://api.recaptcha.net/challenge?k=" & publickey & """></script>" & "<noscript>" &   "<iframe src=""http://api.recaptcha.net/noscript?k=" & publickey &""" frameborder=""1""></iframe><br>" &    "<textarea name=""recaptcha_challenge_field"" rows=""3""cols=""40""></textarea>" &    "<input type=""hidden"" name=""recaptcha_response_field""value=""manual_challenge"">" & "</noscript>"
end function

function recaptcha_confirm(privkey,rechallenge,reresponse)
' Test the captcha field

Dim VarString
VarString = "privatekey=" & privkey & "&remoteip=" & Request.ServerVariables("REMOTE_ADDR") &  "&challenge=" & rechallenge & "&response=" & reresponse

Dim objXmlHttp
Set objXmlHttp = Server.CreateObject("Msxml2.ServerXMLHTTP")
objXmlHttp.open "POST", "http://api-verify.recaptcha.net/verify",False
objXmlHttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
objXmlHttp.send VarString

Dim ResponseString
ResponseString = split(objXmlHttp.responseText, vblf)
Set objXmlHttp = Nothing

if ResponseString(0) = "true" then
  'They answered correctly
   recaptcha_confirm = "true"
else
  'They answered incorrectly
   recaptcha_confirm = ResponseString(1)
end if

end function

function chkEmail(theAddress)
   ' checks for a vaild email
   ' returns 1 for invalid addresses
   ' returns 1 for invalid addresses
   dim atCnt
   chkEmail = 0

   ' chk length
   if len(theAddress) < 5  then 
      ' a@b.c should be the shortest an
      ' address could be
      chkEmail = 1
 
   ' chk format
   ' has at least one "@"
   elseif instr(theAddress,"@") = 0 then
      chkEmail = 1
 
   ' has at least one "."
   elseif instr(theAddress,".") = 0 then
      chkEmail = 1
 
   ' has no more than 3 chars after last "."
   elseif len(theAddress) - instrrev(theAddress,".") > 3 then
      chkEmail = 1
 
   ' has no "_" after the "@"
   elseif instr(theAddress,"_") <> 0 and _
       instrrev(theAddress,"_") > instrrev(theAddress,"@")  then
      chkEmail = 1

   else
      ' has only one "@"
      atCnt = 0
      for i = 1 to len(theAddress)
         if  mid(theAddress,i,1) = "@" then
            atCnt = atCnt + 1
         end if
      next
 
      if atCnt > 1 then
         chkEmail = 1
      end if

      ' chk each char for validity
      for i = 1 to len(theAddress)
        if  not isnumeric(mid(theAddress,i,1)) and (lcase(mid(theAddress,i,1)) < "a" or lcase(mid(theAddress,i,1)) > "z") and mid(theAddress,i,1) <> "_" and mid(theAddress,i,1) <> "." and mid(theAddress,i,1) <> "@" and mid(theAddress,i,1) <> "-" then
            chkEmail = 1
        end if
      next
  end if
end function

Function stripHTML(strHTML)
'Strips the HTML tags from strHTML

  Dim objRegExp, strOutput
  Set objRegExp = New Regexp

  objRegExp.IgnoreCase = True
  objRegExp.Global = True
  objRegExp.Pattern = "<(.|\n)+?>"

  'Replace all HTML tag matches with the empty string
  strOutput = objRegExp.Replace(strHTML, "")
  
  'Replace all < and > with &lt; and &gt;
  strOutput = Replace(strOutput, "<", "&lt;")
  strOutput = Replace(strOutput, ">", "&gt;")
  
  stripHTML = strOutput    'Return the value of strOutput

  Set objRegExp = Nothing
End Function

captcha_challenge = Request.Form("recaptcha_challenge_field")
captcha_response = Request.Form("recaptcha_response_field")

if(instr(Request.ServerVariables("server_name"),"clarkbuilders")) then
captcha_private_key = "6LcsegEAAAAAAN-6IuKDzMKZtPRl-0z3odvIlEEF" 'Clarkbuildersgroup.com
elseif(instr(Request.ServerVariables("server_name"),"idam.clarkrealty")) then
captcha_private_key = "6LekfAEAAAAAAHl24fg8NZ_CI0Pq1Oe8pqvj35vT" 'idam.clarkrealty.com
elseif(instr(Request.ServerVariables("server_name"),"clarkrealtycapital")) then
captcha_private_key = "6LfQJAIAAAAAAN6lTT7cJ4Fzn2S4YxZF96_xqt9M" 'clarkrealtycapital.com
end if

from = Request.Form("from")
addresses = Request.Form("addresses")
comments = replace(stripHTML(Request.form("comments")),VbCrLf,"<br />")

if(Request.Form("submit") = "1") then
	if(recaptcha_confirm(captcha_private_key,captcha_challenge,captcha_response) = "true") then
		    		validate_captcha = "1"
				validate_from = "1"
				validate_addresses = "1"
	
				if(from = "") then
				validate_from = "0"
				end if
				
			    if(addresses = "") then
					validate_addresses = "0"
			    else
					addresses = replace(replace(addresses," ",""),VbCrLf,"")
					
					AddressesArray = Split(addresses, ",")
					
					For Each LineAddress in AddressesArray
						if(chkEmail(LineAddress) = 1) then
							validate_addresses = "0"
							Exit For
						end if
					Next
			    end if
			    
			    if(validate_from = "1" and validate_addresses="1") then
			    	
			    		For Each LineAddress in AddressesArray
					
			    			Set Mail = Server.CreateObject("Persits.MailSender")
					
						'Mail.Host = "mail.clarkrealty.com" ' Specify a valid SMTP server
						Mail.Host = "localhost" ' Specify a valid SMTP server
						
						Mail.IsHTML = True
					
						Mail.From = "no-reply@clarkcapitalrealty.com"
			
						Mail.FromName = from
			
						Mail.Subject = "Announcing Our New Web Site"
			
						
						
						Mail.Body = "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd"">" & VbCrLf
						Mail.Body = Mail.Body & "<html>" & VbCrLf
						Mail.Body = Mail.Body & "<head>" & VbCrLf
						Mail.Body = Mail.Body & "<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-=1"">" & VbCrLf
						Mail.Body = Mail.Body & "<title>Announcing Our New Web Site!</title>" & VbCrLf
						Mail.Body = Mail.Body & "</head>" & VbCrLf
						Mail.Body = Mail.Body & "<body style=""padding:0; margin:0;"" bgcolor=""#ffffff"">" & VbCrLf
						Mail.Body = Mail.Body & "<div align=""center"" style=""padding:0; margin:0; background-color:#ffffff;border:0;"">" & VbCrLf
						Mail.Body = Mail.Body & "<div align=""center"" style=""font-size:10px;font-family:arial;"">" & VbCrLf
						Mail.Body = Mail.Body & "<i>Can't see images? View the online version by clicking <a href=""http://www.clarkrealtycapital.com/email_blast/email.html"">here</a></i>" & VbCrLf
						Mail.Body = Mail.Body & "</div>" & VbCrLf
						Mail.Body = Mail.Body & "<br />" & VbCrLf
						Mail.Body = Mail.Body & "<div style=""border:0;padding:0;margin:0;width:600px;font-family: Arial,Verdana,Helvetica,sans-serif; font-size: 16px;"" align=""left"">"
						Mail.Body = Mail.Body & comments
						Mail.Body = Mail.Body & "</div>"
						Mail.Body = Mail.Body & "<br />"
						Mail.Body = Mail.Body & "<div align=""center"" style=""padding:0; margin:0; background-color:#ffffff;border:0;width:600px;"">" & VbCrLf
						Mail.Body = Mail.Body & "<table width=""600"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" style=""padding:0;border:0;margin:0;font-family:Arial, Verdana, Helvetica, sans-serif; font-size:11px"">" & VbCrLf
						Mail.Body = Mail.Body & "<tr>" & VbCrLf
						Mail.Body = Mail.Body & "<td width=""378"" valign=""top"">" & VbCrLf
						Mail.Body = Mail.Body & "<img width=""378"" height=""152"" src=""http://idam.clarkrealty.com/public/crc/email_blast/header_left.jpg"" alt=""CRC""/>" & VbCrLf
						Mail.Body = Mail.Body & "</td>" & VbCrLf
						Mail.Body = Mail.Body & "<td width=""222"" style=""background-color:#fbefce;"">" & VbCrLf
						Mail.Body = Mail.Body & "<img width=""222"" height=""123"" src=""http://idam.clarkrealty.com/public/crc/email_blast/header_right.jpg"" alt=""CRC""/>" & VbCrLf
						Mail.Body = Mail.Body & "<br />" & VbCrLf
						Mail.Body = Mail.Body & "<a href=""http://www.clarkrealtycapital.com/email_blast.asp""><img width=""222"" height=""29"" border=""0"" src=""http://idam.clarkrealty.com/public/crc/email_blast/send.jpg"" alt=""Forward this to a friend""/></a>" & VbCrLf
						Mail.Body = Mail.Body & "</td>" & VbCrLf
						Mail.Body = Mail.Body & "</tr>" & VbCrLf
						Mail.Body = Mail.Body & "</table>" & VbCrLf
						Mail.Body = Mail.Body & "<table width=""600"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" style=""padding:0;border:0;margin:0;font-family:Arial, Verdana, Helvetica, sans-serif; font-size:16px;line-height:26px;"">" & VbCrLf
						Mail.Body = Mail.Body & "<tr>" & VbCrLf
						Mail.Body = Mail.Body & "<td width=""378"" height=""157"" valign=""top"" align=""left"" valign=""top"">" & VbCrLf
						Mail.Body = Mail.Body & "<div align=""left"" style=""padding:15px;border:0;margin:0;font-family:Arial, Verdana, Helvetica, sans-serif; font-size:12px;line-height:24px;"">" & VbCrLf
						Mail.Body = Mail.Body & "<img width=""347"" height=""19"" src=""http://idam.clarkrealty.com/public/crc/email_blast/announcing.jpg"" alt=""Announcing our new web site!"" />" & VbCrLf
						Mail.Body = Mail.Body & "<br /><br />" & VbCrLf
						Mail.Body = Mail.Body & "<a style=""font-weight:bold;font-family:Arial;color:#0051a2;font-size:16px;line-height:24px;text-decoration:none;"" href=""http://www.clarkrealtycapital.com"">WWW.CLARKREALTYCAPITAL.COM</a>" & VbCrLf
						Mail.Body = Mail.Body & "<br /><br />" & VbCrLf
						Mail.Body = Mail.Body & "Established in 1992, Clark Realty Capital leverages the same entrepreneurial spirit and culture fostered more than a decade ago to make lasting impacts on the world around us.</div>" & VbCrLf
						Mail.Body = Mail.Body & "</td>" & VbCrLf
						Mail.Body = Mail.Body & "<td width=""222"" height=""157"" align=""center"" style=""background-color:#fbefce;"">&nbsp;</td>" & VbCrLf
						Mail.Body = Mail.Body & "</tr>" & VbCrLf
						Mail.Body = Mail.Body & "</table>" & VbCrLf
						Mail.Body = Mail.Body & "<table width=""600"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" style=""padding:0;border:0;margin:0;font-family:Arial, Verdana, Helvetica, sans-serif; font-size:12pxline-height:26px;"">" & VbCrLf
						Mail.Body = Mail.Body & "<tr>" & VbCrLf
						Mail.Body = Mail.Body & "<td width=""273"" valign=""top"">" & VbCrLf
						Mail.Body = Mail.Body & "<div align=""left"" style=""padding:15px;border:0;margin:0;font-family:Arial, Verdana, Helvetica, sans-serif; font-size:12px;line-height:26px;"">Clark Realty Capitalís experience spans more than 60 projects, 38,000 residential units, 10,000 acres of land, and 50 million SF of development, comprising nearly $7 billion of pubic and private work developed or under development." & VbCrLf
						Mail.Body = Mail.Body & "<br /><br />" & VbCrLf
						Mail.Body = Mail.Body & "<b>Meet our team and check out our latest developments at:</b>" & VbCrLf
						Mail.Body = Mail.Body & "<br />" & VbCrLf
						Mail.Body = Mail.Body & "<a href=""http://www.clarkrealtycapital.com/"" style=""font-weight:bold;font-family:Arial;color:#0051a2;font-size:12px;line-height:24px;text-decoration:none;"">WWW.CLARKREALTYCAPITAL.COM</a>" & VbCrLf
						Mail.Body = Mail.Body & "<br /><br />" & VbCrLf
						Mail.Body = Mail.Body & "<a style=""color:#ffffff;text-decoration:none;font-size:18px;"" href=""http://www.clarkrealtycapital.com/email_blast.asp""><img  width=""222"" height=""29"" border=""0"" src=""http://idam.clarkrealty.com/public/crc/email_blast/forward.jpg"" alt=""Forward this to a friend"" /></a>" & VbCrLf
						Mail.Body = Mail.Body & "</div>" & VbCrLf
						Mail.Body = Mail.Body & "</td>" & VbCrLf
						Mail.Body = Mail.Body & "<td width=""327"" valign=""top"">" & VbCrLf
						Mail.Body = Mail.Body & "<img width=""327"" height=""425"" src=""http://idam.clarkrealty.com/public/crc/email_blast/feature_image.jpg"" alt=""Featured Project Image"" />" & VbCrLf
						Mail.Body = Mail.Body & "</td>" & VbCrLf
						Mail.Body = Mail.Body & "</tr>" & VbCrLf
						Mail.Body = Mail.Body & "</table>" & VbCrLf
						Mail.Body = Mail.Body & "</div>" & VbCrLf
						Mail.Body = Mail.Body & "</div>" & VbCrLf
						Mail.Body = Mail.Body & "</body>" & VbCrLf
						Mail.Body = Mail.Body & "</html>"			
						Mail.AddAddress LineAddress
						
						on error resume next     
						Mail.Send
			
					       
			
						If Err <> 0 Then
							    blnSent = "0"
							    mailError = true
			
						else     
							    blnSent = "1"
			
						End If
						set Mail = nothing
					Next
			    end if
			    
	else
		validate_captcha = "0"
	end if
	
end if

%> 


<html>
<head>
<title><%=sPageTitle%> - Send CRC E-mail Blast</title>
	<!--#include file="includes\header.asp" -->	
</head>
<body style="background-image:url('images/bg_tile_no_menu.jpg');">
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			<div id="content" style="background:#fff;">

					<div id="content_text">
					<br /><br />
					<img src="images/launching_our_new_web_site.jpg" alt="Launching Our New Website" />
					<br /><br />
					<table id="main_table" width="950">
					<tr><td valign="top" style="color:#686868;">
					
					<%if(mailError = true) then%>
					<b>There was a technical error. Please try again later.</b>
					<%end if%>
					
					<%
					if(validate_captcha = "1" and validate_from = "1" and validate_addresses = "1") then
					from = ""
					addresses = ""
					comments = ""
					%>
					<b>Thank you for spreading the word about our new site! To send this to send this to another contact(s), please complete the form below.</b><br /><br />
					<%end if
					
					if(validate_captcha = "0") then %>
					<b>Please type in the correct words as they are shown on the image.</b> <br /><br />
					<%
					end if
					if(validate_from = "0") then%>
					<b>Please enter your name.</b> <br /><br />
					<%
					end if
					if(validate_addresses = "0") then%>
					<b>You have entered one or more invalid e-mail addresses.</b><br /><br />
					<%end if%>
					
					<form method="post" action="email_blast.asp" name="feedbackForm" onsubmit="return performCheck('feedbackForm', rules, 'classic');">
						<b style="margin-left:10px;">Enter your name</b><br />
						<input type="text" name="from" style="width:350px;border:1px solid #cfcbbf;" value="<%=from%>" /><br /><br />
						<b style="margin-left:10px;">Enter Email Addresses of Colleagues, Friends, and Families</b><br /><b style="margin-left:10px;">(Separate Multiple Addresses with a Comma)</b><br />
						<textarea style="width:350px;height:150px;border:1px solid #cfcbbf;" name="addresses"><%=addresses%></textarea><br /><br />
						<b style="margin-left:10px;">(Optional) Add a Comment for Recipients</b><br />
						<textarea style="width:350px;height:150px;border:1px solid #cfcbbf;" name="comments"><%=comments%></textarea><br /><br />
						<b style="margin-left:10px;">Anti spam-bot verification</b><br />
						<%
						if(instr(Request.ServerVariables("server_name"),"clarkbuilders")) then
						response.write recaptcha_challenge_writer("6LcsegEAAAAAAEQkOhF6ZkDnddAQ4B660MJNpBiL") 'clarkbuildersgroup.com
						elseif(instr(Request.ServerVariables("server_name"),"idam.clarkrealty")) then
						response.write recaptcha_challenge_writer("6LekfAEAAAAAAIeE2yoAlf2eg__igJhrQ7UWaKTj") 'idam.clarkrealty.com
						elseif(instr(Request.ServerVariables("server_name"),"clarkrealtycapital")) then
						response.write recaptcha_challenge_writer("6LfQJAIAAAAAAGyQ9amwb-DWTr0ijrCc09VLyidS") 'clarkrealtycapital.com
						end if
						%>
						<br /><br />
						<input type="hidden" name="submit" value="1" />
						<input type="image" src="images/send.jpg" border="0" alt="Submit Form">
					</form>
					<!--#include file="includes\footer.asp" -->
					</td></tr></table>
					</div>
			</div>
		</div>
	</div>
</body>
</html>