<!--#include file="includes\config.asp" -->
<%
	cid = prep_sql(request.querystring("cid"))
	title = ""
	title_image = ""
	type_description = ""
	if (cid = clng(DISC_PIO_IDE)) then 
	title = "Pioneering Ideas"
	title_image = "title_pioneering_ideas.jpg"
	type_description = "Our expertise is punctuated with a ""history of firsts"" that have set new standards, sparked industry trends, and opened new markets."
	elseif (cid = clng(DISC_IMP_LIV)) then
	title = "Improving Lives"
	title_image = "title_improving_lives.jpg"
	type_description = "From the creation of affordable housing where none previously existed to the large-scale improvements of inner city neighborhoods and military housing communities, Clark Realty Capital's developments have sparked positive change across the country."
	elseif (cid = clng(DISC_ADV_URB)) then
	title = "Advancing Urbanism"
	title_image = "title_advancing_urbanism.jpg"
	type_description = "At Clark Realty Capital, we see ourselves as modern-day pioneers.  We believe in the concepts of smart growth, transit-oriented design, and the revival of city living."
	elseif (cid = clng(DISC_BUI_COM)) then
	title = "Building Community"
	title_image = "title_building_community.jpg"
	type_description = "Nothing describes Clark Realty Capital more literally or metaphorically than a company that builds community.  Our people take great pride in engaging and unifying stakeholders to create the best developments in which to live, work, shop, and play."
	elseif (cid = clng(DISC_CHA_HOR)) then
	title = "Changing Horizons"
	title_image = "title_changing_horizons.jpg"
	type_description = "The ability to impart a lasting impression on cities across the nation is what drives many of the developers at Clark Realty Capital.  With the responsibility of creating an enduring legacy, we approach each signature building with an uncompromising commitment to iconic architecture and lasting design."
	elseif (cid = clng(DISC_DEV_RES)) then
	title = "Developing Responsibility"
	title_image = "title_developing_responsibility.jpg"
	type_description = "Every project that bears our signature is a result of an unwavering commitment to responsible development.  Whether preserving historical resources, respecting the cultural heritage of surrounding areas, or employing advanced methods of sustainable design, Clark Realty Capital relies on creativity and ingenuity to create developments of enduring quality."	
	end if

	set rsProjectsByCat=server.createobject("adodb.recordset")
	sql="select top 6 * from ipm_project, ipm_project_discipline,ipm_project_field_desc c,ipm_project_field_value d where ipm_project.projectid = d.projectid and c.item_id = d.item_id and c.item_tag = 'IDAM_PROJECT_CR_WEBSITE' and d.item_value = '1' and ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND keyid = " & cid & " order by newid()"
	rsProjectsByCat.Open sql, Conn, 1, 4
	
	set rsCaseStudy = server.createobject("adodb.recordset")
	sql = "select top 1 a.name pname, b.name aname, a.description description, a.city city, a.state_id state, a.projectid pid, b.asset_id aid from ipm_project_field_desc c, ipm_project_field_value d, ipm_project_discipline e, ipm_project a left join ipm_asset b on a.projectid = b.projectid and b.name = 'case_study' where challenge not like '' and favorite = 1 and a.available = 'y' and c.item_id = d.item_id and c.item_tag = 'IDAM_PROJECT_CR_WEBSITE' and d.projectid = a.projectid and d.item_value = '1' and e.projectid = a.projectid and e.keyid = " & cid & " and b.securitylevel_id = 3 and b.available = 'y' order by newid()"
	rsCaseStudy.Open sql, Conn, 1,4
	
	set rsTeam = server.createobject("adodb.recordset")
	sql = "select top 2 firstname,c.item_value,a.userid from ipm_user a,ipm_project_contact b, ipm_user_field_value c, ipm_user_field_value d where a.userid = b.userid and c.user_id = a.userid and c.item_id = 21767007 and d.item_id = 21767014 and a.userid = d.user_id and d.item_value = '1' and b.projectid = " & rsCaseStudy("pid")
	rsTeam.Open sql, Conn, 1,4	
	
%>

<html>
<head>
<title><%=sPageTitle%> - Portfolio</title>
	<!--#include file="includes\header.asp" -->
	<script type="text/javascript" src="js/swfobject.js"></script>
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" id="main_table">
			<tr>
				<td class="left_col">
					<div id="content_text">
					<small><a href="portfolio.asp">Portfolio</a> | <%=title%></small>
					<br /><br />
					<img src="images/<%=title_image%>" alt="<%=title%>" />
					<br /><br />
					<%=type_description%>
					
					<table  cellpadding="8" cellspacing="8" width="100%">
						<tr>

						<%
						z = 1
						do while not rsProjectsByCat.eof
						%>


							<td valign="top" width="50%">
								<table width="100%">
									<tr>	<td valign="top" align="left" width="50%"><div class="portfolio_bg"><a href="project.asp?pid=<%=rsProjectsByCat("projectID")%>"><img  width="137" height="108" class="portfolio_photo" src="<%=sAssetPath%><%=rsProjectsByCat("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=137&height=108&qfactor=<%=assetQFactor%>" alt="<%=trim(rsProjectsByCat("name"))%>"/></a></div></td>
										<td valign="top" align="left" width="50%"><b><%=trim(rsProjectsByCat("name"))%></b><br /><%=rsProjectsByCat("city")%>, <%=rsProjectsByCat("state_id")%></td>
									</tr>
								</table>								
							</td>
						
						<%	
						rsProjectsByCat.moveNext
						if (z mod 2 = 0) then response.write "</tr><tr>"
						z = z + 1
						loop						
						%>

						

						</tr>						
					</table>
					
					<br />
					
					<a href="portfolio_all.asp?cid=<%=cid%>">See All &gt;&gt;</a>
					
					
					</div>
					
					
					
				</td>
				
				<td class="right_col">
				<%if (rsCaseStudy.recordCount > 0) then %>
				<img src="images/portfolio_side.jpg" alt="Case Study" /><div id="flashcontent"><%if (isnull(rsCaseStudy("aname"))) then%><img width="375" height="295" src="<%=sAssetPath%><%=rsCaseStudy("pid")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=375&height=295&qfactor=<%=assetQFactor%>" alt="<%=trim(rsCaseStudy("pname"))%>"/></a><%end if%></div>
					<%if (trim(rsCaseStudy("aname")) = "case_study") then%>
					<script type="text/javascript">
						// <![CDATA[
						var so = new SWFObject("flvplayer9_crc.swf?video=getsmil.asp?id=<%=rsCaseStudy("aid")%>&skinswf=SkinUnderPlaySeekMute.swf", "sotester", "375", "323", "9", "#FFFFFF");
						so.useExpressInstall('expressinstall.swf');
						so.addParam("wmode","transparent");
						so.write("flashcontent");
						// ]]>
					</script>
					<%
					end if%>
				<br />
				<div id="portfolio_text">
				<b><big><%=rsCaseStudy("pname")%></big></b><br />
				<%=rsCaseStudy("city") & ", " & rsCaseStudy("state")%><br />
				<br />
				<%=rsCaseStudy("description")%><br />
				<a href="project.asp?pid=<%=rsCaseStudy("pid")%>">More &gt;&gt;</a>
				<br /><br />
				<big><b class="subheader">MEET THE TEAM</b></big>
				<br />
				
				<%if not rsTeam.eof then%>				
				<table width="100%">
					<tr>
						<td width="50%" valign="top" align="left">
						<img src="<%=sAssetPath%><%=rsTeam("userID")%>&Instance=<%=siDAMInstance%>&type=contact&size=1&width=150&height=110&qfactor=<%=assetQFactor%>" alt="<%=rsTeam("FirstName")%>" /><br />
						<br />
						<big><b><%=rsTeam("firstname")%></b></big><br />
						<%=rsTeam("item_value")%>
						<br /><br />
						<a href="team_profile.asp?id=<%=rsTeam("userID")%>">Profile &gt;&gt;</a>
						</td>
						<td width="50%" valign="top" align="left">
						<%
						
						rsTeam.movenext
						if not rsTeam.eof then
						%>
						<img src="<%=sAssetPath%><%=rsTeam("userID")%>&Instance=<%=siDAMInstance%>&type=contact&size=1&width=150&height=110&qfactor=<%=assetQFactor%>" alt="<%=rsTeam("FirstName")%>" /><br />
						<br />
						<big><b><%=rsTeam("firstname")%></b></big><br />
						<%=rsTeam("item_value")%>
						<br /><br />
						
						<a href="team_profile.asp?id=<%=rsTeam("userID")%>">Profile &gt;&gt;</a>
						<%end if%>						
						</td>
					</tr>
				</table>
				<%end if%>
				</div>
				<%end if%>
				</td>
			</tr>
			<tr>
				<td class="left_col">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col">					
				</td>
			</tr>			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-2");
pageTracker._trackPageview();
</script></body>
</html>
