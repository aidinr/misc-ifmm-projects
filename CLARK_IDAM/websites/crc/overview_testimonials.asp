	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Testimonials</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%"  id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small>Overview | Clients</small>
					<br /><br />
					<img src="images/clients.jpg" alt="overview" />
					<br /><br />
					As a testament to our company-wide commitment to delivering high-quality projects on-time and on-budget, Clark has earned a large number of third-party contracts from repeat customers representing both national firms such as Archstone Communities, and regional developers such as Kettler and RST development. Our team works tirelessly to exceed each project�s distinct goals and fulfill each client�s unique service needs.
					<br /><br />
					Clark Builders Group is the nation's third largest multifamily builder and is consistently ranked among the top ten nationally. Clark�s projects have earned some of the most prestigious accolades in the industry. 
					<br /><br />
					<div id="testimonial_videos">
					
					</div>
					
					</div>
					
				</td>
				
				<td class="right_col_overview">

					
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
