	<!--#include file="includes\config.asp" -->
<%
	set rsRankings=server.createobject("adodb.recordset")
	sql = "select * FROM ipm_awards where type = 1 AND SHOW = 1 and pull_date > getdate() ORDER BY POST_DATE DESC"
	rsRankings.Open sql, Conn, 1, 4
	
	set rsRankingsUDF=server.createobject("adodb.recordset")

%>
<html>
<head>
<title><%=sPageTitle%> - Rankings</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="news.asp">News</a> | Rankings</small>
					<br /><br />
					<img src="images/rankings.jpg" alt="rankings" />
					<br /><br />
					The Clark Realty organization has been ranked in such publications as ENR and Builder magazine. 
					<br /><br />
					
					<table width="550" cellpadding="5" cellspacing="5">
					<%do while not rsRankings.eof

					if(rsRankingsUDF.state = 1) then
					rsRankingsUDF.close
					end if
					
					sql= "select * from ipm_awards_field_value where awards_id = " & rsRankings("awards_id")
						
					rsRankingsUDF.Open sql, Conn, 1, 4					
					
					sAward = ""
					sRecipient = ""
					sRecipientURL = ""
					sOrganization= ""
					sOrganizationURL = ""
					
					do while (rsRankingsUDF.EOF = false)
					
						'Reset the variables
						if (rsRankingsUDF("item_id") = "21764687") then
							sAward = rsRankingsUDF("item_value")
						elseif (rsRankingsUDF("item_id") = "21764690") then
							sRecipient = rsRankingsUDF("item_value")
						elseif (rsRankingsUDF("item_id") = "21764691") then
							sRecipientURL = rsRankingsUDF("item_value")
						elseif (rsRankingsUDF("item_id") = "21764688") then
							sOrganization = rsRankingsUDF("item_value")
						elseif (rsRankingsUDF("item_id") = "21764689") then
							sOrganizationURL = rsRankingsUDF("item_value")
						end if
						
					rsRankingsUDF.moveNext
					loop
					
					%>
					<tr>
					<td valign="top" align="center"><img width="109" height="105" src="<%=sAssetPath%><%=rsRankings("awards_id")%>&Instance=<%=siDAMInstance%>&type=awards&size=2" alt="<%=rsRankings("Headline")%>" /></td>
					<td valign="top"><b><%=rsRankings("Headline")%></b>
					<br />
					<%if not (sAward = "") then %>
					<b>Award: </b> <%=sAward%>
					<br />
					<%end if%>
					<%if not (rsRankings("PublicationTitle") = "") then %>
					<b>Type: </b> <%=rsRankings("PublicationTitle")%>
					<br />
					<%end if
					if not (rsRankings("content") = "") then %>
					<%=replace(rsRankings("content"),VbCrLf,"<br />")%>
					<%
					  end if
					%>
					<%
					if not (sOrganizationURL = "" or sOrganization = "") then%>
					<br />
					For more information, please visit the official site of <a target="_blank" href="<%=sOrganizationURL%>"><%=sOrganization%></a> 
					<%end if%>
					
					</td>
					</tr>					

					<%
					rsRankings.movenext
					loop					
					%>	
					
					</table>
 
					
					
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/rankings_side.jpg" alt="Clark Builders Group is consistently ranked among the top ten multifamily general contractors in the nation." />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
