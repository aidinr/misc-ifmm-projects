﻿	<!--#include file="includes\config.asp" -->
<%
	theID = prep_sql(request.querystring("id"))
	if(theID = "") then
		Response.Redirect "http://www.clarkrealty.com"
	end if

	
	set rsNews=server.createobject("adodb.recordset")
	sql = "select * FROM ipm_news where SHOW = 1 AND News_ID = " &theID
	rsNews.Open sql, Conn, 1, 4



Function HTMLDecode(sText)
HTMLDecode = sText
    Dim I
    sText = Replace(sText, "&quot;", Chr(34))
    sText = Replace(sText, "&lt;"  , Chr(60))
    sText = Replace(sText, "&gt;"  , Chr(62))
    sText = Replace(sText, "&amp;" , Chr(38))
    sText = Replace(sText, "&nbsp;", Chr(32))
sText = Replace(sText, "’", "'")
sText = Replace(sText, "ñ", "n")
sText = Replace(sText, "“", Chr(34))
sText = Replace(sText, "”", Chr(34))



    For I = 1 to 255
        sText = Replace(sText, "&#" & I & ";", Chr(I))
    Next
    HTMLDecode = sText
End Function

%>
<html>
<head>
<title><%=sPageTitle%> - Immediate Release</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="news.asp">News</a> | <a href="news_archives.asp">Press Releases</a> | Immediate Release</small>
					<br /><br />
					<!--<img src="images/for_immediate_release.jpg" alt="Immediate Release" />-->
					<br /><br />
					
					<table cellpadding="5" cellspacing="5" width="550">
					<tr>
						<td valign="top"><%=MonthName(DatePart("m",rsNews("post_date")))%> <%=" " & DatePart("d",rsNews("post_date"))%>, <%=DatePart("yyyy",rsNews("post_date"))%></td>
						<td  valign="top" align="right">For More Information:<br /><%=replace(rsnews("contact"),VBCrLf,"<br />")%></td>
					</tr>
						<tr>
							<td colspan="2">
							<b><%=HTMLDecode(rsNews("Headline"))%></b><br />
							<i><%=HTMLDecode(rsNews("PublicationTitle"))%></i><br />
							<br />	
							
							<div style="float:right;margin-left:25px;">
							<img width="286" height="136" src="<%=sAssetPath%><%=rsNews("news_ID")%>&Instance=<%=siDAMInstance%>&type=news&size=1&width=286&height=136&qfactor=<%=assetQFactor%>" alt="<%=rsNews("Headline")%>"/>
							</div>
							<div>
							<%=HTMLDecode(replace(rsNews("Content"),VbCrLf,"<br />"))%>
							</div>

							</td>
						</tr>
						<tr>
							<td colspan="2" align="right">
							<!--<a href="#" onclick="javascript:window.open('print_news.asp?id=<%=theID%>','print_news','status=0,menubar=0,location=0',false);"><img src="images/print_page.jpg" alt="Print news" /></a>--></td>
						</tr>

					</table>
					
					
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/news_side_bg.gif" alt="Media Relations - All media inquiries should be directed to Joy Lutes at 703.362.0150 or 1.877.846.7760" />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
