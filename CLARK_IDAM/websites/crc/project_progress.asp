	<!--#include file="includes\config.asp" -->
<%
	projectID = prep_sql(request.querystring("pid"))
	set rsProject=server.createobject("adodb.recordset")
	sql="select * from ipm_project a,ipm_project_field_desc b,ipm_project_field_value c where a.projectid = c.projectid and b.item_id = c.item_id and b.item_tag = 'IDAM_PROJECT_CR_WEBSITE' and c.item_value = '1' and AVAILABLE = 'Y' AND PUBLISH = 1 AND a.projectid = " & projectID
	rsProject.Open sql, Conn, 1, 4
	
	if (rsProject.recordcount = "0") then
		Response.Redirect "http://www.clarkrealty.com"
	end if	
	if not (rsProject("publish") = "1") then
		Response.Redirect "http://www.clarkrealty.com"
	end if

	set rsNews=server.createobject("adodb.recordset")
	sql = "select TOP 3 * FROM ipm_news, ipm_news_related_projects where ipm_news.type = 0 AND ipm_news.news_id = ipm_news_related_projects.news_id AND post_date < getdate() AND pull_date > getdate() AND SHOW = 1 AND ipm_news_related_projects.projectid = " & projectID & " ORDER BY POST_DATE DESC"
	rsNews.Open sql, Conn, 1, 4
	
	set rsAwards=server.createobject("adodb.recordset")
	sql = "select TOP 3 * FROM ipm_awards, ipm_awards_related_projects where ipm_awards.type = 0 AND ipm_awards.awards_id = ipm_awards_related_projects.awards_id AND SHOW = 1 AND ipm_awards_related_projects.projectid = " & projectID & " ORDER BY POST_DATE DESC"
	rsAwards.Open sql, Conn, 1, 4
	
	set rsArchitect=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & projectID  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_ARCHITECT'"
	rsArchitect.Open sql, Conn, 1, 4
	
	set rsCompletion=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & projectID  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_COMPLETION'"
	rsCompletion.Open sql, Conn, 1, 4
	
	set rsDiscipline = server.createobject("adodb.recordset")
	sql="select * from ipm_project, ipm_project_discipline,ipm_discipline where ipm_project.projectid = ipm_project_discipline.projectid AND ipm_discipline.keyid = ipm_project_discipline.keyid AND ipm_project.projectid = " & projectID & " order by keyName"
	rsDiscipline.Open sql, Conn, 1, 4

	set rsProgress =  server.createobject("adodb.recordset")
	'sql = "select * from ipm_asset a,ipm_asset_field_value b where a.asset_id = b.asset_id AND b.item_id = 21764789 AND b.item_value = 1 AND a.available = 'Y'  AND a.securitylevel_id = 3 AND a.projectid = " & projectID & " ORDER BY creation_date desc"
	'sql = "select isnull(c.Item_Value,'01/01/1900' ) adate ,a.* from ipm_asset a left outer join ipm_asset_field_value b on a.asset_id = b.asset_id left outer join ipm_asset_field_value c on a.asset_id = c.asset_id AND c.item_id = 21763586 where b.item_id = 21764789 and b.item_value = 1 AND a.available = 'Y'  AND a.securitylevel_id = 3 AND a.projectid = " & projectID  & " ORDER BY CAST(isnull(c.Item_Value,'01/01/1900' ) as date) desc"
	sql = "select isnull(isnull(convert(varchar,cast(d.item_value as dateTIME),101),c.Item_Value),'01/01/1900' )  adate,a.* from ipm_asset a left outer join ipm_asset_field_value b on a.asset_id = b.asset_id left outer join ipm_asset_field_value c on a.asset_id = c.asset_id AND c.item_id = 21763586 left outer join ipm_asset_field_value d on a.Asset_ID = d.ASSET_ID and d.Item_ID = 21763227 where b.item_id = 21764789 and b.item_value = 1 AND a.available = 'Y'  AND a.securitylevel_id = 3 AND a.projectid = " & projectID & " ORDER BY CAST(isnull(isnull(convert(varchar,cast(d.item_value as dateTIME),101),c.Item_Value),'01/01/1900' ) as dateTIME)  desc"
	
	rsProgress.Open sql, Conn, 1,4

%>
<html>
<head>
<title><%=sPageTitle%> - Project</title>
	<!--#include file="includes\header.asp" -->
<link rel="stylesheet" href="css/jquery.lightbox.packed.css" type="text/css" media="screen" />	
<script type="text/javascript" src="js/jquery.lightbox.packed.js"></script>
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
						<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr>
				<td class="left_col_project">
					<div id="content_text">
					<table width="550" align="center">
					<tr>
					<%
					i = 0
					do while not rsProgress.eof
					if(rsProgress("description") <> "") then
					rsProgress("description") = "- " & rsProgress("description")
					end if
					
					
					if rsProgress("adate") = "01/01/1900" then
						sdate = "N/A"
					else
						sdate = FormatDateTime(rsProgress("adate"), 2)					
					end if
				
					%>
					<td align="center"><a rel="lightbox-progress" title="<%=rsProject("name")%> <%=rsProgress("description")%>" href="<%=sAssetPath%><%=rsProgress("asset_ID")%>&Instance=<%=siDAMInstance%>&type=asset&size=1&width=1024&height=768&qfactor=<%=assetQFactor%>"><img src="<%=sAssetPath%><%=rsProgress("asset_ID")%>&Instance=<%=siDAMInstance%>&type=asset&size=1&width=150&height=118&qfactor=<%=assetQFactor%>" /></a><br /><%= sdate %></td>
					<%

					
					rsProgress.moveNext
					i = i + 1
					if (i mod 3 = 0) then
					response.write "</tr><tr>"
					end if
					loop
					
					%>
					
					</tr>
					</table>
					</div>
					
					
				</td>
				
<td class="right_col_project">
				
					<div style="margin-left:10px;padding:10px;">
					<small><a href="portfolio.asp">Portfolio</a> | <% do while not rsDiscipline.eof
		response.write "<a href=""portfolio_cat.asp?cid=" & rsDiscipline("keyID") & """>" & rsDiscipline("keyName") & "</a> "
		rsDiscipline.moveNext
	loop %> | <a href="project.asp?pid=<%=rsProject("projectID")%>"><%=rsProject("name")%></a> | Progress Photos</small>
					<br /><br />
					<b><big><%=rsProject("name")%></big><br />
					<%=rsProject("description")%></b><br /><br />
					<%=rsProject("descriptionmedium")%>
					<br /><br />
					<b>PROJECT DATA</b><br />
					Address: <%=rsProject("address")%><br />
					<%=rsProject("city")%>, <%=rsProject("state_id")%> <%=" " & rsProject("zip")%><br />
					
					<%if (rsArchitect.recordCount > 0) then%><%if not(rsArchitect("item_value") = "") then %><b>Architect:</b> <%=rsArchitect("item_value")%><br /><%end if%><%end if%>	
					
					<%if (rsCompletion.recordCount > 0) then%><%if not(rsCompletion("item_value") = "") then %><b>Completion:</b> <%=MonthName(DatePart("m",rsCompletion("item_value")))%> <%=" " & DatePart("yyyy",rsCompletion("item_value"))%><br /><%end if%><%end if%>	
					<br />
					
					<%if (rsNews.recordCount > 0) then%><b>PRESS RELEASES</b><br /><%end if%>
					<%do while not rsNews.eof %>
						
						<a href="view_news.asp?id=<%=rsNews("news_id")%>"><%=rsNews("headline")%></a>

					<%
					rsNews.movenext
					loop
					%>
					<br /><br />
					<%if (rsAwards.recordCount > 0) then%><b>RECOGNITION</b><br /><%end if%>
					
					<%do while not rsAwards.eof %>
					
						<a href="awards.asp?aid=<%=rsAwards("awards_id")%>"><%=rsAwards("headline")%></a><br />

					<%
					rsAwards.movenext
					loop
					%>
					<br />
					<a href="awards.asp">See More&gt;&gt;</a>
					</div>
				</td>
			</tr>
						<tr>
				<td class="left_col_project">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_project">					
				</td>
			</tr>
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
