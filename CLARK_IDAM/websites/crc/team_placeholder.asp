	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Overview</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small>Team</small>
					<br /><br />
					<img src="images/meet_our_team.jpg" alt="Meet Our Team" />
					<br /><br />
					Clark Realty Capital is an eclectic mix of people working together toward shared goals. Our culture leverages a borderless organizational structure to champion the concept of a �free market� for ideas.  The intended result was to attract great people, unleash imagination, and have fun. As a result, our people have been innovators in established industries, created new products, and successfully launched multiple new business lines.<br />  
					<br />
					Our firm seeks highly motivated and entrepreneurial individuals, undaunted by the responsibility that comes with the freedom to realize big ideas. To promote a diversity of thinking, we place a great deal of emphasis on finding and hiring the brightest candidates with eclectic backgrounds. A relentless pursuit of new ideas and the camaraderie that forms from vetting these ideas provide employees with the opportunity to learn and grow.<br /> 
					<br />
					Here�s what our people have to say:<br />
					<br />
					<i class="quote">�The biggest surprise about working at Clark is that you are constantly stretched to the very limit of your ability.  You don�t have limitations and you don�t really have a blueprint to follow � that�s also the challenge because you don�t wake up in the morning necessarily knowing what to do.�  </i> &nbsp; <b>Bereket S.</b><br />  
					<br />
					<i class="quote">�It�s really exciting for me to work at Clark because it�s not a top down approach of imposing somebody�s ideas on you.  Its very bottom up, ideas are able to percolate up to the top - in that way, it�s very much a meritocracy.�</i> &nbsp; <b> Tom L.</b><br />
					<br />
					<i class="quote">�Our open office environment � what we call �the bullpen� � is a main source of creativity and fun in this organization.  We hear each other�s conversations all the time and jump in on discussions.  We�ve developed a camaraderie that has allowed us to build up a level of trust that I don�t think would have occurred if we hadn�t been in this informal, fun environment.�</i>  &nbsp; <b>Tracey T.</b><br />  
					 <br />
					<i class="quote">�One of the most satisfying things for me about the company culture and working for Clark Realty is the amount of responsibility that is provided to employees from a relatively early time in their tenure with the company.  I am directing a $580 million project across the country from our headquarters and the company really places a lot of trust and responsibility in me to direct the project.�</i> &nbsp;  <b>Fran C.</b>
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/team_side.jpg" alt="A privately held real estate company, CRC actively invests in, develops, finances, and manages award-winning real estate assets across the nation." />
				</td>

			</tr>
			<tr>
				<td class="left_col_overview">
					<!--#include file="includes\footer.asp" -->
					
				</td>
				
				<td class="right_col_overview">
				
					
				</td>

			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
