	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Project Types</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="overview.asp">Overview</a> | Project Types</small>
					<br /><br />
					<img src="images/project_types.jpg" alt="Project Types" />
					<br /><br />
					<img src="images/mixed_use_community.jpg" alt="mixed use"/><br /> 
					Clark is on the cutting-edge of constructing mixed-use and transit-oriented developments in major metropolitan areas. We recognize the rising demand for memorable places where residents can live, work, shop, and play, and we offer the capabilities � all in one shop � to deliver all of the components of an award-winning, vibrant mixed-use community.  <a href="portfolio_cat.asp?cid=<%=DISC_MIX_USE%>">See Projects &gt;&gt;</a>
					<br /><br />
					<img src="images/luxury_apartments.jpg" alt="luxury apartments"/> <br />
					Clark Builders Group provides residents with some of the finest homes in the nation. With a focus on innovative design, high-end finishes, and modern conveniences, Clark delivers projects of the highest caliber, earning some of the most prestigious local, national, and international awards from organizations like the National Association of Homebuilders and the Associated General Contractors of America.  <a href="portfolio_cat.asp?cid=<%=DISC_LUX_APT%>"> See Projects &gt;&gt;</a> 
					<br /><br />
					<img src="images/military_housing.jpg" alt="military housing"/> <br />
					Clark Builders Group has garnered dozens of awards for the quality housing we have built for members of the military.  Because we have experience tackling complex construction issues related to environmental preservation, historic restoration, and anti-terrorism/force protection, Clark is consistently selected as the builder of choice on these large-scale, multi-phase public private ventures.    <a href="portfolio_cat.asp?cid=<%=DISC_MIL_COM%>">See Projects &gt;&gt;</a> 
					<br /><br />
					<img src="images/senior_living.jpg" alt="senior living"/> <br />
					Clark Builders Group constructs award-winning senior living facilities that meet the changing needs of today�s active adults. Our portfolio includes assisted- and independent-living communities; upscale, mid-rise senior housing; and life care condominium towers with on-site skilled nursing facilities.   <a href="portfolio_cat.asp?cid=<%=DISC_SEN_USE%>">See Projects &gt;&gt;</a>
					<br /><br />
					<img src="images/high_density_apartments.jpg" alt="high density apartments"/> <br />
					Clark Builders Group has wide-ranging experience in the high-density marketplace and understands the complex needs of these compact projects. Our team has worked closely with local government agencies, business owners, and neighboring residents to construct attractive urban communities in tight settings with little staging area.    <a href="portfolio_cat.asp?cid=<%=DISC_HIG_DEN%>">See Projects &gt;&gt;</a> 
					
					
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/project_types_side.jpg" alt="subcontracting" />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>			
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
