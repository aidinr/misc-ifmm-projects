	<!--#include file="includes\config.asp" -->
<%
	'begin test current year for news item
	sql = "select * FROM ipm_news where type = 0 and post_date < '" & year(date) + 1 & "-01-01 00:00:00.000' AND post_date > '" & year(date) - 1 & "-12-31 23:59:59.999' and pull_date > getDate() AND SHOW = 1 ORDER BY POST_DATE DESC"
	set rsNewsTest=server.createobject("adodb.recordset")
	rsNewsTest.Open sql, Conn, 1, 4	
	if(rsNewsTest.RecordCount = 0) then
	baseYear = year(date) - 1
	else
	baseYear = year(date)
	end if
	'end test current year for news item

	
	theYear = request.querystring("year")
	if(theYear = "") then
		theYear = baseYear
	end if

	
	if not (theYear = "archived") then
	theYear = Cint(theYear)
	prevYear =  theYear - 1
	nextYear =  theYear + 1
	sql = "select * FROM ipm_news where type = 0 and post_date < '" & nextYear & "-01-01 00:00:00.000' AND post_date > '" & prevYear & "-12-31 23:59:59.999' and pull_date > getDate() AND SHOW = 1  ORDER BY POST_DATE DESC"
	else
	archivedYears = baseYear - 2
	sql = "select * FROM ipm_news where type = 0 and post_date < '" & archivedYears & "-01-01 00:00:00.000' and pull_date > getDate() AND SHOW = 1 ORDER BY POST_DATE DESC"
	end if
	
	set rsNews=server.createobject("adodb.recordset")
	
	rsNews.Open sql, Conn, 1, 4

%>
<html>
<head>
<title><%=sPageTitle%> - Press Releases</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="news.asp">News</a> | Press Releases</small>
					<br /><br />
					<img src="images/press_releases.jpg" alt="Press Releases" />
					<br /><br />
					
					<%
					i = 0
					do while i < 3
					if(baseYear - i = theYear) then %>
					<b><%=baseYear - i%></b> | 
					 
					<%
					else %>
					<a href="news_archives.asp?year=<%=baseYear - i%>"><%=baseYear - i%></a> |
					<%
					end if
					i = i + 1
					loop
					
					if("archived" = theYear) then %>
					<b>Archives</b>
					<%else %>
					<a href="news_archives.asp?year=archived">Archives</a>
					<%
					end if
					%>
					
					
					
					<table cellpadding="5" cellspacing="5" width="550">
					<%
					do while not rsNews.eof %>
					
						<tr>
							<td valign="top" width="30%"><b><a href="view_news.asp?id=<%=rsNews("news_id")%>"><%=MonthName(DatePart("m",rsNews("post_date")))%> <%=" " & DatePart("d",rsNews("post_date"))%>, <%=DatePart("yyyy",rsNews("post_date"))%></a></b></td>
							<td valign="top" width="70%">
								
								
								<a href="view_news.asp?id=<%=rsNews("news_id")%>"><%=rsNews("headline")%></a>
								
							</td>
					<%
					rsNews.movenext
					loop
					%>
					</table>
					
					
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/news_side_bg.gif" alt="Media Relations - All media inquiries should be directed to Joy Lutes at 703.362.0150 or 1.877.846.7760" />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
