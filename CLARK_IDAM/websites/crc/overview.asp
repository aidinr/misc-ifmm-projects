	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Overview</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small>Overview</small>
					<br /><br />
					<img src="images/overview.jpg" alt="overview" />
					<br /><br />
					Established in 1992, Clark Realty Capital leverages the same entrepreneurial spirit and culture fostered more than a decade ago to encourage creative solutions to complex real estate challenges.  Our firm has substantial transactional and operational experience in acquisition, development, financing, and asset management with a diverse portfolio of residential, retail, mixed-use, commercial, and land developments.<br />
					<br />
					Clark Realty Capital�s development portfolio spans more than 60 projects, 38,000 residential units, 10,000 acres of land, and 50 million SF of development, comprising $7 billion of public and private work developed or under development.<br /> 
					<br />
					<b class="subheader">MARKET INNOVATION</b><br />
					Clark Realty Capital is a trendsetter in the real estate market and has a track record of industry �firsts� that demonstrate an ability to identify new opportunities and implement a creative vision. The results have generated superior financial performance, awards, and a reputation for innovation.<br />  
					<br />
					<b class="subheader">CULTURE OF ENTREPRENEURSHIP</b><br />
					An entrepreneurial spirit, outstanding work environment, and the relentless pursuit of new ideas serve as a foundation for each of our endeavors. Our mission is to empower talented people to realize new opportunities and lead our industry in creating value for businesses, communities, and entrepreneurs.<br />  
					<br />
					<b class="subheader">LASTING POSITIVE IMPACTS</b><br />
					At Clark Realty Capital, great ideas are given the freedom and resources to become reality. Working as a team, we have launched numerous successful ventures that have forged new paths in inner city development, created vibrant communities for our military families, and improved lives by developing affordable and workforce housing.
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/overview_side.jpg" alt="A privately held real estate company, CRC actively invests in, develops, finances, and manages award-winning real estate assets across the nation." />
				</td>

			</tr>
			<tr>
				<td class="left_col_overview">
					<!--#include file="includes\footer.asp" -->
					
				</td>
				
				<td class="right_col_overview">
				
					
				</td>

			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-2");
pageTracker._trackPageview();
</script></body>
</html>
