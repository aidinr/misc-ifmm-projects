	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Offices</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="overview.asp">Overview</a> | Offices</small>
					<br /><br />
					<img src="images/offices.jpg" alt="offices" />
					<br /><br />
					<table width="550">
					
					<tr>
						<td valign="top" width="50%">
						<b class="subheader">NORTHEAST</b><br />
						<b>Corporate Headquarters - Arlington Office</b><br />
						4401 Wilson Boulevard, Suite 600<br />
						Arlington, VA 22203<br />
						Phone: 703.294.4500<br />
						Fax: 703.294.4650<br />
						<a href="http://maps.google.com/maps?saddr=&daddr=4401 Wilson Blvd Arlington VA 22203">Get Directions &gt;&gt;</a><br />
						 <br />
						<b>Fort Belvoir Office</b><br />
						5201 Patrick Road<br />
						Fort Belvoir, VA 22060<br />
						Phone: 703.781.2000<br />
						Fax: 703.781.2051<br />
						<a href="http://maps.google.com/maps?saddr=&daddr=5201 Patrick Road Fort Belvoir VA 22060">Get Directions &gt;&gt;</a><br />
						 <br />
						<b>Andrews AFB Office</b><br />
						2141 Atlanta Circle<br />
						Andrews AFB, MD 20762<br />
						Phone: 301.877.2940<br />
						Fax: 301.877.2971<br />
						<a href="http://maps.google.com/maps?saddr=&daddr=2141 Atlanta Circle Andrews AFB MD 20762">Get Directions &gt;&gt;</a><br />
						 <br />
						<b class="subheader">NORTHWEST</b><br />
						<b>Monterey Office</b><br />
						4291 Normandy Road<br />
						Seaside, CA 93955<br />
						Phone:  831.583.2710<br />
						Fax:  831.583.2760<br />
						<a href="http://maps.google.com/maps?saddr=&daddr=548 Abrego Street Monterey CA 93940">Get Directions &gt;&gt;</a><br />
						<br />
						<b>Fort Irwin Office</b><br />
						9005 Barstow Road<br />
						Fort Irwin, CA 92310<br />
						Phone: 760.383.4901<br />
						Fax: 760.383.4951<br />
						<a href="http://maps.google.com/maps?saddr=&daddr=9005 Barstow Road Fort Irwin CA 92310">Get Directions &gt;&gt;</a><br />
						</td>
						<td valign="top" width="50%">
						<b class="subheader">SOUTHEAST</b><br />
						<b>Columbus Office</b><br />
						Building 280 Transportation Street<br />
						Fort Benning, GA 31905<br />
						Phone: 706.683.4201<br />
						Fax: 706.683.4253<br />
						<a href="http://maps.google.com/maps?saddr=&daddr=Building 280 Transportation Street Fort Benning GA 31905">Get Directions &gt;&gt;</a><br />
						<br />
						<b>Tampa Office</b><br />
						8431 Bayshore Boulevard<br />
						Tampa, FL 33621<br />
						Phone: 813.840.1700<br />
						Fax: 813.840.1730<br />
						<a href="http://maps.google.com/maps?saddr=&daddr=8431 Bayshore Blvd Tampa FL 33621">Get Directions &gt;&gt;</a><br />
						<br />
						<b>Albany Office</b><br />
						1210 Spencer Road<br />
						Albany, GA 31705<br />
						Phone: no phone or fax numbers<br />
						Fax: no phone or fax numbers<br />
						<a href="http://maps.google.com/maps?saddr=&daddr=1210 Spencer Road Albany GA 31705">Get Directions &gt;&gt;</a><br />
						<br />
						<b class="subheader">SOUTHWEST</b><br />
						<b>San Diego Office</b><br />
						3655 Nobel Drive, Suite 500<br />
						San Diego, CA 92122<br />
						Phone: 858.320.3900<br />
						Fax: 858.320.3950<br />
						<a href="http://maps.google.com/maps?saddr=&daddr=3655 Nobel Drive San Diego CA 92122">Get Directions &gt;&gt;</a><br />
						</td>
					</tr>
					</table>
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/offices_side.jpg" alt="A privately held real estate company, CRC actively invests in, develops, finances, and manages award-winning real estate assets across the nation." />
				</td>

			</tr>
			<tr>
				<td class="left_col_overview">
					<!--#include file="includes\footer.asp" -->
					
				</td>
				
				<td class="right_col_overview">
				
					
				</td>

			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-2");
pageTracker._trackPageview();
</script></body>
</html>
