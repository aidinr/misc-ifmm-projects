	<!--#include file="includes\config.asp" -->
<%


	set rsFeaturedTeam=server.createobject("adodb.recordset")
	sql="select top 300 userid,firstname,lastname,d.item_value office, position  from ipm_user a,ipm_user_field_value b, ipm_user_field_value c, ipm_user_field_value d  where b.item_id = 21767016 and b.user_id = a.userid  and a.userid <> 21766993 and b.item_value = 1 and c.item_id = 21767014 and c.user_id = a.userid and c.item_value = 1 and  d.user_id = a.userid and d.item_id = 21767007 order by newid()"
	rsFeaturedTeam.Open sql, Conn, 1, 4
	
	featuredString = ""
	

%>	
	
	

<html>
<head>
<title><%=sPageTitle%> - Team</title>
	<!--#include file="includes\header.asp" -->
	<script type="text/javascript" src="js/swfobject.js"></script>
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small>Culture</small>
					<br /><br />
					<img src="images/our_people.jpg" alt="Our People" />
					<br /><br />
					Clark Realty Capital is an eclectic mix of people working together toward shared goals. Our culture leverages a borderless organizational structure to champion the concept of a "free market" for ideas.  The intended result was to attract great people, unleash imagination, and have fun. As a result, our people have been innovators in established industries, created new products, and successfully launched multiple new business lines.<br />
					<br />
					<b>Check out what some of our employees have to say about working at CRC. </b>
					<br />
					
					<table width="550">
					<tr>
						<td valign="top" width="60%">
							<table cellspacing="5" cellpadding="5" width="100%">
							<%
							featuredString = featuredString & rsFeaturedTeam("userid")
							do while not rsFeaturedTeam.eof%>
								<tr>
									<td width="150" valign="top"><img src="<%=sAssetPath%><%=rsFeaturedTeam("userID")%>&Instance=<%=siDAMInstance%>&type=contact&size=1&width=150&height=120&qfactor=<%=assetQFactor%>" alt="<%=rsFeaturedTeam("FirstName")%>" /></td>
									<td valign="top"><b><big><%=rsFeaturedTeam("FirstName")%></big></b><br /><b><%=rsFeaturedTeam("office")%></b><br /><br /><a href="team_profile.asp?id=<%=rsFeaturedTeam("userID")%>">View Profile &gt;&gt;</a></td>
								</tr>
							<%
							rsFeaturedTeam.moveNext
							if not(rsFeaturedTeam.eof) then
							featuredString =  featuredString &  ","  & rsFeaturedTeam("userid")
							end if
							loop%>
							</table>
						</td>
						<td valign="top" width="40%">
							<div style="line-height:24px;">
							<%
								set rsTeam=server.createobject("adodb.recordset")
								sql="select userid,firstname,lastname,d.item_value office, position from ipm_user a, ipm_user_field_value c, ipm_user_field_value d where a.userid <> 21766993 AND c.item_id = 21767014 and c.user_id = a.userid and c.item_value = 1 and d.user_id = a.userid and d.item_id = 21767007 and active = 'Y' and userid not in (" & featuredString & ") order by firstname"
								rsTeam.Open sql, Conn, 1, 4	
							%>
							<%do while not rsTeam.eof%>
							<b><%=rsTeam("FirstName")%>, <%=rsTeam("office")%></b> &nbsp; <a href="team_profile.asp?id=<%=rsTeam("userID")%>">Profile &gt;&gt;</a>
							<br />
							<%
							rsTeam.moveNext
							loop%>
							</div>
						</td>
					</tr>
					</table>
					
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/team_side.jpg" alt="A privately held real estate company, CRC actively invests in, develops, finances, and manages award-winning real estate assets across the nation." /><div id="flashcontent"></div>
					<script type="text/javascript">
						// <![CDATA[
						
						var so = new SWFObject("flvplayer9_crc.swf?video=getsmil_team.asp&skinswf=SkinUnderPlaySeekMute.swf", "sotester", "375", "323", "9", "#FFFFFF");
						so.useExpressInstall('expressinstall.swf');
						so.addParam("wmode","transparent");
						so.write("flashcontent");
						// ]]>
					</script>					
					
				</td>

			</tr>
			<tr>
				<td class="left_col_overview">
					<!--#include file="includes\footer.asp" -->
					
				</td>
				
				<td class="right_col_overview">
				
					
				</td>

			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
