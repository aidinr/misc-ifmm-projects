	<!--#include file="includes\config.asp" -->
<html>
<head>
<title><%=sPageTitle%> - Pacific Beacon Ribbon-Cutting</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small> </small>
					<br /><br />



<font face="Century Gothic" color="#703412" size="5">PACIFIC BEACON RIBBON-CUTTING</font><br>
<br />
<br />


All photography is copyright of Clark Realty Capital, LLC. Please credit photographer as listed in filename. For more information, please contact Joy Lutes at 877.846.7760 or 703.362.0150.
<br />
									<p><font face="Century Gothic" color="#703412" size="3">PROJECT PHOTOGRAPHY</font><br>
										<br />
									</p>
									<table width="550">
					<tr>
					<td valign="top" style="padding-top:1px;">


<p>

<ul>
	<li>
	<a href="images/Amy Fellows 2.jpg" target="_blank">Amy Fellows - Lobby</a>
	</li>

	<li>
	<a href="images/Amy Fellows Photography.jpg" target="_blank">Amy Fellows - Sky Terrace Pool at Night</a>
	</li>
	
	<li>
	<a href="images/David Hebble.jpg" target="_blank">David Hebble - Sky Terrace Pool</a>
	</li>

	<li>
	<a href="images/David Hebble 1.jpg" target="_blank">David Hebble - Courtyard Lounge</a>
	</li>
	
	<li>
	<a href="images/David Hebble 2.jpg" target="_blank">David Hebble - Beacon Room</a>
	</li>
	
	<li>
	<a href="images/David Hebble 3.jpg" target="_blank">David Hebble - Spectacular Views</a>
	</li>
	
	<li>
	<a href="images/David Hebble 4.jpg" target="_blank">David Hebble - WiFi Cafe</a>
	</li>
	
	<li>
	<a href="images/David Hebble 5.jpg" target="_blank">David Hebble - Aerial View Retzer Building</a>
	</li>
	
	<li>
	<a href="images/David Hebble 6.jpg" target="_blank">David Hebble - Model Unit Master Suite</a>
	</li>
	
	<li>
	<a href="images/David Hebble 7.jpg" target="_blank">David Hebble - Model Unit Living Room</a>
	</li>
	
	<li>
	<a href="images/David Hebble 8.jpg" target="_blank">David Hebble - Three Towers</a>
	</li>		
</ul>
<br>
<br>

<font face="Century Gothic" color="#703412" size="3">EVENT PHOTOGRAPHY</font><br>
<ul>
	<li>
	<a href="images/Michael Svoboda Photography_Bryan Lamb.jpg" target="_blank">Michael Svoboda Photography - Bryan Lamb</a>
	</li>

	<li>
	<a href="images/Michael Svoboda Photography_Dietz Building and SEAL Recruits.jpg" target="_blank">Michael Svoboda Photography - Dietz Building and SEAL Recruits</a>
	</li>	

	<li>
	<a href="images/Michael Svoboda Photography_Hering Greets Walsh.jpg" target="_blank">Michael Svoboda Photography - Hering Greets Walsh</a>
	</li>	

	<li>
	<a href="images/Michael Svoboda Photography_Ribboncutting on Stage.jpg" target="_blank">Michael Svoboda Photography - Ribbon-Cutting on Stage</a>
	</li>	
	<br>(L to R) Jim Forburger (Clark), Admiral Edward G. Winters III, Admiral Leendert R. "Len" Hering, Admiral Patrick M. Walsh, Petty Officer First Class Job Gibson, Bryan Lamb (Clark).</br>
	<li>
	<a href="images/Michael Svoboda Photography_RibbonCutting.jpg" target="_blank">Michael Svoboda Photography - Ribbon-Cutting</a>
	</li>	
	<br>(L to R) Jim Forburger (Clark), Admiral Edward G. Winters III, Admiral Leendert R. "Len" Hering, Admiral Patrick M. Walsh, Petty Officer First Class Job Gibson, Bryan Lamb (Clark), Mr. Scott Forrest (Director, Special Venture Acquisition, NAVFAC).</br>
	<li>
	<a href="images/Michael Svoboda Photography_Walsh.jpg" target="_blank">Michael Svoboda Photography - Walsh</a>
	</li>

	<li>
	<a href="images/Michael Svoboda Photography_Wide Angle.jpg" target="_blank">Michael Svoboda Photography - Wide Angle</a>
	</li>
	
	<li>
	<a href="images/Michael Svoboda Photography_Winters Walsh Lamb.jpg" target="_blank">Michael Svoboda Photography - Winters Walsh Lamb</a>
	</li>	

</ul>

</p>
</td>
					</tr>
					</table>
								</div>
					
				</td>
				
				<td class="right_col_overview">
				<div>
				<img src="images/bg_contact.jpg" alt="CBG" />
				</div>
					
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-2");
pageTracker._trackPageview();
</script></body>
</html>
