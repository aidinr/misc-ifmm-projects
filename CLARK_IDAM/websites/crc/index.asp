	<!--#include file="includes/config.asp" -->	
<html>
<head>
<title><%=sPageTitle%> - Home</title>
	<!--#include file="includes/header.asp" -->
	<script type="text/javascript" src="js/swfobject.js"></script>
</head>
<body>
<form method="get" action="portfolio_search.asp">
	<div id="drop_shadow">
		<div id="container">
			
						<!--#include file="includes/page_header.asp" -->
						<!--#include file="includes/menu.asp" -->
			

	<div id="home_content">		
	<div id="flashcontent" style="border: 2px solid white;border-top:0;" >
	</div>
	<script type="text/javascript">
		// <![CDATA[
		
		var so = new SWFObject("CRC_Home_Static.swf", "sotester", "996", "547", "9", "#FFFFFF");
		so.useExpressInstall('expressinstall.swf');
		so.addParam("wmode","transparent");
		so.addParam("menu","false");
		so.write("flashcontent");
		// ]]>
	</script>
			
			<div id="home_bottom">				
				<table width="99%" cellpadding="3" cellspacing="3" align="center" border="0">
					<tr>
						<td width="63%" valign="bottom"><div id="content_footer">
					<a href="http://thebuzz.clarkrealty.com/index.cfm">EMPLOYEE LOGIN</a>  |  <a href="privacy_policy.asp">PRIVACY POLICY</a>  |  <a href="http://www.clarkcapitalventures.com">CLARK CAPITAL VENTURES</a><font color="#00529f"><sup>&reg;</sup></font> &nbsp;&nbsp;&nbsp; &copy; 2008  <div style="display:none;">SITE DESIGN BY <a href="http://www.ifmm.com">IFMM</a></div>

					
					</div></td>
						<td valign="bottom" width="37%"><table cellpadding="3" cellspading="3" border="0"><tr><td valign="bottom"><img src="images/disability.png" alt="Disability" /></td><td valign="bottom"><img src="images/equal_housing.png" alt="Equal Housing" /></td><td valign="bottom"><span id="home_footer">Clark Realty Capital is committed to the principles of fair housing, including providing construction that includes features of accessibility and adaptable design for people with disabilities, as required by state and federal law.</span></td></tr></table></td>					
					</tr>
				</table>
			</div>
			
			</div>
			
		</div>
		</div>
	</div>
</form>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-2");
pageTracker._trackPageview();
</script></body>
</html>
