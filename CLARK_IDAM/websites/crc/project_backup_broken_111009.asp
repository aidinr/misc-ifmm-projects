	<!--#include file="includes\config.asp" -->
<%
	projectID = prep_sql(request.querystring("pid"))
	set rsProject=server.createobject("adodb.recordset")
	sql="select * from ipm_project a,ipm_project_field_desc b,ipm_project_field_value c where a.projectid = c.projectid and b.item_id = c.item_id and b.item_tag = 'IDAM_PROJECT_CR_WEBSITE' and c.item_value = '1' and AVAILABLE = 'Y' AND PUBLISH = 1 AND a.projectid = " & projectID
	rsProject.Open sql, Conn, 1, 4
	
	if (rsProject.recordcount = "0") then
		Response.Redirect "http://www.clarkrealty.com"
	end if	
	if not (rsProject("publish") = "1") then
		Response.Redirect "http://www.clarkrealty.com"
	end if
	
	set rsAssets=server.createobject("adodb.recordset")
	sql = "select * from ipm_asset a,ipm_asset_field_value b where a.asset_id = b.asset_id AND b.item_id = 21764790 AND b.item_value = '1' AND a.available = 'Y' AND a.securitylevel_id = 3 AND a.projectid = " & projectID
	rsAssets.Open sql, Conn, 1, 4
	
	set rsRelated=server.createobject("adodb.recordset")
	sql = "select top 3 c.* from ipm_project a,ipm_project_related b,ipm_project c where b.ref_id = c.projectid and a.projectid = b.project_id AND c.AVAILABLE = 'Y' AND c.publish = 1  AND b.project_id = " & projectID
	rsRelated.Open sql, Conn, 1, 4
	
	set rsNews=server.createobject("adodb.recordset")
	sql = "select TOP 3 * FROM ipm_news, ipm_news_related_projects where ipm_news.type = 0 AND ipm_news.news_id = ipm_news_related_projects.news_id AND post_date < getdate() AND pull_date > getdate() AND SHOW = 1 AND ipm_news_related_projects.projectid = " & projectID & " ORDER BY POST_DATE DESC"
	rsNews.Open sql, Conn, 1, 4
f
	set rsAwards=server.createobject("adodb.recordset")
	sql = "select * FROM ipm_awards, ipm_awards_related_projects where ipm_awards.type = 0 AND ipm_awards.awards_id = ipm_awards_related_projects.awards_id AND SHOW = 1 AND ipm_awards_related_projects.projectid = " & projectID & " ORDER BY headline DESC"
	rsAwards.Open sql, Conn, 1, 4
	
	set rsArchitect=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & projectID  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_ARCHITECT'"
	rsArchitect.Open sql, Conn, 1, 4
	
	set rsCompletion=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & projectID  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_COMPLETION'"
	rsCompletion.Open sql, Conn, 1, 4
	
	discString = DISC_PIO_IDE & "," & DISC_IMP_LIV & "," & DISC_ADV_URB & "," & DISC_BUI_COM & "," & DISC_CHA_HOR & "," & DISC_DEV_RES
	
	set rsDiscipline = server.createobject("adodb.recordset")
	sql="select * from ipm_project, ipm_project_discipline,ipm_discipline where ipm_project.projectid = ipm_project_discipline.projectid AND ipm_discipline.keyid = ipm_project_discipline.keyid AND ipm_project_discipline.keyid IN(" & discString & ") AND ipm_project.projectid = " & projectID & " order by keyName"
	rsDiscipline.Open sql, Conn, 1, 4
	
	set rsNext = server.createobject("adodb.recordset")
	sql = "select top 1 * from ipm_project,ipm_project_field_value d,ipm_project_field_desc c where ipm_project.projectid > " & projectID & " AND publish = 1 AND available = 'Y' and c.item_tag = 'IDAM_PROJECT_CR_WEBSITE' and c.item_id = d.item_id and d.item_value = '1' and d.projectid = ipm_project.projectid order by ipm_project.projectID"
	rsNext.Open sql, Conn, 1, 4
	
	set rsFirst = server.createobject("adodb.recordset")
	sql = "select top 1 * from ipm_project,ipm_project_field_value d,ipm_project_field_desc c WHERE publish = 1 AND available = 'Y' and c.item_tag = 'IDAM_PROJECT_CR_WEBSITE' and c.item_id = d.item_id and d.item_value = '1' and d.projectid = ipm_project.projectid order by ipm_project.projectID"
	rsFirst.Open sql, Conn, 1, 4
	
	set rsProgress =  server.createobject("adodb.recordset")
	sql = "select * from ipm_asset a,ipm_asset_field_value b where a.asset_id = b.asset_id AND b.item_id = 21764789 AND b.item_value = 1 AND a.available = 'Y'  AND a.securitylevel_id = 3 AND a.projectid = " & projectID & " ORDER BY creation_date desc"
	rsProgress.Open sql, Conn, 1,4	
	
	set rsTestimonials =  server.createobject("adodb.recordset")
	sql = "select * from ipm_asset a,ipm_asset_category b where a.projectid = b.projectid AND b.name = 'Web Testimonials' AND a.category_id = b.category_id AND a.available = 'Y' and b.available = 'Y' AND a.securitylevel_id = 3 AND a.projectid = " & projectID & " ORDER BY creation_date desc"
	rsTestimonials.Open sql, Conn, 1,4
	

%>
<html>
<head>
<title><%=sPageTitle%> - Project</title>
	<!--#include file="includes\header.asp" -->
	<link rel="stylesheet" href="css/thickbox.css" type="text/css" media="screen" />
	<script src="js/thickbox.js" type="text/javascript"></script>	
<script type="text/javascript">

var imageGalleryArray = new Array();

<%
	dim z
	z = 1
	
	response.write "imageGalleryArray[" & 0 & "] = """ & sAssetPath & projectID & "&Instance=" & siDAMInstance & "&type=project&size=1&width=625&height=491&qfactor=" & assetQFactor & """;" & vbcrlf

	do while not rsAssets.eof
		response.write "imageGalleryArray[" & z & "] = """ & sAssetPath & rsAssets("asset_id") & "&Instance=" & siDAMInstance & "&type=asset&size=1&width=625&height=491&qfactor=" & assetQFactor & """;" & vbcrlf
		rsAssets.moveNext
		z = z + 1
	loop

%>
imageGallerySize = imageGalleryArray.length;

function switchImage(i) {
	$('#image_container').css("background","url('images/gallery_loading.gif') no-repeat center center");
	prevLink = document.getElementById("prev_arrow");
	nextLink = document.getElementById("next_arrow");
	imageText = document.getElementById("project_image_text");
	$("#project_image").fadeOut("slow",function() {loadImage(imageGalleryArray[i-1],i);});
}
function loadImage(src,i) {
	document.getElementById('project_image').src=src;
	document.getElementById('project_image').onload = function() {
	$("#project_image").fadeIn("slow");
	}
	nextImage = i + 1;
	prevImage = i - 1;
	if(i==1) {
		
		if(imageGallerySize > 1) {
		prevLink.href = "javascript:switchImage(" + imageGalleryArray.length + ")";
		nextLink.href = "javascript:switchImage(2)";
		}
		else {
		prevLink.href = "#";
		nextLink.href = "#";
		}
	}
	else if (i == imageGallerySize) {
		prevLink.href = "javascript:switchImage(" + prevImage + ")";
		nextLink.href = "javascript:switchImage(1)";
	}
	else {
		prevLink.href = "javascript:switchImage(" + prevImage +  ")";
		nextLink.href = "javascript:switchImage(" + nextImage +  ")";
	}
	imageText.innerHTML = "IMAGES " + i +  " / " + imageGallerySize;	
}

</script>

<link rel="stylesheet" href="css/jquery.lightbox.packed.css" type="text/css" media="screen" />	
<script type="text/javascript" src="js/jquery.lightbox.packed.js"></script>
</head>
<body>

	<div id="drop_shadow">
		<div id="container">
			
						<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr>
				<td class="left_col_project">
					
					<div id="image_container" style="width:625px;height:488px;padding:0;margin:0;"><img onload="switchImage(1);" width="625" height="491" src="images/spacer.gif" id="project_image" /></div>
					<div id="project_image_navigation">
					<%
					if (rsNext.RecordCount = 0) then
						nextID = rsFirst("projectID")
					else
						nextID = rsNext("projectID")
					end if
					%>
						<div class="project_image_navigation"><a href="project.asp?pid=<%=nextID%>"><img src="images/see_next_project.jpg" alt="See next project" height=28 width=176/></a></div>
						<div class="project_image_navigation" id="project_image_navigation_arrows_bg">
							<div id="project_image_navigation_arrows"><table align="right"><tr><td style="padding-top:5px;"><a href="#" id="prev_arrow"><img src="images/prev_project_arrow.png" alt="Previous Image" /></a></td><td valign="middle" id="project_image_text"> IMAGES 1 / <%=rsAssets.recordcount + 1%></td><td style="padding-top:5px;"> <a href="#" id="next_arrow"><img src="images/next_project_arrow.png" alt="Next Image" /></a></td></tr></table></div>
						</div>
					</div>
					<br />
					<div id="content_text">
					<table cellpadding="0" cellspacing="0" width="93%">
						<td align="left" valign="top" style="line-height:20px;">
					
					<%if (rsRelated.recordCount > 0) then%><b>RELATED PROJECTS</b><br /><%end if%>	
						<%
					
					do while not rsRelated.eof %>
						
						<a href="project.asp?pid=<%=rsRelated("projectID")%>"><%=rsRelated("Name")%></a><br />

					<%
					rsRelated.movenext
					loop
					%>						

						
						</td>
						<td align="right" valign="top">
						<%if(rsProgress.recordCount > 0) then %>
						<a href="project_progress.asp?pid=<%=projectID%>"><img src="images/progress_photos.jpg" alt="progress photos" /></a><br />
						<% end if %>						
						<a href="generate_pdf.asp?pid=<%=projectID%>"><img src="images/printable_fact_sheet.jpg" alt="printable fact sheet" /></a><br />
						<%if(rsTestimonials.recordCount > 0) then %>
						<a rel="lightbox-myGroup" title="<%=trim(rsProject("name"))%>" href="<%=sAssetPath%><%=rsTestimonials("asset_ID")%>&Instance=<%=siDAMInstance%>&type=asset&size=1&width=640&height=480"><img src="images/testimonial.jpg" alt="testimonial" /></a><br />
						
						<div style="display:none;">
						<%
						rsTestimonials.moveNext
						z = 1
						do while not rsTestimonials.eof
						z = z + 1
						%>
						<a rel="lightbox-myGroup" title="<%=trim(rsProject("name"))%>" href="<%=sAssetPath%><%=rsTestimonials("asset_ID")%>&Instance=<%=siDAMInstance%>&type=asset&size=1&width=640&height=480">Testimonial</a>
						<%
						rsTestimonials.moveNext
						loop
						%>
						</div>
						<%end if%>
						<%if (rsProject("challenge") <> "") then%>
						<a class="thickbox" href="#TB_inline?height=450&width=750&inlineId=caseStudy"><img src="images/case_study.jpg" alt="Case Study" /></a>
						<br />
						<div style="display:none;" id="caseStudy">
						<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
						<td valign="top" width="400" align="left">
						<img width="375" height="280" src="<%=sAssetPath%><%=rsProject("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=375&height=280&qfactor=<%=assetQFactor%>" alt="<%=trim(rsProject("name"))%>" />
						<br /><br />
						<big><b><%=rsProject("Name")%></b></big><br />
						<b><%=rsProject("Description")%></b>
						</td>
						<td valign="top">
						<b>CHALLENGE</b>
						<br />
						<%=rsProject("challenge")%>
						<br /><br />
						<%if(rsProject("resolution") <> "") then%>
						<b>APPROACH</b>
						<br />
						<%=rsProject("resolution")%>
						<br /><br />
						<%end if
						if (rsProject("result") <> "") then%>
						<b>SOLUTION</b>
						<br />
						<%=rsProject("result")%>
						<%end if%>
						</td>
						</tr>
						</table>
						</div>
						<%end if%> 						
						<br />
						<%if not (rsProject("url") = "" or rsProject("url_caption") = "") then%>
						<b><a target="_blank" href="<%=rsProject("url")%>"><%=rsProject("url_caption")%></a></b>
						<%end if%>
						</td>
					</table>
					<br />
					
					</div>
					
					
				</td>
				
				<td class="right_col_project">
				
					<div style="margin-left:10px;padding:10px;">
					<small><a href="portfolio.asp">Portfolio</a> <%if (rsDiscipline.recordCount > 0) then %> | <%response.write "<a href=""portfolio_cat.asp?cid=" & rsDiscipline("keyID") & """>" & rsDiscipline("keyName") & "</a> "%><%rsDiscipline.moveNext%><% do while not rsDiscipline.eof
		response.write ", <a href=""portfolio_cat.asp?cid=" & rsDiscipline("keyID") & """>" & rsDiscipline("keyName") & "</a> "
		rsDiscipline.moveNext
	loop %> | <%else %> | <%end if%> <%=rsProject("name")%></small>
					<br /><br />
					<b><big><%=rsProject("name")%></big><br />
					<%=rsProject("description")%></b><br /><br />
					<%=rsProject("descriptionmedium")%>
					<br /><br />
					<b>PROJECT DATA</b><br />
					<table cellpadding="0" cellspacing="0">
					<tr>
					<td valign="top"><b>Address:&nbsp;&nbsp;&nbsp;</b></td><td> <%=rsProject("address")%><br />
					<%=rsProject("city")%>, <%=rsProject("state_id")%>&nbsp;<%=rsProject("zip")%><br /></td>
					</tr>
					<%if (rsArchitect.recordCount > 0) then%><%if not(rsArchitect("item_value") = "") then %><tr><td valign="top"><b>Architect:&nbsp;&nbsp;&nbsp;</b></td><td> <%=rsArchitect("item_value")%></td></tr><%end if%><%end if%>	
					
					<%if (rsCompletion.recordCount > 0) then%><%if not(rsCompletion("item_value") = "") then %><tr><td valign="top"><b>Completion:&nbsp;&nbsp;&nbsp;</b></td><td> <%=MonthName(DatePart("m",rsCompletion("item_value")))%> <%=" " & DatePart("yyyy",rsCompletion("item_value"))%></td></tr><%end if%><%end if%>	
					
					</table>
					<br />
					<%if (rsNews.recordCount > 0) then%><b>PRESS RELEASES</b><br /><%end if%>
					<%do while not rsNews.eof %>
						
						<a href="view_news.asp?id=<%=rsNews("news_id")%>"><%=rsNews("headline")%></a><br />

					<%
					rsNews.movenext
					loop
					%>
					<br />
					<%if (rsNews.recordCount > 0) then%><a href="news.asp">See More&gt;&gt;</a><%end if%>
					<br />
					<br />
					<%if (rsAwards.recordCount > 0) then%><b>RECOGNITION</b><br /><%end if%>
					
					<%do while not rsAwards.eof %>
					
						<a href="awards.asp"><%=rsAwards("headline")%></a><br />

					<%
					rsAwards.movenext
					loop
					%>
					<br />
					<%if (rsAwards.recordCount > 0) then%><a href="awards.asp">See More&gt;&gt;</a><%end if%>
					</div>
				</td>
			</tr>
						<tr>
				<td class="left_col_project">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_project">					
				</td>
			</tr>
			</table>
			</div>
			
		
		</div>
	</div>
<div id="project_image_mask"></div>
</body>
</html>
