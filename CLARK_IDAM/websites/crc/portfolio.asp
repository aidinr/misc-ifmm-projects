<!--#include file="includes\config.asp" -->

<%

	shown_projects = ""

	set rsPioIde=server.createobject("adodb.recordset")
	sql="select * from ipm_project a, ipm_project_discipline,ipm_project_field_desc c,ipm_project_field_value d where a.projectid = d.projectid and c.item_id = d.item_id and c.item_tag = 'IDAM_PROJECT_CR_WEBSITE' and d.item_value = '1' and a.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND favorite = 1 AND keyid = " & DISC_PIO_IDE & " order by newid()"
	rsPioIde.Open sql, Conn, 1, 4
	
	shown_projects = shown_projects & rsPioIde("projectID")
	
	set rsImpLiv=server.createobject("adodb.recordset")
	sql="select * from ipm_project a, ipm_project_discipline,ipm_project_field_desc c,ipm_project_field_value d where a.projectid = d.projectid and c.item_id = d.item_id and c.item_tag = 'IDAM_PROJECT_CR_WEBSITE' and d.item_value = '1' and a.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND favorite = 1 AND a.projectid NOT IN (" & shown_projects &") AND keyid = " & DISC_IMP_LIV & " order by newid()"
	rsImpLiv.Open sql, Conn, 1, 4
	
	shown_projects = shown_projects & "," & rsImpLiv("projectID")
	
	set rsAdvUrb=server.createobject("adodb.recordset")
	sql="select * from ipm_project a, ipm_project_discipline,ipm_project_field_desc c,ipm_project_field_value d where a.projectid = d.projectid and c.item_id = d.item_id and c.item_tag = 'IDAM_PROJECT_CR_WEBSITE' and d.item_value = '1' and a.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND favorite = 1 AND a.projectid NOT IN (" & shown_projects &") AND keyid = " & DISC_ADV_URB & " order by newid()"
	rsAdvUrb.Open sql, Conn, 1, 4
	
	shown_projects = shown_projects & "," & rsAdvUrb("projectID")
	
	set rsBuiCom=server.createobject("adodb.recordset")
	sql="select * from ipm_project a, ipm_project_discipline,ipm_project_field_desc c,ipm_project_field_value d where a.projectid = d.projectid and c.item_id = d.item_id and c.item_tag = 'IDAM_PROJECT_CR_WEBSITE' and d.item_value = '1' and a.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND favorite = 1 AND a.projectid NOT IN (" & shown_projects &") AND keyid = " & DISC_BUI_COM & " order by newid()"
	rsBuiCom.Open sql, Conn, 1, 4
	
	shown_projects = shown_projects & "," & rsBuiCom("projectID")
	
	set rsChaHor=server.createobject("adodb.recordset")
	sql="select * from ipm_project a, ipm_project_discipline,ipm_project_field_desc c,ipm_project_field_value d where a.projectid = d.projectid and c.item_id = d.item_id and c.item_tag = 'IDAM_PROJECT_CR_WEBSITE' and d.item_value = '1' and a.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND favorite = 1 AND a.projectid NOT IN (" & shown_projects &") AND keyid = " & DISC_CHA_HOR & " order by newid()"
	rsChaHor.Open sql, Conn, 1, 4
	
	shown_projects = shown_projects & "," & rsChaHor("projectID")
	
	set rsDevRes=server.createobject("adodb.recordset")
	sql="select * from ipm_project a, ipm_project_discipline,ipm_project_field_desc c,ipm_project_field_value d where a.projectid = d.projectid and c.item_id = d.item_id and c.item_tag = 'IDAM_PROJECT_CR_WEBSITE' and d.item_value = '1' and a.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND favorite = 1 AND a.projectid NOT IN (" & shown_projects &") AND keyid = " & DISC_DEV_RES & " order by newid()"
	rsDevRes.Open sql, Conn, 1, 4
	
	set rsCaseStudy = server.createobject("adodb.recordset")
	sql = "select top 1 a.name pname, b.name aname, a.description description, a.city city, a.state_id state, a.projectid pid, b.asset_id aid from ipm_project_field_desc c, ipm_project_field_value d,ipm_project a left join ipm_asset b on a.projectid = b.projectid and b.name = 'case_study' where challenge not like '' and favorite = 1 and a.available = 'y' and c.item_id = d.item_id and c.item_tag = 'IDAM_PROJECT_CR_WEBSITE' and d.projectid = a.projectid and d.item_value = '1' and b.securitylevel_id = 3 and b.available = 'y' order by newid()"
	rsCaseStudy.Open sql, Conn, 1,4
	
	set rsTeam = server.createobject("adodb.recordset")
	sql = "select top 2 firstname,c.item_value,a.userid from ipm_user a,ipm_project_contact b, ipm_user_field_value c, ipm_user_field_value d where a.userid = b.userid and c.user_id = a.userid and c.item_id = 21767007 and d.item_id = 21767014 and a.userid = d.user_id and d.item_value = '1' and b.projectid = " & rsCaseStudy("pid")
	rsTeam.Open sql, Conn, 1,4
%>

<html>
<head>
<title><%=sPageTitle%> - Portfolio</title>
	<!--#include file="includes\header.asp" -->
	<script type="text/javascript" src="js/swfobject.js"></script>
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td class="left_col">
					<div id="content_text">
					<small>Portfolio</small>
					<br /><br />
					<img src="images/portfolio.jpg" alt="portfolio" />
					<br /><br />
					Clark Realty Capital's experience spans more than 60 projects, 38,000 residential units, 10,000 acres of land, and 50 million SF of development, comprising nearly $7 billion of pubic and private work developed or under development.
					
					<table  cellpadding="8" cellspacing="8" width="100%" >
						<tr>
							<td width="50%">
								<table width="100%">
									<tr>
										<td colspan="2" align="left"><a href="portfolio_cat.asp?cid=<%=DISC_PIO_IDE%>"><img src="images/pioneering_ideas.jpg" alt="Pioneering Ideas" style="margin-left:5px;" /></a></td>
									</tr>
									<tr>	<td valign="top" align="left" width="50%"><div class="portfolio_bg"><a href="portfolio_cat.asp?cid=<%=DISC_PIO_IDE%>"><img width="137" height="108" class="portfolio_photo" src="<%=sAssetPath%><%=rsPioIde("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=137&height=108&qfactor=<%=assetQFactor%>" alt="<%=trim(rsPioIde("name"))%>"/></a></div></td>
										<td valign="top" align="left" width="50%"><b><%=trim(rsPioIde("name"))%></b><br /><%=rsPioIde("city")%>, <%=rsPioIde("state_id")%></td>
									</tr>
								</table>								
							</td>
							<td width="50%">
								<table width="100%">
									<tr>
										<td colspan="2" align="left"><a href="portfolio_cat.asp?cid=<%=DISC_IMP_LIV%>"><img src="images/improving_lives.jpg" alt="Improving Lives" style="margin-left:5px;" /></a></td>
									</tr>
									<tr>	<td valign="top" align="left" width="50%"><div class="portfolio_bg"><a href="portfolio_cat.asp?cid=<%=DISC_IMP_LIV%>"><img width="137" height="108" class="portfolio_photo" src="<%=sAssetPath%><%=rsImpLiv("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=137&height=108&qfactor=<%=assetQFactor%>" alt="<%=trim(rsImpLiv("name"))%>"/></a></div></td>
										<td valign="top" align="left" width="50%"><b><%=trim(rsImpLiv("name"))%></b><br /><%=rsImpLiv("city")%>, <%=rsImpLiv("state_id")%></td>
									</tr>
								</table>	
							</td>
						</tr>
						<tr>
							<td width="50%">
								<table width="100%">
									<tr>
										<td colspan="2" align="left"><a href="portfolio_cat.asp?cid=<%=DISC_ADV_URB%>"><img src="images/advancing_urbanism.jpg" alt="Advancing Urbanis," style="margin-left:5px;" /></a></td>
									</tr>
									<tr>	<td valign="top" align="left" width="50%"><div class="portfolio_bg"><a href="portfolio_cat.asp?cid=<%=DISC_ADV_URB%>"><img width="137" height="108" class="portfolio_photo" src="<%=sAssetPath%><%=rsAdvUrb("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=137&height=108&qfactor=<%=assetQFactor%>" alt="<%=trim(rsAdvUrb("name"))%>"/></a></div></td>
										<td valign="top" align="left" width="50%"><b><%=trim(rsAdvUrb("name"))%></b><br /><%=rsAdvUrb("city")%>, <%=rsAdvUrb("state_id")%></td>
									</tr>
								</table>								
							</td>
							<td width="50%">
								<table width="100%">
									<tr>
										<td colspan="2" align="left"><a href="portfolio_cat.asp?cid=<%=DISC_BUI_COM%>"><img src="images/building_community.jpg" alt="Building Communities" style="margin-left:5px;" /></a></td>
									</tr>
									<tr>	<td valign="top" align="left" width="50%"><div class="portfolio_bg"><a href="portfolio_cat.asp?cid=<%=DISC_BUI_COM%>"><img width="137" height="108" class="portfolio_photo" src="<%=sAssetPath%><%=rsBuiCom("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=137&height=108&qfactor=<%=assetQFactor%>" alt="<%=trim(rsBuiCom("name"))%>"/></a></div></td>
										<td valign="top" align="left" width="50%"><b><%=trim(rsBuiCom("name"))%></b><br /><%=rsBuiCom("city")%>, <%=rsBuiCom("state_id")%></td>
									</tr>
								</table>	
							</td>
						</tr>
						<tr>
							<td width="50%">
								<table width="100%">
									<tr>
										<td colspan="2" align="left"><a href="portfolio_cat.asp?cid=<%=DISC_CHA_HOR%>"><img src="images/changing_horizons.jpg" alt="Changing Horizons" style="margin-left:5px;" /></a></td>
									</tr>
									<tr>	<td valign="top" align="left" width="50%"><div class="portfolio_bg"><a href="portfolio_cat.asp?cid=<%=DISC_CHA_HOR%>"><img width="137" height="108" class="portfolio_photo" src="<%=sAssetPath%><%=rsChaHor("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=137&height=108&qfactor=<%=assetQFactor%>" alt="<%=trim(rsChaHor("name"))%>"/></a></div></td>
										<td valign="top" align="left" width="50%"><b><%=trim(rsChaHor("name"))%></b><br /><%=rsChaHor("city")%>, <%=rsChaHor("state_id")%></td>
									</tr>
								</table>									
							</td>
							<td width="50%">
								<table width="100%">
									<tr>
										<td colspan="2" align="left"><a href="portfolio_cat.asp?cid=<%=DISC_DEV_RES%>"><img src="images/developing_responsibility.jpg" alt="Developing Responsibility" style="margin-left:5px;" /></a></td>
									</tr>
									<tr>	<td valign="top" align="left" width="50%"><div class="portfolio_bg"><a href="portfolio_cat.asp?cid=<%=DISC_DEV_RES%>"><img width="137" height="108" class="portfolio_photo" src="<%=sAssetPath%><%=rsDevRes("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=137&height=108&qfactor=<%=assetQFactor%>" alt="<%=trim(rsDevRes("name"))%>"/></a></div></td>
										<td valign="top" align="left" width="50%"><b><%=trim(rsDevRes("name"))%></b><br /><%=rsDevRes("city")%>, <%=rsDevRes("state_id")%></td>
									</tr>
								</table>	
							</td>
						</tr>						
					</table>
					
					</div>
					
					
				</td>
				
				<td class="right_col">
				<%if (rsCaseStudy.recordCount > 0) then %>
				<img src="images/portfolio_side.jpg" alt="Case Study" /><div id="flashcontent"><%if (isnull(rsCaseStudy("aname"))) then%><img width="375" height="295" src="<%=sAssetPath%><%=rsCaseStudy("pid")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=375&height=295&qfactor=<%=assetQFactor%>" alt="<%=trim(rsCaseStudy("pname"))%>"/></a><%end if%></div>
					<%if (trim(rsCaseStudy("aname")) = "case_study") then%>
					<script type="text/javascript">
						// <![CDATA[
						var so = new SWFObject("flvplayer9_crc.swf?video=getsmil.asp?id=<%=rsCaseStudy("aid")%>&skinswf=SkinUnderPlaySeekMute.swf", "sotester", "375", "323", "9", "#FFFFFF");
						so.useExpressInstall('expressinstall.swf');
						so.addParam("wmode","transparent");
						so.write("flashcontent");
						// ]]>
					</script>
					<%
					end if%>
				<br />
				<div id="portfolio_text">
				<b><big><%=rsCaseStudy("pname")%></big></b><br />
				<%=rsCaseStudy("city") & ", " & rsCaseStudy("state")%><br />
				<br />
				<%=rsCaseStudy("description")%><br />
				<a href="project.asp?pid=<%=rsCaseStudy("pid")%>">More &gt;&gt;</a>
				<br /><br />
				<big><b class="subheader">MEET THE TEAM</b></big>
				<br />
				
				<%if not rsTeam.eof then%>				
				<table width="100%">
					<tr>
						<td width="50%" valign="top" align="left">
						<img src="<%=sAssetPath%><%=rsTeam("userID")%>&Instance=<%=siDAMInstance%>&type=contact&size=1&width=150&height=110&qfactor=<%=assetQFactor%>" alt="<%=rsTeam("FirstName")%>" /><br />
						<br />
						<big><b><%=rsTeam("firstname")%></b></big><br />
						<%=rsTeam("item_value")%>
						<br /><br />
						<a href="team_profile.asp?id=<%=rsTeam("userID")%>">Profile &gt;&gt;</a>
						</td>
						<td width="50%" valign="top" align="left">
						<%
						rsTeam.movenext
						if not rsTeam.eof then
						%>
						<img src="<%=sAssetPath%><%=rsTeam("userID")%>&Instance=<%=siDAMInstance%>&type=contact&size=1&width=150&height=110&qfactor=<%=assetQFactor%>" alt="<%=rsTeam("FirstName")%>" /><br />
						<br />
						<big><b><%=rsTeam("firstname")%></b></big><br />
						<%=rsTeam("item_value")%>
						<br /><br />
						
						<a href="team_profile.asp?id=<%=rsTeam("userID")%>">Profile &gt;&gt;</a>
						<%end if%>						
						</td>
					</tr>
				</table>
				<%end if%>
				</div>	
				<%end if%>
				</td>
			</tr>
			<tr>
				<td class="left_col">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col">					
				</td>
			</tr>			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-2");
pageTracker._trackPageview();
</script></body>
</html>
