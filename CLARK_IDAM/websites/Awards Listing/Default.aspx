﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" ValidateRequest="false" enableEventValidation="false" Inherits="_Default" %>
<html>
<head>
<title>Award Listing</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />	
	

</head>
<body >
	<div id="drop_shadow">
		<div id="container">
			
			<div id="header">
				
				<table cellpadding="0" cellspacing="0" width="100%" >
				<tr>
					<td width="375">
						<a href="index.asp"><img src="images/spacer.gif" width="375" height="120" alt="home page" /></a>
					</td>
					<td align="right">
									<div id="header_logo">
				<a href="index.asp"><img src="images/clark_builders_group.jpg" /></a>
				<br />
				CONTACT | ADVANCED SEARCH
				</div>
					</td>
				</tr>
				</table>

				
				
			</div>





<!--[if lte IE 6]>
<style type="text/css">
img {
	behavior: url("pngbehavior.htc");
}
    #nav td.navigation { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/menu_bg.png", sizingMethod="scale");
}
    #nav td.navigation_right { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/menu_bg.png", sizingMethod="scale");
}
    #nav td.navigation_bottom_left { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/nav_bg_bottom_left.png", sizingMethod="scale");
}
    #nav td.navigation_bottom_right { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/nav_bg_bottom_right.png", sizingMethod="scale");
}

    #nav td.navigation_bottom { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/nav_bg_bottom.png", sizingMethod="scale");
}

    #nav td.navigation_left {
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/nav_bg_left.png", sizingMethod="scale");	
    }   
</style>
<![endif]-->

<style type="text/css">
   
    #content {
    	height: 100%;
    }
    #home_content {
    	height: 100%;
    }
    #project_image_mask {
    	display: none;
    }
</style>

			
			
			<div id="content">

			<br />
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview" style="width:100%;">
					<div id="content_text">
					Marketing Tools | Generate Awards Listing
					<br /><br />
					<img src="images/awards_reporting.jpg" alt="Project Reporting" />
					<h4>Step 1 - Select Award Search Term</h4>
					<form runat=server method="post" action="Default.aspx">
				<table>
					<tr>
						<td>Keywords:</td>
						<td><input type="text" name="keywords" /></td>
					</tr>
					<tr>
						<td>Type:</td>
						<td>	<select name="awards_type">
							<option value="">All</option>
							<%
							    For Each row In rsAwardsTypes.Rows
							        Dim AwardsType As String = Trim(row("type_value"))
							%>
							
							<option value="<%=row("type_id")%>"><%Response.Write(AwardsType)%></option>
							
							<%
							Next
							
							%>
							</select>
						</td>
					</tr>					
					<tr>
						<td>Organization Name:</td>
						<td><select name="org_name">
						<option value="">All</option>

                        <%
                            For Each row In rsOrganizationName.Rows
                                Dim organizationName As String = Trim(row("item_value"))
							%>
							
							<option value="<%Response.Write(organizationName)%>"><%Response.Write(organizationName)%></option>
							
							<%
							Next
							
							%>

							
							</select></td>
					</tr>
<tr>
						<td>Project:</td>
						<td>
						<select name="related_project">
						<option value="">All</option>
						

                             <%
                                 For Each row In rsRelatedProjects.Rows
                                     Dim projectName As String = Trim(row("name"))
							%>
							
							<option value="<%Response.Write(row("projectID"))%>"><%Response.Write(projectName)%></option>
							
							<%
							Next
							
							%>


						</select>
</td>
					</tr>
					<tr>
						<td>Year:</td>
						<td>
						<select name="awards_years">
						<option value="">All</option>

                        
                             <%
                                 For Each row In rsAwardsYears.Rows
                                   
							%>
							
							<option value="<%response.write(row("awardsyears"))%>"><%Response.Write(row("awardsyears"))%></option>
							
							<%
							Next
							
							%>



						</select>
</td>
					</tr>						
				</table>



<h4>Step 2 - Filter by:</h4>



<%
    For Each row In rsAwardsFilters.Rows
        Dim tmpVals = Split(row("item_default_value"), ";")
        For i = 0 To UBound(tmpVals)
            Response.Write("<input name=""chkbk_filter"" type=""checkbox"" value=""" & tmpVals(i) & """ /> " & tmpVals(i) & "<br/>")
        Next
        
    Next
							%>



<h4>Step 3 - Select Formatting Options</h4>
						<p>
						<table>
							<tr>
								<td vAlign="top">
								<input id="checkboxLetterhead" name="checkboxLetterhead" type="checkbox"></td>
								<td vAlign="top">Show Letterhead</td>
							</tr>
							<tr>
								<td vAlign="top">
								<input id="checkboxCustomizedHeader" name="checkboxCustomizedHeader" type="checkbox"></td>
								<td vAlign="top">Cust. Report Header <br>
								<input id="textboxCustomizedHeader" name="textboxCustomizedHeader" type="text"></td>
							</tr>
							<!--<tr>
			                <td valign="top">
			                    <select name="head_x" id="head_x">
	<option value="106">-51</option>
	<option value="103">-48</option>
	<option value="100">-45</option>
	<option value="97">-42</option>
	<option value="94">-39</option>
	<option value="91">-36</option>
	<option value="88">-33</option>
	<option value="85">-30</option>
	<option value="82">-27</option>
	<option value="79">-24</option>
	<option value="76">-21</option>
	<option value="73">-18</option>
	<option value="70">-15</option>
	<option value="67">-12</option>
	<option value="64">-9</option>
	<option value="61">-6</option>
	<option value="58">-3</option>
	<option selected="selected" value="55">0</option>
	<option value="52">3</option>
	<option value="49">6</option>
	<option value="46">9</option>
	<option value="43">12</option>
	<option value="40">15</option>
	<option value="37">18</option>
	<option value="34">21</option>
	<option value="31">24</option>
	<option value="34">27</option>
	<option value="37">30</option>
	<option value="40">33</option>
	<option value="43">36</option>

</select>
				                </td>
			                <td>Adjust Title's X Position</td>
		                </tr>	-->
							<tr>
								<td vAlign="top">
								<input id="checkboxCustomizedTitle" name="checkboxCustomizedTitle" type="checkbox"></td>
								<td vAlign="top">Cust. Report Title <br>
								<input id="textboxCustomizedTitle" name="textboxCustomizedTitle" type="text"></td>
							</tr>
							<tr>
								<td vAlign="top">
								<input id="checkboxCustomizedLogo" name="checkboxCustomizedLogo" type="checkbox"></td>
								<td vAlign="top">Cust. Logo <br>
								<select id="dropdownCustomizedLogo" name="dropdownCustomizedLogo">
								<option selected value="">Select Logo
								</option>
								<option value="CBG">Clark Builders Group
								</option>
								<option value="CRC">Clark Realty Capital
								</option>
								<option value="CR">Clark Companies</option>
								<option value="CRM">Clark Realty Management
								</option>
								</select></td>
							</tr>
                            <tr>
								<td vAlign="top">
								<input id="checkboxCustomizedCarousel" name="checkboxCustomizedCarousel" type="checkbox"></td>
								<td vAlign="top">Use Carousel Images <br>
                                <asp:DropDownList id=ddCustomizedCarousel runat=server DataValueField=carrousel_id DatatextField=name></asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td vAlign="top">&nbsp;</td>
								<td vAlign="top">&nbsp;</td>
							</tr>
						</table>
						</p>







<table cellpadding="3" cellspacing="3">
		<tr>
			<td><input type="submit" value="Generate Report" /></td>
		</tr>
<input type="hidden" value="1" name="submit" />
</table>
</form>
					
					
					
					
					
					</div>
					
				</td>
				


			</tr>
	
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
