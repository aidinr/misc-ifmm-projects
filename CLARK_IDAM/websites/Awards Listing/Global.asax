﻿<%@ Application Language="VB" %>

<script runat="server">

    Const sBaseIP As String = "idam.clarkrealty.com"
    Const sBaseInternalIP As String = "idam.clarkrealty.com"
    Const sBaseInstance As String = "IDAM_CRC"
    Const sVSIDAMClientUpload As String = "idamClientUpload"
    Const sVSIDAMClientDownload As String = "idamClientDownload"
    Const sVSIDAMClient As String = "IDAMClient"
    Const sVSIDAMClient110Upload As String = "IDAMFileUpload"
    
    Const userName As String = "jburlinson"
    Const password As String = "burlinson"
    
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
        Dim SessionID As String = getSessionID("http://" & sBaseIP & "/" & sVSIDAMClient & "/GetSessionID.aspx")
        Session("clientSessionID") = getSessionID("http://" & sBaseIP & "/" & sVSIDAMClient & "/GetSessionID.aspx")
        Session("clientDownloadSessionID") = getSessionID("http://" & sBaseIP & "/" & sVSIDAMClientDownload & "/GetSessionID.aspx")
        Session("WSLogin") = "http://" & sBaseInternalIP & "/" & sVSIDAMClient & "/(" + Session("clientSessionID") + ")/Login.aspx"
        Session("WSLoginDownload") = "http://" & sBaseInternalIP & "/" & sVSIDAMClientDownload & "/(" + Session("clientSessionID") + ")/Login.aspx"
        Session("WSLoginStatus") = "http://" & sBaseInternalIP & "/" & sVSIDAMClient & "/(" + Session("clientSessionID") + ")/GetLoginStatus.aspx"
        Call LoginToWebserviceClient(Session("WSLogin"), userName, password, sBaseInstance, "false")
        'Call LoginToWebserviceClient(Session("WSLoginStatus"), "alfred09", "teja090108", "IDAM_HEYMAN", "false")
        Call LoginToWebserviceClient(Session("WSLoginDownload"), userName, password, sBaseInstance, "false")
        Session("WSRetreiveAsset") = "http://" & sBaseIP & "/" & sVSIDAMClient & "/(" + Session("clientSessionID") + ")/RetrieveAsset.aspx?instance=" & sBaseInstance & "&"
        Session("WSDownloadAsset") = "http://" & sBaseIP & "/" & sVSIDAMClientDownload & "/(" + Session("clientSessionID") + ")/DownloadFile.aspx?dtype=assetdownload&instance=" & sBaseInstance & "&"
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
       
    
    Function getSessionID(ByVal page As String) As String
        On Error Resume Next
        Dim wc As New Net.WebClient
        Dim data As IO.Stream = wc.OpenRead(page)
        Dim reader As IO.StreamReader = New IO.StreamReader(data)
        Dim str As String = ""
        str = reader.ReadToEnd
        
        Dim result As String = str
        Dim beginpos As String = InStr(result, "SESSIONID=") + 10
        Dim endpos As String = InStr(result, "</span>")
        result = Mid(result, beginpos, endpos - beginpos)
        getSessionID = UCase(result)
    End Function

    Function LoginToWebserviceClient(ByVal loginPage As String, ByVal login As String, ByVal password As String, ByVal instance As String, ByVal encrypted As String) As String
        On Error Resume Next
        
        Dim wc As New Net.WebClient
        Dim data As IO.Stream = wc.OpenRead(loginPage & "?txtLogin=" & login & "&txtPassword=" & password & "&txtInstanceID=" & instance & "&encrypted=" & encrypted)
        Dim reader As IO.StreamReader = New IO.StreamReader(data)
        Dim str As String = ""
        str = reader.ReadToEnd
        Dim result As String = str
        LoginToWebserviceClient = result
    End Function
           
    
</script>