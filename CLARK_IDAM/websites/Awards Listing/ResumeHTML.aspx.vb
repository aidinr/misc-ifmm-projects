﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient


Partial Class ResumeHTML
    Inherits System.Web.UI.Page

    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim adjust_y As String = "33"
        'Dim adjust_x As String = "55"

        Dim adjust_y As String = Request.QueryString("y")
        Dim adjust_x As String = Request.QueryString("x")

        Dim reportTitle As String = Request.QueryString("report")
        Dim reportLetterhead As String = Request.QueryString("letterhead")
        Dim reportHeader As String = Request.QueryString("title")

        If (reportLetterhead = "1") Then
            'Page1Header.Text = "<table style=""margin-top:105px;position:relative;left:5px;"" width=""88%"" cellpadding=""12"" cellspacing=""12"" align=""center"">"

            Page1Header.Text = "<div align=""right"" style=""margin-top: " & adjust_y & "px;margin-right: " & adjust_x & "px;height:22px;font-family: Zapf Humanist 601 BT;color:#5a471c;font-size:22px;padding-top:4px;"">" & reportHeader & "</div><table style=""margin-top:28px;position:relative;left:5px;"" width=""100%"" cellpadding=""0"" cellspacing=""0"" align=""center"">"
        Else
            Page1Header.Text = "<div align=""right"" style=""margin-top:" & adjust_y & "px;margin-right: " & adjust_x & "px;height:22px;font-family: Zapf Humanist 601 BT;color:#5a471c;font-size:22px;padding-top:4px;"">" & reportHeader & "</div><table style=""margin-top:28px;position:relative;left:5px;"" width=""100%"" cellpadding=""0"" cellspacing=""0"" align=""center"">"
        End If

        labelTitle.Text = reportTitle

        Dim theSQL As String = Server.HtmlDecode(Request.QueryString("s"))
        Dim theUser As String = Request.QueryString("uid")

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim sql As String = theSQL '"select firstname, lastname, agency, position, a.userid userid, picture, c.item_value shortbio from ipm_resume_item b,ipm_user a left join ipm_user_field_value c on a.userid = c.user_id and c.item_id = 21767702 where a.userid = b.userid and b.session_id = @sessionid and a.active = 'Y' group by firstname, lastname, agency, position, bio, a.userid, picture,c.item_value"
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim DT1 As New DataTable("resume")
        MyCommand1.Fill(DT1)


        repeaterEmployees.DataSource = DT1
        repeaterEmployees.DataBind()


        If Request.QueryString("usec") <> "" And Request.QueryString("cid") <> "" Then
            Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter("select asset_id from ipm_asset where available = 'Y' and asset_id in (select asset_id from ipm_carrousel_item where active=1 and available='Y' and carrousel_id = " & Request.QueryString("cid") & ")", MyConnection)
            Dim DT2 As New DataTable("resume")
            MyCommand2.Fill(DT2)
            repeaterAwards.DataSource = DT2
            repeaterAwards.DataBind()
            'AddHandler images
        Else
            'Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql.Replace("select distinct", "select distinct top 7"), MyConnection)
            'Dim DT2 As New DataTable("resume")
            'MyCommand2.Fill(DT2)
            'repeaterAwards.DataSource = DT2
            'repeaterAwards.DataBind()
            'AddHandler images
        End If




    End Sub
    Public Sub repeaterAwardImages_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        Dim awardImage As Image = e.Item.FindControl("awardImage")

        awardImage.ImageUrl = Session("WSRetreiveAsset") & "id=" & e.Item.DataItem("asset_id") & "&type=asset&size=0&crop=0"



        awardImage.Width = 98
        awardImage.Height = 95
        awardImage.Visible = True

    End Sub
    Public Sub RepeaterEmployees_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        Dim theSession As String = Request.QueryString("s")

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim labelEducationTitle As Label = e.Item.FindControl("employeeEducationTitle")
            Dim labelEducation As Label = e.Item.FindControl("employeeEducation")
            Dim literalPageBreak As Literal = e.Item.FindControl("literalPageBreak")
            Dim literalPageEnd As Literal = e.Item.FindControl("literalPageEnd")
            Dim employeeImage As Image = e.Item.FindControl("employeeImage")
            Dim employeeImageURL As Label = e.Item.FindControl("employeeImageURL")
            Dim shortbio As Label = e.Item.FindControl("shortbio")

            Dim adjust_y As String = "33"
            Dim adjust_x As String = "55"

            Dim reportHeader As String = Request.QueryString("report")



            Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
            Dim sql2 As String = "select * from ipm_awards_related_projects a,ipm_project b where a.projectid = b.projectid and b.available = 'y' and awards_id = @awards_id"
            Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)
            Dim awards_id As New SqlParameter("@awards_id", SqlDbType.VarChar, 255)
            awards_id.Value = e.Item.DataItem("awards_id")
            MyCommand2.SelectCommand.Parameters.Add(awards_id)
            Dim DT2 As New DataTable("projects")


            MyCommand2.Fill(DT2)

            If (DT2.Rows.Count > 0) Then

                For Each x As DataRow In DT2.Rows

                    If (x("name") <> "") Then
                        labelEducation.Text = labelEducation.Text & x("name") & " - " & x("city") & ", " & x("state_id") & "<BR>"

                    End If

                Next
                If labelEducation.Text.LastIndexOf("<BR>") > 0 Then
                    labelEducation.Text = labelEducation.Text.Substring(0, labelEducation.Text.LastIndexOf("<BR>"))
                End If
            Else
                Dim MyConnection2 As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
                Dim sql22 As String = "select * from ipm_awards where awards_id = @awards_id"
                Dim MyCommand22 As SqlDataAdapter = New SqlDataAdapter(sql22, MyConnection2)
                Dim awards_id2 As New SqlParameter("@awards_id", SqlDbType.VarChar, 255)
                awards_id2.Value = e.Item.DataItem("awards_id")
                MyCommand22.SelectCommand.Parameters.Add(awards_id2)
                Dim DT22 As New DataTable("projects")
                MyCommand22.Fill(DT22)

                Dim content As String = DT22.Rows(0)("content") & ""

                If content.ToUpper().Contains("</A>") Then
                    content = content.Trim().Substring(content.Trim().IndexOf(">") + 1)
                    content = content.Trim().Substring(0, content.Trim().IndexOf("</"))
                End If
                labelEducation.Text = content
            End If

            '?id=200001327&Instance=IDAM_CRC&type=awards&size=2 ?id=200000878&Instance=IDAM_CRC&type=awards&size=2



            ' '' '' '' ''If (Not IsDBNull(e.Item.DataItem("shortbio"))) Then
            ' '' '' '' ''    shortbio.Text = e.Item.DataItem("shortbio").ToString.Replace(vbCrLf, "<br />")
            ' '' '' '' ''End If


            ' '' '' '' ''If (e.Item.ItemIndex = 3) Then
            ' '' '' '' ''    literalPageBreak.Text = "<div style=""page-break-after:always"">&nbsp;</div><table style=""margin-top:80px;position:relative;left:5px;"" width=""88%"" cellpadding=""12"" cellspacing=""12"" align=""center""><tr><td>"
            ' '' '' '' ''ElseIf (e.Item.ItemIndex Mod 6 = 0 And e.Item.ItemIndex <> 0) Then
            ' '' '' '' ''    literalPageBreak.Text = "<div style=""page-break-after:always"">&nbsp;</div>" & "<table width=""612""><tr><td width=""612"" valign=""top"">" & "<div align=""right"" style=""margin-top:" & adjust_y & "px;margin-right: " & adjust_x & "px;height:22px;font-family: Zapf Humanist 601 BT;color:#5a471c;font-size:22px;padding-top:4px;"">" & reportHeader & "</div><table style=""margin-top:28px;position:relative;left:5px;"" width=""88%"" cellpadding=""12"" cellspacing=""12"" align=""center"">" & "<tr><td width=""100%"">"
            ' '' '' '' ''ElseIf (e.Item.ItemIndex Mod 3 = 0 And e.Item.ItemIndex <> 3 And e.Item.ItemIndex <> 0) Then
            ' '' '' '' ''    literalPageBreak.Text = "<div style=""page-break-after:always"">&nbsp;</div><table style=""margin-top:80px;position:relative;left:5px;"" width=""88%"" cellpadding=""12"" cellspacing=""12"" align=""center""><tr><td>"
            ' '' '' '' ''End If

            ' '' '' '' ''If (e.Item.ItemIndex = 2) Then
            ' '' '' '' ''    literalPageEnd.Text = "</tr></table></td></tr></table>"
            ' '' '' '' ''ElseIf (e.Item.ItemIndex Mod 6 = 5) Then
            ' '' '' '' ''    literalPageEnd.Text = "</tr></table></td></tr></table>"
            ' '' '' '' ''ElseIf (e.Item.ItemIndex Mod 3 = 2 And e.Item.ItemIndex <> 2) Then
            ' '' '' '' ''    literalPageEnd.Text = "</td></tr></table>"
            ' '' '' '' ''End If

        End If





    End Sub

End Class
