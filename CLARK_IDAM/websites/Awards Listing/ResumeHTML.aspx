﻿<%@ page language="VB" autoeventwireup="false"  CodeFile="ResumeHTML.aspx.vb"  inherits="ResumeHTML" %>

<style  type="text/css">
    body
    {
        padding:50px;
    }
    
td {
	font-family: Zapf Humanist 601 BT;
	font-size: 7.5pt;
	line-height: 9pt;
	color: #000000;
}
.header {
	font-family: Times New Roman;
	color: #996d50;
	font-size: 21pt;
	line-height: 23pt;
	color: rgb(153,109,80);
	
}
.subheader {
	font-size: 9pt;
	color: #000000;
}
.subheader2 {
	color: rgb(153,109,80);
	font-size: 8pt;
}
.subheader3 {
	font-size: 7.5pt;
	line-height: 10.5pt;
	font-weight: bold;
	color: #000000;
}
.employeeImage 
{
    margin-right: 8px;   
}
.projectvalue {
	line-height: 7.5pt;
}

.crop{
	float:left;
	margin:   .1em   10px .1em 20px ;
	overflow:hidden; /* this is important */

	width:98px;
	height:110px;
	}
	/* input values to crop the image: top, right, bottom, left */
.crop img{
	margin:0px 0px  0px  0px;
	}
</style>
<div style="position:absolute; top:170px; left:50px; height:110px; width:140px;">
<asp:Repeater ID="repeaterAwards" runat="server" OnItemDataBound="repeaterAwardImages_ItemDataBound">
<ItemTemplate>
<p class="crop"><asp:Image ID="awardImage" runat="server"  ></asp:Image></p>
</ItemTemplate>
</asp:Repeater>
</div> 
<table width="100%">
		<tr>
		<td width="100%" valign="top">
		<asp:Literal ID="Page1Header" runat="server"></asp:Literal>
		<tr>
		    <td width="100%">
		    
		    <asp:Label ID="labelTitle" runat="server" CssClass="header"></asp:Label>
		    <hr style="position:relative;top:-3px;width:100%;height:1px;border-bottom:0;border-left:0;border-right:0;border-top:1px solid #5a471c;color:#5a471c" /><br />
		    <asp:Repeater ID="repeaterEmployees" runat="server" OnItemDataBound="repeaterEmployees_ItemDataBound">
		    <ItemTemplate>
		    <asp:Literal ID="literalPageBreak" runat="server"></asp:Literal>
		    
		    <table width="100%">
		        <tr>
		            <td valign="top" width="118">
		            </td>
		            <td valign="top">
		                <div style=" font-weight:bold;"><%# Container.DataItem("headline").ToString & IIf((Container.DataItem("publicationtitle") & "").ToString().Trim() = "", "", " - " & Container.DataItem("publicationtitle").ToString)%></div>
		                 <!--<span style="line-height:11pt;"><i><%#Container.DataItem("org_name")%></i> | <i><%#Container.DataItem("rec_name")%></i><br /></span>-->
		                
		               
		               <span class="subheader2"><asp:Label ID="employeeEducation" runat="server"></asp:Label></span> 
		               <br /><br />
		            </td>
		        </tr>
		    </table>
		    <asp:Literal ID="literalPageEnd" runat="server"></asp:Literal>
		    </ItemTemplate>
		    </asp:Repeater>