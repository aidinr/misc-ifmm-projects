﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class ResumeHTMLControl2
    Inherits System.Web.UI.UserControl
    Public projectslisted As Boolean = True

    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim adjust_y As String = "33"
        'Dim adjust_x As String = "55"

        Dim adjust_y As String = Request.QueryString("y")
        Dim adjust_x As String = Request.QueryString("x")

        Dim reportHeader As String = Request.QueryString("report")
        Dim reportLetterhead As String = Request.QueryString("letterhead")

        If (reportLetterhead = "1") Then
            'Page1Header.Text = "<table style=""margin-top:105px;position:relative;left:5px;"" width=""88%"" cellpadding=""12"" cellspacing=""12"" align=""center"">"

            Page1Header.Text = "" '<div align=""right"" style=""position:relative;width:530px;font-family: Zapf Humanist 601 BT;color:#5a471c;font-size:22px;"">" & reportHeader & "</div>"
            Page2Header.Text = "" '<div align=""right"" style=""position:relative;width:530px;font-family: Zapf Humanist 601 BT;color:#5a471c;font-size:22px;"">" & reportHeader & "</div>"


        Else
            Page1Header.Text = "<div align=""right"" style=""width:530px;height:22px;font-family: Zapf Humanist 601 BT;color:#5a471c;font-size:22px;"">" & reportHeader & "</div>"
            Page2Header.Text = "" '<div align=""right"" style=""width:530px;font-family: Zapf Humanist 601 BT;color:#5a471c;font-size:22px;""></div>"
        End If






        Dim theSession As String = Request.QueryString("s")
        Dim theUser As String = Request.QueryString("uid")

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))

        Dim sql As String = "select b.lastname lastname, b.firstname firstname, a.type type, a.description description, c.name name, bio, agency, email, phone, position, c.projectid projectid, city, state_id, cast(d.Item_Value as datetime) completiondate from ipm_resume_item a, ipm_user b, ipm_project c left outer join IPM_PROJECT_FIELD_VALUE d on c.projectid = d.ProjectID and Item_ID = 21762571 where session_id = @sessionid and a.userid = @userid and a.userid = b.userid and a.projectid = c.projectid order by b.externaluserid asc, completiondate desc"
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim SessionIDParam As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
        Dim UserID As New SqlParameter("@userid", SqlDbType.VarChar, 255)
        SessionIDParam.Value = theSession
        UserID.Value = theUser
        MyCommand1.SelectCommand.Parameters.Add(SessionIDParam)
        MyCommand1.SelectCommand.Parameters.Add(UserID)


        Dim DT1 As New DataTable("resume")

        MyCommand1.Fill(DT1)
        Dim firstrow As DataRow


        If (DT1.Rows.Count = 0) Then
            'no projects path
            Dim sql4 As String = "select * from ipm_user where active = 'y' and userid = @userid"
            Dim userid4 As New SqlParameter("@userid", SqlDbType.VarChar, 255)
            userid4.Value = theUser
            Dim mycommand4 As SqlDataAdapter = New SqlDataAdapter(sql4, MyConnection)
            mycommand4.SelectCommand.Parameters.Add(userid4)
            Dim dt4 As New DataTable("resume")
            mycommand4.Fill(dt4)
            firstrow = dt4.Rows(0)
            repeaterProjects.Visible = False
            projectslisted = False
        Else
            firstrow = DT1.Rows(0)
            repeaterProjects.DataSource = DT1
            repeaterProjects.DataBind()
        End If

        labelName.Text = firstrow("firstname") & " " & firstrow("lastname")
        labelTitle.Text = firstrow("position").ToString.ToUpper()
        Try
            labelCareer.Text = firstrow("bio") & "<br />"
        Catch ex As Exception

        End Try

        'education
        'hard code: undergrad, grad, postgrad 21767697,21767699,21767700
        Dim sql2 As String = "select * from ipm_user_field_value where user_id = @userid and item_id in (21767697,21767699,21767700) order by item_id"
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)
        'hard code: award: 21600162


        Dim UserID2 As New SqlParameter("@userid", SqlDbType.VarChar, 255)

        UserID2.Value = theUser

        MyCommand2.SelectCommand.Parameters.Add(UserID2)


        Dim DT2 As New DataTable("education")


        MyCommand2.Fill(DT2)



        Dim educationFlag As Boolean = False
        If (DT2.Rows.Count > 0) Then

            For Each x As DataRow In DT2.Rows

                If (x("item_value") <> "") Then
                    If (educationFlag = False) Then
                        educationFlag = True
                    End If
                    labelEducation.Text = labelEducation.Text & x("item_value") & "<br />"
                End If


            Next
            If labelEducation.Text <> "" Then
                labelEducationTitle.Text = "<div class=subheader2>EDUCATION</div>"
            Else
                labelEducationTitle.Text = ""
            End If

        Else
            labelEducation.Text = ""
        End If


        Dim sql3 As String = "select * from ipm_user_field_value where user_id = @userid and item_id = 21767701 order by item_id" 'relevant experience
        Dim MyCommand3 As SqlDataAdapter = New SqlDataAdapter(sql3, MyConnection)


        Dim UserID3 As New SqlParameter("@userid", SqlDbType.VarChar, 255)

        UserID3.Value = theUser

        MyCommand3.SelectCommand.Parameters.Add(UserID3)


        Dim DT3 As New DataTable("education")


        MyCommand3.Fill(DT3)

        If (DT3.Rows.Count > 0) Then
            If (Not IsDBNull(DT3.Rows(0)("item_value"))) Then
                If (DT3.Rows(0)("item_value") <> "") Then
                    labelExperience.Text = Server.HtmlDecode(DT3.Rows(0)("item_value"))
                    labelExperienceTitle.Text = "<div class=subheader2>RELEVANT EXPERIENCE</div>"
                End If

            End If


        End If

    End Sub


    Public Sub RepeaterProjects_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        'for each row, get the construction value and project short description
        If (e.Item.ItemType = ListItemType.Item) Then 'Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))

            'const contract value
            Dim sql As String = "select * from ipm_project a left join ipm_project_field_value b on a.projectid = b.projectid where available = 'y' and b.item_id = 21762574 and a.projectid = @projectid"
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

            Dim projectID As New SqlParameter("@projectid", SqlDbType.VarChar, 255)

            MyCommand1.SelectCommand.Parameters.Add(projectID)

            MyCommand1.SelectCommand.Parameters("@projectid").Value = e.Item.DataItem("projectid")


            Dim DT1 As New DataTable("resume")

            MyCommand1.Fill(DT1)
            If (DT1.Rows.Count > 0) Then
                Dim labelValue As Label = e.Item.FindControl("labelValue")
                Dim theValue As Integer = 0

                Try
                    theValue = Integer.Parse(DT1.Rows(0)("item_value").ToString.Replace(",", ""))
                    theValue = Math.Round(theValue / 1000000)
                    labelValue.Text = "$" & theValue.ToString() & " million | " '& DT1.Rows(0)("description")

                Catch ex As Exception
                    labelValue.Text = "" 'DT1.Rows(0)("description")
                End Try
            End If
        End If





    End Sub

End Class
