﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class ResumeHTMLControl
    Inherits System.Web.UI.UserControl
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim theSession As String = Request.QueryString("s")
        Dim theUser As String = Request.QueryString("uid")

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))

        Dim sql As String = "select b.lastname lastname, b.firstname firstname, a.type type, a.description description, c.name name, bio, agency, email, phone, position, c.projectid from ipm_resume_item a, ipm_user b, ipm_project c where session_id = @sessionid and a.userid = @userid and a.userid = b.userid and a.projectid = c.projectid and b.active = 'y'"
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim SessionIDParam As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
        Dim UserID As New SqlParameter("@userid", SqlDbType.VarChar, 255)
        SessionIDParam.Value = theSession
        UserID.Value = theUser
        MyCommand1.SelectCommand.Parameters.Add(SessionIDParam)
        MyCommand1.SelectCommand.Parameters.Add(UserID)


        Dim DT1 As New DataTable("resume")

        MyCommand1.Fill(DT1)

      
        Dim firstrow As DataRow

        If (DT1.Rows.Count = 0) Then
            'no projects path
            Dim sql4 As String = "select * from ipm_user where active = 'y' and userid = @userid"
            Dim userid4 As New SqlParameter("@userid", SqlDbType.VarChar, 255)
            userid4.Value = theUser
            Dim mycommand4 As SqlDataAdapter = New SqlDataAdapter(sql4, MyConnection)
            mycommand4.SelectCommand.Parameters.Add(userid4)
            Dim dt4 As New DataTable("resume")
            mycommand4.Fill(dt4)
            firstrow = dt4.Rows(0)
            userExperience.Visible = False
        Else
            firstrow = DT1.Rows(0)

            userExperience.DataSource = DT1
            userExperience.DataBind()
        End If



        userName.Text = firstrow("firstname") & " " & firstrow("lastname")
        try 
		userTitle.Text = firstrow("position")
	 Catch ex As Exception
	end try
try
        userCompany.Text = firstrow("agency")
 Catch ex As Exception
	end try
        Dim contactphone As Integer = 0
        Dim contactemail As Integer = 0

        If (Not IsDBNull(firstrow("phone"))) Then
            If (firstrow("phone") <> "") Then
                contactphone = 1
            End If
        End If
        If (Not IsDBNull(firstrow("email"))) Then
            If (firstrow("email") <> "") Then
                contactemail = 1
            End If
        End If

        If (contactphone = 1 Or contactemail = 1) Then
            literalContactTitle.Text = "CONTACT"
            If (contactphone = 1 And contactemail = 1) Then
                labelContact.Text = firstrow("phone") & "<br />" & firstrow("email")
            ElseIf (contactphone = 1) Then
                labelContact.Text = firstrow("phone")
            ElseIf (contactemail = 1) Then
                labelContact.Text = firstrow("email")
            End If
        End If

       
try

        labelBio.Text = firstrow("bio")
 Catch ex As Exception
	end try
        'education
        'hard code: undergrad, grad, postgrad 21767697,21767699,21767700
        Dim sql2 As String = "select * from ipm_user_field_value where user_id = @userid and item_id in (21767697,21767699,21767700) order by item_id"
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)
        'hard code: award: 21600162
        Dim sql3 As String = "select * from ipm_user_field_value where user_id = @userid and item_id = 21767698"
        Dim MyCommand3 As SqlDataAdapter = New SqlDataAdapter(sql3, MyConnection)
        Dim UserID2 As New SqlParameter("@userid", SqlDbType.VarChar, 255)
        Dim UserID3 As New SqlParameter("@userid", SqlDbType.VarChar, 255)
        UserID2.Value = theUser
        UserID3.Value = theUser
        MyCommand2.SelectCommand.Parameters.Add(UserID2)
        MyCommand3.SelectCommand.Parameters.Add(UserID3)

        Dim DT2 As New DataTable("education")
        Dim DT3 As New DataTable("awards")

        MyCommand2.Fill(DT2)
        MyCommand3.Fill(DT3)

        If (DT3.Rows.Count > 0) Then

            If (DT3.Rows(0)("item_value") <> "") Then
                labelAwardsTitle.Text = "<div class=""subheader2"">ASSOCIATIONS, AFFILIATIONS, AWARDS</div>"
                labelAwards.Text = "<div class=""subheader3"">" & Replace(DT3.Rows(0)("item_value"), vbCrLf, "<br /></div><div class=""subheader3"">") & "</div>"

            End If

        End If
        Dim educationFlag As Boolean = False
        If (DT2.Rows.Count > 0) Then

            For Each x As DataRow In DT2.Rows

                If (x("item_value") <> "") Then
                    If (educationFlag = False) Then
                        labelEducationTitle.Text = "<div class=""subheader2"">EDUCATION</div>"
                        educationFlag = True
                    End If
                    labelEducation.Text = labelEducation.Text & "<div class=""subheader3"" style=""color:#000000"">"
                    labelEducation.Text = labelEducation.Text & x("item_value")
                    labelEducation.Text = labelEducation.Text & "</div>"
                End If


            Next

        End If



    End Sub

    Public Sub RepeaterProjects_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        'for each row, get the construction value and project short description
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))

            'const contract value
            Dim sql As String = "select * from ipm_project a left join ipm_project_field_value b on a.projectid = b.projectid where available = 'y' and b.item_id = 21762574 and a.projectid = @projectid"
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

            Dim projectID As New SqlParameter("@projectid", SqlDbType.VarChar, 255)

            MyCommand1.SelectCommand.Parameters.Add(projectID)

            MyCommand1.SelectCommand.Parameters("@projectid").Value = e.Item.DataItem("projectid")


            Dim DT1 As New DataTable("resume")

            MyCommand1.Fill(DT1)
            If (DT1.Rows.Count > 0) Then
                Dim labelValue As Label = e.Item.FindControl("labelValue")
                Dim theValue As Integer = 0

                Try
                    theValue = Integer.Parse(DT1.Rows(0)("item_value").ToString.Replace(",", ""))
                    theValue = Math.Round(theValue / 1000000)
                    labelValue.Text = "$" & theValue.ToString() & " million, " '& DT1.Rows(0)("description")

                Catch ex As Exception
                    labelValue.Text = "" 'DT1.Rows(0)("description")
                End Try
            End If
        End If





    End Sub
End Class
