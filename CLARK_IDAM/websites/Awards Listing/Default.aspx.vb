﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text

Imports WebSupergoo.ABCpdf7
Imports WebSupergoo.ABCpdf7.Objects
Imports WebSupergoo.ABCpdf7.Atoms
Imports WebSupergoo.ABCpdf7.Operations

Partial Class _Default
    Inherits System.Web.UI.Page
    Public rsAwardsTypes As DataTable
    Public rsOrganizationName As DataTable
    Public rsRelatedProjects As DataTable
    Public rsAwardsYears As DataTable
    Public rsAwardsFilters As DataTable
    Public rsAwardsCarousels As DataTable

    Function getDataTable(sql As String) As DataTable
        Dim dt1 As New DataTable
        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        MyCommand1.Fill(dt1)
        Return dt1
    End Function


    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim Sql As String = "select * from ipm_awards_type order by type_id"
        rsAwardsTypes = getDataTable(Sql)

        Sql = "select distinct item_value from ipm_awards_field_desc a,ipm_awards_field_value b where a.item_tag = 'IDAM_AWARDS_ORGANIZATION_NAME' AND a.item_id = b.item_id and active = 1 and viewable = 1 and item_value <> '' order by item_value"
        rsOrganizationName = getDataTable(Sql)

        Sql = "select distinct ipm_project.projectid,name from ipm_awards_related_projects,ipm_project where ipm_awards_related_projects.projectid = ipm_project.projectid and available = 'y' order by name"
        rsRelatedProjects = getDataTable(Sql)

        Sql = "select distinct year(post_date) awardsyears from ipm_awards order by awardsyears desc"
        rsAwardsYears = getDataTable(Sql)

        Sql = "select * from ipm_awards_field_desc where item_tag = 'IDAM_AWARDS_FILTERS' and active = 1"
        rsAwardsFilters = getDataTable(Sql)
       
        'populate carousel dropdown
        'ddCustomizedCarousel
        Sql = " select a.* From IPM_CARROUSEL a, IPM_CARROUSEL_SERVICES b where a.Carrousel_ID = b.Carrousel_ID  and a.Available = 'Y' and a.Active = 1 and KeyID in (select KeyID from IPM_SERVICES where KeyName = 'Award')"
        rsAwardsCarousels = getDataTable(Sql)
        ddCustomizedCarousel.DataSource = rsAwardsCarousels
        ddCustomizedCarousel.DataBind()




        If IsPostBack Then
           

            'Pre-set parameters
            Dim report_title As String = Request.Form("report_name")

            If (report_title = "") Then
                report_title = "Awards Report"
            End If

            Dim checkboxCustomizedHeader As String = Request.Form("checkboxCustomizedHeader")
            Dim textboxCustomizedHeader As String = Request.Form("textboxCustomizedHeader")
            If (textboxCustomizedHeader = "") Then
                textboxCustomizedHeader = "Recent Awards and Accolades"
            End If

            Dim checkboxCustomizedTitle As String = Request.Form("checkboxCustomizedTitle")
            Dim textboxCustomizedTitle As String = Request.Form("textboxCustomizedTitle")
            If (textboxCustomizedTitle = "") Then
                textboxCustomizedTitle = "RECOGNITION"
            End If

            Dim checkboxCustomizedLogo As String = Request.Form("checkboxCustomizedLogo")
            Dim dropdownCustomizedLogo As String = Request.Form("dropdownCustomizedLogo")
            If (dropdownCustomizedLogo = "") Then
                dropdownCustomizedLogo = ""
            End If


            Dim checkboxLetterhead As String = Request.Form("checkboxLetterhead")


            Dim keywords = prep_sql(Request.Form("keywords"))
            Dim awardsType = prep_sql(Request.Form("awards_type"))
            Dim orgName = prep_sql(Request.Form("org_name"))
            Dim relatedProject = prep_sql(Request.Form("related_project"))
            Dim awardsYear = prep_sql(Request.Form("awards_years"))
            Dim usec = prep_sql(Request.Form("checkboxCustomizedCarousel"))
            Dim cid = prep_sql(Request.Form("ddCustomizedCarousel"))

            Dim filterlist As String = prep_sql(Request.Form("chkbk_filter"))
            Dim filters As String()
            Dim filterssql As String = ""
            If Not filterlist Is Nothing Then
                filters = filterlist.ToString.Split(",")
                If filters.Length > 0 Then
                    For i = 0 To filters.Length - 1
                        filterssql += "and a.Awards_Id in (select AWARDS_ID from IPM_AWARDS_FIELD_VALUE where Item_ID in (select Item_ID from IPM_AWARDS_FIELD_DESC where item_tag = 'idam_awards_filters') and Item_Value like '%" & filters(i) & "%')"
                    Next
                End If
            End If


            Dim report_output = prep_sql(Request.Form("search_output"))
            Dim report_display = prep_sql(Request.Form("search_display"))

            Dim keywords_sql, awardstype_sql, orgname_sql, relatedProject_sql, awards_sql As String
            If (keywords <> "") Then
                keywords_sql = " AND (headline like '%" & keywords & "%' OR publicationtitle like '%" & keywords & "%' OR content like '%" & keywords & "%' OR name like '%" & keywords & "%' OR e.item_value like '%" & keywords & "%' OR g.item_value like '%" & keywords & "%' OR i.item_value like '%" & keywords & "%')"
            End If

            If (Not awardsType Is Nothing) Then
                awardstype_sql = " AND type = " & awardsType
            End If

            If (Not orgName Is Nothing) Then
                orgname_sql = " AND g.item_value = '" & orgName & "'"
            End If

            If (Not relatedProject Is Nothing) Then
                relatedProject_sql = " AND c.projectid = " & relatedProject
            End If

            If (Not awardsYear Is Nothing) Then
                awards_sql = " AND year(post_date) = " & awardsYear
            End If

            'handle filters
           


            Sql = "select distinct a.awards_id,headline,publicationtitle,e.item_value awards_level,post_date,pull_date,pdf,pdflinktitle,g.item_value org_name,k.item_value org_url,m.item_value rec_url,i.item_value rec_name from ipm_awards_field_desc d,ipm_awards_field_value e,ipm_awards_field_desc f,ipm_awards_field_value g,ipm_awards_field_desc h,ipm_awards_field_value i, ipm_awards_field_desc j, ipm_awards_field_value k, ipm_awards_field_desc l, ipm_awards_field_value m,ipm_awards a left join ipm_awards_related_projects b on a.awards_id = b.awards_id left join ipm_project c on b.projectid = c.projectid where a.show = 1 and d.item_tag = 'IDAM_AWARDS_Level' and f.item_tag = 'IDAM_AWARDS_ORGANIZATION_NAME' and h.item_tag = 'IDAM_AWARDS_RECIPIENT_NAME' and d.item_id = e.item_id and f.item_id = g.item_id and h.item_id = i.item_id and e.awards_id = a.awards_id and g.awards_id = a.awards_id and i.awards_id = a.awards_id and j.item_id = k.item_id and l.item_id = m.item_id and j.item_tag = 'IDAM_ORGANIZATION_URL' and l.item_tag = 'IDAM_AWARDS_RECIPIENT_URL' and k.awards_id = a.awards_id and m.awards_id = a.awards_id " & keywords_sql & awardstype_sql & orgname_sql & relatedProject_sql & awards_sql & filterssql & " order by post_date desc"
            GeneratePDF(3, Sql, checkboxLetterhead, checkboxLetterhead, textboxCustomizedHeader, checkboxCustomizedHeader, textboxCustomizedTitle, checkboxCustomizedTitle, dropdownCustomizedLogo, checkboxCustomizedLogo, usec, cid)
        End If

    End Sub
    Function prep_sql(sql_string)
        If (IsNumeric(sql_string) = True) Then
            sql_string = CLng(sql_string)
        Else
            sql_string = replace(sql_string, "'", "''")
        End If
        prep_sql = sql_string
    End Function

    Public Sub CheckLogin()
        If ConfigurationSettings.AppSettings("UseSecurityOverride") Is Nothing Then
            If (InStr(Request.ServerVariables("SCRIPT_NAME"), "login.aspx") = 0) Then
                If (Session("login") <> "1") Then
                    Response.Redirect("Login.aspx?ref=" & Request.ServerVariables("script_name"))
                    Response.End()
                End If
            End If
        Else
            Session("UserID") = "1"
        End If

    End Sub

    Public Function DeleteResumeItem(ByVal userid As String) As Boolean
        Dim SessionID As String = Session.SessionID
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim sql As String = "delete from ipm_resume_item where userid = @userid and session_id = @sessionid"
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim SessionIDParam As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
        Dim UserIDParam As New SqlParameter("@userid", SqlDbType.VarChar, 255)
        Dim ProjectIDParam As New SqlParameter("@projectid", SqlDbType.VarChar, 255)

        MyCommand1.SelectCommand.Parameters.Add(SessionIDParam)
        MyCommand1.SelectCommand.Parameters("@sessionid").Value = SessionID

        MyCommand1.SelectCommand.Parameters.Add(UserIDParam)
        MyCommand1.SelectCommand.Parameters("@userid").Value = userid

        Dim dt1 As New DataTable("delete")

        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function DeleteResumeItem(ByVal userid As String, ByVal projectid As String) As Boolean
        Dim SessionID As String = Session.SessionID
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim sql As String = "delete from ipm_resume_item where projectid = @projectid and userid = @userid and session_id = @sessionid"
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim SessionIDParam As New SqlParameter("@sessionid", SqlDbType.VarChar, 255)
        Dim UserIDParam As New SqlParameter("@userid", SqlDbType.VarChar, 255)
        Dim ProjectIDParam As New SqlParameter("@projectid", SqlDbType.VarChar, 255)

        MyCommand1.SelectCommand.Parameters.Add(SessionIDParam)
        MyCommand1.SelectCommand.Parameters("@sessionid").Value = SessionID

        MyCommand1.SelectCommand.Parameters.Add(UserIDParam)
        MyCommand1.SelectCommand.Parameters("@userid").Value = userid

        MyCommand1.SelectCommand.Parameters.Add(ProjectIDParam)
        MyCommand1.SelectCommand.Parameters("@projectid").Value = projectid

        Dim dt1 As New DataTable("delete")

        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then
            Return True
        Else
            Return False
        End If

    End Function




    Public Function GetReport(ByVal url As String) As Doc
        Dim theDoc As New Doc

        theDoc.HtmlOptions.ContentCount = 10 ' Otherwise the page will be assumed to be invalid.
        theDoc.HtmlOptions.RetryCount = 10 ' Try to obtain html page 10 times
        theDoc.HtmlOptions.Timeout = 180000 ' The page must be obtained in less then 10 seconds

        theDoc.Rect.Inset(0, 10) ' set up document
        theDoc.Rect.Position(5, 15)
        theDoc.Rect.Width = 602
        theDoc.Rect.Height = 767
        theDoc.HtmlOptions.PageCacheEnabled = False
        Dim theId As Integer = theDoc.AddImageUrl(url, True, 0, True)

        While (theDoc.Chainable(theId))

            theDoc.Page = theDoc.AddPage()
            theId = theDoc.AddImageToChain(theId)
        End While

        'Flatten document

        For i = 1 To theDoc.PageCount
            theDoc.PageNumber = i
            theDoc.Flatten()
            i += 1
        Next
        Return theDoc
    End Function



    Public Sub GeneratePDF(ByVal resumetype As Integer, ByVal sql2 As String, ByVal report_letterhead As String, ByVal checkboxLetterheadChecked As String, ByVal reportheader As String, ByVal checkboxCustomizedHeaderChecked As String, ByVal reporttitle As String, ByVal checkboxCustomizedTitleChecked As String, ByVal dropdownCustomizedLogo As String, ByVal checkboxCustomizedLogoChecked As String, ByVal usec As String, ByVal cid As String)


        Dim adjustX As String = 0 'head_x.SelectedValue
        Dim adjustY As String = 0 'head_y.SelectedValue

        Dim SessionID As String = Session.SessionID
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        'Dim sql2 As String = "select distinct userid,session_id from ipm_resume_item where session_id = @sessionid and userid <> ''"
        'Dim sql2 As String = "select distinct a.userid,session_id,case rtrim(isnull(b.externaluserid,99)) when '' then 99 else rtrim(isnull(b.externaluserid,99)) end employeenum from ipm_resume_item a, ipm_user b where a.userid = b.userid and session_id = @sessionid and a.userid <> '' order by employeenum asc"
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)
        Dim DT2 As New DataTable("SelectedTable")
        MyCommand2.Fill(DT2)
        Dim theDoc As Doc = New Doc()
        theDoc.HtmlOptions.Timeout = 120000
        theDoc.HtmlOptions.PageCacheClear()
        theDoc.HtmlOptions.PageCacheEnabled = False
        theDoc.HtmlOptions.PageCachePurge()
        theDoc.HtmlOptions.BrowserWidth = 750

        theDoc.HtmlOptions.ImageQuality = 100

        theDoc.HtmlOptions.FontEmbed = True
        theDoc.HtmlOptions.FontProtection = False
        theDoc.HtmlOptions.FontSubstitute = False
        Dim windir As String = Environment.GetEnvironmentVariable("windir") & "\Fonts\"
        Call theDoc.EmbedFont(windir & "TT0017M.TTF", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "tt0015m.ttf", "Latin", False, True, True)
        Call theDoc.EmbedFont(windir & "TT0016M.TTF", "Latin", False, True, True)


        theDoc.Rect.Height = theDoc.Rect.Height - 50
        theDoc.Rect.Top = theDoc.Rect.Top + 50
        theDoc.Rect.Bottom = theDoc.Rect.Bottom + 50

        Dim oriW As Integer = theDoc.Rect.Width
        Dim oriH As Integer = theDoc.Rect.Height

        Dim oriX As Integer = theDoc.Rect.Left
        Dim oriY As Integer = theDoc.Rect.Bottom

        Dim theUrl As String = ""



        If Not checkboxLetterheadChecked Is Nothing Then
            If (checkboxLetterheadChecked = "on") Then
                report_letterhead = "1"
            Else
                report_letterhead = "0"
            End If
        End If

        If Not checkboxCustomizedHeaderChecked Is Nothing Then
            If (checkboxCustomizedHeaderChecked = "on") Then

            Else
                reportheader = ""
            End If
        End If

        If Not checkboxCustomizedTitleChecked Is Nothing Then
            If (checkboxCustomizedTitleChecked = "on") Then

            Else
                reporttitle = "Meet Our People"
            End If
        End If

        Dim reportnopagebreak As String = ""

        reportnopagebreak = "0"



        Dim i As Integer = 1
        Dim pageCount As Integer = 1
        Dim theID As Integer

        Dim logoFile As String = ConfigurationSettings.AppSettings("imagepath") & ConfigurationSettings.AppSettings("logofile")

        Dim headerFile As String = ConfigurationSettings.AppSettings("imagepath")


        If Not checkboxLetterheadChecked Is Nothing Then
            If (checkboxLetterheadChecked = "on") Then
                If (dropdownCustomizedLogo = "CBG") Then
                    headerFile = headerFile & "data_header_people_cbg.jpg"
                ElseIf (dropdownCustomizedLogo = "CRC") Then
                    headerFile = headerFile & "data_header_people_crc.jpg"
                ElseIf (dropdownCustomizedLogo = "CR") Then
                    headerFile = headerFile & "data_header_people_cr.jpg"
                ElseIf (dropdownCustomizedLogo = "CRM") Then
                    headerFile = headerFile & "data_header_people_crm.jpg"
                ElseIf (dropdownCustomizedLogo = "DFLLC") Then
                    headerFile = headerFile & "data_header_people_dfllc.jpg"

                ElseIf (dropdownCustomizedLogo = "") Then
                    headerFile = headerFile & "data_header_people_plain.jpg"
                End If
            Else
                headerFile = headerFile & "data_header_people_plain.jpg"
            End If
        End If


        
        theUrl = ConfigurationSettings.AppSettings("pdfurl") & "ResumeHTML.aspx?s=" & Server.UrlEncode(sql2) & "&type=" & resumetype & "&report=" & reportheader & "&letterhead=" & report_letterhead & "&title=" & reporttitle & "&x=" & adjustX & "&y=" & adjustY & "&pb=" & reportnopagebreak & "&usec=" & usec & "&cid=" & cid
        'Response.Write(theURL)

        ''Response.Redirect(theUrl)
        ''Response.End()

        If (i > 1) Then


            theDoc.Rect.Resize(oriW, oriH)
            theDoc.Rect.Position(oriX, oriY)

            theDoc.Transform.Reset()
            theDoc.Page = theDoc.AddPage()
        End If
        theDoc.SetInfo(0, "HostWebBrowser", "0")

        theID = theDoc.AddImageUrl(theUrl)


        Do
            If Not theDoc.Chainable(theID) Then
                Exit Do
            End If

            If (report_letterhead = "1") Then

                theDoc.Rect.Resize(612, 92)
                theDoc.Rect.Move(0, 650)
                If Not (pageCount Mod 2 = 0) Then
                    theDoc.AddImageFile(headerFile, 1)
                Else
                    theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "data_header_plain.jpg", 1)
                End If

            End If

            If Not (pageCount Mod 2 = 0) Then
                theDoc.Transform.Rotate(90, 595, 20)
                If (report_letterhead = "1") Then
                    theDoc.Rect.Position(612, 33)
                    theDoc.Rect.Resize(15, 15)
                    theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "corner.jpg", 1)
                End If
                theDoc.Rect.Position(627, 21)
                theDoc.Rect.Resize(300, 20)
                theDoc.AddHtml("&nbsp; <font size=""2"" face=""Zapf Humanist 601 BT"" color=""#5a471c"">" & reportheader & "</font>")
            Else
                theDoc.Transform.Rotate(90, 32, 20)
                If (report_letterhead = "1") Then
                    theDoc.Rect.Position(52, 7)
                    theDoc.Rect.Resize(15, 15)
                    theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "corner_2.jpg", 1)
                End If
                theDoc.Rect.Position(67, 4)
                theDoc.Rect.Resize(300, 20)
                theDoc.AddHtml("&nbsp; <font size=""2"" face=""Zapf Humanist 601 BT"" color=""#5a471c"">" & reportheader & "</font>")
            End If

            theDoc.Rect.Resize(oriW, oriH - 120)
            theDoc.Rect.Position(oriX, 50)

            theDoc.Transform.Reset()

            theDoc.Page = theDoc.AddPage()
            theID = theDoc.AddImageToChain(theID)

            pageCount = pageCount + 1

        Loop

        If Not theDoc.Chainable(theID) Then

            If (report_letterhead = "1") Then

                theDoc.Rect.Resize(612, 92)
                theDoc.Rect.Move(0, 700)

                If Not (pageCount Mod 2 = 0) Then
                    theDoc.AddImageFile(headerFile, 1)
                Else
                    theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "data_header_plain.jpg", 1)
                End If

            End If

            If Not (pageCount Mod 2 = 0) Then
                theDoc.Transform.Rotate(90, 595, 20)
                If (report_letterhead = "1") Then
                    theDoc.Rect.Position(612, 33)
                    theDoc.Rect.Resize(15, 15)
                    theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "corner.jpg", 1)
                End If
                theDoc.Rect.Position(627, 21)
                theDoc.Rect.Resize(300, 20)
                theDoc.AddHtml("&nbsp; <font size=""2"" face=""Zapf Humanist 601 BT"" color=""#5a471c"">" & reportheader & "</font>")
            Else
                theDoc.Transform.Rotate(90, 32, 20)
                If (report_letterhead = "1") Then
                    theDoc.Rect.Position(52, 7)
                    theDoc.Rect.Resize(15, 15)
                    theDoc.AddImageFile(ConfigurationSettings.AppSettings("imagepath") & "corner_2.jpg", 1)
                End If
                theDoc.Rect.Position(67, 4)
                theDoc.Rect.Resize(300, 20)
                theDoc.AddHtml("&nbsp; <font size=""2"" face=""Zapf Humanist 601 BT"" color=""#5a471c"">" & reportheader & "</font>")
            End If

            pageCount = pageCount + 1
        End If
        i = i + 1




        For i = 1 To theDoc.PageCount
            theDoc.PageNumber = i
            theDoc.Flatten()
        Next

        Dim theDate As String = MonthName(DatePart("m", Date.Now)) & "-" & DatePart("d", Date.Now) & "_-_" & DatePart("h", Date.Now) & "-" & DatePart("n", Date.Now) & "-" & DatePart("s", Date.Now)
        Dim theProjectName As String = "Awards Listing" & "_-_" & theDate & ".pdf"

        theDoc.Save(ConfigurationSettings.AppSettings("pdfpath") & theProjectName)

        Dim stream As System.IO.FileStream

        stream = New FileStream(ConfigurationSettings.AppSettings("pdfpath") & theProjectName, FileMode.Open)
        Dim filesize As Integer = stream.Length

        Response.Clear()
        Response.ContentType = "application/pdf"
        Response.AddHeader("Content-Description", "File Transfer")
        Response.AddHeader("Content-Disposition", "attachment; filename=" & theProjectName)
        Response.AddHeader("Content-Length", filesize)

        Dim b(8192) As Byte

        Do While stream.Read(b, 0, b.Length) > 0
            Response.BinaryWrite(b)
            Response.Flush()
        Loop

        stream.Close()
        Response.End()

    End Sub

    

    Public Function GetProjectDescription(ByVal projectID As String) As String

        Dim projectDescription As String

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim sql2 As String = "select description from ipm_project where projectid = @projectid and available = 'y'"
        Dim projectIDParam As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        projectIDParam.Value = projectID
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)

        MyCommand2.SelectCommand.Parameters.Add(projectIDParam)

        Dim dt1 As New DataTable
        MyCommand2.Fill(dt1)

        projectDescription = Trim(dt1.Rows(0)("description"))

        Return projectDescription

    End Function

    Public Function GetUserName(ByVal userID As String) As String

        Dim userName As String

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim sql2 As String = "select firstname, lastname from ipm_user where userid = @userid and active = 'y'"
        Dim projectIDParam As New SqlParameter("@userid", SqlDbType.VarChar, 255)
        projectIDParam.Value = userID
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)

        MyCommand2.SelectCommand.Parameters.Add(projectIDParam)

        Dim dt1 As New DataTable
        MyCommand2.Fill(dt1)

        userName = Trim(dt1.Rows(0)("firstname")) & " " & Trim(dt1.Rows(0)("lastname"))

        Return userName

    End Function

    Public Function GetProjects(ByVal userID As String) As DataTable


        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim sql2 As String = "select a.projectid projectid,b.description description from ipm_project_contact a, ipm_project b  where a.userid = @userid and a.projectid = b.projectid and b.available = 'y'"
        Dim projectIDParam As New SqlParameter("@userid", SqlDbType.VarChar, 255)
        projectIDParam.Value = userID
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)

        MyCommand2.SelectCommand.Parameters.Add(projectIDParam)

        Dim dt1 As New DataTable
        MyCommand2.Fill(dt1)

        Return dt1

    End Function

End Class
