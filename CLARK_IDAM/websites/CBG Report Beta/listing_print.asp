<!--#include file="includes\config.asp" -->
<%


Class Table ' only needed in ASP

Private mDoc
Private mRect
Private mRectTop
Private mRectLeft
Private mRectWidth
Private mRectHeight
Private mRowTop
Private mRowBottom
Private mHeights()
Private mXPos
Private mYPos
Private mColumns
Private mWidths()
Private mObjects
Private mTruncated
Public RowHeightMin
Public RowHeightMax
Public Padding

' Focus on the document and assign the relevant number of columns
Public Sub Focus(inDoc, inColumns)
  Set mDoc = inDoc
  SetRect mDoc.Rect
  SetColumns inColumns
End Sub

' Add a new page, reset the table rect and move to the first row
Public Sub NewPage()
  mDoc.Page = mDoc.AddPage
  SetRect mRect
  NextRow
End Sub

' Assign a new table rectangle and reset the current table position
Public Sub SetRect(inRect)
  mDoc.Rect = inRect
  mRect = mDoc.Rect
  mRectTop = mDoc.Rect.Top
  mRectLeft = mDoc.Rect.Left
  mRectWidth = mDoc.Rect.Width
  mRectHeight = mDoc.Rect.Height
  mRowTop = mDoc.Rect.Top
  mRowBottom = mDoc.Rect.Top
  ReDim mHeights(0)
  mYPos = -1
  mXPos = -1
End Sub

' Change the number of columns in the table
Public Sub SetColumns(inNum)
  Dim i
  If inNum > 0 Then
    ReDim Preserve mWidths(inNum - 1)
    For i = (mColumns - 1) To (inNum - 1)
      If i >= 0 Then mWidths(i) = 1
    Next
    mColumns = inNum
  End If
End Sub

' Get the current row - a zero based index
Public Property Get Row()
  Row = mYPos
End Property

' Get the current column - a zero based index
Public Property Get Column()
  Column = mXPos
End Property

' Find out if the last row we added was truncated
Public Property Get RowTruncated()
  RowTruncated = mTruncated
End Property

' Change a column width
Public Property Let Width(i, inWidth)
  If i >= 0 And i <= UBound(mWidths) Then mWidths(i) = inWidth
End Property

' Move to the next column in the current row
Public Sub NextCell()
  mXPos = mXPos + 1
  If mXPos >= mColumns Then mXPos = mColumns - 1
  If mXPos < 0 Then mXPos = 0
  SelectCurrentCell mXPos
End Sub

' Move to the next row - return false if the next row would not fit
Public Function NextRow()
  Dim theBottom
  mRowTop = mRowTop - GetRowHeight(mYPos)
  mDoc.Rect = mRect
  mRowBottom = mRowTop
  mDoc.Rect.Top = mRowTop
  If RowHeightMax > 0 Then
    theBottom = mRowTop - RowHeightMax
    If mDoc.Rect.Bottom < theBottom Then mDoc.Rect.Bottom = theBottom
  End If
  NextRow = (mDoc.Rect.Height > RowHeightMin + (2 * Padding))
  If NextRow Then
    mYPos = mYPos + 1
    mXPos = -1
    mObjects = ""
    mTruncated = False
  End If
End Function

' Add text to the currently selected area
Public Function AddText(inText)
  Dim theRect
  Dim thePos
  theRect = mDoc.Rect
  mDoc.Rect.Inset Padding, Padding
  AddText = AddToRow(mDoc.AddText(inText))
  If mTruncated = False Then
    Dim theDrawn
    theDrawn = mDoc.GetInfo(AddText, "Characters")
    If theDrawn = "" Then theDrawn = 0
    If CInt(theDrawn) < Len(inText) Then
      mTruncated = True
    End If
  End If
  thePos = mDoc.Pos.Y - mDoc.FontSize
  If thePos < mRowBottom Then mRowBottom = thePos
  mDoc.Rect = theRect
End Function

' Select the entire table area
Public Sub SelectTable()
  mDoc.Rect = mRect
End Sub

' Select a cell in the current row using a zero based index
Public Sub SelectCell(inIndex)
  GetRowHeight mYPos ' fix the current row height
  SelectCells inIndex, mYPos, inIndex, mYPos
End Sub

' Select a row on the current page using a zero based index
Public Sub SelectRow(inIndex)
  GetRowHeight mYPos ' fix the current row height
  SelectCells 0, inIndex, mColumns - 1, inIndex
End Sub

' Select a column on the current page using a zero based index
Public Sub SelectColumn(inIndex)
  GetRowHeight mYPos ' fix the current row height
  SelectCells inIndex, 0, inIndex, UBound(mHeights) - 1
End Sub

' Select a rectangular area of cells on the current page
Public Sub SelectCells(inX1, inY1, inX2, inY2)
  Dim theTop, theLeft, theTemp, i
  ' check inputs
  If inX1 > inX2 Then
    theTemp = inX1
    inX1 = inX2
    inX2 = theTemp
  End If
  If inY1 > inY2 Then
    theTemp = inX1
    inY1 = inY2
    inY2 = theTemp
  End If
  GetRowHeight mYPos ' fix the current row height
  If inY1 >= UBound(mHeights) Then inY1 = UBound(mHeights) - 1
  If inY2 >= UBound(mHeights) Then inY2 = UBound(mHeights) - 1
  If inY1 < 0 Then Exit Sub
  ' select the cells
  mDoc.Rect = mRect
  theTop = mDoc.Rect.Top
  SelectCurrentCell inX1
  theLeft = mDoc.Rect.Left
  SelectCurrentCell inX2
  mDoc.Rect.Top = theTop
  mDoc.Rect.Bottom = theTop
  mDoc.Rect.Left = theLeft
  For i = 0 To inY2
    mDoc.Rect.Bottom = mDoc.Rect.Bottom - mHeights(i)
    If inY1 > i Then mDoc.Rect.Top = mDoc.Rect.Top - mHeights(i)
  Next
End Sub

' Draw borders round the current selection
Public Sub Frame(inTop, inBott, inLeft, inRight)
  If inTop Then AddToRow mDoc.AddLine(mDoc.Rect.Left, mDoc.Rect.Top, mDoc.Rect.Right, mDoc.Rect.Top)
  If inBott Then AddToRow mDoc.AddLine(mDoc.Rect.Left, mDoc.Rect.Bottom, mDoc.Rect.Right, mDoc.Rect.Bottom)
  If inLeft Then AddToRow mDoc.AddLine(mDoc.Rect.Left, mDoc.Rect.Top, mDoc.Rect.Left, mDoc.Rect.Bottom)
  If inRight Then AddToRow mDoc.AddLine(mDoc.Rect.Right, mDoc.Rect.Top, mDoc.Rect.Right, mDoc.Rect.Bottom)
End Sub

' Color the background of the current selection
Public Sub Fill(inColor)
  Dim theLayer, theColor
  theLayer = mDoc.Layer
  theColor = mDoc.Color
  mDoc.Layer = mDoc.LayerCount + 1
  mDoc.Color = inColor
  AddToRow mDoc.FillRect
  mDoc.Color = theColor
  mDoc.Layer = theLayer
End Sub

' Get the current row height based on the cell contents drawn so far
Private Function GetRowHeight(inRow)
  If UBound(mHeights) <= inRow Then
    ' establish and store current row height
    Dim theHeight
    ReDim Preserve mHeights(inRow + 1)
    mRowBottom = mRowBottom - Padding
    If mRowBottom < mRectTop - mRectHeight Then mRowBottom = mRectTop - mRectHeight
    theHeight = mRowTop - mRowBottom
    If inRow > 0 And theHeight < RowHeightMin Then theHeight = RowHeightMin
    If RowHeightMax > 0 And theHeight > RowHeightMax Then theHeight = RowHeightMax
    mHeights(inRow) = theHeight
  End If
  GetRowHeight = 0
  If inRow >= 0 Then GetRowHeight = mHeights(inRow)
End Function

' Select the current cell
Private Sub SelectCurrentCell(inIndex)
  Dim thePos, theWidth, theTotal, i
  If inIndex >= 0 And inIndex < mColumns Then
    ' get the x offset and width of the cell
    For i = 0 To UBound(mWidths)
      theTotal = theTotal + mWidths(i)
      If i < inIndex Then thePos = thePos + mWidths(i)
    Next
    thePos = thePos * (mRectWidth / theTotal)
    theWidth = mWidths(inIndex) * (mRectWidth / theTotal)
    ' position the cell
    mDoc.Rect.Top = mRowTop
    mDoc.Rect.Left = mRectLeft + thePos
    mDoc.Rect.Width = theWidth
  End If
End Sub

' Add to our list of objects drawn as part of the current row
Private Function AddToRow(inID)
  mObjects = mObjects &"," &inID
  AddToRow = inID
End Function

' Delete all the objects drawn as part of the current row
Public Sub DeleteLastRow()
  Dim theArray
  Dim i
  theArray = Split(mObjects, ",")
  For i = 0 To UBound(theArray)
    If theArray(i) <> "" Then mDoc.Delete CInt(theArray(i))
  Next
  mObjects = ""
End Sub

End Class ' only needed in ASP




	if request.form("submit") = "1" then
	
	'Pre-set parameters
	report_title = Request.Form("report_name")
	
	report_name = prep_sql(Request.Form("search_name"))
	
	
	report_client = ""
	
	report_region = ""

	report_state = ""
	
	report_disc = ""
	
	
	client_flag = 0
	if(Request.Form("search_client").count > 0) then
		for i=1 to Request.Form("search_client").count
			if(client_flag = 0) then
				report_client = report_client & "'" & prep_sql(Request.Form("search_client")(i)) & "'"
				client_flag = 1
			else
				report_client = report_client & ", '" & prep_sql(Request.Form("search_client")(i)) & "'"
			end if
		next
	report_client = " AND c.clientname IN (" & report_client & " )"
	end if
	
	region_flag = 0
	if(Request.Form("search_region").count > 0) then
		for i=1 to Request.Form("search_region").count
			if(region_flag = 0) then
				report_region = report_region & "'" & prep_sql(Request.Form("search_region")(i)) & "'"
				region_flag = 1
			else
				report_region = report_region & ", '" & prep_sql(Request.Form("search_region")(i)) & "'"
			end if
		next
	report_region = " AND item_value IN (" & report_region & " )"
	end if

	state_flag = 0
	if(Request.Form("search_state").count > 0) then
		for i=1 to Request.Form("search_state").count
			if(state_flag = 0) then
				report_state = report_state & "'" & prep_sql(Request.Form("search_state")(i)) & "'"
				state_flag = 1
			else
				report_state = report_state & ", '" & prep_sql(Request.Form("search_state")(i)) & "'"
			end if
		next
	report_state = " AND c.state_id IN (" & report_state & " )"
	end if

	disc_flag = 0
	if(Request.Form("search_disc").count > 0) then
		for i=1 to Request.Form("search_disc").count
			if(disc_flag = 0) then
				report_disc = report_disc & "'" & prep_sql(Request.Form("search_disc")(i)) & "'"
				disc_flag = 1
			else
				report_disc = report_disc & ", '" & prep_sql(Request.Form("search_disc")(i)) & "'"
			end if
		next
	report_disc = " AND keyid IN (" & report_disc & " )"
	end if	
	
	
	if(Request.Form("search_favorites") = "0") then
	report_favorite = " AND c.favorite = 0"
	elseif(Request.Form("search_favorites") = "1") then
	report_favorite = " AND c.favorite = 1"
	else
	report_favorite = ""
	end if

	if(Request.Form("search_published") = "0") then
	report_published = " AND c.publish = 0"
	elseif(Request.Form("search_published") = "1") then
	report_published = " AND c.publish = 1"
	else
	report_published = ""
	end if
	
	
	'Big search filter
	set rsProjects = server.createobject("adodb.recordset")
	
	if(report_disc = "") then
	sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description from ipm_project_field_desc b, ipm_project c LEFT JOIN ipm_project_field_value a ON a.projectid = c.projectid WHERE a.item_id = b.item_id  AND item_tag = 'IDAM_PROJECT_REGION'  " & report_published & " " & report_favorite & report_region & report_name & report_client & report_state & " ORDER BY c.name"
	else
	sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description from ipm_project_field_desc b, ipm_project c LEFT JOIN ipm_project_field_value a ON a.projectid = c.projectid WHERE a.item_id = b.item_id  AND item_tag = 'IDAM_PROJECT_REGION'  " & report_published & " " & report_favorite & report_region & report_name  & report_client & report_state & " AND c.projectID IN (select ipm_project.projectid from ipm_project, ipm_project_discipline where ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 " & report_disc & " ) ORDER BY c.name"
	end if
	
	rsProjects.Open sql, Conn, 1, 4
	
	'Query for additional UDFs
	
	
	
	end if


	set rsArchitect=server.createobject("adodb.recordset")

	
	set rsComplete=server.createobject("adodb.recordset")

	
	set rsValue=server.createobject("adodb.recordset")
	

Set theDoc = Server.CreateObject("ABCpdf6.Doc")


theDoc.HtmlOptions.PageCacheClear

theDoc.HtmlOptions.FontProtection = False
theDoc.HtmlOptions.FontSubstitute = False
theDoc.HtmlOptions.FontEmbed = True

theFont =  theDoc.EmbedFont("C:\WINDOWS\Fonts\tt0015m_.ttf", "Unicode", false, true, true)
theDoc.Font = theFont

theText = ""
theText = theText & "<h3 align=""right"">PORTFOLIO</h3>"

theDoc.Rect.Inset 100, 0
theDoc.Color = "90 71 28"
theDoc.Width = 3

theDoc.addHTML theText

Set theTable = New Table
' focus the table on the document
theTable.Focus theDoc, 5
' first column has double width
					i = 0
					do while not rsProjects.eof
					
					if(rsComplete.State = 1) then
					rsComplete.close
					end if
					
					if(rsArchitect.State = 1) then
					rsArchitect.close
					end if
					
					if(rsValue.State = 1) then
					rsValue.close
					end if
				
					sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_COMPLETION'"
					rsComplete.Open sql, Conn, 1, 4

					sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_ARCHITECT'"
					rsArchitect.Open sql, Conn, 1, 4

					sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_CONSTRUCTIONVALUE'"
					rsValue.Open sql, Conn, 1, 4

					

					'theText = theText & "<img src=""images/pdf/std_line.jpg"" />"
					'theText = theText & "<font size=""2"" face=""Zapf Humanist 601 Bold BT"" color=""#996d50"">" & rsProjects("name") & "|" & rsProjects("city") & "," & rsProjects("state_id") & "</font><br />"
					'theText = theText & "<font size=""2"" face=""Zapf Humanist 601 Bold BT"" color=""#000000"">"
					'theText = theText & "<i>" & rsProjects("description") & "</i><br />"
					
					if not(rsProjects("ClientName") = "") then 
						theText = theText & "Owner: " & rsProjects("ClientName") & "<br />" 
					end if
					
					if rsArchitect.recordCount > 0 then 
						if not(rsArchitect("item_value") = "") then 
							theText = "theText" & "Architect: " & rsArchitect("item_value")
						end if
					end if
					
					if rsValue.recordCount > 0 then
						if not(rsValue("item_value") = "") then 
							theText = theText & "Contract Value: " & rsValue("item_value") & "<br />" 
						end if
					end if
					
					if rsComplete.recordCount > 0 then
						if not(rsComplete("item_value") = "") then
							theText = theText & "Completion Date: " & rsComplete("item_value") & "<br />"
						end if
					end if
					theText = theText & "</font>"
					
					theDoc.addHTML theText
					rsProjects.moveNext
					
					loop
					

'response.write theText					






theDate = DatePart("m",Date) & "." & DatePart("d",Date) & "." & DatePart("yyyy",Date)



For i = 1 To theDoc.PageCount
  theDoc.PageNumber = i
  theDoc.Flatten
Next

theProjectName = report_title
theProjectName = replace(theProjectName," ","_")
theDoc.Save "C:\idam\BETAPUBLIC\cbg\pdf\" & theProjectName & ".pdf"
Response.Redirect "http://idam.clarkrealty.com/PUBLIC/cbg/pdf/" & theProjectName & ".pdf"
%>
