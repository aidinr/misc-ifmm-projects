<!--#include file="includes\config.asp" -->
<%
	if request.form("submit") = "1" then
	
	'Pre-set parameters
	report_title = Request.Form("report_name")
	
	if(report_title = "") then
		report_title = "News Report"
	end if
	
	keywords = prep_sql(Request.Form("keywords"))
	newsType = prep_sql(Request.Form("news_type"))
	pubTitle = prep_sql(Request.Form("pub_title"))
	relatedProject = prep_sql(Request.Form("related_project"))
	newsYear = prep_sql(Request.Form("news_years"))
	
	report_output = prep_sql(Request.Form("search_output"))
	report_display = prep_sql(Request.Form("search_display"))
	
	
	if(keywords <> "") then
		keywords_sql = " AND (headline like '%" & keywords & "%' OR publicationtitle like '%" & keywords & "%' OR content like '%" & keywords & "%' OR name like '%" & keywords & "%')"
	end if

	if(newsType <> "") then
		newstype_sql = " AND type = " & newsType
	end if
	
	if(pubTitle <> "") then
		pubtitle_sql = " AND publicationtitle = '" & pubtitle & "'"
	end if

	if(relatedProject <> "") then
		relatedProject_sql = " AND c.projectid = " & relatedProject
	end if
	
	if(newsYear <> "") then
		year_sql = " AND year(post_date) = " & newsYear
	end if		
	

	if (report_output <> "pdf") then
	'Search Query
	set rsNews = server.createobject("adodb.recordset")
	
	if(Request.Form("search_display") = "detailed") then
	sql = "select distinct post_date,headline,pull_date, type_value, a.news_id, publicationtitle, pdf, pdflinktitle from ipm_news_type d, ipm_news a left join ipm_news_related_projects b on a.news_id = b.news_id left join ipm_project c on b.projectid = c.projectid where a.show = 1 and d.type_id = a.type "  & keywords_sql & newstype_sql & pubtitle_sql & relatedProject_sql & year_sql &  " order by type_value asc, post_date desc"
	'sql = "select  * from ipm_news a left join ipm_news_related_projects b on a.news_id = b.news_id left join ipm_project c on b.projectid = c.projectid where a.show = 1 " & keywords_sql & newstype_sql & pubtitle_sql & relatedProject_sql & year_sql & " order by a.post_date desc"
	else
	sql = "select distinct a.news_id,post_date,headline from ipm_news a left join ipm_news_related_projects b on a.news_id = b.news_id left join ipm_project c on b.projectid = c.projectid where a.show = 1 " & keywords_sql & newstype_sql & pubtitle_sql & relatedProject_sql & year_sql & " order by a.post_date desc"
	end if
	
	
	rsNews.Open sql, Conn, 1, 4
	
	set rsLineRelatedProjects = server.createobject("adodb.recordset")
	set rsLineContent = server.createobject("adodb.recordset")
	
	end if
	
	end if
%>
<%if (report_output = "html") then %>
<html>
<head>
<title>
Report
</title>
<style type="text/css">
td,th {
	
	border: 1px solid #777777;
	padding: 5px;
	text-align: left;
	vertical-align: top;
	font-family: verdana;
	font-size: 12px;
}
table {
	border:1px solid #777777;
	border-collapse: collapse;
}
th {
	background: #DDDDDD;
}
</style>
</head>
<body>
	<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
	<h2><%=report_title%></h2>
	<table width="100%">
		<!--begin table heading-->
		<tr>
		<%if(Request.Form("search_display") = "detailed") then%>
			<th><b>Type</b></th>
			<th><b>Image</b></th>
			<th><b>Post Date</b></th>
			<th><b>Headline</b></th>
			<th><b>Content</b></th>
			<th><b>Related Projects</b></th>
			<th><b>Contact Info</b></th>
			<th><b>Publication Title</b></th>
			<th><b>PDF Available</b></th>
			<th><b>PDF Title</b></th>
			<th><b>Pull Date</b></th>
			
		<%else%>
			<th><b>Post Date</b></th>
			<th><b>Headline</b></th>
			<th><b>Content</b></th>
		<%end if%>
		</tr>
		<!--end table heading-->
		<!--Begin results listing-->
		
			
			<%do while not rsNews.eof
			
				if(rsLineContent.State = 1) then
					rsLineContent.close
				end if
				
				sql = "select content,contact from ipm_news where news_id = " & rsNews("news_id")
				rsLineContent.Open sql, Conn, 1, 4
			%>
			<tr>
			
				

			<%if(Request.Form("search_display") = "detailed") then
				if(rsNews("PDF") = 1) then 
					pdfAvailable = "Yes"
				else
					pdfAvailable = "No"
				end if

				if(rsLineRelatedProjects.State = 1) then
					rsLineRelatedProjects.close
				end if

				sql = "select * from ipm_news_related_projects, ipm_project where ipm_project.projectid =  ipm_news_related_projects.projectid and ipm_news_related_projects.news_id = " & rsNews("news_id")

				rsLineRelatedProjects.Open sql, Conn, 1, 4
				
				if(rsLineRelatedProjects.recordcount > 0) then
					lineRelatedProjects = rsLineRelatedProjects("name")
					rsLineRelatedProjects.moveNext
					
					do while not rsLineRelatedProjects.eof
					lineRelatedProjects = lineRelatedProjects & ", " & rsLineRelatedProjects("name")
					rsLineRelatedProjects.movenext
					loop
				else
					lineRelatedProjects = ""
				end if
				
			%>
			
				<td align="left"><%=rsNews("type_value")%></td>
				<td align="left"><img width="286" height="136" src="<%=sAssetPath%><%=rsNews("News_ID")%>&Instance=<%=siDAMInstance%>&type=news&size=1" alt="<%=rsNews("Headline")%>" /></td>
				<td align="left"><%=rsNews("post_date")%></td>
				<td align="left"><%=trim(rsNews("headline"))%></td>
				<td align="left"><%=replace(rsLineContent("content"),VbCrLf,"<br />")%></td>
				<td align="left"><%=lineRelatedProjects%></td>
				<td align="left"><%=trim(rsLineContent("contact"))%></td>
				<td align="left"><%=trim(rsNews("publicationtitle"))%></td>
				<td align="left"><%=pdfAvailable%></td>
				<td align="left"><%=trim(rsNews("PDFLinkTitle"))%></td>
				<td align="left"><%=rsNews("pull_date")%></td>			
				
			<%else%>
				<td align="left"><%=rsNews("post_date")%></td>
				<td align="left"><%=trim(rsNews("headline"))%></td>
				<td align="left"><%=replace(rsLineContent("content"),VbCrLf,"<br />")%></td>
			<%end if%>
			</tr>
			<%
			rsNews.moveNext
			loop
			%>
		<!--End results listing-->
	</table>
</body>
</html>
<%elseif (report_output = "csv") then

newLine = chr(13) & chr(10)
dateToday = Now
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=" & replace(report_title," ","-") & "_-_" & DatePart( "yyyy", dateToday )  & "-" & DatePart( "m", dateToday ) & "-" & DatePart( "d", dateToday ) & ".csv"
Response.AddHeader "Content-Description", "File Transfer"

response.write report_title & newLine


			if(Request.Form("search_display") = "detailed") then
			response.write "Type,Post Date, Headline,Content,Related Projects,Contact Info, Publication Title, PDF Available, PDF Title, Pull Date"
			else
			response.write "Post Date,Headline,Content"
			end if
			response.write newLine
			'end header
			
			'begin listing
			do while not rsNews.eof
			
				if(rsLineContent.State = 1) then
					rsLineContent.close
				end if
				
				sql = "select content,contact from ipm_news where news_id = " & rsNews("news_id")
				rsLineContent.Open sql, Conn, 1, 4			

			if(Request.Form("search_display") = "detailed") then
			
			
			if(rsNews("PDF") = 1) then 
					pdfAvailable = "Yes"
				else
					pdfAvailable = "No"
				end if

				if(rsLineRelatedProjects.State = 1) then
					rsLineRelatedProjects.close
				end if

				sql = "select * from ipm_news_related_projects, ipm_project where ipm_project.projectid =  ipm_news_related_projects.projectid and ipm_news_related_projects.news_id = " & rsNews("news_id")

				rsLineRelatedProjects.Open sql, Conn, 1, 4
				
				if(rsLineRelatedProjects.recordcount > 0) then
					lineRelatedProjects = rsLineRelatedProjects("name")
					rsLineRelatedProjects.moveNext
					
					do while not rsLineRelatedProjects.eof
					lineRelatedProjects = lineRelatedProjects & ", " & rsLineRelatedProjects("name")
					rsLineRelatedProjects.movenext
					loop
				else
					lineRelatedProjects = ""
				end if
				
				pubTitle = rsNews("publicationtitle") & ""
				pdfTitle = rsNews("PDFLinkTitle") & ""
				
				response.write """" & replace(replace(rsNews("type_value"),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(rsNews("post_date"),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(trim(rsNews("headline")),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(rsLineContent("content"),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(lineRelatedProjects,"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(trim(rsLineContent("contact")),"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(pubTitle,"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(pdfAvailable,"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(pdfTitle,"""",""""""),newLine," ") & """" & ","
				response.write """" & replace(replace(rsNews("pull_date"),"""",""""""),newLine," ") & """" & ","
							
			else
			response.write """" & replace(replace(rsNews("post_date"),"""",""""""),newLine," ") & """" & ","
			response.write """" & replace(replace(rsNews("headline"),"""",""""""),newLine," ") & """" & ","
			response.write """" & replace(replace(rsLineContent("content"),"""",""""""),newLine," ") & """" & ","
			end if
			
			rsNews.moveNext
			response.write newLine
			loop
				
			
			
			'end listing
			
elseif (report_output = "pdf") then

Set theDoc = Server.CreateObject("ABCpdf6.Doc")
theDoc.HtmlOptions.Timeout = 120000

if(Request.Form("search_display") = "detailed") then
'apply a rotation transform
w = theDoc.MediaBox.Width
h = theDoc.MediaBox.Height
l = theDoc.MediaBox.Left
b = theDoc.MediaBox.Bottom 
theDoc.Transform.Rotate 90, l, b
theDoc.Transform.Translate w, 0

'rotate our rectangle
theDoc.Rect.Width = h
theDoc.Rect.Height = w
end if

theDoc.Rect.Inset 36, 18
theDoc.Page = theDoc.AddPage()
theURL = "http://idam.clarkrealty.com:8081/news_print_get.asp?" & Request.Form
response.write theURL


theID = theDoc.AddImageUrl(theURL)

theDate = MonthName(DatePart("m",Date)) & " " & DatePart("d",Date) & ", "& DatePart("yyyy",Date)

Do
  If Not theDoc.Chainable(theID) Then Exit Do

  	if(Request.Form("search_display") = "detailed") then
  	theDoc.Pos.X = 670
	theDoc.Pos.Y = 7
	theDoc.addHTML  theDate
  
	theDoc.Pos.X = 400
	theDoc.Pos.Y = 7
	theDoc.addHTML "Page " & theDoc.PageNumber
	
	
	theDoc.Pos.X = 50
	theDoc.Pos.Y = 7
	theDoc.addHTML report_title	
	else
  	theDoc.Pos.X = 500
	theDoc.Pos.Y = 7
	theDoc.addHTML  theDate
  
	theDoc.Pos.X = 300
	theDoc.Pos.Y = 7
	theDoc.addHTML "Page " & theDoc.PageNumber
	
	
	theDoc.Pos.X = 40
	theDoc.Pos.Y = 7
	theDoc.addHTML report_title
	end if
	
	'Reset before adding page
	'theDoc.Rect.Resize oriW,oriH
	'theDoc.Rect.Position oriX,oriY
  
  theDoc.Page = theDoc.AddPage()
  theID = theDoc.AddImageToChain(theID)
Loop

  If Not theDoc.Chainable(theID) Then
	if(Request.Form("search_display") = "detailed") then
  	theDoc.Pos.X = 670
	theDoc.Pos.Y = 7
	theDoc.addHTML  theDate
  
	theDoc.Pos.X = 400
	theDoc.Pos.Y = 7
	theDoc.addHTML "Page " & theDoc.PageNumber
	
	
	theDoc.Pos.X = 50
	theDoc.Pos.Y = 7
	theDoc.addHTML report_title	
	else
  	theDoc.Pos.X = 500
	theDoc.Pos.Y = 7
	theDoc.addHTML  theDate
  
	theDoc.Pos.X = 300
	theDoc.Pos.Y = 7
	theDoc.addHTML "Page " & theDoc.PageNumber
	
	
	theDoc.Pos.X = 40
	theDoc.Pos.Y = 7
	theDoc.addHTML report_title
	end if
  End if

For i = 1 To theDoc.PageCount
  theDoc.PageNumber = i
  theDoc.Flatten
Next
theDate = DatePart("m",Date) & "-" & DatePart("d",Date) & "-" & DatePart("yyyy",Date)
theProjectName = replace(report_title," ","_") & "_-_" & theDate &".pdf"
theDoc.Save "D:\Reporting Tool Generated PDF\" & theProjectName

FileExt = Mid(theProjectName, InStrRev(theProjectName, ".") + 1)

set stream = server.CreateObject("ADODB.Stream")
stream.type = "1"
stream.open
stream.LoadFromFile "D:\Reporting Tool Generated PDF\" & theProjectName

fileSize = stream.size

'response.write fileSize

response.clear
response.ContentType = "application/pdf"
Response.AddHeader "Content-Description", "File Transfer"
response.addheader "Content-Disposition", "attachment; filename=" & theProjectName
Response.AddHeader "Content-Length", fileSize

if(FileExt ="pdf") then
do while not(stream.eos)
response.BinaryWrite Stream.Read(8192)
response.Flush
loop
stream.Close
response.End
end if

'Response.Redirect "http://idam.clarkrealty.com/PUBLIC/cbg/pdf/" & theProjectName & ".pdf"

end if
%>
