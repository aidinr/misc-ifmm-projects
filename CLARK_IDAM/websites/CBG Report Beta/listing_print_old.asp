<!--#include file="includes\config.asp" -->
<%
	if request.form("submit") = "1" then
	
	'Pre-set parameters
	report_title = Request.Form("report_name")
	
	report_name = prep_sql(Request.Form("search_name"))
	
	
	report_client = ""
	
	report_region = ""

	report_state = ""
	
	report_disc = ""
	
	
	client_flag = 0
	if(Request.Form("search_client").count > 0) then
		for i=1 to Request.Form("search_client").count
			if(client_flag = 0) then
				report_client = report_client & "'" & prep_sql(Request.Form("search_client")(i)) & "'"
				client_flag = 1
			else
				report_client = report_client & ", '" & prep_sql(Request.Form("search_client")(i)) & "'"
			end if
		next
	report_client = " AND c.clientname IN (" & report_client & " )"
	end if
	
	region_flag = 0
	if(Request.Form("search_region").count > 0) then
		for i=1 to Request.Form("search_region").count
			if(region_flag = 0) then
				report_region = report_region & "'" & prep_sql(Request.Form("search_region")(i)) & "'"
				region_flag = 1
			else
				report_region = report_region & ", '" & prep_sql(Request.Form("search_region")(i)) & "'"
			end if
		next
	report_region = " AND item_value IN (" & report_region & " )"
	end if

	state_flag = 0
	if(Request.Form("search_state").count > 0) then
		for i=1 to Request.Form("search_state").count
			if(state_flag = 0) then
				report_state = report_state & "'" & prep_sql(Request.Form("search_state")(i)) & "'"
				state_flag = 1
			else
				report_state = report_state & ", '" & prep_sql(Request.Form("search_state")(i)) & "'"
			end if
		next
	report_state = " AND c.state_id IN (" & report_state & " )"
	end if

	disc_flag = 0
	if(Request.Form("search_disc").count > 0) then
		for i=1 to Request.Form("search_disc").count
			if(disc_flag = 0) then
				report_disc = report_disc & "'" & prep_sql(Request.Form("search_disc")(i)) & "'"
				disc_flag = 1
			else
				report_disc = report_disc & ", '" & prep_sql(Request.Form("search_disc")(i)) & "'"
			end if
		next
	report_disc = " AND keyid IN (" & report_disc & " )"
	end if	
	
	
	if(Request.Form("search_favorites") = "0") then
	report_favorite = " AND c.favorite = 0"
	elseif(Request.Form("search_favorites") = "1") then
	report_favorite = " AND c.favorite = 1"
	else
	report_favorite = ""
	end if

	if(Request.Form("search_published") = "0") then
	report_published = " AND c.publish = 0"
	elseif(Request.Form("search_published") = "1") then
	report_published = " AND c.publish = 1"
	else
	report_published = ""
	end if
	
	
	'Big search filter
	set rsProjects = server.createobject("adodb.recordset")
	
	if(report_disc = "") then
	sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description from ipm_project_field_desc b, ipm_project c LEFT JOIN ipm_project_field_value a ON a.projectid = c.projectid WHERE a.item_id = b.item_id  AND item_tag = 'IDAM_PROJECT_REGION'  " & report_published & " " & report_favorite & report_region & report_name & report_client & report_state & " ORDER BY c.name"
	else
	sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description from ipm_project_field_desc b, ipm_project c LEFT JOIN ipm_project_field_value a ON a.projectid = c.projectid WHERE a.item_id = b.item_id  AND item_tag = 'IDAM_PROJECT_REGION'  " & report_published & " " & report_favorite & report_region & report_name  & report_client & report_state & " AND c.projectID IN (select ipm_project.projectid from ipm_project, ipm_project_discipline where ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 " & report_disc & " ) ORDER BY c.name"
	end if
	
	rsProjects.Open sql, Conn, 1, 4
	
	'Query for additional UDFs
	
	
	
	end if



Set theDoc = Server.CreateObject("ABCpdf6.Doc")
theDoc.HtmlOptions.Timeout = 120000



theDoc.HtmlOptions.PageCacheClear
theDoc.HtmlOptions.BrowserWidth = 640

theDoc.HtmlOptions.FontProtection = False
theDoc.HtmlOptions.FontSubstitute = False
theDoc.HtmlOptions.FontEmbed = True


theDoc.Rect.Inset -14, -18

theFont =  theDoc.EmbedFont("C:\WINDOWS\Fonts\tt0015m_.ttf", "Unicode", false, true, true)
theDoc.Font = theFont
theURL = "http://idam.clarkrealty.com:8081/listing_html.asp?" & Request.Form

theID = theDoc.AddImageUrl(theURL)

theDate = DatePart("m",Date) & "." & DatePart("d",Date) & "." & DatePart("yyyy",Date)

Do
  If Not theDoc.Chainable(theID) Then Exit Do


  
	theDoc.Pos.X = 300
	theDoc.Pos.Y = 25
	theDoc.addHTML "Page " & theDoc.PageNumber

	oriW = theDoc.Rect.Width
	oriH = theDoc.Rect.Height

	oriX = theDoc.Rect.Left
	oriY = theDoc.Rect.Bottom



	
	theDoc.Transform.Rotate 90,890,20

	theDoc.Rect.Position 890,20
	theDoc.Rect.Resize 300,300	
	
	theDoc.addHTML "&nbsp; <font size=""1"" face=""Zapf Humanist 601 Bold BT"" color=""#996d50"">" & report_title & " as of " & theDate & "</font>"
  
	'Reset before adding page
	theDoc.Rect.Resize oriW,oriH
	theDoc.Rect.Position oriX,oriY
	
	theDoc.Transform.Reset
	theDoc.Pos = "0 0"
	
	theDoc.Page = theDoc.AddPage()
	theID = theDoc.AddImageToChain(theID)
Loop

  If Not theDoc.Chainable(theID) Then

  
  	theDoc.Pos.X = 300
	theDoc.Pos.Y = 25
	theDoc.addHTML "Page " & theDoc.PageNumber
	
	theDoc.Transform.Rotate 90,595,20
	
	theDoc.Pos.X = 595
	theDoc.Pos.Y = 20
	theDoc.addHTML "&nbsp; <font size=""1"" face=""Zapf Humanist 601 Bold BT"" color=""#996d50"">" & report_title & " as of " & theDate & "</font>"	
	
  End if

For i = 1 To theDoc.PageCount
  theDoc.PageNumber = i
  theDoc.Flatten
Next

theProjectName = report_title
theProjectName = replace(theProjectName," ","_")
theDoc.Save "C:\idam\BETAPUBLIC\cbg\pdf\" & theProjectName & ".pdf"
Response.Redirect "http://idam.clarkrealty.com/PUBLIC/cbg/pdf/" & theProjectName & ".pdf"
%>
