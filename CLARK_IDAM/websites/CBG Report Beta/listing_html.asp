<!--#include file="includes\config.asp" -->
<%
	if request.querystring("id") <> "" then

	title_y = request.querystring("y")
	title_x = request.querystring("x")
	
	set rsTemp = server.createobject("adodb.recordset")
	sql = "select * FROM CBG_PROJECT_LISTING_REQUESTS where ID = " & prep_sql(request.querystring("id"))
	rsTemp.Open sql, Conn, 1, 4
	'Pre-set parameters
	letterhead = Request.querystring("letterhead")
	logo = Request.querystring("custom_logo")
	logo_path = Request.querystring("custom_logo_path")
	
	report_name = prep_sql(Request.querystring("name"))
	 
	report_client = ""
	report_region = ""
	report_state = ""
	report_disc = ""
	
	do while not rsTemp.EOF

	select case rsTemp("NAME")
		case "client"
			if (rsTemp("VALUE") = "") then
				'rsTemp("VALUE") = "-1"
				report_client = ""
			else
			report_client = " AND d.clientid IN (" & rsTemp("VALUE") & " )"
			end if		
		case "region"
			if (rsTemp("VALUE") = "") then
				'rsTemp("VALUE") = "''"
				report_region = ""
			else
			report_region = " AND item_value IN (" & rsTemp("VALUE") & " )"				
			end if
		case "state"
			if (rsTemp("VALUE") = "") then
				'rsTemp("VALUE") = "''"
				report_state = ""
			else
			report_state = " AND c.state_id IN (" & rsTemp("VALUE") & " )"				
			end if		
			
		case "discipline"
			if (rsTemp("VALUE") = "") then
				'rsTemp("VALUE") = "-1"
				report_disc = ""
			else
			report_disc = " AND keyid IN (" & rsTemp("VALUE") & " )"				
			end if		
			
		case "discipline2"
			if (rsTemp("VALUE") = "") then
				'rsTemp("VALUE") = "-1"
				report_discipline = ""
			else
			report_discipline = " AND officeid IN (" & rsTemp("VALUE") & " )"				
			end if		
						
	end select
	rsTemp.moveNext
	loop
	
	if(Request.Querystring("favorite") = "0") then
	report_favorite = " AND c.favorite = 0"
	elseif(Request.Querystring("favorite") = "1") then
	report_favorite = " AND c.favorite = 1"
	else
	report_favorite = ""
	end if

	if(Request.Querystring("published") = "0") then
	report_published = " AND c.publish = 0"
	elseif(Request.Querystring("published") = "1") then
	report_published = " AND c.publish = 1"
	else
	report_published = ""
	end if
	
	if not(report_name = "") then
		report_name = " AND c.name LIKE '%" & report_name &  "%'"
	end if
	
	
	'Big search filter
	set rsProjects = server.createobject("adodb.recordset")
	
	sql = "select distinct c.projectid,c.name,c.clientname,c.address,c.city,c.state_id,c.zip,c.projectnumber,c.description from ipm_project_field_desc b, ipm_client d,  ipm_project c LEFT JOIN ipm_project_field_value a ON a.projectid = c.projectid WHERE a.item_id = b.item_id  AND item_tag = 'IDAM_PROJECT_REGION'  AND d.name = c.clientname " & report_published & " " & report_favorite & report_region & report_name  & report_client & report_state & " AND c.projectID IN (select ipm_project.projectid from ipm_project, ipm_project_discipline where ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' " & report_disc & " ) AND c.projectID IN (select ipm_project.projectid from ipm_project, ipm_project_office where ipm_project.projectid = ipm_project_office.projectid AND AVAILABLE = 'Y' " & report_discipline & " ) ORDER BY c.name"
'response.write sql
'response.end
	rsProjects.Open sql, Conn, 1, 4
	
	'Query for additional UDFs
	
	
	
	end if
	

	


	set rsArchitect=server.createobject("adodb.recordset")

	
	set rsComplete=server.createobject("adodb.recordset")

	
	set rsValue=server.createobject("adodb.recordset")

%>	
<html>
<head>
<style  type="text/css">
.title {
	font-family: Zapf Humanist 601 BT;
	color: #996d50;
	font-size: 14px;
	color: rgb(153,109,80);
	
}
.description {
	font-family: Zapf Humanist 601 BT;
	font-size: 14px;
	color: #000000;
	
}
.header {
	font-family: Times New Roman;
	color: #996d50;
	font-size: 36px;
}
</style>
</head>
<body>

	<table width="612">
		<td width="612" valign="top">
			<%if (letterhead = "1") then%>
			<br /><br /><br /><br /><br /><br /><br /><br /><br />
			<%else %>
			<%end if%>
			<table width="80%" cellpadding="0" cellspacing="0" align="center">
				<%if not (letterhead = "1") then%>
				<tr>
					<td align="right"><div style="margin-top:<%=title_y%>px;margin-right: <%=title_x%>px;height:24px;font-family: Zapf Humanist 601 BT;color:#5a471c;font-size:24px;padding-top:4px;"><span style="font-family: Zapf Humanist 601 BT;color:#5a471c;font-size:24px; ">PORTFOLIO</span></div><br /><br /></td>
				</tr>
				<%end if%>
				<tr>
					<td align="left"><div class="header">Representative Project Listing</div></td>
				</tr>
				<%
					i = 0
					do while not rsProjects.eof
				
					if(rsComplete.State = 1) then
					rsComplete.close
					end if
					
					if(rsArchitect.State = 1) then
					rsArchitect.close
					end if
					
					if(rsValue.State = 1) then
					rsValue.close
					end if
				
					sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_COMPLETION'"
					rsComplete.Open sql, Conn, 1, 4

					sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_ARCHITECT'"
					rsArchitect.Open sql, Conn, 1, 4

					sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & rsProjects("projectID")  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_CONSTRUCTIONVALUE'"
					rsValue.Open sql, Conn, 1, 4
					
						if(rsComplete("item_value") <> "") then
						lineDate = CDate(rsComplete("item_value"))
						lineDate = MonthName(DatePart("m",lineDate)) & " " & DatePart("d",lineDate) & ", " & DatePart("yyyy",lineDate)
						else
						lineDate = ""
						end if
				
				
				%>
				<tr>
					<td valign="top">
						<div style="line-height:18px;">
						<hr style="width:100%;height:1px;border:1px solid #5a471c;color:#5a471c" />
						<div style="margin-left:15px;">
						<span class="title"><b><%=rsProjects("name")%> | <%=rsProjects("city")%>, <%=rsProjects("state_id")%></b></span><br />
						<span class="description">
						<i><%=rsProjects("description")%></i><br />
						<%if not(rsProjects("ClientName") = "") then%>Owner: <%=rsProjects("ClientName")%><br /><%end if%>
						<%if rsArchitect.recordCount > 0 then%><%if not(rsArchitect("item_value") = "") then%>Architect: <%=rsArchitect("item_value")%><br /><%end if%><%end if%>
						<%if rsValue.recordCount > 0 then%><%if not(rsValue("item_value") = "") then%>Contract Value: <%=rsValue("item_value")%><br /><%end if%><%end if%>
						<%if rsComplete.recordCount > 0 then%><%if not(lineDate = "") then%>Completion Date: <%=lineDate%><br /><%end if%><%end if%>
						</span>
						</div>
						</div>
					
					</td>
				</tr>
				<%
				rsProjects.moveNext
				
				i = i + 1
			
				if(i mod 6 = 0 and not rsProjects.eof) then %>	
				<tr>
					<td valign="top">
					<hr style="width:100%;height:1px;border:1px solid #5a471c;color:#5a471c" />
					</td>
				</table>
				</td>
				</table>				
						<div style="page-break-after:always">&nbsp;</div>

				
						<table width="612">
						<td width="612" valign="top">
						<%if (letterhead = "1") then%>
						<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
						<%else %>
						<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
						<%end if%>
						<table width="80%" cellpadding="0" cellspacing="0" align="center">				
				
				<%
				elseif (rsProjects.eof) then%>
				
				<tr>
					<td valign="top">
					<hr style="width:100%;height:1px;border:1px solid #5a471c;color:#5a471c" />
					</td>
				</table>
				</td>
				</table>
				
				<%end if
				
				loop
				%>
			
			</table>
		
		</td>
	</table>
</body>
</html>

