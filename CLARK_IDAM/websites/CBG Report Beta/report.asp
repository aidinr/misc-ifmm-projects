<!--#include file="includes\config.asp" -->
<%

	'''NOTE: THE DISCIPLINE AND FUNCTIONAL MARKET (OFFICE) TABLES ARE INVERTED DUE TO PAST MISLABELLING

	set rsUDF=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_desc where active = 1 order by item_name"
	rsUDF.Open sql, Conn, 1, 4
	
	set rsClient = server.createobject("adodb.recordset")
	sql = "select * from ipm_client where active = 1 order by name"
	rsClient.Open sql, Conn, 1, 4
	
	set rsFunctional = server.createobject("adodb.recordset")
	sql = "select * from ipm_discipline where keyuse = 1 order by keyname"
	rsFunctional.Open sql, Conn, 1, 4
	
	set rsDiscipline = server.createobject("adodb.recordset")
	sql = "select * from ipm_office where keyuse = 1 order by keyname"
	rsDiscipline.Open sql, Conn, 1, 4
	
	set rsProjectsStates=server.createobject("adodb.recordset")
	sql = "select distinct state_id from ipm_project where available = 'y' and state_id <> '' order by state_id"
	rsProjectsStates.Open sql, Conn, 1, 4

%>

<html>
<head>
<title><%=sPageTitle%> Reports Tool</title>
	<!--#include file="includes\header.asp" -->
	
<script type="text/javascript">
	function toggleFields(fieldsID,indicatorID) {
		theFields = document.getElementById(fieldsID);
		theFieldsIndicator = document.getElementById(indicatorID);
			if(theFields.style.display != "none") {
				theFields.style.display = "none";
				theFieldsIndicator.innerHTML = "-";
			}
			else {
				theFields.style.display = "block";
				theFieldsIndicator.innerHTML = "+";			

			}
	}
</script>
</head>
<body onload="if(document.getElementById('pdf_radio').checked == true) { document.getElementById('pdf_options').style.visibility='visible'; }">
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			
			<div id="content">

			<br />
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview" style="width:100%;">
					<div id="content_text">
					Marketing Tools | Generate Project Report
					<br /><br />
					<img src="images/title_project_reporting.jpg" alt="Project Reporting" />
					<h4>Step 1 - Select Projects</h4>
					<form method="post" action="report_print.asp">

				<table>
					<tr>
						<td>Project Name Contains:</td>
						<td><input type="text" name="search_name" /></td>
					</tr>
					<tr>
						<td>Project Client:</td>
						<td>	<select name="search_client">
							<option value="">All</option>
							<%
							do while not rsClient.eof
							clientName = trim(rsClient("Name"))
							%>
							
							<option value="<%=clientName%>"><%=clientName%></option>
							
							<%rsClient.moveNext
							loop							
							
							%>
							</select>
						</td>
					</tr>					
					<tr>
						<td>Project Type:</td>
						<td><select name="search_functional">
						<option value="">All</option>
							<%
							do while not rsFunctional.eof
							keyName = trim(rsFunctional("keyName"))
							%>
							
							<option value="<%=rsFunctional("KeyID")%>"><%=keyName%></option>
							
							<%rsFunctional.moveNext
							loop							
							
							%>
							</select></td>
					</tr>
					<tr>
						<td>Project Discipline:</td>
						<td><select name="search_discipline">
						<option value="">All</option>
							<%
							do while not rsDiscipline.eof
							keyName = trim(rsDiscipline("keyName"))
							%>
							
							<option value="<%=rsDiscipline("KeyID")%>"><%=keyName%></option>
							
							<%rsDiscipline.moveNext
							loop							
							
							%>
							</select></td>
					</tr>					
					<tr>
						<td>Project Region:</td>
						<td><select name="search_region">
							<option value="">All</option>
							<option value="Northeast">Northeast</option>
							<option value="Northwest">Northwest</option>
							<option value="Southeast">Southeast</option>
							<option value="Southwest">Southwest</option>
						    </select>
						</td>
					</tr>
					<tr>
						<td>Project State:</td>
						<td><select name="search_state">
								<option value="">All</option>
								
								<%do while not rsProjectsStates.eof%>
								<option value="<%=trim(rsProjectsStates("state_id"))%>"><%=trim(rsProjectsStates("state_id"))%></option>
								<%
								rsProjectsStates.moveNext
								loop
								%>
							</select>
</td>
					</tr>					
<tr>
						<td>Favorite Projects:</td>
						<td>
						<input name="search_favorites" type="radio" checked="checked" value="" /> All  <input type="radio" name="search_favorites" value="1" /> Favorites  <input type="radio" name="search_favorites" value="0" />Non-Favorites
</td>
					</tr>
					<tr>
						<td>Published Projects:</td>
						<td>
						<input type="radio" name="search_published" checked="checked" value="" /> All  <input type="radio" name="search_published" value="1" /> Published  <input type="radio" name="search_published" value="0" />Non-Published
</td>
					</tr>						

	
				
					
				</table>


<h4>Step 2 - Select Formatting Options</h4>

<table cellpadding="3" cellspacing="3">
		<tr>
			<td valign="top">Report Name: </td>
			<td><input type="text" name="report_name" /></td>
		</tr>	
					<tr>
						<td valign="top">Output:</td>
						<td> <input name="search_output" type="radio" value="html" checked="checked" onclick="document.getElementById('pdf_options').style.visibility='hidden';" />HTML  <input id="pdf_radio" name="search_output" type="radio" value="pdf" onclick="document.getElementById('pdf_options').style.visibility='visible';" />PDF  <input name="search_output" type="radio" value="csv" onclick="document.getElementById('pdf_options').style.visibility='hidden';" />CSV
						     <div id="pdf_options" style="visibility:hidden;">Page break interval: <input type="text" size="2" name="pdf_page_break" id="pdf_page_break" value="3" /></div>
						</td>
					</tr>
					<tr>
						<td valign="top">Display Image:</td>
						<td> <input name="search_display_image" type="checkbox" checked="checked" value="1" />
						</td>
					</tr>			
		<tr>
			<td valign="top">Fields to display: </td>
			<td></td>
		</tr>
		<tr>
			<td valign="top">Standard Fields</td>

			<td>
			<input type="checkbox" class="udf" name="number" value="1" checked="checked" /> Project Number<br />
			<input type="checkbox" class="udf" name="client" value="1" checked="checked" /> Client<br />
			<input type="checkbox" class="udf" name="address" value="1" checked="checked" /> Address<br />
			<input type="checkbox" class="udf" name="city_state" value="1" checked="checked" /> Location (City, State)<br />
			<input type="checkbox" class="udf" name="zip" value="1" checked="checked" /> ZIP<br />
			<input type="checkbox" class="udf" name="desc" value="1" checked="checked" checked="checked" /> Short Description<br />
			<input type="checkbox" class="udf" name="mdesc" value="1" checked="checked" /> Long Description<br />
			<input type="checkbox" class="udf" name="purl" value="1" checked="checked" /> Project URL<br />
			</td>
		</tr>
		<tr>
			<td valign="top"> <big><b><a id="udfFieldsIndicator" href="javascript:toggleFields('udfFields','udfFieldsIndicator');">-</a></b></big> <a href="javascript:toggleFields('udfFields','udfFieldsIndicator');">User-Defined Fields</a></td>
			<td>
			<div id ="udfFields" style="display:none;">
			<%
			do while not rsUDF.eof
			itemTag = trim(rsUDF("item_tag"))
			%><input type="checkbox" class="udf" name="<%=itemTag%>" value="1" /><%=trim(rsUDF("item_name"))%><br /><%rsUDF.moveNext
			loop
			
			%>
			</div>
			</td>
		</tr>
		<tr>
			<td valign="top"> <big><b><a id="keywordsIndicator" href="javascript:toggleFields('keywordsFields','keywordsIndicator');">-</a></b></big> <a href="javascript:toggleFields('keywordsFields','keywordsIndicator');">Keywords</a></td>
			<td>
			<div id ="keywordsFields" style="display:none;">
			<input type="checkbox" class="udf" name="functional" value="1" /> Functional Markets <br />
			<input type="checkbox" class="udf" name="disciplines" value="1" /> Disciplines <br />
			<input type="checkbox" class="udf" name="keywords" value="1" /> Keywords <br />
			</div>
			</td>
		</tr>
		<tr>
			<td valign="top"> <big><b><a id="caseStudiesIndicator" href="javascript:toggleFields('caseStudiesFields','caseStudiesIndicator');">-</a></b></big> <a href="javascript:toggleFields('caseStudiesFields','caseStudiesIndicator');">Case Studies</a></td>
			<td>
			<div id ="caseStudiesFields" style="display:none;">
			<input type="checkbox" class="udf" name="challenge" value="1" /> Challenge <br />
			<input type="checkbox" class="udf" name="approach" value="1" /> Approach <br />
			<input type="checkbox" class="udf" name="solution" value="1" /> Solution <br />
			</div>
			</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" value="Generate Report" /></td>
		</tr>
<input type="hidden" value="1" name="submit" />
</form>
					
					
					
					
					
					</div>
					
				</td>
				


			</tr>
	
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
