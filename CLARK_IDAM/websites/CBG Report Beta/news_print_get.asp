<!--#include file="includes\config.asp" -->
<%
	if request.querystring("submit") = "1" then
	
	'Pre-set parameters
	report_title = Request.QueryString("report_name")
	
	if(report_title = "") then
		report_title = "News Report"
	end if
	
	keywords = prep_sql(Request.QueryString("keywords"))
	newsType = prep_sql(Request.QueryString("news_type"))
	pubTitle = prep_sql(Request.QueryString("pub_title"))
	relatedProject = prep_sql(Request.QueryString("related_project"))
	newsYear = prep_sql(Request.QueryString("news_years"))
	
	report_output = prep_sql(Request.QueryString("search_output"))
	report_display = prep_sql(Request.QueryString("search_display"))
	
	report_page_break = int(Request.Querystring("pdf_page_break"))
	
	
	if(keywords <> "") then
		keywords_sql = " AND (headline like '%" & keywords & "%' OR publicationtitle like '%" & keywords & "%' OR content like '%" & keywords & "%' OR name like '%" & keywords & "%')"
	end if

	if(newsType <> "") then
		newstype_sql = " AND type = " & newsType
	end if
	
	if(pubTitle <> "") then
		pubtitle_sql = " AND publicationtitle = '" & pubtitle & "'"
	end if

	if(relatedProject <> "") then
		relatedProject_sql = " AND c.projectid = " & relatedProject
	end if
	
	if(newsYear <> "") then
		year_sql = " AND year(post_date) = " & newsYear
	end if		
	

	
	'Search Query
	set rsNews = server.createobject("adodb.recordset")
	if(Request.QueryString("search_display") = "detailed") then
	sql = "select distinct post_date,headline,pull_date, type_value, publicationtitle, a.news_id, pdf, pdflinktitle from ipm_news_type d, ipm_news a left join ipm_news_related_projects b on a.news_id = b.news_id left join ipm_project c on b.projectid = c.projectid where a.show = 1 and d.type_id = a.type "  & keywords_sql & newstype_sql & pubtitle_sql & relatedProject_sql & year_sql &  " order by type_value asc, post_date desc"
	'sql = "select  * from ipm_news a left join ipm_news_related_projects b on a.news_id = b.news_id left join ipm_project c on b.projectid = c.projectid where a.show = 1 " & keywords_sql & newstype_sql & pubtitle_sql & relatedProject_sql & year_sql & " order by a.post_date desc"
	else
	sql = "select distinct a.news_id,post_date,headline from ipm_news a left join ipm_news_related_projects b on a.news_id = b.news_id left join ipm_project c on b.projectid = c.projectid where a.show = 1 " & keywords_sql & newstype_sql & pubtitle_sql & relatedProject_sql & year_sql & " order by a.post_date desc"
	end if
	
	
	rsNews.Open sql, Conn, 1, 4
	
	set rsLineRelatedProjects = server.createobject("adodb.recordset")
	set rsLineContent = server.createobject("adodb.recordset")
	
	end if
%>
<html>
<head>
<title>
Report
</title>
<style type="text/css">
body {
	font-size:10px;
}
td,th {
	
	border: 1px solid #777777;
	padding: 5px;
	text-align: left;
	vertical-align: top;
	font-size:12px;
}
table {
	border:1px solid #777777;
	border-collapse: collapse;
}
</style>
</head>
<body>
<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
<h4><%=report_title%></h4>
<table width="100%">
		<!--begin table heading-->
		<tr>
		<%if(Request.QueryString("search_display") = "detailed") then%>
			<th><b>Type</b></th>
			<th><b>Image</b></th>
			<th><b>Post Date</b></th>
			<th><b>Headline</b></th>
			<th><b>Content</b></th>
			<th><b>Related Projects</b></th>
			<th><b>Contact Info</b></th>
			<th><b>Publication Title</b></th>
			<th><b>PDF Available</b></th>
			<th><b>PDF Title</b></th>
			<th><b>Pull Date</b></th>
			
		<%else%>
			<th><b>Post Date</b></th>
			<th><b>Headline</b></th>
			<th><b>Content</b></th>
		<%end if%>
		</tr>
		<!--end table heading-->
		<!--Begin results listing-->
		
			
			<%
			i = 0
			do while not rsNews.eof
			
			
				if(rsLineContent.State = 1) then
					rsLineContent.close
				end if
				
				sql = "select content,contact from ipm_news where news_id = " & rsNews("news_id")
				rsLineContent.Open sql, Conn, 1, 4			
			
			%>
			<tr>
			<%if(Request.QueryString("search_display") = "detailed") then
				if(rsNews("PDF") = 1) then 
					pdfAvailable = "Yes"
				else
					pdfAvailable = "No"
				end if

				if(rsLineRelatedProjects.State = 1) then
					rsLineRelatedProjects.close
				end if

				sql = "select * from ipm_news_related_projects, ipm_project where ipm_project.projectid =  ipm_news_related_projects.projectid and ipm_news_related_projects.news_id = " & rsNews("news_id")

				rsLineRelatedProjects.Open sql, Conn, 1, 4
				
				if(rsLineRelatedProjects.recordcount > 0) then
					lineRelatedProjects = rsLineRelatedProjects("name")
					rsLineRelatedProjects.moveNext
					
					do while not rsLineRelatedProjects.eof
					lineRelatedProjects = lineRelatedProjects & ", " & rsLineRelatedProjects("name")
					rsLineRelatedProjects.movenext
					loop
				else
					lineRelatedProjects = ""
				end if
				
			%>
			
				<td align="left"><%=rsNews("type_value")%></td>
				<td align="left"><img width="286" height="136" src="<%=sAssetPath%><%=rsNews("News_ID")%>&Instance=<%=siDAMInstance%>&type=news&size=1" alt="<%=rsNews("Headline")%>" /></td>
				<td align="left"><%=rsNews("post_date")%></td>
				<td align="left"><%=trim(rsNews("headline"))%></td>
				<td align="left"><%=replace(rsLineContent("content"),VbCrLf,"<br />")%></td>
				<td align="left"><%=lineRelatedProjects%></td>
				<td align="left"><%=trim(rsLineContent("contact"))%></td>
				<td align="left"><%=trim(rsNews("publicationtitle"))%></td>
				<td align="left"><%=pdfAvailable%></td>
				<td align="left"><%=trim(rsNews("PDFLinkTitle"))%></td>
				<td align="left"><%=rsNews("pull_date")%></td>			
				
			<%else%>
				<td align="left"><%=rsNews("post_date")%></td>
				<td align="left"><%=trim(rsNews("headline"))%></td>
				<td align="left"><%=replace(rsLineContent("content"),VbCrLf,"<br />")%></td>
			<%end if%>
			</tr>
			<%
			rsNews.moveNext
			i = i + 1
			
			if(i mod report_page_break = 0 and not rsNews.eof) then %> 
			</table>
			
			
			<div style="page-break-before:always">&nbsp;</div> 
			<img src="images/clark_builders_group.jpg" alt="Clark Logo" />
			<h4><%=report_title%></h4>
			<table width="100%">
					<!--begin table heading-->
					<tr>
		<%if(Request.QueryString("search_display") = "detailed") then%>
			<th><b>Type</b></th>
			<th><b>Image</b></th>
			<th><b>Post Date</b></th>
			<th><b>Headline</b></th>
			<th><b>Content</b></th>
			<th><b>Related Projects</b></th>
			<th><b>Contact Info</b></th>
			<th><b>Publication Title</b></th>
			<th><b>PDF Available</b></th>
			<th><b>PDF Title</b></th>
			<th><b>Pull Date</b></th>
			
		<%else%>
			<th><b>Post Date</b></th>
			<th><b>Headline</b></th>
			<th><b>Content</b></th>
		<%end if%>
					</tr>
			<%end if
			loop
			%>
		<!--End results listing-->
</table>	
</body>
</html>
