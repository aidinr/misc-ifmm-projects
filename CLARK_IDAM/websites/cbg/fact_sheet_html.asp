<!--#include file="includes\config.asp" -->
<%
	assetQFactor = 1 'overwrite
	
	projectID = prep_sql(request.querystring("pid"))
	set rsProject=server.createobject("adodb.recordset")
	sql="select a.name projectName,a.description,a.descriptionmedium,a.city,a.state_id,b.name state_name,a.address,a.zip  from ipm_project a LEFT JOIN ipm_state b ON a.state_id = b.state_id where AVAILABLE = 'Y' AND projectid = " & projectID
	rsProject.Open sql, Conn, 1, 4

	set rsAssets=server.createobject("adodb.recordset")
	'sql = "select * from ipm_asset a,ipm_asset_category b where a.projectid = b.projectid AND b.name = 'Image Gallery' AND a.category_id = b.category_id AND a.available = 'Y' and b.available = 'Y' AND a.securitylevel_id = 3 AND a.projectid = " & projectID
	sql = "select * from ipm_asset a,ipm_asset_field_value b where a.asset_id = b.asset_id AND b.item_id = 21764790 AND b.item_value = '1' AND a.available = 'Y' AND a.securitylevel_id = 3 AND a.projectid = " & projectID & " order by newid()"
	rsAssets.Open sql, Conn, 1, 4
	
	set rsAwards=server.createobject("adodb.recordset")
	sql = "select top 14 * FROM ipm_awards, ipm_awards_related_projects where ipm_awards.type = 0 AND ipm_awards.awards_id = ipm_awards_related_projects.awards_id AND SHOW = 1 AND ipm_awards_related_projects.projectid = " & projectID & " ORDER BY headline DESC"
	rsAwards.Open sql, Conn, 1, 4
	
	set rsFeatured=server.createobject("adodb.recordset")
	sql = "select * from ipm_asset_field_value a, ipm_asset_field_desc b, ipm_asset c where a.item_id = b.item_id and b.item_tag = 'IDAM_BROCHURE_P1' and a.asset_id = c.asset_id and c.projectid = " & projectID & "and c.available = 'y' and c.securitylevel_id = 3 and a.item_value = '1' order by newid()"
	rsFeatured.Open sql, Conn, 1, 4

	set rsSelectedImages=server.createobject("adodb.recordset")
	sql = "select * from ipm_asset_field_value a, ipm_asset_field_desc b, ipm_asset c where a.item_id = b.item_id and b.item_tag = 'IDAM_BROCHURE_SMALL' and a.asset_id = c.asset_id and c.projectid = " & projectID & "and c.available = 'y' and c.securitylevel_id = 3 and a.item_value = '1' order by newid()"
	rsSelectedImages.Open sql, Conn, 1, 4		
	
	set rsArchitect=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & projectID  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_ARCHITECT' ORDER BY NEWID()"
	rsArchitect.Open sql, Conn, 1, 4
	projectCity = ucase(rsProject("city"))
	projectstate = ucase(rsProject("state_id"))
%>	
<html>
<head>
<style  type="text/css">
td {
	font-family: Zapf Humanist 601 BT;
	font-size: 7.5pt;
	line-height: 10.5pt;
	color: #000000;
}
.header {
	font-family: Times New Roman;
	color: #996d50;
	font-size: 21pt;
	line-height: 23pt;
	color: rgb(153,109,80);
	
}
.subheader {
	font-size: 9pt;
}
.subheader2 {
	color: rgb(153,109,80);
	font-size: 8pt;
	line-height: 11.5pt;
}
.subheader3 {
	font-size: 7.5pt;
	line-height: 10.5pt;
}
</style>
</head>
<body>
	<table width="612">
		<tr>
		<td width="612" valign="top">
			
			
			<table style="margin-top:90px;position:relative;left:5px;" width="88%" cellpadding="12" cellspacing="12" align="center">
				<tr>
					<td width="35%" valign="top">
					<%
					z = 0
					if(rsFeatured.recordCount > 0) then
					z = 1 %>
					<img width="200" height="157" src="<%=sAssetPath%><%=rsFeatured("asset_id")%>&Instance=<%=siDAMInstance%>&type=asset&size=1" /><br /><br />
					<%
					end if
					
					if (z=0) then
					z = 1
					%>
					<img width="200" height="157" src="<%=sAssetPath%><%=projectID%>&Instance=<%=siDAMInstance%>&type=project&size=1" /><br /><br />
					<%
					end if
					
					if(rsSelectedImages.RecordCount > 0) then
					do while not rsSelectedImages.eof
					if (z < 3) then
					%>
					
					<img width="200" height="157" src="<%=sAssetPath%><%=rsSelectedImages("asset_id")%>&Instance=<%=siDAMInstance%>&type=asset&size=1&qfactor=<%=assetQFactor%>" /><br /><br />
					<%
					z = z + 1
					end if
					rsSelectedImages.moveNext
					loop					
					
					elseif(rsAssets.recordCount > 0) then
					
					do while not rsAssets.eof
					if (z < 3) then
					%>
					
					<img width="200" height="157" src="<%=sAssetPath%><%=rsAssets("asset_id")%>&Instance=<%=siDAMInstance%>&type=asset&size=1&qfactor=<%=assetQFactor%>" /><br /><br />
					<%
					z = z + 1
					end if
						rsAssets.moveNext
					loop					
					
					end if
					

					%>
					</td>
					<td width="65%" valign="top" align="left">
						<div style="position:relative;left:15px;top:-5px;">
						<span class="header"><%=rsProject("projectName")%></span>
						<hr style="position:relative;top:-3px;width:271px;height:1px;border-bottom:0;border-left:0;border-right:0;border-top:1px solid #5a471c;color:#5a471c" />
						<div style="position:relative;top:-5px;" class="subheader"><%=ucase(rsProject("city"))%>, <%=ucase(rsProject("state_name"))%><br /></div>
						<br />
						<b class="subheader2">DESCRIPTION</b><br />
						<i class="subheader3"><%=rsProject("description")%></i><br />
						<%=rsProject("descriptionMedium")%><br />
						<br />
						<b class="subheader2">ADDRESS</b><br />
						<%=rsProject("address")%><br />
						<%=rsProject("city")%>, <%=rsProject("state_id")%>  <%=" " & rsProject("zip")%><br />
						<br />
											
						<%if not(rsArchitect("item_value") = "") then%>
						<b class="subheader2">ARCHITECT</b><br />
						<%=rsArchitect("item_value")%><br />
						<br />
						<%end if%>
						<%if (rsAwards.recordCount > 0) then%><b class="subheader2">RECOGNITION</b><br /><%end if%>

						<%do while not rsAwards.eof %>
					
						<%=rsAwards("headline")%><br />

						<%
						rsAwards.movenext
						loop
						%>
						
						</div>
					</td>
			</tr>
			</table>
		
		</td>
		</tr>
	</table>
</body>
</html>

