<!--#include file="includes\config.asp" -->
<%
	set rsUDF=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_desc where active = 1 order by item_name"
	rsUDF.Open sql, Conn, 1, 4
	
	set rsClient = server.createobject("adodb.recordset")
	sql = "select * from ipm_client where active = 1 order by name"
	rsClient.Open sql, Conn, 1, 4
	
	set rsDisc = server.createobject("adodb.recordset")
	sql = "select * from ipm_discipline where keyuse = 1 order by keyname"
	rsDisc.Open sql, Conn, 1, 4

%>
<html>
<head>
<title>
Report
</title>
</head>
<body>
<form method="post" action="report_print.asp">
	<table>
		<tr>
			<td>Report Name: </td>
			<td><input type="text" name="report_name" /></td>
		</tr>
		<tr>
			<td valign="top">Conditions: </td>
			<td>
				<table>
					<tr>
						<td>Project name contains:</td>
						<td><input type="text" name="search_name" /></td>
					</tr>
					<tr>
						<td>Project client:</td>
						<td>	<select name="search_client">
							<option value="">All</option>
							<%
							do while not rsClient.eof
							clientName = trim(rsClient("Name"))
							%>
							
							<option value="<%=clientName%>"><%=clientName%></option>
							
							<%rsClient.moveNext
							loop							
							
							%>
							</select>
						</td>
					</tr>					
					<tr>
						<td>Project type:</td>
						<td><select name="search_discipline">
						<option value="">All</option>
							<%
							do while not rsDisc.eof
							keyName = trim(rsDisc("keyName"))
							%>
							
							<option value="<%=rsDisc("KeyID")%>"><%=keyName%></option>
							
							<%rsDisc.moveNext
							loop							
							
							%>
							</select></td>
					</tr>
										<tr>
						<td>Favorite Projects:</td>
						<td><select name="search_favorites">
						<option value="">All Projects</option>
						<option value="1">Favorites Only</option?
						<option value="0">Non Favorites Only</option?
							</select></td>
					</tr>
					<tr>
						<td>Project region:</td>
						<td><select name="search_region">
							<option value="">All</option>
							<option value="Northeast">Northeast</option>
							<option value="Northwest">Northwest</option>
							<option value="Southeast">Southeast</option>
							<option value="Southwest">Southwest</option>
						    </select>
						</td>
					</tr>
					<tr>
						<td>Project state:</td>
						<td><select name="search_state">
								<option value="">All</option>
								
								<option value="AK">AK</option>
								<option value="AL">AL</option>
								<option value="AR">AR</option>
								<option value="AZ">AZ</option>

								<option value="CA">CA</option>
								<option value="CO">CO</option>
								<option value="CT">CT</option>
								<option value="DC">DC</option>
								<option value="DE">DE</option>
								<option value="FL">FL</option>

								<option value="GA">GA</option>
								<option value="HI">HI</option>
								<option value="IA">IA</option>
								<option value="ID">ID</option>
								<option value="IL">IL</option>
								<option value="IN">IN</option>

								<option value="KS">KS</option>
								<option value="KY">KY</option>
								<option value="LA">LA</option>
								<option value="MA">MA</option>
								<option value="MD">MD</option>
								<option value="ME">ME</option>

								<option value="MI">MI</option>
								<option value="MN">MN</option>
								<option value="MO">MO</option>
								<option value="MS">MS</option>
								<option value="MT">MT</option>
								<option value="NC">NC</option>

								<option value="ND">ND</option>
								<option value="NE">NE</option>
								<option value="NH">NH</option>
								<option value="NJ">NJ</option>
								<option value="NM">NM</option>
								<option value="NV">NV</option>

								<option value="NY">NY</option>
								<option value="OH">OH</option>
								<option value="OK">OK</option>
								<option value="OR">OR</option>
								<option value="PA">PA</option>
								<option value="RI">RI</option>

								<option value="SC">SC</option>
								<option value="SD">SD</option>
								<option value="TN">TN</option>
								<option value="TX">TX</option>
								<option value="UT">UT</option>
								<option value="VA">VA</option>

								<option value="VT">VT</option>
								<option value="WA">WA</option>
								<option value="WI">WI</option>
								<option value="WV">WV</option>
								<option value="WY">WY</option>
							</select>
</td>
					</tr>					
				</table>
			</td>
		</tr>
		<tr>
			<td valign="top">Fields to display: </td>
			<td>
			<input type="checkbox" name="number" value="1" /> Project Number<br />
			<input type="checkbox" name="client" value="1" /> Client<br />
			<input type="checkbox" name="address" value="1" /> Address<br />
			<input type="checkbox" name="city_state" value="1" /> City, State<br />
			<input type="checkbox" name="zip" value="1" /> ZIP<br />
			<input type="checkbox" name="desc" value="1" /> Brief Description<br />
			<%
			do while not rsUDF.eof
			itemTag = trim(rsUDF("item_tag"))
			%>
			
			<input type="checkbox" name="<%=itemTag%>" value="1" /> <%=rsUDF("item_name")%> <br />
			
			<%rsUDF.moveNext
			loop
			
			%>
			</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" value="Submit Report" /></td>
		</tr>
<input type="hidden" value="1" name="submit" />
</form>
</body>
</html>
