	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Capabilities</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" id="main_table">
			<tr>
				<td class="left_col_overview" valign="top">
					<div id="content_text">
					<small>Capabilities</small>
					<br /><br />
					<img src="images/title_capabilities.jpg" alt="capabilities" />
					<br /><br />
					Regardless of project type, Clark Builders Group can provide professional services from concept through design, development, preconstruction, and construction for new and renovation projects. Backed by a history of assured performance and a track record of ingenuity, Clark Builders Group leverages unmatched experience to offer owners value-added services through:
					<br /><br />
					<img src="images/preconstruction_services.jpg" alt="preconstruction services" /><br />
					Added value starts with our proven preconstruction process, which shortens schedules, generates cost savings, and minimizes changes.  <a href="capabilities_preconstruction.asp">See More &gt;&gt;</a><br /> 
					<br />
					<img src="images/general_contracting.jpg" alt="general contracting" /><br />
					Clark Builders Group provides clients with responsive leadership and consistency of management throughout each project's duration, working in partnership with owners, architects, subcontractors, and engineers to deliver successful projects.  <a href="capabilities_general_contracting.asp">See More &gt;&gt;</a><br />
					<br />
					<img src="images/title_design_build.jpg" alt="design build" /><br />
					Using a team of vendors and design experts, Clark's Design Build services offer the ability to efficiently plan and construct award-winning communities that compete with the finest residences on the market.  <a href="capabilities_design_build.asp">See More &gt;&gt;</a><br />
					<br />					
					<img src="images/sustainable_design.jpg" alt="sustainable design" /><br />
					Clark Builders Group commits itself to sustainable growth and design by encouraging the use of energy efficient products and recycling of construction materials.  <a href="capabilities_sustainable.asp">See More &gt;&gt;</a><br />
					<br />
					<img src="images/public_private_partnership.jpg" alt="public private partnership" /><br />
					Clark Builders Group offers local, state, and federal government agencies a solid track record of success on public/private partnerships.  <a href="capabilities_public_private.asp">See More &gt;&gt;</a><br />
					
					
					
					</div>
					
				</td>
				
				<td class="right_col_overview" valign="top">
				
					<img src="images/capabilities_side.jpg" alt="capabilities" />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
