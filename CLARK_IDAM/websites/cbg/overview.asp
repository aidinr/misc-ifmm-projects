	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Overview</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small>Overview</small>
					<br /><br />
					<img src="images/overview.jpg" alt="overview" />
					<br /><br />
					For more than a century, Clark has built a reputation for fair and equitable business dealings across the nation. Clark Builders Group is one of the country's premier builders of multifamily apartments, military housing, hotels, and senior living facilities. Clark provides high-quality, responsive service backed by a significant bonding capacity, and unsurpassed national resources. 
					<br /><br />
					As an affiliate of The Clark Construction Group, one of the nation's largest private general contractors, Clark Builders Group has access to more than 100 years of construction and real estate expertise. This affiliation with one of the nation's most experienced and respected general contractors, along with our relationship with Clark Realty Capital, a premier national developer, affords Clark Builders Group a range of valuable resources including outstanding financial strength and unmatched expertise in development, construction, investment, and asset management. 
					<br /><br />
					Constructing more than 6,000 homes and apartments a year in communities across the United States has positioned Clark Builders Group among the top multifamily general contractors in the industry. Clark Builders Group's professional construction team draws on the knowledge gained through building more than 50,000 housing units since 1992 to turn good ideas into great places to live.

					
					
					
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/overview_side.jpg" alt="Overview" />
				</td>

			</tr>
			<tr>
				<td class="left_col_overview">
					<!--#include file="includes\footer.asp" -->
					
				</td>
				
				<td class="right_col_overview">
				
					
				</td>

			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
