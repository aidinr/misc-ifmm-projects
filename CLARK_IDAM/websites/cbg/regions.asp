<!--#include file="includes\config.asp" -->


<html>
<head>
<title><%=sPageTitle%> - Portfolio</title>
	<!--#include file="includes\header.asp" -->
	<script type="text/javascript" src="js/swfobject.js"></script>
	
	

    <SCRIPT language="JavaScript" type="text/javascript" >
    <!--

    if (document.images)
    {
      preload_image_object = new Image();
      // set image url
      image_url = new Array();
      image_url[0] = "images/regions-map-ne.jpg";
      image_url[1] = "images/regions-map-nw.jpg";
      image_url[2] = "images/regions-map-se.jpg";
      image_url[3] = "images/regions-map-sw.jpg";

       var i = 0;
       for(i=0; i<=3; i++) 
       {
         preload_image_object.src = image_url[i];
         //alert('loading: ' + image_url[i]);
        }
         
    }
    
		function ChangeImageSource(ElementIDIn, SRCIn) 
		{

    	document.getElementById(ElementIDIn).src = SRCIn;
		}


    //-->
    </SCRIPT>     
  	
	
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content" style="height:100%;">

					<div id="content_text2" style="margin-left: 24px;">
					<small>Regions</small>
					<br /><br />
					
					
					<img src="images/regional_interactive_map.jpg" alt="regional interactive map" />
					
					<br />
					<br />
					<div>
						<center>
						<img id="regionsMap" name="regionsMap" src="images/regions-map.jpg" alt="" usemap="#imgmap2011817124442" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg');" style="border: none; border-style: none; border-width: 0px; outline-width: 0px; z-index: 0; margin-left: auto; margin-right: auto;
						" />
					
					
					
						
						
					<map id="imgmap2011817124442" name="imgmap2011817124442">
						<area onfocus="blur();" style="border: none; border-style: none; border-width: 0px; outline-width: 0px;"  onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-nw.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg');" shape="poly" alt="" title="" coords="51,3,189,31,300,35,324,38,347,46,334,63,360,73,375,84,373,119,352,120,351,132,346,149,331,147,300,150,299,159,235,158,229,142,145,136,138,118,113,117,110,146,13,170,6,146,3,127,3,109,10,83,28,33,28,7,35,4,47,11,52,15,49,5" href="region.asp?region=nw" target="" />
						<area onfocus="blur();" style="border: none; border-style: none; border-width: 0px; outline-width: 0px;"  onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-ne.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg');" shape="poly" alt="" title="" coords="348,65,359,56,369,48,372,58,379,56,396,56,405,59,409,65,407,70,414,75,417,91,416,97,425,92,428,104,424,113,420,122,432,126,444,121,454,107,465,97,491,89,497,65,518,59,533,48,533,48,540,19,553,15,564,33,571,43,569,55,554,65,550,73,549,89,555,99,539,109,534,123,524,134,518,152,514,173,517,188,512,207,494,229,482,217,466,211,456,212,434,219,427,217,437,208,451,200,451,189,438,193,447,179,438,170,429,164,418,161,405,172,395,184,381,188,378,197,372,205,361,216,357,205,316,208,310,206,306,197,228,198,232,158,303,159,298,148,343,151,348,145,351,130,348,120,374,120,377,118,376,87,374,77,368,80,353,70,345,64,348,63" href="region.asp?region=ne" target="" />
						<area onfocus="blur();" style="border: none; border-style: none; border-width: 0px; outline-width: 0px;"  onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-sw.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg');" shape="poly" alt="" title="" coords="18,169,110,143,112,116,141,122,140,135,235,147,230,201,217,195,216,268,174,267,151,265,153,274,141,272,116,266,78,244,77,236,56,243,49,227,41,212,18,199,18,188,10,173,13,164,15,165" href="region.asp?region=sw" target="" />
						<area onfocus="blur();" style="border: none; border-style: none; border-width: 0px; outline-width: 0px;"  onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-se.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg');" shape="poly" alt="" title="" coords="485,350,475,332,465,326,449,294,433,285,377,298,371,306,357,302,335,297,321,301,300,314,285,330,283,350,275,352,256,346,244,321,231,299,219,297,211,300,210,308,197,302,178,268,209,266,212,263,216,229,218,196,231,192,278,197,304,200,310,207,354,205,359,214,383,195,386,179,398,176,418,164,421,163,436,165,438,182,444,189,445,195,430,208,425,216,444,217,458,213,475,215,487,219,486,228,491,226,486,239,468,255,470,271,477,297,489,307,495,328,496,341,490,344,487,348" href="region.asp?region=se" target="" />	
					</map>
				</center>
					

				<a href="region.asp?region=nw">
					<img src="images/region-box-nw.jpg" alt="" border="0" onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-nw.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg');" 					
						style="  position: absolute; margin-top: -340px; margin-left: -10px; z-index:500; "		/>
				</a>



				<a href="region.asp?region=sw">
					<img src="images/region-box-sw.jpg" alt="" border="0" onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-sw.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg');" 					
						style="  position: absolute; margin-top: -130px; margin-left: 38px; z-index:500; "		/>
				</a>





				<a href="region.asp?region=ne">
					<img src="images/region-box-ne.jpg" alt="" border="0" onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-ne.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg');" 					
						style="  position: absolute; margin-top: -300px; margin-left: 764px; z-index:500; "		/>
				</a>						


							


				<a href="region.asp?region=se">
					<img src="images/region-box-se.jpg" alt="" border="0" onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-se.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg');" 					
						style="  position: absolute; margin-top: -90px; margin-left: 703px; z-index:500; "		/>
				</a>
						
						
						
				
				
				
						
						
				<a href="region.asp?region=nw" style="position: absolute; margin-top: -225px; margin-left: 9px; z-index:510; font-size: 11px;"
					onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-nw.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg'); onfocus="blur();"">
					View Local Offices &amp; Contacts &gt;&gt;</a>
				


				<a href="region.asp?region=sw" style="position: absolute; margin-top: -15px; margin-left: 57px; z-index:510; font-size: 11px;"
					onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-sw.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg'); onfocus="blur();"">
					View Local Offices &amp; Contacts &gt;&gt;</a>
				
				

				<a href="region.asp?region=ne" style="position: absolute; margin-top: -185px; margin-left: 783px; z-index:510; font-size: 11px;"
					onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-ne.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg'); onfocus="blur();"">
					View Local Offices &amp; Contacts &gt;&gt;</a>
				
				

				<a href="region.asp?region=se" style="position: absolute; margin-top: 25px; margin-left: 722px; z-index:510; font-size: 11px;"
					onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-se.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg'); onfocus="blur();"">
					View Local Offices &amp; Contacts &gt;&gt;</a>
					
					
					
				</div>					
						
					
					<br />
					
					<br />
					<br />
					<br />
					
					<br />
					
					
					<div style="width: 1000px; background-color: #998f5a; vertical-align: middle; text-align: center; padding-top: 4px; padding-bottom: 4px; position: inherit; margin-left: -24px; color: #ffffff;">
						
						<a href="region.asp?region=ne"><span style="color: #ffffff; font-weight: normal;" onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-ne.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg');">NORTHEAST</span></a> &nbsp; | &nbsp;
						<a href="region.asp?region=se"><span style="color: #ffffff; font-weight: normal;" onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-se.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg');">SOUTHEAST</span></a> &nbsp; | &nbsp;
						<a href="region.asp?region=sw"><span style="color: #ffffff; font-weight: normal;" onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-sw.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg');">SOUTHWEST</span></a> &nbsp; | &nbsp;
						<a href="region.asp?region=nw"><span style="color: #ffffff; font-weight: normal;" onmouseover="ChangeImageSource('regionsMap', 'images/regions-map-nw.jpg');" onmouseout="ChangeImageSource('regionsMap', 'images/regions-map.jpg');">NORTHWEST</span></a> 
						
						
						
					</div>				
					
					<!--#include file="includes\footer.asp" -->
				
			</div>

			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
