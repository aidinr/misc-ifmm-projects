	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Overview</title>
	<!--#include file="includes\header.asp" -->
	<script type="text/javascript" src="js/swfobject.js"></script>

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<form style="padding:0;margin:0;" method="get" action="portfolio_search.asp">
<div id="menu">

			<div id="nav">
				<div>
				<a href="overview.asp">	<span id="flashcontent"></span></a>
					<script type="text/javascript">
					// <![CDATA[
		
					var so = new SWFObject("btn_CRC_test.swf", "sotester", "100", "35", "9", "#FFFFFF");
					so.addParam("wmode","transparent");
					so.write("flashcontent");
		
					// ]]>
					</script>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" class="navigation_left"></td>
						<td align="left" class="navigation">			    
							 
							 <a href="overview_history.asp">HISTORY</a>
							 <a href="overview_project_types.asp">PROJECT TYPES</a>
							 <a href="overview_clients.asp">CLIENTS</a>
							 <a href="overview_people.asp">CAREERS</a>
							 <a href="overview_subcontracting.asp">SUBCONTRACTING</a>
							 
							 
						</td>
						<td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
					</tr>
					<tr>
						<td width="10" height="10" class="navigation_bottom_left"></td>
						<td height="10" class="navigation_bottom"></td>
						<td width="8" height="10" class="navigation_bottom_right"></td>
					</tr>
				</table>
				</div>
				
				<div>
				<a href="capabilities.asp"><img width="124" height="28" src="images/menu/menu_capabilities.jpg" /></a>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" class="navigation_left"></td>
						<td align="left" class="navigation">			    
							 
							 <a href="capabilities_preconstruction.asp">PRECONSTRUCTION SERVICES</a>
							 <a href="capabilities_general_contracting.asp">GENERAL CONTRACTING</a>
							 <a href="capabilities_design_build.asp">DESIGN BUILD</a>
							 <a href="capabilities_sustainable.asp">SUSTAINABLE BUILDING</a>
							 <a href="capabilities_public_private.asp">PUBLIC PRIVATE PARTNERSHIPS</a>
							 
						</td>
						<td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
					</tr>
					<tr>
						<td width="10" height="10" class="navigation_bottom_left"></td>
						<td height="10" class="navigation_bottom"></td>
						<td width="8" height="10" class="navigation_bottom_right"></td>
					</tr>
				</table>
				</div>
				
				<div>
				<a href="portfolio.asp"><img width="124" height="28" src="images/menu/menu_portfolio.jpg" /></a>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" class="navigation_left"></td>
						<td align="left" class="navigation">			    
							 
							 <a href="portfolio_cat.asp?cid=<%=DISC_MIX_USE%>">MIXED-USE</a>
							 <a href="portfolio_cat.asp?cid=<%=DISC_LUX_APT%>">LUXURY APARTMENTS</a>
							 <a href="portfolio_cat.asp?cid=<%=DISC_HIG_DEN%>">HIGH-DENSITY APARTMENTS</a>
							 <a href="portfolio_cat.asp?cid=<%=DISC_MIL_COM%>">MILITARY HOUSING</a>
							 <a href="portfolio_cat.asp?cid=<%=DISC_AFF_HOU%>">AFFORDABLE HOUSING</a>
							 <a href="portfolio_cat.asp?cid=<%=DISC_SEN_USE%>">SENIOR LIVING</a>
							 <a href="portfolio_cat.asp?cid=<%=DISC_MSC_ROH%>">RETAIL/OFFICE/HOSPITALITY</a>
							 
						</td>
						<td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
					</tr>
					<tr>
						<td width="10" height="10" class="navigation_bottom_left"></td>
						<td height="10" class="navigation_bottom"></td>
						<td width="8" height="10" class="navigation_bottom_right"></td>
					</tr>
				</table>
				</div>
				
				<div>
				<a href="news.asp"><img width="121" height="28" src="images/menu/menu_news.jpg" /></a>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" class="navigation_left"></td>
						<td align="left" class="navigation">			    
							 
							 <a href="news_archives.asp">PRESS RELEASES</a>
							 <a href="awards.asp">AWARDS</a>
							 <a href="rankings.asp">RANKINGS</a>							 
						</td>
						<td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
					</tr>
					<tr>
						<td width="10" height="10" class="navigation_bottom_left"></td>
						<td height="10" class="navigation_bottom"></td>
						<td width="8" height="10" class="navigation_bottom_right"></td>
					</tr>
				</table>
				</div>	
				
				<div>
				<a href="regions.asp"><img width="126" height="28" src="images/menu/menu_region.jpg" /></a>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" class="navigation_left"></td>
						<td align="left" class="navigation">			    
							 
							 <a href="region.asp?region=ne">NORTHEAST</a>
							 <a href="region.asp?region=nw">NORTHWEST</a>
							 <a href="region.asp?region=se">SOUTHEAST</a>
							 <a href="region.asp?region=sw">SOUTHWEST</a>
							 
						</td>
						<td class="navigation_right">&nbsp;<!--need content for borders to work--></td>
					</tr>
					<tr>
						<td width="10" height="10" class="navigation_bottom_left"></td>
						<td height="10" class="navigation_bottom"></td>
						<td width="8" height="10" class="navigation_bottom_right"></td>
					</tr>
				</table>
				</div>

				
			</div>
			
			<div class="menu_item_search"><form method="get" action="portfolio_search.asp"><input type="text" id="search_projects" name="search" value="SEARCH PROJECTS" onfocus="this.select();this.value=''"/></div>
			
			</div>
</form>
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small>Overview</small>
					<br /><br />
					<img src="images/overview.jpg" alt="overview" />
					<br /><br />
					Clark Realty is a national real estate firm specializing in investment, capital markets, development, construction, property management, advisory services, and asset management. Recent investments have focused on multifamily properties andmixed-use developments with a multifamily component.
					<br /><br />
					Clark Realty's portfolio features more than 30,000 residential units and over 200 properties across the country. Our extensive experience encompasses a wide range of project types.	
					<br /><br />
					Clark Realty is a national real estate firm specializing in investment, capital markets, development, construction, property management, advisory services, and asset management. Recent investments have focused on multifamily properties andmixed-use developments with a multifamily component.
					
					
					
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/overview_side.jpg" alt="Overview" />
				</td>

			</tr>
			<tr>
				<td class="left_col_overview">
					<!--#include file="includes\footer.asp" -->
					
				</td>
				
				<td class="right_col_overview">
				
					
				</td>

			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
