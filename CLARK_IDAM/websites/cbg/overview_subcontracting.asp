	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Subcontracting</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%"  id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="overview.asp">Overview</a> | Subcontracting</small>
					<br /><br />
					<img src="images/subcontracting.jpg" alt="overview" />
					<br /><br />
					An essential element of delivering a successful, quality project is the proper solicitation and management of subcontractors.  Clark Builders Group recognizes the importance of the local and national subcontracting community to our projects and the need to find the most qualified and cost effective solution for procuring each trade.  We have deep relationships with a large roster of experienced subcontractors and suppliers and are continually adding qualified companies to our bid list.  This ensures maximum exposure and results in the lowest possible prices for our clients.<br /><br />Clark's thorough planning procedures and effective management of the design and construction process enables subcontractors to reduce their actual costs to complete the work.  Furthermore, Clark's prompt payment of subcontractor and supplier invoices enables Clark to receive preferred prices and maximum performance from its subcontractors and suppliers.
					<br /><br />
					Clark Builders Group also takes pride in our commitment to the use of small, local, and minority-owned businesses. We actively seek out and retain such firms and encourage our other team members to do the same.  Clark has demonstrated this commitment through consistent subcontracting to LSDBE firms, as well as implementing innovative strategies to increase these firms' suitability to subcontract to Clark's standard.
					<br /><br />
					To become a Clark Builders Group qualified vendor, an authorized representative from your company must complete the Vendor Qualification Form. <a href="Vendor Qualification Form.pdf" target="_blank">Click here to download form</a>
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/subcontracting_side.jpg" alt="subcontracting" />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
