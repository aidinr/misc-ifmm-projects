	<!--#include file="includes\config.asp" -->
<%

Function getFileContents(strIncludeFile)
  Dim objFSO
  Dim objText
  Dim strPage


  'Instantiate the FileSystemObject Object.
  Set objFSO = Server.CreateObject("Scripting.FileSystemObject")


  'Open the file and pass it to a TextStream Object (objText). The
  '"MapPath" function of the Server Object is used to get the
  'physical path for the file.
  Set objText = objFSO.OpenTextFile(Server.MapPath(strIncludeFile))


  'Read and return the contents of the file as a string.
  getFileContents = objText.ReadAll

  objText.Close
  Set objText = Nothing
  Set objFSO = Nothing
End Function

	rid = prep_sql(request.querystring("region"))

	if(rid = "ne") then
	region_title = "Northeast"
	title_image = "northeast_region.jpg"
	offices_include = getFileContents("includes/ne.inc")
	elseif (rid = "nw") then
	region_title = "Northwest"
	title_image = "northwest_region.jpg"
	offices_include = getFileContents("includes/nw.inc")
	elseif (rid = "se") then
	region_title = "Southeast"
	title_image = "southeast_region.jpg"
	offices_include = getFileContents("includes/se.inc")
	elseif (rid = "sw") then
	region_title = "Southwest"
	title_image = "southwest_region.jpg"
	offices_include = getFileContents("includes/sw.inc")
	else
	Response.Redirect "http://www.clarkrealty.com/"
	end if
	


	set rsJobs=server.createobject("adodb.recordset")
	sql = "select TOP 3 * FROM ipm_jobs a, ipm_jobs_type b WHERE a.department = b.type_id AND SHOW = 1 AND POST_DATE < getdate() AND PULL_DATE > getdate() AND b.type_value = '" & region_title & "' ORDER BY NEWID(), Job_Title"
	rsJobs.Open sql, Conn, 1, 4
	
	set rsProjects=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_value a, ipm_project_field_desc b, ipm_project c, ipm_project_field_desc e, ipm_project_field_value f where c.projectid = f.projectid and e.item_id = f.item_id and e.item_tag = 'IDAM_PROJECT_CC_WEBSITE' and f.item_value = '1' and  a.item_id = b.item_id AND b.item_tag = 'IDAM_PROJECT_REGION' AND a.projectid = c.projectid AND c.publish = 1 AND c.favorite = 1 AND a.item_value = '" & region_title  & "' order by newid()"
	rsProjects.Open sql, Conn, 1, 4
	

	
%>
<html>
<head>
<title><%=sPageTitle%> - Region - <%=region_title%></title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">

			<table cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr>
				<td class="left_col_overview" style="background-image:url(images/region_bg.jpg);background-repeat:no-repeat;">
					<div id="content_text" style="padding-left:34px;">
					<small><a href="regions.asp">Regions</a> | <%=region_title%></small>
					<br /><br />
					<img src="images/<%=title_image%>" alt="overview" />
					<br /><br />
					

<%=offices_include%>


					</div>
					
				</td>
				
				<td class="right_col_overview">
				<div style="margin-left:25px;">
				<br /><br /><br />
					<img src="images/featured_projects.png" />
					<br /><br />
					<table cellpadding="0" cellspacing="0">
						

						<%
						if(rsProjects.recordCount > 0) then
						rsProjects.moveFirst
						i = 0
						do while not (rsProjects.eof or i = 5)
						%>

							<tr>
							<td align="left">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tr>	<td valign="top" align="left" width="140"><div style="background:url(images/bg_portfolio_photo_odd.jpg);width:149px;height:118px;background:white;"><a href="project.asp?pid=<%=rsProjects("projectID")%>"><img width="137" height="108" style="margin-left:6px;margin-top:5px;" src="<%=sAssetPath%><%=rsProjects("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=137&height=108&qfactor=<%=assetQFactor%>" alt="<%=rsProjects("name")%>"/></a></div></td>
										<td valign="top" align="left"><div style="margin-left:6px;"><small><b><%=rsProjects("name")%></b><br /><%=rsProjects("city")%>, <%=rsProjects("state_id")%></small></div></td>
									</tr>
								</table>
								<br />
							</td>
							</tr>	
						
						<%	
						rsProjects.moveNext
						i = i + 1
						loop
						end if
						%>

						

											
					</table>					
					
				</div>
					
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
