	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Public Private Partnership</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%"  id="main_table">
			<tr>
				<td class="left_col_overview" valign="top">
					<div id="content_text">
					<small>Capabilities | Public/Private Partnership</small>
					<br /><br />
					<img src="images/title_public_private_partne.jpg" alt="public private partnership" />
					<br /><br />
					
					
					<b>Sustainability Review</b><br />
					


Clark�s preconstruction services, green specification development, LEED� documentation, and knowledge of environmentally preferred materials and techniques provide clients with the expertise to create a quality project that minimizes the impact of modern development on the Earth�s natural resources. With an emphasis on early and open collaboration with the owner and all major project partners, Clark will identify sustainable design opportunities and make recommendations that consider environmental impact, cost effectiveness, and market factors.
					<br />
					<img src="images/green_council.jpg" style="float:right;" /><b>Green Building</b><br />
					Clark is a member of the U.S. Green Building Council (USGBC) and, having already completed eight certified structures, is among only a handful of contractors with significant experience in the construction of LEED� certified buildings. This experience provides tremendous value to owners seeking to obtain LEED� certification. <br />
					<br />
					<b>Low-Impact Design</b><br />
					Our firm boasts extensive experience mitigating environmentally challenging sites, leveraging low-impact construction techniques, and incorporating the best sustainable practices and technologies. Clark�s sustainable building practices lead to Gold SPiRiT-certified homes at its Fort Irwin project, which included environmentally-conscious design features such as solar-operated fans and increased insulation.<br /> 
					<br />
					<b>Recycling Efforts</b><br />
					Clark Builders Group commits itself to recycling efforts at each of its projects. As a result of these initiatives, the City of San Diego recognized Clark Realty with its Recycler of the Year Award for the reuse of 200,000 tons of construction materials at two of its local military projects.<br />

					
					
					</div>
					
				</td>
				
				<td class="right_col_overview" valign="top">
				
					<img src="images/sustainable_side.jpg" alt="capabilities" />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
