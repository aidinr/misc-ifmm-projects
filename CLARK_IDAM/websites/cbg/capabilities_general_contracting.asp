	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Capabilities</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%"  id="main_table">
			<tr>
				<td class="left_col_overview" valign="top">
					<div id="content_text">
					<small><a href="capabilities.asp">Capabilities</a> | Construction</small>
					<br /><br />
					<img src="images/title_construction.jpg" alt="general contracting" />
					<br /><br />
					Clark Builders Group provides clients with responsive leadership and consistency of management throughout each project's duration, working in partnership with owners, architects, subcontractors, and engineers to deliver successful projects. 
					<br /><br />
					<i>General Contracting services include:</i><br />
					<table cellspacing="3" cellpadding="3" width="575">
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Cost and quality control </td>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Project accounting </td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Schedule development and control  </td>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Shop drawings and material submittal review  </td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Subcontractor qualification and management </td>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Construction management</td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Field engineering and site management </td>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Quality control</td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Safety assurance  </td>
							<td></td>
						</tr>

					</table>
					<br />
					<b>Cost Control</b><br />
					Our overarching philosophy is to provide maximum value for the available budget. Clark uses value and life cycle cost analyses to determine the most efficient building systems and most cost effective finishes. Our quality control philosophy demands that standards are established early and maintained by the entire design and construction team throughout the building process.<br />
					<br />
					<b>Scheduling</b><br />
					Clark Builders Group uses precise planning and control techniques to manage each and every project. Critical Path Method (CPM) schedules and detailed project budgets are developed and consistently maintained as a means of monitoring daily priorities, steering progress, and controlling costs.<br />
					<br />
					<b>Project Management</b><br />
					Once the construction phase begins, Clark Builders Group assigns an experienced project management team to monitor and control every aspect of the project and adhere to schedule, budget, quality, and safety goals.<br />
					<br />
					<b>Safety</b><br />
					Clark is dedicated to working safely and strives to achieve "Zero Accidents" on every project we build. In addition to enforcing OSHA regulations, Clark provides ongoing training and leadership to our staff and labor force as needed to ensure a safe work site.<br />

				
					
					
					</div>
					
				</td>
				
				<td class="right_col_overview" valign="top">
				
					<img src="images/general_contracting_side.jpg" alt="preconstruction" />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
