<!--#include file="includes\config.asp" -->

<%
	cid = prep_sql(request.querystring("cid"))
	title = ""
	title_image = ""
	type_description = ""
	if (cid = clng(DISC_MIX_USE)) then 
	title = "Mixed-Use Communities"
	title_image = "title_mixed_use_communities.jpg"
	type_description = "Clark Builders Group understands what brings life and vibrancy to a quality mixed-use development and has answered this new market demand with several pioneering construction projects."
	elseif (cid = clng(DISC_LUX_APT)) then
	title = "Luxury Apartments"
	title_image = "title_luxury_apartments.jpg"
	type_description = "Clark Builders Group constructs many styles of award-winning luxury and market-rate apartments, including mid-rise communities, high-rise residential towers, mixed-use developments, and ""A+"" rental townhomes."
	elseif (cid = clng(DISC_MIL_COM)) then
	title = "Military Housing"
	title_image = "title_military_housing.jpg"
	type_description = "Clark Builders Group is proud to be the nation's preeminent builder of quality housing for members of the Army, Navy, Air Force, Marines and their families. In our state-of-the-art military neighborhoods, thoughtful amenities come together with inspired design to give military personnel and their families a high-morale environment and a sense of community."
	elseif (cid = clng(DISC_SEN_USE)) then
	title = "Senior Living"
	title_image = "title_senior_living.jpg"
	type_description = "Clark Builders Group�s experience encompasses a broad spectrum of assisted- and independent-living communities.  We bring tremendous experience tailoring construction methods, materials, and safety considerations for this unique demographic."
	elseif (cid = clng(DISC_HIG_DEN)) then
	title = "High-Density Apartments"
	title_image = "title_high_density_apartmen.jpg"
	type_description = "Through efficient and effective infrastructures and streamlined construction processes, Clark Builders Group has met the challenges of building on tight sites and in high-traffic urban areas."
	elseif (cid = clng(DISC_AFF_HOU)) then
	title = "Affordable Housing"
	title_image = "title_affordable_housing.jpg"
	type_description = "Clark Builders Group understands the challenges of delivering a high-quality product under the strict budgetary constraints of affordable housing programs.  We take great pride in helping owners navigate the complex requirements of such programs and unlocking the value of their assets to ultimately improve quality of life for residents of these communities."
	elseif (cid = clng(DISC_MSC_ROH)) then
	title = "Retail/Office/Hospitality"
	title_image = "title_retail_office_hospitality.jpg"
	type_description = "Touching upon every market, Clark Builders Group complements its portfolio with wood-framed retail, office, and hospitality properties."	


	elseif (cid = clng(DISC_SUS_BUI)) then
	title = "Sustainable Building"
	title_image = "title_sustainable_building.jpg"
	type_description = "Clark Builders Group boasts an expansive portfolio of innovative and award-winning projects that set the standard for environmental accountability within our industry."	




	end if

	set rsProjectsByCat=server.createobject("adodb.recordset")
	sql="select top 6 * from ipm_project, ipm_project_discipline,ipm_project_field_desc c,ipm_project_field_value d where ipm_project.projectid = d.projectid and c.item_id = d.item_id and c.item_tag = 'IDAM_PROJECT_CC_WEBSITE' and d.item_value = '1' and ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND keyid = " & cid & " order by newid()"
	rsProjectsByCat.Open sql, Conn, 1, 4
	
	set rsRandomFeaturedByCat = server.createobject("adodb.recordset")
	sql="select top 1 * from ipm_project, ipm_project_discipline,ipm_project_field_desc c,ipm_project_field_value d where ipm_project.projectid = d.projectid and c.item_id = d.item_id and c.item_tag = 'IDAM_PROJECT_CC_WEBSITE' and d.item_value = '1' and ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND favorite = 1 AND keyid = " & cid & " order by newid()"
	rsRandomFeaturedByCat.Open sql, Conn, 1, 4
%>

<html>
<head>
<title><%=sPageTitle%> - Portfolio</title>
	<!--#include file="includes\header.asp" -->
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" id="main_table">
			<tr>
				<td class="left_col">
					<div id="content_text">
					<small><a href="portfolio.asp">Portfolio</a> | <%=title%></small>
					<br /><br />
					<img src="images/<%=title_image%>" alt="<%=title%>" />
					<br /><br />
					<%=type_description%>
					
					<table  cellpadding="8" cellspacing="8" width="100%">
						<tr>

						<%
						z = 1
						do while not rsProjectsByCat.eof
						%>


							<td valign="top" width="50%">
								<table width="100%">
									<tr>	<td valign="top" align="left" width="50%"><div class="portfolio_bg"><a href="project.asp?pid=<%=rsProjectsByCat("projectID")%>"><img  width="137" height="108" class="portfolio_photo" src="<%=sAssetPath%><%=rsProjectsByCat("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=137&height=108&qfactor=<%=assetQFactor%>" alt="<%=trim(rsProjectsByCat("name"))%>"/></a></div></td>
										<td valign="top" align="left" width="50%"><b><%=trim(rsProjectsByCat("name"))%></b><br /><%=rsProjectsByCat("city")%>, <%=rsProjectsByCat("state_id")%></td>
									</tr>
								</table>								
							</td>
						
						<%	
						rsProjectsByCat.moveNext
						if (z mod 2 = 0) then response.write "</tr><tr>"
						z = z + 1
						loop						
						%>

						

						</tr>						
					</table>
					
					<br />
					
					<a href="portfolio_all.asp?cid=<%=cid%>">See All &gt;&gt;</a>
					
					
					</div>
					
					
					
				</td>
				
				<td class="right_col">
				<% do while not rsRandomFeaturedByCat.eof %>
				<br /><br /><br />
					<div style="margin-left:65px;">
					<img src="images/featured_project.png" />
					</div>
				
					<div id="portfolio_side_bg">
						<a href="project.asp?pid=<%=rsRandomFeaturedByCat("projectID")%>"><img src="<%=sAssetPath%><%=rsRandomFeaturedByCat("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=2&qfactor=<%=assetQFactor%>" alt="featured" id="portfolio_side_image" /></a>
					</div>
					<br /><br />
					<div id="portfolio_side_text">
					<b><%=rsRandomFeaturedByCat("name")%></b><br />
					<%=rsRandomFeaturedByCat("description")%> <br />


<%

if ltrim(rtrim(cid)) = DISC_SUS_BUI then
%>
					<a href="project.asp?pid=<%=rsRandomFeaturedByCat("projectID")%>"><font color="#4d7d52">Building Green at <%=rsRandomFeaturedByCat("name")%>  &gt;&gt;</font></a>
<% else %>

					<a href="project.asp?pid=<%=rsRandomFeaturedByCat("projectID")%>">More &gt;&gt;</a>
<% end if %>




					</div>
				<% 
				rsRandomFeaturedByCat.moveNext
				loop
				%>
				</td>
			</tr>
			<tr>
				<td class="left_col">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col">					
				</td>
			</tr>			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
