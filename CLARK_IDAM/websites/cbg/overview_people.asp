	<!--#include file="includes\config.asp" -->
<%

	set rsJobs=server.createobject("adodb.recordset")
	sql = "select TOP 3 * FROM ipm_jobs a, ipm_jobs_type b WHERE a.department = b.type_id AND SHOW = 1 AND POST_DATE < getdate() AND PULL_DATE > getdate() ORDER BY NEWID()"
	rsJobs.Open sql, Conn, 1, 4
%>
<html>
<head>
<title><%=sPageTitle%> - People</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="overview.asp">Overview</a> | Careers</small>
					<br /><br />
					<img src="images/careers.jpg" alt="overview" />
					<br /><br />
					Clark Builders Group continually looks for and hires top candidates, providing them with the opportunity to learn and grow. Our culture is based on the tenets of teamwork, keeping it simple, pushing boundaries, facing reality, and having fun.
					<br /><br />
					<b>Culture</b><br />
					Clark Builders Group combines the resources of a large, national firm with the attitude and culture of a small entrepreneurial business to provide employees with a unique working environment. New employees interact with and learn from experienced employees, while taking on many responsibilities early on in their career. These employees get to take a hands-on approach to problem solving, and they are trusted to manage their own time and workload. In short, CBG gives employees the tools and latitude to perform to their highest potential.
					<br /><br />
					
					<b>Career Development</b><br />
					
					Clark Builders Group offers employees a rotation program which includes nine-to-twelve month rotations in field engineering, project management, and estimating. This program affords new employees an opportunity to work in several operational areas throughout the company and develop an understanding of the entire construction process.
					<br /><br />
					<b>Continuing Education</b><br />
					Clark Builders Group also provides employees with a comprehensive development program through the Clark Corporate University. Through educational courses and seminars, employees have the opportunity to supplement on-the-job training. 
					<br /><br />
					<b>Networking</b><br />
					Team CBG, a new program specifically targeted to emerging leaders in the company, fosters social interaction among peers while providing additional insight to the construction industry. Company-funded events have included tours of the new Washington Nationals baseball stadium, hockey games, window factory tours, and trips to NASCAR races.
					<br /><br />
					<b>Benefits</b><br />
					Clark Builders Group offers a top-notch benefits package that includes outstanding health and wellness benefits, an educational assistance program, and paid vacation and holidays. Profit sharing gives employees a stake in the company's success, and a 401(k) savings plan and company matching contributions supplement competitive merit-based pay.
					
					
					
					</div>
				
				</td>
				
				<td class="right_col_overview">
				<br /<br /><br />
					
					<div style="width:330px;padding-left:20px;">
					<b>CAREER OPPORTUNITIES AT CLARK BUILDERS GROUP</b>
					<br /><br />
					<b>Field Supervision</b>
					<br /><br />
					Senior Superintendents, Superintendents and Assistant Superintendents oversee field operations to ensure that self-performed and subcontracted work is in compliance with contract documents and on schedule. They also enforce safety and quality control policies among Clark employees and subcontractors. Qualifications include five years of experience constructing multi-family, apartment or townhome communities.
					<br /><br />
					<b>Project Management</b>
					<br /><br />
					Much like running a company, a Project Executive and Project Engineer are in charge of supervising the business aspects of a construction project. This involves overseeing contract activities on a particular jobsite, such as communication with the owner and architect, submittals, procurement, and budgeting and schedule. Qualified candidates would hold a bachelor's degree in Engineering, Construction Management, or a related discipline and have two or more years of applicable experience.
					<br /><br />
					<b>Contract Management</b>
					<br /><br />
					A Contract Executive has multi-project responsibilities and is responsible for project planning, staffing, budgeting, purchasing, problem solving, and project cost control. Developing and maintaining positive relationships with subcontractors, suppliers, and owners is critical to this role. Qualifications include a bachelor's degree in Engineering, Construction Management, or a related field and extensive applicable experience in construction management.
					<br /><br />
					To view available positions by region, <a href="regions.asp">click here</a>.
					<br /><br />
					To apply online, <a href="overview_form.asp">click here</a>.
				</div>
				
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
