	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Capabilities</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%"  id="main_table">
			<tr>
				<td class="left_col_overview" valign="top">
					<div id="content_text">
					<small><a href="capabilities.asp">Capabilities</a> | Preconstruction Services</small>
					<br /><br />
					<img src="images/title_preconstruction_servi.jpg" alt="preconstruction services" />
					<br /><br />
					Clark's clients benefit from our many years of experience and billions of dollars worth of multifamily construction projects. Added value starts with our proven preconstruction process, which shortens schedules, generates cost savings, and minimizes changes.
					<br /><br />
					<b>Early Involvement</b><br />
					Working in cooperation with clients and design teams during the early planning stages allows Clark Builders Group to realize project goals and exceed owner expectations. Clark's team approach to preconstruction includes input from experts experienced in mechanical and electrical systems, foundations, structural detailing, scheduling, and purchasing to ensure that all critical items are proactively addressed before construction starts. Our project teams become involved during the planning stages of projects, and continue working on these projects through completion of construction. This ensures continuity between the preconstruction and construction phases, so that decisions made during the planning stages are properly implemented in the field. The end result is that Clark's preconstruction expertise on pricing, scheduling, materials, building systems and construction methods deliver projects of best value for clients. 
					<br /><br />
					<b>Value-Added Services</b><br />
					<i>Clark's preconstruction services include:</i><br />
					

					<table cellspacing="3" cellpadding="3" width="575" border="0">
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Feasibility studies</td>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Construction planning </td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Early budget development </td>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Site management planning </td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Pricing updates</td>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Scheduling </td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top"> Value engineering </td>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Excavation/sheeting & shoring planning	</td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top"> Systems analyses/selection </td>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Development of a Guaranteed Maximum Price</td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Construction document review/coordination </td>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Identification and purchasing of long lead items </td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Quality control </td>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Subcontractor selection and qualification</td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Cash flow projections 	</td>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">  Advice on material availability and quality </td>
						</tr>
					</table>

				
					
					
					</div>
					
				</td>
				
				<td class="right_col_overview" valign="top">
					<img src="images/preconstruction_side.jpg" alt="preconstruction" />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
