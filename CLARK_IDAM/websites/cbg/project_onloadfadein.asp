	<!--#include file="includes\config.asp" -->
<%
	projectID = prep_sql(request.querystring("pid"))
	
	set rsProject=server.createobject("adodb.recordset")
	sql="select * from ipm_project where AVAILABLE = 'Y' AND projectid = " & projectID
	rsProject.Open sql, Conn, 1, 4
	
	if (rsProject.recordcount = "0") then
		Response.Redirect "http://www.webarchives.com"
	end if	
	if not (rsProject("publish") = "1") then
		Response.Redirect "http://www.webarchives.com"
	end if
	
	set rsAssets=server.createobject("adodb.recordset")
	sql = "select * from ipm_asset where AVAILABLE = 'Y' AND securitylevel_id >= 0 AND projectid = " & projectID
	rsAssets.Open sql, Conn, 1, 4
	
	set rsArchitect=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & projectID  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_ARCHITECT'"
	rsArchitect.Open sql, Conn, 1, 4
	
	set rsCompletion=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & projectID  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_COMPLETION_DATE'"
	rsCompletion.Open sql, Conn, 1, 4


%>
<html>
<head>
<title><%=sPageTitle%> - Project</title>
	<!--#include file="includes\header.asp" -->
	
<script type="text/javascript">

var imageGalleryArray = new Array();

<%
	dim z
	z = 1
	
	response.write "imageGalleryArray[" & 0 & "] = """ & sAssetPath & projectID & "&Instance=" & siDAMInstance & "&type=project&size=1&width=625&height=491&qfactor=" & assetQFactor & """;" & vbcrlf

	do while not rsAssets.eof
		response.write "imageGalleryArray[" & z & "] = """ & sAssetPath & rsAssets("asset_id") & "&Instance=" & siDAMInstance & "&type=asset&size=1&width=625&height=491&qfactor=" & assetQFactor & """;" & vbcrlf
		rsAssets.moveNext
		z = z + 1
	loop

%>

function switchImage(i) {
	imageGallerySize = imageGalleryArray.length;
	prevLink = document.getElementById('prev_arrow');
	nextLink = document.getElementById('next_arrow');
	imageText = document.getElementById('project_image_text');
	document.getElementById('project_image').src=imageGalleryArray[i-1];
	document.getElementById('project_image').style.visibility = 'hidden';
	document.getElementById('project_image').onload = function() {
	fadeIn('project_image',0);
	}
	nextImage = i + 1;
	prevImage = i - 1;
	if(i==1) {
		prevLink.href = "#";
		nextLink.href = "javascript:switchImage(2)";
	}
	else if (i == imageGallerySize) {
		prevLink.href = "javascript:switchImage(" + prevImage + ")";
		nextLink.href = "#"
	}
	else {
		prevLink.href = "javascript:switchImage(" + prevImage +  ")";
		nextLink.href = "javascript:switchImage(" + nextImage +  ")";
	}
	imageText.innerHTML = "IMAGES " + i +  " / " + imageGallerySize;
	
}

function setOpacity(obj, opacity) {
  opacity = (opacity == 100)?99.999:opacity;
  
  // IE/Win
  obj.style.filter = "alpha(opacity:"+opacity+")";
  
  // Safari<1.2, Konqueror
  obj.style.KHTMLOpacity = opacity/100;
  
  // Older Mozilla and Firefox
  obj.style.MozOpacity = opacity/100;
  
  // Safari 1.2, newer Firefox and Mozilla, CSS3
  obj.style.opacity = opacity/100;
}


function fadeIn(objId,opacity) {
  if (document.getElementById) {
    obj = document.getElementById(objId);
    obj.style.visibility = 'visible';
    if (opacity <= 100) {
      setOpacity(obj, opacity);
      opacity += 10;
      window.setTimeout("fadeIn('"+objId+"',"+opacity+")",25);
    }
  }
}


</script>
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
						<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr>
				<td id="left_col_project">
					
					<div style="background: white;">
					<img style="visibility:'hidden;" onload="fadeIn('project_image',0);" width="625" height="491" src="<%=sAssetPath%><%=projectID%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=625&height=491&qfactor=<%=assetQFactor%>" id="project_image" />
					<div id="project_image_mask"><img src="images/project_image_mask.png" /></div>
					</div>
					<div id="project_image_navigation">
						<div class="project_image_navigation"><img src="images/see_next_project.jpg" alt="See next project" /></div>
						<div class="project_image_navigation" id="project_image_navigation_arrows_bg">
							<div id="project_image_navigation_arrows"><table align="right"><tr><td><a href="#" id="prev_arrow"><img src="images/prev_project_arrow.png" alt="Previous Image" /></a></td><td valign="middle"> <span id="project_image_text"> IMAGES 1/<%=rsAssets.recordcount + 1%> </span></td><td> <a href="javascript:switchImage(2)" id="next_arrow"><img src="images/next_project_arrow.png" alt="Next Image" /></a></td></tr></table></div>
						</div>
					</div>
					<br />
					<div id="content_text">
					<table cellpadding="0" cellspacing="0" width="93%">
						<td align="left" valign="top" style="line-height:20px;">
						<b>RELATED PROJECTS</b><br />
						<a href="#">Ashland</a><br />
						<a href="#">The Clarendon</a><br />
						<a href="#">Pinnacle Town Center</a><br />
						
						</td>
						<td align="right" valign="top">
						<img src="images/progress_photos.jpg" alt="progress photos" /><br />
						<img src="images/printable_fact_sheet.jpg" alt="progress photos" /><br />
						<img src="images/testimonial.jpg" alt="progress photos" /><br />
						<br />
						<b><a href="<%=rsProject("url")%>"><%=rsProject("url_caption")%></a></b>
						</td>
					</table>
					<br />
					
					</div>
					
					<!--#include file="includes\footer.asp" -->
				</td>
				
				<td id="right_col_project">
				
					<div style="margin-left:10px;padding:10px;">
					<small>Search Results | Portfolio | Luxury Apartments | <%=rsProject("name")%></small>
					<br /><br />
					<b><big><%=rsProject("name")%></big><br />
					<%=rsProject("clientName")%></b><br /><br />
					<%=rsProject("descriptionmedium")%>
					<br /><br /><br />
					<b>PROJECT DATA</b><br />
					Address: <%=rsProject("address")%><br />
					<%=rsProject("city")%>, <%=rsProject("state_id")%> <%=rsProject("zip")%><br />
					<b>Architect:</b> <%=rsArchitect("item_value")%><br />
					<b>Completion:</b> <%=rsCompletion("item_value")%><br />
					<br />
					<b>PRESS RELEASES</b><br />
					<a href="#">September 14, 2006: Clark Builders Starts Lorton Town Center in Virginia</a><br />
					<br />
					<b>RECOGNITION</b><br />
					<a href="#">2007 Monument Award Finalist<br />
					See More&gt;&gt;</a>
					</div>
				</td>
			</tr>
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
