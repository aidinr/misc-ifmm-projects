<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!--#include file="includes\config.asp" -->
<%

	'set rsAssets=server.createobject("adodb.recordset")
	'sql = "select top 1 * from ipm_asset a,ipm_asset_category b where a.available = 'y' and a.securitylevel_id = 3 AND a.category_id = b.category_id and b.name = 'web testimonials' order by newid()"
	'sql = prep_sql(sql)
	'rsAssets.Open sql, Conn, 1, 4
	
	set rsAssets=server.createobject("adodb.recordset")
	sql = "select asset_id,a.name,a.description  from ipm_asset a,ipm_asset_category b where b.name = 'Testimonials' and a.projectid = b.projectid and a.projectid = 21763410 and a.category_id = b.category_id and a.available = 'y' and a.securitylevel_id = 3"
	rsAssets.Open sql, Conn, 1, 4
%>
<html>
<head>
<title><%=sPageTitle%> - Clients</title>
	<!--#include file="includes\header.asp" -->
<link rel="stylesheet" href="css/thickbox.css" type="text/css" media="screen" />
<script src="js/thickbox.js" type="text/javascript"></script>	
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%"  id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="overview.asp">Overview</a> | Clients</small>
					<br /><br />
					<img src="images/clients.jpg" alt="overview" />
					<br /><br />
					Our experience has taught us that the right partner can make all the difference in getting a job done. As a testament to our company-wide commitment to delivering high-quality projects on-time and on-budget, Clark has earned a large number of third-party contracts from repeat customers representing both national firms such as Camden Properties and regional developers such as Kettler, Elm Street, and RST Development. 
					<br /><br />
					When clients partner with us, they're working with a team who can look beyond just construction issues. We understand the full development process from assembling sites and obtaining financing to building, leasing, and selling. Clark Builders Group leverages past experience to help our clients spot life cycle issues, stay abreast of trends in products and materials, and better understand today's rapidly changing market conditions.
					<br /><br />
					Each day, our team works tirelessly to exceed each project's distinct goals and fulfill each client's unique service needs. Our people constantly demonstrate their commitment to a project, often displaying a steely core and dogged determination to see a job through. We take great pride in creating places of enduring quality and value, ultimately delivering lasting benefits for our clients.
					
					<div id="testimonial_videos" align="right">
					<%do while not rsAssets.eof%>
					<a class="thickbox" target="_blank" href="view_video.asp?id=<%=rsAssets("asset_id")%>&title1=<%=rsAssets("name")%>&title2=<%=rsAssets("description")%>&KeepThis=true&TB_iframe=true&height=350&width=410&modal=true"><img src="images/view_testimonial.jpg" alt="view_testimonial" /></a><br />
					<%
					rsAssets.movenext
					loop
					%>
					</div>					
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<div id="overview_testimonial">
					<img src="images/clients_side.jpg" alt="Client Testimonial" />
					</div>
					<div id="overview_testimonial_client">
					
					</div>
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
