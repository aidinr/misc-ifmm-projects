<!--#include file="includes\config.asp" -->


<html>
<head>
<title><%=sPageTitle%> - Portfolio</title>
	<!--#include file="includes\header.asp" -->
	<script type="text/javascript" src="js/swfobject.js"></script>
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content" style="height:100%;">

					<div id="content_text">
					<small>Regions</small>
					<br /><br />
					<img src="images/regional_interactive_map.jpg" alt="regional interactive map" />
					<br /><br />					
					</div>
					<div id="flashcontent">
					</div>
				
					<script type="text/javascript">
						// <![CDATA[
						
						var so = new SWFObject("map.swf", "sotester", "1000", "450", "9", "#FFFFFF");
						so.useExpressInstall('expressinstall.swf');
						so.addParam("wmode","transparent");
						so.write("flashcontent");
						
						// ]]>
					</script>					
					
					<!--#include file="includes\footer.asp" -->
				
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
