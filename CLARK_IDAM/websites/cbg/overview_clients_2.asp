	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - History</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr>
				<td id="left_col_overview">
					<div id="content_text">
					<small>Overview | Clients</small>
					<br /><br />
					<img src="images/clients.jpg" alt="overview" />
					<br /><br />
					As a testament to our company-wide commitment to delivering high-quality projects on-time and on-budget, Clark has earned a large number of third-party contracts from repeat customers representing both national firms such as Archstone Communities, and regional developers such as Kettler and RST development. Our team works tirelessly to exceed each project�s distinct goals and fulfill each client�s unique service needs.
					<br /><br />
					Clark Builders Group is the nation's third largest multifamily builder and is consistently ranked among the top ten nationally. Clark�s projects have earned some of the most prestigious accolades in the industry. 
					<br /><br />
					<ul style="list-style-image: url(images/ul.jpg);">
						<li>Promote Teamwork</li>
						<li>Keep it Simple</li>
						<li>Push Boundaries </li>
						<li>Face Reality </li>
						<li>Have Fun </li>
					</ul>
					<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
					<!--#include file="includes\footer.asp" -->
					</div>
					
				</td>
				
				<td id="right_col_overview">
				
					<img src="images/history_side.jpg" alt="Overview" />
				</td>
			</tr>

			
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
