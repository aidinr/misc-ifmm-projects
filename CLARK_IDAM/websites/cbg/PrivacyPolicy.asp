	<!--#include file="includes\config.asp" -->
<html>
<head>
<title><%=sPageTitle%> - Privacy Policy</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr>
				<td class="left_col_overview" >
					<div id="content_text" style="padding-left:34px;">
					<br />
					<img src="images/privacy_policy.jpg" alt="" />
					
				

					<br />
					<br />
					<b><u>Conditions and Terms of Use</u>: </b>
					<br />
					The information contained in this Web site is provided to you by Clark Builders Group LLC, its affiliates and subsidiaries (hereinafter referred to as �CBG� for your convenience). Your use and access to this Web site, or any of the information herein, is conditioned upon your unqualified acceptance of the following provisions.

					<br/ >
					<br />


					<b><u>Scope</u>: </b>
					<br />
					Your use of this information shall be solely for personal and noncommercial uses and shall not be for any unlawful or prohibited use. CBG reserves the right in its sole discretion, without notice, to deny access to this Web site or any of its information. The information provided, or any portion thereof, shall not be altered, published or sold for any purpose. The content (and compilation thereof) included on this Web site is the property of CBG or the suppliers thereof and protected by U.S. and international copyright laws. CBG reserves the right to remove or modify, without notice, any portions of this Web site and your continued use of the Web site constitutes your acceptance of such modifications.

					<br/ >
					<br />

					<b><u>"AS IS", No Warranty or Guarantee</u>:</b>
					<br /> Your use of this Web site is at your sole risk. The information contained in this Web site is provided to you �as is�, �with all faults�, �subject to all defects� and without any guarantees, representations or warranties of any kind, expressed or implied, including but not limited to warranties of merchantability, fitness for a particular purpose, accuracy, completeness, reliability, effectiveness, or that the use hereof will be error free or uninterrupted. In no event shall CBG, its managers, members, officers, directors, employees or agents be liable for any direct, indirect, consequential, special or punitive damages arising out of your use, access (or inability to access) or any defect in the Web site or the content or graphics contained herein whether resulting from loss of use, data, revenue, contracts or profits and whether in an action in contract, tort, strict liability or otherwise whether or not CBG, its managers, members, shareholders, partners, officers, directors, employees or agents 					knew or should have known of the likelihood of such damages.

					<br/ >
					<br />

					<b><u>Links, Advertisements, Coupons and Discounts</u>: </b>

					<br />
					This Web site may contain links to Web sites operated by other parties or advertisements, coupons or discounts relating to other matters or items. Such links, advertisements, coupons or discounts are provided for your general information and any use thereof shall be at your sole risk. CBG makes no representation, warranty or guaranty with respect thereto including but not limited to the accuracy, completeness, effectiveness or benefit thereof and CBG, its managers, members, officers, directors, employees or agents shall have no liability whatsoever with respect thereto. By including such links, advertisements, coupons, and discounts, CBG does not make any endorsements thereof. 
 
					<br />
					<br />
		




					</div>
					
				</td>
				
				<td class="right_col_overview">
				<div>
				<img src="images/bg_contact.jpg" alt="CBG" />
				</div>
					
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
