	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Capabilities</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="capabilities.asp">Capabilities</a> | Design Build</small>
					<br /><br />
					<img src="images/design_build.jpg" alt="design build" />
					<br /><br />
					Public entities and private developers alike are using innovative partnering solutions more and more to attain an advantage in today's competitive marketplace.  In response, Clark Builders Group offers superior services to its clients through its Design Build services. 
					<br /><br />
					Clark's team offers a depth of knowledge and expertise that is unrivaled in our industry.  With over 100 years of affiliated experience building over 50,000 housing units to date and 6,000 more each year, our staff provides unique insight into proven designs and current market trends.  Because a portion of our construction volume is for related entities, Clark clearly understands the development process.  We look beyond the sticks and bricks to consider our partner's entire development and property management process.  
					<br /><br />
					Through its role in Design Build relationships, Clark Builders Group provides guidance to developers and design teams by evaluating specifications and project plans and identifying areas for potential enhancement.  At the request of our partners, we perform constructability reviews, detailed budgeting, and product analysis to offer site-specific design solutions.  We recognize the benefit of harnessing the combined knowledge of the entire project team to:
					<br /><br />
						<table cellspacing="3" cellpadding="3" width="100%" style="margin-left:30px;">
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Develop complete plans, free from errors and gaps</td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Prepare budgets reflecting the full cost of construction and development</td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Anticipate site-specific challenges to avoid schedule delays </td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Prevent rework and unnecessary changes to achieve a low total cost</td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Increase value, quality, and marketability through selection of current materials and technologies</td>
						</tr>
						</table>
					<br />
					Our partners have found that they can depend upon Clark to diligently pursue their interests while they concentrate on other project demands. Every Clark-designed project encompasses a holistic approach to value that anticipates start-up cost, long-term maintenance, and end-user needs.  

					
					
					
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/side_design_build.jpg" alt="Design Build" />
				</td>

			</tr>
			<tr>
				<td class="left_col_overview">
					<!--#include file="includes\footer.asp" -->
					
				</td>
				
				<td class="right_col_overview">
				
					
				</td>

			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
