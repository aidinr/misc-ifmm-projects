	<!--#include file="includes\config.asp" -->
<%
	projectID = prep_sql(request.querystring("pid"))
	set rsProject=server.createobject("adodb.recordset")
	sql="select * from ipm_project where AVAILABLE = 'Y' AND projectid = " & projectID
	rsProject.Open sql, Conn, 1, 4
	
	if (rsProject.recordcount = "0") then
		Response.Redirect "http://www.clarkrealty.com"
	end if	
	if not (rsProject("publish") = "1") then
		Response.Redirect "http://www.clarkrealty.com"
	end if
	
	set rsNews=server.createobject("adodb.recordset")
	sql = "select TOP 3 * FROM ipm_news, ipm_news_related_projects where ipm_news.type = 0 AND ipm_news.news_id = ipm_news_related_projects.news_id AND post_date < getdate() AND pull_date > getdate() AND SHOW = 1 AND ipm_news_related_projects.projectid = " & projectID & " ORDER BY POST_DATE DESC"
	rsNews.Open sql, Conn, 1, 4
	
	set rsAwards=server.createobject("adodb.recordset")
	sql = "select TOP 3 * FROM ipm_news, ipm_news_related_projects where ipm_news.type = 2 AND ipm_news.news_id = ipm_news_related_projects.news_id AND post_date < getdate() AND pull_date > getdate() AND SHOW = 1 AND ipm_news_related_projects.projectid = " & projectID & " ORDER BY POST_DATE DESC"
	rsAwards.Open sql, Conn, 1, 4
	
	set rsArchitect=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & projectID  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_ARCHITECT'"
	rsArchitect.Open sql, Conn, 1, 4
	
	set rsCompletion=server.createobject("adodb.recordset")
	sql = "select * from ipm_project_field_value a, ipm_project_field_desc b WHERE projectid = " & projectID  & " AND a.item_id = b.item_id AND item_tag = 'IDAM_PROJECT_COMPLETION_DATE'"
	rsCompletion.Open sql, Conn, 1, 4
	
	set rsDiscipline = server.createobject("adodb.recordset")
	sql="select * from ipm_project, ipm_project_discipline,ipm_discipline where ipm_project.projectid = ipm_project_discipline.projectid AND ipm_discipline.keyid = ipm_project_discipline.keyid AND ipm_project.projectid = " & projectID & " order by keyName"
	rsDiscipline.Open sql, Conn, 1, 4

	set rsTestimonials =  server.createobject("adodb.recordset")
	sql = "select * from ipm_asset a,ipm_asset_category b where a.projectid = b.projectid AND b.name = 'Testimonials' AND a.category_id = b.category_id AND a.available = 'Y' and b.available = 'Y' AND a.securitylevel_id = 3 AND a.projectid = " & projectID & " ORDER BY creation_date desc"
	rsTestimonials.Open sql, Conn, 1,4

%>
<html>
<head>
<title><%=sPageTitle%> - Project</title>
	<!--#include file="includes\header.asp" -->
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />	
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
						<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr>
				<td class="left_col_project">
					<div id="content_text">
					<table width="550" align="center">
					<tr>
					<%
					i = 0
					do while not rsTestimonials.eof %>
					<td align="center"><a rel="lightbox[testimonials]" title="<%=rsProject("name")%> - <%=rsTestimonials("description")%>" href="<%=sAssetPath%><%=rsTestimonials("asset_ID")%>&Instance=<%=siDAMInstance%>&type=asset&size=1&width=640&height=480&qfactor=<%=assetQFactor%>"><img src="<%=sAssetPath%><%=rsTestimonials("asset_ID")%>&Instance=<%=siDAMInstance%>&type=asset&size=1&width=150&height=118&qfactor=<%=assetQFactor%>" /></a><br />Uploaded <%= FormatDateTime(rsTestimonials("Creation_Date"), 2)%></td>
					<%
					rsTestimonials.moveNext
					i = i + 1
					if (i mod 3 = 0) then
					response.write "</tr><tr>"
					end if
					loop
					
					%>
					
					</tr>
					</table>
					</div>
					
					
				</td>
				
				<td class="right_col_project">
				
					<div style="margin-left:10px;padding:10px;">
					<small>Portfolio | <% do while not rsDiscipline.eof
		response.write "<a href=""portfolio_cat.asp?cid=" & rsDiscipline("keyID") & """>" & rsDiscipline("keyName") & "</a> "
		rsDiscipline.moveNext
	loop %> | <%=rsProject("name")%></small>
					<br /><br />
					<b><big><%=rsProject("name")%></big><br />
					<%=rsProject("clientName")%></b><br /><br />
					<%=rsProject("descriptionmedium")%>
					<br /><br />
					<b>PROJECT DATA</b><br />
					Address: <%=rsProject("address")%><br />
					<%=rsProject("city")%>, <%=rsProject("state_id")%> <%=rsProject("zip")%><br />
					
					<%if not(rsArchitect("item_value") = "") then%><b>Architect:</b> <%=rsArchitect("item_value")%><br /><%end if%>	
					
					<%if not(rsCompletion("item_value") = "") then%><b>Completion:</b> <%=rsCompletion.recordCount%><%=rsCompletion("item_value")%><br /><%end if%>	
					<br />
					
					<%if (rsNews.recordCount > 0) then%><b>PRESS RELEASES</b><br /><%end if%>
					<%do while not rsNews.eof %>
						
						<a href="view_news.asp?id=<%=rsNews("news_id")%>"><%=rsNews("headline")%></a>

					<%
					rsNews.movenext
					loop
					%>
					<br /><br />
					<%if (rsAwards.recordCount > 0) then%><b>RECOGNITION</b><br /><%end if%>
					
					<%do while not rsAwards.eof %>
					
						<a href="awards.asp"><%=rsAwards("headline")%></a><br />

					<%
					rsAwards.movenext
					loop
					%>
					<br />
					<a href="awards.asp">See More&gt;&gt;</a>
					</div>
				</td>
			</tr>
						<tr>
				<td class="left_col_project">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_project">					
				</td>
			</tr>
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
