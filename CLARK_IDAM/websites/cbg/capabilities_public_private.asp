	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Public Private Partnership</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%"  id="main_table">
			<tr>
				<td class="left_col_overview" valign="top">
					<div id="content_text">
					<small><a href="capabilities.asp">Capabilities</a> | Public/Private Partnerships</small>
					<br /><br />
					<img src="images/title_public_private_partne.jpg" alt="public private partnership" />
					<br /><br />
					
					Clark Builders Group offers local, state, and federal government agencies a solid track record of success on public/private partnerships.  Clark understands the importance of working together with government agencies, local residents, and community groups to create projects that maximize value of the public asset and meet the goals of all stakeholders. 
					<br /><br />
					<b>Military Housing</b><br />
					As a trend-setter in military housing construction, Clark Builders Group has a track record of industry "firsts" that demonstrate its ability to handle complex, multi-phase projects and implement a creative vision.  For example, Clark is currently building the nation's first large-scale privatization of bachelors housing at Pacific Beacon at Naval Base San Diego.  In total, Clark is partnered with the departments of the Army, Navy, and Air Force on more than $4.9 billion of housing encompassing 34,000 homes at twelve installations in the US.
					<br /><br />
					<b>Small and Local Business Participation</b><br />
					Clark has been successful in meeting all applicable regulations regarding union labor and compensation rates for construction trades on its public sector construction projects, while also leveraging the expertise of small, local, and disadvantaged businesses.
					

					
					
					</div>
					
				</td>
				
				<td class="right_col_overview" valign="top">
				
					<img src="images/public_private_side.jpg" alt="capabilities" />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
