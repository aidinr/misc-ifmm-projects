	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Sustainable Building</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td id="left_col_overview">
					<div id="content_text">
					<small>Capabilities | Sustainable Building</small>
					<br /><br />
					<img src="images/title_sustainable_building.jpg" alt="sustainable building" />
					<br /><br />
					Clark Builders Group offers local, state, and federal government agencies a solid track record of success on public/private partnerships. Clark understands the importance of working together with government agencies, local residents, and community groups to create projects that maximize value of the public asset and meet the goals of all stakeholders.<br /> 
					<br />
					<b>Military Housing</b><br />
					As a trend-setter in military housing construction, Clark Builders Group has a track record of industry �firsts� that demonstrate its ability to handle complex, multi-phase projects and implement a creative vision. For example, Clark is currently building the nation�s first large-scale privatization of bachelors housing at Pacific Beacon at Naval Base San Diego.  Clark is partnered with the departments of the Army, Navy, and Air Force on more than $4.4 billion of housing encompassing 25,000 homes at ten installations in the US.  Furthermore, Clark has earned dozens of national design, construction, and planning awards for its military housing portfolio�a testament to the quality and care that goes into every Clark home.<br />
					<br />
					<b>Small and Local Business Participation</b><br />
					Clark has been successful in meeting all applicabl regulations regarding union labor and compensation rates for construction trades on its public sector construction projects, while also leveraging the expertise of small, local, and disadvantaged businesses.<br />   

					<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
					<!--#include file="includes\footer.asp" -->
					</div>
					
				</td>
				
				<td id="right_col_overview">
				
					<img src="images/sustainable_side.jpg" alt="capabilities" />
				</td>
			</tr>

			
			</table>
			</div>
			
		
		</div>
	</div>
</body>
</html>
