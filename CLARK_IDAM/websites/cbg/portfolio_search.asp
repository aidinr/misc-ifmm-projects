<!--#include file="includes\config.asp" -->

<%
	search = prep_sql(request.querystring("search"))
	sort = prep_sql(request.querystring("sort"))
	sort_dir = prep_sql(request.querystring("dir"))
	
	advanced = prep_sql(request.querystring("advanced"))
	project_type = prep_sql(request.querystring("type"))
	region = prep_sql(request.querystring("region"))
	state = prep_sql(request.querystring("state"))
	
	if(advanced = "1") then	
	   	parameters = "search=" & search & "&advanced=" & advanced & "&type=" & project_type & "&region=" & region & "&state=" & state 
	else
		parameters = "search=" & search
	end if
	
	title = ""
	title_image = ""
	type_description = ""
	
	th_name = "<a href=""portfolio_search.asp?" & parameters & "&sort=name&dir=asc"">NAME</a>"
	th_loc = "<a href=""portfolio_search.asp?" & parameters & "&sort=location&dir=asc"">LOCATION</a>"
	th_desc = "<a href=""portfolio_search.asp?" & parameters & "&sort=desc&dir=asc"">DESCRIPTION</a>"
	th_completion = "<a href=""portfolio_search.asp?" & parameters & "&sort=completion&dir=asc"">COMPLETION</a>"
	
	if(sort_dir = "asc") then
	next_dir = "desc"
	elseif(sort_dir = "desc") then
	next_dir = "asc"
	else
	next_dir = "asc"
	sort_dir = "asc"
	end if
	if(sort = "name") then
		sql_sort = "a.name " & sort_dir
		th_name = "<a href=""portfolio_search.asp?" & parameters & "&sort=name&dir=" & next_dir & """><b>NAME</b></a>"
	elseif (sort = "location") then
		sql_sort = "a.state_id " & sort_dir & ", a.city " & sort_dir
		th_loc = "<a href=""portfolio_search.asp?" & parameters & "&sort=location&dir=" & next_dir & """><b>LOCATION</b></a>"
	elseif (sort = "completion") then
		sql_sort = "completion " & sort_dir
		th_completion = "<a href=""portfolio_search.asp?" & parameters & "&sort=completion&dir=" & next_dir & """><b>COMPLETION</b></a>"
	elseif (sort = "desc") then
		sql_sort = "a.description " & sort_dir
		th_desc = "<a href=""portfolio_search.asp?" & parameters & "&sort=desc&dir=" & next_dir & """><b>DESCRIPTION</b></a>"		
	else 
		sql_sort = "a.name " & sort_dir
		th_name = "<a href=""portfolio_search.asp?" & parameters & "&sort=name&dir=desc""><b>NAME</b></a>"
	end if
	
	set rsProjectsByCat=server.createobject("adodb.recordset")
	
	if(advanced = "1") then
	
	if(search <> "") then
	Dim WordArray
	WordArray = Split(search, " ")
	
	search_sql = " AND CONTAINS(ipm_project.*,'(FORMSOF(INFLECTIONAL,""" & WordArray(0) & """) "
	simple_sql = """" & WordArray(0) & "*"" "
	
	arrayLength = ubound(WordArray)
	
	if(arrayLength > 0) then
		
		i=1
		do while i <= arrayLength
		 if(WordArray(i) <> "") then
		 search_sql = search_sql & " AND FORMSOF(INFLECTIONAL,""" & WordArray(i) & """) "
		 simple_sql = simple_sql & " AND """ & WordArray(i) & "*"" " 
		 end if
		 i=i+1
		loop
	end if
	search_sql = search_sql & ") OR (" & simple_sql & ")')"
	end if
	
	if(state <> "") then
	state_sql = " AND state_id = '" & state & "' "
	end if
	
	if(region <> "") then
	region_sql = " AND i.item_value = '" & region & "' "
	end if

	if(project_type <> "") then
	disc_sql = " AND ipm_project_discipline.keyid = '" & project_type & "' "
	
	select case project_type
	case "21762217"
		project_type_string = "Mixed-Use"
	case "21762215"
		project_type_string = "Luxury Apartments"
	case "21762216"
		project_type_string = "Military Housing"
	case "21763556"
		project_type_string = "Senior Living"
	case "21762214"
		project_type_string = "High-Density Apartments"
	case "21762213"
		project_type_string = "Affordable Housing"
	case "21762980"
		project_type_string = "Retail/Office/Hospitality"
	end select		
	
	project_type_string = "[Project Type: " & project_type_string & "] "
	
	end if		
	sql = "select a.*, convert(datetime,a.item_value) completion from (select distinct ipm_project.projectid, ipm_project.name, ipm_project.city, ipm_project.state_id, ipm_project.description, a.item_value from ipm_project, ipm_project_discipline,ipm_project_field_value a, ipm_project_field_desc b,ipm_project_field_desc e, ipm_project_field_value f, ipm_project_discipline g, ipm_project_field_desc h, ipm_project_field_value i where ipm_project.projectid = i.projectid and i.item_id = h.item_id and h.item_tag = 'IDAM_PROJECT_REGION' and ipm_project.projectid = f.projectid and e.item_id = f.item_id and e.item_tag = 'IDAM_PROJECT_CC_WEBSITE' and f.item_value = '1' and ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND a.projectid = ipm_project.projectID AND a.item_id = b.item_id AND b.item_tag = 'IDAM_PROJECT_COMPLETION' and g.projectid = ipm_project.projectid " & disc_sql & state_sql & search_sql & region_sql & " and a.item_value <> '') a"
	sql = sql & " UNION ALL " 
	sql = sql &  "select a.*, convert(datetime,'1/1/2050') completion from (select distinct ipm_project.projectid, ipm_project.name, ipm_project.city, ipm_project.state_id, ipm_project.description, a.item_value from ipm_project, ipm_project_discipline,ipm_project_field_value a, ipm_project_field_desc b,ipm_project_field_desc e, ipm_project_field_value f, ipm_project_discipline g, ipm_project_field_desc h, ipm_project_field_value i where ipm_project.projectid = i.projectid and i.item_id = h.item_id and h.item_tag = 'IDAM_PROJECT_REGION' and ipm_project.projectid = f.projectid and e.item_id = f.item_id and e.item_tag = 'IDAM_PROJECT_CC_WEBSITE' and f.item_value = '1' and ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND a.projectid = ipm_project.projectID AND a.item_id = b.item_id AND b.item_tag = 'IDAM_PROJECT_COMPLETION' and g.projectid = ipm_project.projectid " & disc_sql & state_sql & search_sql & region_sql & " and a.item_value = '') a order by " & sql_sort
	else
    
    myarray = Split(search," ")

    Dim counter  

 	search_sql = ""
     
    For counter = 0 To UBound(myArray) 
    
       if UCase(myArray(counter)) = "OF" OR UCase(myArray(counter)) = "AND" then
        ' skipping the concat
       else
        search_sql = search_sql & myArray(counter) & " AND "  
       end if   
    Next  
    if len(search_sql) > 3 then
        search_sql = Mid(search_sql,1,len(search_sql) -5)
    end if

	state = ""
	project_type = ""
	region = ""
	sql = "select a.*, convert(datetime,a.item_value) completion from (select distinct ipm_project.projectid, ipm_project.name, ipm_project.city,ipm_project.state_id, ipm_project.description,a.item_value from ipm_project, ipm_project_discipline,ipm_project_field_value a,ipm_project_field_desc b,ipm_project_field_desc e, ipm_project_field_value f where ipm_project.projectid = f.projectid and e.item_id = f.item_id and e.item_tag = 'IDAM_PROJECT_CC_WEBSITE' and f.item_value = '1' and ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND a.projectid = ipm_project.projectID AND a.item_id = b.item_id AND b.item_tag = 'IDAM_PROJECT_COMPLETION' AND CONTAINS(ipm_project.Search_Tags, '" & search_sql & "') and a.item_value <> '') a"
	sql = sql &  " UNION ALL "
	sql = sql &  "select a.*, convert(datetime,'1/1/2050') completion from (select distinct ipm_project.projectid, ipm_project.name, ipm_project.city,ipm_project.state_id, ipm_project.description,a.item_value from ipm_project, ipm_project_discipline,ipm_project_field_value a,ipm_project_field_desc b,ipm_project_field_desc e, ipm_project_field_value f where ipm_project.projectid = f.projectid and e.item_id = f.item_id and e.item_tag = 'IDAM_PROJECT_CC_WEBSITE' and f.item_value = '1' and ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND a.projectid = ipm_project.projectID AND a.item_id = b.item_id AND b.item_tag = 'IDAM_PROJECT_COMPLETION' AND CONTAINS(ipm_project.Search_Tags, '" & search_sql & "') and a.item_value = '') a order by " & sql_sort
	end if
    
	rsProjectsByCat.Open sql, Conn, 1, 4
	

	


%>

<html>
<head>
<title><%=sPageTitle%> - Portfolio</title>
	<!--#include file="includes\header.asp" -->
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">

					<div id="content_text">
					<small><a href="portfolio.asp">Portfolio</a> | <%if(advanced = "1") then response.write "Advanced" end if%> Search Results: <%=project_type_string%> <%if(region <> "") then response.write "[Region: " & region & "] " end if%> <%if(state <> "") then response.write "[State: " & state & "] " end if%>  <%if(search <> "" and advanced = "1") then response.write "[Keywords: " & search & "] " end if%>  <%if(search <> "" and advanced <> "1") then response.write "Name: [" & search & "] " end if%></small>
					<br /><br />
					<img src="images/<%if(advanced = "1") then response.write "advanced_" end if%>search_results.jpg" alt="Portfolio Search" />
					<br /><br />
					<%=type_description%>
					<br /><br />
					<table id="main_table" width="950">
					<tr><td valign="top">
					<table width="950">
						<tr>
							<th></th>
							<th><%=th_name%></th>
							<th><%=th_loc%></th>
							<th><%=th_desc%></th>
							<th><%=th_completion%></th>
						</tr>
						<%
						z = 1
						do while not rsProjectsByCat.eof

						if(rsProjectsByCat("completion") <> "1/1/2050") then
						lineDate = CDate(rsProjectsByCat("completion"))
						lineDate = MonthName(DatePart("m",lineDate)) & " " & DatePart("yyyy",lineDate)
						else
						lineDate = "TBA"
						end if
						
						if (z mod 2 = 0) then 
						lineStyle = "style=""background:#f6f3e7;"""
						lineStyle2 = "style=""background:url(images/bg_portfolio_photo_odd.jpg);"""
						else
						lineStyle =""
						lineStyle2 = ""
						end if
						z = z + 1
						%>

							<tr>
								<td <%=lineStyle%> align="center"><div class="portfolio_bg" <%=lineStyle2%>><a href="project.asp?pid=<%=rsProjectsByCat("projectID")%>"><img width="137" height="108" class="portfolio_photo" src="<%=sAssetPath%><%=rsProjectsByCat("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=137&height=108&qfactor=<%=assetQFactor%>" alt="<%=rsProjectsByCat("name")%>"/></a></div></td>
								<td <%=lineStyle%> align="center"><%=rsProjectsByCat("name")%></td>
								<td <%=lineStyle%> align="center"><%=rsProjectsByCat("city")%>, <%=rsProjectsByCat("state_id")%></td>
								<td <%=lineStyle%> align="center"><%=rsProjectsByCat("description")%></td>
								<td <%=lineStyle%> align="center"><%=lineDate%></td>
							</tr>
						
						<%	
						rsProjectsByCat.moveNext
						
						loop						
						%>
					
					</table>
					</td></tr></table>
					
					</div>
					
					
					<!--#include file="includes\footer.asp" -->
				
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
