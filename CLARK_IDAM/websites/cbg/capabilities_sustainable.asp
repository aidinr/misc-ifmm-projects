	<!--#include file="includes\config.asp" -->

<html>
<head>
<title><%=sPageTitle%> - Sustainable Building</title>
	<!--#include file="includes\header.asp" -->
	

</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			<!--#include file="includes\page_header.asp" --><!--#include file="includes\menu.asp" --><div class="content"><table cellpadding="0" cellspacing="0"  id="main_table">
			<tr>
				<td id="left_col_overview" valign="top">
					<div id="content_text"><small><a href="capabilities.asp">Capabilities</a> | Sustainable Building</small>
					<br /><br />
					<img src="images/title_sustainable_building.jpg" alt="sustainable building" />
					<br /><br />
					<b>Sustainability Review</b><br />
Clark's preconstruction services, green specification development, LEED documentation, and knowledge of environmentally preferred materials and techniques provide clients with the expertise to create a quality project that minimizes the impact of modern development on the Earth's natural resources. With an emphasis on early and open collaboration with the owner and all major project partners, Clark will identify sustainable design opportunities and make recommendations that consider environmental impact, cost effectiveness, and market factors.
					<br />
					<br />
					<img src="images/green_council.jpg" style="float:right;" /><img width="8" height="88" src="images/spacer.gif" style="float:right;" /><b>Green Building</b><br />
					<div style="margin-right:10px;">

Clark is a member of the U.S. Green Building Council (USGBC) and, with hundreds of LEED Accredited Professionals, is among only a handful of contractors with significant experience in the construction of LEED certified buildings, including the first ever LEED certified multifamily apartment building, and the military's first LEED Platinum new construction project. Clark's experience provides tremendous value to owners seeking to obtain LEED certification, or simply to create a greener project. Leveraging a full range of capabilities, Clark will identify all potential LEED points during the preconstruction phase, providing the client with a clear of any costs associated with sustainability.  Throughout each project Clark will manage the LEED certification process by facilitating open communication, maintaining the project's LEED scorecard, and working with all involved subcontractors and partners.

</div> <br />
					
					<b>Low-Impact Design</b><br />


Clark boasts extensive experience mitigating environmentally challenging sites, leveraging low-impact construction techniques, and incorporating the best sustainable practices and technologies. Clark's sustainable building practices led to an eco-friendly cooling tower and Gold SPiRiT-certified homes at its <a href="project.asp?pid=21766786">Fort Irwin</a> project, which included environmentally-conscious design features such as solar-operated fans and increased insulation.
<br />
<br />
Clark leverages sustainable building techniques across all aspects of construction. Various sustainable project features on Clark projects include but are not limited to: ENERGY STAR appliances and roofing, low VOC paints, geothermal heating and cooling systems, low-flow toilets and faucets, low E windows, pedestrian-friendly streetscapes, and drought-resistant landscaping.  Clark has garnered numerous awards for its sustainable practices to date, including two 2009 ENERGY STAR Leadership in Housing Awards.



<br /> 
					<br />
					<b>Recycling Efforts</b><br />
Clark commits itself to recycling efforts at each of its projects. As a result of these initiatives, the City of San Diego recognized Clark with its Recycler of the Year Award for the reuse of 200,000 tons of construction materials at two of its local military projects.



<br /> 
					<br />
					<b>Clark's Green Stats at a Glance</b><br />



					<table cellspacing="3" cellpadding="3" width="575">
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">First LEED certified multifamily apartment community</td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Over 400 LEED Accredited Professionals</td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">One of America's largest builders of ENERGY STAR qualified homes</td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">First LEED Platinum building in the world</td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">First LEED-NC Platinum military project</td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">Some of the nation's largest LEED certified buildings, including the first LEED certified stadium</td>
						</tr>
						<tr>
							<td valign="top"><div style="margin-top:6px;"><img src="images/ul.jpg" alt="bullet" /></div>  </td><td valign="top">More than 40 LEED certified projects worth over $7 billion nationwide</td>
						</tr>


					</table>
<br />   

					<table cellpadding="4" border="0" width="100%">
						<tr>
							<td align="right" valig="top">
								<a href="portfolio_cat.asp?cid=<%=DISC_SUS_BUI %>">See Our Sustainable Projects &gt;&gt;</a>
							</td>
						</tr>
					</table>
					
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/sustainable_side.jpg" alt="capabilities" />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
