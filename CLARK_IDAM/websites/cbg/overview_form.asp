	<!--#include file="includes\config.asp" -->
	<!--#include file="includes\freeASPUpload.asp" -->
<%
newLine = chr(13) & chr(10)
upload_dir = "C:\idam\resumes\uploads\"

Set Upload = New FreeASPUpload
Upload.Save(upload_dir) 
if Upload.form("submit") = "1" then

			formRegion = Upload.form("region")
			formJob = Upload.form("position")
			formFirst = Upload.form("first")
			formLast = Upload.form("last")
			formAddress = Upload.form("address")
			formCity = Upload.form("city")
			formState = Upload.form("state")
			formZip = Upload.form("zip")
			formPhone = Upload.form("phone")
			formEmail = Upload.form("email")
			
			formFT = Upload.form("full_time")
			formTitle = Upload.form("current_title")
			
			formUdDegree = Upload.form("ud_degree")
			formUdYear = Upload.form("ud_year")
			formUdInstitution = Upload.form("ud_institution")

			formGDegree = Upload.form("g_degree")
			formGYear = Upload.form("g_year")
			formGInstitution = Upload.form("g_institution")

			if(formRegion = "ne") then
			formRegion = "Northeast"
			email_address = "NEjobs@clarkbuildersgroup.com"
			elseif (formRegion = "nw") then
			formRegion = "Northwest"
			email_address = "NWJobs@clarkbuildersgroup.com"
			elseif (formRegion = "se") then
			formRegion = "Southeast"
			email_address = "SEjobs@clarkbuildersgroup.com"
			elseif (formRegion = "sw") then
			formRegion = "Southwest"
			email_address = "SWjobs@clarkbuildersgroup.com"
			end if			
			
	                Set Mail = Server.CreateObject("Persits.MailSender")
                        MailText = MailText & "A job application for the " & formRegion & " region has been submitted." & newLine & newLine
			MailText = MailText & "Position ID - Title: " & formJob & newLine
			MailText = MailText & newLine
			MailText = MailText & "Contact Info: " & newLine
			MailText = MailText & newLine
			MailText = MailText & "Name: " & formFirst & " " & formLast & newLine
			MailText = MailText & "Address: " & formAddress &newLine
			MailText = MailText & "City: " & formCity &newLine
			MailText = MailText & "State: " & formState &newLine
			MailText = MailText & "Zip: " & formZip &newLine
			MailText = MailText & "Daytime Telehone: " & formPhone &newLine
			MailText = MailText & "Email: " & formEmail &newLine
			MailText = MailText & newLine
			MailText = MailText & "Experience: " & newLine
			MailText = MailText & newLine
			MailText = MailText & "# Years Full Time Experience: " & formFT & " " & formLast & newLine
			MailText = MailText & "Current Title: " & formTitle &newLine
			MailText = MailText & newLine
			MailText = MailText & "Undergraduate Degree: " & formUdDegree &newLine
			MailText = MailText & "Undergraduate Institution: " & formUdInstitution &newLine
			MailText = MailText & "Undergraduate Graduation Year: " & formUdYear &newLine
			MailText = MailText & newLine
			MailText = MailText & "Graduate Degree: " & formGDegree &newLine
			MailText = MailText & "Graduate Institution: " & formGInstitution &newLine
			MailText = MailText & "Graduate Graduation Year: " & formGYear &newLine
			
			MailText = MailText & newLine & newLine
			MailTex = MailText & "The candidate's resume and cover letter (if applicable) are attached with this e-mail." &newLine
			
			MailText = MailText & newLine
			MailText = MailText & "End of resume submission."

                        Mail.Host = "localhost" ' Specify a valid SMTP server

                        Mail.From = "jobs@clarkrealty.com"

                        Mail.FromName = "CBG Jobs"

                        Mail.Subject = "New Resume Submission"

                        Mail.Body = MailText
			
                        Mail.AddAddress email_address


			ks = Upload.UploadedFiles.keys
				 
				if (UBound(ks) <> -1) then
				  SaveFiles = "<B>Files uploaded:</B> "
					for each fileKey in Upload.UploadedFiles.keys
					Mail.AddAttachment upload_dir & Upload.UploadedFiles(fileKey).FileName
					next
				end if			
			
                        on error resume next     
                        Mail.Send

                       

                        If Err <> 0 Then
                                    blnSent = "0"

                        else     
                                    blnSent = "1"

                        End If
                        set Mail = nothing

end if
%>
<%
	rid = prep_sql(request.querystring("region"))
	jid = prep_sql(request.querystring("job"))
	if(rid = "ne") then
	region_title = "Northeast"
	ne_select = "selected=""selected"""
	elseif (rid = "nw") then
	region_title = "Northwest"
	nw_select = "selected=""selected"""
	elseif (rid = "se") then
	region_title = "Southeast"
	se_select = "selected=""selected"""
	elseif (rid = "sw") then
	region_title = "Southwest"
	sw_select = "selected=""selected"""
	end if
%>
<html>
<head>
<title><%=sPageTitle%> - Application Form</title>
	<!--#include file="includes\header.asp" -->
	
<script type="text/javascript" src="js/yav.js"></script>
<script type="text/javascript" src="js/yav-config.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
var rules=new Array();
rules[0]='region|required';
rules[1]='position|required';
rules[2]='first|required';
rules[3]='email|required';
rules[4]='email|email';
rules[5]='last|required';
rules[6]='city|required';
rules[7]='state|required';
rules[8]='zip|required';
rules[9]='address|required';
rules[10]='resume|required';
rules[11]='full_time|required';
rules[12]='full_time|numeric';
rules[13]='current_title|required';
rules[14]='ud_year|numeric';
rules[15]='g_year|numeric';
// -->
</script>
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">
			<table cellpadding="0" cellspacing="0" width="100%"  id="main_table">
			<tr>
				<td class="left_col_overview">
					<div id="content_text">
					<small><a href="overview.asp">Overview</a> | Application Form</small>
					<br /><br />
					<img src="images/application_form.jpg" alt="Application Form" />
					<br /><br />
					<%
					if(blnSent = "1") then
					Response.write "<b>Your application has been successfully submitted. Thank you!</b><br />"
					elseif(blnSent="0") then
					Response.write "<b>A technical error has occured. Please try again later.</b><br />"
					end if
					%>
					<%if not(blnSent = "1") then %>
					<form method="post" enctype="multipart/form-data" action="overview_form.asp" name="feedbackForm" onsubmit="return performCheck('feedbackForm', rules, 'classic');">
					<table width="100%" cellpadding="5" cellspacing="5">
						<tr>
							<td><b>Position</b></td>
							<td></td>
						</tr>
						<tr>
							<td>Region</td>
							<td><select name="region" id="job_region" onchange="$('#job_position').load('get_jobs.asp?region=' + this.value);">
								<option value="">--- Select ---</option>
								<option value="ne" <%=ne_select%>>Northeast</option>
								<option value="nw" <%=nw_select%>>Northwest</option>
								<option value="se" <%=se_select%>>Southeast</option>
								<option value="sw" <%=sw_select%>>Southwest</option>
							    </select></td>
						</tr>
						<tr>
							<td>Position</td>
							<td>
							<div id="job_position">
							<select name="position" style="width:250px;">
							<%
							if not(region_title = "") then
							set rsJobs=server.createobject("adodb.recordset")
							sql = "select * FROM ipm_jobs a, ipm_jobs_type b WHERE a.department = b.type_id AND SHOW = 1 AND POST_DATE < getdate() AND PULL_DATE > getdate() AND b.type_value = '" & region_title & "' ORDER BY Job_Title"
							rsJobs.Open sql, Conn, 1, 4	
							%>
							<option value="">--- Select a <%=region_title%> Job---</option>
							<%do while not rsJobs.eof %>
							<option value="<%=rsJobs("Job_ID") & " - " & rsJobs("Job_Title")%>" <%if(cstr(jid)=cstr(rsJobs("Job_ID"))) then response.write "selected=""selected""" end if%>><%=rsJobs("Job_Title")%></option>
							<%
							rsJobs.movenext
							loop
							%>
							<%
							else%>
							<option>--- Select a Region First ---</option>
							<%end if
							%>
							</select>
							</div>
							</td>
						</tr>
						<tr>
							<td><b>Contact Info</b></td>
							<td></td>
						</tr>						
						<tr>
							<td>First Name*</td>
							<td><input type="text" name="first" /></td>
						</tr>
						<tr>
							<td>Last Name*</td>
							<td><input type="text" name="last" /></td>
						</tr>
						<tr>
							<td>Address*</td>
							<td><input type="text" name="address" /></td>
						</tr>
						<tr>
							<td>City*</td>
							<td><input type="text" name="city" /></td>
						</tr>
						<tr>
							<td>State*</td>
							<td><select name="state">
								<option value="">Select one</option>
								<option value="AK">AK</option>
								<option value="AL">AL</option>
								<option value="AR">AR</option>
								<option value="AZ">AZ</option>

								<option value="CA">CA</option>
								<option value="CO">CO</option>
								<option value="CT">CT</option>
								<option value="DC">DC</option>
								<option value="DE">DE</option>
								<option value="FL">FL</option>

								<option value="GA">GA</option>
								<option value="HI">HI</option>
								<option value="IA">IA</option>
								<option value="ID">ID</option>
								<option value="IL">IL</option>
								<option value="IN">IN</option>

								<option value="KS">KS</option>
								<option value="KY">KY</option>
								<option value="LA">LA</option>
								<option value="MA">MA</option>
								<option value="MD">MD</option>
								<option value="ME">ME</option>

								<option value="MI">MI</option>
								<option value="MN">MN</option>
								<option value="MO">MO</option>
								<option value="MS">MS</option>
								<option value="MT">MT</option>
								<option value="NC">NC</option>

								<option value="ND">ND</option>
								<option value="NE">NE</option>
								<option value="NH">NH</option>
								<option value="NJ">NJ</option>
								<option value="NM">NM</option>
								<option value="NV">NV</option>

								<option value="NY">NY</option>
								<option value="OH">OH</option>
								<option value="OK">OK</option>
								<option value="OR">OR</option>
								<option value="PA">PA</option>
								<option value="RI">RI</option>

								<option value="SC">SC</option>
								<option value="SD">SD</option>
								<option value="TN">TN</option>
								<option value="TX">TX</option>
								<option value="UT">UT</option>
								<option value="VA">VA</option>

								<option value="VT">VT</option>
								<option value="WA">WA</option>
								<option value="WI">WI</option>
								<option value="WV">WV</option>
								<option value="WY">WY</option>
								</select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Zip <input type="text"  name="zip" /></td>
						</tr>
						<tr>
							<td>Daytime Phone</td>
							<td><input type="text" name="phone" /></td>
						</tr>
						<tr>
							<td>Email Address*</td>
							<td><input type="text" name="email" /></td>
						</tr>
						<tr>
							<td><b>Experience</b></td>
							<td></td>
						</tr>
						<tr>
							<td># Years Full Time Experience*</td>
							<td><input type="text" name="full_time" /></td>
						</tr>
						<tr>
							<td>Current Title*</td>
							<td><input type="text" name="current_title" /></td>
						</tr>
						<tr>
							<td><i>Undergraduate Degree (if applicable)</i></td>
							<td></td>
						</tr>
						<tr>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Degree</td>
							<td><input type="text" name="ud_degree" /></td>
						</tr>
						<tr>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Institution</td>
							<td><input type="text" name="ud_institution" /></td>
						</tr>
						<tr>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Graduation Year</td>
							<td><input type="text" name="ud_year" /></td>
						</tr>
						<tr>
							<td><i>Graduate Degree (if applicable)</i></td>
							<td></td>
						</tr>
						<tr>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Degree</td>
							<td><input type="text" name="g_degree" /></td>
						</tr>
						<tr>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Institution</td>
							<td><input type="text" name="g_institution" /></td>
						</tr>
						<tr>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Graduation Year</td>
							<td><input type="text" name="g_year" /></td>
						</tr>
						<tr>
							<td><b>Attachments</b></td>
							<td></td>
						</tr>						
						<tr>
							<td>Attach Resume*</td>
							<td><input type="file" name="resume" /></td>
						</tr>
						<tr>
							<td>Attach Cover Letter</td>
							<td><input type="file" name="cover" /></td>
						</tr>
						<tr>
							<td></td>
							<td>* Denotes mandatory fields</td>
						</tr>
						<tr>
							<td><input type="hidden" name="submit" value="1" /></td>
							<td><input type="image" src="images/submit_application.jpg" alt="submit application" /></td>
						</tr>							
					</table>
					</form>
					<%end if%>
					
					</div>
					
				</td>
				
				<td class="right_col_overview">
				
					<img src="images/application_side.jpg" alt="subcontracting" />
				</td>
			</tr>
			<tr>
				<td class="left_col_overview">
				<!--#include file="includes\footer.asp" -->
				</td>
				<td class="right_col_overview">					
				</td>
			</tr>
			
			</table>
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
