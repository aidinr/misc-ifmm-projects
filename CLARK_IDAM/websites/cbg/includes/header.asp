<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script type="text/javascript" src="js/jquery-1.2.1.pack.js"></script>
<script type="text/javascript" src="js/jquery.perciformes.js"></script>

<link rel="stylesheet" type="text/css" href="style/cbg.css" />


<!--[if lte IE 6]>
<style type="text/css">
img {
	behavior: url("pngbehavior.htc");
}
    #nav td.navigation { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/menu_bg.png", sizingMethod="scale");
}
    #nav td.navigation_right { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/menu_bg.png", sizingMethod="scale");
}
    #nav td.navigation_bottom_left { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/nav_bg_bottom_left.png", sizingMethod="scale");
}
    #nav td.navigation_bottom_right { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/nav_bg_bottom_right.png", sizingMethod="scale");
}

    #nav td.navigation_bottom { 
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/nav_bg_bottom.png", sizingMethod="scale");
}

    #nav td.navigation_left {
	background: none;
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="images/menu/nav_bg_left.png", sizingMethod="scale");	
    }

</style>
<![endif]-->

<!--[if IE]>
  <script type="text/javascript">
  // <![CDATA[

  $(document).ready(function() {
    $('#nav div').sfHover();
    $('#search_projects').sfHover();
    $('#search_projects').sfFocus();

  });

  // ]]>
  </script>
<style type="text/css">
    #content {
    	height: 100%;
    }
    #home_content {
    	height: 100%;
    }
    #project_image_mask {
    	display: none;
    }
    #content_text {
    	width: 580px;
    }
</style>
<![endif]-->
