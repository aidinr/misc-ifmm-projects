<!--#include file="includes\config.asp" -->

<%
	cid = prep_sql(request.querystring("cid"))
	sort = prep_sql(request.querystring("sort"))
	sort_dir = prep_sql(request.querystring("dir"))
	title = ""
	title_image = ""
	type_description = ""
	
	  parameters = "cid=" & cid
	
	title = ""
	title_image = ""
	type_description = ""
	
	th_name = "<a href=""portfolio_all.asp?" & parameters & "&sort=name&dir=asc"">NAME</a>"
	th_loc = "<a href=""portfolio_all.asp?" & parameters & "&sort=location&dir=asc"">LOCATION</a>"
	th_desc = "<a href=""portfolio_all.asp?" & parameters & "&sort=desc&dir=asc"">DESCRIPTION</a>"
	th_completion = "<a href=""portfolio_all.asp?" & parameters & "&sort=completion&dir=asc"">COMPLETION</a>"
	
	if(sort_dir = "asc") then
	next_dir = "desc"
	elseif(sort_dir = "desc") then
	next_dir = "asc"
	else
	next_dir = "asc"
	sort_dir = "asc"
	end if
	if(sort = "name") then
		sql_sort = "a.name " & sort_dir
		th_name = "<a href=""portfolio_all.asp?" & parameters & "&sort=name&dir=" & next_dir & """><b>NAME</b></a>"
	elseif (sort = "location") then
		sql_sort = "a.state_id " & sort_dir & ", a.city " & sort_dir
		th_loc = "<a href=""portfolio_all.asp?" & parameters & "&sort=location&dir=" & next_dir & """><b>LOCATION</b></a>"
	elseif (sort = "completion") then
		sql_sort = "completion " & sort_dir
		th_completion = "<a href=""portfolio_all.asp?" & parameters & "&sort=completion&dir=" & next_dir & """><b>COMPLETION</b></a>"
	elseif (sort = "desc") then
		sql_sort = "a.description " & sort_dir
		th_desc = "<a href=""portfolio_all.asp?" & parameters & "&sort=desc&dir=" & next_dir & """><b>DESCRIPTION</b></a>"		
	else 
		sql_sort = "a.name " & sort_dir
		th_name = "<a href=""portfolio_all.asp?" & parameters & "&sort=name&dir=desc""><b>NAME</b></a>"
	end if	
	
	
	if (cid = clng(DISC_MIX_USE)) then 
	title = "Mixed Use Communities"
	title_image = "title_mixed_use_communities.jpg"
	type_description = "Clark is on the cutting edge of building mixed-use and transit-oriented developments in both urban and suburban markets. A rising demand for mixed-use developments stems from the need for compact communities that offer residential units, retail, and commercial space. Through efficient and effective infrastructures and streamlined construction processes, Clark Builders Group has answered this new market demand."
	elseif (cid = clng(DISC_LUX_APT)) then
	title = "Luxury Apartments"
	title_image = "title_luxury_apartments.jpg"
	type_description = "Clark Realty develops, constructs, and manages many styles of luxury and market-rate apartments, including mid-rise communities, high-rise residential towers, mixed-use developments, and ""A+"" rental townhomes. Clark Realty's portfolio includes thousands of luxury residences, many of which have been recognized with the industry's most prestigious awards. Honors include Grand Aurora and Aurora Awards from the Southeast Builder's Conference, AOBA Apartment Community Excellence Awards, Mid-Atlantic Rental and Condo Industry Awards from Delta Associates, Pillars of the Industry Awards from the NAHB's Multifamily Council, and Finest for Family Living Awards from the NVBIA and MNBIA."
	elseif (cid = clng(DISC_MIL_COM)) then
	title = "Military Housing"
	title_image = "title_military_housing.jpg"
	type_description = "Clark Builders Group is proud to be the nation's preeminent builder of quality housing for members of the Army, Navy, Air Force, and Marines and their families. Our team is replacing the military's inventory of obsolete and inadequate housing with the safe, comfortable homes these families deserve.  In our state-of-the-art military neighborhoods, thoughtful amenities come together with inspired design to give military personnel and their families a high-morale environment and a sense of community. Clark Builders Group has garnered numerous industry awards in this segment, including Multifamily Executive's Project of the Year, Military Project of the Year, and Best Use of Technology award."
	elseif (cid = clng(DISC_SEN_USE)) then
	title = "Senior Living"
	title_image = "title_senior_living.jpg"
	type_description = "Clark Builders Group has constructed award-winning senior living facilities that offer a wide variety of services to meet the changing needs of today's active adults. Our experience includes assisted- and independent-living communities; upscale, mid-rise senior housing; and life care condominium towers with on-site skilled nursing facilities."
	elseif (cid = clng(DISC_HIG_DEN)) then
	title = "High-Density Apartments"
	title_image = "title_high_density_apartmen.jpg"
	type_description = "Clark Builders Group also excels at constructing quality affordable housing in metropolitan areas across the United States. We have familiarity with construction standards, draw requirements, and certification procesdures often required when developments are financed through tax credits, HUD, or other housing programs sponsored by various state agencies."
	elseif (cid = clng(DISC_AFF_HOU)) then
	title = "Affordable Housing"
	title_image = "title_affordable_housing.jpg"
	type_description = "Clark Builders Group also excels at constructing quality affordable housing in metropolitan areas across the United States.  We have familiarity with construction standards, draw requirements, and certification procesdures often required when developments are financed through tax credits, HUD, or other housing programs sponsored by various state agencies. "
	elseif (cid = clng(DISC_MSC_ROH)) then
	title = "Retail/Office/Hospitality"
	title_image = "title_retail_office_hospitality.jpg"
	type_description = "As an enhancement to its diverse multifamily portfolio of work, Clark Builders Group also constructs wood-framed retail, office, and hospitality properties."	




	elseif (cid = clng(DISC_SUS_BUI)) then
	title = "Sustainable Building"
	title_image = "title_sustainable_building.jpg"
	type_description = "Clark Builders Group boasts an expansive portfolio of innovative and award-winning projects that set the standard for environmental accountability within our industry."	



	end if

	set rsProjectsByCat=server.createobject("adodb.recordset")
	sql="select a.*, convert(datetime,a.item_value) completion from (select distinct ipm_project.projectid, ipm_project.name, ipm_project.city, ipm_project.state_id, ipm_project.description, a.item_value from ipm_project, ipm_project_discipline,ipm_project_field_value a, ipm_project_field_desc b, ipm_project_field_desc e, ipm_project_field_value f where ipm_project.projectid = f.projectid and e.item_id = f.item_id and e.item_tag = 'IDAM_PROJECT_CC_WEBSITE' and f.item_value = '1' and ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND keyid = " & cid & " AND a.projectid = ipm_project.projectID AND a.item_id = b.item_id AND b.item_tag = 'IDAM_PROJECT_COMPLETION' and a.item_value <> '') a"
	sql= sql & " UNION ALL "
	sql= sql & "select a.*, convert(datetime,'1/1/2050') completion from (select distinct ipm_project.projectid, ipm_project.name, ipm_project.city, ipm_project.state_id, ipm_project.description, a.item_value from ipm_project, ipm_project_discipline,ipm_project_field_value a, ipm_project_field_desc b, ipm_project_field_desc e, ipm_project_field_value f where ipm_project.projectid = f.projectid and e.item_id = f.item_id and e.item_tag = 'IDAM_PROJECT_CC_WEBSITE' and f.item_value = '1' and ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND keyid = " & cid & " AND a.projectid = ipm_project.projectID AND a.item_id = b.item_id AND b.item_tag = 'IDAM_PROJECT_COMPLETION' and a.item_value = '') a"
	sql = sql & " order by " & sql_sort	
	'sql="select a.*, convert(datetime,a.item_value) completion from (select distinct ipm_project.projectid, ipm_project.name, ipm_project.city, ipm_project.state_id, c.item_value roles, ipm_project.description, a.item_value from ipm_project, ipm_project_discipline,ipm_project_field_value a, ipm_project_field_desc b,ipm_project_field_value c, ipm_project_field_desc d, ipm_project_field_desc e, ipm_project_field_value f where ipm_project.projectid = f.projectid and e.item_id = f.item_id and e.item_tag = 'IDAM_PROJECT_CC_WEBSITE' and f.item_value = '1' and ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND keyid = " & cid & " AND a.projectid = ipm_project.projectID AND a.item_id = b.item_id AND b.item_tag = 'IDAM_PROJECT_COMPLETION' AND c.projectid = ipm_project.projectID AND c.item_id = d.item_id AND d.item_tag = 'IDAM_PROJECT_ROLE' and a.item_value <> '') a"
	'sql= sql & " UNION ALL "
	'sql= sql & "select a.*, convert(datetime,'1/1/2050') completion from (select distinct ipm_project.projectid, ipm_project.name, ipm_project.city, ipm_project.state_id, c.item_value roles, ipm_project.description, a.item_value from ipm_project, ipm_project_discipline,ipm_project_field_value a, ipm_project_field_desc b,ipm_project_field_value c, ipm_project_field_desc d, ipm_project_field_desc e, ipm_project_field_value f where ipm_project.projectid = f.projectid and e.item_id = f.item_id and e.item_tag = 'IDAM_PROJECT_CC_WEBSITE' and f.item_value = '1' and ipm_project.projectid = ipm_project_discipline.projectid AND AVAILABLE = 'Y' AND publish = 1 AND keyid = " & cid & " AND a.projectid = ipm_project.projectID AND a.item_id = b.item_id AND b.item_tag = 'IDAM_PROJECT_COMPLETION' AND c.projectid = ipm_project.projectID AND c.item_id = d.item_id AND d.item_tag = 'IDAM_PROJECT_ROLE' and a.item_value = '') a"
	'sql = sql & " order by " & sql_sort
	rsProjectsByCat.Open sql, Conn, 1, 4
	

	


%>

<html>
<head>
<title><%=sPageTitle%> - Portfolio</title>
	<!--#include file="includes\header.asp" -->
</head>
<body>
	<div id="drop_shadow">
		<div id="container">
			
			<!--#include file="includes\page_header.asp" -->
			
			<!--#include file="includes\menu.asp" -->
			
			<div id="content">

					<div id="content_text">
					<small><a href="portfolio.asp">Portfolio</a> | <%=title%></small>
					<br /><br />
					<img src="images/<%=title_image%>" alt="<%=title%>" />
					<br /><br />
					<%=type_description%>
					<br /><br />
					<table width="950">
						<tr>
							<th></th>
							<th><%=th_name%></th>
							<th><%=th_loc%></th>
							<th><%=th_desc%></th>
							<th><%=th_completion%></th>
						</tr>
						<%
						z = 1
						do while not rsProjectsByCat.eof

						
						if (z mod 2 = 0) then 
						lineStyle = "style=""background:#f6f3e7;"""
						lineStyle2 = "style=""background:url(images/bg_portfolio_photo_odd.jpg);"""
						else
						lineStyle =""
						lineStyle2 = ""
						end if
						
						if(rsProjectsByCat("completion") <> "1/1/2050") then
						lineDate = CDate(rsProjectsByCat("completion"))
						lineDate = MonthName(DatePart("m",lineDate)) & " " & DatePart("yyyy",lineDate)
						else
						lineDate = "TBA"
						end if
						
						z = z + 1
						%>

							<tr>
								<td <%=lineStyle%> align="center"><div class="portfolio_bg" <%=lineStyle2%>><a href="project.asp?pid=<%=rsProjectsByCat("projectID")%>"><img class="portfolio_photo" src="<%=sAssetPath%><%=rsProjectsByCat("projectID")%>&Instance=<%=siDAMInstance%>&type=project&size=1&width=137&height=108&qfactor=<%=assetQFactor%>" alt="<%=trim(rsProjectsByCat("name"))%>"/></a></div></td>
								<td <%=lineStyle%> align="center"><%=trim(rsProjectsByCat("name"))%></td>
								<td <%=lineStyle%> align="center"><%=rsProjectsByCat("city")%>, <%=rsProjectsByCat("state_id")%></td>
								<td <%=lineStyle%> align="center"><%=rsProjectsByCat("description")%></td>
								<td <%=lineStyle%> align="center"><%=lineDate%></td>
							</tr>
						
						<%	
						rsProjectsByCat.moveNext
						
						loop						
						%>
					
					</table>
					
					</div>
					
					
					<!--#include file="includes\footer.asp" -->
				
			</div>
			
		
		</div>
	</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5401213-1");
pageTracker._trackPageview();
</script></body>
</html>
