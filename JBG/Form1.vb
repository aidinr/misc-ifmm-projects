Imports System.Data.OleDb
Imports System.Configuration

Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub


    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ConnectionStringIDAM As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
    Public ConnectionStringVISION As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringVISION")

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents chkUpdateOnly As System.Windows.Forms.CheckBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.ListView1 = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.Button2 = New System.Windows.Forms.Button
        Me.chkUpdateOnly = New System.Windows.Forms.CheckBox
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.Button9 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(192, 16)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(208, 32)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Execute JBG Data Extraction"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(16, 368)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(472, 23)
        Me.ProgressBar1.TabIndex = 2
        '
        'ListView1
        '
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.ListView1.FullRowSelect = True
        Me.ListView1.Location = New System.Drawing.Point(16, 56)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(472, 304)
        Me.ListView1.TabIndex = 3
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "ProjectName"
        Me.ColumnHeader1.Width = 500
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(136, 416)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(232, 32)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Move Processed to Repository"
        '
        'chkUpdateOnly
        '
        Me.chkUpdateOnly.Location = New System.Drawing.Point(408, 24)
        Me.chkUpdateOnly.Name = "chkUpdateOnly"
        Me.chkUpdateOnly.Size = New System.Drawing.Size(112, 16)
        Me.chkUpdateOnly.TabIndex = 5
        Me.chkUpdateOnly.Text = "Update Only"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(16, 16)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(144, 32)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "Pre-Process FileListing"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(16, 416)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(88, 32)
        Me.Button4.TabIndex = 7
        Me.Button4.Text = "Add Asset Overrides"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(392, 416)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(96, 32)
        Me.Button5.TabIndex = 8
        Me.Button5.Text = "Project UDFS"
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(136, 480)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(232, 32)
        Me.Button6.TabIndex = 9
        Me.Button6.Text = "Project Plate Upload"
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(531, 56)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(144, 32)
        Me.Button7.TabIndex = 10
        Me.Button7.Text = "Data Migration Folders"
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(531, 115)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(144, 32)
        Me.Button8.TabIndex = 11
        Me.Button8.Text = "Data Migration Groups"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(531, 180)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(144, 32)
        Me.Button9.TabIndex = 12
        Me.Button9.Text = "Data Migration Permissions"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(901, 533)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.chkUpdateOnly)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.ListView1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        'get category id to populate
        Dim catIDUploadLocation As String '= GetDataTable("select category_id from ipm_category where name = 'Projects' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")
        catIDUploadLocation = "0"
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Dim Allprojects As DataTable = GetDataTable("select * from importii ", ConnectionStringIDAM)  'and oid = '041018.00'
        Dim i As Integer

        ProgressBar1.Maximum = Allprojects.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To Allprojects.Rows.Count - 1
            Try
                ProgressBar1.Value = ProgressBar1.Value + 1

                Dim sAlliance As String = ""
                Dim sAllianceFolder, sVisionProjectNumber As String
                Dim sAllianceCatID, sProjectID As String
                Dim sDirectoryRoot As String = "V:\"
                Dim sProjectName As String = Allprojects.Rows(i)("property")
                ListView1.Items.Add(sProjectName)

                ListView1.Refresh()
                Me.Refresh()

                sVisionProjectNumber = Allprojects.Rows(i)("id")

                'get Alliance
                sAlliance = Allprojects.Rows(i)("keyword")

                'create alliance dir if needed
                'does it exist already?
                sAllianceCatID = ""
                If GetDataTable("select category_id from ipm_category where parent_cat_id = " & catIDUploadLocation & " and name = '" & sAlliance.Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                    'get cat id
                    sAllianceCatID = GetDataTable("select category_id from ipm_category where parent_cat_id = " & catIDUploadLocation & " and name = '" & sAlliance.Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("Category_id")
                Else
                    sAllianceCatID = CreateNewAllianceCategory(sAlliance, catIDUploadLocation, "1")
                End If

                'create project folders if needed
                Dim sProjectfolderID As String = "0"
                'create project if needed
                sProjectID = ""
                If GetDataTable("select projectid from ipm_project where category_id = " & sAllianceCatID & " and name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                    'get cat id
                    sProjectID = GetDataTable("select projectid from ipm_project where category_id = " & sAllianceCatID & " and name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("projectid")
                Else
                    sProjectID = CreateNewProject(sProjectName.Trim, sAllianceCatID, sVisionProjectNumber)
                    sProjectfolderID = checkorCreateProjectfolder("0", "Construction Photos", sProjectID)
                    sProjectfolderID = checkorCreateProjectfolder("0", "Finance", sProjectID)
                    sProjectfolderID = checkorCreateProjectfolder("0", "Marketing Materials", sProjectID)
                    sProjectfolderID = checkorCreateProjectfolder("0", "Project Design", sProjectID)
                    'update fields
                    ExecuteTransaction("update ipm_project set address = '" & Allprojects.Rows(i)("street") & "' where projectid = " & sProjectID)
                    ExecuteTransaction("update ipm_project set city = '" & Allprojects.Rows(i)("city") & "' where projectid = " & sProjectID)
                    ExecuteTransaction("update ipm_project set zip = '" & Allprojects.Rows(i)("zipcode") & "' where projectid = " & sProjectID)

                End If




                'add keyword (Level)

                Dim sDiscipline As String = Allprojects.Rows(i)("keyword")
                'check for discpliine
                Dim DTDiscipline As DataTable = GetDataTable("select keyid from ipm_discipline where keyuse = 1 and keyname = '" & sDiscipline.Trim().Replace("'", "''") & "'", Me.ConnectionStringIDAM)
                If DTDiscipline.Rows.Count > 0 Then
                    'discpline exists so simply reference
                    ExecuteTransaction("delete from ipm_project_discipline where projectid = " & Allprojects.Rows(i)("id") & " and keyid = " & DTDiscipline.Rows(0)("Keyid"))
                    ExecuteTransaction("insert into ipm_project_discipline (keyid,projectid) values (" & DTDiscipline.Rows(0)("Keyid") & "," & sProjectID & ")")
                Else
                    'add to disipline table
                    Dim sql As String
                    sql = "exec sp_createnewkeywordDisciplineType "
                    sql += "1,"
                    sql += "'" + sDiscipline.ToString().Replace("'", "''") + "', "
                    sql += "1" + ", "
                    sql += "'Imported from external source'"
                    ExecuteTransaction(sql)
                    'get ID of repo just inserted
                    Dim NewKeywordID As String = GetDataTable("select max(keyid) maxid from ipm_discipline", Me.ConnectionStringIDAM).Rows(0)("maxid")
                    'add external id
                    ExecuteTransaction("insert into ipm_project_discipline (keyid,projectid) values (" & NewKeywordID & "," & sProjectID & ")")
                End If

                'add user defined fields
                Dim sitem_id As String = ""
                For Each column As DataColumn In Allprojects.Columns
                    'check to see if udf
                    If GetDataTable("select item_id from ipm_project_field_desc where item_tag = '" & column.ColumnName & "'", Me.ConnectionStringIDAM).Rows.Count > 0 Then
                        'exists
                        sitem_id = GetDataTable("select item_id from ipm_project_field_desc where item_tag = '" & column.ColumnName & "'", Me.ConnectionStringIDAM).Rows(0)("item_id")
                        ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sitem_id)
                        ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sitem_id & ",'" & Allprojects.Rows(i)(column.ColumnName) & "')")
                    End If
                Next


            Catch ex As Exception

                Me.SendError(ex.Message, "MAIN CATCH", "UNKNOWN")
            End Try
        Next i
        Dim x As String = "what the"

    End Sub

    Function checkorCreateProjectfolder(ByVal parentcatid As String, ByVal sName As String, ByVal sProjectID As String, Optional ByVal bypassSec As Boolean = False) As String

        'check is exists
        Dim sProjectFolderID As String
        Try
            sProjectFolderID = GetDataTable("select category_id from ipm_asset_category where projectid = " & sProjectID & " and parent_cat_id = " & parentcatid & " and name = '" & sName.Trim.Replace("'", "''") & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")
            Return sProjectFolderID
        Catch ex As Exception
            Try
                'create category
                Dim sql As String
                sql = "exec sp_createnewassetcategory_wparent 1," & parentcatid & ","
                sql += "'" & Replace(sName, "'", "''") & "',"
                sql += sProjectID & ","
                sql += "'Y',"
                sql += "'',"
                sql += "'1'"
                ExecuteTransaction(sql)
                'get ID of category just inserted
                Dim catID As String = GetDataTable("select max(category_id) maxid from ipm_asset_category", Me.ConnectionStringIDAM).Rows(0)("maxid")
                sProjectFolderID = catID
                'add security defaults
                If Not bypassSec Then
                    sql = "select * from ipm_project_security where projectid = " & sProjectID
                    Dim securityCatTable As DataTable = GetDataTable(sql, Me.ConnectionStringIDAM)
                    Dim srow As DataRow
                    For Each srow In securityCatTable.Rows
                        sql = "insert into ipm_category_security (category_id, security_id, type) values (" & catID & ",'" & srow("security_id") & "','" & srow("type") & "')"
                        ExecuteTransaction(sql)
                    Next
                End If

                Return sProjectFolderID
            Catch exx As Exception
                Return "0"
            End Try
        End Try


    End Function

    Function addProjectfolderSecurity(ByVal groupid As String, ByVal folderid As String)
        Dim sql As String
        Try
            sql = "insert into ipm_category_security (category_id, security_id, type) values (" & folderid & ",'" & groupid & "','G')"
            ExecuteTransaction(sql)
        Catch ex As Exception
            SendError(ex.Message, "addProjectfolderSecurity", folderid + "::" + groupid)
        End Try

    End Function



    Function SendError(ByVal sError, Optional ByVal object_type = "", Optional ByVal object_id = "")
        'id	float	Unchecked
        'date	datetime	Checked
        'error_level	varchar(50)	Checked
        'machine_id	varchar(50)	Checked
        'app_id	varchar(50)	Checked
        'instance_id	varchar(50)	Checked
        'class_name	varchar(255)	Checked
        'output	varchar(4000)	Checked
        'object_type	varchar(50)	Checkeds
        'object_id	varchar(50)	Checked
        Dim sql As String
        sError = sError.ToString.Replace("'", "''")
        Dim ErrorID As String = 1
        Try
            ErrorID = GetDataTable("select max(id) + 1 maxid from ipm_error", Me.ConnectionStringIDAM).Rows(0)("maxid")
            If ErrorID Is System.DBNull.Value Then
                ErrorID = 1
            End If
        Catch ex As Exception
            ErrorID = 1
        End Try
        Try
            sql = "insert into ipm_error (id,date,output,object_type,object_id) values (" & ErrorID & ",getdate(),'" & sError & "','" & object_type & "','" & object_id & "')"
            ExecuteTransaction(sql)
        Catch ex As Exception

        End Try

    End Function




    Function CreateNewAllianceCategory(ByVal categoryName As String, ByVal parent_id As String, ByVal securitylevel As String) As String
        Try
            Dim sql As String
            sql = "exec sp_createnewcategory 1,"
            sql += "'" & Replace(categoryName, "'", "''") & "',"
            sql += Replace(parent_id, "CAT", "") & ","
            sql += "'Y',"
            sql += "'',"
            sql += "'" & securitylevel & "'"
            ExecuteTransaction(sql)
            'get ID of category just inserted
            Dim catID As String = GetDataTable("select max(category_id) maxid from ipm_category", Me.ConnectionStringIDAM).Rows(0)("maxid")
            'add security defaults
            sql = "select * from ipm_category_security where category_id = " & Replace(parent_id, "CAT", "")
            Dim securityCatTable As DataTable = GetDataTable(sql, Me.ConnectionStringIDAM)
            Dim srow As DataRow
            For Each srow In securityCatTable.Rows
                sql = "insert into ipm_category_security (category_id, security_id, type) values (" & catID & ",'" & srow("security_id") & "','" & srow("type") & "')"
                ExecuteTransaction(sql)
            Next
            Return catID
        Catch ex As Exception
            Return "0"
        End Try
    End Function

    Private Function CreateNewProject(ByVal sProjectName As String, ByVal sCatID As String, ByVal sProjectNumber As String) As String
        'here we go...
        'check to see if external reference
        'MITHUN_PROJECT_MAPPING()
        Dim sProjectID As String = ""
        Try


            Dim sql As String
            'Dim sProjectNumber As String = ProjectItem("oid")
            'Dim sProjectName As String = ProjectItem("name")
            ExecuteTransaction("exec sp_createnewproject2 " & sCatID & ",1,'" & Replace(sProjectName, "'", "''") & "','Imported from automated procedure.','1'")
            ''get ID of category just inserted
            sProjectID = GetDataTable("select max(projectid) maxid from ipm_project", Me.ConnectionStringIDAM).Rows(0)("maxid")
            'add security defaults
            sql = "select * from ipm_category_security where category_id = " & sCatID
            Dim securityCatTable As DataTable = GetDataTable(sql, Me.ConnectionStringIDAM)
            Dim srow As DataRow
            For Each srow In securityCatTable.Rows
                sql = "insert into ipm_project_security (projectid, security_id, type) values (" & sProjectID & ",'" & srow("security_id") & "','" & srow("type") & "')"
                ExecuteTransaction(sql)
            Next
            Dim catID As String = sProjectID
            'import data
            'add additional project information (#,long name, project discipline = discipline, project alliance = services,  type = office
            'add project code to description


            'End If
            'get newly added project id
            Return sProjectID.ToString
        Catch ex As Exception
            If sProjectID <> "" Then
                Return sProjectID
            Else
                Return "0"
            End If

        End Try

    End Function


    Private Sub ExecuteZGFSQL(ByVal catID As String, ByVal sProjectNumber As String)
        Dim sql As String


        'add keywords
        'If chkprojectinformation.Checked Then
        'get the keywords
        'add project discipline = discipline, project(alliance = keyword, Type = office)
        ''discipline
        Dim SourceKeywords As DataTable = GetDataTable("select custdesignservice from projectcustomtabfields where wbs1 = '" & sProjectNumber.Trim & "' and wbs2 = ''", Me.ConnectionStringVISION)
        If SourceKeywords.Rows.Count > 0 Then
            Dim sDiscipline As String = Me.isullcheck(SourceKeywords.Rows(0)("custdesignservice"))
            If sDiscipline <> "" Then
                'check for discpliine
                Dim DTDiscipline As DataTable = GetDataTable("select keyid from ipm_discipline where keyuse = 1 and keyname = '" & sDiscipline.Trim().Replace("'", "''") & "'", Me.ConnectionStringIDAM)
                If DTDiscipline.Rows.Count > 0 Then
                    'discpline exists so simply reference
                    ExecuteTransaction("delete from ipm_project_discipline where projectid = " & catID & " and keyid = " & DTDiscipline.Rows(0)("Keyid"))
                    ExecuteTransaction("insert into ipm_project_discipline (keyid,projectid) values (" & DTDiscipline.Rows(0)("Keyid") & "," & catID & ")")
                Else
                    'add to disipline table
                    sql = "exec sp_createnewkeywordDisciplineType "
                    sql += "1,"
                    sql += "'" + sDiscipline.ToString().Replace("'", "''") + "', "
                    sql += "1" + ", "
                    sql += "'Imported from external source'"
                    ExecuteTransaction(sql)
                    'get ID of repo just inserted
                    Dim NewKeywordID As String = GetDataTable("select max(keyid) maxid from ipm_discipline", Me.ConnectionStringIDAM).Rows(0)("maxid")
                    'add external id
                    ExecuteTransaction("insert into ipm_project_discipline (keyid,projectid) values (" & NewKeywordID & "," & catID & ")")
                End If
            End If
        End If


        'add project type
        Dim SourceType As DataTable = GetDataTable("select cfgprojecttype.* from pr,cfgprojecttype where pr.projecttype = cfgprojecttype.code and pr.wbs1 = '" & sProjectNumber & "' and pr.wbs2 = ''", Me.ConnectionStringVISION)
        If SourceType.Rows.Count > 0 Then
            Dim sType As String = SourceType.Rows(0)("description")

            'check for discpliine
            Dim DTType As DataTable = GetDataTable("select keyid from ipm_office where keyuse = 1 and keyname = '" & sType.Trim().Replace("'", "''") & "'", Me.ConnectionStringIDAM)
            If DTType.Rows.Count > 0 Then
                'discpline exists so simply reference
                ExecuteTransaction("delete from ipm_project_office where projectid = " & catID & " and officeid = " & DTType.Rows(0)("Keyid"))
                ExecuteTransaction("insert into ipm_project_office (officeid,projectid) values (" & DTType.Rows(0)("Keyid") & "," & catID & ")")
            Else
                'add to disipline table
                sql = "exec sp_createnewkeywordOfficeType "
                sql += "1,"
                sql += "'" + sType.ToString().Replace("'", "''") + "', "
                sql += "1" + ", "
                sql += "'Imported from external source'"
                ExecuteTransaction(sql)
                'get ID of repo just inserted
                Dim NewKeywordID As String = GetDataTable("select max(keyid) maxid from ipm_office", Me.ConnectionStringIDAM).Rows(0)("maxid")
                'add external id
                ExecuteTransaction("insert into ipm_project_office (officeid,projectid) values (" & NewKeywordID & "," & catID & ")")
            End If
        End If

        'add additional project description data
        'add owners
        If 1 = 1 Then
            'add project type
            Dim SourceType2 As DataTable = GetDataTable("select principal,projmgr from pr where pr.wbs1 = '" & catID & "' and pr.wbs2 = ''", Me.ConnectionStringVISION)
            If SourceType2.Rows.Count > 0 Then
                Dim sPrincipal As String = SourceType2.Rows(0)("principal")
                Dim sProjManger As String = SourceType2.Rows(0)("projmgr")

                'check 
                If sPrincipal <> "" Then
                    'discpline exists so simply reference
                    ExecuteTransaction("update ipm_project set principalid = '" & sPrincipal & "' where projectid = " & catID)
                End If
                If sProjManger <> "" Then
                    'discpline exists so simply reference
                    ExecuteTransaction("update ipm_project set pmid = '" & sProjManger & "' where projectid = " & catID)
                End If
            End If
        End If




    End Sub





    Public Shared Sub ExecuteTransaction(ByVal sqlstring As String)
        Dim ConnectionString As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
        Dim connection As New OleDbConnection(ConnectionString)
        Dim command As New OleDbCommand
        Dim transaction As OleDbTransaction

        ' Set the Connection to the new OleDbConnection.
        command.Connection = connection

        ' Open the connection and execute the transaction.
        Try
            connection.Open()

            ' Start a local transaction with ReadCommitted isolation level.
            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            ' Assign transaction object for a pending local transaction.
            command.Connection = connection
            command.Transaction = transaction

            ' Execute the commands.
            command.CommandText = sqlstring
            command.ExecuteNonQuery()

            ' Commit the transaction.
            transaction.Commit()
            Console.WriteLine("Executed.")

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Try to rollback the transaction
            Try
                transaction.Rollback()

            Catch
                ' Do nothing here; transaction is not active.
            End Try
        End Try
        ' The connection is automatically closed when the
        ' code exits the Using block.

    End Sub

    Public Shared Function GetDataTable(ByVal query As String, ByVal ConnectionString As String) As DataTable
        'Dim connection1 As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & System.Web.HttpContext.Current.Server.MapPath(dbFile))
        Dim connection1 As OleDbConnection = New OleDbConnection(ConnectionString)
        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)
        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()
        End Try
        Return table1
    End Function
    Function isullcheck(ByVal v)
        If v Is System.DBNull.Value Then
            isullcheck = ""
        Else
            isullcheck = v
        End If
    End Function

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim sRootRepository As String = "d:\IDAM\repository\"
        Dim AllFiles As DataTable = GetDataTable("select b.asset_id, replace(a.directory,'MarketingMasters\','') directory, a.files, b.projectid, c.extension from filelisting a, ipm_asset b, ipm_filetype_lookup c where a.id = b.asset_id and b.media_type = c.media_type and a.processed = 4", ConnectionStringIDAM)
        Dim i As Integer
        Dim sExtensionid As String
        Dim sSourceFile As String
        Dim sDestinationFile As String
        Dim sTifSwitch As Boolean = False
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                If InStr(AllFiles.Rows(i)("Files"), ".jpg") > 0 Then 'only process jpg files
                    'get SourceFile
                    sSourceFile = ""
                    sDestinationFile = ""
                    sSourceFile = "x:\" & AllFiles.Rows(i)("Directory") + "\" + AllFiles.Rows(i)("Files")
                    sDestinationFile = sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource\" + AllFiles.Rows(i)("asset_id").ToString.Trim + "." + AllFiles.Rows(i)("extension").ToString.Trim
                    'is directories created?
                    If Not IO.Directory.Exists(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim) Then
                        IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim)
                        IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource")
                        IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\acrobat")
                    End If
                    'now copy files
                    'check to see if tiff exists .tif
                    If IO.File.Exists(Replace(sSourceFile, ".jpg", ".tif")) Then
                        'delete jpg
                        'IO.File.Delete(sDestinationFile)
                        IO.File.Copy(Replace(sSourceFile, ".jpg", ".tif"), Replace(sDestinationFile, ".jpg", ".tif"), False)
                        'change extension of asset
                        ExecuteTransaction("update ipm_asset set media_type = '85', name = '" & Replace(AllFiles.Rows(i)("Files"), ".jpg", ".tif") & "', filename = '" & Replace(AllFiles.Rows(i)("Files"), ".jpg", ".tif") & "' where asset_id = " & AllFiles.Rows(i)("asset_id"))
                        sTifSwitch = True
                    Else
                        'already there...IO.File.Copy(sSourceFile, sDestinationFile, False)
                    End If

                    'now send to ipm_asset_queue and mark processed = 4
                    If sTifSwitch Then
                        Me.ExecuteTransaction("insert into ipm_asset_queue(instance_id,asset_id,asset_handler,status,priority,engine_id)values('IDAM_NBBJ'," & AllFiles.Rows(i)("asset_id").ToString.Trim & ",2,0,1,'20')")
                        Me.ExecuteTransaction("update filelisting set processed = 5 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)
                    End If
                End If
                Me.ExecuteTransaction("update filelisting set processed = 7 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)
            Catch ex As Exception
                'MsgBox(ex.Message & AllFiles.Rows(i)("asset_id"))
                Me.ExecuteTransaction("update filelisting set processed = 6 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)
            End Try
            ProgressBar1.Value = ProgressBar1.Value + 1
        Next

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'copy field values from cumulus to filelisting

        Dim AllFiles As DataTable = GetDataTable("select a.id,a.[asset identifier],a.[asset name],a.[file data size],a.[creation date],a.[modification date],a.[folder name],[cumulus project name],[project number],[cumulus market name],[vision market name],[vision long name]from cumulus a, cumulus_mapping b where SUBSTRING(replace([folder name],'MarketingMasters\IMAGES\',''),PATINDEX('%\%',replace([folder name],'MarketingMasters\IMAGES\',''))+1,100) = b.[cumulus project name]", ConnectionStringIDAM)
        Dim i As Integer
        Dim x As Integer = 0
        Dim sql As String
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try

                sql = "insert into filelisting (id,directory,files,filedate,assetid,processed,alliance,projectname,projectfolder,alliancefolder,oid) values " & _
                "('" & Me.isullcheck(AllFiles.Rows(i)("id")) & "','" & Me.isullcheck(AllFiles.Rows(i)("folder name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("asset name")) & "','" & Me.isullcheck(AllFiles.Rows(i)("creation date")) & "','" & Me.isullcheck(AllFiles.Rows(i)("asset identifier")) & "',0,'" & Me.isullcheck(AllFiles.Rows(i)("vision market name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("vision long name")).replace("'", "''") & "','','" & Me.isullcheck(AllFiles.Rows(i)("vision market name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("project number")).replace("'", "''") & "')"
                Me.ExecuteTransaction(sql)

            Catch ex As Exception

                x += 1
            End Try
        Next




        'get all others not linked to vision project
        AllFiles = GetDataTable("select a.id,a.[asset identifier],a.[asset name],a.[file data size],a.[creation date],a.[modification date],a.[folder name],SUBSTRING(replace([folder name],'MarketingMasters\IMAGES\',''),PATINDEX('%\%',replace([folder name],'MarketingMasters\IMAGES\',''))+1,100) [cumulus project name],''[project number],'Miscellaneous' [cumulus market name],'Miscellaneous' [vision market name],SUBSTRING(replace([folder name],'MarketingMasters\IMAGES\',''),PATINDEX('%\%',replace([folder name],'MarketingMasters\IMAGES\',''))+1,100) [vision long name]from cumulus a,(select id from cumulus where id not in (select a.id from cumulus a, cumulus_mapping b where SUBSTRING(replace([folder name],'MarketingMasters\IMAGES\',''),PATINDEX('%\%',replace([folder name],'MarketingMasters\IMAGES\',''))+1,100) = b.[cumulus project name])) c where a.id = c.id ", ConnectionStringIDAM)
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try

                sql = "insert into filelisting (id,directory,files,filedate,assetid,processed,alliance,projectname,projectfolder,alliancefolder,oid) values " & _
                "('" & Me.isullcheck(AllFiles.Rows(i)("id")) & "','" & Me.isullcheck(AllFiles.Rows(i)("folder name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("asset name")) & "','" & Me.isullcheck(AllFiles.Rows(i)("creation date")) & "','" & Me.isullcheck(AllFiles.Rows(i)("asset identifier")) & "',0,'" & Me.isullcheck(AllFiles.Rows(i)("vision market name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("vision long name")).replace("'", "''") & "','','" & Me.isullcheck(AllFiles.Rows(i)("vision market name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("project number")).replace("'", "''") & "')"
                Me.ExecuteTransaction(sql)

            Catch ex As Exception

                x += 1
            End Try
        Next






    End Sub

    Private Sub Button1_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Disposed

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click


        '21503534:       General(Staff)
        '21503535        Market Studio 28                                  
        '21503536:       Marketing(Manager)
        '21503539:       Photo(Administrator)
        '21503537:       Marketing(Coordinator)
        '21503538:       Communications(Team)

        'add override for 



    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'add project udfs
        'get all projects with projectnums
        Dim DT_Temp As DataTable
        Dim AllFiles As DataTable = GetDataTable("select projectid, projectnumber from ipm_project where projectnumber <> '' and available = 'Y'", ConnectionStringIDAM)  'and oid = '041018.00'
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        Dim i As Integer


        '21503540: IDAM_VISION_PROJECT_DESCRIPTION()
        '21503541: IDAM_PRIMARY_STUDIO()
        '21503542: IDAM_CLIENT_NAME()
        '21503543: IDAM_PRIMARY_PROJECT_TYPE() = market
        '21503544: IDAM_VISION_PROJECT_NAME()
        '21503545: IDAM_PRIMARY_SERVICE()
        '21503715: IDAM_ACTUAL_COMPLETION_DATE()
        '21503717: IDAM_CONSTRUCTION_START_DATE()
        '21503718: IDAM_CONSTRUCTION_END_DATE()
        '21503719: IDAM_COMPETITION()
        '21503720: IDAM_COUNTRY()
        '21503721: IDAM_GEOGRAPHIC_REGION()
        '21503722: IDAM_AWARDS()
        '21503723: IDAM_PUBLICATIONS()
        '21503724: IDAM_BIM()
        '21503546: IDAM_CONFIDENTIAL()
        '21503714: IDAM_START_DATE()
        '21503716: IDAM_OCCUPANCY_DATE()


        For i = 0 To AllFiles.Rows.Count - 1
            'IDAM_VISION_PROJECT_DESCRIPTION
            DT_Temp = GetDataTable("select description from prdescriptions where desccategory = 'PROJDESC' and wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "'", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503540")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503540,'" & RemoveHtml(DT_Temp.Rows(0)("description")).Replace("'", "''") & "')")
            End If

            'IDAM_PRIMARY_PROJECT_TYPE
            DT_Temp = GetDataTable("select b.description from pr a, cfgprojecttype b where a.projecttype = b.code and a.wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503543")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503543,'" & RemoveHtml(DT_Temp.Rows(0)("description")).Replace("'", "''") & "')")
            End If


            'IDAM_CONSTRUCTION_START_DATE()
            DT_Temp = GetDataTable("select custConstructionStartDate from ProjectCustomTabFields where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503717")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503717,'" & DT_Temp.Rows(0)("custConstructionStartDate") & "')")
            End If


            'IDAM_ACTUAL_COMPLETION_DATE()
            DT_Temp = GetDataTable("select ActCompletionDate from pr where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503715")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503715,'" & DT_Temp.Rows(0)("ActCompletionDate") & "')")
            End If


            'IDAM_CONSTRUCTION_END_DATE()
            DT_Temp = GetDataTable("select ConstComplDate from pr where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503718")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503718,'" & DT_Temp.Rows(0)("ConstComplDate") & "')")
            End If

            'IDAM_COMPETITION()
            DT_Temp = GetDataTable("select custBuildingStatusCompetition from ProjectCustomTabFields where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503719")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503719,'" & Me.isullcheck(DT_Temp.Rows(0)("custBuildingStatusCompetition")).replace("'", "''") & "')")
            End If

            'IDAM_COUNTRY()
            DT_Temp = GetDataTable("select Country from pr where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503720")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503720,'" & Me.isullcheck(DT_Temp.Rows(0)("Country")).replace("'", "''") & "')")
            End If

            'IDAM_GEOGRAPHIC_REGION()
            DT_Temp = GetDataTable("select custlocationcode from ProjectCustomTabFields where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503721")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503721,'" & Me.isullcheck(DT_Temp.Rows(0)("custlocationcode")).replace("'", "''") & "')")
            End If

            '21503722: IDAM_AWARDS()
            Dim ii As Integer
            Dim sAwards As String = ""
            DT_Temp = GetDataTable("select description,institution,awarddate from prawards where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503722")
                For ii = 0 To DT_Temp.Rows.Count - 1
                    'erase any possibel previous value
                    sAwards += Me.isullcheck(DT_Temp.Rows(ii)("institution")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("awarddate")) & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("description")).replace("'", "''") & vbCrLf & vbCrLf
                Next
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503722,'" & sAwards & "')")
            End If


            'select custpublication,custpublisher,custpubdate,custpubarticle, from projects_publications order by seq
            '21503723: IDAM_PUBLICATIONS()
            Dim sPublications As String = ""
            DT_Temp = GetDataTable("select custpublication,custpublisher,custpubdate,custpubarticle from projects_publications where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = '' order by seq", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503723")
                For ii = 0 To DT_Temp.Rows.Count - 1
                    'erase any possibel previous value
                    sPublications += Me.isullcheck(DT_Temp.Rows(ii)("custpubdate")) & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpublisher")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpublication")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpubarticle")).replace("'", "''") & vbCrLf & vbCrLf
                Next
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503723,'" & sPublications & "')")
            End If

            '21503714: IDAM_START_DATE()
            DT_Temp = GetDataTable("select startdate from pr where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503714")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503714,'" & Me.isullcheck(DT_Temp.Rows(0)("startdate")) & "')")
            End If


            '21503716: IDAM_OCCUPANCY_DATE()
            DT_Temp = GetDataTable("select custOccupancyDate from ProjectCustomTabFields where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503716")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503716,'" & Me.isullcheck(DT_Temp.Rows(0)("custOccupancyDate")) & "')")
            End If

        Next

    End Sub






    Public Function RemoveHtml(ByVal sTEXT As String) As String
        Dim sTemp As String
        sTemp = System.Text.RegularExpressions.Regex.Replace(sTEXT, "<[^>]*>", "")
        Return sTemp
    End Function

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'add project plates
        '3 types of imports
        '1 projectid and projectnumber
        '2 projectid no project num = 
        '3 projectnum and no projectid = new project in misc folder - import from vision
        '4 neither = new project in misc folder


        'do not upload fonts


        'get category id to populate
        Dim catIDUploadLocation As String = GetDataTable("select category_id from ipm_category where name = 'Projects' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Dim AllFiles As DataTable = GetDataTable("select * from filelistingprojectplates where processed = 0 and files <> 'thumbs.db'", ConnectionStringIDAM)  'and oid = '041018.00'
        Dim i As Integer
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                ProgressBar1.Value = ProgressBar1.Value + 1
                If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("id"), ConnectionStringIDAM).Rows.Count = 0 Or Me.chkUpdateOnly.Checked Then
                    Dim sAlliance As String = ""
                    Dim sAllianceFolder, sVisionProjectNumber As String
                    Dim sAllianceCatID, sProjectID As String
                    Dim sDirectoryRoot As String = "V:\"
                    Dim sProjectName As String = AllFiles.Rows(i)("projectname")
                    ListView1.Items.Add(sProjectName + ": " + AllFiles.Rows(i)("files"))

                    ListView1.Refresh()
                    Me.Refresh()

                    'get vision project number
                    sVisionProjectNumber = AllFiles.Rows(i)("oid")

                    'get Alliance
                    sAlliance = AllFiles.Rows(i)("alliance")

                    'create alliance dir if needed
                    'does it exist already?
                    sAllianceCatID = ""
                    If GetDataTable("select category_id from ipm_category where parent_cat_id = " & catIDUploadLocation & " and name = '" & sAlliance.Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sAllianceCatID = GetDataTable("select category_id from ipm_category where parent_cat_id = " & catIDUploadLocation & " and name = '" & sAlliance.Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("Category_id")
                    Else
                        sAllianceCatID = CreateNewAllianceCategory(sAlliance, catIDUploadLocation, "1")
                    End If


                    'create project if needed
                    sProjectID = ""
                    If GetDataTable("select projectid from ipm_project where category_id = " & sAllianceCatID & " and name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sProjectID = GetDataTable("select projectid from ipm_project where category_id = " & sAllianceCatID & " and name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("projectid")
                    Else
                        sProjectID = CreateNewProject(sProjectName.Trim, sAllianceCatID, sVisionProjectNumber)
                    End If

                    'create project folders if needed
                    Dim sProjectFolderArray() As String
                    Dim sProjectfolderID As String = "0"
                    Try
                        If AllFiles.Rows(i)("folders").trim <> "" Then
                            sProjectFolderArray = AllFiles.Rows(i)("folders").split("\")
                            'assume value
                            Dim ii As Integer
                            For ii = 0 To sProjectFolderArray.Length - 1
                                If ii = 0 Then 'item at root

                                    sProjectfolderID = checkorCreateProjectfolder("0", sProjectFolderArray(ii), sProjectID)
                                Else
                                    If sProjectFolderArray(ii).Trim <> "" Then
                                        sProjectfolderID = checkorCreateProjectfolder(GetDataTable("select category_id from ipm_asset_category where projectid = " & sProjectID & " and name = '" & sProjectFolderArray(ii - 1).Trim.Replace("'", "''") & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id"), sProjectFolderArray(ii), sProjectID)
                                    Else
                                        'sProjectfolderID = "0"
                                    End If

                                End If
                            Next ii
                        Else
                            sProjectfolderID = "0"
                        End If
                    Catch ex As Exception
                        'put in general
                        sProjectfolderID = "0"
                    End Try



                    'project folder override for level
                    'get level
                    'select a.[approval status] from cumulus a, filelisting b where a.id = b.id and a.id = " &  AllFiles.Rows(i)("id")
                    Dim sLevel As String = "Level2"
                    Try
                        If GetDataTable("select a.[approval status] level from cumulus a, filelisting b where a.id = b.id and a.id = " & AllFiles.Rows(i)("id"), ConnectionStringIDAM).Rows(0)("level") = "L1" Then
                            sLevel = "Level1"
                        End If
                    Catch ex As Exception

                    End Try

                    'set folderid 
                    Try
                        Select Case sLevel
                            Case "Level1"
                                sProjectfolderID = GetDataTable("select category_id from ipm_asset_category where name = 'Images' and projectid = " & sProjectID, ConnectionStringIDAM).Rows(0)("category_id")
                            Case "Level2"
                                sProjectfolderID = GetDataTable("select category_id from ipm_asset_category where name = 'Supplemental Imagery' and projectid = " & sProjectID, ConnectionStringIDAM).Rows(0)("category_id")
                        End Select
                    Catch ex As Exception
                        sProjectfolderID = "0"
                    End Try



                    'get media_type
                    'get extension
                    Dim sExtensionid As String
                    Dim sExtensiontmp() = AllFiles.Rows(i)("Files").split(".")
                    Dim sExtension As String
                    If sExtensiontmp.Length = 1 Then
                        sExtension = "UNK"
                    Else
                        sExtensiontmp.Reverse(sExtensiontmp)
                        sExtension = sExtensiontmp(0)
                    End If
                    'check to see if in filetype_lookup
                    imTypeid = imTypeid + 1
                    Try
                        sExtensionid = GetDataTable("select * from ipm_filetype_lookup where extension = '" & sExtension & "'", Me.ConnectionStringIDAM).Rows(0)("media_type")
                    Catch ex As Exception

                        'add extension
                        Me.ExecuteTransaction("insert into ipm_filetype_lookup (media_type,name,extension,useicon) values (" & imTypeid & ",'" & sExtension & " File" & "','" & sExtension & "',1)")
                        sExtensionid = imTypeid
                    End Try


                    'add asset
                    Dim sAssetID As String
                    'sAssetID = CreateAsset("0","1",sAssetName,"1",sProjectID



                    'check to see if asset already exists and use update
                    If chkUpdateOnly.Checked Then
                        'update location_id,projectid,category_id,
                        If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("id") & " and projectid = " & sProjectID & " and category_id = " & sProjectfolderID, ConnectionStringIDAM).Rows.Count = 0 Then
                            'assume asset is already there just not in the right spot
                            Me.ExecuteTransaction("update ipm_asset set projectid = " & sProjectID & ",category_id = " & sProjectfolderID & " where asset_id = " & AllFiles.Rows(i)("id"))
                        End If
                    Else
                        Me.ExecuteTransaction("insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date) values ('" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sProjectID & ",'1'," & AllFiles.Rows(i)("id") & ",1,1,'" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sExtensionid & "," & sProjectID & "," & sProjectfolderID & ",'" & AllFiles.Rows(i)("filedate") & "','" & AllFiles.Rows(i)("filedate") & "')")
                    End If

                    'add keyword (Level)

                    Dim sDiscipline As String = sLevel
                    'check for discpliine
                    Dim DTDiscipline As DataTable = GetDataTable("select keyid from ipm_services where keyuse = 1 and keyname = '" & sDiscipline.Trim().Replace("'", "''") & "'", Me.ConnectionStringIDAM)
                    If DTDiscipline.Rows.Count > 0 Then
                        'discpline exists so simply reference
                        ExecuteTransaction("delete from ipm_asset_services where asset_id = " & AllFiles.Rows(i)("id") & " and keyid = " & DTDiscipline.Rows(0)("Keyid"))
                        ExecuteTransaction("insert into ipm_asset_services (keyid,asset_id) values (" & DTDiscipline.Rows(0)("Keyid") & "," & AllFiles.Rows(i)("id") & ")")
                    Else
                        'add to disipline table
                        Dim sql As String
                        sql = "exec sp_createnewkeywordServices "
                        sql += "1,"
                        sql += "'" + sDiscipline.ToString().Replace("'", "''") + "', "
                        sql += "1" + ", "
                        sql += "'Imported from external source'"
                        ExecuteTransaction(sql)
                        'get ID of repo just inserted
                        Dim NewKeywordID As String = GetDataTable("select max(keyid) maxid from ipm_services", Me.ConnectionStringIDAM).Rows(0)("maxid")
                        'add external id
                        ExecuteTransaction("insert into ipm_asset_services (keyid,projectid) values (" & NewKeywordID & "," & AllFiles.Rows(i)("id") & ")")
                    End If

                    'add user defined fields

                    Dim UDFSource As DataTable = GetDataTable("select * from cumulus where id = " & AllFiles.Rows(i)("id"), Me.ConnectionStringIDAM)
                    If UDFSource.Rows.Count > 0 Then
                        Dim IDAM_PHOTOGRAPHER_CREDIT, IDAM_PHOTOGRAPHER_CONTACT_INFO, IDAM_ASSET_RIGHTS, IDAM_ARCHIVE_CD, IDAM_CAPTION, IDAM_CUMULUS_CAPTION, IDAM_WORKING_CD As String
                        '21503637:               IDAM_PHOTOGRAPHER_CREDIT()
                        '21503638:               IDAM_PHOTOGRAPHER_CONTACT_INFO()
                        '21503639:               IDAM_ASSET_RIGHTS()
                        '21503640:               IDAM_ARCHIVE_CD()
                        '21503643:               IDAM_CAPTION()
                        '21503642:               IDAM_CUMULUS_CAPTION()
                        '21503641:               IDAM_WORKING_CD()

                        'assign photo credit
                        IDAM_PHOTOGRAPHER_CREDIT = isullcheck(UDFSource.Rows(0)("photographer(s)"))
                        If IDAM_PHOTOGRAPHER_CREDIT <> "" Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id= 21503637 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503637,'" & IDAM_PHOTOGRAPHER_CREDIT & "')")
                        End If

                        IDAM_ARCHIVE_CD = isullcheck(UDFSource.Rows(0)("Archive CD#"))
                        If IDAM_ARCHIVE_CD <> "" Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503640 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503640,'" & IDAM_ARCHIVE_CD & "')")
                        End If

                        IDAM_WORKING_CD = isullcheck(UDFSource.Rows(0)("Working CD#"))
                        If IDAM_WORKING_CD <> "" Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503641 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503641,'" & IDAM_WORKING_CD & "')")
                        End If

                        IDAM_CUMULUS_CAPTION = isullcheck(UDFSource.Rows(0)("Caption"))
                        If IDAM_CUMULUS_CAPTION <> "" And Not IDAM_CUMULUS_CAPTION.IndexOf("Adobe Photoshop 7.0") > 0 Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503642 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503642,'" & IDAM_CUMULUS_CAPTION & "')")
                        End If

                    End If


                    'add security defaults
                    Dim securityCatTable As DataTable
                    If sProjectfolderID = "0" Then
                        securityCatTable = GetDataTable("select * from ipm_project_security where projecid = " & sProjectID, Me.ConnectionStringIDAM)
                    Else
                        securityCatTable = GetDataTable("select * from ipm_category_security where category_id = " & sProjectfolderID, Me.ConnectionStringIDAM)
                    End If

                    Dim srow As DataRow
                    ExecuteTransaction("DELETE FROM ipm_ASSET_security WHERE ASSET_ID = " & AllFiles.Rows(i)("id"))
                    For Each srow In securityCatTable.Rows
                        ExecuteTransaction("insert into ipm_ASSET_security (ASSET_ID, security_id, type) values (" & AllFiles.Rows(i)("id") & ",'" & srow("security_id") & "','" & srow("type") & "')")
                    Next



                    'set processed flag
                    Me.ExecuteTransaction("update filelisting set processed = 1 where id = " & AllFiles.Rows(i)("id"))
                End If

            Catch ex As Exception
                Me.ExecuteTransaction("update filelisting set processed = 3 where id = " & AllFiles.Rows(i)("id"))
                Me.SendError(ex.Message, "MAIN CATCH", "UNKNOWN")
            End Try
        Next i
        Dim x As String = "what the"

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        ExecuteTransaction("update ipm_asset_category set name = 'Marketing' where name = 'Marketing Materials'")
        ExecuteTransaction("update ipm_asset_category set name = 'Studio' where name = 'Studio Assets'")
        'get all projects
        Dim AllProjects As DataTable = GetDataTable("select projectid from ipm_project where available = 'Y'", ConnectionStringIDAM)



        Dim i As Integer
        ProgressBar1.Maximum = AllProjects.Rows.Count
        ProgressBar1.Value = 0
        i = 0
        For Each row As DataRow In AllProjects.Rows

            'get marketing id
            Dim MarketingFolderID As String = ""

            Dim CommunicationFolderID As String = ""
            Dim MarketingConfidentialFolderID As String = ""
            Dim CommunicationConfidentialFolderID As String = ""
            Dim Images As String = ""
            Dim ImagesSupplementalFolderID As String = ""
            Dim MiscFolderID As String = ""
            Me.ProgressBar1.Value = i
            i = i + 1
            Try
                MarketingFolderID = GetDataTable("select category_id from ipm_asset_category where projectid = " & row("projectid") & "and name = 'Marketing'", ConnectionStringIDAM).Rows(0)("category_id")
                MarketingConfidentialFolderID = GetDataTable("select category_id from ipm_asset_category where projectid = " & row("projectid") & " and parent_cat_id = " & MarketingFolderID & " and name = 'Confidential'", ConnectionStringIDAM).Rows(0)("category_id")
                CommunicationFolderID = GetDataTable("select category_id from ipm_asset_category where projectid = " & row("projectid") & "and name = 'Communications'", ConnectionStringIDAM).Rows(0)("category_id")
                CommunicationConfidentialFolderID = GetDataTable("select category_id from ipm_asset_category where projectid = " & row("projectid") & " and parent_cat_id = " & CommunicationFolderID & " and name = 'Confidential'", ConnectionStringIDAM).Rows(0)("category_id")
                Images = GetDataTable("select category_id from ipm_asset_category where projectid = " & row("projectid") & " and parent_cat_id = 0 and name = 'Images'", ConnectionStringIDAM).Rows(0)("category_id")
                ImagesSupplementalFolderID = GetDataTable("select category_id from ipm_asset_category where projectid = " & row("projectid") & " and parent_cat_id = " & Images & " and name = 'Supplemental Imagery'", ConnectionStringIDAM).Rows(0)("category_id")
                MiscFolderID = GetDataTable("select category_id from ipm_asset_category where projectid = " & row("projectid") & " and parent_cat_id = 0 and name = 'Misc'", ConnectionStringIDAM).Rows(0)("category_id")

                'move assets from communication to marketing
                ExecuteTransaction("update ipm_asset set category_id = " & MarketingFolderID & " where category_id = " & MarketingFolderID)
                'move assets from communication/confidential to marketing / confidential
                ExecuteTransaction("update ipm_asset set category_id = " & MarketingConfidentialFolderID & " where category_id = " & CommunicationConfidentialFolderID)

                'delete communication
                ExecuteTransaction("update ipm_asset_category set available = 'N' where category_id = " & CommunicationFolderID)
                ExecuteTransaction("update ipm_asset_category set available = 'N' where category_id = " & CommunicationConfidentialFolderID)

                'move all assets in studio to studio

                'move assets in images/sup[plemental imagery to parent cat
                ExecuteTransaction("update ipm_asset set category_id = " & Images & " where category_id = " & ImagesSupplementalFolderID)
                'delete ImagesSupplementalFolderID
                ExecuteTransaction("update ipm_asset_category set available = 'N' where category_id = " & ImagesSupplementalFolderID)

                'rename images to photography
                ExecuteTransaction("update ipm_asset_category set name = 'Photography' where category_id = " & Images)

                'change parent of images to 'marketing' root cat
                ExecuteTransaction("update ipm_asset_category set parent_cat_id = " & MarketingFolderID & " where category_id = " & Images)

                'move parentid of misc to marketing id
                ExecuteTransaction("update ipm_asset_category set parent_cat_id = " & MarketingFolderID & " where category_id = " & MiscFolderID)


            Catch ex As Exception

            End Try

            Me.Refresh()
        Next



    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click

        Dim PhotoAdministratorID, MarketingManagerID, MarketingCoordinatorID, MarketStudio28ID, GeneralStaffID, CommunicationTeamID As String
        'migrate groups
        PhotoAdministratorID = GetDataTable("select groupid from ipm_group where name = 'Photo Administrator'", ConnectionStringIDAM).Rows(0)("groupid")
        MarketingManagerID = GetDataTable("select groupid from ipm_group where name = 'Marketing Manager'", ConnectionStringIDAM).Rows(0)("groupid")
        MarketingCoordinatorID = GetDataTable("select groupid from ipm_group where name = 'Marketing Coordinator'", ConnectionStringIDAM).Rows(0)("groupid")
        MarketStudio28ID = GetDataTable("select groupid from ipm_group where name = 'Market Studio 28'", ConnectionStringIDAM).Rows(0)("groupid")
        GeneralStaffID = GetDataTable("select groupid from ipm_group where name = 'General Staff'", ConnectionStringIDAM).Rows(0)("groupid")
        CommunicationTeamID = GetDataTable("select groupid from ipm_group where name = 'Communications Team'", ConnectionStringIDAM).Rows(0)("groupid")

        ExecuteTransaction("update ipm_group_user set groupid = " & PhotoAdministratorID & " where groupid = " & MarketingManagerID)
        ExecuteTransaction("update ipm_group_user set groupid = " & PhotoAdministratorID & " where groupid = " & MarketingCoordinatorID)
        ExecuteTransaction("update ipm_group_user set groupid = " & PhotoAdministratorID & " where groupid = " & MarketStudio28ID)
        ExecuteTransaction("delete from ipm_group_user where groupid = " & GeneralStaffID)
        ExecuteTransaction("update ipm_group_user set groupid = " & PhotoAdministratorID & " where groupid = " & CommunicationTeamID)

        ExecuteTransaction("update ipm_group set name = 'Marketing' where groupid = " & PhotoAdministratorID)

        'remove 
        ExecuteTransaction("update ipm_group set active = 'N' where groupid = " & MarketingManagerID)
        ExecuteTransaction("update ipm_group set active = 'N' where groupid = " & MarketingCoordinatorID)
        ExecuteTransaction("update ipm_group set active = 'N' where groupid = " & MarketStudio28ID)
        ExecuteTransaction("update ipm_group set active = 'N' where groupid = " & CommunicationTeamID)
        ExecuteTransaction("update ipm_group set active = 'N' where groupid = " & GeneralStaffID)


        ExecuteTransaction("SELECT groupid, userid    INTO holdkey   FROM ipm_group_user GROUP BY groupid, userid")
        ExecuteTransaction("INSERT ipm_group_user SELECT groupid, userid FROM holdkey")
        





    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        'clear all permissions

    End Sub

    Private Sub Form1_BackColorChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.BackColorChanged

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
