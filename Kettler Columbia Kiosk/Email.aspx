﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Email.aspx.vb" Inherits="Email" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

<script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings("jsFolder")%>jquery-1.2.3.pack.js"></script>
<script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings("jsFolder")%>jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('form').validate();
    });
</script>
        <link rel="stylesheet" type="text/css"  media="screen and (min-width: 1131px)"  href="css/kettler.css" />
        <link rel="stylesheet" type="text/css"  media="screen and (max-width: 1130px)"  href="css/tablet_wide.css" />

          <link rel="stylesheet" type="text/css" href="skins/tango/skin.css" />
        <!--main gallery carousel-->
        <link rel="stylesheet" type="text/css" href="skins/ie7/skin.css" />
        <!--resident services carousel -->
        <link rel="stylesheet" type="text/css" href="skins/tango2/skin.css" />
        <!--live here carousel-->
        <link rel="stylesheet" type="text/css" href="skins/floorplate/skin.css"
        />
        <link rel="stylesheet" type="text/css" href="skins/team/skin.css"
        />
        <!--live here carousel-->
        <link rel="stylesheet" type="text/css" href="css/map.css"
        />
        <link rel="stylesheet" type="text/css" href="css/jqModal_tb.css"
        />
         <link rel="stylesheet" type="text/css" href="css/thickbox.css"
        />

    <title>E-mail</title>
  

    <style type="text/css">
        body 
        {
            background: black;
        }
body, td {
    font-family: Arial;
    font-size: 12px;
    color: white;
}
.error 
{
    font-size: 14px;
    color: Red;   
}
.success 
{
    font-size: 14px;
    font-weight:bold;   
}
</style>
</head>
<body >
    <form id="formEmail" runat="server">
    <div>
    <table width="100%" cellpadding="5" cellspacing="5">
        <tr>
        <td colspan="2">
        <asp:Label ID="labelError" runat="server" CssClass="error"></asp:Label>
        <asp:Label ID="LabelSuccess" runat="server" CssClass="success"></asp:Label>
        </td>
        </tr>
        <tr>
            <td valign="top"><b>Your Name:</b></td>
            <td><input type="text" id="sender_name" name="sender_name" size="39" class="required" /></td>
        </tr>
        <tr>
            <td valign="top"><b>To:</b></td>
            <td><input type="text" name="recipient_email" size="39" class="required email" /></td>
        </tr>
        <tr>
            <td valign="top"><b>Comment:</b></td>
            <td><textarea rows="19" cols="30" name="message"></textarea></td>
        </tr>
        <tr>
            <td><b></b></td>
            <td>
            <input type="hidden" name="submit" value="1" />
            <input type="hidden" name="pagetype" value="<%=request.querystring("pagetype") %>" />
            <input type="hidden" name="id" value="<%=request.querystring("id") %>" />
            <!--<input type="submit" value="Submit Comments" />-->

            <div><asp:Button ID = "btnSubmit" CssClass="unit_floorplan_details_button" runat="server" OnClick="btnSubmit_Click" Text="SEND" /></div> &nbsp; <asp:Button runat="server" ID = "btnClose" cssClass="unit_floorplan_details_button" onClientClick="self.parent.tb_remove();" Text="Close" />   
            
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
              

               
            </td>
        </tr>
                                      
    </table>
    </div>
    </form>
</body>
</html>
