﻿package com.boontaran {
	import flash.text.*;	
	import flash.geom.ColorTransform;
	
	public class Utility {
		//applying movie color and alpha value
		public static function setColor(obj, color, alp=null) {
			if(color == undefined) return;
			
			var clr:ColorTransform = new ColorTransform();
			clr.color = uint(color);
			
			//check the type of object
			if(obj is TextField) {
				obj.textColor = uint(color);
				
			} else {
				obj.transform.colorTransform = clr;
				if(alp!=undefined) obj.alpha = alp;
			}
			
		}
		public static function underScore(str:String) {
			var myPattern:RegExp = / /g;  
 			
 			str=escape(str.replace(myPattern, "_"));  
   			return str;
		}
		public static function getExtension(filename:String) {
			
			var extensionIndex:Number = filename.lastIndexOf( '.' );
        	var extension:String = filename.substr( extensionIndex + 1, filename.length );
			
			return extension;
		}
	}
}