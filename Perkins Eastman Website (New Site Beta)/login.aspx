﻿<%@ page language="VB" autoeventwireup="false" CodeFile="login.aspx.vb" inherits="login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Perkins Eastman: Client Log In</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
</head>
<body class="pe2">
      <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    <form id="form1" runat="server">
    
    
   
<script type="text/javascript">
	jQuery(".main").css("background-color", "#FFF");
</script>
<!--<cfoutput>-->
 <div class="breadcrumbs"><a href="/" class="active">Home</a> | Log in </div>
<div class="head_standard">Client Log-In</div>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td align="left" valign="top">
        <div class="copy_standard" style="width: 600px; padding-right: 30px;"><a href="ftp://ftp.perkinseastman.com/" target="_blank">FTP</a><br /><br />Need help? Contact our help <a href="mailto:itsupport@perkinseastman.com">desk</a>.</div>
        
</td>

</tr>
</table>
  <div class="head_standard">
        Staff log-in</div>
 
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="left" valign="top">
                <div class="copy_standard" style="width: 600px; padding-right: 30px;">
                    <a href="http://orchard.perkinseastman.com/Pages/Default.aspx" target="_blank">ORCHARD</a><br />
                    <br />
					<a href="http://outlook.perkinseastman.com" target="_blank">Outlook</a> using remote access<br />
                    <br />
                    <a href="http://portfolio.perkinseastman.com" target="_blank">Portfolio</a> compatible with Internet Explorer<br />
                    <br />
                    <a href="http://vision.perkinseastman.com/visionclient/" target="_blank">Vision</a>
                    on a Perkins Eastman computer<br />
                    <br />
                    <a href="http://orchard.perkinseastman.com/it/Pages/AccessingVisionRemotely.aspx" target="_blank">Vision</a> on a non-Perkins
                    Eastman computer. For detailed instructions review the ORCHARD tutorial.<br />
                    <br />
                    <a href="ftp://ftp.perkinseastman.com/" target="_blank">FTP</a><br />
                    <br />
                    Need help? Contact our <a href="mailto:itsupport@perkinseastman.com">help desk</a>.</div>
            </td>
        </tr>
    </table>
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>