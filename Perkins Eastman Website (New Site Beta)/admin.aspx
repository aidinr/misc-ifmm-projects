﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="admin.aspx.vb" Inherits="admin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin Login</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
</head>
<body class="pe2">
   <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
     <form id="form1" runat="server">
  <div class="head_standard">Admin Login</div>
   
   <table>
        <tr>
            <td><div class="copy_standard red bold">Login: </div></td>
            <td><asp:TextBox Width="200" ID="textboxUser" TextMode="SingleLine" runat="server"></asp:TextBox> </td>
        </tr>
        <tr>
            <td><div class="copy_standard red bold">Password: </div></td>
            <td><asp:TextBox Width="200" ID="textboxPassword" TextMode="Password" runat="server"></asp:TextBox> </td>
        </tr>
        <tr>
            <td></td>
            <td><asp:Button runat="server" ID="buttonSubmit" Text="Click Here to Login" /></td>
        </tr>
   </table>
     <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html> 
