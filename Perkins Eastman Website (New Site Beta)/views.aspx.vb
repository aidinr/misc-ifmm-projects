﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient

Partial Class views
    Inherits System.Web.UI.Page

    'Disable viewstate output
    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)

    End Sub
    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Nothing
    End Function
    Protected Overrides Function SaveViewState() As Object
        Return Nothing
    End Function
    'end Disable viewstate output

    Private Sub MyPage_Init_DisableViewState(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Init
        Me.EnableViewState = False
    End Sub
    Protected Sub views_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)

        GeneratePOV()
        GenerateCurrentPress()

    End Sub

    Sub GeneratePOV()

        Dim sql = "Select n.News_Id, n.Headline, n.Content, n.Picture, n.PDF, COALESCE( 'by ' + nullif(ltrim(v.Item_Value),'') + ' | ' ,'') + DATENAME(mm,n.Post_Date) + ' ' + DATENAME(day,n.Post_Date) + ', ' + DATENAME(year, n.Post_Date)  DateLoc , v.Item_Value Employee, v2.Item_Value Title, v3.Item_Value video From IPM_NEWS n Left Join IPM_NEWS_FIELD_VALUE v ON (n.News_id = v.News_id AND v.Item_ID = 3409254) Left Join  IPM_NEWS_FIELD_VALUE v2 on ( n.News_Id = v2.NEWS_ID and v2.Item_ID = 3409255 ) Left Join IPM_NEWS_FIELD_VALUE v3 on (n.News_Id = v3.NEWS_ID and v3.Item_ID = 30052966) Where(n.Show = 1 And n.Post_Date < GETDATE() And n.Pull_Date > GETDATE() And n.Type = 10) Order By n.Post_Date DESC"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        POV.DataSource = dt1
        POV.DataBind()

    End Sub

    Sub GenerateCurrentPress()

        Dim sql = "Select e.Headline,e.Content, DATENAME(mm,e.Event_Date) + ' ' + DATENAME(day,e.Event_Date) + ', ' + DATENAME(year, e.Event_Date) Event_Date, l.Item_Value Location, p.Item_Value Presenter, v.Item_Value Venue From IPM_EVENTS e Left Join IPM_EVENTS_FIELD_VALUE l on (l.EVENT_ID = e.Event_Id and l.Item_ID = 3408650) Left Join IPM_EVENTS_FIELD_VALUE p on (p.EVENT_ID = e.Event_Id and p.Item_ID = 3408649) Left Join IPM_EVENTS_FIELD_VALUE v on (v.EVENT_ID = e.Event_Id and v.Item_ID = 3408651) Where(e.Type = 5 And e.Post_Date < GETDATE() And e.Pull_Date > GETDATE() And e.Show = 1) Order By e.Event_Date ASC"


        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        Speaking.DataSource = dt1
        Speaking.DataBind()

    End Sub
    Public Sub POV_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            If e.Item.ItemIndex = 0 Then
                Dim i As System.Web.UI.HtmlControls.HtmlContainerControl = e.Item.FindControl("PressReleaseText")
                i.Attributes.Add("class", "info first")
                Dim i2 As System.Web.UI.HtmlControls.HtmlControl = e.Item.FindControl("tophr")
                i2.Attributes.Add("style", "display:none;")
            Else
                'Dim i As System.Web.UI.HtmlControls.HtmlAnchor = e.Item.FindControl("PressReleaseTitle")
                'i.Attributes.Add("style", "display:none;")
            End If
            Dim j As System.Web.UI.HtmlControls.HtmlAnchor = e.Item.FindControl("POVLink")
            If e.Item.DataItem("PDF") = 1 Then
                j.HRef = Session("WSRetrieveAsset").ToString() + "type=newspdf&amp;size=0&amp;cache=1&amp;id=" + e.Item.DataItem("News_ID").ToString()
            Else
                j.HRef = ""
                j.InnerText = ""
            End If
            Dim k As System.Web.UI.HtmlControls.HtmlAnchor = e.Item.FindControl("POVVideoLink")
            If IsDBNull(e.Item.DataItem("video")) Or e.Item.DataItem("video") = "" Then
                k.InnerText = ""
                k.HRef = ""
            Else
                k.HRef = e.Item.DataItem("video")
            End If
        End If

    End Sub

    Public Sub CP_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            If e.Item.DataItem("Venue") = "" Then
                Dim br2 As System.Web.UI.HtmlControls.HtmlControl = e.Item.FindControl("Venuebr")
                br2.Style.Add("display", "none")
            End If
            If e.Item.DataItem("Location") = "" Then
                Dim span As System.Web.UI.HtmlControls.HtmlControl = e.Item.FindControl("bar")
                span.Style.Add("display", "none")
            End If

        End If

    End Sub

End Class
