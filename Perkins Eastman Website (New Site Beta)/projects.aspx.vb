﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class projects
    Inherits System.Web.UI.Page

    Private practiceName As String
    Private practiceAreaUDFID As String

    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)

        Dim i = Request.QueryString("noproject")

        If (i <> "") Then
            DisplayNoProject()
        Else
            DisplayProjects()
        End If

    End Sub

    Sub DisplayProjects()

        Dim theProjectID = Request.QueryString("list")

        Select Case theProjectID
            Case "2400012"
                practiceName = "Corporate Interiors"
                practiceAreaUDFID = "3408922"
            Case "3409338"
                practiceName = "Education"
                practiceAreaUDFID = "3408922"
            Case "3409328"
                practiceName = "Campus Planning"
                practiceAreaUDFID = "3408924"
            Case "2400014"
                practiceName = "Higher Education"
                practiceAreaUDFID = "3408924"
            Case "2400017"
                practiceName = "K-12 Education"
                practiceAreaUDFID = "3408924"
            Case "3409341"
                practiceName = "Town and Gown"
                practiceAreaUDFID = "3408924"
            Case "2400024"
                practiceName = "Healthcare"
                practiceAreaUDFID = "3408922"
            Case "2400015"
                practiceName = "Hospitality"
                practiceAreaUDFID = "3408922"
            Case "2400016"
                practiceName = "Housing"
                practiceAreaUDFID = "3408922"
            Case "2400018"
                practiceName = "Office and Retail"
                practiceAreaUDFID = "3408922"
            Case "2400019"
                practiceName = "Public and Cultural"
                practiceAreaUDFID = "3408922"
            Case "2400020"
                practiceName = "Science and Technology"
                practiceAreaUDFID = "3408922"
            Case "2400021"
                practiceName = "Senior Living"
                practiceAreaUDFID = "3408922"
            Case "3409343"
                practiceName = "Urbanism"
                practiceAreaUDFID = "3408922"
            Case "3409345"
                practiceName = "Downtown Redevelopment"
                practiceAreaUDFID = "3408924"
            Case "3409347"
                practiceName = "New Town Planning"
                practiceAreaUDFID = "3408924"
            Case "3409349"
                practiceName = "Transit and Development"
                practiceAreaUDFID = "3408924"
            Case "2400022"
                practiceName = "Urban Design"
                practiceAreaUDFID = "3408924"
            Case "3409351"
                practiceName = "Waterfront"
                practiceAreaUDFID = "3408924"
            Case Else
                DisplayNoProject()
                Exit Sub
        End Select
        practiceName = "%" & practiceName & "%"

        Dim sql As String = "select *, b.item_value description_long, c.item_value client_list from IPM_PROJECT a left join ipm_project_field_value b on a.projectid = b.projectid and b.item_id = @catDescriptionUDFID left join ipm_project_field_value c on a.projectid = c.projectid and c.item_id = @clientListUDFID where a.projectid = @project_id and a.AVAILABLE = 'y'"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim catIDParameter As New SqlParameter("@catDescriptionUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(catIDParameter)
        MyCommand1.SelectCommand.Parameters("@catDescriptionUDFID").Value = ConfigurationSettings.AppSettings("DescriptionLongUDFID")

        Dim projectIDParameter As New SqlParameter("@project_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(projectIDParameter)
        MyCommand1.SelectCommand.Parameters("@project_id").Value = theProjectID

        Dim clientListIDParameter As New SqlParameter("@clientListUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(clientListIDParameter)
        MyCommand1.SelectCommand.Parameters("@clientListUDFID").Value = ConfigurationSettings.AppSettings("ClientListUDFID")

        Dim dt1 As DataTable = New DataTable
        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then
            If (Not IsDBNull(dt1.Rows(0)("description_long"))) Then
                literalDescription.Text = dt1.Rows(0)("description_long")
                literalDescription2.Text = dt1.Rows(0)("description_long")
            End If
            If (Not IsDBNull(dt1.Rows(0)("client_list"))) Then
                literalBrowseClient.Text = "<p id=""ClientList"">Browse our client list</p>"
                literalClientList.Text = dt1.Rows(0)("client_list").ToString.Replace(vbCrLf, "").Replace("'", "\'")
            End If
            literalName.Text = dt1.Rows(0)("name")
            literalName2.Text = dt1.Rows(0)("name")
            literalPageTitle.Text = "Perkins Eastman: " & dt1.Rows(0)("name")
        End If

        MyCommand1.Fill(dt1)

        'Dim sql2 As String = "select * from ipm_project_related b, ipm_project a left join ipm_project_field_value c on c.projectid = a.projectid and c.item_id = @shortNameUDFID where b.project_id = @project_id and a.projectid = b.ref_id and available = 'Y'"
        'Select a.ProjectID, COALESCE(c.Item_Value, a.Name) Item_Value, ISNULL(d.item_value, '0') disabled from IPM_PROJECT a join IPM_PROJECT_FIELD_VALUE b on a.ProjectID = b.ProjectID and b.Item_ID = 3408924  left join IPM_PROJECT_FIELD_VALUE c on a.ProjectID = c.ProjectID and c.Item_ID = 2400053 left join IPM_PROJECT_FIELD_VALUE d on a.ProjectID = d.ProjectID and d.Item_ID = 3409575 Where b.Item_Value Like '%Waterfront%' and a.Show = 1 and a.Available = 'Y'
        Dim sql2 = "Select a.ProjectID, COALESCE(c.Item_Value, a.Name) Item_Value, ISNULL(d.item_value, '0') disabled  from IPM_PROJECT a join IPM_PROJECT_FIELD_VALUE b on a.ProjectID = b.ProjectID and b.Item_ID = @practiceAreaUDFID left join IPM_PROJECT_FIELD_VALUE c on a.ProjectID = c.ProjectID and c.Item_ID = 2400053  left join IPM_PROJECT_FIELD_VALUE d on a.ProjectID = d.ProjectID and d.Item_ID = 3409575 left join IPM_PROJECT_FIELD_VALUE e on a.ProjectID = e.ProjectID and e.Item_ID = 3409768 Where b.Item_Value Like @practiceName  and a.Show = 1 and a.Available = 'Y' Order By isnull(e.Item_Value, 9999999999) asc"
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)

        Dim practiceAreaUDFIDParameter As New SqlParameter("@practiceAreaUDFID", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(practiceAreaUDFIDParameter)
        MyCommand2.SelectCommand.Parameters("@practiceAreaUDFID").Value = practiceAreaUDFID

        Dim practiceNameParameter As New SqlParameter("@practiceName", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(practiceNameParameter)
        MyCommand2.SelectCommand.Parameters("@practiceName").Value = practiceName

        Dim dt2 As DataTable = New DataTable

        MyCommand2.Fill(dt2)

        repeaterProjects.DataSource = dt2
        repeaterProjects.DataBind()

        Dim sql3 As String = "select * from ipm_asset a, ipm_project b where a.available = 'y' and b.available = 'y' and a.projectid = b.projectid and b.projectid = @project_id and a.media_type = 10"
        Dim MyCommand3 As SqlDataAdapter = New SqlDataAdapter(sql3, MyConnection)

        Dim projectIDParameter3 As New SqlParameter("@project_id", SqlDbType.VarChar, 255)
        MyCommand3.SelectCommand.Parameters.Add(projectIDParameter3)
        MyCommand3.SelectCommand.Parameters("@project_id").Value = theProjectID

        Dim dt3 As DataTable = New DataTable

        MyCommand3.Fill(dt3)


        If (dt3.Rows.Count > 0) Then

            featuredImage.ImageUrl = Session("WSRetrieveAsset") & "type=asset&size=1&cache=1&width=600&height=480&id=" & dt3.Rows(0)("asset_id")


        End If

    End Sub
    Public Sub RepeaterProjects_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim thehREF As HtmlAnchor = e.Item.FindControl("projectDetailsLink")

            If Not (IsDBNull(e.Item.DataItem("disabled"))) Then
                If (e.Item.DataItem("disabled").ToString().Trim() = "1") Then
                    thehREF.HRef = ""
                Else
                    thehREF.HRef = "projectDetails.aspx?p=" + e.Item.DataItem("projectid").ToString().Trim() + "&c=" + Request.QueryString("list") + "&pn=" + practiceName.replace("%", "") + "&pa=" + practiceAreaUDFID
                End If
            Else
                thehREF.HRef = "projectDetails.aspx?p=" + e.Item.DataItem("<projectid").ToString().Trim() + "&c=" + Request.QueryString("list") + "&pn=" + practiceName.replace("%", "") + "&pa=" + practiceAreaUDFID
            End If


        End If
    End Sub

    Private Sub DisplayNoProject()

        literalName.Text = "No Matching Projects Found"
        literalName2.Text = "No Matching Project Found"
        featuredImage.Visible = False
        repeaterProjects.Visible = False
        viewProjectText.Visible = False

    End Sub
End Class