﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient

Partial Class news
    Inherits System.Web.UI.Page

    Protected Sub news_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)

        GeneratePressReleases()
        GenerateCurrentPress()

    End Sub

    Sub GeneratePressReleases()

        Dim sql = "Select n.News_Id, n.Headline, n.Content, n.Picture, n.PDF, DATENAME(mm,n.Post_Date) + ' ' + DATENAME(day,n.Post_Date) + ', ' + DATENAME(year, n.Post_Date) + COALESCE(' | ' + nullif(ltrim(v.Item_Value),''), '') DateLoc From IPM_NEWS n Left Join IPM_NEWS_FIELD_VALUE v ON (n.News_id = v.News_id AND v.Item_ID = 3409256) Where(n.Show = 1 And n.Post_Date < GETDATE() And n.Pull_Date > GETDATE() And n.Type = 8) Order By n.Post_Date DESC"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        PressReleses.DataSource = dt1
        PressReleses.DataBind()

    End Sub

    Sub GenerateCurrentPress()

        Dim sql = "Select n.News_ID, n.Headline, n.Content, v.Item_Value url, DATENAME(mm,n.Post_Date) + ' ' + DATENAME(day,n.Post_Date) + ', ' + DATENAME(year, n.Post_Date) PostDate From IPM_NEWS n left join IPM_NEWS_FIELD_VALUE v on n.News_Id = v.NEWS_ID and v.Item_ID = 3407671 Where(n.Show = 1 And n.Post_Date < GETDATE() And n.Pull_Date > GETDATE() And n.Type = 0) Order By n.Post_Date DESC"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        CurrentPress.DataSource = dt1
        CurrentPress.DataBind()

    End Sub
    Public Sub PressReleses_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            If e.Item.ItemIndex = 0 Then
                Dim i As System.Web.UI.HtmlControls.HtmlContainerControl = e.Item.FindControl("PressReleaseText")
                i.Attributes.Add("class", "info item blue_bg first")
            Else
                Dim i As System.Web.UI.HtmlControls.HtmlAnchor = e.Item.FindControl("PressReleaseTitle")
                i.Attributes.Add("style", "display:none;")
            End If
            Dim more As System.Web.UI.HtmlControls.HtmlAnchor = e.Item.FindControl("More")
            If e.Item.DataItem("PDF") = 1 Then
                more.HRef = Session("WSRetrieveAsset").ToString() + "type=newspdf&amp;size=0&amp;cache=1&amp;id=" + e.Item.DataItem("News_ID").ToString()
            Else
                more.InnerText = ""
                more.HRef = ""
            End If
        End If

    End Sub

    Public Sub CurrentPress_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim pdflink As System.Web.UI.HtmlControls.HtmlAnchor = e.Item.FindControl("urllink")
            If e.Item.DataItem("url").ToString.Trim.Length > 0 Then
                pdflink.HRef = e.Item.DataItem("url").ToString().Trim()
            Else
                pdflink.HRef = ""
            End If
        End If

    End Sub
End Class
