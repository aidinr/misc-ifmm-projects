﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class eek
    Inherits System.Web.UI.Page
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Clear()
        Response.Status = "301 Moved Permanently"
        Response.AddHeader("Location", "eekArchitects.aspx")


        'Dim headerControl As Web.UI.Control
        'headerControl = Me.LoadControl("includes/header.ascx")
        'pageHeader.Controls.Add(headerControl)

        'Dim bodyTopControl As Web.UI.Control
        'bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        'bodyTop.Controls.Add(bodyTopControl)

        'Dim bodyFooterControl As Web.UI.Control
        'bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        'bodyBottom.Controls.Add(bodyFooterControl)

        'DisplayPage()


    End Sub

    Sub DisplayPage()

        Dim sql As String = "Select * from IPM_PROJECT_FIELD_VALUE where ProjectID = 3409169 and Item_ID in (3409606,3409607,3409781)"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim dt1 As DataTable = New DataTable
        MyCommand1.Fill(dt1)

        For Each row As DataRow In dt1.Rows
            Select row("Item_ID").ToString().Trim()
                Case "3409606"
                    breadcrumbs.InnerHtml = "<a href=""/"" class=""active"">Home</a> | <a href=""/#linkPage1"" class=""active"">About us</a> | " + row("Item_Value").ToString()
                    title.InnerHtml = row("Item_Value").ToString()
                Case "3409607"
                    eekText.Text = row("Item_Value").ToString()
                    literalDescription.Text = row("Item_Value").ToString()
                Case "3409781"
                    If (Not IsDBNull(row("Item_Value"))) Then
                        If row("Item_Value").ToString().Trim() <> "" Then
                            literalBrowseClient.Text = "<p id=""ClientList"">Browse selected projects</p>"
                            literalClientList.Text = row("Item_Value").ToString.Replace(vbCrLf, "").Replace("'", "\'")
                        End If
                    End If
            End Select
        Next row

        Dim sql2 As String = "Select Asset_ID,* from IPM_ASSET where ProjectID = 3409169 and Available= 'Y' and Name = 'Website_Landing Page_Battery_Park.jpg'"
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)

        Dim dt2 As DataTable = New DataTable

        MyCommand2.Fill(dt2)
        If (dt2.Rows.Count > 0) Then
            If (Not IsDBNull(dt2.Rows(0)("Asset_ID"))) Then
                eekPic.Src = Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=480&amp;width=600&amp;id=" + dt2.Rows(0)("Asset_ID").ToString()
            End If
        End If

    End Sub
 
End Class