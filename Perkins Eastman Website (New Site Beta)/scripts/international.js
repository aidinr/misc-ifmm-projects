
jQuery('.project_details').css('position', 'relative');

if (jQuery('p#ClientList').html()) {
    var map = '\
<p id="mapBox" style="border: 1px solid #888; background: #fff; display: none; position: absolute; left: 0; top: 0; width: 598px; height: 295px; overflow: hidden; padding: 50px 0 30px 0; margin: 0;">\
<a id="mapClose" style="position: absolute; right: 13px; top: 10px; font-size: 13px; font-family: arial, helvetica, sans-serif;cursor: pointer; color: #EF2D21; font-weight: 600; text-decoration: none;">[X]  <u>close</u></a>\
<img id="mapImage"src="images/map/clear.gif" alt=""usemap="#worldmap" style="width: 598px; height: 295px; overflow: hidden; border: 0; background: #fff url(images/map/world.png) no-repeat scroll 0 0;">\
</p>\
<map id="worldmap" name="worldmap">\
  <area shape="poly" coords="62,49,93,15,167,0,283,1,277,7,263,30,236,40,226,53,202,44,197,57,209,65,214,80,212,83,198,78,188,89,183,82,185,78,185,73,178,81,160,88,160,80,152,74,146,75,142,73,92,73,86,72,71,62,83,61,73,51,69,52" href="projects_thumbs.aspx?list=2400103&name=The Americas&c_id=4,9,21,25,16" alt="The Americas" title="The Americas" onmouseover="showmap(\'theamericas\')" onmouseout="showmap()" />\
  <area shape="poly" coords="87,75,74,64,81,62,81,60,80,58,74,53,71,51,68,53,65,50,63,50,63,30,110,2,270,2,284,3,263,31,242,39,227,53,210,66,216,81,210,83,192,88,185,88,187,82,184,76,179,76,180,81,160,90,160,81,152,74,152,74" href="projects_thumbs.aspx?list=2400103&name=The Americas&c_id=4,9,21,25,16" alt="The Americas" title="The Americas" onmouseover="showmap(\'theamericas\')" onmouseout="showmap()" />\
  <area shape="poly" coords="179,294,162,277,176,216,157,189,159,164,98,128,90,101,121,109,140,123,164,126,166,123,166,116,177,123,198,138,207,150,211,160,229,170,246,188,247,200,240,222,236,231,213,262,195,276,211,288,211,288" href="projects_thumbs.aspx?list=2400103&name=The Americas&c_id=4,9,21,25,16" alt="The Americas" title="The Americas" onmouseover="showmap(\'theamericas\')" onmouseout="showmap()" />\
  <area shape="poly" coords="545,275,473,266,488,204,456,185,414,159,403,121,411,120,407,114,408,114,421,100,424,87,451,68,465,66,500,63,516,76,524,77,518,87,518,93,535,80,540,81,539,102,512,147,589,212,598,280" href="projects_thumbs.aspx?list=2400103&name=Asia&c_id=1,10,13,18,7" alt="Asia" title="Asia" onmouseover="showmap(\'asia\')" onmouseout="showmap()" />\
  <area shape="poly" coords="416,94,394,72,389,74,394,81,392,88,389,87,378,81,374,79,378,89,369,87,365,83,364,94,354,98,349,105,334,105,315,99,308,93,298,95,288,98,275,96,275,83,284,69,274,53,281,46,315,27,313,-1,598,2,598,55,575,74,544,87,538,82,529,79,522,88,514,83,521,77,504,64,497,70,486,70,486,72,474,75,464,66,460,71,450,69,444,77,432,92,432,92,422,92" href="projects_thumbs.aspx?list=2400103&name=Europe&c_id=11,15,22,24,19,26" alt="Europe" title="Europe" onmouseover="showmap(\'europe\')" onmouseout="showmap()" />\
  <area shape="poly" coords="254,32,279,46" href="projects_thumbs.aspx?list=2400103&name=Europe&c_id=11,15,22,24,19,26" alt="Europe" title="Europe" onmouseover="showmap(\'europe\')" onmouseout="showmap()" />\
  <area shape="poly" coords="267,106,276,117" href="projects_thumbs.aspx?list=2400103&name=Europe&c_id=11,15,22,24,19,26" alt="Europe" title="Europe" onmouseover="showmap(\'europe\')" onmouseout="showmap()" />\
  <area shape="poly" coords="341,253,326,254,319,238,311,211,315,198,303,172,299,163,280,168,266,151,267,123,284,117,280,107,287,100,289,98,306,96,313,96,323,109,332,106,357,106,357,99,368,95,371,91,375,94,383,96,388,89,397,84,397,76,414,94,423,99,419,111,410,120,412,122,404,121,401,123,401,130,396,146,374,177,390,208,381,235,377,236" href="projects_thumbs.aspx?list=2400103&name=Africa and Middle East&c_id=8,3,5,6,12,17,20,23" alt="Africa / Middle East" title="Africa / Middle East" onmouseover="showmap(\'middle\')" onmouseout="showmap()" />\
</map>\
';



    jQuery('<p id="mapPlus" style="display: block;position: absolute; right: 20px; bottom: 78px; width: 238px; height: 123px; overflow: hidden; padding: 0; margin: 0; cursor: pointer;" \
><a class="mapPlus" title="Click to Select a Region" style="display: block; width: 238px; height: 123px; padding: 0; margin: 0; cursor: pointer; background: #fff url(images/map/world.png) no-repeat scroll 0 -1475px" \
></a></p>').insertBefore('p#ClientList');

    if (jQuery('img#featuredImage').attr('src')) { jQuery(map).insertBefore('img#featuredImage'); }
    else if (jQuery('.breadcrumbs').html()) {
        jQuery(map).insertBefore('.breadcrumbs');
        jQuery('#mapBox').css('top', '167px');
    }

    jQuery('a.mapPlus').hover(function() {
        jQuery('a.mapPlus').css('background-position', '0 -1598px');
    },
  function() {
      jQuery('a.mapPlus').css('background-position', '0 -1475px');
  }
);

    jQuery('a.mapPlus').click(function() {
        jQuery('img#featuredImage').css('visibility', 'hidden');
        jQuery('#mapBox').css('display', 'block');
    });

    jQuery('a#mapClose').click(function() {
        jQuery('#mapBox').css('display', 'none');
        jQuery('img#featuredImage').css('visibility', 'visible');
    });


}


if (jQuery('.leftColumn').html()) {

    jQuery('.regionProjects a').hover(function() {
        jQuery(this).css('background-color', '#B3B3B5');
        jQuery(this).css('color', '#fff');
    }, function() {
        jQuery(this).css('background-color', '#fff');
        jQuery(this).css('color', '#9E9EA0');
    });
}


function showmap(continent) {
    var cmap = document.getElementById('mapImage');
    if (continent == 'theamericas') cmap.style.backgroundPosition = '0 -295px';
    else if (continent == 'asia') cmap.style.backgroundPosition = '0 -590px';
    else if (continent == 'europe') cmap.style.backgroundPosition = '0 -885px';
    else if (continent == 'middle') cmap.style.backgroundPosition = '0 -1180px';
    else cmap.style.backgroundPosition = '0 0';
}
