﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="searchResults.aspx.vb" Inherits="searchResults" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Perkins Eastman: Search Results</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
</head>
<body class="pe2">
    <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    <form id="form1" runat="server">
    <div class="breadcrumbs"><a href="/" class="active" >Home</a> | SEARCH RESULTS</div>
<div class="head_standard"><asp:Literal ID="literalSearchResults" runat="server"></asp:Literal></div>
    <div class="copy_standard bold red"><asp:Literal ID="literalCountProjects" runat="server"></asp:Literal></div>
	
	<asp:repeater ID="repeaterProjects" runat="server" OnItemDataBound="repeaterProjects_ItemDataBound">
	    <ItemTemplate>
	            <table cellpadding="0" cellspacing="0" style="margin-left:15px;">
	            <tr>
	            <td valign="top"><img src="<%=Session("WSRetrieveAsset")%>crop=1&type=project&size=1&width=60&height=60&id=<%#container.dataitem("projectid")%>" alt="" style="margin-right:5px;margin-bottom:10px;" /></td>
	            <td valign="top">
	            
	            <div class="copy_standard no_bottom_margin"><a href="projectDetails.aspx?p=<%#container.dataitem("projectid") %>"><asp:Literal ID="literalShortName" runat="server"></asp:Literal><asp:Literal ID="literalLocation" runat="server"></asp:Literal></a></div>
	             <div class="copy_standard ital" style="margin-left: 30px; margin-right: 15px;"><asp:literal ID="literalDescription" runat="server"></asp:literal></div>
	            
	            </td>
	            
	             </tr>
	             </table>
	    </ItemTemplate>
	</asp:repeater>
		<div class="copy_standard bold red"><asp:Literal ID="literalCountPressReleases" runat="server"></asp:Literal></div>
	<asp:repeater ID="repeaterNews" runat="server" OnItemDataBound="repeaterNews_ItemDataBound">
	    <ItemTemplate>
	    <table cellpadding="0" cellspacing="0" style="margin-left:15px;">
	            <tr>
	            <td valign="top"><img src="<%=Session("WSRetrieveAsset")%>crop=0&type=news&size=1&width=60&height=60&id=<%#container.dataitem("news_id")%>" alt="" style="margin-right:5px;margin-bottom:10px;" /></td>
	            <td valign="top">
	            <div class="copy_standard no_bottom_margin"><asp:HyperLink ID="hyperlinkPage" runat="server"><asp:Literal ID="literalCategoryName" runat="server"></asp:Literal> </asp:HyperLink></a></div>
	             <div class="copy_standard ital" style="margin-left: 30px; margin-right: 15px;"><asp:literal ID="literalDescription" runat="server"></asp:literal></div>
	             </td>
	             </tr>
	             </table> 

	    </ItemTemplate>
	</asp:repeater>
	<div class="copy_standard bold red"><asp:Literal ID="literalCountCategories" runat="server"></asp:Literal></div>
	<asp:repeater ID="repeaterCategories" runat="server" OnItemDataBound="repeaterCategories_ItemDataBound">
	    <ItemTemplate>
	    <table cellpadding="0" cellspacing="0" style="margin-left:15px;">
	            <tr>
	            <td valign="top"><img src="<%=Session("WSRetrieveAsset")%>crop=1&type=project&size=1&width=60&height=60&id=<%#container.dataitem("projectid")%>" alt="" style="margin-right:5px;margin-bottom:10px;" /></td>
	            <td valign="top">
	            <div class="copy_standard no_bottom_margin"><asp:HyperLink ID="hyperlinkPage" runat="server"><asp:Literal ID="literalCategoryName" runat="server"></asp:Literal></asp:HyperLink></div>
	             <div class="copy_standard ital" style="margin-left: 30px; margin-right: 15px;"><asp:literal ID="literalDescription" runat="server"></asp:literal></div>
	             </td>
	             </tr>
	             </table>

	    </ItemTemplate>
	</asp:repeater>
	<asp:repeater ID="repeaterMisc" runat="server" OnItemDataBound="repeaterMisc_ItemDataBound">
	    <ItemTemplate>
	            <div class="copy_standard no_bottom_margin"><asp:HyperLink ID="hyperlinkPage" runat="server"><asp:Literal ID="literalCategoryName" runat="server"></asp:Literal></asp:HyperLink></div>
	             <div class="copy_standard ital" style="margin-left: 30px; margin-right: 15px;"><asp:literal ID="literalDescription" runat="server"></asp:literal></div>

	    </ItemTemplate>
	</asp:repeater>
		
		
			
            
    
            
	            
	            

 <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
