﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class region
    Inherits System.Web.UI.Page
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)

        DisplayProjects()


    End Sub

    Sub DisplayProjects()

        Dim regionName = Request.QueryString("name")

        Dim sql As String = "Select v.Item_Value from IPM_PROJECT p Join IPM_PROJECT_FIELD_VALUE v on p.ProjectID = v.ProjectID Join IPM_PROJECT_FIELD_DESC d on d.Item_ID = v.Item_ID and d.Item_Tag = @Region where p.Name = 'Public Web Site Content'"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim regionParameter1 As New SqlParameter("@Region", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(regionParameter1)
        MyCommand1.SelectCommand.Parameters("@Region").Value = "IDAM_REGION_" + regionName.Trim().Replace(" ", "_")

        Dim dt1 As DataTable = New DataTable
        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then
            If (Not IsDBNull(dt1.Rows(0)("Item_Value"))) Then
                project_description.InnerHtml = dt1.Rows(0)("Item_Value")
            End If
        End If

        Dim sql3 As String = "Select v.Item_Value from IPM_PROJECT p Join IPM_PROJECT_FIELD_VALUE v on p.ProjectID = v.ProjectID Join IPM_PROJECT_FIELD_DESC d on d.Item_ID = v.Item_ID and d.Item_Tag = @Region where p.Name = 'Public Web Site Content'"

        Dim MyCommand3 As SqlDataAdapter = New SqlDataAdapter(sql3, MyConnection)

        Dim regionParameter3 As New SqlParameter("@Region", SqlDbType.VarChar, 255)
        MyCommand3.SelectCommand.Parameters.Add(regionParameter3)
        MyCommand3.SelectCommand.Parameters("@Region").Value = "IDAM_REGION_CLIENTS_" + regionName.Trim().Replace(" ", "_")

        Dim dt3 As DataTable = New DataTable
        MyCommand3.Fill(dt3)

        If (dt3.Rows.Count > 0) Then
            If (Not IsDBNull(dt3.Rows(0)("Item_Value"))) Then
                'Response.Write(dt3.Rows(0)("Item_Value").ToString)
                ClientList.Text = dt3.Rows(0)("Item_Value")
                literalBrowseClient.Text = dt3.Rows(0)("Item_Value")
            End If
        End If

        literalPageTitle.Text = "Perkins Eastman: " & regionName
        Dim descriptionText As String = ""
        Select Case regionName
            Case "Asia"
                descriptionText = "<p class=""mapAsia""></p>"
            Case "China"
                descriptionText = "<p class=""mapChina""></p>"
            Case "Europe"
                descriptionText = "<p class=""mapEurope""></p>"
            Case "The Americas"
                descriptionText = "<p class=""mapAmerica""></p>"
            Case "Africa and the Middle East"
                descriptionText = "<p class=""mapAfrica""></p>"

        End Select
        literalDescription2.Text = descriptionText
        'select * from ipm_project_related b, ipm_project a left join ipm_project_field_value c on c.projectid = a.projectid and c.item_id = 2400053 where b.project_id = 2400103 and a.projectid = b.ref_id and available = 'Y' and b.ref_id in (select ProjectID from IPM_PROJECT_OFFICE where officeid in (1,4,7,8,11)) order by ordering asc

        Dim sql2 As String = "select a.ProjectID, COALESCE( b.Item_Value, a.name) name, d.Item_Value disable  from ipm_project a  join ipm_project_field_value c on c.projectid = a.projectid and c.item_id = 3408923 and c.item_value = @region left join IPM_PROJECT_FIELD_VALUE b on b.ProjectID = a.ProjectID and b.Item_ID = 2400053 left join IPM_PROJECT_FIELD_VALUE d on a.ProjectID = d.ProjectID and d.Item_ID = 3409575 Left Join IPM_PROJECT_FIELD_VALUE e on a.ProjectID = e.ProjectID and e.Item_ID = 3409769 where available = 'Y' Order By isnull(e.Item_Value, 9999999999) asc"
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)

        Dim regionParameter As New SqlParameter("@region", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(regionParameter)
        MyCommand2.SelectCommand.Parameters("@region").Value = regionName

        Dim shortNameIDParameter As New SqlParameter("@shortNameUDFID", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(shortNameIDParameter)
        MyCommand2.SelectCommand.Parameters("@shortNameUDFID").Value = 3408923 ' ConfigurationSettings.AppSettings("ShortNameUDFID")

        Dim dt2 As DataTable = New DataTable

        MyCommand2.Fill(dt2)

        repeaterProjects.DataSource = dt2
        repeaterProjects.DataBind()

        'Dim sql3 As String = "select * from ipm_asset a, ipm_project b where a.available = 'y' and b.available = 'y' and a.projectid = b.projectid and b.projectid = @project_id and a.media_type = 10"
        'Dim MyCommand3 As SqlDataAdapter = New SqlDataAdapter(sql3, MyConnection)

        'Dim projectIDParameter4 As New SqlParameter("@project_id", SqlDbType.VarChar, 255)
        'MyCommand3.SelectCommand.Parameters.Add(projectIDParameter4)
        'MyCommand3.SelectCommand.Parameters("@project_id").Value = theProjectID

        'Dim dt3 As DataTable = New DataTable

        'MyCommand3.Fill(dt3)


        'If (dt3.Rows.Count > 0) Then

        'featuredImage.ImageUrl = Session("WSRetrieveAsset") & "type=asset&size=1&width=600&height=480&id=" & dt3.Rows(0)("asset_id")


        'End If

    End Sub
    Public Sub RepeaterProjects_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim thehREF As HtmlAnchor = e.Item.FindControl("projectLink")

            If Not (IsDBNull(e.Item.DataItem("disable"))) Then
                If (e.Item.DataItem("disable").ToString().Trim() = "1") Then
                    thehREF.HRef = ""
                Else
                    thehREF.HRef = "projectDetails.aspx?p=" + e.Item.DataItem("projectid").ToString() + "&amp;c=;parent=REGIONS," + Request.QueryString("name").Replace(" ", "%20")
                End If
            Else
                thehREF.HRef = "projectDetails.aspx?p=" + e.Item.DataItem("projectid").ToString() + "&amp;c=&amp;parent=REGIONS," + Request.QueryString("name").Replace(" ", "%20")
            End If


        End If
    End Sub
End Class