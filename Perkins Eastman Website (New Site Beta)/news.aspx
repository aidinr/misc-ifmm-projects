﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="news.aspx.vb" Inherits="news" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
     <title>Perkins Eastman: News</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
    <meta name="keywords" content="Architectural Awards" />
</head>

<body class="pe3">

    <!--<form id="form1" runat="server"> -->
  <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    
    <div class="breadcrumbs"><a href="/" class="active">Home</a> | NEWS</div>
   <!--<cfoutput>-->

<div class="head_standard"><asp:Literal ID="ContentName" runat="server"></asp:Literal></div>


<div class="left"> <h1 class="headline"> PRESS RELEASES </h1> 

<div class="list_holder"> 

<asp:Repeater runat="server" ID="PressReleses" OnItemDataBound="PressReleses_ItemDataBound">
<ItemTemplate>
<div class="item blue_bg"> <a id="PressReleaseImageLink" runat="server"><img src="<%=Session("WSRetrieveAsset")%>type=news&amp;crop=1&amp;size=1&amp;height=84&amp;width=117&amp;id=<%#container.dataitem("News_Id")%>" alt="" height="84 px" width="117 px"/></a> 

<div id="PressReleaseText" runat="server" class="info"> <a id="PressReleaseTitle" runat="server" class="red_item"><%# DataBinder.Eval(Container.DataItem, "Headline")%> </a> <strong> <%# DataBinder.Eval(Container.DataItem, "DateLoc")%> </strong> 

<p><%# DataBinder.Eval(Container.DataItem, "Content")%> <a id="More" runat="server" class="red_item"> more [+]</a></p> 
</div> 
</div> 
</ItemTemplate>
</asp:Repeater>

</div> 
</div> 

<div class="right"> 

<div class="list_holder2"> <h1 class="headline"> CURRENT PRESS </h1> 
<ul class="less_margin"> 
<li> <strong> If you are writing about our firm, projects, or industry please <a href="mailto:s.yates@perkinseastman.com?subject=Media%20Inquiry" class="director">contact Director of Communications, Steven Yates. </a> </strong> </li> 
<asp:Repeater runat="server" ID="CurrentPress"  OnItemDataBound="CurrentPress_ItemDataBound">
<ItemTemplate>
<li> <a id="urllink" runat="server" class="bolded" href="">"<%# DataBinder.Eval(Container.DataItem, "Headline")%>" </a> <strong> <%# DataBinder.Eval(Container.DataItem, "Content")%> | <%# DataBinder.Eval(Container.DataItem, "PostDate")%> </strong> </li>
</ItemTemplate>
</asp:Repeater>
</ul>
</div> 
</div>
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
<!--    </form> --> 
</body>
</html>