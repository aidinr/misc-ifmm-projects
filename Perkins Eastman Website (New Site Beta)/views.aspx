﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="views.aspx.vb" Inherits="views" EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
     <title>Perkins Eastman: Views</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
    <meta name="keywords" content="Architectural Awards" />
</head>

<body class="pe3">

  <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
 <!--   <form id="form1" runat="server"> -->
    
    <div class="breadcrumbs"><a href="/" class="active" >Home</a> | <a href="/#linkPage4" class="active" >INSIGHTS</a> | Point of View</div>
   <!--<cfoutput>-->

<div class="head_standard"><asp:Literal ID="ContentName" runat="server"></asp:Literal></div>


<div class="left"> <h1 class="headline"> Point of View </h1> 

<div class="list_holder"> 

<asp:Repeater runat="server" ID="POV" OnItemDataBound="POV_ItemDataBound">
<ItemTemplate>
<hr id="tophr" runat="server" />
<div class="item blue_bg"> 
    <a id="PressReleaseImageLink" class="POVPerson" runat="server" style="float: left; text-decoration: none;">
        <img src="<%=Session("WSRetrieveAsset")%>type=news&amp;crop=1&amp;size=1&amp;height=84&amp;width=117&amp;id=<%#container.dataitem("News_Id")%>" alt="" height="84 px" width="117 px"/>
        <span class="povName" ><%# Container.DataItem("Employee")%></span>
        <span class="povNameTitle" ><%# Container.DataItem("Title")%></span>
    </a>
 

<div id="PressReleaseText" runat="server" class="info">
 <a id="PressReleaseTitle" runat="server" class="red_item">
    <%# DataBinder.Eval(Container.DataItem, "Headline").ToString.Replace("&","&amp;")%> 
 </a> <strong class="povDateline"> <%# DataBinder.Eval(Container.DataItem, "DateLoc")%> 
 </strong> 
<p><%# DataBinder.Eval(Container.DataItem, "Content").ToString.Replace("&", "&amp;")%></p>
<a id="POVLink" runat="server" class="red_item_link" href="#"> Download complete white paper here [+] </a>
<a id="POVVideoLink" runat="server" class="red_item_link vimeo" href="#">View Video [+]</a>  
</div> 
</div> 
</ItemTemplate>
</asp:Repeater>

</div> 
</div> 

<div class="right"> 

<div class="list_holder2"> <h1 class="headline"> SPEAKING ENGAGEMENTS </h1> 
<ul class="less_margin"> 
<asp:Repeater runat="server" ID="Speaking" OnItemDataBound="CP_ItemDataBound">
<ItemTemplate>
<li> <a class="se"> <%# DataBinder.Eval(Container.DataItem, "Headline").ToString.Replace("&","&amp;")%> </a> 
    <strong> <%# DataBinder.Eval(Container.DataItem, "Presenter").ToString.Replace("&", "&amp;")%> 
    <span class="block">
    <%# DataBinder.Eval(Container.DataItem, "Venue")%><span id="Venuebr" runat="server" ><br /></span>
    <%# DataBinder.Eval(Container.DataItem, "Event_Date")%><span id="bar" runat="server"> | </span><%# DataBinder.Eval(Container.DataItem, "Location")%>
    </span>
    </strong> 

<p> <%# DataBinder.Eval(Container.DataItem, "Content").ToString.Replace("&","&amp;")%> </p> </li> 
</ItemTemplate>
</asp:Repeater>
</ul>
<p class="inviteUs">
<span>Invite us to speak:</span><br />The principals and architects of Perkins Eastman speak on a variety of topics related to our firm, 
practice areas, and design expertise. If you would like to arrange for a speaker, 
please contact us via <a href="mailto:info@perkinseastman.com?subject=Invite%20us%20to%20speak">email</a>.
</p>  
</div> 
</div>
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    <!--</form>--> 
</body>
</html>
