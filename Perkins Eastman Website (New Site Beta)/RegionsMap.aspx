﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RegionsMap.aspx.vb" Inherits="RegionsMap" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="literalPageTitle" runat="server"></asp:Literal></title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
   
</head>
<body class="pe2">
    <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    <form id="form1" runat="server">
        <div id="ContentBody" style="display: none;"><asp:Literal ID="literalDescription" runat="server"></asp:Literal></div>
        <div id="ClientListContent" style="display: none;">Loading...</div>
        <div class="breadcrumbs"><a href="/" class="active">Home</a> | <a href="/#linkPage3" class="active">REGIONS</a> | <asp:Literal ID="literalName2" runat="server"></asp:Literal></div>
        <div class="head_project"><asp:Literal ID="literalName" runat="server"></asp:Literal></div>
        
        <div class="project_details">

            <p id="mapBox">
                <img id="mapImage" src="images/map/clear.gif" alt="" usemap="#worldmap" style="width: 598px; height: 295px; overflow: hidden; border: 0; background: url(&quot;images/map/world.png&quot;) no-repeat scroll 0pt 0pt rgb(255, 255, 255);" />
            </p>
            <map id="worldmap" name="worldmap">
                <area shape="poly" coords="62,49,93,15,167,0,283,1,277,7,263,30,236,40,226,53,202,44,197,57,209,65,214,80,212,83,198,78,188,89,183,82,185,78,185,73,178,81,160,88,160,80,152,74,146,75,142,73,92,73,86,72,71,62,83,61,73,51,69,52"
                href="/region.aspx?name=The Americas"
                alt="The Americas" title="The Americas" onmouseover="showmap('theamericas')"
                onmouseout="showmap()" />
                <area shape="poly" coords="87,75,74,64,81,62,81,60,80,58,74,53,71,51,68,53,65,50,63,50,63,30,110,2,270,2,284,3,263,31,242,39,227,53,210,66,216,81,210,83,192,88,185,88,187,82,184,76,179,76,180,81,160,90,160,81,152,74,152,74"
                href="/region.aspx?name=The Americas"
                alt="The Americas" title="The Americas" onmouseover="showmap('theamericas')"
                onmouseout="showmap()" />
                <area shape="poly" coords="179,294,162,277,176,216,157,189,159,164,98,128,90,101,121,109,140,123,164,126,166,123,166,116,177,123,198,138,207,150,211,160,229,170,246,188,247,200,240,222,236,231,213,262,195,276,211,288,211,288"
                href="/region.aspx?name=The Americas"
                alt="The Americas" title="The Americas" onmouseover="showmap('theamericas')"
                onmouseout="showmap()" />
                <area shape="poly" coords="545,275,473,266,488,204,456,185,414,159,403,121,411,120,407,114,408,114,421,100,424,87,451,68,465,66,500,63,516,76,524,77,518,87,518,93,535,80,540,81,539,102,512,147,589,212,598,280"
                href="/region.aspx?name=Asia"
                alt="Asia" title="Asia" onmouseover="showmap('asia')" onmouseout="showmap()" />
                <area shape="poly" coords="416,94,394,72,389,74,394,81,392,88,389,87,378,81,374,79,378,89,369,87,365,83,364,94,354,98,349,105,334,105,315,99,308,93,298,95,288,98,275,96,275,83,284,69,274,53,281,46,315,27,313,-1,598,2,598,55,575,74,544,87,538,82,529,79,522,88,514,83,521,77,504,64,497,70,486,70,486,72,474,75,464,66,460,71,450,69,444,77,432,92,432,92,422,92"
                href="/region.aspx?name=Europe"
                alt="Europe" title="Europe" onmouseover="showmap('europe')" onmouseout="showmap()" />
                <area shape="poly" coords="254,32,279,46" href="/region.aspx?name=Europe"
                alt="Europe" title="Europe" onmouseover="showmap('europe')" onmouseout="showmap()" />
                <area shape="poly" coords="267,106,276,117" href="/region.aspx?name=Europe"
                alt="Europe" title="Europe" onmouseover="showmap('europe')" onmouseout="showmap()" />
                <area shape="poly" coords="341,253,326,254,319,238,311,211,315,198,303,172,299,163,280,168,266,151,267,123,284,117,280,107,287,100,289,98,306,96,313,96,323,109,332,106,357,106,357,99,368,95,371,91,375,94,383,96,388,89,397,84,397,76,414,94,423,99,419,111,410,120,412,122,404,121,401,123,401,130,396,146,374,177,390,208,381,235,377,236"
                href="/region.aspx?name=Africa and the Middle East"
                alt="Africa / Middle East" title="Africa / Middle East" onmouseover="showmap('middle')"
                onmouseout="showmap()" />
            </map>

           	<p id="MapText" runat="server" class="project_description regionsMap"></p>
        </div>

        <div style="margin: 0px 0px 0px 0px;">
        </div>
        <div style="height: 15px; margin: 0px;">&nbsp;</div>

        <script type="text/javascript">

            function showmap(continent) {
                var cmap = document.getElementById('mapImage');
                if (continent == 'theamericas') cmap.style.backgroundPosition = '0 -295px';
                else if (continent == 'asia') cmap.style.backgroundPosition = '0 -590px';
                else if (continent == 'europe') cmap.style.backgroundPosition = '0 -885px';
                else if (continent == 'middle') cmap.style.backgroundPosition = '0 -1180px';
                else cmap.style.backgroundPosition = '0 0';
            }
        </script>
    
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
