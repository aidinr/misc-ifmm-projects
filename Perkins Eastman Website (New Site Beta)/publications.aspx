﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="publications.aspx.vb" Inherits="publications" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Perkins Eastman: Publications</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
    <meta name="keywords" content="Building Type Basics for Elementary and Secondary Schools, 2nd Edition, Public Architecture Now!, Building Type Basics for Senior Living, International Practice for Architects, Architect's Essentials of Starting, Assessing, and Transitioning a Design Firm, The Architect’s Guide to Design-Build Services, 1000x Architecture of the Americas, Details in Architecture, Contemporary Living Spaces for the Elderly, Retiring in Style Around the World, Healthcare Spaces No. 3, Healthcare Spaces No. 2, Educational Environments No. 3, Schools and Kindergartens A Design Manual, Corporate Interiors No. 6" />
</head>
<body class="pe2">
     <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    <form id="form1" runat="server">

<script type="text/javascript">
	jQuery(".main").css("background-color", "#FFF");
</script>
 <div class="breadcrumbs" ><a href="/" class="active">Home</a> | <a href="/#linkPage4" class="active" >INSIGHTS</a> | PUBLICATIONS</div>
<div class="head_standard">Publications</div>


                <asp:Repeater ID="RepeaterPublications" runat="server" OnItemDataBound="RepeaterPublications_ItemDataBound">
                    <ItemTemplate>
                            <table cellpadding="0" cellspacing="0" border="0" height="198">
                                <tr>
                                    <td align="left" valign="top">
            	                    <img src="<%=Session("WSRetrieveAsset")%>type=news&size=1&width=140&height=183&id=<%#Container.DataItem("news_id")%>" width="140" height="183" style="float: left; margin-bottom: 0px; margin-left: 15px;" alt=""/>
                                    </td>
                                    <td align="left" valign="top">
                                        <div class="copy_standard red bold ital" style="margin-bottom: 0px;"><%#Container.DataItem("headline")%></div>
                                        <div class="copy_standard" style="margin-top: 0px; margin-right: 15px;">
                                            <%#Container.DataItem("publicationtitle")%><br />
                                            <asp:Literal ID="literalAuthor" runat="server"></asp:Literal>
                                            <%#Container.DataItem("content")%><br /><br />
                                            <%#Container.DataItem("contact")%>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                    </ItemTemplate>
                </asp:Repeater>
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
