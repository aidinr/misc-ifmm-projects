﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="eekArchitects.aspx.vb" Inherits="eekArchitects" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><asp:Literal ID="literalPageTitle" runat="server"></asp:Literal></title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
</head>
<body id="eek" class="pe2">
      <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>

        <div id="ClientListContent" style="display: none;">Loading...</div>
        <div id="ContentBody" style="display: none;"><asp:Literal ID="literalDescription" runat="server"></asp:Literal></div>

        <div id="breadcrumbs" runat="server" class="breadcrumbs"></div>

        <div id="title" runat="server" class="head_project"></div>

        <div class="project_details">

        <p class="eekPageLeft">
        <img id="eekPic" runat="server" alt="" src="" />
        </p>

        <p class="project_description eekPageRight">
            <asp:Literal id="eekText" runat="server"></asp:Literal>
            <asp:Literal ID="literalBrowseClient" runat="server" ></asp:Literal>
         </p>
               
        </div>

        <div style="margin: 0px 0px 0px 0px;">

        </div>

        <div style="height: 15px; margin: 0px;">&nbsp;</div>

  <script type="text/javascript">
      jQuery("#ClientList").click(function () {
          var data = "";
          if (jQuery("#ClientList").html() == 'Browse selected projects') {
              data = '<asp:literal id="literalClientList" runat="server"></asp:literal>';
              jQuery("#ClientListContent").html(data);
              jQuery(".project_description").html(jQuery("#ClientListContent").html());
              jQuery("#mapPlus").css('display', 'none');
              jQuery("#ClientList").html('Hide selected projects');
          } else {
              jQuery(".project_description").html(jQuery("#ContentBody").html());
              jQuery("#mapPlus").css('display', 'block');
              jQuery("#ClientList").html('Browse selected projects');
          }
      });

</script>
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
 
</body>
</html>