﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class press
    Inherits System.Web.UI.Page
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Redirect("/news.aspx")

        'Dim headerControl As Web.UI.Control
        'headerControl = Me.LoadControl("includes/header.ascx")
        'pageHeader.Controls.Add(headerControl)

        'Dim bodyTopControl As Web.UI.Control
        'bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        'bodyTop.Controls.Add(bodyTopControl)

        'Dim bodyFooterControl As Web.UI.Control
        'bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        'bodyBottom.Controls.Add(bodyFooterControl)

        'GeneratePressReleases()
        'GenerateSpeakingEngagements()
        'GenerateNews()



    End Sub

    Sub GeneratePressReleases()

        Dim sql = "select * from ipm_news where type = @type_id and show = 1 and post_date < getdate() and pull_date > getdate() order by post_date desc"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim typeIDParameter As New SqlParameter("@type_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(typeIDParameter)
        MyCommand1.SelectCommand.Parameters("@type_id").Value = ConfigurationSettings.AppSettings("NewsTypePressRelease")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        RepeaterPressReleases.DataSource = dt1
        RepeaterPressReleases.DataBind()
    End Sub

    Sub GenerateSpeakingEngagements()

        Dim sql = "select a.headline headline, a.content content, b.item_value presenter, c.item_value dates, d.item_value topic, e.item_value location, cast(isnull(f.item_value,10000) as int) sort_value  from ipm_news a left join ipm_news_field_value b on b.news_id = a.news_id and b.item_id = @presenter_id left join ipm_news_field_value c on a.news_id = c.news_id and c.item_id = @dates_id left join ipm_news_field_value d on a.news_id = d.news_id and d.item_id = @topic_id left join ipm_news_field_value e on a.news_id = e.news_id and e.item_id = @location_id left join ipm_news_field_value f on a.news_id = f.news_id and f.item_id = @sortUDFID where type = @type_id and show = 1 and post_date < getdate() and pull_date > getdate() order by sort_value, post_date desc"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim typeIDParameter As New SqlParameter("@type_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(typeIDParameter)
        MyCommand1.SelectCommand.Parameters("@type_id").Value = ConfigurationSettings.AppSettings("NewsTypeSpeakingEngagement")

        Dim presenterIDParameter As New SqlParameter("@presenter_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(presenterIDParameter)
        MyCommand1.SelectCommand.Parameters("@presenter_id").Value = ConfigurationSettings.AppSettings("presenterItemID")

        Dim datesIDParameter As New SqlParameter("@dates_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(datesIDParameter)
        MyCommand1.SelectCommand.Parameters("@dates_id").Value = ConfigurationSettings.AppSettings("datesItemID")

        Dim topicIDParameter As New SqlParameter("@topic_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(topicIDParameter)
        MyCommand1.SelectCommand.Parameters("@topic_id").Value = ConfigurationSettings.AppSettings("topicItemID")

        Dim locationIDParameter As New SqlParameter("@location_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(locationIDParameter)
        MyCommand1.SelectCommand.Parameters("@location_id").Value = ConfigurationSettings.AppSettings("locationItemID")

        Dim sortIDParameter As New SqlParameter("@sortUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(sortIDParameter)
        MyCommand1.SelectCommand.Parameters("@sortUDFID").Value = "2401236"

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        RepeaterSpeakingEngagements.DataSource = dt1
        RepeaterSpeakingEngagements.DataBind()
    End Sub

    Sub GenerateNews()

        Dim sql = "select * from ipm_news where type = @type_id and show = 1 and post_date < getdate() and pull_date > getdate() order by post_date desc"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim typeIDParameter As New SqlParameter("@type_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(typeIDParameter)
        MyCommand1.SelectCommand.Parameters("@type_id").Value = ConfigurationSettings.AppSettings("NewsTypeGeneralNews")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        RepeaterNews.DataSource = dt1
        RepeaterNews.DataBind()
    End Sub


    Public Sub RepeaterPressRelease_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim projectPDF As Literal = e.Item.FindControl("literalPressPDF")
            Dim literalBoldStart As Literal = e.Item.FindControl("literalBoldStart")
            Dim literalBoldEnd As Literal = e.Item.FindControl("literalBoldEnd")

            If (e.Item.DataItem("pdf") = "1") Then

                'projectPDF.Text = "Click&nbsp;<a target=""blank"" href=""" & Session("WSRetrieveAsset") & "id=" & e.Item.DataItem("news_id") & "&type=newspdf&size=0&cache=1"">here</a> for more information."
                projectPDF.Text = "Click&nbsp;<a target=""blank"" href=""GetPDF.aspx?pdf=" & e.Item.DataItem("news_id") & """>here</a> for more information."

            End If

            If (e.Item.ItemIndex = 0) Then
                literalBoldStart.Text = "<b>"
                literalBoldEnd.Text = "</b>"
            End If

        End If

    End Sub
    Public Sub RepeaterSpeakingEngagements_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim theTopic As Literal = e.Item.FindControl("literalTopic")

            If Not (IsDBNull(e.Item.DataItem("topic"))) Then
                If (e.Item.DataItem("topic") <> "") Then

                    'theTopic.Text = "&ldquo;" & e.Item.DataItem("topic") & "&rdquo;<br />"
                    theTopic.Text = e.Item.DataItem("topic")

                End If
            End If
            

        End If

    End Sub


End Class
