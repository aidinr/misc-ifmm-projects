﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient

Partial Class featuredProjectsAjax
    Inherits System.Web.UI.Page

    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim projectID As String = Request.QueryString("p")
        Dim sleepTime As String = Request.QueryString("s")

        If (Trim(sleepTime) <> "") Then
            Try
                System.Threading.Thread.Sleep(CInt(sleepTime))
            Catch ex As Exception

            End Try


        End If
        Dim sql As String = "select *, j.item_value project_name, d.item_value country_name, h.item_value city_name, i.item_value state_name from IPM_CARROUSEL_item a inner join ipm_asset b on a.asset_id = b.asset_id and b.available = 'y' inner join ipm_project c on b.ProjectID = c.projectid and c.available = 'y' left join ipm_project_field_value d on d.item_id = @country_item_id and d.projectid = c.projectid left join IPM_STATE e on e.State_id = c.State_id left join ipm_carrousel_sort f on a.Carrousel_ID = f.Carrousel_ID and f.Sort_Name = @sortName and f.available = 'y' left join IPM_CARROUSEL_ITEM_SORT g on a.Asset_ID = g.Item_ID and g.sort_id = f.sort_id left join ipm_project_field_value h on h.item_id = @cityUDFID and h.projectid = b.projectid left join ipm_project_field_value i on i.item_id = @stateUDFID and i.projectid = b.projectid left join ipm_project_field_value j on j.projectid = b.projectid and j.item_id = @shortNameUDFID where  a.Carrousel_ID = @carouselID and a.Available = 'y' order by g.Sort_value asc"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim countryIDParameter As New SqlParameter("@country_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(countryIDParameter)
        MyCommand1.SelectCommand.Parameters("@country_item_id").Value = ConfigurationSettings.AppSettings("countryUDFID")

        Dim cityIDParameter As New SqlParameter("@cityUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(cityIDParameter)
        MyCommand1.SelectCommand.Parameters("@cityUDFID").Value = ConfigurationSettings.AppSettings("cityUDFID")

        Dim stateIDParameter As New SqlParameter("@stateUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(stateIDParameter)
        MyCommand1.SelectCommand.Parameters("@stateUDFID").Value = ConfigurationSettings.AppSettings("stateUDFID")

        Dim carouselIDParameter As New SqlParameter("@carouselID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(carouselIDParameter)
        MyCommand1.SelectCommand.Parameters("@carouselID").Value = ConfigurationSettings.AppSettings("homepageCarouselID")

        Dim shortNameIDParameter As New SqlParameter("@shortNameUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(shortNameIDParameter)
        MyCommand1.SelectCommand.Parameters("@shortNameUDFID").Value = ConfigurationSettings.AppSettings("shortNameUDFID")

        Dim sortNameParamter As New SqlParameter("@sortName", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(sortNameParamter)
        MyCommand1.SelectCommand.Parameters("@sortName").Value = ConfigurationSettings.AppSettings("sortName")



        Dim DT1 As New DataTable("featuredproject")
        Try
            MyCommand1.Fill(DT1)
        Catch ex As Exception

        End Try


        If (DT1.Rows.Count > 0) Then

            Dim i As Integer = -1

            For Each row As DataRow In DT1.Rows

                i = i + 1

                If (row("projectid").ToString.Trim() = projectID.Trim()) Then
                    literalImage.Text = "<img src=""" & Session("WSRetrieveAsset") & "type=asset&size=0&id=" & row("asset_id") & """ border=""0"" style=""margin: 0; margin-left: 0; margin-top: 0; width: 900px;"" />"
                    literalURL.Text = "<a href=""projectDetails.aspx?p=" & row("projectid") & """>" & row("project_name").ToString.Trim() & "</a>"

                    Try
                        If Not (IsDBNull(literalLocation.Text = row("city_name"))) Then
                            literalLocation.Text = row("city_name") & ", "
                        End If


                        If (Not IsDBNull(row("state_name"))) Then


                            If (Trim(row("state_name")) = "") Then
                                If Not (IsDBNull(row("country_name"))) Then
                                    If (Trim(row("country_name")) <> "") Then
                                        literalLocation.Text = literalLocation.Text & row("country_name")
                                    End If
                                End If
                            ElseIf (Trim(row("state_name")) <> "") Then
                                literalLocation.Text = literalLocation.Text & row("state_name")
                            End If
                        ElseIf Not (IsDBNull(row("country_name"))) Then
                            If (Trim(row("country_name")) <> "") Then
                                literalLocation.Text = literalLocation.Text & row("country_name")
                            End If
                        End If

                    Catch ex As Exception

                    End Try

                    Exit For

                End If

            Next
            If (i = DT1.Rows.Count - 1) Then
                literalProjectID.Text = DT1.Rows(0)("projectid")
            ElseIf (i < DT1.Rows.Count - 1) Then
                literalProjectID.Text = DT1.Rows(i + 1)("projectid")
            Else
                literalProjectID.Text = DT1.Rows(0)("projectid")
            End If



        End If




    End Sub

End Class
