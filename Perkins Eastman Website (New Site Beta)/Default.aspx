﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Perkins Eastman: Home Page</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
    <meta name="keywords" content="Perkins Eastman, Architects, Architecture, Planning, Programming, Sustainable design, LEED" />
    <script type="text/javascript"> 
    // <![CDATA[
    document.write('<style type="text/css">#Pages {visibility: hidden;}</style>');
    // ]]>
    </script> 
</head>
<body>
   <!-- <form id="form1" runat="server"> -->
    <div id="PageControls" class="delay9">
<p id="PageLeft"><a href="./"></a></p>
<p id="PageRight"><a href="./"></a></p>
</div>
    <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>

    <div id="Page">
<div id="Pages">
<div class="container" id="Page1"> 

<div class="left"> 

<div class="box">
<!-- Vertical Menu --> 

<div id="smoothmenu1" class="menu"> 
<ul class="short"> 
<!-- Adding class to specify height of the menu on different pages --> 
<li> <a href="firmProfile.aspx">Firm Profile</a> </li> 
<li> <a href="designPhilosophy.aspx">Design Approach</a> </li> 
<li> <a href="leadership.aspx">Leadership</a> </li> 
<li> <a href="awards.aspx">Honors and Awards</a> </li> 
<li> <a href="eekArchitects.aspx">EE&amp;K</a> </li> 
</ul> 
</div> 

<div class="awards"> <h2 id="IDAM_PAGE1_TITLE" runat="server"  class="heading"></h2> 
<img id="Page1Middle" runat="server" alt=" " src=" " />
<span id="IDAM_PAGE1_PiC_LABEL" runat="server" class="title"  ></span> 
<span id="IDAM_PAGE1_SUB_TITLE" runat="server"></span> 
<p id="IDAM_PAGE1_DESC" runat="server"></p>
<a id="IDAM_PAGE1_PROJECT_LINK" runat="server" class="red_link right" href="">Project Details [+]</a> 
<a id="IDAME_PAGE1_AWARDS_LINK" class="red_link right" href="/awards.aspx" >Awards Details [+]</a> 
</div>
</div>

<div class="featured"> <h2 id="IDAM_PAGE1_TITLE2" runat="server" class="heading"></h2> 
<!-- To pass W3C check you need to have alt=" " and before the src or .net eats it -->
<a id="Page1BottomLeftImgLink" runat="server" href=""><img id="Page1BottomLeft" runat="server" alt=" " src="" /></a> 
<a id="IDAM_PAGE1_TITLE2_PICLABEL" runat="server" class="title title_push_bot" href="" ></a> 
<p id="IDAM_PAGE1_TITLE2_TEXT" runat="server"></p>
<a class="logoAbB" style="position: absolute; bottom: 2px; left: 0"><img src="assets/images/dynamic/logo_bot.png" class="logo_bot" alt=""/></a>
<a href="eekArchitects.aspx" id="IDAM_PAGE1_TITLE2_URL" runat="server" class="red_link red_link_push_top right">more [+]</a>
</div>
</div>

<div class="right"> 
<!-- Big image on home page --> <img id="Page1Right" runat="server"  alt=" " src="" />
</div>
</div>

<div class="container" id="Page2"> 

<div class="left"> 

<div class="featured_projects"> 
<h2 ID="IDAM_PAGE2_PROJECT_TITLE" runat="server" class="heading"></h2> 
<img id="Page2TopLeft" runat="server"  alt=" " src=" " /> 
<span ID="IDAM_PAGE2_CAP_TITLE" runat="server"  class="title"></span> 
<span id="IDAM_PAGE2_CITY" runat="server" ></span>, <span id="IDAM_PAGE2_STATE" runat="server"></span> 
<p id="IDAM_PAGE2_DESCRIPTION" runat="server" style="margin: 11px 0;"> </p> 
<a id="IDAM_PAGE2_FPLINK" runat="server" class="red_link right" href="">More [+]</a> 
<!-- image --><img id="Page2BottomLeft" runat="server" alt=" " class="img_box" src=""  />
</div>
<!-- Vertical Menu -->

<div id="smoothmenu2" class="menu"> 
<ul class="long"> 
<!-- Adding class to specify height of the menu on different pages --> 
<li> <a href="projects.aspx?list=2400012">Corporate Interiors</a> </li> 
<li> <a>Education</a> 
<ul> 
<li> <a href="projects.aspx?list=3409328">Campus Planning</a> </li> 
<li> <a href="projects.aspx?list=2400014">Higher Education</a> </li> 
<li> <a href="projects.aspx?list=2400017">K-12 Education</a> </li> 
<li> <a href="projects.aspx?list=3409341" class="no_border">Town and Gown</a> </li> 
</ul> </li> 
<li> <a href="projects.aspx?list=2400024">Healthcare</a> </li> 
<li> <a href="projects.aspx?list=2400015">Hospitality</a> </li> 
<li> <a href="projects.aspx?list=2400016">Housing</a> </li> 
<li> <a href="projects.aspx?list=2400018">Office and Retail</a> </li> 
<li> <a href="projects.aspx?list=2400019">Public | Cultural </a> </li> 
<li> <a href="projects.aspx?list=2400020">Science | Technology</a> </li> 
<li> <a href="projects.aspx?list=2400021">Senior Living</a> </li> 
<li> <a>Urbanism</a> 
<ul> 
<li> <a href="projects.aspx?list=3409345">Downtown Redevelopment</a> </li> 
<li> <a href="projects.aspx?list=3409347">New Town Planning</a> </li>
<li> <a href="projects.aspx?list=3409349">Transit and Development</a></li> 
<li> <a href="projects.aspx?list=2400022">Urban Design</a> </li> 
<li> <a href="projects.aspx?list=3409351" class="no_border">Waterfront</a> </li> 
</ul> </li> 
</ul>
</div>
</div>

<div class="right"> 

<div class="bproj"> 
    <a id="MPP_LINK" runat="server" class="bprojImg2" href="" >
    <span id="MPP_TEXT1" runat="server"></span><br />
    <span id="MPP_TEXT2" runat="server"></span>
    </a>

<div class="in_the_news"> 

<div class="awards"> 
<h2 id="IDAM_PAGE2_TITLE2" runat="server" class="heading"></h2>
<img id="Page2BottomMiddle" runat="server" alt=" " src=""/>
<span ID="IDAM_PAGE2_IMAGE3_TEXT" runat="server" class="title title_push_bot"></span> 
<a ID="IDAM_PAGE2_IAMGE3_LINK" runat="server" class="red_link right" href="#">Project Details [+]</a> 
<a href="news.aspx" class="red_link right">News Details [+]</a> 
</div> <img id="Page2BottomRight" runat="server"  class="right" alt=" " src=""/> 
</div> 
</div>
</div>
</div>

<div class="container" id="Page3"> 

<div class="left"> 
    <a id="MRP_LINK" runat="server" class="bprojImg3" href="">
        <span id="MRP_TEXT1" runat="server"></span><br />
        <span id="MRP_TEXT2" runat="server"></span>
    </a> 
</div> 

<div class="right push_left"> 

<div class="left"> 
<!-- Vertical Menu -->

<div id="smoothmenu3" class="menu"> 
<ul class="short title_push_bot"> 
<!-- Adding class to specify height of the menu on different pages --> 
<li> <a href="/RegionsMap.aspx">Regions Map</a> </li> 
<li> <a href="/region.aspx?name=Africa and the Middle East">Africa and the Middle East</a> </li> 
<li> <a href="/region.aspx?name=Asia">Asia</a> </li>
<li> <a href="/region.aspx?name=China">China</a> </li>
<li> <a href="/region.aspx?name=Europe">Europe</a> </li> 
<li> <a href="/region.aspx?name=The Americas">The Americas</a> </li> 
</ul>
</div>

<div class="in_the_news2"> 

<div class="awards"> 
<h2 ID="IDAM_PAGE3_PIC2_TITLE" runat="server" class="heading"></h2> 
<img id="Page3Middle" runat="server" alt=" " src=""/>
<a ID="IDAM_PAGE3_PIC2_CAPTION" runat="server" href="" class="title title_push_bot"></a> 
<span id="IDAM_PAGE_PIC2_SUBCAP" runat="server"></span> 
<p id="IDAM_PAGE3_PIC2_DESCRIPTION" runat="server" style="margin: 11px 0;"></p> 
<a id="IDAM_PAGE3_PIC2_PROJECTLINK" runat="server"  class="red_link right" href="">Project Details [+]</a> 
<a id="IDAM_PAGE3_PIC2_NEWSLINK" runat="server" class="red_link right" href="">News Details [+]</a> 
</div>
</div>
</div>

<div class="right"> 

<div class="featured_projects push_left"> <img id="Page3TopRight" runat="server" alt=" " style="height:  290px;" src="" /> 
<!-- image --> 
<img id="Page3BottomRight" runat="server" alt=" " style="margin-top: 18px" src="" /> 
</div>
</div>
</div>
</div>

<div class="container" id="Page4"> 

<div class="left"> 

<div id="page4POV">
<!-- silly ie fix -->
<h2 id="IDAM_PAGE4_PIC1_TITLE" runat="server" class="heading"></h2>
<img id="Page4TopLeft" runat="server" alt=" " src=""/>
<span id="IDAM_PAGE4_PIC1_CAP" runat="server" class="title title_push_bot2" href="views.aspx"></span>
<span id="IDAM_PAGE4_PIC1_SUBTITLE" runat="server" class="span_block"></span>
<a id="IDAM_PAGE4_PIC1_DESC_LINK" runat="server" href="views.aspx"><span id="IDAM_PAGE4_PIC1_DESCRIPTION" runat="server" class="red_text"></span></a>
</div>

<div class="in_the_news2 push_top left"> <img id="Page4BottomLeft" runat="server" alt=" " src="" />
</div>
</div>

<div class="middle"> <img id="Page4Middle" runat="server" alt=" " src="" /></div>

<div class="right"> 
<!-- Vertical Menu --> 

<div id="smoothmenu4" class="menu"> 
<ul class="short title_push_bot"> 
<!-- Adding class to specify height of the menu on different pages --> 
<li> <a href="views.aspx">Point of View</a> </li> 
<li> <a href="publications.aspx">Publications</a> </li> 
</ul> 
</div> 

<div class="in_the_news2"> 

<div class="awards"> 
<h2 id="IDAM_PAGE4_PIC4_TITLE" runat="server" class="heading green"></h2> 
<img id="Page4Right" runat="server" alt=" " src=""/>
<span id="IDAM_PAGE4_PIC4_CAP" runat="server" href="views.aspx" class="title title_push_bot2"></span> 
<span id="IDAM_PAGE4_PIC4_SUBCAP" runat="server" class="span_block"> </span> 
<a id="IDAM_PAGE4_PIC4_DESCRIPTION" runat="server" class="red_text" href="views.aspx"></a> 
<span id="IDAM_PAGE4_PIC4_LOCDATE" runat="server" class="span_block"></span> 
</div> 
</div>
</div>
</div>

</div>
</div> 


    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    <!--</form> -->
</body>
</html>
