﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class locations
    Inherits System.Web.UI.Page
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)

        GenerateLocations()


    End Sub

    Sub GenerateLocations()

        Dim sql As String = "select *, b.name folderName from IPM_ASSET_CATEGORY b, IPM_ASSET a  left join ipm_asset_field_value c on c.item_id = @websiteDescriptionUDFID and c.asset_id = a.asset_id where a.ProjectID = @projectid and b.CATEGORY_ID = a.Category_ID and b.NAME = @folderName and media_type = 21355126 and a.available = 'Y'"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim projectIDParameter As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(projectIDParameter)
        MyCommand1.SelectCommand.Parameters("@projectid").Value = ConfigurationSettings.AppSettings("aboutUsProjectID")

        Dim folderNameParameter As New SqlParameter("@folderName", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(folderNameParameter)
        MyCommand1.SelectCommand.Parameters("@folderName").Value = ConfigurationSettings.AppSettings("locationsFolderName")

        Dim websiteDescriptionParameter As New SqlParameter("@websiteDescriptionUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(websiteDescriptionParameter)
        'MyCommand1.SelectCommand.Parameters("@websiteDescriptionUDFID").Value = ConfigurationSettings.AppSettings("websiteContentUDFID")
        MyCommand1.SelectCommand.Parameters("@websiteDescriptionUDFID").Value = 2400398

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then
            Try

                literalLocationTitle.Text = dt1.Rows(0)("folderName").ToString.ToUpper
                If dt1.Rows(0)("item_value").ToString.Trim <> "" Then
                    literalLocation.Text = dt1.Rows(0)("item_value").ToString.Trim
                Else
                    'literalLocation.Text = "This is placeholder content.  The data was forked for development purposes and some data is not available in the development environment.  The real conent will be displayed when the site goes live."
                End If


            Catch ex As Exception

                'literalLocationTitle.Text = "This is placeholder content.  The data was forked for development purposes and some data is not available in the development environment.  The real conent will be displayed when the site goes live."
                'literalLocation.Text = "This is placeholder content.  The data was forked for development purposes and some data is not available in the development environment.  The real conent will be displayed when the site goes live."

            End Try
        Else

            'literalLocationTitle.Text = "This is placeholder content.  The data was forked for development purposes and some data is not available in the development environment.  The real conent will be displayed when the site goes live."
            'literalLocation.Text = "This is placeholder content.  The data was forked for development purposes and some data is not available in the development environment.  The real conent will be displayed when the site goes live."

        End If

    End Sub

End Class
