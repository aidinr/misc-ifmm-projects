
var moving = 0;
var menuClose;
var menuM = 0;
var movingAuto;
var IEold = false /*@cc_on || @_jscript_version < 5.8 @*/;

function closeDelay(e) {
    if (menuM != 1) $(e).fadeOut(300);
}

$(window).unload(function () {
    $('input').val('');
    document.body.innerHTML = '';
});

// Do this for ALL pages
$(document).ready(function () {

    $('#ClientList').css('text-decoration','none');
    $('#ClientList').hover(
  function () {
    $(this).css('text-decoration','underline');
  },
  function () {
    $(this).css('text-decoration','none');
  }
);

    $('#searchQ').val($('#searchQ').attr('title'));
    $('#searchQ').focus(function () {
        if ($('#searchQ').val() == $('#searchQ').attr('title')) $('#searchQ').val('');
    });
    $('#searchQ').blur(function () {
        if (!$('#searchQ').val()) $('#searchQ').val($('#searchQ').attr('title'));
    });

    var path = document.location.pathname.replace(/\//gi, "").replace(/%20/gi, " ").replace(/\\/gi, "\/");
    if ($('#smoothmenu1').html().indexOf(path) >2) $('.linkPage1').parent().addClass('selected');
    else if ($('#smoothmenu2').html().indexOf(path) >2) $('.linkPage2').parent().addClass('selected');
    else if ($('#smoothmenu3').html().indexOf(path) >2) $('.linkPage3').parent().addClass('selected');
    else if ($('#smoothmenu4').html().indexOf(path) >2) $('.linkPage4').parent().addClass('selected');

    if (!$('#PageControls').attr('id'))  $('ul.menu').css('visibility','visible');

    if ($('#fancybox-overlay').attr('id') && $('h1.headline').attr('class')  && $('h1.headline').first().html().toLowerCase().indexOf('point of view') != -1  )   $('body.pe3 .left .list_holder div.item a[href*=pdf]').fancybox({'type': 'iframe', 'href': '/lead.htm', 'transitionIn': 'none', 'transitionOut': 'none', 'titlePosition': 'outside', 'onClosed':  function(link) {
           window.open(link, '_blank');
	   return false;
    }});

    if ($('#PageControls').attr('id')) movingAuto = setInterval("PageRight(1)", (Math.round($('#PageControls').attr('class').substr(5)) * 1000));

    ddsmoothmenu.init({
        mainmenuid: "smoothmenu1",
        //Menu  DIV  id
        orientation: 'v',
        //Horizontal  or  vertical  menu:  Set  to  "h"  or  "v"
        classname: 'menu',
        //class  added  to  menu's  outer  DIV
        //customtheme:  ["#804000",  "#482400"],
        contentsource: "markup" //"markup"  or  ["container_id",  "path_to_menu_file"]
    })
    ddsmoothmenu.init({
        mainmenuid: "smoothmenu2",
        //Menu  DIV  id
        orientation: 'v',
        //Horizontal  or  vertical  menu:  Set  to  "h"  or  "v"
        classname: 'menu',
        //class  added  to  menu's  outer  DIV
        //customtheme:  ["#804000",  "#482400"],
        contentsource: "markup" //"markup"  or  ["container_id",  "path_to_menu_file"]
    })
    ddsmoothmenu.init({
        mainmenuid: "smoothmenu3",
        //Menu  DIV  id
        orientation: 'v',
        //Horizontal  or  vertical  menu:  Set  to  "h"  or  "v"
        classname: 'menu',
        //class  added  to  menu's  outer  DIV
        //customtheme:  ["#804000",  "#482400"],
        contentsource: "markup" //"markup"  or  ["container_id",  "path_to_menu_file"]
    })
    ddsmoothmenu.init({
        mainmenuid: "smoothmenu4",
        //Menu  DIV  id
        orientation: 'v',
        //Horizontal  or  vertical  menu:  Set  to  "h"  or  "v"
        classname: 'menu',
        //class  added  to  menu's  outer  DIV
        //customtheme:  ["#804000",  "#482400"],
        contentsource: "markup" //"markup"  or  ["container_id",  "path_to_menu_file"]
    })

    $('#subDrop div div.menu').each(function (index) {
        $(this).find('a').last().addClass('lastm');
        $(this).find('ul').last().prev('a').addClass('lastm');
    });

    $('.pe2 .linkPage1, .pe3 .linkPage1').hover(

    function () {
        clearTimeout(menuClose);
        $('#subDrop #smoothmenu1').fadeIn(300);
        $('#subDrop #smoothmenu2').fadeOut(200);
        $('#subDrop #smoothmenu3').fadeOut(200);
        $('#subDrop #smoothmenu4').fadeOut(200);
    }, function () {
        menuClose = setTimeout("closeDelay('#subDrop #smoothmenu1')", 850);
    });
    $('#subDrop #smoothmenu1').hover(

    function () {
        menuM = 1;
    }, function () {
        $('#subDrop #smoothmenu1').fadeOut(300);
        menuM = 0;
    });
    $('.pe2 .linkPage2, .pe3 .linkPage2').hover(

    function () {
        clearTimeout(menuClose);
        $('#subDrop #smoothmenu1').fadeOut(200);
        $('#subDrop #smoothmenu2').fadeIn(300);
        $('#subDrop #smoothmenu3').fadeOut(200);
        $('#subDrop #smoothmenu4').fadeOut(200);
    }, function () {
        menuClose = setTimeout("closeDelay('#subDrop #smoothmenu2')", 850);
    });
    $('#subDrop #smoothmenu2').hover(

    function () {
        menuM = 1;
    }, function () {
        $('#subDrop #smoothmenu2').fadeOut(300);
        menuM = 0;
    });
    $('.pe2 .linkPage3, .pe3 .linkPage3').hover(

    function () {
        clearTimeout(menuClose);
        $('#subDrop #smoothmenu1').fadeOut(200);
        $('#subDrop #smoothmenu2').fadeOut(200);
        $('#subDrop #smoothmenu3').fadeIn(300);
        $('#subDrop #smoothmenu4').fadeOut(200);
    }, function () {
        menuClose = setTimeout("closeDelay('#subDrop #smoothmenu3')", 850);
    });
    $('#subDrop #smoothmenu3').hover(

    function () {
        menuM = 1;
    }, function () {
        $('#subDrop #smoothmenu3').fadeOut(300);
        menuM = 0;
    });
    $('.pe2 .linkPage4, .pe3 .linkPage4').hover(

    function () {
        clearTimeout(menuClose);
        $('#subDrop #smoothmenu1').fadeOut(200);
        $('#subDrop #smoothmenu2').fadeOut(200);
        $('#subDrop #smoothmenu3').fadeOut(200);
        $('#subDrop #smoothmenu4').fadeIn(300);
    }, function () {
        menuClose = setTimeout("closeDelay('#subDrop #smoothmenu4')", 850);
    });
    $('#subDrop #smoothmenu4').hover(

    function () {
        menuM = 1;
    }, function () {
        $('#subDrop #smoothmenu4').fadeOut(300);
        menuM = 0;
    });


    $('.BrowseClientList a').first().after('<a class="clientListHide" style="display: none">Hide the client list</a>');
    $('.BrowseClientList a').first().click(function() {
	$('.rightSidePromo').addClass('sp');
	$('.BrowseClientList a.clientListShow').hide();
	$('.BrowseClientList a.clientListHide').show();

     });

    $('.BrowseClientList a.clientListHide').click(function() {
	$('.rightSidePromo').removeClass('sp');
	$('.BrowseClientList a.clientListHide').hide();
	$('.BrowseClientList a.clientListShow').show();
     });


    $('.BrowseClientList a.hide').live('click',function() {
	$(this).removeClass('hide');
	$('.head_project a').remove();
     });

        if ($('#linkPrevProject').attr('id') && !$('#linkPrevProject').html()) $('.projectCaption').hide()

	if( $('.list_holder').hasClass('list_holder') && $('.list_holder2').hasClass('list_holder2')) {
		var h = $('.list_holder').height();
		if ($('.list_holder2').height() > h) h = $('.list_holder2').height();
		$('.list_holder').height(h+'px');
 	}
	$('.main').css('visibility','visible');

   if ( $('.head_standard').attr('class') && $('.head_standard').first().html().toLowerCase().indexOf('what makes us different') != -1)  {
    $('td .copy_standard').first().addClass('noIndent');
   }
   if ( $('.head_standard').attr('class') && $('.head_standard').first().html().toLowerCase().indexOf('locations') != -1)  {
    $('td .copy_standard').addClass('noIndent');
   }

   $('.pe3 div#wrapper div.main div.right div.list_holder2 ul.less_margin li a').each(function (index) {
     //   if ( $(this).attr('href') && $(this).attr('href')=='#') $(this).removeAttr('href');
    });


});


// Only do this for main page
$(document).ready(function () {
    if ($('#PageControls').attr('id')) {

        $('.linkPage1').click(function () {
            moving = 1;
            clearInterval(movingAuto);
            var iii = 0;
            while ($('.container').first().attr('id') !== 'Page1') {
                if (iii > 9) return false;
                $('#Pages').prepend($('.container').last());
            }
            moving = 0;
            checkPage();
        });
        $('.linkPage2').click(function () {
            moving = 1;
            clearInterval(movingAuto);
            var iii = 0;
            while ($('.container').first().attr('id') !== 'Page2') {
                if (iii > 9) return false;
                $('#Pages').prepend($('.container').last());
            }
            moving = 0;
            checkPage();
        });
        $('.linkPage3').click(function () {
            moving = 1;
            clearInterval(movingAuto);
            var iii = 0;
            while ($('.container').first().attr('id') !== 'Page3') {
                if (iii > 9) return false;
                $('#Pages').prepend($('.container').last());
            }
            moving = 0;
            checkPage();
        });
        $('.linkPage4').click(function () {
            moving = 1;
            clearInterval(movingAuto);
            var iii = 0;
            while ($('.container').first().attr('id') !== 'Page4') {
                if (iii > 9) return false;
                $('#Pages').prepend($('.container').last());
            }
            moving = 0;
            checkPage();
        });


        if (location.hash.substr(5)) {
            var iii = 0;
            while ($('.container').first().attr('id') != location.hash.substr(5)) {
                if (iii > 9) return false;
                $('#Pages').prepend($('.container').last());
            }
        }

        checkPage();

        $('#Page, #PageLeft, #PageRight, #header').mouseenter(function () {
            $('#Page').addClass('hover');
        }).mouseleave(function () {
            $('#Page').removeClass('hover');
        });



        $('#PageRight').click(function () {
            clearInterval(movingAuto);
            PageRight();
        });

        $('#PageLeft').click(function () {
            clearInterval(movingAuto);
            if (moving == 1) return false;
            moving = 1;
            checkPage();
            $('#Pages').prepend($('.container').last());
            $('#Pages').css('marginLeft', '-960px');
            $('#Pages').animate({
                marginLeft: '0'
            }, 900, function () {
                moving = 0;
                checkPage();
            });
        });

    }
  if( $('.head_standard').first().html()=='Publications') $('body').addClass('pagePublications');

   $('a').each(function(index) {
    if (!$(this).attr('href')) {
	$(this).addClass('anchorNoLink');
    }
   });

   $('p.BrowseClientList a').removeClass('anchorNoLink');

   if(IEold) $('#Pages').addClass('IE7');

});

function PageRight(auto) {

    if($(location).attr('hash') && auto==1) return false;
    var fb = 0;
    var f = document.getElementById('fancybox-overlay');
    if(f) {
     if ($('#fancybox-overlay').css('display')=='block' ) return false;
    }

    if (moving == 1) return false;
    if (!auto || !$('#Page').hasClass('hover')) {
        moving = 1;
        checkPage();
        $('#Pages').animate({
            marginLeft: '-960px'
        }, 900, function () {
            $('#Pages').css('marginLeft', '0');
            $('#Pages').append($('.container').first());
            moving = 0;
            checkPage();
        });
    }
}

function checkPage() {
    $('ul.menu li').removeAttr('class');
    if (!moving) {
        $('#PageLeft a').attr('href', '#link' + $('.container').last().attr('id'));
        $('#PageRight a').attr('href', '#link' + $('.container:eq(1)').attr('id'));
        $('.link' + $('.container').first().attr('id')).parent().addClass('selected');
	$('ul.menu').css('visibility','visible');
    }
    $('.slide, #Pages').css('visibility','visible');
}

$(document).ready(function() {

if( $('.vimeo:eq(0)').attr('href') ) {

var modalCSS ='\
<style type="text/css">\
#modalIframe{\
position: fixed;\
margin: auto;\
top: 90px;\
left: 15%;\
border: 0;\
padding: 22px;\
display: none;\
z-index: 1112;\
opacity: 0;\
visibility: hidden\
}\
#modalIframe iframe {\
margin: 0;\
padding: 0;\
border: 4px solid #eee;\
}\
#modalIframe a {\
position: absolute;\
display: block;\
top: 0;\
right: 0;\
width: 30px;\
height: 30px;\
background: transparent url(assets/video/closebox.png) no-repeat scroll 100% 2px;\
cursor: pointer;\
}\
#modalIframe a:hover {\
background-position: 100% -28px;\
}\
#modalOverlay {\
position: fixed;\
margin: 0;\
top: 0;\
left: 0;\
width: 100%;\
height: 100%;\
padding: 0;\
display: none;\
background: #707070;\
z-index: 1111;\
}\
#modalLoading {\
left: 48%;\
position: fixed;\
z-index: 1114;\
top: 33%;\
width: 32px;\
height: 32px;\
padding: 20px;\
border: 5px solid #111;\
background: #fff;\
display: none;\
}\
</style>';

 $('head').append(modalCSS);
 $('body').append('<div id="modalIframe"></div><div id="modalOverlay"></div><div id="modalLoading"><img src="assets/video/loading.gif" alt="Loading" title="Loading" /></div>');
 $("a.vimeo").click(function () {
	$("#modalIframe").removeAttr('style');
	$("#modalIframe").css("display", "block");
	$("#modalLoading").css("opacity", "0");
	$("#modalLoading").css("display", "block");
	var src = 'http://vimeo.com/moogaloop.swf?clip_id='+ getID($(this).attr('href')) +'&autoplay=1';
	$("#modalIframe").html('<iframe width="676" height="500" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src="'+ src +'" onload="modalShow(this)"></iframe><a></a>');
	$("#modalLoading").animate({"opacity": 1},1100);
	$("#modalOverlay").css("opacity", "0");
	$("#modalOverlay").css("display", "block");
	$("#modalOverlay").animate({"opacity": .8});

	return false;
 });
 $("#modalIframe").click(function () { 
	$("#modalIframe").css("visibility", "hidden");
	$("#modalIframe").css("opacity", "0");
	$("#modalIframe").css("display", "none");
	$("#modalIframe").removeAttr('style');
	$("#modalIframe").html('');
	jQuery("#modalOverlay").css("display", "none");
	return false;
 });

}

});

Array.prototype.getMax = function () {
    return Math.max.apply(Math, this);
}

function getID(str) {
    return str.split("?")[0].match(/\d+/g).getMax();
}

function modalShow(e) {
 var x = ($('body').width() / 2 ) - (e.width/2);
 $('#modalIframe').css('left',x+'px');
 $('#modalIframe').animate({"opacity": 1});
 $("#modalLoading").css("display", "none");
 $("#modalIframe").css("visibility", "visible");
 $("#modalIframe").css("opacity", "1");
}

