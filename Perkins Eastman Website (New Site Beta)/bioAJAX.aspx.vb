﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient

Partial Class bioAJAX
    Inherits System.Web.UI.Page

    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim userID As String = Request.QueryString("i")



        Dim sql = "select a.userid userid, FirstName,LastName, Bio, c.Item_Value qualifications from IPM_USER a left join ipm_user_field_value b on a.userid = b.user_id and b.item_id = @publish_item_id left join ipm_user_field_value c on a.userid = c.user_id and c.item_id = @qualifications_item_id  where a.USERID = @userid and Active = 'y'"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim userIDParameter As New SqlParameter("@userid", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userIDParameter)
        MyCommand1.SelectCommand.Parameters("@userid").Value = userID

        Dim qualificationsIDParameter As New SqlParameter("@qualifications_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(qualificationsIDParameter)
        MyCommand1.SelectCommand.Parameters("@qualifications_item_id").Value = ConfigurationSettings.AppSettings("qualificationsUDFID")


        Dim publishIDParameter As New SqlParameter("@publish_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(publishIDParameter)
        MyCommand1.SelectCommand.Parameters("@publish_item_id").Value = ConfigurationSettings.AppSettings("publishUDFID")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then

            Try

                literalName.Text = dt1.Rows(0)("firstname").ToString.Trim & " " & dt1.Rows(0)("lastname").ToString.Trim

                If (Not IsDBNull(dt1.Rows(0)("qualifications"))) Then

                    literalName.Text = literalName.Text & " " & dt1.Rows(0)("qualifications").ToString.Trim

                End If

            Catch ex As Exception

            End Try

            Try

                literalBio.Text = dt1.Rows(0)("bio").ToString.Trim.Replace(vbCrLf, "<br />")

            Catch ex As Exception

            End Try

            imageBio.ImageUrl = Session("WSRetrieveAsset").ToString().Replace("&amp;", "&") & "type=contact&size=1&width=250&height=350&id=" & dt1.Rows(0)("userid").ToString.Trim

        End If

    End Sub

End Class
