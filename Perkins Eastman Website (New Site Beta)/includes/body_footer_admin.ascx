﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="body_footer_admin.ascx.vb" Inherits="includes_body_footer_admin" %>
</div>
<!--
<div class="footer" align="center"><br />Copyright &copy; 2011 Perkins Eastman Architects PC. All rights reserved. Contact <a href="mailto:s.yates@perkinseastman.com">Webmaster</a>. Last Updated: 05.07.2009
&nbsp;<a href="rssFeeds.aspx">RSS Feed</a>.&nbsp;<a href="rssFeeds.cfm"><img src="./images/rss_feed.gif" border="0" /></a>&nbsp;Follow us on  <a href="http://twitter.com/PerkinsEastman" target="_blank">Twitter</a> and <a href="http://www.facebook.com/#!/pages/New-York-NY/Perkins-Eastman/100930636494?v=wall&ref=ts" target="_blank">Facebook</a>.
-->
<div id="footer"> <address> Copyright &copy; 2011 Perkins Eastman. All rights reserved. | Learn more about <a href="#linkPage1" class="red_link">EE&amp;K a Perkins Eastman company</a>. </address> 
<ul> 
<li> Connect </li> 
<li> <a href="http://www.linkedin.com/company/perkins-eastman" class="linked">Linkedin</a> </li> 
<li> <a href="http://twitter.com/PerkinsEastman" class="twit">Twitter</a> </li> 
<li> <a href="http://www.facebook.com/PerkinsEastman?v=wall&amp;ref=ts" class="fb">Facebook</a> </li> 
<li> <a href="#" class="gtalk">Gtalk</a> </li> 
<li> <a href="#" class="v">V</a> </li> 
<li> <a href="/rssFeeds.aspx" class="rss">RSS</a> </li>
<li> <a href="mailto:info@perkinseastman.com" class="mail">Mail</a> </li> 

</ul>
