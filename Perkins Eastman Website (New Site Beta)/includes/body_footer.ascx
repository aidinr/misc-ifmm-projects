﻿<%@ control language="VB" autoeventwireup="false" inherits="includes_body_footer" CodeFile="body_footer.ascx.vb" %>
<div class="c"></div>
</div>
<div id="footer">
<br />
<!--
Copyright &copy; 2011 Perkins Eastman. All rights reserved. Contact <a href="mailto:s.yates@perkinseastman.com">Webmaster</a>.
&nbsp; <a href="rssFeeds.aspx">RSS Feed</a>.&nbsp;<a href="rssFeeds.cfm"><img src="./images/rss_feed.gif" border="0" /></a>&nbsp;Follow us on  <a href="http://twitter.com/PerkinsEastman" target="_blank">Twitter </a> and <a href="http://www.facebook.com/#!/pages/New-York-NY/Perkins-Eastman/100930636494?v=wall&ref=ts" target="_blank">Facebook</a>.
-->
 <address> Copyright &copy; 2012 Perkins Eastman. All rights reserved. | Learn more about <a href="/eek.aspx" class="red_link">EE&amp;K a Perkins Eastman company</a>. </address> 
<ul> 
<li> Connect </li> 
<li> <a href="http://www.linkedin.com/company/perkins-eastman" class="linked">Linkedin</a> </li> 
<li> <a href="http://twitter.com/PerkinsEastman" class="twit">Twitter</a> </li> 
<li> <a href="http://www.facebook.com/PerkinsEastman?v=wall&amp;ref=ts" class="fb">Facebook</a> </li> 
<li> <a href="http://vimeo.com/perkinseastmanvideos" class="v">V</a> </li> 
<li> <a href="/rssFeeds.aspx" class="rss">RSS</a> </li>
<li> <a href="mailto:info@perkinseastman.com?subject=General%20Inquiry" class="mail">Mail</a> </li> 
</ul>
<script type="text/javascript">
    var emptyText = "Search Perkins Eastman";

    function focusSearch() {
        jQuery("input[name='s']").removeClass("gray");
        if (jQuery("input[name='s']").attr("value") == emptyText) {
            jQuery("input[name='s']").attr("value", '');
        }
        jQuery("input[name='s']").select();
    }

    function changeSearch() {
        if (jQuery("input[name='s']").attr("value") == emptyText) {
            jQuery("input[name='s']").addClass("gray");
        } else {
            jQuery("input[name='s']").removeClass("gray");
        }
    }

    function blurSearch() {
        jQuery("input[name='s']").addClass("gray");
        if (jQuery("input[name='s']").attr("value") == '') {
            jQuery("input[name='s']").attr("value", emptyText);
        }
    }

</script>
<!--
<input type="text" name="s" value="Search Perkins Eastman" class="searchString gray" onfocus="javascript: focusSearch();" onchange="javascript: changeSearch();" onblur="javascript: blurSearch();" />
<input type="hidden" name="submit" value="1" />
<input type="image" src="./images/search.gif" width="56" class="searchButton" />
-->
</div>
</div>



<!-- remove footerMenu innerText to get rid of W3C errors on default.aspx-->
<div id="footerMenu" runat="server">
<div id="subDrop">
<div>
<!-- Vertical Menu --> 
<div id="smoothmenu1" class="menu"> 
<ul class="short"> 
<!-- Adding class to specify height of the menu on different pages --> 
<li> <a href="firmProfile.aspx">Firm Profile</a> </li> 
<li> <a href="designPhilosophy.aspx">Design Approach</a> </li> 
<li> <a href="leadership.aspx">Leadership</a> </li> 
<li> <a href="awards.aspx">Honors and Awards</a> </li> 
<li> <a href="eekArchitects.aspx">EE&amp;K</a> </li> 
</ul> 
</div> 

<!-- Vertical Menu -->
<div id="smoothmenu2" class="menu"> 
<ul class="long"> 
<!-- Adding class to specify height of the menu on different pages --> 
<li> <a href="projects.aspx?list=2400012">Corporate Interiors</a> </li> 
<li> <a >Education</a> 
<ul> 
<li> <a href="projects.aspx?list=3409328">Campus Planning</a> </li> 
<li> <a href="projects.aspx?list=2400014">Higher Education</a> </li> 
<li> <a href="projects.aspx?list=2400017">K-12 Education</a> </li> 
<li> <a href="projects.aspx?list=3409341" class="no_border">Town and Gown</a> </li> 
</ul> </li> 
<li> <a href="projects.aspx?list=2400024">Healthcare</a> </li> 
<li> <a href="projects.aspx?list=2400015">Hospitality</a> </li> 
<li> <a href="projects.aspx?list=2400016">Housing</a> </li> 
<li> <a href="projects.aspx?list=2400018">Office and Retail</a> </li> 
<li> <a href="projects.aspx?list=2400019">Public | Cultural </a> </li> 
<li> <a href="projects.aspx?list=2400020">Science | Technology</a> </li> 
<li> <a href="projects.aspx?list=2400021">Senior Living</a> </li> 
<li> <a>Urbanism </a><ul> 
<li> <a href="projects.aspx?list=3409345">Downtown Redevelopment</a> </li> 
<li> <a href="projects.aspx?list=3409347">New Town Planning</a> </li>
<li> <a href="projects.aspx?list=3409349">Transit and Development</a></li>  
<li> <a href="projects.aspx?list=2400022">Urban Design</a> </li> 
<li> <a href="projects.aspx?list=3409351" class="no_border">Waterfront</a> </li> 
</ul> </li> </ul>
</div>

<!-- Vertical Menu -->

<div id="smoothmenu3" class="menu"> 

<ul class="short title_push_bot"> 
<!-- Adding class to specify height of the menu on different pages --> 
<li> <a href="/RegionsMap.aspx">Regions Map</a> </li> 
<li> <a href="/region.aspx?name=Africa and the Middle East">Africa and the Middle East</a> </li> 
<li> <a href="/region.aspx?name=Asia">Asia</a> </li>
<li> <a href="/region.aspx?name=China">China</a> </li> 
<li> <a href="/region.aspx?name=Europe">Europe</a> </li> 
<li> <a href="/region.aspx?name=The Americas">The Americas</a> </li> 
</ul>
</div>

<!-- Vertical Menu --> 

<div id="smoothmenu4" class="menu"> 
<ul class="short title_push_bot"> 
<!-- Adding class to specify height of the menu on different pages --> 
<li> <a href="views.aspx">Point of View</a> </li> 
<li> <a href="publications.aspx">Publications</a> </li> 
</ul> 
</div> 
</div>
</div>
</div>



<script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-6618817-2");
        pageTracker._trackPageview();
    } catch (err) { }
</script>
