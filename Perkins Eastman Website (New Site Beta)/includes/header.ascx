﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="header.ascx.vb" Inherits="includes_header" %>
<!--Website Design by Katy Gillen of Perkins Eastman-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="perkins, perkins eastman, architect, architects, architecture, zearalenone, consulting, interior design, planning, programming, architectural, interiors, urban design, strategic planning, master planning, arlington architect, charlotte architect, chicago architect, dubai architect, new york architect, oakland architect, pittsburgh architect, san francisco architect, shanghai architect, stamford architect, toronto architect, washington dc architect, brooklyn architect, queens architect, bronx architect, staten island architect, manhattan architect, aaron schwarz, brad perkins, bradford perkins, david hoglund, jonathan stark, mary-jean eastman, faia, academic medical center design, academic medical center planning, academic medical center programming, active adult communities, active adult lifestyle, acute care, adult care homes, adult day care, age-associated memory impairment, aging in place, alzheimer's disease, ambulatory facility design, ambulatory facility planning, ambulatory facility programming, assisted living, benchmarking, branding, cancer care  architect, cancer care design, cancer care planning, cancer care programming, ccrc architect, ccrc architect, ccrc design, ccrc design, ccrc planning, ccrc planning, ccrc programming, ccrc programming, churn management, civic architect, civic design, club architect , club design, club interiors , club planning, club programming, club renovations, community hospital design, community hospital planning , community hospital programming, community redevelopment, congregate care, continuing care retirement community, ccrc, continuum of care, corporate design, corporate environments, corporate headquarters, corporate interiors, corporate programming, corporate restack, country club architect , country club design, country club design, country club interiors , country club planning, country club programming, country club renovations, court architect, court design, court planning, court programming, courthouse architect, courthouse design, courthouse planning, courthouse programming, dementia, design excellence, education architect, education design, education master planning, education planning, education programming, emergency department design, emergency department planning, emergency department programming, geriatric medicine, gerontology, green architecture, green design, health care architect, health care design, health care planning, health care programming, healthcare architect, healthcare design, healthcare environments , healthcare master planning, healthcare planning, healthcare programming, higher education architect, higher education design, higher education master planning, higher education planning, higher education programming, home health care, hospice, hospitality architect , hospitality design , hospitality interiors , hospitality planning, hospitality programming, hotel architect, hotel design, hotel planning, hotel programming, independent living facility, inpatient  facility design, inpatient facility planning, inpatient facility programming, institutional architect, institutional design, institutional design, institutional interiors , institutional planning, institutional programming, institutional renovations, intermediate care facility, international retail, judicial architect, judicial design, judicial programming, justice architect, justice design, k-12 education, k-12 education architect, k-12 education design, k-12 education master planning, k-12 education planning, k-12 education programming, Laboratory design, Laboratory programming, leed architect, leed design, life care, lifestyle center architect, lifestyle center design, long term care, memory support facility, mixed-use architect, mixed-use design, mixed-use master planning, naturally occurring retirement communities, norcs, new urbanism, oncology planning, oncology programming, pacu architect, pacu design, pacu planning, pacu programming, pediatric facility design, pediatric facility planning, pediatric facility programming, personal care, research facility design, residential care, resort master planning, resort planning , respite care, retail architect, retail design, retirement communities, retirement living, school architect, school design, school planning, school programming, science facility design, senior housing developments, senior living communities, seniors architect, seniors architect, seniors design, seniors design, seniors planning, seniors planning, seniors programming, seniors programming, shopping center architect, shopping center design, skilled nursing care, sub acute care, surgery design, surgery planning, sustainable architecture, sustainable design, urban redevelopment, wellness facilities, workplace environments" />

<link href="css/peapc.css" rel="stylesheet" type="text/css" />
<!--
<script type="text/javascript" src="scripts/jquery-1.3.1.min.js"></script>
-->
<script type="text/javascript">
	//var $j = jQuery.noConflict();
</script>
<link href="css/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />

<link type="text/css" rel="stylesheet" href="assets/css/reset.css" />
<link type="text/css" rel="stylesheet" href="assets/css/menu.css" />
<link type="text/css" rel="stylesheet" href="assets/css/style.css" />
<script src="assets/js/jq.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/fancybox/jq.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="assets/fancybox/jq.fancybox.css" media="screen" />
<script src="assets/js/js.js" type="text/javascript" defer="defer"></script>
<script src="assets/js/menu.js" type="text/javascript"></script>
<script type="text/javascript">
// <![CDATA[
document.write('<style type="text/css">.main{visibility: hidden;}</style>');
// ]]>
</script>
<script type="text/javascript" language="javascript">

<!--

//Disable right mouse click Script
//By Maximus (maximus@nsimail.com) w/ mods by DynamicDrive
//For full source code, visit http://www.dynamicdrive.com

//var message="All photographs are copyrighted material licensed to Perkins Eastman. Any other use is not permitted without written consent. Please contact Webmaster.";

///////////////////////////////////
/*
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}

function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}

if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}

document.oncontextmenu=new Function("alert(message);return false")
*/
// --> 
</script>
<script src="scripts/SpryMenuBar.js?v=2" type="text/javascript"></script>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/

#MenuBar5 li {
	width: auto;
	white-space: nowrap;
}
#MenuBar5 ul {
	width: auto;
}
#MenuBar5 ul li {
	float: none;
	background-color: transparent;
}
#MenuBar5 a.MenuBarItemSubmenu {
	background-position: 100% 50%;
}
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>