<cfscript>
	username = "webuser";
	password = "let4me$in";
	firmid = "2";

	webService = CreateObject("webservice", "http://services.cosential.com/external/wde.cfc?WSDL");
	json = CreateObject("component", "json");	
</cfscript>
<cffunction name="stripHTML">
	<cfargument name="data" required="yes">
    <cfargument name="repWith" reguired="no" default="">
    <cfscript>
		if (NOT IsDefined('repWith')) repWith = "";
		retval = data;
		retval = ReReplaceNoCase(retval, "<p.*?>", "", "ALL");
		retval = ReReplaceNoCase(retval, "<div.*?>", "", "ALL");
		retval = ReReplaceNoCase(retval, "</p.*?>", repWith, "ALL");
		retval = ReReplaceNoCase(retval, "</div.*?>", repWith, "ALL");
		return retval;
	</cfscript>
</cffunction>
<cffunction name="stripAllHTML">
	<cfargument name="data" required="yes">
    <cfscript>
		retval = data;
		retval = ReReplaceNoCase(retval, "<.*?>", "", "ALL");
		return retval;
	</cfscript>
</cffunction>