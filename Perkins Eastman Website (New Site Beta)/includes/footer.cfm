</div>
<div class="footer" align="center"><br />Copyright &copy; 2009 Perkins Eastman Architects PC. All rights reserved. Contact <a href="mailto:s.yates@perkinseastman.com">Webmaster</a>. Last Updated: 05.07.2009
<cfoutput>
</cfoutput>&nbsp;<a href="rssFeeds.cfm">RSS Feed</a>.&nbsp;<a href="rssFeeds.cfm"><img src="./images/rss_feed.gif" border="0" /></a>&nbsp;Follow us on  <a href="http://twitter.com/PerkinsEastman" target="_blank">Twitter</a>.</div>
<cfparam name="session.searchString" default="Search Perkins Eastman">
<cfif isDefined('url.s')><cfset session.searchString = url.s></cfif>
<script type="text/javascript">
	var emptyText = "Search Perkins Eastman";

	function focusSearch() {
		jQuery("input[name='s']").removeClass("gray");
		if (jQuery("input[name='s']").attr("value") == emptyText) {
			jQuery("input[name='s']").attr("value", '');
		}
		jQuery("input[name='s']").select();
	}
	
	function changeSearch() {
		if (jQuery("input[name='s']").attr("value") == emptyText) {
			jQuery("input[name='s']").addClass("gray");				
		} else {
			jQuery("input[name='s']").removeClass("gray");
		}
	}
	
	function blurSearch() {
		jQuery("input[name='s']").addClass("gray");
		if (jQuery("input[name='s']").attr("value") == '') {
			jQuery("input[name='s']").attr("value", emptyText);
		}
	}

</script>
<form action="searchResults.cfm" method="get">
<cfoutput><input type="text" name="s" value="#session.searchString#" class="searchString gray" onfocus="javascript: focusSearch();" onchange="javascript: changeSearch();" onblur="javascript: blurSearch();" /></cfoutput>
<input type="image" src="./images/search.gif" width="56" class="searchButton" />
</form>
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-6618817-2");
pageTracker._trackPageview();
} catch(err) {}
</script>
</body>
</html>