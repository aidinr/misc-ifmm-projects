﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="projects.aspx.vb" Inherits="projects" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="literalPageTitle" runat="server"></asp:Literal></title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
   
</head>
<body class="pe2">
    <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    <form id="form1" runat="server">
        <div id="ContentBody" style="display: none;"><asp:Literal ID="literalDescription" runat="server"></asp:Literal></div>
        <div id="ClientListContent" style="display: none;">Loading...</div>
        <div class="breadcrumbs"><a href="/" class="active">Home</a> | <a href="/#linkPage2"  class="active">Projects</a> | <asp:Literal ID="literalName2" runat="server"></asp:Literal></div>
        <div class="head_project"><asp:Literal ID="literalName" runat="server"></asp:Literal></div>
        
        <div class="project_details">
            <asp:Image runat="server" ID="featuredImage" CssClass="project_big_image" />
        	<p class="project_description"><asp:Literal ID="literalDescription2" runat="server"></asp:Literal></p>
        	<asp:Literal ID="literalBrowseClient" runat="server" Text="<p>&nbsp;</p>"></asp:Literal>
        	
            <p id="viewProjectText" runat="server">View selected projects below</p>
           
        </div>
        <div style="margin: 0px 0px 0px 0px;">
        
        <asp:Repeater ID="repeaterProjects" runat="server" OnItemDataBound="RepeaterProjects_ItemDataBound">
            <ItemTemplate>
                <span style="float: left; margin: 0px 0px 0px 15px;">
    	        <div class="project_rollover_caption" id="projectName<%#Container.itemIndex %>">&nbsp;</div>
                <a id="projectDetailsLink" runat="server" href=""><img class="project_thumb_image" id="projectImage<%#Container.itemIndex %>" src="<%=Session("WSRetrieveAsset")%>crop=1&type=project&size=1&width=60&height=60&id=<%#container.dataitem("projectid")%>" alt="" /></a>
            </span>
            
                <script type="text/javascript">
                    jQuery("#projectImage<%#Container.itemIndex %>").mouseover(function() {
                    jQuery("#projectName<%#Container.itemIndex %>").html('<div><%#container.dataitem("item_value").tostring.replace("'","\'")%></div>');
                    });
                    jQuery("#projectImage<%#Container.itemIndex %>").mouseout(function() {
                    jQuery("#projectName<%#Container.itemIndex %>").html("&nbsp;");
                    });
	            </script>
            </ItemTemplate>
        </asp:Repeater>

          

        </div>
        <div style="height: 15px; margin: 0px;">&nbsp;</div>
    <%if request.querystring("list") = "2400103" then%>
    <script src="scripts/international.js" type="text/javascript"></script>
    <%end if%>

  <script type="text/javascript">
      jQuery("#ClientList").click(function() {
          var data = "";
          if (jQuery("#ClientList").html() == 'Browse our client list') {
              data = '<asp:literal id="literalClientList" runat="server"></asp:literal>';
              jQuery("#ClientListContent").html(data);
              jQuery(".project_description").html(jQuery("#ClientListContent").html());
              jQuery("#mapPlus").css('display', 'none');
              jQuery("#ClientList").html('Hide the client list');
          } else {
              jQuery(".project_description").html(jQuery("#ContentBody").html());
              jQuery("#mapPlus").css('display', 'block');
              jQuery("#ClientList").html('Browse our client list');
          }
      });

            </script>
    
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
