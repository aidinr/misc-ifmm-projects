﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class RegionsMap
    Inherits System.Web.UI.Page
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)

        DisplayProjects()


    End Sub

    Sub DisplayProjects()

        literalDescription.Text = "REGIONS MAP"
        'literalDescription2.Text = "REGIONS MAP"
        'literalBrowseClient.Text = "<p id=""ClientList"">Browse our client list</p>"
        'literalClientList.Text = "literalClientList"
        literalName.Text = "REGIONS MAP"
        literalName2.Text = "REGIONS MAP"
        literalPageTitle.Text = "Perkins Eastman: " & "Regions Served by Perkins Eastman International"

        Dim sql As String = "Select v.Item_Value from IPM_PROJECT p Join IPM_PROJECT_FIELD_VALUE v on p.ProjectID = v.ProjectID Join IPM_PROJECT_FIELD_DESC d on d.Item_ID = v.Item_ID and d.Item_Tag = 'IDAM_REGIONS_MAP_TEXT' where p.Name = 'Public Web Site Content'"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim dt1 As DataTable = New DataTable
        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then
            If (Not IsDBNull(dt1.Rows(0)("Item_Value"))) Then
                MapText.InnerHtml = dt1.Rows(0)("Item_Value")
            End If
        End If


        'featuredImage.ImageUrl = Session("WSRetrieveAsset") & "type=asset&size=1&width=600&height=480&id=" & dt3.Rows(0)("asset_id")



    End Sub

End Class