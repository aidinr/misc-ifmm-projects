<%@ Control Language="C#" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<script runat="server">
  protected void Page_Load(object sender, EventArgs e)
  {
    ComboBoxAlignment.DropImageUrl = PrefixWithSkinFolderLocation(ComboBoxAlignment.DropImageUrl);
    ComboBoxAlignment.DropHoverImageUrl = PrefixWithSkinFolderLocation(ComboBoxAlignment.DropHoverImageUrl);
  }
  private string PrefixWithSkinFolderLocation(string str)
  {
    string prefix = this.Attributes["SkinFolderLocation"];
    return str.IndexOf(prefix) == 0 ? str : prefix + "/" + str;
  }
</script>

<ComponentArt:Dialog
  ID="MediaDialog"
  RunAt="server"
  AllowDrag="true"
  AllowResize="false"
  Modal="false"
  Alignment="MiddleCentre"
  Width="600"
  Height="298"
  ContentCssClass="dlg-media">
  <ClientEvents>
    <OnShow EventHandler="MediaDialog_OnShow" />
    <OnClose EventHandler="MediaDialog_OnClose" />
  </ClientEvents>
  <Content>

    <div class="dlg-title draggable" onmousedown="<%=MediaDialog.ClientID %>.StartDrag(event);">
      <div class="left"></div>
      <div class="mid">
        <a class="close" href="javascript:void(0);" onclick="<%=MediaDialog.ClientID %>.close();this.blur();return false;"></a>
        <span>Insert / Edit Media</span>
      </div>
      <div class="right"></div>
    </div>

    <div class="dlg-tabstrip">
      <div class="left"></div>
      <div class="tabs">
        <a href="javascript:void(0);" class="tab-selected" onclick="toggle_dialog_tab(this,<%=MediaDialog.ClientID %>,0);this.blur();"><span class="image">Image</span></a>
      </div>
      <div class="right"></div>
    </div>

    <div class="dlg-content">
      <div class="left image"></div>
      <div class="mid image">

        <ComponentArt:MultiPage
          ID="MediaPages"
          RunAt="server">
          
          <ComponentArt:PageView RunAt="server" id="Image">
            <div class="content" style="width:375px;height:216px;float:left;">
              <div class="row top" title="Required"><span class="label">Image URL:</span><input id="<%=this.ClientID %>_image_url" type="text" class="image-url" onkeypress="toggle_preview_button(this);" onfocus="toggle_preview_button(this);" onblur="toggle_preview_button(this);" /><a href="javascript:void(0);" onclick="load_image(this,<%=MediaDialog.ClientID %>);this.blur();" class="preview-url-disabled" title="Preview this image"></a></div>
              <div class="row" title="Required"><span class="label">Image Description:</span><input id="<%=this.ClientID %>_image_desc"  type="text" class="image-description" /></div>
              <div class="row" title="Required">
                <span class="label">Width:</span><input type="text" id="<%=this.ClientID %>_image_width" class="image-width" />
                <span class="label-narrow">Height:</span><input id="<%=this.ClientID %>_image_height" type="text" class="image-height float-left" />
              </div>
              <div class="row" title="Optional">
                <span class="label">Alignment:</span>
                <span class="combo">

                  <ComponentArt:ComboBox
                    ID="ComboBoxAlignment"
                    RunAt="server"
                    Width="104"
                    Height="18"
                    ItemCssClass="simple-item"
                    ItemHoverCssClass="simple-item-hover"
                    CssClass="dlg-combobox"
                    TextBoxCssClass="dlg-textfield default-cursor"
                    DropImageUrl="images/dialog/dropdown.png"
                    DropHoverImageUrl="images/dialog/dropdown-hover.png"
                    KeyboardEnabled="false"
                    TextBoxEnabled="false"
                    DropDownResizingMode="bottom"
                    DropDownWidth="190"
                    DropDownHeight="160"
                    DropDownCssClass="simple"
                    DropDownContentCssClass="simple-content"
                    SelectedIndex="0">
                    <DropDownHeader>
                      <div class="menu-header"></div>
                    </DropDownHeader>
                    <DropDownFooter>
                      <div class="menu-footer"></div>
                    </DropDownFooter>
                    <Items>
                      <ComponentArt:ComboBoxItem Text="(Default)" Value="" />
                      <ComponentArt:ComboBoxItem Text="Baseline" Value="baseline" />
                      <ComponentArt:ComboBoxItem Text="Top" Value="top" />
                      <ComponentArt:ComboBoxItem Text="Middle" Value="middle" />
                      <ComponentArt:ComboBoxItem Text="Bottom" Value="bottom" />
                      <ComponentArt:ComboBoxItem Text="TextTop" Value="texttop" />
                      <ComponentArt:ComboBoxItem Text="Absolute middle" Value="absmiddle" />
                      <ComponentArt:ComboBoxItem Text="Absolute bottom" Value="absbottom" />
                      <ComponentArt:ComboBoxItem Text="Left" Value="left" />
                      <ComponentArt:ComboBoxItem Text="Right" Value="right" />
                    </Items>
                  </ComponentArt:ComboBox>

                </span>
                <span class="label-narrow">Border:</span><input id="<%=this.ClientID %>_image_border" type="text" class="image-border float-left" />
              </div>
              <div class="hr"><span style="display:none;">.</span></div>
              <div class="row" title="Optional">
                <span class="label">Horizontal Spacing:</span><input id="<%=this.ClientID %>_image_hspace"  type="text" class="image-hspace" />
                <span class="label">Vertical Spacing:</span><input  id="<%=this.ClientID %>_image_vspace" type="text" class="image-vspace float-left" />
              </div>
              <div class="row" title="Optional"><span class="label">CSS Class:</span><input id="<%=this.ClientID %>_image_cssclass" type="text" class="image-class" /></div>
              <div class="row" title="Optional"><span class="label">Inline CSS:</span><input id="<%=this.ClientID %>_image_inline" type="text" class="image-inline" /></div>
            </div>
            <div class="image-preview">
              <div class="status">Preview</div>
              <div class="thumbnail"></div>
            </div>
          </ComponentArt:PageView>

          <ComponentArt:PageView RunAt="server">
            <div style="width:582px;line-height:216px;text-align:center;">Flash: Coming soon</div>
          </ComponentArt:PageView>

          <ComponentArt:PageView RunAt="server">
            <div style="width:582px;line-height:216px;text-align:center;">Video: Coming soon</div>
          </ComponentArt:PageView>

        </ComponentArt:MultiPage>

      </div>
      <div class="right image"></div>
    </div>
    <div class="dlg-buttons">
      <div class="left"></div>
      <div class="mid">
        <a onclick="<%=MediaDialog.ClientID %>.close(true);this.blur();return false;" href="javascript:void(0);" class="button-70 float-right"><span>Insert</span></a>
        <a onclick="<%=MediaDialog.ClientID %>.close();this.blur();return false;" href="javascript:void(0);" class="button-70 float-right first"><span>Close</span></a>
      </div>
      <div class="right"></div>
    </div>

  </Content>
</ComponentArt:Dialog>

<script type="text/javascript">

<%=MediaDialog.ClientID %>.ParentControlID = "<%=this.ClientID %>";

</script>