<%@ Control Language="C#" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<script runat="server">
  protected void Page_Load(object sender, EventArgs e)
  {
    ComboBoxFontFace.DropImageUrl = PrefixWithSkinFolderLocation(ComboBoxFontFace.DropImageUrl);
    ComboBoxFontFace.DropHoverImageUrl = PrefixWithSkinFolderLocation(ComboBoxFontFace.DropHoverImageUrl);
  }
  private string PrefixWithSkinFolderLocation(string str)
  {
    string prefix = this.Attributes["SkinFolderLocation"];
    return str.IndexOf(prefix) == 0 ? str : prefix + "/" + str;
  }
</script>

<ComponentArt:ComboBox
	ID="ComboBoxFontFace"
	RunAt="server"
	Width="72"
	Height="22"
	ItemClientTemplateId="FontFaceItemTemplate"
	ItemCssClass="mnu-item"
	ItemHoverCssClass="mnu-item-h"
	CssClass="cmb"
	HoverCssClass="cmb-h"
	FocusedCssClass="cmb-h"
	TextBoxCssClass="cmb-txt"
	TextBoxHoverCssClass="cmb-txt"
	DropImageUrl="images/editor/ddn.png"
	DropHoverImageUrl="images/editor/ddn-h.png"
	KeyboardEnabled="false"
	TextBoxEnabled="false"
	DropDownResizingMode="bottom"
	DropDownWidth="190"
	DropDownHeight="160"
	DropDownCssClass="mnu mnu-font"
	DropDownContentCssClass="mnu-con"
	SelectedIndex="6"
>

	<DropDownHeader>
		<div class="mnu-hdr"></div>
	</DropDownHeader>

	<DropDownFooter>
		<div class="mnu-ftr"></div>
	</DropDownFooter>

	<Items>
		<ComponentArt:ComboBoxItem Text="Arial" Value="arial" />
		<ComponentArt:ComboBoxItem Text="Arial Black" Value="'arial black'" />
		<ComponentArt:ComboBoxItem Text="Comic Sans MS" Value="'comic sans ms'" />
		<ComponentArt:ComboBoxItem Text="Courier New" Value="courier new" />
		<ComponentArt:ComboBoxItem Text="Garamond" Value="garamond" />
		<ComponentArt:ComboBoxItem Text="Georgia" Value="georgia" />
		<ComponentArt:ComboBoxItem Text="Tahoma" Value="tahoma" />
		<ComponentArt:ComboBoxItem Text="Times New Roman" Value="'times new roman'" />
		<ComponentArt:ComboBoxItem Text="Trebuchet MS" Value="'trebuchet ms'" />
		<ComponentArt:ComboBoxItem Text="Verdana" Value="verdana" />
	</Items>

	<ClientTemplates>
		<ComponentArt:ClientTemplate ID="FontFaceItemTemplate">
			<div>
				<span class="ico"></span>
				<span style="float:left;font-family:## DataItem.get_value() ##;font-size:11px;vertical-align:middle;line-height:22px;font-weight:normal;padding:0 0 0 12px;">## DataItem.get_text() ##</span>
			</div>
		</ComponentArt:ClientTemplate>
	</ClientTemplates>
</ComponentArt:ComboBox>