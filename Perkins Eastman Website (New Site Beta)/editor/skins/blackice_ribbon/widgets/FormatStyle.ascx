<%@ Control Language="C#" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<script runat="server">
  protected void Page_Load(object sender, EventArgs e)
  {
    ComboBoxStyle.DropImageUrl = PrefixWithSkinFolderLocation(ComboBoxStyle.DropImageUrl);
    ComboBoxStyle.DropHoverImageUrl = PrefixWithSkinFolderLocation(ComboBoxStyle.DropHoverImageUrl);
  }
  private string PrefixWithSkinFolderLocation(string str)
  {
    string prefix = this.Attributes["SkinFolderLocation"];
    return str.IndexOf(prefix) == 0 ? str : prefix + "/" + str;
  }
</script>

<ComponentArt:ComboBox
	ID="ComboBoxStyle"
	RunAt="server"
	AutoFilter="false"
	Width="72"
	Height="22"
	ItemCssClass="mnu-item"
	ItemHoverCssClass="mnu-item-h"
	CssClass="cmb"
	HoverCssClass="cmb-h"
	FocusedCssClass="cmb-h"
	TextBoxCssClass="cmb-txt"
	TextBoxHoverCssClass="cmb-txt"
	DropImageUrl="images/editor/ddn.png"
	DropHoverImageUrl="images/editor/ddn-h.png"
	KeyboardEnabled="false"
	TextBoxEnabled="false"
	DropDownResizingMode="bottom"
	DropDownWidth="189"
	DropDownHeight="160"
	DropDownCssClass="mnu mnu-style"
	DropDownContentCssClass="mnu-con"
	SelectedIndex="2"
>

	<DropDownHeader>
		<div class="mnu-hdr"></div>
	</DropDownHeader>

	<DropDownFooter>
		<div class="mnu-ftr"></div>
	</DropDownFooter>

	<Items>
		<ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Normal" Value="Normal" />
		<ComponentArt:ComboBoxItem Enabled="false" CssClass="mnu-break" />
		<ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Heading 1" Value="Heading 1" />
		<ComponentArt:ComboBoxItem Enabled="false" CssClass="mnu-break" />
		<ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Heading 2" Value="Heading 2" />
		<ComponentArt:ComboBoxItem Enabled="false" CssClass="mnu-break" />
		<ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Heading 3" Value="Heading 3" />
		<ComponentArt:ComboBoxItem Enabled="false" CssClass="mnu-break" />
		<ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Bold" Value="Bold" />
		<ComponentArt:ComboBoxItem Enabled="false" CssClass="mnu-break" />
		<ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Italic" Value="Italic" />
		<ComponentArt:ComboBoxItem Enabled="false" CssClass="mnu-break" />
		<ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Bold + Italic" Value="Bold + Italic" />
	</Items>

	<ClientTemplates>
		<ComponentArt:ClientTemplate ID="StyleItemTemplate">## StyleDropDownItemHTML(Parent.ParentEditor,DataItem) ##</a></ComponentArt:ClientTemplate>
	</ClientTemplates>
</ComponentArt:ComboBox>
