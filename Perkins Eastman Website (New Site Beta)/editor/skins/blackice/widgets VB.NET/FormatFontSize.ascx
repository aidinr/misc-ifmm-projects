<%@ Control Language="VB" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<script runat="server">

Sub Page_Load(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load  
        ComboBoxFontSize.DropImageUrl = PrefixWithSkinFolderLocation(ComboBoxFontSize.DropImageUrl)
        ComboBoxFontSize.DropHoverImageUrl = PrefixWithSkinFolderLocation(ComboBoxFontSize.DropHoverImageUrl)
End Sub

Private Function PrefixWithSkinFolderLocation(ByVal str As String) As String
    Dim prefix As String
    prefix = Me.Attributes("SkinFolderLocation")
    If str.IndexOf(prefix) = 0 Then
        Return str
    Else
        Return prefix & "/" & str
    End If
End Function

</script>

<ComponentArt:ComboBox
  ID="ComboBoxFontSize"
  RunAt="server"
  Width="36"
  Height="18"
  ItemCssClass="size-item"
  ItemHoverCssClass="size-item-hover"
  CssClass="combobox"
  HoverCssClass="combobox-hover"
  FocusedCssClass="combobox-hover"
  TextBoxCssClass="combobox-textfield"
  TextBoxHoverCssClass="combobox-textfield-hover"
  DropImageUrl="images/editor/dropdown.png"
  DropHoverImageUrl="images/editor/dropdown-hover.png"
  KeyboardEnabled="false"
  TextBoxEnabled="false"
  DropDownResizingMode="bottom"
  DropDownWidth="64"
  DropDownHeight="160"
  DropDownCssClass="menu"
  DropDownContentCssClass="size-content"
  SelectedIndex="4">
  
  <DropDownHeader>
    <div class="size-header"></div>
  </DropDownHeader>

  <DropDownFooter>
    <div class="size-footer"></div>
  </DropDownFooter>

  <Items>
    <ComponentArt:ComboBoxItem Text="8" Value="8px" />
    <ComponentArt:ComboBoxItem Text="9" Value="9px" />
    <ComponentArt:ComboBoxItem Text="10" Value="10px" />
    <ComponentArt:ComboBoxItem Text="11" Value="11px" />
    <ComponentArt:ComboBoxItem Text="12" Value="12px" />
    <ComponentArt:ComboBoxItem Text="14" Value="14px" />
    <ComponentArt:ComboBoxItem Text="16" Value="16px" />
    <ComponentArt:ComboBoxItem Text="18" Value="18px" />
    <ComponentArt:ComboBoxItem Text="20" Value="20px" />
    <ComponentArt:ComboBoxItem Text="22" Value="22px" />
    <ComponentArt:ComboBoxItem Text="24" Value="24px" />
    <ComponentArt:ComboBoxItem Text="26" Value="26px" />
    <ComponentArt:ComboBoxItem Text="28" Value="28px" />
    <ComponentArt:ComboBoxItem Text="36" Value="36px" />
    <ComponentArt:ComboBoxItem Text="48" Value="48px" />
    <ComponentArt:ComboBoxItem Text="72" Value="72px" />
  </Items>
</ComponentArt:ComboBox>