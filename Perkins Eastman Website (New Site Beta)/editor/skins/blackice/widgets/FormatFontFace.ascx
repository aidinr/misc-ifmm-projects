<%@ Control Language="C#" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<script runat="server">
  protected void Page_Load(object sender, EventArgs e)
  {
    ComboBoxFontFace.DropImageUrl = PrefixWithSkinFolderLocation(ComboBoxFontFace.DropImageUrl);
    ComboBoxFontFace.DropHoverImageUrl = PrefixWithSkinFolderLocation(ComboBoxFontFace.DropHoverImageUrl);
  }
  private string PrefixWithSkinFolderLocation(string str)
  {
    string prefix = this.Attributes["SkinFolderLocation"];
    return str.IndexOf(prefix) == 0 ? str : prefix + "/" + str;
  }
</script>

<ComponentArt:ComboBox
  ID="ComboBoxFontFace"
  RunAt="server"
  Width="120"
  Height="18"
  ItemClientTemplateId="FontFaceItemTemplate"
  ItemCssClass="menu-item"
  ItemHoverCssClass="menu-item-hover"
  CssClass="combobox"
  HoverCssClass="combobox-hover"
  FocusedCssClass="combobox-hover"
  TextBoxCssClass="combobox-textfield"
  TextBoxHoverCssClass="combobox-textfield-hover"
  DropImageUrl="images/editor/dropdown.png"
  DropHoverImageUrl="images/editor/dropdown-hover.png"
  KeyboardEnabled="false"
  TextBoxEnabled="false"
  DropDownResizingMode="bottom"
  DropDownWidth="190"
  DropDownHeight="160"
  DropDownCssClass="menu"
  DropDownContentCssClass="menu-content"
  SelectedIndex="6">
  
  <DropDownHeader>
    <div class="menu-header"></div>
  </DropDownHeader>

  <DropDownFooter>
    <div class="menu-footer"></div>
  </DropDownFooter>

  <Items>
    <ComponentArt:ComboBoxItem Text="Arial" Value="arial" />
    <ComponentArt:ComboBoxItem Text="Arial Black" Value="'arial black'" />
    <ComponentArt:ComboBoxItem Text="Comic Sans MS" Value="'comic sans ms'" />
    <ComponentArt:ComboBoxItem Text="Courier New" Value="courier new" />
    <ComponentArt:ComboBoxItem Text="Garamond" Value="garamond" />
    <ComponentArt:ComboBoxItem Text="Georgia" Value="georgia" />
    <ComponentArt:ComboBoxItem Text="Tahoma" Value="tahoma" />
    <ComponentArt:ComboBoxItem Text="Times New Roman" Value="'times new roman'" />
    <ComponentArt:ComboBoxItem Text="Trebuchet MS" Value="'trebuchet ms'" />
    <ComponentArt:ComboBoxItem Text="Verdana" Value="verdana" />
  </Items>

  <ClientTemplates>
    <ComponentArt:ClientTemplate ID="FontFaceItemTemplate">
      <img alt="" src="## Parent.ParentEditor.SkinFolderLocation ##/images/menus/icon-font.png" width="22" height="24" class="menu-icon" />
      <span style="float:left;font-family:## DataItem.get_value() ##;font-size:11px;vertical-align:middle;line-height:22px;font-weight:normal;">## DataItem.get_text() ##</span>
    </ComponentArt:ClientTemplate>
  </ClientTemplates>
</ComponentArt:ComboBox>