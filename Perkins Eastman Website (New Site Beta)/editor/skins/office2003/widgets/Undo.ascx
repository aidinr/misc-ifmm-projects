<%@ Control Language="C#" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<ComponentArt:Menu
	ID="UndoMenu"
	RunAt="server"
	ContextMenu="custom"
	Orientation="Vertical"
	CssClass="mnu-undo"
	ExpandDuration="0"
	CollapseDuration="0"
	TopGroupExpandDirection="BelowLeft"
	TopGroupExpandOffsetX="1"
	TopGroupExpandOffsetY="-3"
	ShadowEnabled="false"
>
	<Items>
		<ComponentArt:MenuItem ClientTemplateId="UndoTemplate" />
	</Items>
	<ClientTemplates>
		<ComponentArt:ClientTemplate ID="UndoTemplate">
			<span>Undo</span>
			<div class="list">## UndoMenuHtml(Parent); ##</div>
		</ComponentArt:ClientTemplate>
	</ClientTemplates>
</ComponentArt:Menu>

