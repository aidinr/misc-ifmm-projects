<%@ Control Language="C#" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<script runat="server">
  protected void Page_Load(object sender, EventArgs e)
  {
    ComboBoxStyle.DropImageUrl = PrefixWithSkinFolderLocation(ComboBoxStyle.DropImageUrl);
    ComboBoxStyle.DropHoverImageUrl = PrefixWithSkinFolderLocation(ComboBoxStyle.DropHoverImageUrl);
  }
  private string PrefixWithSkinFolderLocation(string str)
  {
    string prefix = this.Attributes["SkinFolderLocation"];
    return str.IndexOf(prefix) == 0 ? str : prefix + "/" + str;
  }
</script>

<ComponentArt:ComboBox
	ID="ComboBoxStyle"
	RunAt="server"
	AutoFilter="false"
	Width="83"
	Height="18"
	ItemCssClass="itm"
	ItemHoverCssClass="itm-h"
	CssClass="cmb"
	HoverCssClass="cmb-h"
	FocusedCssClass="cmb-h"
	TextBoxCssClass="txt"
	DropImageUrl="images/combobox/ddn.png"
	DropHoverImageUrl="images/combobox/ddn-h.png"
	KeyboardEnabled="false"
	TextBoxEnabled="false"
	DropDownResizingMode="bottom"
	DropDownWidth="190"
	DropDownHeight="160"
	DropDownCssClass="ddn"
	DropDownContentCssClass="ddn-style"
	SelectedIndex="2"
>
	<DropDownFooter><div class="ddn-ftr"></div></DropDownFooter>

	<Items>
		<ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Normal" Value="Normal" />
		<ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Heading 1" Value="Heading 1" />
		<ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Heading 2" Value="Heading 2" />
		<ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Heading 3" Value="Heading 3" />
		<ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Bold" Value="Bold" />
		<ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Italic" Value="Italic" />
		<ComponentArt:ComboBoxItem ClientTemplateId="StyleItemTemplate" Text="Bold + Italic" Value="Bold + Italic" />
	</Items>

	<ClientTemplates>
		<ComponentArt:ClientTemplate ID="StyleItemTemplate"><div style="height:34px;line-height:34px;margin-left:-30px !important">## StyleDropDownItemHTML(Parent.ParentEditor,DataItem) ##</div></ComponentArt:ClientTemplate>
	</ClientTemplates>
</ComponentArt:ComboBox>
