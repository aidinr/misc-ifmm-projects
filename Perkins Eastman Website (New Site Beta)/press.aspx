﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="press.aspx.vb" Inherits="press" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
     <title>Perkins Eastman: News</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
    <meta name="keywords" content="Press releases, Press, speaking engagements" />
</head>
<body>
    <form id="form1" runat="server">
     <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>


<script type="text/javascript">
    jQuery(".main").css("background-color", "#FFF");
</script>
<!--<cfoutput>-->
 <div class="breadcrumbs"><a href="/">Home</a> | PRESS RELEASES</div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td valign="top" align="left" width="50%">

    <asp:Repeater ID="RepeaterPressReleases" runat="server" OnItemDataBound="RepeaterPressRelease_ItemDataBound">
        <HeaderTemplate><div class="head_standard">Press Releases</div></HeaderTemplate>
        <ItemTemplate>
            <div class="copy_standard red bold" style="margin-bottom: 0px;"><%#String.Format("{0:MMMM d, yyyy}", Container.DataItem("post_date"))%>:</div>
						
            <div class="copy_standard" style="margin-top: 0px; margin-right: 15px;"><asp:literal ID="literalBoldStart" runat="server"></asp:literal><%#Container.DataItem("Headline")%> <asp:literal ID="literalPressPDF" runat="server"></asp:literal><asp:literal ID="literalBoldEnd" runat="server"></asp:literal></div>

        </ItemTemplate>
    </asp:Repeater>
    
    <br />
    
    <asp:Repeater ID="RepeaterSpeakingEngagements" runat="server" OnItemDataBound="RepeaterSpeakingEngagements_ItemDataBound">
        <HeaderTemplate><div class="head_standard">Speaking Engagements</div></HeaderTemplate>
        <ItemTemplate>
            <div class="copy_standard" style="margin-top: 0px; margin-right: 15px;">

            <span style="font-weight: bold; color: rgb(220,41,30)"><%#Container.DataItem("dates")%>:</span><br /><%#Container.DataItem("headline")%> <br /><%#Container.DataItem("location")%><br /><%#Container.DataItem("presenter")%><br /><asp:literal ID="literalTopic" runat="server"></asp:literal><br /><br /><%#Container.DataItem("content")%></div>
 
        </ItemTemplate>
    </asp:Repeater>
</td>
<td valign="top" align="left" width="50%">
    <asp:Repeater ID="RepeaterNews" runat="server">
        <HeaderTemplate><div class="head_standard">Press</div>
        <div class="copy_standard"><b>If you are writing about our firm, projects, or industry please <a href="mailto:s.yates@perkinseastman.com">contact Director of Communications, Steven Yates</a>.<br /><br />To stay up-to-date on Perkins Eastman, subscribe to our <a href="rssfeeds.aspx">RSS feed</a>. <a href="rssfeeds.aspx"><img src="./images/rss_feed.gif" border="0" /></a><br /><br />Follow us on <a href="http://twitter.com/PerkinsEastman" target="_blank">Twitter</a>.</b></div>
        </HeaderTemplate>
        <ItemTemplate>
       <div class="copy_standard red bold" style="margin-bottom: 0px;"><%#String.Format("{0:MMMM d, yyyy}", Container.DataItem("post_date"))%>:</div>
						
        <div class="copy_standard" style="margin-top: 0px; margin-right: 15px;">
        <%#Container.DataItem("Content")%> 
        </div>     
        </ItemTemplate>
    </asp:Repeater>
</td>
</tr>
</table>
<!--</cfoutput>-->
    
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>