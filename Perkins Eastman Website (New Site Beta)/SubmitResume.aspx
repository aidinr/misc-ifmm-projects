﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Submitresume.aspx.vb" Inherits="SubmitResume" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Submit Resume</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
</head>
<body>
    <form id="form1" runat="server">
    <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    <script type="text/javascript">
	jQuery(".main").css("background-color", "#FFF");
	
	function loadStates() {
		jQuery("#stateLabel").fadeOut("fast");		
		jQuery("#stateDropdown").fadeOut("fast", function() {		
			//jQuery("#stateLabeldropdown").html('');
			jQuery("#loader").fadeIn("fast", function() {
				jQuery("#stateDropdown").load("resume.cfm?c=" + jQuery("#countrySelector").attr("value"), null, function() {
					if (jQuery("#stateLabelSource").html() == "") {
						jQuery("#stateLabel").html('');
					} else {
						jQuery("#stateLabel").html('* ' + jQuery("#stateLabelSource").html() + ':');
					}
					jQuery("#loader").fadeOut("fast", function() {
						jQuery("#stateLabel").fadeIn("fast");
						jQuery("#stateDropdown").fadeIn("fast");
					});
				});
			});
		});
	}
	
	function validateForm() {
		if (jQuery("input[name='FirstName']").attr("value") == "") {
			jQuery("#validationError").html('You must enter a first name.');
			jQuery("input[name='FirstName']").focus();
			return;
		}
		if (jQuery("input[name='LastName']").attr("value") == "") {
			jQuery("#validationError").html('You must enter a last name.');
			jQuery("input[name='LastName']").focus();
			return;
		}
		if (jQuery("input[name='HomePhoneNumber']").attr("value") == "") {
			jQuery("#validationError").html('You must enter a home phone number.');
			jQuery("input[name='HomePhoneNumber']").focus();
			return;
		}
		if (jQuery("input[name='EmailAddress']").attr("value") == "") {
			jQuery("#validationError").html('You must enter an email address.');
			jQuery("input[name='EmailAddress']").focus();
			return;
		}
		if (jQuery("input[name='City']").attr("value") == "") {
			jQuery("#validationError").html('You must enter a city.');
			jQuery("input[name='City']").focus();
			return;
		}
		if (jQuery("select[name='StateID']").attr("value") == "") {
			jQuery("#validationError").html('You must select a ' + jQuery("#stateLabelSource").html().toLowerCase() + '.');
			jQuery("select[name='StateID']").focus();
			return;
		}
		if (jQuery("select[name='officeid']").attr("value") == "") {
			jQuery("#validationError").html('You must select at least one office.');
			jQuery("select[name='officeid']").focus();
			return;
		}
		if (jQuery("textarea[name='Resume']").attr("value") == "") {
			jQuery("#validationError").html('You must paste your resume text.');
			jQuery("textarea[name='Resume']").focus();
			return;
		}		
		jQuery("#submitImage").attr("src", "images/careers/ajax-loader.gif");
		document.getElementById('resume_form').submit();
	}
</script>

<!--<cfoutput>-->
<div class="head_standard" style="margin-bottom: 5px;">Submit your resume</div>
<div class="formLabel" style="text-align: left; margin-left: 15px; margin-bottom: 15px;">* Denotes required fields. <strong id="validationError" style="background-color: ##FF0;"></strong></div>
<form action="resumePost.cfm" method="post" enctype="multipart/form-data" name="resume_form" id="resume_form">
<cfif variables.JobID gt 0><input type="hidden" name="JobID" value="#variables.JobID#"><cfelse><input type="hidden" name="JobID" value=""></cfif>
<input type="hidden" name="ClientFileName" value="" />
<img id="loader" src="images/careers/ajax-loader.gif" style="position: absolute; left: 250px; top: 305px; display: none;" />
<div class="resumeForm"><table cellpadding="0" cellspacing="0" border="0">

    <tr>
		<td align="right" valign="top"><div class="formLabel ls">* First Name:</div></td>
		<td align="left" valign="top"><input name="FirstName" type="text" id="FirstName" value="#form.FirstName#" tabindex="1"></td>
		<td align="right" valign="top"><div class="formLabel">* Last Name:</div></td>
		<td align="left" valign="top"><input name="LastName" type="text" id="LastName" value="#form.LastName#" tabindex="2"></td>
	</tr>
    <tr>
		<td align="right" valign="top"><div class="formLabel ls">* Home Phone Number:</div></td>
		<td align="left" valign="top"><input name="HomePhoneNumber" type="text" id="HomePhoneNumber" value="#form.HomePhoneNumber#" tabindex="3"></td>
		<td align="right" valign="top"><div class="formLabel">Mobile Phone Number:</div></td>
		<td align="left" valign="top"><input name="MobilePhoneNumber" type="text" id="MobilePhoneNumber" value="#form.MobilePhoneNumber#" tabindex="4"></td>
	</tr>
    <tr>
		<td align="right" valign="top"><div class="formLabel ls">Address 1:</div></td>
		<td align="left" valign="top"><input name="Address1" type="text" id="Address1" value="#form.Address1#" tabindex="6"></td>
		<td align="right" valign="top"><div class="formLabel">* Email Address:</div></td>
		<td align="left" valign="top"><input name="EmailAddress" type="text" id="EmailAddress" value="#form.EmailAddress#" tabindex="5"></td>
	</tr>
    <tr>
		<td align="right" valign="top"><div class="formLabel ls">Address 2:</div></td>
		<td align="left" valign="top"><input name="Address2" type="text" id="Address2" value="#form.Address2#" tabindex="7"></td>
		<td align="right" valign="top"><div class="formLabel">* City:</div></td>
		<td align="left" valign="top"><input name="City" type="text" id="City" value="#form.City#" tabindex="8"></td>
	</tr>
    <tr>
		<td align="right" valign="top"><div class="formLabel ls" id="stateLabel"></div></td>
		<td align="left" valign="top"><div id="stateDropdown"></div></td>
		<td align="right" valign="top"><div class="formLabel">Country:</div></td>
		<td align="left" valign="top">
        	<select id="countrySelector" name="CountryID" tabindex="9" onchange="loadStates();">
            	<option value="12345">--- Select Country ---</option>
			<cfloop from="1" to=#ArrayLen(countries)# index="i">
        		<cfif form.CountryID eq countries[i].ID>
            		<option value="#countries[i].ID#" selected>#countries[i].CountryName#</option>
	            <cfelseif countries[i].CountryName NEQ "">
            		<option value="#countries[i].ID#">#countries[i].CountryName#</option>
				</cfif>
			</cfloop>
        	</select>
		</td>
	</tr>
    <tr>
		<td align="right" valign="top"><div class="formLabel ls"></div></td>
		<td align="left" valign="top"></td>
		<td align="right" valign="top"><div class="formLabel">Postal Code:</div></td>
		<td align="left" valign="top"><input name="PostalCode" type="text" id="PostalCode" value="#form.PostalCode#" tabindex="11"></td>
	</tr>
    <tr>
		<td align="right" valign="top"><div class="formLabel ls">* Office Location:<br />Use CTRL+click to select multiple options.</div></td>
		<td align="left" valign="top">
        	<select name="officeid" size="8" multiple  tabindex="12" style="margin-bottom: 15px;">
              <cfloop from="1" to=#ArrayLen(offices)# index="i">
                <option value="#offices[i].id#">#offices[i].Name#</option>
              </cfloop>
        	</select>
        </td>
		<td align="right" valign="top"><div class="formLabel"></div></td>
		<td align="left" valign="top">
        	<!---
        	<select name="jobcat" size="8" multiple tabindex="13">
                <option>Administrative</option>
                <option>Architecture</option>
                <option>Interior Design</option>
                <option>Marketing</option>
                <option>Internship</option>
        	</select>
			--->
        </td>
	</tr>
    <tr>
		<td align="right" valign="top"><div class="formLabel ls">Position Applying For:</div></td>
		<td align="left" valign="top"><input type="text" name="position" value="#form.position#" tabindex="14" style="margin-bottom: 15px;"></td>
		<td align="right" valign="top"><div class="formLabel">Salary Requirement:</div></td>
		<td align="left" valign="top"><input name="salary" type="text" id="salary" value="#form.salary#" style="width: 155px;" tabindex="15"></td>
	</tr>
    <tr>
		<td align="right" valign="top"><div class="formLabel ls">*Paste your cover letter and resume text into this box:</div></td>
		<td align="left" valign="top" colspan="3"><textarea name="Resume" id="Resume" style="height: 200px; width: 490px; margin-bottom: 15px;" tabindex="16">#form.Resume#</textarea></td>
	</tr>
    <tr>
		<td align="right" valign="top"><div class="formLabel ls" style="margin-bottom: 15px;">Upload Resume/Portfolio:</div></td>
		<td align="left" valign="top" colspan="3">
        	<script type="text/javascript">
				function fileChanged() {
					var s1 = jQuery("input[name='ClientFile']").attr("value").split("/");
					var s2 = s1[s1.length-1].split("\\");
					jQuery("input[name='ClientFileName']").attr("value", s2[s2.length-1]);
				}
			</script>
        	<input type="file" name="ClientFile" tabindex="17" style="margin-bottom: 10px; width: auto;" onchange="fileChanged();">
        </td>
	</tr>
    <tr>
    	<td align="right" valign="top"><div class="formLabel ls"></div></td>
		<td align="left" valign="top" colspan="3">
        	<div class="formLabel" style="margin-bottom: 15px; text-align: left; margin-left: 0px;">Only 1 file accepted.<br />Please combine all documents. (.doc, .docx, or .pdf files only)</div>
        </td>
    </tr>
    <tr>
    	<td align="right" valign="top"><div class="formLabel" style="cursor: pointer; text-decoration: underline;" onClick="window.open('disclaimer.html','popup','width=600,height=600,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=50,top=0'); return false">Disclaimer</div></td>
		<td align="left" valign="top" colspan="3">
            <img id="submitImage" onClick="validateForm(); return false;" style="cursor: pointer; margin-bottom: 15px;" src="../../images/submit.gif" border="0">
			<img onClick="document.getElementById('resume_form').reset(); return false;" style="cursor: pointer; margin-bottom: 15px;" src="../../images/reset.gif" border="0">
        </td>
    </tr>
</table></div>
</form>
<!--</cfoutput>-->
    
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
