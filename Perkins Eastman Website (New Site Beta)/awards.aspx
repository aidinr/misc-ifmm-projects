﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="awards.aspx.vb" Inherits="awards" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Perkins Eastman: Honor and Awards</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
    <meta name="keywords" content="Architectural Awards" />

</head>
<body class="pe2">
  <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    <form id="form1" runat="server">
    
    <div class="breadcrumbs"><a href="/" class="active">Home</a> | <a href="/#linkPage1" class="active">About us</a> | Honors and Awards</div>
   <!--<cfoutput>-->
<div class="head_standard"><asp:Literal ID="ContentName" runat="server"></asp:Literal></div>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td align="left" valign="top">
<asp:Repeater ID="repeaterAwards" runat="server">
<ItemTemplate>
    <img src="<%=Session("WSRetrieveAsset")%>type=asset&amp;size=1&amp;cache=1&amp;width=80&amp;height=80&amp;crop=1&amp;id=<%#container.dataitem("asset_id")%>" border="0"  style="margin-left: 15px;" alt=" " /><br /><br />
</ItemTemplate>
</asp:Repeater>
</td>
<td align="left" valign="top"><div class="copy_standard" style="margin-right: 15px;">
<asp:Literal ID="awards" runat="server"></asp:Literal></div></td>
</tr>
</table>
<!--</cfoutput>-->
    
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form> 
</body>
</html>
