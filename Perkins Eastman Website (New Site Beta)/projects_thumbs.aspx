﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="projects_thumbs.aspx.vb" Inherits="projects_thumbs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="literalPageTitle" runat="server"></asp:Literal></title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>

</head>
<body class="pe2">
    <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    <form id="form1" runat="server">
        <div id="ContentBody" style="display: none;"><asp:Literal ID="literalDescription" runat="server"></asp:Literal></div>
        <div id="ClientListContent" style="display: none;">Loading...</div>
        <div class="breadcrumbs"><a href="/" class="active">Home</a> | <a href="/projects.aspx?list=2400103"  class="active">International</a> | <%=request.querystring("name") %></div>
        <div class="head_project"><%=request.querystring("name") %></div>
        
        
        
        <div class="rightColumn">
				<p class="project_description"><asp:Literal ID="literalDescription2" runat="server"></asp:Literal></p>
        	<asp:Literal ID="literalBrowseClient" runat="server" Text="<p>&nbsp;</p>"></asp:Literal>
        	
            <p>View selected projects below</p>
		</div>
		<div class="leftColumn">
		
		
		
		 <asp:Repeater ID="repeaterProjects" runat="server">
            <ItemTemplate>
                <p class="regionProjects">
                <a href="projectDetails.aspx?p=<%#container.dataitem("projectid")%>&amp;c=<%=request.querystring("list") %>">
                <img alt="" class="project_thumb_image" src="<%=Session("WSRetrieveAsset")%>crop=1&type=project&size=1&width=104&height=133&id=<%#container.dataitem("projectid")%>" />
				<span><%#container.dataitem("item_value").tostring.replace("'","\'")%></span> </a></p>
                
            </ItemTemplate>
        </asp:Repeater>
		
		</div>
		<div style="height: 15px; margin: 0px; clear: both;">&nbsp;</div>
        
        
        
        <div style="margin: 0px 0px 0px 0px;">
        
       

		</div>
        <div style="height: 15px; margin: 0px;">&nbsp;</div>
 <%if request.querystring("list") = "2400103" then%>
    <script src="scripts/international.js" type="text/javascript"></script>
    <%end if%>

  <script type="text/javascript">
      jQuery("#ClientList").click(function() {
          var data = "";
          if (jQuery("#ClientList").html() == 'Browse our client list') {
              data = '<asp:literal id="literalClientList" runat="server"></asp:literal>';
              jQuery("#ClientListContent").html(data);
              jQuery(".project_description").html(jQuery("#ClientListContent").html());
              jQuery("#mapPlus").css('display', 'none');
              jQuery("#ClientList").html('Hide the client list');
          } else {
              jQuery(".project_description").html(jQuery("#ContentBody").html());
              jQuery("#mapPlus").css('display', 'block');
              jQuery("#ClientList").html('Browse our client list');
          }
      });

            </script>
    
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
