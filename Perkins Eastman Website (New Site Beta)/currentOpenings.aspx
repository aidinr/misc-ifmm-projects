﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="currentOpenings.aspx.vb" Inherits="currentOpenings" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Current Openings</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
</head>
<body class="pe2">
    <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    <form id="form1" runat="server">
    
    
    <script type="text/javascript">
jQuery(".main").css("background-color", "#fff");
jQuery(".main").css("height", "560px");
</script>

<!--<cfoutput>-->

<div class="head_standard">#cObj.ContentName#</div>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
	<td valign="top">
    	<div class="copy_standard" style="width: 500px; margin-right:30px">#stripHTML(cObj.ContentBody)#</div>
	</td>
	<td valign="top">
    	<div class="copy_standard red" style="width: 330px; font-size: 10pt;">Sort our current openings by office location. Use CTRL+click to select multiple options. To see positions for all office locations, use "Select All".</div>
        <div class="copy_standard red" style="font-size: 10pt; font-weight:bold;">Office Locations<br />
			<select id="jobLocation" class="copy_standard" multiple="multiple" style="width: 215px; height: 160px; margin-left:0px; float: left;" name="location">
            	<option value="All">Select All</option>
                <!--
                <cfloop from = "1" to = #ArrayLen(cObj.ContentSubCategories)# index = "i">
                	<option value="#cObj.ContentSubCategories[i].Name#">#cObj.ContentSubCategories[i].Name#</option>
                </cfloop>
                -->
            </select>
            <img id="sortButton" src="images/careers/sort.gif" class="copy_standard" style="cursor: pointer;" /><br />
            <img id="aLoader" src="images/careers/ajax-loader.gif" class="copy_standard" style="display: none; margin-left: 30px;" />
		</div>
	</td>
</tr>
</table>
<div id="results" class="copy_standard">
</div>

<!--</cfoutput>-->

<script type="text/javascript">
jQuery("select").change(function() {
	if (jQuery("select option:selected").val() == "All") {
		jQuery("select option:selected").each(function() {
			jQuery(this).removeAttr("selected");
		});
		jQuery("select").attr("selectedIndex", 0);
	}
});

jQuery("#sortButton").click(function() {
	jQuery("#results").fadeOut("fast", function() {
		//jQuery(".main").animate({height: "560px"}, 1500);
		//jQuery(".main").css("height", "560px");
		jQuery("#aLoader").fadeIn("fast");
		jQuery.ajaxSetup({cache: false});
		jQuery("#results").load("currentOpeningsAJAX.cfm", jQuery("select option:selected"), function() {
			jQuery("#aLoader").fadeOut("fast", function() {
				//jQuery(".main").animate({height: "auto"}, 1500);
				jQuery(".main").css("height", "auto");
				jQuery("#results").fadeIn("fast");
			});
		});
	});
});
</script>
    
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
