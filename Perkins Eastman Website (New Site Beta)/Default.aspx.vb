﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Partial Class _Default
    Inherits System.Web.UI.Page

    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)
        'get rid of menu in footer for this page to pass W3C
        Dim footdiv As HtmlControls.HtmlContainerControl = bodyFooterControl.FindControl("footerMenu")
        footdiv.InnerText = ""

        LoadFields()
        LoadFields2()
        LoadPictures()
        LoadFP()
        LoadAwards()
        LoadMainLinks()
        LoadEventDate()

    End Sub

    Private Sub LoadFields()

        Dim sql = "Select 'AboutNews_' + Item_Tag Item_Tag, Item_Value from IPM_NEWS_FIELD_VALUE v join IPM_NEWS_FIELD_DESC d on( d.Item_id = v.Item_id) where News_ID = ( Select Top 1 n.News_ID from IPM_NEWS n Join IPM_NEWS_FIELD_VALUE v ON (n.News_ID = v.News_ID and Item_ID = 3407670 AND Item_Value = 'About Us Page') Where Post_Date <= getdate() AND Pull_Date >= getdate() AND Show = 1 Order By Post_Date DESC ) AND Item_value <> '' "
        sql += " Union Select 'InsightsPOV_' + Item_Tag Item_Tag, Item_Value from IPM_NEWS_FIELD_VALUE v join IPM_NEWS_FIELD_DESC d on( d.Item_id = v.Item_id)  where News_ID = ( Select Top 1 n.News_ID from IPM_NEWS n Join IPM_NEWS_FIELD_VALUE v ON (n.News_ID = v.News_ID and Item_ID = 3407670 AND Item_Value = 'Insights Page') Where Post_Date <= getdate() AND Pull_Date >= getdate() AND Show = 1 Order By Post_Date DESC ) AND Item_value <> ''"
        sql += " Union Select 'InsightsSpeak_' + Item_Tag Item_Tag, Item_Value from IPM_EVENTS_FIELD_VALUE v join IPM_EVENTS_FIELD_DESC d on( d.Item_id = v.Item_id) where Event_ID = ( Select Top 1 e.Event_ID from IPM_EVENTS e Join IPM_EVENTS_FIELD_VALUE v ON (e.Event_id = v.Event_Id and Item_id = 30052974 and Item_value = 'Insights Page') Where Post_Date <= getdate() AND Pull_Date >= getdate() AND Show = 1 Order By Post_Date DESC )"
        sql += " Union Select 'RegionsNews_' + Item_Tag Item_Tag, Item_Value from IPM_NEWS_FIELD_VALUE v join IPM_NEWS_FIELD_DESC d on( d.Item_id = v.Item_id) where News_ID = (Select Top 1 n.News_ID from IPM_NEWS n Join IPM_NEWS_FIELD_VALUE v ON (n.News_ID = v.News_ID and Item_ID = 3407670 AND Item_Value = 'Regions Page') Where Post_Date <= getdate() AND Pull_Date >= getdate() AND Show = 1 Order By Post_Date DESC) AND Item_value <> ''"
        sql += " Union Select 'ProjectNews_' + Item_Tag Item_Tag, Item_Value from IPM_NEWS_FIELD_VALUE v join IPM_NEWS_FIELD_DESC d on( d.Item_id = v.Item_id) where News_ID = (Select Top 1 n.News_ID from IPM_NEWS n Join IPM_NEWS_FIELD_VALUE v ON (n.News_ID = v.News_ID and Item_ID = 3407670 AND Item_Value = ' Projects Page') Where Post_Date <= getdate() AND Pull_Date >= getdate() AND Show = 1 Order By Post_Date DESC) AND Item_value <> ''"
        sql += " Union Select 'ProjectFP_' + Item_Tag Item_Tag, v.Item_value from ipm_project_field_value v join ipm_project_field_desc d on (v.item_id = d.item_id) where v.projectid = ( Select top 1 p.projectid from ipm_project p join ipm_project_field_value v on (p.projectid = v.projectid and v.item_ID = 2400055 and v.Item_value = 1) join ipm_project_field_value o on (p.projectid = o.projectid and o.item_id = 2400056) Where p.Show = 1 and p.Available = 'Y' Order by o.item_value) and v.item_value <> ''"
        sql += " Union Select 'AboutAwards_' + Item_Tag Item_Tag, Item_Value from IPM_AWARDS_FIELD_VALUE v join IPM_AWARDS_FIELD_DESC d on( d.Item_id = v.Item_id) where Awards_ID = ( Select Top 1 n.Awards_ID from IPM_Awards n Join IPM_AWARDS_FIELD_VALUE v ON (n.Awards_ID = v.Awards_ID and Item_ID = 3409245 AND Item_Value = 'About Us') Where Post_Date <= getdate() AND Pull_Date >= getdate() AND Show = 1 Order By Post_Date DESC ) AND Item_value <> ''"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        For Each row As DataRow In dt1.Rows
            Select Case row("Item_Tag").ToString().Trim()
                Case "AboutAwards_IDAM_AWARDTITLE"
                    IDAM_PAGE1_TITLE.InnerText = row("Item_Value").ToString.ToUpper()
                Case "AboutAwards_IDAM_COLOR"
                    IDAM_PAGE1_TITLE.Style.Add("background-color", "#" + row("Item_Value").ToString())
                Case "AboutNews_IDAM_NEWSTITLE"
                    IDAM_PAGE1_TITLE2.InnerText = row("Item_Value").ToString()
                Case "AboutNews_IDAM_NEWSCOLOR"
                    IDAM_PAGE1_TITLE2.Style.Add("background-color", "#" + row("Item_Value").ToString())
                Case "AboutNews_IDAM_NEWSFEATURE"
                    IDAM_PAGE1_TITLE2_TEXT.InnerHtml = Server.HtmlDecode(row("Item_Value").ToString())
                Case "AboutNews_IDAM_NEWS_URL"
                    If row("Item_Value").ToString().Trim <> "" Then
                        IDAM_PAGE1_TITLE2_URL.HRef = row("Item_Value").ToString()
                    End If
                Case "ProjectFP_IDAM_PROJECTDISPLAY"
                    IDAM_PAGE2_PROJECT_TITLE.InnerText() = row("Item_Value").ToString()
                Case "ProjectFP_IDAM_PROJECTCOLOR"
                    IDAM_PAGE2_PROJECT_TITLE.Style.Add("background-color", "#" + row("Item_Value").ToString())
                Case "ProjectFP_IDAM_PROJECT_SHORT_NAME"
                    IDAM_PAGE2_CAP_TITLE.InnerText = row("Item_Value").ToString()
                Case "ProjectFP_IDAM_PROJECT_CITY"
                    IDAM_PAGE2_CITY.InnerText = row("Item_Value").ToString()
                Case "ProjectFP_IDAM_PROJECT_STATE"
                    IDAM_PAGE2_STATE.InnerText = row("Item_Value").ToString()
                Case "ProjectFP_IDAM_SHORT"
                    IDAM_PAGE2_DESCRIPTION.InnerText() = row("Item_Value").ToString()
                Case "ProjectNews_IDAM_NEWSFEATURE"
                    IDAM_PAGE2_IMAGE3_TEXT.InnerHtml = row("Item_value").ToString()
                Case "ProjectNews_IDAM_NEWSTITLE"
                    IDAM_PAGE2_TITLE2.InnerText = row("Item_Value").ToString()
                Case "ProjectNews_IDAM_NEWSCOLOR"
                    IDAM_PAGE2_TITLE2.Style.Add("background-color", "#" + row("Item_Value").ToString())
                Case "RegionsNews_IDAM_NEWSTITLE"
                    IDAM_PAGE3_PIC2_TITLE.InnerText = row("Item_Value").ToString()
                Case "RegionsNews_IDAM_NEWSCOLOR"
                    IDAM_PAGE3_PIC2_TITLE.Style.Add("background-color", "#" + row("Item_Value").ToString())
                Case "RegionsNews_IDAM_NEWSFEATURE"
                    IDAM_PAGE3_PIC2_DESCRIPTION.InnerHtml = row("Item_Value").ToString()
                Case "InsightsPOV_IDAM_NEWSTITLE"
                    IDAM_PAGE4_PIC1_TITLE.InnerText = row("Item_Value").ToString().ToUpper()
                Case "InsightsPOV_IDAM_NEWSCOLOR"
                    IDAM_PAGE4_PIC1_TITLE.Style.Add("background-color", "#" + row("Item_Value").ToString())
                Case "InsightsPOV_IDAM_POVEMPLOYEE"
                    IDAM_PAGE4_PIC1_CAP.InnerText = row("Item_Value").ToString()
                Case "InsightsPOV_IDAM_POVTITLE"
                    IDAM_PAGE4_PIC1_SUBTITLE.InnerText = row("Item_Value").ToString()
                Case "InsightsSpeak_IDAM_EVENTTITLE"
                    IDAM_PAGE4_PIC4_TITLE.InnerText = row("Item_Value").ToString()
                Case "InsightsSpeak_IDAM_EVENTCOLOR"
                    IDAM_PAGE4_PIC4_TITLE.Style.Add("background-color", row("Item_Value").ToString())
                Case "InsightsSpeak_IDAM_PRESENTER"
                    IDAM_PAGE4_PIC4_CAP.InnerText = row("Item_Value").ToString()
                Case "InsightsSpeak_IDAM_TITLE"
                    IDAM_PAGE4_PIC4_SUBCAP.InnerText = row("Item_Value").ToString()
                Case "InsightsSpeak_IDAM_LOC"
                    IDAM_PAGE4_PIC4_LOCDATE.InnerText = row("Item_Value").ToString()
            End Select
        Next row

    End Sub
    Private Sub LoadFields2()

        Dim sql = "Select * From (Select top 1 cast('AboutUSNews' as varchar(20)) Type,n.News_ID, Content, Picture, PDF, cast(NULL as decimal(18,0)) ProjectID from IPM_NEWS n Join IPM_NEWS_FIELD_VALUE v ON (n.News_ID = v.News_ID and Item_ID = 3407670 AND Item_Value = 'About Us Page') Where(Post_Date <= getdate() And Pull_Date >= getdate() And Show = 1) Order By Post_Date DESC) t "
        sql += " Union Select * from (Select top 1 cast('InsightPOV' as varchar(20)) Type,n.News_ID, Headline, Picture, PDF, cast(NULL as decimal(18,0)) ProjectID from IPM_NEWS n Join IPM_NEWS_FIELD_VALUE v ON (n.News_ID = v.News_ID and Item_ID = 3407670 AND Item_Value = 'Insights Page') Where Post_Date <= getdate() AND Pull_Date >= getdate() AND Show = 1 Order By Post_Date DESC) t2"
        sql += " Union select * from (select top 1 cast('InsightSpeak' as varchar(20)) Type, a.asset_id, e.headline,  0 pict, 0 pdf, cast(NULL as decimal(18,0)) ProjectID from ipm_events e join ipm_events_field_value v on (e.event_id = v.event_id and v.Item_id = 30052974 and v.Item_value = 'Insights Page') left join ipm_asset a on (e.event_id = a.Category_id and a.available = 'Y') where e.show = 1 and e.type = 5 Order By e.Post_Date Desc, a.Upload_date desc) t3"
        sql += " Union Select * from (Select top 1 cast('RegionsNews' as varchar(20)) Type,n.News_ID, Headline, Picture, PDF, ProjectID from IPM_NEWS n Join IPM_NEWS_FIELD_VALUE v ON (n.News_ID = v.News_ID and Item_ID = 3407670 AND Item_Value = 'Regions Page') Left join IPM_NEWS_RELATED_PROJECTS p on (n.news_id = p.News_ID) Where Post_Date <= getdate() AND Pull_Date >= getdate() AND Show = 1 Order By Post_Date DESC) t4"
        sql += " Union Select * from (Select top 1 cast('ProjectNews' as varchar(20)) Type,n.News_ID, Headline, Picture, PDF, ProjectID from IPM_NEWS n Join IPM_NEWS_FIELD_VALUE v ON (n.News_ID = v.News_ID and Item_ID = 3407670 AND Item_Value = ' Projects Page') Left join IPM_NEWS_RELATED_PROJECTS p on (n.news_id = p.News_ID) Where Post_Date <= getdate() AND Pull_Date >= getdate() AND Show = 1 Order By Post_Date DESC) t5"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)



        Dim dt1 As New DataTable
        Dim rows As System.Int32 = MyCommand1.Fill(dt1)

        IDAM_PAGE4_PIC1_DESCRIPTION.InnerText = rows.tostring()

        For Each row As DataRow In dt1.Rows
            Select Case row("Type").ToString().Trim()
                Case "AboutUSNews"
                    IDAM_PAGE1_TITLE2_PICLABEL.InnerText = Left(row("Content").ToString(), 168)
                    If row("Picture") = 1 Then
                        Page1BottomLeft.Src = Session("WSRetrieveAsset").ToString() + "type=news&amp;crop=1&amp;size=1&amp;height=123&amp;width=460&amp;id=" + row("News_ID").ToString()
                    End If
                    'If row("pdf") = 1 Then
                    'IDAM_PAGE1_TITLE2_PDF.HRef = Session("WSRetrieveAsset").ToString() + "type=newspdf&amp;size=0&amp;cache=1&amp;id=" + row("News_ID").ToString()
                    'Page1BottomLeftImgLink.HRef = Session("WSRetrieveAsset").ToString() + "type=newspdf&amp;size=0&amp;cache=1&amp;id=" + row("News_ID").ToString()
                    'IDAM_PAGE1_TITLE2_PICLABEL.HRef = Session("WSRetrieveAsset").ToString() + "type=newspdf&amp;size=0&amp;cache=1&amp;id=" + row("News_ID").ToString()
                    'IDAM_PAGE1_TITLE2_PDF.InnerText = "more [+]"
                    'End If
                Case "InsightPOV"
                    IDAM_PAGE4_PIC1_DESCRIPTION.InnerText = row("Content").ToString()
                    If row("Picture") = 1 Then
                        Page4TopLeft.Src = Session("WSRetrieveAsset").ToString() + "type=news&amp;cache=1&amp;crop=1&amp;size=1&amp;height=123&amp;width=220&amp;id=" + row("News_ID").ToString()
                    End If
                    If row("PDF") = 1 Then
                        'IDAM_PAGE4_PIC1_IMG_LINK.HRef = Session("WSRetrieveAsset").ToString() + "type=newspdf&amp;size=0&amp;cache=1&amp;id=" + row("News_ID").ToString()
                        'IDAM_PAGE4_PIC1_CAP.HRef = Session("WSRetrieveAsset").ToString() + "type=newspdf&amp;size=0&amp;cache=1&amp;id=" + row("News_ID").ToString()
                        IDAM_PAGE4_PIC1_DESC_LINK.HRef = Session("WSRetrieveAsset").ToString() + "type=newspdf&amp;size=0&amp;cache=1&amp;id=" + row("News_ID").ToString()
                    End If
                Case "InsightSpeak"
                    If Not IsDBNull(row("News_ID")) Then
                        Page4Right.Src = (Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=157&amp;width=220&amp;id=" + row("News_ID").ToString())
                    End If
                    IDAM_PAGE4_PIC4_DESCRIPTION.InnerText = Regex.Replace(row("Content").ToString(), "<.*?>", "")
                    If row("PDF") = 1 Then
                        IDAM_PAGE4_PIC4_DESCRIPTION.HRef = Session("WSRetrieveAsset").ToString() + "type=newspdf&amp;size=0&amp;cache=1&amp;id=" + row("News_ID").ToString()
                    End If
                Case "RegionsNews"
                    If row("Picture") = 1 Then
                        Page3Middle.Src = (Session("WSRetrieveAsset").ToString() + "type=news&amp;crop=1&amp;size=1&amp;height=146&amp;width=220&amp;id=" + row("News_ID").ToString())
                    End If
                    If row("PDF") = 1 Then
                        IDAM_PAGE3_PIC2_NEWSLINK.HRef = Session("WSRetrieveAsset").ToString() + "type=newspdf&amp;size=0&amp;cache=1&amp;id=" + row("News_ID").ToString()
                        'IDAM_PAGE3_PIC2_IMG_LINK.HRef = Session("WSRetrieveAsset").ToString() + "type=newspdf&amp;size=0&amp;cache=1&amp;id=" + row("News_ID").ToString()
                        IDAM_PAGE3_PIC2_CAPTION.HRef = Session("WSRetrieveAsset").ToString() + "type=newspdf&amp;size=0&amp;cache=1&amp;id=" + row("News_ID").ToString()
                    Else
                        IDAM_PAGE3_PIC2_NEWSLINK.InnerText = ""
                    End If
                    If Not IsDBNull(row("ProjectID")) Then
                        IDAM_PAGE3_PIC2_PROJECTLINK.HRef = "projectDetails.aspx?p=" + row("ProjectID").ToString() + "&amp;c="
                    Else
                        IDAM_PAGE3_PIC2_PROJECTLINK.InnerText = ""
                    End If
                    'IDAM_PAGE3_PIC2_DESCRIPTION.InnerText = row("Content").ToString()
                Case "ProjectNews"
                    If row("Picture") = 1 Then
                        Page2BottomMiddle.Src = (Session("WSRetrieveAsset").ToString() + "type=news&amp;crop=1&amp;size=1&amp;height=112&amp;width=220&amp;id=" + row("News_ID").ToString())
                    End If
                    'IDAM_PAGE2_IMAGE3_TEXT.InnerText() = row("Content").ToString()
                    If Not IsDBNull(row("ProjectID")) Then
                        IDAM_PAGE2_IAMGE3_LINK.HRef = "projectDetails.aspx?p=" + row("ProjectID").ToString() + "&amp;c="
                    Else
                        IDAM_PAGE2_IAMGE3_LINK.InnerText = ""
                    End If
            End Select
        Next row



    End Sub
    Private Sub LoadPictures()

        Dim sql = "Select Name, Asset_ID from IPM_ASSET where ProjectID = 3409169 and Available ='Y' and  Name in ('about.png', 'project page left.png', 'project page top right.png', 'project page right.png', 'regions.png', 'region page top.png', 'region page bottom.png','insight.png','insight page left.png') "

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        For Each row As DataRow In dt1.Rows
            Select Case row("NAME").ToString()
                Case "about.png"
                    Page1Right.Src = (Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=604&amp;width=460&amp;id=" + row("Asset_ID").ToString())
                Case "project page left.png"
                    Page2BottomLeft.Src = (Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=300&amp;width=220&amp;id=" + row("Asset_ID").ToString())
                Case "Page2BottomMiddle"
                    Page2BottomMiddle.Src = (Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=112&amp;width=220&amp;id=" + row("Asset_ID").ToString())
                Case "project page right.png"
                    Page2BottomRight.Src = (Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=199&amp;width=220&amp;id=" + row("Asset_ID").ToString())
                Case "project page top right.png"
                    MPP_LINK.Style.Add("background", "transparent url('" + (Session("WSRetrieveAsset").ToString() + "type=asset&crop=1&cache=1&size=1&height=383&width=460&id=" + row("Asset_ID").ToString()) + "') no-repeat scroll 0 0")
                Case "Page3Middle"
                    Page3Middle.Src = (Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=146&amp;width=220&amp;id=" + row("Asset_ID").ToString())
                Case "Page3TopRight"
                    Page3TopRight.Src = (Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=286&amp;width=220&amp;id=" + row("Asset_ID").ToString())
                Case "regions.png"
                    MRP_LINK.Style.Add("background", "transparent url('" + (Session("WSRetrieveAsset").ToString() + "type=asset&crop=1&cache=1&size=1&height=604&width=460&id=" + row("Asset_ID").ToString()) + "') no-repeat scroll 0 0")
                Case "region page top.png"
                    Page3TopRight.Src = (Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=296&amp;width=220&amp;id=" + row("Asset_ID").ToString())
                Case "region page bottom.png"
                    Page3BottomRight.Src = (Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=296&amp;width=220&amp;id=" + row("Asset_ID").ToString())
                Case "Page4TopLeft"
                    Page4TopLeft.Src = (Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=157&amp;width=220&amp;id=" + row("Asset_ID").ToString())
                Case "insight page left.png"
                    Page4BottomLeft.Src = (Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=296&amp;width=220&amp;id=" + row("Asset_ID").ToString())
                Case "insight.png"
                    Page4Middle.Src = (Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=604&amp;width=460&amp;id=" + row("Asset_ID").ToString())
                Case "Page4Right"
                    Page4Right.Src = (Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=157&amp;width=220&amp;id=" + row("Asset_ID").ToString())
            End Select
        Next row

    End Sub
    Private Sub LoadFP()

        'Dim sql = "Select ProjectId, Asset_ID from IPM_ASSET where projectid = (Select top 1 p.projectid from ipm_project p join ipm_project_field_value v on (p.projectid = v.projectid and v.item_ID = 2400055 and v.Item_value = 1) join ipm_project_field_value o on (p.projectid = o.projectid and o.item_id = 2400056) Where p.Show = 1 and p.Available = 'Y' Order by o.item_value) and Name = 'featured_small.jpg' and Available = 'Y' and Active = 1"
        Dim sql = "Select top 1 p.projectid , a.Asset_ID from ipm_project p join ipm_project_field_value v on (p.projectid = v.projectid and v.item_ID = 2400055 and v.Item_value = 1) join ipm_project_field_value o on (p.projectid = o.projectid and o.item_id = 2400056) join IPM_ASSET a on (a.ProjectID = p.ProjectID and a.Name = 'featured_small.jpg' and a.Available = 'Y' and Active = 1) Where p.Show = 1 and p.Available = 'Y'   Order by o.item_value"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)
        If (dt1.Rows.Count > 0) Then
            Page2TopLeft.Src = (Session("WSRetrieveAsset").ToString() + "type=asset&amp;crop=1&amp;size=1&amp;height=147&amp;width=220&amp;id=" + dt1.Rows(0)("Asset_ID").ToString())
            IDAM_PAGE2_FPLINK.HRef = "projectDetails.aspx?p=" + dt1.Rows(0)("Projectid").ToString() + "&amp;c="
            'Page2TopLeftImgLink.HRef = "projectDetails.aspx?p=" + dt1.Rows(0)("Asset_ID").ToString() + "&amp;c="
            'IDAM_PAGE2_CAP_TITLE.HRef = "projectDetails.aspx?p=" + dt1.Rows(0)("Projectid").ToString() + "&amp;c="
        End If

    End Sub
    Private Sub LoadAwards()

        Dim sql = "Select top 1 a.Awards_Id, Headline, Content, PublicationTitle, ProjectID from IPM_AWARDS a Join IPM_AWARDS_FIELD_VALUE v on (a.Awards_id = v.Awards_ID) Left join IPM_AWARDS_RELATED_PROJECTS p on (a.Awards_ID = p.Awards_ID) where v.Item_ID = 3409245 and v.Item_Value = 'About Us' And a.Show = 1 and a.Post_Date < getdate() and a.Pull_Date > getdate() Order By Post_Date DESC"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)
        If (dt1.Rows.Count > 0) Then
            Page1Middle.Src = (Session("WSRetrieveAsset").ToString() + "type=awards&amp;crop=1&amp;cache=1&amp;size=1&amp;height=147&amp;width=220&amp;id=" + dt1.Rows(0)("Awards_ID").ToString())
            IDAM_PAGE1_PiC_LABEL.InnerText = dt1.Rows(0)("Headline").ToString()
            IDAM_PAGE1_SUB_TITLE.InnerText = dt1.Rows(0)("PublicationTitle").ToString()
            IDAM_PAGE1_DESC.InnerText = dt1.Rows(0)("Content").ToString()
            If IsDBNull(dt1.Rows(0)("ProjectID")) Then
                IDAM_PAGE1_PROJECT_LINK.InnerText = ""
            Else
                IDAM_PAGE1_PROJECT_LINK.HRef = "/projectDetails.aspx?p=" + dt1.Rows(0)("ProjectID").ToString() + "&amp;c="
            End If

        End If

    End Sub

    Private Sub LoadMainLinks()

        Dim sql = "select d.Item_Tag, v.Item_Value from ipm_project p Left join IPM_PROJECT_FIELD_VALUE v on (p.ProjectID = v.ProjectID) Left join IPM_PROJECT_FIELD_DESC d on (v.Item_ID = d.Item_ID and ((d.Item_Group  = 'Main Project Page') or (d.Item_Group = 'Main Regions Page'))) where p.name = 'Public Web Site Content' and d.item_TAG is not NULL and v.Item_Value <> '' "

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        For Each row As DataRow In dt1.Rows
            Select Case row("Item_Tag").ToString()
                Case "IDAM_MPP_TEXT1"
                    MPP_TEXT1.InnerText = row("Item_Value").ToString()
                Case "IDAM_MPP_TEXT2"
                    MPP_TEXT2.InnerText = row("Item_Value").ToString()
                Case "IDAM_MPP_LINK"
                    MPP_LINK.HRef = row("Item_Value").ToString()
                Case "IDAM_MRP_TEXT1"
                    MRP_TEXT1.InnerText = row("Item_Value").ToString()
                Case "IDAM_MRP_TEXT2"
                    MRP_TEXT2.InnerText = row("Item_Value").ToString()
                Case "IDAM_MRP_LINK"
                    MRP_LINK.HRef = row("Item_Value").ToString()
            End Select
        Next row

    End Sub
    Private Sub LoadEventDate()

        Dim sql = "Select DATENAME(mm, event_date) + ' ' + DATENAME(d,event_date) + ', ' + DATENAME(YYYY, Event_Date) as EventDate from IPM_Events where event_id = (Select Top 1 e.Event_ID from IPM_EVENTS e Join IPM_EVENTS_FIELD_VALUE v ON (e.Event_id = v.Event_Id and Item_id = 30052974 and Item_value = 'Insights Page') Where Post_Date <= getdate() AND Pull_Date >= getdate() AND Show = 1 Order By Post_Date DESC )"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then
            Dim temp As String = IDAM_PAGE4_PIC4_LOCDATE.InnerText
            If temp.Trim() = "" Then
                IDAM_PAGE4_PIC4_LOCDATE.InnerText = dt1.Rows(0)(0).ToString()
            Else
                IDAM_PAGE4_PIC4_LOCDATE.InnerText = temp.Trim() + " | " + dt1.Rows(0)(0).ToString()
            End If
        End If

    End Sub

'Select DATENAME(mm, event_date) + ' ' + DATENAME(d,event_date) + ', ' + DATENAME(YYYY, Event_Date) as EventDate from IPM_Events where event_id = (Select Top 1 e.Event_ID from IPM_EVENTS e Join IPM_EVENTS_FIELD_VALUE v ON (e.Event_id = v.Event_Id and Item_id = 30052974 and Item_value = 'Insights Page') Where Post_Date <= getdate() AND Pull_Date >= getdate() AND Show = 1 Order By Post_Date DESC )
End Class
