﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="region.aspx.vb" Inherits="region" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="literalPageTitle" runat="server"></asp:Literal></title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>

<style type="text/css">

.mapAmerica {
 padding: 0;
 margin: 0;
 background: #fff url(/images/worldSmall.png) no-repeat scroll 0 -139px;
 height: 140px;
 width: 283px;
}

.mapAsia {
 padding: 0;
 margin: 0;
 background: #fff url(/images/worldSmall.png) no-repeat scroll 0 -279px;
 height: 140px;
 width: 283px;
}

.mapEurope {
 padding: 0;
 margin: 0;
 background: #fff url(/images/worldSmall.png) no-repeat scroll 0 -419px;
 height: 139px;
 width: 283px;
}

.mapAfrica {
 padding: 0;
 margin: 0;
 background: #fff url(/images/worldSmall.png) no-repeat scroll 0 -559px;
 height: 139px;
 width: 283px;
}


</style>

</head>
<body class="pe2">
    <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
        <div id="ContentBody" style="display: none;"><asp:Literal ID="literalDescription" runat="server"></asp:Literal></div>
        <div id="ClientListContent" style="display: none;">Loading...</div>
        <div class="breadcrumbs"><a href="/" class="active">Home</a> | <a href="/#linkPage3"  class="active">REGIONS</a> | <a href="RegionsMap.aspx" class="active">REGIONS MAP</a> | <%=request.querystring("name") %></div>
        <div class="head_project"><%=request.querystring("name") %></div>
        
        
        
        <div class="rightColumn">
        <div class="rightSidePromo">
            <p id="project_description" runat="server" class="project_description">
            </p>
            <div class="SelectedProjectsH">Selected Projects</div>
                            <div class="SelectedProjects">
                            <asp:Literal ID="ClientList" runat="server" Text=""></asp:Literal>
                            </div>
                            <p class="BrowseClientList">
                                <a class="clientListShow">Browse our client list</a>
                            </p>
                    </div>
				    <div class="project_rightColShort"><asp:Literal ID="literalDescription2" runat="server"></asp:Literal></div>
        	        <asp:Literal ID="literalBrowseClient" runat="server" Text=""></asp:Literal>
        	
		</div>
		<div class="leftColumn">
		
		
		
		 <asp:Repeater ID="repeaterProjects" runat="server" OnItemDataBound="RepeaterProjects_ItemDataBound">
            <ItemTemplate>
                <p class="regionProjects">
                <a id="projectLink" runat="server" href="#" >
                <img alt="" class="project_thumb_image" src="<%=Session("WSRetrieveAsset")%>crop=1&amp;type=project&amp;size=1&amp;width=132&amp;height=104&amp;id=<%#container.dataitem("projectid")%>" />
				<span><%#container.dataitem("name").tostring.replace("'","\'")%></span> </a></p>
                
            </ItemTemplate>
        </asp:Repeater>
		
		</div>
		<div style="height: 15px; margin: 0px; clear: both;">&nbsp;</div>
        
        
        
        <div style="margin: 0px 0px 0px 0px;">
        
       

		</div>
        <div style="height: 15px; margin: 0px;">&nbsp;</div>
    
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>

</body>
</html>
