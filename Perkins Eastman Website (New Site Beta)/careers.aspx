﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="careers.aspx.vb" Inherits="careers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Perkins Eastman: Careers</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
    <meta name="keywords" content="Internship, Employment, Full time, Part time" />
</head>
<body class="pe2">
      <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    <form id="form1" runat="server">
    
    
   
<script type="text/javascript">
	jQuery(".main").css("background-color", "#FFF");
</script>
<!--<cfoutput>-->
 <div class="breadcrumbs"><a href="/" class="active">Home</a> | Careers</div>
<div class="head_standard"><asp:Literal ID="literalTitle" runat="server"></asp:Literal></div>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td align="left" valign="top">
        <div class="copy_standard" style="width: 390px; padding-right: 30px;"><asp:Literal ID="literalContent" runat="server"></asp:Literal></div>
        <div class="copy_standard"><!--<a href="currentOpenings.cfm">View current openings</a>--></div>
</td>
<td align="left" valign="top">
	<!--
	<cfscript>
		results = webservice.getContentCategoryByName(username, password, firmid, 'Promoting Leadership', '');
		results = json.decode(results);
		cObj = results.ContentCategory;
	</cfscript>
	-->
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td valign="top">
            	<!--<cfif ArrayLen(cObj.Images) GTE 1><img src="#cObj.Images[1].ImageURL#" style="" /></cfif>-->
            	<asp:Literal ID="literalPromotingLeadershipImage" runat="server"></asp:Literal>
            </td>
            <td valign="top">
                <div style="width: 300px; height: 210px; background-color: #CCCCCC; overflow: hidden; margin-left: 0px; margin-bottom: 30px; margin-right: 0px; margin-top: 0px;">
                    <asp:Literal ID="literalPromotingLeadership" runat="server"></asp:Literal>
                </div>
            </td>
        </tr>
    </table>
    <!--
    <cfscript>
		results = webservice.getContentCategory(username, password, firmid, 'L3K1919', '');
		results = json.decode(results);
		cObj = results.ContentCategory;
	</cfscript>
	-->
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td valign="top">
                <!--<cfif ArrayLen(cObj.Images) GTE 1><img src="#cObj.Images[1].ImageURL#" style="" /></cfif>-->
                <asp:Literal ID="literalInternshipImage" runat="server"></asp:Literal>
            </td>
            <td valign="top">
                <div style="width: 300px; height: 210px; background-color: #CCCCCC; overflow: hidden; margin-left: 0px; margin-bottom: 30px; margin-right: 0px; margin-top: 0px;">
                    <asp:Literal ID="literalInternship" runat="server"></asp:Literal>
                </div>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>


    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
