﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Net

Partial Class awards
    Inherits System.Web.UI.Page

    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)

        GenerateAwards()

    End Sub

    Sub GenerateAwards()

        Dim sql As String = "Select d.Item_Name, v.Item_Value from IPM_PROJECT p Join IPM_PROJECT_FIELD_VALUE v on(p.ProjectID = v.ProjectID) join IPM_PROJECT_FIELD_DESC d on (v.Item_ID = d.Item_ID and d.Item_Name = 'Honors and Awards') Where name = 'Public Web Site Content' and Available = 'Y'"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        'Dim projectIDParameter As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        'MyCommand1.SelectCommand.Parameters.Add(projectIDParameter)
        'MyCommand1.SelectCommand.Parameters("@projectid").Value = ConfigurationSettings.AppSettings("awardsProjectID")

        'Dim websiteDescriptionParameter As New SqlParameter("@websiteDescriptionUDFID", SqlDbType.VarChar, 255)
        'MyCommand1.SelectCommand.Parameters.Add(websiteDescriptionParameter)
        'MyCommand1.SelectCommand.Parameters("@websiteDescriptionUDFID").Value = ConfigurationSettings.AppSettings("websiteContentUDFID")


        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then
            Try

                'ContentName.Text = dt1.Rows(0)("project_name")
                ContentName.Text = dt1.Rows(0)("Item_Name")
                awards.Text = dt1.Rows(0)("item_value")

            Catch ex As Exception

                'awards.Text = "This is placeholder content.  The data was forked for development purposes and some data is not available in the development environment.  The real conent will be displayed when the site goes live."

            End Try
        End If

        'Dim sql2 As String = "select * from IPM_project_related a, ipm_project b, ipm_project c where a.Project_ID = @projectid and a.project_id = b.projectid and a.ref_id = c.projectid and b.available = 'y' and c.available = 'y'"
        Dim sql2 = "Select Asset_ID from IPM_ASSET a join IPM_ASSET_CATEGORY c on (a.CATEGORY_ID = c.CATEGORY_ID) where a.ProjectID = @projectid And c.Name = 'Awards Page' and a.Available = 'Y' and c.Available = 'Y' and c.AVAILABLE_DATE < getdate()"

        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)

        Dim projectIDParameter2 As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(projectIDParameter2)
        MyCommand2.SelectCommand.Parameters("@projectid").Value = ConfigurationSettings.AppSettings("WebSiteContentProjectID")


        Dim dt2 As New DataTable
        MyCommand2.Fill(dt2)

        repeaterAwards.DataSource = dt2
        repeaterAwards.DataBind()

        'awards.Text = GetFile(Session("WSDownloadAsset").ToString & "size=0&assetid=" & dt1.Rows(0)("asset_id").ToString)


        'Dim objReader As StreamReader = New StreamReader(System.Environment.CurrentDirectory & "\content\firmProfile.txt")
        'Dim objReader As StreamReader = New StreamReader("c:\projects\Perkins_Eastman_Website\content\awards.txt")

        'awards.Text = objReader.ReadToEnd()


    End Sub

    Function GetFile(ByVal URL As String) As String
        Try
            Dim clientreq As HttpWebRequest
            clientreq = HttpWebRequest.Create(URL)
            clientreq.Timeout = CType(10000, Integer)
            Dim clientres As HttpWebResponse = clientreq.GetResponse
            Dim data As IO.Stream = clientres.GetResponseStream
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd

            Return str

        Catch ex As Exception
            Throw New Exception("WebService not available." + URL)
        End Try
    End Function


End Class
