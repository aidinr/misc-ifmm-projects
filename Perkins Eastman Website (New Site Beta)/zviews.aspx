﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="views.aspx.vb" Inherits="views" EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
     <title>Perkins Eastman: Views</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
    <meta name="keywords" content="Architectural Awards" />
</head>

<body>

  <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
 <!--   <form id="form1" runat="server"> -->
    
    <div class="breadcrumbs"><a href="/" class="active" >Home</a> | Point of View</div>
   <!--<cfoutput>-->

<div class="head_standard"><asp:Literal ID="ContentName" runat="server"></asp:Literal></div>


<div class="left"> <h1 class="headline"> Point of View </h1> 

<div class="list_holder"> 

<asp:Repeater runat="server" ID="POV" OnItemDataBound="POV_ItemDataBound">
<ItemTemplate>
<div class="item blue_bg"> <a id="PressReleaseImageLink" runat="server"><img src="<%=Session("WSRetrieveAsset")%>type=news&amp;crop=1&amp;size=1&amp;height=84&amp;width=117&amp;id=<%#container.dataitem("News_Id")%>" alt="" height="84 px" width="117 px"/></a> 

<div id="PressReleaseText" runat="server" class="info"> <a id="PressReleaseTitle" runat="server" class="red_item"><%# DataBinder.Eval(Container.DataItem, "Headline")%> </a> <strong> <%# DataBinder.Eval(Container.DataItem, "DateLoc")%> </strong> 

<p><%# DataBinder.Eval(Container.DataItem, "Content")%></p> 
</div> 
</div> 
</ItemTemplate>
</asp:Repeater>

</div> 
</div> 

<div class="right"> 

<div class="list_holder2"> <h1 class="headline"> SPEAKING ENGAGEMENTS </h1> 
<ul class="less_margin"> 
<asp:Repeater runat="server" ID="Speaking">
<ItemTemplate>
<li> <a > "<%# DataBinder.Eval(Container.DataItem, "Headline")%>" </a> <strong> <%# DataBinder.Eval(Container.DataItem, "Contact")%> <br/> <%# DataBinder.Eval(Container.DataItem, "Event_Date")%>  <br/> </strong> 

<p> <%# DataBinder.Eval(Container.DataItem, "Content")%> </p> </li> 
</ItemTemplate>
</asp:Repeater>
</ul> 
</div> 
</div>
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    <!--</form>--> 
</body>
</html>
