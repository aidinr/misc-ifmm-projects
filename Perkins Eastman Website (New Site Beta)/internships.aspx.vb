﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class careers
    Inherits System.Web.UI.Page
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)

        GenerateCareers()
        GeneratePromotingLeaderShip()
        GenerateInternship()

        'GenerateCareers2()
        'GeneratePromotingLeaderShip2()
        'GenerateInternship2()

    End Sub

    Sub GenerateCareers()

        Dim sql As String = "select *, b.name folderName from IPM_ASSET_CATEGORY b, IPM_ASSET a  left join ipm_asset_field_value c on c.item_id = @websiteDescriptionUDFID and c.asset_id = a.asset_id where a.ProjectID = @projectid and b.CATEGORY_ID = a.Category_ID and b.NAME = @folderName and a.available = 'Y'"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim projectIDParameter As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(projectIDParameter)
        MyCommand1.SelectCommand.Parameters("@projectid").Value = ConfigurationSettings.AppSettings("careersProjectID")

        Dim folderNameParameter As New SqlParameter("@folderName", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(folderNameParameter)
        MyCommand1.SelectCommand.Parameters("@folderName").Value = ConfigurationSettings.AppSettings("careersFolderName").ToString.Trim


        Dim websiteDescriptionParameter As New SqlParameter("@websiteDescriptionUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(websiteDescriptionParameter)
        'MyCommand1.SelectCommand.Parameters("@websiteDescriptionUDFID").Value = ConfigurationSettings.AppSettings("websiteContentUDFID")
        MyCommand1.SelectCommand.Parameters("@websiteDescriptionUDFID").Value = 2400398

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then
            Try

                literalTitle.Text = dt1.Rows(0)("foldername")
                literalContent.Text = dt1.Rows(0)("item_value")

            Catch ex As Exception
                literalContent.Text = "This is placeholder content.  The data was forked for development purposes and some data is not available in the development environment.  The real conent will be displayed when the site goes live."

            End Try
        End If


    End Sub

    Sub GeneratePromotingLeaderShip()

        Dim sql As String = "select *, b.name folderName from IPM_ASSET_CATEGORY b, IPM_ASSET a  left join ipm_asset_field_value c on c.item_id = @websiteDescriptionUDFID and c.asset_id = a.asset_id where a.ProjectID = @projectid and b.CATEGORY_ID = a.Category_ID and b.NAME = @folderName and a.available = 'Y'"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim projectIDParameter As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(projectIDParameter)
        MyCommand1.SelectCommand.Parameters("@projectid").Value = ConfigurationSettings.AppSettings("careersProjectID")

        Dim folderNameParameter As New SqlParameter("@folderName", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(folderNameParameter)
        MyCommand1.SelectCommand.Parameters("@folderName").Value = ConfigurationSettings.AppSettings("promotingLeadershipFolderName").ToString.Trim

        Dim websiteDescriptionParameter As New SqlParameter("@websiteDescriptionUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(websiteDescriptionParameter)
        'MyCommand1.SelectCommand.Parameters("@websiteDescriptionUDFID").Value = ConfigurationSettings.AppSettings("websiteContentUDFID")
        MyCommand1.SelectCommand.Parameters("@websiteDescriptionUDFID").Value = 2400398

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then

            For Each row As DataRow In dt1.Rows
                If (row("media_type") = "10") Then
                    literalPromotingLeadershipImage.Text = "<img src=""" & Session("WSRetrieveAsset") & "type=asset&size=1&width=150&height=210&crop=1&id=" & row("asset_id") & """ alt="""" />"
                Else
                    Try
                        literalPromotingLeadership.Text = row("item_value")

                    Catch ex As Exception

                        literalPromotingLeadership.Text = "This is placeholder content.  The data was forked for development purposes and some data is not available in the development environment.  The real conent will be displayed when the site goes live."

                    End Try
                End If

            Next
        End If


    End Sub

    Sub GenerateInternship()

        Dim sql As String = "select *, b.name folderName from IPM_ASSET_CATEGORY b, IPM_ASSET a  left join ipm_asset_field_value c on c.item_id = @websiteDescriptionUDFID and c.asset_id = a.asset_id where a.ProjectID = @projectid and b.CATEGORY_ID = a.Category_ID and b.NAME = @folderName and a.available = 'Y'"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim projectIDParameter As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(projectIDParameter)
        MyCommand1.SelectCommand.Parameters("@projectid").Value = ConfigurationSettings.AppSettings("careersProjectID")

        Dim folderNameParameter As New SqlParameter("@folderName", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(folderNameParameter)
        MyCommand1.SelectCommand.Parameters("@folderName").Value = ConfigurationSettings.AppSettings("internshipFolderName").ToString.Trim

        Dim websiteDescriptionParameter As New SqlParameter("@websiteDescriptionUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(websiteDescriptionParameter)
        'MyCommand1.SelectCommand.Parameters("@websiteDescriptionUDFID").Value = ConfigurationSettings.AppSettings("websiteContentUDFID")
        MyCommand1.SelectCommand.Parameters("@websiteDescriptionUDFID").Value = 2400398

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)



        If (dt1.Rows.Count > 0) Then

            For Each row As DataRow In dt1.Rows
                If (row("media_type") = "10") Then
                    literalInternshipImage.Text = "<img src=""" & Session("WSRetrieveAsset") & "type=asset&size=1&width=150&height=210&crop=1&id=" & row("asset_id") & """ alt="""" />"
                Else
                    Try
                        literalInternship.Text = row("item_value")

                    Catch ex As Exception

                        literalInternship.Text = "This is placeholder content.  The data was forked for development purposes and some data is not available in the development environment.  The real conent will be displayed when the site goes live."

                    End Try
                End If

            Next
        End If


    End Sub

  
End Class
