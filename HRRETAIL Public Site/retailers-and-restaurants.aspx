﻿<%@ Page Title="" Language="VB" MasterPageFile="~/HRMaster1.master" AutoEventWireup="false" CodeFile="retailers-and-restaurants.aspx.vb" Inherits="retailers_and_restaurants" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

		<!-- Page Content START -->
		<div class="page-content">
			<div class="top-section fluid-padding less-padding">
				<ul class="breadcrumbs">
					<li><a href="/">Home</a> &gt;</li>
					<li><span>Retailers &amp; Restaurants</span></li>
				</ul>
			</div>



            <div class="top-section retailer-search fluid-padding purple-gradient">
                <div class="left">
                    <h1>Retail Search</h1>
                    <p class="layout-buttons">
                     <a class="list-view no">grid view</a>
                    </p>
                </div>
                    <div class="top-part">
                        <div id="properties-auto" class="auto">
                            <input type="text" id="retailers-and-restaurants-q" class="q" value="" alt="Search by retailer or restaurant name"/>
                            <input type="button" class="search retailers-and-restaurants-submit" value="search"/>
                        </div>
                    </div>
		                <div class="RetailSelects">
					<select name="Type_of_Retailer" id="Type_of_Retailer" class="styled-select">
						<option value="">Any Type</option>
					</select>
					<select name="Broker" id="Broker" class="styled-select">
						<option value="">Any Broker</option>
					</select>
		                </div>
                <br class="clear" />
            </div>






			<div class="fluid-padding ">			
                        <asp:Literal runat="server" ID="tenants_lbl"/>
			</div>

		</div>
		<!-- Page Content END -->

</asp:Content>
