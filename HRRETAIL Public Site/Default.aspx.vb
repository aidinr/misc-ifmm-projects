﻿Imports System.Data

Partial Class _Default
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CarouselSQL As String = ""
        Dim CarouselOUT As String = ""
        CarouselSQL &= "SELECT "
        CarouselSQL &= "CI.Asset_ID,  "
        CarouselSQL &= "P.ProjectID,  "
        CarouselSQL &= "P.Name, "
        CarouselSQL &= "P.Address, "
        CarouselSQL &= "F1.Item_Value AS IDAM_PROPERTYADDRESS, "
        CarouselSQL &= "F2.Item_Value AS IDAM_COUNTY, "
        CarouselSQL &= "F3.Item_Value AS IDAM_PROPDECRIPTION, "
        CarouselSQL &= "F4.Item_Value AS IDAM_PROPCONTACT, "
        CarouselSQL &= "F5.Item_Value AS IDAM_CLIENT, "
        CarouselSQL &= "F6.Item_Value AS IDAM_STATUS, "
        CarouselSQL &= "F7.Item_Value AS IDAM_PAGETITLE, "
        CarouselSQL &= "F8.Item_Value AS IDAM_PAGEURL, "
        CarouselSQL &= "F9.Item_Value AS IDAM_LATLONG, "
        CarouselSQL &= "F10.Item_Value AS IDAM_PROP_SERVICE, "
        CarouselSQL &= "F11.Item_Value AS IDAM_URL, "
        CarouselSQL &= "F12.Item_Value AS IDAM_PROJVID, "
        CarouselSQL &= "F13.Item_Value AS IDAM_PAGEDESC, "
        CarouselSQL &= "F14.Item_Value AS IDAM_PAGEMETADATAKEYWORDS, "
        CarouselSQL &= "F15.Item_Value AS idam_homecartext, "
        CarouselSQL &= "F16.Item_Value AS idam_homecarred, "
        CarouselSQL &= "F17.Item_Value AS idam_homecarline2 "
        CarouselSQL &= "FROM IPM_CARROUSEL_ITEM CI  "
        CarouselSQL &= "JOIN IPM_ASSET PA ON PA.Asset_ID = CI.Asset_ID AND CI.Available = 'Y'  "
        CarouselSQL &= "JOIN IPM_PROJECT P ON P.ProjectID = PA.ProjectID  "
        CarouselSQL &= "LEFT JOIN IPM_CARROUSEL_SORT SR on SR.Carrousel_ID = CI.Carrousel_ID  "
        CarouselSQL &= "LEFT JOIN IPM_CARROUSEL_ITEM_SORT SI on SI.Sort_ID = SR.Sort_ID and SI.Item_ID = PA.Asset_ID  "
        CarouselSQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F1 on P.ProjectID = F1.ProjectID and F1.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROPERTYADDRESS')  "
        CarouselSQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F2 on P.ProjectID = F2.ProjectID and F2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_COUNTY')  "
        CarouselSQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F3 on P.ProjectID = F3.ProjectID and F3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROPDECRIPTION')  "
        CarouselSQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F4 on P.ProjectID = F4.ProjectID and F4.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROPCONTACT')  "
        CarouselSQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F5 on P.ProjectID = F5.ProjectID and F5.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_CLIENT')  "
        CarouselSQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F6 on P.ProjectID = F6.ProjectID and F6.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_STATUS')  "
        CarouselSQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F7 on P.ProjectID = F7.ProjectID and F7.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PAGETITLE')  "
        CarouselSQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F8 on P.ProjectID = F8.ProjectID and F8.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PAGEURL')  "
        CarouselSQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F9 on P.ProjectID = F9.ProjectID and F9.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_LATLONG')  "
        CarouselSQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F10 on P.ProjectID = F10.ProjectID and F10.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROP_SERVICE')  "
        CarouselSQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F11 on P.ProjectID = F11.ProjectID and F11.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_URL')  "
        CarouselSQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F12 on P.ProjectID = F12.ProjectID and F12.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJVID')  "
        CarouselSQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F13 on P.ProjectID = F13.ProjectID and F13.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PAGEDESC')  "
        CarouselSQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F14 on P.ProjectID = F14.ProjectID and  F14.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PAGEMETADATAKEYWORDS')  "
        CarouselSQL &= "LEFT JOIN IPM_ASSET_FIELD_VALUE F15 on PA.Asset_ID = F15.ASSET_ID and F15.Item_ID = (Select Item_ID From IPM_ASSET_FIELD_DESC where Item_Tag = 'idam_homecartext') "
        CarouselSQL &= "LEFT JOIN IPM_ASSET_FIELD_VALUE F16 on PA.Asset_ID = F16.ASSET_ID and F16.Item_ID = (Select Item_ID From IPM_ASSET_FIELD_DESC where Item_Tag = 'idam_homecarred') "
        CarouselSQL &= "LEFT JOIN IPM_ASSET_FIELD_VALUE F17 on PA.Asset_ID = F17.ASSET_ID and F17.Item_ID = (Select Item_ID From IPM_ASSET_FIELD_DESC where Item_Tag = 'idam_homecarline2') "
        CarouselSQL &= "WHERE CI.Carrousel_ID = 2  "
        CarouselSQL &= "and CI.Available = 'Y' AND CI.Available_Date <= getdate()  "
        CarouselSQL &= "ORDER BY SI.Sort_Value "
        Dim CarouselDT As DataTable
        CarouselDT = New DataTable("FuturedProjectDT")
        CarouselDT = DBFunctions.GetDataTable(CarouselSQL)
        If CarouselDT.Rows.Count > 0 Then
            CarouselOUT &= "<ul>"
            For R As Integer = 0 To CarouselDT.Rows.Count - 1
                Dim PrFriendlyLink As String = "project_" & CarouselDT.Rows(R)("ProjectID").ToString.Trim & "_" & CMSFunctions.FormatFriendlyUrl(CarouselDT.Rows(R)("Name").ToString.Trim.ToLower, "_")
                CarouselOUT &= "<li>"
                CarouselOUT &= "<p>"
                CarouselOUT &= "<img src=""/dynamic/image/week/asset/liquid/x683/95/ffffff/Center/" & CarouselDT.Rows(R)("Asset_ID").ToString.Trim & ".jpg"" alt=""""/>"
                CarouselOUT &= "</p>"
                CarouselOUT &= "<div class=""info"">"
                CarouselOUT &= "<div class=""purple-ghost-box p10"">"
                CarouselOUT &= "" & Replace(formatfunctionssimple.AutoFormatText(CarouselDT.Rows(R)("idam_homecartext").ToString.Trim.ToLower), CarouselDT.Rows(R)("idam_homecarred").ToString.Trim.ToLower, "<span class=""red"">" & CarouselDT.Rows(R)("idam_homecarred").ToString.Trim & "</span>")
                CarouselOUT &= "</div>"
                CarouselOUT &= "<div class=""pink-ghost-box p10"">"
                CarouselOUT &= "" & CarouselDT.Rows(R)("idam_homecarline2").ToString.Trim & ""
                CarouselOUT &= "</div>"
                CarouselOUT &= "</div>"
                CarouselOUT &= "</li>"
            Next
                CarouselOUT &= "</ul>"
                carousel_images.Text = CarouselOUT
        End If

        Dim EmptyProjectFilters As New ProjectFiltersStructure

        case_studies.Text = IDAMFunctions.PrintProjects("2410606", 0, 8, "Case Studies Home", EmptyProjectFilters)
        news_slider1.Text = IDAMFunctions.PrintNews(0, 10, "Home")
        tenants_lis1.Text = IDAMFunctions.PrintTeants(0, 10, True, "Home Page")


    End Sub

    'Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
    '    Dim stringWriter As New System.IO.StringWriter()
    '    Dim htmlWriter As New HtmlTextWriter(stringWriter)
    '    MyBase.Render(htmlWriter)
    '    Dim html As String = stringWriter.ToString()
    '    html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
    '    writer.Flush()
    '    writer.Write(html)
    '    If Master.OriginalUrlPage.Contains("?") = False Then
    '        CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
    '    Else
    '        CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
    '    End If
    'End Sub


End Class
