﻿Imports System.Drawing
Public Class StaticGoogleMap

    Public Const GOOGLE_STATIC_MAP_URL As String = "http://maps.googleapis.com/maps/api/staticmap?"

    Private _Markers As New MarkerList

    Public Property Markers() As MarkerList
        Get
            Return _Markers
        End Get
        Set(ByVal value As MarkerList)
            _Markers = value
        End Set
    End Property

    Public Function GenerateMarkers() As String
        ' If we have markers, process.
        If _Markers.Count > 0 Then
            Dim strMarkerText As String = ""
            For Each objMarker As Marker In _Markers
                strMarkerText += "&" + objMarker.GenerateMarkerURLText
            Next
            Return strMarkerText
        Else
            ' If there are no markers added return an empty string.
            Return ""
        End If
    End Function

    Private _Sensor As Boolean = False
    Public Property Sensor() As Boolean
        Get
            Return _Sensor
        End Get
        Set(ByVal value As Boolean)
            _Sensor = value
        End Set
    End Property

    Private _LatLng As String = Nothing
    Public Property LatLng() As String
        Get
            Return _LatLng
        End Get
        Set(ByVal value As String)
            _LatLng = value
        End Set
    End Property

    Public Function SetUKPosition() As Boolean
        Try
            SetLatLng("53.7617", "-3.054")
            Zoom = 6
            ImageWidth = 640
            ImageHeight = 640
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Sub SetLatLng(ByVal gLat As String, ByVal gLng As String)
        _LatLng = gLat + "," + gLng
    End Sub

    Private _Address As String = Nothing
    Public Property Address() As String
        Get
            Return _Address
        End Get
        Set(ByVal value As String)
            _Address = value
        End Set
    End Property

    Private _Zoom As Integer = 6
    Public Property Zoom() As Integer
        Get
            Return _Zoom
        End Get
        Set(ByVal value As Integer)
            _Zoom = value
        End Set
    End Property

    Private _ImageHeight As Integer = 320
    Private _ImageWidth As Integer = 320

    Public Property ImageHeight() As Integer
        Get
            Return _ImageHeight
        End Get
        Set(ByVal value As Integer)
            _ImageHeight = value
        End Set
    End Property

    Public Property ImageWidth() As Integer
        Get
            Return _ImageWidth
        End Get
        Set(ByVal value As Integer)
            _ImageWidth = value
        End Set
    End Property

    Public Enum ImageFormats
        PNG8 = 0
        PNG32 = 1
        GIF = 2
        JPG = 3
        JPG_BASELINE = 4
    End Enum

    Private _ImageFormat As ImageFormats = ImageFormats.PNG8
    Public Property ImageFormat() As ImageFormats
        Get
            Return _ImageFormat
        End Get
        Set(ByVal value As ImageFormats)
            _ImageFormat = value
        End Set
    End Property

    Public Enum MapTypes
        ROADMAP = 0
        SATELLITE = 1
        TERRAIN = 2
        HYBRID = 3
    End Enum

    Private _MapType As MapTypes = MapTypes.ROADMAP
    ''' <summary>
    ''' Specifies the type of map you would like to return. Default is road map.
    ''' </summary>
    ''' <value>MapType</value>
    ''' <returns>MapType</returns>
    ''' <remarks></remarks>
    Public Property MapType() As MapTypes
        Get
            Return _MapType
        End Get
        Set(ByVal value As MapTypes)
            _MapType = value
        End Set
    End Property

    ''' <summary>
    ''' Generates a static map URL
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Public Function GenerateMap() As String
        ' Create a parameters string
        Dim strParams As String = ""
        ' Calculate whether we need a centre specifying.
        If Not _LatLng Is Nothing Or Not _Address Is Nothing Then
            strParams += IIf(strParams.Trim <> "", "&", "") + "center=" + IIf(_LatLng Is Nothing, _Address, _LatLng)
        End If
        ' Calculate the zoom
        If Not _Zoom = Nothing Then
            strParams += IIf(strParams.Trim <> "", "&", "") + "zoom=" + _Zoom.ToString
        End If
        ' Set the sensor
        strParams += IIf(strParams.Trim <> "", "&", "") + "sensor=" + _Sensor.ToString.ToLower
        ' Set the height / width of the image
        strParams += IIf(strParams.Trim <> "", "&", "") + "size=" + _ImageWidth.ToString + "x" + _ImageHeight.ToString
        ' Set the image type
        Select Case _ImageFormat
            Case ImageFormats.GIF
                strParams += IIf(strParams.Trim <> "", "&", "") + "format=gif"
            Case ImageFormats.JPG
                strParams += IIf(strParams.Trim <> "", "&", "") + "format=jpg"
            Case ImageFormats.JPG_BASELINE
                strParams += IIf(strParams.Trim <> "", "&", "") + "format=jpg-baseline"
            Case ImageFormats.PNG32
                strParams += IIf(strParams.Trim <> "", "&", "") + "format=png32"
        End Select
        'strParams += Replace("&apistyle=s.t:6|s.e:g|p.h:#909090|p.s:-100|p.l:-26|p.v:on,s.t:5|p.h:#dfdfdf|p.s:-100|p.l:-2|p.v:simplified,s.t:49|p.h:#959595|p.s:-100|p.l:-9|p.v:simplified,s.t:82|p.h:#e1e0e0|p.s:-89|p.l:-7|p.v:on,s.t:2|p.h:#e1e0e0|p.s:-96|p.l:46|p.v:on,s.t:50|p.h:#c7c7c7|p.s:-100|p.l:5|p.v:simplified", "|", "%7C", 1, -1, 1)
        ' Set the map type
        Select Case _MapType
            Case MapTypes.HYBRID
                strParams += IIf(strParams.Trim <> "", "&", "") + "maptype=hybrid"
            Case MapTypes.SATELLITE
                strParams += IIf(strParams.Trim <> "", "&", "") + "maptype=satellite"
            Case MapTypes.TERRAIN
                strParams += IIf(strParams.Trim <> "", "&", "") + "maptype=terrain"
        End Select

        strParams += "&style=saturation:-100|hue:0xaaaaaa"
        'MsgBox(GOOGLE_STATIC_MAP_URL + strParams + GenerateMarkers())

        Return GOOGLE_STATIC_MAP_URL + strParams + GenerateMarkers()

    End Function

    Public Function GenerateMapAsImage() As Bitmap
        Dim strMapURL As String = GenerateMap()
        Dim objImg As Bitmap = New Bitmap(New IO.MemoryStream(New System.Net.WebClient().DownloadData(strMapURL)))
        Return objImg
    End Function

End Class
Public Class MarkerList
    Inherits List(Of Marker)
End Class
Public Class Marker

    Public Enum MarkerSize
        TINY = 0
        SMALL = 1
        MID = 2
        NORMAL = 3
    End Enum

    Public Enum MarkerType
        [DEFAULT] = 0
        CUSTOM = 1
    End Enum

    Public Enum MarkerColour
        CUSTOM = 0
        BLACK = 1
        BROWN = 2
        GREEN = 3
        PURPLE = 4
        YELLOW = 5
        BLUE = 6
        GRAY = 7
        ORANGE = 8
        RED = 9
        WHITE = 10
        NONE = 11
    End Enum

    Public Enum MarkerLabel
        A = 1
        B = 2
        C = 3
        D = 4
        E = 5
        F = 6
        G = 7
        H = 8
        I = 9
        J = 10
        K = 11
        L = 12
        M = 13
        N = 14
        O = 15
        P = 16
        Q = 17
        R = 18
        S = 19
        T = 20
        U = 21
        V = 22
        W = 23
        X = 24
        Y = 25
        Z = 26
        NUM_0 = 27
        NUM_1 = 28
        NUM_2 = 29
        NUM_3 = 30
        NUM_4 = 31
        NUM_5 = 32
        NUM_6 = 33
        NUM_7 = 34
        NUM_8 = 35
        NUM_9 = 36
    End Enum

    Private _Size As MarkerSize = MarkerSize.NORMAL
    Public Property Size() As MarkerSize
        Get
            Return _Size
        End Get
        Set(ByVal value As MarkerSize)
            _Size = value
        End Set
    End Property

    Private _Colour As MarkerColour = MarkerColour.BLUE
    Public Property Colour() As MarkerColour
        Get
            Return _Colour
        End Get
        Set(ByVal value As MarkerColour)
            _Colour = value
        End Set
    End Property

    Private _CustomColor As String
    ''' <summary>
    ''' 24 bit colour (example: color=0xFFFFCC)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CustomColor() As String
        Get
            Return _CustomColor
        End Get
        Set(ByVal value As String)
            _CustomColor = value
        End Set
    End Property

    Private _Label As MarkerLabel = Nothing
    Public Property Label() As MarkerLabel
        Get
            Return _Label
        End Get
        Set(ByVal value As MarkerLabel)
            _Label = value
        End Set
    End Property

    Private _MarkerURL As String = ""
    Public Property MarkerURL() As String
        Get
            Return _MarkerURL
        End Get
        Set(ByVal value As String)
            _MarkerURL = value
        End Set
    End Property

    Private _MarkerShadow As Boolean = True
    Public Property MarkerShadow() As Boolean
        Get
            Return _MarkerShadow
        End Get
        Set(ByVal value As Boolean)
            _MarkerShadow = value
        End Set
    End Property

    Private _Type As MarkerType = MarkerType.DEFAULT
    Public Property Type() As MarkerType
        Get
            Return _Type
        End Get
        Set(ByVal value As MarkerType)
            _Type = value
        End Set
    End Property

    Private _MarkerLocation As String
    Public Property MarkerLocation() As String
        Get
            Return _MarkerLocation
        End Get
        Set(ByVal value As String)
            _MarkerLocation = value
        End Set
    End Property

    Private Function GetMarkerImageType() As String
        ' Locate the type of icon we're trying to generate first.
        Select Case _Type
            Case MarkerType.CUSTOM
                Return "icon:" + EncodeCustomURL(MarkerURL)
            Case Else
                Return ""
        End Select

    End Function

    Private Function GetMarkerColor() As String
        Select Case Colour
            Case MarkerColour.BLACK
                Return "color:black"
            Case MarkerColour.BLUE
                Return "color:blue"
            Case MarkerColour.BROWN
                Return "color:brown"
            Case MarkerColour.CUSTOM
                Return "color:" + CustomColor
            Case MarkerColour.GRAY
                Return "color:gray"
            Case MarkerColour.GREEN
                Return "color:green"
            Case MarkerColour.NONE
                Return ""
            Case MarkerColour.ORANGE
                Return "color:orange"
            Case MarkerColour.PURPLE
                Return "color:purple"
            Case MarkerColour.RED
                Return "color:red"
            Case MarkerColour.WHITE
                Return "color:white"
            Case MarkerColour.YELLOW
                Return "color:yellow"
        End Select
    End Function

    Private Function GetMarkerSize() As String
        Select Case Size
            Case MarkerSize.TINY
                Return "size:tiny"
            Case MarkerSize.SMALL
                Return "size:small"
            Case MarkerSize.MID
                Return "size:mid"
            Case Else
                Return ""
        End Select
    End Function

    Private Function GetMarkerLabel() As String
        Select Case Label
            Case MarkerLabel.A
                Return "A"
            Case MarkerLabel.B
                Return "B"
            Case MarkerLabel.C
                Return "C"
            Case MarkerLabel.D
                Return "D"
            Case MarkerLabel.E
                Return "E"
            Case MarkerLabel.F
                Return "F"
            Case MarkerLabel.G
                Return "G"
            Case MarkerLabel.H
                Return "H"
            Case MarkerLabel.I
                Return "I"
            Case MarkerLabel.J
                Return "J"
            Case MarkerLabel.K
                Return "K"
            Case MarkerLabel.L
                Return "L"
            Case MarkerLabel.M
                Return "M"
            Case MarkerLabel.N
                Return "N"
            Case MarkerLabel.O
                Return "O"
            Case MarkerLabel.P
                Return "P"
            Case MarkerLabel.Q
                Return "Q"
            Case MarkerLabel.R
                Return "R"
            Case MarkerLabel.S
                Return "S"
            Case MarkerLabel.T
                Return "T"
            Case MarkerLabel.U
                Return "U"
            Case MarkerLabel.V
                Return "V"
            Case MarkerLabel.W
                Return "W"
            Case MarkerLabel.X
                Return "X"
            Case MarkerLabel.Y
                Return "Y"
            Case MarkerLabel.Z
                Return "Z"
            Case MarkerLabel.NUM_0
                Return "0"
            Case MarkerLabel.NUM_1
                Return "1"
            Case MarkerLabel.NUM_2
                Return "2"
            Case MarkerLabel.NUM_3
                Return "3"
            Case MarkerLabel.NUM_4
                Return "4"
            Case MarkerLabel.NUM_5
                Return "5"
            Case MarkerLabel.NUM_6
                Return "6"
            Case MarkerLabel.NUM_7
                Return "7"
            Case MarkerLabel.NUM_8
                Return "8"
            Case MarkerLabel.NUM_9
                Return "9"
            Case Else
                Return ""
        End Select

    End Function

    Private Function EncodeCustomURL(ByVal strURL As String) As String
        Dim strTmpURL As String = strURL
        strTmpURL = Replace(strTmpURL, "|", "%257C", 1, -1, 1)
        strTmpURL = Replace(strTmpURL, "&", "%26", 1, -1, 1)
        Return strTmpURL
    End Function

    Public Function GenerateMarkerURLText() As String
        Dim strMarkerURLText As String = "markers="
        strMarkerURLText += IIf(GetMarkerSize() <> "", IIf(strMarkerURLText <> "markers=", "|", "") + GetMarkerSize(), "")
        strMarkerURLText += IIf(GetMarkerImageType() <> "", IIf(strMarkerURLText <> "markers=", "|", "") + GetMarkerImageType(), "")
        strMarkerURLText += IIf(GetMarkerLabel() <> "", IIf(strMarkerURLText <> "markers=", "|", "") + "label:" + GetMarkerLabel(), "")
        strMarkerURLText += IIf(MarkerShadow = False, IIf(strMarkerURLText <> "markers=", "|", "") + "shadow:false", "")
        strMarkerURLText += IIf(GetMarkerColor() <> "", IIf(strMarkerURLText <> "markers=", "|", "") + GetMarkerColor(), "")
        strMarkerURLText += IIf(Label <> Nothing, IIf(strMarkerURLText <> "markers=", "|", "") + "", "")
        strMarkerURLText += IIf(strMarkerURLText <> "markers=", "|", "") + MarkerLocation
        Return Replace(strMarkerURLText, "|", "%7C", 1, -1, 1)
    End Function

End Class