Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.VisualBasic

Public Class formatfunctions

    Public Shared Function AutoFormatText(ByVal input As String) As String

        input = HttpUtility.HtmlEncode(input)


' HTML List
input =	RegularExpressions.Regex.Replace(input, "^- (?=[0-9a-zA-Z])([^\n]*)", "<span class=""SpecialList""> $1 </span>",RegexOptions.Multiline)
input =	RegularExpressions.Regex.Replace(input, "^ - (?=[0-9a-zA-Z])([^\n]*)", "<span class=""SpecialList2""> $1 </span>",RegexOptions.Multiline)
input =	RegularExpressions.Regex.Replace(input, "^  - (?=[0-9a-zA-Z])([^\n]*)", "<span class=""SpecialList3""> $1 </span>",RegexOptions.Multiline)

' Phone Numbers
input = RegularExpressions.Regex.Replace(input, "\b(?:(?<=\d{3})-(?=\d{4})|(?<=\d{3})-(?=\d{3}))\b", ".")  ' Replace delimited phone numbers
input = RegularExpressions.Regex.Replace(input, "\b(\d{3})(\d{3})(\d{4})\b", "$1.$2.$3")  ' Replace non-delimited phone numbers
input = RegularExpressions.Regex.Replace(input, "\((\d{3})\)\s*(?=\d{3})", "$1.")  ' Replace parenthesized area codes

' Bold Heading
input = RegularExpressions.Regex.Replace(input, "(^|\n)(?=.{1,100}\n)([A-Z][A-Za-z]*(?:(?: [a-zA-Z0-9]+)* [A-Z][A-Za-z\?\!]*)?)(\r\n\r\n)", "$1<strong>$2</strong>$3")

' BB Links
' {[link=url]}text{[/link]}
input = RegularExpressions.Regex.Replace(input, "\{\[link=([^\]]+)\]\}([^\]]+)\{\[\/link\]\}", "<a targete=""_blank"" href=""$1"">$2</a>")

' Links
input = RegularExpressions.Regex.Replace(input, "(?<!\S)(https?://\S+[^\s@,.""']+)", "<a target=""_blank"" href=""$1"">$1</a>")
input = RegularExpressions.Regex.Replace(input, "(?<!\S)(\www\.\S+[^\s@,.""']+)", "<a target=""_blank"" href=""http://$1"">$1</a>")

' Email Addresses
input = RegularExpressions.Regex.Replace(input, "(?<!\S)\w[\w\.]*\w?@[\w\.]+\w", "<a href=""mailto:$0"">$0</a>")


' Twitter
input = RegularExpressions.Regex.Replace(input, "(?<!\S)\((@)(\S+[^\s\)]+)\)", "(<a href=""http://twitter.com/$2"">$1$2</a>)")

' Google Maps
input = RegularExpressions.Regex.Replace(input, "(?<!\S){([-+]?[0-9]*\.?[0-9]+), ?([-+]?[0-9]*\.?[0-9]+)}", "<a target=""_blank"" href=""https://maps.google.com/?q=$1,$2&z=10"">Map</a>")

' Introductory Statement (Bold Before Colon)
' input = System.Text.RegularExpressions.Regex.Replace(input, "^([\w\d ]*:\s)", "<strong>$1</strong>", System.Text.RegularExpressions.RegexOptions.Multiline)

' Spaces
input = RegularExpressions.Regex.Replace(input, "  ", " &nbsp;")

' Line Breaks
input = RegularExpressions.Regex.Replace(input, VbNewLine, VbNewLine +"<br />")

' Dash Symbol
input = RegularExpressions.Regex.Replace(input,"-- ", "&mdash; ")

' Bullet Symbol
input = RegularExpressions.Regex.Replace(input, "\{\-\}", "&#8226;")

' Copyright Symbol
input = RegularExpressions.Regex.Replace(input, "\{[cC]\}", "&#169;")

' Trademark Symbol
input = RegularExpressions.Regex.Replace(input, "\{[tT][mM]\}", "&#8482;")

' Registered Trademark Symbol
input = RegularExpressions.Regex.Replace(input, "\{[rR]\}", "&#174;")

' Fix Double Entities
input = RegularExpressions.Regex.Replace(input, "(?<=&)amp;(?=#?\w+;)", String.Empty)

        Return input
    End Function

    Public Shared Function AutoFormatTextList(ByVal input As String) As String
        input = RegularExpressions.Regex.Replace(input, vbNewLine, vbNewLine + "")
        Return input
    End Function

End Class