﻿Imports PdfSharp
Imports PdfSharp.Drawing
Imports PdfSharp.Pdf
Imports PdfSharp.Drawing.Layout
Imports System.Diagnostics
Imports System.Drawing
Imports System.Data


Imports System.IO
Imports System.Net

Public Class ExportPDF

    Dim XStartPoint As Integer = 0
    Dim YStartPoint As Integer = 0
    Dim LineSpace As Integer = 0

    '---------Fonts

    Dim FontOpenSansLightRegular8 As New XFont("Open Sans Light", 7, XFontStyle.Regular)

    Dim FontOpenSansRegular8 As New XFont("Open Sans", 8, XFontStyle.Regular)
    Dim FontOpenSansBold8 As New XFont("Open Sans", 8, XFontStyle.Bold)

    Dim FontOpenSansRegular10 As New XFont("Open Sans", 10, XFontStyle.Regular)
    Dim FontOpenSansBold10 As New XFont("Open Sans", 10, XFontStyle.Bold)

    Dim FontOpenSansRegular12 As New XFont("Open Sans", 12, XFontStyle.Regular)
    Dim FontOpenSansBold12 As New XFont("Open Sans", 12, XFontStyle.Bold)

    Dim FontOpenSansRegular14 As New XFont("Open Sans", 14, XFontStyle.Regular)
    Dim FontOpenSansRegular16 As New XFont("Open Sans", 16, XFontStyle.Regular)
    Dim FontOpenSansRegular18 As New XFont("Open Sans", 18, XFontStyle.Regular)
    Dim FontOpenSansRegular28 As New XFont("Open Sans", 28, XFontStyle.Regular)
    Dim FontOpenSansRegular30 As New XFont("Open Sans", 30, XFontStyle.Regular)
    '---------End Fonts


    Dim PDFDocument As PdfDocument
    Dim PDFPage As PdfPage
    Dim PDFGraphics As XGraphics
    Dim PDFPen As XPen
    Dim PDFTextFormatter As XTextFormatter
    Dim PDFStringFormat As XStringFormat
    Public ProjectData As New ProjectStructure

    Public Sub GetPDF(ByVal PropertyId As Integer)

        ProjectData = IDAMFunctions.GetProjectDetails(PropertyId)
        '---------Property Data
        Dim ClientName As String = "HR RETAIL"
        Dim ClientLogoBMP As Bitmap
        Dim PropertyTitle As String = ProjectData.ProjectTitle.ToUpper
        Dim PropertyAddress As String = ProjectData.ProjectAddress
        '---------End Property Data

        PDFDocument = New PdfDocument
        PDFDocument.Options.NoCompression = True
        PDFDocument.Options.CompressContentStreams = False
        PDFDocument.Info.Title = ClientName & " - " & PropertyTitle
        PDFDocument.Info.Subject = ClientName & " - " & PropertyTitle

         Dim FirstPageHeaderHeight As Integer = 0
        Dim NextPagesHeaderHeight As Integer = 45

        NewPDFPage_Template1(PropertyTitle, PropertyAddress)

        Dim LeasingAgentDT As New DataTable
        LeasingAgentDT = IDAMFunctions.GetProjectContacts(PropertyId, 0)

        XStartPoint = 25
        YStartPoint = 125

        If LeasingAgentDT.Rows.Count > 1 Then
            PDFGraphics.DrawString("LEASING AGENTS", FontOpenSansRegular14, XBrushes.Gray, XStartPoint, YStartPoint, XStringFormats.TopLeft)
        Else
            PDFGraphics.DrawString("LEASING AGENT", FontOpenSansRegular14, XBrushes.Gray, XStartPoint, YStartPoint, XStringFormats.TopLeft)
        End If

        XStartPoint = 25
        YStartPoint = 135
        LineSpace = 15
        Dim AgentSpace As Integer = 0
        Dim NewAgent As Integer = 0
        Dim CountAgents As Integer = 0
        For R As Integer = 0 To LeasingAgentDT.Rows.Count - 1
            CountAgents = CountAgents + 1
            If R = 0 Then
                PDFGraphics.DrawString(LeasingAgentDT.Rows(R).Item("FirstName").ToString.Trim & " " & LeasingAgentDT.Rows(R).Item("LastName").ToString.Trim, FontOpenSansRegular12, New XSolidBrush(XColor.FromArgb(255, 239, 61, 67)), XStartPoint, YStartPoint + 1 * LineSpace, XStringFormats.TopLeft)
                PDFGraphics.DrawString(LeasingAgentDT.Rows(R).Item("Email").ToString.Trim, FontOpenSansRegular12, XBrushes.Gray, XStartPoint, YStartPoint + 2 * LineSpace, XStringFormats.TopLeft)
                PDFGraphics.DrawString(LeasingAgentDT.Rows(R).Item("Cell").ToString.Trim & " " & LeasingAgentDT.Rows(R).Item("LastName").ToString.Trim, FontOpenSansRegular12, XBrushes.Gray, XStartPoint, YStartPoint + 3 * LineSpace, XStringFormats.TopLeft)
            Else
                PDFGraphics.DrawString(LeasingAgentDT.Rows(R).Item("FirstName").ToString.Trim & " " & LeasingAgentDT.Rows(R).Item("LastName").ToString.Trim, FontOpenSansRegular12, XBrushes.Red, XStartPoint, (YStartPoint + AgentSpace) + 1 * LineSpace, XStringFormats.TopLeft)
                PDFGraphics.DrawString(LeasingAgentDT.Rows(R).Item("Email").ToString.Trim, FontOpenSansRegular12, XBrushes.Gray, XStartPoint, (YStartPoint + AgentSpace) + 2 * LineSpace, XStringFormats.TopLeft)
                PDFGraphics.DrawString(LeasingAgentDT.Rows(R).Item("Cell").ToString.Trim & " " & LeasingAgentDT.Rows(R).Item("LastName").ToString.Trim, FontOpenSansRegular12, XBrushes.Gray, XStartPoint, (YStartPoint + AgentSpace) + 3 * LineSpace, XStringFormats.TopLeft)
            End If
            AgentSpace = AgentSpace + 60
        Next

        Dim PropertyDescription As String = ProjectData.ProjectDescription.ToString.Trim
        Dim PropertyDescriptionSplit As String() = ProjectData.ProjectDescription.Split(VbNewLine)
        For i As Integer = 0 To PropertyDescriptionSplit.Length - 1
            PropertyDescriptionSplit(i) = Replace(PropertyDescriptionSplit(i), "-", "•", 1, 1)
        Next

        XStartPoint = 125
        YStartPoint = 200
        PDFTextFormatter.Alignment = XParagraphAlignment.Left
        PDFTextFormatter.DrawString(PropertyDescriptionSplit(0).ToUpper, FontOpenSansRegular18, New XSolidBrush(XColor.FromArgb(255, 73, 23, 109)), New XRect(YStartPoint, XStartPoint, 200, 300), XStringFormats.TopLeft)


        XStartPoint = 155
        YStartPoint = 200
        PDFTextFormatter.Alignment = XParagraphAlignment.Left

        Dim asss As String = String.Join(vbNewLine, PropertyDescriptionSplit, 1, PropertyDescriptionSplit.Length - 1)
        asss = Replace(asss, vbNewLine, "")
        PDFTextFormatter.DrawString(asss, FontOpenSansRegular10, XBrushes.Gray, New XRect(YStartPoint, XStartPoint, 200, 1000), XStringFormats.TopLeft)


        Dim SliderSQL As String = ""
        SliderSQL &= "select  Asset_Id "
        SliderSQL &= "from IPM_ASSET A "
        SliderSQL &= "where PDF = 0 AND Active = 1 AND Available = 'Y' and ProjectID = " & PropertyId & " "
        SliderSQL &= "and  A.Category_ID = (select Category_ID From IPM_ASSET_CATEGORY where Name = 'Property Images' AND ProjectID = " & PropertyId & ")  "
        Dim SliderDT As New DataTable("SliderDT")
        SliderDT = DBFunctions.GetDataTable(SliderSQL)
        If SliderDT.Rows.Count > 0 Then

            Dim Image1WebRequest As HttpWebRequest = HttpWebRequest.Create("http://hrretail.153.ifmmbeta.com/dynamic/image/week/asset/best/485x280/92/ffffff/Center/" & SliderDT.Rows(0).Item("Asset_Id").ToString.Trim & ".png")
            Dim Image1WebResponse As HttpWebResponse = CType(Image1WebRequest.GetResponse(), HttpWebResponse)
            If Image1WebResponse.StatusCode = HttpStatusCode.OK Then
                Dim PropertyImage1 As Bitmap
                PropertyImage1 = New Bitmap(System.Drawing.Image.FromStream(Image1WebResponse.GetResponseStream(), True, True))
                XStartPoint = 400
                YStartPoint = 125
                PDFGraphics.DrawImage(XImage.FromGdiPlusImage(PropertyImage1), XStartPoint, YStartPoint)
            End If

            If SliderDT.Rows.Count > 1 Then
                Dim Image2WebRequest As HttpWebRequest = HttpWebRequest.Create("http://hrretail.153.ifmmbeta.com/dynamic/image/week/asset/best/235x120/92/ffffff/Center/" & SliderDT.Rows(1).Item("Asset_Id").ToString.Trim & ".png")
                Dim Image2WebResponse As HttpWebResponse = CType(Image2WebRequest.GetResponse(), HttpWebResponse)
                If Image2WebResponse.StatusCode = HttpStatusCode.OK Then
                    Dim PropertyImage2 As Bitmap
                    PropertyImage2 = New Bitmap(System.Drawing.Image.FromStream(Image2WebResponse.GetResponseStream(), True, True))
                    XStartPoint = 400
                    YStartPoint = 340
                    PDFGraphics.DrawImage(XImage.FromGdiPlusImage(PropertyImage2), XStartPoint, YStartPoint)
                End If
            End If

            If SliderDT.Rows.Count > 2 Then
                Dim Image3WebRequest As HttpWebRequest = HttpWebRequest.Create("http://hrretail.153.ifmmbeta.com/dynamic/image/week/asset/best/235x120/92/ffffff/Center/" & SliderDT.Rows(2).Item("Asset_Id").ToString.Trim & ".png")
                Dim Image3WebResponse As HttpWebResponse = CType(Image3WebRequest.GetResponse(), HttpWebResponse)
                If Image3WebResponse.StatusCode = HttpStatusCode.OK Then
                    Dim PropertyImage3 As Bitmap
                    PropertyImage3 = New Bitmap(System.Drawing.Image.FromStream(Image3WebResponse.GetResponseStream(), True, True))
                    XStartPoint = 587
                    YStartPoint = 340
                    PDFGraphics.DrawImage(XImage.FromGdiPlusImage(PropertyImage3), XStartPoint, YStartPoint)
                End If
            End If
        End If

        Dim NoImageSpace As Integer = 0
        If SliderDT.Rows.Count <= 1 Then
            NoImageSpace = 100
        End If

        XStartPoint = 400
        YStartPoint = 440 - NoImageSpace
        PDFGraphics.DrawString("DEMOGRAPHICS", FontOpenSansRegular12, XBrushes.Gray, XStartPoint, YStartPoint, XStringFormats.TopLeft)

        XStartPoint = 400
        YStartPoint = 468 - NoImageSpace
        Dim RowLineSpace As Integer = 18
        If ProjectData.RowH1 <> "" Then
            PDFGraphics.DrawString("............................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, XStartPoint, YStartPoint, XStringFormats.TopLeft)
        End If
        If ProjectData.RowH2 <> "" Then
            PDFGraphics.DrawString("............................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, XStartPoint, YStartPoint + 1 * RowLineSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.RowH3 <> "" Then
            PDFGraphics.DrawString("............................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, XStartPoint, YStartPoint + 2 * RowLineSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.RowH4 <> "" Then
            PDFGraphics.DrawString("............................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, XStartPoint, YStartPoint + 3 * RowLineSpace, XStringFormats.TopLeft)
        End If
        XStartPoint = 400
        YStartPoint = 460 - NoImageSpace
        Dim RowSpace As Integer = 18
        PDFGraphics.DrawString("Distance", FontOpenSansRegular10, XBrushes.Gray, XStartPoint, YStartPoint, XStringFormats.TopLeft)
        If ProjectData.RowH1 <> "" Then
            PDFGraphics.DrawString(ProjectData.RowH1, FontOpenSansRegular10, XBrushes.Gray, XStartPoint, YStartPoint + 1 * RowSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.RowH2 <> "" Then
            PDFGraphics.DrawString(ProjectData.RowH2, FontOpenSansRegular10, XBrushes.Gray, XStartPoint, YStartPoint + 2 * RowSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.RowH3 <> "" Then
            PDFGraphics.DrawString(ProjectData.RowH3, FontOpenSansRegular10, XBrushes.Gray, XStartPoint, YStartPoint + 3 * RowSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.RowH4 <> "" Then
            PDFGraphics.DrawString(ProjectData.RowH4, FontOpenSansRegular10, XBrushes.Gray, XStartPoint, YStartPoint + 4 * RowSpace, XStringFormats.TopLeft)
        End If
        Dim ColSpace As Integer = 100
        If ProjectData.ColH1 <> "" Then
            PDFGraphics.DrawString(ProjectData.ColH1, FontOpenSansBold10, XBrushes.Gray, XStartPoint + 1 * ColSpace, YStartPoint, XStringFormats.TopLeft)
        End If
        If ProjectData.ColH2 <> "" Then
            PDFGraphics.DrawString(ProjectData.ColH2, FontOpenSansBold10, XBrushes.Gray, XStartPoint + 2 * ColSpace, YStartPoint, XStringFormats.TopLeft)
        End If
        If ProjectData.ColH3 <> "" Then
            PDFGraphics.DrawString(ProjectData.ColH3, FontOpenSansBold10, XBrushes.Gray, XStartPoint + 3 * ColSpace, YStartPoint, XStringFormats.TopLeft)
        End If

        If ProjectData.Row1Col1 <> "" Then
            PDFGraphics.DrawString(ProjectData.Row1Col1, FontOpenSansRegular10, XBrushes.Gray, XStartPoint + 1 * ColSpace, YStartPoint + 1 * RowSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.Row1Col2 <> "" Then
            PDFGraphics.DrawString(ProjectData.Row1Col2, FontOpenSansRegular10, XBrushes.Gray, XStartPoint + 2 * ColSpace, YStartPoint + 1 * RowSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.Row1Col3 <> "" Then
            PDFGraphics.DrawString(ProjectData.Row1Col3, FontOpenSansRegular10, XBrushes.Gray, XStartPoint + 3 * ColSpace, YStartPoint + 1 * RowSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.Row2Col1 <> "" Then
            PDFGraphics.DrawString(ProjectData.Row2Col1, FontOpenSansRegular10, XBrushes.Gray, XStartPoint + 1 * ColSpace, YStartPoint + 2 * RowSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.Row2Col2 <> "" Then
            PDFGraphics.DrawString(ProjectData.Row2Col2, FontOpenSansRegular10, XBrushes.Gray, XStartPoint + 2 * ColSpace, YStartPoint + 2 * RowSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.Row2Col3 <> "" Then
            PDFGraphics.DrawString(ProjectData.Row2Col3, FontOpenSansRegular10, XBrushes.Gray, XStartPoint + 3 * ColSpace, YStartPoint + 2 * RowSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.Row3Col1 <> "" Then
            PDFGraphics.DrawString(ProjectData.Row3Col1, FontOpenSansRegular10, XBrushes.Gray, XStartPoint + 1 * ColSpace, YStartPoint + 3 * RowSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.Row3Col2 <> "" Then
            PDFGraphics.DrawString(ProjectData.Row3Col2, FontOpenSansRegular10, XBrushes.Gray, XStartPoint + 2 * ColSpace, YStartPoint + 3 * RowSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.Row3Col3 <> "" Then
            PDFGraphics.DrawString(ProjectData.Row3Col3, FontOpenSansRegular10, XBrushes.Gray, XStartPoint + 3 * ColSpace, YStartPoint + 3 * RowSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.Row4Col1 <> "" Then
            PDFGraphics.DrawString(ProjectData.Row4Col1, FontOpenSansRegular10, XBrushes.Gray, XStartPoint + 1 * ColSpace, YStartPoint + 4 * RowSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.Row4Col2 <> "" Then
            PDFGraphics.DrawString(ProjectData.Row4Col2, FontOpenSansRegular10, XBrushes.Gray, XStartPoint + 2 * ColSpace, YStartPoint + 4 * RowSpace, XStringFormats.TopLeft)
        End If
        If ProjectData.Row4Col3 <> "" Then
            PDFGraphics.DrawString(ProjectData.Row4Col3, FontOpenSansRegular10, XBrushes.Gray, XStartPoint + 3 * ColSpace, YStartPoint + 4 * RowSpace, XStringFormats.TopLeft)
        End If


        NewPDFPage_Template2(PropertyTitle, ProjectData.SiteMapId)

        Dim TenantsXStartPoint As Integer = 550
        '----------TENANTS LOGO--------------'
        Dim TenantsLogoLineSpace As Integer = 80
        Dim TenantsLogoDT As New DataTable
        TenantsLogoDT = IDAMFunctions.GetSpaces(PropertyId, 0, False, True, "Property Tenants Logo Site Plan")
        Dim UniqueTenants As New DataTable
        UniqueTenants = TenantsLogoDT.DefaultView.ToTable(True, "Tenant_ID")
        For R As Integer = 0 To TenantsLogoDT.Rows.Count - 1
            TenantsLogoDT.DefaultView.RowFilter = "Tenant_ID = '" & UniqueTenants.Rows(R).Item("Tenant_ID") & "'"
            Dim UniqueAsstes As New DataTable
            UniqueAsstes = TenantsLogoDT.DefaultView.ToTable
            Dim TenantsLogoWebRequest As HttpWebRequest = HttpWebRequest.Create("http://hrretail.153.ifmmbeta.com/dynamic/image/always/asset/fit/110x90/85/ffffff/North/" & UniqueAsstes.Rows(0)("Asset_ID").ToString.Trim & ".png")
            Dim TenantsLogoWebResponse As HttpWebResponse = CType(TenantsLogoWebRequest.GetResponse(), HttpWebResponse)
            If TenantsLogoWebResponse.StatusCode = HttpStatusCode.OK Then
                Dim TenantsLogoImage As Bitmap
                TenantsLogoImage = New Bitmap(System.Drawing.Image.FromStream(TenantsLogoWebResponse.GetResponseStream(), True, True))
                PDFGraphics.DrawImage(XImage.FromGdiPlusImage(TenantsLogoImage), TenantsXStartPoint + 150, YStartPoint + 60 + R * TenantsLogoLineSpace)
            End If
        Next
        '----------END TENANTS LOGO--------------'


        '---------TENANTS TEXT----------------'
        Dim TenantsDT As New DataTable
        TenantsDT = IDAMFunctions.GetSpaces(PropertyId, 0, False, True, "Site Plan Tenants")

        Dim TenantsHeight As Integer
        Dim TenantsWidth As Integer
        Dim TenantsLineSpace As Integer = 14
        Dim TenantsCount As Integer = TenantsDT.Rows.Count

        If TenantsDT.Rows.Count > 0 Then
            PDFGraphics.DrawString("TENANTS", FontOpenSansRegular12, XBrushes.Gray, TenantsXStartPoint, YStartPoint + 60, XStringFormats.TopLeft)
            TenantsHeight = PDFGraphics.MeasureString("DUMMY TENANT", FontOpenSansRegular10, XStringFormats.TopLeft).Height

            YStartPoint = 80
            For R As Integer = 0 To TenantsDT.Rows.Count - 1
                TenantsWidth = PDFGraphics.MeasureString(TenantsDT.Rows(R)("ShortName").ToString.Trim, FontOpenSansRegular10, XStringFormats.TopLeft).Width

                'PDFGraphics.DrawString(TenantsWidth.ToString, FontOpenSansRegular10, XBrushes.Gray, TenantsXStartPoint - 25, YStartPoint, XStringFormats.TopLeft)

                PDFGraphics.DrawString(TenantsDT.Rows(R)("Suite").ToString.Trim, FontOpenSansRegular10, XBrushes.Gray, TenantsXStartPoint, YStartPoint, XStringFormats.TopLeft)

                'PDFGraphics.DrawString(TenantsDT.Rows(R)("ShortName").ToString.Trim, FontOpenSansRegular10, XBrushes.Gray, TenantsXStartPoint + 40, YStartPoint, XStringFormats.TopLeft)

                Dim xxx As Integer = TenantsWidth \ 100

                PDFTextFormatter.Alignment = XParagraphAlignment.Left
                PDFTextFormatter.DrawString(TenantsDT.Rows(R)("ShortName").ToString.Trim, FontOpenSansRegular10, XBrushes.Gray, New XRect(TenantsXStartPoint + 40, YStartPoint, 85, 100), XStringFormats.TopLeft)


                'PDFGraphics.DrawString(xxx.ToString, FontOpenSansRegular10, XBrushes.Gray, TenantsXStartPoint - 65, YStartPoint, XStringFormats.TopLeft)

                If TenantsWidth + 1 >= 100 Then
                    If xxx = 1 Then
                        YStartPoint = YStartPoint + 46
                    Else
                        YStartPoint = YStartPoint + 28
                    End If
                Else
                    YStartPoint = YStartPoint + TenantsLineSpace
                End If





                If YStartPoint + TenantsLineSpace > 530 Then
                    NewPDFPage_Template2(PropertyTitle, ProjectData.SiteMapId)
                    YStartPoint = 80
                End If
            Next
        End If
        '----------END TENANTS TEXT----------------'


        '-------------AVAILABLE SPACES---------------'
        Dim AvailableSpacesDT As New DataTable
        AvailableSpacesDT = IDAMFunctions.GetSpaces(PropertyId, 0, False, False, "Site Plan Available Spaces")
        If AvailableSpacesDT.Rows.Count > 0 Then

            If TenantsCount > 0 Then
                YStartPoint = YStartPoint + 10
            Else
                YStartPoint = 60
            End If

            PDFGraphics.DrawString("AVAILABLE SPACES", FontOpenSansRegular12, XBrushes.Gray, TenantsXStartPoint, YStartPoint, XStringFormats.TopLeft)
            YStartPoint = YStartPoint + 20
            For R As Integer = 0 To AvailableSpacesDT.Rows.Count - 1
                Dim SuiteWidth As Integer = 0
                Dim SfOut As String = ""
                If AvailableSpacesDT.Rows(R).Item("SF").ToString <> "" Then
                    Dim SQLDec As Decimal = AvailableSpacesDT.Rows(R).Item("SF")
                    SfOut = SQLDec.ToString("#,0") & " SF"
                End If
                'PDFGraphics.DrawString(AvailableSpacesDT.Rows(R)("Suite").ToString.Trim, FontOpenSansRegular10, New XSolidBrush(XColor.FromArgb(255, 239, 61, 67)), TenantsXStartPoint, YStartPoint, XStringFormats.TopLeft)
                SuiteWidth = PDFGraphics.MeasureString(Replace(AvailableSpacesDT.Rows(R)("Suite").ToString.Trim, " ", "-"), FontOpenSansRegular10, XStringFormats.TopLeft).Width


                PDFTextFormatter.Alignment = XParagraphAlignment.Left

                'PDFTextFormatter.DrawString(SuiteWidth, FontOpenSansRegular10, New XSolidBrush(XColor.FromArgb(255, 239, 61, 67)), New XRect(TenantsXStartPoint - 10, YStartPoint, 25, 50), XStringFormats.TopLeft)

                PDFTextFormatter.DrawString(AvailableSpacesDT.Rows(R)("Suite").ToString.Trim, FontOpenSansRegular10, New XSolidBrush(XColor.FromArgb(255, 239, 61, 67)), New XRect(TenantsXStartPoint, YStartPoint, 25, 50), XStringFormats.TopLeft)
                PDFGraphics.DrawString(SfOut, FontOpenSansRegular10, New XSolidBrush(XColor.FromArgb(255, 239, 61, 67)), TenantsXStartPoint + 40, YStartPoint, XStringFormats.TopLeft)

                If SuiteWidth > 25 Then
                    If SuiteWidth > 35 Then
                        YStartPoint = YStartPoint + 46
                    Else
                        YStartPoint = YStartPoint + 28
                    End If

                Else
                    YStartPoint = YStartPoint + TenantsLineSpace
                End If


                'YStartPoint = YStartPoint + TenantsLineSpace
                If YStartPoint + TenantsLineSpace > 530 Then
                    NewPDFPage_Template2(PropertyTitle, ProjectData.SiteMapId)
                    YStartPoint = 80
                End If
            Next
        End If
        '-------------END AVAILABLE SPACES---------------'

        If ProjectData.AerialId <> "" Then
            NewPDFPage_Template4(PropertyTitle)
        End If


        '------------------------MAP--------------------------'
        If ProjectData.AreaMap.ToString.Trim <> "" Then
            If CMSFunctions.IsValidCoords(ProjectData.AreaMap) = True Then
                NewPDFPage_Template3(PropertyTitle)
                Dim Map1 As New StaticGoogleMap
                Map1.SetLatLng(ProjectData.AreaMap.Trim.Split(",")(0), ProjectData.AreaMap.Trim.Split(",")(1))
                Map1.Sensor = True
                Map1.ImageFormat = StaticGoogleMap.ImageFormats.PNG32
                Map1.MapType = StaticGoogleMap.MapTypes.ROADMAP
                Map1.Zoom = 12
                Map1.ImageHeight = 622
                Map1.ImageWidth = 495
                Dim Map1Marker As New Marker
                Map1Marker.MarkerLocation = ProjectData.AreaMap.ToString.Trim
                Map1Marker.MarkerShadow = False
                Map1Marker.Type = Marker.MarkerType.CUSTOM
                Map1Marker.MarkerURL = "http://hrretail.153.ifmmbeta.com/assets/images/pins/pin.png"
                Map1.Markers.Add(Map1Marker)
                Dim Map1Image As Bitmap
                Map1Image = New Bitmap(Map1.GenerateMapAsImage)
                PDFGraphics.DrawImage(XImage.FromGdiPlusImage(Map1Image), 25, 65)

                Dim Map2 As New StaticGoogleMap
                Map2.SetLatLng(ProjectData.AreaMap.Trim.Split(",")(0), ProjectData.AreaMap.Trim.Split(",")(1))
                Map2.Sensor = True
                Map2.ImageFormat = StaticGoogleMap.ImageFormats.PNG32
                Map2.MapType = StaticGoogleMap.MapTypes.ROADMAP
                Map2.Zoom = 16
                Map2.ImageHeight = 622
                Map2.ImageWidth = 495
                Dim Map2Marker As New Marker
                Map2Marker.MarkerLocation = ProjectData.AreaMap.ToString.Trim
                Map2Marker.MarkerShadow = False
                Map2Marker.Type = Marker.MarkerType.CUSTOM
                Map2Marker.MarkerURL = "http://hrretail.153.ifmmbeta.com/assets/images/pins/pin.png"
                Map2.Markers.Add(Map1Marker)
                Dim Map2Image As Bitmap
                Map2Image = New Bitmap(Map2.GenerateMapAsImage)
                PDFGraphics.DrawImage(XImage.FromGdiPlusImage(Map2Image), 397, 65)
            End If
        End If
        '------------------------END MAP--------------------------'



        'Dim fileinfo As FileInfo
        'fileinfo = New FileInfo("D:\websites\hrretail\assets\documents\brochures\" & PropertyId & ".pdf")
        'If fileinfo.Exists Then
        '    fileinfo.Delete()
        'End If
        Dim FileName As String = "D:\websites\hrretail\assets\documents\brochures\" & PropertyId & ".pdf"
        PDFDocument.Save(FileName)

        Dim FilePath As String = ""
        FilePath &= "D:\websites\hrretail\assets\documents\brochures\" & PropertyId & ".pdf"
        HttpContext.Current.response.Clear()
        HttpContext.Current.response.Charset = ""
        HttpContext.Current.response.ContentType = "application/pdf"
        HttpContext.Current.response.AddHeader("Content-Disposition", "attachment; fileName=" & CMSFunctions.FormatFriendlyUrl(ProjectData.ProjectTitle, "-") & ".pdf")
        HttpContext.Current.Response.TransmitFile(FilePath)
        HttpContext.Current.Response.End()

    End Sub



    Public Sub NewPDFPage_Template1(ByVal PropertyTitle As String, ByVal PropertyAddress As String)
        XStartPoint = 0
        YStartPoint = 0

        PDFPage = New PdfPage
        PDFPage.Size = PageSize.Letter
        PDFPage.Orientation = PageOrientation.Landscape
        PDFDocument.AddPage(PDFPage)
        PDFGraphics = XGraphics.FromPdfPage(PDFPage)
        PDFStringFormat = New XStringFormat
        PDFStringFormat.Alignment = XStringAlignment.Near
        PDFStringFormat.LineAlignment = XLineAlignment.Near
        PDFTextFormatter = New XTextFormatter(PDFGraphics)

        Dim ClientLogoBMP As Bitmap
        ClientLogoBMP = New Bitmap(System.Drawing.Image.FromFile("D:\websites\hrretail\cms\data\Image\hr_logo.png"))
        PDFGraphics.DrawImage(XImage.FromGdiPlusImage(ClientLogoBMP), 25, 25, 100, 58)

        PDFGraphics.DrawString(PropertyTitle, FontOpenSansRegular28, New XSolidBrush(XColor.FromArgb(255, 239, 61, 67)), 200, 20, XStringFormats.TopLeft)
        PDFTextFormatter.Alignment = XParagraphAlignment.Left
        PDFTextFormatter.DrawString(PropertyAddress, FontOpenSansRegular14, XBrushes.Gray, New XRect(200, 55, 600, 60), XStringFormats.TopLeft)

        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 90, XStringFormats.TopLeft)
        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 95, XStringFormats.TopLeft)


        '----------------FOOTER-----------------'
        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 555, XStringFormats.TopLeft)
        Dim ClientFooterLogoBMP As Bitmap
        ClientFooterLogoBMP = New Bitmap(System.Drawing.Image.FromFile("D:\websites\hrretail\cms\data\Image\chainlinks_large.gif"))
        PDFGraphics.DrawImage(XImage.FromGdiPlusImage(ClientFooterLogoBMP), 25, 572, 90, 23)

        Dim CityState As String = ProjectData.ProjectCity & ", " & ProjectData.ProjectState
        PDFGraphics.DrawString("WASHINGTON, DC • BALTIMORE, MARYLAND", FontOpenSansRegular8, XBrushes.Gray, 200, 580, XStringFormats.TopLeft)
        PDFGraphics.DrawString("www.hrretail.com", FontOpenSansBold10, New XSolidBrush(XColor.FromArgb(255, 73, 23, 109)), 673, 580, XStringFormats.TopLeft)

        '----------------END FOOTER-----------------'
    End Sub
    Public Sub NewPDFPage_Template2(ByVal PropertyTitle As String, ByVal SiteMapId As String)
        XStartPoint = 0
        YStartPoint = 0
        '----------------HEADER-----------------'
        PDFPage = New PdfPage
        PDFPage.Size = PageSize.Letter
        PDFPage.Orientation = PageOrientation.Landscape
        PDFDocument.AddPage(PDFPage)
        PDFGraphics = XGraphics.FromPdfPage(PDFPage)
        PDFStringFormat = New XStringFormat
        PDFStringFormat.Alignment = XStringAlignment.Near
        PDFStringFormat.LineAlignment = XLineAlignment.Near
        PDFTextFormatter = New XTextFormatter(PDFGraphics)
        PDFGraphics.DrawString(PropertyTitle, FontOpenSansRegular18, New XSolidBrush(XColor.FromArgb(255, 239, 61, 67)), 25, 15, XStringFormats.TopLeft)

        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 40, XStringFormats.TopLeft)
        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 45, XStringFormats.TopLeft)
        '----------------END HEADER-----------------'

        If SiteMapId <> "" Then
            Dim SitePlanWebRequest As HttpWebRequest = HttpWebRequest.Create("http://hrretail.153.ifmmbeta.com/dynamic/image/week/asset/liquid/680x615/90/ffffff/NorthWest/" & SiteMapId & ".png")
            Dim SitePlanWebResponse As HttpWebResponse = CType(SitePlanWebRequest.GetResponse(), HttpWebResponse)
            If SitePlanWebResponse.StatusCode = HttpStatusCode.OK Then
                Dim SitePlanImage As Bitmap
                SitePlanImage = New Bitmap(System.Drawing.Image.FromStream(SitePlanWebResponse.GetResponseStream(), True, True))
                PDFGraphics.DrawImage(XImage.FromGdiPlusImage(SitePlanImage), 25, 65)
            End If
        End If

        Dim InfoText As String = "Information herein has been obtained from sources believed to be reliable. While we do not doubt its accuracy, we have not verified it and make no guarantee, warranty or representation about it. Independent confirmation of its accuracy and completeness is your responsibility. H & R Retail, Inc."
        PDFTextFormatter.Alignment = XParagraphAlignment.Left
        PDFTextFormatter.DrawString(InfoText, FontOpenSansLightRegular8, XBrushes.Gray, New XRect(25, 539, 730, 30), XStringFormats.TopLeft)

        '----------------FOOTER-----------------'
        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 555, XStringFormats.TopLeft)

        Dim ClientFooterLogoBMP As Bitmap
        ClientFooterLogoBMP = New Bitmap(System.Drawing.Image.FromFile("D:\websites\hrretail\cms\data\Image\chainlinks_large.gif"))
        PDFGraphics.DrawImage(XImage.FromGdiPlusImage(ClientFooterLogoBMP), 25, 572, 90, 23)

        Dim CityState As String = ProjectData.ProjectCity & ", " & ProjectData.ProjectState
        PDFGraphics.DrawString("WASHINGTON, DC • BALTIMORE, MARYLAND", FontOpenSansRegular8, XBrushes.Gray, 200, 580, XStringFormats.TopLeft)
        PDFGraphics.DrawString("www.hrretail.com", FontOpenSansBold10, New XSolidBrush(XColor.FromArgb(255, 73, 23, 109)), 673, 580, XStringFormats.TopLeft)
        '----------------END FOOTER-----------------'
    End Sub
    Public Sub NewPDFPage_Template3(ByVal PropertyTitle As String)
        XStartPoint = 0
        YStartPoint = 0
        '----------------HEADER-----------------'
        PDFPage = New PdfPage
        PDFPage.Size = PageSize.Letter
        PDFPage.Orientation = PageOrientation.Landscape
        PDFDocument.AddPage(PDFPage)
        PDFGraphics = XGraphics.FromPdfPage(PDFPage)
        PDFStringFormat = New XStringFormat
        PDFStringFormat.Alignment = XStringAlignment.Near
        PDFStringFormat.LineAlignment = XLineAlignment.Near
        PDFTextFormatter = New XTextFormatter(PDFGraphics)
        PDFGraphics.DrawString(PropertyTitle, FontOpenSansRegular18, New XSolidBrush(XColor.FromArgb(255, 239, 61, 67)), 25, 15, XStringFormats.TopLeft)

        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 40, XStringFormats.TopLeft)
        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 45, XStringFormats.TopLeft)


        Dim InfoText As String = "Information herein has been obtained from sources believed to be reliable. While we do not doubt its accuracy, we have not verified it and make no guarantee, warranty or representation about it. Independent confirmation of its accuracy and completeness is your responsibility. H & R Retail, Inc."
        PDFTextFormatter.Alignment = XParagraphAlignment.Left
        PDFTextFormatter.DrawString(InfoText, FontOpenSansLightRegular8, XBrushes.Gray, New XRect(25, 539, 730, 30), XStringFormats.TopLeft)

        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 555, XStringFormats.TopLeft)

        Dim ClientFooterLogoBMP As Bitmap
        ClientFooterLogoBMP = New Bitmap(System.Drawing.Image.FromFile("D:\websites\hrretail\cms\data\Image\chainlinks_large.gif"))
        PDFGraphics.DrawImage(XImage.FromGdiPlusImage(ClientFooterLogoBMP), 25, 572, 90, 23)

        Dim CityState As String = ProjectData.ProjectCity & ", " & ProjectData.ProjectState
        PDFGraphics.DrawString("WASHINGTON, DC • BALTIMORE, MARYLAND", FontOpenSansRegular8, XBrushes.Gray, 200, 580, XStringFormats.TopLeft)
        PDFGraphics.DrawString("www.hrretail.com", FontOpenSansBold10, New XSolidBrush(XColor.FromArgb(255, 73, 23, 109)), 673, 580, XStringFormats.TopLeft)
        '----------------END FOOTER-----------------'
    End Sub
    Public Sub NewPDFPage_Template4(ByVal PropertyTitle As String)
        XStartPoint = 0
        YStartPoint = 0
        '----------------HEADER-----------------'
        PDFPage = New PdfPage
        PDFPage.Size = PageSize.Letter
        PDFPage.Orientation = PageOrientation.Landscape
        PDFDocument.AddPage(PDFPage)
        PDFGraphics = XGraphics.FromPdfPage(PDFPage)
        PDFStringFormat = New XStringFormat
        PDFStringFormat.Alignment = XStringAlignment.Near
        PDFStringFormat.LineAlignment = XLineAlignment.Near
        PDFTextFormatter = New XTextFormatter(PDFGraphics)
        PDFGraphics.DrawString(PropertyTitle, FontOpenSansRegular18, New XSolidBrush(XColor.FromArgb(255, 239, 61, 67)), 25, 15, XStringFormats.TopLeft)

        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 40, XStringFormats.TopLeft)
        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 45, XStringFormats.TopLeft)


        Dim SitePlanWebRequest As HttpWebRequest = HttpWebRequest.Create("http://hrretail.153.ifmmbeta.com/dynamic/image/week/asset/fit/990x615/90/ffffff/Center/" & ProjectData.AerialId & ".png")
        Dim SitePlanWebResponse As HttpWebResponse = CType(SitePlanWebRequest.GetResponse(), HttpWebResponse)
        If SitePlanWebResponse.StatusCode = HttpStatusCode.OK Then
            Dim SitePlanImage As Bitmap
            SitePlanImage = New Bitmap(System.Drawing.Image.FromStream(SitePlanWebResponse.GetResponseStream(), True, True))
            PDFGraphics.DrawImage(XImage.FromGdiPlusImage(SitePlanImage), 25, 65)
        End If


        Dim InfoText As String = "Information herein has been obtained from sources believed to be reliable. While we do not doubt its accuracy, we have not verified it and make no guarantee, warranty or representation about it. Independent confirmation of its accuracy and completeness is your responsibility. H & R Retail, Inc."
        PDFTextFormatter.Alignment = XParagraphAlignment.Left
        PDFTextFormatter.DrawString(InfoText, FontOpenSansLightRegular8, XBrushes.Gray, New XRect(25, 539, 730, 30), XStringFormats.TopLeft)

        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 555, XStringFormats.TopLeft)
        Dim ClientFooterLogoBMP As Bitmap
        ClientFooterLogoBMP = New Bitmap(System.Drawing.Image.FromFile("D:\websites\hrretail\cms\data\Image\chainlinks_large.gif"))
        PDFGraphics.DrawImage(XImage.FromGdiPlusImage(ClientFooterLogoBMP), 25, 572, 90, 23)

        Dim CityState As String = ProjectData.ProjectCity & ", " & ProjectData.ProjectState
        PDFGraphics.DrawString("WASHINGTON, DC • BALTIMORE, MARYLAND", FontOpenSansRegular8, XBrushes.Gray, 200, 580, XStringFormats.TopLeft)
        PDFGraphics.DrawString("www.hrretail.com", FontOpenSansBold10, New XSolidBrush(XColor.FromArgb(255, 73, 23, 109)), 673, 580, XStringFormats.TopLeft)
        '----------------END FOOTER-----------------'
    End Sub



    Public Sub GetCaseStudy(ByVal PropertyId As Integer)

        ProjectData = IDAMFunctions.GetProjectDetails(PropertyId)
        '---------Property Data
        Dim ClientName As String = "HR RETAIL"
        Dim ClientLogoBMP As Bitmap
        Dim PropertyTitle As String = ProjectData.ProjectTitle.ToUpper
        Dim PropertyAddress As String = ProjectData.ProjectAddress
        '---------End Property Data

        PDFDocument = New PdfDocument
        PDFDocument.Options.NoCompression = True
        PDFDocument.Options.CompressContentStreams = False
        PDFDocument.Info.Title = ClientName & " - " & PropertyTitle
        PDFDocument.Info.Subject = ClientName & " - " & PropertyTitle

        Dim FirstPageHeaderHeight As Integer = 0
        Dim NextPagesHeaderHeight As Integer = 45

        NewCaseStudyPDFPage_Template1(PropertyTitle, PropertyAddress)

        Dim LeasingAgentDT As New DataTable
        LeasingAgentDT = IDAMFunctions.GetProjectContacts(PropertyId, 0)

        XStartPoint = 25
        YStartPoint = 125
        If LeasingAgentDT.Rows.Count > 1 Then
            PDFGraphics.DrawString("LEASING AGENTS", FontOpenSansRegular14, XBrushes.Gray, XStartPoint, YStartPoint, XStringFormats.TopLeft)
        Else
            PDFGraphics.DrawString("LEASING AGENT", FontOpenSansRegular14, XBrushes.Gray, XStartPoint, YStartPoint, XStringFormats.TopLeft)
        End If

        XStartPoint = 25
        YStartPoint = 135
        LineSpace = 15
        Dim AgentSpace As Integer = 0
        Dim NewAgent As Integer = 0
        Dim CountAgents As Integer = 0
        For R As Integer = 0 To LeasingAgentDT.Rows.Count - 1
            CountAgents = CountAgents + 1
            If R = 0 Then
                PDFGraphics.DrawString(LeasingAgentDT.Rows(R).Item("FirstName").ToString.Trim & " " & LeasingAgentDT.Rows(R).Item("LastName").ToString.Trim, FontOpenSansRegular12, New XSolidBrush(XColor.FromArgb(255, 239, 61, 67)), XStartPoint, YStartPoint + 1 * LineSpace, XStringFormats.TopLeft)
                PDFGraphics.DrawString(LeasingAgentDT.Rows(R).Item("Email").ToString.Trim, FontOpenSansRegular12, XBrushes.Gray, XStartPoint, YStartPoint + 2 * LineSpace, XStringFormats.TopLeft)
                PDFGraphics.DrawString(LeasingAgentDT.Rows(R).Item("Cell").ToString.Trim & " " & LeasingAgentDT.Rows(R).Item("LastName").ToString.Trim, FontOpenSansRegular12, XBrushes.Gray, XStartPoint, YStartPoint + 3 * LineSpace, XStringFormats.TopLeft)
            Else
                PDFGraphics.DrawString(LeasingAgentDT.Rows(R).Item("FirstName").ToString.Trim & " " & LeasingAgentDT.Rows(R).Item("LastName").ToString.Trim, FontOpenSansRegular12, XBrushes.Red, XStartPoint, (YStartPoint + AgentSpace) + 1 * LineSpace, XStringFormats.TopLeft)
                PDFGraphics.DrawString(LeasingAgentDT.Rows(R).Item("Email").ToString.Trim, FontOpenSansRegular12, XBrushes.Gray, XStartPoint, (YStartPoint + AgentSpace) + 2 * LineSpace, XStringFormats.TopLeft)
                PDFGraphics.DrawString(LeasingAgentDT.Rows(R).Item("Cell").ToString.Trim & " " & LeasingAgentDT.Rows(R).Item("LastName").ToString.Trim, FontOpenSansRegular12, XBrushes.Gray, XStartPoint, (YStartPoint + AgentSpace) + 3 * LineSpace, XStringFormats.TopLeft)
            End If
            AgentSpace = AgentSpace + 60
        Next


        XStartPoint = 200
        YStartPoint = 125
        Dim BlockSpace As Integer = 40
        Dim BlockWidth As Integer
        Dim BlockHeight As Integer
        Dim BlockLenght As Integer = 200
        Dim BlockLineSpace As Integer = 18
        Dim NoOfRows As Integer
        Dim NoOfNewLines As Integer

        Dim LineTextHeight As Integer = PDFGraphics.MeasureString("XXXXX", FontOpenSansRegular12, XStringFormats.TopLeft).Height

        NoOfRows = 0
        NoOfNewLines = 0
        BlockWidth = 0

        PDFGraphics.DrawString("SERVICE LINE", FontOpenSansRegular14, XBrushes.Gray, 200, YStartPoint, XStringFormats.TopLeft)
        XStartPoint = YStartPoint + LineTextHeight
        PDFTextFormatter.Alignment = XParagraphAlignment.Left
        PDFTextFormatter.DrawString("Tenant Representation", FontOpenSansRegular12, XBrushes.Gray, New XRect(200, XStartPoint, 200, 300), XStringFormats.TopLeft)

        NoOfNewLines = "Tenant Representation".Split(VbNewLine).Length - 1
        BlockWidth = PDFGraphics.MeasureString("Tenant Representation", FontOpenSansRegular12, XStringFormats.TopLeft).Width
        BlockHeight = BlockWidth \ BlockLenght
        If BlockHeight = 0 Then
            NoOfRows = 1
        Else
            NoOfRows = BlockHeight
        End If
        XStartPoint = YStartPoint + NoOfNewLines + NoOfRows * LineTextHeight
        YStartPoint = XStartPoint + LineTextHeight + 5
        PDFGraphics.DrawString("MARKET", FontOpenSansRegular14, XBrushes.Gray, 200, YStartPoint, XStringFormats.TopLeft)

        NoOfRows = 0
        NoOfNewLines = 0
        BlockWidth = 0

        XStartPoint = YStartPoint + LineTextHeight
        PDFTextFormatter.Alignment = XParagraphAlignment.Left
        PDFTextFormatter.DrawString(ProjectData.CaseStudy_Market, FontOpenSansRegular12, XBrushes.Gray, New XRect(200, XStartPoint, 200, 300), XStringFormats.TopLeft)
        NoOfNewLines = ProjectData.CaseStudy_Market.Split(VbNewLine).Length - 1
        BlockWidth = PDFGraphics.MeasureString(ProjectData.CaseStudy_Market, FontOpenSansRegular12, XStringFormats.TopLeft).Width
        BlockHeight = BlockWidth \ BlockLenght
        If BlockHeight = 0 Then
            NoOfRows = 1
        Else
            NoOfRows = BlockHeight
        End If
        XStartPoint = YStartPoint + NoOfNewLines + NoOfRows * LineTextHeight
        YStartPoint = XStartPoint + 2 * LineTextHeight + 5
        PDFGraphics.DrawString("TIMING", FontOpenSansRegular14, XBrushes.Gray, 200, YStartPoint, XStringFormats.TopLeft)

        NoOfRows = 0
        NoOfNewLines = 0
        BlockWidth = 0

        XStartPoint = YStartPoint + LineTextHeight
        PDFTextFormatter.Alignment = XParagraphAlignment.Left
        PDFTextFormatter.DrawString(ProjectData.CaseStudy_Timing, FontOpenSansRegular12, XBrushes.Gray, New XRect(200, XStartPoint, 200, 300), XStringFormats.TopLeft)
        BlockWidth = PDFGraphics.MeasureString(ProjectData.CaseStudy_Timing, FontOpenSansRegular12, XStringFormats.TopLeft).Width
        BlockHeight = BlockWidth \ BlockLenght
        If BlockHeight = 0 Then
            NoOfRows = 1
        Else
            NoOfRows = BlockHeight
        End If
        XStartPoint = YStartPoint + NoOfNewLines + NoOfRows * LineTextHeight
        YStartPoint = XStartPoint + LineTextHeight + 5
        PDFGraphics.DrawString("RESULTS", FontOpenSansRegular14, XBrushes.Gray, 200, YStartPoint, XStringFormats.TopLeft)


        XStartPoint = YStartPoint + LineTextHeight
        PDFTextFormatter.Alignment = XParagraphAlignment.Left
        PDFTextFormatter.DrawString(ProjectData.CaseStudy_Mission, FontOpenSansRegular12, XBrushes.Gray, New XRect(200, XStartPoint, 200, 300), XStringFormats.TopLeft)
        BlockWidth = PDFGraphics.MeasureString(ProjectData.CaseStudy_Mission, FontOpenSansRegular12, XStringFormats.TopLeft).Width
        BlockHeight = BlockWidth \ BlockLenght
        If BlockHeight = 0 Then
            NoOfRows = 1
        Else
            NoOfRows = BlockHeight
        End If

        XStartPoint = YStartPoint + NoOfNewLines + NoOfRows * LineTextHeight
        YStartPoint = XStartPoint + LineTextHeight + 15


        Dim breakString As String = Environment.NewLine & Environment.NewLine
        Dim StringSplit As String() = ProjectData.CaseStudy_Locations.Split({breakString}, StringSplitOptions.None)
        Dim ItemsCount As Integer = 0
        Dim LocationSpace As Integer = 10
        For i As Integer = 0 To StringSplit.Length - 1

            Dim LocationBlock As String() = StringSplit(i).Split(vbNewLine)
            For z As Integer = 0 To LocationBlock.Length - 1
                ItemsCount = ItemsCount + 1
                If z = 0 Then
                    PDFGraphics.DrawString(LocationBlock(z), FontOpenSansBold8, XBrushes.Gray, 200, YStartPoint + (ItemsCount + i) * LocationSpace, XStringFormats.TopLeft)
                Else
                    PDFGraphics.DrawString(LocationBlock(z), FontOpenSansRegular8, XBrushes.Gray, 195, YStartPoint + (ItemsCount + i) * LocationSpace, XStringFormats.TopLeft)
                End If
            Next
        Next


        Dim SliderSQL As String = ""
        SliderSQL &= "select  Asset_Id "
        SliderSQL &= "from IPM_ASSET A "
        SliderSQL &= "where PDF = 0 AND Active = 1 AND Available = 'Y' and ProjectID = " & PropertyId & " "
        SliderSQL &= "and  A.Category_ID = (select Category_ID From IPM_ASSET_CATEGORY where Name = 'Images' AND ProjectID = " & PropertyId & ")  "
        Dim SliderDT As New DataTable("SliderDT")
        SliderDT = DBFunctions.GetDataTable(SliderSQL)
        If SliderDT.Rows.Count > 0 Then

            Dim Image1WebRequest As HttpWebRequest = HttpWebRequest.Create("http://hrretail.153.ifmmbeta.com/dynamic/image/week/asset/best/485x280/92/ffffff/Center/" & SliderDT.Rows(0).Item("Asset_Id").ToString.Trim & ".png")
            Dim Image1WebResponse As HttpWebResponse = CType(Image1WebRequest.GetResponse(), HttpWebResponse)
            If Image1WebResponse.StatusCode = HttpStatusCode.OK Then
                Dim PropertyImage1 As Bitmap
                PropertyImage1 = New Bitmap(System.Drawing.Image.FromStream(Image1WebResponse.GetResponseStream(), True, True))
                XStartPoint = 400
                YStartPoint = 125
                PDFGraphics.DrawImage(XImage.FromGdiPlusImage(PropertyImage1), XStartPoint, YStartPoint)
            End If

            If SliderDT.Rows.Count > 1 Then
                Dim Image2WebRequest As HttpWebRequest = HttpWebRequest.Create("http://hrretail.153.ifmmbeta.com/dynamic/image/week/asset/best/235x120/92/ffffff/Center/" & SliderDT.Rows(1).Item("Asset_Id").ToString.Trim & ".png")
                Dim Image2WebResponse As HttpWebResponse = CType(Image2WebRequest.GetResponse(), HttpWebResponse)
                If Image2WebResponse.StatusCode = HttpStatusCode.OK Then
                    Dim PropertyImage2 As Bitmap
                    PropertyImage2 = New Bitmap(System.Drawing.Image.FromStream(Image2WebResponse.GetResponseStream(), True, True))
                    XStartPoint = 400
                    YStartPoint = 340
                    PDFGraphics.DrawImage(XImage.FromGdiPlusImage(PropertyImage2), XStartPoint, YStartPoint)
                End If
            End If

            If SliderDT.Rows.Count > 2 Then
                Dim Image3WebRequest As HttpWebRequest = HttpWebRequest.Create("http://hrretail.153.ifmmbeta.com/dynamic/image/week/asset/best/235x120/92/ffffff/Center/" & SliderDT.Rows(2).Item("Asset_Id").ToString.Trim & ".png")
                Dim Image3WebResponse As HttpWebResponse = CType(Image3WebRequest.GetResponse(), HttpWebResponse)
                If Image3WebResponse.StatusCode = HttpStatusCode.OK Then
                    Dim PropertyImage3 As Bitmap
                    PropertyImage3 = New Bitmap(System.Drawing.Image.FromStream(Image3WebResponse.GetResponseStream(), True, True))
                    XStartPoint = 587
                    YStartPoint = 340
                    PDFGraphics.DrawImage(XImage.FromGdiPlusImage(PropertyImage3), XStartPoint, YStartPoint)
                End If
            End If
        End If

        Dim NoImageSpace As Integer = 0
        If SliderDT.Rows.Count <= 1 Then
            NoImageSpace = 100
        End If


        'Dim fileinfo As FileInfo
        'fileinfo = New FileInfo("D:\websites\hrretail\assets\documents\brochures\" & PropertyId & ".pdf")
        'If fileinfo.Exists Then
        '    fileinfo.Delete()
        'End If
        Dim FileName As String = "D:\websites\hrretail\assets\documents\brochures\" & PropertyId & ".pdf"
        PDFDocument.Save(FileName)

        Dim FilePath As String = ""
        FilePath &= "D:\websites\hrretail\assets\documents\brochures\" & PropertyId & ".pdf"
        HttpContext.Current.response.Clear()
        HttpContext.Current.response.Charset = ""
        HttpContext.Current.response.ContentType = "application/pdf"
        HttpContext.Current.response.AddHeader("Content-Disposition", "attachment; fileName=" & CMSFunctions.FormatFriendlyUrl(ProjectData.ProjectTitle, "-") & ".pdf")
        HttpContext.Current.Response.TransmitFile(FilePath)
        HttpContext.Current.Response.End()

    End Sub


    Public Sub NewCaseStudyPDFPage_Template1(ByVal PropertyTitle As String, ByVal PropertyAddress As String)
        XStartPoint = 0
        YStartPoint = 0

        PDFPage = New PdfPage
        PDFPage.Size = PageSize.Letter
        PDFPage.Orientation = PageOrientation.Landscape
        PDFDocument.AddPage(PDFPage)
        PDFGraphics = XGraphics.FromPdfPage(PDFPage)
        PDFStringFormat = New XStringFormat
        PDFStringFormat.Alignment = XStringAlignment.Near
        PDFStringFormat.LineAlignment = XLineAlignment.Near
        PDFTextFormatter = New XTextFormatter(PDFGraphics)

        Dim ClientLogoBMP As Bitmap
        ClientLogoBMP = New Bitmap(System.Drawing.Image.FromFile("D:\websites\hrretail\cms\data\Image\hr_logo.png"))
        PDFGraphics.DrawImage(XImage.FromGdiPlusImage(ClientLogoBMP), 25, 25, 100, 58)

        PDFGraphics.DrawString(PropertyTitle, FontOpenSansRegular28, New XSolidBrush(XColor.FromArgb(255, 239, 61, 67)), 200, 47, XStringFormats.TopLeft)

        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 90, XStringFormats.TopLeft)
        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 95, XStringFormats.TopLeft)


        '----------------FOOTER-----------------'
        PDFGraphics.DrawString("...............................................................................................................................................................................................................................................................................................................................................................................................................", FontOpenSansRegular8, XBrushes.Gray, 0, 555, XStringFormats.TopLeft)
        Dim ClientFooterLogoBMP As Bitmap
        ClientFooterLogoBMP = New Bitmap(System.Drawing.Image.FromFile("D:\websites\hrretail\cms\data\Image\chainlinks_large.gif"))
        PDFGraphics.DrawImage(XImage.FromGdiPlusImage(ClientFooterLogoBMP), 25, 572, 90, 23)

        Dim CityState As String = ProjectData.ProjectCity & ", " & ProjectData.ProjectState
        PDFGraphics.DrawString("WASHINGTON, DC • BALTIMORE, MARYLAND", FontOpenSansRegular8, XBrushes.Gray, 200, 580, XStringFormats.TopLeft)
        PDFGraphics.DrawString("www.hrretail.com", FontOpenSansBold10, New XSolidBrush(XColor.FromArgb(255, 73, 23, 109)), 673, 580, XStringFormats.TopLeft)

        '----------------END FOOTER-----------------'
    End Sub


    


End Class
