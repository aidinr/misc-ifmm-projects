Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Web
Imports System.Security.Cryptography
Imports System.Data
Public Class CMSFunctions


    Public Shared Function Hello(theTest As String) As String
        Return "Hello " & theTest
    End Function
    Public Shared Function GetText(ByVal theFile As String) As String
        Dim returnString As String = ""
        Try
            Using sr As New StreamReader(theFile)
                returnString = sr.ReadToEnd()
                sr.Close()
            End Using
        Catch ex As Exception
            returnString = ex.Message
        End Try
        Return returnString
    End Function
    Public Shared Function FormatSimple(ByVal theFile As String) As String
        Dim returnString As String = ""
        Try
            Using sr As New StreamReader(theFile)
                returnString = FormatFunctionsSimple.AutoFormatText(sr.ReadToEnd())
                sr.Close()
            End Using
        Catch ex As Exception
            returnString = "Not Available"
        End Try
        Return returnString
    End Function
    Public Shared Function FormatRich(ByVal theFile As String) As String
        Dim returnString As String = ""
        Try
            Using sr As New StreamReader(theFile)
                returnString = sr.ReadToEnd()
                sr.Close()
            End Using
        Catch ex As Exception
            returnString = ex.Message
        End Try

        Return returnString
    End Function
    Public Shared Function FormatList(ByVal thePath As String, ByVal theFile As String, ByVal tag As String) As String
        Dim returnString As String = ""
        Try
            Using sr As New StreamReader(thePath & theFile & ".txt")
                returnString = formatfunctionslist.AutoFormatText(sr.ReadToEnd(), tag)
            End Using
        Catch ex As Exception
            returnString = "Not Available"
        End Try
        Return returnString
    End Function
    Public Shared Function FormatLink(ByVal thePath As String, ByVal theFile As String, ByVal tag As String, ByVal UrlStartPart As String) As String
        Dim returnString As String = ""
        Dim Href As String = ""
        Dim OutLink As String = ""
        Try
            Using sr As New StreamReader(thePath & theFile & ".txt")
                returnString = sr.ReadToEnd()
                Dim Split() As String = returnString.split(VbNewLIne)
                For Each Link As String In Split
                    Href = ""
                    Href = UrlStartPart & formaturl.friendlyurl(Link)
                    OutLink &= "<" & tag & ">" + "<a href=" + """" + Href + """" + ">" + formatfunctionssingle.AutoFormatText(Link) + "</a>" + "</" & tag & ">" + vbNewLine
                Next
            End Using
        Catch ex As Exception
            returnString = ex.Message
        End Try
        returnString = OutLink
        Return returnString
    End Function
    Public Shared Function Format(ByVal theFile As String) As String
        Dim returnString As String = ""
        Try
            Using sr As New StreamReader(theFile)
                returnString = formatfunctions.AutoFormatText(sr.ReadToEnd())
                sr.Close()
            End Using
        Catch ex As Exception
            returnString = ex.Message
        End Try

        Return returnString
    End Function
    Public Shared Function Image(ByVal thePath As String, ByVal theField As String) As String
        Dim returnString As String = ""
        For Each f As String In Directory.GetFiles(thePath & "Image\", theField & ".*")
            If Path.GetExtension(f) <> ".txt" And Path.GetExtension(f) <> ".tmp" Then
                returnString = Path.GetFileName(f) & "?t=" & HttpContext.Current.Server.UrlEncode(System.IO.File.GetLastWriteTime(f).ToString())
            End If
        Next
        Return returnString
    End Function
    Public Shared Function CheckTextFile(ByVal theFile As String, ByVal i As Integer) As Boolean
        Dim returnStatus As Boolean = True
        If Not File.Exists(theFile & "_xa" & i.ToString.Trim & ".txt") Then
            returnStatus = False
        End If
        If Not File.Exists(theFile & "_xb" & i.ToString.Trim & ".txt") Then
            returnStatus = False
        End If
        Return returnStatus
    End Function
    Public Shared Function GetMetaTitle(ByVal thePath As String, ByVal thePage As String) As String
        Return CMSFunctions.FormatSimple(thePath & "Metadata\MetadataTitle_" & thePage & ".txt")
    End Function
    Public Shared Function GetMetaDescription(ByVal thePath As String, ByVal thePage As String) As String
        Return CMSFunctions.FormatSimple(thePath & "Metadata\metadataDescription_" & thePage & ".txt")
    End Function
    Public Shared Function GetMetaKeywords(ByVal thePath As String, ByVal thePage As String) As String
        Return CMSFunctions.FormatSimple(thePath & "Metadata\metadataKeywords_" & thePage & ".txt")
    End Function
    Public Shared Function GetMetaGA(ByVal thePath As String, ByVal thePage As String) As String
        Return CMSFunctions.FormatSimple(thePath & "Metadata\GoogleAnalytics.txt")
    End Function
    Public Shared Function GetTextSimple(ByVal thePath As String, ByVal theField As String) As String
        Return CMSFunctions.FormatSimple(thePath & "Text\" & theField & ".txt")
    End Function
    Public Shared Function getContentText(ByVal thePath As String, ByVal theField As String) As String
        If File.Exists(thePath & "Text\" & theField & ".txt") = True Then
            Return CMSFunctions.FormatSimple(thePath & "Text\" & theField & ".txt")
        Else
            Return "Not available"
        End If
    End Function
    Public Shared Function getContentMulti(ByVal thePath As String, ByVal theField As String) As String
        If File.Exists(thePath & "Text\" & theField & ".txt") = True Then
            Return CMSFunctions.Format(thePath & "Text\" & theField & ".txt")
        Else
            Return "Not available"
        End If
    End Function
    Public Shared Function GetContentRich(ByVal thePath As String, ByVal theField As String) As String
        If File.Exists(thePath & "Rich\" & theField & ".txt") = True Then
            Return CMSFunctions.FormatRich(thePath & "Rich\" & theField & ".txt")
        Else
            Return "Not available"
        End If
    End Function
    Public Shared Function GetContentAlt(ByVal thePath As String, ByVal theField As String) As String
        Return CMSFunctions.FormatSimple(thePath & theField & ".txt")
    End Function
    Public Shared Function GetLink(ByVal thePath As String, ByVal theField As String, ByVal Type As String) As String
        Dim out As String = ""
        Dim split1 As String()
        If File.Exists(thePath & theField & ".txt") Then
            Try
                Using sr As New StreamReader(thePath & theField & ".txt")
                    split1 = Split(Sr.ReadToEnd(), "|-|")
                    sr.Close()
                End Using
            Catch ex As Exception
                out = ex.Message
            End Try
            If Type = "href" Then
                out = formatfunctionssimple.AutoFormatText(split1(0))
            Else
                out = formatfunctionssimple.AutoFormatText(split1(1))
            End If
        Else
            out = "#"
        End If
        Return Out
    End Function
    Public Shared Function GetTextFormat(ByVal thePath As String, ByVal theField As String) As String
        Return CMSFunctions.Format(thePath & "Text\Text_" & theField & ".txt")
    End Function
    Public Shared Function GetRichFormat(ByVal thePath As String, ByVal theField As String) As String
        Return CMSFunctions.FormatRich(thePath & "Rich\Rich_" & theField & ".txt")
    End Function
    Public Shared Function GetFile(ByVal thePath As String, ByVal theField As String, ByVal Type As String) As String
        Dim out As String = ""
        Dim split1 As String()
        If File.Exists(thePath & theField & ".txt") Then
            Try
                Using sr As New StreamReader(thePath & theField & ".txt")
                    split1 = Split(Sr.ReadToEnd(), "|-|")
                    sr.Close()
                End Using
            Catch ex As Exception
                out = ex.Message
            End Try
            If Type = "text" Then
                If split1(0) = "" Then
                    out = "Not Available"
                Else
                    out = formatfunctionssimple.AutoFormatText(split1(0))
                End If
            Else
                out = formatfunctionssimple.AutoFormatText(split1(1))
            End If
        Else
            If Type = "text" Then
                out = "Not Available"
            Else
                out = "#"
            End If
        End If
        Return out
    End Function
    Public Shared Function GetTextFormatBulletList(thePath As String, theField As String) As String
        Return CMSFunctions.FormatBulletList(thePath & "Text\Text_" & theField & ".txt")
    End Function
    Public Shared Function FormatBulletList(theFile As String) As String
        Dim returnString As String = ""
        Try
            Using sr As New StreamReader(theFile)
                returnString = formatfunctions.AutoFormatTextList(sr.ReadToEnd())
            End Using
        Catch ex As Exception
            returnString = ex.Message
        End Try

        Return returnString
    End Function


    Public Shared Sub WriteHtmlFile(ByVal myPath As String, ByVal myPage As String, ByVal myFileName As String)
        myPage &= vbNewLIne & " <!-- Cached " & DateTime.Now & " -->"
        Dim myHtmlWriter As System.IO.StreamWriter
        Dim myFile As String
        If myFileName <> "" Then
            myFile = myFileName
        Else
            myFile = "index"
        End If
        myHtmlWriter = System.IO.File.CreateText(myPath & "Cache\" & myFile & ".htm")
        Dim myFilePath As String = (myPath & "Cache\" & myFile & ".htm")
        myHtmlWriter.Write(myPage.ToString)
        myHtmlWriter.Close()
    End Sub
    Public Shared Sub DeleteHtmlFile(ByVal myPath As String, ByVal myPage As String, ByVal myFileName As String)
        Dim myFile As String
        If myFileName <> "" Then
            myFile = myFileName
        Else
            myFile = "index"
        End If
        System.IO.File.Delete(myPath & "Cache\" & myFile & ".htm")
    End Sub
    Public Shared Function FormatFriendlyUrl(ByVal input As String, ByVal Sep As String) As String
        If input <> "" Then
            ' remove entities
            input = Regex.Replace(input, "&\w+;", "")
            'remove anything that is not letters, numbers, dash, or space
            input = Regex.Replace(input, "[^A-Za-z0-9\-\s]", "")
            ' remove any leading or trailing spaces left over
            input = input.Trim()
            ' replace spaces with single dash
            input = Regex.Replace(input, "\s+", Sep)
            ' if we end up with multiple dashes, collapse to single dash            
            input = Regex.Replace(input, "\-{2,}", Sep)
            ' make it all lower case
            input = input.ToLower()
            ' if it's too long, clip it
            If input.Length > 80 Then
                input = input.Substring(0, 79)
            End If
            input = Replace(input, "-", Sep)
            ' remove trailing dash, if there is one
            If input.EndsWith(Sep) Then
                input = input.Substring(0, input.Length - 1)
            End If
            input = RegularExpressions.Regex.Replace(input, "[^A-Za-z0-9]+", Sep, RegexOptions.Multiline)
        End If
        Return input
    End Function
    Public Shared Function FormatFriendlyUrl_NoLower(ByVal input As String, ByVal Sep As String) As String
        If input <> "" Then
            input = Regex.Replace(input, "[^A-Za-z0-9\-]", Sep)
        End If
        Return input
    End Function

    Public Shared Function FormatNiceText(ByVal input As String, ByVal CapFirst As Integer) As String
        Dim NewItemString As String = ""
        NewItemString = Replace(input, "_", " ")
        If CapFirst = 1 Then
            NewItemString = StrConv(NewItemString, VbStrConv.ProperCase)
        End If
        Return NewItemString
    End Function
    Public Shared Function GetSHA1(ByVal strToHash As String) As String
        Dim bytesToHash() As Byte = System.Text.Encoding.ASCII.GetBytes(strToHash)
        Dim sha1Obj As New SHA1CryptoServiceProvider
        bytesToHash = sha1Obj.ComputeHash(bytesToHash)
        Dim Res As String = ""
        For Each b As Byte In bytesToHash
            Res += b.ToString("x2")
        Next
        Return Res
    End Function
    Public Shared Function GetMD5(ByVal SourceText As String) As String
        Dim ByteSourceText() As Byte = System.Text.Encoding.ASCII.GetBytes(SourceText)
        Dim Md5 As New MD5CryptoServiceProvider()
        Dim ByteHash() As Byte = Md5.ComputeHash(ByteSourceText)
        Dim Res As String = ""
        For Each b As Byte In ByteHash
            Res += b.ToString("x2")
        Next
        Return Res
    End Function
    Public Shared Function StringOutNumbers(ByVal HotStripper As String) As String
        Dim NewValue As String = ""
        If HotStripper.Contains("Item") = True Then
            Dim Split() As String = HotStripper.Split("Item")
            Dim StringStrip As String = ""
            StringStrip = Split(1)
            For I As Integer = 0 To StringStrip.Length - 1
                If IsNumeric(StringStrip.Substring(I, 1)) = True Then
                    NewValue += StringStrip.Substring(I, 1)
                End If
            Next
        End If
        Return NewValue
    End Function
    Public Shared Function GetItemNumber(ByVal ItemString As String) As String
        Dim ItemNumber As String = ""
        'ItemNumber = Regex.Match(ItemString, "Item_[0-9a-zA-Z\-_]_", 0).ToString
        ItemNumber = Regex.Match(ItemString, "Item_[A-Za-z0-9]+_", 0).ToString
        ItemNumber = Replace(ItemNumber, "Item_", "")
        ItemNumber = Replace(ItemNumber, "_", "")
        Return ItemNumber
    End Function
    Public Shared Function StringOutOneNumber(ByVal HotStripper As String) As String
        Dim NewValue As String = ""
        For I As Integer = 0 To HotStripper.Length - 1
            If IsNumeric(HotStripper.Substring(I, 1)) = True Then
                NewValue += HotStripper.Substring(I, 1)
            End If
        Next
        Return NewValue
    End Function
    Public Shared Function StringOutNumbers1(ByVal HotStripper As String) As String
        Dim NewValue As String = ""
        For I As Integer = 0 To HotStripper.Length - 1
            If IsNumeric(HotStripper.Substring(I, 1)) = True Then
                NewValue += HotStripper.Substring(I, 1)
            End If
        Next
        Return NewValue
    End Function




    Public Shared Function GetRandomKey() As String
        Dim NumKeys As Integer
        Dim RandomKey As String = ""
        NumKeys = 1
        Dim KeyGen As New RandomKeyGenerator
        KeyGen.KeyLetters = "abcdefghijklmnopqrstuvwxyz"
        KeyGen.KeyNumbers = "0123456789"
        KeyGen.KeyChars = 35
        For i_Keys As Integer = 1 To NumKeys
            RandomKey = KeyGen.Generate()
        Next
        Return RandomKey
    End Function
    Public Shared Function GetWords(ByVal input As String, ByVal WordNo As Integer) As String
        Dim s As String = input
        Dim output As String = ""
        Dim sp() As Char = {" "}
        Dim noOfWordsRequired As Integer = WordNo
        Dim words As String() = s.Split(sp)
        If noOfWordsRequired < words.Length Then
            For i As Integer = 0 To noOfWordsRequired - 1
                output &= words(i) & " "
            Next
        Else
            output = s
        End If
        input = output
        Return input
    End Function
    Public Shared Function GetChars(ByVal input As String, ByVal charsno As Integer) As String
        Dim s As String = input
        Dim output As String = ""
        Dim sp() As Char = {" "}
        'Dim words = s.Split(New () {" "C}, StringSplitOptions.RemoveEmptyEntries)
        Dim words As String() = s.Split(sp)
        'If words(0).Length > length Then
        '    Return words(0)
        'End If
        Dim sb = New StringBuilder()
        For Each word As String In words
            If sb.Length + word.Length > charsno Then
                Return String.Format("{0}...", sb.ToString().TrimEnd(" "c))
            End If
            sb.Append(word + " ")
        Next
        Return String.Format("{0}...", sb.ToString().TrimEnd(" "c))
    End Function


    Public Shared Function GetCmsName(ByVal CssClass As String) As String
        Dim CmsName As String = ""
        CmsName = RegularExpressions.Regex.Match(CssClass, "cmsName_[0-9a-zA-Z\-_]+", 0).ToString
        CmsName = Replace(CmsName, "cmsName_", "")
        Return CmsName
    End Function
    Public Shared Function GetCmsType(ByVal CssClass As String) As String
        Dim CmsType As String = ""
        CmsType = RegularExpressions.Regex.Match(CssClass, "cmsType_[0-9a-zA-Z\-_]+", 0).ToString
        CmsType = Replace(CmsType, "cmsType_", "")
        Return CmsType
    End Function
    Public Shared Function GetCmsFillType(ByVal CssClass As String) As String
        Dim CmsName As String = ""
        CmsName = RegularExpressions.Regex.Match(CssClass, "cmsFill_[0-9a-zA-Z\-_]+", 0).ToString
        CmsName = Replace(CmsName, "cmsFill_", "")
        Return CmsName
    End Function


    Public Shared Function IsValidCoords(ByVal input As String) As Boolean
        Dim m As Match = Regex.Match(input, "^((-(180|1[0-7]\d|\d?\d))|(180|1[0-7]\d|\d?\d))(\.\d+)?\s*,\s*((-(180|1[0-7]\d|\d?\d))|(180|1[0-7]\d|\d?\d))(\.\d+)?$", RegexOptions.IgnoreCase)
        If (m.Success) Then
            IsValidCoords = True
        Else
            IsValidCoords = False
        End If
    End Function



End Class
