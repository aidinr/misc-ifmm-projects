﻿Imports Microsoft.VisualBasic
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Runtime.Serialization.Json
Imports System.Runtime.Serialization
Imports System.Web.Script.Serialization


''' <summary>
''' Base class for components.
''' </summary>
<Serializable()> _
<DataContract()> _
Public MustInherit Class Component
    ''' <summary>
    ''' Get the object from JSON.
    ''' </summary>
    ''' <typeparam name="T">The class type to be deserialized.</typeparam>
    ''' <param name="json">The serialization string.</param>
    ''' <returns>Returns the object deserialized from the JSON string.</returns>
    Public Shared Function FromJSON(Of T As Class)(ByVal json As String) As T
        Dim obj As T = Nothing
        Using ms As New MemoryStream()
            Dim buffer As Byte() = Encoding.UTF8.GetBytes(json)
            ms.Write(buffer, 0, buffer.Length)
            ms.Position = 0
            Dim ser As New DataContractJsonSerializer(GetType(T))
            obj = TryCast(ser.ReadObject(ms), T)
        End Using

        Return obj
    End Function

    ''' <summary>
    ''' Serialize an object to JSON.
    ''' </summary>
    ''' <returns>Returns a string representing the serialized object.</returns>
    Public Overridable Function ToJSON() As String
        Dim json As String = Nothing
        Using ms As New MemoryStream()
            Dim ser As New DataContractJsonSerializer(Me.GetType())
            ser.WriteObject(ms, Me)
            json = Encoding.UTF8.GetString(ms.ToArray())
        End Using

        Return json
    End Function
End Class

''' <summary>
''' Custom field class.
''' </summary>
<DataContract()> _
<Serializable()> _
Public Class CustomField
    Inherits Component
    ''' <summary>
    ''' Name of the custom field. Only accepted names.
    ''' </summary>
    <DataMember(Name:="name", EmitDefaultValue:=False)> _
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(ByVal value As String)
            m_Name = value
        End Set
    End Property
    Private m_Name As String
    ''' <summary>
    ''' Value of the custom field.
    ''' </summary>
    <DataMember(Name:="value", EmitDefaultValue:=False)> _
    Public Property Value() As String
        Get
            Return m_Value
        End Get
        Set(ByVal value As String)
            m_Value = value
        End Set
    End Property
    Private m_Value As String
End Class

''' <summary>
''' Represents a single List in Constant Contact.
''' </summary>
<DataContract()> _
Public Class ContactList
    Inherits Component
    ''' <summary>
    ''' Gets or sets the id.
    ''' </summary>
    <DataMember(Name:="id", EmitDefaultValue:=False)> _
    Public Property Id() As String
        Get
            Return m_Id
        End Get
        Set(ByVal value As String)
            m_Id = value
        End Set
    End Property
    Private m_Id As String
    ''' <summary>
    ''' Gets or sets the status.
    ''' </summary>
    <DataMember(Name:="status", EmitDefaultValue:=False)> _
    Public Property Status() As String
        Get
            Return m_Status
        End Get
        Set(ByVal value As String)
            m_Status = value
        End Set
    End Property
    Private m_Status As String
    ''' <summary>
    ''' Gets or sets the number of contacts in the list
    ''' </summary>
    <DataMember(Name:="contact_count", EmitDefaultValue:=False)> _
    Public Property ContactCount() As Integer
        Get
            Return m_ContactCount
        End Get
        Set(ByVal value As Integer)
            m_ContactCount = value
        End Set
    End Property
    Private m_ContactCount As Integer
    ''' <summary>
    ''' Gets or sets the contact list name
    ''' </summary>
    <DataMember(Name:="name", EmitDefaultValue:=False)> _
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(ByVal value As String)
            m_Name = value
        End Set
    End Property
    Private m_Name As String

    ''' <summary>
    ''' Class constructor.
    ''' </summary>
    Public Sub New()
    End Sub
End Class

''' <summary>
''' Represents a single EmailAddress of a Contact.
''' </summary>
<DataContract()> _
<Serializable()> _
Public Class EmailAddress
    Inherits Component
    ''' <summary>
    ''' Email address id.
    ''' </summary>
    <DataMember(Name:="id", EmitDefaultValue:=False)> _
    Public Property Id() As String
        Get
            Return m_Id
        End Get
        Set(ByVal value As String)
            m_Id = value
        End Set
    End Property
    Private m_Id As String
    ''' <summary>
    ''' Gets or sets the OPT out date.
    ''' </summary>
    <DataMember(Name:="opt_out_date", EmitDefaultValue:=False)> _
    Public Property OptOutDate() As String
        Get
            Return m_OptOutDate
        End Get
        Set(ByVal value As String)
            m_OptOutDate = value
        End Set
    End Property
    Private m_OptOutDate As String
    ''' <summary>
    ''' Gets or sets the status.
    ''' </summary>
    <DataMember(Name:="status", EmitDefaultValue:=False)> _
    Public Property Status() As String
        Get
            Return m_Status
        End Get
        Set(ByVal value As String)
            m_Status = value
        End Set
    End Property
    Private m_Status As String
    ''' <summary>
    ''' Gets or sets the OPT source.
    ''' </summary>
    <DataMember(Name:="opt_in_source", EmitDefaultValue:=False)> _
    Public Property OptInSource() As String
        Get
            Return m_OptInSource
        End Get
        Set(ByVal value As String)
            m_OptInSource = value
        End Set
    End Property
    Private m_OptInSource As String
    ''' <summary>
    ''' Gets or sets the OPT date.
    ''' </summary>
    <DataMember(Name:="opt_in_date", EmitDefaultValue:=False)> _
    Public Property OptInDate() As String
        Get
            Return m_OptInDate
        End Get
        Set(ByVal value As String)
            m_OptInDate = value
        End Set
    End Property
    Private m_OptInDate As String
    ''' <summary>
    ''' Gets or sets the OPT source.
    ''' </summary>
    <DataMember(Name:="opt_out_source", EmitDefaultValue:=False)> _
    Public Property OptOutSource() As String
        Get
            Return m_OptOutSource
        End Get
        Set(ByVal value As String)
            m_OptOutSource = value
        End Set
    End Property
    Private m_OptOutSource As String
    ''' <summary>
    ''' Gets or sets the confirmation status.
    ''' </summary>
    <DataMember(Name:="confirm_status", EmitDefaultValue:=False)> _
    Public Property ConfirmStatus() As String
        Get
            Return m_ConfirmStatus
        End Get
        Set(ByVal value As String)
            m_ConfirmStatus = value
        End Set
    End Property
    Private m_ConfirmStatus As String
    ''' <summary>
    ''' Gets or sets the email address.
    ''' </summary>
    <DataMember(Name:="email_address")> _
    Public Property EmailAddr() As String
        Get
            Return m_EmailAddr
        End Get
        Set(ByVal value As String)
            m_EmailAddr = value
        End Set
    End Property
    Private m_EmailAddr As String

    ''' <summary>
    ''' Class constructor.
    ''' </summary>
    Public Sub New()
    End Sub

    ''' <summary>
    ''' Class constructor.
    ''' </summary>
    ''' <param name="emailAddress">Email address.</param>
    Public Sub New(ByVal emailAddress__1 As String)
        Me.EmailAddr = emailAddress__1
    End Sub
End Class

''' <summary>
''' Note class.
''' </summary>
<DataContract()> _
<Serializable()> _
Public Class Note
    Inherits Component
    ''' <summary>
    ''' Gets or sets the id.
    ''' </summary>
    <DataMember(Name:="id", EmitDefaultValue:=False)> _
    Public Property Id() As String
        Get
            Return m_Id
        End Get
        Set(ByVal value As String)
            m_Id = value
        End Set
    End Property
    Private m_Id As String
    ''' <summary>
    ''' Gets or sets the note content.
    ''' </summary>
    <DataMember(Name:="note", EmitDefaultValue:=False)> _
    Public Property Content() As String
        Get
            Return m_Content
        End Get
        Set(ByVal value As String)
            m_Content = value
        End Set
    End Property
    Private m_Content As String
    ''' <summary>
    ''' Gets or sets the datetime when note was created.
    ''' </summary>
    <DataMember(Name:="created_date", EmitDefaultValue:=False)> _
    Public Property CreatedDate() As String
        Get
            Return m_CreatedDate
        End Get
        Set(ByVal value As String)
            m_CreatedDate = value
        End Set
    End Property
    Private m_CreatedDate As String
    ''' <summary>
    ''' Gets or sets the datetime when note was modified.
    ''' </summary>
    <DataMember(Name:="modified_date", EmitDefaultValue:=False)> _
    Public Property ModifiedDate() As String
        Get
            Return m_ModifiedDate
        End Get
        Set(ByVal value As String)
            m_ModifiedDate = value
        End Set
    End Property
    Private m_ModifiedDate As String
End Class

''' <summary>
''' Represents a single Address of a Contact.
''' </summary>
<DataContract()> _
<Serializable()> _
Public Class Address
    Inherits Component
    ''' <summary>
    ''' Gets or sets the id.
    ''' </summary>
    <DataMember(Name:="id", EmitDefaultValue:=False)> _
    Public Property Id() As String
        Get
            Return m_Id
        End Get
        Set(ByVal value As String)
            m_Id = value
        End Set
    End Property
    Private m_Id As String
    ''' <summary>
    ''' Gets or sets the first address line.
    ''' </summary>
    <DataMember(Name:="line1", EmitDefaultValue:=False)> _
    Public Property Line1() As String
        Get
            Return m_Line1
        End Get
        Set(ByVal value As String)
            m_Line1 = value
        End Set
    End Property
    Private m_Line1 As String
    ''' <summary>
    ''' Gets or sets the second address line.
    ''' </summary>
    <DataMember(Name:="line2", EmitDefaultValue:=False)> _
    Public Property Line2() As String
        Get
            Return m_Line2
        End Get
        Set(ByVal value As String)
            m_Line2 = value
        End Set
    End Property
    Private m_Line2 As String
    ''' <summary>
    ''' Gets or sets the third address line.
    ''' </summary>
    <DataMember(Name:="line3", EmitDefaultValue:=False)> _
    Public Property Line3() As String
        Get
            Return m_Line3
        End Get
        Set(ByVal value As String)
            m_Line3 = value
        End Set
    End Property
    Private m_Line3 As String
    ''' <summary>
    ''' Gets or sets the city.
    ''' </summary>
    <DataMember(Name:="city", EmitDefaultValue:=False)> _
    Public Property City() As String
        Get
            Return m_City
        End Get
        Set(ByVal value As String)
            m_City = value
        End Set
    End Property
    Private m_City As String
    ''' <summary>
    ''' Gets or sets the address type.
    ''' </summary>
    <DataMember(Name:="address_type", EmitDefaultValue:=False)> _
    Public Property AddressType() As String
        Get
            Return m_AddressType
        End Get
        Set(ByVal value As String)
            m_AddressType = value
        End Set
    End Property
    Private m_AddressType As String
    ''' <summary>
    ''' Gets or sets the state code.
    ''' </summary>
    <DataMember(Name:="state_code", EmitDefaultValue:=False)> _
    Public Property StateCode() As String
        Get
            Return m_StateCode
        End Get
        Set(ByVal value As String)
            m_StateCode = value
        End Set
    End Property
    Private m_StateCode As String
    ''' <summary>
    ''' Gets or sets the contry code.
    ''' </summary>
    <DataMember(Name:="country_code", EmitDefaultValue:=False)> _
    Public Property CountryCode() As String
        Get
            Return m_CountryCode
        End Get
        Set(ByVal value As String)
            m_CountryCode = value
        End Set
    End Property
    Private m_CountryCode As String
    ''' <summary>
    ''' Gets or sets the postal code.
    ''' </summary>
    <DataMember(Name:="postal_code", EmitDefaultValue:=False)> _
    Public Property PostalCode() As String
        Get
            Return m_PostalCode
        End Get
        Set(ByVal value As String)
            m_PostalCode = value
        End Set
    End Property
    Private m_PostalCode As String
    ''' <summary>
    ''' Gets or sets the subpostal code.
    ''' </summary>
    <DataMember(Name:="sub_postal_code", EmitDefaultValue:=False)> _
    Public Property SubPostalCode() As String
        Get
            Return m_SubPostalCode
        End Get
        Set(ByVal value As String)
            m_SubPostalCode = value
        End Set
    End Property
    Private m_SubPostalCode As String

    ''' <summary>
    ''' Class constructor.
    ''' </summary>
    Public Sub New()
    End Sub
End Class

<DataContract()> _
<Serializable()> _
Public Class Contact
    Inherits Component
    ''' <summary>
    ''' Gets or sets the id.
    ''' </summary>
    <DataMember(Name:="id", EmitDefaultValue:=False)> _
    Public Property Id() As String
        Get
            Return m_Id
        End Get
        Set(ByVal value As String)
            m_Id = value
        End Set
    End Property
    Private m_Id As String
    ''' <summary>
    ''' Gets or sets the status.
    ''' </summary>
    <DataMember(Name:="status", EmitDefaultValue:=False)> _
    Public Property Status() As String
        Get
            Return m_Status
        End Get
        Set(ByVal value As String)
            m_Status = value
        End Set
    End Property
    Private m_Status As String
    ''' <summary>
    ''' Gets or sets the first name.
    ''' </summary>
    <DataMember(Name:="first_name", EmitDefaultValue:=False)> _
    Public Property FirstName() As String
        Get
            Return m_FirstName
        End Get
        Set(ByVal value As String)
            m_FirstName = value
        End Set
    End Property
    Private m_FirstName As String
    ''' <summary>
    ''' Gets or sets the middle name.
    ''' </summary>
    <DataMember(Name:="middle_name", EmitDefaultValue:=False)> _
    Public Property MiddleName() As String
        Get
            Return m_MiddleName
        End Get
        Set(ByVal value As String)
            m_MiddleName = value
        End Set
    End Property
    Private m_MiddleName As String
    ''' <summary>
    ''' Gets or sets the last name.
    ''' </summary>
    <DataMember(Name:="last_name", EmitDefaultValue:=False)> _
    Public Property LastName() As String
        Get
            Return m_LastName
        End Get
        Set(ByVal value As String)
            m_LastName = value
        End Set
    End Property
    Private m_LastName As String
    ''' <summary>
    ''' Gets or sets the confirmation flag.
    ''' </summary>
    <DataMember(Name:="confirmed", EmitDefaultValue:=False)> _
    Public Property Confirmed() As Boolean
        Get
            Return m_Confirmed
        End Get
        Set(ByVal value As Boolean)
            m_Confirmed = value
        End Set
    End Property
    Private m_Confirmed As Boolean
    ''' <summary>
    ''' Gets or sets the source.
    ''' </summary>
    <DataMember(Name:="source", EmitDefaultValue:=False)> _
    Public Property Source() As String
        Get
            Return m_Source
        End Get
        Set(ByVal value As String)
            m_Source = value
        End Set
    End Property
    Private m_Source As String
    ''' <summary>
    ''' Gets or sets the email addresses.
    ''' </summary>
    <DataMember(Name:="email_addresses")> _
    Public Property EmailAddresses() As IList(Of EmailAddress)
        Get
            Return m_EmailAddresses
        End Get
        Private Set(ByVal value As IList(Of EmailAddress))
            m_EmailAddresses = value
        End Set
    End Property
    Private m_EmailAddresses As IList(Of EmailAddress)
    ''' <summary>
    ''' Gets or sets the prefix name.
    ''' </summary>
    <DataMember(Name:="prefix_name", EmitDefaultValue:=False)> _
    Public Property PrefixName() As String
        Get
            Return m_PrefixName
        End Get
        Set(ByVal value As String)
            m_PrefixName = value
        End Set
    End Property
    Private m_PrefixName As String
    ''' <summary>
    ''' Gets or sets job title.
    ''' </summary>
    <DataMember(Name:="job_title", EmitDefaultValue:=False)> _
    Public Property JobTitle() As String
        Get
            Return m_JobTitle
        End Get
        Set(ByVal value As String)
            m_JobTitle = value
        End Set
    End Property
    Private m_JobTitle As String
    ''' <summary>
    ''' Gets or sets the addresses.
    ''' </summary>
    <DataMember(Name:="addresses")> _
    Public Property Addresses() As IList(Of Address)
        Get
            Return m_Addresses
        End Get
        Private Set(ByVal value As IList(Of Address))
            m_Addresses = value
        End Set
    End Property
    Private m_Addresses As IList(Of Address)
    ''' <summary>
    ''' Gets or sets the notes.
    ''' </summary>
    <DataMember(Name:="notes")> _
    Public Property Notes() As IList(Of Note)
        Get
            Return m_Notes
        End Get
        Set(ByVal value As IList(Of Note))
            m_Notes = value
        End Set
    End Property
    Private m_Notes As IList(Of Note)
    ''' <summary>
    ''' Gets or sets the company name.
    ''' </summary>
    <DataMember(Name:="company_name", EmitDefaultValue:=False)> _
    Public Property CompanyName() As String
        Get
            Return m_CompanyName
        End Get
        Set(ByVal value As String)
            m_CompanyName = value
        End Set
    End Property
    Private m_CompanyName As String
    ''' <summary>
    ''' Gets or sets the home phone.
    ''' </summary>
    <DataMember(Name:="home_phone", EmitDefaultValue:=False)> _
    Public Property HomePhone() As String
        Get
            Return m_HomePhone
        End Get
        Set(ByVal value As String)
            m_HomePhone = value
        End Set
    End Property
    Private m_HomePhone As String
    ''' <summary>
    ''' Gets or sets the work phone.
    ''' </summary>
    <DataMember(Name:="work_phone", EmitDefaultValue:=False)> _
    Public Property WorkPhone() As String
        Get
            Return m_WorkPhone
        End Get
        Set(ByVal value As String)
            m_WorkPhone = value
        End Set
    End Property
    Private m_WorkPhone As String
    ''' <summary>
    ''' Gets or sets the cell phone.
    ''' </summary>
    <DataMember(Name:="cell_phone", EmitDefaultValue:=False)> _
    Public Property CellPhone() As String
        Get
            Return m_CellPhone
        End Get
        Set(ByVal value As String)
            m_CellPhone = value
        End Set
    End Property
    Private m_CellPhone As String
    ''' <summary>
    ''' Gets or sets the fax number.
    ''' </summary>
    <DataMember(Name:="fax", EmitDefaultValue:=False)> _
    Public Property Fax() As String
        Get
            Return m_Fax
        End Get
        Set(ByVal value As String)
            m_Fax = value
        End Set
    End Property
    Private m_Fax As String
    ''' <summary>
    ''' Gets or sets the date and time the contact was added
    ''' </summary>
    <DataMember(Name:="created_date", EmitDefaultValue:=False)> _
    Public Property DateCreated() As String
        Get
            Return m_DateCreated
        End Get
        Set(ByVal value As String)
            m_DateCreated = value
        End Set
    End Property
    Private m_DateCreated As String
    ''' <summary>
    ''' Gets or sets the date and time contact's information was last modified
    ''' </summary>
    <DataMember(Name:="modified_date", EmitDefaultValue:=False)> _
    Public Property DateModified() As String
        Get
            Return m_DateModified
        End Get
        Set(ByVal value As String)
            m_DateModified = value
        End Set
    End Property
    Private m_DateModified As String
    ''' <summary>
    ''' Gets or sets the list of custom fields.
    ''' </summary>
    <DataMember(Name:="custom_fields")> _
    Public Property CustomFields() As IList(Of CustomField)
        Get
            Return m_CustomFields
        End Get
        Set(ByVal value As IList(Of CustomField))
            m_CustomFields = value
        End Set
    End Property
    Private m_CustomFields As IList(Of CustomField)
    ''' <summary>
    ''' Gets or sets the lists.
    ''' </summary>
    <DataMember(Name:="lists")> _
    Public Property Lists() As List(Of ContactList)
        Get
            Return m_Lists
        End Get
        Private Set(ByVal value As List(Of ContactList))
            m_Lists = value
        End Set
    End Property
    Private m_Lists As List(Of ContactList)
    ''' <summary>
    ''' Gets or sets the source details.
    ''' </summary>
    <DataMember(Name:="source_details", EmitDefaultValue:=False)> _
    Public Property SourceDetails() As String
        Get
            Return m_SourceDetails
        End Get
        Set(ByVal value As String)
            m_SourceDetails = value
        End Set
    End Property
    Private m_SourceDetails As String

    ''' <summary>
    ''' Class constructor.
    ''' </summary>
    Public Sub New()
        Me.EmailAddresses = New List(Of EmailAddress)()
        Me.Addresses = New List(Of Address)()
        Me.CustomFields = New List(Of CustomField)()
        Me.Lists = New List(Of ContactList)()
        Me.Notes = New List(Of Note)()
    End Sub
End Class


''' <summary>
''' Container for a get on a collection, such as Contacts, Campaigns, or TrackingData.
''' </summary>
''' <typeparam name="T">An object derived from Component class.</typeparam>
<DataContract()> _
<Serializable()> _
Public Class ResultSet(Of T As Component)
    ''' <summary>
    ''' Gets or sets the array of result objects returned.
    ''' </summary>
    <DataMember(Name:="results")> _
    Public Property Results() As IList(Of T)
        Get
            Return m_Results
        End Get
        Set(ByVal value As IList(Of T))
            m_Results = value
        End Set
    End Property
    Private m_Results As IList(Of T)
    ''' <summary>
    ''' Gets or sets the next link.
    ''' </summary>
    <DataMember(Name:="meta")> _
    Public Property Meta() As Meta
        Get
            Return m_Meta
        End Get
        Set(ByVal value As Meta)
            m_Meta = value
        End Set
    End Property
    Private m_Meta As Meta

    ''' <summary>
    ''' Class constructor.
    ''' </summary>
    Public Sub New()
    End Sub
End Class

''' <summary>
''' Meta class.
''' </summary>
<DataContract()> _
<Serializable()> _
Public Class Meta
    ''' <summary>
    ''' Gets or sets the pagination link.
    ''' </summary>
    <DataMember(Name:="pagination")> _
    Public Property Pagination() As Pagination
        Get
            Return m_Pagination
        End Get
        Set(ByVal value As Pagination)
            m_Pagination = Value
        End Set
    End Property
    Private m_Pagination As Pagination

    ''' <summary>
    ''' Class constructor.
    ''' </summary>
    Public Sub New()
    End Sub
End Class

''' <summary>
''' Pagination class.
''' </summary>
<DataContract()> _
<Serializable()> _
Public Class Pagination
    ''' <summary>
    ''' Gets or sets the next link.
    ''' </summary>
    <DataMember(Name:="next_link")> _
    Public Property [Next]() As String
        Get
            Return m_Next
        End Get
        Set(ByVal value As String)
            m_Next = Value
        End Set
    End Property
    Private m_Next As String

    ''' <summary>
    ''' Class constructor.
    ''' </summary>
    Public Sub New()
    End Sub

    ''' <summary>
    ''' Format the URL for the next page call.
    ''' </summary>
    ''' <returns>Returns the URL for the next page call.</returns>
    Public Function GetNextUrl() As String
        Return "" 'ACHTUNG! [String].Concat(Config.Endpoints.BaseUrl, Me.[Next].Replace("/v2/", [String].Empty))
    End Function
End Class

Public Class ConstantContactUtil

    Public Shared Function GetLists(ByVal apiKey As String, ByVal accessToken As String) As ContactList()
        Dim url As String = "https://api.constantcontact.com/v2/lists"
        Dim address As String = url
        Dim key1 As String

        Dim responseText As String = ""

        If url.Contains("?") Then
            key1 = "&"
        Else
            key1 = "?"
        End If

        If Not String.IsNullOrEmpty(apiKey) Then
            address = String.Format("{0}{1}api_key={2}", url, key1, apiKey)
        End If

        Dim request As HttpWebRequest = WebRequest.Create(address)

        request.ContentType = "application/json"
        request.Accept = "application/json"
        ' Add token as HTTP header
        request.Headers.Add("Authorization", "Bearer " + accessToken)

        Dim response As HttpWebResponse = Nothing
        Try
            response = request.GetResponse()
            If request.HaveResponse And response Is Nothing Then
                Throw New WebException("Response was not returned or is null")
            End If

            'urlResponse.StatusCode = response.StatusCode
            If response.StatusCode <> HttpStatusCode.OK Then
                Throw New WebException("Response with status: " + response.StatusCode + " " + response.StatusDescription)
            End If
            Using reader As New StreamReader(response.GetResponseStream(), Encoding.UTF8)
                responseText = reader.ReadToEnd()
            End Using
        Catch ex As Exception
        Finally
        End Try

        If Not String.IsNullOrEmpty(responseText) Then
            Using ms As New MemoryStream
                Dim buffer As Byte() = Encoding.UTF8.GetBytes(responseText)
                ms.Write(buffer, 0, buffer.Length)
                ms.Position = 0

                Dim ser As New DataContractJsonSerializer(GetType(ContactList()))
                Dim obj As ContactList()
                obj = TryCast(ser.ReadObject(ms), ContactList())
                Return obj
            End Using
        End If
        Return Nothing
    End Function

    'Public Shared LastError As String
    Public Shared Function GetByEmail(ByVal apiKey As String, ByVal accessToken As String, ByVal email As String) As ResultSet(Of Contact)

        Dim url As String = "https://api.constantcontact.com/v2/contacts?email=" & email & "&limit=1"
        Dim address As String = url
        Dim key1 As String

        Dim responseText As String = ""

        If url.Contains("?") Then
            key1 = "&"
        Else
            key1 = "?"
        End If

        If Not String.IsNullOrEmpty(apiKey) Then
            address = String.Format("{0}{1}api_key={2}", url, key1, apiKey)
        End If

        Dim request As HttpWebRequest = WebRequest.Create(address)

        request.ContentType = "application/json"
        request.Accept = "application/json"
        ' Add token as HTTP header
        request.Headers.Add("Authorization", "Bearer " + accessToken)

        Dim response As HttpWebResponse = Nothing
        Try
            response = request.GetResponse()
            If request.HaveResponse And response Is Nothing Then
                Throw New WebException("Response was not returned or is null")
            End If

            'urlResponse.StatusCode = response.StatusCode            
            Using reader As New StreamReader(response.GetResponseStream(), Encoding.UTF8)
                responseText = reader.ReadToEnd()
            End Using

            If response.StatusCode <> HttpStatusCode.OK Then
                Throw New WebException("Response with status: " + response.StatusCode + " " + response.StatusDescription)
            End If
        Catch ex As Exception
        Finally
        End Try

        If Not String.IsNullOrEmpty(responseText) Then
            Using ms As New MemoryStream
                Dim buffer As Byte() = Encoding.UTF8.GetBytes(responseText)
                ms.Write(buffer, 0, buffer.Length)
                ms.Position = 0

                Dim ser As New DataContractJsonSerializer(GetType(ResultSet(Of Contact)))
                Dim obj As ResultSet(Of Contact)
                obj = TryCast(ser.ReadObject(ms), ResultSet(Of Contact))
                Return obj
            End Using
        End If
        Return Nothing
    End Function

    Public Shared Sub UpdateContact(ByVal apiKey As String, ByVal accessToken As String, ByVal contact As Contact)
        Dim url As String = "https://api.constantcontact.com/v2/contacts/" & contact.Id

        Dim address As String = url
        Dim key1 As String

        If url.Contains("?") Then
            key1 = "&"
        Else
            key1 = "?"
        End If

        Dim responseText As String = ""

        If Not String.IsNullOrEmpty(apiKey) Then
            address = String.Format("{0}{1}api_key={2}", url, key1, apiKey)
        End If

        Dim request As HttpWebRequest = WebRequest.Create(address)

        request.Method = "PUT"
        request.ContentType = "application/json"
        request.Accept = "application/json"
        ' Add token as HTTP header
        request.Headers.Add("Authorization", "Bearer " + accessToken)

        'https://api.constantcontact.com/v2/contacts/1?api_key=62rgaqhb38mxcs98brbrndpq
        'https://api.constantcontact.com/v2/contacts/1?api_key=62rgaqhb38mxcs98brbrndpq

        'Dim serializer As New JavaScriptSerializer()
        'Dim json As String = serializer.Serialize(contact)
        Dim json As String = contact.ToJSON()
        Dim requestBodyBytes As Byte() = System.Text.Encoding.UTF8.GetBytes(json)
        request.GetRequestStream().Write(requestBodyBytes, 0, requestBodyBytes.Length)
        request.GetRequestStream().Close()

        Dim response As HttpWebResponse = Nothing
        Try
            response = TryCast(request.GetResponse(), HttpWebResponse)
            If request.HaveResponse And response Is Nothing Then
                Throw New WebException("Response was not returned or is null")
            End If

            'urlResponse.StatusCode = response.StatusCode            
            Using reader As New StreamReader(response.GetResponseStream(), Encoding.UTF8)
                responseText = reader.ReadToEnd()
            End Using

            If response.StatusCode <> HttpStatusCode.OK Then
                Throw New WebException("Response with status: " + response.StatusCode + " " + response.StatusDescription)
            End If
        Catch ex As Exception
            
        Finally
        End Try

    End Sub

    Public Shared Sub DeleteContact(ByVal apiKey As String, ByVal accessToken As String, ByVal contact As Contact)
        Dim url As String = "https://api.constantcontact.com/v2/contacts/" & contact.Id

        Dim address As String = url
        Dim key1 As String

        If url.Contains("?") Then
            key1 = "&"
        Else
            key1 = "?"
        End If

        Dim responseText As String = ""

        If Not String.IsNullOrEmpty(apiKey) Then
            address = String.Format("{0}{1}api_key={2}", url, key1, apiKey)
        End If

        Dim request As HttpWebRequest = WebRequest.Create(address)

        request.Method = "DELETE"
        'request.ContentType = "application/json"
        request.Accept = "application/json"
        ' Add token as HTTP header
        request.Headers.Add("Authorization", "Bearer " + accessToken)

        'https://api.constantcontact.com/v2/contacts/1?api_key=62rgaqhb38mxcs98brbrndpq
        'https://api.constantcontact.com/v2/contacts/1?api_key=62rgaqhb38mxcs98brbrndpq

        'Dim serializer As New JavaScriptSerializer()
        'Dim json As String = serializer.Serialize(contact)
        Dim json As String = contact.ToJSON()
        'Dim requestBodyBytes As Byte() = System.Text.Encoding.UTF8.GetBytes(json)
        'request.GetRequestStream().Write(requestBodyBytes, 0, requestBodyBytes.Length)
        'request.GetRequestStream().Close()

        Dim response As HttpWebResponse = Nothing
        Try
            response = TryCast(request.GetResponse(), HttpWebResponse)
            If request.HaveResponse And response Is Nothing Then
                Throw New WebException("Response was not returned or is null")
            End If

            'urlResponse.StatusCode = response.StatusCode
            If (response.StatusCode <> HttpStatusCode.OK) and (response.StatusCode <> HttpStatusCode.NoContent) Then
                Throw New WebException("Response with status: " + response.StatusCode + " " + response.StatusDescription)
            End If
            Using reader As New StreamReader(response.GetResponseStream(), Encoding.UTF8)
                responseText = reader.ReadToEnd()
            End Using
        Catch ex As Exception

        Finally
        End Try

    End Sub

    Public Shared Sub AddContact(ByVal apiKey As String, ByVal accessToken As String, ByVal contact As Contact)
        Dim url As String = "https://api.constantcontact.com/v2/contacts/"

        Dim address As String = url
        Dim key1 As String

        If url.Contains("?") Then
            key1 = "&"
        Else
            key1 = "?"
        End If

        Dim responseText As String

        If Not String.IsNullOrEmpty(apiKey) Then
            address = String.Format("{0}{1}api_key={2}", url, key1, apiKey)
        End If

        Dim request As HttpWebRequest = WebRequest.Create(address)

        request.Method = "POST"
        request.ContentType = "application/json"
        request.Accept = "application/json"
        ' Add token as HTTP header
        request.Headers.Add("Authorization", "Bearer " + accessToken)

        Dim json As String = contact.ToJSON()
        Dim requestBodyBytes As Byte() = System.Text.Encoding.UTF8.GetBytes(json)
        request.GetRequestStream().Write(requestBodyBytes, 0, requestBodyBytes.Length)
        request.GetRequestStream().Close()

        Dim response As HttpWebResponse = Nothing
        Try
            response = request.GetResponse()
            If request.HaveResponse And response Is Nothing Then
                Throw New WebException("Response was not returned or is null")
            End If

            'urlResponse.StatusCode = response.StatusCode
            If (response.StatusCode <> HttpStatusCode.OK) And (response.StatusCode <> HttpStatusCode.Created) Then
                Throw New WebException("Response with status: " + response.StatusCode + " " + response.StatusDescription)
            End If
            Using reader As New StreamReader(response.GetResponseStream(), Encoding.UTF8)
                responseText = reader.ReadToEnd()
            End Using
        Catch ex As Exception
        Finally
        End Try

    End Sub

End Class

Public Class ConstantContactHelper
    Public Shared Sub SubmitContactByID(ByVal apiKey As String, ByVal accessToken As String, ByVal email As String, ByVal name As String, ByVal comments As String, ByVal interests As String, ByVal contactListId As String)
        Dim contacts As ResultSet(Of Contact) = ConstantContactUtil.GetByEmail(apiKey, accessToken, email)
        Dim chosenContact As Contact = Nothing
        If Not contacts Is Nothing Then
            If contacts.Results.Count > 0 Then
                chosenContact = contacts.Results(0)
            End If
        End If

        Dim Deleted As Boolean = False

        If Not chosenContact Is Nothing Then

            Dim newContact As New Contact()
            newContact.Id = chosenContact.Id
            newContact.Status = chosenContact.Status

            For Each addr1 As Address In chosenContact.Addresses
                newContact.Addresses.Add(addr1)
            Next

            newContact.CellPhone = chosenContact.CellPhone
            newContact.CompanyName = chosenContact.CompanyName
            newContact.Confirmed = chosenContact.Confirmed
            newContact.DateCreated = chosenContact.DateCreated
            newContact.DateModified = chosenContact.DateModified

            'If Not String.IsNullOrEmpty(name) Then
            'Dim nameField As CustomField = New CustomField()
            'nameField.Name = "CustomField1"
            'nameField.Value = name
            'newContact.CustomFields.Add(nameField)
            'End If

            If Not String.IsNullOrEmpty(comments) Then
                Dim commentsField As CustomField = New CustomField()
                commentsField.Name = "CustomField2"
                commentsField.Value = comments
                newContact.CustomFields.Add(commentsField)
            End If

            If Not String.IsNullOrEmpty(interests) Then
                Dim interestsField As CustomField = New CustomField()
                interestsField.Name = "CustomField3"
                interestsField.Value = interests
                newContact.CustomFields.Add(interestsField)
            End If

            For Each email1 As EmailAddress In chosenContact.EmailAddresses
                newContact.EmailAddresses.Add(email1)
            Next

            newContact.Fax = chosenContact.Fax
            newContact.FirstName = System.Text.RegularExpressions.Regex.Match(name, "^(?:(?<firstname>[a-zA-Z]+(?: +[a-zA-Z]+)*) +)?(?=(?<lastname>[a-zA-Z]+)$)").Value 
            newContact.LastName =  System.Text.RegularExpressions.Regex.Match(name, "(?<=^(?:(?<firstname>[a-zA-Z]+(?: +[a-zA-Z]+)*) +)?)(?<lastname>[a-zA-Z]+)$").Value

            newContact.HomePhone = chosenContact.HomePhone
            newContact.JobTitle = chosenContact.JobTitle

            Dim ListFound As Boolean = False
            For Each cl1 As ContactList In chosenContact.Lists
                Dim newContactList As New ContactList
                newContactList.Id = cl1.Id
                newContact.Lists.Add(newContactList)
                If cl1.Id = contactListId Then
                    ListFound = True
                End If
            Next

            If Not ListFound Then
                Dim newContactList As New ContactList
                newContactList.Id = contactListId
                newContact.Lists.Add(newContactList)
            End If

            newContact.MiddleName = "" 'chosenContact.MiddleName

            For Each note1 As Note In chosenContact.Notes
                newContact.Notes.Add(note1)
            Next

            newContact.PrefixName = chosenContact.PrefixName
            newContact.Source = chosenContact.Source
            newContact.SourceDetails = chosenContact.SourceDetails
            newContact.WorkPhone = chosenContact.WorkPhone

            Dim jsonString As String = newContact.ToJSON()

            ConstantContactUtil.UpdateContact(apiKey, accessToken, newContact)
        Else
            Dim newContact As New Contact()
            'add lists [Required]
            Dim newContactList As New ContactList
            newContactList.Id = contactListId
            newContact.Lists.Add(newContactList)

            'add email_addresses [Required]
            Dim emailAddress As New EmailAddress()
            emailAddress.Status = "ACTIVE"
            emailAddress.ConfirmStatus = "NO_CONFIRMATION_REQUIRED"
            emailAddress.EmailAddr = email
            newContact.EmailAddresses.Add(emailAddress)
            newContact.Status = "ACTIVE"

            newContact.FirstName = System.Text.RegularExpressions.Regex.Match(name, "^(?:(?<firstname>[a-zA-Z]+(?: +[a-zA-Z]+)*) +)?(?=(?<lastname>[a-zA-Z]+)$)").Value
            newContact.LastName = System.Text.RegularExpressions.Regex.Match(name, "(?<=^(?:(?<firstname>[a-zA-Z]+(?: +[a-zA-Z]+)*) +)?)(?<lastname>[a-zA-Z]+)$").Value

            'If Not String.IsNullOrEmpty(name) Then
            'Dim nameField As CustomField = New CustomField()
            'nameField.Name = "CustomField1"
            'nameField.Value = name
            'newContact.CustomFields.Add(nameField)
            'End If

            If Not String.IsNullOrEmpty(comments) Then
                Dim commentsField As CustomField = New CustomField()
                commentsField.Name = "CustomField2"
                commentsField.Value = comments
                newContact.CustomFields.Add(commentsField)
            End If

            If Not String.IsNullOrEmpty(interests) Then
                Dim interestsField As CustomField = New CustomField()
                interestsField.Name = "CustomField3"
                interestsField.Value = interests
                newContact.CustomFields.Add(interestsField)
            End If

            ConstantContactUtil.AddContact(apiKey, accessToken, newContact)
        End If

    End Sub

    Public Shared Sub SubmitContact(ByVal apiKey As String, ByVal accessToken As String, ByVal email As String, ByVal name As String, ByVal comments As String, ByVal interests As String, ByVal contactList As String)
        Dim lists As ContactList() = ConstantContactUtil.GetLists(apiKey, accessToken)

        For Each cl2 As ContactList In lists
            If cl2.Name = contactList Then
                'find contactlist with necessary name
                SubmitContactByID(apiKey, accessToken, email, name, comments, interests, cl2.Id)
                Exit For
            End If
        Next

    End Sub
End Class
