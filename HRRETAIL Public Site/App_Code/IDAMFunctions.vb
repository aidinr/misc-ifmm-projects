Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Web
Imports System.Data
Imports System.Runtime.Serialization.Json
Imports Newtonsoft.Json
Imports System.Globalization
Public Class IDAMFunctions

    Public Shared Function GetNews(ByVal NewsType As Integer, ByVal Limit As Integer) As DataTable
        Dim QuerySQL As String = ""
        If Limit <> 0 Then
            QuerySQL &= "SELECT TOP " & Limit & " "
        Else
            QuerySQL &= "SELECT "
        End If
        QuerySQL &= "N.*, "
        QuerySQL &= "F1.Item_Value as NewsUrl, "
        QuerySQL &= "F2.Item_Value as UrlTitle, "
        QuerySQL &= "F3.Item_Value as RelProjects, "
        QuerySQL &= "DATENAME(mm,n.Post_Date) + ' ' + DATENAME(day,n.Post_Date) + ', ' + DATENAME(year, n.Post_Date) DateLoc, "
        QuerySQL &= "F4.Item_Value as EventStaff, "
        QuerySQL &= "F5.Item_Value as IDAM_NEWSINTRO "
        QuerySQL &= "FROM IPM_NEWS N "
        QuerySQL &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F1 on N.News_Id = F1.NEWS_ID and F1.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWSURL' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F2 on N.News_Id = F2.NEWS_ID and F2.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_URLTITLE' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F3 on N.News_Id = F3.NEWS_ID and F3.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWSRELATEDPROJ' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F4 on N.News_Id = F4.NEWS_ID and F4.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_EVENTSTAFF' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F5 on N.News_Id = F5.NEWS_ID and F5.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWSINTRO' AND Active = 1) "
        QuerySQL &= "WHERE 1=1 "
        QuerySQL &= "AND N.Show = 1 "
        If NewsType <> -1 Then
            QuerySQL &= "AND N.Type = " & NewsType & " "
        End If
        QuerySQL &= "AND N.post_date < getdate() "
        QuerySQL &= "AND N.pull_date > getdate() "
        QuerySQL &= "ORDER BY N.Post_Date desc "
        Dim QueryDT As New DataTable("QueryDT")
        QueryDT = DBFunctions.GetDataTable(QuerySQL)
        Return QueryDT
    End Function
    Public Shared Function PrintNews(ByVal NewsType As Integer, ByVal Limit As Integer, ByVal TemplatePage As String) As String
        Dim PrintOUT As String = ""
        Dim PrintDT As New DataTable("PrintDT")
        PrintDT = IDAMFunctions.GetNews(NewsType, Limit)
        If PrintDT.Rows.Count > 0 Then
            Select Case TemplatePage
                Case "Home"
                    PrintOUT &= "<ul class=""slider-3"">"
                    For R As Integer = 0 To PrintDT.Rows.Count - 1
                        PrintOUT &= "<li>"
                        PrintOUT &= "<span>" & formatfunctionssimple.AutoFormatText(PrintDT.Rows(R)("DateLoc").ToString.Trim) & "</span>"
                        PrintOUT &= "<a href=""#news!" & PrintDT.Rows(R)("News_ID").ToString.Trim & """>"
                        PrintOUT &= formatfunctionssimple.AutoFormatText(PrintDT.Rows(R)("Headline").ToString.Trim)
                        PrintOUT &= "</a><em>"
                        PrintOUT &= formatfunctions.AutoFormatText(PrintDT.Rows(R)("IDAM_NEWSINTRO").ToString.Trim)
                        PrintOUT &= "</em>"
                        PrintOUT &= "</a>"
                        PrintOUT &= "</li>"
                    Next
                    PrintOUT &= "</ul>"
                Case "News Page"
                    PrintOUT &= "<ul class=""left news-list"">"
                    For R As Integer = 0 To PrintDT.Rows.Count - 1
                        PrintOUT &= "<li class=""id" & PrintDT.Rows(R)("News_ID").ToString.Trim & """ > "
                        PrintOUT &= "<h2>" & formatfunctionssimple.AutoFormatText(PrintDT.Rows(R)("Headline").ToString.Trim) & "</h2>"
                        PrintOUT &= "<span class=""light-gray"">" & formatfunctionssimple.AutoFormatText(PrintDT.Rows(R)("DateLoc").ToString.Trim) & "</span>"
                        PrintOUT &= "<div class=""text"">"
                        PrintOUT &= "<p>" & formatfunctions.AutoFormatText(PrintDT.Rows(R)("Content").ToString.Trim) & ""
                        If PrintDT.Rows(R)("PDF").ToString.Trim = "1" Then
                            PrintOUT &= "<a href=""/dynamic/document/always/news/download/" & PrintDT.Rows(R)("News_ID").ToString.Trim & "/" & PrintDT.Rows(R)("News_ID").ToString.Trim & ".pdf"" target=""_blank"" class=""download"">Download</a>"
                        ElseIf PrintDT.Rows(R)("PDF").ToString.Trim = "0" And PrintDT.Rows(R)("NewsUrl").ToString.Trim <> "" Then
                            PrintOUT &= "<a href=""" & formatfunctionssimple.AutoFormatText(PrintDT.Rows(R)("NewsUrl").ToString.Trim) & """ target=""_blank"" class=""download"">VIEW</a>"
                        End If
                        PrintOUT &= "</p>"
                        PrintOUT &= "</div>"
                        PrintOUT &= "</li>"
                    Next
                    PrintOUT &= "</ul>"
                Case "Broker Blasts"
                    PrintOUT &= "<h2>broker blasts</h2>"
                    PrintOUT &= "<ul>"
                    For R As Integer = 0 To PrintDT.Rows.Count - 1
                        PrintOUT &= "<li>"
                        If PrintDT.Rows(R)("PDF").ToString.Trim = "1" Then
                            PrintOUT &= "<a href=""/dynamic/document/always/news/download/" & PrintDT.Rows(R)("News_ID").ToString.Trim & "/" & PrintDT.Rows(R)("News_ID").ToString.Trim & ".pdf"">" & formatfunctionssimple.AutoFormatText(PrintDT.Rows(R)("Headline").ToString.Trim) & ""
                        ElseIf PrintDT.Rows(R)("PDF").ToString.Trim = "0" And PrintDT.Rows(R)("NewsUrl").ToString.Trim <> "" Then
                            PrintOUT &= "<a href=""" & formatfunctionssimple.AutoFormatText(PrintDT.Rows(R)("NewsUrl").ToString.Trim) & """ >" & formatfunctionssimple.AutoFormatText(PrintDT.Rows(R)("Headline").ToString.Trim) & ""
                        Else
                            PrintOUT &= "<a href=""#news!" & PrintDT.Rows(R)("News_ID").ToString.Trim & """>" & formatfunctionssimple.AutoFormatText(PrintDT.Rows(R)("Headline").ToString.Trim) & ""
                        End If
                        PrintOUT &= "</a>"
                        Dim DateLoc As DateTime = PrintDT.Rows(R)("DateLoc").ToString.Trim
                        PrintOUT &= "<span class=""right"">" & DateLoc.ToString("MM/dd/yyyy") & "</span>"
                        PrintOUT &= "</li>"
                    Next
                    PrintOUT &= "</ul>	"
            End Select
        End If
        Return PrintOUT
    End Function

    Public Shared Function GetTeants(ByVal TenantType As Integer, ByVal Limit As Integer, ByVal Futured As Boolean) As DataTable
        Dim QuerySQL As String = ""
        If Limit <> 0 Then
            QuerySQL &= "SELECT TOP " & Limit & " "
        Else
            QuerySQL &= "SELECT "
        End If
        QuerySQL &= "T.*,  "
        QuerySQL &= "F1.Item_Value AS IDAM_TENANT_Retail_CATEGORY, "
        QuerySQL &= "F2.Item_Value AS IDAM_TENANT_SITE_CRITERIA, "
        QuerySQL &= "F3.Item_Value AS IDAM_TENANT_REPRESENTATIVE, "
        QuerySQL &= "F4.Item_Value AS IDAM_TENANT_CASE_STUDIES, "
        QuerySQL &= "F5.Item_Value AS IDAM_TENANTFEATURED, "
        QuerySQL &= "TA.Asset_ID AS LogoId, "
        QuerySQL &= "TA1.Asset_ID as BigImagId "

        QuerySQL &= "FROM IPM_TENANT T "

        QuerySQL &= "left JOIN "
        QuerySQL &= "(IPM_ASSET TA "
        QuerySQL &= "join IPM_ASSET_FIELD_VALUE F6 ON  TA.Asset_ID = F6.ASSET_ID and isnull(F6.Item_Value,'0')='1' AND F6.Item_ID = (Select Item_ID From IPM_ASSET_FIELD_DESC where Item_Tag = 'IDAM_LOGO' AND Active = 1) "
        QuerySQL &= "join (select A.Category_ID, min(A.Asset_ID) Asset_ID "
        QuerySQL &= "from IPM_ASSET A "
        QuerySQL &= "JOIN IPM_ASSET_FIELD_VALUE F ON A.Asset_ID = F.Asset_ID and isnull(F.Item_Value,'0')='1' "
        QuerySQL &= "AND F.Item_ID = (Select Item_ID From IPM_ASSET_FIELD_DESC where Item_Tag = 'IDAM_LOGO' AND Active = 1) "
        QuerySQL &= "where A.Available = 'Y' "
        QuerySQL &= "group by A.Category_ID ) X on TA.Category_ID = X.Category_ID and TA.Asset_ID = X.Asset_ID "
        QuerySQL &= ") ON TA.Category_ID = T.Tenant_ID AND TA.Available = 'Y' AND TA.Active = 1 "

        QuerySQL &= "left JOIN "
        QuerySQL &= "(IPM_ASSET TA1 "
        QuerySQL &= "left join IPM_ASSET_FIELD_VALUE F61 ON  TA1.Asset_ID = F61.ASSET_ID  and isnull(F61.Item_Value,'0')='0' AND F61.Item_ID = (Select Item_ID From IPM_ASSET_FIELD_DESC where Item_Tag = 'IDAM_LOGO' AND Active = 1) "
        QuerySQL &= "join (select A.Category_ID, min(A.Asset_ID) Asset_ID "
        QuerySQL &= "from IPM_ASSET A "
        QuerySQL &= "left JOIN IPM_ASSET_FIELD_VALUE F ON A.Asset_ID = F.Asset_ID "
        QuerySQL &= "AND F.Item_ID = (Select Item_ID From IPM_ASSET_FIELD_DESC where Item_Tag = 'IDAM_LOGO' AND Active = 1) "
        QuerySQL &= "where A.Available = 'Y' "
        QuerySQL &= "and isnull(F.Item_Value,'0')='0' "
        QuerySQL &= "group by A.Category_ID ) X1 on TA1.Category_ID = X1.Category_ID and TA1.Asset_ID = X1.Asset_ID "
        QuerySQL &= ") ON TA1.Category_ID = T.Tenant_ID AND TA1.Available = 'Y' AND TA1.Active = 1  "
        QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F1 ON T.Tenant_ID = F1.TENANT_ID AND F1.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_Retail_CATEGORY' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F2 ON T.Tenant_ID = F2.TENANT_ID AND F2.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_SITE_CRITERIA' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F3 ON T.Tenant_ID = F3.TENANT_ID AND F3.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_REPRESENTATIVE' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F4 ON T.Tenant_ID = F4.TENANT_ID AND F4.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_CASE_STUDIES' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F5 ON T.Tenant_ID = F5.TENANT_ID AND F5.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANTFEATURED' AND Active = 1) "
        QuerySQL &= "WHERE 1 = 1 "
        QuerySQL &= "AND T.Active = 1 "
        QuerySQL &= "AND T.Available = 'Y' "
        QuerySQL &= "AND F1.Item_Value is not null AND F1.Item_Value <> '' "
        QuerySQL &= "and TA.Asset_ID is not null "
        If Futured = True Then
            QuerySQL &= "AND F5.Item_Value = '1' "
        End If
        QuerySQL &= "ORDER BY ShortName "
        Dim QueryDT As New DataTable("QueryDT")
        QueryDT = DBFunctions.GetDataTable(QuerySQL)
        Return QueryDT
    End Function
    Public Shared Function PrintTeants(ByVal TenantType As Integer, ByVal Limit As Integer, ByVal Futured As Boolean, ByVal TemplatePage As String) As String
        Dim PrintOUT As String = ""
        Dim PrintDT As New DataTable("PrintDT")
        PrintDT = IDAMFunctions.GetTeants(TenantType, Limit, Futured)
        If PrintDT.Rows.Count > 0 Then
            Select Case TemplatePage
                Case "Home Page"
                    PrintOUT &= "<ul class=""slider-1"">"
                    For R As Integer = 0 To PrintDT.Rows.Count - 1
                        PrintOUT &= "<li><a href=""#retailers-and-restaurants!" & PrintDT.Rows(R)("Tenant_ID").ToString.Trim & """>"
                        PrintOUT &= "<img src=""/dynamic/image/always/asset/padded/168x155/85/ffffff/Center/" & PrintDT.Rows(R)("LogoId").ToString.Trim & ".png"" alt=""""/>"
                        PrintOUT &= "</a></li>"
                    Next
                    PrintOUT &= "</ul>"
                Case "RR Page"
                    PrintOUT &= "<ul class=""retailer-list info-pops"">"
                    For R As Integer = 0 To PrintDT.Rows.Count - 1
                        PrintOUT &= "<li class=""rollover-item imgG"">"
                        PrintOUT &= "<a class=""id" & PrintDT.Rows(R)("Tenant_ID").ToString.Trim & """>"
                        PrintOUT &= "<img src=""/assets/images/clear.gif"" longdesc=""/dynamic/image/always/asset/padded/168x155/85/ffffff/Center/" & PrintDT.Rows(R)("LogoId").ToString.Trim & ".png"" alt=""""/>"
                        PrintOUT &= "</a>"
                        PrintOUT &= "<div class=""info-pop rr"">"
                        PrintOUT &= "<div class=""info-pop-content"">"
                        PrintOUT &= "<div class=""fluid-padding"">"
                        PrintOUT &= "<div class=""rCol1"">"
                        If PrintDT.Rows(R)("BigImagId").ToString <> "" Then
                            PrintOUT &= "<img src=""/assets/images/clear.gif"" longdesc=""/dynamic/image/always/asset/fit/520x325/85/333333/NorthWest/" & PrintDT.Rows(R)("BigImagId").ToString.Trim & ".jpg"" alt=""""/>"
                        End If
                        PrintOUT &= "</div>"
                        PrintOUT &= "<div class=""rCol2"">"
                        PrintOUT &= "<h3>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R)("LongName").ToString.Trim) & "</h3>"
                        PrintOUT &= "<p>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R)("Description").ToString.Trim) & "</p>"
                        PrintOUT &= "</div>"
                        PrintOUT &= "<div class=""rCol3"">"
                        PrintOUT &= "<h3>Retail Category</h3>"
                        PrintOUT &= "<p class=""cat"">" & PrintDT.Rows(R)("IDAM_TENANT_Retail_CATEGORY").ToString.Trim & "</p>"

                        If PrintDT.Rows(R)("IDAM_TENANT_SITE_CRITERIA").ToString <> "" Then
                            PrintOUT &= "<h3>Site Criteria</h3>"
                            PrintOUT &= "<p>"
                            PrintOUT &= PrintDT.Rows(R)("IDAM_TENANT_SITE_CRITERIA").ToString.Trim
                            PrintOUT &= "</p>"
                        End If
                        'REPRESENTATIVE
                        If PrintDT.Rows(R)("IDAM_TENANT_REPRESENTATIVE").ToString <> "" Then
                            Dim ListOfGroups As String()
                            ListOfGroups = PrintDT.Rows(R)("IDAM_TENANT_REPRESENTATIVE").ToString.Split(",")
                            Dim FormattedListOfGroups As String = ""
                            For Each Group As String In ListOfGroups
                                'If IsNumeric(Group) Then
                                FormattedListOfGroups &= "'" & Group.ToString.Trim & "',"
                                'End If
                            Next
                            If FormattedListOfGroups <> "" Then
                                Dim QuerySQL As String = ""
                                QuerySQL &= "SELECT "
                                QuerySQL &= "U.UserID, "
                                QuerySQL &= "F4.Item_Value AS IDAM_FULLNAME "
                                QuerySQL &= "FROM IPM_USER U "
                                QuerySQL &= "LEFT JOIN IPM_USER_FIELD_VALUE F4 ON U.UserID = F4.USER_ID AND F4.Item_ID = (SELECT Item_ID FROM IPM_USER_FIELD_DESC WHERE Item_Tag = 'IDAM_FULLNAME' AND Active = 1) "
                                QuerySQL &= "WHERE F4.Item_Value IN (" & Left(FormattedListOfGroups, Len(FormattedListOfGroups) - 1) & ")"
                                QuerySQL &= "AND F4.Item_Value is not null AND F4.Item_Value <> '' "
                                QuerySQL &= "ORDER BY REVERSE(LEFT(REVERSE(F4.Item_Value), CHARINDEX(' ', REVERSE(F4.Item_Value)) - 1)) "

                                Dim UsersDT As New DataTable("UsersDT")
                                UsersDT = DBFunctions.GetDataTable(QuerySQL)

                                If UsersDT.Rows.Count > 0 Then
                                    If UsersDT.Rows.Count > 1 Then
                                        PrintOUT &= "<h3>Tenant REPRESENTATIVES</h3>"
                                    Else
                                        PrintOUT &= "<h3>Tenant REPRESENTATIVE</h3>"
                                    End If
                                    PrintOUT &= "<p class=""broker"">"
                                    For U As Integer = 0 To UsersDT.Rows.Count - 1
                                        PrintOUT &= "<a href=""#team!" & UsersDT.Rows(U).Item("UserID").ToString.Trim & """>" & UsersDT.Rows(U).Item("IDAM_FULLNAME") & "</a>"
                                    Next
                                    PrintOUT &= "</p>"
                                End If


                            End If
                        End If

                        'Case Studies
                        If PrintDT.Rows(R)("IDAM_TENANT_CASE_STUDIES").ToString <> "" Then
                            Dim ListOfGroups As String()
                            ListOfGroups = PrintDT.Rows(R)("IDAM_TENANT_CASE_STUDIES").ToString.Split(",")
                            Dim FormattedListOfGroups As String = ""
                            For Each Group As String In ListOfGroups
                                If IsNumeric(Group) Then
                                    FormattedListOfGroups &= Group & ","
                                End If
                            Next
                            If FormattedListOfGroups <> "" Then
                                Dim QuerySQL As String = ""
                                QuerySQL &= "SELECT "
                                QuerySQL &= "P.ProjectID,  "
                                QuerySQL &= "P.Name  "
                                QuerySQL &= "FROM IPM_PROJECT P  "
                                QuerySQL &= "WHERE P.ProjectID IN (" & Left(FormattedListOfGroups, Len(FormattedListOfGroups) - 1) & ")"
                                Dim CaseStudiesDT As New DataTable("CaseStudiesDT")
                                CaseStudiesDT = DBFunctions.GetDataTable(QuerySQL)
                                If CaseStudiesDT.Rows.Count > 0 Then
                                    If CaseStudiesDT.Rows.Count > 1 Then
                                        PrintOUT &= "<h3>Case Studies</h3>"
                                    Else
                                        PrintOUT &= "<h3>Case Study</h3>"
                                    End If
                                    PrintOUT &= "<p>"
                                    For U As Integer = 0 To CaseStudiesDT.Rows.Count - 1
                                        PrintOUT &= "<a href=""#case-studies!" & CaseStudiesDT.Rows(U).Item("ProjectID") & """>" & FormatFunctions.AutoFormatText(CaseStudiesDT.Rows(U).Item("Name").ToString.Trim) & "</a>"
                                    Next
                                    PrintOUT &= "</p>"
                                End If
                            End If
                        End If

                        PrintOUT &= "</div>"
                        PrintOUT &= "<br class=""clear"" />"
                        PrintOUT &= "<button type=""button"" class=""close-pop""></button>"
                        PrintOUT &= "</div>"
                        PrintOUT &= "</div>"
                        PrintOUT &= "</div>"
                        PrintOUT &= "</li>"
                    Next
                    PrintOUT &= "</ul>"
            End Select


        End If
        Return PrintOUT
    End Function

    Public Shared Function GetUsers(ByVal TitleGroups As String, ByVal Limit As Integer) As DataTable
        Dim QuerySQL As String = ""
        If Limit <> 0 Then
            QuerySQL &= "SELECT TOP " & Limit & " "
        Else
            QuerySQL &= "SELECT "
        End If
        QuerySQL &= "U.*, "
        QuerySQL &= "F1.Item_Value AS IDAM_EDUCATION, "
        QuerySQL &= "F2.Item_Value AS IDAM_USERSORT, "
        QuerySQL &= "F3.Item_Value AS IDAM_SPECIALTIES, "
        QuerySQL &= "F4.Item_Value AS IDAM_FULLNAME "
        QuerySQL &= "FROM IPM_USER U "
        QuerySQL &= "LEFT JOIN IPM_USER_FIELD_VALUE F1 ON U.UserID = F1.USER_ID AND F1.Item_ID = (SELECT Item_ID FROM IPM_USER_FIELD_DESC WHERE Item_Tag = 'IDAM_EDUCATION' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_USER_FIELD_VALUE F2 ON U.UserID = F2.USER_ID AND F2.Item_ID = (SELECT Item_ID FROM IPM_USER_FIELD_DESC WHERE Item_Tag = 'IDAM_USERSORT' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_USER_FIELD_VALUE F3 ON U.UserID = F3.USER_ID AND F3.Item_ID = (SELECT Item_ID FROM IPM_USER_FIELD_DESC WHERE Item_Tag = 'IDAM_SPECIALTIES' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_USER_FIELD_VALUE F4 ON U.UserID = F4.USER_ID AND F4.Item_ID = (SELECT Item_ID FROM IPM_USER_FIELD_DESC WHERE Item_Tag = 'IDAM_FULLNAME' AND Active = 1) "
        QuerySQL &= "WHERE 1 = 1 "
        QuerySQL &= "AND Active = 'Y' "
        QuerySQL &= "AND F4.Item_Value is not null AND F4.Item_Value <> '' "
        QuerySQL &= "ORDER BY CAST(isnull(F2.Item_Value, -1) as integer) desc    "
        'If TitleGroups <> "" Then
        '    Dim ListOfGroups As String()
        '    ListOfGroups = TitleGroups.Split(",")
        '    Dim FormattedListOfGroups As String = ""
        '    For Each Group As String In ListOfGroups
        '        FormattedListOfGroups &= "'" & Group & "',"
        '    Next
        '    QuerySQL &= "AND U.Position IN (" & Left(FormattedListOfGroups, Len(FormattedListOfGroups) - 1) & ")"
        'End If
        Dim QueryDT As New DataTable("QueryDT")
        QueryDT = DBFunctions.GetDataTable(QuerySQL)
        Return QueryDT
    End Function
    Public Shared Function PrintUsers(ByVal TitleGroups As String, ByVal Limit As Integer, ByVal HeaderTitle As String) As String
        Dim PrintOUT As String = ""
        Dim PrintDT As New DataTable("PrintDT")
        PrintDT = IDAMFunctions.GetUsers(TitleGroups, Limit)
        If PrintDT.Rows.Count > 0 Then
            Dim UniqueDepartmentsDT As New DataTable
            UniqueDepartmentsDT = PrintDT.DefaultView.ToTable(True, "Department")
            Dim UsersDT As DataTable
            For R As Integer = 0 To UniqueDepartmentsDT.Rows.Count - 1
                PrintOUT &= "<h2 class=""people-title"">" & UniqueDepartmentsDT.Rows(R).Item("Department") & "</h2>"
                PrintOUT &= "<ul class=""team-list info-pops"">"
                PrintDT.DefaultView.RowFilter = "Department = '" & UniqueDepartmentsDT.Rows(R).Item("Department") & "'"
                UsersDT = New DataTable
                UsersDT = PrintDT.DefaultView.ToTable
                For R2 As Integer = 0 To UsersDT.Rows.Count - 1
                    PrintOUT &= "<li class=""rollover-item"">"
                    PrintOUT &= "<div class=""hover-parent"">"
                    PrintOUT &= "<img src=""" & "/dynamic/image/always/contact/best/220x154/92/ffffff/North/" & UsersDT.Rows(R2)("UserID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
                    PrintOUT &= "<a class=""show-info-pop rollover id" & UsersDT.Rows(R2).Item("UserID").ToString.Trim & """><i></i></a>"
                    PrintOUT &= "</div>"
                    PrintOUT &= "<div class=""titles"">"
                    PrintOUT &= "<h2>" & UsersDT.Rows(R2).Item("IDAM_FULLNAME").ToString.Trim & "</h2>"
                    PrintOUT &= "<p>" & UsersDT.Rows(R2).Item("Position").ToString.Trim & "</p>"
                    PrintOUT &= "</div>"
                    PrintOUT &= "<div class=""info-pop""><div class=""info-pop-content"">"
                    PrintOUT &= "<div class=""fluid-padding"">"
                    PrintOUT &= "<div class=""text first"">"
                    PrintOUT &= "<h3>" & UsersDT.Rows(R2).Item("FirstName").ToString.Trim & " " & UsersDT.Rows(R2).Item("LastName").ToString.Trim
                    PrintOUT &= "<em>" & UsersDT.Rows(R2).Item("Position").ToString.Trim & "<em></h3>" 
                    PrintOUT &= "<h6>Phone</h6>"
                    PrintOUT &= "" & UsersDT.Rows(R2).Item("Phone").ToString.Trim & ""
                    PrintOUT &= "<h6>direct</h6>"
                    PrintOUT &= "" & UsersDT.Rows(R2).Item("Cell").ToString.Trim & ""
                    PrintOUT &= "<h6>email</h6>"
                    PrintOUT &= "<a href=""mailto:" & UsersDT.Rows(R2).Item("Email").ToString.Trim & """>" & UsersDT.Rows(R2).Item("Email").ToString.Trim & "</a>"
                    PrintOUT &= "<br/><br/>"
                    PrintOUT &= "<a href=""vCard-" & UsersDT.Rows(R2)("UserID").ToString.Trim & ".vcf"" class=""fancy-download"">vCard</a>"
                    PrintOUT &= "</div>							"
                    PrintOUT &= "<div class=""text second"">"
                    PrintOUT &= formatfunctions.AutoFormatText(UsersDT.Rows(R2).Item("Bio").ToString.Trim)
                    PrintOUT &= "</div>"
                    PrintOUT &= "<div class=""text last"">"
                    PrintOUT &= "<h6>Specialties</h6>"
                    PrintOUT &= "<p>" & FormatFunctions.AutoFormatText(UsersDT.Rows(R2).Item("IDAM_SPECIALTIES").ToString.Trim) & "</p>"
                    PrintOUT &= "<h6>EDUCATION</h6>"
                    PrintOUT &= "<p><span>" & formatfunctions.AutoFormatText(UsersDT.Rows(R2).Item("IDAM_EDUCATION").ToString.Trim) & "</span></p>"

                    Dim QuerySQL As String = ""
                    QuerySQL &= "SELECT "
                    QuerySQL &= "P.ProjectID, "
                    QuerySQL &= "P.Name "
                    QuerySQL &= "FROM IPM_PROJECT P  "
                    QuerySQL &= "JOIN IPM_PROJECT_CONTACT PC ON PC.UserID = " & UsersDT.Rows(R2).Item("UserID").ToString.Trim & " AND PC.ProjectID = P.ProjectID "
                    QuerySQL &= "AND 1=1 "
                    QuerySQL &= "AND P.Available = 'Y' "
                    QuerySQL &= "AND P.Show = 1 "
                    QuerySQL &= "AND P.Category_ID = 2410606 "
                    Dim CaseStudiesDT As New DataTable("CaseStudiesDT")
                    CaseStudiesDT = DBFunctions.GetDataTable(QuerySQL)
                    If CaseStudiesDT.Rows.Count > 0 Then
                        If CaseStudiesDT.Rows.Count > 1 Then
                            PrintOUT &= "<h6>Case Studies</h6>"
                        Else
                            PrintOUT &= "<h6>Case Study</h6>"
                        End If
                        For U As Integer = 0 To CaseStudiesDT.Rows.Count - 1
                            PrintOUT &= "<a href=""#case-studies!" & CaseStudiesDT.Rows(U).Item("ProjectID") & """>" & CaseStudiesDT.Rows(U).Item("Name") & "</a><br />"
                        Next
                    End If


                    PrintOUT &= "</div>"
                    PrintOUT &= "<br class=""clear""/>"
                    PrintOUT &= "<button type=""button"" class=""close-pop""></button>"
                    PrintOUT &= "</div>"
                    PrintOUT &= "</div></div>"
                    PrintOUT &= "</li>"

                Next
                PrintOUT &= "</ul>"
            Next
        End If
        Return PrintOUT
    End Function

    Public Shared Function GetProjects(ByVal CategoryGroups As String, ByVal Limit As Integer, ByVal TemplatePage As String, ByVal ProjectFilters As ProjectFiltersStructure) As DataTable
        Dim ProjectFiltersSQL As New ProjectFiltersStructure
        ProjectFiltersSQL = ProjectFilters
        Dim QuerySQL As String = ""
        If Limit <> 0 Then
            QuerySQL &= "SELECT TOP " & Limit & " "
        Else
            QuerySQL &= "SELECT "
        End If
        QuerySQL &= " * FROM Projects_Cache P "
        QuerySQL &= "WHERE 1 = 1 "

        If TemplatePage <> "Case Studies Home" And TemplatePage <> "Case Studies" And TemplatePage <> "Retail Solutions" Then
            QuerySQL &= "AND P.ProjectState is not null and P.ProjectState <> '' "
            QuerySQL &= "AND P.ProjectCounty is not null and P.ProjectCounty <> '' "
        End If
        'Projects Collection
        If ProjectFiltersSQL.ProjectCollection <> "" Then
            Dim ListOfGroups As String()
            ListOfGroups = ProjectFiltersSQL.ProjectCollection.Split(VbNewLine)
            Dim FormattedListOfGroups As String = ""
            For Each Group As String In ListOfGroups
                If IsNumeric(Group) Then
                    FormattedListOfGroups &= Group & ","
                End If
            Next
            If FormattedListOfGroups <> "" Then
                QuerySQL &= "AND P.ProjectId IN (" & Left(FormattedListOfGroups, Len(FormattedListOfGroups) - 1) & ")"
            End If
        End If
        'Property Space
        If ProjectFiltersSQL.SizeOfSpace <> "" Then
            Select Case ProjectFiltersSQL.SizeOfSpace
                Case "x05000SF"
                    QuerySQL &= "AND P.Size0_5000 = 1 "
                Case "x500115000SF"
                    QuerySQL &= "AND P.Size5001_15000 = 1 "
                Case "x15001SFp"
                    QuerySQL &= "AND P.Size15000p = 1 "
                Case "PadSites"
                    QuerySQL &= "AND P.Pad = 1  "
            End Select
        End If
        'Property State
        If ProjectFiltersSQL.PropertyState <> "" Then
            QuerySQL &= "AND P.ProjectState =  '" & Replace(HttpUtility.UrlDecode(ProjectFiltersSQL.PropertyState), "'", "''") & "'  "
        End If
        'Property County
        If ProjectFiltersSQL.PropertyCounty <> "" Then
            Dim CountyS As String() = HttpUtility.UrlDecode(ProjectFiltersSQL.PropertyCounty).Split("/")
            QuerySQL &= "AND P.ProjectCounty =  '" & Replace(CountyS(0).Trim, "'", "''") & "'  "
            QuerySQL &= "AND P.ProjectState =  '" & Replace(CountyS(1).Trim, "'", "''") & "'  "
        End If
        'Property Anchors
        If ProjectFiltersSQL.PropertyAnchor <> "" Then
            QuerySQL &= "AND P.ProjectID	IN ( "
            QuerySQL &= "SELECT "
            QuerySQL &= "P.ProjectID "
            QuerySQL &= "FROM IPM_SPACE S "
            QuerySQL &= "JOIN IPM_SPACE_TENANT ST (nolock) ON S.Space_ID = ST.Space_ID "
            QuerySQL &= "JOIN IPM_TENANT T (nolock) ON ST.Tenant_ID = T.Tenant_ID "
            QuerySQL &= "JOIN IPM_PROJECT P (nolock) ON P.ProjectID = S.ProjectID  "
            QuerySQL &= "WHERE 1 = 1 "
            QuerySQL &= "AND T.Active = 1 "
            QuerySQL &= "AND T.Available = 'Y' "
            QuerySQL &= "AND P.Available = 'Y' "
            QuerySQL &= "AND P.Show = 1 "
            QuerySQL &= "AND T.ShortName = '" & Replace(HttpUtility.UrlDecode(ProjectFiltersSQL.PropertyAnchor), "'", "''") & "'  "
            QuerySQL &= ")"
        End If
        'Search Phrase
        If ProjectFiltersSQL.SearchPhrase <> "" Then
            QuerySQL &= "AND ( P.ProjectName like '%" & ProjectFiltersSQL.SearchPhrase & "%' or P.ProjectAddress like '%" & ProjectFiltersSQL.SearchPhrase & "%'  or P.ProjectState like '%" & ProjectFiltersSQL.SearchPhrase & "%' or P.Zip like '%" & ProjectFiltersSQL.SearchPhrase & "%' or P.Search_Tags like '%" & ProjectFiltersSQL.SearchPhrase & "%' "
            QuerySQL &= "OR P.ProjectID	IN ( "
            QuerySQL &= "SELECT "
            QuerySQL &= "P.ProjectID "
            QuerySQL &= "FROM IPM_SPACE S (nolock) "
            QuerySQL &= "JOIN IPM_SPACE_TENANT ST (nolock) ON S.Space_ID = ST.Space_ID "
            QuerySQL &= "JOIN IPM_TENANT T (nolock) ON ST.Tenant_ID = T.Tenant_ID "
            QuerySQL &= "JOIN IPM_PROJECT P (nolock) ON P.ProjectID = S.ProjectID  "
            QuerySQL &= "WHERE 1 = 1 "
            QuerySQL &= "AND T.Active = 1 "
            QuerySQL &= "AND T.Available = 'Y' "
            QuerySQL &= "AND P.Available = 'Y' "
            QuerySQL &= "AND P.Show = 1 "
            QuerySQL &= "AND T.ShortName like '%" & Replace(ProjectFiltersSQL.SearchPhrase, "'", "''") & "%'  "
            QuerySQL &= "))"
        End If
        'Project Categories
        If CategoryGroups <> "" Then
            If CategoryGroups.Contains(",") Then
                Dim ListOfGroups As String()
                ListOfGroups = CategoryGroups.Split(",")
                Dim FormattedListOfGroups As String = ""
                For Each Group As String In ListOfGroups
                    FormattedListOfGroups &= "'" & Group & "',"
                Next
                QuerySQL &= "AND P.Category_ID IN (" & Left(FormattedListOfGroups, Len(FormattedListOfGroups) - 1) & ")"
            Else
                QuerySQL &= "AND P.Category_ID IN (" & CategoryGroups & ")"
            End If
        End If

        QuerySQL &= " ORDER BY P.Space_Available DESC   "

        Dim QueryDT As New DataTable("QueryDT")
        QueryDT = DBFunctions.GetDataTable(QuerySQL)
        Return QueryDT
    End Function
    Public Shared Function PrintProjects(ByVal CategoryGroups As String, ByVal Limit As Integer, ByVal ImageFromAssets As Integer, ByVal TemplatePage As String, ByVal ProjectFilters As ProjectFiltersStructure) As String
        Dim ProjectFiltersSQL As New ProjectFiltersStructure
        ProjectFiltersSQL = ProjectFilters

        Dim PrintOUT As String = ""
        Dim PrintDT As New DataTable("PrintDT")
        PrintDT = IDAMFunctions.GetProjects(CategoryGroups, Limit, TemplatePage, ProjectFilters)
        If PrintDT.Rows.Count > 0 Then
            '---TEMPLATE START---'
            Select Case TemplatePage
                Case "Properties"
                    Dim MapPrintOUT As String = ""
                    Dim ListPrintOut As String = ""

                    Dim MapResponseList As New List(Of MapResponseStructure)
                    MapPrintOUT &= "<div id=""gmapJ"">"
                    ListPrintOut &= "<div id=""search-details"" class=""fluid-padding"">"
                    ListPrintOut &= "<ul class=""property-list"">"
                    For R As Integer = 0 To PrintDT.Rows.Count - 1

                        Dim LinkOut As String = ""
                        If PrintDT.Rows(R).Item("SeoURL").ToString.Trim <> "" Then
                            LinkOut &= "#" & CMSFunctions.FormatFriendlyUrl_NoLower(PrintDT.Rows(R).Item("SeoURL").ToString.Trim, "-")
                        Else
                            LinkOut &= "#property-" & PrintDT.Rows(R).Item("ProjectID").ToString.Trim & "-" & CMSFunctions.FormatFriendlyUrl(PrintDT.Rows(R).Item("ProjectName").ToString.Trim, "-")
                        End If
                        Dim IdOut As String = ""
                        IdOut = PrintDT.Rows(R).Item("ProjectID").ToString.Trim
                        Dim AddressOut As String = ""
                        AddressOut = PrintDT.Rows(R).Item("ProjectAddress").ToString.Trim
                        Dim SfOut As String = ""
                        If PrintDT.Rows(R).Item("Space_Available").ToString <> "" Then
                            Dim SQLDec As Decimal = PrintDT.Rows(R).Item("Space_Available")
                            SfOut = SQLDec.ToString("#,0") & " SF"
                        End If
                        Dim NameOut As String = ""
                        NameOut = formatfunctions.AutoFormatText(PrintDT.Rows(R).Item("ProjectName").ToString.Trim)

                        Dim MapResponse As New MapResponseStructure
                        If PrintDT.Rows(R).Item("LatLong").ToString.Trim <> "" And PrintDT.Rows(R).Item("LatLong").Contains(",") Then
                            MapResponse.lat = PrintDT.Rows(R).Item("LatLong").Split(",")(0).ToString.Trim
                            MapResponse.lng = PrintDT.Rows(R).Item("LatLong").Split(",")(1).ToString.Trim
                        End If

                        MapResponse.data = New PropertyDataStructure With {.link = LinkOut, .title = NameOut, .id = IdOut, .sf = SfOut, .address = AddressOut}
                        If CMSFunctions.IsValidCoords(PrintDT.Rows(R).Item("LatLong").ToString.Trim) = True Then
                            MapResponseList.Add(MapResponse)
                        End If

                        ListPrintOut &= "<li>"
                        If PrintDT.Rows(R).Item("SeoURL").ToString.Trim <> "" Then
                            ListPrintOut &= "<a href=""" & CMSFunctions.FormatFriendlyUrl_NoLower(PrintDT.Rows(R).Item("SeoURL").ToString.Trim, "-") & """>"
                        Else
                            ListPrintOut &= "<a href=""#property-" & PrintDT.Rows(R).Item("ProjectID").ToString.Trim & "-" & CMSFunctions.FormatFriendlyUrl(PrintDT.Rows(R).Item("ProjectName").ToString.Trim, "-") & """ class=""img-link"">"
                        End If
                        ListPrintOut &= "<img class=""prop"" src=""assets/images/clear.gif"" longdesc=""" & "/dynamic/image/always/project/best/220x153/92/ffffff/Center/" & PrintDT.Rows(R).Item("ProjectID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
                        ListPrintOut &= "<strong>" & formatfunctions.AutoFormatText(PrintDT.Rows(R).Item("ProjectName").ToString.Trim) & "</strong>"
                        ListPrintOut &= "<i class=""Street"">" & PrintDT.Rows(R).Item("ProjectAddress").ToString.Trim & "</i>"
                        ListPrintOut &= "<i class=""City"">" & PrintDT.Rows(R).Item("ProjectCity").ToString.Trim & ", " & PrintDT.Rows(R).Item("State_Id").ToString.Trim & "</i>"
                        ListPrintOut &= "<em>Space Available<br />" & SfOut & "</em>"
                        ListPrintOut &= "</a>"
                        ListPrintOut &= "</li>"
                    Next
                    MapPrintOUT &= JsonConvert.SerializeObject(MapResponseList, Formatting.Indented) & "</div>"
                    ListPrintOut &= "</ul>"
                    ListPrintOut &= "</div>"

                    Dim ButtomOUT As String = ""
                    'Buttom Grouped
                    Dim StatesXCountiesDT As New DataTable
                    Dim UniqueStatesXCountiesDT As New DataTable
                    Dim UniqueStatesDT As New DataTable

                    StatesXCountiesDT = PrintDT.DefaultView.ToTable(False, "ProjectID", "ProjectState", "ProjectCounty")
                    UniqueStatesXCountiesDT = StatesXCountiesDT.DefaultView.ToTable(True, "ProjectState", "ProjectCounty")
                    UniqueStatesDT = PrintDT.DefaultView.ToTable(True, "ProjectState")

                    Dim CountiesDT As DataTable
                    Dim GroupedProjectsDT As DataTable

                    ButtomOUT &= "<div id=""search-map"">"
                    ButtomOUT &= "<div class=""completeList fluid-padding"">"
                    If ProjectFiltersSQL.SizeOfSpace <> "" Or ProjectFiltersSQL.PropertyState <> "" Or ProjectFiltersSQL.PropertyCounty <> "" Or ProjectFiltersSQL.PropertyAnchor <> "" Then
                        ButtomOUT &= "<h2><strong>List of Matching Properties</strong></h2>"
                    Else
                        ButtomOUT &= "<h2><strong>Complete List of Properties</strong></h2>"
                    End If
                    ButtomOUT &= "<div id=""propListComplete"">"
                    ButtomOUT &= "<p class=""c""></p>"
                    For R As Integer = 0 To UniqueStatesDT.Rows.Count - 1
                        ButtomOUT &= "<div>"
                        ButtomOUT &= "<h3>" & UniqueStatesDT.Rows(R).Item("ProjectState") & "</h3>"
                        CountiesDT = New DataTable
                        UniqueStatesXCountiesDT.DefaultView.RowFilter = "ProjectState = '" & UniqueStatesDT.Rows(R).Item("ProjectState") & "'"
                        UniqueStatesXCountiesDT.DefaultView.Sort = "ProjectCounty ASC"
                        CountiesDT = UniqueStatesXCountiesDT.DefaultView.ToTable


                        For R1 As Integer = 0 To CountiesDT.Rows.Count - 1
                            ButtomOUT &= "<strong>" & CountiesDT.Rows(R1).Item("ProjectCounty") & "</strong>"
                            GroupedProjectsDT = New DataTable
                            PrintDT.DefaultView.RowFilter = "ProjectCounty = '" & Replace(CountiesDT.Rows(R1).Item("ProjectCounty"), "'", "''") & "' AND ProjectState = '" & UniqueStatesDT.Rows(R).Item("ProjectState") & "'"
                            PrintDT.DefaultView.Sort = "ProjectName ASC"

                            GroupedProjectsDT = PrintDT.DefaultView.ToTable
                            For R2 As Integer = 0 To GroupedProjectsDT.Rows.Count - 1
                                If GroupedProjectsDT.Rows(R2).Item("SeoURL").ToString.Trim <> "" Then
                                    ButtomOUT &= "<a href=""#" & CMSFunctions.FormatFriendlyUrl_NoLower(GroupedProjectsDT.Rows(R2).Item("SeoURL").ToString.Trim, "-") & """>"
                                Else
                                    ButtomOUT &= "<a href=""#property-" & GroupedProjectsDT.Rows(R2).Item("ProjectID").ToString.Trim & "-" & CMSFunctions.FormatFriendlyUrl(GroupedProjectsDT.Rows(R2).Item("ProjectName").ToString.Trim, "-") & """ >"
                                End If
                                ButtomOUT &= formatfunctions.AutoFormatText(GroupedProjectsDT.Rows(R2).Item("ProjectName").ToString.Trim)
                                ButtomOUT &= "</a>"
                            Next
                        Next
                        ButtomOUT &= "</div>"
                    Next
                    ButtomOUT &= "<p class=""c""></p></div><p class=""c""></p>"
                    ButtomOUT &= "</div><p class=""c""></p>"
                    ButtomOUT &= "</div>"
                    PrintOut = MapPrintOUT & ListPrintOut & ButtomOUT
                Case "Properties Search"
                    For R As Integer = 0 To PrintDT.Rows.Count - 1
                        If PrintDT.Rows(R).Item("SeoURL").ToString.Trim <> "" Then
                            PrintOUT &= "<a href=""#" & CMSFunctions.FormatFriendlyUrl_NoLower(PrintDT.Rows(R).Item("SeoURL").ToString.Trim, "-") & """>"
                        Else
                            PrintOUT &= "<a href=""#property-" & PrintDT.Rows(R).Item("ProjectID").ToString.Trim & "-" & CMSFunctions.FormatFriendlyUrl(PrintDT.Rows(R).Item("ProjectName").ToString.Trim, "-") & """>"
                        End If
                        PrintOUT &= formatfunctions.AutoFormatText(PrintDT.Rows(R).Item("ProjectName").ToString.Trim)
                        PrintOUT &= "</a>"
                    Next
                Case "Case Studies Home"
                    PrintOUT &= "<ul class=""slider-2"">"
                    For R As Integer = 0 To PrintDT.Rows.Count - 1
                        PrintOUT &= "<li class=""rollover-item"">"
                        PrintOUT &= "<a href=""#case-studies!" & PrintDT.Rows(R).Item("ProjectID") & """>"
                        PrintOUT &= "<img src=""/dynamic/image/always/project/best/220x220/85/ffffff/North/" & PrintDT.Rows(R)("ProjectID").ToString.Trim & ".jpg"" alt=""""/>"
                        PrintOUT &= "<span><strong>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R)("ProjectName").ToString.Trim) & "</strong>"
                        PrintOUT &= "<em>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R).Item("IDAM_HIGHTLIGHT").ToString.Trim) & "</em>"
                        PrintOUT &= "</span></a>"
                        PrintOUT &= "</li>"
                    Next
                    PrintOUT &= "</ul>"
                Case "Case Studies"
                    PrintOUT &= "<ul class=""case-study-list info-pops"">"
                    For R As Integer = 0 To PrintDT.Rows.Count - 1
                        Dim LeasingAgentsDT As New DataTable
                        LeasingAgentsDT = IDAMfunctions.GetProjectContacts(PrintDT.Rows(R)("ProjectID").ToString.Trim, 0)

                        PrintOUT &= VbNewLine & "<li class=""rollover-item"">"
                        PrintOUT &= "<div class=""hover-parent"">"
                        PrintOUT &= "<img src=""/dynamic/image/always/project/best/220x220/85/ffffff/North/" & PrintDT.Rows(R)("ProjectID").ToString.Trim & ".jpg"" alt=""""/>"
                        PrintOUT &= "<a class=""purple-ghost-box show-info-pop rollover id" & PrintDT.Rows(R)("ProjectID").ToString.Trim & """>"
                        PrintOUT &= "<strong>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R)("ProjectName").ToString.Trim) & "</strong>"
                        PrintOUT &= "<span>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R)("IDAM_HIGHTLIGHT").ToString.Trim) & "</span>"
                        PrintOUT &= "</a>"
                        PrintOUT &= "</div>"
                        PrintOUT &= "<div class=""info-pop"">"
                        PrintOUT &= "<div class=""info-pop-content"">"
                        PrintOUT &= "<div class=""fluid-padding"">"
                        PrintOUT &= "<div class=""caseCol1"">"

                        Dim SliderSQL As String = ""
                        SliderSQL &= "SELECT Asset_ID FROM IPM_ASSET WHERE ProjectID = " & PrintDT.Rows(R)("ProjectID").ToString.Trim & " "
                        SliderSQL &= "AND Category_ID =  (SELECT Category_ID FROM ASSET_CATEGORY WHERE Name = 'Images' AND ProjectID = " & PrintDT.Rows(R)("ProjectID").ToString.Trim & " Group By Category_ID ) "
                        SliderSQL &= "AND Active = 1 AND Available = 'Y' "

                        Dim SliderDT As New DataTable("SliderDT")
                        SliderDT = DBFunctions.GetDataTable(SliderSQL)
                        If SliderDT.Rows.Count > 0 Then
                            PrintOUT &= "<div class=""info-pop-slider"">"
                            PrintOUT &= "<ul>"
                            For S As Integer = 0 To SliderDT.Rows.Count - 1
                                PrintOUT &= "<li>"
                                PrintOUT &= "<img src=""/dynamic/image/always/asset/best/457x224/85/333333/Center/" & SliderDT.Rows(S)("Asset_ID").ToString.Trim & ".jpg"" alt=""""/>"
                                PrintOUT &= "</li>"
                            Next
                            PrintOUT &= "</ul>"
                            PrintOUT &= "</div>"
                        End If


                        Dim BrochureSQL As String = ""
                        BrochureSQL &= "SELECT max(Asset_ID) as Asset_ID "
                        BrochureSQL &= "FROM IPM_ASSET A "
                        BrochureSQL &= "WHERE A.ProjectID = " & PrintDT.Rows(R)("ProjectID").ToString.Trim & ""
                        BrochureSQL &= "AND A.Category_ID = (select Category_ID From IPM_ASSET_CATEGORY where Name = 'Brochure' AND ProjectID = A.ProjectID AND Active = 1 AND Available = 'Y') "
                        BrochureSQL &= "AND A.Active = 1 AND A.Available = 'Y' "
                        Dim BrochureDTT As New DataTable
                        BrochureDTT = DBFunctions.GetDataTable(BrochureSQL)
                        Dim BrochureId As String
                        If BrochureDTT.Rows.Count > 0 Then
                            BrochureId = BrochureDTT.Rows(0).Item("Asset_ID").ToString.Trim
                        End If
                        If BrochureId <> "" Then
                            PrintOUT &= "<a href=""/dynamic/document/always/asset/download/" & BrochureId & "/" & CMSFunctions.FormatFriendlyUrl(PrintDT.Rows(R)("ProjectName").ToString.Trim, "_") & "_brochure.pdf"" class=""fancy-download"">Download Case Study</a>"
                        End If
                        PrintOUT &= "</div>"

                        PrintOUT &= "<div class=""text last"">"

                        PrintOUT &= "<h3>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R)("ProjectName").ToString.Trim) & "</h3>"

                        If PrintDT.Rows(R).Item("IDAM_PROP_SERVICE").ToString.Trim <> "" Then
                            Dim ServiceLines As String() = Right(PrintDT.Rows(R).Item("IDAM_PROP_SERVICE").ToString.Trim, Len(PrintDT.Rows(R).Item("IDAM_PROP_SERVICE").ToString.Trim) - 1).Split(",")
                            If ServiceLines.Count > 0 Then
                                PrintOUT &= "<h6>Service Line</h6>"
                                PrintOUT &= "<div>"
                                For s As Integer = 0 To ServiceLines.Count - 1
                                    If s + 1 = ServiceLines.Count Then
                                        PrintOUT &= ServiceLines(s)
                                    Else
                                        PrintOUT &= ServiceLines(s) & "<br />"
                                    End If
                                Next
                                PrintOUT &= "</div>"
                            End If
                        End If

                        If PrintDT.Rows(R)("IDAM_MARKET").ToString.Trim <> "" Then
                            PrintOUT &= "<h6>Market</h6>"
                            PrintOUT &= "<span>"
                            PrintOUT &= FormatFunctions.AutoFormatText(PrintDT.Rows(R)("IDAM_MARKET").ToString.Trim)
                            PrintOUT &= "</span>"
                        End If

                        If PrintDT.Rows(R)("IDAM_TIMING").ToString.Trim <> "" Then
                            PrintOUT &= "<h6>Timing</h6>"
                            PrintOUT &= "<span>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R)("IDAM_TIMING").ToString.Trim) & "</span>"
                        End If

                        If PrintDT.Rows(R)("IDAM_MISSION").ToString.Trim <> "" Then
                            PrintOUT &= "<h6>Results</h6>"
                            PrintOUT &= "<div>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R)("IDAM_MISSION").ToString.Trim) & "</div>"
                        End If

                        Dim RcSQL As String = ""
                        RcSQL &= "select P.ProjectID, P.Name from IPM_PROJECT_RELATED RP JOIN IPM_PROJECT P On P.ProjectID = RP.Ref_Id  "
                        RcSQL &= "where RP.Project_Id = " & PrintDT.Rows(R)("ProjectID").ToString.Trim & ""
                        Dim RcDT As New DataTable("RcDT")
                        RcDT = DBFunctions.GetDataTable(RcSQL)
                        If RcDT.Rows.Count > 0 Then
                            PrintOUT &= "<h6>related projects</h6>"
                            For rc As Integer = 0 To RcDT.Rows.Count - 1
                                PrintOUT &= "<a href=""#properties!" & RcDT.Rows(rc).Item("ProjectID") & """>" & RcDT.Rows(rc).Item("Name") & "</a><br />"
                            Next
                        End If


                        If LeasingAgentsDT.Rows.Count > 0 Then
                            If LeasingAgentsDT.Rows.Count > 1 Then
                                PrintOUT &= "<h6>AGENTS</h6>"
                            Else
                                PrintOUT &= "<h6>AGENT</h6>"
                            End If
                            PrintOUT &= "<span class=""agents"">"
                            For U As Integer = 0 To LeasingAgentsDT.Rows.Count - 1
                                PrintOUT &= "<a href=""#team!" & LeasingAgentsDT.Rows(U).Item("UserID").ToString.Trim & """>" & LeasingAgentsDT.Rows(U).Item("FirstName") & " " & LeasingAgentsDT.Rows(U).Item("LastName") & "</a>"
                            Next
                            PrintOUT &= "</span>"
                        End If

                        'PrintOUT &= "<h6>AGENTS</h6><span class=""agents""><a href=""team#2410626"">Bradley A. Buslik</a><a href=""team#2410620"">Geoffrey L. Mackler</a></span>"


                        PrintOUT &= "</div>"
                        PrintOUT &= "<br class=""clear""/>"
                        PrintOUT &= "<button type=""button"" class=""close-pop""></button>"
                        PrintOUT &= "</div>"
                        PrintOUT &= "</div>"
                        PrintOUT &= "</div>"
                        PrintOUT &= "</li>"
                    Next
                    PrintOUT &= "</ul>"
                Case "Retail Solutions"
                    For R As Integer = 0 To PrintDT.Rows.Count - 1
                        PrintOUT &= "<li class=""rollover-item"">"
                        PrintOUT &= "<div class=""rollover-parent"">"
                        PrintOUT &= "<a class=""purple-ghost-box rollover"" href=""#case-studies!" & PrintDT.Rows(R).Item("ProjectID") & """>"
                        PrintOUT &= "<img src=""/dynamic/image/always/project/best/220x220/85/ffffff/North/" & PrintDT.Rows(R)("ProjectID").ToString.Trim & ".jpg"" alt=""""/>"
                        PrintOUT &= "<span>"
                        PrintOUT &= "<strong>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R)("ProjectName").ToString.Trim) & "</strong>"
                        PrintOUT &= "<em>Case study</em>"
                        PrintOUT &= "</span>"
                        PrintOUT &= "</a>"
                        PrintOUT &= "</div>"
                        PrintOUT &= "</li>	"
                    Next
            End Select
            '---TEMPLATE END---'
        Else

            Select Case TemplatePage
                Case "Properties"
                    Dim ProjectFilters1 As New ProjectFiltersStructure
                    PrintOUT = IDAMFunctions.PrintProjects1("2409776", 0, 1, "Properties", ProjectFilters1)
            End Select
        End If
        Return PrintOUT
    End Function

    Public Shared Function GetProjects1(ByVal CategoryGroups As String, ByVal Limit As Integer, ByVal TemplatePage As String, ByVal ProjectFilters As ProjectFiltersStructure) As DataTable
        Dim ProjectFiltersSQL As New ProjectFiltersStructure
        ProjectFiltersSQL = ProjectFilters
        Dim QuerySQL As String = ""
        If Limit <> 0 Then
            QuerySQL &= "SELECT TOP " & Limit & " "
        Else
            QuerySQL &= "SELECT "
        End If
        QuerySQL &= " * FROM Projects_Cache P "
        QuerySQL &= "WHERE 1 = 1 "
        If TemplatePage <> "Case Studies Home" And TemplatePage <> "Case Studies" And TemplatePage <> "Retail Solutions" Then
            QuerySQL &= "AND P.State_id is not null and P.State_id <> '' "
            QuerySQL &= "AND P.ProjectCounty is not null and P.ProjectCounty <> '' "
        End If
        'Projects Collection
        If ProjectFiltersSQL.ProjectCollection <> "" Then
            Dim ListOfGroups As String()
            ListOfGroups = ProjectFiltersSQL.ProjectCollection.Split(VbNewLine)
            Dim FormattedListOfGroups As String = ""
            For Each Group As String In ListOfGroups
                If IsNumeric(Group) Then
                    FormattedListOfGroups &= Group & ","
                End If
            Next
            If FormattedListOfGroups <> "" Then
                QuerySQL &= "AND P.ProjectId IN (" & Left(FormattedListOfGroups, Len(FormattedListOfGroups) - 1) & ")"
            End If
        End If
        'Property Space
        If ProjectFiltersSQL.SizeOfSpace <> "" Then
            Select Case ProjectFiltersSQL.SizeOfSpace
                Case "x05000SF"
                    QuerySQL &= "AND P.Size0_5000 = 1 "
                Case "x500115000SF"
                    QuerySQL &= "AND P.Size5001_15000 = 1 "
                Case "x15001SFp"
                    QuerySQL &= "AND P.Size15000p = 1 "
                Case "PadSites"
                    QuerySQL &= "AND P.Pad = 1  "
            End Select
        End If
        'Property State
        If ProjectFiltersSQL.PropertyState <> "" Then
            QuerySQL &= "AND P.ProjectState =  '" & Replace(HttpUtility.UrlDecode(ProjectFiltersSQL.PropertyState), "'", "''") & "'  "
        End If
        'Property County
        If ProjectFiltersSQL.PropertyCounty <> "" Then
            Dim CountyS As String() = HttpUtility.UrlDecode(ProjectFiltersSQL.PropertyCounty).Split("/")
            QuerySQL &= "AND P.ProjectCounty =  '" & Replace(CountyS(0).Trim, "'", "''") & "'  "
            QuerySQL &= "AND P.ProjectState =  '" & Replace(CountyS(1).Trim, "'", "''") & "'  "
        End If
        'Property Anchors
        If ProjectFiltersSQL.PropertyAnchor <> "" Then
            QuerySQL &= "AND P.ProjectID	IN ( "
            QuerySQL &= "SELECT "
            QuerySQL &= "P.ProjectID "
            QuerySQL &= "FROM IPM_SPACE S "
            QuerySQL &= "JOIN IPM_SPACE_TENANT ST (nolock) ON S.Space_ID = ST.Space_ID "
            QuerySQL &= "JOIN IPM_TENANT T (nolock) ON ST.Tenant_ID = T.Tenant_ID "
            QuerySQL &= "JOIN IPM_PROJECT P (nolock) ON P.ProjectID = S.ProjectID  "
            QuerySQL &= "WHERE 1 = 1 "
            QuerySQL &= "AND T.Active = 1 "
            QuerySQL &= "AND T.Available = 'Y' "
            QuerySQL &= "AND P.Available = 'Y' "
            QuerySQL &= "AND P.Show = 1 "
            QuerySQL &= "AND T.ShortName = '" & Replace(HttpUtility.UrlDecode(ProjectFiltersSQL.PropertyAnchor), "'", "''") & "'  "
            QuerySQL &= ")"
        End If
        'Search Phrase
        If ProjectFiltersSQL.SearchPhrase <> "" Then
            QuerySQL &= "AND ( P.ProjectName like '%" & ProjectFiltersSQL.SearchPhrase & "%' or P.ProjectAddress like '%" & ProjectFiltersSQL.SearchPhrase & "%'  or P.ProjectState like '%" & ProjectFiltersSQL.SearchPhrase & "%' or P.Zip like '%" & ProjectFiltersSQL.SearchPhrase & "%' or P.Search_Tags like '%" & ProjectFiltersSQL.SearchPhrase & "%' "
            QuerySQL &= "OR P.ProjectID	IN ( "
            QuerySQL &= "SELECT "
            QuerySQL &= "P.ProjectID "
            QuerySQL &= "FROM IPM_SPACE S (nolock) "
            QuerySQL &= "JOIN IPM_SPACE_TENANT ST (nolock) ON S.Space_ID = ST.Space_ID "
            QuerySQL &= "JOIN IPM_TENANT T (nolock) ON ST.Tenant_ID = T.Tenant_ID "
            QuerySQL &= "JOIN IPM_PROJECT P (nolock) ON P.ProjectID = S.ProjectID  "
            QuerySQL &= "WHERE 1 = 1 "
            QuerySQL &= "AND T.Active = 1 "
            QuerySQL &= "AND T.Available = 'Y' "
            QuerySQL &= "AND P.Available = 'Y' "
            QuerySQL &= "AND P.Show = 1 "
            QuerySQL &= "AND T.ShortName like '%" & Replace(ProjectFiltersSQL.SearchPhrase, "'", "''") & "%'  "
            QuerySQL &= "))"
        End If
        'Project Categories
        If CategoryGroups <> "" Then
            If CategoryGroups.Contains(",") Then
                Dim ListOfGroups As String()
                ListOfGroups = CategoryGroups.Split(",")
                Dim FormattedListOfGroups As String = ""
                For Each Group As String In ListOfGroups
                    FormattedListOfGroups &= "'" & Group & "',"
                Next
                QuerySQL &= "AND P.Category_ID IN (" & Left(FormattedListOfGroups, Len(FormattedListOfGroups) - 1) & ")"
            Else
                QuerySQL &= "AND P.Category_ID IN (" & CategoryGroups & ")"
            End If
        End If
        QuerySQL &= " ORDER BY P.ProjectState ASC, P.ProjectCounty ASC,  P.ProjectName ASC   "

        Dim QueryDT As New DataTable("QueryDT")
        QueryDT = DBFunctions.GetDataTable(QuerySQL)
        Return QueryDT
    End Function
    Public Shared Function PrintProjects1(ByVal CategoryGroups As String, ByVal Limit As Integer, ByVal ImageFromAssets As Integer, ByVal TemplatePage As String, ByVal ProjectFilters As ProjectFiltersStructure) As String
        Dim ProjectFiltersSQL As New ProjectFiltersStructure
        ProjectFiltersSQL = ProjectFilters
        Dim PrintOUT As String = ""
        Dim PrintDT As New DataTable("PrintDT")
        PrintDT = IDAMFunctions.GetProjects(CategoryGroups, Limit, TemplatePage, ProjectFilters)
        If PrintDT.Rows.Count > 0 Then
            '---TEMPLATE START---'
            Select Case TemplatePage
                Case "Properties"
                    Dim ButtomOUT As String = ""
                    'Buttom Grouped
                    Dim StatesXCountiesDT As New DataTable
                    Dim UniqueStatesXCountiesDT As New DataTable
                    Dim UniqueStatesDT As New DataTable

                    StatesXCountiesDT = PrintDT.DefaultView.ToTable(False, "ProjectID", "ProjectState", "ProjectCounty")
                    UniqueStatesXCountiesDT = StatesXCountiesDT.DefaultView.ToTable(True, "ProjectState", "ProjectCounty")
                    UniqueStatesDT = PrintDT.DefaultView.ToTable(True, "ProjectState")

                    Dim CountiesDT As DataTable
                    Dim GroupedProjectsDT As DataTable

                    If ProjectFiltersSQL.SizeOfSpace <> "" Or ProjectFiltersSQL.PropertyState <> "" Or ProjectFiltersSQL.PropertyCounty <> "" Or ProjectFiltersSQL.PropertyAnchor <> "" Then
                        ButtomOUT &= "<h2><strong>List of Matching Properties</strong></h2>"
                    Else
                        ButtomOUT &= "<h2><strong>Complete List of Properties</strong></h2>"
                    End If
                    ButtomOUT &= "<div id=""propListComplete"">"
                    ButtomOUT &= "<p class=""c""></p>"
                    For R As Integer = 0 To UniqueStatesDT.Rows.Count - 1
                        ButtomOUT &= "<div>"
                        ButtomOUT &= "<h3>" & UniqueStatesDT.Rows(R).Item("ProjectState") & "</h3>"
                        CountiesDT = New DataTable
                        UniqueStatesXCountiesDT.DefaultView.RowFilter = "ProjectState = '" & UniqueStatesDT.Rows(R).Item("ProjectState") & "'"
                        CountiesDT = UniqueStatesXCountiesDT.DefaultView.ToTable
                        For R1 As Integer = 0 To CountiesDT.Rows.Count - 1
                            ButtomOUT &= "<strong>" & CountiesDT.Rows(R1).Item("ProjectCounty") & "</strong>"
                            GroupedProjectsDT = New DataTable
                            PrintDT.DefaultView.RowFilter = "ProjectCounty = '" & Replace(CountiesDT.Rows(R1).Item("ProjectCounty"), "'", "''") & "' AND ProjectState = '" & UniqueStatesDT.Rows(R).Item("ProjectState") & "'"
                            GroupedProjectsDT = PrintDT.DefaultView.ToTable
                            For R2 As Integer = 0 To GroupedProjectsDT.Rows.Count - 1
                                If GroupedProjectsDT.Rows(R2).Item("SeoURL").ToString.Trim <> "" Then
                                    ButtomOUT &= "<a href=""#" & CMSFunctions.FormatFriendlyUrl_NoLower(GroupedProjectsDT.Rows(R2).Item("SeoURL").ToString.Trim, "-") & """>"
                                Else
                                    ButtomOUT &= "<a href=""#property-" & GroupedProjectsDT.Rows(R2).Item("ProjectID").ToString.Trim & "-" & CMSFunctions.FormatFriendlyUrl(GroupedProjectsDT.Rows(R2).Item("ProjectName").ToString.Trim, "-") & """ >"
                                End If
                                ButtomOUT &= formatfunctions.AutoFormatText(GroupedProjectsDT.Rows(R2).Item("ProjectName").ToString.Trim)
                                ButtomOUT &= "</a>"
                            Next
                        Next
                        ButtomOUT &= "</div>"
                    Next
                    ButtomOUT &= "</div>"
                    PrintOut = ButtomOUT
            End Select
            '---TEMPLATE END---'
        End If
        Return PrintOUT
    End Function

    Public Shared Function GetProjectDetails(ByVal ProjectId As Integer) As ProjectStructure
        Dim QuerySQL As String = ""
        QuerySQL &= "SELECT "
        QuerySQL &= "P.*, "
        QuerySQL &= "PP.*, "
        QuerySQL &= "F1.Item_Value AS IDAM_PROPERTYADDRESS, "
        QuerySQL &= "F2.Item_Value AS IDAM_COUNTY, "
        QuerySQL &= "F3.Item_Value AS IDAM_PROPDECRIPTION, "
        QuerySQL &= "F4.Item_Value AS IDAM_PROPCONTACT, "
        QuerySQL &= "F5.Item_Value AS IDAM_CLIENT, "
        QuerySQL &= "F6.Item_Value AS IDAM_STATUS, "
        QuerySQL &= "F7.Item_Value AS IDAM_PAGETITLE, "
        QuerySQL &= "F8.Item_Value AS SeoURL, "
        QuerySQL &= "F9.Item_Value AS IDAM_LATLONG, "
        QuerySQL &= "F10.Item_Value AS IDAM_PROP_SERVICE, "
        QuerySQL &= "F11.Item_Value AS IDAM_URL, "
        QuerySQL &= "F12.Item_Value AS IDAM_PROJVID, "
        QuerySQL &= "F13.Item_Value AS IDAM_PAGEDESC, "
        QuerySQL &= "F14.Item_Value AS IDAM_PAGEMETADATAKEYWORDS, "
        QuerySQL &= "F15.Item_Value AS IDAM_MARKET, "
        QuerySQL &= "F16.Item_Value AS IDAM_TIMING, "
        QuerySQL &= "F17.Item_Value AS IDAM_MISSION, "
        QuerySQL &= "F18.Item_Value AS IDAM_LOCATIONS, "
        QuerySQL &= "MarketArial.Asset_ID AS AerialId, "
        QuerySQL &= "Brochure.Asset_ID AS BrochureId, "
        QuerySQL &= "Demographic.Asset_ID AS DemographicId, "
        'QuerySQL &= "SiteMap.Asset_ID AS SiteMapId, "
        QuerySQL &= "SiteMapPDF.Asset_ID AS SiteMapPDF "
        'QuerySQL &= "SiteMap.Description as Coordinates "
        QuerySQL &= "FROM IPM_PROJECT P "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_PROPERTYINFO PP ON P.ProjectID = PP.Projectid "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F1 on P.ProjectID = F1.ProjectID and F1.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROPERTYADDRESS' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F2 on P.ProjectID = F2.ProjectID and F2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_COUNTY' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F3 on P.ProjectID = F3.ProjectID and F3.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROPDECRIPTION' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F4 on P.ProjectID = F4.ProjectID and F4.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROPCONTACT' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F5 on P.ProjectID = F5.ProjectID and F5.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_CLIENT' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F6 on P.ProjectID = F6.ProjectID and F6.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_STATUS' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F7 on P.ProjectID = F7.ProjectID and F7.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PAGETITLE' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F8 on P.ProjectID = F8.ProjectID and F8.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'SeoURL' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F9 on P.ProjectID = F9.ProjectID and F9.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_LATLONG' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F10 on P.ProjectID = F10.ProjectID and F10.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROP_SERVICE' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F11 on P.ProjectID = F11.ProjectID and F11.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_URL' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F12 on P.ProjectID = F12.ProjectID and F12.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PROJVID' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F13 on P.ProjectID = F13.ProjectID and F13.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PAGEDESC' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F14 on P.ProjectID = F14.ProjectID and  F14.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PAGEMETADATAKEYWORDS' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F15 on P.ProjectID = F15.ProjectID and  F15.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_MARKET' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F16 on P.ProjectID = F16.ProjectID and  F16.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_TIMING' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F17 on P.ProjectID = F17.ProjectID and  F17.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_MISSION' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F18 on P.ProjectID = F18.ProjectID and  F18.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_LOCATIONS' AND Active = 1)  "
        QuerySQL &= "LEFT JOIN (select ProjectID, Category_ID, max(Asset_Id) Asset_Id from IPM_ASSET where PDF = 0 AND Active = 1 AND Available = 'Y' group by ProjectID, Category_ID) MarketArial ON MarketArial.ProjectID = P.ProjectID AND MarketArial.Category_ID = (select Category_ID From IPM_ASSET_CATEGORY where Name = 'Market Aerial' AND ProjectID = P.ProjectID AND Active = 1 AND Available = 'Y')  "
        QuerySQL &= "LEFT JOIN (select ProjectID, Category_ID, max(Asset_Id) Asset_Id from IPM_ASSET where Active = 1 AND Available = 'Y' group by ProjectID, Category_ID) Brochure ON Brochure.ProjectID = P.ProjectID AND Brochure.Category_ID = (select Category_ID From IPM_ASSET_CATEGORY where Name = 'Brochure' AND ProjectID = P.ProjectID AND Active = 1 AND Available = 'Y') "
        QuerySQL &= "LEFT JOIN (select ProjectID, Category_ID, max(Asset_Id) Asset_Id from IPM_ASSET where Active = 1 AND Available = 'Y' group by ProjectID, Category_ID) Demographic ON Demographic.ProjectID = P.ProjectID AND Demographic.Category_ID = (select Category_ID From IPM_ASSET_CATEGORY where Name = 'Demographics' AND ProjectID = P.ProjectID AND Active = 1 AND Available = 'Y')  "
        'QuerySQL &= "LEFT JOIN (select ProjectID, Category_ID, max(Asset_Id) Asset_Id, Description  from IPM_ASSET where PDF = 0 AND Active = 1 AND Available = 'Y' group by ProjectID, Category_ID, Description) SiteMap ON SiteMap.ProjectID = P.ProjectID AND SiteMap.Category_ID = (select Category_ID From IPM_ASSET_CATEGORY where Name = 'Site Plan' AND ProjectID = P.ProjectID AND Active = 1 AND Available = 'Y') "
        QuerySQL &= "LEFT JOIN (select ProjectID, Category_ID, max(Asset_Id) Asset_Id, Description  from IPM_ASSET where PDF = 1 AND Active = 1 AND Available = 'Y' group by ProjectID, Category_ID, Description) SiteMapPDF ON SiteMapPDF.ProjectID = P.ProjectID AND SiteMapPDF.Category_ID = (select Category_ID From IPM_ASSET_CATEGORY where Name = 'Site Plan' AND ProjectID = P.ProjectID AND Active = 1 AND Available = 'Y') "
        QuerySQL &= "WHERE 1 = 1 "

        QuerySQL &= "AND P.ProjectID = " & ProjectId & ""
        Dim QueryDT As New DataTable("QueryDT")
        QueryDT = DBFunctions.GetDataTable(QuerySQL)
        If QueryDT.Rows.Count > 0 Then
            Dim ProjectData As New ProjectStructure
            ProjectData.ProjectTitle = QueryDT.Rows(0).Item("Name").ToString.Trim
            ProjectData.ProjectAddress = QueryDT.Rows(0).Item("Address").ToString.Trim & ", " & QueryDT.Rows(0).Item("City").ToString.Trim & ", " & QueryDT.Rows(0).Item("State_Id").ToString.Trim

            ProjectData.ProjectState = QueryDT.Rows(0).Item("State_Id").ToString.Trim
            ProjectData.ProjectCounty = QueryDT.Rows(0).Item("IDAM_COUNTY").ToString.Trim
            ProjectData.ProjectCity = QueryDT.Rows(0).Item("City").ToString.Trim

            ProjectData.ProjectDescription = QueryDT.Rows(0).Item("IDAM_PROPDECRIPTION").ToString.Trim
            ProjectData.ProjectWebsite = QueryDT.Rows(0).Item("IDAM_URL").ToString.Trim

            If QueryDT.Rows(0).Item("colH1").ToString.Trim <> "" Then
                ProjectData.ColH1 = IIf(CMSFunctions.StringOutNumbers1(QueryDT.Rows(0).Item("colH1").ToString) > 1, QueryDT.Rows(0).Item("colH1").ToString.Trim & " Miles", QueryDT.Rows(0).Item("colH1").ToString.Trim & " Mile")
            End If
            If QueryDT.Rows(0).Item("colH2").ToString.Trim <> "" Then
                ProjectData.ColH2 = IIf(CMSFunctions.StringOutNumbers1(QueryDT.Rows(0).Item("colH2").ToString) > 1, QueryDT.Rows(0).Item("colH2").ToString.Trim & " Miles", QueryDT.Rows(0).Item("colH2").ToString.Trim & " Mile")
            End If
            If QueryDT.Rows(0).Item("colH3").ToString.Trim <> "" Then
                ProjectData.ColH3 = IIf(CMSFunctions.StringOutNumbers1(QueryDT.Rows(0).Item("colH3").ToString) > 1, QueryDT.Rows(0).Item("colH3").ToString.Trim & " Miles", QueryDT.Rows(0).Item("colH3").ToString.Trim & " Mile")
            End If

            ProjectData.RowH1 = QueryDT.Rows(0).Item("rowH1").ToString.Trim
            ProjectData.RowH2 = QueryDT.Rows(0).Item("rowH2").ToString.Trim
            ProjectData.RowH3 = QueryDT.Rows(0).Item("rowH3").ToString.Trim
            ProjectData.RowH4 = QueryDT.Rows(0).Item("rowH4").ToString.Trim

            If QueryDT.Rows(0).Item("p3mile").ToString <> "" And IsNumeric(QueryDT.Rows(0).Item("p3mile").ToString) Then
                Dim DecValue As Decimal = QueryDT.Rows(0).Item("p3mile").ToString.Trim
                ProjectData.Row1Col1 = IIf(QueryDT.Rows(0).Item("p3mile").ToString.Contains("$"), DecValue.ToString("C0"), DecValue.ToString("#,0"))
            End If
            If QueryDT.Rows(0).Item("p5mile").ToString <> "" And IsNumeric(QueryDT.Rows(0).Item("p5mile").ToString) Then
                Dim DecValue As Decimal = QueryDT.Rows(0).Item("p5mile").ToString.Trim
                ProjectData.Row1Col2 = IIf(QueryDT.Rows(0).Item("p5mile").ToString.Contains("$"), DecValue.ToString("C0"), DecValue.ToString("#,0"))
            End If
            If QueryDT.Rows(0).Item("p10mile").ToString <> "" And IsNumeric(QueryDT.Rows(0).Item("p10mile").ToString) Then
                Dim DecValue As Decimal = QueryDT.Rows(0).Item("p10mile").ToString.Trim
                ProjectData.Row1Col3 = IIf(QueryDT.Rows(0).Item("p10mile").ToString.Contains("$"), DecValue.ToString("C0"), DecValue.ToString("#,0"))
            End If

            If QueryDT.Rows(0).Item("h3mile").ToString <> "" And IsNumeric(QueryDT.Rows(0).Item("h3mile").ToString) Then
                Dim DecValue As Decimal = QueryDT.Rows(0).Item("h3mile").ToString.Trim
                'DecValue = Math.Round(DecValue)
                ProjectData.Row2Col1 = IIf(QueryDT.Rows(0).Item("h3mile").ToString.Contains("$"), DecValue.ToString("C0"), DecValue.ToString("#,0"))
            End If
            If QueryDT.Rows(0).Item("h5mile").ToString <> "" And IsNumeric(QueryDT.Rows(0).Item("h5mile").ToString) Then
                Dim DecValue As Decimal = QueryDT.Rows(0).Item("h5mile").ToString.Trim
                ProjectData.Row2Col2 = IIf(QueryDT.Rows(0).Item("h5mile").ToString.Contains("$"), DecValue.ToString("C0"), DecValue.ToString("#,0"))
            End If
            If QueryDT.Rows(0).Item("h10mile").ToString <> "" And IsNumeric(QueryDT.Rows(0).Item("h10mile").ToString) Then
                Dim DecValue As Decimal = QueryDT.Rows(0).Item("h10mile").ToString.Trim
                ProjectData.Row2Col3 = IIf(QueryDT.Rows(0).Item("h10mile").ToString.Contains("$"), DecValue.ToString("C0"), DecValue.ToString("#,0"))
            End If

            If QueryDT.Rows(0).Item("ahh3mile").ToString <> "" And IsNumeric(QueryDT.Rows(0).Item("ahh3mile").ToString) Then
                Dim DecValue As Decimal = QueryDT.Rows(0).Item("ahh3mile").ToString.Trim
                ProjectData.Row3Col1 = IIf(QueryDT.Rows(0).Item("ahh3mile").ToString.Contains("$"), DecValue.ToString("C0"), DecValue.ToString("#,0"))
            End If
            If QueryDT.Rows(0).Item("ahh5mile").ToString <> "" And IsNumeric(QueryDT.Rows(0).Item("ahh5mile").ToString) Then
                Dim DecValue As Decimal = QueryDT.Rows(0).Item("ahh5mile").ToString.Trim
                ProjectData.Row3Col2 = IIf(QueryDT.Rows(0).Item("ahh5mile").ToString.Contains("$"), DecValue.ToString("C0"), DecValue.ToString("#,0"))
            End If
            If QueryDT.Rows(0).Item("ahh10mile").ToString <> "" And IsNumeric(QueryDT.Rows(0).Item("ahh10mile").ToString) Then
                Dim DecValue As Decimal = QueryDT.Rows(0).Item("ahh10mile").ToString.Trim
                ProjectData.Row3Col3 = IIf(QueryDT.Rows(0).Item("ahh10mile").ToString.Contains("$"), DecValue.ToString("C0"), DecValue.ToString("#,0"))
            End If

            If QueryDT.Rows(0).Item("mhh3mile").ToString <> "" And IsNumeric(QueryDT.Rows(0).Item("mhh3mile").ToString) Then
                Dim DecValue As Decimal = QueryDT.Rows(0).Item("mhh3mile").ToString.Trim
                ProjectData.Row4Col1 = IIf(QueryDT.Rows(0).Item("mhh3mile").ToString.Contains("$"), DecValue.ToString("C0"), DecValue.ToString("#,0"))
            End If
            If QueryDT.Rows(0).Item("mhh5mile").ToString <> "" And IsNumeric(QueryDT.Rows(0).Item("mhh5mile").ToString) Then
                Dim DecValue As Decimal = QueryDT.Rows(0).Item("mhh5mile").ToString.Trim
                ProjectData.Row4Col2 = IIf(QueryDT.Rows(0).Item("mhh5mile").ToString.Contains("$"), DecValue.ToString("C0"), DecValue.ToString("#,0"))
            End If
            If QueryDT.Rows(0).Item("mhh10mile").ToString <> "" And IsNumeric(QueryDT.Rows(0).Item("mhh10mile").ToString) Then
                Dim DecValue As Decimal = QueryDT.Rows(0).Item("mhh10mile").ToString.Trim
                ProjectData.Row4Col3 = IIf(QueryDT.Rows(0).Item("mhh10mile").ToString.Contains("$"), DecValue.ToString("C0"), DecValue.ToString("#,0"))
            End If

            ProjectData.PropertyService = New List(Of String)
            If QueryDT.Rows(0).Item("IDAM_PROP_SERVICE").ToString.Trim <> "" Then
                Dim PropType As String() = Right(QueryDT.Rows(0).Item("IDAM_PROP_SERVICE").ToString.Trim, Len(QueryDT.Rows(0).Item("IDAM_PROP_SERVICE").ToString.Trim) - 1).Split(",")
                For Each TypePp As String In PropType
                    ProjectData.PropertyService.add(TypePp)
                Next
            End If
            ProjectData.AerialId = QueryDT.Rows(0).Item("AerialId").ToString.Trim
            ProjectData.AreaMap = QueryDT.Rows(0).Item("IDAM_LATLONG").ToString.Trim
            ProjectData.BrochureId = QueryDT.Rows(0).Item("BrochureId").ToString.Trim
            ProjectData.DemographicId = QueryDT.Rows(0).Item("DemographicId").ToString.Trim
            'ProjectData.SiteMapId = QueryDT.Rows(0).Item("SiteMapId").ToString.Trim
            ProjectData.SiteMapPDF = QueryDT.Rows(0).Item("SiteMapPDF").ToString.Trim
            'ProjectData.SiteMapCoordinates = QueryDT.Rows(0).Item("Coordinates").ToString.Trim

            ProjectData.ProjectSEOTitle = QueryDT.Rows(0).Item("IDAM_PAGETITLE").ToString.Trim
            ProjectData.ProjectSEODescription = QueryDT.Rows(0).Item("IDAM_PAGEDESC").ToString.Trim
            ProjectData.ProjectSEOKeyWords = QueryDT.Rows(0).Item("IDAM_PAGEMETADATAKEYWORDS").ToString.Trim


            ProjectData.CaseStudy_Market = QueryDT.Rows(0).Item("IDAM_MARKET").ToString.Trim
            ProjectData.CaseStudy_Timing = QueryDT.Rows(0).Item("IDAM_TIMING").ToString.Trim
            ProjectData.CaseStudy_Mission = QueryDT.Rows(0).Item("IDAM_MISSION").ToString.Trim
            ProjectData.CaseStudy_Locations = QueryDT.Rows(0).Item("IDAM_LOCATIONS").ToString.Trim

            Return ProjectData
        End If
    End Function

    Public Shared Function GetProjectContacts(ByVal ProjectId As Integer, ByVal Limit As Integer) As DataTable
        Dim QuerySQL As String = ""
        If Limit <> 0 Then
            QuerySQL &= "SELECT TOP " & Limit & " "
        Else
            QuerySQL &= "SELECT "
        End If
        QuerySQL &= "PC.*, "
        QuerySQL &= "U.*, "
        QuerySQL &= "F1.Item_Value AS IDAM_EDUCATION, "
        QuerySQL &= "F1.Item_Value AS IDAM_USERSORT, "
        QuerySQL &= "F1.Item_Value AS IDAM_SPECIALTIES "
        QuerySQL &= "FROM IPM_PROJECT_CONTACT PC "
        QuerySQL &= "JOIN IPM_USER U ON PC.UserID = U.UserID "
        QuerySQL &= "LEFT JOIN IPM_USER_FIELD_VALUE F1 ON U.UserID = F1.USER_ID AND F1.Item_ID = (SELECT Item_ID FROM IPM_USER_FIELD_DESC WHERE Item_Tag = 'IDAM_EDUCATION' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_USER_FIELD_VALUE F2 ON U.UserID = F2.USER_ID AND F2.Item_ID = (SELECT Item_ID FROM IPM_USER_FIELD_DESC WHERE Item_Tag = 'IDAM_USERSORT' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_USER_FIELD_VALUE F3 ON U.UserID = F3.USER_ID AND F3.Item_ID = (SELECT Item_ID FROM IPM_USER_FIELD_DESC WHERE Item_Tag = 'IDAM_SPECIALTIES' AND Active = 1) "
        QuerySQL &= "WHERE 1 = 1 "
        QuerySQL &= "AND Active = 'Y' "
        QuerySQL &= "AND PC.ProjectID = " & ProjectId & ""
        Dim QueryDT As New DataTable("QueryDT")
        QueryDT = DBFunctions.GetDataTable(QuerySQL)
        Return QueryDT
    End Function
    Public Shared Function PrintProjectContacts(ByVal ProjectId As Integer, ByVal Limit As Integer) As String
        Dim PrintOUT As String = ""
        Dim PrintDT As New DataTable("PrintDT")
        PrintDT = IDAMFunctions.GetProjectContacts(ProjectId, Limit)
        If PrintDT.Rows.Count > 0 Then
            '---TEMPLATE START---'
            If PrintDT.Rows.Count > 1 Then
                PrintOUT &= "<h5>LEASING AGENTS</h5>"
            Else
                PrintOUT &= "<h5>LEASING AGENT</h5>"
            End If
            For R As Integer = 0 To PrintDT.Rows.Count - 1
                PrintOUT &= "<p class=""leasingAgent"">"
                PrintOUT &= "<span class=""red-text"">" & PrintDT.Rows(R).Item("FirstName").ToString.Trim & " " & PrintDT.Rows(R).Item("LastName").ToString.Trim & "</span>"
                PrintOUT &= "<span>Office: " & FormatFunctions.AutoFormatText(PrintDT.Rows(R).Item("Phone").ToString.Trim) & "</span>"
                PrintOUT &= "<span>Direct: " & FormatFunctions.AutoFormatText(PrintDT.Rows(R).Item("Cell").ToString.Trim) & "</span>"
                PrintOUT &= "<span>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R).Item("Email").ToString.Trim) & "</span>"
                PrintOUT &= "</p>"
            Next
            '---TEMPLATE END---'
        End If
        Return PrintOUT
    End Function

    Public Shared Function GetSpaces(ByVal ProjectId As Integer, ByVal Limit As Integer, ByVal FuturedTenants As Boolean, ByVal MajorTenants As Boolean, ByVal TemplatePage As String) As DataTable
        Dim QuerySQL As String = ""
        If Limit <> 0 Then
            QuerySQL &= "SELECT TOP " & Limit & " "
        Else
            QuerySQL &= "SELECT "
        End If
        Select Case TemplatePage
            Case "Property Tenants Logo"
                QuerySQL &= "S.Space_ID, "
                QuerySQL &= "T.Tenant_ID, "
                QuerySQL &= "TA.Asset_ID "
                QuerySQL &= "FROM IPM_SPACE S "
                QuerySQL &= "JOIN IPM_SPACE_TENANT ST ON S.Space_ID = ST.Space_ID "
                QuerySQL &= "JOIN IPM_TENANT T ON ST.Tenant_ID = T.Tenant_ID "
                QuerySQL &= "left JOIN "
                QuerySQL &= "(IPM_ASSET TA "
                QuerySQL &= "join IPM_ASSET_FIELD_VALUE F6 ON  TA.Asset_ID = F6.ASSET_ID and isnull(F6.Item_Value,'0')='1' AND F6.Item_ID = (Select Item_ID From IPM_ASSET_FIELD_DESC where Item_Tag = 'IDAM_LOGO' AND Active = 1) "
                QuerySQL &= "join (select A.Category_ID, min(A.Asset_ID) Asset_ID "
                QuerySQL &= "from IPM_ASSET A "
                QuerySQL &= "JOIN IPM_ASSET_FIELD_VALUE F ON A.Asset_ID = F.Asset_ID and isnull(F.Item_Value,'0')='1' "
                QuerySQL &= "AND F.Item_ID = (Select Item_ID From IPM_ASSET_FIELD_DESC where Item_Tag = 'IDAM_LOGO' AND Active = 1) "
                QuerySQL &= "where A.Available = 'Y' "
                QuerySQL &= "group by A.Category_ID ) X on TA.Category_ID = X.Category_ID and TA.Asset_ID = X.Asset_ID "
                QuerySQL &= ") ON TA.Category_ID = T.Tenant_ID AND TA.Available = 'Y' AND TA.Active = 1 "
                QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F1 ON T.Tenant_ID = F1.TENANT_ID AND F1.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_Retail_CATEGORY' AND Active = 1) "
                QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F2 ON T.Tenant_ID = F2.TENANT_ID AND F2.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_SITE_CRITERIA' AND Active = 1) "
                QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F3 ON T.Tenant_ID = F3.TENANT_ID AND F3.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_REPRESENTATIVE' AND Active = 1) "
                QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F4 ON T.Tenant_ID = F4.TENANT_ID AND F4.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_CASE_STUDIES' AND Active = 1) "
                QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F5 ON T.Tenant_ID = F5.TENANT_ID AND F5.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANTFEATURED' AND Active = 1) "
                QuerySQL &= "WHERE 1 = 1 "
                QuerySQL &= "AND T.Active = 1 "
                QuerySQL &= "AND T.Available = 'Y' "
                QuerySQL &= "AND S.ProjectID = " & ProjectId & ""
                QuerySQL &= "and TA.Asset_ID is not null  "
                If FuturedTenants = True Then
                    QuerySQL &= "AND F5.Item_Value = '1' "
                End If
                If MajorTenants = True Then
                    QuerySQL &= "AND T.Major = 1 "
                End If
                QuerySQL &= "GROUP BY T.Tenant_ID, TA.Asset_ID, S.Space_ID "
            Case "Property Tenants Logo Site Plan"
                QuerySQL &= "S.Space_ID, "
                QuerySQL &= "S.Description, "
                QuerySQL &= "T.Tenant_ID, "
                QuerySQL &= "TA.Asset_ID "
                QuerySQL &= "FROM IPM_SPACE S "
                QuerySQL &= "JOIN IPM_SPACE_TENANT ST ON S.Space_ID = ST.Space_ID "
                QuerySQL &= "JOIN IPM_TENANT T ON ST.Tenant_ID = T.Tenant_ID "
                QuerySQL &= "left JOIN "
                QuerySQL &= "(IPM_ASSET TA "
                QuerySQL &= "join IPM_ASSET_FIELD_VALUE F6 ON  TA.Asset_ID = F6.ASSET_ID and isnull(F6.Item_Value,'0')='1' AND F6.Item_ID = (Select Item_ID From IPM_ASSET_FIELD_DESC where Item_Tag = 'IDAM_LOGO' AND Active = 1) "
                QuerySQL &= "join (select A.Category_ID, min(A.Asset_ID) Asset_ID "
                QuerySQL &= "from IPM_ASSET A "
                QuerySQL &= "JOIN IPM_ASSET_FIELD_VALUE F ON A.Asset_ID = F.Asset_ID and isnull(F.Item_Value,'0')='1' "
                QuerySQL &= "AND F.Item_ID = (Select Item_ID From IPM_ASSET_FIELD_DESC where Item_Tag = 'IDAM_LOGO' AND Active = 1) "
                QuerySQL &= "where A.Available = 'Y' "
                QuerySQL &= "group by A.Category_ID ) X on TA.Category_ID = X.Category_ID and TA.Asset_ID = X.Asset_ID "
                QuerySQL &= ") ON TA.Category_ID = T.Tenant_ID AND TA.Available = 'Y' AND TA.Active = 1 "
                QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F1 ON T.Tenant_ID = F1.TENANT_ID AND F1.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_Retail_CATEGORY' AND Active = 1) "
                QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F2 ON T.Tenant_ID = F2.TENANT_ID AND F2.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_SITE_CRITERIA' AND Active = 1) "
                QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F3 ON T.Tenant_ID = F3.TENANT_ID AND F3.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_REPRESENTATIVE' AND Active = 1) "
                QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F4 ON T.Tenant_ID = F4.TENANT_ID AND F4.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_CASE_STUDIES' AND Active = 1) "
                QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F5 ON T.Tenant_ID = F5.TENANT_ID AND F5.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANTFEATURED' AND Active = 1) "
                QuerySQL &= "WHERE 1 = 1 "
                QuerySQL &= "AND T.Active = 1 "
                QuerySQL &= "AND T.Available = 'Y' "
                QuerySQL &= "AND S.ProjectID = " & ProjectId & ""
                QuerySQL &= "and TA.Asset_ID is not null  "
                If FuturedTenants = True Then
                    QuerySQL &= "AND F5.Item_Value = '1' "
                End If
                If MajorTenants = True Then
                    QuerySQL &= "AND T.Major = 1 "
                End If
                QuerySQL &= "GROUP BY T.Tenant_ID, TA.Asset_ID, S.Space_ID, S.Description "
            Case "Property Available Spaces"
                QuerySQL &= ""
                QuerySQL &= "S.* "
                QuerySQL &= "FROM IPM_SPACE S "
                QuerySQL &= "LEFT JOIN IPM_SPACE_TENANT ST ON S.Space_ID = ST.Space_ID where ST.Space_ID is null "
                QuerySQL &= "AND S.ProjectID = " & ProjectId & ""
                QuerySQL &= " order by  CAST(isnull(dbo.NumericOnly(S.Suite), 0) as integer) asc "
            Case "Site Plan Available Spaces"
                QuerySQL &= ""
                QuerySQL &= "S.* "
                QuerySQL &= "FROM IPM_SPACE S "
                QuerySQL &= "LEFT JOIN IPM_SPACE_TENANT ST ON S.Space_ID = ST.Space_ID where ST.Space_ID is null "
                QuerySQL &= "AND S.ProjectID = " & ProjectId & ""
                QuerySQL &= " order by  CAST(isnull(dbo.NumericOnly(S.Suite), 0) as integer) asc "
            Case "Site Plan Tenants"
                QuerySQL &= ""
                QuerySQL &= "S.*, "
                QuerySQL &= "T.ShortName "
                QuerySQL &= "FROM IPM_SPACE S "
                QuerySQL &= "JOIN IPM_SPACE_TENANT ST ON S.Space_ID = ST.Space_ID "
                QuerySQL &= "JOIN IPM_TENANT T ON ST.Tenant_ID = T.Tenant_ID "
                QuerySQL &= "AND S.ProjectID = " & ProjectId & ""
                QuerySQL &= " order by  CAST(isnull(dbo.NumericOnly(S.Suite), 0) as integer) asc "
            Case "Site Plan Spaces"
                QuerySQL &= ""
                QuerySQL &= " S.* FROM IPM_SPACE S WHERE S.ProjectID = " & ProjectId & " AND S.Available = 'Y' AND S.Active = 1 "
                QuerySQL &= " order by  CAST(isnull(dbo.NumericOnly(S.Suite), 0) as integer) asc "
        End Select
        Dim QueryDT As New DataTable("QueryDT")
        QueryDT = DBFunctions.GetDataTable(QuerySQL)
        Return QueryDT
    End Function
    Public Shared Function PrintSpaces(ByVal ProjectId As Integer, ByVal Limit As Integer, ByVal FuturedTenants As Boolean, ByVal MajorTenants As Boolean, ByVal TemplatePage As String) As String
        Dim PrintOUT As String = ""
        Dim PrintDT As New DataTable("PrintDT")
        PrintDT = IDAMFunctions.GetSpaces(ProjectId, Limit, FuturedTenants, MajorTenants, TemplatePage)
        If PrintDT.Rows.Count > 0 Then
            Select Case TemplatePage
                Case "Property Tenants Logo"
                    PrintOUT &= "<h5>ANCHORS</h5>"
                    Dim UniqueTenants As New DataTable
                    UniqueTenants = PrintDT.DefaultView.ToTable(True, "Tenant_ID")

                    For R As Integer = 0 To UniqueTenants.Rows.Count - 1
                        PrintDT.DefaultView.RowFilter = "Tenant_ID = '" & UniqueTenants.Rows(R).Item("Tenant_ID") & "'"
                        Dim UniqueAsstes As New DataTable
                        UniqueAsstes = PrintDT.DefaultView.ToTable
                        PrintOUT &= "<span class=""property-logo-link"">"
                        PrintOUT &= "<a class=""spaceItem"" rel=""" & UniqueAsstes.Rows(0)("Space_ID").ToString.Trim & """>"
                        PrintOUT &= "<img src=""/dynamic/image/always/asset/fit/200x150/85/ffffff/NorthWest/" & UniqueAsstes.Rows(0)("Asset_ID").ToString.Trim & ".png"" alt=""""/>"
                        PrintOUT &= "<em></em></a>"
                        PrintOUT &= "</span>"
                    Next
                Case "Property Tenants Logo Site Plan"
                    PrintOUT &= "<div class=""anchorLogos"">"
                    Dim UniqueTenants As New DataTable
                    UniqueTenants = PrintDT.DefaultView.ToTable(True, "Tenant_ID")
                    For R As Integer = 0 To UniqueTenants.Rows.Count - 1
                        PrintDT.DefaultView.RowFilter = "Tenant_ID = '" & UniqueTenants.Rows(R).Item("Tenant_ID") & "'"
                        Dim UniqueAsstes As New DataTable
                        UniqueAsstes = PrintDT.DefaultView.ToTable

                        PrintOUT &= "<span class=""property-logo-link"">"
                        PrintOUT &= "<a class=""spaceItem"" rel=""" & UniqueAsstes.Rows(0)("Space_ID").ToString.Trim & """>"
                        PrintOUT &= "<img src=""/dynamic/image/always/asset/fit/140x110/85/ffffff/North/" & UniqueAsstes.Rows(0)("Asset_ID").ToString.Trim & ".png"" alt=""""/>"
                        PrintOUT &= "<em></em></a>"
                        PrintOUT &= "</span>"
                    Next
                    PrintOUT &= "</div>"
                Case "Property Available Spaces"
                    Dim SitePlansDT As New DataTable

                    SitePlansDT = PrintDT.DefaultView.ToTable(True, "Description", "ProjectID")

                    PrintOUT &= "<div class=""Available_Space"">" & VbNewLine
                    PrintOUT &= "<select name=""Available_Spaces"" id=""Available_Space"" class=""styled-select"">" & VbNewLine
                    PrintOUT &= "<option value="""">--- Select ---</option>" & VbNewLine
                    For SP As Integer = 0 To SitePlansDT.Rows.Count - 1

                        Dim GetSitePlanIDSQL As String = ""
                        GetSitePlanIDSQL &= "SELECT "
                        GetSitePlanIDSQL &= "A.Asset_ID, "
                        GetSitePlanIDSQL &= "A.Name "
                        GetSitePlanIDSQL &= "FROM IPM_ASSET A "
                        GetSitePlanIDSQL &= "LEFT JOIN IPM_ASSET_FIELD_VALUE Sort ON Sort.ASSET_ID = A.Asset_ID AND Sort.Item_ID = (SELECT Item_ID From IPM_ASSET_FIELD_DESC where Item_Tag = 'IDAM_SITEPLANORDER' AND Active = 1 AND Available = 'Y') "
                        GetSitePlanIDSQL &= "WHERE A.Category_ID = (select Category_ID From IPM_ASSET_CATEGORY where Name = 'Site Plan' AND ProjectID =  " & SitePlansDT.Rows(SP).Item("ProjectID").ToString.Trim & "  AND Active = 1 AND Available = 'Y') "
                        GetSitePlanIDSQL &= "AND PDF = 0 AND Active = 1 AND Available = 'Y' AND Version_ID = 0 AND A.Name = '" & SitePlansDT.Rows(SP).Item("Description").ToString.Trim & "' "
                        Dim GetSitePlanIdDT As New DataTable("GetSitePlanIdDT")
                        GetSitePlanIdDT = DBFunctions.GetDataTable(GetSitePlanIDSQL)
                        If GetSitePlanIdDT.Rows.Count > 0 Then
                            PrintOUT &= "<optgroup rel=""" & GetSitePlanIdDT.Rows(0).Item("Asset_ID").ToString.Trim & """ label=""" & SitePlansDT.Rows(SP).Item("Description") & """>" & VbNewLine
                        Else
                            PrintOUT &= "<optgroup rel=""0"" label=""" & SitePlansDT.Rows(SP).Item("Description") & """>" & VbNewLine
                        End If


                        PrintDT.DefaultView.RowFilter = "Description = '" & SitePlansDT.Rows(SP).Item("Description") & "'"
                        Dim GroupedSpacesDT As New DataTable
                        GroupedSpacesDT = PrintDT.DefaultView.ToTable
                        For GS As Integer = 0 To GroupedSpacesDT.Rows.Count - 1
                            Dim SfOut As String = ""
                            Dim SQLDec As Decimal = GroupedSpacesDT.Rows(GS)("SF")
                            SfOut = SQLDec.ToString("#,0") & " SF"

                            PrintOUT &= "<option value=""" & GroupedSpacesDT.Rows(GS)("Space_ID").ToString.Trim & """>" & FormatFunctions.AutoFormatText(GroupedSpacesDT.Rows(GS)("Suite").ToString.Trim) & ": " & SfOut & "</option>" & VbNewLine
                        Next
                        PrintOUT &= "</optgroup>" & VbNewLine
                    Next
                    PrintOUT &= "</select>" & VbNewLine
                    PrintOUT &= "</div>"


                    'PrintOUT &= "<div class=""Available_Space""><select name=""Available_Spaces"" id=""Available_Space"" class=""styled-select""><option value="""">--- Select ---</option>"
                    'For R As Integer = 0 To PrintDT.Rows.Count - 1
                    '    PrintOUT &= "<option value=""" & PrintDT.Rows(R)("Space_ID").ToString.Trim & """>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R)("Suite").ToString.Trim) & "</option>"
                    'Next
                    'PrintOUT &= "</select></div>"
                Case "Site Plan Available Spaces"
                    PrintOUT &= "<h2>AVAILABLE SPACES</h2>"
                    PrintOUT &= "<ul class=""left list"">"
                    For R As Integer = 0 To PrintDT.Rows.Count - 1
                        Dim SfOut As String = ""
                        If PrintDT.Rows(R).Item("SF").ToString <> "" Then
                            Dim SQLDec As Decimal = PrintDT.Rows(R).Item("SF")
                            SfOut = SQLDec.ToString("#,0") & " SF"
                        End If
                        PrintOUT &= "<li>"
                        PrintOUT &= "<a class=""spaceItem"" rel=""" & PrintDT.Rows(R)("Space_ID").ToString.Trim & """>"
                        PrintOUT &= "<span>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R)("Suite").ToString.Trim) & "</span><var>" & SfOut & "</var>"
                        PrintOUT &= "<em></em></a>"
                        PrintOUT &= "</li>"
                    Next
                    PrintOUT &= "</ul>	"
                Case "Site Plan Tenants"
                    PrintOUT &= "<h2>TENANTS</h2>"
                    PrintOUT &= "<ul class=""left list"">"
                    For R As Integer = 0 To PrintDT.Rows.Count - 1
                        PrintOUT &= "<li>"
                        PrintOUT &= "<a class=""spaceItem"" rel=""" & PrintDT.Rows(R)("Space_ID").ToString.Trim & """>"
                        PrintOUT &= "<span>" & PrintDT.Rows(R)("Suite").ToString.Trim & "</span><var>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R)("ShortName").ToString.Trim) & "</var>"
                        PrintOUT &= "<em></em></a>"
                        PrintOUT &= "</li>"
                    Next
                    PrintOUT &= "</ul>	"
                Case "Site Plan Spaces"
                    PrintOUT &= "<select id=""SpaceID""><option value="""">---</option>"
                    For R As Integer = 0 To PrintDT.Rows.Count - 1
                        PrintOUT &= "<option value=""" & PrintDT.Rows(R)("Space_ID").ToString.Trim & """>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R)("Suite").ToString.Trim) & "</option>"
                    Next
                    PrintOUT &= "</select>"
            End Select
        End If
        Return PrintOUT
    End Function

    Public Shared Function GetProjectNews(ByVal ProjectId As Integer, ByVal Limit As Integer) As DataTable
        Dim QuerySQL As String = ""
        If Limit <> 0 Then
            QuerySQL &= "SELECT TOP " & Limit & " "
        Else
            QuerySQL &= "SELECT "
        End If
        QuerySQL &= "N.*, "
        QuerySQL &= "F1.Item_Value as NewsUrl, "
        QuerySQL &= "F2.Item_Value as UrlTitle, "
        QuerySQL &= "DATENAME(mm,N.Post_Date) + ' ' + DATENAME(DAY,N.Post_Date) + ', ' + DATENAME(YEAR, N.Post_Date) DateLoc, "
        QuerySQL &= "F4.Item_Value as EventStaff "
        QuerySQL &= "FROM IPM_NEWS N "
        QuerySQL &= "JOIN IPM_NEWS_RELATED_PROJECTS PRN ON N.News_Id = PRN.News_Id AND PRN.ProjectID = " & ProjectId & ""
        QuerySQL &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F1 on N.News_Id = F1.NEWS_ID and F1.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_NEWS_URL' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F2 on N.News_Id = F2.NEWS_ID and F2.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_URLTITLE' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F4 on N.News_Id = F4.NEWS_ID and F4.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_EVENTSTAFF' AND Active = 1) "
        QuerySQL &= "WHERE 1=1 "
        QuerySQL &= "AND N.Show = 1 "
        QuerySQL &= "AND N.post_date < getdate() "
        QuerySQL &= "AND N.pull_date > getdate() "
        QuerySQL &= "ORDER BY N.Post_Date desc "
        Dim QueryDT As New DataTable("QueryDT")
        QueryDT = DBFunctions.GetDataTable(QuerySQL)
        Return QueryDT
    End Function
    Public Shared Function PrintProjectNews(ByVal ProjectId As Integer, ByVal Limit As Integer) As String
        Dim PrintOUT As String = ""
        Dim PrintDT As New DataTable("PrintDT")
        PrintDT = IDAMFunctions.GetProjectNews(ProjectId, Limit)
        If PrintDT.Rows.Count > 0 Then
            '---TEMPLATE START---'
            PrintOUT &= "<h5>RELATED NEWS</h5>"
            PrintOUT &= "<span class=""red-text"">"
            For R As Integer = 0 To PrintDT.Rows.Count - 1
                PrintOUT &= "<a href=""#news!" & PrintDT.Rows(R)("News_ID").ToString.Trim & """>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R).Item("Headline").ToString.Trim) & "</a><br/>"
            Next
            PrintOUT &= "</span>"
            '---TEMPLATE END---'
        End If
        Return PrintOUT
    End Function

    Public Shared Function GetProjectCaseStudies(ByVal ProjectId As Integer, ByVal Limit As Integer, ByVal FuturedTenants As Boolean, ByVal MajorTenants As Boolean) As DataTable
        Dim QuerySQL As String = ""
        QuerySQL &= "SELECT "
        QuerySQL &= "P.ProjectID, "
        QuerySQL &= "P.Name "
        QuerySQL &= "FROM IPM_PROJECT P "
        QuerySQL &= "JOIN IPM_PROJECT_RELATED RP ON RP.Project_Id = P.ProjectID "
        QuerySQL &= "AND RP.Ref_Id = " & ProjectId & ""
        Dim QueryDT As New DataTable("QueryDT")
        QueryDT = DBFunctions.GetDataTable(QuerySQL)
        Return QueryDT

    End Function
    Public Shared Function PrintProjectCaseStudies(ByVal ProjectId As Integer, ByVal Limit As Integer, ByVal FuturedTenants As Boolean, ByVal MajorTenants As Boolean) As String
        Dim PrintOUT As String = ""
        Dim PrintDT As New DataTable("PrintDT")
        PrintDT = IDAMFunctions.GetProjectCaseStudies(ProjectId, Limit, FuturedTenants, MajorTenants)
        If PrintDT.Rows.Count > 0 Then
            PrintOUT &= "<h5>RELATED CASE STUDIES</h5>"
            PrintOUT &= "<span class=""red-text"">"
            For R As Integer = 0 To PrintDT.Rows.Count - 1
                PrintOUT &= "<a href=""#case-studies!" & PrintDT.Rows(R).Item("ProjectID") & """>" & FormatFunctions.AutoFormatText(PrintDT.Rows(R).Item("Name").ToString.Trim) & "</a><br />"
            Next
            PrintOUT &= "</span>"
        End If
        Return PrintOUT
    End Function

    Public Shared Function GetProjectsFilters(ByVal FilterName As String, ByVal TemplatePage As String) As DataTable
        Dim QuerySQL As String = ""
        QuerySQL &= "SELECT "
        Select Case TemplatePage
            Case "Properties"
                Select Case FilterName
                    Case "State"
                        QuerySQL &= "S.Name  AS FilterText  "
                        QuerySQL &= "FROM IPM_PROJECT P "
                        QuerySQL &= "JOIN IPM_STATE S ON S.State_id = P.State_id AND Active = 1 "
                        QuerySQL &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F2 (nolock) on P.ProjectID = F2.ProjectID and F2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_COUNTY')  "
                        QuerySQL &= "WHERE 1 = 1 "
                        QuerySQL &= "AND P.Show = 1 "
                        QuerySQL &= "AND P.Available = 'Y' "
                        QuerySQL &= "AND S.State_id is not null AND S.State_id <> '' "
                        QuerySQL &= "AND P.Category_ID IN (2409776)"
                        QuerySQL &= "AND P.State_id is not null and P.State_id <> '' "
                        QuerySQL &= "AND F2.Item_Value is not null and F2.Item_Value <> '' "
                        QuerySQL &= "GROUP BY S.Name "
                        QuerySQL &= "ORDER BY S.Name "
                    Case "County"
                        QuerySQL &= "F2.Item_Value + '/ ' + S.Name AS FilterText "
                        QuerySQL &= "FROM IPM_PROJECT P "
                        QuerySQL &= "JOIN IPM_PROJECT_FIELD_VALUE F2 on P.ProjectID = F2.ProjectID and F2.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_COUNTY')  "
                        QuerySQL &= "JOIN IPM_STATE S ON S.State_id = P.State_id "
                        QuerySQL &= "WHERE 1 = 1 "
                        QuerySQL &= "AND P.Show = 1 "
                        QuerySQL &= "AND P.Available = 'Y' "
                        QuerySQL &= "AND F2.Item_Value is not null AND F2.Item_Value <> '' "
                        QuerySQL &= "AND P.Category_ID IN (2409776)"
                        QuerySQL &= "GROUP BY F2.Item_Value, S.Name "
                        QuerySQL &= "ORDER BY F2.Item_Value "
                    Case "Anchor"
                        QuerySQL &= "T.ShortName AS FilterText "
                        QuerySQL &= "FROM IPM_TENANT T "
                        QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F1 ON T.Tenant_ID = F1.TENANT_ID AND F1.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_Retail_CATEGORY' AND Active = 1) "
                        QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F2 ON T.Tenant_ID = F2.TENANT_ID AND F2.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_SITE_CRITERIA' AND Active = 1) "
                        QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F3 ON T.Tenant_ID = F3.TENANT_ID AND F3.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_REPRESENTATIVE' AND Active = 1) "
                        QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F4 ON T.Tenant_ID = F4.TENANT_ID AND F4.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANT_CASE_STUDIES' AND Active = 1) "
                        QuerySQL &= "LEFT JOIN IPM_TENANT_FIELD_VALUE F5 ON T.Tenant_ID = F5.TENANT_ID AND F5.Item_ID = (Select Item_ID From IPM_TENANT_FIELD_DESC where Item_Tag = 'IDAM_TENANTFEATURED' AND Active = 1) "
                        QuerySQL &= "WHERE 1 = 1 "
                        QuerySQL &= "AND T.Active = 1 "
                        QuerySQL &= "AND T.Available = 'Y' "
                        QuerySQL &= "AND T.Major = 1 "
                        QuerySQL &= "AND t. TENANT_ID in ( "
                        QuerySQL &= "SELECT "
                        QuerySQL &= "T.Tenant_ID "
                        QuerySQL &= "FROM IPM_SPACE S "
                        QuerySQL &= "JOIN IPM_SPACE_TENANT ST ON S.Space_ID = ST.Space_ID "
                        QuerySQL &= "JOIN IPM_TENANT T ON ST.Tenant_ID = T.Tenant_ID "
                        QuerySQL &= "JOIN IPM_PROJECT P ON P.ProjectID = S.ProjectID  "
                        QuerySQL &= ")"
                        QuerySQL &= "ORDER BY T.ShortName asc "
                End Select
        End Select
        Dim QueryDT As New DataTable("QueryDT")
        QueryDT = DBFunctions.GetDataTable(QuerySQL)
        Return QueryDT
    End Function
    Public Shared Function PrintProjectsFilters(ByVal FilterName As String, ByVal TemplatePage As String) As String
        Dim PrintOUT As String = ""
        Dim PrintDT As New DataTable("PrintDT")
        PrintDT = IDAMFunctions.GetProjectsFilters(FilterName, TemplatePage)
        If PrintDT.Rows.Count > 0 Then
            Select Case TemplatePage
                Case "Properties"
                    For R As Integer = 0 To PrintDT.Rows.Count - 1
                        PrintOUT &= "<option value=""" & HttpUtility.UrlEncode(PrintDT.Rows(R)("FilterText").ToString.Trim) & """>" & FormatFunctions.AutoFormatText(Replace(PrintDT.Rows(R)("FilterText").ToString.Trim, "/", ",")) & "</option>"
                    Next
            End Select
        End If
        Return PrintOUT
    End Function

End Class

Public Class ProjectStructure

    Private ProjectTitleString As String

    Private ProjectStateString As String
    Private ProjectCountyString As String
    Private ProjectCityString As String

    Private ProjectAddressString As String
    Private ProjectDescriptionString As String
    Private ProjectWebsiteString As String

    Private ProjectSEOTitleString As String
    Private ProjectSEODescriptionString As String
    Private ProjectSEOKeyWordsString As String


    Private ColH1String As String
    Private ColH2String As String
    Private ColH3String As String

    Private RowH1String As String
    Private RowH2String As String
    Private RowH3String As String
    Private RowH4String As String

    Private Row1Col1String As String
    Private Row1Col2String As String
    Private Row1Col3String As String

    Private Row2Col1String As String
    Private Row2Col2String As String
    Private Row2Col3String As String

    Private Row3Col1String As String
    Private Row3Col2String As String
    Private Row3Col3String As String

    Private Row4Col1String As String
    Private Row4Col2String As String
    Private Row4Col3String As String


    Private PropertyServiceString As List(Of String)

    Private AerialIdString As String
    Private AreaMapString As String

    Private BrochureIdString As String
    Private DemographicIdString As String
    Private SiteMapIdString As String
    Private SiteMapPDFString As String

    Private SiteMapCoordinatesString As String

    Private CaseStudy_MarketString As String
    Private CaseStudy_TimingString As String
    Private CaseStudy_MissionString As String
    Private CaseStudy_LocationsString As String


    Public Property CaseStudy_Market() As String
        Get
            Return CaseStudy_MarketString
        End Get
        Set(ByVal value As String)
            CaseStudy_MarketString = value
        End Set
    End Property

    Public Property CaseStudy_Timing() As String
        Get
            Return CaseStudy_TimingString
        End Get
        Set(ByVal value As String)
            CaseStudy_TimingString = value
        End Set
    End Property

    Public Property CaseStudy_Mission() As String
        Get
            Return CaseStudy_MissionString
        End Get
        Set(ByVal value As String)
            CaseStudy_MissionString = value
        End Set
    End Property

    Public Property CaseStudy_Locations() As String
        Get
            Return CaseStudy_LocationsString
        End Get
        Set(ByVal value As String)
            CaseStudy_LocationsString = value
        End Set
    End Property


    Public Property ProjectTitle() As String
        Get
            Return ProjectTitleString
        End Get
        Set(ByVal value As String)
            ProjectTitleString = value
        End Set
    End Property
    Public Property ProjectAddress() As String
        Get
            Return ProjectAddressString
        End Get
        Set(ByVal value As String)
            ProjectAddressString = value
        End Set
    End Property

    Public Property ProjectState() As String
        Get
            Return ProjectStateString
        End Get
        Set(ByVal value As String)
            ProjectStateString = value
        End Set
    End Property

    Public Property ProjectCounty() As String
        Get
            Return ProjectCountyString
        End Get
        Set(ByVal value As String)
            ProjectCountyString = value
        End Set
    End Property

    Public Property ProjectCity() As String
        Get
            Return ProjectCityString
        End Get
        Set(ByVal value As String)
            ProjectCityString = value
        End Set
    End Property


    Public Property ProjectDescription() As String
        Get
            Return ProjectDescriptionString
        End Get
        Set(ByVal value As String)
            ProjectDescriptionString = value
        End Set
    End Property
    Public Property ProjectWebsite() As String
        Get
            Return ProjectWebsiteString
        End Get
        Set(ByVal value As String)
            ProjectWebsiteString = value
        End Set
    End Property


    Public Property ColH1() As String
        Get
            Return ColH1String
        End Get
        Set(ByVal value As String)
            ColH1String = value
        End Set
    End Property
    Public Property ColH2() As String
        Get
            Return ColH2String
        End Get
        Set(ByVal value As String)
            ColH2String = value
        End Set
    End Property
    Public Property ColH3() As String
        Get
            Return ColH3String
        End Get
        Set(ByVal value As String)
            ColH3String = value
        End Set
    End Property

    Public Property RowH1() As String
        Get
            Return RowH1String
        End Get
        Set(ByVal value As String)
            RowH1String = value
        End Set
    End Property
    Public Property RowH2() As String
        Get
            Return RowH2String
        End Get
        Set(ByVal value As String)
            RowH2String = value
        End Set
    End Property
    Public Property RowH3() As String
        Get
            Return RowH3String
        End Get
        Set(ByVal value As String)
            RowH3String = value
        End Set
    End Property
    Public Property RowH4() As String
        Get
            Return RowH4String
        End Get
        Set(ByVal value As String)
            RowH4String = value
        End Set
    End Property

    Public Property Row1Col1() As String
        Get
            Return Row1Col1String
        End Get
        Set(ByVal value As String)
            Row1Col1String = value
        End Set
    End Property
    Public Property Row1Col2() As String
        Get
            Return Row1Col2String
        End Get
        Set(ByVal value As String)
            Row1Col2String = value
        End Set
    End Property
    Public Property Row1Col3() As String
        Get
            Return Row1Col3String
        End Get
        Set(ByVal value As String)
            Row1Col3String = value
        End Set
    End Property

    Public Property Row2Col1() As String
        Get
            Return Row2Col1String
        End Get
        Set(ByVal value As String)
            Row2Col1String = value
        End Set
    End Property
    Public Property Row2Col2() As String
        Get
            Return Row2Col2String
        End Get
        Set(ByVal value As String)
            Row2Col2String = value
        End Set
    End Property
    Public Property Row2Col3() As String
        Get
            Return Row2Col3String
        End Get
        Set(ByVal value As String)
            Row2Col3String = value
        End Set
    End Property

    Public Property Row3Col1() As String
        Get
            Return Row3Col1String
        End Get
        Set(ByVal value As String)
            Row3Col1String = value
        End Set
    End Property
    Public Property Row3Col2() As String
        Get
            Return Row3Col2String
        End Get
        Set(ByVal value As String)
            Row3Col2String = value
        End Set
    End Property
    Public Property Row3Col3() As String
        Get
            Return Row3Col3String
        End Get
        Set(ByVal value As String)
            Row3Col3String = value
        End Set
    End Property

    Public Property Row4Col1() As String
        Get
            Return Row4Col1String
        End Get
        Set(ByVal value As String)
            Row4Col1String = value
        End Set
    End Property
    Public Property Row4Col2() As String
        Get
            Return Row4Col2String
        End Get
        Set(ByVal value As String)
            Row4Col2String = value
        End Set
    End Property
    Public Property Row4Col3() As String
        Get
            Return Row4Col3String
        End Get
        Set(ByVal value As String)
            Row4Col3String = value
        End Set
    End Property

    Public Property PropertyService() As List(Of String)
        Get
            Return PropertyServiceString
        End Get
        Set(ByVal value As List(Of String))
            PropertyServiceString = value
        End Set
    End Property
    Public Property AerialId() As String
        Get
            Return AerialIdString
        End Get
        Set(ByVal value As String)
            AerialIdString = value
        End Set
    End Property
    Public Property AreaMap() As String
        Get
            Return AreaMapString
        End Get
        Set(ByVal value As String)
            AreaMapString = value
        End Set
    End Property
    Public Property BrochureId() As String
        Get
            Return BrochureIdString
        End Get
        Set(ByVal value As String)
            BrochureIdString = value
        End Set
    End Property
    Public Property DemographicId() As String
        Get
            Return DemographicIdString
        End Get
        Set(ByVal value As String)
            DemographicIdString = value
        End Set
    End Property
    Public Property SiteMapId() As String
        Get
            Return SiteMapIdString
        End Get
        Set(ByVal value As String)
            SiteMapIdString = value
        End Set
    End Property

    Public Property SiteMapPDF() As String
        Get
            Return SiteMapPDFString
        End Get
        Set(ByVal value As String)
            SiteMapPDFString = value
        End Set
    End Property

    Public Property SiteMapCoordinates() As String
        Get
            Return SiteMapCoordinatesString
        End Get
        Set(ByVal value As String)
            SiteMapCoordinatesString = value
        End Set
    End Property

    Public Property ProjectSEOTitle() As String
        Get
            Return ProjectSEOTitleString
        End Get
        Set(ByVal value As String)
            ProjectSEOTitleString = value
        End Set
    End Property
    Public Property ProjectSEODescription() As String
        Get
            Return ProjectSEODescriptionString
        End Get
        Set(ByVal value As String)
            ProjectSEODescriptionString = value
        End Set
    End Property
    Public Property ProjectSEOKeyWords() As String
        Get
            Return ProjectSEOKeyWordsString
        End Get
        Set(ByVal value As String)
            ProjectSEOKeyWordsString = value
        End Set
    End Property

End Class

Public Class ProjectFiltersStructure
    Private ProjectCollectionString As String
    Private SizeOfSpaceString As String
    Private PropertyStateString As String
    Private PropertyCountyString As String
    Private PropertyAnchorString As String
    Private SearchPhraseString As String

    Public Property SizeOfSpace() As String
        Get
            Return SizeOfSpaceString
        End Get
        Set(ByVal value As String)
            SizeOfSpaceString = value
        End Set
    End Property
    Public Property PropertyState() As String
        Get
            Return PropertyStateString
        End Get
        Set(ByVal value As String)
            PropertyStateString = value
        End Set
    End Property
    Public Property PropertyCounty() As String
        Get
            Return PropertyCountyString
        End Get
        Set(ByVal value As String)
            PropertyCountyString = value
        End Set
    End Property
    Public Property PropertyAnchor() As String
        Get
            Return PropertyAnchorString
        End Get
        Set(ByVal value As String)
            PropertyAnchorString = value
        End Set
    End Property
    Public Property ProjectCollection() As String
        Get
            Return ProjectCollectionString
        End Get
        Set(ByVal value As String)
            ProjectCollectionString = value
        End Set
    End Property
    Public Property SearchPhrase() As String
        Get
            Return SearchPhraseString
        End Get
        Set(ByVal value As String)
            SearchPhraseString = value
        End Set
    End Property

End Class

Public Class MapResponseStructure
    Private LatString As String
    Private LongString As String
    Private PropertyDataObject As Object

    Public Property lat() As String
        Get
            Return LatString
        End Get
        Set(ByVal value As String)
            LatString = value
        End Set
    End Property

    Public Property lng() As String
        Get
            Return LongString
        End Get
        Set(ByVal value As String)
            LongString = value
        End Set
    End Property

    Public Property data() As Object
        Get
            Return PropertyDataObject
        End Get
        Set(ByVal value As Object)
            PropertyDataObject = value
        End Set
    End Property
End Class

Public Class PropertyDataStructure
    Private linkString As String
    Private titleString As String
    Private idString As String
    Private sfString As String
    Private addressString As String

    Public Property link() As String
        Get
            Return linkString
        End Get
        Set(ByVal value As String)
            linkString = value
        End Set
    End Property

    Public Property title() As String
        Get
            Return titleString
        End Get
        Set(ByVal value As String)
            titleString = value
        End Set
    End Property

    Public Property id() As String
        Get
            Return idString
        End Get
        Set(ByVal value As String)
            idString = value
        End Set
    End Property

    Public Property sf() As String
        Get
            Return sfString
        End Get
        Set(ByVal value As String)
            sfString = value
        End Set
    End Property

    Public Property address() As String
        Get
            Return addressString
        End Get
        Set(ByVal value As String)
            addressString = value
        End Set
    End Property
End Class



