﻿<%@ Page Title="" Language="VB" MasterPageFile="~/HRMaster1.master" AutoEventWireup="false" CodeFile="privacy.aspx.vb" Inherits="privacy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!-- Page Content START -->
<div class="page-content">
    <div class="top-section fluid-padding">
        	<h1 class="cms cmsType_TextSingle cmsName_Privacy_Top_Heading">About Us</h1>
        	<h2 class="cms cmsType_TextSingle cmsName_Privacy_Heading">Privacy Policy</h2>
        	<h3 class="redtext cms cmsType_TextSingle cmsName_About_Heading_Red_Text">Privacy</h3>
    </div>
    <div class="main general about-page">
        <div class="content">
            <div class="leftMain cms cmsType_Rich cmsName_Privacy_Content"></div>
        </div>
        <div class="sidebar">
            <div class="sidebarContent">
                <div class="cms cmsType_Rich cmsName_Privacy_Right_Column"></div>
            </div>
            <br class="clear" />
        </div>
        <div class="c"></div>
    </div>
    <div class="c"></div>
</div>
<!-- Page Content END -->


</asp:Content>

