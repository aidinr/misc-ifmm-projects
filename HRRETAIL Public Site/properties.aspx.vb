﻿
Partial Class properties
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        StateList_lbl.Text = IDAMFunctions.PrintProjectsFilters("State", "Properties")
        CountyList_lbl.Text = IDAMFunctions.PrintProjectsFilters("County", "Properties")
        AnchorList_Lbl.Text = IDAMFunctions.PrintProjectsFilters("Anchor", "Properties")
    End Sub

End Class
