﻿<%@ Page Title="" Language="VB" MasterPageFile="~/HRMaster1.master" AutoEventWireup="false" CodeFile="retail-solutions.aspx.vb" Inherits="retailsolutions" %>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!-- Page Content START -->
		<div class="page-content">
			<div class="top-section fluid-padding">
				<h1>about us</h1>
				<h2>complete   <span class="red">retail</span> solutions</h2>
			</div>			
			<div class="main">
				<ul class="services-list cmsGroup cmsName_Retail_Solutions">

					<!--
					<li class="fluid-padding cmsGroupItem">
						<div class="text left first">
							<h2 class="people-title cms cmsType_TextSingle cmsName_Title">Properties & Leasing</h2>
							<br/>
							<h3>Select Clients<h3>
							<p class="cms cmsType_Linkset cmsName_Clients">
							 <br/><a href="Client_Link" class="light-gray">Client Name</a>
							 <br/><a href="Client_Link" class="light-gray">Client Name</a>
							 <br/><a href="Client_Link" class="light-gray">Client Name</a>
							</p>
						</div>
						<div class="text left second">
							<h2 class="cms cmsType_TextSingle cmsName_Heading">650 million square feet of space and counting.</h2>
							<p class="cms cmsType_TextMulti cmsName_Content">We represent a wide range of retail property types, including urban and main street districts, specialty and lifestyle centers, mixed-use projects, power centers, grocery anchored shopping complexes, and single-tenant buildings.</p>
							<ul class="cms cmsType_TextMulti cmsName_Items">
								<li>Merchandising Strategy / Tenant Mix Evaluation</li>
								<li>Letter of Intent Negotiation</li>
								<li>Marketing Materials for both print and digital</li>
								<li>Competitive & Portfolio Review</li>
								<li>E-mail Marketing Campaigns</li>
								<li>Demographic Studies and Mapping</li>
								<li>Letter of Intent Negotiation</li>
								<li>Aerial Photographic Surveys</li>
							</ul>
						</div>
		
						<div class="info-pop-slider right">
							<p class="cms cmsType_TextMulti cmsName_Case_Studies"></p>
						</div>
					</li>
					-->
                    <%
                           For i As Integer = 1 To 20 - 1
                                 If Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Retail_Solutions_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
                            %>
                                            

                        <li class="fluid-padding cmsGroupItem">
						<div class="text left first">
							<h2 class="people-title cms cmsType_TextSingle cmsName_Retail_Solutions_Item_<%=i%>_Title">Properties &amp; Leasing</h2>
							<br/>
							<h3>Select Clients</h3>
							<p class="cms cmsType_Linkset cmsName_Retail_Solutions_Item_<%=i%>_Clients"></p>
						</div>
						<div class="text left second">
							<h2 class="cms cmsType_TextSingle cmsName_Retail_Solutions_Item_<%=i%>_Heading">650 million square feet of space and counting.</h2>
							<p class="cms cmsType_TextMulti cmsName_Retail_Solutions_Item_<%=i%>_Content">We represent a wide range of retail property types, including urban and main street districts, specialty and lifestyle centers, mixed-use projects, power centers, grocery anchored shopping complexes, and single-tenant buildings.</p>
							<ul class="cms cmsFill_Static cmsType_TextMulti cmsName_Retail_Solutions_Item_<%=i%>_Items">
                                <%=CMSFunctions.FormatList(Server.MapPath("~") & "cms\data\Text\", "Retail_Solutions_Item_" & i & "_Items", "li")%>
							</ul>
						</div>


                            <%
                                                                                
                                        Dim CaseStudyOUT As String = ""
                                        Dim ProjectFilters As New ProjectFiltersStructure
                                        ProjectFilters.ProjectCollection = CMSFunctions.GetText(Server.MapPath("~") & "cms\data\Text\Retail_Solutions_Item_" & i & "_Case_Studies.txt")


		
                            %>

						<div class="info-pop-slider right">
						<p class="caseStudyRight"><em class="cms cmsFill_Static cmsType_TextMulti cmsName_Retail_Solutions_Item_<%=i%>_Case_Studies"></em></p>
                            <%
                                        CaseStudyOUT = IDAMFunctions.PrintProjects("2410606", 0, 1, "Retail Solutions", ProjectFilters)
                             %>
                                        <%If CaseStudyOUT <> "" Then%>
							<ul>
								<%= CaseStudyOUT%>
							</ul>
                            <%End If%>
						</div>
					</li>
                    
                              <%
                              End If
                          Next
                          %>






				</ul>			
			</div>	
		</div>
		<!-- Page Content END -->
</asp:Content>
