﻿<%@ Page Title="" Language="VB" MasterPageFile="~/HRMaster1.master" AutoEventWireup="false" CodeFile="news.aspx.vb" Inherits="news" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


		<!-- Page Content START -->
		<div class="page-content">
			<div class="top-section fluid-padding">
				<h1>about us</h1>
				<h2>news &amp; <span class="red">daily clips</span></h2>
			</div>
			<div class="main news-page">
            <asp:Literal runat="server" ID="news_list1"/>
            	<script src="/assets/js/tw.js" type="text/javascript"></script>
				<div class="right sidebar daily">
				   <div class="rightSide">
					<div class="tweetHead"><h2>Twitter Daily Clips</h2></div>
					<div id="tweets"></div>
					<div class="signup subscription-form">
						<button type="button" class="signup show-hidden-form">sign up for daily clips</button>
						<form method="post" action="/" class="site-form hidden-form clips">
							<button type="button" class="close-button">Close</button>
							<p>
								Sign up to receive Daily Clips e-mails. Our compilation of retail news from around the Washington, DC and Baltimore area.
								<small></small>
							</p>
							<br/>
							<ol>
								<li class="full-width"><input type="text" name="" value="" id="f2f223e" alt="First &amp; Last Name"/></li>
								<li class="clear full-width">
									<input type="text" name="" value="" id="f2f23" alt="Email Address"/>
									<input type="submit" name="" class="submit" value="Submit"/>
								</li>
							</ol>
						</form>
					</div>
					<a href="https://twitter.com/HRDailyClips" class="twitter-follow" target="_blank">or Follow us on twitter</a>
				   </div>
				</div>
				<br class="clear"/>
			</div>
		</div>
		<!-- Page Content END -->
</asp:Content>
