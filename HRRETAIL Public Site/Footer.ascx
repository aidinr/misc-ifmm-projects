﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Footer.ascx.vb" Inherits="Footer" %>
<%@ Import Namespace="System.IO" %>

<!-- Footer START -->	
		<div class="footer fluid-padding">
			<div class="top">
				<div>
					<a href="#" class="cms cmsType_Link cmsName_Link">
                    <em>
                    <img class="mb20 cms cmsType_Image cmsName_Footer_Logo" src="cms/data/Sized/liquid/135x/65/ffffff/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Footer_Logo")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Footer_Logo")%>" />
                    </em>
                    </a>
					<p class="cms cmsType_TextMulti cmsName_Footer_Content">H&amp;R Retail is a proud member of ChainLinks Retail Advisors, the leading retail real estate service provider in the United States. This connection allows us to leverage our exprerience and knowledge in local markets throughout all fifty states.</p>
				</div>
				<div>
					<h2>main menu</h2>
					<ul class="menu">
						<li><a href="about">ABOUT US</a></li>
						<li><a href="properties">PROPERTIES</a></li>
						<li><a href="retailers-and-restaurants">RETAILERS &amp; RESTAURANTS</a></li>
						<li><a href="investment-sales">INVESTMENT SALES</a></li>
						<li><a href="urban">H&amp;R URBAN</a></li>
						<li><a href="case-studies">Case Studies</a></li>
					</ul>
				</div>
				<div>
					<h2>Contact</h2>
					<ul class="menu">
						<li><a href="contact">contact us</a></li>
						<li><a href="news">Daily Clips</a></li>
						<li><a class="PortfolioRequest">Portfolio Request</a></li>
					</ul>		
					<h2>login</h2>
					<ul>
						<li><a href="#" class="cms cmsType_Link cmsName_Broker_Link">broker login</a></li>
						<li><a href="#" class="cms cmsType_Link cmsName_Client_Access_Link">client access</a></li>
					</ul>											
				</div>
				<div>
					<h2>Resources</h2>
					<ul class="resources cmsGroup cmsName_Footer_Resources">
					<!--
						<li class="cmsGroupItem"><a href="#" class="cms cmsType_File cmsName_Document">Sample Downloadable Document</a></li>
					-->

                       <%
                      For i As Integer = 1 To 20 - 1
                               If Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Footer_Resources_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
                            %>
                            <li class="cmsGroupItem"><a href="#" class="cms cmsType_File cmsName_Footer_Resources_Item_<%=i%>_Document">Property availability sheet</a></li>
                              <%
                              End If
                          Next
                          %>
					</ul>
				</div>
			</div>
			
			<div class="bottom">
				<ul class="left menu">
					<li><address class="cms cmsType_TextSingle cmsName_Copyright">&copy; 2013 H&amp;R RETAIL. All rights reserved.</address></li>
					<li><a href="terms">Terms of Use</a></li>
					<li><a href="privacy">Privacy Policy</a></li>
				</ul>

				<ul class="socials right">
					<li class="cms cmsFill_Static cmsType_TextSingle cmsName_Twitter"><a href="<%=CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Twitter")%>" class="tw" target="_blank">Twitter</a></li>
					<li class="cms cmsFill_Static cmsType_TextSingle cmsName_Facebook"><a href="<%=CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Facebook")%>" class="fb" target="_blank">Facebook</a></li>
					<li class="cms cmsFill_Static cmsType_TextSingle cmsName_LinkedIn"><a href="<%=CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "LinkedIn")%>" class="ln" target="_blank">LinkedIn</a></li>
				</ul>
                		
			</div>
		</div>
		<!-- Footer END -->	
