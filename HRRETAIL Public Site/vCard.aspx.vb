﻿Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Partial Class vCard
    Inherits System.Web.UI.Page

    Protected Sub vcard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim UserId As Integer
        If Not Request.QueryString("id") Is Nothing Then
            UserId = Request.QueryString("id").Trim
            If IsNumeric(UserId) Then
                cmpVCard.VCard(Response, UserId)
            End If
        End If
    End Sub
End Class

Public Class cmpVCard
    Inherits System.ComponentModel.Component

    Public Shared Sub VCard(ByVal response As HttpResponse, ByVal UsrId As Integer)
        Dim QuerySQL As String = ""
        QuerySQL &= "SELECT "
        QuerySQL &= "U.*, "
        QuerySQL &= "F1.Item_Value AS IDAM_EDUCATION, "
        QuerySQL &= "F2.Item_Value AS IDAM_USERSORT, "
        QuerySQL &= "F3.Item_Value AS IDAM_SPECIALTIES "
        QuerySQL &= "FROM IPM_USER U "
        QuerySQL &= "LEFT JOIN IPM_USER_FIELD_VALUE F1 ON U.UserID = F1.USER_ID AND F1.Item_ID = (SELECT Item_ID FROM IPM_USER_FIELD_DESC WHERE Item_Tag = 'IDAM_EDUCATION' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_USER_FIELD_VALUE F2 ON U.UserID = F2.USER_ID AND F2.Item_ID = (SELECT Item_ID FROM IPM_USER_FIELD_DESC WHERE Item_Tag = 'IDAM_USERSORT' AND Active = 1) "
        QuerySQL &= "LEFT JOIN IPM_USER_FIELD_VALUE F3 ON U.UserID = F3.USER_ID AND F3.Item_ID = (SELECT Item_ID FROM IPM_USER_FIELD_DESC WHERE Item_Tag = 'IDAM_SPECIALTIES' AND Active = 1) "
        QuerySQL &= "WHERE 1 = 1 "
        QuerySQL &= "AND U.UserID = " & UsrId & ""
        Dim QueryDT As New DataTable("QueryDT")
        QueryDT = DBFunctions.GetDataTable(QuerySQL)

        If QueryDT.Rows.Count > 0 Then
            Dim nameFirst As String = QueryDT.Rows(0).Item("FirstName").ToString
            Dim nameLast As String = QueryDT.Rows(0).Item("LastName").ToString
            Dim nameTitle As String = "Mr"
            Dim email As String = QueryDT.Rows(0).Item("Email").ToString
            Dim uRL As String = "www.hrretail.com"
            Dim telephone As String = QueryDT.Rows(0).Item("Phone").ToString
            Dim fax As String = "(0) 1555 555555"
            Dim mobile As String = QueryDT.Rows(0).Item("Cell").ToString
            Dim company As String = QueryDT.Rows(0).Item("Agency").ToString
            Dim department As String = QueryDT.Rows(0).Item("FirstName").ToString
            Dim title As String = QueryDT.Rows(0).Item("Position").ToString
            Dim profession As String = "Developer"
            Dim office As String = "Bedroom"
            Dim addressTitle As String = "Chemlock"
            Dim streetName As String = "25 Nowhere Str"
            Dim city As String = "London"
            Dim region As String = "Surrey"
            Dim postCode As String = "GU30 7BZ"
            Dim country As String = "ENGLAND"

            response.Clear()
            response.Charset = ""
            response.ContentType = "text/vcard"

            response.AddHeader("Content-Disposition", "attachment; fileName=" + RegularExpressions.Regex.Replace(nameFirst + " " + nameLast, "[^a-zA-Z]+", "_") + ".vcf")

            Dim stringWrite As New System.IO.StringWriter()
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            stringWrite.WriteLine("BEGIN:VCARD")
            stringWrite.WriteLine("VERSION:2.1")
            stringWrite.WriteLine("N:" & nameLast & ";" & nameFirst)
            stringWrite.WriteLine("FN:" & nameFirst & " " & nameLast)
            stringWrite.WriteLine("ORG:" & company)
            stringWrite.WriteLine("TITLE:" & title)
            stringWrite.WriteLine("TEL;WORK;VOICE:" & telephone) 'zzzzzzzzzz
            stringWrite.WriteLine("TEL;CELL;VOICE:" & mobile)
            stringWrite.WriteLine("EMAIL;PREF;INTERNET:" & email)
            stringWrite.WriteLine("URL;WORK:" & uRL)
            stringWrite.WriteLine("REV:" & DateTime.Now.Year & DateTime.Now.Month & DateTime.Now.Day)
            stringWrite.WriteLine("END:VCARD")
            response.Write(stringWrite.ToString())
            response.End()
        End If

    End Sub
End Class
