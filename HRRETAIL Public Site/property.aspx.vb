﻿Imports System.Collections.Generic
Imports System.Data
Imports System.IO

Partial Class property1
    Inherits System.Web.UI.Page
    'Inherits CMSBasePage

    Public ProjectTitle As String = ""
    Public ProjectSEOTitle As String
    Public ProjectSEODescription As String
    Public ProjectSEOKeywords As String

    Dim ProjectId As String
    Dim ProjectURLname As String = ""
    Dim ProjectRealName As String = ""
    Dim ProjectRealURL As String = ""
    Dim ProjectIsGood As Boolean = False

    Public SitePlanID As String

    Protected Sub property1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim pageURL As String = HttpContext.Current.Request.RawUrl
        pageURL = Replace(pageURL, "/", "")
        Dim p As String = pageURL
        Dim x as Integer = InStr(pageURL,"?")
        If x > 3 
        pageURL = pageURL.Substring(0, (x-1))
        End if

        If p.Contains("planimage") Then
        p = p.Substring(p.IndexOf("planimage")+10)
        x = InStr(p,"&")
        If x > 3 
        p = p.Substring(0, (x-1))
        End if

        p = Server.UrlDecode(p)
        Else
        p = "0"
        End if
        Dim pzI As Integer = Convert.ToInt32(p)

        If Not Request.QueryString("id") Is Nothing Then
            ProjectId = Request.QueryString("id").ToString
            If IsNumeric(ProjectId) = True Then
                Dim CheckProjectSql As String = "SELECT P.ProjectID, P.Name FROM IPM_PROJECT P WHERE available='Y' and P.ProjectID = " & ProjectId & ""
                Dim CheckProjectDT As New DataTable("CheckProjectDT")
                CheckProjectDT = DBFunctions.GetDataTable(CheckProjectSql)
                If CheckProjectDT.Rows.Count > 0 Then
                    ProjectId = CheckProjectDT.Rows(0)("ProjectID").ToString.Trim
                    ProjectRealName = CheckProjectDT.Rows(0)("Name").ToString.Trim
                    ProjectIsGood = True
                Else
                    Dim CheckProjectByUrlSql As String = ""
                    CheckProjectByUrlSql &= "SELECT IV.ProjectID, P.Name, F1.Item_Value AS SeoURL "
                    CheckProjectByUrlSql &= "FROM IPM_PROJECT_FIELD_VALUE IV "
                    CheckProjectByUrlSql &= "JOIN IPM_PROJECT P ON P.ProjectID = IV.ProjectID "
                    CheckProjectByUrlSql &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F1 on P.ProjectID = F1.ProjectID and F1.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PAGEURL' AND Active = 1) "
                    CheckProjectByUrlSql &= "WHERE P.available = 'Y' and IV.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PAGEURL') AND dbo.UDF_GenerateSlug(IV.Item_Value,'-') = '" & pageURL & "'"
                    Dim CheckProjectByUrlDT As New DataTable("CheckProjectByUrlDT")
                    CheckProjectByUrlDT = DBFunctions.GetDataTable(CheckProjectByUrlSql)
                    If CheckProjectByUrlDT.Rows.Count > 0 Then
                        ProjectId = CheckProjectByUrlDT.Rows(0)("ProjectID").ToString.Trim
                        ProjectRealName = CheckProjectByUrlDT.Rows(0)("Name").ToString.Trim
                        ProjectRealURL = CheckProjectByUrlDT.Rows(0)("SeoURL").ToString.Trim
                        ProjectIsGood = True
                    End If
                End If
                'if is not numeric
            Else
                Dim CheckProjectByUrlSql As String = ""
                CheckProjectByUrlSql &= "SELECT IV.ProjectID, P.Name, F1.Item_Value AS SeoURL "
                CheckProjectByUrlSql &= "FROM IPM_PROJECT_FIELD_VALUE IV "
                CheckProjectByUrlSql &= "JOIN IPM_PROJECT P ON P.ProjectID = IV.ProjectID "
                CheckProjectByUrlSql &= "LEFT JOIN IPM_PROJECT_FIELD_VALUE F1 on P.ProjectID = F1.ProjectID and F1.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PAGEURL' AND Active = 1) "
                CheckProjectByUrlSql &= "WHERE P.available = 'Y' and IV.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_PAGEURL') AND dbo.UDF_GenerateSlug(IV.Item_Value,'-') = '" & pageURL & "'"
                Dim CheckProjectByUrlDT As New DataTable("CheckProjectByUrlDT")
                CheckProjectByUrlDT = DBFunctions.GetDataTable(CheckProjectByUrlSql)
                If CheckProjectByUrlDT.Rows.Count > 0 Then
                    ProjectId = CheckProjectByUrlDT.Rows(0)("ProjectID").ToString.Trim
                    ProjectRealName = CheckProjectByUrlDT.Rows(0)("Name").ToString.Trim
                    ProjectRealURL = CheckProjectByUrlDT.Rows(0)("SeoURL").ToString.Trim
                    ProjectIsGood = True
                End If
            End If
        End If


        If ProjectIsGood = True Then
            Dim ProjectData As New ProjectStructure
            ProjectData = IDAMFunctions.GetProjectDetails(ProjectId)

            ProjectSEOTitle = ProjectData.ProjectSEOTitle
            ProjectSEODescription = ProjectData.ProjectSEODescription
            ProjectSEOKeywords = ProjectData.ProjectSEOKeyWords

            ProjectTitle_lbl.Text = ProjectData.ProjectTitle
            ProjectTitle = ProjectData.ProjectTitle
            ProjectAddress_lbl.Text = formatfunctions.AutoFormatText(ProjectData.ProjectAddress)
            ProjectDescription_lbl.Text = formatfunctions.AutoFormatText(ProjectData.ProjectDescription)
            If ProjectData.ProjectWebsite <> "" Then
                Dim ProjectWebsiteOUT As String = ""
                ProjectWebsiteOUT &= "<h5>PROPERTY WEBSITE</h5>"
                ProjectWebsiteOUT &= "<span class=""red-text"">"
                ProjectWebsiteOUT &= "<a href=""" & ProjectData.ProjectWebsite & """>" & Replace(ProjectData.ProjectWebsite, "http://", "") & "</a>"
                ProjectWebsiteOUT &= "</span>	"
                ProjectWebsite_lbl.Text = ProjectWebsiteOUT
            End If

            Dim SliderSQL As String = ""
            SliderSQL &= "Select Asset_Id "
            SliderSQL &= "from IPM_ASSET A "
            SliderSQL &= "where PDF = 0 AND Active = 1 AND Available = 'Y' and ProjectID = " & ProjectId & " "
            SliderSQL &= "and  A.Category_ID = (select Category_ID From IPM_ASSET_CATEGORY where Name = 'Property Images' AND available = 'Y' and active = 1 and ProjectID = " & ProjectId & ")  "
            Dim SliderDT As New DataTable("SliderDT")
            SliderDT = DBFunctions.GetDataTable(SliderSQL)

            Dim ImageOUT As String = ""
            If SliderDT.Rows.Count > 0 Then
                ImageOUT &= "<div class=""prop-slider"">"
                ImageOUT &= "<ul>"
                For R As Integer = 0 To SliderDT.Rows.Count - 1
                    ImageOUT &= "<li>"
                    ImageOUT &= "<img src=""/dynamic/image/week/asset/best/460x300/92/ffffff/Center/" & SliderDT.Rows(R).Item("Asset_Id").ToString.Trim & ".jpg"" alt="""" class=""propImg""/>"
                    ImageOUT &= "</li>"
                Next
                ImageOUT &= "</ul>"
                ImageOUT &= "</div>"
            Else
                ImageOUT &= "<img src=""" & "/dynamic/image/week/project/best/460x300/92/ffffff/Center/" & ProjectId & ".jpg" & """" & " alt="""" class=""propImg"" />"
            End If
            property_img_lbl.Text = ImageOUT


            Dim DemoOut As String = ""
            DemoOut &= "<h5>DEMOGRAPHICS</h5>"
            DemoOut &= "<table class=""demographics-table"">"
            DemoOut &= "<thead>"
            DemoOut &= "<tr>"
            DemoOut &= "<th class=""first"">Distance</th>"
            DemoOut &= "<th>" & ProjectData.ColH1 & "</th>"
            DemoOut &= "<th>" & ProjectData.ColH2 & "</th>"
            DemoOut &= "<th>" & ProjectData.ColH3 & "</th>"
            DemoOut &= "</tr>"
            DemoOut &= "</thead>"
            DemoOut &= "<tbody>"
            If ProjectData.RowH1 <> "" Then
                DemoOut &= "<tr>"
                DemoOut &= "<td class=""first"">" & ProjectData.RowH1 & "</td>"
                DemoOut &= "<td>" & ProjectData.Row1Col1 & "</td>"
                DemoOut &= "<td>" & ProjectData.Row1Col2 & "</td>"
                DemoOut &= "<td>" & ProjectData.Row1Col3 & "</td>"
                DemoOut &= "</tr>"
            End If
            If ProjectData.RowH2 <> "" Then
                DemoOut &= "<tr>"
                DemoOut &= "<td class=""first"">" & ProjectData.RowH2 & "</td>"
                DemoOut &= "<td>" & ProjectData.Row2Col1 & "</td>"
                DemoOut &= "<td>" & ProjectData.Row2Col2 & "</td>"
                DemoOut &= "<td>" & ProjectData.Row2Col3 & "</td>"
                DemoOut &= "</tr>"
            End If
            If ProjectData.RowH3 <> "" Then
                DemoOut &= "<tr>"
                DemoOut &= "<td class=""first"">" & ProjectData.RowH3 & "</td>"
                DemoOut &= "<td>" & ProjectData.Row3Col1 & "</td>"
                DemoOut &= "<td>" & ProjectData.Row3Col2 & "</td>"
                DemoOut &= "<td>" & ProjectData.Row3Col3 & "</td>"
                DemoOut &= "</tr>"
            End If
            If ProjectData.RowH4 <> "" Then
                DemoOut &= "<tr>"
                DemoOut &= "<td class=""first"">" & ProjectData.RowH4 & "</td>"
                DemoOut &= "<td>" & ProjectData.Row4Col1 & "</td>"
                DemoOut &= "<td>" & ProjectData.Row4Col2 & "</td>"
                DemoOut &= "<td>" & ProjectData.Row4Col3 & "</td>"
                DemoOut &= "</tr>"
            End If
            DemoOut &= "</tbody>"
            DemoOut &= "</table>"

            demographics_lbl.Text = DemoOut


            Leasing_Agents_lbl.Text = IDAMfunctions.PrintProjectContacts(ProjectId, 0)
            Anchors_lbl.Text = IDAMFunctions.PrintSpaces(ProjectId, 0, False, True, "Property Tenants Logo")

            '---start services---'
            Dim ServicesTypes As New List(Of String)
            ServicesTypes = ProjectData.PropertyService
            Dim ServicesTypeOut As String = ""
            If ServicesTypes.Count > 0 Then
                ServicesTypeOut &= "<h5>SERVICES</h5>"
                ServicesTypeOut &= "<span class=""red-text"">"
                For Each ServiceType As String In ServicesTypes
                    ServicesTypeOut &= "<a href=""#retail-solutions"">" & ServiceType & "</a><br/>"
                Next
                ServicesTypeOut &= "</span>"
                services_lbl.Text = ServicesTypeOut
            End If
            '---end services---'

            related_news_lbl.Text = IDAMfunctions.PrintProjectNews(ProjectId, 0)
            case_studies_lbl.Text = IDAMFunctions.PrintProjectCaseStudies(ProjectId, 0, False, True)
            If ProjectData.AerialId <> "" Then
                aerial_map_tab_lbl.Text = "<li><a rel=""market-aerial"">Market Aerial</a></li>"
                Dim MarketAerialOUT As String = ""
                MarketAerialOUT &= "<div id=""market-aerial"">"
                MarketAerialOUT &= "<div id=""aerial-map"">"
                MarketAerialOUT &= "<div id=""pan""><img src=""" & "/dynamic/image/week/asset/fit/5000x5000/90/000000/Center/" & ProjectData.AerialId & ".jpg" & """" & " alt=""""/></div>"
                MarketAerialOUT &= "<p class=""zoom""><a class=""zoom zoomIn""></a><a class=""zoom zoomOut""></a><a class=""z""></a></p><p class=""fitMe""><a class=""fit""></a></p>"
                MarketAerialOUT &= "</div>"
                MarketAerialOUT &= "</div>"
                aerial_map_lbl.Text = MarketAerialOUT
            End If

            If ProjectData.AreaMap <> "" Then
                If CMSFunctions.IsValidCoords(ProjectData.AreaMap) = True Then
                    area_map_tab_lbl.Text = "<li><a rel=""area-map"">Area Map</a></li>"
                    Dim AreaMapOUT As String = ""
                    AreaMapOUT &= "<div id=""area-map"" class=""gmap3"">"
                    AreaMapOUT &= "<div class=""locationMap"">" & ProjectData.AreaMap & "</div>"
                    AreaMapOUT &= "<div class=""locationMapMarkers"">"
                    AreaMapOUT &= "<a href=""https://maps.google.com/?q=" & FormatFunctions.AutoFormatText(ProjectData.AreaMap) & "&#38;z=17&#38;type=map""><em>" & FormatFunctions.AutoFormatText(ProjectData.AreaMap) & "</em><img src=""assets/images/pins/pin.png"" alt="""" /></a>"
                    AreaMapOUT &= "</div>"
                    AreaMapOUT &= "</div>"
                    area_map_lbl.Text = AreaMapOUT
                End If
            End If

            'If ProjectData.SiteMapId <> "" Then
            site_plan_tab_lbl.Text = "<li><a rel=""site-plan"">Site Plan</a></li>"
            Dim SitePlanOUT As String = ""

            If pzI > 2 Then
                SitePlanOUT &= "<img src=""" & "/dynamic/image/week/asset/liquid/2000x/90/ffffff/NorthWest/" & pzI & ".gif" & """" & " alt="""" usemap=""#site-plan-img""/>"
            Else
                SitePlanOUT &= "<img longdesc=""/dynamic/image/week/asset/liquid/2000x/90/ffffff/NorthWest/"" src=""assets/images/clear.gif"" alt="""" class=""map-img"" usemap=""#site-plan-img""/>"
            End If

            site_plan_lbl.Text = SitePlanOUT

            If pzI > 0 Then
                Dim MapOut As String = ""
                Dim CoordinatesSQL As String = "select * from IPM_ASSET_FIELD_VALUE where Item_ID = 2420047 AND ASSET_ID = " & pzI & ""
                Dim CoordinatesDT As New DataTable
                CoordinatesDT = DBFunctions.GetDataTable(CoordinatesSQL)
                Dim SiteMapCoordinates As String = ""
                If CoordinatesDT.Rows.Count > 0 Then
                    SiteMapCoordinates = CoordinatesDT.Rows(0).Item("Item_Value").ToString.ToString
                End If

                If SiteMapCoordinates <> "" Then
                    MapOut &= "<map name=""site-plan-img"" id=""site-map-img"">"
                    MapOut &= SiteMapCoordinates
                    MapOut &= "</map>"
                    site_plan_coordinates_lbl.Text = MapOut
                Else
                    MapOut &= "<map name=""site-plan-img"" id=""site-map-img""><area shape=""rect"" alt="""" coords=""10,10,120,120"" /></map>"
                End If
                site_plan_coordinates_lbl.Text = MapOut
            End If

            If pzI > 0 Then
                SitePlanID = pzI
            Else
                SitePlanID = ProjectData.SiteMapId
            End If

            spaces_select_lbl.Text = IDAMFunctions.PrintSpaces(ProjectId, 0, False, True, "Site Plan Spaces")

            'End If

            Dim PropertyDownloadOUT As String = ""
            Dim BrochureID As String

            If ProjectData.BrochureId.ToString.Trim = "" Then
                BrochureID = ProjectId
            Else
                BrochureID = ProjectData.BrochureId.ToString.Trim
            End If

            Dim DownloadsSQL As String = ""
            DownloadsSQL &= ""
            DownloadsSQL &= "SELECT "
            DownloadsSQL &= "A.Asset_ID ,"
            DownloadsSQL &= "A.Name "
            DownloadsSQL &= "FROM IPM_ASSET A "
            DownloadsSQL &= "WHERE A.Category_ID = (select Category_ID From IPM_ASSET_CATEGORY where Name = 'Downloads' AND ProjectID = " & ProjectId & " AND Active = 1 AND Available = 'Y') "
            DownloadsSQL &= "AND Active = 1 AND Available = 'Y' AND Version_ID = 0 "
            Dim DownloadsDT As New DataTable
            DownloadsDT = DBFunctions.GetDataTable(DownloadsSQL)
            Dim DownloadsOUT As String = ""

            'Dim sortedDown as new Li
            Dim SortedDownloads1 As New List(Of SortedDownloads)


            If ProjectData.DemographicId <> "" Or ProjectData.SiteMapId <> "" Or BrochureID <> "" Or DownloadsDT.Rows.Count > 0 Then
                PropertyDownloadOUT &= "<div class=""propDownloads"">"
                PropertyDownloadOUT &= "<a>Downloads</a>"

                PropertyDownloadOUT &= "<p>"

                Dim BrochureDownload As New SortedDownloads
                BrochureDownload.Name = "Brochure"

                If ProjectData.BrochureId <> "" Then
                    BrochureDownload.Id = BrochureID
                    BrochureDownload.Url = "/dynamic/document/week/asset/download/" & BrochureID & "/" & CMSFunctions.FormatFriendlyUrl(BrochureID, "_") & "_brochure.pdf"

                    'PropertyDownloadOUT &= "<a href=""/dynamic/document/week/asset/download/" & BrochureID & "/" & CMSFunctions.FormatFriendlyUrl(BrochureID, "_") & "_brochure.pdf"">Brochure</a>"
                Else
                    BrochureDownload.Id = BrochureID
                    BrochureDownload.Url = "/pdf/" & ProjectId & "/" & CMSFunctions.FormatFriendlyUrl(ProjectData.ProjectTitle, "-") & ".pdf"

                    'PropertyDownloadOUT &= "<a href=""/pdf/" & ProjectId & "/" & CMSFunctions.FormatFriendlyUrl(ProjectData.ProjectTitle, "-") & ".pdf"">Brochure</a>"
                End If
                SortedDownloads1.Add(BrochureDownload)

            
                If ProjectData.DemographicId <> "" Then
                    Dim DemographicDownload As New SortedDownloads
                    DemographicDownload.Name = "Demographic"
                    DemographicDownload.Id = ProjectData.DemographicId.ToString.Trim
                    DemographicDownload.Url = "/dynamic/document/week/asset/download/" & ProjectData.DemographicId.ToString.Trim & "/" & CMSFunctions.FormatFriendlyUrl(ProjectData.ProjectTitle, "_") & "_demographic.pdf"
                    SortedDownloads1.Add(DemographicDownload)

                    'PropertyDownloadOUT &= "<a href=""/dynamic/document/week/asset/download/" & ProjectData.DemographicId.ToString.Trim & "/" & CMSFunctions.FormatFriendlyUrl(ProjectData.ProjectTitle, "_") & "_demographic.pdf"">Demographic</a>"
                End If

                If ProjectData.SiteMapPDF <> "" Then
                    Dim SiteMapDownload As New SortedDownloads
                    SiteMapDownload.Name = "Site Plan"
                    SiteMapDownload.Id = ProjectData.SiteMapPDF.ToString.Trim
                    SiteMapDownload.Url = "/dynamic/document/week/asset/download/" & ProjectData.SiteMapPDF.ToString.Trim & "/" & CMSFunctions.FormatFriendlyUrl(ProjectData.ProjectTitle, "_") & "_site_map.pdf"
                    SortedDownloads1.Add(SiteMapDownload)

                    'PropertyDownloadOUT &= "<a href=""/dynamic/document/week/asset/download/" & ProjectData.SiteMapPDF.ToString.Trim & "/" & CMSFunctions.FormatFriendlyUrl(ProjectData.ProjectTitle, "_") & "_site_map.pdf"">Site Plan</a>"
                End If


                If DownloadsDT.Rows.Count > 0 Then
                    For R As Integer = 0 To DownloadsDT.Rows.Count - 1
                        Dim OtherDownload As New SortedDownloads
                        OtherDownload.Name = DownloadsDT.Rows(R).Item("Name").ToString.Trim
                        OtherDownload.Id = DownloadsDT.Rows(R).Item("Asset_ID").ToString.Trim
                        OtherDownload.Url = "/dynamic/document/week/asset/download/" & DownloadsDT.Rows(R).Item("Asset_ID").ToString.Trim & "/" & CMSFunctions.FormatFriendlyUrl(DownloadsDT.Rows(R).Item("Name").ToString.Trim, "_") & ".pdf"
                        SortedDownloads1.Add(OtherDownload)

                        'PropertyDownloadOUT &= "<a href=""/dynamic/document/week/asset/download/" & DownloadsDT.Rows(R).Item("Asset_ID").ToString.Trim & "/" & CMSFunctions.FormatFriendlyUrl(DownloadsDT.Rows(R).Item("Name").ToString.Trim, "_") & ".pdf"">" & DownloadsDT.Rows(R).Item("Name").ToString.Trim & "</a>"
                    Next
                End If

                SortedDownloads1 = SortedDownloads1.ToList.OrderBy(Function(a) a.Name).ToList()
                For i As Integer = 0 To SortedDownloads1.Count - 1
                    PropertyDownloadOUT &= "<a href=""" & SortedDownloads1(i).Url & """>" & SortedDownloads1(i).Name & "</a>"
                Next

                PropertyDownloadOUT &= "</p>"
                PropertyDownloadOUT &= "</div>"
            End If

         

            property_downloads_lbl.Text = PropertyDownloadOUT


            'If ProjectData.SiteMapId <> "" Then
            available_spaces_lbl.Text = IDAMFunctions.PrintSpaces(ProjectId, 0, False, True, "Property Available Spaces")
            'End If


            Dim QuerySQL As String = "SELECT TOP 1 Asset_ID FROM IPM_ASSET A WHERE A.ProjectID = " & ProjectId & " AND A.Category_ID = (select Category_ID From IPM_ASSET_CATEGORY where Name = 'Property Logo' AND ProjectID =" & ProjectId & " AND AVAILABLE= 'Y' AND ACTIVE = 1) AND Active = 1 And Available = 'Y' "
            Dim AssetID As Integer = 0
            AssetID = DBFunctions.ScalarSql(QuerySQL)
            If AssetID <> 0 Then
                ProjectLogo_lbl.Text = "<img src=""" & "/dynamic/image/week/asset/fit/200x200/92/ffffff/North/" & AssetID & ".jpg" & """" & " alt=""""/>"
            End If


            Dim MultipleSitePlansSQL As String = ""
            MultipleSitePlansSQL &= ""
            MultipleSitePlansSQL &= "SELECT "
            MultipleSitePlansSQL &= "A.Asset_ID ,"
            MultipleSitePlansSQL &= "A.Name "
            MultipleSitePlansSQL &= "FROM IPM_ASSET A "
            MultipleSitePlansSQL &= "LEFT JOIN IPM_ASSET_FIELD_VALUE Sort ON Sort.ASSET_ID = A.Asset_ID AND Sort.Item_ID = (SELECT Item_ID From IPM_ASSET_FIELD_DESC where Item_Tag = 'IDAM_SITEPLANORDER' AND Active = 1 AND Available = 'Y') "
            MultipleSitePlansSQL &= "WHERE A.Category_ID = (select Category_ID From IPM_ASSET_CATEGORY where Name = 'Site Plan' AND ProjectID = " & ProjectId & " AND Active = 1 AND Available = 'Y') "
            MultipleSitePlansSQL &= "AND PDF = 0 AND Active = 1 AND Available = 'Y' AND Version_ID = 0 "
            MultipleSitePlansSQL &= "order by CAST(isnull(Sort.Item_Value, 99999) as integer) asc "
            Dim MultipleSitePlansDT As New DataTable
            MultipleSitePlansDT = DBFunctions.GetDataTable(MultipleSitePlansSQL)
            Dim MultipleSitePlansOUT As String = ""
            If MultipleSitePlansDT.Rows.Count > 0 Then
                MultipleSitePlansOUT &= "<div class=""select-site-plan"">" & vbNewLine
                For i As Integer = 0 To MultipleSitePlansDT.Rows.Count - 1
                    MultipleSitePlansOUT &= "<a rel=""" & MultipleSitePlansDT.Rows(i).Item("Asset_ID").ToString.Trim & """>" & MultipleSitePlansDT.Rows(i).Item("Name").ToString.Trim.Split(".")(0) & "</a>" & vbNewLine
                Next
                MultipleSitePlansOUT &= "</div>" & vbNewLine
                MultipleSitePlans_lbl.Text = MultipleSitePlansOUT
            End If

            Dim AllUniquePlansSQL As String = ""
            AllUniquePlansSQL &= ""
            AllUniquePlansSQL &= "SELECT Description FROM IPM_SPACE S "
            AllUniquePlansSQL &= "WHERE 1 = 1 "
            AllUniquePlansSQL &= "AND S.Active = 1 AND S.Available = 'Y' "
            AllUniquePlansSQL &= "AND S.ProjectID =  " & ProjectId & " "
            AllUniquePlansSQL &= "GROUP BY Description "
            Dim SitePlansDT As New DataTable
            SitePlansDT = DBFunctions.GetDataTable(AllUniquePlansSQL)

            Dim SitePlan_TenantsDT As New DataTable
            SitePlan_TenantsDT = IDAMFunctions.GetSpaces(ProjectId, 0, False, True, "Site Plan Tenants")
            Dim SitePlan_TenantsLogoDT As New DataTable
            SitePlan_TenantsLogoDT = IDAMFunctions.GetSpaces(ProjectId, 0, False, True, "Property Tenants Logo Site Plan")
            Dim SitePlan_AvailableSpaceDT As New DataTable
            SitePlan_AvailableSpaceDT = IDAMFunctions.GetSpaces(ProjectId, 0, False, False, "Site Plan Available Spaces")

            Dim PrintOUT As String = ""
            Dim SitePlan_TenantsOUT As String = ""
            Dim SitePlan_TenantsLogoOUT As String = ""
            Dim SitePlan_AvailableSpaceOUT As String = ""
            If SitePlansDT.Rows.Count > 0 Then 'Site Plans
                For SP As Integer = 0 To SitePlansDT.Rows.Count - 1
                    SitePlan_TenantsOUT = ""
                    SitePlan_TenantsLogoOUT = ""
                    SitePlan_AvailableSpaceOUT = ""
                    PrintOUT &= "<div class=""two-columns siteSpace""><ins>" & SitePlansDT.Rows(SP).Item("Description") & "</ins>" & vbNewLine
                    PrintOUT &= "<div class=""left"">" & vbNewLine
                    'START Tenants
                    SitePlan_TenantsDT.DefaultView.RowFilter = "Description = '" & SitePlansDT.Rows(SP).Item("Description") & "'"
                    Dim GroupedSitePlan_TenantsDT As New DataTable
                    GroupedSitePlan_TenantsDT = SitePlan_TenantsDT.DefaultView.ToTable
                    If GroupedSitePlan_TenantsDT.Rows.Count > 0 Then
                        SitePlan_TenantsOUT &= "<h2>TENANTS</h2>" & vbNewLine
                        SitePlan_TenantsOUT &= "<ul class=""left list tenants"">" & vbNewLine
                        For GT As Integer = 0 To GroupedSitePlan_TenantsDT.Rows.Count - 1
                            SitePlan_TenantsOUT &= "<li>" & vbNewLine
                            SitePlan_TenantsOUT &= "<a class=""spaceItem"" rel=""" & GroupedSitePlan_TenantsDT.Rows(GT)("Space_ID").ToString.Trim & """>" & vbNewLine
                            SitePlan_TenantsOUT &= "<span>" & GroupedSitePlan_TenantsDT.Rows(GT)("Suite").ToString.Trim & "</span><var>" & FormatFunctions.AutoFormatText(GroupedSitePlan_TenantsDT.Rows(GT)("ShortName").ToString.Trim) & "</var>" & vbNewLine
                            SitePlan_TenantsOUT &= "<em></em></a>" & vbNewLine
                            SitePlan_TenantsOUT &= "</li>" & vbNewLine
                        Next ' Next Tenant
                        SitePlan_TenantsOUT &= "</ul>	" & vbNewLine
                    End If ' If Tenants Exist
                    'END Tenants


                    'START Tenants Logos
                    SitePlan_TenantsLogoDT.DefaultView.RowFilter = "Description = '" & SitePlansDT.Rows(SP).Item("Description") & "'"
                    Dim GroupedSitePlan_TenantLogossDT As New DataTable
                    GroupedSitePlan_TenantLogossDT = SitePlan_TenantsLogoDT.DefaultView.ToTable
                    If GroupedSitePlan_TenantLogossDT.Rows.Count > 0 Then
                        SitePlan_TenantsLogoOUT &= "<div class=""anchorLogos"">" & vbNewLine
                        For GT As Integer = 0 To GroupedSitePlan_TenantLogossDT.Rows.Count - 1
                            SitePlan_TenantsLogoOUT &= "<span class=""property-logo-link"">" & vbNewLine
                            SitePlan_TenantsLogoOUT &= "<a class=""spaceItem"" rel=""" & GroupedSitePlan_TenantLogossDT.Rows(GT)("Space_ID").ToString.Trim & """>" & vbNewLine
                            SitePlan_TenantsLogoOUT &= "<img src=""/dynamic/image/always/asset/fit/140x110/85/ffffff/North/" & GroupedSitePlan_TenantLogossDT.Rows(GT)("Asset_ID").ToString.Trim & ".png"" alt=""""/>" & vbNewLine
                            SitePlan_TenantsLogoOUT &= "<em></em></a>" & vbNewLine
                            SitePlan_TenantsLogoOUT &= "</span>" & vbNewLine
                        Next ' Next Tenant Logos
                        SitePlan_TenantsLogoOUT &= "</div>" & vbNewLine
                    End If ' If Tenants Logos Exist
                    'END Tenants Logos


                    'START Available Spaces
                    SitePlan_AvailableSpaceDT.DefaultView.RowFilter = "Description = '" & SitePlansDT.Rows(SP).Item("Description") & "'"
                    Dim GroupedSitePlan_AvailableSpaceDT As New DataTable
                    GroupedSitePlan_AvailableSpaceDT = SitePlan_AvailableSpaceDT.DefaultView.ToTable

                    If GroupedSitePlan_AvailableSpaceDT.Rows.Count > 0 Then
                        SitePlan_AvailableSpaceOUT &= "<h2>AVAILABLE SPACES</h2>" & vbNewLine
                        SitePlan_AvailableSpaceOUT &= "<ul class=""left list available"">" & vbNewLine
                        For R As Integer = 0 To GroupedSitePlan_AvailableSpaceDT.Rows.Count - 1
                            Dim SfOut As String = ""
                            If GroupedSitePlan_AvailableSpaceDT.Rows(R).Item("SF").ToString <> "" Then
                                Dim SQLDec As Decimal = GroupedSitePlan_AvailableSpaceDT.Rows(R).Item("SF")
                                SfOut = SQLDec.ToString("#,0") & " SF"
                            End If
                            SitePlan_AvailableSpaceOUT &= "<li>" & vbNewLine
                            SitePlan_AvailableSpaceOUT &= "<a class=""spaceItem"" rel=""" & GroupedSitePlan_AvailableSpaceDT.Rows(R)("Space_ID").ToString.Trim & """>" & vbNewLine
                            SitePlan_AvailableSpaceOUT &= "<span>" & FormatFunctions.AutoFormatText(GroupedSitePlan_AvailableSpaceDT.Rows(R)("Suite").ToString.Trim) & "</span><var>" & SfOut & "</var>" & vbNewLine
                            SitePlan_AvailableSpaceOUT &= "<em></em></a>" & vbNewLine
                            SitePlan_AvailableSpaceOUT &= "</li>" & vbNewLine
                        Next ' Next Vailable Space
                        SitePlan_AvailableSpaceOUT &= "</ul>" & vbNewLine
                    End If ' If Available Spaces Exist
                    'END Available Spaces

                    PrintOUT &= SitePlan_TenantsOUT & vbNewLine
                    PrintOUT &= SitePlan_TenantsLogoOUT & vbNewLine
                    PrintOUT &= "</div>" & vbNewLine
                    PrintOUT &= "<div class=""right"">" & vbNewLine

                    PrintOUT &= SitePlan_AvailableSpaceOUT & vbNewLine
                    PrintOUT &= "</div>" & vbNewLine
                    PrintOUT &= "</div>" & vbNewLine
                    PrintOUT &= "<div class=""clear""></div>" & vbNewLine

                Next ' Next Site Plan

                allsiteplandata_lbl.Text = PrintOUT

            End If ' If Site Plans Exist

        Else
            Response.Status = "307 Temporary Redirect"
            Response.AddHeader("Location", "/404")
        End If
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        If ProjectIsGood = True Then
            Dim stringWriter As New System.IO.StringWriter()
            Dim htmlWriter As New HtmlTextWriter(stringWriter)
            MyBase.Render(htmlWriter)
            Dim html As String = stringWriter.ToString()
            html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
            writer.Flush()
            writer.Write(html)
            If Master.OriginalUrlPage.Contains("?") = False Then
                If Master.UrlPage.ToLower = "property-" & ProjectId & "-" & CMSFunctions.FormatFriendlyUrl(ProjectRealName, "-").ToLower Or Master.UrlPage.ToLower = ProjectRealURL Then
                    CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
                End If
            Else
                CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
            End If
        End If

    End Sub


End Class


Public Class SortedDownloads
    Private IdValue As Integer
    Private nameValue As String
    Private urlValue As String

    Public Property Id() As Integer
        Get
            Return IdValue
        End Get
        Set(ByVal value As Integer)
            IdValue = value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return nameValue
        End Get
        Set(ByVal value As String)
            nameValue = value
        End Set
    End Property
    Public Property Url() As String
        Get
            Return urlValue
        End Get
        Set(ByVal value As String)
            urlValue = value
        End Set
    End Property
End Class
