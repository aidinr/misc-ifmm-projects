﻿<%@ Page Title="" Language="VB" MasterPageFile="~/HRMaster1.master" AutoEventWireup="false" CodeFile="urban.aspx.vb" Inherits="urban" %>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

		<!-- Page Content START -->
		<div class="page-content">
			<div class="top-section fluid-padding">
				<h1 class="cms cmsType_TextSingle cmsName_Urban_Top_Heading">Services</h1>
				<h2 class="cms cmsType_TextSingle cmsName_Urban_Heading">Urban Success</h2>
				<h3 class="redtext cms cmsType_TextSingle cmsName_Urban_Heading_Red_Text">Urban</h3>
			</div>			
			<div class="main fluid-padding investment-sales-page">
				<div class="text left half-width mr20">
					<p class="cms cmsType_TextMulti cmsName_Urban_Intro"></p>
					<h3 class="cms cmsType_TextSingle cmsName_Urban_Intro_Heading">Proven marketing process.</h3>
					<p class="cms cmsType_TextMulti cmsName_Urban_Bottom_Intro"></p>
				</div>
				<a href="#" class="purple-link cms cmsType_Link cmsName_Urban_Website"></a>
				<div class="clear mb20">&nbsp;</div>
				<ul class="cmsGroup cmsName_Urban_Info">
				<!--
					<li class="cmsGroupItem">
						<strong class="cms cmsType_TextSingle cmsName_Title">varius Scelerisque Vestibulum</strong>
						<p class="cms cmsType_TextMulti cmsName_Content">Nam ultricies dolor eu velit varius scelerisque. Vestibulum in lacus in felis pretium feugiat non sed elit. Duis pretium, urna sed pellentesque tincidunt, neque massa imperdiet nisi, sed cursus leo magna.</p>
					</li>
				-->	
					


                    <%
                           For i As Integer = 1 To 20 - 1
                                 If Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Urban_Info_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
                            %>

                    <li class="cmsGroupItem">
						<strong class="cms cmsType_TextSingle cmsName_Urban_Info_Item_<%=i %>_Title">varius Scelerisque Vestibulum</strong>
						<p class="cms cmsType_TextMulti cmsName_Urban_Info_Item_<%=i %>_Content">Nam ultricies dolor eu velit varius scelerisque. Vestibulum in lacus in felis pretium feugiat non sed elit. Duis pretium, urna sed pellentesque tincidunt, neque massa imperdiet nisi, sed cursus leo magna.</p>
					</li>

                              <%
                              End If
                          Next
                          %>

				</ul>				
			</div>	


<div class="urban fluid-padding">
<div class="latlong cms cmsType_TextSingle cmsName_Urban_Map"></div>
<div class="zoom cms cmsType_TextSingle cmsName_Urban_Map_Zoom_Number"></div>


				<div id="urbanLocations" class="cmsGroup cmsName_Urban_Map">
					<!--
					<p class="cmsGroupItem">
						<img src="assets/images/urbanlogo.gif" class="img cms location cmsType_TextSingle cmsName_Logo" alt="" />
						<em class="cms location cmsType_TextSingle cmsName_Location">38.900, -77.333</em>
					</p>
					-->
                    <%
                           For i As Integer = 1 To 90 - 1
                                 If Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Urban_Map_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
                            %>
                            
                    <p class="cmsGroupItem">
					<img src="cms/data/Sized/liquid/80x/75/ffffff/North/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Urban_Map_Item_" & i & "_Logo")%>" class="img cmsFill_Static cms cmsType_Image location cmsType_TextSingle cmsName_Urban_Map_Item_<%=i%>_Logo" alt="" title="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Urban_Map_Item_" & i & "_Logo")%>" />
					<em class="cms location cmsType_TextSingle cmsName_Urban_Map_Item_<%=i%>_Location">38.888, -77.499</em>
					</p>
                              <%
                              End If
                          Next
                          %>



				</div>
</div>

<div id="urban-map" class="gmap3"></div>

		</div>
		<!-- Page Content END -->	
</asp:Content>
