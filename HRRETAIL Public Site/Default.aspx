﻿<%@ Page Debug="true" Title="" Language="VB" MasterPageFile="~/HRMaster1.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>
<%@ MasterType  virtualPath="~/HRMaster1.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<!-- Page Content START -->
		<div class="page-content">
		<div id="homePage">

			<!-- Main Slider START -->
			<div class="main-slider">

            <asp:Literal runat="server" ID="carousel_images"/>
            				
				<a class="left-arrow"> </a>
				<a class="right-arrow"></a>
			</div>
			<!-- Main Slider END -->
			
			<!-- Clients logos START -->
			<div class="featured-row fluid-padding imgG">
				<div class="controls left">
					<h2>Retailers &amp; Restaurants</h2>
					<p>H&amp;R Retail creates retail success.</p>
					<span><a href="retailers-and-restaurants">View all retailers</a></span>
					<button type="button" class="hsl left-1">&nbsp;</button>
					<button type="button" class="hsr right-1">&nbsp;</button>
				</div>
				<div class="horizontal-slider rr">
					<asp:Literal runat="server" ID="tenants_lis1"/>
					<br class="clear"/>
				</div>
			</div>
			<!-- Clients logos END -->
			

			<!-- Case studies START -->

			<!-- Case studies START -->
			<div class="featured-row fluid-padding case-studies">
				<div class="controls left">
					<h2>case studies</h2>
					<p>Take a look at our success stories.</p>
					<span><a href="case-studies">View all case studies</a></span>
					<button type="button" class="hsl left-2"></button>
					<button type="button" class="hsr right-2"></button>
				</div>
				<div class="horizontal-slider">
				<asp:Literal runat="server" ID="case_studies"/>
				<br class="clear"/>
				</div>
				<br class="clear"/>
			</div>
			<!-- Case studies END -->		
			
			<!-- News START -->
			<div class="featured-row fluid-padding in-the-news">
				<div class="controls left">
					<h2>news &amp; daily clips</h2>
					<p>H&amp;R Retail in the news</p>
					<span><a href="news">View all news</a></span>
					<button type="button" class="hsl left-3">&nbsp;</button>
					<button type="button" class="hsr right-3">&nbsp;</button>
				</div>
				<div class="horizontal-slider news">

                <asp:Literal runat="server" ID="news_slider1"/>
                				
					<br class="clear"/>
				</div>
			</div>
			<!-- News END -->
		</div>
		</div>
		<!-- Page Content END -->	

<div class="unique code">
</div>

</asp:Content>
