﻿Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports System.Threading
Imports System.Net
Imports System.Collections.Generic
Imports System.Drawing
Partial Class siteplan
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ReadCookie()
        Response.Clear()
        If Not Request.QueryString("id") Is Nothing Then
            Dim SitePlanID As String = Request.QueryString("id")
            If IsNumeric(SitePlanID) Then
                If Not Request.Form("SiteMapData") Is Nothing Then
                    Dim SiteMapCoodrinates As String = ""
                    SiteMapCoodrinates = Request.Form("SiteMapData").ToString
                    If SiteMapCoodrinates <> "" Then
                        Dim DeleteString As String = "delete from IPM_ASSET_FIELD_VALUE where ASSET_ID = " & SitePlanID & " and Item_ID = 2420047"
                        DBFunctions.NonQuerySql(DeleteString)
                        Dim InsertString As String = "insert into IPM_ASSET_FIELD_VALUE values(" & SitePlanID & ", 2420047, '" & SiteMapCoodrinates & "')"
                        DBFunctions.NonQuerySql(InsertString)

                        'Dim SQLString As String = ""
                        'SQLString = "UPDATE IPM_ASSET SET Description = '" & SiteMapCoodrinates & "' WHERE Asset_ID = " & SitePlanID & ""
                        'DBFunctions.NonQuerySql(SQLString)

                        If (Not IsNothing(Request.UrlReferrer)) Then

                          'Response.Write(Request.UrlReferrer.GetLeftPart(UriPartial.Path) + "?" + DateTime.Now.ToString("yyyyhhmmssfff") )
                          Response.Status = "302 Found"
                          Response.AddHeader("Location", Request.UrlReferrer.GetLeftPart(UriPartial.Path) + "?t=" + DateTime.Now.ToString("yyyyhhmmssfff"))

                        Else
                          Response.Write("<html><head><title></title><script type=""text/javascript"">window.history.go(-1)</script></head><body>Saved</body></html>")
                        End If

                    Else
                        Response.Write("Error")
                    End If
                End If
            End If
        End If
        Response.Flush()
        Response.End()
    End Sub

    Protected Sub ReadCookie() 'Get the cookie name the user entered
        'Grab the cookie
        Dim cookie As HttpCookie = Request.Cookies("SiteEdit")
        'Check to make sure the cookie exists
        If cookie Is Nothing Then
            Response.Redirect(Server.MapPath("~") & "cms\assets\login.html")
        Else
            'check the cookie values
            Dim CookieValue As String = cookie.Value
            Dim SessionFile As String = Server.MapPath("~") & "cms\data\Users\" & CookieValue.Split("/")(0) & "\" & CookieValue.Split("/")(1) & ".dat"
            If (Not File.Exists(SessionFile)) Then
                Response.Redirect(Server.MapPath("~") & "cms\assets\login.html")
            Else
                'Do work
            End If
        End If
    End Sub

End Class
