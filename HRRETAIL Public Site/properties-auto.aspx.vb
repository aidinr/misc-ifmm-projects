﻿
Partial Class properties_auto
    Inherits System.Web.UI.Page

    Protected Sub properties_auto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim SearchPhrase As String = ""
        Dim SearchWords() As String
        If Not Request.QueryString("q") Is Nothing Then
            SearchPhrase = Request.QueryString("q").Trim
            SearchWords = SearchPhrase.Replace("  ", " ").Split(" ")
        End If
        Dim ProjectFilters As New ProjectFiltersStructure
        ProjectFilters.SearchPhrase = SearchPhrase
        Dim PropertySearchOUT As String = ""
        PropertySearchOUT = IDAMFunctions.PrintProjects("2409776", 12, 1, "Properties Search", ProjectFilters)
        Response.Clear()
        Response.Write(PropertySearchOUT)
        Response.Flush()
    End Sub
End Class
