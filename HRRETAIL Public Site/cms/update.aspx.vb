Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports System.Threading
Imports System.Net
Imports System.Collections.Generic
Imports System.Drawing
Partial Class _update
    Inherits System.Web.UI.Page

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ReadCookie()

        Dim returnText As String = "<html><head><title></title><script type=""text/javascript"">setTimeout(""parent.parent.cmsModalUpdated()"",110)</script></head><body></body></html>"
        Response.Clear()
        Dim value As String = ""
        Dim filepath As String = Server.MapPath("~") & "cms\data\"

        Dim cmsType As String = ""
        If Not Request.Form("cmsType") Is Nothing Then
            cmsType = Request.Form("cmsType").ToUpper
        End If

        Select Case cmsType
            Case "METADATA"
                filepath &= "Metadata\"
                Dim cmsPage As String = ""
                If Not Request.Form("cmsPage") Is Nothing Then
                    cmsPage = Request.Form("cmsPage").ToUpper
                End If


                Dim GoogleAnalyticsAccount As String = ""
                If Not Request.Form("GoogleAnalyticsAccount") Is Nothing Then
                    If Request.Form("GoogleAnalyticsAccount").ToString <> "" Then
                        GoogleAnalyticsAccount = Request.Form("GoogleAnalyticsAccount")
                    End If
                End If
                If GoogleAnalyticsAccount <> "" Then
                    If Not Directory.Exists(filepath) Then
                        Try
                            Directory.CreateDirectory(filepath)
                        Catch ex As Exception
                        End Try
                    End If
                    Dim thefile As String = filepath & "GoogleAnalytics.txt"
                    Dim fileinfo As FileInfo = New FileInfo(thefile)
                    If fileinfo.Exists Then
                        fileinfo.Delete()
                    End If
                    Dim fs As FileStream = Nothing
                    If (Not File.Exists(thefile)) Then
                        fs = File.Create(thefile)
                        Using fs
                        End Using
                        If File.Exists(thefile) Then
                            Using sw As StreamWriter = New StreamWriter(thefile)
                                sw.Write(GoogleAnalyticsAccount)
                            End Using
                        End If
                    End If
                End If


                For i As Integer = 0 To Request.Form.Count - 1
                    If Request.Form.Keys(i).Length > 8 Then
                        Select Case Request.Form.Keys(i).Substring(0, 8).ToUpper
                            Case "METADATA"
                                If Not Directory.Exists(filepath) Then
                                    Try
                                        Directory.CreateDirectory(filepath)
                                    Catch ex As Exception
                                    End Try
                                End If

                                Dim thefile As String = filepath & Request.Form.Keys(i) & "_" & cmsPage & ".txt"
                                Dim fileinfo As FileInfo = New FileInfo(thefile)
                                If fileinfo.Exists Then
                                    fileinfo.Delete()
                                End If
                                Dim fs As FileStream = Nothing
                                If (Not File.Exists(thefile)) Then
                                    fs = File.Create(thefile)
                                    Using fs
                                    End Using
                                    If File.Exists(thefile) Then
                                        Using sw As StreamWriter = New StreamWriter(thefile)
                                            sw.Write(Request.Form.Item(i))
                                        End Using
                                    End If
                                End If
                        End Select
                    End If
                Next
            Case "TEXT"
                filepath &= "Text\"
                For i As Integer = 0 To Request.Form.Count - 1
                    If Request.Form.Keys(i).IndexOf("_") > -1 Then
                        Select Case Request.Form.Keys(i).Split("_")(0)
                            Case "Text"
                                If Not Directory.Exists(filepath) Then
                                    Try
                                        Directory.CreateDirectory(filepath)
                                    Catch ex As Exception
                                    End Try
                                End If

                                Dim thefile As String = filepath & Right(Request.Form.Keys(i), Len(Request.Form.Keys(i)) - 5) & ".txt"

                                Dim fileinfo As FileInfo = New FileInfo(thefile)
                                If fileinfo.Exists Then
                                    fileinfo.Delete()
                                End If
                                Dim fs As FileStream = Nothing
                                If (Not File.Exists(thefile)) Then

                                    fs = File.Create(thefile)
                                    Using fs
                                    End Using
                                    If File.Exists(thefile) Then
                                        Using sw As StreamWriter = New StreamWriter(thefile)
                                            sw.Write(Request.Form.Item(i))
                                        End Using
                                    End If
                                End If
                        End Select
                    End If
                Next
            Case "RICH"
                filepath &= "Rich\"
                For i As Integer = 0 To Request.Form.Count - 1
                    If Request.Form.Keys(i).IndexOf("_") > -1 Then
                        Select Case Request.Form.Keys(i).Split("_")(0)
                            Case "Rich"
                                If Not Directory.Exists(filepath) Then
                                    Try
                                        Directory.CreateDirectory(filepath)
                                    Catch ex As Exception
                                    End Try
                                End If

                                'Dim thefile As String = filepath & Request.Form.Keys(i) & ".txt"
                                Dim thefile As String = filepath & Right(Request.Form.Keys(i), Len(Request.Form.Keys(i)) - 5) & ".txt"

                                Dim fileinfo As FileInfo = New FileInfo(thefile)
                                If fileinfo.Exists Then
                                    fileinfo.Delete()
                                End If
                                Dim fs As FileStream = Nothing
                                If (Not File.Exists(thefile)) Then
                                    fs = File.Create(thefile)
                                    Using fs
                                    End Using
                                    If File.Exists(thefile) Then
                                        Using sw As StreamWriter = New StreamWriter(thefile)
                                            sw.Write(Request.Form.Item(i))
                                        End Using
                                    End If
                                End If
                        End Select
                    End If
                Next

            Case "LINK"
                filepath &= "Link\"

                If Not Directory.Exists(filepath) Then
                    Try
                        Directory.CreateDirectory(filepath)
                    Catch ex As Exception
                    End Try
                End If

                Dim thefile As String = filepath & Request.Form("linkitem") & ".txt"
                Dim fileinfo As FileInfo = New FileInfo(thefile)
                If fileinfo.Exists Then
                    fileinfo.Delete()
                End If
                Dim fs As FileStream = Nothing
                If (Not File.Exists(thefile)) Then
                    fs = File.Create(thefile)
                    Using fs
                    End Using
                    fs.Close()
                    If File.Exists(thefile) Then
                        Using sw As StreamWriter = New StreamWriter(thefile)
                            sw.Write(Request.Form("href") & "|-|" & Request.Form("linktext"))
                            sw.Close()
                        End Using

                    End If
                End If
            Case "TEXTSET"
                filepath &= "Textset\"

                Dim cmsTextsetName As String = ""
                If Not Request.Form("cmsTextsetName") Is Nothing Then
                    cmsTextsetName = Request.Form("cmsTextsetName")
                End If

                If Not Directory.Exists(filepath & cmsTextsetName.Substring(8) & "\") Then
                    Try
                        Directory.CreateDirectory(filepath & cmsTextsetName.Substring(8) & "\")
                    Catch ex As Exception
                    End Try
                End If


                For Each files As String In Directory.GetFiles(filepath & cmsTextsetName.Substring(8) & "\")
                    File.Delete(files)
                Next
                For i As Integer = 0 To Request.Form.Count - 1
                    If Request.Form.Keys(i).IndexOf("_") > -1 Then
                        Dim keyName() As String = Request.Form.Keys(i).Split("_")
                        Dim setFolder As String = "Textset"
                        For j As Integer = 1 To keyName.Length - 2
                            setFolder &= "_" & keyName(j)
                        Next
                        setFolder &= "\"
                        Select Case keyName(0)
                            Case "Text"
                                If Not Directory.Exists(filepath & setFolder.Substring(8)) Then
                                    Try
                                        Directory.CreateDirectory(filepath & setFolder.Substring(8))
                                    Catch ex As Exception
                                    End Try
                                End If
                                Dim thefile As String = filepath & setFolder.Substring(8) & Request.Form.Keys(i).Substring(5) & ".txt"
                                Dim fileinfo As FileInfo = New FileInfo(thefile)
                                If fileinfo.Exists Then
                                    fileinfo.Delete()
                                End If
                                Dim fs As FileStream = Nothing
                                If (Not File.Exists(thefile)) Then
                                    fs = File.Create(thefile)
                                    Using fs
                                    End Using
                                    If File.Exists(thefile) Then
                                        Using sw As StreamWriter = New StreamWriter(thefile)
                                            sw.Write(Request.Form.Item(i))
                                        End Using
                                    End If
                                End If
                        End Select
                    End If
                Next
            Case "LINKSET"
                filepath &= "Linkset\"
                Dim cmsTextsetName As String = ""
                If Not Request.Form("cmsLinksetName") Is Nothing Then
                    cmsTextsetName = Request.Form("cmsLinksetName")
                End If

                If Not Directory.Exists(filepath & cmsTextsetName.Substring(8) & "\") Then
                    Try
                        Directory.CreateDirectory(filepath & cmsTextsetName.Substring(8) & "\")
                    Catch ex As Exception
                    End Try
                End If


                For Each files As String In Directory.GetFiles(filepath & cmsTextsetName.Substring(8) & "\")
                    File.Delete(files)
                Next
                For i As Integer = 0 To Request.Form.Count - 1
                    If Request.Form.Keys(i).IndexOf("_") > -1 Then
                        Dim keyName() As String = Request.Form.Keys(i).Split("_")
                        Dim setFolder As String = "Linkset"
                        For j As Integer = 1 To keyName.Length - 2
                            setFolder &= "_" & keyName(j)
                        Next
                        setFolder &= "\"
                        Select Case keyName(0)
                            Case "Text"
                                If Not Directory.Exists(filepath & setFolder.Substring(8)) Then
                                    Try
                                        Directory.CreateDirectory(filepath & setFolder.Substring(8))
                                    Catch ex As Exception
                                    End Try
                                End If
                                Dim thefile As String = filepath & setFolder.Substring(8) & Request.Form.Keys(i).Substring(5) & ".txt"
                                Dim fileinfo As FileInfo = New FileInfo(thefile)
                                If fileinfo.Exists Then
                                    fileinfo.Delete()
                                End If
                                Dim fs As FileStream = Nothing
                                If (Not File.Exists(thefile)) Then
                                    fs = File.Create(thefile)
                                    Using fs
                                    End Using
                                    If File.Exists(thefile) Then
                                        Using sw As StreamWriter = New StreamWriter(thefile)
                                            sw.Write(Request.Form.Item(i))
                                        End Using
                                    End If
                                End If
                        End Select
                    End If
                Next
            Case "IMAGE"
                Dim thefile As String = ""
                Dim fileinfo As FileInfo
                filepath &= "Image\"

                Dim DeleteImage As String = ""
                Dim width As String = ""
                Dim height As String = ""
                Dim filename As String = ""
                Dim fileExt As String = ""
                Dim ImageName As String = ""
                Dim ImageAlt As String = ""

                If Not Request.Form("filename") Is Nothing Then
                    ImageName = Request.Form("filename")
                    ImageName = Right(ImageName, Len(ImageName) - 6)
                End If
                If Not Request.Form("width") Is Nothing Then
                    width = Request.Form("width")
                End If
                If Not Request.Form("height") Is Nothing Then
                    height = Request.Form("height")
                End If
                If Not Request.Form("filename") Is Nothing Then
                    filename = Request.Form("filename")
                End If
                If Not Request.Form("alt") Is Nothing Then
                    ImageAlt = Request.Form("alt")
                End If
                If Not Request.Form("delete") Is Nothing Then
                    DeleteImage = Request.Form("delete")
                End If

                If DeleteImage = "1" Then

                    thefile = filepath & ImageName & ".gif"
                    fileinfo = New FileInfo(thefile)
                    If fileinfo.Exists Then
                        fileinfo.Delete()
                    End If
                    thefile = filepath & ImageName & ".png"
                    fileinfo = New FileInfo(thefile)
                    If fileinfo.Exists Then
                        fileinfo.Delete()
                    End If
                    thefile = filepath & ImageName & ".jpg"
                    fileinfo = New FileInfo(thefile)
                    If fileinfo.Exists Then
                        fileinfo.Delete()
                    End If
                    thefile = filepath & ImageName & ".txt"
                    fileinfo = New FileInfo(thefile)
                    If fileinfo.Exists Then
                        fileinfo.Delete()
                    End If

                    For Each f As String In Directory.GetFiles(Server.MapPath("~") & "cms\data\Sized\", ImageName & ".*", SearchOption.AllDirectories)
                        Dim FileToDelete As String = Path.GetDirectoryName(f) & "\" & Path.GetFileName(f)
                        If FileToDelete <> "" Then
                            Dim fso
                            fso = CreateObject("Scripting.FileSystemObject")
                            fso.DeleteFile(FileToDelete)
                        End If
                    Next

                Else
                    If Request.Files.Count > 0 Then

                        Select Case Request.Files(0).ContentType
                            Case "image/gif"
                                fileExt = ".gif"
                            Case "image/png"
                                fileExt = ".png"
                            Case "image/jpeg"
                                fileExt = ".jpg"
                            Case Else
                                returnText = "<html><head><title></title><script type=""text/javascript"">alert(""You must upload a valid jpg or png or gif image."");setTimeout(""parent.parent.cmsModalUpdated()"",110)</script></head><body></body></html>"
                        End Select

                        If fileExt <> "" Then

                            If Not Directory.Exists(filepath) Then
                                Try
                                    Directory.CreateDirectory(filepath)
                                Catch ex As Exception
                                End Try
                            End If

                            thefile = filepath & ImageName & ".gif"
                            fileinfo = New FileInfo(thefile)
                            If fileinfo.Exists Then
                                fileinfo.Delete()
                            End If
                            thefile = filepath & ImageName & ".png"
                            fileinfo = New FileInfo(thefile)
                            If fileinfo.Exists Then
                                fileinfo.Delete()
                            End If
                            thefile = filepath & ImageName & ".jpg"
                            fileinfo = New FileInfo(thefile)
                            If fileinfo.Exists Then
                                fileinfo.Delete()
                            End If
                            thefile = filepath & ImageName & ".txt"
                            fileinfo = New FileInfo(thefile)
                            If fileinfo.Exists Then
                                fileinfo.Delete()
                            End If

                            For Each f As String In Directory.GetFiles(Server.MapPath("~") & "cms\data\Sized\", ImageName & ".*", SearchOption.AllDirectories)
                                Dim FileToDelete As String = Path.GetDirectoryName(f) & "\" & Path.GetFileName(f)
                                If FileToDelete <> "" Then
                                    Dim fso
                                    fso = CreateObject("Scripting.FileSystemObject")
                                    fso.DeleteFile(FileToDelete)
                                End If
                            Next

                            Request.Files(0).SaveAs(filepath & ImageName & fileExt)
                            Dim AltFile As String = filepath & ImageName & ".txt"
                            Dim fs As FileStream = Nothing
                            If (Not File.Exists(AltFile)) Then
                                fs = File.Create(AltFile)
                                Using fs
                                End Using
                                fs.Close()
                                If File.Exists(AltFile) Then
                                    Using sw As StreamWriter = New StreamWriter(AltFile)
                                        sw.Write(ImageAlt)
                                        sw.Close()
                                    End Using
                                End If
                            End If

                        End If
                    End If
                End If

            Case "FILE"
                filepath &= "File\"
                Dim thefile As String = ""
                Dim DocName As String = ""
                Dim DocExt As String = ""
                Dim DocLinkText As String = ""
                Dim DocFileItem As String = ""
                Dim DeleteFile As String = ""
                Dim fileinfo As FileInfo

                If Not Request.Form("filename") Is Nothing Then
                    DocName = Request.Form("filename")
                End If
                If Not Request.Form("linktext") Is Nothing Then
                    DocLinkText = Request.Form("linktext")
                End If
                If Not Request.Form("fileitem") Is Nothing Then
                    DocFileItem = Request.Form("fileitem")
                End If
                If Not Request.Form("delete") Is Nothing Then
                    DeleteFile = Request.Form("delete")
                End If

                Dim ExistFile As String = ""
                ExistFile = Request.Files(0).FileName.ToString


                If DeleteFile = "1" AndAlso ExistFile = "" Then
                    Directory.Delete(filepath & DocFileItem, True)
                    Dim TxtFile As String = filepath & DocFileItem & ".txt"
                    fileinfo = New FileInfo(TxtFile)
                    If fileinfo.Exists Then
                        fileinfo.Delete()
                    End If
                End If


                If DeleteFile = "1" AndAlso ExistFile <> "" Then
                    Directory.Delete(filepath & DocFileItem, True)
                    Dim TxtFile As String = filepath & DocFileItem & ".txt"
                    fileinfo = New FileInfo(TxtFile)
                    If fileinfo.Exists Then
                        fileinfo.Delete()
                    End If

                    Dim FI As New FileInfo(Request.Files(0).FileName)
                    Select Case FI.Extension.ToLower
                        Case ".pdf"
                            DocExt = ".pdf"
                        Case Else
                            returnText = "<html><head><title></title><script type=""text/javascript"">alert(""You must upload a valid PDF document."");setTimeout(""parent.parent.cmsModalUpdated()"",110)</script></head><body></body></html>"
                    End Select

                    If DocExt <> "" Then
                        If Not Directory.Exists(filepath & DocFileItem & "\") Then
                            Try
                                Directory.CreateDirectory(filepath & DocFileItem & "\")
                            Catch ex As Exception
                            End Try
                        End If

                        Request.Files(0).SaveAs(filepath & DocFileItem & "\" & Request.Form("filename") & DocExt)

                        Dim TxtFile1 As String = filepath & DocFileItem & ".txt"
                        Dim fs As FileStream = Nothing
                        If (Not File.Exists(TxtFile1)) Then
                            fs = File.Create(TxtFile1)
                            Using fs
                            End Using
                            fs.Close()
                        End If
                        If File.Exists(TxtFile1) Then
                            Using sw As StreamWriter = New StreamWriter(TxtFile1)
                                sw.Write(DocLinkText & "|-|" & "cms/data/File/" & DocFileItem & "/" & DocName & DocExt)
                                sw.Close()
                            End Using
                        End If
                    End If

                ElseIf ExistFile <> "" Then
                    Dim FI As New FileInfo(Request.Files(0).FileName)
                    Select Case FI.Extension.ToLower
                        Case ".pdf"
                            DocExt = ".pdf"
                        Case Else
                            returnText = "<html><head><title></title><script type=""text/javascript"">alert(""You must upload a valid PDF document."");setTimeout(""parent.parent.cmsModalUpdated()"",110)</script></head><body></body></html>"
                    End Select

                    If DocExt <> "" Then

                        If Not Directory.Exists(filepath & DocFileItem & "\") Then
                            Try
                                Directory.CreateDirectory(filepath & DocFileItem & "\")
                            Catch ex As Exception
                            End Try
                        End If

                        Request.Files(0).SaveAs(filepath & DocFileItem & "\" & Request.Form("filename") & DocExt)

                        Dim TxtFile1 As String = filepath & DocFileItem & ".txt"
                        Dim fs As FileStream = Nothing
                        If (Not File.Exists(TxtFile1)) Then
                            fs = File.Create(TxtFile1)
                            Using fs
                            End Using
                            fs.Close()
                        End If
                        If File.Exists(TxtFile1) Then
                            Using sw As StreamWriter = New StreamWriter(TxtFile1)
                                sw.Write(DocLinkText & "|-|" & "cms/data/File/" & DocFileItem & "/" & DocName & DocExt)
                                sw.Close()
                            End Using
                        End If
                    End If
                End If

                If ExistFile = "" Then
                    Dim TxtFile As String = filepath & DocFileItem & ".txt"
                    Dim FileContent As String = ""
                    If File.Exists(TxtFile) Then
                        Using sr As New StreamReader(TxtFile)
                            FileContent = Sr.ReadToEnd()
                            sr.Close()
                        End Using
                        Dim OldFileName As String = ""
                        OldFileName = FileContent.Split("/").Last
                        Using sw As StreamWriter = New StreamWriter(TxtFile)
                            sw.Write(DocLinkText & "|-|" & "cms/data/File/" & DocFileItem & "/" & DocName & ".pdf")
                            sw.Close()
                        End Using
                        Dim fso
                        fso = CreateObject("Scripting.FileSystemObject")
                        fso.MoveFile(filepath & DocFileItem & "\" & OldFileName, filepath & DocFileItem & "\" & DocName & ".pdf")
                    Else
                        returnText = "<html><head><title></title><script type=""text/javascript"">alert(""You must upload a valid PDF document."");setTimeout(""parent.parent.cmsModalUpdated()"",110)</script></head><body></body></html>"
                    End If
                End If

        End Select

        Dim out1 As String = ""
        Dim out2 As String = ""
        Dim out3 As String = ""


        If Not Request.QueryString("cmsGroup") Is Nothing And Not Request.QueryString("cmsGroupItemDelete") Is Nothing Then
            Dim cmsGroupItemDelete As Integer = Request.QueryString("cmsGroupItemDelete")
            Dim cmsGroup As String = Request.QueryString("cmsGroup")

            Dim Items As New List(Of String)
            For Each f As String In Directory.GetFiles(Server.MapPath("~") & "cms\data\", cmsGroup & "_Item" & "*" & ".*", SearchOption.AllDirectories)
                Items.Add(Path.GetFileName(f) & "|-|" & Path.GetDirectoryName(f))
            Next
            'Items.Sort()
            Dim MComparer As New MyComparer()
            Dim SortedItems() As String = Items.ToArray
            Array.Sort(SortedItems, MComparer)


            Dim Dirs As New List(Of String)
            For Each f As String In Directory.GetDirectories(Server.MapPath("~") & "cms\data\", cmsGroup & "_Item" & "*", SearchOption.AllDirectories)
                Dirs.Add(Path.GetFileName(f) & "|-|" & Path.GetDirectoryName(f))
            Next
            'Dirs.Sort()
            Dim SortedDirs() As String = Dirs.ToArray
            Array.Sort(SortedDirs, MComparer)

            For Each Item As String In SortedItems
                Dim FindPart As String = ""
                Dim ReplacePart As String = ""

                Dim split1 As String() = Split(Item, "|-|")
                Dim ItemName As String = split1(0)
                Dim ItemPatch As String = split1(1)
                If CMSFunctions.GetItemNumber(ItemName) = cmsGroupItemDelete Then
                    Dim FileToDelete As String = split1(1) & "\" & split1(0)

                    Dim fso
                    fso = CreateObject("Scripting.FileSystemObject")
                    fso.DeleteFile(FileToDelete)
                ElseIf CMSFunctions.GetItemNumber(ItemName) > cmsGroupItemDelete Then
                    Dim OldItem As String = ""
                    OldItem = ItemPatch & "\" & ItemName

                    '#PDF FILES#
                    If ItemPatch.Contains("File") = True And ItemName.Contains(".txt") = True Then
                        Dim FileContent As String = ""

                        Using sr As New StreamReader(OldItem)
                            FileContent = Sr.ReadToEnd()
                            sr.Close()
                        End Using

                        Dim StringFind As String = Replace(ItemName, ".txt", "")
                        FindPart = "Item_" & CMSFunctions.GetItemNumber(ItemName)
                        ReplacePart = "Item_" & CMSFunctions.GetItemNumber(ItemName) - 1
                        Dim StringReplace As String = Replace(ItemName, FindPart, ReplacePart)


                        'Dim StringReplace As String = Replace(ItemName, CMSFunctions.GetItemNumber(ItemName), CMSFunctions.GetItemNumber(ItemName) - 1)
                        StringReplace = Replace(StringReplace, ".txt", "")
                        FileContent = Replace(FileContent, StringFind, StringReplace)

                        Using sw As StreamWriter = New StreamWriter(OldItem)
                            sw.Write(FileContent)
                            sw.Close()
                        End Using
                    End If
                    '#PDF FILES#


                    Dim NewItem As String = ""
                    FindPart = "Item_" & CMSFunctions.GetItemNumber(ItemName)
                    ReplacePart = "Item_" & CMSFunctions.GetItemNumber(ItemName) - 1
                    NewItem = Replace(ItemName, FindPart, ReplacePart)

                    'NewItem = Replace(ItemName, CMSFunctions.GetItemNumber(ItemName), CMSFunctions.GetItemNumber(ItemName) - 1)
                    Dim FileToRename As String = split1(1) & "\" & NewItem
                    Dim fso
                    fso = CreateObject("Scripting.FileSystemObject")
                    fso.MoveFile(OldItem, FileToRename)
                End If
            Next

            For Each Dir As String In SortedDirs
                Dim split1 As String() = Split(Dir, "|-|")
                Dim ItemName As String = split1(0)
                Dim ItemPatch As String = split1(1)
                If CMSFunctions.GetItemNumber(ItemName) = cmsGroupItemDelete Then
                    Dim FileToDelete As String = split1(1) & "\" & split1(0)
                    Directory.Delete(FileToDelete, True)
                ElseIf CMSFunctions.GetItemNumber(ItemName) > cmsGroupItemDelete Then
                    Dim OldItem As String = ""
                    OldItem = ItemPatch & "\" & ItemName
                    Dim NewItem As String = ""

                    Dim FindPart As String = ""
                    Dim ReplacePart As String = ""
                    FindPart = "Item_" & CMSFunctions.GetItemNumber(ItemName)
                    ReplacePart = "Item_" & CMSFunctions.GetItemNumber(ItemName) - 1
                    NewItem = Replace(ItemName, FindPart, ReplacePart)

                    'NewItem = Replace(ItemName, CMSFunctions.GetItemNumber(ItemName), CMSFunctions.GetItemNumber(ItemName) - 1)
                    Dim FileToRename As String = split1(1) & "\" & NewItem
                    My.Computer.FileSystem.RenameDirectory(OldItem, NewItem)
                End If
            Next
        End If


        If Not Request.QueryString("cmsGroup") Is Nothing And Not Request.QueryString("cmsGroupItemUp") Is Nothing Then
            Dim cmsGroupItemUp As Integer = Request.QueryString("cmsGroupItemUp")
            Dim cmsGroup As String = Request.QueryString("cmsGroup")

            Dim Items1 As New List(Of String)
            Dim Items2 As New List(Of String)
            Dim Items3 As New List(Of String)

            For Each f As String In Directory.GetFiles(Server.MapPath("~") & "cms\data\", cmsGroup & "_Item" & "*", SearchOption.AllDirectories)
                'out1 &= Path.GetFileName(f) & "|-|" & Path.GetDirectoryName(f) & "</br>"

                If CMSFunctions.GetItemNumber(Path.GetFileName(f)) + 1 = cmsGroupItemUp Then
                    Items1.Add(Path.GetFileName(f) & "|-|" & Path.GetDirectoryName(f))
                End If
                'out2 &= CMSFunctions.GetItemNumber(Path.GetFileName(f)) & "</br>"

                If CMSFunctions.GetItemNumber(Path.GetFileName(f)) = cmsGroupItemUp Then
                    out2 &= Path.GetFileName(f) & "|-|" & Path.GetDirectoryName(f)
                    Items2.Add(Path.GetFileName(f) & "|-|" & Path.GetDirectoryName(f))
                End If
            Next

            For Each Item1 As String In Items1
                Dim TempFile As String() = Split(Item1, "|-|")
                Dim fso
                fso = CreateObject("Scripting.FileSystemObject")
                fso.MoveFile(TempFile(1) & "\" & TempFile(0), TempFile(1) & "\" & "tmp_" & TempFile(0))
                Items3.Add("tmp_" & TempFile(0) & "|-|" & TempFile(1))
            Next

            For Each Item2 As String In Items2
                Dim TempFile As String() = Split(Item2, "|-|")
                Dim NewItem As String = ""

                Dim FindPart As String = ""
                Dim ReplacePart As String = ""
                FindPart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0))
                ReplacePart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0)) - 1
                NewItem = Replace(TempFile(0), FindPart, ReplacePart)

                'NewItem = Replace(TempFile(0), CMSFunctions.GetItemNumber(TempFile(0)), CMSFunctions.GetItemNumber(TempFile(0)) - 1)
                Dim FileToRename As String = TempFile(1) & "\" & NewItem

                '----------------------'
                If TempFile(1).Contains("File") = True And TempFile(0).Contains(".txt") = True Then
                    Dim FileContent As String = ""

                    Using sr As New StreamReader(TempFile(1) & "\" & TempFile(0))
                        FileContent = Sr.ReadToEnd()
                        sr.Close()
                    End Using

                    Dim StringFind As String = Replace(TempFile(0), ".txt", "")


                    Dim FindPart1 As String = ""
                    Dim ReplacePart1 As String = ""
                    FindPart1 = "Item_" & CMSFunctions.GetItemNumber(TempFile(0))
                    ReplacePart1 = "Item_" & CMSFunctions.GetItemNumber(TempFile(0)) - 1
                    Dim StringReplace As String = Replace(TempFile(0), FindPart1, ReplacePart1)

                    'Dim StringReplace As String = Replace(TempFile(0), CMSFunctions.GetItemNumber(TempFile(0)), CMSFunctions.GetItemNumber(TempFile(0)) - 1)
                    StringReplace = Replace(StringReplace, ".txt", "")
                    FileContent = Replace(FileContent, StringFind, StringReplace)

                    Using sw As StreamWriter = New StreamWriter(TempFile(1) & "\" & TempFile(0))
                        sw.Write(FileContent)
                        sw.Close()
                    End Using
                End If
                '----------------------'

                Dim fso
                fso = CreateObject("Scripting.FileSystemObject")
                fso.MoveFile(TempFile(1) & "\" & TempFile(0), FileToRename)
            Next

            For Each Item3 As String In Items3
                Dim TempFile As String() = Split(Item3, "|-|")
                Dim NewItem As String = ""

                Dim FindPart As String = ""
                Dim ReplacePart As String = ""
                FindPart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0))
                ReplacePart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0)) + 1
                NewItem = Replace(TempFile(0), FindPart, ReplacePart)

                'NewItem = Replace(TempFile(0), CMSFunctions.GetItemNumber(TempFile(0)), CMSFunctions.GetItemNumber(TempFile(0)) + 1)
                NewItem = Replace(NewItem, "tmp_", "")
                '----------------------'
                If TempFile(1).Contains("File") = True And TempFile(0).Contains(".txt") = True Then
                    Dim FileContent As String = ""

                    Using sr As New StreamReader(TempFile(1) & "\" & TempFile(0))
                        FileContent = Sr.ReadToEnd()
                        sr.Close()
                    End Using

                    Dim StringFind As String = Replace(TempFile(0), ".txt", "")
                    StringFind = Replace(StringFind, "tmp_", "")

                    Dim FindPart1 As String = ""
                    Dim ReplacePart1 As String = ""
                    FindPart1 = "Item_" & CMSFunctions.GetItemNumber(TempFile(0))
                    ReplacePart1 = "Item_" & CMSFunctions.GetItemNumber(TempFile(0)) + 1
                    Dim StringReplace As String = Replace(TempFile(0), FindPart1, ReplacePart1)

                    'Dim StringReplace As String = Replace(TempFile(0), CMSFunctions.GetItemNumber(TempFile(0)), CMSFunctions.GetItemNumber(TempFile(0)) + 1)
                    StringReplace = Replace(StringReplace, ".txt", "")
                    StringReplace = Replace(StringReplace, "tmp_", "")
                    FileContent = Replace(FileContent, StringFind, StringReplace)

                    Using sw As StreamWriter = New StreamWriter(TempFile(1) & "\" & TempFile(0))
                        sw.Write(FileContent)
                        sw.Close()
                    End Using
                End If
                '----------------------'

                Dim FileToRename As String = TempFile(1) & "\" & NewItem
                Dim fso
                fso = CreateObject("Scripting.FileSystemObject")
                fso.MoveFile(TempFile(1) & "\" & TempFile(0), FileToRename)
            Next


            'Dir
            Dim Dirs1 As New List(Of String)
            Dim Dirs2 As New List(Of String)
            Dim Dirs3 As New List(Of String)
            For Each f As String In Directory.GetDirectories(Server.MapPath("~") & "cms\data\", cmsGroup & "_Item" & "*", SearchOption.AllDirectories)
                If CMSFunctions.GetItemNumber(Path.GetFileName(f)) + 1 = cmsGroupItemUp Then
                    Dirs1.Add(Path.GetFileName(f) & "|-|" & Path.GetDirectoryName(f))
                End If
                If CMSFunctions.GetItemNumber(Path.GetFileName(f)) = cmsGroupItemUp Then
                    Dirs2.Add(Path.GetFileName(f) & "|-|" & Path.GetDirectoryName(f))
                End If
            Next
            For Each Dir1 As String In Dirs1
                Dim TempFile As String() = Split(Dir1, "|-|")
                My.Computer.FileSystem.RenameDirectory(TempFile(1) & "\" & TempFile(0), "tmp_" & TempFile(0))
                Dirs3.Add("tmp_" & TempFile(0) & "|-|" & TempFile(1))
            Next
            For Each Dir2 As String In Dirs2
                Dim TempFile As String() = Split(Dir2, "|-|")
                Dim NewItem As String = ""

                Dim FindPart As String = ""
                Dim ReplacePart As String = ""
                FindPart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0))
                ReplacePart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0)) - 1
                NewItem = Replace(TempFile(0), FindPart, ReplacePart)

                'NewItem = Replace(TempFile(0), CMSFunctions.GetItemNumber(TempFile(0)), CMSFunctions.GetItemNumber(TempFile(0)) - 1)
                Dim FileToRename As String = TempFile(1) & "\" & NewItem
                My.Computer.FileSystem.RenameDirectory(TempFile(1) & "\" & TempFile(0), NewItem)
            Next
            For Each Dir3 As String In Dirs3
                Dim TempFile As String() = Split(Dir3, "|-|")
                Dim NewItem As String = ""

                Dim FindPart As String = ""
                Dim ReplacePart As String = ""
                FindPart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0))
                ReplacePart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0)) + 1
                NewItem = Replace(TempFile(0), FindPart, ReplacePart)

                'NewItem = Replace(TempFile(0), CMSFunctions.GetItemNumber(TempFile(0)), CMSFunctions.GetItemNumber(TempFile(0)) + 1)
                NewItem = Replace(NewItem, "tmp_", "")
                Dim FileToRename As String = TempFile(1) & "\" & NewItem
                My.Computer.FileSystem.RenameDirectory(TempFile(1) & "\" & TempFile(0), NewItem)
            Next

        End If


        If Not Request.QueryString("cmsGroup") Is Nothing And Not Request.QueryString("cmsGroupItemDown") Is Nothing Then
            Dim cmsGroupItemDown As Integer = Request.QueryString("cmsGroupItemDown")
            Dim cmsGroup As String = Request.QueryString("cmsGroup")

            Dim Items1 As New List(Of String)
            Dim Items2 As New List(Of String)
            Dim Items3 As New List(Of String)

            For Each f As String In Directory.GetFiles(Server.MapPath("~") & "cms\data\", cmsGroup & "_Item" & "*", SearchOption.AllDirectories)
                If CMSFunctions.GetItemNumber(Path.GetFileName(f)) - 1 = cmsGroupItemDown Then
                    Items1.Add(Path.GetFileName(f) & "|-|" & Path.GetDirectoryName(f))
                End If
                If CMSFunctions.GetItemNumber(Path.GetFileName(f)) = cmsGroupItemDown Then
                    Items2.Add(Path.GetFileName(f) & "|-|" & Path.GetDirectoryName(f))
                End If
            Next

            For Each Item1 As String In Items1
                Dim TempFile As String() = Split(Item1, "|-|")
                Dim fso
                fso = CreateObject("Scripting.FileSystemObject")
                fso.MoveFile(TempFile(1) & "\" & TempFile(0), TempFile(1) & "\" & "tmp_" & TempFile(0))
                Items3.Add("tmp_" & TempFile(0) & "|-|" & TempFile(1))
            Next

            For Each Item2 As String In Items2
                Dim TempFile As String() = Split(Item2, "|-|")
                Dim NewItem As String = ""

                Dim FindPart As String = ""
                Dim ReplacePart As String = ""
                FindPart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0))
                ReplacePart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0)) + 1
                NewItem = Replace(TempFile(0), FindPart, ReplacePart)

                'NewItem = Replace(TempFile(0), CMSFunctions.GetItemNumber(TempFile(0)), CMSFunctions.GetItemNumber(TempFile(0)) + 1)
                Dim FileToRename As String = TempFile(1) & "\" & NewItem
                '----------------------'
                If TempFile(1).Contains("File") = True And TempFile(0).Contains(".txt") = True Then
                    Dim FileContent As String = ""

                    Using sr As New StreamReader(TempFile(1) & "\" & TempFile(0))
                        FileContent = Sr.ReadToEnd()
                        sr.Close()
                    End Using

                    Dim StringFind As String = Replace(TempFile(0), ".txt", "")

                    Dim FindPart1 As String = ""
                    Dim ReplacePart1 As String = ""
                    FindPart1 = "Item_" & CMSFunctions.GetItemNumber(TempFile(0))
                    ReplacePart1 = "Item_" & CMSFunctions.GetItemNumber(TempFile(0)) + 1
                    Dim StringReplace As String = Replace(TempFile(0), FindPart1, ReplacePart1)

                    'Dim StringReplace As String = Replace(TempFile(0), CMSFunctions.GetItemNumber(TempFile(0)), CMSFunctions.GetItemNumber(TempFile(0)) + 1)
                    StringReplace = Replace(StringReplace, ".txt", "")
                    FileContent = Replace(FileContent, StringFind, StringReplace)

                    Using sw As StreamWriter = New StreamWriter(TempFile(1) & "\" & TempFile(0))
                        sw.Write(FileContent)
                        sw.Close()
                    End Using
                End If
                '----------------------'

                Dim fso
                fso = CreateObject("Scripting.FileSystemObject")
                fso.MoveFile(TempFile(1) & "\" & TempFile(0), FileToRename)
            Next

            For Each Item3 As String In Items3
                Dim TempFile As String() = Split(Item3, "|-|")
                Dim NewItem As String = ""

                Dim FindPart As String = ""
                Dim ReplacePart As String = ""
                FindPart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0))
                ReplacePart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0)) - 1
                NewItem = Replace(TempFile(0), FindPart, ReplacePart)

                'NewItem = Replace(TempFile(0), CMSFunctions.GetItemNumber(TempFile(0)), CMSFunctions.GetItemNumber(TempFile(0)) - 1)
                NewItem = Replace(NewItem, "tmp_", "")
                '----------------------'
                If TempFile(1).Contains("File") = True And TempFile(0).Contains(".txt") = True Then
                    Dim FileContent As String = ""

                    Using sr As New StreamReader(TempFile(1) & "\" & TempFile(0))
                        FileContent = Sr.ReadToEnd()
                        sr.Close()
                    End Using

                    Dim StringFind As String = Replace(TempFile(0), ".txt", "")
                    StringFind = Replace(StringFind, "tmp_", "")

                    Dim FindPart1 As String = ""
                    Dim ReplacePart1 As String = ""
                    FindPart1 = "Item_" & CMSFunctions.GetItemNumber(TempFile(0))
                    ReplacePart1 = "Item_" & CMSFunctions.GetItemNumber(TempFile(0)) - 1
                    Dim StringReplace As String = Replace(TempFile(0), FindPart1, ReplacePart1)

                    'Dim StringReplace As String = Replace(TempFile(0), CMSFunctions.GetItemNumber(TempFile(0)), CMSFunctions.GetItemNumber(TempFile(0)) - 1)
                    StringReplace = Replace(StringReplace, ".txt", "")
                    StringReplace = Replace(StringReplace, "tmp_", "")
                    FileContent = Replace(FileContent, StringFind, StringReplace)

                    Using sw As StreamWriter = New StreamWriter(TempFile(1) & "\" & TempFile(0))
                        sw.Write(FileContent)
                        sw.Close()
                    End Using
                End If
                '----------------------'

                Dim FileToRename As String = TempFile(1) & "\" & NewItem
                Dim fso
                fso = CreateObject("Scripting.FileSystemObject")
                fso.MoveFile(TempFile(1) & "\" & TempFile(0), FileToRename)
            Next


            'Dir
            Dim Dirs1 As New List(Of String)
            Dim Dirs2 As New List(Of String)
            Dim Dirs3 As New List(Of String)
            For Each f As String In Directory.GetDirectories(Server.MapPath("~") & "cms\data\", cmsGroup & "_Item" & "*", SearchOption.AllDirectories)
                If CMSFunctions.GetItemNumber(Path.GetFileName(f)) - 1 = cmsGroupItemDown Then
                    Dirs1.Add(Path.GetFileName(f) & "|-|" & Path.GetDirectoryName(f))
                End If
                If CMSFunctions.GetItemNumber(Path.GetFileName(f)) = cmsGroupItemDown Then
                    Dirs2.Add(Path.GetFileName(f) & "|-|" & Path.GetDirectoryName(f))
                End If
            Next
            For Each Dir1 As String In Dirs1
                Dim TempFile As String() = Split(Dir1, "|-|")
                My.Computer.FileSystem.RenameDirectory(TempFile(1) & "\" & TempFile(0), "tmp_" & TempFile(0))
                Dirs3.Add("tmp_" & TempFile(0) & "|-|" & TempFile(1))
            Next
            For Each Dir2 As String In Dirs2
                Dim TempFile As String() = Split(Dir2, "|-|")
                Dim NewItem As String = ""

                Dim FindPart As String = ""
                Dim ReplacePart As String = ""
                FindPart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0))
                ReplacePart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0)) + 1
                NewItem = Replace(TempFile(0), FindPart, ReplacePart)

                'NewItem = Replace(TempFile(0), CMSFunctions.GetItemNumber(TempFile(0)), CMSFunctions.GetItemNumber(TempFile(0)) + 1)
                Dim FileToRename As String = TempFile(1) & "\" & NewItem
                My.Computer.FileSystem.RenameDirectory(TempFile(1) & "\" & TempFile(0), NewItem)
            Next
            For Each Dir3 As String In Dirs3
                Dim TempFile As String() = Split(Dir3, "|-|")
                Dim NewItem As String = ""

                Dim FindPart As String = ""
                Dim ReplacePart As String = ""
                FindPart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0))
                ReplacePart = "Item_" & CMSFunctions.GetItemNumber(TempFile(0)) - 1
                NewItem = Replace(TempFile(0), FindPart, ReplacePart)

                'NewItem = Replace(TempFile(0), CMSFunctions.GetItemNumber(TempFile(0)), CMSFunctions.GetItemNumber(TempFile(0)) - 1)
                NewItem = Replace(NewItem, "tmp_", "")
                Dim FileToRename As String = TempFile(1) & "\" & NewItem
                My.Computer.FileSystem.RenameDirectory(TempFile(1) & "\" & TempFile(0), NewItem)
            Next

        End If

        'Response.Write("LIST 1----" & "<br>" & out1 & "<br>" & "<br>" & "<br>")
        'Response.Write("LIST 2----" & "<br>" & out2 & "<br>" & "<br>" & "<br>")
        'Response.Write("LIST 3----" & "<br>" & out3 & "<br>" & "<br>" & "<br>")

        Response.Write(returnText)
        Response.Flush()
        Response.End()

    End Sub


    Protected Sub ReadCookie() 'Get the cookie name the user entered
        'Grab the cookie
        Dim cookie As HttpCookie = Request.Cookies("SiteEdit")
        'Check to make sure the cookie exists
        If cookie Is Nothing Then
            Response.Redirect("assets\login.html")
        Else
            'check the cookie values
            Dim CookieValue As String = cookie.Value
            Dim SessionFile As String = Server.MapPath("~") & "cms\data\Users\" & CookieValue.Split("/")(0) & "\" & CookieValue.Split("/")(1) & ".dat"
            If (Not File.Exists(SessionFile)) Then
                Response.Redirect("assets\login.html")
            Else
                'Do work
            End If
        End If
    End Sub


End Class

Public Class MyComparer
    Implements IComparer(Of String)

    Declare Unicode Function StrCmpLogicalW Lib "shlwapi.dll" ( _
        ByVal s1 As String, _
        ByVal s2 As String) As Int32

    Public Function Compare(ByVal x As String, ByVal y As String) As Integer _
        Implements System.Collections.Generic.IComparer(Of String).Compare

        Return StrCmpLogicalW(x, y)

    End Function

End Class