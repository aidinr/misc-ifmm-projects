Imports System.Web
Imports System.Data.SqlClient
Imports System.Data

Partial Class _logout
    Inherits System.Web.UI.Page

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim out As String = "1"
        Response.Cookies("SiteEdit").Value = "0"
        Response.Cookies("SiteEdit").Expires = DateTime.Now.AddDays(-1)

        Response.Clear()
        Response.Write(out)
        Response.Flush()
        Response.End()
    End Sub

End Class