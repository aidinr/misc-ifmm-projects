﻿<%@ Page Title="" Language="VB" MasterPageFile="~/HRMaster1.master" AutoEventWireup="false" CodeFile="history.aspx.vb" Inherits="history" %>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!-- Page Content START -->
		<div class="page-content">
			<div class="top-section fluid-padding">
		        	<h1 class="cms cmsType_TextSingle cmsName_History_Top_Heading"></h1>
        			<h2 class="cms cmsType_TextSingle cmsName_History_Heading"></h2>
        			<h3 class="redtext cms cmsType_TextSingle cmsName_History_Heading_Red_Text"></h3>
			</div>



	<div id="timeline">
		<ul id="timeEvent" class="cmsGroup cmsName_Timeline">
		<!--
			<li class="cmsGroupItem">
			 <div>
				<h1 class="cms cmsType_TextSingle cmsName_Year">1983</h1>
				<p class="cms cmsType_TextMulti cmsName_Content">Donec semper quam scelerisque tortor dictum gravida. In hac habitasse platea dictumst. Nam pulvinar, odio sed rhoncus suscipit, sem diam ultrices mauris, eu consequat purus metus eu velit. Proin metus odio, aliquam eget molestie nec, gravida ut sapien. Phasellus quis est sed turpis sollicitudin venenatis sed eu odio. Praesent eget neque eu eros interdum malesuada non vel leo. Sed fringilla porta ligula.</p>
				<em><img class="cms cmsType_Image cmsName_Image" src="assets/images/temp/1.jpg" alt="" /></em>
			 </div>
			</li>
		-->

          <%
                           For i As Integer = 1 To 20 - 1
                                 If Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Timeline_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
                            %>
                            <li class="cmsGroupItem">
			 <div>
				<h1 class="cms cmsType_TextSingle cmsName_Timeline_Item_<%=i %>_Year">1983</h1>
				<em><img class="cms cmsType_Image cmsName_Timeline_Item_<%=i%>_Image" src="cms/data/Sized/padded/350x250/90/ffffff/North/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Timeline_Item_" & i & "_Image")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Timeline_Item_" & i & "_Image")%>" /></em><span class="clearSmall"></span>
				<p class="cms cmsType_TextMulti cmsName_Timeline_Item_<%=i %>_Content">Donec semper quam scelerisque tortor dictum gravida. In hac habitasse platea dictumst. Nam pulvinar, odio sed rhoncus suscipit, sem diam ultrices mauris, eu consequat purus metus eu velit. Proin metus odio, aliquam eget molestie nec, gravida ut sapien. Phasellus quis est sed turpis sollicitudin venenatis sed eu odio. Praesent eget neque eu eros interdum malesuada non vel leo. Sed fringilla porta ligula.</p>
			 </div>
			</li>

                                <%
                              End If
                          Next
                          %>

		
		</ul>
		<a class="next"></a>
		<a class="prev"></a>
		<span class="c"></span>
	</div>

		</div>
		<!-- Page Content END -->	

</asp:Content>

