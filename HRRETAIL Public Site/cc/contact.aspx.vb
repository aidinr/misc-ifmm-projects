﻿Partial Class contactWebForm
    Inherits System.Web.UI.Page
    Protected Sub SubmitButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitButton.Click
        Dim ApiKey As String = ConfigurationManager.AppSettings("apikey").ToString()
        Dim AccessToken As String = ConfigurationManager.AppSettings("accesstoken").ToString()
        ConstantContactHelper.SubmitContact(ApiKey, AccessToken, emailId.Value, nameId.Value, commentsId.Value, interestId.Value, "Web Contact")
    End Sub
End Class
