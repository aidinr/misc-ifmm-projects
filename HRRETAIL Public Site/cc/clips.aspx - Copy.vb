﻿Imports System.Net
Imports System.Configuration
Partial Class clips
    Inherits System.Web.UI.Page
    Protected Sub SubmitButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitButton.Click
        Dim ApiKey As String = ConfigurationManager.AppSettings("apikey").ToString()
        Dim AccessToken As String = ConfigurationManager.AppSettings("accesstoken").ToString()
        ConstantContactHelper.SubmitContact(ApiKey, AccessToken, emailId.Value, nameId.Value, "", "", "Daily Clips")
    End Sub
End Class
