﻿Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports System.Threading
Imports System.Net
Imports System.Collections.Generic
Imports System.Drawing
Partial Class siteplancode
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Clear()
        If Not Request.QueryString("id") Is Nothing Then
            Dim SitePlanID As String = Request.QueryString("id")
            If IsNumeric(SitePlanID) Then
                Dim SitePlanHTMLSQL As String = ""
                SitePlanHTMLSQL &= "SELECT "
                SitePlanHTMLSQL &= "A.Asset_ID ,"
                SitePlanHTMLSQL &= "A.Name,"
                SitePlanHTMLSQL &= "HTML.Item_Value AS SitePlanHtml "
                SitePlanHTMLSQL &= "FROM IPM_ASSET A "
                SitePlanHTMLSQL &= "JOIN IPM_ASSET_FIELD_VALUE HTML ON HTML.ASSET_ID = A.Asset_ID AND HTML.Item_ID = (SELECT Item_ID From IPM_ASSET_FIELD_DESC where Item_Tag = 'IDAM_SITEPLANHTML' AND Active = 1 AND Available = 'Y')"
                SitePlanHTMLSQL &= "WHERE PDF = 0 AND Active = 1 AND Available = 'Y' "
                SitePlanHTMLSQL &= "AND A.Asset_ID = " & SitePlanID & " "
                Dim SitePlanHTMLDT As New DataTable
                SitePlanHTMLDT = DBFunctions.GetDataTable(SitePlanHTMLSQL)
                If SitePlanHTMLDT.Rows.Count > 0 Then
                    Response.Write(SitePlanHTMLDT.Rows(0).Item("SitePlanHtml").ToString.Trim)
                End If
            End If
        End If
        Response.Flush()
        Response.End()
    End Sub

    

End Class
