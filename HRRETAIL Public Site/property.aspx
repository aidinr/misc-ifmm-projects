﻿<%@ Page Title="" Language="VB" MasterPageFile="~/HRMaster1.master" AutoEventWireup="false" CodeFile="property.aspx.vb" Inherits="property1" %>
<%@ MasterType  virtualPath="~/HRMaster1.master"%>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%
Dim pageURL As String = HttpContext.Current.Request.RawUrl
Dim PageName As String = Path.GetFileName(Request.PhysicalPath).ToLower
pageURL = Replace(pageURL, "/", "")
If InStr(pageURL, "?") > 0 Then
pageURL = pageURL.Substring(0, pageURL.IndexOf("?"))
pageURL = Replace(pageURL, "/", "")
End If
%>

<%If File.Exists(Server.MapPath("~") & "cms\data\Metadata\" & "metadataTitle_" & pageURL & ".txt") Then%>
<%If CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Metadata\" & "metadataTitle_" & pageURL & ".txt") <> "" Then%>
<%="<title>" & CMSFunctions.GetMetaTitle(Server.MapPath("~") & "cms\data\", pageURL) & "</title>"%>
<%Else%>
<%  If ProjectSEOTitle <> "" Then%>
<%= "<title>" & ProjectSEOTitle & "</title>"%>
<%Else%>
<%= "<title>" & ProjectTitle & " / H&amp;R Retail</title>"%>
<%End If%>
<%End If%>
<%Else%>
<%  If ProjectSEOTitle <> "" Then%>
<%= "<title>" & ProjectSEOTitle & "</title>"%>
<%Else%>
<%= "<title>" & ProjectTitle & " / H&amp;R Retail</title>"%>
<%End If%>
<%End If%>

<%  If File.Exists(Server.MapPath("~") & "cms\data\Metadata\" & "metadataDescription_" & pageURL & ".txt") Then%>
<%  If CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Metadata\" & "metadataDescription_" & pageURL & ".txt") <> "" Then%>
<meta name="description" content="<% =CMSFunctions.GetMetaDescription(Server.MapPath("~") & "cms\data\", pageURL)%>" />
<%Else%>
<%  If ProjectSEODescription <> "" Then%>
<meta name="description" content="<% =ProjectSEODescription%>" />
<%Else%>
<%="<meta name=""description"" content="" "" />"%>
<%End If%>
<%End If%>
<%Else%>
<%  If ProjectSEODescription <> "" Then%>
<meta name="description" content="<% =ProjectSEODescription%>" />
<%Else%>
<meta name="description" content=" " />
<%End If%>
<%End If%>



<%  If File.Exists(Server.MapPath("~") & "cms\data\Metadata\" & "metadataKeywords_" & pageURL & ".txt") Then%>
<%  If CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Metadata\" & "metadataKeywords_" & pageURL & ".txt") <> "" Then%>
<meta name="keywords" content="<% =CMSFunctions.GetMetaKeywords(Server.MapPath("~") & "cms\data\", pageURL)%>" />
<%Else%>
<%  If ProjectSEOKeywords <> "" Then%>
<meta name="keywords" content="<% =ProjectSEOKeywords%>" />
<%Else%>
<%="<meta name=""keywords"" content="" "" />"%>
<%End If%>
<%End If%>
<%Else%>
<%  If ProjectSEOKeywords <> "" Then%>
<meta name="keywords" content="<% =ProjectSEOKeywords%>" />
<%Else%>
<meta name="keywords" content=" " />
<%End If%>
<%End If%>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<%
Dim pageURL As String = HttpContext.Current.Request.RawUrl
Dim PageName As String = Path.GetFileName(Request.PhysicalPath).ToLower
pageURL = Replace(pageURL, "/", "")
If InStr(pageURL, "?") > 0 Then
pageURL = pageURL.Substring(0, pageURL.IndexOf("?"))
pageURL = Replace(pageURL, "/", "")
End If
%>

		<!-- Page Content START -->
		<div class="page-content">			
			<div class="top-section fluid-padding less-padding">
				<ul class="breadcrumbs">
					<li><a href="/">Home</a> &gt;</li>
					<li><a href="properties">Properties</a> &gt;</li>
					<li><a href="<%=pageURL %>"><%=ProjectTitle%></a> </li>
				</ul>
			</div>	
			<div class="top-section fluid-padding gray-gradient property-page">
				<div class="left">
					<h1><asp:Literal runat="server" ID="ProjectTitle_lbl"/></h1>
					<span><asp:Literal runat="server" ID="ProjectAddress_lbl"/></span>
				</div>
				
                <asp:Literal runat="server" ID="property_downloads_lbl"/>
               
				<ul class="tab-controls clear">
					<li><a class="active"  rel="overview">Overview</a></li>
					<asp:Literal runat="server" ID="site_plan_tab_lbl"/>
					<asp:Literal runat="server" ID="aerial_map_tab_lbl"/>
					<asp:Literal runat="server" ID="area_map_tab_lbl"/>
				</ul>
			</div>		

			<div class="tabs-content property">
				<div class="active fluid-padding" id="overview">
					<div class="two-columns">
						<div class="left two-columns">
							<div class="left text">

                                <asp:Literal runat="server" ID="ProjectDescription_lbl"/>

                                <asp:Literal runat="server" ID="case_studies_lbl"/>
							
								<asp:Literal runat="server" ID="related_news_lbl"/>

							</div>


                            

							<div class="right text">

                            <asp:Literal runat="server" ID="available_spaces_lbl"/>

                                <asp:Literal runat="server" ID="Leasing_Agents_lbl"/>

                                <asp:Literal runat="server" ID="Anchors_lbl"/>

							<asp:Literal runat="server" ID="services_lbl"/>
                                			
							</div>
						</div>
						<div class="right text mobile-padding">

                            <asp:Literal runat="server" ID="property_img_lbl"/>

                             <asp:Literal runat="server" ID="demographics_lbl"/>

							
							<div class="property-web-logo">
                                <asp:Literal runat="server" ID="ProjectWebsite_lbl"/>
								<br/>					
                                <asp:Literal runat="server" ID="ProjectLogo_lbl"/>
							</div>
						</div>
					</div>
				</div> 
				<div id="site-plan" class="tabled-lists">



			

            <asp:Literal runat="server" ID="MultipleSitePlans_lbl"/>


		<div class="map">

<form method="post" action="siteplan.aspx?id=<%=SitePlanID%>" id="sitePlanForm">
<textarea id="SiteMapData" name="SiteMapData" cols="10" rows="10"></textarea>
<asp:Literal runat="server" ID="spaces_select_lbl"/>
<input type="submit" value="Submit" id="sitePlanFormSubmit" />
</form>

<div id="site-plan-container">
<asp:Literal runat="server" ID="site_plan_lbl"/>
<asp:Literal runat="server" ID="site_plan_coordinates_lbl"/>
</div>


					</div>


                    <asp:Literal runat="server" ID="allsiteplandata_lbl"/>

				<%--	<div class="two-columns">
						<div class="left">
                        <asp:Literal runat="server" ID="tenants_siteplan_lbl"/>
                            <asp:Literal runat="server" ID="tenants_logo_siteplan_lbl"/>
						</div>
						<div class="right">
                        <asp:Literal runat="server" ID="available_spaces_siteplan_lbl"/>
						</div>
					</div>--%>




				</div> 
				
				 <asp:Literal runat="server" ID="aerial_map_lbl"/>
                <asp:Literal runat="server" ID="area_map_lbl"/>

				 					
			</div>
			
		</div>
		<!-- Page Content END -->


</asp:Content>

