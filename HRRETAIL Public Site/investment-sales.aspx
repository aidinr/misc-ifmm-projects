﻿<%@ Page Title="" Language="VB" MasterPageFile="~/HRMaster1.master" AutoEventWireup="false" CodeFile="investment-sales.aspx.vb" Inherits="investment_sales" %>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

		<!-- Page Content START -->
		<div class="page-content">
			<div class="top-section fluid-padding">
				<h1 class="cms cmsType_TextSingle cmsName_Investment_Sales_Top_Heading">Investment Sales</h1>
				<h2 class="cms cmsType_TextSingle cmsName_Investment_Sales_Heading">Transactional Retail Success</h2>
				<h3 class="redtext cms cmsType_TextSingle cmsName_Investment_Sales_Heading_Red_Text">Retail</h3>
			</div>			
			<div class="main fluid-padding investment-sales-page">
				<div class="text left half-width mr20">
					<p class="cms cmsType_TextMulti cmsName_Investment_Sales_Intro"></p>
					<h3 class="cms cmsType_TextSingle cmsName_Investment_Sales_Intro_Heading">Proven marketing process.</h3>
					<p class="cms cmsType_TextMulti cmsName_Investment_Sales_Bottom_Intro"></p>
				</div>
				<a href="#" class="purple-link cms cmsType_Link cmsName_Investment_Sales_Website"></a>
				<div class="clear mb20">&nbsp;</div>
				
                
                <ul class="red-border-list cmsGroup cmsName_Investment_Sales_Info">
				<!--
					<li class="cmsGroupItem">
						<strong class="cms cmsType_TextSingle cmsName_Title">varius Scelerisque Vestibulum</strong>
						<p class="cms cmsType_TextMulti cmsName_Content">Nam ultricies dolor eu velit varius scelerisque. Vestibulum in lacus in felis pretium feugiat non sed elit. Duis pretium, urna sed pellentesque tincidunt, neque massa imperdiet nisi, sed cursus leo magna.</p>
					</li>
				-->	

                 <%
                           For i As Integer = 1 To 20 - 1
                                 If Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Investment_Sales_Info_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
                            %>

                    <li class="cmsGroupItem">
						<strong class="cms cmsType_TextSingle cmsName_Investment_Sales_Info_Item_<%=i%>_Title">varius Scelerisque Vestibulum</strong>
						<p class="cms cmsType_TextMulti cmsName_Investment_Sales_Info_Item_<%=i%>_Content">Nam ultricies dolor eu velit varius scelerisque. Vestibulum in lacus in felis pretium feugiat non sed elit. Duis pretium, urna sed pellentesque tincidunt, neque massa imperdiet nisi, sed cursus leo magna.</p>
					</li>

                              <%
                              End If
                          Next
                          %>

					
				</ul>		
                
                
                		
			</div>	
		</div>
		<!-- Page Content END -->	
</asp:Content>

