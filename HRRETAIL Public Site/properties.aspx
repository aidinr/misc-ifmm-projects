﻿<%@ Page Title="" Language="VB" MasterPageFile="~/HRMaster1.master" AutoEventWireup="false" CodeFile="properties.aspx.vb" Inherits="properties" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <!-- Page Content START -->
        <div class="page-content">
            <div class="top-section fluid-padding less-padding">
                <ul class="breadcrumbs">
                    <li><a href="/">Home</a> &gt;</li>
                    <li><span>Properties</span>
                    </li>
                </ul>
            </div>
            <div class="top-section properties-search retailer-search fluid-padding red-gradient">
                <div class="left">
                    	<h1>Property Search</h1>

                    	<!-- <h2></h2>  -->


                    <p class="layout-buttons">
                     <a class="map-view">map view</a>
                     <a class="list-view">grid view</a>
                     <a class="full-view">list view</a>
                    </p>
                </div>
                <form method="post" action="/" id="properties-word-search">
                    <div class="top-part">
                        <div id="properties-auto" class="auto">
                            <input type="text" name="properties-q" id="properties-q" class="q" value="" alt="Search by property name, address, anchor or keyword" />
                            <input type="submit" class="submit" value="search" /><em></em>
                        </div>
                    </div>
                </form>
                <form method="post" action="/" id="properties-search"  class="form">
                    <div class="clear"></div>
                    <select id="Size_of_Space" name="Size_of_Space" class="styled-select">
                        <option value="">Any Size</option>
                        <option value="x05000SF">Up to 5,000 SF</option>
                        <option value="x500115000SF">5,001 - 15,000 SF</option>
                        <option value="x15001SFp">15,000 SF +</option>
                        <option value="PadSites">Pad Sites</option>
                    </select>
                    <select id="State" name="State" class="styled-select">
                    <option value="">Any State</option>
                    <asp:Literal runat="server" ID="StateList_lbl"/>
                    </select>
                    <select id="County" name="County" class="styled-select">
                        <option value="">Any County</option>
                        <asp:Literal runat="server" ID="CountyList_lbl"/>
                    </select>
                    <select id="Anchor" name="Anchor" class="styled-select">
                        <option value="">Don't Limit by Anchor</option>
                        <asp:Literal runat="server" ID="AnchorList_Lbl"/>
                    </select>
                </form>
                <br class="clear" />
            </div>
            <div id="p-search-map"><div id="gmap" class="gmap3"></div></div>
            <div id="p-search-results"></div>
            <div class="c"></div>
        </div>

        <!-- Page Content END -->
    </asp:Content>