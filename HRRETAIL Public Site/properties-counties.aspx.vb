﻿Imports System.Data

Partial Class properties_counties
    Inherits System.Web.UI.Page
    Protected Sub properties_counties_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim ProjectFilters As New ProjectFiltersStructure



        Dim PrintOUT As String = ""
        Dim PrintDT As New DataTable("PrintDT")
        PrintDT = IDAMFunctions.GetProjects("2409776", 0, "Properties", ProjectFilters)

        Dim ButtomOUT As String = ""

        Dim StatesXCountiesDT As New DataTable
        Dim UniqueStatesXCountiesDT As New DataTable
        Dim UniqueStatesDT As New DataTable

        StatesXCountiesDT = PrintDT.DefaultView.ToTable(False, "Name", "ProjectID", "ProjectState", "IDAM_COUNTY")
        UniqueStatesXCountiesDT = StatesXCountiesDT.DefaultView.ToTable(True, "ProjectState", "IDAM_COUNTY")
        UniqueStatesDT = UniqueStatesXCountiesDT.DefaultView.ToTable(True, "ProjectState")

        Dim teststrt As String = ""
        For i As Integer = 0 To StatesXCountiesDT.Rows.Count - 1
            teststrt &= StatesXCountiesDT.Rows(i).Item("ProjectState") & "----" & StatesXCountiesDT.Rows(i).Item("IDAM_COUNTY") & "----" & StatesXCountiesDT.Rows(i).Item("ProjectID") & "----" & StatesXCountiesDT.Rows(i).Item("Name") & "<BR>"
        Next
        HttpContext.Current.Response.Write(teststrt)
        HttpContext.Current.Response.End()

    End Sub
End Class
