Imports System.Web
Imports System.Data.SqlClient
Imports System.Data

Partial Class sitemap
    Inherits System.Web.UI.Page
    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Response.AddHeader("Content-type", "application/xml")
        Dim out As String = "<?xml version=""1.0"" encoding=""UTF-8"" ?>" & vbNewLine
        out &= "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">" & vbNewLine
        Dim url() As String = { _
"http://www.hrretail.com/", _
"http://www.hrretail.com/about", _
"http://www.hrretail.com/history", _
"http://www.hrretail.com/team", _
"http://www.hrretail.com/retail-solutions", _
"http://www.hrretail.com/news", _
"http://www.hrretail.com/resources", _
"http://www.hrretail.com/contact", _
"http://www.hrretail.com/properties", _
"http://www.hrretail.com/retailers-and-restaurants", _
"http://www.hrretail.com/investment-sales", _
"http://www.hrretail.com/urban", _
"http://www.hrretail.com/case-studies", _
"http://www.hrretail.com/terms", _
"http://www.hrretail.com/privacy" _
}

        Dim yesterday As String = Date.Now.AddDays(-1).ToString("yyyy-MM-dd")
        For i As Integer = 0 To url.Length - 1
            out &= "<url><loc>" & url(i).Trim & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
        Next


        Dim ProjectFilters As New ProjectFiltersStructure
        Dim ProjectsDT As New DataTable("FProjects")
        ProjectsDT = IDAMFunctions.GetProjects("2409776", 0, "Properties", ProjectFilters)
        If ProjectsDT.Rows.Count > 0 Then
            For i As Integer = 0 To ProjectsDT.Rows.Count - 1
                If ProjectsDT.Rows(i)("ProjectName").ToString.Trim <> "" Then

                    Dim LinkOut As String = ""
                    If ProjectsDT.Rows(i).Item("SeoURL").ToString.Trim <> "" Then
                        LinkOut &= CMSFunctions.FormatFriendlyUrl_NoLower(ProjectsDT.Rows(i).Item("SeoURL").ToString.Trim, "-")
                    Else
                        LinkOut &= "property-" & ProjectsDT.Rows(i).Item("ProjectID").ToString.Trim & "-" & CMSFunctions.FormatFriendlyUrl(ProjectsDT.Rows(i).Item("ProjectName").ToString.Trim, "-")
                    End If
                    out &= "<url><loc>http://www.hrretail.com/" & LinkOut & "</loc><lastmod>" & ProjectsDT.Rows(i)("update_date").ToString.Trim & "</lastmod><changefreq>weekly</changefreq><priority>1.0</priority></url>" & vbNewLine
                End If
                'out &= "<url><loc>http://www.wcsmith.com/pdf/" & DT3.Rows(i)("ProjectID").ToString.Trim & ".pdf" & "</loc><lastmod>" & DT3.Rows(i)("update_date").ToString.Trim & "</lastmod><changefreq>weekly</changefreq><priority>1.0</priority></url>" & vbNewLine
            Next
        End If



        out &= "</urlset>"

        Response.Write(out)

    End Sub

End Class