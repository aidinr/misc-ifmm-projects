﻿<%@ Page Title="" Language="VB" MasterPageFile="~/HRMaster1.master" AutoEventWireup="false" CodeFile="404.aspx.vb" Inherits="page_404" %>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!-- Page Content START -->
		<div class="page-content">
			<div class="top-section fluid-padding">
				<h1>There was an error</h1>
				<h2>we could <span class="red">not find</span> the page you were looking for. </h2>
			</div>

			<div class="contact-details fluid-padding">
				<form method="post" action="/" class="site-form">
					<ol>
						<li><input type="text" name="" value="" id="d1" alt="First &amp; Last Name"/></li>
						<li><input type="text" name="" value="" id="d2" alt="Email Address"/></li>
						<li>
							<input name="Area_of_Interest" type="hidden" id="Area_of_Interest" />
							<div class="options">
							 <a class="select">Area of Interest</a>
							 <p>
							  <a rel="General Information">General Information</a>
							  <a rel="Signup for Daily Clips">Signup for Daily Clips</a>
							  <a rel="Signup for Broker Blasts">Signup for Broker Blasts</a>
							  <a rel="Portfolio Request">Portfolio Request</a>
							  <a rel="Career Opportunities">Career Opportunities</a>
							  <a rel="Interest Not Specified">Other</a>
							 </p>
							</div>
						</li>
						<li class="clear full-width">
							<textarea name="contactComments" cols="2" rows="3"></textarea>
						</li>
						<li class="clear"><input type="submit" name="" class="submit" value="SUBMIT"/></li>
					</ol>
				</form>
				<div id="contactLocations" class="cmsGroup cmsName_Contact_Locations">
					<!--
					<p class="cmsGroupItem">
						<strong class="city cms cmsType_TextSingle cmsName_City">City, XY</strong>
						<em class="cms cmsType_TextMulti cmsName_Address">
						1234 Wisconsin Avenue<br/>
						Suite 123<br/>
						City, XY 12345<br/>
						Telephone: 301.555.1212<br/>
						Facsimile: 301.555.2222						</em>
						<em class="cms location cmsType_TextSingle cmsName_Location">38.900, -77.333</em>
					</p>
					-->
                    <%
                           For i As Integer = 1 To 20 - 1
                                 If Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Contact_Locations_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
                            %>
                            


                    <p class="cmsGroupItem">
					<strong class="city cms cmsType_TextSingle cmsName_Contact_Locations_Item_<%=i%>_City"><%= CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Contact_Locations_Item_" & i & "_City")%></strong>
					<em class="cms cmsType_TextMulti cmsName_Contact_Locations_Item_<%=i%>_Address">
					<%= CMSFunctions.getContentMulti(Server.MapPath("~") & "cms\data\", "Contact_Locations_Item_" & i & "_Address")%>
					</em>
					<em class="cms location cmsType_TextSingle cmsName_Contact_Locations_Item_<%=i%>_Location"><%= CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Contact_Locations_Item_" & i & "_Location")%></em>
					</p>

                              <%
                              End If
                          Next
                          %>

				<%--	<p class="cmsGroupItem">
					<strong class="city cms cmsType_TextSingle cmsName_Contact_Locations_Item_1_City">Washington, DC</strong>
					<em class="cms cmsType_TextMulti cmsName_Contact_Locations_Item_1_Address">
					 7201 Wisconsin Avenue <br/>
					 Suite 600 <br/>
					 Bethesda, MD 20814 <br/>
					 Telephone: 301.656.3030 <br/>
					 Fax: 301.555.1212
					</em>
					<em class="cms location cmsType_TextSingle cmsName_Contact_Locations_Item_1_Location">38.888, -77.499</em>
					</p>--%>

				</div>
			</div>
		</div>
		<!-- Page Content END -->	
</asp:Content>
