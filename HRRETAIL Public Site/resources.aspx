﻿<%@ Page Title="" Language="VB" MasterPageFile="~/HRMaster1.master" AutoEventWireup="false" CodeFile="resources.aspx.vb" Inherits="resources" %>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
		<!-- Page Content START -->
		<div class="page-content">
			<div class="top-section fluid-padding">
				<h1>about us</h1>
				<h2>resources &amp; <span class="red">downloads</span></h2>
			</div>
			<div class="main fluid-padding ">
				<div class="left">
					<h2 class="maps-title">MAPS</h2>
					<ul class="resources-list left cmsGroup cmsName_Resources_Maps"> 
						<!--
<li class="cmsGroupItem">
<a href="#">
<img src="assets/images/temp/property-list.jpg" class="cms cmsType_Image cmsName_Image" alt=""/>
<strong class="cms cmsType_File cmsName_Document">Regional Mall Map</strong>
<em class="cms cmsType_TextSingle cmsName_Description">PDF Download</em>
</a>
</li>
--> 
						
                         <%
                           For i As Integer = 1 To 20 - 1
                                 If Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Resources_Maps_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
                            %>

                        <li class="cmsGroupItem">
<a href="<%=CMSFunctions.GetFile(Server.MapPath("~") & "cms\data\File\", "Resources_Maps_Item_" & i &"_Document", "link") %>">
<img class="cms cmsType_Image cmsName_Resources_Maps_Item_<%=i%>_Image" src="cms/data/Sized/best/220x153/65/ffffff/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Resources_Maps_Item_" & i & "_Image")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Resources_Maps_Item_" & i & "_Image")%>" />
<strong class="cms cmsFill_Static cmsType_File cmsName_Resources_Maps_Item_<%=i%>_Document"><%=CMSFunctions.GetFile(Server.MapPath("~") & "cms\data\File\", "Resources_Maps_Item_" & i &"_Document", "text") %></strong>
<em class="cms cmsType_TextSingle cmsName_Resources_Maps_Item_<%=i%>_Description">fddasfadsfdsf</em> 
</a> 
</li> 

                              <%
                              End If
                          Next
                          %>




					</ul>
				</div>
				<div class="left downloads">
					<div class="left">
						<h2>business forms</h2>
						<ul class="cmsGroup cmsName_Resources_Business_Forms">
							<!--
							<li class="cmsGroupItem"><a class="cms cmsType_File cmsName_Document" href="#">Example Form</a></li>
							-->
							
                            <%
                           For i As Integer = 1 To 20 - 1
                                    If Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Resources_Business_Forms_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
                            %>
                           <li class="cmsGroupItem"><a class="cms cmsType_File cmsName_Resources_Business_Forms_Item_<%= i %>_Document" href="#">Portfolio Review Form</a></li>
                              <%
                              End If
                          Next
                          %>


						</ul>
						<h2>other resources</h2>
						<ul class="cmsGroup cmsName_Resources_Other">
							<!--
							<li class="cmsGroupItem"><a class="cms cmsType_File cmsName_Document" href="#">Example Report</a></li>
							-->
							                            
                            <%
                           For i As Integer = 1 To 20 - 1
                                    If Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Resources_Other_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
                            %>
                           <li class="cmsGroupItem"><a class="cms cmsType_File cmsName_Resources_Other_Item_<%=i %>_Document" href="#">Property Availability Report</a></li>
                              <%
                              End If
                          Next
                          %>

						</ul>						
					</div>
					<div class="left">
						
                        <asp:Literal runat="server" ID="Broker_Blasts_Lbl"/>

						<div class="subscription-form">
							<button type="button" class="signup show-hidden-form">sign up for broker Blasts</button>
							<form method="post" action="/" class="site-form hidden-form brokerblast">
								<button type="button" class="close-button">Close</button>
								<p>Sign up to receive Broker Blast e-mails about our properties</p>
								<br/>
								<ol>
									<li class="full-width"><input type="text" name="name" class="required" value="" id="f13" alt="First &amp; Last Name"/></li>
									<li class="clear full-width">
										<input type="text" class="required email" name="email" value="" id="f2321" alt="Email Address"/>
										<input type="submit" name="" class="submit" value="Submit"/>
									</li>
								</ol>
							</form>
						</div>						
					</div>
				</div>
				<br class="clear"/>
			</div>
		</div>
		<!-- Page Content END -->	


</asp:Content>

