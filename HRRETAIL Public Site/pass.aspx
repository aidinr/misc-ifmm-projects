<%@ Page Language="C#"%>
<html>
<head>
<title>Hello World!</title>
</head>
<body>

<script language="C#" runat="server">

public static string GenerateRandomString(int length) 
{
   
            //Removed O, o, 0, l, 1 
            string allowedLetterChars = "abcdefghjkmnpqrstuvwxyz"; 
            string allowedNumberChars = "23456789"; 
            char[] chars = new char[length]; 
            bool useLetter =  true; 
            Random rd = new Random(); 
            for (int i = 0; i < length; i++)
            { 
                if (useLetter) 
                { 
                    chars[i] = allowedLetterChars[rd.Next(0, allowedLetterChars.Length)]; 
                    useLetter = false; 
                } 
                else 
                { 
                    chars[i] = allowedNumberChars[rd.Next(0, allowedNumberChars.Length)]; 
                    useLetter = true; 
                } 

            } 

            return new string(chars); 
}  
    
</script>

<% Response.Write("<p>Random Password: " + GenerateRandomString(8) + "</p>"); %>

</body>
</html>