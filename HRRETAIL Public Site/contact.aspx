﻿<%@ Page Title="" Language="VB" MasterPageFile="~/HRMaster1.master" AutoEventWireup="false" CodeFile="contact.aspx.vb" Inherits="contact" %>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript" >
    $(document).ready(function () {
        if (getParameterByName("careers") == "true") {
            $("#selectorid").html("Career Opportunities");
            $("#Area_of_Interest").val("Career Opportunities");
        }
    });

 function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
</script>
<!-- Page Content START -->
		<div class="page-content">
			<div class="top-section fluid-padding">
				<h1>contact</h1>
				<h2>we would <span class="red">love</span> to hear from you </h2>
			</div><em class="latlong cms cmsFill_Static cmsType_TextSingle cmsName_Contact_Map" data-latlong="<%= CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Contact_Map") %>"></em><em class="zoom cms cmsFill_Static cmsType_TextSingle cmsName_Contact_Map_Zoom_Number" data-zoom="<%= CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Contact_Map_Zoom_Number") %>"></em>
			<div id="contacts-map" class="locationMap gmap3"></div>
			<div class="contact-details fluid-padding">

				<h2 class="thanks cms cmsType_TextMulti cmsName_Thanks"></h2>
				<form method="post" action="/" class="site-form contact">
					<ol>
						<li><input type="text" name="Name" class="required d1" value="" alt="First &amp; Last Name"/></li>
						<li class="spf"><input type="text" class="required email"  name="E-mail" value=""/></li>
						<li><input type="text" name="Email" class="required email d2" value="" alt="Email Address"/></li>
						<li>
							<input name="Area_of_Interest" type="hidden" id="Area_of_Interest" />
							<div class="options">
							 <a class="select" id="selectorid">Area of Interest</a>
							 <p>
							  <a rel="General Information">General Information</a>
							  <a rel="Signup for Daily Clips">Signup for Daily Clips</a>
							  <a rel="Signup for Broker Blasts">Signup for Broker Blasts</a>
							  <a rel="Portfolio Request">Portfolio Request</a>
							  <a rel="Career Opportunities">Career Opportunities</a>
							  <a rel="Interest Not Specified">Other</a>
							 </p>
							</div>
						</li>
						<li class="spf">
							<textarea class="required" name="Message" cols="2" rows="3"></textarea>
						</li>
						<li class="clear full-width">
							<textarea name="Comments" class="required" cols="2" rows="3"></textarea>
						</li>
						<li class="clear"><input type="submit" name="" class="submit" value="SUBMIT"/><span class="cms cmsFill_Static cmsType_TextSingle cmsName_Contact_Page_Recipient"></span></li>
					</ol>
				</form>
				<div id="contactLocations" class="cmsGroup cmsName_Contact_Locations">
					<!--
					<p class="cmsGroupItem">
						<strong class="city cms cmsType_TextSingle cmsName_City">City, XY</strong>
						<em class="cms cmsType_TextMulti cmsName_Address">
						1234 Wisconsin Avenue<br/>
						Suite 123<br/>
						City, XY 12345<br/>
						Telephone: 301.555.1212<br/>
						Facsimile: 301.555.2222						</em>
					</p>
					-->
                    <%
                           For i As Integer = 1 To 20 - 1
                                 If Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Contact_Locations_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
                            %>
                            


                    <p class="cmsGroupItem">
					<strong class="city cms cmsType_TextSingle cmsName_Contact_Locations_Item_<%=i%>_City">Washington, DC</strong>
					<em class="cms cmsType_TextMulti cmsName_Contact_Locations_Item_<%=i%>_Address">
					 7201 Wisconsin Avenue <br/>
					 Suite 600 <br/>
					 Bethesda, MD 20814 <br/>
					 Telephone: 301.656.3030 <br/>
					 Fax: 301.555.1212
					</em>
					<em class="cms location cmsFill_Static cmsType_TextSingle cmsName_Contact_Locations_Item_<%=i%>_Location" data-latlong="<%= CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Contact_Locations_Item_" & i & "_Location") %>"></em>
					</p>

                              <%
                              End If
                          Next
                          %>

				<%--	<p class="cmsGroupItem">
					<strong class="city cms cmsType_TextSingle cmsName_Contact_Locations_Item_1_City">Washington, DC</strong>
					<em class="cms cmsType_TextMulti cmsName_Contact_Locations_Item_1_Address">
					 7201 Wisconsin Avenue <br/>
					 Suite 600 <br/>
					 Bethesda, MD 20814 <br/>
					 Telephone: 301.656.3030 <br/>
					 Fax: 301.555.1212
					</em>
					<em class="cms location cmsType_TextSingle cmsName_Contact_Locations_Item_1_Location">38.888, -77.499</em>
					</p>--%>

				</div>
			</div>
		</div>
		<!-- Page Content END -->	
</asp:Content>
