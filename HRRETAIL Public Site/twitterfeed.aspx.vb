﻿Imports System.Web
Imports Newtonsoft.Json
Imports System.Data
Imports System.Collections.Generic

Partial Class twitter_feed
    Inherits System.Web.UI.Page


    Protected Sub twitter_feed_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Clear()
        Response.ContentType = "application/javascript; charset=utf-8"

        Dim results As Integer = 5
        Dim start As Integer
        If Not Request.QueryString("results") Is Nothing Then
            results = Request.QueryString("results")
        End If
        If Not Request.QueryString("start") Is Nothing Then
            start = Request.QueryString("start")
        End If

        Dim TweetsOUT As String = ""
        Dim TweetsFeedSQL As String = ""
        TweetsFeedSQL &= "SELECT "
        TweetsFeedSQL &= "N.*, "
        TweetsFeedSQL &= "ROW_NUMBER() OVER (ORDER BY N.Post_Date desc) AS RowNum "
        TweetsFeedSQL &= "FROM IPM_NEWS N where Type = 3 "

        Dim NoFilterTweetsDT As New DataTable
        NoFilterTweetsDT = DBFunctions.GetDataTable(TweetsFeedSQL)

        If start = 0 Then
            NoFilterTweetsDT.DefaultView.RowFilter = "RowNum >= 1 AND RowNum <= " & results & ""
        Else
            NoFilterTweetsDT.DefaultView.RowFilter = "RowNum >= " & start & " AND RowNum <= " & start + results & ""
        End If

        Dim TweetsFeedDT As New DataTable
        TweetsFeedDT = NoFilterTweetsDT.DefaultView.ToTable

        Dim TweetsResponseList As New TweetsStructure
        TweetsResponseList.tweets = New List(Of TweetStructure)
        TweetsResponseList.datetime = Date.Now

        If TweetsFeedDT.Rows.Count > 0 Then

            Dim TweetResponse As TweetStructure
            For i As Integer = 0 To TweetsFeedDT.Rows.Count - 1
                TweetResponse = New TweetStructure With {.datetime = TweetsFeedDT.Rows(i).Item("Post_Date"), .user = TweetsFeedDT.Rows(i).Item("PublicationTitle"), .account = TweetsFeedDT.Rows(i).Item("PublicationTitle").ToString.Split("@")(1), .content = TweetsFeedDT.Rows(i).Item("Headline"), .image = TweetsFeedDT.Rows(i).Item("Contact")}
                TweetsResponseList.tweets.Add(TweetResponse)
            Next

            TweetsOUT &= "twitterFeed("
            TweetsOUT &= JsonConvert.SerializeObject(TweetsResponseList, Formatting.Indented)
            TweetsOUT &= ")"
            Response.Write(TweetsOUT)
        End If



    End Sub


End Class

Public Class TweetsStructure
    Private datetimeString As String
    Private tweetsDataObject As List(Of TweetStructure)
    'Public Property tweets() As Object
    '    Get
    '        Return tweetsDataObject
    '    End Get
    '    Set(ByVal value As Object)
    '        tweetsDataObject = value
    '    End Set
    'End Property

    Public Property datetime() As String
        Get
            Return datetimeString
        End Get
        Set(ByVal value As String)
            datetimeString = value
        End Set
    End Property

    Public Property tweets() As List(Of TweetStructure)
        Get
            Return tweetsDataObject
        End Get
        Set(ByVal value As List(Of TweetStructure))
            tweetsDataObject = value
        End Set
    End Property


End Class

Public Class TweetStructure
    Private datetimeString As String
    Private userString As String
    Private accountString As String
    Private contentString As String
    Private imageString As String
    Public Property datetime() As String
        Get
            Return datetimeString
        End Get
        Set(ByVal value As String)
            datetimeString = value
        End Set
    End Property
    Public Property user() As String
        Get
            Return userString
        End Get
        Set(ByVal value As String)
            userString = value
        End Set
    End Property
    Public Property account() As String
        Get
            Return accountString
        End Get
        Set(ByVal value As String)
            accountString = value
        End Set
    End Property
    Public Property content() As String
        Get
            Return contentString
        End Get
        Set(ByVal value As String)
            contentString = value
        End Set
    End Property
    Public Property image() As String
        Get
            Return imageString
        End Get
        Set(ByVal value As String)
            imageString = value
        End Set
    End Property
End Class
