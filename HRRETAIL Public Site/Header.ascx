﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Header.ascx.vb" Inherits="Header" %>
<%@ Import Namespace="System.IO" %>
<!-- Header START -->
		<div class="header fluid-padding">
			<h1 class="left"><a href="./" class="link">H&amp;R Retail</a></h1>
<!--
			<div class="right">
				<form method="post" action="/" class="right mb20">
					<fieldset>
						<legend>Site search form</legend>
						<input type="text" name="" value="" id="f1" alt="Search Site" />
						<input type="submit" name="" value="" id="f2"/>
						<br class="clear"/>
					</fieldset>
				</form> 		
			</div>
-->
			<div class="right clear menu-holder">
				<a class="mobile-navigation">MENU</a>
				<ul class="menu">
					<li>
						<a href="about">about us</a>
						<ul>
							<li class="subAbout"><a href="about">About H&amp;R Retail</a></li>
							<li><a href="history">History</a></li>
							<li><a href="team">Our Team</a></li>
							<li><a href="retail-solutions">Retail Solutions</a></li>
							<li><a href="news">News &amp; Daily Clips</a></li>
							<li><a href="resources">Resources</a></li>
                            <li><a href="careers">Careers</a></li>
							<li><a href="contact">Contact</a></li>
						</ul>
					</li>
					<li><a href="properties">properties</a></li>
					<li><a href="retailers-and-restaurants">retailers &amp; restaurants</a></li>
					<li><a href="investment-sales">Investment sales</a></li>
					<li><a href="urban">H&amp;R Urban</a></li>
				</ul> 
			</div>   			
			<br class="clear"/>
		</div>
		<!-- Header END -->
