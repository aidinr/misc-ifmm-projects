var timelineJS = 1;
jQuery.fn.timelinr = function(options){
	// default plugin settings
	settings = jQuery.extend({
		containerDiv: 				'#timeline',		// value: any HTML tag or #id, default to #timeline
		datesDiv: 					'#timeDates',			// value: any HTML tag or #id, default to #timeDates
		datesSelectedClass: 		'selected',			// value: any class, default to selected
		datesSpeed: 				'normal',			// value: integer between 100 and 1000 (recommended) or 'slow', 'normal' or 'fast'; default to normal
		timeEventDiv: 					'#timeEvent',			// value: any HTML tag or #id, default to #timeEvent
		timeEventSelectedClass: 		'selected',			// value: any class, default to selected
		timeEventSpeed: 				'fast',				// value: integer between 100 and 1000 (recommended) or 'slow', 'normal' or 'fast'; default to fast
		timeEventTransparency: 		0,				// value: integer between 0 and 1 (recommended), default to 0.2
		timeEventTransparencySpeed: 	500,				// value: integer between 100 and 1000 (recommended), default to 500 (normal)
		prevButton: 				'#timeline .prev',			// value: any HTML tag or #id, default to #prev
		nextButton: 				'#timeline .next',			// value: any HTML tag or #id, default to #next
		arrowKeys: 					'false',			// value: true | false, default to false
		startAt: 					1					// value: integer, default to 1 (first)
	}, options);

	$(function(){


		$('#timeEvent li').css('width', ($('#timeEvent').outerWidth()) +'px');
		$('#timeEvent li:eq(0)').css('visibility','visible');

		$('#timeEvent li:eq(0)').css('visibility','visible')
		setTimeout("$('#timeEvent li').css('visibility','visible')",450);

		// setting variables... many of them
		var howManyDates = $(settings.datesDiv+' li').length;
		var howManytimeEvent = $(settings.timeEventDiv+' li').length;
		var currentDate = $(settings.datesDiv).find('a.'+settings.datesSelectedClass);
		var currentIssue = $(settings.timeEventDiv).find('li.'+settings.timeEventSelectedClass);
		var widthContainer = $(settings.containerDiv).width();
		var heightContainer = $(settings.containerDiv).height();
		var widthtimeEvent = $(settings.timeEventDiv).width();
		var heighttimeEvent = $(settings.timeEventDiv).height();
		var widthIssue = $(settings.timeEventDiv+' li').width();
		var heightIssue = $(settings.timeEventDiv+' li').height();
		var widthDates = $(settings.datesDiv).width();
		var heightDates = $(settings.datesDiv).height();
		var widthDate = $(settings.datesDiv+' li').width();
		var heightDate = $(settings.datesDiv+' li').height();
		// set positions!
			$(settings.timeEventDiv).width(widthIssue*howManytimeEvent);
			$(settings.datesDiv).width(widthDate*howManyDates).css('marginLeft',widthContainer/2-widthDate/2);
			var defaultPositionDates = parseInt($(settings.datesDiv).css('marginLeft').substring(0,$(settings.datesDiv).css('marginLeft').indexOf('px')));
		
		$(settings.datesDiv+' a').click(function(event){
			event.preventDefault();
			// first vars
			var whichIssue = $(this).text();
			var currentIndex = $(this).parent().prevAll().length;
			// moving the elements
			$(settings.timeEventDiv).animate({'marginLeft':-widthIssue*currentIndex},{queue:false, duration:settings.timeEventSpeed});
			$(settings.timeEventDiv+' li').animate({'opacity':settings.timeEventTransparency},{queue:false, duration:settings.timeEventSpeed}).removeClass(settings.timeEventSelectedClass).eq(currentIndex).addClass(settings.timeEventSelectedClass).fadeTo(settings.timeEventTransparencySpeed,1);
			// prev/next buttons now disappears on first/last issue | bugfix from 0.9.51: lower than 1 issue hide the arrows | bugfixed: arrows not showing when jumping from first to last date
			if(howManyDates == 1) {
				$(settings.prevButton+','+settings.nextButton).fadeOut('fast');
			} else if(howManyDates == 2) {
				if($(settings.timeEventDiv+' li:first-child').hasClass(settings.timeEventSelectedClass)) {
					$(settings.prevButton).fadeOut('fast');
				 	$(settings.nextButton).fadeIn('fast');
				} 
				else if($(settings.timeEventDiv+' li:last-child').hasClass(settings.timeEventSelectedClass)) {
					$(settings.nextButton).fadeOut('fast');
					$(settings.prevButton).fadeIn('fast');
				}
			} else {
				if( $(settings.timeEventDiv+' li:first-child').hasClass(settings.timeEventSelectedClass) ) {
					$(settings.nextButton).fadeIn('fast');
					$(settings.prevButton).fadeOut('fast');
				} 
				else if( $(settings.timeEventDiv+' li:last-child').hasClass(settings.timeEventSelectedClass) ) {
					$(settings.prevButton).fadeIn('fast');
					$(settings.nextButton).fadeOut('fast');
				}
				else {
					$(settings.nextButton+','+settings.prevButton).fadeIn('slow');
				}	
			}
			// now moving the dates
			$(settings.datesDiv+' a').removeClass(settings.datesSelectedClass);
			$(this).addClass(settings.datesSelectedClass);
				$(settings.datesDiv).animate({'marginLeft':defaultPositionDates-(widthDate*currentIndex)},{queue:false, duration:'settings.datesSpeed'});
		});

		$(settings.nextButton).bind('click', function(event){
			event.preventDefault();
				var currentPositiontimeEvent = parseInt($(settings.timeEventDiv).css('marginLeft').substring(0,$(settings.timeEventDiv).css('marginLeft').indexOf('px')));
				var currentIssueIndex = currentPositiontimeEvent/widthIssue;
				var currentPositionDates = parseInt($(settings.datesDiv).css('marginLeft').substring(0,$(settings.datesDiv).css('marginLeft').indexOf('px')));
				var currentIssueDate = currentPositionDates-widthDate;
				if(currentPositiontimeEvent <= -(widthIssue*howManytimeEvent-(widthIssue))) {
					$(settings.timeEventDiv).stop();
					$(settings.datesDiv+' li:last-child a').click();
				} else {
					if (!$(settings.timeEventDiv).is(':animated')) {
						$(settings.timeEventDiv).animate({'marginLeft':currentPositiontimeEvent-widthIssue},{queue:false, duration:settings.timeEventSpeed});
						$(settings.timeEventDiv+' li').animate({'opacity':settings.timeEventTransparency},{queue:false, duration:settings.timeEventSpeed});
						$(settings.timeEventDiv+' li.'+settings.timeEventSelectedClass).removeClass(settings.timeEventSelectedClass).next().fadeTo(settings.timeEventTransparencySpeed, 1).addClass(settings.timeEventSelectedClass);
						$(settings.datesDiv).animate({'marginLeft':currentIssueDate},{queue:false, duration:'settings.datesSpeed'});
						$(settings.datesDiv+' a.'+settings.datesSelectedClass).removeClass(settings.datesSelectedClass).parent().next().children().addClass(settings.datesSelectedClass);
					}
				}
			// prev/next buttons now disappears on first/last issue | bugfix from 0.9.51: lower than 1 issue hide the arrows
			if(howManyDates == 1) {
				$(settings.prevButton+','+settings.nextButton).fadeOut('fast');
			} else if(howManyDates == 2) {
				if($(settings.timeEventDiv+' li:first-child').hasClass(settings.timeEventSelectedClass)) {
					$(settings.prevButton).fadeOut('fast');
				 	$(settings.nextButton).fadeIn('fast');
				} 
				else if($(settings.timeEventDiv+' li:last-child').hasClass(settings.timeEventSelectedClass)) {
					$(settings.nextButton).fadeOut('fast');
					$(settings.prevButton).fadeIn('fast');
				}
			} else {
				if( $(settings.timeEventDiv+' li:first-child').hasClass(settings.timeEventSelectedClass) ) {
					$(settings.prevButton).fadeOut('fast');
				} 
				else if( $(settings.timeEventDiv+' li:last-child').hasClass(settings.timeEventSelectedClass) ) {
					$(settings.nextButton).fadeOut('fast');
				}
				else {
					$(settings.nextButton+','+settings.prevButton).fadeIn('slow');
				}	
			}
		});

		$(settings.prevButton).click(function(event){
			event.preventDefault();
				var currentPositiontimeEvent = parseInt($(settings.timeEventDiv).css('marginLeft').substring(0,$(settings.timeEventDiv).css('marginLeft').indexOf('px')));
				var currentIssueIndex = currentPositiontimeEvent/widthIssue;
				var currentPositionDates = parseInt($(settings.datesDiv).css('marginLeft').substring(0,$(settings.datesDiv).css('marginLeft').indexOf('px')));
				var currentIssueDate = currentPositionDates+widthDate;
				if(currentPositiontimeEvent >= 0) {
					$(settings.timeEventDiv).stop();
					$(settings.datesDiv+' li:first-child a').click();
				} else {
					if (!$(settings.timeEventDiv).is(':animated')) {
						$(settings.timeEventDiv).animate({'marginLeft':currentPositiontimeEvent+widthIssue},{queue:false, duration:settings.timeEventSpeed});
						$(settings.timeEventDiv+' li').animate({'opacity':settings.timeEventTransparency},{queue:false, duration:settings.timeEventSpeed});
						$(settings.timeEventDiv+' li.'+settings.timeEventSelectedClass).removeClass(settings.timeEventSelectedClass).prev().fadeTo(settings.timeEventTransparencySpeed, 1).addClass(settings.timeEventSelectedClass);
						$(settings.datesDiv).animate({'marginLeft':currentIssueDate},{queue:false, duration:'settings.datesSpeed'});
						$(settings.datesDiv+' a.'+settings.datesSelectedClass).removeClass(settings.datesSelectedClass).parent().prev().children().addClass(settings.datesSelectedClass);
					}
				}
			// prev/next buttons now disappears on first/last issue | bugfix from 0.9.51: lower than 1 issue hide the arrows
			if(howManyDates == 1) {
				$(settings.prevButton+','+settings.nextButton).fadeOut('fast');
			} else if(howManyDates == 2) {
				if($(settings.timeEventDiv+' li:first-child').hasClass(settings.timeEventSelectedClass)) {
					$(settings.prevButton).fadeOut('fast');
				 	$(settings.nextButton).fadeIn('fast');
				} 
				else if($(settings.timeEventDiv+' li:last-child').hasClass(settings.timeEventSelectedClass)) {
					$(settings.nextButton).fadeOut('fast');
					$(settings.prevButton).fadeIn('fast');
				}
			} else {
				if( $(settings.timeEventDiv+' li:first-child').hasClass(settings.timeEventSelectedClass) ) {
					$(settings.prevButton).fadeOut('fast');
				} 
				else if( $(settings.timeEventDiv+' li:last-child').hasClass(settings.timeEventSelectedClass) ) {
					$(settings.nextButton).fadeOut('fast');
				}
				else {
					$(settings.nextButton+','+settings.prevButton).fadeIn('slow');
				}	
			}
		});


		// keyboard navigation, added since 0.9.1
		if(settings.arrowKeys=='true') {
				$(document).keydown(function(event){
					if (event.keyCode == 39) { 
				       $(settings.nextButton).click();
				    }
					if (event.keyCode == 37) { 
				       $(settings.prevButton).click();
				    }
				});
		}
		// default position startAt, added since 0.9.3
		$(settings.datesDiv+' li').eq(settings.startAt-1).find('a').trigger('click');
	});
};
