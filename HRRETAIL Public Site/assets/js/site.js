document.write('<style type="text/css">body{visibility:hidden;opacity: 0}</style>');
var isMSIE = /*@cc_on!@*/ 0;
var homeAuto;
var oldHash;
var projectFinder;
var ajaxData;
var points = [];
var InfoPopOpened = 0;
var moving = 0;
var hash = 'home'
var homeData;
var homeTitle;
var LastLoadTime;
var SelectedSpace = 0;
var google;
var maps;
var r;
var a = 1;
var _gaq = _gaq || [];
var mobile = checkMobile();
mobile=false;
window.google = window.google || {};
google.maps = google.maps || {};

if(window.self === window.top) {

    var current_hash = window.location.hash;

    history.navigationMode = 'compatible';
    $('*').unbind();
    $(window).unload(function () {
        scroll(0, 0);
        $('*').unbind();
        $('input[type=text], select, textarea').val('');
    });

    WebFontConfig = {
        google: {
            families: ['Open+Sans:400,300,300italic,400italic,700,700italic,800:latin']
        },
        active: function () {
            $('body').css('opacity', '1');
        }
    };
    (function () {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();

    // if(!mobile) {
    //   $(window).resize(function () {
    //     if(this.resizeTO) clearTimeout(this.resizeTO);
    //     this.resizeTO = setTimeout(function () {
    //             hash = window.location.hash.substr(0, window.location.hash.indexOf('!')).replace('#', '');
    //             if(!isMSIE && hash!='news' && hash!='properties' && hash!='retailers-and-restaurants') ajax_content();
    //             points = [];
    //             calculate_dimensions();
    //         },
    //         100);
    //   });
    // }

    // ON Hash Change
    $(window).on('hashchange', function () {
        if(window.location.hash.indexOf('?') > 0) document.location = String(document.location.href).replace('#', '').split('!')[0];
        else {
            if(hash && oldHash && hash.split('!')[0] == window.location.hash.split('!')[0].substr(1) && window.location.hash.split('!')[1] && oldHash !=
                window.location.hash) {
                if($('.info-pops').hasClass('info-pops')) {
                    var id = window.location.hash.split('!')[1];
                    if(id) $('.info-pops a.id' + id).click();
                }
            } else {
                hash = window.location.hash.replace('#', '');
                if(new Date().getTime() - LastLoadTime > 400) ajax_content();
            }
        }
    });

    if(!readCookie('SiteEdit') && window.location.search.substr(0, 1) != '?') {
        if(getPage() != '' && current_hash.length > 2) {
            document.location = './' + current_hash;
        } else if(getPage() != '' && current_hash.length < 2) {
            document.location = './#' + getPage();
        } else if(getPage() == '' && current_hash.length < 2) {
            document.location = './#home';
        }
    }
    $(document).ready(function () {

        if(readCookie('SiteEdit') || window.location.search.substr(0, 5) == '?edit') {
            $('body').css('visibility', 'visible').addClass('loaded');
            if(readCookie('SiteEdit')) $('body').addClass('cmsEditing');
            var css = document.createElement('link');
            css.setAttribute("rel", "stylesheet");
            css.setAttribute("type", "text/css");
            css.setAttribute("href", '/cms/assets/cms.css');
            $('head').append(css);
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = '/cms/assets/cms.js';
            $('head').append(script);
        }

        homeTitle = document.title;
        homeData = $('<textarea/>').html($('#homePage').html()).val();


if(readCookie('SiteEdit')) {
 $('.select-site-plan a').each(function () {
  $(this).attr('href', window.location.href.split("?")[0] + '?planimage=' + $(this).attr('rel') + '&t=' + new Date().getTime());
 });
loadCar();
}

        if((readCookie('SiteEdit') || window.location.search.substr(0, 1) == '?') && getPage() == '') {
            $('.page-content:eq(0)').html(homeData);
        }

        $('a[href="' + getPage() + '"]').addClass('active');
        $('.header .menu a[href="' + getPage() + '"]').parent().parent().parent().find('a:eq(0)').addClass('active');

        $(':input').attr('autocomplete', 'off');

        $('body').append('<div class="sticky-contacts"></div>');
        $('.sticky-contacts').load('sticky-contact.txt', function () {
            $('.show-hidden-form').on('click', function () {
                $('.sticky-contacts').removeClass('propertyaspx');
                $(this).parent().find('.hidden-form').fadeToggle();
            });
            $('.sticky-contacts .close-button').on('click', function () {
                $('.sticky-contacts').removeClass('propertyaspx');
                $(this).parent().fadeOut();
            });
        });

        /* Mobile navigation (collapsing) */
        $('.mobile-navigation').on('click', function () {
            $(this).toggleClass('active');
            $('.menu').stop(true, true).slideToggle(300);
        });

        /* Hidden forms */
        $('.show-hidden-form').on('click', function () {
            $(this).parent().find('.hidden-form').fadeToggle();
        });

        if(window.location.search != '?edit' && !readCookie('SiteEdit')) {
            if(window.location.hash) {
                hash = window.location.hash.replace('#', '');
                ajax_content();
            }
        }
        if(!window.location.hash) pageInit();

        $('.menu ul li').click(function () {
            $(this).parent().fadeOut(222, function () {
                setTimeout(" $('.menu ul').removeAttr('style')", 800);
            });
        });

        (new Image).src = '/assets/images/zoom.png';
        (new Image).src = '/assets/images/zoom.cur';

        setTimeout("$('body').css('opacity', '1');$('body').css('visibility', 'visible');$('html').css('background', '#333');", 800);

        if(!readCookie('SiteEdit')) {
            $('body').append(
                '<div id="contactStuff"><iframe id="cccontact" src="/cc/contact"></iframe><iframe id="ccbroker" src="/cc/broker"></iframe><iframe id="ccclips" src="/cc/clips"></iframe></div>'
            );
        }

        if($('#sitePlanForm').attr('id')) {
            $('#sitePlanForm').unbind();
            $('#sitePlanForm').submit(function () {
                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    if(typeof SitePlanPluginClose == 'function') {
                      SitePlanPluginClose();
                      $('siteplanplugindataelement').remove();
                    }
                    document.location = String(document.location).split('?')[0] + '?v=' + new Date().getTime();
                });
                return false;
            });
        }

        // End Document Ready
    });

    /* Styled selects */

    function pretty_selects() {
        $('.styled-select').each(function () {
            $(this).wrap('<div class="pretty-select" />');

            $('<div class="select-text" />').prependTo($(this).parent('.pretty-select'));
            $(this).parent().children('.select-text').text($(this).attr('name').replace(/_/gi, ' '));
        }).change(function () {
            var t = $(this).children('option:selected').text().trim();
            if($('.properties-search').hasClass('properties-search')) {
                var t2 = t.substr(0, 17);
                if(t != t2) t = t2 + '' + '\u2026';
            }
            if($(this).val() == '') $(this).parent().children('.select-text').text($(this).attr('name').replace(/_/gi, ' '));
            else {
              if(!$('body').hasClass('propertyaspx')) $(this).parent().children('.select-text').text(t);
            }
        });
    }

    /* Escape key */
    $(document).keyup(function (e) {
        if(e.keyCode == 27) {
            if($('.growMe').hasClass('growMe')) hide_active_info_pop();
        }
    });

    /* Map styles */
    var mapStyle = [{
        featureType: 'water',
        elementType: 'geometry',
        stylers: [{
            hue: '#909090'
        }, {
            saturation: -100
        }, {
            lightness: -26
        }, {
            visibility: 'on'
        }]
    }, {
        featureType: 'landscape',
        elementType: 'all',
        stylers: [{
            hue: '#dfdfdf'
        }, {
            saturation: -100
        }, {
            lightness: -2
        }, {
            visibility: 'simplified'
        }]
    }, {
        featureType: 'road.highway',
        elementType: 'all',
        stylers: [{
            hue: '#959595'
        }, {
            saturation: -100
        }, {
            lightness: -9
        }, {
            visibility: 'simplified'
        }]
    }, {
        featureType: 'landscape.natural',
        elementType: 'all',
        stylers: [{
            hue: '#e1e0e0'
        }, {
            saturation: -89
        }, {
            lightness: -7
        }, {
            visibility: 'on'
        }]
    }, {
        featureType: 'poi',
        elementType: 'all',
        stylers: [{
            hue: '#e1e0e0'
        }, {
            saturation: -96
        }, {
            lightness: 46
        }, {
            visibility: 'on'
        }]
    }, {
        featureType: 'road.arterial',
        elementType: 'all',
        stylers: [{
            hue: '#c7c7c7'
        }, {
            saturation: -100
        }, {
            lightness: 5
        }, {
            visibility: 'simplified'
        }]
    }];
}

function SitePlan() {

 //setTimeout("$('.map-img').css('opacity',1)", 900);

 if(!readCookie('SiteEdit')) {
    if(typeof(csiteplan)==='undefined') $('<script src="/assets/js/siteplan.js"></script>').appendTo('head');
    else csiteplan.init();
 }
 else {
     if(!getq('planimage')) $('#site-plan-container').replaceWith('<h2>Select a Site Plan</h2>');
     else {
	$('a[rel="'+ getq('planimage') +'"]').addClass('selected');
        if (typeof SitePlanPluginClose == 'function') SitePlanPluginClose();
        if (typeof SitePlanPluginOpen == 'function') SitePlanPluginOpen();
     }
 }
}


function initPropertyMap() {
    if($('#area-map.active div div div div div').is('div')) return false;
    $('.locationMap').css('opacity', 0);
    var markers = [];
    var ll = $('.locationMapMarkers a:eq(0)').attr('href').replace('<', '');
    $('.locationMapMarkers a').each(function () {
        var marker = {};
        marker.latLng = [];
        marker.latLng[0] = $.trim($(this).find('em:eq(0)').text().split(",")[0]);
        marker.latLng[1] = $.trim($(this).find('em:eq(0)').text().split(",")[1]);
        marker.ll = escape($.trim($(this).find('em:eq(0)').text().split(",")[0]) + ',' + $.trim($(this).find('em:eq(0)').text().split(",")[1]));
        markers.push(marker);
    });

    $("#area-map").gmap3({
        map: {
            options: {
                center: [Number($('.locationMap:eq(0)').text().replace(/ /g, '').split(",")[0]), Number($('.locationMap:eq(0)').text().replace(/ /g, '').split(
                    ",")[1])],
                zoom: 16,
                mapTypeId: "RoadMap",
                zoomControl: true,
                panControl: true,
                draggable: true,
                scaleControl: false,
                scrollwheel: false,
                navigationControl: false,
                streetViewControl: false,
                mapTypeControlOptions: {
                    mapTypeIds: ["RoadMap", google.maps.MapTypeId.SATELLITE]
                }
            }
        },
        styledmaptype: {
            id: "RoadMap",
            options: {
                name: "Map",
            },
            styles: mapStyle
        },
        marker: {
            values: markers,
            options: {
                icon: new google.maps.MarkerImage("/assets/images/pins/pin.png")
            },
            events: {
                click: function () {
                    var win = window.open(ll, '_blank');
                    win.focus();
                },
            }
        }
    });
}

function initContactMap() {
    $('.locationMap').css('opacity', 0);
    var markers = [];
    $('#contactLocations p').each(function () {
        var marker = {};
        marker.latLng = [];
        marker.latLng[0] = $(this).find('.location').attr('data-latlong').split(",")[0];
        marker.latLng[1] = $(this).find('.location').attr('data-latlong').split(",")[1];
        marker.options = {};
        marker.options.icon = '/assets/images/marker.png';
        marker.clickable = true;
        marker.options.title = $(this).find('strong').text().replace('EDIT', '');
        marker.events = {};
        var e = this;
        e.l = 'https://maps.google.com/?q=' + escape($(this).find('em.location').attr('data-latlong')) + '&z=17&type=map';
        marker.events.click = function () {
            var win = window.open(e.l, '_blank');
            win.focus();
        }
        markers.push(marker);
    });

    var n = Number($('.zoom').attr('data-zoom'));
    if(n > 1) n = n;
    else n = 1;

    $(".locationMap").gmap3({
        map: {
            options: {
                center: [Number($('.latlong:eq(0)').attr('data-latlong').replace(/ /g, '').split(",")[0]), Number($('.latlong:eq(0)').attr('data-latlong').replace(/ /g, '').split(",")[1])],
                zoom: n,
                mapTypeId: "RoadMap",
                zoomControl: true,
                panControl: true,
                draggable: true,
                scaleControl: false,
                scrollwheel: false,
                navigationControl: false,
                streetViewControl: false,
                mapTypeControlOptions: {
                    mapTypeIds: ["RoadMap", google.maps.MapTypeId.SATELLITE]
                }
            }
        },
        styledmaptype: {
            id: "RoadMap",
            options: {
                name: "Map",
            },
            styles: mapStyle
        },
        marker: {
            values: markers
        }
    });

    $('.locationMap').animate({
        opacity: 1
    }, 500, function () {
        // Animation complete.
    });
}

function calculate_dimensions() {
    var wWidth = $(window).outerWidth();
    var wHeight = $(window).outerHeight();

    if( wWidth < 766 && $('.team-list li').is('li')) {
        $('.team-list').css('width', ($('.team-list li').filter(function (index) {
            return $('.team-list li:eq(0)').offset().top == $(this).offset().top;
        }).length * ($('.team-list li').outerWidth()) + 'px'));
    }

    if($('.main-slider ul').is('ul')) {
        if(wWidth <= 799) $('.main-slider ul li .info').css('width', Math.floor(wWidth * 0.8) + 'px').css('marginLeft', Math.floor(wWidth * 0.1) + 'px');
        else $('.main-slider ul li .info').css('width', '').css('marginLeft', '');
    }

    if($('.retailer-list').is('ul')) {
        if(wWidth <= 799) $('.retailer-list').css('width', ($('.retailer-list li').filter(function (index) {
            return $('.retailer-list li:eq(0)').offset().top == $(this).offset().top;
        }).length * ($('.retailer-list li').outerWidth() + 20) + 'px'));
        else $('.retailer-list').css('width', '').css('marginLeft', '');
    }


    // width this we make sure that menu is always visible when user tries to resize
    if(wWidth > 749) {
        $('.menu').css({
            'display': 'block'
        });
    } else {
        $('.menu').css({
            'display': 'none'
        });
    }
}

function getPage() {
    var nPage = window.location.pathname;
    nPage = nPage.substring(nPage.lastIndexOf('/') + 1);
    return nPage;
}

function loadCar() {
    clearInterval(homeAuto);
    if($('.main-slider ul li p img[longdesc]').attr('longdesc')) {
        $('.main-slider ul li p img[longdesc]:eq(0)').attr('src', $('.main-slider ul li p img[longdesc]:eq(0)').attr('longdesc')).load(function () {
            $(this).removeAttr('longdesc');
            loadCar();
        });
    } else {
        $('.main-slider ul').append($('.main-slider ul').html());
        $('.main-slider ul li .info:eq(0)').fadeIn(400);
        $('.main-slider .left-arrow, .main-slider .right-arrow').fadeIn(450);

        $('.main-slider').hover(function () {
            $(this).addClass('over');
        }, function () {
            $(this).removeClass('over');
        });

        $('.main-slider .left-arrow').click(function () {
            clearInterval(homeAuto);
            if(moving == 1) return false;
            moving = 1;
            $('.main-slider ul li .info:visible').fadeOut(500);
            $('.main-slider ul').prepend($('.main-slider ul li').last());
            $('.main-slider ul li:eq(0)').css('marginLeft', '-' + $('.main-slider ul li:eq(0)').width() + 'px');
            $('.main-slider ul li').animate({
                marginLeft: '0'
            }, 650, function () {
                $('.main-slider ul li .info:eq(0)').fadeIn(500);
                moving = 0;
            });
        });
        $('.main-slider .right-arrow').click(function () {
            clearInterval(homeAuto);
            homePageSlideNext();
        });
        homeAuto = setInterval("homePageSlideNext(1);", 8500);
    }
}

function homePageSlideNext(a) {
    if(moving == 1 || (a && $('.main-slider').hasClass('over'))) return false;
    moving = 1;
    $('.main-slider ul li .info:visible').fadeOut(500);
    $('.main-slider ul li:eq(0)').animate({
        marginLeft: '-' + $('.main-slider ul li:eq(0)').width() + 'px'
    }, 650, function () {
        $('.main-slider ul').append($('.main-slider ul li:eq(0)'));
        $('.main-slider ul li').css('marginLeft', '0');
        moving = 0;
        $('.main-slider ul li .info:eq(0)').fadeIn(500);
    });
}

function pageInit() {

    setTimeout("loadGoogleAnalytics(GoogleAnalyticsAccount)", 1000);

    if($('.main-slider ul li p img').is('img')) {
        $('.main-slider ul li').last().find('p img').attr('src', $('.main-slider ul li').last().find('p img').attr('src')).load(function () {
            loadCar();
        });
    }


    if( !$('.cmsEditing').hasClass('cmsEditing') && $('.info-pop-slider').hasClass('info-pop-slider')) $('.info-pop-slider').not(':has("img")').remove();

    if($('.horizontal-slider').length) {
        var $total_sliders = $('.horizontal-slider').length;
        if($total_sliders) {
            for(var $i = 1; $i <= $total_sliders; $i++) {

                if($('.horizontal-slider ul.slider-' + $i).parent().hasClass('rr')) {
                    $('.horizontal-slider ul.slider-' + $i).carouFredSel({
                        prev: '.left-' + $i,
                        next: '.right-' + $i,
                        auto: {
                            delay: 3000,
                            timeoutDuration: 4500,
                        },
                        scroll: {
                            items: 1
                        }
                    });
                } else if($i == 2) {
                    $('.horizontal-slider ul.slider-' + $i).carouFredSel({
                        prev: '.left-' + $i,
                        next: '.right-' + $i,
                        auto: false,
                        scroll: {
                            items: 1
                        }
                    });
                } else {
                    $('.horizontal-slider ul.slider-' + $i).carouFredSel({
                        prev: '.left-' + $i,
                        next: '.right-' + $i,
                        auto: false,
                        scroll: {
                            items: 1
                        }
                    });
                }
            }
        }
    }
    setTimeout(function () {
        $(':input[alt]').each(function () {
            $(this).val($(this).attr('alt'));
            $(this).focus(function () {
                $(this).val('');
            });
            $(this).blur(function () {
                if(!$(this).val()) $(this).val($(this).attr('alt'));
            });
        });
    }, 900);





    if($('.propDownloads').is('div')) {
        $('.propDownloads').hover(function (){
           $('.propDownloads p').slideDown(234);
        }, function () {
           $('.propDownloads p').slideUp(234);
        });

        $('.propDownloads p a').click(function (){
         $('.propDownloads p').slideUp(234);
        });
    }







    if($('.site-form.contact').is('form')) {

        $('.contact-details div.options').hover(function (){
           $('.contact-details .options p').slideDown(234);
        }, function () {
           $('.contact-details .options p').slideUp(234);
        });

        $('.contact-details div.options a').click(function (){
         $('#Area_of_Interest').val($(this).attr('rel'));
         $('.contact-details div.options a.select').html($(this).text());
         $('.contact-details .options p').slideUp(234);
        });

        $('.site-form.contact').unbind();
        $('.site-form.contact').submit(function () {
            if($('.cmsEditing').hasClass('cmsEditing')) {
                alert('You must log out before sending a message.');
                return false;
            }
            var error = 0;
            $('.site-form.contact :input.required').focus(function () {
                $(this).removeClass('inputerror');
            });
            $(this).find(':input.required').each(function () {
                if(!$(this).parent().hasClass('spf')) {
                    if(!$(this).val() || $(this).val() == $(this).attr('alt') || $(this).val() == $(this).attr('title') || ($(this).hasClass('email') &&
                        /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test($(this).val()) == false)) {
                        $(this).addClass('inputerror');
                        error = 1;
                    }
                }
            });
            if(error == 1) {
                alert('You must provide valid information for all required fields.');
                return false;
            } else {
                if($(this).find('select').val() == 'Signup for Daily Clips') {
                    $('#ccclips').contents().find('#nameId').val($(this).find('.d1').val());
                    $('#ccclips').contents().find('#emailId').val($(this).find('.d2').val());
                    $('#ccclips').contents().find('#commentsId').val($(this).find('.textarea[name=Comments]').val());
                    $('#ccclips').contents().find('input').last().click().css('background', 'red');
                } else if($(this).find('select').val() == 'Signup for Broker Blasts') {
                    $('#ccbroker').contents().find('#nameId').val($(this).find('.d1').val());
                    $('#ccbroker').contents().find('#emailId').val($(this).find('.d2').val());
                    $('#ccbroker').contents().find('input').last().click().css('background', 'red');
                } else {
                    $('#cccontact').contents().find('#nameId').val($(this).find('.d1').val());
                    $('#cccontact').contents().find('#emailId').val($(this).find('.d2').val());
                    $('#cccontact').contents().find('#interestId').val($(this).find(':input[name="Area_of_Interest"]').val());
                    $('#cccontact').contents().find('#commentsId').val($(this).find('textarea[name=Comments]').val());
                    $('#cccontact').contents().find('input').last().click().css('background', 'red');
                    $.post('/submit/contact', $(this).serialize()).done(function (data) {});
                }

                _gaq.push(['_setAccount', GoogleAnalyticsAccount]);
                _gaq.push(['_trackPageview', '/contact_submitted_' + getPage()]);

                $('.site-form.contact').animate({
                    opacity: 0
                }, 500, function () {
                    $('.site-form.contact').css('visibility', 'hidden');
                });
                if($('.thanks').hasClass('thanks')) $('.thanks').show().animate({
                    opacity: 1
                }, 600, function () {});

                $('.sticky-contacts').animate({
                    opacity: 0
                }, 600, function () {
                    $('.sticky-contacts').remove();
                });
                return false;
            }
            return false;
        });
    }




    if($('.site-form.portfolio').is('form')) {
        $('.site-form.portfolio').unbind();
        $('.site-form.portfolio').submit(function () {
            if($('.cmsEditing').hasClass('cmsEditing')) {
                alert('You must log out before sending a message.');
                return false;
            }
            var error = 0;
            $('.site-form.portfolio :input.required').focus(function () {
                $(this).removeClass('inputerror');
            });

            $(this).find(':input.required').each(function () {
                if(!$(this).parent().hasClass('spf')) {
                    if(!$(this).val() || $(this).val() == $(this).attr('alt') || $(this).val() == $(this).attr('title') || ($(this).hasClass('email') &&
                        /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test($(this).val()) == false)) {
                        $(this).addClass('inputerror');
                        error = 1;
                    }
                }
            });
            if(error == 1) {
                alert('You must provide valid information for all required fields.');
                return false;
            } else {
                $('#cccontact').contents().find('#nameId').val($(this).find('#pfr1').val());
                $('#cccontact').contents().find('#emailId').val($(this).find('#pfr2').val());
                $('#portInterest').val('Portfolio Request (' + $('.top-section .left h1:eq(0)').text() + ')');
                $('#cccontact').contents().find('#interestId').val($('#portInterest').val());
                $('#cccontact').contents().find('#commentsId').val($('#pfr3').val());
                $('#cccontact').contents().find('input').last().click().css('background', 'red');
                $.post('/submit/contact', $(this).serialize()).done(function (data) {});

                $('.site-form.portfolio').animate({
                    opacity: 0
                }, 500, function () {
                    $('.site-form.portfolio').css('visibility', 'hidden');
                });

                $('.sticky-contacts').animate({
                    opacity: 0
                }, 600, function () {
                    $('.sticky-contacts').remove();
                });
                return false;
            }
            return false;
        });
    }

    if($('.PortfolioRequest').hasClass('PortfolioRequest')) {
        $('.PortfolioRequest').unbind();
        $('.PortfolioRequest').click(function () {
            $('.site-form.hidden-form.portfolio').closest('.sticky-contacts').addClass('propertyaspx');
            $('.site-form.hidden-form.portfolio').last().fadeToggle();
        });
    }

    if($('.subscription-form .site-form.brokerblast').is('form')) {
        $('.subscription-form .site-form.brokerblast').unbind();
        $('.subscription-form .site-form.brokerblast').submit(function () {
            if($('.cmsEditing').hasClass('cmsEditing')) {
                alert('You must log out before sending a message.');
                return false;
            }
            var error = 0;
            $('.subscription-form .site-form.brokerblast :input.required').focus(function () {
                $(this).removeClass('inputerror');
            });

            $(this).find(':input.required').each(function () {
                if(!$(this).parent().hasClass('spf')) {
                    if(!$(this).val() || $(this).val() == $(this).attr('alt') || $(this).val() == $(this).attr('title') || ($(this).hasClass('email') &&
                        /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test($(this).val()) == false)) {
                        $(this).addClass('inputerror');
                        error = 1;
                    }
                }
            });
            if(error == 1) {
                alert('You must provide valid information for all required fields.');
                return false;
            } else {
                $('#ccbroker').contents().find('#nameId').val($(this).find('#f13').val());
                $('#ccbroker').contents().find('#emailId').val($(this).find('#f2321').val());
                $('#ccbroker').contents().find('input').last().click().css('background', 'red');
                $('.subscription-form .site-form.brokerblast').animate({
                    opacity: 0
                }, 500, function () {
                    $(this).css('visibility', 'hidden');
                });

                $('.signup.show-hidden-form').animate({
                    opacity: 0
                }, 600, function () {
                    $(this).css('visibility', 'hidden');
                });
                return false;
            }
            return false;
        });
    }

    if($('.subscription-form .site-form.clips').is('form')) {
        $('.subscription-form .site-form.clips').unbind();
        $('.subscription-form .site-form.clips').submit(function () {
            if($('.cmsEditing').hasClass('cmsEditing')) {
                alert('You must log out before sending a message.');
                return false;
            }
            var error = 0;
            $('.subscription-form .site-form.clips :input.required').focus(function () {
                $(this).removeClass('inputerror');
            });

            $(this).find(':input.required').each(function () {
                if(!$(this).parent().hasClass('spf')) {
                    if(!$(this).val() || $(this).val() == $(this).attr('alt') || $(this).val() == $(this).attr('title') || ($(this).hasClass('email') &&
                        /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test($(this).val()) == false)) {
                        $(this).addClass('inputerror');
                        error = 1;
                    }
                }
            });
            if(error == 1) {
                alert('You must provide valid information for all required fields.');
                return false;
            } else {
                $('#ccclips').contents().find('#nameId').val($(this).find('#f2f223e').val());
                $('#ccclips').contents().find('#emailId').val($(this).find('#f2f23').val());
                $('#ccclips').contents().find('input').last().click().css('background', 'red');
                $('.subscription-form .site-form.clips').animate({
                    opacity: 0
                }, 500, function () {
                    $(this).css('visibility', 'hidden');
                });

                $('.signup.show-hidden-form').animate({
                    opacity: 0
                }, 600, function () {
                    $(this).css('visibility', 'hidden');
                });
                return false;
            }
            return false;
        });
    }




    if($('#properties-word-search').is('form')) {
        $('#properties-word-search input').prop('disabled', true);
        $('#properties-search select').prop('disabled', true);
    }
    if($('.tweet').hasClass('tweet')) {
        twit();
    }

    if ($('#timeline').is('div')) {
        timeline();
    }

    if($('.downloadDocs').hasClass('downloadDocs')) {
        $('<iframe />', {
            name: 'downDoc',
            id: 'downDoc',
        }).appendTo('.page-content');
        $('.downloadDocs').change(function () {
            $('#downDoc').attr('src', 'http://' + String(window.location.hostname) + $(this).val());
            $('.downloadDocs').val('');
        });
    }

    if($('.imgG').hasClass('imgG')) {
        // Greyscale client logos
        $(".imgG a").unbind();

        $(".imgG a").hover(function () {
            $(this).find('img.grey').stop().animate({
                opacity: 0
            }, 300);
        }, function () {
            $(this).find('img.grey').stop().animate({
                opacity: 1
            }, 200);
        });
    }

    if($('#p-search-results').is('div')) {
        var store = window.location.hash.split('!')[1];
        if(store) $('#Anchor').val(store);

        propertyfinder();
        $('#Size_of_Space, #State, #County, #Anchor').change(function () {
            propertyfinder();
        });
        $('#properties-word-search').submit(function () {
            if($('#properties-q').val().length > 0) propertyfinder('keywords');
            return false;
        });
    }

    if($('#retailers-and-restaurants-q').is('input')) {
        $('#retailers-and-restaurants-q').keyup(function () {
            if(String($('#retailers-and-restaurants-q').val()).length > 0) {
                $('.retailer-list li.rollover-item').hide();
                $('.retailer-list li.rollover-item').filter(function (index) {
                    return $(this).text().toLowerCase().replace(/[^a-z0-9-]/g, '').indexOf($('#retailers-and-restaurants-q').val().toLowerCase().replace(
                        /[^a-z0-9-]/g, '')) > -1
                }).show();
            } else {
                $('.retailer-list li.rollover-item').show();
            }

        });

        loadRRImg();

        $('.retailer-list').css('opacity', 0);
        setTimeout(function () {
            $('.retailer-list').css('opacity', 0).animate({
                opacity: 1
            }, 350, function () {});
        }, 350);

        var cats = '';
        if($('#Type_of_Retailer').is('select') && $('#Broker').is('select')) {
            $('.retailer-list li.rollover-item p.cat').each(function () {
                cats += $.trim($(this).text()) + ',';
            });
            cats = cats.replace(/\, /g, ',');
            cats = cats.split(',');
            cats = cats.filter(function (elem, pos) {
                return cats.indexOf(elem) == pos;
            }).sort();
            $.each(cats, function (i, c) {
                if(c) $('#Type_of_Retailer').append('<option value="' + c + '">' + c + '</option>');
            });
        }

        $('#Type_of_Retailer').change(function () {
            $('#retailers-and-restaurants-q').val('').focus().blur();
            if($('.growMe').hasClass('growMe')) hide_active_info_pop();
            if($('#Broker').val()) {
                $('#Broker').val('');
                $('#Broker').prev('.select-text').html($('#Broker').attr('name').replace(/_/gi, ' '));
            }
            if($(this).val()) {
                $('.retailer-list li.rollover-item').hide();
                $('.retailer-list li.rollover-item').filter(function (index) {
                    return $(this).text().toLowerCase().replace(/[^a-z0-9-]/g, '').indexOf($('#Type_of_Retailer').val().toLowerCase().replace(
                        /[^a-z0-9-]/g, '')) > -1
                }).show();
            } else $('.retailer-list li.rollover-item').show();
return false;
        });

        var brokers = '';
        if($('#Type_of_Retailer').is('select') && $('#Broker').is('select')) {
            $('.retailer-list li.rollover-item p.broker a').each(function () {
                brokers += $.trim($(this).text()) + '|';
            });
            brokers = brokers.split('|');
            brokers = brokers.filter(function (elem, pos) {
                return brokers.indexOf(elem) == pos;
            }).sort(function (a, b) {
                var last_a = a.split(' ').pop();
                var last_b = b.split(' ').pop();
                return last_a < last_b ? -1 : 1;
            });
            $.each(brokers, function (i, c) {
                if(c) $('#Broker').append('<option value="' + c + '">' + c + '</option>');
            });
        }

        $('#Broker').change(function () {
            $('#retailers-and-restaurants-q').val('').focus().blur();
            if($('.growMe').hasClass('growMe')) hide_active_info_pop();
            if($('#Type_of_Retailer').val()) {
                $('#Type_of_Retailer').val('');
                $('#Type_of_Retailer').prev('.select-text').html($('#Type_of_Retailer').attr('name').replace(/_/gi, ' '));
            }
            if($(this).val()) {
                $('.retailer-list li.rollover-item').hide();
                $('.retailer-list li.rollover-item').filter(function (index) {
                    return $(this).text().toLowerCase().replace(/[^a-z0-9-]/g, '').indexOf($('#Broker').val().toLowerCase().replace(/[^a-z0-9-]/g, '')) > -
                        1
                }).show();
            } else $('.retailer-list li.rollover-item').show();
return false;
        });

        $('.retailers-and-restaurants-submit').click(function () {
            $('.retailer-list.info-pops').css('visibility', 'hidden');
            setTimeout("$('.retailer-list.info-pops').css('visibility','visible');", 200);
            return false;
        });

    }

    if($('#properties-q').is('input')) {
        $('#properties-q').keyup(function () {
            if(String($('#properties-q').val()).length > 2) {
                autoKeywords('properties-auto');
            }
        });
    }

    if($('a.more').hasClass('more')) {
        $('a.more').unbind();
        $('a.more').on('click', function () {
            if($(this).hasClass('opened')) {

                $(this).removeClass('opened').text('View More');
                $(this).parent().find('.text').animate({
                    height: '78px',
                    maxHeight: '78px'
                }, 333);

            } else {
                $(this).addClass('opened').text('Hide More');
                var h = $(this).parent().find('.text p:eq(0)').height();
                $(this).parent().find('.text').animate({
                    height: h + 'px',
                    maxHeight: h + 'px'
                }, 333);
            }
        });
    }

    if($('.info-pops').hasClass('info-pops')) {
        $('.imgG>a, .info-pops .rollover-item>.hover-parent, .info-pops .rollover-item>.titles').click(function () {

            if($(this).parent().find('a[class]')) {
                var id = $(this).parent().find('a[class]:eq(0)').attr('class');
                id = (id.match(/id[0-9]+(\b|$)/)) ? id.match(/[id+0-9]+(\b|$)/)[0].substr(2) : '';
                window.location.hash = window.location.hash.split('!')[0] + '!' + id;
            }

            var x = window.scrollX,
                y = window.scrollY;
            var person = $(this).closest('.rollover-item');
            var info = person.find('.info-pop');
            var h = info.height();
            info.css('height', '0').css('minHeight', '0').css('opacity', 1).show().addClass('active');
            $('.info-pops').addClass('showSlide');

            if(InfoPopOpened == 1) {
                if(person.offset().top == $('.rollover-item.active').offset().top) {
                    $('.rollover-item').css('height', '').removeClass('growMe').removeClass('active');
                    $('.info-pop').removeAttr('style').removeClass('growMe').removeClass('active');
                    var l = 40 + $('.rollover-item:eq(0)').height();
                    $('.rollover-item').css('opacity', 0.4);
                    (person).addClass('growMe').addClass('active').css('opacity', 1);
                    $('.info-pops .rollover-item').filter(function (index) {
                        return person.offset().top == $(this).offset().top;
                    }).addClass('growMe');
                    (info).addClass('growMe').show();
                    $('li.growMe').css('height', (l + h) + 'px');
                    scroll(x, y);
                    return false;
                } else {
                    $('.rollover-item').css('height', '').removeClass('growMe').removeClass('active');
                    $('.info-pop').removeAttr('style').removeClass('growMe').removeClass('active');
                    InfoPopOpened = 0;
                }
            }
            InfoPopOpened = 1;
            $('.info-pops .rollover-item').filter(function (index) {
                return person.offset().top == $(this).offset().top;
            }).addClass('growMe');
            (person).css('height', '').addClass('growMe').addClass('active').css('opacity', 1);
            (info).addClass('growMe').show();

            var l = 40 + h + $('.rollover-item:eq(0)').height();



            if($(window).outerWidth()>675) $('li.growMe').css('height', l + 'px');
            else $('li.growMe').css('minHeight', l + 'px').css('height','auto');

            if($(window).outerWidth()>675) $('.growMe.info-pop.active').show().css('height', h + 'px');
            else $('.growMe.info-pop.active').show().css('minHeight', l + 'px').css('height','auto');

            var t = 0;
            if(((person.offset().top) - 30) != $(document).scrollTop()) t = 150;
            $('html, body').animate({
                scrollTop: ((person.offset().top) - 30)
            }, t, function () {
                //
            });

        });
        $('.close-pop').unbind();
        $('.close-pop').on('click', function () {
            hide_active_info_pop();
        });
    }

    if($('.signup').hasClass('signup')) {
        $('.signup.show-hidden-form').unbind();
        $('.signup.show-hidden-form').on('click', function () {
            $(this).parent().find('.hidden-form').fadeToggle();
        });
        $('.signup .close-button').on('click', function () {
            $(this).parent().fadeOut();
        });
    }

    if($('.signup .close-button').hasClass('close-button')) {
        $('.signup .close-button').unbind();
        $('.signup .close-button').on('click', function () {
            $(this).parent().fadeOut();
        });
    }

    if($('.tab-controls').hasClass('tab-controls')) {
        tabControls();
        propCar();
        $('a[href="#properties"], a[href="properties"]').addClass('active');
    }

    if($('#Available_Space').is('select')) {
        $('#Available_Space').change(function () {
            if($(this).val() && $('.tab-controls li a[rel="site-plan"]').is('a')) {
                $('#site-plan-container img.map-img').attr('src','/assets/images/clear.gif');
                SelectedSpace = $(this).val();
                $('.tab-controls li a[rel="site-plan"]').click();
            }
        });
    }

    if($('.layout-buttons').hasClass('layout-buttons')) {
        propSearch();
    }

    if($('#contacts-map').hasClass('locationMap')) initContactMap();
    if($('#urban-map').hasClass('gmap3')) initUrbanMap();
    if($('.property-map-holder').length) initPropertySearchhMap();

    if($('.styled-select').hasClass('styled-select')) pretty_selects();

    if($('.redtext').hasClass('redtext') && !readCookie('SiteEdit') && !$('body').hasClass('cmsEditing')) {
        var r = $('.redtext:eq(0)').text();
        $('.redtext').prev('h2').html($('.redtext').prev('h2').text().replace(new RegExp(r, "i"), '<span class="red">' + r + '</span>'));
    }

    if($('.retailer-search .select-text').hasClass('select-text')) {
        $('.retailer-search .select-text').hover(function () {
            $(this).addClass('over');
        }, function () {
            $(this).removeClass('over');
        });
    }
    if($('.info-pops').hasClass('info-pops')) {
        var id = window.location.hash.split('!')[1];
        if(id) $('.info-pops a.id' + id).click();
    }
    if($('.news-page').hasClass('news-page')) {
        var id = window.location.hash.split('!')[1];
        if(id) {
            $('.news-page .id' + id).addClass('selected');
            $('.news-page .id' + id + ' .more').click();
            $('html, body').animate({
                scrollTop: (($('.news-page .id' + id).offset().top) - 0)
            }, 533, function () {
                //
            });
        }
    }

    if($('.info-pop-slider li:eq(1)').is('li')) {
        var w = $('.info-pop-slider:eq(1)').width();
        $('.info-pop-slider ul').filter(function () {
            return($(this).find('li').length > 1);
        }).after('<button class="left-arrow" /><button class="right-arrow" />');
        $('.info-pop-slider .left-arrow').click(function () {
            if(moving == 1) return false;
            moving = 1;
            $(this).parent().find('ul').prepend($(this).parent().find('li').last());
            $(this).parent().find('li:eq(0)').css('marginLeft', '-' + w + 'px');
            $(this).parent().find('li:eq(0)').animate({
                marginLeft: '0'
            }, 450, function () {
                moving = 0;
            });
        });

        $('.info-pop-slider .right-arrow').click(function () {
            if(moving == 1) return false;
            moving = 1;
            $(this).parent().find('ul li:eq(0)').animate({
                marginLeft: '-' + w + 'px'
            }, 450, function () {
                $(this).parent().find('li').css('marginLeft', '0');
                $(this).parent().append($(this));
                moving = 0;
            });
        });
    }
}

function ajax_content() {
    if(new Date().getTime() - LastLoadTime < 0) return false;
    LastLoadTime = new Date().getTime();
    if(readCookie('SiteEdit') || $('body').hasClass('cmsEditing')) return false;
    if(ajaxData) ajaxData.abort();
    if(projectFinder) projectFinder.abort();
    $('a.active').removeClass('active');
    $('.page-content:eq(0)').stop().animate({
        opacity: 0
    }, 100, function () {
        $('.gmap3').gmap3('destroy').remove();
        $('html, body').stop().animate({
            scrollTop: 0
        }, 90);
    });

    var h = hash;
    if(hash.indexOf('!') > 0) h = hash.substring(0, hash.indexOf('!'));

    if(h == 'home') {
        if(homeTitle) populate(homeTitle, 'defaultaspx', homeData);
    } else {
        ajaxData = $.ajax({
            url: hash.split('!')[0],
            error: function (x) {
                if(!x.status || x.status == 0) return;
                var error = x.responseText;
                error = error.substring(error.indexOf('<title>') + 7);
                error = error.substring(0, error.indexOf('<'));
                alert('Server Error ' + x.status + ':\n' + error)
            },
            success: function (data) {
                ajaxData = null;
                oldHash = hash;
                var m = data.substr(8 + (data.indexOf(' class="')), 30).match(/[a-z-]+aspx(\b|$)/i);
                data = $('<div>' + data + '</div>');
                var title = $(data).find('title').text();
                data = $(data).find('.page-content:eq(0)').html();
                populate(title, aspx = m ? m[0] : '', $('<div>' + data + '</div>'));
            }
        });
    }
}

function populate(title, aspx, data) {
    if($('body').attr("class")) {
        var m = $('body').attr("class").match(/[a-z-]+aspx(\b|$)/i);
        if(m) $('body').removeClass(m ? m[0] : '');
    }
    $('body').addClass(aspx);
    document.title = title;
    $('.page-content:eq(0)').html(data);

    $('.page-content:eq(0)').stop().animate({
        opacity: 1
    }, 333);

    $(':input').attr('autocomplete', 'off');

    InfoPopOpened = 0;
    moving = 0;

    if($('.news-list .text').hasClass('text')) {
        $('.news-list .text').each(function () {
            if($(this).find('p:eq(0)').height() > $(this).height()) {
                $(this).after('<a class="more">View More</a>');
            }
        });
    }

    $('.header .menu a[href="' + hash + '"]').parent().parent().parent().find('a:eq(0)').addClass('active');
    calculate_dimensions();
    aLinks();

    pageInit();
    $('a[href="#' + hash + '"]').addClass('active');
    $('body').css('visibility', 'visible').addClass('loaded');
    scroll(0, 0);
}

function createCookie(name, value, days) {
    if(days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while(c.charAt(0) == ' ') c = c.substring(1, c.length);
        if(c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

var autoComplete;

function autoKeywords(k) {
    if(autoComplete) autoComplete.abort();
    autoComplete = $.get(k + '?q=' + escape($('.q:eq(0)').val()), function (data) {
        $('.auto em').html('');
        if(data) {

            if($('body').hasClass('cmsEditing')) data = data.replace(/#/g, '');

            $('.auto em').css('display', 'block');
            $('.auto em').html(data);
            $('.auto em, .q').hover(function () {
                $('.q').addClass('over');
            }, function () {
                $('.q').removeClass('over');
                setTimeout(function () {
                    if(!$('.q').hasClass('over'))
                        $('.auto em').fadeOut(333);
                }, 250);
            });
        }
    });
}

function propSearch() {
    $('.layout-buttons a').on('click', function () {
        $('body').removeClass('showMap');
        $('.layout-buttons a.selected').removeClass('selected');
        $(this).addClass('selected');
        $('#p-search-results').attr('class', $(this).attr('data-view'));
        if($('.list-view').hasClass('selected')) {
            $('#search-map').hide();
            $('#p-search-map').removeClass('active');
            $('#search-details').show();
        } else if($('.full-view').hasClass('selected')) {
        $('body').removeClass('showMap');
            $('#search-details').hide();
            $('#p-search-map').removeClass('active').removeClass('loaded');
            $('#search-map').show();
        } else {
        $('body').addClass('showMap');
            $('#search-details').hide();
            $('#p-search-map').addClass('active').show();
            $('#search-map').show();

        }
        createCookie('layout', $('.layout-buttons a').index($(this)), 30);
    });

    var x = readCookie('layout');
    if(x) $('.layout-buttons a:eq(' + x + ')').click();
    else $('.layout-buttons a:eq(0)').click();

    if($('.list-view').hasClass('selected')) {
        $('#search-map').hide();
        $('#search-details').show();

    } else {
        $('#search-map').hide();
        $('#search-details').show();
    }
}

function tabControls() {
    $('.tab-controls li a').unbind();
    $('.tab-controls li a').click(function () {

        if($(this).hasClass('active')) return false;
        if($('.tab-controls li a.moving').hasClass('moving')) return false;
        $('.tab-controls li a').addClass('moving');

        if($(this).html() == 'Site Plan') $('body').addClass('SitePlan');
        else $('body').removeClass('SitePlan');

        if(typeof SitePlanPluginClose == 'function') {
          SitePlanPluginClose();
          $('siteplanplugindataelement').remove();
        }
        var tab = $(this).attr('rel');
        $('.tab-controls li a').removeClass('active');
        $('.tab-controls li a[rel="' + tab + '"]').addClass('active');

        if(!readCookie('SiteEdit') && !$('body').hasClass('cmsEditing')) createCookie('propTab', $('.top-section .left').text().replace(/[^a-zA-Z0-9-]/g,
            '').substr(0, 10) + tab);
        else createCookie('propTab', -1);

        $('.tabs-content > div.active').show().fadeOut(170, function () {
            $('.tabs-content > div#' + tab).css('opacity', 0).show().addClass('active');
            $(this).removeClass('active');
            $('.tabs-content > div#' + tab).animate({
                opacity: 1
            }, 300, function () {
                $('.tab-controls li a').removeClass('moving');
                if(tab == 'area-map') initPropertyMap();
                else if(tab == 'site-plan') SitePlan();
            });
        });
    });

    var tab = null;
    var tab = readCookie('propTab');
    if(tab && tab.substr(0, 10) == $('.top-section .left').text().replace(/[^a-zA-Z0-9-]/g, '').substr(0, 10)) {
        tab = tab.substr(10);
        $('.tab-controls li a').removeClass('active');
        $('.tab-controls li a[rel="' + tab + '"]').addClass('active');
        $('.tabs-content > div').hide().removeClass('active');
        $('.tabs-content > div#' + tab).css('opacity', 1).show().addClass('active');
        if(!readCookie('SiteEdit')) {
          if(tab == 'area-map') initPropertyMap();
          else if(tab == 'site-plan') SitePlan();
        }
    }
}

function aLinks() {
    if(mobile || readCookie('SiteEdit') || $('body').hasClass('cmsEditing') || window.location.search.substr(0, 1) == '?') {
        //
    } else {
        $('[href="/"],[href="./"]').each(function (ev) {
            $(this).attr('href', '#home');
        });
        $('a[href]').not('[href^="http"],[href^="mailto:"],[href="/"],[href="./"],[href^="#"],a[href*="."]').each(function (ev) {
            $(this).attr('href', '#' + $(this).attr('href').replace('#!', '!').replace(/\//g, ''));
        });
    }
}

function searchMapLoad() {
    if($('#gmap div div div div div').is('div') == true) {
        $('#gmap').addClass('loaded');
        $('.loading').remove();
        $('#gmap').css('opacity', '1');
    } else setTimeout('searchMapLoad()', 100);
}

function hide_active_info_pop() {
    if($('.growMe').hasClass('growMe')) {
        $('.info-pops').removeClass('showSlide');
        $('.rollover-item, .info-pop').removeAttr('style').removeClass('growMe').removeClass('active');
        setTimeout("InfoPopOpened = 0;", 85);
    }
    return false;
}

var pf;

function propertyfinder(a) {

    $('#gmap.loaded').removeClass('loaded');
    $('.completeList, #p-search-results').html('');

    $('.auto em').fadeOut(200);
    if(projectFinder) projectFinder.abort();
    $('.loading').remove();
    $('#p-search-results, #p-search-map').append('<p class="loading" />');
    var ii = 0;

    if( $('#p-search-map').css('marginLeft').replace('px','') <1 ) $('#gmap').css('width', ($(window).width()) + 'px');
    else $('#gmap').css('width', ($(window).width() - 60) + 'px');

    createCookie('propTab', null, -1);

    var u = '/properties-all';
    if(a && a == 'keywords') {
        pf = 1;
        u = 'properties-finder?term=' + escape($('#properties-q').val());
        $('Size_of_Space').val('');
    } else {
        if(pf == 1) return false;
        if($('#properties-search select option:selected[value!=""]').length > 0) u = '/properties-finder?' + $('#properties-search').serialize();
        else u = '/properties-all';
    }

    projectFinder = $.ajax({
        url: u,
        cache: true,
        error: function (x) {
            if(!x.status || x.status == 0) return;
            var error = x.responseText;
            error = error.substring(error.indexOf('<title>') + 7);
            error = error.substring(0, error.indexOf('<'));
            alert('Server Error ' + x.status + ':\n' + error)
        },
        success: function (data) {
            pf = null;

            $('#properties-word-search input').prop('disabled', false);
            $('#properties-search select').prop('disabled', false);
            if(!data) {
                $('#gmap').gmap3('clear');
            } else if(data.substr(12, 8) == 'Complete') {
                $('#search-details').html('<h2>No Matching Properties Found</h2>');
                $('#gmap').gmap3('clear');
                $('#gmap, #p-search-results div').css('opacity', 1);
                $('#gmap>div').css('opacity', 0.2);
                $('.completeList').html(FixEntities(data));
                $('#gmap').append('<h2>No Matching Properties Found</h2>');
                $('.loading').remove();
                $('#gmap').addClass('loaded');
                $('#gmap').css('opacity', '1');
                return false;
            } else {
                if($('body').hasClass('cmsEditing')) data = data.replace(/#/g, '');
                $('#gmap h2').remove();
                $('#gmap>div').css('opacity', 1);
                $('#p-search-results').html(FixEntities(data));
                $('#search-map .completeList h2').append('<a href="' + $('.footer ul.resources>li>a:eq(0)').attr('href') +
                    '">Property Availability Report</a>')
                var points = eval($('#gmapJ').html());
                $('#p-search-results #gmapJ').remove();
                if(points) {
                    if($('.list-view').hasClass('selected')) $('#search-details').show();
                    else $('#search-map').show();
                    if(!$('#gmap div div div div').is('div')) {
                        $('#gmap').gmap3({
                            map: {
                                options: {
                                    mapTypeId: "RoadMap",
                zoomControl: true,
                panControl: true,
                draggable: true,
                scaleControl: false,
                scrollwheel: false,
                navigationControl: false,
                streetViewControl: false,
                                    mapTypeControlOptions: {
                                        mapTypeIds: ["RoadMap", google.maps.MapTypeId.SATELLITE]
                                    }
                                }
                            },
                            styledmaptype: {
                                id: "RoadMap",
                                options: {
                                    name: "Map",
                                },
                                styles: mapStyle
                            },
                            marker: {
                                values: points,
                                cluster: {
                                    radius: 40,
                                    events: { // events trigged by clusters
                                        click: function (cluster, event, data) {
                                            var bounds = new google.maps.LatLngBounds(
                                                new google.maps.LatLng(data.data.markers[0].lat, data.data.markers[0].lng),
                                                new google.maps.LatLng(data.data.markers[0].lat, data.data.markers[0].lng));
                                            for(var i = 1; i < data.data.markers.length; i++) {
                                                bounds.extend(new google.maps.LatLng(data.data.markers[i].lat, data.data.markers[i].lng));
                                            }
                                            var map = $(this).gmap3("get");
                                            map.fitBounds(bounds);
                                        },
                                        mouseover: function (cluster) {
                                            $(cluster.shadow.getDOMElement()).css("cursor",
                                                "url(/assets/images/zoom.png),url(/assets/images/zoom.cur),pointer");
                                        },

                                    },
                                    // This style will be used for clusters with more than 1 marker
                                    0: {
                                        content: '<a class="mapCluster">CLUSTER_COUNT</a>',
                                        width: 33,
                                        height: 33,
                                    }
                                },
                                options: {
                                    icon: new google.maps.MarkerImage("/assets/images/pins/pin.png")

                                },
                                events: {
                                    mouseover: function (marker, event, context) {
                                        r = randomString(12);
                                        $(this).gmap3({
                                            clear: "overlay"
                                        }, {
                                            overlay: {
                                                latLng: marker.getPosition(),
                                                options: {
                                                    content: '<div class="infobulle" id="' + r + '"><a href="' + context.data.link + '" title="' + context
                                                        .data.title + '"><span class="mapText"><span class="mapClusterContent"><img src="/dynamic/image/week/project/best/115x90/68/777777/Center/' + context
                                                        .data.id + '.jpg" alt="" title="' + context.data.title + '"/><span class="mapInfo"><strong>' + context
                                                        .data.title + '</strong><span>' + context.data.address + '</span><em>' + context.data.sf + '</em></span></span></a><span class="mapC"></span></div>',
                                                    offset: {
                                                        x: -165,
                                                        y: -150
                                                    }
                                                }
                                            }
                                        });
                                    },
                                    mouseout: function () {
                                        eval("setTimeout(\"$('#" + r + "').parent().fadeOut(300)\", 3600)");
                                    },


                                    click: function (marker, event, context) {
                                        document.location = context.data.link;
                                    }

                                }
                            }

                        });

                        var bounds = new google.maps.LatLngBounds(
                            new google.maps.LatLng(points[0].lat, points[0].lng),
                            new google.maps.LatLng(points[0].lat, points[0].lng));
                        for(var i = 1; i < points.length; i++) {
                            bounds.extend(new google.maps.LatLng(points[i].lat, points[i].lng));
                        }
                        $('#gmap').gmap3("get").fitBounds(bounds);

                    } else {
                        $('#gmap').gmap3('clear');
                        $('#gmap').gmap3({
                            marker: {
                                values: points,
                                cluster: {
                                    radius: 40,
                                    events: { // events trigged by clusters
                                        click: function (cluster, event, data) {
                                            var bounds = new google.maps.LatLngBounds(
                                                new google.maps.LatLng(data.data.markers[0].lat, data.data.markers[0].lng),
                                                new google.maps.LatLng(data.data.markers[0].lat, data.data.markers[0].lng));
                                            for(var i = 1; i < data.data.markers.length; i++) {
                                                bounds.extend(new google.maps.LatLng(data.data.markers[i].lat, data.data.markers[i].lng));
                                            }
                                            var map = $(this).gmap3("get");
                                            map.fitBounds(bounds);
                                        },
                                        mouseover: function (cluster) {
                                            $(cluster.shadow.getDOMElement()).css("cursor",
                                                "url(/assets/images/zoom.png),url(/assets/images/zoom.cur),pointer");
                                        },

                                    },
                                    // This style will be used for clusters with more than 1 marker
                                    0: {
                                        content: '<a class="mapCluster">CLUSTER_COUNT</a>',
                                        width: 33,
                                        height: 33,
                                    }
                                },
                                options: {
                                    icon: new google.maps.MarkerImage("/assets/images/pins/pin.png")
                                },
                                events: {
                                    mouseover: function (marker, event, context) {
                                        $(this).gmap3({
                                            clear: "overlay"
                                        }, {
                                            overlay: {
                                                latLng: marker.getPosition(),
                                                options: {
                                                    content: '<div class="infobulle"><div class="mapText"><span class="mapClusterContent"><a href="' + context
                                                        .data.link + '" title="' + context.data.title + '"><img src="/dynamic/image/week/project/best/115x90/68/777777/Center/' + context
                                                        .data.id + '.jpg" alt="" title="' + context.data.title + '"/></a><span class="mapInfo"><a href="' + context
                                                        .data.link + '"><strong>' + context.data.title + '</strong><span>' + context.data.address + '</span><em>' + context
                                                        .data.sf + '</em></a></span><a class="mapClose" onclick="$(this).parent().parent().hide()"></a></span></div><span class="mapC"></span>' + '</div>',
                                                    offset: {
                                                        x: -165,
                                                        y: -80
                                                    }
                                                }
                                            }
                                        });
                                    },
                                    mouseout: function () {
                                        $(this).gmap3({
                                            clear: "overlay"
                                        });
                                    }
                                }
                            }

                        });
                        var bounds = new google.maps.LatLngBounds(
                            new google.maps.LatLng(points[0].lat, points[0].lng),
                            new google.maps.LatLng(points[0].lat, points[0].lng));
                        for(var i = 1; i < points.length; i++) {
                            bounds.extend(new google.maps.LatLng(points[i].lat, points[i].lng));
                        }
                        $('#gmap').gmap3("get").fitBounds(bounds);
                        $('#gmap').css('opacity', '1');
                        $('.loading').remove();
                    }
                } else {
                    $('#gmap').css('opacity', '1');
                    $('.loading').remove();
                }
                setTimeout('searchMapLoad()', 200);
                loadPropImg();
            }
        }

    });
}

function twit() {
    if($('#twitterfeed').is('script')) $('#twitterfeed').remove();
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.id = 'twitterfeed';
    script.src = 'twitterfeed?results=10&start=0';
    $('head').append(script);
    setTimeout(function () {
        $('.daily').animate({
            opacity: 1
        }, 250);
    }, 300);
}

function loadPropImg() {
    $('img.prop[longdesc]:eq(0)').attr('src', $('img.prop[longdesc]:eq(0)').attr('longdesc')).removeAttr('longdesc').load(function () {
        if($('img.prop[longdesc]').hasClass('prop')) loadPropImg();
    }).error(function () {
        if($('img.prop[longdesc]').hasClass('prop')) loadPropImg();
    });
}

function loadRRImg() {
    if($('.retailer-list img[longdesc]').is('img')) {
        $('.retailer-list img[longdesc]:eq(0)').attr('src', $('.retailer-list img[longdesc]:eq(0)').attr('longdesc')).removeAttr('longdesc').load(function () {
            loadRRImg();
        }).error(function () {
            $(this).attr('src', '/assets/images/clear.gif');
            loadRRImg();
        });
    } else {
        loadRRImg2();
    }
}

function loadRRImg2() {
    if($('.info-pop-content .rCol1 img[longdesc]').is('img')) {
        $('.info-pop-content .rCol1 img[longdesc]:eq(0)').attr('src', $('.info-pop-content .rCol1 img[longdesc]:eq(0)').attr('longdesc')).removeAttr('longdesc')
            .load(function () {
                loadRRImg2();
            }).error(function () {
                $(this).attr('src', '/assets/images/clear.gif');
                loadRRImg2;
            });
    }
}

if(window.location.hash.indexOf('?') > 0) document.location = String(document.location.href).replace('#', '');

$(document).ready(function () {
    if(window.location.search.substr(0, 8) == '?refresh') {
        $('img').each(function () {
            if($(this).attr('src')) {
                var img = $(this).attr('src').split('?')[0];
                $(this).attr('src', img + '?refresh');
            }
            if($(this).attr('longdesc')) {
                var img = $(this).attr('longdesc').split('?')[0];
                $(this).attr('longdesc', img + '?refresh');
            }
        });
        $('*').each(function () {
            if($(this).css('background-image') && $(this).css('background-image') != 'none') {
                var img = $(this).css('background-image').split('?')[0].replace('url(', '').replace(/[\"\'\)]/g, '');
                $(this).css('backgroundImage', 'url("' + img + '?refresh' + '")');
            }
        });
    }

});

$(window).load(function () {
    if(!$('body').hasClass('cmsEditing') && !$('#speed a').is('a')) {
        $('body').append('<div id="speed" />');
        $('.menu a[href]').each(function () {
            var u = $(this).attr('href').replace('#', '');
            if(!$('#speed a[rel="' + u + '"]').is('a') && u != window.location.href.substring(window.location.href.lastIndexOf('/') + 1).replace('#',
                '').split('?')[0].split('!')[0]) $('#speed').append('<a rel="' + u + '" />');
        });
        $('#speed').prepend('<a rel="properties-all" />');
        setTimeout("cachePages()", 450);
    }
});

var cacheLoad;

function cachePages() {
    if(mobile) return false;

    if(readCookie('SiteEdit') || mobile) return false;
    //if(cacheLoad) cacheLoad.abort();
    if($('#speed a:eq(0)').attr('rel')) {
        var u = $('#speed a:eq(0)').attr('rel');
        $('#speed a:eq(0)').appendTo($('#speed'));
        cacheLoad = $.ajax({
            url: u,
            cache: true,
            error: function (x) {
                var e = Number($('#speed a[rel=' + u + ']').text());
                if(e > 4) $('#speed a[rel=' + u + ']').remove();
                else $('#speed a[rel=' + u + ']').html(e + 1);
            },
            success: function (data) {
                $('#speed a[rel=' + u + ']').remove();
            }
        });
        setTimeout("cachePages()", 600);
    }
}

function propCar() {
    if($('.prop-slider li:eq(1)').is('li')) {
        var w = 460;
        $('.prop-slider ul').filter(function () {
            return($(this).find('li').length > 1);
        }).after('<button class="left-arrow" /><button class="right-arrow" />');
        $('.prop-slider .left-arrow').click(function () {
            if(moving == 1) return false;
            moving = 1;
            $(this).parent().find('ul').prepend($(this).parent().find('li').last());
            $(this).parent().find('li:eq(0)').css('marginLeft', '-' + w + 'px');
            $(this).parent().find('li:eq(0)').animate({
                marginLeft: '0'
            }, 450, function () {
                moving = 0;
            });
        });

        $('.prop-slider .right-arrow').click(function () {
            if(moving == 1) return false;
            moving = 1;
            $(this).parent().find('ul li:eq(0)').animate({
                marginLeft: '-' + w + 'px'
            }, 450, function () {
                $(this).parent().find('li').css('marginLeft', '0');
                $(this).parent().append($(this));
                moving = 0;
            });
        });
    }
}

function FixEntities(str) {
    return str.replace(/&(#(?:x[0-9a-f]+|\d+)|[a-z]+);?/gi, function ($0, $1) {
        if($1[0] === "#") {
            return String.fromCharCode($1[1].toLowerCase() === "x" ? parseInt($1.substr(2), 16) : parseInt($1.substr(1), 10));
        }
    });
}

function randomString(length) {
    var chars = 'abcdefghijklmnopqrstuvwxyz';
    var result = '';
    for(var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}

function loadGoogleAnalytics(acct) {
    if(readCookie('SiteEdit')) return false;
    if($('#gacode').attr('id')) {
        _gaq.push(['_setAccount', GoogleAnalyticsAccount]);
        _gaq.push(['_trackPageview', getPage()]);
    } else {
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."),
            pageTracker,
            s;
        s = document.createElement('script');
        s.src = gaJsHost + 'google-analytics.com/ga.js';
        s.id = 'gacode';
        s.type = 'text/javascript';
        s.onloadDone = false;

        function init() {
            pageTracker = _gat._getTracker(acct);
            pageTracker._trackPageview();
        }
        s.onload = function () {
            s.onloadDone = true;
            init();
        };
        s.onreadystatechange = function () {
            if(('loaded' === s.readyState || 'complete' === s.readyState) && !s.onloadDone) {
                s.onloadDone = true;
                init();
            }
        };
        document.getElementsByTagName('head')[0].appendChild(s);
    }
}

function initUrbanMap() {
    $('#urban-map').css('opacity', 0);
    var markers = [];
    $('#urbanLocations p').each(function () {
        var marker = {};
        marker.latLng = [];
        marker.latLng[0] = $(this).find('.location').text().split(",")[0].replace(/EDIT/gi, '');
        marker.latLng[1] = $(this).find('.location').text().split(",")[1].replace(/EDIT/gi, '');
        marker.options = {};
        marker.options.icon = $(this).find('.img').attr('src');
        marker.options.clickable = false,
        markers.push(marker);
    });
    var n = Number($('.zoom').text().replace('EDIT', ''));
    if(n > 1) n = n;
    else n = 1;

    $("#urban-map").gmap3({
        map: {
            options: {
                center: [Number($('.latlong:eq(0)').text().replace(/ /g, '').replace('EDIT', '').split(",")[0]), Number($('.latlong:eq(0)').text().replace(
                    / /g, '').replace('EDIT', '').split(",")[1])],
                zoom: n,
                mapTypeId: "RoadMap",
                zoomControl: true,
                panControl: true,
                draggable: true,
                scaleControl: false,
                scrollwheel: false,
                navigationControl: false,
                streetViewControl: false,
                mapTypeControlOptions: {
                    mapTypeIds: ["RoadMap", google.maps.MapTypeId.SATELLITE]
                }
            }
        },
        styledmaptype: {
            id: "RoadMap",
            options: {
                name: "Map",
            },
            styles: mapStyle
        },
        marker: {
            values: markers
        }
    });

    $('#urban-map').animate({
        opacity: 1
    }, 500, function () {
        // Animation complete.
    });
}

$(window).bind('load', function() {
    activeLevel();
});

function activeLevel () {
    $('div.two-columns.siteSpace:last').addClass('inactive-level');
    $('.inactive-level').slideUp('fast');
    $('.select-site-plan a:last').click(function() {
        $('div.two-columns.siteSpace').removeClass('inactive-level');
        $('div.two-columns.siteSpace:first').addClass('inactive-level');
        $('.inactive-level').slideUp('fast');
    });
    $('.select-site-plan a:first').click(function() {
        $('div.two-columns.siteSpace').removeClass('inactive-level');
        $('div.two-columns.siteSpace:last').addClass('inactive-level');
        $('.inactive-level').slideUp('fast');
    });
}

function twitLinks(str) {
   var exp = /(\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
   str = str.replace(exp,"<a href='$1' target='_blank'>$1</a>");
   exp = /(^|\s)#(\w+)/g;
   str = str.replace(exp, "$1<a href='https://twitter.com/search?q=%23$2' target='_blank'>#$2</a>");
   exp = /(^|\s)@(\w+)/g;
   str = str.replace(exp, "$1<a href='https://twitter.com/$2' target='_blank'>@$2</a>");
   return str;
}
function twitterFeed(data) {
   var datetime = data.datetime;
   var t = '';
   $.each( data.tweets, function( i, item ) {
     t += '<p>..'
     t += '<strong class="twitDateContent">'+ twitLinks(item.content) +'</strong>';
     t += '<em class="twitDateTime">'+ formatDateTime(item.datetime, datetime) +'</em>';
     t += '</p>'
   });
   $('.tweet').html(t);
}

function resetTime(d) {
    d = new Date(d);
    d.setHours(0);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    return d;
}

function formatDateTime(datePast,dateCurrent) {
    var dPast = resetTime(datePast);
    var dCurrent = resetTime(dateCurrent);
    if ( (dCurrent.getDate() + " | " + dCurrent.getMonth() + ' | '+ dCurrent.getFullYear()) ==  (dPast.getDate() + " | " + dPast.getMonth() + ' | '+ dPast.getFullYear()) ) {
     tCurrent = new Date(dateCurrent);
     tPast = new Date(datePast);
     var m = Math.floor((Math.abs(tPast - tCurrent)/1000)/60);
     if (m<5) return 'Just Posted';
     else if (m<60) return m + ' Minutes Ago';
     else if (m<120) return 'One Hour Ago';
     else return Math.ceil(m/60) + ' Hours Ago';
    }
    var dt = (dCurrent-dPast)/(24*3600*1000);
    var arr = ["Today", "Yesterday", "Two Days Ago", "Three Days Ago"];
    return (dt>=0&&dt<4)?arr[dt]:["January","Febuary","March","April","May","June","July","August","September","October","November","December"][dPast.getMonth(dPast)] + " " + dPast.getDate() + ", " + dPast.getFullYear();
}

function timeline() {
    if (typeof timelineJS == "undefined") {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = '/assets/js/timeline.js';
        $('head').append(script);
    }

    $('#timeline').prepend('<ul id="timeDates"></ul>');
    $('#timeEvent li').each(function () {
        $('#timeDates').append('<li><a>' + $(this).find('h1').text().replace(/[^\d.]/g, '') + '</a></li>');
    });

    $(function () {
        $().timelinr({
            arrowKeys: 'true'
        })
    });
}

function checkMobile () {
var check = false;
(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
return check; }

function getq(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
