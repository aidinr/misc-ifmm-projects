﻿<%@ Page Language="VB"  MasterPageFile="~/HRMaster1.master" AutoEventWireup="false" CodeFile="careers.aspx.vb" Inherits="careers" %>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!-- Page Content START -->
<div class="page-content">
    <div class="top-section fluid-padding">
        	<h1 class="cms cmsType_TextSingle cmsName_Careers_Top_Heading"></h1>
        	<h2 class="cms cmsType_TextSingle cmsName_Careers_Heading"></h2>
        	<h3 class="redtext cms cmsType_TextSingle cmsName_Careers_Heading_Red_Text"></h3>
    </div>
    <div class="main general about-page">
        <div class="content">
            <div class="leftMain cms cmsType_Rich cmsName_Careers_Content"></div>
            <img class="cms cmsType_Image cmsName_Careers_Item_1_Image" src="cms/data/Sized/padded/350x250/90/ffffff/North/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Careers_Item_1_Image")%>" alt="" />

        </div>
        <div class="sidebar">
            <div class="sidebarContent">
                <h2 style="    font-size: 26px;    font-weight: normal;">Available Positions</h2> <br /> 
            		<ul class="cmsGroup cmsName_Careers_Other">
							<!--
							<li class="cmsGroupItem"><a class="cms cmsType_File cmsName_Document" href="#">Example Report</a></li>
							-->
							                      
                            <%
                           For i As Integer = 1 To 20 - 1
                                    If Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Careers_Other_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
                            %>
                                 
                                
                                 <li class="cmsGroupItem"><div class="cms cmsType_Rich cmsName_Careers_Right_Column<%=i %>"></div><br /><a class="cms cmsType_File cmsName_Careers_Other_Item_<%=i %>_Document" href="#">View More</a></li>
                                
                              <%
                              End If
                          Next
                          %>

					</ul>	
                
                <br />
                <br />
                 <a href="/contact?careers=true" ><img src="assets/images/submit_resume.jpg"  /></a>
           
            </div>
            <br class="clear" />
            
           
        </div>
        <div class="c"></div>
    </div>
    <div class="c"></div>
</div>
<!-- Page Content END -->
	
</asp:Content>
