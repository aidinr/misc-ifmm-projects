﻿
Partial Class CaseStudyPDF
    Inherits System.Web.UI.Page

    Protected Sub propertyPDF_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim id As Integer
        If Not Request.QueryString("id") Is Nothing Then
            id = Request.QueryString("id")
            Dim ExPDF As New ExportPDF
            ExPDF.GetCaseStudy(id)
        End If
    End Sub

End Class
