﻿Imports System.IO

Partial Class properties_finder
    Inherits System.Web.UI.Page
    'Inherits CMSBasePage
    Protected Sub property_finder_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim OriginalUrlPage As String
        Dim UrlPage As String
        Dim pageUrl As String = HttpContext.Current.Request.RawUrl
        OriginalUrlPage = HttpContext.Current.Request.RawUrl
        Dim pageName As String = Path.GetFileName(Request.PhysicalPath)
        pageUrl = Replace(pageUrl, "/", "")
        If InStr(pageUrl, "?") > 0 Then
            pageUrl = pageUrl.Substring(0, pageUrl.IndexOf("?"))
            pageUrl = Replace(pageUrl, "/", "")
        End If
        UrlPage = pageUrl


        Dim SizeOfSpace As String = ""
        Dim PropertyState As String = ""
        Dim PropertyCounty As String = ""
        Dim PropertyAnchor As String = ""
        If Not Request.QueryString("Size_of_Space") Is Nothing Then
            SizeOfSpace = Request.QueryString("Size_of_Space")
        End If
        If Not Request.QueryString("State") Is Nothing Then
            PropertyState = Request.QueryString("State")
        End If
        If Not Request.QueryString("County") Is Nothing Then
            PropertyCounty = Request.QueryString("County")
        End If
        If Not Request.QueryString("Anchor") Is Nothing Then
            PropertyAnchor = Request.QueryString("Anchor")
        End If

        Dim ProjectFilters As New ProjectFiltersStructure
        ProjectFilters.SizeOfSpace = SizeOfSpace
        ProjectFilters.PropertyState = PropertyState
        ProjectFilters.PropertyCounty = PropertyCounty
        ProjectFilters.PropertyAnchor = PropertyAnchor
        Dim SearchPhrase As String = ""
        If Not Request.QueryString("term") Is Nothing Then
            SearchPhrase = Request.QueryString("term").Trim
            ProjectFilters.SearchPhrase = SearchPhrase
        End If

        Dim PropertyFinderOUT As String = ""
        PropertyFinderOUT = IDAMFunctions.PrintProjects("2409776", 0, 1, "Properties", ProjectFilters)

        'Dim PropertyFinderButtom As String = ""
        'PropertyFinderButtom = IDAMFunctions.PrintProjects("2409776", 0, 1, "Properties Buttom", ProjectFilters)

        Response.Clear()
        Response.Cache.SetExpires(DateTime.Now.AddSeconds(86400))
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Write(PropertyFinderOUT)
        Response.Flush()

        If OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", PropertyFinderOUT, UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", PropertyFinderOUT, UrlPage)
        End If

    End Sub

End Class
