﻿Imports System.Net.Mail
Imports System.IO
Imports System.Threading

Partial Class contact_submit
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim EmailToNo As Integer
        Dim EmailTo As String = ""
        If Not Request.Form("Recipient") Is Nothing Then
            EmailToNo = Request.Form("Recipient")
        End If

        EmailTo = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Text\Contact_Page_Recipient.txt")

        Dim EmailBody As StringBuilder = New StringBuilder
        Dim message As MailMessage = New MailMessage(ConfigurationManager.AppSettings("ContactEmailFrom").ToString(), EmailTo)

        If (String.IsNullOrEmpty(Request.Params("Name"))) Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.Write("-7")
            HttpContext.Current.Response.Flush()
            Return
            Exit Sub
        End If

        If (Not String.IsNullOrEmpty(Request.Params("E-mail"))) Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.Write("-6")
            HttpContext.Current.Response.Flush()
            Return
            Exit Sub
        End If

        If (Not String.IsNullOrEmpty(Request.Params("Message"))) Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.Write("-5")
            HttpContext.Current.Response.Flush()
            Return
            Exit Sub
        End If

        If (String.IsNullOrEmpty(Request.Params("Email"))) Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.Write("-4")
            HttpContext.Current.Response.Flush()
            Return
            Exit Sub
        End If

        If (String.IsNullOrEmpty(Request.Params("Comments"))) Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.Write("-3")
            HttpContext.Current.Response.Flush()
            Return
            Exit Sub
        End If

        If (String.IsNullOrEmpty(EmailTo)) Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.Write("-2")
            HttpContext.Current.Response.Flush()
            Return
            Exit Sub
        End If

        Try
            EmailBody.Append("Name: " + Request.Params("Name") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Email: " + Request.Params("Email") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Interest: " + Request.Params("Area_of_Interest") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Comments: " + Request.Params("Comments") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("IP Address: " + Request.ServerVariables("REMOTE_ADDR").ToString())

            message.IsBodyHtml = False
            message.Body = EmailBody.ToString()
            message.From = New System.Net.Mail.MailAddress(ConfigurationManager.AppSettings("ContactEmailFrom").ToString())


            If (String.IsNullOrEmpty(Request.Params("Area_of_Interest"))) Then
                message.Subject = "Website Inquiry"
            Else
                message.Subject = "Website Inquiry: " & Request.Params("Area_of_Interest")
            End If

            message.ReplyTo = New MailAddress(Request.Params("email"))
            Dim SmtpMail As New SmtpClient
            SmtpMail.EnableSsl = True
            Dim basicAuthenticationInfo As New System.Net.NetworkCredential(ConfigurationManager.AppSettings("SMTPUser").ToString(), ConfigurationManager.AppSettings("SMTPPassword").ToString())
            SmtpMail.Host = ConfigurationManager.AppSettings("SMTPServer").ToString()
            SmtpMail.Port = ConfigurationManager.AppSettings("SMTPPort").ToString()
            SmtpMail.UseDefaultCredentials = False
            SmtpMail.Credentials = basicAuthenticationInfo
            SmtpMail.Send(message)
            WriteToFile()

            Dim Response As HttpResponse = Context.Response

            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.BufferOutput = False
            HttpContext.Current.Response.ContentType = "text/html"
            Context.HttpContext.Current.Response.Write("1")
            HttpContext.Current.Response.Flush()
            Try
                HttpContext.Current.Response.End()
            Catch ex As Exception

            End Try
            Thread.Sleep(5000)
        Catch ex As Exception
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.Write("-9")
            HttpContext.Current.Response.Write(ex.Message)
            HttpContext.Current.Response.Flush()
            Try
                HttpContext.Current.Response.End()
            Catch ex1 As Exception

            End Try
            Thread.Sleep(5000)
        End Try

    End Sub
    Public Sub WriteToFile()
        Try
            Dim FILE_NAME As String = Server.MapPath("submissions/contact.txt")
            Dim Msg As String
            Msg = Request.Params("Name") & "|"
            Msg &= Request.Params("Email") & "|" 
            Msg &= Request.Params("Area_of_Interest") & "|" 
            Msg &= Request.Params("Comments") & "|" 
            Msg &= Request.ServerVariables("REMOTE_ADDR").ToString()
            Msg &= Environment.NewLine

            If System.IO.File.Exists(FILE_NAME) = True Then
                Dim file_stream As FileStream = File.Open(FILE_NAME, FileMode.Append)
                Dim bytes As Byte() = New UTF8Encoding().GetBytes(Msg)
                file_stream.Write(bytes, 0, bytes.Length)
                file_stream.Flush()
                file_stream.Close()
            End If
        Catch ex As Exception

        End Try
      
    End Sub
End Class