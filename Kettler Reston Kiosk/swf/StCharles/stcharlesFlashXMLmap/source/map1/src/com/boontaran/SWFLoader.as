/*
3 oct 2010		: - add a canceling method
*/
package com.boontaran {
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	import flash.events.*;
	
	
	public class SWFLoader {
		private static var calls:Dictionary = new Dictionary(true);
		
		public static function load(file:String,onComplete:Function,onNotFound:Function=null,onProgress:Function=null):Loader {
			var ldr:Loader = new Loader();
			ldr.contentLoaderInfo.addEventListener(Event.COMPLETE ,completed);
			ldr.load(new URLRequest(file));
			
			var call:Array = new Array();
			call['onComplete'] = onComplete;
			
			if(onNotFound != null) {
				ldr.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,  ioError);
				call['notFound'] = onNotFound;
			} 
			if(onProgress != null)  {
				ldr.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, progress);
				call['onProgress'] = onProgress;
			}
			calls[ldr]=call;
			
			return ldr;
		}
		// !!!
		public static function cancel(ldr:Loader):void {
			ldr.close();
			delete calls[ldr];
		}
		private static function completed(e:Event):void {
			var ldr:Loader = e.target.loader;
			
			removeListeners(ldr);
			
			var callBack:Function = calls[ldr]['onComplete'];
			delete calls[ldr];
			callBack(ldr);
		}
		private static function ioError(e:Event):void {
			var ldr:Loader = e.target.loader;
			removeListeners(ldr);
			
			var callBack:Function = calls[ldr]['notFound'] ;
			delete calls[ldr];
			callBack();
		}
		private static function progress(e:ProgressEvent):void {
			var ldr:Loader = e.target.loader;
			var callBack:Function = calls[ldr]['onProgress'] ;
			callBack(Number(e.bytesLoaded/e.bytesTotal));
		}
		private static function removeListeners(ldr:Loader):void {
			if(calls[ldr]['onComplete']) {
				ldr.contentLoaderInfo.removeEventListener(Event.COMPLETE , completed);
			} 
			if(calls[ldr]['onNotFound']) {
				ldr.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR,  ioError);
			} 
			if(calls[ldr]['onProgress'])  {
				ldr.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, progress);	
			}
		}
	
	}
}