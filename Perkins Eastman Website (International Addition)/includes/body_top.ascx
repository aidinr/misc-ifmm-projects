﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="body_top.ascx.vb" Inherits="includes_body_top" %>
<div class="global">
	<div class="header">

<div>
</div></div>
	<div class="nav"><ul id="MenuBar5" class="MenuBarHorizontal">
  <li><a href="Default.aspx" style="width:65px;text-align:left">&nbsp;&nbsp;&nbsp;HOME</a></li>
  <li><a class="MenuBarItemSubmenu" href="firmProfile.aspx" style="width:80px">ABOUT US</a>
   	<ul>
    	<li><a href="firmProfile.aspx" style="font-size:10px">FIRM PROFILE</a></li>
    	<li><a href="designPhilosophy.aspx" style="font-size:10px">DESIGN PHILOSOPHY</a></li>
        <li><a href="leadership.aspx" style="font-size:10px">LEADERSHIP</a></li>
        <!--<li><a href="services.aspx" style="font-size:10px">SERVICES</a></li>-->
    	<li><a href="awards.aspx" style="font-size:10px">AWARDS</a></li>
   	</ul>
  </li>
  <li><a class="MenuBarItemSubmenu" href="projects.aspx?list=2400012" style="width:85px">PROJECTS</a>
   	    <ul>
    	
    	<asp:Repeater ID="repeaterCategories" runat="server">
    	
    	<ItemTemplate><li><a href="projects.aspx?list=<%#Container.DataItem("projectid") %>" style="font-size:10px"><%#Container.DataItem("name").ToString.ToUpper%></a></li></ItemTemplate>
    	
    	</asp:Repeater>
     	</ul>
  </li>
  <li><a class="MenuBarItemSubmenu" href="projects.aspx?list=<%=ConfigurationSettings.AppSettings("internationalProjectID") %>" style="width:110px">INTERNATIONAL</a>
        <ul>
        	<li><a href="projects.aspx?list=<%=ConfigurationSettings.AppSettings("internationalProjectID") %>" style="font-size:10px">INTERNATIONAL PORTFOLIO</a></li>
			<li><a href="Presentation/Home.html" onClick="window.open('http://www.perkinseastman.com/Presentation/Home.html','popup','width=800,height=600,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false;" style="font-size:10px">CHINA PORTFOLIO</a></li>
        </ul>
      </li>
  <li><a href="press.aspx" style="width:60px">&nbsp;&nbsp;&nbsp;&nbsp;NEWS</a></li>
  <li><a href="publications.aspx" style="width:90px">PUBLICATIONS</a></li>
  <li><a class="MenuBarItemSubmenu" href="careers.aspx" style="width:70px">CAREERS</a>
   	<ul>
    	<li><a href="careers.aspx" style="font-size:10px">SUBMIT RESUME</a></li>
		<li><a href="internships.aspx" style="font-size:10px">INTERNSHIPS</a></li>
    	<!--<li><a href="employee_spotlight.aspx" style="font-size:10px">EMPLOYEE SPOTLIGHT</a></li>-->
   </ul>
  </li>
  <li><a href="client.aspx" style="width:62px">&nbsp;LOG IN</a>
     	<ul>
    	<li><a href="client.aspx" style="font-size:10px">CLIENT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
		<li><a href="staff.aspx" style="font-size:10px">STAFF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
   </ul>
  </li>
<li><a href="locations.aspx" style="width:80px">&nbsp;&nbsp;LOCATIONS</a></li>
</ul></div><script type="text/javascript">
               var MenuBar5 = new Spry.Widget.MenuBar("MenuBar5", { imgDown: "images/down.gif", imgRight: "images/down.gif" });</script>
<!--
<script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script>
<script type="text/javascript" src="http://twitter.com/statuses/user_timeline/Perkins Eastman.json?callback=twitterCallback2&amp;count=2"></script>
-->
<div class="main">