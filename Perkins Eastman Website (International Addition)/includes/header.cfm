<cfinclude template="wsheader.cfm">
<cfscript>
	result = webService.getProjectCategory(username, password, toString(firmid), '', '');
	result = json.decode(result);
	pCats = result.PROJECTCATEGORY;	
</cfscript>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<!--Website Design by Katy Gillen of Perkins Eastman-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="architect, architects, architecture, design, interior design" />
<title>Perkins Eastman</title>
<link href="css/peapc.css" rel="stylesheet" type="text/css" />
<cfinclude template="jquery.cfm">
<link href="css/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<script language=JavaScript>
<!--

//Disable right mouse click Script
//By Maximus (maximus@nsimail.com) w/ mods by DynamicDrive
//For full source code, visit http://www.dynamicdrive.com

var message="All photographs are copyrighted material licensed to Perkins Eastman. Any other use is not permitted without written consent. Please contact Webmaster.";

///////////////////////////////////
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}

function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}

if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}

document.oncontextmenu=new Function("alert(message);return false")

// --> 
</script>
<script src="scripts/SpryMenuBar.js" type="text/javascript"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/

#MenuBar5 li {
	width: auto;
	white-space: nowrap;
}
#MenuBar5 ul {
	width: auto;
}
#MenuBar5 ul li {
	float: none;
	background-color: transparent;
}
#MenuBar5 a.MenuBarItemSubmenu {
	background-position: 100% 50%;
}
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
</head>
<body>
<div class="global">
	<div class="header">
    <div id="twitter_div">

<div id="twitter_update_list"></div>
</div>
<div>
<script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script>
<script type="text/javascript" src="http://twitter.com/statuses/user_timeline/perkinseastman.json?callback=twitterCallback2&amp;count=1"></script>
</div></div>
	<div class="nav"><ul id="MenuBar5" class="MenuBarHorizontal">
  <li><a href="index.cfm" style="width:65px;text-align:left">&nbsp;&nbsp;&nbsp;HOME</a></li>
  <li><a class="MenuBarItemSubmenu" href="firmProfile.cfm" style="width:80px">ABOUT US</a>
   	<ul>
    	<li><a href="firmProfile.cfm" style="font-size:10px">FIRM PROFILE</a></li>
    	<li><a href="designPhilosophy.cfm" style="font-size:10px">DESIGN PHILOSOPHY</a></li>
        <li><a href="leadership.cfm" style="font-size:10px">LEADERSHIP</a></li>
        <!--<li><a href="services.cfm" style="font-size:10px">SERVICES</a></li>-->
    	<li><a href="awards.cfm" style="font-size:10px">AWARDS</a></li>
   	</ul>
  </li>
  <li><a class="MenuBarItemSubmenu" href="#" style="width:85px">PROJECTS</a>
   	<ul>
    	<cfloop from = "1" to = #ArrayLen(pCats.PROJECTSUBCATEGORIES)# index = "i">
			<cfset subCat = pCats.PROJECTSUBCATEGORIES[i]>
    		<cfset exclude = 'ON THE BOARDS,INTERNATIONAL,FEATURED PROJECTS'>
    		<cfif NOT ListFind(exclude, ucase(subCat.NAME))>
				<cfoutput><li><a href="projects.cfm?list=#subCat.ID#" style="font-size:10px">#ucase(subCat.NAME)#</a></li></cfoutput>
        	<cfelseif ucase(subCat.NAME) EQ "INTERNATIONAL">
        		<cfset InternationalCategoryID = subCat.ID>
        	<cfelseif ucase(subCat.NAME) EQ "FEATURED PROJECTS">
        		<cfset FeaturedProjectsCategoryID = subCat.ID>
			</cfif>
        </cfloop>
   	</ul>
  </li>
  <cfif IsDefined('InternationalCategoryID')>
      <li><cfoutput><a class="MenuBarItemSubmenu" href="projects.cfm?list=#InternationalCategoryID#" style="width:110px">INTERNATIONAL</a></cfoutput>
        <ul>
        	<li><cfoutput><a href="projects.cfm?list=#InternationalCategoryID#" style="font-size:10px">INTERNATIONAL PORTFOLIO</a></li></cfoutput>
			<li><a href="China Portfolio" onClick="window.open('http://www.perkinseastman.com/Presentation/Home.html','popup','width=800,height=600,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false;" style="font-size:10px">CHINA PORTFOLIO</a></li>
        </ul>
      </li>
  </cfif>
  <li><a href="press.cfm" style="width:60px">&nbsp;&nbsp;&nbsp;&nbsp;NEWS</a></li>
  <li><a href="publications.cfm" style="width:90px">PUBLICATIONS</a></li>
  <li><a class="MenuBarItemSubmenu" href="careers.cfm" style="width:70px">CAREERS</a>
   	<ul>
    	<li><a href="careers.cfm" style="font-size:10px">WHAT MAKES US DIFFERENT?</a></li>
    	<li><a href="currentOpenings.cfm" style="font-size:10px">CURRENT OPENINGS</a></li>
    	<li><a href="resume.cfm" style="font-size:10px">SUBMIT RESUME</a></li>
		<li><a href="internships.cfm" style="font-size:10px">INTERNSHIPS</a></li>
    	<!--<li><a href="employee_spotlight.cfm" style="font-size:10px">EMPLOYEE SPOTLIGHT</a></li>-->
   </ul>
  </li>
  <li><a href="ftp://ftp.perkinseastman.com" style="width:62px" target="_blank">&nbsp;FTP SITE</a></li>
<li><a href="locations.cfm" style="width:80px">&nbsp;&nbsp;LOCATIONS</a></li>
</ul></div><script type="text/javascript">
var MenuBar5 = new Spry.Widget.MenuBar("MenuBar5", {imgDown:"images/down.gif", imgRight:"images/down.gif"});</script>
<script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script>
<script type="text/javascript" src="http://twitter.com/statuses/user_timeline/Perkins Eastman.json?callback=twitterCallback2&amp;count=2"></script>
<div class="main">