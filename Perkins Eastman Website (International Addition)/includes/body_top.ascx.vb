﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class includes_body_top
    Inherits System.Web.UI.UserControl

    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sql As String = "select * from IPM_project where category_id = @cat_id and AVAILABLE = 'y' and publish = 1   order by name asc" 'and name <> 'International'

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim catIDParameter As New SqlParameter("@cat_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(catIDParameter)
        MyCommand1.SelectCommand.Parameters("@cat_id").Value = ConfigurationSettings.AppSettings("websiteCategoryID")

        Dim dt1 As DataTable = New DataTable

        MyCommand1.Fill(dt1)

        repeaterCategories.DataSource = dt1
        repeaterCategories.DataBind()


    End Sub

End Class
