﻿<%@ control language="VB" autoeventwireup="false" inherits="includes_body_footer" CodeFile="body_footer.ascx.vb" %>
</div>
<div class="footer" align="center"><br />
Copyright &copy; 2011 Perkins Eastman. All rights reserved. Contact <a href="mailto:s.yates@perkinseastman.com">Webmaster</a>.
&nbsp; <a href="rssFeeds.aspx">RSS Feed</a>.&nbsp;<a href="rssFeeds.cfm"><img src="./images/rss_feed.gif" border="0" /></a>&nbsp;Follow us on  <a href="http://twitter.com/PerkinsEastman" target="_blank">Twitter </a> and <a href="http://www.facebook.com/#!/pages/New-York-NY/Perkins-Eastman/100930636494?v=wall&ref=ts" target="_blank">Facebook</a>.
<script type="text/javascript">
    var emptyText = "Search Perkins Eastman";

    function focusSearch() {
        jQuery("input[name='s']").removeClass("gray");
        if (jQuery("input[name='s']").attr("value") == emptyText) {
            jQuery("input[name='s']").attr("value", '');
        }
        jQuery("input[name='s']").select();
    }

    function changeSearch() {
        if (jQuery("input[name='s']").attr("value") == emptyText) {
            jQuery("input[name='s']").addClass("gray");
        } else {
            jQuery("input[name='s']").removeClass("gray");
        }
    }

    function blurSearch() {
        jQuery("input[name='s']").addClass("gray");
        if (jQuery("input[name='s']").attr("value") == '') {
            jQuery("input[name='s']").attr("value", emptyText);
        }
    }

</script>
<input type="text" name="s" value="Search Perkins Eastman" class="searchString gray" onfocus="javascript: focusSearch();" onchange="javascript: changeSearch();" onblur="javascript: blurSearch();" />
<input type="hidden" name="submit" value="1" />
<input type="image" src="./images/search.gif" width="56" class="searchButton" />
</div>
<script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-6618817-2");
        pageTracker._trackPageview();
    } catch (err) { }
</script>
</body>
</html>