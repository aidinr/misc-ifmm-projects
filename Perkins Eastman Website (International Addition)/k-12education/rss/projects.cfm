<cfinclude template="../includes/wsheader.cfm">

<cfscript>
	function rootWeb() {
		s = arrayToList(listToArray(CGI.SCRIPT_NAME, "/"), "/");
		s = listDeleteAt(s, listLen(s, "/"), "/");
		s = listDeleteAt(s, listLen(s, "/"), "/");
		s = 'HTTP://' & CGI.HTTP_HOST & "/" & s & "/";
		return s;
	}	
	
	results = webservice.getProjectRSS(username, password, firmid, 'Perkins Eastman Projects', "http://" & CGI.HTTP_HOST & CGI.SCRIPT_NAME, 'On The Boards,Featured Projects','', "#rootWeb()#projectDetails.cfm?p=PROJECT_ID");
</cfscript>

<cfcontent type="text/xml" reset="yes"><cfoutput>#results#</cfoutput>