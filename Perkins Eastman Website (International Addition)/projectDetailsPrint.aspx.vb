﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient



Partial Class projectDetailsPrint
    Inherits System.Web.UI.Page

    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim projectID As String = Request.QueryString("p")
        Dim imageIndex As String = Request.QueryString("i")

        Dim sql = "select *, a.Name projectname, c.Item_Value cityName, d.Item_Value stateName, e.Item_Value countryName, f.item_value projectDescription  from ipm_project_related b, IPM_PROJECT a left join IPM_PROJECT_FIELD_VALUE c on a.ProjectID = c.ProjectID and c.Item_ID = @cityUDFID left join IPM_PROJECT_FIELD_VALUE d on a.ProjectID = d.ProjectID and d.Item_ID = @stateUDFID left join IPM_PROJECT_FIELD_VALUE e on a.ProjectID = e.ProjectID and e.Item_ID = @countryUDFID left join ipm_project_field_value f on f.projectid = a.projectid and f.item_id = @longDescriptionUDFID where  a.ProjectID = b.Ref_Id and a.projectid = @project_id"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim projectIDParameter As New SqlParameter("@project_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(projectIDParameter)
        MyCommand1.SelectCommand.Parameters("@project_id").Value = projectID

        Dim cityIDParameter As New SqlParameter("@cityUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(cityIDParameter)
        MyCommand1.SelectCommand.Parameters("@cityUDFID").Value = ConfigurationSettings.AppSettings("cityUDFID")

        Dim stateIDParameter As New SqlParameter("@stateUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(stateIDParameter)
        MyCommand1.SelectCommand.Parameters("@stateUDFID").Value = ConfigurationSettings.AppSettings("stateUDFID")

        Dim countryIDParameter As New SqlParameter("@countryUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(countryIDParameter)
        MyCommand1.SelectCommand.Parameters("@countryUDFID").Value = ConfigurationSettings.AppSettings("countryUDFID")

        Dim longDescriptionIDParameter As New SqlParameter("@longDescriptionUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(longDescriptionIDParameter)
        MyCommand1.SelectCommand.Parameters("@longDescriptionUDFID").Value = ConfigurationSettings.AppSettings("DescriptionLongUDFID")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        Dim categoryID As String = ""

        If (dt1.Rows.Count > 0) Then
            Try


                literalName.Text = dt1.Rows(0)("name").ToString.Trim
                literalCity.Text = dt1.Rows(0)("cityname").ToString.Trim & ", "
                literalDescription.Text = dt1.Rows(0)("projectDescription").ToString.Trim




                If (Not IsDBNull(dt1.Rows(0)("statename"))) Then


                    If (Trim(dt1.Rows(0)("statename")) = "") Then
                        If Not (IsDBNull(dt1.Rows(0)("countryname"))) Then
                            If (Trim(dt1.Rows(0)("countryname")) <> "") Then
                                literalStateOrCountry.Text = dt1.Rows(0)("countryname")
                            End If
                        End If
                    ElseIf (Trim(dt1.Rows(0)("statename")) <> "") Then
                        literalStateOrCountry.Text = dt1.Rows(0)("statename")
                    End If
                ElseIf Not (IsDBNull(dt1.Rows(0)("countryname"))) Then
                    If (Trim(dt1.Rows(0)("countryname")) <> "") Then
                        literalStateOrCountry.Text = dt1.Rows(0)("countryname")
                    End If
                End If



            Catch ex As Exception

            End Try
        End If






        Dim sql2 As String = "select top 9 * from (select a.Asset_ID,a.projectid,1 apply,c.Item_value from ipm_asset_category b,ipm_asset a left join ipm_asset_field_value c on c.item_id = @orderingUDFID and c.asset_id = a.asset_id where a.projectid = @projectid and a.category_id = b.category_id and a.available = 'y' and b.available = 'y' and b.name = 'Public Website Images' UNION select a.Asset_ID,a.projectid,b.Item_Value apply,c.Item_value from ipm_asset a left join ipm_asset_field_value B on B.item_id = @applytowebsiteUDFID and B.asset_id = a.asset_id left join ipm_asset_field_value c on c.item_id = @orderingUDFID and c.asset_id = a.asset_id where a.projectid = @projectid AND a.available = 'y' and B.Item_value = 1 and b.Item_Value is not null ) a order by a.item_value asc"

        'select top 9 * from (select a.Asset_ID,a.projectid,1 apply,c.Item_value from ipm_asset_category b,ipm_asset a left join ipm_asset_field_value c on c.item_id = @orderingUDFID and c.asset_id = a.asset_id where a.projectid = @projectid and a.category_id = b.category_id and a.available = 'y' and b.available = 'y' and b.name = 'Public Website Images' UNION select a.Asset_ID,a.projectid,b.Item_Value apply,c.Item_value from ipm_asset a left join ipm_asset_field_value B on B.item_id = @applytowebsiteUDFID and B.asset_id = a.asset_id left join ipm_asset_field_value c on c.item_id = @orderingUDFID and c.asset_id = a.asset_id where a.projectid = @projectid AND a.available = 'y' and B.Item_ID = 1 and b.Item_Value is not null ) a order by a.item_value asc



        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)

        Dim projectIDParameter2 As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(projectIDParameter2)
        MyCommand2.SelectCommand.Parameters("@projectid").Value = projectID

        Dim folderNameParameter As New SqlParameter("@folderName", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(folderNameParameter)
        MyCommand2.SelectCommand.Parameters("@folderName").Value = ConfigurationSettings.AppSettings("PublicFolderName")

        Dim orderingParameter As New SqlParameter("@orderingUDFID", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(orderingParameter)
        MyCommand2.SelectCommand.Parameters("@orderingUDFID").Value = ConfigurationSettings.AppSettings("assetOrderingUDFID")

        Dim applytowebsiteParameter As New SqlParameter("@applytowebsiteUDFID", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(applytowebsiteParameter)
        MyCommand2.SelectCommand.Parameters("@applytowebsiteUDFID").Value = ConfigurationSettings.AppSettings("applytowebsiteUDFID")



        Dim dt2 As New DataTable
        MyCommand2.Fill(dt2)

        Try

            projectImage.ImageUrl = Session("WSRetrieveAsset") & "type=asset&size=1&width=600&height=480&id=" & dt2.Rows(imageIndex)("asset_id")


        Catch ex As Exception

        End Try

    End Sub

End Class
