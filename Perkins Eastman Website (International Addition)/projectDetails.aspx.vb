﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient


Partial Class projectDetails
    Inherits System.Web.UI.Page

    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)

        DisplayProject()


    End Sub

    Sub DisplayProject()

        Dim projectID As String = Request.QueryString("p")
        Dim catID As String = Request.QueryString("c")



        Dim sql = "select *, a.Name projectname, x.name categoryname, c.Item_Value cityName, d.Item_Value stateName, e.Item_Value countryName, x.projectid categoryid, f.item_value projectDescription  from ipm_project_related b, ipm_project x, IPM_PROJECT a left join IPM_PROJECT_FIELD_VALUE c on a.ProjectID = c.ProjectID and c.Item_ID = @cityUDFID left join IPM_PROJECT_FIELD_VALUE d on a.ProjectID = d.ProjectID and d.Item_ID = @stateUDFID left join IPM_PROJECT_FIELD_VALUE e on a.ProjectID = e.ProjectID and e.Item_ID = @countryUDFID left join ipm_project_field_value f on f.projectid = a.projectid and f.item_id = @longDescriptionUDFID where a.Available = 'y' and x.Available = 'y' and a.ProjectID = b.Ref_Id and b.Project_Id = x.projectid and a.projectid = @project_id"
        If (catID <> "") Then
            sql = sql & " and x.projectid = @cat_id"
        End If
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim projectIDParameter As New SqlParameter("@project_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(projectIDParameter)
        MyCommand1.SelectCommand.Parameters("@project_id").Value = projectID

        Dim catIDParameter As New SqlParameter("@cat_id", SqlDbType.VarChar, 255)
        If (catID <> "") Then
            MyCommand1.SelectCommand.Parameters.Add(catIDParameter)
            MyCommand1.SelectCommand.Parameters("@cat_id").Value = catID
        End If


        Dim cityIDParameter As New SqlParameter("@cityUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(cityIDParameter)
        MyCommand1.SelectCommand.Parameters("@cityUDFID").Value = ConfigurationSettings.AppSettings("cityUDFID")

        Dim stateIDParameter As New SqlParameter("@stateUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(stateIDParameter)
        MyCommand1.SelectCommand.Parameters("@stateUDFID").Value = ConfigurationSettings.AppSettings("stateUDFID")

        Dim countryIDParameter As New SqlParameter("@countryUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(countryIDParameter)
        MyCommand1.SelectCommand.Parameters("@countryUDFID").Value = ConfigurationSettings.AppSettings("countryUDFID")

        Dim longDescriptionIDParameter As New SqlParameter("@longDescriptionUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(longDescriptionIDParameter)
        MyCommand1.SelectCommand.Parameters("@longDescriptionUDFID").Value = ConfigurationSettings.AppSettings("DescriptionLongUDFID")




        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        Dim categoryID As String = ""

        If (dt1.Rows.Count > 0) Then
            Try
                literalBrProjectName.Text = dt1.Rows(0)("projectname").ToString.Trim

                literalName.Text = dt1.Rows(0)("projectname").ToString.Trim
                literalPageTitle.Text = "Perkins Eastman: " & dt1.Rows(0)("projectname").ToString.Trim

                literalDescription.Text = stripHTMLTag(dt1.Rows(0)("projectDescription").ToString.Trim, "p")
                literalDescription.Text = stripHTMLTag(literalDescription.Text.ToString.Trim, "div")
                literalDescription.Text = stripHTMLTag(literalDescription.Text.ToString.Trim, "P")
                literalDescription.Text = stripHTMLTag(literalDescription.Text.ToString.Trim, "DIV")

                categoryID = dt1.Rows(0)("categoryid")

                If (catID = "") Then
                    catID = categoryID
                End If

                linkBackToCategory.NavigateUrl = "projects.aspx?list=" & categoryID
                linkBackToCategory.Text = dt1.Rows(0)("categoryname").ToString.Trim

                literalBrCatName.Text = "<a href=""projects.aspx?list=" & categoryID & """>" & dt1.Rows(0)("categoryname").ToString.Trim & "</a>"


                If (Not IsDBNull(dt1.Rows(0)("cityname"))) Then
                    literalCity.Text = dt1.Rows(0)("cityname").ToString.Trim & ", "

                End If


                If (Not IsDBNull(dt1.Rows(0)("statename"))) Then


                    If (Trim(dt1.Rows(0)("statename")) = "") Then
                        If Not (IsDBNull(dt1.Rows(0)("countryname"))) Then
                            If (Trim(dt1.Rows(0)("countryname")) <> "") Then
                                literalStateOrCountry.Text = dt1.Rows(0)("countryname")
                            End If
                        End If
                    ElseIf (Trim(dt1.Rows(0)("statename")) <> "") Then
                        literalStateOrCountry.Text = dt1.Rows(0)("statename")
                    End If
                ElseIf Not (IsDBNull(dt1.Rows(0)("countryname"))) Then
                    If (Trim(dt1.Rows(0)("countryname")) <> "") Then
                        literalStateOrCountry.Text = dt1.Rows(0)("countryname")
                    End If
                End If

            Catch ex As Exception

            End Try
        End If

        Dim sql2 As String = "select top 9 * from (select a.Asset_ID,a.projectid,1 apply,c.Item_value from ipm_asset_category b,ipm_asset a left join ipm_asset_field_value c on c.item_id = @orderingUDFID and c.asset_id = a.asset_id where a.projectid = @projectid and a.category_id = b.category_id and a.available = 'y' and b.available = 'y' and b.name = 'Public Website Images' UNION select a.Asset_ID,a.projectid,b.Item_Value apply,c.Item_value from ipm_asset a left join ipm_asset_field_value B on B.item_id = @applytowebsiteUDFID and B.asset_id = a.asset_id left join ipm_asset_field_value c on c.item_id = @orderingUDFID and c.asset_id = a.asset_id where a.projectid = @projectid AND a.available = 'y' and B.Item_value = 1 and b.Item_Value is not null ) a order by a.item_value asc"

        'select top 9 * from (select a.Asset_ID,a.projectid,1 apply,c.Item_value from ipm_asset_category b,ipm_asset a left join ipm_asset_field_value c on c.item_id = @orderingUDFID and c.asset_id = a.asset_id where a.projectid = @projectid and a.category_id = b.category_id and a.available = 'y' and b.available = 'y' and b.name = 'Public Website Images' UNION select a.Asset_ID,a.projectid,b.Item_Value apply,c.Item_value from ipm_asset a left join ipm_asset_field_value B on B.item_id = @applytowebsiteUDFID and B.asset_id = a.asset_id left join ipm_asset_field_value c on c.item_id = @orderingUDFID and c.asset_id = a.asset_id where a.projectid = @projectid AND a.available = 'y' and B.Item_ID = 1 and b.Item_Value is not null ) a order by a.item_value asc



        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)

        Dim projectIDParameter2 As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(projectIDParameter2)
        MyCommand2.SelectCommand.Parameters("@projectid").Value = projectID

        Dim folderNameParameter As New SqlParameter("@folderName", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(folderNameParameter)
        MyCommand2.SelectCommand.Parameters("@folderName").Value = ConfigurationSettings.AppSettings("PublicFolderName")

        Dim orderingParameter As New SqlParameter("@orderingUDFID", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(orderingParameter)
        MyCommand2.SelectCommand.Parameters("@orderingUDFID").Value = ConfigurationSettings.AppSettings("assetOrderingUDFID")

        Dim applytowebsiteParameter As New SqlParameter("@applytowebsiteUDFID", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(applytowebsiteParameter)
        MyCommand2.SelectCommand.Parameters("@applytowebsiteUDFID").Value = ConfigurationSettings.AppSettings("applytowebsiteUDFID")

        Dim dt2 As New DataTable
        MyCommand2.Fill(dt2)

        repeaterThumbnail.DataSource = dt2
        repeaterThumbnail.DataBind()

        If (dt2.Rows.Count > 0) Then

            literalFirstImage.Text = dt2.Rows(0)("asset_id")

        End If



        Dim sql3 As String = "select * from ipm_project_related b, ipm_project a left join ipm_project_field_value c on a.projectid = c.projectid and c.item_id = @shortNameUDFID where b.project_id = @project_id and a.projectid = b.ref_id and available = 'Y' order by ordering asc"


        Dim MyCommand3 As SqlDataAdapter = New SqlDataAdapter(sql3, MyConnection)

        Dim projectIDParameter3 As New SqlParameter("@project_id", SqlDbType.VarChar, 255)
        MyCommand3.SelectCommand.Parameters.Add(projectIDParameter3)
        MyCommand3.SelectCommand.Parameters("@project_id").Value = catID





        Dim shortNameParameter As New SqlParameter("@shortNameUDFID", SqlDbType.VarChar, 255)
        MyCommand3.SelectCommand.Parameters.Add(shortNameParameter)
        MyCommand3.SelectCommand.Parameters("@shortNameUDFID").Value = ConfigurationSettings.AppSettings("shortNameUDFID")

        Dim dt3 As DataTable = New DataTable
        MyCommand3.Fill(dt3)

        If (dt3.Rows.Count > 0) Then

            Dim i As Integer = 0

            For Each row As DataRow In dt3.Rows

                If (row("projectid").ToString.Trim() = projectID.Trim()) Then
                    Exit For
                End If


                i = i + 1

            Next


            If (i < dt3.Rows.Count - 1) Then
                linkNextProject.NavigateUrl = "projectDetails.aspx?p=" & dt3.Rows(i + 1)("projectid") & "&c=" & catID
                linkNextProject.Text = "NEXT PROJECT"
            Else
                linkNextProject.NavigateUrl = "projectDetails.aspx?p=" & dt3.Rows(0)("projectid") & "&c=" & catID
                linkNextProject.Text = "NEXT PROJECT"
            End If

            If (i = 0) Then
                linkPrevProject.NavigateUrl = "projectDetails.aspx?p=" & dt3.Rows(dt3.Rows.Count - 1)("projectid") & "&c=" & catID
                linkPrevProject.Text = "PREVIOUS PROJECT"
            Else
                linkPrevProject.NavigateUrl = "projectDetails.aspx?p=" & dt3.Rows(i - 1)("projectid") & "&c=" & catID
                linkPrevProject.Text = "PREVIOUS PROJECT"
            End If


        End If






    End Sub

    Function stripHTML(ByVal strHTML As String) As String
        'Strips the HTML tags from strHTML using split and join

        'Ensure that strHTML contains something
        If Len(strHTML) = 0 Then
            stripHTML = strHTML
            Exit Function
        End If

        Dim arysplit, i, j, strOutput

        arysplit = Split(strHTML, "<")

        'Assuming strHTML is nonempty, we want to start iterating
        'from the 2nd array postition
        If Len(arysplit(0)) > 0 Then j = 1 Else j = 0

        'Loop through each instance of the array
        For i = j To UBound(arysplit)
            'Do we find a matching > sign?
            If InStr(arysplit(i), ">") Then
                'If so, snip out all the text between the start of the string
                'and the > sign
                arysplit(i) = Mid(arysplit(i), InStr(arysplit(i), ">") + 1)
            Else
                'Ah, the < was was nonmatching
                arysplit(i) = "<" & arysplit(i)
            End If
        Next

        'Rejoin the array into a single string
        strOutput = Join(arysplit, "")

        'Snip out the first <
        strOutput = Mid(strOutput, 2 - j)

        'Convert < and > to &lt; and &gt;
        'strOutput = Replace(strOutput, ">", "&gt;")
        'strOutput = Replace(strOutput, "<", "&lt;")

        Return strOutput
    End Function

    Function stripHTMLTag(ByVal strHTML As String, ByVal strTag As String) As String
        'Strips the HTML tags from strHTML using split and join

        'Ensure that strHTML contains something
        If Len(strHTML) = 0 Then
            stripHTMLTag = strHTML
            Exit Function
        End If

        Dim arysplit, i, j, strOutput

        arysplit = Split(strHTML, "<" & strTag)

        'Assuming strHTML is nonempty, we want to start iterating
        'from the 2nd array postition
        If Len(arysplit(0)) > 0 Then j = 1 Else j = 0

        'Loop through each instance of the array
        For i = j To UBound(arysplit)
            'Do we find a matching > sign?
            If InStr(arysplit(i), ">") Then
                'If so, snip out all the text between the start of the string
                'and the > sign
                arysplit(i) = Mid(arysplit(i), InStr(arysplit(i), ">") + 1)
            Else
                'Ah, the < was was nonmatching
                arysplit(i) = "<" & arysplit(i)
            End If
        Next

        'Rejoin the array into a single string
        strOutput = Join(arysplit, "")

        'Snip out the first <
        strOutput = Mid(strOutput, 2 - j)

        'Convert < and > to &lt; and &gt;
        'strOutput = Replace(strOutput, ">", "&gt;")
        'strOutput = Replace(strOutput, "<", "&lt;")

        Return strOutput
    End Function


End Class
