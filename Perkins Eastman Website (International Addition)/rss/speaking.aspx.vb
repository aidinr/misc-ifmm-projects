﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Net
Imports System.Xml
Partial Class rss_speaking
    Inherits System.Web.UI.Page

    Protected Sub rss_speaking_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sql = "select a.headline headline, a.content content, b.item_value presenter, c.item_value dates, d.item_value topic, e.item_value location, a.post_date from ipm_news a left join ipm_news_field_value b on b.news_id = a.news_id and b.item_id = @presenter_id left join ipm_news_field_value c on a.news_id = c.news_id and c.item_id = @dates_id left join ipm_news_field_value d on a.news_id = d.news_id and d.item_id = @topic_id left join ipm_news_field_value e on a.news_id = e.news_id and e.item_id = @location_id where type = @type_id and show = 1 and post_date < getdate() and pull_date > getdate() order by post_date desc"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim typeIDParameter As New SqlParameter("@type_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(typeIDParameter)
        MyCommand1.SelectCommand.Parameters("@type_id").Value = ConfigurationSettings.AppSettings("NewsTypeSpeakingEngagement")

        Dim presenterIDParameter As New SqlParameter("@presenter_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(presenterIDParameter)
        MyCommand1.SelectCommand.Parameters("@presenter_id").Value = ConfigurationSettings.AppSettings("presenterItemID")

        Dim datesIDParameter As New SqlParameter("@dates_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(datesIDParameter)
        MyCommand1.SelectCommand.Parameters("@dates_id").Value = ConfigurationSettings.AppSettings("datesItemID")

        Dim topicIDParameter As New SqlParameter("@topic_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(topicIDParameter)
        MyCommand1.SelectCommand.Parameters("@topic_id").Value = ConfigurationSettings.AppSettings("topicItemID")

        Dim locationIDParameter As New SqlParameter("@location_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(locationIDParameter)
        MyCommand1.SelectCommand.Parameters("@location_id").Value = ConfigurationSettings.AppSettings("locationItemID")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)


        GetRss("Perkins Eastman Speaking Engagaments", "http://betawebsite.perkinseastman.com/rss/press.aspx", "", dt1, String.Format("{0:U}", System.DateTime.Now))
    End Sub

    Public Sub GetRss(ByVal pTitle As String, ByVal pLinkURL As String, ByVal pDescription As String, ByVal pDataSource As DataTable, ByVal pDate As String)

        Response.Clear()
        Response.ContentType = "text/xml"
        Dim objX As New XmlTextWriter(Response.OutputStream, Encoding.UTF8)

        Dim theContent As String = ""
        Dim theTopic As String = ""



        objX.WriteStartDocument()
        objX.WriteStartElement("rss")
        objX.WriteAttributeString("version", "2.0")
        objX.WriteAttributeString("xmlns:dc", "http://purl.org/dc/elements/1.1/")
        objX.WriteAttributeString("xmlns:itunes", "http://www.itunes.com/dtds/podcast-1.0.dtd")
        objX.WriteAttributeString("xmlns:rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
        objX.WriteAttributeString("xmlns:taxo", "http://purl.org/rss/1.0/modules/taxonomy/")
        objX.WriteStartElement("channel")
        objX.WriteElementString("title", pTitle)
        objX.WriteElementString("link", pLinkURL)
        objX.WriteElementString("description", pDescription)
        objX.WriteElementString("pubDate", pDate)
        For Each row As DataRow In pDataSource.Rows
            If Not (IsDBNull(row("topic"))) Then
                theTopic = row("topic")
            Else
                theTopic = ""
            End If

            theContent = row("dates") & "<br />" & row("location") & "<br />" & row("presenter") & "<br />" & row("topic") & "<br /><br />" & row("content")


            objX.WriteStartElement("item")
            objX.WriteElementString("title", row("headline"))
            objX.WriteElementString("description", theContent)
            objX.WriteElementString("category", "News")
            objX.WriteElementString("pubDate", String.Format("{0:U}", row("post_date")))
            objX.WriteEndElement()
        Next



        objX.WriteEndElement()
        objX.WriteEndElement()
        objX.WriteEndDocument()
        objX.Flush()
        objX.Close()
        'Response.End()


    End Sub
End Class
