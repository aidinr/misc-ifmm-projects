﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Net
Imports System.Xml



Partial Class rss_press
    Inherits System.Web.UI.Page

    Protected Sub rss_press_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sql = "select * from ipm_news where type = @type_id and show = 1 and post_date < getdate() and pull_date > getdate() order by post_date desc"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim typeIDParameter As New SqlParameter("@type_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(typeIDParameter)
        MyCommand1.SelectCommand.Parameters("@type_id").Value = ConfigurationSettings.AppSettings("NewsTypeGeneralNews")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        GetRss("Perkins Eastman Press", "http://betawebsite.perkinseastman.com/rss/press.aspx", "", dt1, System.DateTime.Now.ToString)
    End Sub

    Public Sub GetRss(ByVal pTitle As String, ByVal pLinkURL As String, ByVal pDescription As String, ByVal pDataSource As DataTable, ByVal pDate As String)

        Response.Clear()
        Response.ContentType = "text/xml"
        Dim objX As New XmlTextWriter(Response.OutputStream, Encoding.UTF8)

        objX.WriteStartDocument()
        objX.WriteStartElement("rss")
        objX.WriteAttributeString("version", "2.0")
        objX.WriteStartElement("channel")
        objX.WriteElementString("title", pTitle)
        objX.WriteElementString("link", pLinkURL)
        objX.WriteElementString("description", pDescription)
        objX.WriteElementString("pubDate", pDate)
        For Each row As DataRow In pDataSource.Rows
            objX.WriteStartElement("item")
            objX.WriteElementString("title", String.Format("{0:MMMM d, yyyy}", row("post_date")))
            objX.WriteElementString("description", row("content"))
            objX.WriteElementString("category", "News")
            objX.WriteElementString("pubDate", row("post_date"))
            objX.WriteEndElement()
        Next



        objX.WriteEndElement()
        objX.WriteEndElement()
        objX.WriteEndDocument()
        objX.Flush()
        objX.Close()
        'Response.End()


    End Sub

  
End Class
