﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Net
Imports System.Xml



Partial Class rss_projects
    Inherits System.Web.UI.Page

    Protected Sub rss_press_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sql = "select a.update_date, a.name longname, a.projectid, c.item_value city_name, d.item_value state_name, e.item_value country_name, f.item_value long_description, g.item_value short_name from ipm_project_related b, ipm_project a left join IPM_PROJECT_FIELD_VALUE c on a.ProjectID = c.ProjectID and c.Item_ID = @cityUDFID left join IPM_PROJECT_FIELD_VALUE d on a.ProjectID = d.ProjectID and d.Item_ID = @stateUDFID left join IPM_PROJECT_FIELD_VALUE e on a.ProjectID = e.ProjectID and e.Item_ID = @countryUDFID left join ipm_project_field_value f on f.projectid = a.projectid and f.item_id = @longDescriptionUDFID left join ipm_project_field_value g on g.projectid = a.projectid and g.item_id = @shortNameUDFID where a.available ='y' and a.projectid = b.ref_id and b.project_id in (select ProjectID from IPM_PROJECT where Available = 'y' and Category_ID = @websiteCategoryID) group by a.projectid, c.item_value, d.item_value, e.item_value, f.item_value, g.item_value, a.update_date, a.name order by short_name"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        MyCommand1.SelectCommand.Parameters.Add(New SqlParameter("@searchtag", SqlDbType.VarChar, 255))
        MyCommand1.SelectCommand.Parameters("@searchtag").Value = ""

        Dim shortNameIDParameter As New SqlParameter("@shortNameUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(shortNameIDParameter)
        MyCommand1.SelectCommand.Parameters("@shortNameUDFID").Value = ConfigurationSettings.AppSettings("ShortNameUDFID")

        Dim cityIDParameter As New SqlParameter("@cityUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(cityIDParameter)
        MyCommand1.SelectCommand.Parameters("@cityUDFID").Value = ConfigurationSettings.AppSettings("cityUDFID")

        Dim stateIDParameter As New SqlParameter("@stateUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(stateIDParameter)
        MyCommand1.SelectCommand.Parameters("@stateUDFID").Value = ConfigurationSettings.AppSettings("stateUDFID")

        Dim countryIDParameter As New SqlParameter("@countryUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(countryIDParameter)
        MyCommand1.SelectCommand.Parameters("@countryUDFID").Value = ConfigurationSettings.AppSettings("countryUDFID")

        Dim longDescriptionIDParameter As New SqlParameter("@longDescriptionUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(longDescriptionIDParameter)
        MyCommand1.SelectCommand.Parameters("@longDescriptionUDFID").Value = ConfigurationSettings.AppSettings("DescriptionLongUDFID")

        Dim websiteCategoryIDParameter As New SqlParameter("@websiteCategoryID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(websiteCategoryIDParameter)
        MyCommand1.SelectCommand.Parameters("@websiteCategoryID").Value = ConfigurationSettings.AppSettings("websiteCategoryID")



        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        GetRss("Perkins Eastman Projects", "http://betawebsite.perkinseastman.com/rss/projects.aspx", "", dt1, System.DateTime.Now.ToString)
    End Sub

    Public Sub GetRss(ByVal pTitle As String, ByVal pLinkURL As String, ByVal pDescription As String, ByVal pDataSource As DataTable, ByVal pDate As String)

        Response.Clear()
        Response.ContentType = "text/xml"
        Dim objX As New XmlTextWriter(Response.OutputStream, Encoding.UTF8)

        objX.WriteStartDocument()
        objX.WriteStartElement("rss")
        objX.WriteAttributeString("version", "2.0")
        objX.WriteStartElement("channel")
        objX.WriteElementString("title", pTitle)
        objX.WriteElementString("link", pLinkURL)
        objX.WriteElementString("description", pDescription)
        objX.WriteElementString("pubDate", pDate)
        For Each row As DataRow In pDataSource.Rows
            objX.WriteStartElement("item")
            objX.WriteElementString("link", "http://www.perkinseastman.com/projectDetails.aspx?p=" & row("projectid"))
            objX.WriteElementString("title", row("longname"))
            objX.WriteElementString("description", "<img src='" & ConfigurationSettings.AppSettings("IDAMCacheLocation") & "PROJECT/" & WebArchives.iDAM.WebCommon.Util.FileUtil.GetAssetPath(row("projectid")).Replace("\", "/") & "/IDAM_PE_1_project_1_60_60_" & row("projectid") & "_1.jpg" & "' style='float: left; border: 1px solid black; margin-right: 10px; margin-bottom: 10px;' />" & row("long_description") & "<br><br><a href='" & "http://www.perkinseastman.com/projectDetails.aspx?p=" & row("projectid") & "' target='_blank'>view project</a>")
            objX.WriteElementString("category", "Projects")
            objX.WriteElementString("pubDate", row("update_date"))
            'objX.WriteStartElement("image")
            'objX.WriteElementString("url", ConfigurationSettings.AppSettings("IDAMCacheLocation") & "PROJECT/" & WebArchives.iDAM.WebCommon.Util.FileUtil.GetAssetPath(row("projectid")).Replace("\", "/") & "/IDAM_PE_1_project_1_60_60_" & row("projectid") & "_1.jpg")
            'objX.WriteElementString("title", "<a href='" & ConfigurationSettings.AppSettings("IDAMCacheLocation") & "PROJECT/" & WebArchives.iDAM.WebCommon.Util.FileUtil.GetAssetPath(row("projectid")).Replace("\", "/") & "/IDAM_PE_1_project_1_60_60_" & row("projectid") & "_1.jpg" & "' style='float: left; border: 1px solid black; margin: 10px; </a>" & row("longname") & "<a href='" & "http://www.perkinseastman.com/projectDetails.aspx?p=" & row("projectid") & "' target='_blank'>view project</a>")
            'objX.WriteElementString("link", "http://www.perkinseastman.com/projectDetails.aspx?p=" & row("projectid"))


            'objX.WriteEndElement()
            objX.WriteEndElement()
        Next



        objX.WriteEndElement()
        objX.WriteEndElement()
        objX.WriteEndDocument()
        objX.Flush()
        objX.Close()
        'Response.End()


    End Sub


End Class
