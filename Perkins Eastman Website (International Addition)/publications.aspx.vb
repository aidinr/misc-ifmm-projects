﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class publications
    Inherits System.Web.UI.Page
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)


        GeneratePublications()


    End Sub

    Sub GeneratePublications()

        Dim sql = "select *, cast(isnull(c.item_value,10000) as int) sort_value from ipm_news a left join ipm_news_field_value b on a.news_id = b.news_id and b.item_id = @authorUDFID left join ipm_news_field_value c on a.news_id = c.news_id and c.item_id = @sortUDFID where type = @type_id and show = 1 and post_date < getdate() and pull_date> getdate() order by sort_value asc, post_date desc"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim typeIDParameter As New SqlParameter("@type_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(typeIDParameter)
        MyCommand1.SelectCommand.Parameters("@type_id").Value = ConfigurationSettings.AppSettings("NewsTypePublication")

        Dim authorIDParameter As New SqlParameter("@authorUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(authorIDParameter)
        MyCommand1.SelectCommand.Parameters("@authorUDFID").Value = ConfigurationSettings.AppSettings("authorItemID")

        Dim sortIDParameter As New SqlParameter("@sortUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(sortIDParameter)
        MyCommand1.SelectCommand.Parameters("@sortUDFID").Value = "2401236"
        'MyCommand1.SelectCommand.Parameters("@sortUDFID").Value = ConfigurationSettings.AppSettings("authorItemID")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        RepeaterPublications.DataSource = dt1
        RepeaterPublications.DataBind()



    End Sub

    Public Sub RepeaterPublications_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        Dim theAuthor As Literal = e.Item.FindControl("literalAuthor")

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            If Not (IsDBNull(e.Item.DataItem("item_value"))) Then
                theAuthor.Text = e.Item.DataItem("item_value").ToString.Trim() & "<br />"
            End If

        End If
    End Sub

End Class
