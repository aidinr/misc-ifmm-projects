﻿<%@ page language="VB" autoeventwireup="false" inherits="leadership" CodeFile="leadership.aspx.vb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Perkins Eastman: Leadership</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
    <meta name="keywords" content="Bradford Perkins, Aaron Schwarz, Daniel Cinelli, Mary-Jean Eastman, Jonathan Stark, Pamela Loeffelman, David Hoglund, Carl Ordemann, Perkins Eastman Black, Susan Black, BFJ Planning, Paul Buckhurst, Frank Fish, Georges Jacquemart, RGR Landscape, Geoffrey Roesch, Tanya Barth , Urbanomics, Regina Armstrong, Russell Design, Anthony Russell" />
</head>
<body>
    <form id="form1" runat="server">
   <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
<script type="text/javascript">
    jQuery(".main").css("background-color", "#FFF");

    function showBio(i) {
        jQuery("#imageLoading").fadeIn("fast", function() {
            jQuery("#bio").load("bioAJAX.aspx?i=" + i, null, function() {
                jQuery("#imageLoading").hide();
                jQuery("#bio").slideDown("slow");
            });
        });
    }

    function closeBio() {
        jQuery("#bio").slideUp("slow");
        jQuery(".shadow").hide();
        jQuery("#imageLoading").hide();
    }
</script>
<!--<cfoutput>-->
<div id="bio" style="position: absolute; border: #666 solid 1px; width: 775px; height: 405px; top: 165px; left: 15px; background-color: #FFF; z-index: 1000; display: none; filter: progid:DXImageTransform.Microsoft.Shadow(Color=gray, direction=135);" onclick="closeBio();"></div>
<img id="imageLoading" src="images/careers/ajax-loader.gif" style="position: absolute; top: 140px; left: 445px; z-index: 1001; display: none;" />
<div class="breadcrumbs"><a href="/">Home</a> | <a href="/firmProfile.aspx">About us</a> | Leadership</div>
<div class="head_standard">Leadership</div>

<asp:Repeater ID="RepeaterExecutives" runat="server" Onitemdatabound="RepeaterExecutives_ItemDataBound">
<HeaderTemplate>
<div class="copy_standard red bold">Principal and Executive Directors</div>
    
		    <div class="copy_standard" style="margin-right: 15px;">

        	<table border="0" cellpadding="0" cellspacing="0" width="100%">

            <tr>
</HeaderTemplate>
<ItemTemplate>
                <td width="33%">
               <%#Container.DataItem("firstname").ToString.Trim & " " & Container.DataItem("lastname").ToString.Trim & " " & Container.DataItem("qualifications").ToString.Trim%> 
                &nbsp;<span class="copy_standard red" style="font-size: 9px; text-transform: lowercase; text-decoration: underline; cursor: pointer; margin-left: 0px;" onclick="showBio(<%#Container.DataItem("userid").ToString.Trim%>);">more info</span>
				</td>
                <asp:Literal ID="RepeaterLineBreak" runat="server"></asp:Literal>
</ItemTemplate>
<FooterTemplate>

                </tr>

            </table>
		
	    </div>

</FooterTemplate>
</asp:Repeater>

<div class="copy_standard red bold">Principals</div>

    
		<div class="copy_standard" style="margin-right: 15px;">
	        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
         <tbody>
           <tr>
			<!--Left Column: Principals-->
            	<td width="33%"valign="top"> 
                    Andrew J. Adelhardt III Esq.<br />
                    Danyal K. Akol AIA, ASHE<br />
                    Christine Albright AIA, LEED AP<br />
                    Roland Baer AIA<br />
                    Shawn Basler AIA<br />
                    Matthew Bell AIA<br/>
                    Jeffrey Brand AIA<br />
                    Peter David Cavaluzzi FAIA<br/>
                    Stefani Danes AIA, LEED AP<br />
                    Joseph DesRosier AIA<br />
                    Susan DiMotta IIDA, AAHID, LEED AP<br />
                    Alexa B. Donaphin AIA, LEED AP<br />
                    William Donohoe<br/>
                    F. Bradford Drake AIA<br/>
                    Stanton Eckstut FAIA<br/>
                    David Ginsberg FAIA<br />
                    James Greenberg AIA<br/>
                    Francis C. Gunther AIA<br />
                    David W. Hance RA, LEED AP <br/>
          </td>
          <!--Middle Column: Principals-->
           		<td width="33%"valign="top">                    
                    Emily J. Kelly AIA<br />
           		    Donald G. King<br />
           		    Stuart Lachs AIA, LEED AP<br />
           		    Robert G. Larsen FAIA, ACHA<br />
           		    Nicholas Leahy AIA, LEED AP<br />
           		    Mitchell Levy AIA, LEED AP<br />
           		    Michael K. Lew AIA<br />
           		    Theodore Liebman FAIA<br />
           		    Mark McCarthy AIA, LEED AP<br />
           		    Leslie G. Moldow FAIA, LEED AP<br />
           		    Richard J. Northway, Jr. AIA, LEED AP<br />
           		    Sean O'Donnell AIA, LEED AP<br/>
           		    John R.A. Pears RIBA<br />
           		    Robert J. Pizzano RA<br />
           		    Stephen Quick FAIA, LEED AP<br />
           		    Duncan W. Reid AIA<br />
           		    Richard Rosen AIA, LEED AP<br />
                    Steve Rosenstein<br />
                   	Carlo Salvador AIA, LEED AP<br />     
                </td>
 <!--Right Column: Principals-->
           		<td width="33%"valign="top">
 		            James L. Sawyer AIA<br />
                    Alan M. Schlossberg AIA, LEED AP<br />
                    David Segmiller AIA<br/>
                    Joseph Shein AIA, FACHA<br />
                    Martin L. Siefering AIA<br />
                    R. Douglas Smith AIA<br/>
                    Richard Sprow AIA<br />
                    Gary D. Steiner AIA<br />
                    Diana Ming Sung AIA<br />
                    Jorge Szendiuch AIA, LEED AP<br />
                    Mark van Summern AIA<br />
                    Ron Vitale AIA<br />
                    Jerry R. Walleck AIA<br />
                    Charles F. Williams<br />
                    Stephen C. Wright AIA <br/>
                    Ming Wu AIA<br/>
                    Ahmad Yousufi<br />
                <br />
                <br />
                </td>
             </tr>
           </tbody></table>	
</div>
<div class="copy_standard red bold">Associate Principals</div>
	<div class="copy_standard" style="margin-right: 15px;">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody><tr>
				<!--Left Column: Associate Principals-->
            	<td width="33%"valign="top"> 	
                    John Amanat AIA, LEED AP<br />
                    Hilary Kinder Bertsch AIA<br/>
                    J. Kilpatrick Burke AIA<br />
                    Omar T. Calderon AIA<br />
                    Maureen Carley-Vallejo IIDA<br />
                    Jonathan Cohn AIA, LEED AP<br/>
                    Danile DeBoo LEED AP<br />
                    Jay P. Epstein AIA<br />
                    Hamilton Esi<br />
                </td>
                <!--Middle Column: Associate Principals-->
                <td width="33%"valign="top"> 
                    Eric Fang AIA, LEED AP<br/>
                    Stephen Forneris AIA<br />
                    Marc F. Goldstein AIA<br />
                    Joseph P. Hassel<br />
                    David L. Hutchinson AIA<br />
                    Alejandro Knopoff LEED AP<br />
                    David Koren CSPM, Assoc. AIA<br />
                    Kim K. Lam<br />
                    Gilles LeGorrec<br />
               </td>
              	<!--Right Column: Associate Principals-->
               <td width="33%"valign="top"> 
                    Robert J. Marino AIA, LEED AP<br />
                    David Meech AIA, LEED AP<br />
                    Fritz Morris AIA, CSI, LEED AP<br/>
                    Smita Rawoot<br />
                    Lynne Rizk<br />
                    Christine Schlendorf<br />
                    Jana G. Silsby AIA, LEED AP<br />
                    Kurt Thompson AIA<br />
				</td>
			</tr>
          </tbody>
	</table>
		
	</div>
    




	<div class="copy_standard red bold">Perkins Eastman Black</div>

    
    	
        	
        	
				<div class="copy_standard bold" style="margin-left: 15px; margin-bottom: 0px;">Principal and Directors</div>
            	<div class="copy_standard" style="margin-left: 15px; margin-right: 15px; margin-top: 0px;">
	
	
    
    
	
	
    
		
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody><tr>
			
            	<td width="33%">
                Bradford Perkins FAIA, MRAIC, AICP
                
				</td>
                </tr><tr>
			
            	<td width="33%">

                Mary-Jean Eastman FAIA, MRAIC, IIDA
                
				</td>
                </tr><tr>
			
            	<td width="33%">
                Susan Black MRAIC, OAA, AOCA
                
				</td>
                </tr><tr>
			
            </tr>
            </tbody></table>
		
	
		
	</div>
            
        
        	
        	
        
        	
        	
        
    




	<div class="copy_standard red bold"><a href="http://www.bfjplanning.com" target="blank">BFJ Planning</a></div>
    
    	
        	
        	
				<div class="copy_standard bold" style="margin-left: 15px; margin-bottom: 0px;">Principals</div>
            	<div class="copy_standard" style="margin-left: 15px; margin-right: 15px; margin-top: 0px;">
	
	
    
    
	
	
    
		
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody><tr>
			
            	<td width="33%">

                Paul Buckhurst ARIBA, AICP
                
				</td>
                </tr><tr>
			
            	<td width="33%">
                Frank S. Fish FAICP
                
				</td>
                </tr><tr>
			
            	<td width="33%">
                Georges Jacquemart PE, AICP
                
				</td>

                </tr><tr>
			
            </tr>
            </tbody></table>
		
	</div>
            
        
        	
        	
        
        	
        	
        
    




	<div class="copy_standard red bold"><a href="http://www.rgrlandscape.com" target="blank">RGR Landscape</a></div>
    
    	
        	
        	
				<div class="copy_standard bold" style="margin-left: 15px; margin-bottom: 0px;">Principals</div>
            	<div class="copy_standard" style="margin-left: 15px; margin-right: 15px; margin-top: 0px;">
	
	
    
    
	
	
    
		
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">

            <tbody><tr>
			
            	<td width="33%">
                R. Geoffrey Roesch ASLA, AIA, LEED AP
                
				</td>
                </tr><tr>
			
            	<td width="33%">
                Tanya L. Barth RA
                
				</td>
                </tr><tr>
			
            </tr>

            </tbody></table>
		
	</div>
            
        
        	
        	
        
    




	<div class="copy_standard red bold"><a href="http://www.urbanomics.org" target="blank">Urbanomics</a></div>
    
    	
        	
        	
				<div class="copy_standard bold" style="margin-left: 15px; margin-bottom: 0px;">Principal</div>
            	<div class="copy_standard" style="margin-left: 15px; margin-right: 15px; margin-top: 0px;">
	
	
    
    
	
	
    
		
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody><tr>
			
            	<td width="33%">

                Regina Armstrong
                
				</td>
                </tr><tr>
			
            </tr>
            </tbody></table>
		
	</div>
            
        
        	
        	
        
    




	<div class="copy_standard red bold"><a href="http://www.russelldesign.com" target="blank">Russell Design</a></div>
    
    	
        	
        	
				<div class="copy_standard bold" style="margin-left: 15px; margin-bottom: 0px;">Principal</div>

            	<div class="copy_standard" style="margin-left: 15px; margin-right: 15px; margin-top: 0px;">
	
	
    
    
	
	
    
		
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody><tr>
			
            	<td width="33%">
                Anthony Russell
                
				</td>
                </tr>
                </tbody> 
                </table> 
                </div> 
<!--
<asp:Repeater ID="RepeaterPrincipals" runat="server" OnItemDataBound="RepeaterPrincipals_ItemDataBound">
<HeaderTemplate>
<div class="copy_standard red bold">Principals</div>
    
		    <div class="copy_standard" style="margin-right: 15px;">

        	<table border="0" cellpadding="0" cellspacing="0" width="100%">

            <tr>
</HeaderTemplate>
<ItemTemplate>
                <td width="33%">
                <%#Container.DataItem("firstname").ToString.Trim & " " & Container.DataItem("lastname").ToString.Trim & " " & Container.DataItem("qualifications").ToString.Trim%> 
                <asp:Literal ID="RepeaterLineBreak" runat="server"></asp:Literal>
</ItemTemplate>
<FooterTemplate>

                </tr>

            </table>
		
	    </div>

</FooterTemplate>
</asp:Repeater>


<asp:Repeater ID="RepeaterAssociatePrincipals" runat="server" OnItemDataBound="RepeaterAssociatePrincipals_ItemDataBound">
<HeaderTemplate>
<div class="copy_standard red bold">Associate Principals</div>
    
		    <div class="copy_standard" style="margin-right: 15px;">

        	<table border="0" cellpadding="0" cellspacing="0" width="100%">

            <tr>
</HeaderTemplate>
<ItemTemplate>
                <td width="33%">
               <%#Container.DataItem("firstname").ToString.Trim & " " & Container.DataItem("lastname").ToString.Trim & " " & Container.DataItem("qualifications").ToString.Trim%> 
				</td>
                <asp:Literal ID="RepeaterLineBreak" runat="server"></asp:Literal>
</ItemTemplate>
<FooterTemplate>

                </tr>

            </table>
		
	    </div>

</FooterTemplate>
</asp:Repeater>


<asp:Repeater ID="RepeaterPerkinsEastmanBlackDirectors" runat="server" OnItemDataBound="RepeaterPerkinsEastmanBlackDirectors_ItemDataBound">
<HeaderTemplate>
<div class="copy_standard red bold">Perkins Eastman Black</div>
<div class="copy_standard bold" style="margin-left: 15px; margin-bottom: 0px;">Principal and Directors</div>

            	<div class="copy_standard" style="margin-left: 15px; margin-right: 15px; margin-top: 0px;">
	
	
    
    
	
	
    
		
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>

</HeaderTemplate>
<ItemTemplate>
                	<td width="33%">
                <%#Container.DataItem("firstname").ToString.Trim & " " & Container.DataItem("lastname").ToString.Trim & " " & Container.DataItem("qualifications").ToString.Trim%> 
				</td>

                <asp:Literal ID="RepeaterLineBreak" runat="server"></asp:Literal>
</ItemTemplate>
<FooterTemplate>
 </tr>
            </table>
		
	</div>

</FooterTemplate>
</asp:Repeater>

<asp:Repeater ID="RepeaterPerkinsEastmanBlackPrincipals" runat="server" OnItemDataBound="RepeaterPerkinsEastmanBlackPrincipals_ItemDataBound">
<HeaderTemplate>
<div class="copy_standard bold" style="margin-left: 15px; margin-bottom: 0px;">Principals</div>

            	<div class="copy_standard" style="margin-left: 15px; margin-right: 15px; margin-top: 0px;">
	
	
    
    
	
	
    
		
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>

</HeaderTemplate>
<ItemTemplate>
                	<td width="33%">
               <%#Container.DataItem("firstname").ToString.Trim & " " & Container.DataItem("lastname").ToString.Trim & " " & Container.DataItem("qualifications").ToString.Trim%> 
				</td>

                <asp:Literal ID="RepeaterLineBreak" runat="server"></asp:Literal>
</ItemTemplate>
<FooterTemplate>
 </tr>
            </table>
		
	</div>

</FooterTemplate>
</asp:Repeater>

<asp:Repeater ID="RepeaterPrincipalsBJFPLanning" runat="server" OnItemdatabound="RepeaterPrincipalsBJFPLanning_DataBound">
<HeaderTemplate>
	<div class="copy_standard red bold"><asp:HyperLink ID="companyURL" runat="server"></asp:HyperLink></div>
    
    	
        	
        	
				<div class="copy_standard bold" style="margin-left: 15px; margin-bottom: 0px;">Principals</div>
            	<div class="copy_standard" style="margin-left: 15px; margin-right: 15px; margin-top: 0px;">
	
	
    
    
	
	
    
		
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
<td width="100%">

</HeaderTemplate>
<ItemTemplate>
                	
               <%#Container.DataItem("firstname").ToString.Trim & " " & Container.DataItem("lastname").ToString.Trim & " " & Container.DataItem("qualifications").ToString.Trim%> <br />
</ItemTemplate>
<FooterTemplate>
</td>
 </tr>
            </table>
		
	</div>

</FooterTemplate>
</asp:Repeater>

<asp:Repeater ID="RepeaterRGRLandscape" runat="server" OnItemdatabound="RepeaterRGRLandscape_ItemDataBound">
<HeaderTemplate>
	<div class="copy_standard red bold"><asp:HyperLink ID="companyURL" runat="server"></asp:HyperLink></div>
    
    	
        	
        	
				<div class="copy_standard bold" style="margin-left: 15px; margin-bottom: 0px;">Principals</div>
            	<div class="copy_standard" style="margin-left: 15px; margin-right: 15px; margin-top: 0px;">
	
	
    
    
	
	
    
		
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>

<td width="100%">
</HeaderTemplate>
<ItemTemplate>
                	
               <%#Container.DataItem("firstname").ToString.Trim & " " & Container.DataItem("lastname").ToString.Trim & " " & Container.DataItem("qualifications").ToString.Trim%> <br />
				

</ItemTemplate>
<FooterTemplate>
</td>
 </tr>
            </table>
		
	</div>

</FooterTemplate>
</asp:Repeater>

<asp:Repeater ID="RepeaterPrincipalsUrbanomics" runat="server" OnItemdatabound="RepeaterPrincipalsUrbanomics_ItemDataBound">
<HeaderTemplate>
	<div class="copy_standard red bold"><asp:HyperLink ID="companyURL" runat="server"></asp:HyperLink></div>
    
    	
        	
        	
				<div class="copy_standard bold" style="margin-left: 15px; margin-bottom: 0px;">Principals</div>
            	<div class="copy_standard" style="margin-left: 15px; margin-right: 15px; margin-top: 0px;">
	
	
    
    
	
	
    
		
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
<td width="100%">

</HeaderTemplate>
<ItemTemplate>
                	
               <%#Container.DataItem("firstname").ToString.Trim & " " & Container.DataItem("lastname").ToString.Trim & " " & Container.DataItem("qualifications").ToString.Trim%> <br />
				

</ItemTemplate>
<FooterTemplate>
</td>
 </tr>
            </table>
		
	</div>

</FooterTemplate>
</asp:Repeater>


<asp:Repeater ID="RepeaterPrincipalsRusselDesign" runat="server" OnItemdatabound="RepeaterPrincpalsRusselDesign_ItemDataBound">
<HeaderTemplate>
	<div class="copy_standard red bold"><asp:HyperLink ID="companyURL" runat="server"></asp:HyperLink></div>
    
    	
        	
        	
				<div class="copy_standard bold" style="margin-left: 15px; margin-bottom: 0px;">Principals</div>
            	<div class="copy_standard" style="margin-left: 15px; margin-right: 15px; margin-top: 0px;">
	
	
    
    
	
	
    
		
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
<td width="100%">

</HeaderTemplate>
<ItemTemplate>
                	
                <%#Container.DataItem("firstname").ToString.Trim & " " & Container.DataItem("lastname").ToString.Trim & " " & Container.DataItem("qualifications").ToString.Trim%> <br />
				

                
</ItemTemplate>
<FooterTemplate>
</td>
 </tr>
            </table>
		
	</div>

</FooterTemplate>
</asp:Repeater>
-->

<div id="blackScreen" style="position: absolute; width: 2048px; height: 2048px; top: -5px; left: -500px; background-color: #000; z-index: 999; overflow: hidden; display: none;"></div>
    
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
