﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions


Partial Class searchResults
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)

        Dim totalSearchCount = 0

        Dim projectSearchCount As Integer = DisplayProjectResults()
        Dim categorySearchCount As Integer = DisplayCategoryResults()
        Dim newsSearchCount As Integer = DisplayNewsResults()
      
        totalSearchCount = projectSearchCount + categorySearchCount + newsSearchCount

        Dim searchString As String = Request.QueryString("s").Trim.Replace("\", "").Replace("'", "").Replace("""", "").Replace(",", "").Replace(";", "")

        literalSearchResults.Text = totalSearchCount.ToString & " search results for <span class=""highlight""><i>" & searchString & " </i></span>"



    End Sub

    Function DisplayProjectResults() As Integer
        Dim searchCount As Integer = 0

        Dim searchString As String = Request.QueryString("s").Trim.Replace("\", "").Replace("'", "").Replace("""", "").Replace(",", "").Replace(";", "")
        Dim sqlSearch As String = ""

        Dim i As Integer = 0

        Dim searchStringArray As String() = searchString.Split(" ")

        If (searchStringArray.Length > 0) Then
            sqlSearch = sqlSearch & " formsof(inflectional," & searchStringArray(0) & ") "
        End If

        For Each s As String In searchStringArray
            If (i > 0) Then
                sqlSearch = sqlSearch & " and formsof(inflectional," & s & ") "
            End If
            i = i + 1
        Next

        Dim sql = "select a.projectid, c.item_value city_name, d.item_value state_name, e.item_value country_name, f.item_value long_description, g.item_value short_name from ipm_project_related b, ipm_project a left join IPM_PROJECT_FIELD_VALUE c on a.ProjectID = c.ProjectID and c.Item_ID = @cityUDFID left join IPM_PROJECT_FIELD_VALUE d on a.ProjectID = d.ProjectID and d.Item_ID = @stateUDFID left join IPM_PROJECT_FIELD_VALUE e on a.ProjectID = e.ProjectID and e.Item_ID = @countryUDFID left join ipm_project_field_value f on f.projectid = a.projectid and f.item_id = @longDescriptionUDFID left join ipm_project_field_value g on g.projectid = a.projectid and g.item_id = @shortNameUDFID where a.available ='y' and a.projectid = b.ref_id and b.project_id in (select ProjectID from IPM_PROJECT where Available = 'y' and Category_ID = @websiteCategoryID) and contains(a.search_tags,@searchtag) group by a.projectid, c.item_value, d.item_value, e.item_value, f.item_value, g.item_value order by short_name"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        MyCommand1.SelectCommand.Parameters.Add(New SqlParameter("@searchtag", SqlDbType.VarChar, 255))
        MyCommand1.SelectCommand.Parameters("@searchtag").Value = sqlSearch


        Dim shortNameIDParameter As New SqlParameter("@shortNameUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(shortNameIDParameter)
        MyCommand1.SelectCommand.Parameters("@shortNameUDFID").Value = ConfigurationSettings.AppSettings("ShortNameUDFID")

        Dim cityIDParameter As New SqlParameter("@cityUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(cityIDParameter)
        MyCommand1.SelectCommand.Parameters("@cityUDFID").Value = ConfigurationSettings.AppSettings("cityUDFID")

        Dim stateIDParameter As New SqlParameter("@stateUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(stateIDParameter)
        MyCommand1.SelectCommand.Parameters("@stateUDFID").Value = ConfigurationSettings.AppSettings("stateUDFID")

        Dim countryIDParameter As New SqlParameter("@countryUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(countryIDParameter)
        MyCommand1.SelectCommand.Parameters("@countryUDFID").Value = ConfigurationSettings.AppSettings("countryUDFID")

        Dim longDescriptionIDParameter As New SqlParameter("@longDescriptionUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(longDescriptionIDParameter)
        MyCommand1.SelectCommand.Parameters("@longDescriptionUDFID").Value = ConfigurationSettings.AppSettings("DescriptionLongUDFID")

        Dim websiteCategoryIDParameter As New SqlParameter("@websiteCategoryID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(websiteCategoryIDParameter)
        MyCommand1.SelectCommand.Parameters("@websiteCategoryID").Value = ConfigurationSettings.AppSettings("websiteCategoryID")


        Dim DT1 As DataTable = New DataTable("SearchResults")

        Try
            MyCommand1.Fill(DT1)
        Catch ex As Exception

        End Try


        searchCount = DT1.Rows.Count

        If (searchCount > 0) Then

            repeaterProjects.DataSource = DT1
            repeaterProjects.DataBind()

            literalCountProjects.Text = searchCount & " Projects"
        End If


        Return searchCount
    End Function
    Function DisplayCategoryResults() As Integer
        'also searches misc pages
        Dim searchCount As Integer = 0

        Dim searchString As String = Request.QueryString("s").Trim.Replace("\", "").Replace("'", "").Replace("""", "").Replace(",", "").Replace(";", "")
        Dim sqlSearch As String = ""

        Dim i As Integer = 0

        Dim searchStringArray As String() = searchString.Split(" ")

        If (searchStringArray.Length > 0) Then
            sqlSearch = sqlSearch & " formsof(inflectional," & searchStringArray(0) & ") "
        End If

        For Each s As String In searchStringArray
            If (i > 0) Then
                sqlSearch = sqlSearch & " and formsof(inflectional," & s & ") "
            End If
            i = i + 1
        Next

        Dim sql = "select a.name, a.projectid, c.item_value long_description from ipm_project a left join IPM_PROJECT_FIELD_VALUE c on a.ProjectID = c.ProjectID and c.Item_ID = @longdescriptionUDFID where a.available ='y' and Category_ID = @websiteCategoryID and contains(a.search_tags,@searchtag) order by a.name"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        MyCommand1.SelectCommand.Parameters.Add(New SqlParameter("@searchtag", SqlDbType.VarChar, 255))
        MyCommand1.SelectCommand.Parameters("@searchtag").Value = sqlSearch

        Dim websiteCategoryIDParameter As New SqlParameter("@websiteCategoryID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(websiteCategoryIDParameter)
        MyCommand1.SelectCommand.Parameters("@websiteCategoryID").Value = ConfigurationSettings.AppSettings("websiteCategoryID")

        Dim longDescriptionIDParameter As New SqlParameter("@longDescriptionUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(longDescriptionIDParameter)
        MyCommand1.SelectCommand.Parameters("@longDescriptionUDFID").Value = ConfigurationSettings.AppSettings("DescriptionLongUDFID")


        Dim DT1 As DataTable = New DataTable("SearchResults")

        Try
            MyCommand1.Fill(DT1)
        Catch ex As Exception

        End Try




      


        Dim sql2 As String = "select *, c.name categoryName from IPM_ASSET a  left join ipm_asset_field_value b on b.Item_ID = @longDescriptionUDFID and a.Asset_ID = b.ASSET_ID left join ipm_asset_category c on a.category_id = c.category_id and c.available = 'y' where a.ProjectID in (select projectid from IPM_PROJECT where Category_ID = @websiteCategoryID and Available   = 'y') and a.Available = 'y' and (media_Type = 21355126 or media_type = 21472985) and contains(a.search_tags,@searchtag)  order by c.name"

        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)
        MyCommand2.SelectCommand.Parameters.Add(New SqlParameter("@searchtag", SqlDbType.VarChar, 255))
        MyCommand2.SelectCommand.Parameters("@searchtag").Value = sqlSearch

        Dim websiteCategoryIDParameter2 As New SqlParameter("@websiteCategoryID", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(websiteCategoryIDParameter2)
        MyCommand2.SelectCommand.Parameters("@websiteCategoryID").Value = ConfigurationSettings.AppSettings("websiteContentCategoryID")

        Dim longDescriptionIDParameter2 As New SqlParameter("@longDescriptionUDFID", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(longDescriptionIDParameter2)
        MyCommand2.SelectCommand.Parameters("@longDescriptionUDFID").Value = ConfigurationSettings.AppSettings("websiteContentUDFID")


        Dim DT2 As DataTable = New DataTable("SearchResults")

        Try
            MyCommand2.Fill(DT2)
        Catch ex As Exception

        End Try

        searchCount = DT1.Rows.Count + DT2.Rows.Count

        If (searchCount > 0) Then
            repeaterCategories.DataSource = DT1
            repeaterCategories.DataBind()
            repeaterMisc.DataSource = DT2
            repeaterMisc.DataBind()




            literalCountCategories.Text = searchCount & " Miscellaneous Pages"

        End If

        

        Return searchCount
    End Function
    Function DisplayNewsResults() As Integer
        Dim searchCount As Integer = 0
        Dim searchString As String = "%" & Request.QueryString("s").Trim.Replace("\", "").Replace("'", "").Replace("""", "").Replace(",", "").Replace(";", "") & "%"

        Dim sql = "select * from ipm_news where Post_Date < GETDATE() and Pull_Date > GETDATE() and Show = 1 and (Headline like @searchString or Content like @searchString ) and (Type = @PressReleaseTypeID or type = @NewsTypeID or type = @SpeakingTypeID or type = @PublicationTypeID) order by post_date"
        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim searchParameter As New SqlParameter("@searchString", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(searchParameter)
        MyCommand1.SelectCommand.Parameters("@searchString").Value = searchString

        Dim pressReleaseIDParameter As New SqlParameter("@PressReleaseTypeID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(pressReleaseIDParameter)
        MyCommand1.SelectCommand.Parameters("@PressReleaseTypeID").Value = ConfigurationSettings.AppSettings("NewsTypePressRelease")

        Dim newsIDParameter As New SqlParameter("@NewsTypeID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(newsIDParameter)
        MyCommand1.SelectCommand.Parameters("@NewsTypeID").Value = ConfigurationSettings.AppSettings("NewsTypeGeneralNews")

        Dim speakingIDParameter As New SqlParameter("@SpeakingTypeID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(speakingIDParameter)
        MyCommand1.SelectCommand.Parameters("@SpeakingTypeID").Value = ConfigurationSettings.AppSettings("NewsTypeSpeakingEngagement")

        Dim publicationIDParameter As New SqlParameter("@PublicationTypeID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(publicationIDParameter)
        MyCommand1.SelectCommand.Parameters("@PublicationTypeID").Value = ConfigurationSettings.AppSettings("NewsTypePublication")


        Dim DT1 As DataTable = New DataTable("SearchResults")

        Try
            MyCommand1.Fill(DT1)
        Catch ex As Exception

        End Try


        searchCount = DT1.Rows.Count

        If (searchCount > 0) Then
            repeaterNews.DataSource = DT1
            repeaterNews.DataBind()


            literalCountPressReleases.Text = searchCount & " News and Publications"
        End If

        
        Return searchCount
    End Function
  

    Public Sub RepeaterProjects_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim literalLocation As Literal = e.Item.FindControl("literalLocation")
            Dim literalDescription As Literal = e.Item.FindControl("literalDescription")
            Dim literalShortName As Literal = e.Item.FindControl("literalShortName")

            Try
                If Not (IsDBNull(e.Item.DataItem("city_name"))) Then
                    literalLocation.Text = e.Item.DataItem("city_name") & ", "
                End If


                If Not (IsDBNull(e.Item.DataItem("state_name"))) Then


                    If (Trim(e.Item.DataItem("state_name")) = "") Then
                        If Not (IsDBNull(e.Item.DataItem("country_name"))) Then
                            If (Trim(e.Item.DataItem("country_name")) <> "") Then
                                literalLocation.Text = literalLocation.Text & e.Item.DataItem("country_name")
                            End If
                        End If
                    ElseIf (Trim(e.Item.DataItem("state_name")) <> "") Then
                        literalLocation.Text = literalLocation.Text & e.Item.DataItem("state_name")
                    End If
                ElseIf Not (IsDBNull(e.Item.DataItem("country_name"))) Then
                    If (Trim(e.Item.DataItem("country_name")) <> "") Then
                        literalLocation.Text = literalLocation.Text & e.Item.DataItem("country_name")
                    End If
                End If

                literalLocation.Text = "(" & literalLocation.Text & ")"

            Catch ex As Exception

            End Try

            'trim description

            Dim searchString As String = Request.QueryString("s").Trim.Replace("\", "").Replace("'", "").Replace("""", "").Replace(",", "").Replace(";", "")

            Dim searchStringArray As String() = searchString.Split(" ")

            Dim descriptionString As String = stripHTML(e.Item.DataItem("long_description").trim)
            Dim shortNameString As String = e.Item.DataItem("short_name").trim
            Dim locationString As String = literalLocation.Text

            Dim firstOccur As New Integer


            If (searchStringArray.Length > 0) Then
                firstOccur = descriptionString.IndexOf(searchStringArray(0), StringComparison.CurrentCultureIgnoreCase)
                If (firstOccur < (Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2)) Then
                    descriptionString = descriptionString.Substring(0, Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) & "..."
                ElseIf (firstOccur + searchStringArray(0).Length + (Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2) > descriptionString.Length - 1) Then
                    descriptionString = "..." & descriptionString.Substring(firstOccur - (Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2), Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2) & descriptionString.Substring(firstOccur, descriptionString.Length - 1 - firstOccur)
                Else
                    descriptionString = "..." & descriptionString.Substring(firstOccur - (Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2), Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2) & descriptionString.Substring(firstOccur, Math.Ceiling(Integer.Parse((ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2) + searchStringArray(0).Length) & "..."
                End If
            End If
           

            For Each s As String In searchStringArray
                descriptionString = Regex.Replace(descriptionString, s, "<span class=""highlight"">" & s & "</span>", RegexOptions.IgnoreCase)
                shortNameString = Regex.Replace(shortNameString, s, "<span class=""highlight"">" & s & "</span>", RegexOptions.IgnoreCase)
                locationString = Regex.Replace(locationString, s, "<span class=""highlight"">" & s & "</span>", RegexOptions.IgnoreCase)

                'descriptionString = descriptionString.Replace(s, "<span class=""highlight"">" & s & "</span>")
                'shortNameString = shortNameString.Replace(s, "<span class=""highlight"">" & s & "</span>")
                'locationString = locationString.Replace(s, "<span class=""highlight"">" & s & "</span>")
            Next

            literalDescription.Text = descriptionString
            literalShortName.Text = shortNameString
            literalLocation.Text = locationString


        End If

    End Sub


    Public Sub repeaterCategories_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim literalDescription As Literal = e.Item.FindControl("literalDescription")
            Dim literalName As Literal = e.Item.FindControl("literalCategoryName")
            Dim hyperlinkPage As HyperLink = e.Item.FindControl("hyperlinkPage")

            'trim description

            Dim searchString As String = Request.QueryString("s").Trim.Replace("\", "").Replace("'", "").Replace("""", "").Replace(",", "").Replace(";", "")

            Dim searchStringArray As String() = searchString.Split(" ")

            Dim descriptionString As String = stripHTML(e.Item.DataItem("long_description").trim)
            Dim NameString As String = e.Item.DataItem("name").trim

            Dim firstOccur As Integer = 0

            If (searchStringArray.Length > 0) Then
                firstOccur = descriptionString.IndexOf(searchStringArray(0), StringComparison.CurrentCultureIgnoreCase)
                If (firstOccur < Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2) Then
                    descriptionString = descriptionString.Substring(0, Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) & "..."
                ElseIf (firstOccur + Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2 + searchStringArray(0).Length > descriptionString.Length - 1) Then
                    descriptionString = "..." & descriptionString.Substring(firstOccur - Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2, Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2) & descriptionString.Substring(firstOccur) & "..."
                Else
                    descriptionString = "..." & descriptionString.Substring(firstOccur - Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2, Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2) & descriptionString.Substring(firstOccur, Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2 + searchStringArray(0).Length) & "..."
                End If
            End If


            For Each s As String In searchStringArray
                descriptionString = Regex.Replace(descriptionString, s, "<span class=""highlight"">" & s & "</span>", RegexOptions.IgnoreCase)
                NameString = Regex.Replace(NameString, s, "<span class=""highlight"">" & s & "</span>", RegexOptions.IgnoreCase)

            Next

            literalDescription.Text = descriptionString
            literalName.Text = NameString
            hyperlinkPage.NavigateUrl = "projects.aspx?list=" & e.Item.DataItem("projectid")


        End If


    End Sub
    Public Sub repeaterNews_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim literalDescription As Literal = e.Item.FindControl("literalDescription")
            Dim literalName As Literal = e.Item.FindControl("literalCategoryName")
            Dim hyperlinkPage As HyperLink = e.Item.FindControl("hyperlinkPage")
            'trim description

            Dim searchString As String = Request.QueryString("s").Trim.Replace("\", "").Replace("'", "").Replace("""", "").Replace(",", "").Replace(";", "")

            Dim searchStringArray As String() = searchString.Split(" ")

            Dim descriptionString As String = stripHTML(e.Item.DataItem("content").trim)
            Dim NameString As String = e.Item.DataItem("headline").trim

            Dim firstOccur As Integer = 0

            If (searchStringArray.Length > 0) Then
                firstOccur = descriptionString.IndexOf(searchStringArray(0), StringComparison.CurrentCultureIgnoreCase)
                If (firstOccur < Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2) Then
                    Try
                        descriptionString = descriptionString.Substring(0, Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) & "..."
                    Catch ex As Exception
                        descriptionString = descriptionString
                    End Try

                ElseIf (firstOccur + Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2 + searchStringArray(0).Length > descriptionString.Length - 1) Then
                    descriptionString = "..." & descriptionString.Substring(firstOccur - Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2, Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2) & descriptionString.Substring(firstOccur) & "..."
                Else
                    descriptionString = "..." & descriptionString.Substring(firstOccur - Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2, Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2) & descriptionString.Substring(firstOccur, Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2 + searchStringArray(0).Length) & "..."
                End If
            End If


            For Each s As String In searchStringArray
                descriptionString = Regex.Replace(descriptionString, s, "<span class=""highlight"">" & s & "</span>", RegexOptions.IgnoreCase)
                NameString = Regex.Replace(NameString, s, "<span class=""highlight"">" & s & "</span>", RegexOptions.IgnoreCase)

            Next

            literalDescription.Text = descriptionString
            literalName.Text = NameString

            'link the pages properly
            If (e.Item.DataItem("type") = ConfigurationSettings.AppSettings("NewsTypePublication")) Then
                hyperlinkPage.NavigateUrl = "publications.aspx"
            Else
                hyperlinkPage.NavigateUrl = "press.aspx"
            End If

        End If

    End Sub


    Public Sub repeaterMisc_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim literalDescription As Literal = e.Item.FindControl("literalDescription")
            Dim literalName As Literal = e.Item.FindControl("literalCategoryName")
            Dim hyperlinkPage As HyperLink = e.Item.FindControl("hyperlinkPage")
            'trim description

            Dim searchString As String = Request.QueryString("s").Trim.Replace("\", "").Replace("'", "").Replace("""", "").Replace(",", "").Replace(";", "")

            Dim searchStringArray As String() = searchString.Split(" ")

            Dim descriptionString As String = stripHTML(e.Item.DataItem("item_value").trim)
            Dim NameString As String = e.Item.DataItem("categoryname").trim

            Dim firstOccur As Integer = 0

            If (searchStringArray.Length > 0) Then
                firstOccur = descriptionString.IndexOf(searchStringArray(0), StringComparison.CurrentCultureIgnoreCase)
                If (firstOccur < Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2) Then
                    descriptionString = descriptionString.Substring(0, Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) & "..."
                ElseIf (firstOccur + Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2 + searchStringArray(0).Length > descriptionString.Length - 1) Then
                    descriptionString = "..." & descriptionString.Substring(firstOccur - Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2, Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2) & descriptionString.Substring(firstOccur) & "..."
                Else
                    descriptionString = "..." & descriptionString.Substring(firstOccur - Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2, Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2) & descriptionString.Substring(firstOccur, Math.Ceiling(Integer.Parse(ConfigurationSettings.AppSettings("searchSnippetLength"))) / 2 + searchStringArray(0).Length) & "..."
                End If
            End If


            For Each s As String In searchStringArray
                descriptionString = Regex.Replace(descriptionString, s, "<span class=""highlight"">" & s & "</span>", RegexOptions.IgnoreCase)
                NameString = Regex.Replace(NameString, s, "<span class=""highlight"">" & s & "</span>", RegexOptions.IgnoreCase)

            Next

            literalDescription.Text = descriptionString
            literalName.Text = NameString

            'link the pages properly
            If (e.Item.DataItem("categoryname") = ConfigurationSettings.AppSettings("designPhilosophyFolderName")) Then
                hyperlinkPage.NavigateUrl = "designPhilosophy.aspx"
            ElseIf (e.Item.DataItem("categoryname") = ConfigurationSettings.AppSettings("firmProfileFolderName")) Then
                hyperlinkPage.NavigateUrl = "firmProfile.aspx"
            ElseIf (e.Item.DataItem("categoryname") = ConfigurationSettings.AppSettings("locationsFolderName")) Then
                hyperlinkPage.NavigateUrl = "locations.aspx"
            ElseIf (e.Item.DataItem("categoryname") = ConfigurationSettings.AppSettings("careersFolderName") Or e.Item.DataItem("categoryname") = ConfigurationSettings.AppSettings("InternshipFolderName") Or e.Item.DataItem("categoryname") = ConfigurationSettings.AppSettings("promotingLeadershipFolderName")) Then
                hyperlinkPage.NavigateUrl = "careers.aspx"
            ElseIf (e.Item.DataItem("categoryname") = ConfigurationSettings.AppSettings("InternshipsFolderName") Or e.Item.DataItem("categoryname") = ConfigurationSettings.AppSettings("JurorsFolderName") Or e.Item.DataItem("categoryname") = ConfigurationSettings.AppSettings("ShanghaiFolderName")) Then
                hyperlinkPage.NavigateUrl = "internships.aspx"
            ElseIf (e.Item.DataItem("categoryname") = ConfigurationSettings.AppSettings("awardsFolderName")) Then
                hyperlinkPage.NavigateUrl = "awards.aspx"


            End If

        End If



    End Sub


    Function stripHTML(ByVal strHTML As String) As String
        'Strips the HTML tags from strHTML using split and join

        'Ensure that strHTML contains something
        If Len(strHTML) = 0 Then
            stripHTML = strHTML
            Exit Function
        End If

        Dim arysplit, i, j, strOutput

        arysplit = Split(strHTML, "<")

        'Assuming strHTML is nonempty, we want to start iterating
        'from the 2nd array postition
        If Len(arysplit(0)) > 0 Then j = 1 Else j = 0

        'Loop through each instance of the array
        For i = j To UBound(arysplit)
            'Do we find a matching > sign?
            If InStr(arysplit(i), ">") Then
                'If so, snip out all the text between the start of the string
                'and the > sign
                arysplit(i) = Mid(arysplit(i), InStr(arysplit(i), ">") + 1)
            Else
                'Ah, the < was was nonmatching
                arysplit(i) = "<" & arysplit(i)
            End If
        Next

        'Rejoin the array into a single string
        strOutput = Join(arysplit, "")

        'Snip out the first <
        strOutput = Mid(strOutput, 2 - j)

        'Convert < and > to &lt; and &gt;
        strOutput = Replace(strOutput, ">", "&gt;")
        strOutput = Replace(strOutput, "<", "&lt;")

        Return strOutput
    End Function

End Class
