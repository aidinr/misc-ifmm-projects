﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Perkins Eastman: Home Page</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
    <meta name="keywords" content="Perkins Eastman, Architects, Architecture, Planning, Programming, Sustainable design, LEED" />

</head>
<body>
    <form id="form1" runat="server">
    <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    <script type="text/javascript">	
	var c = 0;
	var p = '';
	
	function loadFeaturedProject() {
		if (c == 0) {
			jQuery(".mainLoad:eq(1) > img").load(function() {
				jQuery(".mainLoad:eq(1)").fadeIn("slow", function(){ 
					jQuery(".mainLoad:eq(0)").hide();
					jQuery(".mainLoad:eq(0)").css("z-index", "2");
					jQuery(".mainLoad:eq(1)").css("z-index", "1");
					p = jQuery(".mainLoad:eq(1) > ##projectID").html();
					jQuery(".mainLoad:eq(0)").load("featuredProjectsAJAX.aspx?s=5000&p=" + p, null, function(){ loadFeaturedProject(); });
					c = 1;
				});
				});
		} else {
			jQuery(".mainLoad:eq(0) > img").load(function() {
				jQuery(".mainLoad:eq(0)").fadeIn("slow", function() { 
					jQuery(".mainLoad:eq(1)").hide(); 
					jQuery(".mainLoad:eq(1)").css("z-index", "2");
					jQuery(".mainLoad:eq(0)").css("z-index", "1");
					p = jQuery(".mainLoad:eq(0) > ##projectID").html();
					jQuery(".mainLoad:eq(1)").load("featuredProjectsAJAX.aspx?s=5000&p=" + p, null, function(){ loadFeaturedProject(); });
					c = 0;
				});
				});
		}
	}
	
	function firstLoadFeaturedProject() {
		jQuery(".mainLoad:eq(1)").hide();
		jQuery(".mainLoad:eq(1)").css("z-index", "2");
		jQuery(".mainLoad:eq(0)").css("z-index", "1");
		p = jQuery(".mainLoad:eq(0) > ##projectID").html();
		jQuery(".mainLoad:eq(1)").load("featuredProjectsAJAX.aspx?s=5000&p=" + p, null, function(){ loadFeaturedProject(); });
	}
		
	jQuery(document).ready( function() {
		jQuery(".main").css("height", "560px");
		jQuery.ajaxSetup({cache: false});
		firstLoadFeaturedProject();
	});
</script>

<div class="mainLoad">

<div style="display: none;" id="projectID"><asp:Literal id="literalProjectID" runat="server"></asp:Literal></div>
<asp:Literal id="literalImage" runat="server"></asp:Literal>
<div class="homeCopy">
	    <asp:Literal id="literalURL" runat="server"></asp:Literal>
    <br /><asp:Literal id="literalLocation" runat="server"></asp:Literal>
</div>

</div>
<div class="mainLoad">
</div>
	<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="300" height="75" id="Home_Main" align="middle" class="flash">
		<param name="allowScriptAccess" value="sameDomain" />

        <param name="movie" value="images/Home_Main.swf" />
        <param name="quality" value="high" />
        <param name="wmode" value="transparent" />
        <param name="bgcolor" value="#000000" />
        <embed src="images/Home_Main.swf" quality="high" wmode="transparent" width="300" height="75" name="Home_Main" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
	</object>
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
