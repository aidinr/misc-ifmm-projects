﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="projectDetailsPrint.aspx.vb" Inherits="projectDetailsPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Perkins Eastman</title>
    <style type="text/css">
body {
	margin-bottom: 0px;
	margin-top: 0px;
	margin-left: 0px;
	margin-right: 0px;
}
.global { 	
	position:absolute;
	left: 50%;
	top: 0px;			
	width: 630px;
	margin-left: -315px;
	margin-top: 0px;
	margin-bottom: 0px;
	border: 0px;
}
.header {
	color:#8c8d8e;
	font-family:Arial, Helvetica, sans-serif;
	font-size:10pt;
	font-weight:800;
	letter-spacing:.5px;
	line-height:10pt;
	margin-left:0px;
	margin-top:15px;
	margin-bottom:0px;
	text-align:left;
	text-transform:uppercase;
	
}
.subheader {
	color:#8c8d8e;
	font-family:Arial, Helvetica, sans-serif;
	font-size:8pt;
	letter-spacing:0.5px;
	line-height:11pt;
	margin-left:0px;
	margin-top:0px;
	margin-bottom:15px;
	text-align:left;
	text-transform:uppercase;
	vertical-align:middle;
}
.copy {
	color:#333;
	font-family:Arial, Helvetica, sans-serif;
	font-size:8pt;
	letter-spacing:.2px;
	line-height:10pt;
	margin-bottom:15px;
	margin-left:0px;
	margin-right:30px;
	margin-top: 0px;
	text-align:left;
}
.footer {
	color:#666666;
	font-family:Arial, Helvetica, sans-serif;
	font-size:7pt;
	letter-spacing:1px;
	line-height:8pt;
	margin-left:0px;
	margin-top: 30px;
	text-align:center;
	text-transform:uppercase;
}
.logo {
	margin-top: 15px;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="global">
    <div class="header"><asp:Literal ID="literalName" runat="server"></asp:Literal></div>
            <div class="subheader"><asp:Literal ID="literalCity" runat="server"></asp:Literal><asp:Literal ID="literalStateOrCountry" runat="server"></asp:Literal></div>
            <div class="copy"><asp:Literal ID="literalDescription" runat="server"></asp:Literal></div>
            <table cellpadding="0" cellspacing="0" border="0" width="600px">
            	<tr><td align="center"><asp:Image ID="projectImage" runat="server" /><br /><br /></td></tr>
            	<tr><td align="right" class="logo"><img src="images/project/logo_red_1.gif" /></td></tr>
            </table>                
            <div class="footer">Copyright &copy; 2011 Perkins Eastman. All rights reserved.</div>
    </div>
    </form>
</body>
</html>
