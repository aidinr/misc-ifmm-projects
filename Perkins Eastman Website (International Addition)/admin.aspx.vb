﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Net
Partial Class admin
    Inherits System.Web.UI.Page

    Protected Sub admin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer_admin.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)
    End Sub

    Sub btnSearchString_click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonSubmit.Click
        Dim theLogin As String = textboxUser.Text
        Dim thePassword As String = textboxPassword.Text

        Dim sql As String = "select * from ipm_user where active = 'y' and login = @login and password = @password"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))

        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)
        Dim Login As New SqlParameter("@login", SqlDbType.VarChar, 255)
        Dim PasswordParam As New SqlParameter("@password", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(Login)
        MyCommand1.SelectCommand.Parameters.Add(PasswordParam)
        MyCommand1.SelectCommand.Parameters("@login").Value = theLogin
        MyCommand1.SelectCommand.Parameters("@password").Value = thePassword
        Dim DT1 As New DataTable("logintable")

        MyCommand1.Fill(DT1)


        If (Trim(theLogin <> "")) Then
            If (DT1.Rows.Count > 0) Then
                Session("UserID") = DT1.Rows(0)("userid")
                Session("Login") = 1
                Response.Redirect("Default.aspx")
                Response.End()
            Else
                Response.Redirect("Login.aspx?err=1")
                Response.End()
            End If
        End If

    End Sub
End Class
