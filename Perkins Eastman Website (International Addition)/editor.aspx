﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="editor.aspx.vb" Inherits="editor" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Test editor</title>
    <link rel="stylesheet" type="text/css" href="editor/skins/office2003/css/style.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <ComponentArt:Editor runat="server" ID="Editor1"
    PreserveRelativePaths="true"
    TemplateFile="1ToolBar.ascx"
    SkinFolderLocation="editor/skins/office2003"
    SourceCssClass="SourceCssClass"
    DesignCssClass="DesignCssClass"
    HighlightElementCssClass="HighlightCssClass"
    CssFileURL="editor/skins/office2003/css/iframe.css"
    ContentHTML="Hi!"
    BreadcrumbClientTemplateId="breadcrumbTemplate"
    BreadcrumbCssClass="f-l"
    BreadcrumbSeparator="<div class='sep'></div>"
    Width="578"
    Height="500"
>

    <ClientTemplates>
        <ComponentArt:ClientTemplate Id="breadcrumbTemplate">
            <a onclick="this.blur();return false;" href="javascript:void(0);" class="btn"><span>&lt;##DataItem.breadcrumb##&gt;</span></a>
        </ComponentArt:ClientTemplate>
    </ClientTemplates>

    <ToolBars>
        <ComponentArt:ToolBar
            ID="FormattingToolBar"
            RunAt="server"
            ImagesBaseUrl="editor/skins/office2003/images"
            CssClass="toolbar"
            Width="100"
            Height="25"
            DefaultItemImageWidth="25"
            DefaultItemImageHeight="25"
            UseFadeEffect="false"
            SiteMapXmlFile="editor/features/office2003/MinimalToolBar.xml"
        >

            <ClientEvents>
                <ItemMouseUp EventHandler="font_toolbar_mouseup" />
            </ClientEvents>
        </ComponentArt:ToolBar>
    </ToolBars>
</ComponentArt:Editor>
    </div>
    </form>
</body>
</html>
