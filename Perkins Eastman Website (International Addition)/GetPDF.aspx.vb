﻿
Partial Class GetPDF
    Inherits System.Web.UI.Page

    Protected Sub GetPDF_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim theID As String = Request.QueryString("pdf")
        If (theID <> "") Then
            Response.Redirect(Session("WSRetrieveAsset") & "id=" & theID & "&type=newspdf&size=0&cache=1")
        Else
            Response.Redirect("Default.aspx")
        End If


    End Sub
End Class
