﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="projectDetails.aspx.vb" Inherits="projectDetails" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="literalPageTitle" runat="server"></asp:Literal></title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
</head>
<body>
    <form id="form1" runat="server">
    <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    
    <script type="text/javascript">
        
        function loadImage(i,n) {			
			
	    if (jQuery("#imageLoader").html() != '') {
        jQuery("#imageLoader > img").fadeOut("fast");
        }
			
			jQuery("#ajaxLoader").show();
			
		jQuery.post("projectdetailsImageAJAX.aspx", { filename: i}, function(data) {
        jQuery("#imageLoader").html(data);
        jQuery("#imageLoader > img").load(function() {
        jQuery("#ajaxLoader").hide();
        jQuery("#imageLoader > img").fadeIn("fast");
        jQuery("#printLink").attr("href", 'projectDetailsPrint.aspx?p=<%=request.querystring("p")%>&i=' + n.toString());
        });
        });
        }
		
		jQuery(document).ready(function() {
        loadImage('<asp:literal id="literalFirstImage" runat="server"></asp:literal>',0);
        });
	</script>
	<div class="breadcrumbs"><a href="/">Home</a> | <a href="/projects.aspx?list=2400012">Projects</a> | <asp:Literal ID="literalBrCatName" runat="server"></asp:Literal> | <asp:Literal ID="literalBrProjectName" runat="server"></asp:Literal></div>
    <img id="ajaxLoader" src="images/project/ajax-loader.gif" style="position: absolute; top: 380px; left: 310px; display: none;" />    
	<div class="head_project" style="margin-bottom:0px"><asp:Literal ID="literalName" runat="server"></asp:Literal></div>
    <div class="projectCopy">
    	<table width="600" cellpadding="0" cellspacing="0">
        	<tr>
            	<td align="left"><asp:Literal ID="literalCity" runat="server"></asp:Literal><asp:Literal ID="literalStateOrCountry" runat="server"></asp:Literal></td>
             	<td align="right" valign="middle"><img src="images/project/print.gif" style="height: 10px; width: 10px; border: 0px;" />&nbsp;<a id="printLink" href="projectDetailsPrint.aspx?p=<%=request.querystring("p") %>" target="_blank" style="font-size:7pt;">print page</a></td>
            </tr>
        </table>
    </div>
    <div class="project_details">
        <div id="imageLoader" style="width: 600px; height: 480px; margin-right: 15px; margin-top: 0px; margin-bottom:0px; margin-left:0px; vertical-align:top; float: left;"></div>        
        <asp:Repeater ID="repeaterThumbnail" runat="server">
            <ItemTemplate>
                <img alt="" class="project_thumb_image" onclick="javascript: loadImage('<%#Container.DataItem("asset_id") %>',<%#Container.itemindex %>);" style="margin-bottom: 10px; margin-right: 10px; margin-top: 0px; margin-left: 0px;" id="pImage<%#Container.itemindex %>" src="<%=Session("WSRetrieveAsset")%>crop=1&type=asset&size=1&width=60&height=60&id=<%#container.dataitem("asset_id")%>" />    
            </ItemTemplate>
        </asp:Repeater>
        	<p class="project_description" style="position: absolute; bottom: 85px; width: 250px; left: 632px; height: 258px;"><asp:Literal ID="literalDescription" runat="server"></asp:Literal></p>
    </div>
    <div class="projectCaption">
    	<table width="600" cellpadding="0" cellspacing="0">
        	<tr>
            	<td align="left" valign="middle" width="34%"><img src="images/project/back.gif" style="height: 10px; border: 0px;" alt="" />&nbsp;back to <asp:HyperLink ID="linkBackToCategory" runat="server"></asp:HyperLink></td>
                <td align="right" valign="middle" width="33%"><img src="images/project/back.gif" style="height: 10px; border: 0px;" />&nbsp;<asp:HyperLink ID="linkPrevProject" runat="server"></asp:HyperLink></td>
             	<td align="right" valign="middle" width="33%"><asp:HyperLink ID="linkNextProject" runat="server"></asp:HyperLink>&nbsp;<img src="images/project/forward.gif" style="height: 10px; border: 0px;" /></td>
            </tr>
        </table>
    </div>
    
    
    
     <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
