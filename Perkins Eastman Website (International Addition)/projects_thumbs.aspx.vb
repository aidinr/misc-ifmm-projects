﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class projects_thumbs
    Inherits System.Web.UI.Page
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)

        DisplayProjects()


    End Sub

    Sub DisplayProjects()

        Dim theProjectID = Request.QueryString("list")

        Dim sql As String = "select *, b.item_value description_long, c.item_value client_list from IPM_PROJECT a left join ipm_project_field_value b on a.projectid = b.projectid and b.item_id = @catDescriptionUDFID left join ipm_project_field_value c on a.projectid = c.projectid and c.item_id = @clientListUDFID where a.projectid = @project_id and a.AVAILABLE = 'y'"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim catIDParameter As New SqlParameter("@catDescriptionUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(catIDParameter)
        MyCommand1.SelectCommand.Parameters("@catDescriptionUDFID").Value = ConfigurationSettings.AppSettings("DescriptionLongUDFID")

        Dim projectIDParameter As New SqlParameter("@project_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(projectIDParameter)
        MyCommand1.SelectCommand.Parameters("@project_id").Value = theProjectID

        Dim clientListIDParameter As New SqlParameter("@clientListUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(clientListIDParameter)
        MyCommand1.SelectCommand.Parameters("@clientListUDFID").Value = ConfigurationSettings.AppSettings("ClientListUDFID")

        Dim dt1 As DataTable = New DataTable
        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then
            If (Not IsDBNull(dt1.Rows(0)("description_long"))) Then
                literalDescription.Text = dt1.Rows(0)("description_long")
                literalDescription2.Text = dt1.Rows(0)("description_long")
            End If
            If (Not IsDBNull(dt1.Rows(0)("client_list"))) Then
                literalBrowseClient.Text = "<p id=""ClientList"">Browse our client list</p>"
                literalClientList.Text = dt1.Rows(0)("client_list").ToString.Replace(vbCrLf, "").Replace("'", "\'")
            End If
            'literalName.Text = dt1.Rows(0)("name")
            literalPageTitle.Text = "Perkins Eastman: " & dt1.Rows(0)("name")
        End If

        MyCommand1.Fill(dt1)



        'select * from ipm_project_related b, ipm_project a left join ipm_project_field_value c on c.projectid = a.projectid and c.item_id = 2400053 where b.project_id = 2400103 and a.projectid = b.ref_id and available = 'Y' and b.ref_id in (select ProjectID from IPM_PROJECT_OFFICE where officeid in (1,4,7,8,11)) order by ordering asc

        Dim sql2 As String = "select * from ipm_project_related b, ipm_project a left join ipm_project_field_value c on c.projectid = a.projectid and c.item_id = @shortNameUDFID where b.project_id = @project_id and a.projectid = b.ref_id and available = 'Y' and b.ref_id in (select ProjectID from IPM_PROJECT_OFFICE where officeid in (" & Request.QueryString("c_id") & ")) order by ordering asc"
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)

        Dim projectIDParameter2 As New SqlParameter("@project_id", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(projectIDParameter2)
        MyCommand2.SelectCommand.Parameters("@project_id").Value = theProjectID

        Dim shortNameIDParameter As New SqlParameter("@shortNameUDFID", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(shortNameIDParameter)
        MyCommand2.SelectCommand.Parameters("@shortNameUDFID").Value = ConfigurationSettings.AppSettings("ShortNameUDFID")

        '' ''Dim projectIDParameter3 As New SqlParameter("@c_id", SqlDbType.VarChar, 255)
        '' ''MyCommand2.SelectCommand.Parameters.Add(projectIDParameter3)
        '' ''MyCommand2.SelectCommand.Parameters("@c_id").Value = Request.QueryString("c_id")

        Dim dt2 As DataTable = New DataTable



        MyCommand2.Fill(dt2)

        repeaterProjects.DataSource = dt2
        repeaterProjects.DataBind()

        Dim sql3 As String = "select * from ipm_asset a, ipm_project b where a.available = 'y' and b.available = 'y' and a.projectid = b.projectid and b.projectid = @project_id and a.media_type = 10"
        Dim MyCommand3 As SqlDataAdapter = New SqlDataAdapter(sql3, MyConnection)

        Dim projectIDParameter4 As New SqlParameter("@project_id", SqlDbType.VarChar, 255)
        MyCommand3.SelectCommand.Parameters.Add(projectIDParameter4)
        MyCommand3.SelectCommand.Parameters("@project_id").Value = theProjectID

        Dim dt3 As DataTable = New DataTable

        MyCommand3.Fill(dt3)


        If (dt3.Rows.Count > 0) Then

            'featuredImage.ImageUrl = Session("WSRetrieveAsset") & "type=asset&size=1&width=600&height=480&id=" & dt3.Rows(0)("asset_id")


        End If

    End Sub

End Class