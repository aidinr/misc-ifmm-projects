﻿<%@ page language="VB" autoeventwireup="false" CodeFile="client.aspx.vb" inherits="client" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Perkins Eastman: Client Log In</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
</head>
<body>
    <form id="form1" runat="server">
      <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    
    
   
<script type="text/javascript">
	jQuery(".main").css("background-color", "#FFF");
</script>
<!--<cfoutput>-->
 <div class="breadcrumbs"><a href="/">Home</a> | <a href="/client.aspx">Log in</a> | CLIENT</div>
<div class="head_standard">Client Log-In</div>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td align="left" valign="top">
        <div class="copy_standard" style="width: 600px; padding-right: 30px;"><a href="ftp://ftp.perkinseastman.com/" target="_blank">FTP</a><br /><br />Need help? Contact our help <a href="mailto:itsupport@perkinseastman.com">desk</a>.</div>
        
</td>

</tr>
</table>
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>