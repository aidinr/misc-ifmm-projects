﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient

Partial Class leadership
    Inherits System.Web.UI.Page
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)

        GeneratePEDirectors()
        'GeneratePEPrincipals()
        'GeneratePEAssociatePrincipals()
        'GeneratePEBDirectors()
        'GeneratePEBPrincipals()
        'GenerateBFJPrincipals()
        'GenerateRGRPrincipals()
        'GenerateUrbanomicsPrincipals()
        'GenerateRusselPrincipals()


    End Sub

    Sub GeneratePEDirectors()

        Dim sql = "select a.userid userid, FirstName,LastName, Bio, c.Item_Value qualifications from IPM_USER a left join ipm_user_field_value b on a.userid = b.user_id and b.item_id = @publish_item_id left join ipm_user_field_value c on a.userid = c.user_id and c.item_id = @qualifications_item_id  left join ipm_user_field_value d on a.userid = d.user_id and d.item_id = @sort_item_id where Active = 'y' and agency = @company and position = @title order by d.item_value"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim qualificationsIDParameter As New SqlParameter("@qualifications_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(qualificationsIDParameter)
        MyCommand1.SelectCommand.Parameters("@qualifications_item_id").Value = ConfigurationSettings.AppSettings("qualificationsUDFID")


        Dim publishIDParameter As New SqlParameter("@publish_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(publishIDParameter)
        MyCommand1.SelectCommand.Parameters("@publish_item_id").Value = ConfigurationSettings.AppSettings("publishUDFID")

        Dim sortIDParameter As New SqlParameter("@sort_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(sortIDParameter)
        MyCommand1.SelectCommand.Parameters("@sort_item_id").Value = ConfigurationSettings.AppSettings("sortUDFID")

        Dim companyParameter As New SqlParameter("@company", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(companyParameter)
        MyCommand1.SelectCommand.Parameters("@company").Value = ConfigurationSettings.AppSettings("PECompanyName")

        Dim titleParameter As New SqlParameter("@title", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(titleParameter)
        MyCommand1.SelectCommand.Parameters("@title").Value = ConfigurationSettings.AppSettings("PEExecutiveTitle")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        RepeaterExecutives.DataSource = dt1
        RepeaterExecutives.DataBind()



      

    End Sub


    Sub GeneratePEPrincipals()

        Dim sql = "select a.userid userid, FirstName,LastName, Bio, c.Item_Value qualifications from IPM_USER a left join ipm_user_field_value b on a.userid = b.user_id and b.item_id = @publish_item_id left join ipm_user_field_value c on a.userid = c.user_id and c.item_id = @qualifications_item_id  left join ipm_user_field_value d on a.userid = d.user_id and d.item_id = @sort_item_id where Active = 'y' and agency = @company and position = @title order by d.item_value"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim qualificationsIDParameter As New SqlParameter("@qualifications_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(qualificationsIDParameter)
        MyCommand1.SelectCommand.Parameters("@qualifications_item_id").Value = ConfigurationSettings.AppSettings("qualificationsUDFID")


        Dim publishIDParameter As New SqlParameter("@publish_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(publishIDParameter)
        MyCommand1.SelectCommand.Parameters("@publish_item_id").Value = ConfigurationSettings.AppSettings("publishUDFID")

        Dim sortIDParameter As New SqlParameter("@sort_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(sortIDParameter)
        MyCommand1.SelectCommand.Parameters("@sort_item_id").Value = ConfigurationSettings.AppSettings("sortUDFID")

        Dim companyParameter As New SqlParameter("@company", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(companyParameter)
        MyCommand1.SelectCommand.Parameters("@company").Value = ConfigurationSettings.AppSettings("PECompanyName")

        Dim titleParameter As New SqlParameter("@title", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(titleParameter)
        MyCommand1.SelectCommand.Parameters("@title").Value = ConfigurationSettings.AppSettings("PEPrincipalTitle")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        RepeaterPrincipals.DataSource = dt1
        RepeaterPrincipals.DataBind()

    End Sub

    Sub GeneratePEAssociatePrincipals()

        Dim sql = "select a.userid userid, FirstName,LastName, Bio, c.Item_Value qualifications from IPM_USER a left join ipm_user_field_value b on a.userid = b.user_id and b.item_id = @publish_item_id left join ipm_user_field_value c on a.userid = c.user_id and c.item_id = @qualifications_item_id  left join ipm_user_field_value d on a.userid = d.user_id and d.item_id = @sort_item_id where Active = 'y' and agency = @company and position = @title order by d.item_value"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim qualificationsIDParameter As New SqlParameter("@qualifications_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(qualificationsIDParameter)
        MyCommand1.SelectCommand.Parameters("@qualifications_item_id").Value = ConfigurationSettings.AppSettings("qualificationsUDFID")


        Dim publishIDParameter As New SqlParameter("@publish_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(publishIDParameter)
        MyCommand1.SelectCommand.Parameters("@publish_item_id").Value = ConfigurationSettings.AppSettings("publishUDFID")

        Dim sortIDParameter As New SqlParameter("@sort_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(sortIDParameter)
        MyCommand1.SelectCommand.Parameters("@sort_item_id").Value = ConfigurationSettings.AppSettings("sortUDFID")

        Dim companyParameter As New SqlParameter("@company", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(companyParameter)
        MyCommand1.SelectCommand.Parameters("@company").Value = ConfigurationSettings.AppSettings("PECompanyName")

        Dim titleParameter As New SqlParameter("@title", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(titleParameter)
        MyCommand1.SelectCommand.Parameters("@title").Value = ConfigurationSettings.AppSettings("PEAssociatePrincipalTitle")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        RepeaterAssociatePrincipals.DataSource = dt1
        RepeaterAssociatePrincipals.DataBind()

    End Sub

    Sub GeneratePEBDirectors()

        Dim sql = "select a.userid userid, FirstName,LastName, Bio, c.Item_Value qualifications from IPM_USER a left join ipm_user_field_value b on a.userid = b.user_id and b.item_id = @publish_item_id left join ipm_user_field_value c on a.userid = c.user_id and c.item_id = @qualifications_item_id  left join ipm_user_field_value d on a.userid = d.user_id and d.item_id = @sort_item_id where Active = 'y' and agency = @company and position = @title order by d.item_value"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim qualificationsIDParameter As New SqlParameter("@qualifications_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(qualificationsIDParameter)
        MyCommand1.SelectCommand.Parameters("@qualifications_item_id").Value = ConfigurationSettings.AppSettings("qualificationsUDFID")


        Dim publishIDParameter As New SqlParameter("@publish_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(publishIDParameter)
        MyCommand1.SelectCommand.Parameters("@publish_item_id").Value = ConfigurationSettings.AppSettings("publishUDFID")

        Dim sortIDParameter As New SqlParameter("@sort_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(sortIDParameter)
        MyCommand1.SelectCommand.Parameters("@sort_item_id").Value = ConfigurationSettings.AppSettings("sortUDFID")

        Dim companyParameter As New SqlParameter("@company", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(companyParameter)
        MyCommand1.SelectCommand.Parameters("@company").Value = ConfigurationSettings.AppSettings("PEBlackCompanyName")

        Dim titleParameter As New SqlParameter("@title", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(titleParameter)
        MyCommand1.SelectCommand.Parameters("@title").Value = ConfigurationSettings.AppSettings("PEBlackDirectorTitle")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        RepeaterPerkinsEastmanBlackDirectors.DataSource = dt1
        RepeaterPerkinsEastmanBlackDirectors.DataBind()


    End Sub

    Sub GeneratePEBPrincipals()


        Dim sql = "select a.userid userid, FirstName,LastName, Bio, c.Item_Value qualifications from IPM_USER a left join ipm_user_field_value b on a.userid = b.user_id and b.item_id = @publish_item_id left join ipm_user_field_value c on a.userid = c.user_id and c.item_id = @qualifications_item_id  left join ipm_user_field_value d on a.userid = d.user_id and d.item_id = @sort_item_id where Active = 'y' and agency = @company and position = @title order by d.item_value"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim qualificationsIDParameter As New SqlParameter("@qualifications_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(qualificationsIDParameter)
        MyCommand1.SelectCommand.Parameters("@qualifications_item_id").Value = ConfigurationSettings.AppSettings("qualificationsUDFID")


        Dim publishIDParameter As New SqlParameter("@publish_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(publishIDParameter)
        MyCommand1.SelectCommand.Parameters("@publish_item_id").Value = ConfigurationSettings.AppSettings("publishUDFID")

        Dim sortIDParameter As New SqlParameter("@sort_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(sortIDParameter)
        MyCommand1.SelectCommand.Parameters("@sort_item_id").Value = ConfigurationSettings.AppSettings("sortUDFID")

        Dim companyParameter As New SqlParameter("@company", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(companyParameter)
        MyCommand1.SelectCommand.Parameters("@company").Value = ConfigurationSettings.AppSettings("PEBlackCompanyName")

        Dim titleParameter As New SqlParameter("@title", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(titleParameter)
        MyCommand1.SelectCommand.Parameters("@title").Value = ConfigurationSettings.AppSettings("PEBlackPrincipalTitle")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        RepeaterPerkinsEastmanBlackPrincipals.DataSource = dt1
        RepeaterPerkinsEastmanBlackPrincipals.DataBind()

    End Sub

    Sub GenerateBFJPrincipals()

        Dim sql = "select a.userid userid, FirstName,LastName, Bio, c.Item_Value qualifications from IPM_USER a left join ipm_user_field_value b on a.userid = b.user_id and b.item_id = @publish_item_id left join ipm_user_field_value c on a.userid = c.user_id and c.item_id = @qualifications_item_id  left join ipm_user_field_value d on a.userid = d.user_id and d.item_id = @sort_item_id where Active = 'y' and agency = @company and position = @title order by d.item_value"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim qualificationsIDParameter As New SqlParameter("@qualifications_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(qualificationsIDParameter)
        MyCommand1.SelectCommand.Parameters("@qualifications_item_id").Value = ConfigurationSettings.AppSettings("qualificationsUDFID")


        Dim publishIDParameter As New SqlParameter("@publish_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(publishIDParameter)
        MyCommand1.SelectCommand.Parameters("@publish_item_id").Value = ConfigurationSettings.AppSettings("publishUDFID")

        Dim sortIDParameter As New SqlParameter("@sort_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(sortIDParameter)
        MyCommand1.SelectCommand.Parameters("@sort_item_id").Value = ConfigurationSettings.AppSettings("sortUDFID")

        Dim companyParameter As New SqlParameter("@company", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(companyParameter)
        MyCommand1.SelectCommand.Parameters("@company").Value = ConfigurationSettings.AppSettings("BFJCompanyName")

        Dim titleParameter As New SqlParameter("@title", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(titleParameter)
        MyCommand1.SelectCommand.Parameters("@title").Value = ConfigurationSettings.AppSettings("BFJPrincipalTitle")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        RepeaterPrincipalsBJFPLanning.DataSource = dt1
        RepeaterPrincipalsBJFPLanning.DataBind()

    End Sub

    Sub GenerateRGRPrincipals()
        Dim sql = "select a.userid userid, FirstName,LastName, Bio, c.Item_Value qualifications from IPM_USER a left join ipm_user_field_value b on a.userid = b.user_id and b.item_id = @publish_item_id left join ipm_user_field_value c on a.userid = c.user_id and c.item_id = @qualifications_item_id  left join ipm_user_field_value d on a.userid = d.user_id and d.item_id = @sort_item_id where Active = 'y' and agency = @company and position = @title order by d.item_value"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim qualificationsIDParameter As New SqlParameter("@qualifications_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(qualificationsIDParameter)
        MyCommand1.SelectCommand.Parameters("@qualifications_item_id").Value = ConfigurationSettings.AppSettings("qualificationsUDFID")


        Dim publishIDParameter As New SqlParameter("@publish_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(publishIDParameter)
        MyCommand1.SelectCommand.Parameters("@publish_item_id").Value = ConfigurationSettings.AppSettings("publishUDFID")

        Dim sortIDParameter As New SqlParameter("@sort_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(sortIDParameter)
        MyCommand1.SelectCommand.Parameters("@sort_item_id").Value = ConfigurationSettings.AppSettings("sortUDFID")

        Dim companyParameter As New SqlParameter("@company", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(companyParameter)
        MyCommand1.SelectCommand.Parameters("@company").Value = ConfigurationSettings.AppSettings("RGRCompanyName")

        Dim titleParameter As New SqlParameter("@title", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(titleParameter)
        MyCommand1.SelectCommand.Parameters("@title").Value = ConfigurationSettings.AppSettings("RGRPrincipalTitle")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        RepeaterRGRLandscape.DataSource = dt1
        RepeaterRGRLandscape.DataBind()

    End Sub

    Sub GenerateUrbanomicsPrincipals()
        Dim sql = "select a.userid userid, FirstName,LastName, Bio, c.Item_Value qualifications from IPM_USER a left join ipm_user_field_value b on a.userid = b.user_id and b.item_id = @publish_item_id left join ipm_user_field_value c on a.userid = c.user_id and c.item_id = @qualifications_item_id  left join ipm_user_field_value d on a.userid = d.user_id and d.item_id = @sort_item_id where Active = 'y' and agency = @company and position = @title order by d.item_value"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim qualificationsIDParameter As New SqlParameter("@qualifications_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(qualificationsIDParameter)
        MyCommand1.SelectCommand.Parameters("@qualifications_item_id").Value = ConfigurationSettings.AppSettings("qualificationsUDFID")


        Dim publishIDParameter As New SqlParameter("@publish_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(publishIDParameter)
        MyCommand1.SelectCommand.Parameters("@publish_item_id").Value = ConfigurationSettings.AppSettings("publishUDFID")

        Dim sortIDParameter As New SqlParameter("@sort_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(sortIDParameter)
        MyCommand1.SelectCommand.Parameters("@sort_item_id").Value = ConfigurationSettings.AppSettings("sortUDFID")

        Dim companyParameter As New SqlParameter("@company", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(companyParameter)
        MyCommand1.SelectCommand.Parameters("@company").Value = ConfigurationSettings.AppSettings("UrbanomicsCompanyName")

        Dim titleParameter As New SqlParameter("@title", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(titleParameter)
        MyCommand1.SelectCommand.Parameters("@title").Value = ConfigurationSettings.AppSettings("UrbanomicsPrincipalTitle")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        RepeaterPrincipalsUrbanomics.DataSource = dt1
        RepeaterPrincipalsUrbanomics.DataBind()

    End Sub

    Sub GenerateRusselPrincipals()
        Dim sql = "select a.userid userid, FirstName,LastName, Bio, c.Item_Value qualifications from IPM_USER a left join ipm_user_field_value b on a.userid = b.user_id and b.item_id = @publish_item_id left join ipm_user_field_value c on a.userid = c.user_id and c.item_id = @qualifications_item_id  left join ipm_user_field_value d on a.userid = d.user_id and d.item_id = @sort_item_id where Active = 'y' and agency = @company and position = @title order by d.item_value"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim qualificationsIDParameter As New SqlParameter("@qualifications_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(qualificationsIDParameter)
        MyCommand1.SelectCommand.Parameters("@qualifications_item_id").Value = ConfigurationSettings.AppSettings("qualificationsUDFID")


        Dim publishIDParameter As New SqlParameter("@publish_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(publishIDParameter)
        MyCommand1.SelectCommand.Parameters("@publish_item_id").Value = ConfigurationSettings.AppSettings("publishUDFID")

        Dim sortIDParameter As New SqlParameter("@sort_item_id", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(sortIDParameter)
        MyCommand1.SelectCommand.Parameters("@sort_item_id").Value = ConfigurationSettings.AppSettings("sortUDFID")

        Dim companyParameter As New SqlParameter("@company", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(companyParameter)
        MyCommand1.SelectCommand.Parameters("@company").Value = ConfigurationSettings.AppSettings("RusselCompanyName")

        Dim titleParameter As New SqlParameter("@title", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(titleParameter)
        MyCommand1.SelectCommand.Parameters("@title").Value = ConfigurationSettings.AppSettings("RusselPrincipalTitle")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        RepeaterPrincipalsRusselDesign.DataSource = dt1
        RepeaterPrincipalsRusselDesign.DataBind()

    End Sub
    Public Sub RepeaterExecutives_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        Dim lineBreakLiteral As Literal = e.Item.FindControl("RepeaterLineBreak")

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            If (e.Item.ItemIndex Mod 3 = 2) Then


                lineBreakLiteral.Text = "</tr></tr>"
            End If
        End If

    End Sub

    Public Sub RepeaterPrincipals_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        Dim lineBreakLiteral As Literal = e.Item.FindControl("RepeaterLineBreak")

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            If (e.Item.ItemIndex Mod 3 = 2) Then


                lineBreakLiteral.Text = "</tr></tr>"
            End If
        End If

    End Sub

    Public Sub RepeaterAssociatePrincipals_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        Dim lineBreakLiteral As Literal = e.Item.FindControl("RepeaterLineBreak")

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            If (e.Item.ItemIndex Mod 3 = 2) Then


                lineBreakLiteral.Text = "</tr></tr>"
            End If
        End If

    End Sub

    Public Sub RepeaterPerkinsEastmanBlackDirectors_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        Dim lineBreakLiteral As Literal = e.Item.FindControl("RepeaterLineBreak")

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            If (e.Item.ItemIndex Mod 3 = 2) Then


                lineBreakLiteral.Text = "</tr></tr>"
            End If
        End If

    End Sub

    Public Sub RepeaterPerkinsEastmanBlackPrincipals_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        Dim lineBreakLiteral As Literal = e.Item.FindControl("RepeaterLineBreak")

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            If (e.Item.ItemIndex Mod 3 = 2) Then


                lineBreakLiteral.Text = "</tr></tr>"
            End If
        End If

    End Sub

    Public Sub RepeaterRGRLandscape_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        Dim theLink As HyperLink = e.Item.FindControl("companyURL")

        If (e.Item.ItemType = ListItemType.Header) Then

            theLink.NavigateUrl = ConfigurationSettings.AppSettings("RGRWebsite")
            theLink.Text = ConfigurationSettings.AppSettings("RGRCompanyName")
            theLink.Target = "_blank"

        End If

    End Sub

    Public Sub RepeaterPrincipalsUrbanomics_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        Dim theLink As HyperLink = e.Item.FindControl("companyURL")

        If (e.Item.ItemType = ListItemType.Header) Then

            theLink.NavigateUrl = ConfigurationSettings.AppSettings("UrbanomicsWebsite")
            theLink.Text = ConfigurationSettings.AppSettings("UrbanomicsCompanyName")
            theLink.Target = "_blank"

        End If

    End Sub

    Public Sub RepeaterPrincpalsRusselDesign_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        Dim theLink As HyperLink = e.Item.FindControl("companyURL")

        If (e.Item.ItemType = ListItemType.Header) Then

            theLink.NavigateUrl = ConfigurationSettings.AppSettings("RusselWebsite")
            theLink.Text = ConfigurationSettings.AppSettings("RusselCompanyName")
            theLink.Target = "_blank"

        End If

    End Sub

    Public Sub RepeaterPrincipalsBJFPLanning_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        Dim theLink As HyperLink = e.Item.FindControl("companyURL")

        If (e.Item.ItemType = ListItemType.Header) Then

            theLink.NavigateUrl = ConfigurationSettings.AppSettings("BFJWebsite")
            theLink.Text = ConfigurationSettings.AppSettings("BFJCompanyName")
            theLink.Target = "_blank"

        End If

    End Sub


End Class



