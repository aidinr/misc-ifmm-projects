<%@ Control Language="C#" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<script runat="server">
	protected void Page_Load(object sender, EventArgs e)
	{
		ComboBoxFontSize.DropImageUrl = PrefixWithSkinFolderLocation(ComboBoxFontSize.DropImageUrl);
		ComboBoxFontSize.DropHoverImageUrl = PrefixWithSkinFolderLocation(ComboBoxFontSize.DropHoverImageUrl);
	}
	private string PrefixWithSkinFolderLocation(string str)
	{
		string prefix = this.Attributes["SkinFolderLocation"];
		return str.IndexOf(prefix) == 0 ? str : prefix + "/" + str;
	}
</script>

<ComponentArt:ComboBox
	ID="ComboBoxFontSize"
	RunAt="server"
	Width="38"
	Height="18"
	ItemCssClass="itm"
	ItemHoverCssClass="itm-h"
	CssClass="cmb"
	HoverCssClass="cmb-h"
	FocusedCssClass="cmb-h"
	TextBoxCssClass="txt"
	DropImageUrl="images/combobox/ddn.png"
	DropHoverImageUrl="images/combobox/ddn-h.png"
	KeyboardEnabled="false"
	TextBoxEnabled="false"
	DropDownResizingMode="bottom"
	DropDownWidth="64"
	DropDownHeight="160"
	DropDownCssClass="ddn"
	DropDownContentCssClass="ddn-size"
	SelectedIndex="0"
>
	<DropDownFooter><div class="ddn-ftr"></div></DropDownFooter>

	<Items>
		<ComponentArt:ComboBoxItem Text="8" Value="8" />
		<ComponentArt:ComboBoxItem Text="9" Value="9" />
		<ComponentArt:ComboBoxItem Text="10" Value="10" />
		<ComponentArt:ComboBoxItem Text="11" Value="11" />
		<ComponentArt:ComboBoxItem Text="12" Value="12" />
		<ComponentArt:ComboBoxItem Text="14" Value="14" />
		<ComponentArt:ComboBoxItem Text="16" Value="16" />
		<ComponentArt:ComboBoxItem Text="18" Value="18" />
		<ComponentArt:ComboBoxItem Text="20" Value="20" />
		<ComponentArt:ComboBoxItem Text="22" Value="22" />
		<ComponentArt:ComboBoxItem Text="24" Value="24" />
		<ComponentArt:ComboBoxItem Text="26" Value="26" />
		<ComponentArt:ComboBoxItem Text="28" Value="28" />
		<ComponentArt:ComboBoxItem Text="36" Value="36" />
		<ComponentArt:ComboBoxItem Text="48" Value="48" />
		<ComponentArt:ComboBoxItem Text="72" Value="72" />
	</Items>
</ComponentArt:ComboBox>