<%@ Control Language="C#" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<script runat="server">
	protected void Page_Load(object sender, EventArgs e)
	{
		ComboBoxFontFace.DropImageUrl = PrefixWithSkinFolderLocation(ComboBoxFontFace.DropImageUrl);
		ComboBoxFontFace.DropHoverImageUrl = PrefixWithSkinFolderLocation(ComboBoxFontFace.DropHoverImageUrl);
	}
	private string PrefixWithSkinFolderLocation(string str)
	{
		string prefix = this.Attributes["SkinFolderLocation"];
		return str.IndexOf(prefix) == 0 ? str : prefix + "/" + str;
	}
</script>

<ComponentArt:ComboBox
	ID="ComboBoxFontFace"
	RunAt="server"
	Width="100"
	Height="18"
	ItemCssClass="itm"
	ItemHoverCssClass="itm-h"
	CssClass="cmb"
	HoverCssClass="cmb-h"
	FocusedCssClass="cmb-h"
	TextBoxCssClass="txt"
	DropImageUrl="images/combobox/ddn.png"
	DropHoverImageUrl="images/combobox/ddn-h.png"
	KeyboardEnabled="false"
	TextBoxEnabled="false"
	DropDownResizingMode="bottom"
	DropDownWidth="190"
	DropDownHeight="220"
	DropDownCssClass="ddn"
	DropDownContentCssClass="ddn-face"
	SelectedIndex="6"
>
	<DropDownFooter><div class="ddn-ftr"></div></DropDownFooter>

	<Items>
		<ComponentArt:ComboBoxItem Text="Arial" Value="arial" ClientTemplateId="FontFaceItemTemplate" />
		<ComponentArt:ComboBoxItem Text="Arial Black" Value="'arial black'" ClientTemplateId="FontFaceItemTemplate" />
		<ComponentArt:ComboBoxItem Text="Comic Sans" Value="'comic sans ms'" ClientTemplateId="FontFaceItemTemplate" />
		<ComponentArt:ComboBoxItem Text="Courier New" Value="courier new" ClientTemplateId="FontFaceItemTemplate" />
		<ComponentArt:ComboBoxItem Text="Garamond" Value="garamond" ClientTemplateId="FontFaceItemTemplate" />
		<ComponentArt:ComboBoxItem Text="Georgia" Value="georgia" ClientTemplateId="FontFaceItemTemplate" />
		<ComponentArt:ComboBoxItem Text="Tahoma" Value="tahoma" ClientTemplateId="FontFaceItemTemplate" />
		<ComponentArt:ComboBoxItem Text="Times" Value="'times new roman'" ClientTemplateId="FontFaceItemTemplate" />
		<ComponentArt:ComboBoxItem Text="Trebuchet" Value="'trebuchet ms'" ClientTemplateId="FontFaceItemTemplate" />
		<ComponentArt:ComboBoxItem Text="Verdana" Value="verdana" ClientTemplateId="FontFaceItemTemplate" />
	</Items>

	<ClientTemplates>
		<ComponentArt:ClientTemplate ID="FontFaceItemTemplate">
			<div style="font-family:## DataItem.get_value() ##;" class="ttf">## DataItem.get_text() ##</div>
		</ComponentArt:ClientTemplate>
	</ClientTemplates>
</ComponentArt:ComboBox>
