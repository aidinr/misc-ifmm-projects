<%@ Control Language="C#" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<ComponentArt:Menu
	ID="RedoMenu"
	RunAt="server"
	ContextMenu="custom"
	Orientation="Vertical"
	CssClass="mnu-undo"
	ExpandDuration="0"
	CollapseDuration="0"
	TopGroupExpandDirection="BelowLeft"
	TopGroupExpandOffsetX="1"
	TopGroupExpandOffsetY="-3"
	ShadowEnabled="false"
>
	<Items>
		<ComponentArt:MenuItem ClientTemplateId="RedoTemplate" />
	</Items>
	<ClientTemplates>
		<ComponentArt:ClientTemplate ID="RedoTemplate">
			<span>Redo</span>
			<div class="list">## RedoMenuHtml(Parent); ##</div>
		</ComponentArt:ClientTemplate>
	</ClientTemplates>
</ComponentArt:Menu>

