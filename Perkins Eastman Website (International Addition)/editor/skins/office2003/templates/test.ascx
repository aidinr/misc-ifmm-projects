<%@ Control Language="C#" ClassName="EditorTemplate" %>


<table width="100%" height="100%" border="0">
	<tr>
		<td height="50">
			<table cellspacing="0" cellpadding="0" width="100%" height="100%" border="0">
				<tr>
					<td>$$ToolBars[0]$$</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td width="100%">
			<table width="100%" height="100%" style="border-spacing:0;padding:0;margin:0;border:none;" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td style="width:40px;">aaa</td>
					<td width="100%" height="100%">$$editorarea$$</td>
					<td style="width:40px;">bbb</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td height="50">
			<table cellspacing="0" cellpadding="0" width="100%" height="100%" border="0">
				<tr>
					<td>
						<a onclick="$$parent$$.DesignMode();select_statusbar_button(this);this.blur();return false;" href="javascript:void(0);" class="button-selected"><span>Design</span></a>&nbsp;|&nbsp;
						<a onclick="$$parent$$.SourceMode();select_statusbar_button(this);this.blur();return false;" href="javascript:void(0);" class="button"><span>HTML</span></a>&nbsp;|&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>

</table>