<%@ Control Language="C#" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<script runat="server">
  protected void Page_Load(object sender, EventArgs e)
  {
    ComboBoxFontSize.DropImageUrl = PrefixWithSkinFolderLocation(ComboBoxFontSize.DropImageUrl);
    ComboBoxFontSize.DropHoverImageUrl = PrefixWithSkinFolderLocation(ComboBoxFontSize.DropHoverImageUrl);
  }
  private string PrefixWithSkinFolderLocation(string str)
  {
    string prefix = this.Attributes["SkinFolderLocation"];
    return str.IndexOf(prefix) == 0 ? str : prefix + "/" + str;
  }
</script>

<ComponentArt:ComboBox
	ID="ComboBoxFontSize"
	RunAt="server"
	Width="33"
	Height="22"
	ItemCssClass="mnu-item"
	ItemHoverCssClass="mnu-item-h"
	CssClass="cmb"
	HoverCssClass="cmb-h"
	FocusedCssClass="cmb-h"
	TextBoxCssClass="cmb-txt"
	TextBoxHoverCssClass="cmb-txt"
	DropImageUrl="images/editor/ddn.png"
	DropHoverImageUrl="images/editor/ddn-h.png"
	KeyboardEnabled="false"
	TextBoxEnabled="false"
	DropDownResizingMode="bottom"
	DropDownWidth="64"
	DropDownHeight="160"
	DropDownCssClass="mnu mnu-size"
	DropDownContentCssClass="mnu-con"
	SelectedIndex="4"
>

	<DropDownHeader>
		<div class="mnu-hdr"></div>
	</DropDownHeader>

	<DropDownFooter>
		<div class="mnu-ftr"></div>
	</DropDownFooter>

	<Items>
		<ComponentArt:ComboBoxItem Text="8" Value="8px" />
		<ComponentArt:ComboBoxItem Text="9" Value="9px" />
		<ComponentArt:ComboBoxItem Text="10" Value="10px" />
		<ComponentArt:ComboBoxItem Text="11" Value="11px" />
		<ComponentArt:ComboBoxItem Text="12" Value="12px" />
		<ComponentArt:ComboBoxItem Text="14" Value="14px" />
		<ComponentArt:ComboBoxItem Text="16" Value="16px" />
		<ComponentArt:ComboBoxItem Text="18" Value="18px" />
		<ComponentArt:ComboBoxItem Text="20" Value="20px" />
		<ComponentArt:ComboBoxItem Text="22" Value="22px" />
		<ComponentArt:ComboBoxItem Text="24" Value="24px" />
		<ComponentArt:ComboBoxItem Text="26" Value="26px" />
		<ComponentArt:ComboBoxItem Text="28" Value="28px" />
		<ComponentArt:ComboBoxItem Text="36" Value="36px" />
		<ComponentArt:ComboBoxItem Text="48" Value="48px" />
		<ComponentArt:ComboBoxItem Text="72" Value="72px" />
	</Items>
</ComponentArt:ComboBox>