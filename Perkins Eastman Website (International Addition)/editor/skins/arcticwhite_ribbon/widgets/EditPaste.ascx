<%@ Control Language="C#" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<ComponentArt:Menu
	Id="PasteMenu"
	RunAt="server"
	ContextMenu="Custom"
	Orientation="Vertical"
	CollapseDuration="0"
	TopGroupExpandDirection="BelowLeft"
	TopGroupExpandOffsetY="0"
	CssClass="mnu mnu-paste"
	DefaultItemLookId="MenuItem"
>
	<ItemLooks>
		<ComponentArt:ItemLook LookId="MenuItem" CssClass="mnu-item" HoverCssClass="mnu-item-h" />
	</ItemLooks>

	<Items>
		<ComponentArt:MenuItem Text="Paste" ClientSideCommand="Editor1.PasteHTML(true);" ClientTemplateId="PasteMenuItem" Value="paste" />
		<ComponentArt:MenuItem Text="Paste from Word" ClientSideCommand="Editor1.PasteHTML(true);" ClientTemplateId="PasteMenuItem" Value="word" />
	</Items>

	<ClientTemplates>
		<ComponentArt:ClientTemplate ID="PasteMenuItem">
			<div class="## DataItem.get_value() ##">
				<span class="ico"></span>
				<span class="lbl">## DataItem.get_text() ##</span>
			</div>
		</ComponentArt:ClientTemplate>
	</ClientTemplates>
</ComponentArt:Menu>


<script type="text/javascript">
	<%=PasteMenu.ClientID %>.ParentControlID = "<%=this.ClientID %>";
</script>