<%@ Control Language="C#" ClassName="EditorTemplate" %>
		<div class="ed">
			<%-- Ribbon toolbar interface --%>
			<div class="rbn">
				<%-- Left rounded edge of the ribbon --%>
				<div class="l"></div>

				<%-- Main area of the ribbon --%>
				<div class="m">

					<%-- "Font" toolbar group --%>
					<div class="grp fnt">
						<div class="l"></div>
						<div class="m">
							<%-- Toolbars --%>
							<div class="tb">
								<div class="tb0">$$ToolBars[0]$$</div>
								<div class="tb1">$$ToolBars[1]$$</div>
							</div>

							<%-- Label --%>
							<div class="lab">
								<div class="txt">Font</div>
								<div class="sh">Font</div>
							</div>
						</div>
						<div class="r"></div>
					</div>

					<%-- "Paragraph" toolbar group --%>
					<div class="grp para">
						<div class="l"></div>
						<div class="m">
							<%-- Toolbars --%>
							<div class="tb">
								<div class="tb2">$$ToolBars[2]$$</div>
								<div class="tb3">$$ToolBars[3]$$</div>
								<div class="tb4">$$ToolBars[4]$$</div>
							</div>

							<%-- Label --%>
							<div class="lab">
								<div class="txt">Paragraph</div>
								<div class="sh">Paragraph</div>
							</div>
						</div>
						<div class="r"></div>
					</div>

					<%-- "Commands" toolbar group --%>
					<div class="grp cmd">
						<div class="l"></div>
						<div class="m">
							<%-- Toolbars --%>
							<div class="tb">
								<div class="tb5">$$ToolBars[5]$$</div>
								<div class="tb6">$$ToolBars[6]$$</div>
							</div>

							<%-- Label --%>
							<div class="lab">
								<div class="txt">Commands</div>
								<div class="sh">Commands</div>
							</div>
						</div>
						<div class="r"></div>
					</div>

					<%-- "Insert" toolbar group --%>
					<div class="grp ins">
						<div class="l"></div>
						<div class="m">
							<%-- Toolbars --%>
							<div class="tb">
								<div class="tb7">$$ToolBars[7]$$</div>
								<div class="tb8">$$ToolBars[8]$$</div>
							</div>

							<%-- Label --%>
							<div class="lab">
								<div class="txt">Insert</div>
								<div class="sh">Insert</div>
							</div>
						</div>
						<div class="r"></div>
					</div>

				</div>

				<%-- Right rounded edge of the ribbon --%>
				<div class="r"></div>
			</div>
			<%-- /Ribbon toolbar interface --%>

			<%-- Textfield wrapper --%>
			<div class="tf" style="">
				<%-- Top (rounded corners) section --%>
				<div class="t">
					<div class="l"></div>
					<div class="r"></div>
				</div>

				<%-- Middle section --%>
				<div class="m" style="!important;">
					<%-- The editor's textarea goes in here --%>
					<div class="txt">$$editorarea$$</div>
				</div>

				<%-- Bottom (rounded corners) section --%>
				<div class="b">
					<div class="l"></div>
					<div class="r"></div>
				</div>
			</div>
			<%-- /Textfield wrapper --%>

			<%-- Statusbar --%>
			<div class="sb">
				<div class="l"></div>
				<div class="m">
					<%-- Mode-changing tabs --%>
					<div class="tabs">
						<a href="javascript:void(0);" onclick="$$parent$$.DesignMode();select_statusbar_button(this);this.blur();" class="button-selected">
							<span class="l"></span>
							<span class="m"><span class="icon design"></span>Design</span>
							<span class="r"></span>
						</a>

						<a href="javascript:void(0);" onclick="$$parent$$.SourceMode();select_statusbar_button(this);this.blur();" class="button">
							<span class="l"></span>
							<span class="m"><span class="icon html"></span>HTML</span>
							<span class="r"></span>
						</a>
					</div>
					<%-- /Mode-changing tabs --%>

					<%-- Path / breadcrumbs element --%>
					<div class="bc">
						<div class="l">
							<span>Path:</span>
							<div class="cap"></div>
						</div>

						<div class="m">$$breadcrumbs$$</div>

						<div class="r"></div>
					</div>

					<div class="sep" title=" | "></div>

					<div class="count">
						<div class="words" title="Word Count">
							<div class="txt">Words:</div>
							<div class="val">$$wordcount$$</div>
						</div>
						<div class="chars" title="Character Count">
							<div class="txt">Chars:</div>
							<div class="val">$$charactercount$$</div>
						</div>
					</div>
				</div>
				<div class="r"></div>
			</div>
			<%-- /Statusbar --%>
		</div>

		<%-- Preload CSS images --%>
		<div style="display:none;"><img src="../images/editor/static.png" width="1" height="1" /></div>
		<div style="display:none;"><img src="../images/toolbar/static.png" width="1" height="1" /></div>
		<%-- /Preload CSS images --%>