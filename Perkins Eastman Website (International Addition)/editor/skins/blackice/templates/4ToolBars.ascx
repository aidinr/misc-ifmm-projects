<%@ Control Language="C#" ClassName="EditorTemplate" %>

<table cellspacing="0" cellpadding="0" width="100%" class="wrapper">
  <tr>
    <td width="9" height="30" class="top-left"></td>
    <td width="100%" height="30" class="top">

      <div class="toolbar-row top">
        $$ToolBars[0]$$
      </div>
      <div class="toolbar-row">
        <div class="float-left" style="width:310px;">
          $$ToolBars[1]$$
        </div>
        <div class="float-left">
          $$ToolBars[2]$$
        </div>
      </div>
      <div class="toolbar-row">
        $$ToolBars[3]$$
      </div>

    </td>
    <td width="9" height="30" class="top-right"></td>
  </tr>

  <tr class="textfield-top">
    <td width="9" height="8" class="left"><img alt="" src="<%= this.Attributes["SkinFolderLocation"] %>/images/toolbar/_blank.png" width="9" height="8" /></td>
    <td width="100%" height="8" class="mid"><img alt="" src="<%= this.Attributes["SkinFolderLocation"] %>/images/toolbar/_blank.png" width="9" height="8" /></td>
    <td width="9" height="8" class="right"><img alt="" src="<%= this.Attributes["SkinFolderLocation"] %>/images/toolbar/_blank.png" width="9" height="8" /></td>
  </tr>

  <tr class="textfield">
    <td width="9" height="100%" class="left"><img alt="" src="<%= this.Attributes["SkinFolderLocation"] %>/images/toolbar/_blank.png" width="9" height="8" /></td>
    <td width="100%" height="275" class="mid">
      $$editorarea$$
    </td>
    <td width="9" height="100%" class="right"><img alt="" src="<%= this.Attributes["SkinFolderLocation"] %>/images/toolbar/_blank.png" width="9" height="8" /></td>
  </tr>

  <tr class="textfield-bottom">
    <td width="9" height="5" class="left"><img alt="" src="<%= this.Attributes["SkinFolderLocation"] %>/images/toolbar/_blank.png" width="9" height="5" /></td>
    <td width="100%" height="5" class="mid"><img alt="" src="<%= this.Attributes["SkinFolderLocation"] %>/images/toolbar/_blank.png" width="9" height="5" /></td>
    <td width="9" height="5" class="right"><img alt="" src="<%= this.Attributes["SkinFolderLocation"] %>/images/toolbar/_blank.png" width="9" height="5" /></td>
  </tr>

  <tr class="textfield-statusbar">
    <td width="9" height="31" class="left"><img alt="" src="<%= this.Attributes["SkinFolderLocation"] %>/images/toolbar/_blank.png" width="9" height="31" /></td>
    <td width="100%" height="31" class="mid">
      <div id="status-bar">
        <div class="mid">
          <div id="tabs">
            <a onclick="$$parent$$.DesignMode();select_statusbar_button(this);this.blur();return false;" href="javascript:void(0);" class="button-selected"><span><img src="<%= this.Attributes["SkinFolderLocation"] %>/images/editor/icon-design.png" style="display:block;" width="16" height="16" alt="Design" class="icon" />Design</span></a>
            <a onclick="$$parent$$.SourceMode();select_statusbar_button(this);this.blur();return false;" href="javascript:void(0);" class="button"><span><img src="<%= this.Attributes["SkinFolderLocation"] %>/images/editor/icon-html.png" style="display:block;" width="16" height="16" alt="HTML" class="icon" />HTML</span></a>
          </div>

          <div id="path-breadcrumbs">
            <div class="left">Path:</div>
            <div class="mid">$$breadcrumbs$$</div>
            <div class="right"></div>
          </div>

          <div id="count">
            <div class="words" title="Word Count">
              <div class="label">Words:</div>
              <div class="value">$$wordcount$$</div>
            </div>
            <div class="chars" title="Character Count">
              <div class="label">Chars:</div>
              <div class="value">$$charactercount$$</div>
            </div>
          </div>
        </div>
      </div>
    </td>
    <td width="9" height="31" class="right"><img alt="" src="<%= this.Attributes["SkinFolderLocation"] %>/images/toolbar/_blank.png" width="9" height="31" /></td>
  </tr>
</table>