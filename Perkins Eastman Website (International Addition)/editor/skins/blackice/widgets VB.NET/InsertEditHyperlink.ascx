<%@ Control Language="VB" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<script runat="server">

Sub Page_Load(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load  
    LinkAnchor.DropImageUrl = PrefixWithSkinFolderLocation(LinkAnchor.DropImageUrl)
    LinkAnchor.DropHoverImageUrl = PrefixWithSkinFolderLocation(LinkAnchor.DropHoverImageUrl)
    LinkType.DropImageUrl = PrefixWithSkinFolderLocation(LinkType.DropImageUrl)
    LinkType.DropHoverImageUrl = PrefixWithSkinFolderLocation(LinkType.DropHoverImageUrl)
    LinkCssClass.DropImageUrl = PrefixWithSkinFolderLocation(LinkCssClass.DropImageUrl)
    LinkCssClass.DropHoverImageUrl = PrefixWithSkinFolderLocation(LinkCssClass.DropHoverImageUrl)
End Sub
  
Private Function PrefixWithSkinFolderLocation(ByVal str As String) As String
    Dim prefix As String
    prefix = Me.Attributes("SkinFolderLocation")
    If str.IndexOf(prefix) = 0 Then
        Return str
    Else
        Return prefix & "/" & str
    End If
End Function

</script>

<ComponentArt:Dialog
  ID="HyperlinkDialog"
  RunAt="server"
  AllowDrag="true"
  AllowResize="false"
  Modal="false"
  Alignment="MiddleCentre"
  Width="442"
  Height="155"
  ContentCssClass="dlg-link">
  <ClientEvents>
    <OnShow EventHandler="HyperlinkDialog_OnShow" />
    <OnClose EventHandler="HyperlinkDialog_OnClose" />
  </ClientEvents>
  <Content>

    <div class="dlg-title draggable" onmousedown="<%=HyperlinkDialog.ClientID %>.StartDrag(event);">
      <div class="left"></div>
      <div class="mid">
        <a class="close" href="javascript:void(0);" onclick="<%=HyperlinkDialog.ClientID %>.close();this.blur();return false;"></a>
        <span>Insert / Edit Link</span>
      </div>
      <div class="right"></div>
    </div>

    <div class="dlg-tabstrip">
      <div class="left"></div>
      <div class="tabs">
        <a href="javascript:void(0);" class="tab-selected" onclick="toggle_dialog_tab(this,<%=HyperlinkDialog.ClientID %>,0);this.blur();"><span class="link">Link</span></a>
        <a href="javascript:void(0);" class="tab" onclick="toggle_dialog_tab(this,<%=HyperlinkDialog.ClientID %>,1);this.blur();"><span class="anchor">Anchor</span></a>
        <a href="javascript:void(0);" class="tab" onclick="toggle_dialog_tab(this,<%=HyperlinkDialog.ClientID %>,2);this.blur();"><span class="email">Email</span></a>
      </div>
      <div class="right"></div>
    </div>

    <div class="dlg-content">
      <div class="left link-basic"></div>
      <div class="mid link-basic">

        <ComponentArt:MultiPage
          ID="HyperlinkPages"
          RunAt="server">

          <ComponentArt:PageView RunAt="server" id="Link">

            <div class="row top" title="Required">
              <span class="label">Link URL:</span><input id="<%=Me.ClientID %>_link_url" type="text" class="link-url"
              onkeypress="toggle_preview_button(this);" onfocus="toggle_preview_button(this);"
              onblur="toggle_preview_button(this);" /><a href="javascript:void(0);"
              onclick="check_url(this);this.blur();" class="preview-url-disabled" title="Preview URL in new window"></a>
            </div>

            <div class="row" title="Optional">
              <span class="label">Link Text:</span><input id="<%=Me.ClientID %>_link_text" type="text" class="link-text" />
              <span class="label-narrow">Target:</span><input id="<%=Me.ClientID %>_link_target" type="text" class="link-target float-left" />
            </div>

            <div class="hr"><span style="display:none;">.</span></div>

            <div class="row">
              <span class="label" title="Optional">Tooltip:</span><input id="<%=Me.ClientID %>_link_tooltip" type="text" class="link-tooltip" disabled="disabled" />
              <span class="label-narrow">Anchor:</span>
              <span class="combo">
                <ComponentArt:ComboBox
                  ID="LinkAnchor"
                  RunAt="server"
                  Width="121"
                  Height="18"
                  ItemCssClass="simple-item"
                  ItemHoverCssClass="simple-item-hover"
                  CssClass="dlg-combobox"
                  TextBoxCssClass="dlg-textfield"
                  DropImageUrl="images/dialog/dropdown.png"
                  DropHoverImageUrl="images/dialog/dropdown-hover.png"
                  DropDownResizingMode="bottom"
                  DropDownWidth="190"
                  DropDownHeight="160"
                  DropDownCssClass="simple"
                  DropDownContentCssClass="simple-content"
                  SelectedIndex="6"
                  Enabled="false">
                <ClientEvents>
                    <Init EventHandler="ComboBoxInit" />
                  </ClientEvents>
                  <DropDownHeader>
                    <div class="menu-header"></div>
                  </DropDownHeader>
                  <DropDownFooter>
                    <div class="menu-footer"></div>
                  </DropDownFooter>
                  <Items>
                    <ComponentArt:ComboBoxItem Text="anchor #1" Value="anchor1" />
                    <ComponentArt:ComboBoxItem Text="anchor #2" Value="anchor2" />
                    <ComponentArt:ComboBoxItem Text="anchor #3" Value="anchor3" />
                    <ComponentArt:ComboBoxItem Text="anchor #4" Value="anchor4" />
                  </Items>
                </ComponentArt:ComboBox>
              </span>
            </div>

            <div class="row">
              <span class="label" title="Select a protocol for the hyperlink">Type:</span>
              <span class="combo">

                <ComponentArt:ComboBox
                  ID="LinkType"
                  RunAt="server"
                  Width="121"
                  Height="18"
                  ItemCssClass="simple-item"
                  ItemHoverCssClass="simple-item-hover"
                  CssClass="dlg-combobox"
                  TextBoxCssClass="dlg-textfield default-cursor"
                  DropImageUrl="images/dialog/dropdown.png"
                  DropHoverImageUrl="images/dialog/dropdown-hover.png"
                  KeyboardEnabled="false"
                  TextBoxEnabled="false"
                  DropDownResizingMode="bottom"
                  DropDownWidth="190"
                  DropDownHeight="160"
                  DropDownCssClass="simple"
                  DropDownContentCssClass="simple-content"
                  ItemClientTemplateId="LinkTypeItemTemplate"
                  SelectedIndex="3"
                  Enabled="false">
                  <ClientEvents>
                    <Change EventHandler="link_type_change" />
                    <Init EventHandler="ComboBoxInit" />
                  </ClientEvents>
                  <DropDownHeader>
                    <div class="menu-header"></div>
                  </DropDownHeader>
                  <DropDownFooter>
                    <div class="menu-footer"></div>
                  </DropDownFooter>
                  <Items>
                    <ComponentArt:ComboBoxItem Text="(Other)" Value="" />
                    <ComponentArt:ComboBoxItem Text="Local File" Value="file://" />
                    <ComponentArt:ComboBoxItem Text="FTP" Value="ftp://" />
                    <ComponentArt:ComboBoxItem Text="HTTP" Value="http://" />
                    <ComponentArt:ComboBoxItem Text="HTTP (Secure)" Value="https://" />
                    <ComponentArt:ComboBoxItem Text="Javascript" Value="javascript:" />
                    <ComponentArt:ComboBoxItem Text="Telnet" Value="telnet:" />
                    <ComponentArt:ComboBoxItem Text="Usenet" Value="news:" />
                  </Items>
                  <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="LinkTypeItemTemplate">
                      <span title="## (DataItem.get_text() == "(Other)") ? "Other: Please specify" : DataItem.get_text() + " (" + DataItem.get_value() + ")" ##">## DataItem.get_text() ##</span>
                    </ComponentArt:ClientTemplate>
                  </ClientTemplates>
                </ComponentArt:ComboBox>

              </span>
              <input type="text" class="link-type-other float-left disabled" id="link_type_other" disabled="disabled" />
            </div>

            <div class="row" title="Optional">
              <span class="label">CSS Class:</span>
              <span class="combo">

                <ComponentArt:ComboBox
                  ID="LinkCssClass"
                  RunAt="server"
                  Width="121"
                  Height="18"
                  ItemCssClass="simple-item"
                  ItemHoverCssClass="simple-item-hover"
                  CssClass="dlg-combobox"
                  TextBoxCssClass="dlg-textfield default-cursor"
                  DropImageUrl="images/dialog/dropdown.png"
                  DropHoverImageUrl="images/dialog/dropdown-hover.png"
                  KeyboardEnabled="false"
                  TextBoxEnabled="false"
                  DropDownResizingMode="bottom"
                  DropDownWidth="190"
                  DropDownHeight="160"
                  DropDownCssClass="simple"
                  DropDownContentCssClass="simple-content">
                  <DropDownHeader>
                    <div class="menu-header"></div>
                  </DropDownHeader>
                  <DropDownFooter>
                    <div class="menu-footer"></div>
                  </DropDownFooter>
                  <Items>
                    <ComponentArt:ComboBoxItem Text="css class 1" Value="cssClass1" />
                    <ComponentArt:ComboBoxItem Text="css class 2" Value="cssClass2" />
                    <ComponentArt:ComboBoxItem Text="css class 3" Value="cssClass3" />
                    <ComponentArt:ComboBoxItem Text="css class 4" Value="cssClass4" />
                    <ComponentArt:ComboBoxItem Text="css class 5" Value="cssClass5" />
                    <ComponentArt:ComboBoxItem Text="css class 6" Value="cssClass6" />
                  </Items>
                </ComponentArt:ComboBox>

              </span>
            </div>
          </ComponentArt:PageView>

          <ComponentArt:PageView  id="Anchor" RunAt="server">
            <div class="row top"><span class="label">Name:</span><input id="<%=Me.ClientID %>_anchor_name" type="text" class="anchor-name" /></div>
            <div class="row"><span class="label">Text:</span><input id="<%=Me.ClientID %>_anchor_text" type="text" class="anchor-text" /></div>
          </ComponentArt:PageView>

          <ComponentArt:PageView  id="Email" RunAt="server">
            <div class="row top"><span class="label">Address:</span><input id="<%=Me.ClientID %>_email_address"  type="text" class="email-address" /></div>
            <div class="row"><span class="label">Subject:</span><input id="<%=Me.ClientID %>_email_subject"  type="text" class="email-subject" /></div>
            <div class="row" title="Optional"><span class="label">Link Text:</span><input id="<%=Me.ClientID %>_email_text" type="text" class="email-text" />
            </div>
          </ComponentArt:PageView>

        </ComponentArt:MultiPage>

      </div>
      <div class="right link-basic"></div>
    </div>

    <div class="dlg-buttons">
      <div class="left"></div>
      <div class="mid">
        <a onclick="toggle_hyperlink_options(this,<%=HyperlinkDialog.ClientID %>);this.blur();return false;" href="javascript:void(0);" id="<%=Me.ClientID %>_hyperlink-options" class="button-70 float-left"><span class="advanced">Advanced</span></a>
        <a onclick="<%=HyperlinkDialog.ClientID %>.close(true);this.blur();return false;" href="javascript:void(0);" class="button-70 float-right"><span>Insert</span></a>
        <a onclick="<%=HyperlinkDialog.ClientID %>.close();this.blur();return false;" href="javascript:void(0);" class="button-70 float-right first"><span>Close</span></a>
      </div>
      <div class="right"></div>
    </div>
  </Content>
</ComponentArt:Dialog>


<script type="text/javascript">

<%=HyperlinkDialog.ClientID %>.ParentControlID = "<%=Me.ClientID %>";

function ComboBoxInit(combobox)
{
    combobox.ParentControlID = "<%=Me.ClientID %>";
}

</script>