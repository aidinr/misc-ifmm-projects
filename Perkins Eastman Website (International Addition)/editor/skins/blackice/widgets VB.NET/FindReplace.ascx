<%@ Control Language="VB" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<ComponentArt:Dialog
  ID="FindDialog"
  RunAt="server"
  AllowDrag="true"
  AllowResize="false"
  Modal="false"
  Alignment="MiddleCentre"
  Width="430"
  Height="126"
  ContentCssClass="dlg-find">
  <ClientEvents>
    <OnShow EventHandler="FindDialog_OnShow" />
    <OnClose EventHandler="FindDialog_OnClose" />
  </ClientEvents>

  <Content>
  
    <div class="dlg-title draggable" onmousedown="<%=FindDialog.ClientID %>.StartDrag(event);">
      <div class="left"></div>
      <div class="mid">
        <a class="close" href="javascript:void(0);" onclick="<%=FindDialog.ClientID %>.close();this.blur();return false;"></a>
        <span>Find</span>
      </div>
      <div class="right"></div>
    </div>

    <div class="dlg-tabstrip">
      <div class="left"></div>
      <div class="tabs">
        <a href="javascript:void(0);" class="tab-selected" onclick="toggle_dialog_tab(this,<%=FindDialog.ClientID %>,0);this.blur();"><span class="find">Find</span></a>
        <a href="javascript:void(0);" class="tab" onclick="toggle_dialog_tab(this,<%=FindDialog.ClientID %>,1);this.blur();"><span class="replace">Replace</span></a>
      </div>
      <div class="right"></div>
    </div>

    <div class="dlg-content">
      <div class="left find"></div>
      <div class="mid find">

        <ComponentArt:MultiPage
          ID="FindPages"
          RunAt="server">
          
          <ComponentArt:PageView ID="PageView1" RunAt="server">
            <div class="fields">
              <div class="row top"><span class="label">Find what:</span><input id="<%=Me.ClientID %>_find_text" type="text" class="input-find" name="find_text"/></div>
              <div class="row">
                <span class="label dir">Direction:</span>
                <a class="radio" href="javascript:void(0);" onclick="toggle_radio(this);this.blur();return false;">Up</a>
                <a class="radio-selected" href="javascript:void(0);" onclick="toggle_radio(this);this.blur();return false;">Down</a>
              </div>
              <div class="row widgets"><a id="<%=Me.ClientID %>_find_matchwholeword" class="checkbox match-word" href="javascript:void(0);" onclick="toggle_checkbox(this);this.blur();return false;">Match whole word only</a></div>
              <div class="row widgets"><a id="<%=Me.ClientID %>_find_matchcase" class="checkbox match-case" href="javascript:void(0);" onclick="toggle_checkbox(this);this.blur();return false;">Match case</a></div>
            </div>
            <div class="btns">
              <a onclick="FindDialog_Find(<%=FindDialog.ClientID %>);this.blur();return false;" href="javascript:void(0);" class="button-70 float-right"><span>Find Next</span></a>
            </div>
          </ComponentArt:PageView>

          <ComponentArt:PageView ID="PageView2" RunAt="server">
            <div class="fields">
              <div class="row top"><span class="label">Find what:</span><input id="<%=Me.ClientID %>_replace_text" type="text" class="input-find" name="replace_text"/></div>
              <div class="row"><span class="label">Replace with:</span><input id="<%=Me.ClientID %>_replace_replacetext" type="text" class="input-replace" name="replace_replacetext"/></div>
              <div class="row">
                <span class="label" style="">Direction:</span>
                <a class="radio" href="javascript:void(0);" onclick="toggle_radio(this);this.blur();return false;">Up</a>
                <a class="radio-selected" href="javascript:void(0);" onclick="toggle_radio(this);this.blur();return false;">Down</a>
              </div>
              <div class="row widgets"><a id="<%=Me.ClientID %>_replace_matchwholeword" class="checkbox match-word" href="javascript:void(0);" onclick="toggle_checkbox(this);this.blur();return false;">Match whole word only</a></div>
              <div class="row widgets"><a id="<%=Me.ClientID %>_replace_matchcase" class="checkbox match-case" href="javascript:void(0);" onclick="toggle_checkbox(this);this.blur();return false;">Match case</a></div>
            </div>
            <div class="btns">
              <a onclick="FindDialog_FindReplace(<%=FindDialog.ClientID %>);this.blur();return false;" href="javascript:void(0);" class="button-70"><span>Find Next</span></a>
              <a onclick="FindDialog_Replace(<%=FindDialog.ClientID %>);this.blur();return false;" href="javascript:void(0);" class="button-70"><span>Replace</span></a>
              <a onclick="FindDialog_ReplaceAll(<%=FindDialog.ClientID %>);this.blur();return false;" href="javascript:void(0);" class="button-70"><span>Replace All</span></a>
            </div>
          </ComponentArt:PageView>
          
        </ComponentArt:MultiPage>
        
      </div>
      <div class="right find"></div>
    </div>

    <div class="dlg-buttons">
      <div class="left"></div>
      <div class="mid">
        <a onclick="<%=FindDialog.ClientID %>.close();this.blur();return false;" href="javascript:void(0);" class="button-70 float-right"><span>Close</span></a>
      </div>
      <div class="right"></div>
    </div>

  </Content>

</ComponentArt:Dialog>

<script type="text/javascript">

<%=FindPages.ClientID %>.ParentControlID = "<%=Me.ClientID %>";
<%=FindDialog.ClientID %>.ParentControlID = "<%=Me.ClientID %>";

</script>