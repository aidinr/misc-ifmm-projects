﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="designPhilosophy.aspx.vb" Inherits="designPhilosophy" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Perkins Eastman : Design Philosophy</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
    <meta name="keywords" content="Research, Building Types, Firm Structure, Expertise, Sustainable Design" />
</head>
<body>
    <form id="form1" runat="server">
   <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    
    
    <script type="text/javascript">
	jQuery(".main").css("background-color", "#FFF");
</script>
<script type="text/javascript">
	jQuery(".main").css("background-color", "#FFF");
</script>
<div class="breadcrumbs"><a href="/">Home</a> | <a href="/firmProfile.aspx">About us</a> | Design Philosophy</div>
<div class="head_standard"><asp:literal ID="contentName" runat="server"></asp:literal></div>
<div class="copy_standard" style="margin-right: 15px;"><img src="images/Design-Philosophy-Banner.jpg" /></div>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td align="left" valign="top" width="577"><div class="copy_standard" style="margin-right: 15px; width:578px;"><asp:Literal ID="designPhilosophy" runat="server"></asp:Literal></div></td>
<td align="left" valign="top">
<asp:Repeater ID="repeaterDesignPhilosophy" runat="server">
<ItemTemplate>
    <img src="<%=Session("WSRetrieveAsset")%>type=asset&size=1&width=278&height=278&id=<%#container.dataitem("asset_id")%>" border="0" / ><br /><br />
</ItemTemplate>
</asp:Repeater>
</td>
</tr>
</table>

    
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
