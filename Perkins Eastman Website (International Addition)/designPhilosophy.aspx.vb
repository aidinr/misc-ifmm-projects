﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Net
Partial Class designPhilosophy
    Inherits System.Web.UI.Page
    Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim headerControl As Web.UI.Control
        headerControl = Me.LoadControl("includes/header.ascx")
        pageHeader.Controls.Add(headerControl)

        Dim bodyTopControl As Web.UI.Control
        bodyTopControl = Me.LoadControl("includes/body_top.ascx")
        bodyTop.Controls.Add(bodyTopControl)

        Dim bodyFooterControl As Web.UI.Control
        bodyFooterControl = Me.LoadControl("includes/body_footer.ascx")
        bodyBottom.Controls.Add(bodyFooterControl)

        GenerateDesignPhilosophy()

    End Sub

    Sub GenerateDesignPhilosophy()


        Dim sql As String = "select *, b.name folderName from IPM_ASSET_CATEGORY b, IPM_ASSET a  left join ipm_asset_field_value c on c.item_id = @websiteDescriptionUDFID and c.asset_id = a.asset_id where a.ProjectID = @projectid and b.CATEGORY_ID = a.Category_ID and b.NAME = @folderName and media_type = 21355126 and a.available = 'Y'"

        Dim MyConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connstring"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter(sql, MyConnection)

        Dim projectIDParameter As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(projectIDParameter)
        MyCommand1.SelectCommand.Parameters("@projectid").Value = ConfigurationSettings.AppSettings("aboutUsProjectID")

        Dim folderNameParameter As New SqlParameter("@folderName", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(folderNameParameter)
        MyCommand1.SelectCommand.Parameters("@folderName").Value = ConfigurationSettings.AppSettings("designPhilosophyFolderName")

        Dim websiteDescriptionParameter As New SqlParameter("@websiteDescriptionUDFID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(websiteDescriptionParameter)
        MyCommand1.SelectCommand.Parameters("@websiteDescriptionUDFID").Value = ConfigurationSettings.AppSettings("websiteContentUDFID")

        Dim dt1 As New DataTable
        MyCommand1.Fill(dt1)

        If (dt1.Rows.Count > 0) Then
            Try

                contentName.Text = dt1.Rows(0)("foldername")
                designPhilosophy.Text = dt1.Rows(0)("item_value")

            Catch ex As Exception

            End Try
        End If

        Dim sql2 As String = "select *, b.name folderName from IPM_ASSET a, IPM_ASSET_CATEGORY b where a.ProjectID = @projectid and b.CATEGORY_ID = a.Category_ID and b.NAME = @folderName and media_type = 10 and a.available = 'y'"
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter(sql2, MyConnection)

        Dim projectIDParameter2 As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(projectIDParameter2)
        MyCommand2.SelectCommand.Parameters("@projectid").Value = ConfigurationSettings.AppSettings("aboutUsProjectID")

        Dim projectIDParameter3 As New SqlParameter("@folderName", SqlDbType.VarChar, 255)
        MyCommand2.SelectCommand.Parameters.Add(projectIDParameter3)
        MyCommand2.SelectCommand.Parameters("@folderName").Value = ConfigurationSettings.AppSettings("designPhilosophyFolderName")

        Dim dt2 As New DataTable
        MyCommand2.Fill(dt2)

        repeaterDesignPhilosophy.DataSource = dt2
        repeaterDesignPhilosophy.DataBind()


        'designPhilosophy.Text = GetFile(Session("WSDownloadAsset").ToString & "size=0&assetid=" & dt1.Rows(0)("asset_id").ToString)


        'Dim objReader As StreamReader = New StreamReader(System.Environment.CurrentDirectory & "\content\firmProfile.txt")
        'Dim objReader As StreamReader = New StreamReader("c:\projects\Perkins_Eastman_Website\content\designPhilosophy.txt")

        'designPhilosophy.Text = objReader.ReadToEnd()



    End Sub

    Function GetFile(ByVal URL As String) As String
        Try
            Dim clientreq As HttpWebRequest
            clientreq = HttpWebRequest.Create(URL)
            clientreq.Timeout = CType(10000, Integer)
            Dim clientres As HttpWebResponse = clientreq.GetResponse
            Dim data As IO.Stream = clientres.GetResponseStream
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd

            Return str

        Catch ex As Exception
            Throw New Exception("WebService not available." + URL)
        End Try
    End Function

End Class

