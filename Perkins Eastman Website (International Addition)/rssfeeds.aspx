﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="rssfeeds.aspx.vb" Inherits="rssfeeds" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Perkins Eastman: RSS Feeds</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
</head>
<body>
    <form id="form1" runat="server">
     <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
     <div class="head_standard">Perkins Eastman RSS Feeds</div>

<div class="copy_standard red bold">What is an RSS Feed?</div>
<div class="copy_standard">An RSS (Really Simple Syndication) Feed is a format for distributing and gathering content from many different sources across the World Wide Web, including newspapers, magazines, and other news-worthy Web sites. Each "feed" can be subscribed to and displayed in an RSS Reader.<br /><br />
<a href="rss/pressReleases.aspx"><img src="images/rss_feed.gif" border="0" /></a>&nbsp;<a href="rss/pressReleases.aspx">Subscribe</a> to a feed of our press releases: <a href="rss/pressReleases.aspx">http://www.perkinseastman.com/rss/pressReleases.aspx</a><br /><br />
<a href="rss/press.aspx"><img src="images/rss_feed.gif" border="0" /></a>&nbsp;<a href="rss/press.aspx">Subscribe</a> to a feed of our recent press: <a href="rss/press.aspx">http://www.perkinseastman.com/rss/press.aspx</a><br /><br />
<a href="rss/speaking.aspx"><img src="images/rss_feed.gif" border="0" /></a>&nbsp;<a href="rss/speaking.aspx">Subscribe</a> to a feed of our speaking engagements: <a href="rss/speaking.aspx">http://www.perkinseastman.com/rss/speaking.aspx</a><br />
    <br />
<a href="rss/speaking.aspx"><img src="images/rss_feed.gif" border="0" /></a>&nbsp;<a 
        href="rss/projects.aspx">Subscribe</a> to a feed of our projects: 
    <a href="rss/projects.aspx">http://www.perkinseastman.com/rss/projects.aspx</a><br /><br />
<!--<a href="rss/projects.cfm"><img src="images/rss_feed.gif" border="0" /></a>&nbsp;<a href="rss/projects.cfm">Subscribe</a> to a feed of our projects: <a href="rss/projects.cfm">http://www.perkinseastman.com/rss/projects.aspx</a><br /><br /><br />-->

To view our feeds, an RSS reader is required. You will need the URL of the RSS feed that you wish to subscribe to.

<ul class="copy_standard" style="list-style-type: square; color: rgb(220, 41, 30);">
	<li style="margin: 0pt 0pt 0pt 5px; list-style:square"><span class="copy_standard" style="color: rgb(51, 51, 51);">Download an RSS Reader (i.e. <a href="http://www.feedreader.com/" target="_blank">FeedReader</a>, <a href="http://www.google.com/reader" target="_blank">Google Reader</a>, <a href="http://www.apple.com/itunes/overview/" target="_blank">iTunes</a>).</span></li>
    <li style="margin: 0pt 0pt 0pt 5px;"><span class="copy_standard" style="color: rgb(51, 51, 51);">Locate the URL of the RSS feed that you wish to subscribe, above (i.e. <a href="rss/pressReleases.aspx">http://www.perkinseastman.com/rss/pressReleases.aspx</a>).</span></li>
    <li style="margin: 0pt 0pt 0pt 5px;"><span class="copy_standard" style="color: rgb(51, 51, 51);">Copy and paste the URL of the RSS feed (there is usually an "add feed" or "subscribe" button) in the RSS Reader.</span></li>
    <li style="margin: 0pt 0pt 0pt 5px;"><span class="copy_standard" style="color: rgb(51, 51, 51);">The information in the feed will be updated when the feed contains new content.</span></li>
</ul></div>
     <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
