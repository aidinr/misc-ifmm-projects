﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="firmProfile.aspx.vb" Inherits="firmProfile" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Perkins Eastman: Firm Profile</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
    <meta name="keywords" content="Sustainability, Innovation, BFJ Planning, RGR Landscape, Urbanomics, Russell Design" />
    
    <link rel="stylesheet" type="text/css" href="editor/skins/office2003/css/style.css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
    
    
    <script type="text/javascript">
	jQuery(".main").css("background-color", "#FFF");
</script>
<div class="breadcrumbs"><a href="/">Home</a> | <a href="/firmProfile.aspx">About us</a> | Firm Profile</div>
<div class="head_standard"><asp:Literal ID="contentName" runat="server"></asp:Literal></div>
<div class="copy_standard" style="margin-right: 15px;"><img src="images/Firm-Profile-Banner.jpg" /></div>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td align="left" valign="top" width="577">

<asp:Literal ID="literalFirmProfile" runat="server"></asp:Literal>

<asp:Literal ID="literalEditorSave" runat="server"></asp:Literal>
<ComponentArt:Editor runat="server" ID="Editor1"
    PreserveRelativePaths="true"
    TemplateFile="1ToolBar.ascx"
    SkinFolderLocation="editor/skins/office2003"
    SourceCssClass="SourceCssClass"
    DesignCssClass="DesignCssClassPE"
    HighlightElementCssClass="HighlightCssClass"
    CssFileURL="editor/skins/office2003/css/iframe.css"
    ContentHTML="Hi!"
    BreadcrumbClientTemplateId="breadcrumbTemplate"
    BreadcrumbCssClass="f-l"
    BreadcrumbSeparator="<div class='sep'></div>"
    Width="678"
    Height="800"
>

    <ClientTemplates>
        <ComponentArt:ClientTemplate Id="breadcrumbTemplate">
            <a onclick="this.blur();return false;" href="javascript:void(0);" class="btn"><span>&lt;##DataItem.breadcrumb##&gt;</span></a>
        </ComponentArt:ClientTemplate>
    </ClientTemplates>

    <ToolBars>
        <ComponentArt:ToolBar
            ID="FormattingToolBar"
            RunAt="server"
            ImagesBaseUrl="editor/skins/office2003/images"
            CssClass="toolbar"
            Width="100"
            Height="25"
            DefaultItemImageWidth="25"
            DefaultItemImageHeight="25"
            UseFadeEffect="false"
            SiteMapXmlFile="editor/features/office2003/MinimalToolBar.xml"
        >

            <ClientEvents>
                <ItemMouseUp EventHandler="font_toolbar_mouseup" />
            </ClientEvents>
        </ComponentArt:ToolBar>
    </ToolBars>
</ComponentArt:Editor>
</td>
<td align="left" valign="top">

<asp:Repeater ID="repeaterFirmProfile" runat="server">
<ItemTemplate>
    <img src="<%=Session("WSRetrieveAsset")%>type=asset&size=1&width=278&height=278&id=<%#container.dataitem("asset_id")%>" border="0" / ><br /><br />
</ItemTemplate>
</asp:Repeater>

</td>
</tr>
</table>

    
    <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
