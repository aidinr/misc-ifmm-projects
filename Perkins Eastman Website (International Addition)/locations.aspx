﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="locations.aspx.vb" Inherits="locations" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <title>Perkins Eastman: Locations</title>
    <asp:PlaceHolder id="pageHeader" runat="server"></asp:PlaceHolder>
    <meta name="keywords" content="New York, Arlington, Boston, Charlotte, Chicago, Oakland, Pittsburgh, Stamford, Toronto, Shanghai, Guayaquil, Mumbai, Dubai, Ecuador, India, UEA, China, Canada, Virginia, Washington, DC, North Carolina, Massachusetts, Pennsylvania, California, Connecticut" />
    
</head>
<body>
    <form id="form1" runat="server">
   <asp:PlaceHolder ID="bodyTop" runat="server"></asp:PlaceHolder>
   
   <script type="text/javascript">
	jQuery(".main").css("background-color", "#FFF");
</script>
<!--<cfoutput>-->
<div class="breadcrumbs"><a href="/">Home</a> | LOCATIONS</div>
<div class="copy_standard" style="margin-right: 15px; width:870px">
<table cellspacing="0" cellpadding="0" border="0" width="870">      <tbody valign="top">          <tr valign="top">              <td colspan="2">              <div class="head_standard"><asp:literal ID="literalLocationTitle" runat="server"></asp:literal></div>              </td>          </tr>          <tr>              <td colspan="3">              <div class="copy_standard no_top_margin">

<asp:literal ID="literalLocation" runat="server"></asp:literal>


</div>              </td>          </tr>      </tbody>  </table>  <p>&nbsp;</p>  <p>&nbsp;</p> <P>
</div>
<!--</cfoutput>-->
   
   <asp:PlaceHolder ID="bodyBottom" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
