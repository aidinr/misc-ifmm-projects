﻿Imports System.Data.OleDb
Imports System.Configuration
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Net
Imports System.Web
Imports Aurigma.GraphicsMill
Imports Aurigma.GraphicsMill.WinControls
Imports Aurigma.GraphicsMill.Codecs
Imports Aurigma.GraphicsMill.Transforms
Imports System.Drawing

Imports Aurigma.GraphicsMill.Codecs.Psd

Public Class IDAMPushMain

    Public Shared Function iDAMPushMainFunction(delay As Integer) As Boolean
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Dim ConnectionStringIDAM As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
        'Leadtools.RasterSupport.SetLicense(ConfigurationSettings.AppSettings("IDAM.LICENSEPATH") & "Interface Multimedia-IMGPRO18_OEMSVR.lic", "ysxHXp5Tupb3bQ7DAdFzk+WRWcBFLmEIkd2b3tpphDOboLDYHWPI1wmYpXqhCnFE")

        'get files to migrate
        'select a.asset_id,a.name,d.name projectname from IPM_ASSET a,ipm_asset_field_value b, ipm_asset_field_desc c, ipm_project d where a.asset_id = b.asset_id and c.item_id = b.Item_ID and c.Item_Tag = 'IDAM_FILE_SYSTEM_PUSH_REPLICATION' and b.item_value = 1 and a.proc_status = 0 and a.Available = 'Y' and a.ProjectID = d.projectid


        Dim IDAM_FILE_SYSTEM_NETWORK_LOCATION_ID As String = IDAMPushFunctions.GetDataTable("select item_id from ipm_asset_field_desc where item_tag = 'IDAM_FILE_SYSTEM_NETWORK_LOCATION'", ConnectionStringIDAM).Rows(0)("item_id")
        Dim IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID As String = IDAMPushFunctions.GetDataTable("select item_id from ipm_asset_field_desc where item_tag = 'IDAM_FILE_SYSTEM_INFORMATION_STATUS'", ConnectionStringIDAM).Rows(0)("item_id")
        Dim IDAM_FILE_SYSTEM_PUSH_REPLICATION_ID As String = IDAMPushFunctions.GetDataTable("select item_id from ipm_asset_field_desc where item_tag = 'IDAM_FILE_SYSTEM_PUSH_REPLICATION'", ConnectionStringIDAM).Rows(0)("item_id")

        Dim currentasset_id As String = ""
        Dim currentfilepath As String = ""
        Do While 1 = 1
            Try
                System.Threading.Thread.Sleep(delay)
                'preprocess
                'IDAM_FILE_SYSTEM_NETWORK_LOCATION()
                'IDAM_FILE_SYSTEM_INFORMATION_STATUS()
                Dim filepath As String 


                Dim path As String = "C:\Users\damjan\Desktop\12068_site.rvt"
        Dim creator As ThumbnailCreator  = new ThumbnailCreator()
        creator.DesiredSize = new Size(600, 600)
        Dim bm As System.Drawing.Bitmap = creator.GetThumbNail(path)
                bm.Save("C:\Users\damjan\Desktop\12068_site.png", System.Drawing.Imaging.ImageFormat.Png)


                                'Dim new_filename = IDAMPushFunctions.SafeOSName("")
                                'new_filename =  new_filename.ToUpper.Replace(".PSD", "") & "_hires.JPG"
                                'Dim reader = new PsdReader(filepath)
                                'Dim colorConverter = new Aurigma.GraphicsMill.Transforms.ColorConverter(PixelFormat.Format24bppRgb)
                                'Dim resolution = new ResolutionModifier(Integer.Parse(System.Configuration.ConfigurationSettings.AppSettings("IDAM.JPG_RESOLUTION") & ""), Integer.Parse(System.Configuration.ConfigurationSettings.AppSettings("IDAM.JPG_RESOLUTION") & ""))
                                'Dim w As Integer = reader.Width
                                'Dim h As Integer = reader.Height
                                'Dim newWidth As Integer
                                'Dim newHeight As Integer
                                'Dim ratio As Decimal
                                'If w > h then
                                    
                                '    newWidth = Integer.Parse(System.Configuration.ConfigurationSettings.AppSettings("IDAM.SIZE") & "")
                                '    ratio = h/w
                                '    newHeight = Integer.Parse(System.Configuration.ConfigurationSettings.AppSettings("IDAM.SIZE") & "") * ratio
                                'ElseIf w = h then
                                '    newHeight = Integer.Parse(System.Configuration.ConfigurationSettings.AppSettings("IDAM.SIZE") & "")
                                '    newWidth = Integer.Parse(System.Configuration.ConfigurationSettings.AppSettings("IDAM.SIZE") & "")
                                'Else
                                '    newHeight = Integer.Parse(System.Configuration.ConfigurationSettings.AppSettings("IDAM.SIZE") & "")
                                '    ratio = w/h
                                '    newWidth = Integer.Parse(System.Configuration.ConfigurationSettings.AppSettings("IDAM.SIZE") & "") * ratio

                                'End If
                                'Dim resize = new Resize( newWidth, newHeight, ResizeInterpolationMode.High)
                                ''resizer.Flags = Leadtools.RasterSizeFlags.Resample
                                ''resizer.Run(MyImage)
                                ''MyImage.XResolution = c
                                ''MyImage.YResolution = Integer.Parse(System.Configuration.ConfigurationSettings.AppSettings("IDAM.JPG_RESOLUTION") & "")

                                ''SAVE New file
                                ''_codecs.Save(MyImage, filepath, Leadtools.RasterImageFormat.Jpeg, Integer.Parse(System.Configuration.ConfigurationSettings.AppSettings("IDAM.JPG_BIT")))
                                'Dim newFilepath As String = filepath.ToUpper.Replace(new_filename.ToUpper, new_filename.ToUpper)
                                'newFilepath = newFilepath.ToUpper.Replace(".PSD",  ".JPG")

                                'Using writer3 = ImageWriter.Create(newFilepath)
                                '    colorConverter.DefaultSourceProfile = new ColorProfile(ConfigurationSettings.AppSettings("IDAM.LICENSEPATH") & "ISOcoated_v2_eci.icc")
                                '    colorConverter.DestinationProfile = ColorProfile.FromSrgb()

                                '    colorConverter.Receivers.Clear()
                                '    colorConverter.Receivers.Add(resize)
                                '    resize.Receivers.Add(resolution)
                                '    resolution.Receivers.Add(writer3)
                                '    reader.MergeLayers(colorConverter)
                                'End Using
                                

                                ' ''DELETE ORIGINAL
                                'reader.Close()
                                'File.Delete(filepath)
                                'filepath = newFilepath
                                'filepath = filepath.ToUpper.Replace(".PSD",  ".JPG")
                                
                                

            Catch ex As Exception





            End Try
        Loop
    End Function


    Private Shared Function CreateDownloadURL(ASSETID As String) As String
        ' Code that runs when a new session is started
        ' Code that runs when a new session is started
        Dim assetDownload = ConfigurationSettings.AppSettings("IDAM.IDAMDOWNLOADURL") & ""
        Dim sBaseInstance = ConfigurationSettings.AppSettings("IDAM.IDAMINSTANCE") & ""

        Dim theLogin As String = ConfigurationSettings.AppSettings("IDAM.IDAMDOWNLOADUSERID") & ""
        Dim thePassword As String = ConfigurationSettings.AppSettings("IDAM.IDAMDOWNLOADPWD") & ""
        Dim loginStatus As Boolean = False
        Dim loginResult1 As String = ""
        Dim sessionID As String = "(S(" & GetSessionID(ConfigurationSettings.AppSettings("IDAM.IDAMGETSESSIONURL")) & "))"
        Try

            loginResult1 = LoginToClient(Replace(ConfigurationSettings.AppSettings("IDAM.IDAMLOGINURL"), "[SESSIONID]", sessionID), theLogin, thePassword, sBaseInstance)

        Catch Ex As Exception
            Throw Ex
        End Try

        assetDownload = Replace(assetDownload, "[SESSIONID]", sessionID) + "?dtype=assetdownload&assetid=" & ASSETID & "&instance=" & sBaseInstance & "&size=0"

        Dim loginResult2 As String = ""
        CreateDownloadURL = assetDownload

    End Function




    Private Shared Function LoginToClient(ByVal pURL As String, ByVal pUsername As String, ByVal pPassword As String, ByVal pInstanceID As String) As Boolean
        Dim fullURL As String
        Try

            Dim wc As New Net.WebClient
            Dim result As String
            fullURL = GetLoginRequestURL(pURL, pUsername, pPassword, pInstanceID, False, False, False, 2)
            Dim data As IO.Stream = wc.OpenRead(fullURL)
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            Return (str.IndexOf("SUCCESS") >= 0)
        Catch ex As Exception
            'Log.Error("error logging in to :" + fullURL, ex)
            Return False
        End Try
    End Function


    Public Shared Function GetSessionID(ByVal URL As String) As String
        Try
            Dim clientreq As HttpWebRequest
            clientreq = HttpWebRequest.Create(URL)
            clientreq.Timeout = CType(60000, Integer)
            Dim clientres As HttpWebResponse = clientreq.GetResponse
            Dim data As IO.Stream = clientres.GetResponseStream
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            If str.IndexOf("SESSIONID") >= 0 Then
                Dim strtidx As Integer = str.IndexOf("SESSIONID=") + 10
                Return str.Substring(strtidx, str.IndexOf("</span>") - strtidx)
            Else
                Throw New Exception("No session ID found. check URL if it has the proper format" + URL)
            End If
        Catch ex As Exception
            Throw New Exception("WebService not available." + URL)
        End Try
    End Function

    Public Const LOGIN As String = "txtLogin"
    Public Const PASSWORD As String = "txtPassword"
    Public Const INSTANCEID As String = "txtInstanceID"
    Public Const DIRECT As String = "direct"
    Public Const ENCRYPTED As String = "encrypted"
    Public Const AUTHENTICATIONLEVEL As String = "level"
    Public Const SECUREMODE As String = "secure"
    Public Const LDAPDOMAIN As String = "ld"
    Public Const LDAPPATH As String = "ldp"

    Public Shared Function GetLoginRequestURL(ByVal pLocation As String, ByVal pLogin As String, ByVal pPassword As String, ByVal pInstanceID As String, ByVal pSecureMode As Boolean, Optional ByVal pEncrypted As Boolean = False, Optional ByVal pDirectMode As Boolean = False, Optional ByVal pAuthenticationLevel As Integer = 0, Optional ByVal pLDAPDomain As String = "", Optional ByVal pLDAPPath As String = "") As String
        If pLocation Is Nothing Then
            Throw New ArgumentException("location cannot be null")
        End If
        If pLogin Is Nothing Then
            Throw New ArgumentException("login cannot be null")
        End If
        If pInstanceID Is Nothing Then
            Throw New ArgumentException("pInstanceID cannot be null")
        End If
        If pPassword Is Nothing Then
            Throw New ArgumentException("pPassword cannot be null")
        End If

        Dim url As New System.Text.StringBuilder
        url.Append(pLocation)
        url.Append("?")
        url.Append(LOGIN).Append("=").Append(pLogin).Append("&")
        url.Append(PASSWORD).Append("=").Append(pPassword).Append("&")
        url.Append(INSTANCEID).Append("=").Append(pInstanceID).Append("&")
        url.Append(DIRECT).Append("=").Append(pDirectMode.ToString).Append("&")
        url.Append(ENCRYPTED).Append("=").Append(pEncrypted.ToString).Append("&")
        url.Append(AUTHENTICATIONLEVEL).Append("=").Append(pAuthenticationLevel.ToString).Append("&")
        url.Append(SECUREMODE).Append("=").Append(pSecureMode.ToString).Append("&")
        If pLDAPDomain <> "" Then
            url.Append(LDAPDOMAIN).Append("=").Append(pLDAPDomain.ToString).Append("&")
        End If
        If pLDAPPath <> "" Then
            url.Append(LDAPPATH).Append("=").Append(pLDAPPath.ToString)
        End If


        Return url.ToString

    End Function



End Class




''Public Class ClientLoginRequestURLContract

''    Public Const LOGIN As String = "txtLogin"
''    Public Const PASSWORD As String = "txtPassword"
''    Public Const INSTANCEID As String = "txtInstanceID"
''    Public Const DIRECT As String = "direct"
''    Public Const ENCRYPTED As String = "encrypted"
''    Public Const AUTHENTICATIONLEVEL As String = "level"
''    Public Const SECUREMODE As String = "secure"
''    Public Const LDAPDOMAIN As String = "ld"
''    Public Const LDAPPATH As String = "ldp"

''    Public Shared Function GetLoginRequestURL(ByVal pLocation As String, ByVal pLogin As String, ByVal pPassword As String, ByVal pInstanceID As String, ByVal pSecureMode As Boolean, Optional ByVal pEncrypted As Boolean = False, Optional ByVal pDirectMode As Boolean = False, Optional ByVal pAuthenticationLevel As Integer = 0, Optional ByVal pLDAPDomain As String = "", Optional ByVal pLDAPPath As String = "") As String
''        If pLocation Is Nothing Then
''            Throw New ArgumentException("location cannot be null")
''        End If
''        If pLogin Is Nothing Then
''            Throw New ArgumentException("login cannot be null")
''        End If
''        If pInstanceID Is Nothing Then
''            Throw New ArgumentException("pInstanceID cannot be null")
''        End If
''        If pPassword Is Nothing Then
''            Throw New ArgumentException("pPassword cannot be null")
''        End If

''        Dim url As New Text.StringBuilder
''        url.Append(pLocation)
''        url.Append("?")
''        url.Append(LOGIN).Append("=").Append(pLogin).Append("&")
''        url.Append(PASSWORD).Append("=").Append(pPassword).Append("&")
''        url.Append(INSTANCEID).Append("=").Append(pInstanceID).Append("&")
''        url.Append(DIRECT).Append("=").Append(pDirectMode.ToString).Append("&")
''        url.Append(ENCRYPTED).Append("=").Append(pEncrypted.ToString).Append("&")
''        url.Append(AUTHENTICATIONLEVEL).Append("=").Append(pAuthenticationLevel.ToString).Append("&")
''        url.Append(SECUREMODE).Append("=").Append(pSecureMode.ToString).Append("&")
''        If pLDAPDomain <> "" Then
''            url.Append(LDAPDOMAIN).Append("=").Append(pLDAPDomain.ToString).Append("&")
''        End If
''        If pLDAPPath <> "" Then
''            url.Append(LDAPPATH).Append("=").Append(pLDAPPath.ToString)
''        End If


''        Return url.ToString

''    End Function
''End Class
