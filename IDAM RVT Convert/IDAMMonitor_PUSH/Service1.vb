﻿Imports System.Data.OleDb
Imports System.Configuration
Imports System.IO
Imports System.Text.RegularExpressions
Public Class IDAMPush

    Protected Overrides Sub OnStart(ByVal args() As String)
        Dim idamrefresh As Integer = 10000
        If ConfigurationManager.AppSettings("IDAM.REFRESH") <> "" Then
            idamrefresh = ConfigurationManager.AppSettings("IDAM.REFRESH")
        End If
        IDAMPushMain.iDAMPushMainFunction(idamrefresh)
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub
End Class
