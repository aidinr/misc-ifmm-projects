﻿Imports System.Data.OleDb
Imports System.Configuration
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Net




Public Class IDAMPushFunctions


    Public Shared Function SafeOSName(ByVal nom As String) As String

        'First Trim the raw string
        Dim safe As String = nom.Trim

        'Replace spaces with hyphens
        'safe = safe.Replace(" ", "-").ToLower()

        '' ''Replace double spaces with singles
        ' ''If safe.IndexOf("--") > 1 Then
        ' ''    While (safe.IndexOf("--") > -1)
        ' ''        safe = safe.Replace("--", "-")
        ' ''    End While
        ' ''End Iff

        'Trim out illegal characters
        '"\"M\"\\a/ry/ h**ad:>> a\\/:*?\"| li*tt|le|| la\"mb.?"
        For Each lDisallowed In System.IO.Path.GetInvalidFileNameChars()
            safe = safe.Replace(lDisallowed.ToString(), "")
        Next
        For Each lDisallowed In System.IO.Path.GetInvalidPathChars()
            safe = safe.Replace(lDisallowed.ToString(), "")
        Next
        
        'safe = Regex.Replace(safe, "\/:*?""<>|", "")

        'Trim Length
        If safe.Length > 250 Then
            safe = safe.Substring(0, 249)
        End If

        ''Clean the beginning and end of filename
        'Dim replace As Char() = {"-", "."}
        'safe = safe.TrimStart(replace)
        'safe = safe.TrimEnd(replace)

        Return safe

    End Function



    Public Shared Function getPreviewImageFromIDAM(ByVal AssetId, ByVal instanceid, ByVal browseURL, ByVal cachepath, ByVal page) As String


        ' ''Dim _downservice As New idamds.AssetDownloadServiceSoapClient
        ' ''Dim _wsurl As String = "192.168.1.48:8020/IDAM"

        '' ''Apply session id to ensure validation to proper session
        ' ''Dim _sessionid As String = GetSessionID("http://" & _wsurl & "/getsessionid.aspx")


        '' ''WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLLocationIDAMDownloadType(+"?type=" + e.Parameters(2) + "&assetid=" + e.Parameters(1) + "&filename=" + e.Parameters(3) + ".zip" + "&instance=" + IDAMInstance + "','DownloadTypeAsset','width=500,height=75,location=no")
        ' '' ''http://idamimg.webarchives.com/IDAMCLIENT/(S(x5da5fq1kpuowy550gjut455))/RetrieveAsset.aspx?id=30149910&instance=IDAM_WEBARCHIVES&type=asset&size=2&height=32&width=32&crop=1&qfactor=50


        ' ''_downservice.Endpoint.Address = New EndpointAddress("http://" & _wsurl & "/(S(" & _sessionid & "))/assetdownloadservice.asmx")


        ' ''_downservice.Login("jburlinson", WebArchives.iDAM.WebCommon.Security.Encrypt("Kimberly777"), "IDAM_WEBARCHIVES", True)
        ' ''Dim _status As Boolean = _downservice.GetLoginStatus
        ' ''Dim _ticket As String = _downservice.RequestAssetByDownloadID("IDAM_WEBARCHIVES", 30149910, 30059017)

       
        Dim idamsize As String = "4000"
        If System.Configuration.ConfigurationSettings.AppSettings("IDAM.SIZE") <> "" Then
            idamsize = System.Configuration.ConfigurationSettings.AppSettings("IDAM.SIZE")
        End If
        'RetrieveAsset.aspx?id=30056115&instance=IDAM_WEBARCHIVES&type=asset&size=1&cache=1
        Dim downloadURL As String = browseURL & "?id=" & AssetId & "&instance=" & instanceid & "&type=asset&size=1&width=" & idamsize & "&height=" & idamsize & "&cache=1&page=" & page
        Dim cachepathname As String = cachepath
        SaveUrlToPath(cachepathname, downloadURL)
    End Function

    Public Shared Function getOriginalFileFromIDAM(ByVal downloadURL, ByVal cachepath) As String


        ' ''Dim _downservice As New idamds.AssetDownloadServiceSoapClient
        ' ''Dim _wsurl As String = "192.168.1.48:8020/IDAM"

        '' ''Apply session id to ensure validation to proper session
        ' ''Dim _sessionid As String = GetSessionID("http://" & _wsurl & "/getsessionid.aspx")


        '' ''WebArchives.iDAM.Web.Core.IDAMWebSession.Config.URLLocationIDAMDownloadType(+"?type=" + e.Parameters(2) + "&assetid=" + e.Parameters(1) + "&filename=" + e.Parameters(3) + ".zip" + "&instance=" + IDAMInstance + "','DownloadTypeAsset','width=500,height=75,location=no")
        ' '' ''http://idamimg.webarchives.com/IDAMCLIENT/(S(x5da5fq1kpuowy550gjut455))/RetrieveAsset.aspx?id=30149910&instance=IDAM_WEBARCHIVES&type=asset&size=2&height=32&width=32&crop=1&qfactor=50


        ' ''_downservice.Endpoint.Address = New EndpointAddress("http://" & _wsurl & "/(S(" & _sessionid & "))/assetdownloadservice.asmx")


        ' ''_downservice.Login("jburlinson", WebArchives.iDAM.WebCommon.Security.Encrypt("Kimberly777"), "IDAM_WEBARCHIVES", True)
        ' ''Dim _status As Boolean = _downservice.GetLoginStatus
        ' ''Dim _ticket As String = _downservice.RequestAssetByDownloadID("IDAM_WEBARCHIVES", 30149910, 30059017)


        Dim cachepathname As String = cachepath
        SaveUrlToPath(cachepathname, downloadURL)
    End Function




    Public Shared Sub CopyStream(ByRef source As Stream, ByRef target As Stream)
        Dim buffer As Byte() = New Byte(65535) {}
        If source.CanSeek Then
            source.Seek(0, SeekOrigin.Begin)
        End If
        Dim bytes As Integer = source.Read(buffer, 0, buffer.Length)
        Try
            While (bytes > 0)

                target.Write(buffer, 0, bytes)
                bytes = source.Read(buffer, 0, buffer.Length)
            End While
        Finally
            ' Or target.Close(); if you're done here already.
            target.Flush()
        End Try
    End Sub



    Public Shared Sub SaveUrlToPath(ByVal sPath As String, ByVal sURL As String)

        Dim wc As Net.WebClient
        Dim stream As System.IO.Stream
        Try
            Dim t As Date = Now
            wc = New Net.WebClient
            'ping first to prevent a major bug on downloadfile call. If there's a 404, sPath will be deleted
            stream = wc.OpenRead(sURL)
            Dim tempPath As String = sPath
            SaveStreamToFilePath(tempPath, stream)

        Catch ex As Exception

        Finally
            If Not wc Is Nothing Then
                If Not stream Is Nothing Then
                    stream.Close()
                End If
                wc.Dispose()
            End If
        End Try
    End Sub


    Public Shared Sub SaveStreamToFilePath(ByVal sFilePath As String, ByRef istream As Stream)
        Dim outputStream As New FileStream(sFilePath, FileMode.Create)

        Try
            CopyStream(istream, outputStream)
        Finally
            outputStream.Close()
            istream.Close()
        End Try
    End Sub



    Public Shared Sub ExecuteTransaction(ByVal sqlstring As String)
        Dim ConnectionString As String = System.Configuration.ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
        Dim connection As New OleDbConnection(ConnectionString)
        Dim command As New OleDbCommand
        Dim transaction As OleDbTransaction

        ' Set the Connection to the new OleDbConnection.
        command.Connection = connection

        ' Open the connection and execute the transaction.
        Try
            connection.Open()

            ' Start a local transaction with ReadCommitted isolation level.
            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            ' Assign transaction object for a pending local transaction.
            command.Connection = connection
            command.Transaction = transaction

            ' Execute the commands.
            command.CommandText = sqlstring
            command.ExecuteNonQuery()

            ' Commit the transaction.
            transaction.Commit()
            Console.WriteLine("Executed.")

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Try to rollback the transaction
            Try
                transaction.Rollback()

            Catch
                ' Do nothing here; transaction is not active.
            End Try
        End Try
        ' The connection is automatically closed when the
        ' code exits the Using block.

    End Sub

    Public Shared Function GetDataTable(ByVal query As String, ByVal ConnectionString As String) As DataTable
        'Dim connection1 As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & System.Web.HttpContext.Current.Server.MapPath(dbFile))
        Dim connection1 As OleDbConnection = New OleDbConnection(ConnectionString)
        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)
        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()

        End Try
        Return table1
    End Function
    Public Shared Function isullcheck(ByVal v)
        If v Is System.DBNull.Value Then
            isullcheck = ""
        Else
            isullcheck = v
        End If
    End Function

    
    Public Shared Function RewriteURLWithSessionID(ByVal sURL As String, ByVal sSessionID As String, Optional ByVal useNet20Format As Boolean = False) As String
        Dim uri As New Uri(sURL)
        Dim Root As String = uri.AbsoluteUri.Replace(uri.PathAndQuery, "") + uri.PathAndQuery.Substring(0, InStrRev(uri.PathAndQuery, "/"))
        If sSessionID <> "" Then

            If useNet20Format Then
                Return Root + "(S(" + sSessionID + "))/" + uri.AbsoluteUri.Replace(Root, "")
            Else
                Return Root + "(" + sSessionID + ")/" + uri.AbsoluteUri.Replace(Root, "")

            End If
        Else
            Return sURL
        End If

    End Function

    Public Shared Function GetSessionID(ByVal URL As String) As String
        Try
            Dim clientreq As HttpWebRequest
            clientreq = HttpWebRequest.Create(URL)
            clientreq.Timeout = CType(10000, Integer)
            Dim clientres As HttpWebResponse = clientreq.GetResponse
            Dim data As IO.Stream = clientres.GetResponseStream
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            If str.IndexOf("SESSIONID") >= 0 Then
                Dim strtidx As Integer = str.IndexOf("SESSIONID=") + 10
                Return str.Substring(strtidx, str.IndexOf("</span>") - strtidx)
            Else
                Throw New Exception("No session ID found. check URL if it has the proper format" + URL)
            End If
        Catch ex As Exception
            Throw New Exception("WebService not available." + URL)
        End Try
    End Function
End Class


Public Class ClientLoginRequestURLContract

    Public Const LOGIN As String = "txtLogin"
    Public Const PASSWORD As String = "txtPassword"
    Public Const INSTANCEID As String = "txtInstanceID"
    Public Const DIRECT As String = "direct"
    Public Const ENCRYPTED As String = "encrypted"
    Public Const AUTHENTICATIONLEVEL As String = "level"
    Public Const SECUREMODE As String = "secure"
    Public Const LDAPDOMAIN As String = "ld"
    Public Const LDAPPATH As String = "ldp"

    Public Shared Function GetLoginRequestURL(ByVal pLocation As String, ByVal pLogin As String, ByVal pPassword As String, ByVal pInstanceID As String, ByVal pSecureMode As Boolean, Optional ByVal pEncrypted As Boolean = False, Optional ByVal pDirectMode As Boolean = False, Optional ByVal pAuthenticationLevel As Integer = 0, Optional ByVal pLDAPDomain As String = "", Optional ByVal pLDAPPath As String = "") As String
        If pLocation Is Nothing Then
            Throw New ArgumentException("location cannot be null")
        End If
        If pLogin Is Nothing Then
            Throw New ArgumentException("login cannot be null")
        End If
        If pInstanceID Is Nothing Then
            Throw New ArgumentException("pInstanceID cannot be null")
        End If
        If pPassword Is Nothing Then
            Throw New ArgumentException("pPassword cannot be null")
        End If

        Dim url As New Text.StringBuilder
        url.Append(pLocation)
        url.Append("?")
        url.Append(LOGIN).Append("=").Append(pLogin).Append("&")
        url.Append(PASSWORD).Append("=").Append(pPassword).Append("&")
        url.Append(INSTANCEID).Append("=").Append(pInstanceID).Append("&")
        url.Append(DIRECT).Append("=").Append(pDirectMode.ToString).Append("&")
        url.Append(ENCRYPTED).Append("=").Append(pEncrypted.ToString).Append("&")
        url.Append(AUTHENTICATIONLEVEL).Append("=").Append(pAuthenticationLevel.ToString).Append("&")
        url.Append(SECUREMODE).Append("=").Append(pSecureMode.ToString).Append("&")
        If pLDAPDomain <> "" Then
            url.Append(LDAPDOMAIN).Append("=").Append(pLDAPDomain.ToString).Append("&")
        End If
        If pLDAPPath <> "" Then
            url.Append(LDAPPATH).Append("=").Append(pLDAPPath.ToString)
        End If


        Return url.ToString

    End Function
End Class

