﻿Imports System.Data.OleDb
Imports System.Configuration
Imports System.IO
Imports System.Text.RegularExpressions
Public Class Service1

    Public IDAMROOT As String = ConfigurationSettings.AppSettings("IDAM.IDAMROOT")
    Public PEROOT As String = ConfigurationSettings.AppSettings("IDAM.PEROOT")
    Public ConnectionStringIDAM As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
    Public ConnectionStringVISION As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringVISION")
    Public ConnectionStringEXTENSIS As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringEXTENSIS")
    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.


        'get files to migrate
        'select a.asset_id,a.name,d.name projectname from IPM_ASSET a,ipm_asset_field_value b, ipm_asset_field_desc c, ipm_project d where a.asset_id = b.asset_id and c.item_id = b.Item_ID and c.Item_Tag = 'IDAM_FILE_SYSTEM_PUSH_REPLICATION' and b.item_value = 1 and a.proc_status = 0 and a.Available = 'Y' and a.ProjectID = d.projectid


        Dim IDAM_FILE_SYSTEM_NETWORK_LOCATION_ID As String = GetDataTable("select item_id from ipm_asset_field_desc where item_tag = 'IDAM_FILE_SYSTEM_NETWORK_LOCATION'", ConnectionStringIDAM).Rows(0)("item_id")
        Dim IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID As String = GetDataTable("select item_id from ipm_asset_field_desc where item_tag = 'IDAM_FILE_SYSTEM_INFORMATION_STATUS'", ConnectionStringIDAM).Rows(0)("item_id")
        Dim IDAM_FILE_SYSTEM_PUSH_REPLICATION_ID As String = GetDataTable("select item_id from ipm_asset_field_desc where item_tag = 'IDAM_FILE_SYSTEM_PUSH_REPLICATION'", ConnectionStringIDAM).Rows(0)("item_id")

        Dim currentasset_id As String = ""
        Dim currentfilepath As String = ""
        Do While 1 = 1
            Try
                currentasset_id = ""
                currentfilepath = ""
                System.Threading.Thread.Sleep(10000)
                'preprocess
                'IDAM_FILE_SYSTEM_NETWORK_LOCATION()
                'IDAM_FILE_SYSTEM_INFORMATION_STATUS()
                Dim AllAssets As DataTable = GetDataTable("select a.asset_id,a.filename,a.name,d.name projectname from IPM_ASSET a,ipm_asset_field_value b, ipm_asset_field_desc c, ipm_project d where a.asset_id = b.asset_id and c.item_id = b.Item_ID and c.Item_Tag = 'IDAM_FILE_SYSTEM_PUSH_REPLICATION' and b.item_value = 1 and a.proc_status = 0 and a.Available = 'Y' and a.ProjectID = d.projectid", ConnectionStringIDAM)  'and oid = '041018.00'

                For Each row As DataRow In AllAssets.Rows
                    currentasset_id = row("asset_id")
                    Dim filename As String = SafeOSName(row("filename"))
                    Dim filenamedestination As String = Regex.Replace(filename, ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONFILENAMEREPLACESTRING"), ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONFILENAMEREPLACEWITHSTRING"))
                    Dim filepath As String = ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONLOCATION") & SafeOSName(row("projectname")) & "\" & filenamedestination
                    'use legacy path?
                    If GetDataTable("select item_value from IPM_ASSET_FIELD_VALUE a, IPM_ASSET_FIELD_DESC b where a.Item_ID = b.Item_ID and b.Item_Tag = 'IDAM_FILE_SYSTEM_OVERRIDE_PATH' and a.ASSET_ID = '" & row("asset_id") & "'", ConnectionStringIDAM).Rows.Count > 0 Then
                        Dim item_value = GetDataTable("select item_value from IPM_ASSET_FIELD_VALUE a, IPM_ASSET_FIELD_DESC b where a.Item_ID = b.Item_ID and b.Item_Tag = 'IDAM_FILE_SYSTEM_OVERRIDE_PATH' and a.ASSET_ID = '" & row("asset_id") & "'", ConnectionStringIDAM).Rows(0)("item_value").ToString.Trim
                        If item_value <> "" Then
                            filepath = ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONLOCATION") & item_value.Replace(ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONPATHSCRUB"), "")
                            filepath = Regex.Replace(filepath, ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONFILENAMEREPLACESTRING"), ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONFILENAMEREPLACEWITHSTRING"))

                        End If
                    End If
                    'change extension
                    filepath = Regex.Replace(filepath, Path.GetExtension(filepath), ".jpg")
                    currentfilepath = filepath
                    'check to see if dir exists
                    If Not Directory.Exists(Path.GetDirectoryName(filepath)) Then
                        Directory.CreateDirectory(Path.GetDirectoryName(filepath))
                    End If
                    'check for file
                    If Not File.Exists(filepath) Then
                        'execute copy
                        getPreviewImageFromIDAM(row("asset_id"), ConfigurationSettings.AppSettings("IDAM.IDAMINSTANCE"), ConfigurationSettings.AppSettings("IDAM.IDAMBROWSEURL"), filepath, "")
                    End If
                    'confirm file got there
                    'CODE0 (Not Replicated)
                    'CODE1 (Replicated)
                    'CODE2 (ERROR)
                    If File.Exists(filepath) Then
                        'update status and network location - SUCCESS
                        ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_NETWORK_LOCATION_ID & " and asset_id = " & row("asset_id"))
                        ExecuteTransaction("insert into ipm_asset_field_value (item_id,asset_id,item_value) values (" & IDAM_FILE_SYSTEM_NETWORK_LOCATION_ID & "," & row("asset_id") & ",'" & filepath & "')")
                        ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID & " and asset_id = " & row("asset_id"))
                        ExecuteTransaction("insert into ipm_asset_field_value (item_id,asset_id,item_value) values (" & IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID & "," & row("asset_id") & ",'CODE1 (Replicated)')")
                    Else
                        'update status and network location - FAILURE
                        ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_NETWORK_LOCATION_ID & " and asset_id = " & row("asset_id"))
                        ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID & " and asset_id = " & row("asset_id"))
                        ExecuteTransaction("insert into ipm_asset_field_value (item_id,asset_id,item_value) values (" & IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID & "," & row("asset_id") & ",'CODE2 (ERROR)')")
                    End If
                    'change checkbox so doesnt process again
                    ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_PUSH_REPLICATION_ID & " and asset_id = " & row("asset_id"))
                Next
            Catch ex As Exception
                'change checkbox so doesnt process again
                ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_PUSH_REPLICATION_ID & " and asset_id = " & currentasset_id)
                'update status and network location - FAILURE
                ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_NETWORK_LOCATION_ID & " and asset_id = " & currentasset_id)
                ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID & " and asset_id = " & currentasset_id)
                ExecuteTransaction("insert into ipm_asset_field_value (item_id,asset_id,item_value) values (" & IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID & "," & currentasset_id & ",'CODE2 (ERROR)')")
            End Try
        Loop
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub






    Public Function SafeOSName(ByVal nom As String) As String

        'First Trim the raw string
        Dim safe As String = nom.Trim

        'Replace spaces with hyphens
        'safe = safe.Replace(" ", "-").ToLower()

        '' ''Replace double spaces with singles
        ' ''If safe.IndexOf("--") > 1 Then
        ' ''    While (safe.IndexOf("--") > -1)
        ' ''        safe = safe.Replace("--", "-")
        ' ''    End While
        ' ''End If

        'Trim out illegal characters
        '"\"M\"\\a/ry/ h**ad:>> a\\/:*?\"| li*tt|le|| la\"mb.?"
        safe = Regex.Replace(safe, "\/:*?""<>|", "")

        'Trim Length
        If safe.Length > 250 Then
            safe = safe.Substring(0, 249)
        End If

        ''Clean the beginning and end of filename
        'Dim replace As Char() = {"-", "."}
        'safe = safe.TrimStart(replace)
        'safe = safe.TrimEnd(replace)

        Return safe

    End Function




    Function getPreviewImageFromIDAM(ByVal AssetId, ByVal instanceid, ByVal browseURL, ByVal cachepath, ByVal page) As String
        If page = "" Then
            page = "0"
        End If
        'RetrieveAsset.aspx?id=30056115&instance=IDAM_WEBARCHIVES&type=asset&size=1&cache=1
        Dim downloadURL As String = browseURL & "?id=" & AssetId & "&instance=" & instanceid & "&type=asset&size=1&width=4000&height=4000&cache=1&page=" & page
        Dim cachepathname As String = cachepath
        SaveUrlToPath(cachepathname, downloadURL)
    End Function





    Sub CopyStream(ByRef source As Stream, ByRef target As Stream)
        Dim buffer As Byte() = New Byte(65535) {}
        If source.CanSeek Then
            source.Seek(0, SeekOrigin.Begin)
        End If
        Dim bytes As Integer = source.Read(buffer, 0, buffer.Length)
        Try
            While (bytes > 0)

                target.Write(buffer, 0, bytes)
                bytes = source.Read(buffer, 0, buffer.Length)
            End While
        Finally
            ' Or target.Close(); if you're done here already.
            target.Flush()
        End Try
    End Sub



    Sub SaveUrlToPath(ByVal sPath As String, ByVal sURL As String)

        Dim wc As Net.WebClient
        Dim stream As System.IO.Stream
        Try
            Dim t As Date = Now
            wc = New Net.WebClient
            'ping first to prevent a major bug on downloadfile call. If there's a 404, sPath will be deleted
            stream = wc.OpenRead(sURL)
            Dim tempPath As String = sPath
            SaveStreamToFilePath(tempPath, stream)

        Catch ex As Exception

        Finally
            If Not wc Is Nothing Then
                If Not stream Is Nothing Then
                    stream.Close()
                End If
                wc.Dispose()
            End If
        End Try
    End Sub


    Sub SaveStreamToFilePath(ByVal sFilePath As String, ByRef istream As Stream)
        Dim outputStream As New FileStream(sFilePath, FileMode.Create)

        Try
            CopyStream(istream, outputStream)
        Finally
            outputStream.Close()
            istream.Close()
        End Try
    End Sub



    Public Shared Sub ExecuteTransaction(ByVal sqlstring As String)
        Dim ConnectionString As String = System.Configuration.ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
        Dim connection As New OleDbConnection(ConnectionString)
        Dim command As New OleDbCommand
        Dim transaction As OleDbTransaction

        ' Set the Connection to the new OleDbConnection.
        command.Connection = connection

        ' Open the connection and execute the transaction.
        Try
            connection.Open()

            ' Start a local transaction with ReadCommitted isolation level.
            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            ' Assign transaction object for a pending local transaction.
            command.Connection = connection
            command.Transaction = transaction

            ' Execute the commands.
            command.CommandText = sqlstring
            command.ExecuteNonQuery()

            ' Commit the transaction.
            transaction.Commit()
            Console.WriteLine("Executed.")

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Try to rollback the transaction
            Try
                transaction.Rollback()

            Catch
                ' Do nothing here; transaction is not active.
            End Try
        End Try
        ' The connection is automatically closed when the
        ' code exits the Using block.

    End Sub

    Public Shared Function GetDataTable(ByVal query As String, ByVal ConnectionString As String) As DataTable
        'Dim connection1 As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & System.Web.HttpContext.Current.Server.MapPath(dbFile))
        Dim connection1 As OleDbConnection = New OleDbConnection(ConnectionString)
        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)
        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()

        End Try
        Return table1
    End Function
    Function isullcheck(ByVal v)
        If v Is System.DBNull.Value Then
            isullcheck = ""
        Else
            isullcheck = v
        End If
    End Function

End Class
