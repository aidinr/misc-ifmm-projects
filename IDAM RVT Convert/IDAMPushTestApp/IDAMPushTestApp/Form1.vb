﻿Imports IDAMMonitor


Public Class Form1

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim idamrefresh As Integer = 10000
        If Configuration.ConfigurationSettings.AppSettings("IDAM.REFRESH") <> "" Then
            idamrefresh = Configuration.ConfigurationSettings.AppSettings("IDAM.REFRESH")
        End If
        IDAMPushMain.iDAMPushMainFunction(idamrefresh)
    End Sub
End Class
