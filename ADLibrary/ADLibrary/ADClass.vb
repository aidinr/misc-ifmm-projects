﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Serialization
'Imports log4net
Imports System.Net.Mail
Imports System.Data.OleDb
Imports System
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.DirectoryServices
Imports System.Security
Imports System.Security.Principal
Imports System.Threading
Imports System.Globalization
Imports System.Text.StringBuilder

Public Class ADClass

    '---damjan need

    Public Shared Function GetUsersinGroup(ByVal UserName As String, ByVal Password As String, ByVal ADFullPath As String, ByVal AttributeName As String, ByVal AttributeValue As String, ByVal FirstName As String, LastName As String) As DataSet
        Dim dsUsers As New DataSet()
        Dim dirEntry As DirectoryEntry = New DirectoryEntry(ADFullPath, UserName, Password, AuthenticationTypes.Secure)

        'create instance fo the direcory searcher
        Dim dirSearch As New DirectorySearcher()

        'set the search filter
        dirSearch.SearchRoot = dirEntry
        dirSearch.SearchScope = SearchScope.Subtree
        'deSearch.PropertiesToLoad.Add("cn");
        dirSearch.Filter = "(&(objectClass=person)(" & AttributeName & "=" + AttributeValue + "))"

        'get the group result
        Dim searchResults As SearchResultCollection = dirSearch.FindAll()

        'Create a new table object within the dataset
        Dim dtUser As DataTable = dsUsers.Tables.Add("Users")
        dtUser.Columns.Add("UserName")
        dtUser.Columns.Add("DisplayName")
        dtUser.Columns.Add("EMailAddress")

        'Create default row
        Dim drUser As DataRow = dtUser.NewRow()
        drUser("UserName") = "0"
        drUser("DisplayName") = "(Not Specified)"
        drUser("EMailAddress") = "(Not Specified)"
        dtUser.Rows.Add(drUser)

        'if the group is valid, then continue, otherwise return a blank dataset
        If Not searchResults Is Nothing Then
            For Each item As SearchResult In searchResults
                Dim rwUser As DataRow = dtUser.NewRow()

                'populate the column
                Try

                    rwUser("UserName") = item.Properties("cn")(0).ToString ' GetProperty(deUser, "cn")

                    Try
                        rwUser("DisplayName") = item.Properties("displayName")(0).ToString 'GetProperty(deUser, "displayName")
                    Catch ex As Exception
                        rwUser("DisplayName") = ""

                    End Try
                    Try
                        rwUser("EMailAddress") = item.Properties("mail")(0).ToString ' GetProperty(deUser, "mail")
                    Catch ex As Exception
                        rwUser("EMailAddress") = ""
                    End Try
                    'append the row to the table of the dataset
                    If FirstName = "" And LastName = "" Then
                        dtUser.Rows.Add(rwUser)
                    Else
                        If (FirstName <> "") Then
                            If rwUser("DisplayName").ToString().Contains(FirstName) Then
                                If LastName = "" Then
                                    dtUser.Rows.Add(rwUser)
                                ElseIf (rwUser("DisplayName").ToString().Contains(LastName)) Then
                                    dtUser.Rows.Add(rwUser)
                                End If
                            End If
                        End If

                        If (LastName <> "") Then
                            If (rwUser("DisplayName").ToString().Contains(LastName)) Then
                                dtUser.Rows.Add(rwUser)
                            End If
                        End If

                    End If

                Catch ex As Exception
                End Try
            Next

        End If

        '' ----------------------------- UNIVERSAL GROUP USER SEARCH ----------


        ''create instance fo the direcory searcher
        'dirSearch = New DirectorySearcher()
        'dirSearch.SearchScope = SearchScope.Subtree
        ''set the search filter
        'dirSearch.SearchRoot = dirEntry
        ''deSearch.PropertiesToLoad.Add("cn");
        'dirSearch.Filter = "(&(objectClass=group)(groupType:1.2.840.113556.1.4.803:=8)(cn=" + GroupName + "))"

        ''get the group result
        'searchResults = dirSearch.FindOne()

        ''Create a new table object within the dataset
        ''Dim dtUser As DataTable = dsUsers.Tables.Add("Users")
        ''dtUser.Columns.Add("UserName")
        ''dtUser.Columns.Add("DisplayName")
        ''dtUser.Columns.Add("EMailAddress")

        ' ''Create default row
        ''Dim drUser As DataRow = dtUser.NewRow()
        ''drUser("UserName") = "0"
        ''drUser("DisplayName") = "(Not Specified)"
        ''drUser("EMailAddress") = "(Not Specified)"
        ''dtUser.Rows.Add(drUser)

        ''if the group is valid, then continue, otherwise return a blank dataset
        'If Not searchResults Is Nothing Then
        '    'create a link to the group object, so we can get the list of members
        '    'within the group
        '    Dim dirGroup As New DirectoryEntry(searchResults.Path, ADAdminUser, ADAdminPassword, AuthenticationTypes.Secure)
        '    'assign a property collection
        '    Dim propCollection As System.DirectoryServices.PropertyCollection = dirGroup.Properties
        '    Dim n As Integer = propCollection("member").Count
        '    For l As Integer = 0 To n - 1

        '        'if there are members fo the group, then get the details and assign to the table
        '        'create a link to the user object sot hat the FirstName, LastName and SUername can be gotten
        '        Dim deUser As New DirectoryEntry(ADServer + "/" + propCollection("member")(l).ToString(), ADAdminUser, ADAdminPassword, AuthenticationTypes.Secure)
        '        'Dim deUser As DirectoryEntry = GetDirectoryEntry(ADServer + "/" + propCollection("member")(l).ToString())

        '        'set a new empty row
        '        Dim rwUser As DataRow = dtUser.NewRow()

        '        'populate the column
        '        If GetProperty(deUser, "SamAccounttype") = "805306368" Then ' user record
        '            rwUser("UserName") = GetProperty(deUser, "cn")
        '            rwUser("DisplayName") = GetProperty(deUser, "displayName")
        '            rwUser("EMailAddress") = GetProperty(deUser, "mail")
        '            'append the row to the table of the dataset
        '            dtUser.Rows.Add(rwUser)
        '        End If
        '        'close the directory entry object

        '        deUser.Close()
        '    Next
        '    dirGroup.Close()
        'End If


        dirEntry.Close()
        dtUser.Rows.RemoveAt(0)
        Return dsUsers
    End Function


    Public Shared Function LoginUser(ByVal UserName As String, ByVal Password As String, ByVal ADFullPath As String) As Boolean
        'create an instance of the DirectoryEntry
        Dim dirEntry As DirectoryEntry

        dirEntry = New DirectoryEntry(ADFullPath, UserName, Password, AuthenticationTypes.Secure)


        'create instance fo the direcory searcher
        Dim dirSearch As New DirectorySearcher(dirEntry)

        dirSearch.SearchRoot = dirEntry
        'set the search filter
        dirSearch.Filter = "(&(objectCategory=user)(cn=" + UserName + "))"
        'deSearch.SearchScope = SearchScope.Subtree;

        'find the first instance
        Dim searchResults As SearchResult
        Try
            searchResults = dirSearch.FindOne()
        Catch ex As Exception
            Return False

        End Try
        Return True
    End Function

    'Private Sub btnLogin_Click(sender As System.Object, e As System.EventArgs) Handles btnLogin.Click
    '    If LoginUser(txtUsername.Text, txtPassword.Text, txtADFullPath.Text) = True Then
    '        MsgBox("Logged in")
    '    Else
    '        MsgBox("Not Logged in")
    '    End If
    'End Sub
    'Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
    '    GetUsersinGroup(txtUsername.Text, txtPassword.Text, txtADFullPath.Text, "badPwdCount", "0", "", "")
    'End Sub



End Class
