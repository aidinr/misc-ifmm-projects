﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports WebArchives.iDAM.WebCommon


Imports Microsoft.VisualBasic


Partial Class client_login
    Inherits System.Web.UI.Page

   

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
       

    End Sub


    Public Shared Function ScrubSQLInput(ByVal sInput As String, Optional ByVal removequotes As Boolean = True) As String
        If removequotes Then
            sInput = sInput.Replace("'", "")
        End If
        ScrubSQLInput = sInput.Replace(";", "").Replace("--", "").Replace("/*", "").Replace("*/", "").Replace("Xp_", "").Replace("[", "[[]").Replace("%", "[%]").Replace("¬", "").Replace("&#172", "-").Replace("&#173", "-").Replace("&#174", "-")
    End Function


    Public Shared Function checkNulltoString(ByVal sfield As Object)
        Try
            If sfield Is System.DBNull.Value Or sfield Is Nothing Then
                checkNulltoString = ""
            Else
                checkNulltoString = sfield
            End If
        Catch ex As Exception

            checkNulltoString = ""
        End Try
    End Function

    Protected Sub Page_AbortTransaction(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AbortTransaction

    End Sub

    Private Sub handleCookie(ByVal AutoLogin As Boolean)
        If AutoLogin Then

            If GetCookie("Password") = "" And Login_Name.Text.Trim <> "" Then
                SetCookie("Password", WebArchives.iDAM.WebCommon.Security.Encrypt(Login_Password.Text.Trim))
                SetCookie("Login", Login_Name.Text)

            End If
        Else
            If Not Me.rememberme.Checked Then
                SetCookie("RememberMe", "")
                SetCookie("Login", "")
                SetCookie("Password", "")
            Else
                SetCookie("RememberMe", "ON")
                SetCookie("Password", WebArchives.iDAM.WebCommon.Security.Encrypt(Login_Password.Text.Trim))
                SetCookie("Login", Login_Name.Text)

            End If

        End If
    End Sub


    Private Sub Login(ByVal pUserName As String, ByVal pPassword As String, ByVal pEncrypted As Boolean, Optional ByVal pConfigId As String = Nothing, Optional ByVal AutoLogin As Boolean = False)
        'retrieve the selected index config id
       
        'retrieve the config service for the config id
        'init the webadminconfig
        Dim Redirect_Loc As String = ""
        If Request.QueryString("r") <> "" Then
            Redirect_Loc = Server.UrlEncode(Request.QueryString("r")).ToString()
        ElseIf Request.QueryString("p") <> "" Then
            Redirect_Loc = Server.UrlEncode(Request.QueryString("p")).ToString()
        End If


        Dim loginSuccess As Boolean = False
        Try

            loginSuccess = LoginToIdam(pUserName, pPassword, pEncrypted)

            If GetCookie("RememberMe") = "ON" Then
                handleCookie(True)
            Else
                handleCookie(False)
            End If

        Catch ex As Exception

            SetCookie("RememberMe", "")
        End Try

        If loginSuccess Then
            Dim tmpLoginPrevious As String = ""
            tmpLoginPrevious = GetCookie("LoginPrevious")
            SetCookie("LoginPrevious", pUserName)
            Response.Redirect("client_home.aspx")
        End If
        'set for next login check

    End Sub


    Function LoginToIdam(ByVal slogin As String, ByVal spassword As String, ByVal encrypt As Boolean)

        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        If spassword = "" Then Exit Function
        If encrypt Then
            spassword = WebArchives.iDAM.WebCommon.Security.Decrypt(spassword)
        End If

        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select a.* from ipm_user a, ipm_user_field_value b where a.login = @LoginID and a.password = @PasswordID and a.active = 'Y' and a.userid = b.user_id and b.item_id = 2405233 and b.item_value = 1", MyConnection)

        Dim loginid As New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(loginid)
        MyCommand1.SelectCommand.Parameters("@LoginID").Value = ScrubSQLInput(slogin)

        Dim passwordid As New SqlParameter("@PasswordID", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(passwordid)
        MyCommand1.SelectCommand.Parameters("@PasswordID").Value = ScrubSQLInput(spassword)

        Dim DT1 As New DataTable("Login")

        MyCommand1.Fill(DT1)


        If DT1.Rows.Count = 1 Then
            Session("logged_in") = "1"
            Session("userid") = DT1.Rows(0)("userid")
            Session("userfirstname") = checkNulltoString(DT1.Rows(0)("firstname"))
            Session("userlastname") = checkNulltoString(DT1.Rows(0)("lastname"))
            Session("usercompany") = checkNulltoString(DT1.Rows(0)("agency"))
            Session("useremail") = checkNulltoString(DT1.Rows(0)("email"))


            Session("userfax") = checkNulltoString(DT1.Rows(0)("fax"))
            Session("userphone") = checkNulltoString(DT1.Rows(0)("phone"))
            Session("useraddress") = checkNulltoString(DT1.Rows(0)("workaddress"))
            Session("usertitle") = checkNulltoString(DT1.Rows(0)("position"))
            Session("usercity") = checkNulltoString(DT1.Rows(0)("workcity"))
            Session("userstate") = checkNulltoString(DT1.Rows(0)("workstate"))
            Session("userzip") = checkNulltoString(DT1.Rows(0)("workzip"))



            Response.Redirect("client_home.aspx")
        Else
            SetCookie("RememberMe", "")
            ltrErrorMessage.Text = "Error logging in.  Please try again."
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.QueryString("logout") = "1" Then
            Session("logged_in") = "0"
            SetCookie("RememberMe", "")
        End If

        Login_Name.Attributes.Add("onkeydown", "javascript:if(event.keyCode==13) {window.location = 'IDAM.aspx?page=Results&Type=QuickSearch&keyword=' + document.PageForm.quicksearchkeyword.value; return false}")
        Login_Password.Attributes.Add("onkeydown", "javascript:if(event.keyCode==13) {window.location = 'IDAM.aspx?page=Results&Type=QuickSearch&keyword=' + document.PageForm.quicksearchkeyword.value; return false}")

        If IsPostBack Then

            'save cookie
            If GetCookie("RememberMe") = "ON" Then
                handleCookie(True)
            Else
                handleCookie(false)
            End If


            If GetCookie("RememberMe") = "ON" Then
                HandleAutoLogin(True)
            End If



            LoginToIdam(ScrubSQLInput(Login_Name.Text).Trim, ScrubSQLInput(Login_Password.Text), False)
        Else
            If GetCookie("RememberMe") = "ON" And Request.QueryString("logout") <> "1" Then
                HandleAutoLogin(False)
            End If

        End If
    End Sub



    Private Sub HandleAutoLogin(ByVal formValues As Boolean)
        Dim configId As String = "0"
       

        If formValues Then
            SetCookie("Password", ScrubSQLInput(Login_Password.Text).Trim)
            SetCookie("Login", ScrubSQLInput(Login_Name.Text))
            If rememberme.Checked Then
                SetCookie("RememberMe", "ON")
            Else
                SetCookie("RememberMe", "")
            End If
        End If

        If (GetCookie("RememberMe") = "ON" And GetCookie("Login") <> "") Or (Me.rememberme.Checked And formValues) Then

            LoginToIdam(GetCookie("Login"), GetCookie("Password"), True)
        
        End If
    End Sub


    Public Shared Function ClearCookie(ByVal cookieName As String)
        Dim rBase As HttpApplication = HttpContext.Current.ApplicationInstance
        If Not (rBase.Request.Cookies(cookieName)) Is Nothing And cookieName <> "ASP.NET_SessionId" And cookieName <> "Login" And cookieName <> "Password" Then
            rBase.Response.Cookies(cookieName).Value = ""
            rBase.Response.Cookies(cookieName).Expires = DateTime.Now.AddDays(-30)
            GetCookie(cookieName)
            CreateNewCookie(cookieName, "")
            Dim c As HttpCookie
            c = rBase.Request.Cookies(cookieName)
            c.Value = ""
            c.Expires = DateTime.Now.AddYears(-30)
            rBase.Response.Cookies.Add(c)
        End If
    End Function



    Public Shared Function CreateNewCookie(ByVal cookieName As String, ByVal Value As String)
        Dim rBase As HttpApplication = HttpContext.Current.ApplicationInstance
        Dim cookie As HttpCookie = New HttpCookie(cookieName)
        cookie.Value = Value
        cookie.Expires = DateTime.Now.AddYears(30)
        rBase.Response.Cookies.Add(cookie)

    End Function


    Public Shared Function GetCookie(ByVal cookieName As String) As String
        Dim rBase As HttpApplication = HttpContext.Current.ApplicationInstance
        Try
            GetCookie = rBase.Request.Cookies(cookieName).Value
        Catch ex As Exception
            GetCookie = ""
        End Try
    End Function


    Public Shared Function SetCookie(ByVal cookieName As String, ByVal Value As String)
        Dim rBase As HttpApplication = HttpContext.Current.ApplicationInstance
        If Not (rBase.Request.Cookies(cookieName)) Is Nothing Then
            Dim Cookie As New HttpCookie(cookieName)
            Cookie.Value = Value
            Cookie.Expires = DateTime.Now.AddYears(30)
            rBase.Response.Cookies.Add(Cookie)
        Else
            CreateNewCookie(cookieName, Value)
        End If
    End Function


    Public Function checkCookie(ByVal cookieID As String)
        Dim rBase As HttpApplication = HttpContext.Current.ApplicationInstance
        If Not rBase.Request.Cookies(cookieID) Is Nothing Then
            checkCookie = rBase.Request.Cookies(cookieID).Value.ToString
        Else
            checkCookie = ""
        End If
    End Function


End Class
