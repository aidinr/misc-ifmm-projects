﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.Xml.XPath
Imports System.Net
Imports System.IO


Partial Class information_request
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim successString As String = Request.QueryString("success")
        Dim errorString As String = Request.QueryString("errors")
        If (successString = "success") Then
            labelSuccess.Text = "Thank you for your request. Your submission has been successfully submitted.<br /><br />"
        ElseIf (successString <> Nothing) Then

            errorString = errorString.Replace("-", "<br />")
            labelSuccess.Text = "A technical error has occured. Please try your request again:<br /><br />" & errorString & "<br /><br />"
        End If




        'Dim ws = New acadiaws.Service1

        'Dim ds As DataSet = ws.GetT1Feed("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=informationrequestconfig")

        'ds.WriteXml("C:\projects\Acadia .NET\xml\inforequestconfig.xml")

        'Dim ds2 As DataSet = ws.GetT1Feed("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=informationrequest")

        'ds2.WriteXml("C:\projects\Acadia .NET\xml\inforequest.xml")

    End Sub

    Sub imageButtonSubmit_click(ByVal sender As Object, ByVal e As EventArgs) Handles imageButtonSubmit.Click

        'Response.Redirect("Properties.aspx?search=search&state=" & searchState.SelectedValue & "&string=" & searchString.Text)
        Dim requestAR As Boolean = checkBoxReport.Checked
        Dim questions As Boolean = checkBoxQuestion.Checked

        Dim firstName As String = textboxFirstName.Text
        Dim lastname As String = textboxLastName.Text
        Dim title As String = textboxTitle.Text
        Dim organization As String = textboxOrganization.Text
        Dim investortype As String = formInvestorType.SelectedValue
        Dim address1 As String = textboxAddress1.Text
        Dim address2 As String = textboxAddress2.Text
        Dim city As String = textboxCity.Text
        Dim state As String = formState.SelectedValue
        Dim zip As String = textboxZip.Text
        Dim country As String = formCountry.SelectedValue
        Dim phone As String = textboxPhone.Text
        Dim fax As String = textboxFax.Text
        Dim email As String = textboxEmail.Text
        Dim comment As String = textboxComments.Text


        Dim theXML As String = "<inforequest ID=""" & ConfigurationSettings.AppSettings("compID") & """><UserData>"
        theXML = theXML & "<fname>" & firstName & "</fname>"
        theXML = theXML & "<lname>" & lastname & "</lname>"
        theXML = theXML & "<title>" & title & "</title>"
        theXML = theXML & "<institution>" & organization & "</institution>"
        theXML = theXML & "<investor_type>" & investortype & "</investor_type>"
        theXML = theXML & "<addr1>" & address1 & "</addr1>"
        theXML = theXML & "<addr2>" & address2 & "</addr2>"
        theXML = theXML & "<city>" & city & "</city>"
        theXML = theXML & "<state>" & state & "</state>"
        theXML = theXML & "<zip>" & zip & "</zip>"
        theXML = theXML & "<country>" & country & "</country>"
        theXML = theXML & "<fax>" & phone & "</fax>"
        theXML = theXML & "<email>" & fax & "</email>"
        theXML = theXML & "<lname>" & email & "</lname>"
        theXML = theXML & "<comment>" & comment & "</comment>"
        theXML = theXML & "</UserData>"

        If (requestAR Or questions) Then
            theXML = theXML & "<Materials>"
            If (requestAR) Then
                theXML = theXML & "<Material>"
                theXML = theXML & "<material_id>A/R</material_id><qty>1</qty>"
                theXML = theXML & "</Material>"
            End If
            If (questions) Then
                theXML = theXML & "<Material>"
                theXML = theXML & "<material_id>WC2</material_id><qty>1</qty>"
                theXML = theXML & "</Material>"
            End If

            theXML = theXML & "</Materials>"
        End If

        theXML = theXML & "</inforequest>"

        Console.WriteLine(theXML)
        Dim request As WebRequest = WebRequest.Create("http://www.corporate-ir.net/ireye/xmlreq.asp")

        request.Method = "post"
        request.ContentType = "text/xml"
        request.ContentLength = theXML.Length.ToString

        Dim newStream As Stream = request.GetRequestStream()

        Dim encoding As ASCIIEncoding = New ASCIIEncoding()

        Dim postdata As Byte() = encoding.GetBytes(theXML)


        newStream.Write(postdata, 0, postdata.Length)
        ''Console.WriteLine("The value of 'ContentLength' property after sending the data is {0}", request.ContentLength)
        newStream.Flush()
        newStream.Close()


        Dim webResponse As WebResponse = request.GetResponse
        'Dim i As Integer = 0

        'While i < webResponse.Headers.Count
        '    Console.WriteLine(ControlChars.Cr + "Header Name:{0}, Header value :{1}", webResponse.Headers.Keys(i), webResponse.Headers(i))
        '    i = i + 1
        'End While

        Dim theResponseStream As Stream = webResponse.GetResponseStream
        Dim readData(webResponse.ContentLength) As Byte


        theResponseStream.Read(readData, 0, webResponse.ContentLength)

        theResponseStream.Close()



        Dim theResponseXml As XmlDocument = New XmlDocument()

        Dim memStream As MemoryStream = New MemoryStream(readData)

        theResponseXml.Load(memStream)

        Dim nav As XPathNavigator = theResponseXml.CreateNavigator
        Dim itCode As XPathNodeIterator = nav.Select("Response/return_code/@code")


        itCode.MoveNext()

        Dim theCode As String = itCode.Current.Value

        If (theCode = "0") Then 'success, no errors
            Response.Redirect("information_request.aspx?success=success")
        Else 'failed, get error messages


            Dim theErrorsString As String = ""
            Dim itErrors As XPathNodeIterator = nav.Select("Response/errors/error/.")

            While itErrors.MoveNext
                theErrorsString = itErrors.Current.Value & "-"



            End While

            Response.Redirect("information_request.aspx?success=failed&errors=" & theErrorsString)
        End If


 









    End Sub

End Class
