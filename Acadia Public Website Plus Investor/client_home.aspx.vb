﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml


Imports Microsoft.VisualBasic
Imports System.Data.OleDb


Partial Class client_home
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("logged_in") <> "1" Then
            Response.Redirect("client_login.aspx")
        End If


        txtnewpassword.Attributes.Add("onkeyup", "javascript:checkPassword(this.value);")

        If Not IsPostBack Then


            txtFax.Text = Session("userfax")
            txtPhone.Text = Session("userphone")
            txtAddress.Text = Session("useraddress")
            txtTitle.Text = Session("usertitle")
            txtCity.Text = Session("usercity")
            txtZip.Text = Session("userzip")

            drpdnstate.SelectedValue = Session("userstate")

            buildfundlisting()
            TabStrip1.SelectedTab = TabStrip1.Tabs(0)
        Else
            ltrlnotification.Text = ""
        End If





    End Sub
    Sub buildfundlisting()


        Dim newTabinit As New ComponentArt.Web.UI.TabStripTab
        newTabinit.Text = "My Account"
        newTabinit.ID = "MyAccount"
        TabStrip1.Tabs.Add(newTabinit)


        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select * from ipm_project where category_id = 2404759 and available = 'Y' and projectid in (select projectid from ipm_project_security Where ((security_id in  (select a.groupid from ipm_group a, ipm_group_user b where a.active = 'Y' and a.groupid = b.groupid and b.userid = @userid) and type = 'G')) or (security_id = @userid and type = 'U')  )", MyConnection)
        Dim userid As New SqlParameter("@userid", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(userid)
        MyCommand1.SelectCommand.Parameters("@userid").Value = Session("userid")
        Dim DT1 As New DataTable("Funds")

        MyCommand1.Fill(DT1)

        For Each row In DT1.Rows
            Dim newTab As New ComponentArt.Web.UI.TabStripTab
            newTab.Text = row("name")
            newTab.ID = row("projectid")
            TabStrip1.Tabs.Add(newTab)
        Next


    End Sub

    Protected Sub CallBackFileList_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBackFileList.Callback


        If e.Parameters(0) <> "MyAccount" Then



            Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select * from ipm_asset_category where parent_cat_id = 0 and projectid = @projectid and available = 'Y' and category_id in (select category_id from ipm_category_security Where ((security_id in  (select a.groupid from ipm_group a, ipm_group_user b where a.active = 'Y' and a.groupid = b.groupid and b.userid = @userid) and type = 'G')) or (security_id = @userid and type = 'U')  ) order by description asc", MyConnection)

            Dim userid As New SqlParameter("@userid", SqlDbType.VarChar, 255)
            MyCommand1.SelectCommand.Parameters.Add(userid)
            MyCommand1.SelectCommand.Parameters("@userid").Value = Session("userid")

            Dim projectid As New SqlParameter("@projectid", SqlDbType.VarChar, 255)
            MyCommand1.SelectCommand.Parameters.Add(projectid)
            MyCommand1.SelectCommand.Parameters("@projectid").Value = e.Parameters(0)
            Dim DT1 As New DataTable("Funds")

            MyCommand1.Fill(DT1)

            For Each row In DT1.Rows
                LtrFileList.Text += "<div id=""idfilelistheading"" class=""filelistheading"">" & row("name").ToString.ToUpper & "</div>"
                'get sub cats
                Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter("select * from ipm_asset_category where parent_cat_id = " & row("category_id") & " and projectid = " & row("projectid") & " and available = 'Y' and category_id in (select category_id from ipm_category_security Where ((security_id in  (select a.groupid from ipm_group a, ipm_group_user b where a.active = 'Y' and a.groupid = b.groupid and b.userid = " & Session("userid") & ") and type = 'G')) or (security_id = " & Session("userid") & " and type = 'U')  ) order by description asc", MyConnection)
                Dim DT2 As New DataTable("subsections")
                MyCommand2.Fill(DT2)
                For Each row2 In DT2.Rows
                    LtrFileList.Text += "<div id=""idfilelistsubheading"" onclick=""javascript:CallBackFiles.Callback(" & row2("category_id") & ");"" class=""filelistsubheading""><a onclick=""javascript:CallBackFiles.Callback(" & row2("category_id") & ");"">" & row2("name") & "</a></div>"
                Next
            Next

            LtrFileList.Text += "<script language='javascript'>$('#myaccount').css('display', 'none');$('#fundinfo').css('display', 'block');</script>"
            LtrFileList.RenderControl(e.Output)
        Else
            LtrFileList.Text = "<script language='javascript'>$('#myaccount').css('display', 'block');$('#fundinfo').css('display', 'none');</script>"
            LtrFileList.RenderControl(e.Output)
        End If
    End Sub

    Protected Sub CallBackFiles_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallBackFiles.Callback





        Dim MyConnectionf As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1f As SqlDataAdapter = New SqlDataAdapter("select * from ipm_asset where category_id = @catid and available = 'Y' order by description asc, name asc", MyConnectionf)
        Dim catidf As New SqlParameter("@catid", SqlDbType.VarChar, 255)
        MyCommand1f.SelectCommand.Parameters.Add(catidf)
        MyCommand1f.SelectCommand.Parameters("@catid").Value = e.Parameters(0)
        Dim DT1f As New DataTable("Funds")
        MyCommand1f.Fill(DT1f)

        For Each row In DT1f.Rows
            'LtrFileList2.Text += "<div class=""filelistingfile""><table cellpadding=""0"" cellspacing=""0""><tr><td><div style=""padding-right:5px;""><img src=""" & Session("WSRetreiveAsset").replace("RetrieveAsset.aspx", "RetrieveIcon.aspx") & "size=0&id=" & row("media_type") & """/></div></td><td><a href=""" & Session("WSDownloadAsset") & "size=0&assetid=" & row("asset_id") & """ >" & row("name") & "</a></td></tr></table></div>"
            LtrFileList2.Text += "<div class=""filelistingfile""><table cellpadding=""0"" cellspacing=""0""><tr><td><div style=""padding-right:5px;""><img src=""images/docicon.jpg""/></div></td><td><a href=""" & Session("WSDownloadAsset") & "size=0&assetid=" & row("asset_id") & """ >" & row("name") & "</a></td></tr></table></div>"
        Next

        If DT1f.Rows.Count > 0 Then
            LtrFileList2.Text += "<br>"
        End If





        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select * from ipm_asset_category where parent_cat_id = @catid and available = 'Y' order by description asc, name asc", MyConnection)
        Dim catid As New SqlParameter("@catid", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(catid)
        MyCommand1.SelectCommand.Parameters("@catid").Value = e.Parameters(0)
        Dim DT1 As New DataTable("Funds")
        MyCommand1.Fill(DT1)


        For Each row In DT1.Rows
            LtrFileList2.Text += "<div class=""filelistingheading"">" & row("name").ToString.ToUpper & "</div>"
            'get sub cats
            Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter("select * from ipm_asset where category_id = " & row("category_id") & " and projectid = " & row("projectid") & " and available = 'Y' and asset_id in (select asset_id from ipm_asset_security Where ((security_id in  (select a.groupid from ipm_group a, ipm_group_user b where a.active = 'Y' and a.groupid = b.groupid and b.userid = " & Session("userid") & ") and type = 'G')) or (security_id = " & Session("userid") & " and type = 'U')  ) order by name ", MyConnection)
            Dim DT2 As New DataTable("subsections")
            MyCommand2.Fill(DT2)
            Dim LtrFileList2tmp As String = ""
            For Each row2 In DT2.Rows
                'LtrFileList2tmp += "<div class=""filelistingfile""><table cellpadding=""0"" cellspacing=""0""><tr><td><div style=""padding-right:5px;""><img src=" & Session("WSRetreiveAsset") & "qfactor=25&width=16&height=16&crop=1&size=1&type=asset&id=" & row2("asset_id") & "&cache=1""/></div></td><td><a href=""" & Session("WSDownloadAsset") & "size=0&assetid=" & row2("asset_id") & """ >" & row2("name") & "</a></td></tr></table></div>"
                LtrFileList2tmp += "<div class=""filelistingfile""><table cellpadding=""0"" cellspacing=""0""><tr><td><div style=""padding-right:5px;""><img src=""images/docicon.jpg""/></div></td><td><a href=""" & Session("WSDownloadAsset") & "size=0&assetid=" & row2("asset_id") & """ >" & row2("name") & "</a></td></tr></table></div>"
            Next
            If LtrFileList2tmp = "" Then
                LtrFileList2.Text += "<div class=""filelistingfile"">No files available.</div>"
            Else
                LtrFileList2.Text += LtrFileList2tmp
            End If
        Next


        If LtrFileList2.Text = "" Then
            LtrFileList2.Text = "<div class=""auto-style10"">None.</div>"
        End If



        LtrFileList2.RenderControl(e.Output)
    End Sub



    Public Shared Sub ExecuteTransaction(ByVal sqlstring As String)

        Dim ConnectionString As String

        ConnectionString = System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRINGWRITE")

        Dim connection As New OleDbConnection(ConnectionString)
        Dim command As New OleDbCommand
        Dim transaction As OleDbTransaction
        Dim failed As Boolean = False

        ' Set the Connection to the new OleDbConnection.
        command.Connection = connection

        ' Open the connection and execute the transaction.
        Try
            connection.Open()

            ' Start a local transaction with ReadCommitted isolation level.
            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            ' Assign transaction object for a pending local transaction.
            command.Connection = connection
            command.Transaction = transaction

            ' Execute the commands.
            command.CommandText = sqlstring
            command.ExecuteNonQuery()

            ' Commit the transaction.
            transaction.Commit()
            Console.WriteLine("Executed.")
            'execute Auditing

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Try to rollback the transaction
            Try
                transaction.Rollback()
                failed = True
            Catch
                ' Do nothing here; transaction is not active.
            End Try
        Finally

        End Try

        ' The connection is automatically closed when the
        ' code exits the Using block.

    End Sub
















    Protected Sub btnmodifyaccount2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnmodifyaccount2.Click
        'Session("usercompany") = txtcompany.Text
        ''Session("useremail") = txtemail.Text
        ''Session("userfirstname") = txtfirstname2.Text
        ''Session("userlastname") = txtlastname.Text


        ExecuteTransaction("update ipm_user set fax = '" & txtFax.Text.Replace("'", "''") & "' where userid = " & Session("userid"))
        ExecuteTransaction("update ipm_user set phone = '" & txtPhone.Text.Replace("'", "''") & "' where userid = " & Session("userid"))
        ExecuteTransaction("update ipm_user set workaddress = '" & txtAddress.Text.Replace("'", "''") & "' where userid = " & Session("userid"))
        ExecuteTransaction("update ipm_user set position = '" & txtTitle.Text.Replace("'", "''") & "' where userid = " & Session("userid"))
        ExecuteTransaction("update ipm_user set workcity = '" & txtCity.Text.Replace("'", "''") & "' where userid = " & Session("userid"))
        ExecuteTransaction("update ipm_user set workzip = '" & txtZip.Text.Replace("'", "''") & "' where userid = " & Session("userid"))

        ExecuteTransaction("update ipm_user set workstate = '" & drpdnstate.SelectedValue & "' where userid = " & Session("userid"))


        Session("userfax") = txtFax.Text
        Session("userphone") = txtPhone.Text
        Session("useraddress") = txtAddress.Text
        Session("usertitle") = txtTitle.Text
        Session("usercity") = txtCity.Text
        Session("userzip") = txtZip.Text
        Session("userstate") = drpdnstate.SelectedValue

        'ltrlnotification.Text = "<font color=red>Change made.</font><br><br>"

    End Sub

    Protected Sub btnupdatepassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdatepassword.Click


        'ExecuteTransaction("update ipm_user set fax = '" & txtFax.Text.Replace("'", "''") & "' where userid = " & Session("userid"))
        If txtexistingpassword.Text <> "" Then

            Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
            Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select password from ipm_user where userid = @userid", MyConnection)
            Dim catid As New SqlParameter("@userid", SqlDbType.VarChar, 255)
            MyCommand1.SelectCommand.Parameters.Add(catid)
            MyCommand1.SelectCommand.Parameters("@userid").Value = Session("userid")
            Dim DT1 As New DataTable("Funds")
            MyCommand1.Fill(DT1)
            If DT1.Rows.Count > 0 Then
                If txtexistingpassword.Text = DT1.Rows(0)("password") Then
                    'go ahead and make change
                    If txtnewpassword.Text = txtnewpassword2.Text Then
                        ltrlnotification.Text = "<font color=red>Changes made.</font><br><br>"
                        ExecuteTransaction("update ipm_user set password = '" & txtnewpassword.Text.Replace("'", "''") & "' where userid = " & Session("userid"))

                    Else
                        ltrlnotification.Text = "<font color=red>New passwords do not match.</font><br><br>"

                    End If
                Else
                    'no change
                    ltrlnotification.Text = "<font color=red>Original password does not match.</font><br><br>"
                End If
            End If

        End If

    End Sub
End Class
