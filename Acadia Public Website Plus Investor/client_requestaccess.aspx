﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="client_requestaccess" CodeFile="~/client_requestaccess.aspx.vb"  %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
 <style type="text/css">
 	@import url("css/style.css");
</style>
<script src="js/jquery.tools.min.js" type="text/javascript"></script>
<script src="js/jquery.perciformes.js" type="text/javascript"></script>
<script src="js/navi.js" type="text/javascript"></script>



<style>
#property_search {
	position: absolute;
	top:98px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
</style>

<!--[if lte IE 7]>
<style type="text/css">
html, body {
	overflow-x: hidden;
}
</style>
<![endif]-->
<!--[if IE 7]>
<style>
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 20px;
	left:1px;
}

#property_search {
	position: absolute;
	top:122px;
	left: 50%;
	margin-left: 277px;
	z-index: 1;	
	background: #f9a131;
}


</style>
<![endif]-->
<style type="text/css">
@-moz-document url-prefix() {
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1007;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 25px;
	left:1px;
}

#property_search {
	position: absolute;
	top:102px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
#header {
	position:relative;
	margin-top:19px;
	margin-bottom: 14px;	
}
}
</style>



<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    function validateForm() {

        $('form').valid();
    };

</script>
<title>Acadia Realty Trust - Company - Employment Application</title>
</head>
<body>
    <form id="form1" runat="server">
				
    <div id="container">
    <div id="header">
    <!--property search-->
				<!-- end of property search-->
			<table width="100%" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				<td width="424" valign="top"><a href="default.aspx"><img class="navi" src="images/logo.jpg" /></a></td>
				<td valign="bottom" align="right">
				<!--menu-->
		
		<!--menu-->
				</td>
				</tr>
			</table>
		</div>
		<div id="body">
			<div class="body_content">
				
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td valign="top" width="584">
						
						    <div class="cookie_crumbs"></div>
						    
						    <div class="general_title"><br />
								<!--To request access to the Acadia Realty site 
								please fill out the form below.--> <br /><br />
								<asp:literal ID="labelError" runat="server"></asp:literal>
								<asp:Panel runat=server ID=formfieldspanel>
								<table cellpadding="0" cellspacing="0" class="application_form">
									<tr>
										<td valign="top" width="200"><strong>
										ACCESS REQUEST FORM</strong></td>
										<td valign="top"></td>
									</tr>
									<tr>
										<td valign="top" width="200">First Name*</td>
										<td valign="top">
										<asp:TextBox ID="formFirstName" CssClass="required" runat="server"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td valign="top" width="200">Last Name*</td>
										<td valign="top">
										<asp:TextBox ID="formLastName" CssClass="required" runat="server"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td valign="top" width="200">Title*</td>
										<td valign="top">
										<asp:TextBox CssClass="required" ID="formCurrentTitle" runat="server"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td valign="top" width="200">
										Organization*</td>
										<td valign="top">
										<asp:TextBox CssClass="required" ID="formExperience" runat="server"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td valign="top" width="200">Type*</td>
										<td valign="top">
										<asp:DropDownList CssClass="required" ID="formBestWay" runat="server">
											<asp:ListItem Value="Investor">Investor</asp:ListItem>
											<asp:ListItem Value="Investor Affiliate">Investor Affiliate</asp:ListItem>
											<asp:ListItem Value="Lender">Lender</asp:ListItem>
										</asp:DropDownList>
										</td>
									</tr>
                                    <tr>
										<td valign="top" width="200">
										Affiliate of (if an affiliate)*</td>
										<td valign="top">
										<asp:TextBox CssClass="required" ID="formAffiliateOf" runat="server"></asp:TextBox>
										</td>
									</tr>									
									<tr>
										<td valign="top" width="200">Address 1*</td>
										<td valign="top">
										<asp:TextBox ID="formAddress" CssClass="required" runat="server"></asp:TextBox>
										</td>
									</tr>
                                    <tr>
										<td valign="top" width="200">Address 2*</td>
										<td valign="top">
										<asp:TextBox ID="formAddress2" CssClass="required" runat="server"></asp:TextBox>
										</td>
									</tr>									
									<tr>
										<td valign="top" width="200">City*</td>
										<td valign="top">
										<asp:TextBox ID="formCity" CssClass="required" runat="server"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td valign="top" width="200">State*</td>
										<td valign="top">
										<asp:DropDownList CssClass="required" ID="formState" runat="server">
											<asp:ListItem Value="AK">Alaska</asp:ListItem>
											<asp:ListItem Value="AL">Alabama</asp:ListItem>
											<asp:ListItem Value="AR">Arkansas</asp:ListItem>
											<asp:ListItem Value="AZ">Arizona</asp:ListItem>
											<asp:ListItem Value="CA">California</asp:ListItem>
											<asp:ListItem Value="CO">Colorado</asp:ListItem>
											<asp:ListItem Value="CT">Connecticut</asp:ListItem>
											<asp:ListItem Value="DC">District of Columbia</asp:ListItem>
											<asp:ListItem Value="DE">Delaware</asp:ListItem>
											<asp:ListItem Value="FL">Florida</asp:ListItem>
											<asp:ListItem Value="GA">Georgia</asp:ListItem>
											<asp:ListItem Value="HI">Hawaii</asp:ListItem>
											<asp:ListItem Value="IA">Iowa</asp:ListItem>
											<asp:ListItem Value="ID">Idaho</asp:ListItem>
											<asp:ListItem Value="IL">Illinois</asp:ListItem>
											<asp:ListItem Value="IN">Indiana</asp:ListItem>
											<asp:ListItem Value="KS">Kansas</asp:ListItem>
											<asp:ListItem Value="KY">Kentucky</asp:ListItem>
											<asp:ListItem Value="LA">Louisiana</asp:ListItem>
											<asp:ListItem Value="MA">Massachusetts</asp:ListItem>
											<asp:ListItem Value="MD">Maryland</asp:ListItem>
											<asp:ListItem Value="ME">Maine</asp:ListItem>
											<asp:ListItem Value="MI">Michigan</asp:ListItem>
											<asp:ListItem Value="MN">Minnesota</asp:ListItem>
											<asp:ListItem Value="MO">Missouri</asp:ListItem>
											<asp:ListItem Value="MS">Mississippi</asp:ListItem>
											<asp:ListItem Value="MT">Montana</asp:ListItem>
											<asp:ListItem Value="NC">North Carolina</asp:ListItem>
											<asp:ListItem Value="ND">North Dakota</asp:ListItem>
											<asp:ListItem Value="NE">Nebraska</asp:ListItem>
											<asp:ListItem Value="NV">Nevada</asp:ListItem>
											<asp:ListItem Value="NH">New Hampshire</asp:ListItem>
											<asp:ListItem Value="NJ">New Jersey</asp:ListItem>
											<asp:ListItem Value="NM">New Mexico</asp:ListItem>
											<asp:ListItem Value="NY">New York</asp:ListItem>
											<asp:ListItem Value="OH">Ohio</asp:ListItem>
											<asp:ListItem Value="OK">Oklahoma</asp:ListItem>
											<asp:ListItem Value="OR">Oregon</asp:ListItem>
											<asp:ListItem Value="PA">Pennsylvania</asp:ListItem>
											<asp:ListItem Value="RI">Rhode Island</asp:ListItem>
											<asp:ListItem Value="SC">South Carolina</asp:ListItem>
											<asp:ListItem Value="SD">South Dakota</asp:ListItem>
											<asp:ListItem Value="TN">Tennessee</asp:ListItem>
											<asp:ListItem Value="TX">Texas</asp:ListItem>
											<asp:ListItem Value="UT">Utah</asp:ListItem>
											<asp:ListItem Value="VA">Virginia</asp:ListItem>
											<asp:ListItem Value="VT">Vermont</asp:ListItem>
											<asp:ListItem Value="WA">Washington</asp:ListItem>
											<asp:ListItem Value="WI">Wisconsin</asp:ListItem>
											<asp:ListItem Value="WV">West Virginia</asp:ListItem>
											<asp:ListItem Value="WY">Wyoming</asp:ListItem>
										</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td valign="top" width="200">Zip*</td>
										<td valign="top">
										<asp:TextBox ID="formZip" CssClass="required" runat="server"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td valign="top" width="200">Country*</td>
										<td valign="top">
										<asp:TextBox ID="formCountry" CssClass="required" runat="server"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td valign="top" width="200">Phone #*</td>
										<td valign="top">
										<asp:TextBox ID="formPhone" CssClass="required" runat="server"></asp:TextBox>
										</td>
									</tr>
<tr>
										<td valign="top" width="200">Fax #*</td>
										<td valign="top">
										<asp:TextBox ID="formFax" CssClass="required" runat="server"></asp:TextBox>
										</td>
									</tr>
									
									<tr>
										<td valign="top" width="200">Email Address*</td>
										<td valign="top">
										<asp:TextBox ID="formEmail" CssClass="required email" runat="server"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td valign="top" width="200">Reenter Email Address*</td>
										<td valign="top">
										<asp:TextBox ID="formEmail2" CssClass="required email" runat="server"></asp:TextBox>
										</td>
									</tr>
									
									
									<tr>
										<td valign="top" width="200">&nbsp;</td>
										<td valign="top">
										&nbsp;</td>
									</tr>
									<tr>
										<td valign="top" width="200">Type the two words*</td>
										<td valign="top">
										<recaptcha:RecaptchaControl  ID="recaptcha"  runat="server" Theme="white"  PublicKey="6LcZf7sSAAAAACI3kWDqQ6n8D0aXX7sU1GrAWU1W" PrivateKey="6LcZf7sSAAAAAHWrxO1osl8YHHxye9Bj5cmq2BPd" />
										</td>
									</tr>
									<tr>
										<td valign="top" width="200"><b>* Denotes mandatory fields</b></td>
										<td valign="top">
										<asp:ImageButton runat="server" OnClientClick="validateForm();" ID="submitApp" ImageUrl="images/button_submit_application.jpg" CssClass="submitFormButton" />
										</td>
									</tr>
								</table>
								</asp:Panel>
							</div>
						</td>
						<td valign="top">&nbsp;</td>
					</tr>
				
				</table>
			</div>
		</div>
	</div>
	<div id="footer_container">
		<div id="footer">
			<table cellpadding="0" cellspacing="0">
				<tr>
				<td valign="top" style="width: 520px">
				<div style="width:400px;">
				<big><b>Acadia Realty Trust</b></big><br />
				Acadia Realty Trust
				Acadia Realty Trust (NYSE:AKR) is a fully integrated, self-managed and self-administered equity REIT focused primarily on the ownership, acquisition, redevelopment and management of retail properties, including neighborhood / community shopping centers and mixed-use properties with retail components. 
				</div>
				</td>
				<td valign="top" width="120">
				&nbsp;</td>
				<td valign="top" width="120">
				&nbsp;</td>
				<td valign="top">
				<big><b>Corporate Headquarters</b></big><br />
				1311 Mamaroneck Avenue<br />
				Suite 260<br />
				White Plains, NY 10605<br />
				<a href="mailto:info@acadiarealty.com">info@acadiarealty.com</a><br />
				<a target="_blank" href="http://maps.google.com/maps?f=d&daddr=1311 Mamaroneck Avenue, White Plains, NY 10605">Google map directions</a><br />
				</td>
				</tr>
				<tr>
					<td colspan="4">
					<br /><br />
					© Copyright 2010 Acadia Realty Trust&nbsp;&nbsp; 
					|&nbsp;&nbsp; <a href="terms.html" target="_blank">Terms and Conditions </a>&nbsp;&nbsp; |&nbsp;&nbsp; 
					<a href="privacy.html" target="_blank">Privacy Statement </a>					</td>
				
				</tr>
			</table>
			</div>
	</div>	
	

	<script type="text/javascript">

	    var timeout = 500;
	    var closetimer = 0;
	    var ddmenuitem = 0;

	    function jsddm_open() {
	        jsddm_canceltimer();
	        jsddm_close();
	        ddmenuitem = $(this).find('ul').css('visibility', 'visible');
	    }

	    function jsddm_close()
	    { if (ddmenuitem) ddmenuitem.css('visibility', 'hidden'); }

	    function jsddm_timer()
	    { closetimer = window.setTimeout(jsddm_close, timeout); }

	    function jsddm_canceltimer() {
	        if (closetimer) {
	            window.clearTimeout(closetimer);
	            closetimer = null;
	        } 
	    }

	    $(document).ready(function() {
	        $('#jsddm > li').bind('mouseover', jsddm_open)
	        $('#jsddm > li').bind('mouseout', jsddm_timer)
	    });

	    document.onclick = jsddm_close;
</script>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-17447452-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

</script></form>

</body>
</html>
