﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.Xml.XPath
Partial Class news_and_events
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim theYear As String = Request.QueryString("newsyear")

        Dim searchString As String = Request.Form("newskeyword")

        Dim showEvents As String = Request.QueryString("showEvents")




        If (theYear = "") Then
            theYear = Date.Today.ToString("yyyy")
        End If

        Dim NewsDS As XmlDataSource = New XmlDataSource



        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=newsreleases", ConfigurationSettings.AppSettings("NewsXMLFileName"))
        NewsDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("NewsXMLFileName")

        If (searchString <> "") Then
            'NewsDS.XPath = "IRXML/NewsReleases/NewsCategory/NewsRelease[substring(Date/@Date,1,4) = '" & theYear & "' and contains(Title,'" & searchString & "') = true]"
            NewsDS.XPath = "IRXML/NewsReleases/NewsCategory/NewsRelease[contains(DisplayDateStart,'" & theYear & "')  and contains(translate(Title,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'),'" & searchString.ToUpper & "')]"
        Else
            NewsDS.XPath = "IRXML/NewsReleases/NewsCategory/NewsRelease[contains(DisplayDateStart,'" & theYear & "')]"
        End If


        RepeaterNews.DataSource = NewsDS
        RepeaterNews.DataBind()
        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=events2", ConfigurationSettings.AppSettings("EventsXMLFileName"))
        Dim doc As XPathDocument = New XPathDocument(ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("EventsXMLFileName"))
        Dim nav As XPathNavigator = doc.CreateNavigator()


        Dim expr As XPathExpression
        expr = nav.Compile("IRXML/Events/Event[@EventStatus=0]")

        expr.AddSort("DisplayableDatesTimes/DisplayableDateTime/@EventStartDate", XmlSortOrder.Descending, XmlCaseOrder.None, "", XmlDataType.Text)


        Dim yearsString As String = ""
        Dim earliestYear As Integer = ConfigurationSettings.AppSettings("EarliestNewsYear")
        Dim currentyear As Integer = CInt(Date.Now.ToString("yyyy"))
        Dim indexYear As Integer = currentyear
        Dim firstYearFlag As Integer = 0

        Do While indexYear >= earliestYear
            If (firstYearFlag = 0) Then
                If (theYear = indexYear) Then
                    yearsString = yearsString & "<b>" & indexYear & "</b>"
                Else
                    yearsString = yearsString & "<a href=""news_and_events.aspx?newsyear=" & indexYear & "&showevents=" & showEvents & """>" & indexYear & "</a>"

                End If
                firstYearFlag = 1
            Else
                If (theYear = indexYear) Then
                    yearsString = yearsString & " | " & "<b>" & indexYear & "</b>"
                Else
                    yearsString = yearsString & " | " & "<a href=""news_and_events.aspx?newsyear=" & indexYear & "&showevents=" & showEvents & """>" & indexYear & "</a>"
                End If

            End If
            indexYear -= 1
        Loop

        literalYears.Text = yearsString

        Dim iterator As XPathNodeIterator = nav.Select(expr)
        Dim iterator2 As XPathNodeIterator = nav.Select(expr) 'future

        Dim theLiteralString As String = ""
        Dim theLiteralFutureString As String = ""
        Dim newscounter As Integer = 1
        Do While iterator.MoveNext()
            Dim itTitle As XPathNodeIterator = iterator.Current.Select("EventTitle")
            Dim itDateStart As XPathNodeIterator = iterator.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@EventStartDate")
            Dim itTimeStart As XPathNodeIterator = iterator.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@EventStartTime")
            Dim itDateEnd As XPathNodeIterator = iterator.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@EventEndDate")
            Dim itTimeEnd As XPathNodeIterator = iterator.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@EventEndTime")
            Dim itTimeZone As XPathNodeIterator = iterator.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@TimeZone")

            Dim itDrillDown As XPathNodeIterator = iterator.Current.Select("@AllowCalendarToDetailsDrilldown")
            Dim itEventID As XPathNodeIterator = iterator.Current.Select("@EventID")
            Dim itAddress As XPathNodeIterator
            Dim itCity As XPathNodeIterator
            Dim itState As XPathNodeIterator
            Dim locationID As XPathNodeIterator = Nothing


            Dim itsupportMaterials As XPathNodeIterator
            Dim itSpeakers As XPathNodeIterator




            Dim theAddress As String = ""
            Dim theCity As String = ""
            Dim theState As String = ""

            Dim thesupportMaterialString As String = ""
            Dim theSpeakersString As String = ""


            Try
                itAddress = iterator.Current.Select("Location/Address1/.")
                itAddress.MoveNext()
                If (itAddress.CurrentPosition > 0) Then
                    theAddress = itAddress.Current.Value
                End If
            Catch ex As Exception

            End Try

            Try
                itCity = iterator.Current.Select("Location/City/.")
                itCity.MoveNext()
                If (itCity.CurrentPosition > 0) Then
                    theCity = itCity.Current.Value
                End If

            Catch ex As Exception

            End Try

            Try
                itState = iterator.Current.Select("Location/State/.")
                itState.MoveNext()
                If (itState.CurrentPosition > 0) Then
                    theState = itState.Current.Value
                End If
            Catch ex As Exception

            End Try

            Try
                itsupportMaterials = iterator.Current.Select("SupportMaterials/SupportMaterial")
                Dim initFlag As Integer = 0

                Do While itsupportMaterials.MoveNext
                    If (initFlag = 0) Then
                        initFlag = 1
                        thesupportMaterialString = thesupportMaterialString & "Supporting Materials: <br />"
                    End If
                    Dim itsupportMaterialsURL As XPathNodeIterator = itsupportMaterials.Current.Select("URL/.")
                    Dim itsupportMaterialsLabel As XPathNodeIterator = itsupportMaterials.Current.Select("LinkLabel/.")
                    Dim itsupportMaterialIcon As XPathNodeIterator = itsupportMaterials.Current.Select("Encodings/Encoding/EncodingIconUrl/.")

                    itsupportMaterialsURL.MoveNext()
                    itsupportMaterialsLabel.MoveNext()
                    itsupportMaterialIcon.MoveNext()


                    thesupportMaterialString = thesupportMaterialString & "<img src=""" & itsupportMaterialIcon.Current.Value.ToString.Replace("/", "/") & """ alt=""encoding"" /> &nbsp; <a href=""" & itsupportMaterialsURL.Current.Value & """>" & itsupportMaterialsLabel.Current.Value & "</a><br />"

                Loop

            Catch ex As Exception

            End Try

            Try
                itSpeakers = iterator.Current.Select("Speakers/Speaker")

                Dim initFlag As Integer = 0
                Do While itSpeakers.MoveNext
                    If (initFlag = 0) Then
                        initFlag = 1
                        theSpeakersString = theSpeakersString & "Speakers:<br />"
                    End If
                    Dim itSpeakersFirstName As XPathNodeIterator = itSpeakers.Current.Select("FirstName/.")
                    Dim itSpeakersMiddleName As XPathNodeIterator = itSpeakers.Current.Select("MiddleInitial/.")
                    Dim itSpeakersLastName As XPathNodeIterator = itSpeakers.Current.Select("LastName/.")
                    Dim itSpeakersTitle As XPathNodeIterator = itSpeakers.Current.Select("Title/.")

                    itSpeakersFirstName.MoveNext()
                    itSpeakersLastName.MoveNext()
                    itSpeakersMiddleName.MoveNext()
                    itSpeakersTitle.MoveNext()

                    Dim mnameString As String = ""

                    If (itSpeakersMiddleName.CurrentPosition > 0) Then
                        mnameString = itSpeakersMiddleName.Current.Value
                    End If

                    theSpeakersString = theSpeakersString & itSpeakersFirstName.Current.Value & " " & " " & mnameString & " " & itSpeakersLastName.Current.Value & ", " & itSpeakersTitle.Current.Value & "<br />"



                Loop
            Catch ex As Exception

            End Try

            itTitle.MoveNext()
            itDateStart.MoveNext()
            itTimeStart.MoveNext()
            itDateEnd.MoveNext()
            itTimeEnd.MoveNext()
            itDrillDown.MoveNext()
            itEventID.MoveNext()
            itTimeZone.MoveNext()


            Dim theTitle As String = itTitle.Current.Value
            Dim theStartDate As DateTime = Nothing
            Dim theStartTime As DateTime = Nothing
            Dim theEndDate As DateTime = Nothing
            Dim theEndTime As DateTime = Nothing
            Dim theTimeZone As String = itTimeZone.Current.Value


            Dim theDrillDown As String = itDrillDown.Current.Value
            Dim theEID As String = itEventID.Current.Value




            Dim provider As CultureInfo = CultureInfo.InvariantCulture

            Try
                theStartDate = DateTime.ParseExact(itDateStart.Current.Value, "yyyyMMdd", provider)
                If (theStartDate > FormatDateTime(Date.Now, DateFormat.ShortDate)) Then
                    Continue Do

                End If
            Catch ex As Exception
                Continue Do
            End Try
            Try
                theEndDate = DateTime.ParseExact(itDateEnd.Current.Value, "yyyyMMdd", provider)
            Catch ex As Exception

            End Try
            Try
                theStartTime = DateTime.Parse(itTimeStart.Current.Value, provider)
            Catch ex As Exception

            End Try
            Try
                theEndTime = DateTime.Parse(itTimeEnd.Current.Value, provider)
            Catch ex As Exception

            End Try

            If (theDrillDown = "No") Then
                theLiteralString = theLiteralString & "<b>" & theTitle & "</b><br />"
            Else
                theLiteralString = theLiteralString & "<a href=""event_detail.aspx?id=" & theEID & """>" & theTitle & "</a><br />"

            End If

            theLiteralString = theLiteralString & theStartDate.ToString("MM/dd/yy")

            If (theStartTime <> Nothing) Then
                theLiteralString = theLiteralString & " " & theStartTime.ToString("hh:mm tt") & " " & theTimeZone & "<br /> "
            End If

            If (theEndDate <> Nothing) Then
                theLiteralString = theLiteralString & "through <br />" & theEndDate.ToString("MM/dd/yy")

            End If
            If (theEndDate <> Nothing) Then
                theLiteralString = theLiteralString & " " & theEndTime.ToString("hh:mm tt") & " " & theTimeZone & "<br /> "
            End If

            If (theSpeakersString <> "") Then
                theLiteralString = theLiteralString & theSpeakersString
            End If

            If (theAddress <> "" Or theCity <> "" Or theState <> "") Then
                theLiteralString = theLiteralString & "Location: "
                If (theAddress <> "") Then
                    theLiteralString = theLiteralString & theAddress & ", "
                End If
                If (theCity <> "") Then
                    theLiteralString = theLiteralString & theCity & ", "
                End If
                If (theState <> "") Then
                    theLiteralString = theLiteralString & theState & ""
                End If
                theLiteralString = theLiteralString & "<br />"
            End If

            If (thesupportMaterialString <> "") Then
                theLiteralString = theLiteralString & thesupportMaterialString
            End If

            theLiteralString = theLiteralString & "<br /><br />"

            If newscounter = 3 And showEvents <> "show" Then Exit Do
            newscounter += 1
        Loop

        If 1 = 1 Then




            Do While iterator2.MoveNext()
                Dim itTitle As XPathNodeIterator = iterator2.Current.Select("EventTitle")
                Dim itDateStart As XPathNodeIterator = iterator2.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@EventStartDate")
                Dim itTimeStart As XPathNodeIterator = iterator2.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@EventStartTime")
                Dim itDateEnd As XPathNodeIterator = iterator2.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@EventEndDate")
                Dim itTimeEnd As XPathNodeIterator = iterator2.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@EventEndTime")
                Dim itTimeZone As XPathNodeIterator = iterator2.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@TimeZone")

                Dim itDrillDown As XPathNodeIterator = iterator2.Current.Select("@AllowCalendarToDetailsDrilldown")
                Dim itEventID As XPathNodeIterator = iterator2.Current.Select("@EventID")
                Dim itAddress As XPathNodeIterator
                Dim itCity As XPathNodeIterator
                Dim itState As XPathNodeIterator
                Dim locationID As XPathNodeIterator = Nothing


                Dim itsupportMaterials As XPathNodeIterator
                Dim itSpeakers As XPathNodeIterator




                Dim theAddress As String = ""
                Dim theCity As String = ""
                Dim theState As String = ""

                Dim thesupportMaterialString As String = ""
                Dim theSpeakersString As String = ""


                Try
                    itAddress = iterator2.Current.Select("Location/Address1/.")
                    itAddress.MoveNext()
                    If (itAddress.CurrentPosition > 0) Then
                        theAddress = itAddress.Current.Value
                    End If
                Catch ex As Exception

                End Try

                Try
                    itCity = iterator2.Current.Select("Location/City/.")
                    itCity.MoveNext()
                    If (itCity.CurrentPosition > 0) Then
                        theCity = itCity.Current.Value
                    End If

                Catch ex As Exception

                End Try

                Try
                    itState = iterator2.Current.Select("Location/State/.")
                    itState.MoveNext()
                    If (itState.CurrentPosition > 0) Then
                        theState = itState.Current.Value
                    End If
                Catch ex As Exception

                End Try

                Try
                    itsupportMaterials = iterator2.Current.Select("SupportMaterials/SupportMaterial")
                    Dim initFlag As Integer = 0

                    Do While itsupportMaterials.MoveNext
                        If (initFlag = 0) Then
                            initFlag = 1
                            thesupportMaterialString = thesupportMaterialString & "Supporting Materials: <br />"
                        End If
                        Dim itsupportMaterialsURL As XPathNodeIterator = itsupportMaterials.Current.Select("URL/.")
                        Dim itsupportMaterialsLabel As XPathNodeIterator = itsupportMaterials.Current.Select("LinkLabel/.")
                        Dim itsupportMaterialIcon As XPathNodeIterator = itsupportMaterials.Current.Select("Encodings/Encoding/EncodingIconUrl/.")

                        itsupportMaterialsURL.MoveNext()
                        itsupportMaterialsLabel.MoveNext()
                        itsupportMaterialIcon.MoveNext()


                        thesupportMaterialString = thesupportMaterialString & "<img src=""" & itsupportMaterialIcon.Current.Value.ToString.Replace("/", "/") & """ alt=""encoding"" /> &nbsp; <a href=""" & itsupportMaterialsURL.Current.Value & """>" & itsupportMaterialsLabel.Current.Value & "</a><br />"

                    Loop

                Catch ex As Exception

                End Try

                Try
                    itSpeakers = iterator2.Current.Select("Speakers/Speaker")

                    Dim initFlag As Integer = 0
                    Do While itSpeakers.MoveNext
                        If (initFlag = 0) Then
                            initFlag = 1
                            theSpeakersString = theSpeakersString & "Speakers:<br />"
                        End If
                        Dim itSpeakersFirstName As XPathNodeIterator = itSpeakers.Current.Select("FirstName/.")
                        Dim itSpeakersMiddleName As XPathNodeIterator = itSpeakers.Current.Select("MiddleInitial/.")
                        Dim itSpeakersLastName As XPathNodeIterator = itSpeakers.Current.Select("LastName/.")
                        Dim itSpeakersTitle As XPathNodeIterator = itSpeakers.Current.Select("Title/.")

                        itSpeakersFirstName.MoveNext()
                        itSpeakersLastName.MoveNext()
                        itSpeakersMiddleName.MoveNext()
                        itSpeakersTitle.MoveNext()

                        Dim mnameString As String = ""

                        If (itSpeakersMiddleName.CurrentPosition > 0) Then
                            mnameString = itSpeakersMiddleName.Current.Value
                        End If

                        theSpeakersString = theSpeakersString & itSpeakersFirstName.Current.Value & " " & " " & mnameString & " " & itSpeakersLastName.Current.Value & ", " & itSpeakersTitle.Current.Value & "<br />"



                    Loop
                Catch ex As Exception

                End Try

                itTitle.MoveNext()
                itDateStart.MoveNext()
                itTimeStart.MoveNext()
                itDateEnd.MoveNext()
                itTimeEnd.MoveNext()
                itDrillDown.MoveNext()
                itEventID.MoveNext()
                itTimeZone.MoveNext()


                Dim theTitle As String = itTitle.Current.Value
                Dim theStartDate As DateTime = Nothing
                Dim theStartTime As DateTime = Nothing
                Dim theEndDate As DateTime = Nothing
                Dim theEndTime As DateTime = Nothing
                Dim theTimeZone As String = itTimeZone.Current.Value


                Dim theDrillDown As String = itDrillDown.Current.Value
                Dim theEID As String = itEventID.Current.Value




                Dim provider As CultureInfo = CultureInfo.InvariantCulture

                Try
                    theStartDate = DateTime.ParseExact(itDateStart.Current.Value, "yyyyMMdd", provider)
                    If (theStartDate < FormatDateTime(Date.Now, DateFormat.ShortDate)) Then
                        Continue Do

                    End If
                Catch ex As Exception
                    Continue Do
                End Try
                Try
                    theEndDate = DateTime.ParseExact(itDateEnd.Current.Value, "yyyyMMdd", provider)
                Catch ex As Exception

                End Try
                Try
                    theStartTime = DateTime.Parse(itTimeStart.Current.Value, provider)
                Catch ex As Exception

                End Try
                Try
                    theEndTime = DateTime.Parse(itTimeEnd.Current.Value, provider)
                Catch ex As Exception

                End Try

                If (theDrillDown = "No") Then
                    theLiteralFutureString = theLiteralFutureString & "<table><tr><td><img src=""images/WebcastIcon.png""/></td><td><b>" & theTitle & "</b><br />"
                Else
                    theLiteralFutureString = theLiteralFutureString & "<table><tr><td><img src=""images/WebcastIcon.png""/></td><td><a href=""event_detail.aspx?id=" & theEID & """>" & theTitle & "</a><br />"

                End If

                theLiteralFutureString = theLiteralFutureString & theStartDate.ToString("MM/dd/yy")

                If (theStartTime <> Nothing) Then
                    theLiteralFutureString = theLiteralFutureString & " " & theStartTime.ToString("hh:mm tt") & " " & theTimeZone & "<br /> "
                End If

                If (theEndDate <> Nothing) Then
                    theLiteralFutureString = theLiteralFutureString & "through <br />" & theEndDate.ToString("MM/dd/yy")

                End If
                If (theEndDate <> Nothing) Then
                    theLiteralFutureString = theLiteralFutureString & " " & theEndTime.ToString("hh:mm tt") & " " & theTimeZone & "<br /> "
                End If

                If (theSpeakersString <> "") Then
                    theLiteralFutureString = theLiteralFutureString & theSpeakersString
                End If

                If (theAddress <> "" Or theCity <> "" Or theState <> "") Then
                    theLiteralFutureString = theLiteralFutureString & "Location: "
                    If (theAddress <> "") Then
                        theLiteralFutureString = theLiteralFutureString & theAddress & ", "
                    End If
                    If (theCity <> "") Then
                        theLiteralFutureString = theLiteralFutureString & theCity & ", "
                    End If
                    If (theState <> "") Then
                        theLiteralFutureString = theLiteralFutureString & theState & ""
                    End If
                    theLiteralFutureString = theLiteralFutureString & "<br />"
                End If

                If (thesupportMaterialString <> "") Then
                    theLiteralFutureString = theLiteralFutureString & thesupportMaterialString
                End If

                theLiteralFutureString = theLiteralFutureString & "</td></tr></table><br /><br />"

            Loop


            literalPastEvents.Text = theLiteralString
        Else
            literalPastEvents.Text = "<a href=""news_and_events.aspx?newsyear=" & theYear & "&showevents=show"">Show past events</a>"
        End If
        If (theLiteralFutureString = "") Then
            theLiteralFutureString = "There are currently no events scheduled.<br /><br />"
        End If

        If showEvents <> "show" Then
            literalPastEvents.Text += "<a href=""news_and_events.aspx?newsyear=" & theYear & "&showevents=show"">Show All</a>"
        End If

        literalFutureEvents.Text = theLiteralFutureString




    End Sub

    Public Sub RepeaterEventsOnItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim literalEventDates As Literal = e.Item.FindControl("literalEventDates")
            Dim hyperlinkEvent As HyperLink = e.Item.FindControl("hyperlinkEvent")
            Dim literalEvent As Literal = e.Item.FindControl("literalEvent")

            Dim navigableDataItem As XPathNavigator = CType(e.Item.DataItem, IXPathNavigable).CreateNavigator

            Dim itTitle As XPathNodeIterator = navigableDataItem.Select("EventTitle")
            Dim itDateStart As XPathNodeIterator = navigableDataItem.Select("DisplayableDatesTimes/DisplayableDateTime/@EventStartDate")
            Dim itTimeStart As XPathNodeIterator = navigableDataItem.Select("DisplayableDatesTimes/DisplayableDateTime/@EventStartTime")
            Dim itDateEnd As XPathNodeIterator = navigableDataItem.Select("DisplayableDatesTimes/DisplayableDateTime/@EventEndDate")
            Dim itTimeEnd As XPathNodeIterator = navigableDataItem.Select("DisplayableDatesTimes/DisplayableDateTime/@EventEndTime")
            Dim itDrillDown As XPathNodeIterator = navigableDataItem.Select("@AllowCalendarToDetailsDrilldown")
            Dim itEventID As XPathNodeIterator = navigableDataItem.Select("@EventID")


            itTitle.MoveNext()
            itDateStart.MoveNext()
            itTimeStart.MoveNext()
            itDateEnd.MoveNext()
            itTimeEnd.MoveNext()
            itDrillDown.MoveNext()
            itEventID.MoveNext()


            Dim theTitle As String = itTitle.Current.Value
            Dim theStartDate As DateTime = Nothing
            Dim theStartTime As DateTime = Nothing
            Dim theEndDate As DateTime = Nothing
            Dim theEndTime As DateTime = Nothing

            Dim theDrillDown As String = itDrillDown.Current.Value
            Dim theEID As String = itEventID.Current.Value


            Dim provider As CultureInfo = CultureInfo.InvariantCulture

            Try
                theStartDate = DateTime.ParseExact(itDateStart.Current.Value, "yyyyMMdd", provider)
            Catch ex As Exception

            End Try
            Try
                theEndDate = DateTime.ParseExact(itDateEnd.Current.Value, "yyyyMMdd", provider)
            Catch ex As Exception

            End Try
            Try
                theStartTime = DateTime.Parse(itTimeStart.Current.Value, provider)
            Catch ex As Exception

            End Try
            Try
                theEndTime = DateTime.Parse(itTimeEnd.Current.Value, provider)
            Catch ex As Exception

            End Try

            literalEvent.Text = theTitle
            literalEventDates.Text = theStartDate.ToString("mm/dd/yy")

            If (theStartTime <> Nothing) Then
                literalEventDates.Text = literalEventDates.Text & " " & theStartTime.ToString("hh:mm tt")
            End If

            If (theEndDate <> Nothing) Then
                literalEventDates.Text = literalEventDates.Text & "<br /> through <br />" & theEndDate.ToString("mm/dd/yy")

            End If
            If (theEndDate <> Nothing) Then
                literalEventDates.Text = literalEventDates.Text & " " & theEndTime.ToString("hh:mm tt")
            End If


            If (theDrillDown = "No") Then
                hyperlinkEvent.Enabled = False
            Else
                hyperlinkEvent.NavigateUrl = "event_detail.aspx?id=" & theEID

            End If


        End If

    End Sub

End Class
