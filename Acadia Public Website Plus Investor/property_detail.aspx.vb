﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Partial Class property_detail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim ws = New acadiaws.Service1
        Dim ws2 = New acadiaws.Service1

        Dim theProjectID = Request.QueryString("id")

        Dim dt As DataTable = ws.ListProjects("", "", "", "", "", "", theProjectID, "")

        If (dt.Rows.Count > 0) Then
            literalProjectCrumbs.Text = dt.Rows(0)("name").ToString.Trim

            Page.Header.Title = "Acadia Realty Trust - Property Detail - " & dt.Rows(0)("name").ToString.Trim & " - " & dt.Rows(0)("city").ToString.Trim & ", " & dt.Rows(0)("state_id").ToString.Trim

            literalProjectName.Text = dt.Rows(0)("name").ToString.Trim
            literalProjectDescription.Text = dt.Rows(0)("location").ToString.Trim.Replace("vblf", "<br />")
            literalProjectLocation.Text = dt.Rows(0)("city").ToString.Trim & ", " & dt.Rows(0)("state_id").ToString.Trim & " " & dt.Rows(0)("zip").ToString.Trim
            literalProjectAddress.Text = dt.Rows(0)("address") & "<br />" & dt.Rows(0)("city").ToString.Trim & ", " & dt.Rows(0)("state_id").ToString.Trim & " " & dt.Rows(0)("zip").ToString.Trim
            hyperlinkProjectMap.NavigateUrl = "http://maps.google.com/maps?f=d&daddr=" & dt.Rows(0)("address") & " " & dt.Rows(0)("city").ToString.Trim & " " & dt.Rows(0)("state_id").ToString.Trim & " " & dt.Rows(0)("zip").ToString.Trim

            literalGLA.Text = FormatNumber(dt.Rows(0)("gla").ToString.Trim, 0) & " SF"

            Try
                
                
                literalDT1.Text = dt.Rows(0)("colH1").ToString
                literalDT2.Text = dt.Rows(0)("colH2").ToString
                literalDT3.Text = dt.Rows(0)("colH3").ToString
                if FormatNumber(dt.Rows(0)("colH1").ToString, 0)  <= 1 then
                	literalDT1s.Text = ""
                	else
                	literalDT1s.Text = "s"
                end if
                if FormatNumber(dt.Rows(0)("colH2").ToString, 0)  <= 1 then
                	literalDT2s.Text = ""
                	else
                	literalDT2s.Text = "s"
                end if

                if FormatNumber(dt.Rows(0)("colH3").ToString, 0)  <= 1 then
                	literalDT3s.Text = ""
                	else
                	literalDT3s.Text = "s"
                end if
                Catch ex As Exception
                 
			literalDT1s.Text = ""
			literalDT2s.Text = ""
			literalDT3s.Text = ""


				end try
try
				
				
				literalPtitle.Text = dt.Rows(0)("rowh1").ToString
				literalHtitle.Text = dt.Rows(0)("rowh2").ToString
				literalAHItitle.Text = dt.Rows(0)("rowh3").ToString
				literalMHItitle.Text = dt.Rows(0)("rowh4").ToString
 Catch ex As Exception
				literalPtitle.Text = "Population"
				literalHtitle.Text = "Households"
				literalAHItitle.Text = "Avg. HH Income"
				literalMHItitle.Text = "Med. HH Income"
End Try
try	
				literalParking.Text = FormatNumber(dt.Rows(0)("parking").ToString.Trim, 0)
                literalP1.Text = FormatNumber(dt.Rows(0)("p3mile").ToString, 0)
                literalP3.Text = FormatNumber(dt.Rows(0)("p5mile").ToString, 0)
                literalP5.Text = FormatNumber(dt.Rows(0)("p10mile").ToString, 0)
 Catch ex As Exception
 
                literalParking.Text = dt.Rows(0)("parking").ToString.Trim

                literalP1.Text = dt.Rows(0)("p3mile")
                literalP3.Text = dt.Rows(0)("p5mile")
                literalP5.Text = dt.Rows(0)("p10mile")
end try
try
                literalH1.Text = FormatNumber(dt.Rows(0)("h3mile").ToString, 0)
                literalH3.Text = FormatNumber(dt.Rows(0)("h5mile").ToString, 0)
                literalH5.Text = FormatNumber(dt.Rows(0)("h10mile").ToString, 0)
 Catch ex As Exception
 
                literalH1.Text = dt.Rows(0)("h3mile")
                literalH3.Text = dt.Rows(0)("h5mile")
                literalH5.Text = dt.Rows(0)("h10mile")
end try
try
                literalAHI1.Text = FormatNumber(dt.Rows(0)("ahh3mile").ToString, 0)
                literalAHI3.Text = FormatNumber(dt.Rows(0)("ahh5mile").ToString, 0)
                literalAHI5.Text = FormatNumber(dt.Rows(0)("ahh10mile").ToString, 0)
 Catch ex As Exception
                literalAHI1.Text = dt.Rows(0)("ahh3mile")
                literalAHI3.Text = dt.Rows(0)("ahh5mile")
                literalAHI5.Text = dt.Rows(0)("ahh10mile")
end try
try
                literalMHI1.Text = FormatNumber(dt.Rows(0)("mhh3mile").ToString, 0)
                literalMHI3.Text = FormatNumber(dt.Rows(0)("mhh5mile").ToString, 0)
                literalMHI5.Text = FormatNumber(dt.Rows(0)("mhh10mile").ToString, 0)
                
            Catch ex As Exception


                literalMHI1.Text = dt.Rows(0)("mhh3mile")
                literalMHI3.Text = dt.Rows(0)("mhh5mile")
                literalMHI5.Text = dt.Rows(0)("mhh10mile")
            End Try


            projectImage1.ImageUrl = Session("WSRetreiveAsset") & "qfactor=25&width=504&height=305&crop=1&size=1&type=project&cropcolor=black&id=" & theProjectID
            projectImage1Thumb.ImageUrl = Session("WSRetreiveAsset") & "qfactor=25&width=84&height=84&crop=1&size=1&type=project&id=" & theProjectID
            hyperlinkImage1.NavigateUrl = (Session("WSRetreiveAsset") & "qfactor=25&size=1&width=800&height=800&type=project&id=" & theProjectID).ToString.Replace("(", "%28").Replace(")", "%29")
            hyperlinkImage1.Attributes.Add("rel", "lightbox")
            hyperlinkImage1.Attributes.Add("title", dt.Rows(0)("name").ToString.Trim)

            hyperlinkImage1Thumb.NavigateUrl = "javascript:switchImage('" & Session("WSRetreiveAsset") & "qfactor=25&width=504&height=305&crop=2&size=1&type=project&cropcolor=black&id=" & theProjectID & "'," & theProjectID & ",'asset','" & dt.Rows(0)("name").ToString.Trim & "');"


            Dim dtSpaces As DataTable = ws2.GetSpaces(theProjectID)


            Dim dtAvailableSpaces As DataTable = New DataTable
            dtAvailableSpaces.Columns.Add(New DataColumn("suite", GetType(String)))
            dtAvailableSpaces.Columns.Add(New DataColumn("assetid", GetType(String)))

            Dim z As DataRow = dtAvailableSpaces.NewRow
            z("suite") = "Select Available Space"
            z("assetid") = ""
            dtAvailableSpaces.Rows.Add(z)


            Dim dtTenants As DataTable = New DataTable
            dtTenants.Columns.Add("asset_id")


            For Each y As DataRow In dtSpaces.Rows
                Dim dtTenant As DataTable = ws2.GetSpaceTenant(y("space_id"))
                Dim dtSpaceAssets As DataTable = ws2.GetSpaceAssets(y("space_id"))
                If (dtTenant.Rows.Count = 0) Then
                    Dim r As DataRow = dtAvailableSpaces.NewRow
                    r("suite") = "Space " & y("suite")
                    If (dtSpaceAssets.Rows.Count > 0) Then
                        r("assetid") = dtSpaceAssets.Rows(0)("asset_id")
                    Else
                        r("assetid") = "0"
                    End If

                    dtAvailableSpaces.Rows.Add(r)


                Else
                    Dim dtMajorTenant As DataTable = ws.GetTenantDetail(dtTenant(0)("tenant_id"))

                    If (dtMajorTenant.Rows(0)("major") = "1") Then
                        Dim dtMajorTenantAssets = ws2.GetTenantAssets(dtTenant(0)("tenant_id"))

                        'For Each z As DataColumn In dtMajorTenantAssets.Columns
                        '    Console.WriteLine(z.ColumnName)
                        'Next

                        If (dtMajorTenantAssets.Rows.Count > 0) Then
                            Dim r As DataRow = dtTenants.NewRow
                            r("asset_id") = dtMajorTenantAssets.Rows(0)("Asset_ID")
                            dtTenants.Rows.Add(r)
                        End If


                    End If

                End If


            Next

            Dim dvAvailableSpaces As DataView = New DataView(dtAvailableSpaces)
            dvAvailableSpaces.Sort = "suite asc"

            dropdownAvailableSpace.DataTextField = "suite"
            dropdownAvailableSpace.DataValueField = "assetid"

            dropdownAvailableSpace.DataSource = dvAvailableSpaces
            dropdownAvailableSpace.DataBind()

            'For Each x As DataColumn In dtTenants.Columns
            '    Console.WriteLine(x.ColumnName)
            'Next

            repeaterMajorTenants.DataSource = dtTenants
            repeaterMajorTenants.DataBind()

            Dim dtProjectContact As DataTable = ws.GetProjectContacts(theProjectID)

            repeaterProjectContact.DataSource = dtProjectContact
            repeaterProjectContact.DataBind()

            'For Each x As DataColumn In dtProjectContact.Columns
            '    Console.WriteLine(x.ColumnName)
            'Next


            Dim dtProjectUDF As DataTable = ws.GetProjectUDF(theProjectID)
            repeaterProjectContact.DataSource = dtProjectContact
            repeaterProjectContact.DataBind()

            For Each x As DataColumn In dtProjectUDF.Columns
                Console.WriteLine(x.ColumnName)
            Next

            For Each x As DataRow In dtProjectUDF.Rows
                If (x("item_name").ToString.Trim = "Property Summary") Then
                    literalProjectDescription.Text = x("item_value").ToString.Replace(vbLf, "<br />").Trim
                End If
            Next



            Dim dtAssets = ws.GetProjectAssets(theProjectID)

            'For Each x As DataColumn In dtAssets.Columns
            '    Console.WriteLine(x.ColumnName)
            'Next

            Dim dtDownloadAssets As DataTable = New DataTable
            dtDownloadAssets.Columns.Add("name")
            dtDownloadAssets.Columns.Add("asset_id")

            Dim dtGalleryAssets As DataTable = New DataTable
            dtGalleryAssets.Columns.Add("name")
            dtGalleryAssets.Columns.Add("asset_id")

            For Each x As DataRow In dtAssets.Rows
                If (x("catname") = "Downloads") Then
                    Dim r As DataRow = dtDownloadAssets.NewRow
                    r("name") = x("name")
                    r("asset_id") = x("asset_id")
                    dtDownloadAssets.Rows.Add(r)
                ElseIf (x("catname") = "Property Images") Then
                    Dim r As DataRow = dtGalleryAssets.NewRow
                    r("name") = x("name")
                    r("asset_id") = x("asset_id")
                    dtGalleryAssets.Rows.Add(r)
                End If
            Next

            RepeaterAssetDownloads.DataSource = dtDownloadAssets
            RepeaterAssetDownloads.DataBind()

            RepeaterGalleryThumbs.DataSource = dtGalleryAssets
            RepeaterGalleryThumbs.DataBind()



        End If

        'competition map check

        Dim dtCompAssets = ws.GetProjectAssets(theProjectID)
        Dim compFlag As Integer = 0

        For Each x As DataRow In dtCompAssets.Rows
            If (x("catname") = "Competition Map") Then
                compFlag = 1
            End If
        Next


        If (compFlag = 0) Then
            placeholderCompetitionMap.Visible = False
        End If

    End Sub
End Class
