﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.Xml.XPath

Partial Class sec_filings
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim thegroup As String = Request.QueryString("group")



        Dim SECFilingsDS As XmlDataSource = New XmlDataSource
        Functions.ConvertDataSetToXMLDataSource("http://irxml.corporate-ir.net/filings/listing.data?limit=1&sXbrl=1&compid=61503", ConfigurationSettings.AppSettings("SECXMLFileName"))
        SECFilingsDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("SECXMLFileName")

        If (thegroup <> "" And thegroup <> "All Forms") Then
            SECFilingsDS.XPath = "TR.Filings_listing/Resultlist/RESULT[FILINGDATA/FORM_GROUP = '" & thegroup & "']"
        Else
            'SECFilingsDS.XPath = "Wizard.com_results/Hitlist/HIT"
            SECFilingsDS.XPath = "TR.Filings_listing/Resultlist/RESULT"
        End If



        'Dim pds As PagedDataSource = New PagedDataSource

        'pds.DataSource = SECFilingsDS
        'pds.CurrentPageIndex = 0
        'pds.PageSize = CInt(ConfigurationSettings.AppSettings("searchSize"))
        'pds.CurrentPageIndex = 0
        RepeaterSEC.DataSource = SECFilingsDS
        RepeaterSEC.DataBind()






        Dim theGroups As String = ""


        Dim doc As XPathDocument = New XPathDocument(ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("SECXMLFileName"))
        Dim nav As XPathNavigator = doc.CreateNavigator()

        Dim expr As XPathExpression
        'TR.Filings_listing/Resultlist/RESULT
        'expr = nav.Compile("Wizard.com_results/Hitlist/HIT[not(FILING_INFO/FORM_GROUP = preceding::FILING_INFO/FORM_GROUP) and not(FILING_INFO/FORM_GROUP = '')]")
        'expr.AddSort("FILING_INFO/FORM_GROUP/.", XmlSortOrder.Ascending, XmlCaseOrder.None, "", XmlDataType.Text)

        expr = nav.Compile("TR.Filings_listing/Resultlist/RESULT[not(FILINGDATA/FORM_GROUP = preceding::FILINGDATA/FORM_GROUP) and not(FILINGDATA/FORM_GROUP = '')]")
        expr.AddSort("FILINGDATA/FORM_GROUP/.", XmlSortOrder.Ascending, XmlCaseOrder.None, "", XmlDataType.Text)


        Dim iterator As XPathNodeIterator = nav.Select(expr)

        Do While iterator.MoveNext
            Dim itFormGroup As XPathNodeIterator = iterator.Current.Select("FILINGDATA/FORM_GROUP")

            itFormGroup.MoveNext()

            If (thegroup = itFormGroup.Current.Value) Then
                theGroups = theGroups & "<option selected=""selected"" value=""" & itFormGroup.Current.Value & """>" & itFormGroup.Current.Value & "</option>"
            Else
                theGroups = theGroups & "<option value=""" & itFormGroup.Current.Value & """>" & itFormGroup.Current.Value & "</option>"
            End If


        Loop

        literalGroups.Text = theGroups


        '<option value="<%#XPath("FILING_INFO/FORM_GROUP") %>"><%#XPath("FILING_INFO/FORM_GROUP")%></option>

        'repeaterGroups.DataSource = SECGroupsDS
        'repeaterGroups.DataBind()






    End Sub

    Public Sub RepeaterSECItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim imageWord As Image = e.Item.FindControl("imageWord")
            Dim imageExcel As Image = e.Item.FindControl("imageExcel")
            Dim imagePDF As Image = e.Item.FindControl("imagePDF")
            Dim imageXBRL As Image = e.Item.FindControl("imageXBRL")

            Dim linkword As HyperLink = e.Item.FindControl("linkWord")
            Dim linkexcel As HyperLink = e.Item.FindControl("linkExcel")
            Dim linkpdf As HyperLink = e.Item.FindControl("linkPDF")
            Dim linkxbrl As HyperLink = e.Item.FindControl("linkXBRL")

            Dim navigableDataItem As XPathNavigator = CType(e.Item.DataItem, IXPathNavigable).CreateNavigator

            Dim wordIterator As XPathNodeIterator = navigableDataItem.Select("FILINGDATA/DOC_LINK")
            wordIterator.MoveNext()
            Dim wordURL As String = wordIterator.Current.Value

            Dim excelIterator As XPathNodeIterator = navigableDataItem.Select("FILINGDATA/XLS_LINK")
            excelIterator.MoveNext()
            Dim excelURL As String = excelIterator.Current.Value

            Dim pdfIterator As XPathNodeIterator = navigableDataItem.Select("FILINGDATA/PDF_LINK")
            pdfIterator.MoveNext()
            Dim pdfURL As String = pdfIterator.Current.Value

            Dim xbrlIterator As XPathNodeIterator = navigableDataItem.Select("FILINGDATA/XBRL_LINK")
            xbrlIterator.MoveNext()
            Dim xbrlURL As String = xbrlIterator.Current.Value

            If (wordURL <> "") Then
                imageWord.ImageUrl = "images/word.gif"
                linkword.NavigateUrl = wordURL
            Else
                imageWord.ImageUrl = "images/word_off.gif"
            End If

            If (excelURL <> "") Then
                imageExcel.ImageUrl = "images/excel.gif"
                linkexcel.NavigateUrl = excelURL
            Else
                imageExcel.ImageUrl = "images/excel_off.gif"
            End If

            If (pdfURL <> "") Then
                imagePDF.ImageUrl = "images/pdf.gif"
                linkpdf.NavigateUrl = pdfURL
            Else
                imagePDF.ImageUrl = "images/pdf_off.gif"
            End If

            If (xbrlURL <> "") Then
                imageXBRL.ImageUrl = "images/xbrl.gif"
                linkxbrl.NavigateUrl = xbrlURL
            Else
                imageXBRL.ImageUrl = "images/xbrl_off.gif"
            End If

        End If
    End Sub

End Class
