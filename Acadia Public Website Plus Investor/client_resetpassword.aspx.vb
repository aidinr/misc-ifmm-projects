﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml

Imports System.Web.Mail
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Partial Class client_resetpassword
    Inherits System.Web.UI.Page

    Protected Sub client_resetpassword_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("rid") = "" Then
                'Response.Redirect("client_login.aspx")
            End If
            'check rid is valid
            If Not isValidUserRequest(Request.QueryString("rid")) Then
                'Response.Redirect("client_login.aspx")
            End If
        End If
        txtnewpassword.Attributes.Add("onkeyup", "javascript:checkPassword(this.value);")
    End Sub

    Function isValidUserRequest(ByVal tmppassword As String) As Boolean
        'get item_id
        Dim _tmp_password_item_id As String = Functions.CheckAddUserUDF("sp_createnewuserfield", "Temporary Password", "System", "IDAM_TEMPORARY_PASSWORD", "", 1, "Security", 1, 1)

        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select user_id userid from ipm_user_field_value where item_id = " & _tmp_password_item_id & " and item_value = @tmppassword", MyConnection)
        Dim catid As New SqlParameter("@tmppassword", SqlDbType.VarChar, 255)
        MyCommand1.SelectCommand.Parameters.Add(catid)
        MyCommand1.SelectCommand.Parameters("@tmppassword").Value = tmppassword
        Dim DT1 As New DataTable("Funds")
        MyCommand1.Fill(DT1)
        If DT1.Rows.Count <> 1 Then
            'invalid request
            Session("userid") = ""
            Response.Redirect("client_login.aspx")

        Else
            'valid request
            'get user info
            'check for use of tmp password
            Dim _use_password_item_id As String = Functions.CheckAddUserUDF("sp_createnewuserfield", "Use Temporary Password", "System", "IDAM_USE_TEMPORARY_PASSWORD", "", 10, "Security", 1, 1)
            If Functions.GetDataTable("select user_id from ipm_user_field_value where item_id = " & _use_password_item_id & " and item_value = '1' and user_id = " & DT1.Rows(0)("userid"), System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING")).Rows.Count = 1 Then


                Dim usertb As DataTable = Functions.GetDataTable("select firstname, lastname, email from ipm_user where userid = " & DT1.Rows(0)("userid"), System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
                username.Text = usertb.Rows(0)("firstname") & " " & usertb.Rows(0)("lastname")
                Session("userid") = DT1.Rows(0)("userid")
                Session("usertempemailaddress") = usertb.Rows(0)("email")




            Else
                'dont use
                Session("userid") = ""
                Response.Redirect("client_login.aspx")
            End If
        End If
    End Function

    Protected Sub sendconfirmationemail()
        Dim strMSG As String = ""

        strMSG = "<br><br>Your password has been changed.  Please contact Acadia Realty immediately if you do not beleive you recently modified your password.<br><br>"


        Dim strSenderEmail As String = System.Configuration.ConfigurationManager.AppSettings("EMAILRequestSender")

        Try
            Dim insMail As New MailMessage
            insMail.BodyFormat = MailFormat.Html
            With insMail
                .From = strSenderEmail
                .To = Session("usertempemailaddress")
                .Subject = "Acadia Realty Account Information"
                .Body = strMSG
            End With
            SmtpMail.SmtpServer = System.Configuration.ConfigurationManager.AppSettings("SMTPServer")
            SmtpMail.Send(insMail)



        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnupdatepassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdatepassword.Click

        If Page.IsValid Then
            'ExecuteTransaction("update ipm_user set fax = '" & txtFax.Text.Replace("'", "''") & "' where userid = " & Session("userid"))
            If txtnewpassword.Text <> "" Then

                'go ahead and make change
                If txtnewpassword.Text = txtnewpassword2.Text Then
                    ltrlnotification.Text = "<font color=red>Password reset.  Please click <a href=""client_login.aspx"">here</a> to login.</font><br><br>"
                    Functions.ExecuteTransaction("update ipm_user set password = '" & txtnewpassword.Text.Replace("'", "''") & "' where userid = " & Session("userid"))
                    sendconfirmationemail()
                    Functions.unsetTemporaryPassword(Session("userid"))
                Else
                    ltrlnotification.Text = "<font color=red>New passwords do not match.</font><br><br>"

                End If


            End If
        Else
            ltrlnotification.Text = "<font color=red>Please check the validation value and try again.</font><br><br>"
        End If


    End Sub
End Class
