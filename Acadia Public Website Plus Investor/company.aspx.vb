﻿Imports System.Data

Partial Class company
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim ws = New acadiaws.Service1
        Dim dtAssets = ws.GetProjectAssets("2405123")

        For Each x As DataRow In dtAssets.Rows
            If (x("catname") = "Page Graphics") Then
                If x("name") = "Company Right Side Graphic" Then
                    imgsideimage.ImageUrl = Session("WSRetreiveAsset") & "qfactor=25&width=351&height=528&crop=1&size=1&type=asset&id=" & x("asset_id") & "&cache=1"
                End If
            End If
        Next
        If imgsideimage.ImageUrl = "" Then
            imgsideimage.ImageUrl = "images\company_image.jpg"
        End If




        For Each x As DataRow In dtAssets.Rows
            If (x("catname") = "Page Downloads") Then
                If x("name") = "Capabilities" Then
                    ltrlDownload.Text = Session("WSDownloadAsset") & "size=0&assetid=" & x("asset_id")
                End If
            End If
        Next
        If ltrlDownload.Text = "" Then
            ltrlDownload.Text = "pdf\AKR_Capabilities.pdf"
        End If

    End Sub
End Class
