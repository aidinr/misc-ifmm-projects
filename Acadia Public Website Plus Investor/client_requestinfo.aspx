﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="client_requestinfo" CodeFile="client_requestinfo.aspx.vb"  CodeBehind="client_requestinfo.aspx.vb" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<style type="text/css">
 	@import url("css/style.css");
</style>
<script src="js/jquery.tools.min.js" type="text/javascript"></script>
<script src="js/jquery.perciformes.js" type="text/javascript"></script>
<script src="js/navi.js" type="text/javascript"></script>



<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    function validateForm() {
   
       $('form').valid();
    };

</script>
<style>


.updateprofilebutton 
{
  color:white; 
  background-color:#1A4473;
  font-family: Verdana; 
  font-size: 11px; 
  cursor:pointer;
width:50px;
  padding:5px;
  padding-left:15px;
    padding-right:15px;
}


.updateprofilebuttonhover
{
	
  color:#1A4473; 
  background-color:  #DBE0E7;

}


#property_search {
	position: absolute;
	top:98px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
</style>

<!--[if lte IE 7]>
<style type="text/css">
html, body {
	overflow-x: hidden;
}
</style>
<![endif]-->
<!--[if IE 7]>
<style>
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 20px;
	left:1px;
		color:  #1A4473;
	font-family: Verdana, Geneva, Tahoma, sans-serif;

}

#property_search {
	position: absolute;
	top:122px;
	left: 50%;
	margin-left: 277px;
	z-index: 1;	
	background: #f9a131;
}


</style>
<![endif]-->
<style type="text/css">
@-moz-document url-prefix() {
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1007;
}

.body_content {
	color:  #1A4473;
	font-family: Verdana, Geneva, Tahoma, sans-serif;

	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 25px;
	left:1px;
}

#property_search {
	position: absolute;
	top:102px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
#header {
	position:relative;
	margin-top:19px;
	margin-bottom: 14px;	
}
}
    .style1
    {
        width: 344px;
    }
</style>


<title>Acadia Realty Trust - Company - About Us</title>
</head>
<body>
    <form id="form1" runat="server">
   <div id="container">
   		<!--property search-->
		<!-- end of property search-->
		<div id="header">
			<table width="100%" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				<td width="424" valign="top"><a href="default.aspx"><img class="navi" src="images/logo.jpg" /></a></td>
				<td valign="bottom" align="right">
				<!--menu-->
		<!--menu-->
				
				</td>
				</tr>
			</table>
		</div>
		<div id="body">
			<div class="body_content">
				
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td valign="top" class="style1">
						
						    <div class="general_title"></div>
						
						    <div class="general_content_sas">
						    	<strong>To reset your password, enter your email address and a new temporary password will be generated and emailed to you.<br />
								<br />
								<br />
								
								<br />
								</strong>
								<font color=red><asp:Literal ID="ltrErrorMessage" runat="server"></asp:Literal></font>
								<div class="general_content_sas">
								<table style="width: 100%">
									<tr>
										<td style="width: 195px">Enter Email 
										Address</td>
										
										<td>
										<asp:TextBox id="Email1" runat="server" Width="218px" Text=""></asp:TextBox>
										    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                ControlToValidate="Email1" ErrorMessage="RequiredFieldValidator">*</asp:RequiredFieldValidator>
									</td>
									</tr>
								
									<tr>
										<td style="height: 26px; width: 195px;">
										Confirm Email Address</td>
									
										<td style="height: 26px"><strong>
										<asp:TextBox id="Email2" runat="server" CausesValidation="True" 
                                                 Width="217px" ></asp:TextBox>
										    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                ControlToValidate="Email2" ErrorMessage="RequiredFieldValidator">*</asp:RequiredFieldValidator>
										</strong></td>
									</tr>
									<tr>
										<td style="width: 195px">&nbsp;</td>
										<td>
										<br>
										
										<recaptcha:RecaptchaControl  ID="recaptcha" Theme="white"  runat="server"  PublicKey="6LcZf7sSAAAAACI3kWDqQ6n8D0aXX7sU1GrAWU1W" PrivateKey="6LcZf7sSAAAAAHWrxO1osl8YHHxye9Bj5cmq2BPd" />
										<br />
										<asp:ImageButton runat="server" OnClientClick="validateForm();" ID="submitApp" ImageUrl="images/button_submit_application.jpg" CssClass="submitFormButton" />
										
										

										
										<asp:Button id="Button1" runat="server"       class="updateprofilebutton" Visible=false
                                                Text="Continue" Height="23px" Width="66px" />
										&nbsp;</td>
									</tr>
									<tr>
										<td style="width: 195px">&nbsp;</td>
										<td>
										&nbsp;</td>
									</tr>
								</table>
								</div>
								<br />
								<br />
								</strong></div>
						</td>
						<td valign="top" style="height:600px;">&nbsp;</td>
					</tr>
				
				</table>
			</div>
		</div>
	</div>
	<div id="footer_container">
		<div id="footer">
			<table cellpadding="0" cellspacing="0">
				<tr>
				<td valign="top" width="520">
				<div style="width:400px;">
				<big><b>Acadia Realty Trust</b></big><br />
				Acadia Realty Trust
				Acadia Realty Trust (NYSE:AKR) is a fully integrated, self-managed and self-administered equity REIT focused primarily on the ownership, acquisition, redevelopment and management of retail properties, including neighborhood / community shopping centers and mixed-use properties with retail components. 
				</div>
				</td>
				<td valign="top" width="120">
				&nbsp;</td>
				<td valign="top" width="120">
				&nbsp;</td>
				<td valign="top">
				<big><b>Corporate Headquarters</b></big><br />
				1311 Mamaroneck Avenue<br />
				Suite 260<br />
				White Plains, NY 10605<br />
				<a href="mailto:info@acadiarealty.com">info@acadiarealty.com</a><br />
				<a target="_blank" href="http://maps.google.com/maps?f=d&daddr=1311 Mamaroneck Avenue, White Plains, NY 10605">Google map directions</a><br />
				</td>
				</tr>
				<tr>
					<td colspan="4">
					<br /><br />
					© Copyright 2010 Acadia Realty Trust&nbsp;&nbsp; 
					|&nbsp;&nbsp; <a href="terms.html" target="_blank">Terms and Conditions </a>&nbsp;&nbsp; |&nbsp;&nbsp; 
					<a href="privacy.html" target="_blank">Privacy Statement </a>					</td>
				
				</tr>
			</table>
			</div>
	</div>	
<script type="text/javascript">
    function showRecaptcha(element) {
        Recaptcha.create("your_public_key", element, {
            theme: "clean",
            callback: Recaptcha.focus_response_field
        });
    }
      </script>

	<script type="text/javascript">
    
    
    
    
    
    
    
	var timeout    = 500;
	var closetimer = 0;
	var ddmenuitem = 0;
	
	function jsddm_open()
	{  jsddm_canceltimer();
	   jsddm_close();
	   ddmenuitem = $(this).find('ul').css('visibility', 'visible');}
	
	function jsddm_close()
	{  if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}
	
	function jsddm_timer()
	{  closetimer = window.setTimeout(jsddm_close, timeout);}
	
	function jsddm_canceltimer()
	{  if(closetimer)
	   {  window.clearTimeout(closetimer);
	      closetimer = null;}}
	
	$(document).ready(function()
	{ 	   
	   
 $('.updateprofilebutton').hover(
  function() {
      $(this).addClass("updateprofilebuttonhover");
  },
  function() {
      $(this).removeClass("updateprofilebuttonhover");
  }   
);


  $("#TextBox2").keydown(checkForEnter);
	   
	   
	   });


	   function checkForEnter(event) {
	       if (event.keyCode == 13) {
	           document.forms[0].submit();
	       }
	   }
	
	   
	
	
</script>
    </form>
</body>
</html>
