﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Partial Class analyst_coverage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim EstimatesDS As XmlDataSource = New XmlDataSource
        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=earningsestimates", ConfigurationSettings.AppSettings("EstimatesXMLFileName"))
        EstimatesDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("EstimatesXMLFileName")
        EstimatesDS.XPath = "IRXML/EarningsEstimates/EarningEstimate"

        RepeaterEstimates.DataSource = EstimatesDS
        RepeaterEstimates.DataBind()

        EstimatesDS.XPath = "IRXML/EarningsEstimates/EarningEstimate[position()=1]"

        'load analysts from people.xml

        RepeaterFutures.DataSource = EstimatesDS
        RepeaterFutures.DataBind()

        Dim AnalystsDS As XmlDataSource = New XmlDataSource
        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=people2", ConfigurationSettings.AppSettings("PeopleXMLFileName2"))
        AnalystsDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("PeopleXMLFileName2")
        AnalystsDS.XPath = "IRXML/People/Analyst"

        RepeaterAnalysts.DataSource = AnalystsDS
        RepeaterAnalysts.DataBind()


    End Sub
End Class
