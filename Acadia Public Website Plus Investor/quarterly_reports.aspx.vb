﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.Xml.XPath

Partial Class quarterly_reports
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=items", ConfigurationSettings.AppSettings("ItemsXMLFileName"))
        Dim doc As XPathDocument = New XPathDocument(ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("ItemsXMLFileName"))
        Dim nav As XPathNavigator = doc.CreateNavigator()

        Dim expr As XPathExpression
        expr = nav.Compile("IRXML/Items/Item[@Status='Active' and @GroupCode='OR' and @Type='Other Financial Reports']")
        expr.AddSort("Date/@Date", XmlSortOrder.Descending, XmlCaseOrder.None, "", XmlDataType.Text)
        '<Title>Acadia Highlights- Citi CEO Conference</Title> 
        Dim thecurrentcurrentgroup As String = ""
        Dim iterator As XPathNodeIterator = nav.Select(expr)

        Dim theString As String = ""
        Dim theCounter As Integer = 0
        Dim theStarterCounter As Integer = 0
        Dim thecurrentdate As Date
        Dim thecurrentgroup As String = ""
        Dim newcurrentgroup As String = ""
        Do While iterator.MoveNext()
            theCounter += 1
            Dim itDate As XPathNodeIterator = iterator.Current.Select("Date")
            Dim itTitle As XPathNodeIterator = iterator.Current.Select("Title")
            Dim itURL As XPathNodeIterator = iterator.Current.Select("URL")
            Dim itType As XPathNodeIterator = iterator.Current.Select("@Type")
            Dim itID As XPathNodeIterator = iterator.Current.Select("@EventID")
            Dim itEncodingIcone As XPathNodeIterator = iterator.Current.Select("EncodingIconUrl")
            Dim itEncodingCodec As XPathNodeIterator = iterator.Current.Select("EncodingCodec")

            itDate.MoveNext()
            itTitle.MoveNext()
            itURL.MoveNext()
            itType.MoveNext()
            itID.MoveNext()
            itEncodingIcone.MoveNext()
            itEncodingCodec.MoveNext()


            'filter out proxy statement
            If InStr(itTitle.Current.Value, "Proxy", CompareMethod.Text) = 0 Then
                theStarterCounter += 1

                'get latest report
                If theStarterCounter = 1 Then
                    literalMainReport.Text = itTitle.Current.Value
                    literalMainReportLink.text = itURL.Current.Value
                Else
                    'all others
                    'get current date
                    thecurrentdate = itDate.Current.Value
                    thecurrentgroup = thecurrentdate.Year
                    If newcurrentgroup = "" Then
                        newcurrentgroup = thecurrentgroup
                        thecurrentcurrentgroup = thecurrentgroup
                    End If

                    ''"<table cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""stock_quote""><tr><td colspan=""2""><b>b sdfgsdfg</b></td></tr>"
                    ''"</table><br/>"




                    If thecurrentgroup = newcurrentgroup Then
                        'build 1st list
                        literalFirstGroupHeading.text = thecurrentgroup
                        literalFirstGroup.text += "<tr><td width=""2""><img src=""images/li_gray.gif"" /></td><td><a href=""" & itURL.Current.Value & """ target=""_blank"">" & itTitle.Current.Value & "</a></td></tr>"

                    Else
                        'build new 2nd list
                        If thecurrentcurrentgroup <> thecurrentgroup Then
                            thecurrentcurrentgroup = thecurrentgroup
                            'new group
                            If literalSecondGroup.Text <> "" Then
                                literalSecondGroup.Text += "</table><br/>"
                            End If
                            literalSecondGroup.Text += "<table cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""stock_quote""><tr><td colspan=""2""><b> " & thecurrentgroup & "</b></td></tr>"
                        End If
                        'literalSecondGroupHeading.Text = thecurrentgroup
                        literalSecondGroup.Text += "<tr><td width=""2""><img src=""images/li_gray.gif"" /></td><td><a href=""" & itURL.Current.Value & """ target=""_blank"">" & itTitle.Current.Value & "</a></td></tr>"



                    End If

                End If

            End If
        Loop
        literalSecondGroup.Text += "</table><br/>"













    End Sub
End Class
