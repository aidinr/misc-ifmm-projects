﻿Imports System.Xml
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Partial Class Carousel_XML
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Request.ContentType = "text/xml"


        Dim ws = New acadiaws.Service1
        Dim ws2 = New acadiaws.Service1

        Dim dtAssets = ws.GetProjectAssets(ConfigurationSettings.AppSettings("CarouselProjectID"))

        'For Each x As DataColumn In dtAssets.Columns
        '    Console.WriteLine(x.ColumnName)
        'Next

        Dim dtDownloadAssets As DataTable = New DataTable
        dtDownloadAssets.Columns.Add("asset_id")
        dtDownloadAssets.Columns.Add("description")

        For Each x As DataRow In dtAssets.Rows
            Dim r As DataRow = dtDownloadAssets.NewRow
            r("description") = x("description")
            r("asset_id") = x("asset_id")
            dtDownloadAssets.Rows.Add(r)
        Next

        RepeaterXML.DataSource = dtDownloadAssets
        RepeaterXML.DataBind()


    End Sub
End Class
