﻿
Partial Class view_webcast
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim theID As String = Request.QueryString("ID")

        If (Session("Webcast_Authorize_" & theID.ToString) = True) Then

            hyperlinkWebCast.NavigateUrl = ConfigurationSettings.AppSettings("BaseWebCastURL") & "EventId=" & Request.QueryString("id") & "&StreamId=" & Request.QueryString("streamid") & "&RGS=1"
            literalPopup.Text = ConfigurationSettings.AppSettings("BaseWebCastURL") & "EventId=" & Request.QueryString("id") & "&StreamId=" & Request.QueryString("streamid") & "&RGS=1"
            
            'literalPopup.Text = "http://investors.acadiarealty.com/phoenix.zhtml?p=irol-eventDetails&c=61503&eventID=" & Request.QueryString("id")
            

        Else
            Response.Redirect("news_and_events.aspx")
        End If

    End Sub
End Class
