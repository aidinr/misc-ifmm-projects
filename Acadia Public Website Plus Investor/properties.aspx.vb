﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data

Partial Class properties
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim callbackcheck As Boolean = False
        Try
            If (InStr(Request.Form.AllKeys(0).ToString, "Callback") > 0) Then
                callbackcheck = True
            End If
        Catch ex As Exception

        End Try

        If Not callbackcheck Then

            Dim searchControl As Web.UI.Control

            searchControl = Me.LoadControl("search.ascx")

            placeholderSearchForm.Controls.Add(searchControl)

            Dim searchMode = Request.QueryString("search")







            'brochure download
            Dim ws = New acadiaws.Service1
            Dim dtAssets = ws.GetProjectAssets("2405123")

            For Each x As DataRow In dtAssets.Rows
                If (x("catname") = "Page Downloads") Then
                    If x("name") = "Search Results Property List" Then
                        ltrdownload.Text = Session("WSDownloadAsset") & "size=0&assetid=" & x("asset_id")
                        'http://idam.ifmm.com/IDAM/(S(ty4jme55yzfr4x452j4kp345))/DownloadFile.aspx?dtype=assetdownload&instance=IDAM_ARCADIA&size=0&assetid=2401917
                    End If
                End If
            Next
            If ltrdownload.Text = "" Then
                ltrdownload.Text = "pdf\PropertyListBrochureElectronic.pdf"
            End If















            Dim dt As DataTable = New DataTable


            If (searchMode = "search") Then
                Dim searchState = Request.QueryString("state").Trim
                Dim searchString = Request.QueryString("string").Trim

                If (searchString = "Keyword Search") Then
                    searchString = ""
                End If

                If (searchState = "All States") Then
                    searchState = ""
                End If

                dt = ws.ListProjects(searchString.Trim, "", "", "", "", "", "", searchState.Trim)

                If (searchString = "") Then
                    If (searchState = "") Then
                        literalSearchTerms.Text = "All Properties"
                    Else
                        literalSearchTerms.Text = searchState
                    End If
                Else
                    If (searchState = "") Then
                        literalSearchTerms.Text = searchString
                    Else
                        literalSearchTerms.Text = searchState & " and " & searchString
                    End If

                End If



            Else
                dt = ws.ListProjects("", "", "", "", "", "", "", "")
                literalSearchTerms.Text = "All Properties"
            End If

            Dim dv As DataView = New DataView(dt)
            dv.Sort = "name asc, city asc"

            Dim dTbl As DataTable = dv.Table

            Dim pgDataSc As PagedDataSource = New PagedDataSource
            pgDataSc.DataSource = dv
            pgDataSc.AllowPaging = True
            pgDataSc.PageSize = CInt(ConfigurationSettings.AppSettings("searchSize"))
            pgDataSc.CurrentPageIndex = 0
            RepeaterSearchResults.DataSource = pgDataSc

            RepeaterSearchResults.DataBind()

            Dim pageCount As Integer = pgDataSc.PageCount

            Dim pagingString As String = ""
            If pageCount > 0 Then
                For i As Integer = 0 To pageCount - 1
                    If (i = pgDataSc.CurrentPageIndex) Then
                        pagingString = pagingString & "<b>" & (i + 1) & "</b> "
                    Else
                        pagingString = pagingString & "<a href=""javascript:switchPage(" & i & ");"">" & (i + 1) & "</a> "
                    End If

                Next
            End If
            literalPaging.Text = pagingString
            literalPagingBottom.Text = pagingString


            'For Each x As DataColumn In dt.Columns
            '    Console.WriteLine(x.ColumnName)
            'Next

            'RepeaterSearchResults.DataSource = dv
            'RepeaterSearchResults.DataBind()

            Session("searchPaging") = 0
            Session("searchSort") = "name"
            Session("searchSortby") = "asc"
            Session("searchFilter") = ""

            textboxFilter.Attributes.Add("onkeydown", "if(event.keyCode==13){filterSearch(this.value);return false;}")

        End If
    End Sub

    Public Sub RepeaterSearchResultsItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim literalAvailability As Literal = e.Item.FindControl("literalAvailability")
            Dim literalTenants As Literal = e.Item.FindControl("literalTenants")
            Dim theProjectID As String = e.Item.DataItem("projectID")

            Dim literalmanaged2 As Literal = e.Item.FindControl("literlmanaged2")

            Dim ws = New acadiaws.Service1

            Dim dt As DataTable = ws.GetSpaces(theProjectID)

            Dim dtmanaged As DataTable = ws.GetProjectUDF(theProjectID)
            For Each row In dtmanaged.Rows
                If row("item_tag") = "IDAM_PROTYPE" Then
                    literalmanaged2.Text = "<br/>" & row(2)
                End If
            Next
            'For Each x As DataColumn In dt.Columns
            '    Console.WriteLine(x.ColumnName)
            'Next

            Dim availableFlag As Boolean = False
            Dim smallestSF As Integer = 0
            Dim largestSF As Integer = 0
            Dim majorTenant As String = ""
            Dim majorTenantAll As String = ""
            Dim count As Integer = 0

            For Each y As DataRow In dt.Rows
                Dim dt1 As DataTable = ws.GetSpaceTenant(y("space_id"))
                Dim currentSF As Integer = CInt(y("sf"))



                If (dt1.Rows.Count = 0) Then
                    availableFlag = True
                    If (smallestSF = 0) Then
                        smallestSF = currentSF
                    Else
                        If (currentSF < smallestSF) Then
                            smallestSF = currentSF
                        End If
                    End If

                    If (largestSF = 0) Then
                        largestSF = currentSF
                    Else
                        If (currentSF > largestSF) Then
                            largestSF = currentSF
                        End If
                    End If
                Else
                    Dim dt2 As DataTable = ws.GetTenantDetail(dt1.Rows(0)("tenant_id"))


                    If (dt2.Rows(0)("major") = "1") Then
                        count += 1
                        If (count <= 3) Then
                            majorTenant = majorTenant & dt2.Rows(0)("shortName").ToString.Trim & ", "
                        End If
                        majorTenantAll = majorTenantAll & dt2.Rows(0)("shortName").ToString.Trim & ", "
                    End If




                End If


            Next

            If (availableFlag = True) Then
                If (smallestSF = largestSF) Then
                    literalAvailability.Text = FormatNumber(smallestSF.ToString, 0).ToString.Trim & " SF"
                Else
                    literalAvailability.Text = FormatNumber(smallestSF.ToString, 0).ToString.Trim & " - " & FormatNumber(largestSF.ToString, 0).ToString.Trim & " SF"
                End If
            End If

            If (majorTenant.Length > 0) Then
                majorTenant = majorTenant.Substring(0, majorTenant.Length - 2)
                majorTenantAll = majorTenantAll.Substring(0, majorTenantAll.Length - 2)
            End If
            If (count > 3) Then
                literalTenants.Text = majorTenant.ToString.Trim & ", <a href=""#"" class=""tenantTooltip"" title=""" & majorTenantAll & """>More...</a>"
            Else
                literalTenants.Text = majorTenant.ToString.Trim
            End If


        End If


    End Sub

    Public Sub searchCallback_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles searchCallback.Callback
        If (e.Parameters(0) = "paging") Then

            executeCallback(CInt(e.Parameters(1)), Session("searchFilter"), Session("searchSort"), Session("searchSortby"))
            Session("searchPaging") = e.Parameters(1)
        ElseIf (e.Parameters(0) = "sort") Then
            Dim sSortBy As String
            If Session("searchSortby") = "asc" Then
                sSortBy = "desc"
            Else
                sSortBy = "asc"
            End If
            executeCallback(Session("searchPaging"), Session("searchFilter"), e.Parameters(1), sSortBy)
            Session("searchSort") = e.Parameters(1)
            Session("searchSortby") = sSortBy
        ElseIf (e.Parameters(0) = "filter") Then
            Session("searchPaging") = "0"
            executeCallback(Session("searchPaging"), e.Parameters(1), Session("searchSort"), Session("searchSortby"))
            Session("searchFilter") = e.Parameters(1)

        End If


        placeholderSearchForm.RenderControl(e.Output)
        literalPaging.RenderControl(e.Output)
        RepeaterSearchResults.RenderControl(e.Output)
		literalPagingBottom.RenderControl(e.Output)
    End Sub

    Private Sub executeCallback(ByVal paging As Integer, ByVal filter As String, ByVal sort As String, ByVal sortby As String)

        Dim searchMode = Request.QueryString("search")

        Dim dt As DataTable = New DataTable

        Dim ws = New acadiaws.Service1

        If (searchMode = "search") Then
            Dim searchState = Request.QueryString("state").Trim
            Dim searchString = Request.QueryString("string").Trim

            If (searchString = "Keyword Search") Then
                searchString = ""
            End If

            If (searchState = "All States") Then
                searchState = ""
            End If

            dt = ws.ListProjects(searchString.Trim, "", "", "", "", "", "", searchState.Trim)

            If (searchString = "") Then
                If (searchState = "") Then
                    literalSearchTerms.Text = "All Properties"
                Else
                    literalSearchTerms.Text = searchState
                End If
            Else
                If (searchState = "") Then
                    literalSearchTerms.Text = searchString
                Else
                    literalSearchTerms.Text = searchState & " and " & searchString
                End If

            End If



        Else
            dt = ws.ListProjects("", "", "", "", "", "", "", "")
            literalSearchTerms.Text = "All Properties"
        End If

        Dim dv As DataView = New DataView(dt)
        'name, location, gla, availability
        Dim sSort As String
        Select Case sort
            Case "name"
                sSort = "name"
            Case "location"
                sSort = "state_id"
            Case "gla"
                sSort = "gla"
            Case Else
                sSort = "state_id"
        End Select

        dv.Sort = sSort & " " & sortby


        If (filter <> "") Then
            dv.RowFilter = "name LIKE '%" & filter & "%'"

        End If




        Dim dTbl As DataTable = dv.Table

        Dim pgDataSc As PagedDataSource = New PagedDataSource
        pgDataSc.DataSource = dv
        pgDataSc.AllowPaging = True
        pgDataSc.PageSize = CInt(ConfigurationSettings.AppSettings("searchSize"))
        pgDataSc.CurrentPageIndex = paging

        RepeaterSearchResults.DataSource = pgDataSc
        RepeaterSearchResults.DataBind()

        Dim pageCount As Integer = pgDataSc.PageCount

        Dim pagingString As String = ""
        If pageCount > 0 Then

            For i As Integer = 0 To pageCount - 1
                If (i = pgDataSc.CurrentPageIndex) Then
                    pagingString = pagingString & "<b>" & (i + 1) & "</b> "
                Else
                    pagingString = pagingString & "<a href=""javascript:switchPage(" & i & ");"">" & (i + 1) & "</a> "
                End If

            Next
        End If
        literalPaging.Text = pagingString
literalPagingBottom.Text = pagingString

    End Sub
End Class
