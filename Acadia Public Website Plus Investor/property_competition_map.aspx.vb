﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data

Partial Class property_competition_map
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim ws = New acadiaws.Service1
        Dim ws2 = New acadiaws.Service1

        Dim theProjectID As String = Request.QueryString("id")

        Dim dt As DataTable = ws.ListProjects("", "", "", "", "", "", theProjectID, "")

        If (dt.Rows.Count > 0) Then
            literalProjectCrumbs.Text = dt.Rows(0)("name").ToString.Trim

            Page.Header.Title = "Acadia Realty Trust - Competition Map - " & dt.Rows(0)("name").ToString.Trim & " - " & dt.Rows(0)("city").ToString.Trim & ", " & dt.Rows(0)("state_id").ToString.Trim

            literalProjectName.Text = dt.Rows(0)("name").ToString.Trim
            literalProjectLocation.Text = dt.Rows(0)("city").ToString.Trim & ", " & dt.Rows(0)("state_id").ToString.Trim & " " & dt.Rows(0)("zip").ToString.Trim
        End If


        Dim dtAssets = ws.GetProjectAssets(theProjectID)


        For Each x As DataRow In dtAssets.Rows
            If (x("catname") = "Competition Map") Then
                imageCompetitionPlan.ImageUrl = Session("WSRetreiveAsset") & "qfactor=25&width=900&height=1450&size=1&type=asset&id=" & x("asset_id")
                hyperlinkImage1.NavigateUrl = (Session("WSRetreiveAsset") & "qfactor=25&width=1200&height=1000&size=1&type=asset&id=" & x("asset_id")).ToString.Replace("(", "%28").Replace(")", "%29")
                imageCompetitionPlan.AlternateText = x("name")
                imageCompetitionPlan.ImageAlign = ImageAlign.Middle
            End If
        Next
        hyperlinkImage1.Attributes.Add("rel", "lightbox")
        hyperlinkImage1.Attributes.Add("title", dt.Rows(0)("name").ToString.Trim)


 
    End Sub
End Class
