﻿Imports Microsoft.VisualBasic

Imports System.Data
Imports System.IO
Imports System.Xml
Imports System.Data.OleDb

Public Class Functions

    Public Shared Function ConvertDataSetToXMLDataSource(ByVal XMLURL As String, ByVal XMLFileName As String) As Boolean
        Dim _DSet As DataSet = New DataSet()
        Dim _DS As XmlDataSource = New XmlDataSource
        If File.GetLastWriteTime(ConfigurationSettings.AppSettings("XMLLocation") & XMLFileName) < DateTime.Now.AddMinutes(-10) Then

            Dim doc As XmlDocument = New XmlDocument()
            doc.Load(XMLURL)
            ' Load the XML data from a reader object.
            ' Ignore the white spaces.
            doc.PreserveWhitespace = False
            doc.Save(ConfigurationSettings.AppSettings("XMLLocation") & XMLFileName)
            'old way
            '_DSet.ReadXml(XMLURL)
            '_DSet.WriteXml(ConfigurationSettings.AppSettings("XMLLocation") & XMLFileName)
        End If

        Return True
    End Function




    Public Shared Function setNewTemporaryPassword(ByVal userid As String) As String
        Dim _tmp_password_item_id As String = Functions.CheckAddUserUDF("sp_createnewuserfield", "Temporary Password", "System", "IDAM_TEMPORARY_PASSWORD", "", 1, "Security", 1, 1)
        Dim _use_password_item_id As String = Functions.CheckAddUserUDF("sp_createnewuserfield", "Use Temporary Password", "System", "IDAM_USE_TEMPORARY_PASSWORD", "", 10, "Security", 1, 1)
        Dim _newPassword As String = ""
        _newPassword = System.Guid.NewGuid.ToString
        ExecuteTransaction("delete from ipm_user_field_value where item_id = " & _tmp_password_item_id & " and user_id = " & userid.Trim)
        ExecuteTransaction("insert into ipm_user_field_value  (user_id,item_id,item_value) values ('" & userid.Trim & "'," & _tmp_password_item_id & ",'" & _newPassword & "')")

        ExecuteTransaction("delete from ipm_user_field_value where item_id = " & _use_password_item_id & " and user_id = " & userid.Trim)
        ExecuteTransaction("insert into ipm_user_field_value  (user_id,item_id,item_value) values ('" & userid.Trim & "'," & _use_password_item_id & ",'1')")

        Return _newPassword
    End Function

    Public Shared Sub unsetTemporaryPassword(ByVal userid As String)
        Dim _use_password_item_id As String = Functions.CheckAddUserUDF("sp_createnewuserfield", "Use Temporary Password", "System", "IDAM_USE_TEMPORARY_PASSWORD", "", 10, "Security", 1, 1)
        ExecuteTransaction("delete from ipm_user_field_value where item_id = " & _use_password_item_id & " and userid = " & userid)
        ExecuteTransaction("insert into ipm_user_field_value  (userid,item_id,item_value) values (" & userid & "," & _use_password_item_id & ",'0')")
    End Sub


    'sp_createnewprojectfield
    Public Shared Function CheckAddUserUDF(ByVal sUseStoredPro, ByVal item_Name, ByVal item_tab, ByVal item_tag, ByVal Item_Description, ByVal Item_type, ByVal item_group, ByVal viewable, ByVal editable) As String
        Dim dt As New DataTable
        dt = GetDataTable("select item_id from ipm_user_field_desc where item_tag = '" & item_tag & "'", ConfigurationSettings.AppSettings("CONNECTIONSTRINGWRITE"))
        If dt.Rows.Count.Equals(0) Then
            Dim sql As String
            Dim sKeyUse As String
            sql = "exec {0} "
            sql += "1,"
            sql += "'" + item_tag.ToString().Replace("'", "''") + "', "
            sql += "'" + Item_Description.ToString().Replace("'", "''") + "', "
            sql += "'', "
            sql += "'" + Item_type.ToString().Replace("'", "''") + "', "
            sql += "'" + item_group.ToString().Replace("'", "''") + "', "
            sql += "'User Field', "
            sql += viewable.ToString() + ", "
            sql += editable.ToString() + ", "
            sql += "'" + item_Name.ToString().Replace("'", "''") + "'"
            sql += ",'" + item_tab.ToString().Replace("'", "''") + "' "
            sql = String.Format(sql, sUseStoredPro)
            ExecuteTransaction(sql)
            'get maxid
            Return GetDataTable("select item_id from ipm_user_field_desc where item_tag = '" & item_tag & "'", ConfigurationSettings.AppSettings("CONNECTIONSTRINGWRITE")).Rows(0)("item_id")
        Else
            Return dt.Rows(0)("item_id")
        End If

    End Function


    Public Shared Function GetDataTable(ByVal query As String, ByVal ConnectionString As String) As DataTable
        'Dim connection1 As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & System.Web.HttpContext.Current.Server.MapPath(dbFile))
        Dim connection1 As OleDbConnection = New OleDbConnection(ConfigurationSettings.AppSettings("CONNECTIONSTRINGWRITE"))
        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)
        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()

        End Try
        Return table1
    End Function

    Public Shared Sub ExecuteTransaction(ByVal sqlstring As String)
        Dim ConnectionString As String = ConfigurationSettings.AppSettings("CONNECTIONSTRINGWRITE")
        Dim connection As New OleDbConnection(ConnectionString)
        Dim command As New OleDbCommand
        Dim transaction As OleDbTransaction

        ' Set the Connection to the new OleDbConnection.
        command.Connection = connection

        ' Open the connection and execute the transaction.
        Try
            connection.Open()

            ' Start a local transaction with ReadCommitted isolation level.
            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            ' Assign transaction object for a pending local transaction.
            command.Connection = connection
            command.Transaction = transaction

            ' Execute the commands.
            command.CommandText = sqlstring
            command.ExecuteNonQuery()

            ' Commit the transaction.
            transaction.Commit()
            Console.WriteLine("Executed.")

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Try to rollback the transaction
            Try
                transaction.Rollback()

            Catch
                ' Do nothing here; transaction is not active.
            End Try
        End Try
        ' The connection is automatically closed when the
        ' code exits the Using block.

    End Sub


End Class
