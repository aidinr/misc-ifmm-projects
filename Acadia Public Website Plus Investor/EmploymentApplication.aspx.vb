﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.IO


Partial Class EmploymentApplication
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

    End Sub

    Function isEmail(ByVal inputEmail As String) As Boolean

        inputEmail = inputEmail.ToString()
        Dim strRegex As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
        Dim re As Regex = New Regex(strRegex)
        If (re.IsMatch(inputEmail)) Then
            Return True
        Else
            Return False
        End If

    End Function

    Sub btnSubmit_click(ByVal sender As Object, ByVal e As EventArgs) Handles submitApp.Click
        If Page.IsValid Then

            Dim department As String = formDepartment.SelectedValue

            Dim firstName As String = formFirstName.Text
            Dim lastName As String = formLastName.Text
            Dim address As String = formAddress.Text
            Dim city As String = formCity.Text
            Dim state As String = formState.SelectedValue
            Dim zip As String = formZip.Text
            Dim phone As String = formPhone.Text  
            Dim email As String = formEmail.Text
            Dim bestway As String = formBestWay.SelectedValue

            Dim experience As String = formExperience.Text
            Dim currentTitle As String = formCurrentTitle.Text
            Dim education As String = formEducation.SelectedValue
            Dim eligible As String = formEligible.SelectedValue

            Dim mail As MailMessage = New MailMessage
            mail.From = New MailAddress("support@ifmm.com", "Acadia Realty Employment Application Info")
            mail.Subject = "Acadia online employment application"
            mail.To.Add(New MailAddress("mburlinson@ifmm.com", "Mark Burlinson"))
            mail.To.Add(New MailAddress("careers@acadiarealty.com", "Careers"))


            Dim theMessage As String = "=====================================================" & vbCrLf
            theMessage = theMessage & "ACADIA REALTY TRUST ONLINE EMPLOYMENT APPLICATION FORM" & vbCrLf
            theMessage = theMessage & "======================================================" & vbCrLf
            theMessage = theMessage & vbCrLf & vbCrLf
            theMessage = theMessage & "The following application has been submitted online:" & vbCrLf
            theMessage = theMessage & vbCrLf & vbCrLf
            theMessage = theMessage & "Department: " & department & vbCrLf
            theMessage = theMessage & vbCrLf
            theMessage = theMessage & "First Name: " & firstName & vbCrLf
            theMessage = theMessage & "Last Name: " & lastName & vbCrLf
            theMessage = theMessage & "Address: " & address & vbCrLf
            theMessage = theMessage & "City: " & city & vbCrLf
            theMessage = theMessage & "State: " & state & vbCrLf
            theMessage = theMessage & "Zip: " & zip & vbCrLf
            theMessage = theMessage & "Daytime Phone: " & phone & vbCrLf
            theMessage = theMessage & "Email: " & email & vbCrLf
            theMessage = theMessage & "Best way to contact: " & bestway & vbCrLf
            theMessage = theMessage & vbCrLf
            theMessage = theMessage & "Experience: " & experience & vbCrLf
            theMessage = theMessage & "Current Title: " & currentTitle & vbCrLf
            theMessage = theMessage & "Education: " & education & vbCrLf
            theMessage = theMessage & "Eligible to work in the US: " & eligible & vbCrLf

            theMessage = theMessage & vbCrLf & vbCrLf
            theMessage = theMessage & "Attached with this application are the following file(s):"
            theMessage = theMessage & vbCrLf & vbCrLf

            If (formResume.HasFile) Then
                Dim theResumeFileName As String = Path.GetFileName(formResume.PostedFile.FileName)
                theMessage = theMessage & "Resume file: " & theResumeFileName & vbCrLf
                Dim resumeAttachment As New Attachment(formResume.FileContent, theResumeFileName)
                mail.Attachments.Add(resumeAttachment)
            End If

            If (formCoverLetter.HasFile) Then
                Dim theResumeFileName As String = Path.GetFileName(formCoverLetter.PostedFile.FileName)
                theMessage = theMessage & "Cover Letter file: " & theResumeFileName & vbCrLf
                Dim resumeAttachment As New Attachment(formCoverLetter.FileContent, theResumeFileName)
                mail.Attachments.Add(resumeAttachment)
            End If
            theMessage = theMessage & vbCrLf & vbCrLf
            theMessage = theMessage & "NOTICE: SCAN ALL FILES WITH UP TO DATE ANTI VIRUS SOFTWARE BEFORE OPENING!"
            theMessage = theMessage & vbCrLf & vbCrLf

            theMessage = theMessage & "======================================================" & vbCrLf
            theMessage = theMessage & "END OF APPLICATION" & vbCrLf
            theMessage = theMessage & "======================================================" & vbCrLf

            mail.Body = theMessage

            Dim smtp As New SmtpClient()

            Try
                smtp.Send(mail)
                labelError.Text = "<span style=""color:red;font-weight:bold;"">Thank you for your application.</span><br /><br />"
            Catch ex As Exception
                labelError.Text = "<span style=""color:red;font-weight:bold;"">A technical error has occured. Please try again later.</span><br /><br />"
            End Try



        Else
            labelError.Text = "<span style=""color:red;font-weight:bold;"">A technical error has occured. Please try again later.</span><br /><br />"
        End If


    End Sub

End Class
