﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.IO




Partial Class investor_relations
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        'begin stock prices


        Dim ws = New acadiaws.Service1
        Dim dtAssets = ws.GetProjectAssets("2405123")

        For Each x As DataRow In dtAssets.Rows
            If (x("catname") = "Page Graphics") Then
                If x("name") = "Investor Relations Right Side Graphic" Then
                    imgsideimages.ImageUrl = Session("WSRetreiveAsset") & "qfactor=25&width=351&height=528&crop=1&size=1&type=asset&id=" & x("asset_id") & "&cache=1"
                End If
            End If
        Next
        If imgsideimages.ImageUrl = "" Then
            imgsideimages.ImageUrl = "images/main_ir.jpg"
        End If



        Dim xmlDS As DataSet = New DataSet()


        ''Dim ds As DataSet = ws.GetT1Feed("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=quotes")
        'http://xml.corporate-ir.net/irxmlclient.asp?compid=######&reqtype=quotes 

        xmlDS.ReadXml("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=quotes")
        'xmlDS.ReadXml(ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("QuoteXMLFileName"))

        Dim dt As DataTable = New DataTable()
        dt = xmlDS.Tables("Stock_Quote")

        Dim dtDate = xmlDS.Tables("Date")


        For Each x As DataRow In dt.Rows
            If (x("ticker") = "AKR") Then



                Dim changeString As String = x("change").ToString.Trim & " (" & Math.Round((x("change") / x("open")) * 100, 3) & "%)"
                Try

                    Dim change As Double = Convert.ToDouble(x("change").ToString.Trim)


                    If (change < 0) Then
                        imageChange.ImageUrl = "images/down.gif"
                        literalChange.Text = "<span style=""color:red"">" & changeString & "</span>"
                    Else
                        imageChange.ImageUrl = "images/up.gif"
                        literalChange.Text = "<span style=""color:green"">" & changeString & "</span>"
                    End If

                Catch ex As Exception
                    literalChange.Text = changeString
                End Try


                literalClass.Text = x("class").ToString.Trim
                literalExchange.Text = x("exchange").ToString.Trim
                literalCurrency.Text = x("currency").ToString.Trim
                literalVolume.Text = FormatNumber(x("volume").ToString.Trim, 0)
                literalTicker.Text = x("ticker").ToString.Trim
                literalPrice.Text = x("trade").ToString.Trim


                For Each y As DataRow In dtDate.Rows
                    If (y("stock_quote_id") = x("stock_quote_id")) Then
                        literalDate.Text = y("date_text").ToString.Trim
                    End If

                Next


                Exit For
            End If
        Next


        'begin company ir profile

        Dim CompanyDS As DataSet = New DataSet()
        'CompanyDS.ReadXml(ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("CompanyXMLFileName"))
        CompanyDS.ReadXml("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=company")

        Dim dtCompany As DataTable = New DataTable()
        Dim dtCompanyProfile As DataTable = New DataTable()


        dtCompanyProfile = CompanyDS.Tables("CompanyProfile")


        literalCompany.Text = dtCompanyProfile.Rows(0)("companyprofile_text")


        'begin news



        'Dim CommitteeDS As XmlDataSource = New XmlDataSource
        'CommitteeDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("CommitteeXMLFileName")
        'CommitteeDS.XPath = "IRXML/Committees/Committee"




        Dim newsDS As XmlDataSource = New XmlDataSource


        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=newsreleases", ConfigurationSettings.AppSettings("NewsXMLFileName"))
        newsDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("NewsXMLFileName")
        newsDS.XPath = "IRXML/NewsReleases/NewsCategory/NewsRelease[position()<=" & ConfigurationSettings.AppSettings("numberOfNewsDisplayedAtIRHomePage") & "]"



        repeaterNews.DataSource = newsDS
        repeaterNews.DataBind()


        'Dim newsDS As DataSet = New DataSet()
        'newsDS.ReadXml(ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("NewsXMLFileName"))

        'Dim dtNews As DataTable = New DataTable




        'dtNews = newsDS.Tables("NewsRelease")

        'Dim rowCount As Integer = dtNews.Rows.Count
        'rowCount -= Integer.Parse(ConfigurationSettings.AppSettings("numberOfNewsDisplayedAtIRHomePage")) + 1

        'For i As Integer = 0 To rowCount
        '    dtNews.Rows.RemoveAt(dtNews.Rows.Count - 1)
        'Next

        'Dim counter As Integer = 0




        'repeaterNews.datasource = dtNews
        'repeaterNews.databind()




        'begin events

        'Dim eventsDS As DataSet = New DataSet()
        'eventsDS.ReadXml(ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("EventsXMLFileName"))

        'Dim dtEvents As DataTable = New DataTable

        'dtEvents = eventsDS.Tables("Event")

        'Dim deleteArray = New ArrayList


        'Dim i As Integer = 0

        'For Each x As DataRow In dtEvents.Rows
        '    'Dim beginShowDate As DateTime = New DateTime()
        '    'Dim endShowDate As DateTime = New DateTime()
        '    'Dim eventEndDate As DateTime = New DateTime()

        '    'Dim provider As CultureInfo = CultureInfo.InvariantCulture

        '    'beginShowDate = DateTime.ParseExact(x("EventDisplayFromDate").ToString.Trim & " " & x("EventDisplayFromTime").ToString.Trim, "yyyyMMdd HH:mm:ss", provider)
        '    'endShowDate = DateTime.ParseExact(x("EventDisplayToDate").ToString.Trim & " " & x("EventDisplayToTime").ToString.Trim, "yyyyMMdd HH:mm:ss", provider)
        '    'eventEndDate = DateTime.ParseExact(x("EventEndDate").ToString.Trim, "yyyyMMdd", provider)

        '    If (x("EventStatus") = "0") Then

        '    Else
        '        deleteArray.Add(i)
        '    End If

        '    i += 1

        'Next

        'i = 0

        'For Each x As Integer In deleteArray
        '    dtEvents.Rows.RemoveAt(x - i)
        '    i += 1
        'Next

        'repeaterEvents.DataSource = dtEvents

        'repeaterEvents.DataBind()



    End Sub

    ' Use this function to get xml string from a dataset 
    Public Function ConvertDataSetToXML(ByVal xmlDS As DataSet) As String
        Dim stream As MemoryStream
        Dim writer As XmlTextWriter
        
        Try

            stream = New MemoryStream()
            ' Load the XmlTextReader from the stream   
            writer = New XmlTextWriter(stream, Encoding.Unicode)
            ' Write to the file with the WriteXml method.   
            xmlDS.WriteXml(writer)
            Dim count As Integer = stream.Length
            Dim arr As Byte() '= New Byte(count)
            stream.Seek(0, SeekOrigin.Begin)
            stream.Read(arr, 0, count)
            Dim utf As UnicodeEncoding = New UnicodeEncoding

            Return utf.GetString(arr).Trim()

        Catch

            Return String.Empty

        Finally

            If Not writer Is Nothing Then
                writer.Close()
            End If

        End Try

    End Function

    'Public Sub repeaterNewsItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
    '    If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
    '        Dim literalAvailability As Literal = e.Item.FindControl("literalAvailability")


    '    End If
    'End Sub


End Class
