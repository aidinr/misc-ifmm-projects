﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="client_home" CodeFile="~/client_home.aspx.vb"  EnableViewState="true"%>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        @import url("css/style.css");
        @import url("tabStyle.css");
    </style>
    <!--<script src="http://cdn.jquerytools.org/1.2.2/full/jquery.tools.min.js" type="text/javascript"></script>-->
    <link href="css/jquery-ui.css"
        rel="stylesheet" type="text/css" />

    <script src="js/jquery.min.js" type="text/javascript"></script>

    <script src="js/jquery-ui.min.js"
        type="text/javascript"></script>

    <link href="css/ui-lightness.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery.perciformes.js" type="text/javascript"></script>

    <script src="js/navi.js" type="text/javascript"></script>

<script type="text/javascript" src="js/password.js"></script> 

    <script>

        function GetFileList(sender, eventArgs) {

            CallBackFileList.Callback(TabStrip1.getSelectedTab().ID);
        }

        function GetFiles(cid) {

            CallBackFiles.Callback(cid);
        }






        function rebindfilelist() {



            $('.filelistsubheading').hover(
  function() {
      $(this).addClass("filelistsubheadinghover");
  },
  function() {
      $(this).removeClass("filelistsubheadinghover");
  }
);



            $('.filelistsubheading').click(
  function() {
      $('.filelistsubheading').removeClass("filelistsubheadinghover");
      $('.filelistsubheading').removeClass("filelistsubheadingselect");
      $(this).addClass("filelistsubheadingselect");
  }
);


        }

    
    
    </script>

    <style>
        #property_search
        {
            position: absolute;
            top: 98px;
            left: 50%;
            margin-left: 277px;
            z-index: 1006;
            background: #f9a131;
        }
        .filelistheading
        {
            color: orange;
            font-weight: bold;
            font-size: 12px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            padding-bottom: 8px;
            padding-top: 15px;
        }
        .filelistsubheading
        {
            color: #1A4473;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            padding-left: 5px;
            border-bottom: dotted 1px #1A4473;
            width: 400px;
            font-size: 11px;
        }
        .filelistsubheadinghover
        {
            color: orange;
            background-color: #00224b;
            cursor: pointer;
            text-decoration: none;
        }
        .filelistsubheadinghover a
        {
            color: orange;
            background-color: #00224b;
            cursor: pointer;
            text-decoration: none;
        }
        .filelistsubheadingselect
        {
            color: #1A4473;
            background-color: #dbe0e7;
            cursor: pointer;
            text-decoration: none;
        }
        .filelistingheading
        {
            color: black;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-weight: bold;
            padding-left: 5px;
            border-bottom: dotted 1px #1A4473;
            width: 300px;
            font-size: 11px;
        }
        .filelistingfile
        {
            color: #1A4473;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            padding-left: 5px;
            border-bottom: dotted 1px #1A4473;
            width: 300px;
            font-size: 11px;
            cursor: pointer;
        }
        .updateprofilebutton
        {
            color: white;
            background-color: #1A4473;
            font-family: Verdana;
            font-size: 11px;
            cursor: pointer;
            width: 113px;
            padding: 5px 9px;
            float:left;
            margin-right:20px;
            text-align:center;
        }
                .updateprofilebuttonundo
        {
       
 
            cursor: pointer;
            width: 113px;
            padding: 5px 9px;
            float:left;
            margin-right:20px;
            text-align:center;
        }
        
        
        
        .updateprofilebuttonhover
        {
            color: #1A4473;
            background-color: #DBE0E7;
        }
    </style>
    <!--[if lte IE 7]>
<style type="text/css">
html, body {
    overflow-x: hidden;
}
</style>
<![endif]-->
    <!--[if IE 7]>
<style>
#jsddm {
    margin: 0;
    padding: 0;
    top: 0px;
    left: 0px;
    position: relative;
    float: left;
    z-index: 1;
}

.body_content {
    background: #ffffff;
    color: #000000;
    font-size: 12px;
    line-height: 20px;
    position:relative;
    top: 20px;
    left:1px;
}

#property_search {
    position: absolute;
    top:122px;
    left: 50%;
    margin-left: 277px;
    z-index: 1;	
    background: #f9a131;
}


</style>
<![endif]-->
    <style type="text/css">
        @-moz-document@-moz-documenturl-prefix(){
        #jsddm{margin:0;padding:0;top:0px;left:0px;position:relative;float:left;z-index:1007;
        }
        .body_content
        {
            background: #ffffff;
            color: #000000;
            font-size: 12px;
            line-height: 20px;
            position: relative;
            top: 25px;
            left: 1px;
        }
        #property_search
        {
            position: absolute;
            top: 102px;
            left: 50%;
            margin-left: 277px;
            z-index: 1006;
            background: #f9a131;
        }
        #header
        {
            position: relative;
            margin-top: 19px;
            margin-bottom: 14px;
        }
        }
        .auto-style1
        {
            border-width: 0;
            background-color: #DBE0E7;
        }
        .auto-style2
        {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: large;
            background-color: #DBE0E7;
            padding-top: 15px;
	color: #6C7F98;
}
        .auto-style3 a {
	    color: #FFFFFF;
    }
        
.InputFieldMain
{
    color:#6D6D6D;border:1px  dotted #B0AEAE;padding:5px;
    width:300px;background-color:white;
}

select
{
  font-family: Verdana; 
  font-size:11px;
}







        .auto-style3 {
	font-size: x-large;
	color: #EB8F00;
}
.auto-style4 {
	background-color: #EB8F00;
}
.auto-style5 {
	background-color: #FFFFFF;
}







        </style>
    <title>Acadia Realty Trust - Client Site</title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="container">
        <!--property search-->
        <!-- end of property search-->
        <div id="header">
            <table width="100%" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td width="424" valign="top">
                        <a href="default.aspx">
                            <img class="navi" src="images/logo.jpg" /></a>
                    </td>
                    <td valign="top" align="right">
                        <!--menu-->
                        <!--menu-->
                	    WELCOME <%=session("userfirstname").tostring.toupper%> <%=session("userlastname").tostring.toupper%> | 
					    <a href="client_login.aspx?logout=1" >LOG OUT</a></td>
                </tr>
            </table>
        </div>
        <div id="body">
            <div class="body_content">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td valign="top">
                            <div class="general_title">
                            </div>
                            <div class="general_content_sas" style="width: 929px">
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                               <br />
                                <span class="auto-style3">
								<span class="auto-style5"><br />
                               FUND REPORTING
                                
                                </span><span class="auto-style4">
								<span class="auto-style5">
                                
                                <!-- Welcome
                                                <%=Session("userfirstname") %>!-->
                                                
                                
                                
                                </span></span></span>
                                                
                                
                                
                                <br /><br /><br />
                                <ComponentArt:TabStrip ID="TabStrip1" runat="server" CssClass="TopGroup" DefaultItemLookId="DefaultTabLook"
                                    DefaultSelectedItemLookId="SelectedTabLook">
                                    <ItemLooks>
                                        <ComponentArt:ItemLook LookId="DefaultTabLook" CssClass="DefaultTab" HoverCssClass="DefaultTabHover"
                                            LabelPaddingLeft="25" LabelPaddingRight="25" LabelPaddingTop="5" LabelPaddingBottom="2"
                                            LeftIconWidth="25" LeftIconHeight="28" />
                                        <ComponentArt:ItemLook LookId="SelectedTabLook" CssClass="SelectedTab" LabelPaddingLeft="25"
                                            LabelPaddingRight="25" LabelPaddingTop="5" LabelPaddingBottom="2" LeftIconWidth="25"
                                            LeftIconHeight="28" />
                                    </ItemLooks>
                                    <ClientEvents>
                                        <TabSelect EventHandler="GetFileList" />
                                    </ClientEvents>
                                </ComponentArt:TabStrip>
                                <div style="width: 100%; height: 4px; border-bottom: 4px #1A4473 solid; margin-top: -4px;">
                                </div>
                                
                                
                                
                                
                                
                                
                                 <div id="myaccount"  >
                                
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width: 515px; height: 100%;" valign="top" >
                                            <div style="padding-left:15px;">
                                            <br /><br /><br />
                                            <asp:Literal ID="ltrlnotification" runat="server"></asp:Literal>
                                                <%=Session("userfirstname") %>
                                                <%=Session("userlastname")%><br />
                                                <%=Session("usercompany")%><br />
                                                <%=Session("usertitle")%><br />
                                                <%=Session("useraddress")%><br />
                                                <%=Session("usercity")%>,&nbsp; 
                                                <%=Session("userstate")%>&nbsp;
                                                <%=Session("userzip")%><br />
                                                
                                                <%  If Session("userphone") <> "" Then
                                                        Response.Write("Phone: " & Session("userphone"))
                                                    End If
                                                   %>
                                               <br />
                                                <%  If Session("userfax") <> "" Then
                                                                Response.Write("Fax: " & Session("userfax"))
                                                    End If
                                                   %>
                                            <br /><br /><br />
                                            </div>
                                              <div id="create-user" class="updateprofilebuttonundo">
                                                  <img src="images/button_update_profile-1.png" /></div>&nbsp;<div id="update-password" class="updateprofilebuttonundo">
                                                      <img src="images/button_change_password-1.png" /></div>
                                        </td>
                                        <td style="min-height: 500px; width: 400px; padding-left: 15px; height: 100%;" valign="top" class="auto-style1">
                                           
                                           
                                           
                                            <div id="divmodifyaccount" style="display:none;padding:20px;padding-right:0px; width: 300px;">              
                                        <b>Modify Account Information </b>     <br /><br />
       <table style="width: 100%">
       <tr>
            <td style="height: 26px">
                Title
            </td>
            <td style="height: 26px">
                <strong>
                    <asp:TextBox ID="txtTitle"  runat="server" CausesValidation="True" 
                        Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtTitle"
                        ErrorMessage="RequiredFieldValidator">*</asp:RequiredFieldValidator>
                </strong>
            </td>
        </tr>
        <tr>
            <td>
                Address
            </td>
            <td>
                <asp:TextBox ID="txtAddress" runat="server"  Width="200px" Text=""></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAddress"
                    ErrorMessage="RequiredFieldValidator">*</asp:RequiredFieldValidator>
                    
            </td>        </tr>
        <tr>
            <td style="height: 26px">
                City
            </td>
            <td style="height: 26px">
                <strong>
                    <asp:TextBox ID="txtCity"  runat="server" CausesValidation="True" 
                        Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCity"
                        ErrorMessage="RequiredFieldValidator">*</asp:RequiredFieldValidator>
                </strong>
            </td>
        </tr>
        <tr>
            <td style="height: 26px">
                State
            </td>
            <td style="height: 26px">
            
            
          <asp:DropDownList ID=drpdnstate runat=server >
<asp:ListItem Value="AL" Text=""></asp:ListItem>
<asp:ListItem Value="AL" Text="Alabama"></asp:ListItem>
<asp:ListItem Value="AK" Text="Alaska"></asp:ListItem>
<asp:ListItem Value="AZ" Text="Arizona"></asp:ListItem>
<asp:ListItem Value="AR" Text="Arkansas"></asp:ListItem>
<asp:ListItem Value="CA" Text="California"></asp:ListItem>
<asp:ListItem Value="C0" Text="Colorado"></asp:ListItem>
<asp:ListItem Value="CT" Text="Connecticut"></asp:ListItem>
<asp:ListItem Value="DE" Text="Delaware"></asp:ListItem>
<asp:ListItem Value="DC" Text="District of Columbia"></asp:ListItem>
<asp:ListItem Value="FL" Text="Florida"></asp:ListItem>
<asp:ListItem Value="GA" Text="Georgia"></asp:ListItem>
<asp:ListItem Value="GM" Text="Guam"></asp:ListItem>
<asp:ListItem Value="HI" Text="Hawaii"></asp:ListItem>
<asp:ListItem Value="ID" Text="Idaho"></asp:ListItem>
<asp:ListItem Value="IL" Text="Illinois"></asp:ListItem>
<asp:ListItem Value="IN" Text="Indiana"></asp:ListItem>
<asp:ListItem Value="IA" Text="Iowa"></asp:ListItem>
<asp:ListItem Value="KA" Text="Kansas"></asp:ListItem>
<asp:ListItem Value="KY" Text="Kentucky"></asp:ListItem>
<asp:ListItem Value="LA" Text="Louisiana"></asp:ListItem>
<asp:ListItem Value="ME" Text="Maine"></asp:ListItem>
<asp:ListItem Value="MD" Text="Maryland"></asp:ListItem>
<asp:ListItem Value="MA" Text="Massachusetts"></asp:ListItem>
<asp:ListItem Value="MI" Text="Michigan"></asp:ListItem>
<asp:ListItem Value="MN" Text="Minnesota"></asp:ListItem>
<asp:ListItem Value="MS" Text="Mississippi"></asp:ListItem>
<asp:ListItem Value="MO" Text="Missouri"></asp:ListItem>
<asp:ListItem Value="MT" Text="Montana"></asp:ListItem>
<asp:ListItem Value="NE" Text="Nebraska"></asp:ListItem>
<asp:ListItem Value="NV" Text="Nevada"></asp:ListItem>
<asp:ListItem Value="NH" Text="New Hampshire"></asp:ListItem>
<asp:ListItem Value="NJ" Text="New Jersey"></asp:ListItem>
<asp:ListItem Value="NM" Text="New Mexico"></asp:ListItem>
<asp:ListItem Value="NY" Text="New York"></asp:ListItem>
<asp:ListItem Value="NC" Text="North Carolina"></asp:ListItem>
<asp:ListItem Value="ND" Text="North Dakota"></asp:ListItem>
<asp:ListItem Value="OH" Text="Ohio"></asp:ListItem>
<asp:ListItem Value="OK" Text="Oklahoma"></asp:ListItem>
<asp:ListItem Value="OR" Text="Oregon"></asp:ListItem>
<asp:ListItem Value="PA" Text="Pennsylvania"></asp:ListItem>
<asp:ListItem Value="RI" Text="Rhode Island"></asp:ListItem>
<asp:ListItem Value="SC" Text="South Carolina"></asp:ListItem>
<asp:ListItem Value="SD" Text="South Dakota"></asp:ListItem>
<asp:ListItem Value="TN" Text="Tennessee"></asp:ListItem>
<asp:ListItem Value="TX" Text="Texas"></asp:ListItem>
<asp:ListItem Value="UT" Text="Utah"></asp:ListItem>
<asp:ListItem Value="VT" Text="Vermont"></asp:ListItem>
<asp:ListItem Value="VA" Text="Virginia"></asp:ListItem>
<asp:ListItem Value="WA" Text="Washington"></asp:ListItem>
<asp:ListItem Value="WV" Text="West Virginia"></asp:ListItem>
<asp:ListItem Value="WI" Text="Wisconsin"></asp:ListItem>
<asp:ListItem Value="WY" Text="Wyoming"></asp:ListItem>



          
          </asp:DropDownList>
                                        
 
            
            
            
               
            </td>
        </tr>
        <tr>
            <td style="height: 26px">
                Zip
            </td>
            <td style="height: 26px">
                <strong>
                    <asp:TextBox ID="txtZip"  runat="server" CausesValidation="True" 
                        Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtZip"
                        ErrorMessage="RequiredFieldValidator">*</asp:RequiredFieldValidator>
                </strong>
            </td>
        </tr>
        <tr>
            <td style="height: 26px">
                Phone #
            </td>
            <td style="height: 26px">
                <strong>
                    <asp:TextBox ID="txtPhone"  runat="server" CausesValidation="True" 
                        Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPhone"
                        ErrorMessage="RequiredFieldValidator">*</asp:RequiredFieldValidator>
                </strong>
            </td>
        </tr>
        <tr>
            <td style="height: 26px">
                Fax #
            </td>
            <td style="height: 26px">
                <strong>
                    <asp:TextBox ID="txtFax"  runat="server" 
                        Width="200px"></asp:TextBox>
                  
                </strong>
            </td>
        </tr>
        
    </table>
    <br />
    <asp:imageButton ID="btnmodifyaccount2" runat="server" Text="Modify Account"  ImageUrl="images/button_modify_account-1.png" /><asp:imageButton ID="Button1" runat="server" Text="Cancel"  ImageUrl="images/button_cancel-1.png"  />
       
                                            
                                            
            </div>
                                           
                                           
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
           <div id="updatepassword" style="display:none;padding:20px;padding-right:0px; width: 300px;">              
                                  <b>Change Password</b>    <br />        <br />    
           <table style="width: 100%">
           <tr>
                <td style="height: 26px">
                    Existing Password
                </td>
                <td style="height: 26px">
                    <strong>
                        <asp:TextBox ID="txtexistingpassword" TextMode=Password  runat="server" CausesValidation="True" 
                            Width="150px"></asp:TextBox>
                       
                    </strong>
                </td>
            </tr>
            <tr>
                <td>
                    New Password
                </td>
                <td>
                    <asp:TextBox ID="txtnewpassword" runat="server" TextMode=Password   Width="150px" Text=""></asp:TextBox>

                        
                </td>
            </tr>
            <tr>
                <td style="height: 26px">
                    Re-Enter Password
                </td>
                <td style="height: 26px">
                    <strong>
                        <asp:TextBox ID="txtnewpassword2"  TextMode=Password  runat="server" CausesValidation="True" 
                            Width="150px"></asp:TextBox>
                       
                    </strong>
                </td>
            </tr>
            <tr>
                <td style="height: 26px">
                    Password Strength
                </td>
                <td style="height: 26px">
                   <div style="border: 0px solid gray; width: 154px;"> 
<div id="progressBar"  style="font-size: 1px; height: 20px; width: 0px; border: 1px solid white;"> 
</div> 
</div>
                </td>
            </tr>
           
            
        </table>
        
        <br />
        <asp:imageButton ID="btnupdatepassword" runat="server" Text="Update Password"   ImageUrl="images/button_update_password-1.png"    /><asp:imageButton ID="Button3" runat="server" Text="Cancel" ImageUrl="images/button_change_password-1.png"   />
           
                                                
                                                
                </div>
                                     
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                           
                                           
                                           
                                           
                                        </td>
                                    </tr>
                                </table>
                                
                                
                            <!--    <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width: 515px; height: 100%;" valign="top" >
</td><td style="min-height: 500px; width: 400px; padding-left: 15px; height: 100%;" valign="top" class="auto-style1"></td></tr></table>-->
                                
                                
                                
                                </div>
                                
                                
                                
                                
                                
                                
                                
                                
                              
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                <div id="fundinfo" style="float:left;position:relative;">
                                
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width: 515px; height: 58px;" valign="top">
                                            <ComponentArt:CallBack ID="CallBackFileList" runat="server">
                                                <ClientEvents>
                                                    <CallbackComplete EventHandler="rebindfilelist" />
                                                </ClientEvents>
                                                <Content>
                                                    <asp:Literal ID="LtrFileList" runat="server"></asp:Literal>
                                                </Content>
                                                <LoadingPanelClientTemplate>
                                                    <img src="spinner.gif" />
                                                </LoadingPanelClientTemplate>
                                            </ComponentArt:CallBack>
                                        </td>
                                        <td style="min-height: 500px; width: 400px; padding-left: 15px; height: 58px;" valign="top"
                                            class="auto-style1">
                                            <div class="auto-style2">
                                                AVAILABLE DOCUMENTS</div>
                                            <br />
                                            <ComponentArt:CallBack ID="CallBackFiles" runat="server">
                                                <Content>
                                                    Please select from the list to the left to access available downloadable documents.
                                                    <asp:Literal ID="LtrFileList2" runat="server"></asp:Literal>
                                                </Content>
                                                <LoadingPanelClientTemplate>
                                                    <img src="spinner.gif" />
                                                </LoadingPanelClientTemplate>
                                            </ComponentArt:CallBack>
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                                
                                
                                </div>
                                
                                
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div id="footer_container">
        <div id="footer">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top" width="520">
                        <div style="width: 400px;">
                            <big><b>Acadia Realty Trust</b></big><br />
                            Acadia Realty Trust Acadia Realty Trust (NYSE:AKR) is a fully integrated, self-managed
                            and self-administered equity REIT focused primarily on the ownership, acquisition,
                            redevelopment and management of retail properties, including neighborhood / community
                            shopping centers and mixed-use properties with retail components.
                            
                        </div>
                    </td>
                    <td valign="top" width="120">
                        &nbsp;
                    </td>
                    <td valign="top" width="120">
                        &nbsp;
                    </td>
                    <td valign="top">
                        <big><b>Corporate Headquarters</b></big><br />
                        1311 Mamaroneck Avenue<br />
                        Suite 260<br />
                        White Plains, NY 10605<br />
                        <a href="mailto:info@acadiarealty.com">info@acadiarealty.com</a><br />
                        <a target="_blank" href="http://maps.google.com/maps?f=d&daddr=1311 Mamaroneck Avenue, White Plains, NY 10605">
                            Google map directions</a><br />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <br />
                        <br />
                        © Copyright 2010 Acadia Realty Trust&nbsp;&nbsp; |&nbsp;&nbsp; <a href="terms.html"
                            target="_blank">Terms and Conditions </a>&nbsp;&nbsp; |&nbsp;&nbsp; <a href="privacy.html"
                                target="_blank">Privacy Statement </a>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        
        var timeout = 500;
        var closetimer = 0;
        var ddmenuitem = 0;

        function jsddm_open() {
            jsddm_canceltimer();
            jsddm_close();
            ddmenuitem = $(this).find('ul').css('visibility', 'visible');
        }

        function jsddm_close()
        { if (ddmenuitem) ddmenuitem.css('visibility', 'hidden'); }

        function jsddm_timer()
        { closetimer = window.setTimeout(jsddm_close, timeout); }

        function jsddm_canceltimer() {
            if (closetimer) {
                window.clearTimeout(closetimer);
                closetimer = null;
            } 
        }

        $(document).ready(function() {



            $('.updateprofilebutton').hover(
  function() {
      $(this).addClass("updateprofilebuttonhover");
  },
  function() {
      $(this).removeClass("updateprofilebuttonhover");
  }
);






            $("#dialog-form").dialog({
                autoOpen: false,
                height: 450,
                width: 350,
                modal: true,
                buttons: {
                    'Modify account': function() {

                        document.forms[0].submit();
                        //$(this).dialog('close');
                    },
                    Cancel: function() {
                        $(this).dialog('close');
                    }
                },
                close: function() {

                }
            });



            $('#create-user').click(function() {
                //$('#dialog-form').dialog('open');
                $('#divmodifyaccount').css('display', 'block');
                $('#updatepassword').css('display', 'none');

            });


            $('#update-password').click(function() {
                //$('#dialog-form').dialog('open');
                $('#updatepassword').css('display', 'block');
                $('#divmodifyaccount').css('display', 'none');

            });




        });

        document.onclick = jsddm_close;

        CallBackFileList.Callback(TabStrip1.getSelectedTab().ID);

        document.getElementById("btnupdatepassword").disabled = true;
       
    </script>

    
    <div id="dialog-form" title="Modify Account">
       
     
       
    </div>
    </form>
</body>
</html>
