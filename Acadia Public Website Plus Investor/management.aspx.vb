﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.Text.RegularExpressions
Partial Class management
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim ManagementDS As XmlDataSource = New XmlDataSource
        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=people", ConfigurationSettings.AppSettings("PeopleXMLFileName"))
        ManagementDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("PeopleXMLFileName")
        ManagementDS.XPath = "IRXML/People/Person[@PersonType='Senior Management']"

        RepeaterManagementLong.DataSource = ManagementDS
        RepeaterManagementLong.DataBind()

        RepeaterManagementShort.DataSource = ManagementDS
        RepeaterManagementShort.DataBind()
    End Sub
End Class
