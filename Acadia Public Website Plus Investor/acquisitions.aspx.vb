﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data


Partial Class acquisitions
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim ws = New acadiaws.Service1
        Dim dtAssets = ws.GetProjectAssets(ConfigurationSettings.AppSettings("AcquisitionsPID"))

        'For Each x As DataColumn In dtAssets.Columns
        '    Console.WriteLine(x.ColumnName)
        'Next

        Dim dtDownloadAssets As DataTable = New DataTable
        dtDownloadAssets.Columns.Add("name")
        dtDownloadAssets.Columns.Add("asset_id")


        For Each x As DataRow In dtAssets.Rows
            Dim r As DataRow = dtDownloadAssets.NewRow
            r("name") = x("name")
            r("asset_id") = x("asset_id")
            dtDownloadAssets.Rows.Add(r)
        Next

        hyperlinkAcquisitions.NavigateUrl = Session("WSDownloadAsset") & "size=0&assetid=" & dtDownloadAssets.Rows(0)("asset_id")

    End Sub
End Class
