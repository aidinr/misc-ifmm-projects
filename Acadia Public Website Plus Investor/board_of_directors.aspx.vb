﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.Text.RegularExpressions
Partial Class board_of_directors
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim bodDS As XmlDataSource = New XmlDataSource
        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=people", ConfigurationSettings.AppSettings("PeopleXMLFileName"))
        bodDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("PeopleXMLFileName")
        bodDS.XPath = "IRXML/People/Person[@PersonType='Board of Directors']"

        RepeaterBODLong.DataSource = bodDS
        RepeaterBODLong.DataBind()

        RepeaterBODShort.DataSource = bodDS
        RepeaterBODShort.DataBind()


    End Sub
End Class
