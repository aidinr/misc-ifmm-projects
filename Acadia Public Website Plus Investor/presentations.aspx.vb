﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.Xml.XPath

Partial Class presentations
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        'Dim PresentationsDS As XmlDataSource = New XmlDataSource
 
        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=items", ConfigurationSettings.AppSettings("ItemsXMLFileName"))
        'PresentationsDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("ItemsXMLFileName")
        'PresentationsDS.XPath = "IRXML/Items/Item[@GroupCode!='AR' and @GroupCode!='OR']"

        'RepeaterPresentations.DataSource = PresentationsDS
        'RepeaterPresentations.DataBind()


        Dim doc As XPathDocument = New XPathDocument(ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("ItemsXMLFileName"))
        Dim nav As XPathNavigator = doc.CreateNavigator()

        Dim expr As XPathExpression
        expr = nav.Compile("IRXML/Items/Item[@GroupCode!='AR' and @GroupCode!='OR']")

        expr.AddSort("Date/@Date", XmlSortOrder.Descending, XmlCaseOrder.None, "", XmlDataType.Text)



        Dim iterator As XPathNodeIterator = nav.Select(expr)

        Dim theString As String = ""
        Dim theCounter As Integer = 0
        Do While iterator.MoveNext()
            theCounter += 1
            Dim itDate As XPathNodeIterator = iterator.Current.Select("Date")
            Dim itTitle As XPathNodeIterator = iterator.Current.Select("Title")
            Dim itURL As XPathNodeIterator = iterator.Current.Select("URL")
            Dim itType As XPathNodeIterator = iterator.Current.Select("@Type")
            Dim itID As XPathNodeIterator = iterator.Current.Select("@EventID")
            Dim itEncodingIcone As XPathNodeIterator = iterator.Current.Select("EncodingIconUrl")
            Dim itEncodingCodec As XPathNodeIterator = iterator.Current.Select("EncodingCodec")

            itDate.MoveNext()
            itTitle.MoveNext()
            itURL.MoveNext()
            itType.MoveNext()
            itID.MoveNext()
            itEncodingIcone.MoveNext()
            itEncodingCodec.MoveNext()


            If (theCounter Mod 2 = 1) Then
                theString = theString & "<tr class=""odd"">"
            Else
                theString = theString & "<tr class=""even"">"
            End If
            theString = theString & "<td>" & itDate.Current.Value & "</td>"
            If (itType.Current.Value = "Streaming Multimedia") Then
                theString = theString & "<td><a href=""event_detail.aspx?id=" & itID.Current.Value & """ target=""_blank"">" & itTitle.Current.Value & "</td>"
            Else
                theString = theString & "<td><a href=""" & itURL.Current.Value & """ target=""_blank"">" & itTitle.Current.Value & "</td>"

            End If
            theString = theString & "<td align=""center""><img src=""" & itEncodingIcone.Current.Value & """ alt=""" & itEncodingCodec.Current.Value & """ /></td>"
            theString = theString & "</tr>"
            literalPresentations.Text = theString

        Loop

    End Sub

    Public Sub RepeaterPresentationsOnItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim hyperlinkPresentation As HyperLink = e.Item.FindControl("hyperlinkPresentation")
            Dim literalPresentation As Literal = e.Item.FindControl("literalPresentation")

            Dim navigableDataItem As XPathNavigator = CType(e.Item.DataItem, IXPathNavigable).CreateNavigator

            Dim itTitle As XPathNodeIterator = navigableDataItem.Select("Title")
            Dim itURL As XPathNodeIterator = navigableDataItem.Select("URL")
            Dim itType As XPathNodeIterator = navigableDataItem.Select("@Type")
            Dim itID As XPathNodeIterator = navigableDataItem.Select("@EventID")

            itTitle.MoveNext()
            itURL.MoveNext()
            itType.MoveNext()
            itID.MoveNext()


            Dim theTitle As String = itTitle.Current.Value
            Dim theURL As String = itURL.Current.Value
            Dim theType As String = itType.Current.Value
            Dim theID As String = itID.Current.Value

            If (theType = "Streaming Multimedia") Then
                literalPresentation.Text = theTitle
                Dim item2XML As XmlDocument = New XmlDocument
                item2XML.Load(ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("EventsXMLFileName"))

                Dim nav As XPathNavigator = item2XML.CreateNavigator
                Dim itStreamID As XPathNodeIterator = nav.Select("IRXML/Events/Event/Webcasts/Webcast/Streams/Stream/@ID")
                itStreamID.MoveNext()

                Dim theStreamID As String = itStreamID.Current.Value

                hyperlinkPresentation.NavigateUrl = ConfigurationSettings.AppSettings("BaseWebCastURL") & "EventId=" & theID & "&StreamId=" & theStreamID & "RGS=1"
                hyperlinkPresentation.Attributes.Add("target", "_blank")


            Else
                literalPresentation.Text = theTitle
                hyperlinkPresentation.NavigateUrl = theURL
            End If
          

        End If
    End Sub

End Class
