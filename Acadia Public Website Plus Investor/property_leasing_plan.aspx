<%@ Page Language="VB" AutoEventWireup="false" CodeFile="property_leasing_plan.aspx.vb" Inherits="property_leasing_plan" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<style type="text/css">
 	@import url("css/style.css");
</style>
<link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" />
<script src="http://cdn.jquerytools.org/1.2.2/full/jquery.tools.min.js" type="text/javascript"></script>
<script src="js/jquery.perciformes.js" type="text/javascript"></script>
<script src="js/navi.js" type="text/javascript"></script>
<script type="text/javascript" src="js/slimbox2.js"></script>
<script src="http://use.typekit.com/spa4odv.js" type="text/javascript"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>


<style>
#property_search {
	position: absolute;
	top:98px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
</style>

<!--[if lte IE 7]>
<style type="text/css">
html, body {
	overflow-x: hidden;
}
</style>
<![endif]-->
<!--[if IE 7]>
<style>
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 20px;
	left:1px;
}

#property_search {
	position: absolute;
	top:122px;
	left: 50%;
	margin-left: 277px;
	z-index: 1;	
	background: #f9a131;
}


</style>
<![endif]-->
<style type="text/css">
@-moz-document url-prefix() {
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1007;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 25px;
	left:1px;
}

#property_search {
	position: absolute;
	top:102px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
#header {
	position:relative;
	margin-top:19px;
	margin-bottom: 14px;	
}
}
</style>


<title>Acadia Realty Trust</title>
</head>
<body>
    <form id="form1" runat="server">
    
    <div id="container">
    <!--property search-->
		<div id="property_search">
		<a href="javascript:toggleSearch();"><img src="images/property_search.jpg" alt="property search" /></a>
		<div id="property_search_form">
	        <asp:PlaceHolder ID="placeholderSearchForm" runat="server"></asp:PlaceHolder>
	        </div>
		</div>
		<!-- end of property search-->
		<div id="header">
			<table width="100%" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				<td width="424" valign="top"><a href="default.aspx"><img class="navi" src="images/logo.jpg" alt="" /></a></td>
				<td valign="bottom" align="right">
				<!--menu-->
		<ul id="jsddm">
			<li class="main_nav"><a href="company.aspx">
			<img class="navi" src="images/navi_company_off.jpg" /></a>
			<ul>
				<!--<li><a href="about.aspx"><img class="navi" src="images/subnavi_about_off.png" alt="about"/></a></li>-->
				<li><a href="employment.aspx">
				<img alt="employment" class="navi" src="images/subnavi_employment_off.png" /></a></li>
				<!--<li class="last"><a href="benefits.aspx"><img class="navi" src="images/subnavi_benefits_off.png" alt="benefit"/></a></li>-->
			</ul>
			</li>
			<li class="main_nav"><a href="properties.aspx">
			<img src="images/navi_properties_on.jpg" /></a> </li>
			<li class="main_nav"><a href="investor_relations.aspx">
			<img class="navi" src="images/navi_ir_off.jpg" /></a>
			<ul>
				<li><a href="news_and_events.aspx">
				<img alt="news and events" class="navi" src="images/subnavi_news_off.png" /></a></li>
				<li><a href="reporting.aspx">
				<img alt="reporting" class="navi" src="images/subnavi_reporting_off.png" /></a></li>
				<li><a href="quarterly_reports.aspx">
				<img alt="quarterly reports" class="navi" src="images/subnavi_quarterly_off.png" /></a></li>
				<li><a href="annual_reports.aspx">
				<img alt="annual reports" class="navi" src="images/subnavi_annual_off.png" /></a></li>
				<li><a href="presentations.aspx">
				<img alt="presentations" class="navi" src="images/subnavi_presentations_off.png" /></a></li>
				<li><a href="analyst_coverage.aspx">
				<img alt="analyst" class="navi" src="images/subnavi_analyst_off.png" /></a></li>
				<li><a href="sec_filings.aspx">
				<img alt="filings" class="navi" src="images/subnavi_filings_off.png" /></a></li>
				<li><a href="dividends.aspx">
				<img alt="dividends" class="navi" src="images/subnavi_dividends_off.png" /></a></li>
				<li><a href="shareholders_information.aspx">
				<img alt="shareholders" class="navi" src="images/subnavi_shareholders_off.png" /></a></li>
				<li><a href="information_request.aspx">
				<img alt="information request" class="navi" src="images/subnavi_information_off.png" /></a></li>
				<li><a href="corporate_governance.aspx">
				<img alt="corporate" class="navi" src="images/subnavi_corporate_off.png" /></a></li>
				<li><a href="board_of_directors.aspx">
				<img alt="board of directors" class="navi" src="images/subnavi_bod_off.png" /></a></li>
				<li><a href="management.aspx">
				<img alt="management" class="navi" src="images/subnavi_management_off.png" /></a></li>
				<li><a href="committee_composition.aspx">
				<img alt="committee compositions" class="navi" src="images/subnavi_committee_off.png" /></a></li>
				<li><a href="faq.aspx">
				<img alt="faq" class="navi" src="images/subnavi_faq_off.png" /></a></li>
			</ul>
			</li>
			<li class="main_nav"><a href="acquisitions.aspx">
			<img alt="acquisitions" class="navi" src="images/navi_acquisitions_off.jpg" /></a>
			<!--
					 	<ul>
						      <li><a href="dispositions.aspx"><img class="navi" src="images/subnavi_dispositions_off.png" alt="about"/></a></li>
						      <li class="last"><a href="submit.aspx"><img class="navi" src="images/subnavi_submit_off.png" alt="about"/></a></li>
						    </ul>
						    --></li>
			<li class="main_nav"><a href="contact.aspx">
			<img alt="contact" class="navi" src="images/navi_contact_off.jpg" /></a>
			</li>
		</ul>
		<!--menu-->
				
				</td>
				</tr>
			</table>
		</div>
		<div id="body">
			                <div class="body_content" style="border:1px solid white;">

						
						    <div class="cookie_crumbs"><a href="Default.aspx">Home</a> | <a href="Properties.aspx">Properties</a> | <a href="property_detail.aspx?id=<%=Request.QueryString("id")%>"><asp:Literal ID="literalProjectCrumbs" runat="server"></asp:Literal></a> | Leasing Plan</div>
						    
						    <div class="project_detail_header">
						        <span class="project_detail_name"><asp:Literal ID="literalProjectName" runat="server"></asp:Literal></span><br />
						        <span class="project_detail_location"><asp:Literal ID="literalProjectLocation" runat="server"></asp:Literal></span>
						    </div>
						    
						    <div class="project_detail_content">
						    
						    <a href="property_detail.aspx?id=<%=request.querystring("id") %>"><img class="project_detail_navi navi" src="images/subnavi_property_off.jpg" alt="property detail" class="project_detail_navi" /></a><a href="property_leasing_plan.aspx?id=<%=request.querystring("id") %>"><img class="project_detail_navi" src="images/subnavi_leasing_on.jpg" alt="leasing map" /></a><asp:PlaceHolder ID="placeholderCompetitionMap" runat="server"><a href="property_competition_map.aspx?id=<%=request.querystring("id") %>"><img class="project_detail_navi navi" src="images/subnavi_competition_off.jpg" alt="competition map" style=""margin-right:0;" /></a></asp:PlaceHolder>
						    <div style="position: relative; width: 35px;left:896px;top:-38px;  z-index: 1;" id="layer1"><asp:HyperLink cssclass="image1link" runat="server" ID="hyperlinkImage1"><img src="images/plus.jpg" alt="Enlarge image" title="Enlarge image" /></asp:HyperLink></div>
						    <div style="text-align:center;position:relative;margin-top:-40px;">

						    <div style="position:relative;margin-top:40px;"><asp:Image runat="server" ID="imageLeasingPlan" CssClass="leasing_plan_image" /></div>
							<div  style="text-align:right;"><asp:HyperLink  runat="server" ID="printlink" Text="Click to Print This Page">Click to Print This Leasing Document</asp:HyperLink></div>
						    </div>
						    
						    <asp:Repeater ID="repeaterFloorSelectors" runat="server" OnItemDataBound="repeaterFloorSelectorsOnItemDataBound">
						        <ItemTemplate><asp:Literal ID="literalButtonSelector" runat="server"></asp:Literal><a href="?id=<%=Request.querystring("id") %>&floor=<%#Container.DataItem("floor").ToString.trim %>">Floor <%#Container.DataItem("floor").ToString.Trim%></a></div></ItemTemplate>
						    </asp:Repeater>
						     
						    
						    <div class="leasing_plan_table_container" style="width: 94%">
						    
						   
						    
						    <table cellspacing="0" cellpadding="0" width="100%">
						    
						    <tr>
						        <td valign="top" width="300">
						        
						       
						        <asp:Repeater ID="repeaterCurrentTenants" runat="server" OnItemDataBound="repeaterCurrentTenantsItemDataBound">
						        <HeaderTemplate>
						         <table cellpadding="0" cellspacing="0" class="leasing_plan_table">
						        <tr>
						            <td valign="top" colspan="2"><b>TENANT LISTING</b></td>
						            <td valign="top" align="right"><b>SQUARE FT</b></td>
						        </tr>
						        </HeaderTemplate>
						            <ItemTemplate>
						             <tr>
						             <td valign="top" width="21"><asp:literal ID="literalSpace" runat="server"></asp:literal> </td>
						            <td valign="top" width="175"><%#Container.DataItem("shortname")%></td>
						            <td valign="top" align="right" width="75"><%#FormatNumber(Container.DataItem("SF"), 0)%></td>
						            </tr>
						            </ItemTemplate>
						        <FooterTemplate>
						        </table>    
						        </FooterTemplate>
						        </asp:Repeater>
						    
						    
						    
						    </td>
						        <td valign="top" width="270">
						        
						        
						             <asp:Repeater ID="repeaterAvailableSpaces" runat="server" OnItemDataBound="repeaterAvailableSpacesItemDataBound">
						             <HeaderTemplate>
						              <table cellpadding="0" cellspacing="0" class="leasing_plan_table">
						            <tr>
						            <td valign="top"><b>AVAILABLE SPACES</b></td>
						            <td valign="top" align="left"><b>SQUARE FT</b></td>
						            </tr>
						             </HeaderTemplate>
						            <ItemTemplate>
						             <tr>
						            <td valign="top" width="150"><Asp:literal ID="literalSpace" runat="server"></Asp:literal></td>
						            <td valign="top" align="left" width="110"><%#FormatNumber(Container.DataItem("SF"), 0)%></td>
						            </tr>
						            </ItemTemplate>
						        <FooterTemplate>
						            </table>
						            
						        </FooterTemplate>
						        </asp:Repeater>
						    
						        
						        </td>
						        <td valign="top">
                                                    <div style="margin-left:40px;">
						                           <b>DOWNLOADS</b><br /><br />
						                           <asp:Repeater ID="RepeaterAssetDownloads" runat="server">
						                           <HeaderTemplate><ul class="download_assets_gray"></HeaderTemplate>
						                            <ItemTemplate><li><a target="_blank" href="<%=Session("WSDownloadAsset") %>size=0&assetid=<%#Container.DataItem("asset_id")%>"><%#Container.DataItem("name")%></a></li></ItemTemplate>
						                           <FooterTemplate></ul><br /></FooterTemplate>
						                           </asp:Repeater>
                                                    </div>
						        
						        </td>
						    </tr>
						   
						   
						    </table>
						    </div>
						    </div>
						    
						    </div>
		</div>
	</div>
	<div id="footer_container">
		<div id="footer">
			<table cellpadding="0" cellspacing="0">
				<tr>
				<td valign="top" width="520">
				<div style="width:400px;">
				<big><b>Acadia Realty Trust</b></big><br />
				Acadia Realty Trust (NYSE:AKR) is a fully integrated, self-managed and self-administered equity REIT focused primarily on the ownership, acquisition, redevelopment and management of retail properties, including neighborhood / community shopping centers and mixed-use properties with retail components. 
				</div>
				</td>
				<td valign="top" width="120">
				<big><b>Menu</b></big><br />
				<a href="company.aspx">Company</a><br />
				<a href="properties.aspx">Properties</a><br />
				<a href="Investor_Relations.aspx">Investor Relations</a><br />
				<a href="acquisitions.aspx">Acquisitions</a><br />
				<a href="contact.aspx">Contact Us</a><br />
				</td>
				<td valign="top" width="120">
				<big><b>Follow Us</b></big><br />
				<!--<a href="#">RSS Feed</a><br />-->
				<!--<a href="#">Twitter</a><br />-->
				<a href="#">RSS Feed</a><br />
				<a href="http://www.linkedin.com/companies/acadia-realty-trust" target="_blank">LinkedIn</a>
				</td>
				<td valign="top">
				<big><b>Corporate Headquarters</b></big><br />
				1311 Mamaroneck Avenue<br />
				Suite 260<br />
				White Plains, NY 10605<br />
				<a href="mailto:info@acadiarealty.com">info@acadiarealty.com</a><br />
				<a target="_blank" href="http://maps.google.com/maps?f=d&daddr=1311 Mamaroneck Avenue, White Plains, NY 10605">Google map directions</a><br />
				</td>
				</tr>
				<tr>
					<td colspan="4">
					<br /><br />
					&copy; Copyright 2010 Acadia Realty Trust&nbsp;&nbsp; 
					|&nbsp;&nbsp; <a href="terms.html" target="_blank">Terms and Conditions </a>&nbsp;&nbsp; |&nbsp;&nbsp; 
					<a href="privacy.html" target="_blank">Privacy Statement </a>					</td>
				
				</tr>
			</table>
			</div>
	</div>	
	

	<script type="text/javascript">
    
	var timeout    = 500;
	var closetimer = 0;
	var ddmenuitem = 0;
	
	function jsddm_open()
	{  jsddm_canceltimer();
	   jsddm_close();
	   ddmenuitem = $(this).find('ul').css('visibility', 'visible');}
	
	function jsddm_close()
	{  if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}
	
	function jsddm_timer()
	{  closetimer = window.setTimeout(jsddm_close, timeout);}
	
	function jsddm_canceltimer()
	{  if(closetimer)
	   {  window.clearTimeout(closetimer);
	      closetimer = null;}}
	
	$(document).ready(function()
	{  $('#jsddm > li').bind('mouseover', jsddm_open)
	   $('#jsddm > li').bind('mouseout',  jsddm_timer)});
	
	document.onclick = jsddm_close;
</script>
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17447452-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script></form>

</body>
</html>
