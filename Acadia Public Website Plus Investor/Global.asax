﻿<%@ Application Language="VB" %>
<%@ Import Namespace="WebArchives.iDAM.WebCommon.Security " %>
<%@ Import Namespace="System.Net" %>

<script runat="server">
    Dim sBaseIP As String = ConfigurationSettings.AppSettings("IDAMLocation")
    Dim sBaseInstance As String = ConfigurationSettings.AppSettings("IDAMInstance")
    Dim sVSIDAMClientDownload As String = ConfigurationSettings.AppSettings("IDAMLocationDownload")
    Dim sVSIDAMClient As String = ConfigurationSettings.AppSettings("IDAMLocationBrowse")
    
    
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
        ' Code that runs when a new session is started
        Dim assetRetrieve = sBaseIP & "/" & sVSIDAMClient
        Dim assetDownload = sBaseIP & "/" & sVSIDAMClientDownload
        
        Dim theLogin As String = ConfigurationSettings.AppSettings("IDAMLogin")
        Dim thePassword As String = ConfigurationSettings.AppSettings("IDAMPassword")
        thePassword = WebArchives.iDAM.WebCommon.Security.Encrypt(thePassword)

        Dim loginStatus As Boolean = False

        Dim loginResult1 As String = ""

        Dim sessionID As String = "(S(" & GetSessionID("http://" & assetRetrieve & "/GetSessionID.aspx") & "))"
        Session("clientSessionID") = sessionID
        Dim iDAMBrowse As New browsesecure.BrowseService
        iDAMBrowse.Url = "http://" & assetRetrieve & "/" & sessionID & "/" & "BrowseService.asmx"
        'Response.Write(iDAMBrowse.Url)
        Try
            loginResult1 = iDAMBrowse.Login(theLogin, thePassword, sBaseInstance, True)
        Catch Ex As Exception
            Response.Write(Ex)
        End Try
        
        Dim sessionIDDownload As String = "(S(" & GetSessionID("http://" & assetDownload & "/GetSessionID.aspx") & "))"
        
        Dim iDAMDownload As New DownloadSecure.AssetDownloadService
        iDAMDownload.Url = "http://" & assetDownload & "/" & sessionIDDownload & "/" & "AssetDownloadService.asmx"
       
        Try
            loginResult1 = iDAMDownload.Login(theLogin, thePassword, sBaseInstance, True)
        Catch Ex As Exception
            'Response.Write(Ex)
        End Try
        
        Session("WSRetreiveAsset") = "http://" & assetRetrieve & "/" + sessionID + "/RetrieveAsset.aspx?instance=" & sBaseInstance & "&"
        Session("WSDownloadAsset") = "http://" & assetDownload & "/" & sessionIDDownload + "/DownloadFile.aspx?dtype=assetdownload&instance=" & sBaseInstance & "&"
        
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
    
    Public Shared Function GetSessionID(ByVal URL As String) As String
        Try
            Dim clientreq As HttpWebRequest
            clientreq = HttpWebRequest.Create(URL)
            clientreq.Timeout = CType(10000, Integer)
            Dim clientres As HttpWebResponse = clientreq.GetResponse
            Dim data As IO.Stream = clientres.GetResponseStream
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            If str.IndexOf("SESSIONID") >= 0 Then
                Dim strtidx As Integer = str.IndexOf("SESSIONID=") + 10
                Return str.Substring(strtidx, str.IndexOf("</span>") - strtidx)
            Else
                Throw New Exception("No session ID found. check URL if it has the proper format" + URL)
            End If
        Catch ex As Exception
            Throw New Exception("WebService not available." + URL)
        End Try
    End Function
       
</script>