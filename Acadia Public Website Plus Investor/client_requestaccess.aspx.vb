﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.IO


Partial Class client_requestaccess
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       

    End Sub

    Function isEmail(ByVal inputEmail As String) As Boolean

        inputEmail = inputEmail.ToString()
        Dim strRegex As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
        Dim re As Regex = New Regex(strRegex)
        If (re.IsMatch(inputEmail)) Then
            Return True
        Else
            Return False
        End If

    End Function

    Sub btnSubmit_click(ByVal sender As Object, ByVal e As EventArgs) Handles submitApp.Click
        If Page.IsValid Then

            ' Dim department As String = formDepartment.SelectedValue

            Dim firstName As String = formFirstName.Text
            Dim lastName As String = formLastName.Text
            Dim address As String = formAddress.Text

            Dim address2 As String = formAddress2.Text

            Dim city As String = formCity.Text
            Dim state As String = formState.SelectedValue
            Dim Country As String = formCountry.Text
            Dim zip As String = formZip.Text
            Dim phone As String = formPhone.Text
            Dim fax As String = formFax.Text
            Dim email As String = formEmail.Text
            Dim bestway As String = formBestWay.SelectedValue

            Dim experience As String = formExperience.Text
            Dim currentTitle As String = formCurrentTitle.Text
            Dim AffiliateOf As String = formAffiliateOf.Text


            'Dim education As String = formEducation.SelectedValue
            'Dim eligible As String = formEligible.SelectedValue

            Dim mail As MailMessage = New MailMessage
            mail.From = New MailAddress("support@ifmm.com", "Acadia Realty Employment Application Info")
            mail.Subject = "Acadia Client Access Request"
            'mail.To.Add(New MailAddress("support@ifmm.com", "Support"))
            mail.To.Add(New MailAddress(System.Configuration.ConfigurationManager.AppSettings("clientaccessrequestRecipient"), "Acadia Realty"))



            Dim theMessage As String = "=====================================================" & vbCrLf
            theMessage = theMessage & "ACADIA REALTY TRUST CLIENT ACCESS REQUEST FORM SUBMITTAL" & vbCrLf
            theMessage = theMessage & "======================================================" & vbCrLf
            theMessage = theMessage & vbCrLf & vbCrLf
            theMessage = theMessage & "The following form has been submitted online:" & vbCrLf
            theMessage = theMessage & vbCrLf & vbCrLf
            'theMessage = theMessage & "Department: " & department & vbCrLf
            theMessage = theMessage & vbCrLf
            theMessage = theMessage & "First Name: " & firstName & vbCrLf
            theMessage = theMessage & "Last Name: " & lastName & vbCrLf
            theMessage = theMessage & "Address: " & address & vbCrLf & address2 & vbCrLf
            theMessage = theMessage & "City: " & city & vbCrLf
            theMessage = theMessage & "State: " & state & vbCrLf
            theMessage = theMessage & "Country: " & Country & vbCrLf
            theMessage = theMessage & "Zip: " & zip & vbCrLf
            theMessage = theMessage & "Daytime Phone: " & phone & vbCrLf
            theMessage = theMessage & "Fax: " & fax & vbCrLf
            theMessage = theMessage & "Email: " & email & vbCrLf
            theMessage = theMessage & "Type: " & bestway & vbCrLf
            theMessage = theMessage & vbCrLf
            theMessage = theMessage & "Organization: " & experience & vbCrLf
            theMessage = theMessage & "Current Title: " & currentTitle & vbCrLf
            theMessage = theMessage & "Affiliate: " & AffiliateOf & vbCrLf
            'theMessage = theMessage & "Eligible to work in the US: " & eligible & vbCrLf

            theMessage = theMessage & vbCrLf & vbCrLf


            theMessage = theMessage & "======================================================" & vbCrLf
            theMessage = theMessage & "END OF FORM SUBMITTAL" & vbCrLf
            theMessage = theMessage & "======================================================" & vbCrLf

            mail.Body = theMessage

            Dim smtp As New SmtpClient()

            Try
                smtp.Send(mail)
                labelError.Text = "<span style=""color:red;font-weight:bold;"">Thank you for your request. An Acadia Realty Trust representative will email your access instructions. If you do not receive your credentials in 24 hours, please contact an Acadia Realty Trust representative at 914-288-8100</span><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />"
                formfieldspanel.Visible = False
            Catch ex As Exception
                labelError.Text = "<span style=""color:red;font-weight:bold;"">A technical error has occured. Please try again later.</span><br /><br />"
            End Try



        Else
            labelError.Text = "<span style=""color:red;font-weight:bold;"">A technical error has occured. Please try again later.</span><br /><br />"
        End If


    End Sub

End Class
