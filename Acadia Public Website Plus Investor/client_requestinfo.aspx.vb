﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports WebArchives.iDAM.WebCommon
Imports System.Web.Mail

Partial Class client_requestinfo
    Inherits System.Web.UI.Page

   

    Protected Sub client_requestinfo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            If Email1.Text <> Email2.Text Then
                ltrErrorMessage.Text = "Error.  Please make sure the emails match."
            Else
                ltrErrorMessage.Text = ""
            End If

        End If
    End Sub


    Public Shared Function ScrubSQLInput(ByVal sInput As String, Optional ByVal removequotes As Boolean = True) As String
        If removequotes Then
            sInput = sInput.Replace("'", "")
        End If
        ScrubSQLInput = sInput.Replace(";", "").Replace("--", "").Replace("/*", "").Replace("*/", "").Replace("Xp_", "").Replace("[", "[[]").Replace("%", "[%]").Replace("¬", "").Replace("&#172", "-").Replace("&#173", "-").Replace("&#174", "-")
    End Function

    Protected Sub submitApp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles submitApp.Click
        If Page.IsValid Then
            If Email1.Text <> Email2.Text Then
                ltrErrorMessage.Text = "Error.  Please make sure the emails match."
            Else
                ltrErrorMessage.Text = ""

                'get username
                'sender email.
                Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
                Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select userid,login,password from ipm_user where email = @email and active = 'Y'", MyConnection)

                Dim email As New SqlParameter("@email", SqlDbType.VarChar, 255)
                MyCommand1.SelectCommand.Parameters.Add(email)
                MyCommand1.SelectCommand.Parameters("@email").Value = ScrubSQLInput(Email1.Text)

                Dim DT1 As New DataTable("Login")

                MyCommand1.Fill(DT1)
                If DT1.Rows.Count = 1 Then
                    'send email

                    Dim strMSG As String = ""
                    Dim _newpassword = Functions.setNewTemporaryPassword(DT1.Rows(0)("userid"))
                    strMSG = "<br><br>Your password has been changed.  Please click the link below to create a new password.<br><br> <a href=" & Request.UrlReferrer.AbsoluteUri.Replace("client_requestinfo.aspx", "client_resetpassword.aspx") & "?rid=" & _newpassword & ">Create new password.</a>"
                    strMSG += "<br><br>" & Request.Form("EmailMessage")

                    Dim strSenderEmail As String = System.Configuration.ConfigurationManager.AppSettings("EMAILRequestSender")

                    Try
                        Dim insMail As New MailMessage
                        insMail.BodyFormat = MailFormat.Html
                        With insMail
                            .From = strSenderEmail
                            .To = Email1.Text
                            .Subject = "Acadia Realty Account Information"
                            .Body = strMSG
                        End With
                        SmtpMail.SmtpServer = System.Configuration.ConfigurationManager.AppSettings("SMTPServer")
                        SmtpMail.Send(insMail)

                        ltrErrorMessage.Text = "<b><font color=red>A reset password request has been sent to your email account.</font></b><br>"

                    Catch ex As Exception
                        ltrErrorMessage.Text = "<b><font color=red>Email failed.  Please contact your system administrator.</font></b><br>"
                        'undo tmppassword
                        Functions.unsetTemporaryPassword(DT1.Rows(0)("userid"))
                    End Try



                Else
                    ltrErrorMessage.Text = "<b><font color=red>Email address not registered to a valid user account.  Please check your email and try again.</font></b><br>"

                End If

            End If


        Else
        ltrErrorMessage.Text = "<font color=red>Please ensure the validation is entered correctly.</font><br>"
        End If

    End Sub
End Class
