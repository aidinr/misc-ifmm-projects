<%@ Page Language="VB" AutoEventWireup="false" CodeFile="email_alerts_sec.aspx.vb" Inherits="email_alerts"  EnableViewStateMac=false EnableViewState="false"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<style type="text/css">
 p.MsoNormal
	{margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	margin-left: 0in;
	margin-right: 0in;
	margin-top: 0in;
}
</style>
<style type="text/css">
 	@import url("css/style.css");
</style>
<script src="http://cdn.jquerytools.org/1.2.2/full/jquery.tools.min.js" type="text/javascript"></script>
<script src="js/jquery.perciformes.js" type="text/javascript"></script>
<script src="js/navi.js" type="text/javascript"></script>
<script src="http://use.typekit.com/spa4odv.js" type="text/javascript"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>


<style>
#property_search {
	position: absolute;
	top:98px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
</style>

<!--[if lte IE 7]>
<style type="text/css">
html, body {
	overflow-x: hidden;
}
</style>
<![endif]-->
<!--[if IE 7]>
<style>
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 20px;
	left:1px;
}

#property_search {
	position: absolute;
	top:122px;
	left: 50%;
	margin-left: 277px;
	z-index: 1;	
	background: #f9a131;
}


</style>
<![endif]-->
<style type="text/css">
@-moz-document url-prefix() {
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1007;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 25px;
	left:1px;
}

#property_search {
	position: absolute;
	top:102px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
#header {
	position:relative;
	margin-top:19px;
	margin-bottom: 14px;	
}
}
</style>




<title>Acadia Realty Trust - Acquisitions</title>
</head>
<body>
    <form id="form1" runat="server">
   <div id="container">
		   <!--property search-->
		<div id="property_search">
		<a href="javascript:toggleSearch();"><img src="images/property_search.jpg" alt="property search" /></a>
		<div id="property_search_form">
	        <asp:PlaceHolder ID="placeholderSearchForm" runat="server"></asp:PlaceHolder>
	        </div>
		</div>
		<!-- end of property search-->
		<div id="header">
			<table width="100%" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				<td width="424" valign="top"><a href="default.aspx"><img class="navi" src="images/logo.jpg" /></a></td>
				<td valign="bottom" align="right">
				<!--menu-->
		<ul id="jsddm">
			<li class="main_nav"><a href="company.aspx">
			<img class="navi" src="images/navi_company_off.jpg" /></a>
			<ul>
				<!--<li><a href="about.aspx"><img class="navi" src="images/subnavi_about_off.png" alt="about"/></a></li>-->
				<li><a href="employment.aspx">
				<img alt="employment" class="navi" src="images/subnavi_employment_off.png" /></a></li>
				<!--<li class="last"><a href="benefits.aspx"><img class="navi" src="images/subnavi_benefits_off.png" alt="benefit"/></a></li>-->
			</ul>
			</li>
			<li class="main_nav"><a href="properties.aspx">
			<img class="navi" src="images/navi_properties_off.jpg" /></a> </li>
			<li class="main_nav"><a href="investor_relations.aspx">
			<img class="navi" src="images/navi_ir_off.jpg" /></a>
			<ul>
				<li><a href="news_and_events.aspx">
				<img alt="news and events" class="navi" src="images/subnavi_news_off.png" /></a></li>
				<li><a href="reporting.aspx">
				<img alt="reporting" class="navi" src="images/subnavi_reporting_off.png" /></a></li>
				<li><a href="quarterly_reports.aspx">
				<img alt="quarterly reports" class="navi" src="images/subnavi_quarterly_off.png" /></a></li>
				<li><a href="annual_reports.aspx">
				<img alt="annual reports" class="navi" src="images/subnavi_annual_off.png" /></a></li>
				<li><a href="presentations.aspx">
				<img alt="presentations" class="navi" src="images/subnavi_presentations_off.png" /></a></li>
				<li><a href="analyst_coverage.aspx">
				<img alt="analyst" class="navi" src="images/subnavi_analyst_off.png" /></a></li>
				<li><a href="sec_filings.aspx">
				<img alt="filings" class="navi" src="images/subnavi_filings_off.png" /></a></li>
				<li><a href="dividends.aspx">
				<img alt="dividends" class="navi" src="images/subnavi_dividends_off.png" /></a></li>
				<li><a href="shareholders_information.aspx">
				<img alt="shareholders" class="navi" src="images/subnavi_shareholders_off.png" /></a></li>
				<li><a href="information_request.aspx">
				<img alt="information request" class="navi" src="images/subnavi_information_off.png" /></a></li>
				<li><a href="corporate_governance.aspx">
				<img alt="corporate" class="navi" src="images/subnavi_corporate_off.png" /></a></li>
				<li><a href="board_of_directors.aspx">
				<img alt="board of directors" class="navi" src="images/subnavi_bod_off.png" /></a></li>
				<li><a href="management.aspx">
				<img alt="management" class="navi" src="images/subnavi_management_off.png" /></a></li>
				<li><a href="committee_composition.aspx">
				<img alt="committee compositions" class="navi" src="images/subnavi_committee_off.png" /></a></li>
				<li><a href="faq.aspx">
				<img alt="faq" class="navi" src="images/subnavi_faq_off.png" /></a></li>
			</ul>
			</li>
			<li class="main_nav"><a href="acquisitions.aspx">
			<img alt="acquisitions" src="images/navi_acquisitions_on.jpg" /></a>
			<!--
					 	<ul>
						      <li><a href="dispositions.aspx"><img class="navi" src="images/subnavi_dispositions_off.png" alt="about"/></a></li>
						      <li class="last"><a href="submit.aspx"><img class="navi" src="images/subnavi_submit_off.png" alt="about"/></a></li>
						    </ul>
						    --></li>
			<li class="main_nav"><a href="contact.aspx">
			<img alt="contact" class="navi" src="images/navi_contact_off.jpg" /></a>
			</li>
		</ul>
		<!--menu-->
				
				</td>
				</tr>
			</table>
		</div>
		<div id="body">
			<div class="body_content">
				
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td valign="top" width="584">
						
						    <div class="cookie_crumbs"><a href="Default.aspx">Home</a> 
								| Email Alerts</div>
						    
						    <div class="general_title"><img src="images/title_email_alerts.jpg"/></div>
						
						    <div class="general_content">
						    	<p class="MsoNormal">&nbsp;</p>
								<span class="Apple-style-span" style="border-collapse: separate; color: rgb(0, 0, 0); font-family: 'Times New Roman'; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-align: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; -webkit-text-decorations-in-effect: none; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; font-size: medium;">
								<span class="Apple-style-span" style="color: rgb(102, 102, 102); font-family: Arial, Verdana, Helvetica, sans-serif; font-size: 12px;">
								<%if not isPostBack then%>
								<table border="0" cellpadding="0" cellspacing="0" style="font-size: 12px;" width="100%">
									<tr class="ccbnBgTxt" style="background-color: rgb(255, 255, 255);">
										<td style="font-size: 12px;" valign="top">
										<span class="ccbnTxt" style="font-family: Arial, Verdana, Helvetica, sans-serif; font-size: 12px; color: rgb(102, 102, 102);">
										Use the Yes/No buttons to subscribe or 
										unsubscribe from an alert and then click 
										Submit to save your preferences.<span class="Apple-converted-space">&nbsp;</span><br />
										<br />
										An e-mail will be sent to you shortly 
										confirming that your change has been 
										processed. You can return to this page 
										at any time to modify your selections.<br />
										<br />
										Information on file for e-mail address: 
										(<%response.write(request.form("email"))%>)</span></td>
									</tr>
									<tr class="ccbnBgSpacer" style="background-color: rgb(255, 255, 255);">
										<td style="font-size: 12px;">
										<img height="10" src="spacer.gif" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px;" width="1" /></td>
									</tr>
									<tr class="ccbnBgError" style="background-color: rgb(255, 255, 255);">
										<td class="ccbnError" style="font-size: 12px; font-family: Arial, Verdana, Helvetica, sans-serif; color: rgb(255, 0, 0);" valign="top">
										<span class="ccbnError" style="font-family: Arial, Verdana, Helvetica, sans-serif; font-size: 12px; color: rgb(255, 0, 0);">
										</span></td>
									</tr>
									<tr class="ccbnBgTtl" style="background-color: rgb(255, 255, 255);">
										<td style="font-size: 12px;" valign="middle">
										<span class="ccbnTtl" style="font-family: Arial, Verdana, Helvetica, sans-serif; font-size: 13px; color: rgb(102, 102, 102); font-weight: bold;">
										Please enter / update your information</span></td>
									</tr>
									<tr class="ccbnBgInput" style="background-color: rgb(255, 255, 255);">
										<td style="font-size: 12px;">
										<table border="0" cellpadding="3" cellspacing="1" style="font-size: 12px;" width="100%">
											<tr class="ccbnBgLabel" style="background-color: rgb(255, 255, 255);">
												<td align="center" style="font-size: 12px;" valign="middle"><input name="email" type="hidden" value="<%response.write(request.form("email"))%>" />
												<span class="ccbnLabel" style="font-family: Arial, Verdana, Helvetica, sans-serif; font-size: 11px; color: rgb(17, 131, 188); font-weight: bold;">
												Yes</span></td>
												<td align="center" style="font-size: 12px;" valign="middle">
												<span class="ccbnLabel" style="font-family: Arial, Verdana, Helvetica, sans-serif; font-size: 11px; color: rgb(17, 131, 188); font-weight: bold;">
												No</span></td>
												<td style="font-size: 12px;" valign="top" width="100%">
												<img height="1" src="spacer.gif" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px;" width="1" /></td>
											</tr>
											<tr class="ccbnBgTxt" style="background-color: rgb(255, 255, 255);">
												<td align="center" style="font-size: 12px;" valign="top">
												<input name="subsec" <asp:Literal runat="server" ID="subsecon" ></asp:Literal> type="radio" value="YES" /></td>
												<td align="center" style="font-size: 12px;" valign="top">
												<input <asp:Literal runat="server" ID="subsecoff" ></asp:Literal> name="subsec" type="radio" value="NO" /></td>
												<td style="font-size: 12px;" width="100%">
												<span class="ccbnTxt" style="font-family: Arial, Verdana, Helvetica, sans-serif; font-size: 12px; color: rgb(102, 102, 102);">
												<label for="ir-sec">Acadia 
												Realty Trust SEC Filing Alert</label></span></td>
											</tr>
											<tr class="ccbnBgTxt" style="background-color: rgb(255, 255, 255);">
												<td align="center" style="font-size: 12px;" valign="top">
												</td>
												<td align="center" style="font-size: 12px;" valign="top">
												</td>
												<td style="font-size: 12px;" width="100%">
												<span class="ccbnTxt" style="font-family: Arial, Verdana, Helvetica, sans-serif; font-size: 12px; color: rgb(102, 102, 102);">
												</span></td>
											</tr>
											<tr class="ccbnBgTxt" style="background-color: rgb(255, 255, 255);">
												<td align="center" style="font-size: 12px;" valign="top">
												</td>
												<td align="center" style="font-size: 12px;" valign="top">
												</td>
												<td style="font-size: 12px;" width="100%">
												<span class="ccbnTxt" style="font-family: Arial, Verdana, Helvetica, sans-serif; font-size: 12px; color: rgb(102, 102, 102);">
												</span></td>
											</tr>
											
										</table>
										</td>
									</tr>
									<tr class="ccbnBgInput" style="background-color: rgb(255, 255, 255);">
										<td style="font-size: 12px;">
										<span class="ccbnBgButton" style="background-color: rgb(255, 255, 255);">
										<table border="0" cellpadding="0" cellspacing="0" style="font-size: 12px;" width="10%">
											<tr>
												<td style="font-size: 12px;">
												<span class="ccbnNavigation" style="margin-top: 1em; font-size: 13px; font-weight: bold; color: rgb(17, 131, 188);" valign="top">
												</span></td>
												<td style="font-size: 12px;" valign="bottom">
												<input src="images/button_submit.jpg" type="image" /></td>
											</tr>
										</table>
										</span></td>
									</tr>
								</table>
								<%else%>
								<asp:Literal runat="server" ID="literalsubmitreport" ></asp:Literal><br/><br/><br/><br/><br/><br/>
								<%end if%>
								</span></span><br />
								<br />
								<br />

<u1:p></u1:p><br />
						    </div>
						</td>
						<td valign="top" class="background_image">
						&nbsp;</td>
					</tr>
				
				</table>
			</div>
		</div>
	</div>
	<div id="footer_container">
		<div id="footer">
			<table cellpadding="0" cellspacing="0">
				<tr>
				<td valign="top" width="520">
				<div style="width:400px;">
				<big><b>Acadia Realty Trust</b></big><br />
				Acadia Realty Trust
				Acadia Realty Trust (NYSE:AKR) is a fully integrated, self-managed and self-administered equity REIT focused primarily on the ownership, acquisition, redevelopment and management of retail properties, including neighborhood / community shopping centers and mixed-use properties with retail components. 
				</div>
				</td>
				<td valign="top" width="120">
				<big><b>Menu</b></big><br />
				<a href="company.aspx">Company</a><br />
				<a href="properties.aspx">Properties</a><br />
				<a href="Investor_Relations.aspx">Investor Relations</a><br />
				<a href="acquisitions.aspx">Acquisitions</a><br />
				<a href="contact.aspx">Contact Us</a><br />
				</td>
				<td valign="top" width="120">
				<big><b>Follow Us</b></big><br />
				<!--<a href="#">RSS Feed</a><br />-->
				<!--<a href="#">Twitter</a><br />-->
				<a href="#">RSS Feed</a><br />
				<a href="http://www.linkedin.com/companies/acadia-realty-trust" target="_blank">LinkedIn</a>
				</td>
				<td valign="top">
				<big><b>Corporate Headquarters</b></big><br />
				1311 Mamaroneck Avenue<br />
				Suite 260<br />
				White Plains, NY 10605<br />
				<a href="mailto:info@acadiarealty.com">info@acadiarealty.com</a><br />
				<a target="_blank" href="http://maps.google.com/maps?f=d&daddr=1311 Mamaroneck Avenue, White Plains, NY 10605">Google map directions</a><br />
				</td>
				</tr>
				<tr>
					<td colspan="4">
					<br /><br />
					© Copyright 2010 Acadia Realty Trust&nbsp;&nbsp; 
					|&nbsp;&nbsp; <a href="terms.html" target="_blank">Terms and Conditions </a>&nbsp;&nbsp; |&nbsp;&nbsp; 
					<a href="privacy.html" target="_blank">Privacy Statement </a>					</td>
				
				</tr>
			</table>
			</div>
	</div>	
	
	

	<script type="text/javascript">
    
	var timeout    = 500;
	var closetimer = 0;
	var ddmenuitem = 0;
	
	function jsddm_open()
	{  jsddm_canceltimer();
	   jsddm_close();
	   ddmenuitem = $(this).find('ul').css('visibility', 'visible');}
	
	function jsddm_close()
	{  if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}
	
	function jsddm_timer()
	{  closetimer = window.setTimeout(jsddm_close, timeout);}
	
	function jsddm_canceltimer()
	{  if(closetimer)
	   {  window.clearTimeout(closetimer);
	      closetimer = null;}}
	
	$(document).ready(function()
	{  $('#jsddm > li').bind('mouseover', jsddm_open)
	   $('#jsddm > li').bind('mouseout',  jsddm_timer)});
	
	document.onclick = jsddm_close;
</script>
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17447452-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script></form>

</body>
</html>