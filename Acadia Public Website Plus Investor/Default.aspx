﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default_test.aspx.vb" Inherits="_Default_test" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
<style type="text/css">
@import url('css/style.css');
</style>
<!--All Begin-->
<script src="http://cdn.jquerytools.org/1.2.2/full/jquery.tools.min.js" type="text/javascript"></script>
<script src="js/jquery.perciformes.js" type="text/javascript"></script>
<script src="js/navi.js" type="text/javascript"></script>
<script src="http://use.typekit.com/spa4odv.js" type="text/javascript"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<!--All End-->
<script src="js/cs.js" type="text/javascript"></script>
<link href="js/coin-slider-styles.css" rel="stylesheet" type="text/css" />

<style>
#property_search {
	position: absolute;
	top:98px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
</style>
<!--All Begin-->
<!--[if lte IE 7]>
<style type="text/css">
html, body {
	overflow-x: hidden;
}
</style>
<![endif]-->
<style type="text/css">
body {
	background: url("images/background_home.jpg") repeat-x;
}
</style>
<!--[if IE 7]>
<style>
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 20px;
	left:1px;
}

#property_search {
	position: absolute;
	top:122px;
	left: 50%;
	margin-left: 277px;
	z-index: 1;	
	background: #f9a131;
}


</style>
<![endif]-->
<style type="text/css">
@-moz-document url-prefix() {
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1007;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 25px;
	left:1px;
}

#property_search {
	position: absolute;
	top:102px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
#header {
	position:relative;
	margin-top:19px;
	margin-bottom: 14px;	
}
}
.auto-style1 {
	margin-top: 7px;
}
</style>
<!--All End-->
<title>Acadia Realty Trust - Home Page</title>
</head>

<body>

<form id="form1" runat="server">
<!--remove-->
	<div id="container">
	
	<!--All Begin-->
		<div id="property_search">
			<a href="javascript:toggleSearch();">
			<img alt="property search" src="images/property_search.jpg" /></a>
			<div id="property_search_form">
				<asp:PlaceHolder ID="placeholderSearchForm" runat="server"></asp:PlaceHolder>
			</div>
		</div>
		
		<div id="header" style="position:relative;">
			<table cellpadding="0" cellspacing="0" width="100%" >
				<tr>
					<td valign="top" width="424"><a href="default.aspx">
					<img class="naviss" src="images/logo.jpg" /></a></td>
					<td align="right" valign="bottom">
					
						<div style="position:relative;left:0px;right:10px; top:-10px; width: 76px;padding-right:8px;" class="auto-style1">
							<a href="client_login.aspx" style="color:white;" >client login</a></div>
		<ul id="jsddm">
			<li class="main_nav"><a href="company.aspx">
			<img class="navi" src="images/navi_company_off.jpg" /></a>
			<ul>
				<!--<li><a href="about.aspx"><img class="navi" src="images/subnavi_about_off.png" alt="about"/></a></li>-->
				<li><a href="employment.aspx">
				<img alt="employment" class="navi" src="images/subnavi_employment_off.png" /></a></li>
				<!--<li class="last"><a href="benefits.aspx"><img class="navi" src="images/subnavi_benefits_off.png" alt="benefit"/></a></li>-->
			</ul>
			</li>
			<li class="main_nav"><a href="properties.aspx">
			<img class="navi" src="images/navi_properties_off.jpg" /></a> </li>
			<li class="main_nav"><a href="investor_relations.aspx">
			<img class="navi" src="images/navi_ir_off.jpg" /></a>
			<ul>
				<li><a href="news_and_events.aspx">
				<img alt="news and events" class="navi" src="images/subnavi_news_off.png" /></a></li>
				<li><a href="reporting.aspx">
				<img alt="reporting" class="navi" src="images/subnavi_reporting_off.png" /></a></li>
				<li><a href="quarterly_reports.aspx">
				<img alt="quarterly reports" class="navi" src="images/subnavi_quarterly_off.png" /></a></li>
				<li><a href="annual_reports.aspx">
				<img alt="annual reports" class="navi" src="images/subnavi_annual_off.png" /></a></li>
				<li><a href="presentations.aspx">
				<img alt="presentations" class="navi" src="images/subnavi_presentations_off.png" /></a></li>
				<li><a href="analyst_coverage.aspx">
				<img alt="analyst" class="navi" src="images/subnavi_analyst_off.png" /></a></li>
				<li><a href="sec_filings.aspx">
				<img alt="filings" class="navi" src="images/subnavi_filings_off.png" /></a></li>
				<li><a href="dividends.aspx">
				<img alt="dividends" class="navi" src="images/subnavi_dividends_off.png" /></a></li>
				<li><a href="shareholders_information.aspx">
				<img alt="shareholders" class="navi" src="images/subnavi_shareholders_off.png" /></a></li>
				<li><a href="information_request.aspx">
				<img alt="information request" class="navi" src="images/subnavi_information_off.png" /></a></li>
				<li><a href="corporate_governance.aspx">
				<img alt="corporate" class="navi" src="images/subnavi_corporate_off.png" /></a></li>
				<li><a href="board_of_directors.aspx">
				<img alt="board of directors" class="navi" src="images/subnavi_bod_off.png" /></a></li>
				<li><a href="management.aspx">
				<img alt="management" class="navi" src="images/subnavi_management_off.png" /></a></li>
				<li><a href="committee_composition.aspx">
				<img alt="committee compositions" class="navi" src="images/subnavi_committee_off.png" /></a></li>
				<li><a href="faq.aspx">
				<img alt="faq" class="navi" src="images/subnavi_faq_off.png" /></a></li>
				
			</ul>
			</li>
			<li class="main_nav"><a href="acquisitions.aspx">
			<img alt="acquisitions" class="navi" src="images/navi_acquisitions_off.jpg" /></a>
			<!--
					 	<ul>
						      <li><a href="dispositions.aspx"><img class="navi" src="images/subnavi_dispositions_off.png" alt="about"/></a></li>
						      <li class="last"><a href="submit.aspx"><img class="navi" src="images/subnavi_submit_off.png" alt="about"/></a></li>
						    </ul>
						    --></li>
			<li class="main_nav"><a href="contact.aspx">
			<img alt="contact" class="navi" src="images/navi_contact_off.jpg" /></a>
			</li>
		</ul>
					
				<!--remove-->	
				
					</td>
				</tr>
			</table>
		</div>
		<div id="body" class="home">
			<div class="body_content">
				<div style="height: 466px;">
					<div id="gamesHolder">
						<div id="games">
							<asp:Repeater ID="RepeaterFeatured" runat="server">
<ItemTemplate>
<a href="property_detail.aspx?id=<%#Container.DataItem("projectid")%>" target="_top">
<img src="images/ftimage<%#Container.DataItem("projectid") %>.jpg" alt="<%#Container.DataItem("projectid") %>" />
<span>
<div class="slider_messageds_cs"><%#Container.DataItem("challenge")%></div>
<div class="slider_message_cs"><%#Container.DataItem("challenge")%></div>
<div class="slider_title_cs"><b><%#Container.DataItem("name")%>, <%#Container.DataItem("city")%>, <%#Container.DataItem("state_id")%></b></div>		
</span>
</a>
</ItemTemplate>
</asp:Repeater>
						</div>
					</div>
					<div style="display: none;">
<asp:Repeater ID="RepeaterFeaturedCache" runat="server">
<ItemTemplate>

</ItemTemplate>
</asp:Repeater>

										</div>
				</div>
				<table cellpadding="0" cellspacing="0" style="margin-top: 20px;" width="100%">
					<tr>
						<td valign="top" width="235">
						<img alt="stock quote" src="images/stock_quote_home.jpg" />
						<table cellpadding="0" cellspacing="0" class="stock_quote" style="margin-top: 15px;">
							<tr>
								<td colspan="2"><b>
								<asp:Literal ID="literalTicker" runat="server"></asp:Literal>
								</b>(<asp:Literal ID="literalClass" runat="server"></asp:Literal>)</td>
							</tr>
							<tr>
								<td valign="top" width="90">Exchange</td>
								<td align="right" valign="top">
								<span class="blue">
								<asp:Literal ID="literalExchange" runat="server"></asp:Literal>
								</span>(<span class="blue"><asp:Literal ID="literalCurrency" runat="server"></asp:Literal></span>)</td>
							</tr>
							<tr>
								<td valign="top" width="90">Price</td>
								<td align="right" valign="top">
								<span class="blue">
								<asp:Literal ID="literalPrice" runat="server"></asp:Literal>
								</span></td>
							</tr>
							<tr>
								<td valign="top" width="90">Change</td>
								<td align="right" valign="top">
								<asp:Image id="imageChange" runat="server" />
								<span class="blue">
								<asp:Literal ID="literalChange" runat="server"></asp:Literal>
								</span></td>
							</tr>
							<tr>
								<td valign="top" width="90">Volume</td>
								<td align="right" valign="top">
								<span class="blue">
								<asp:Literal ID="literalVolume" runat="server"></asp:Literal>
								</span></td>
							</tr>
						</table>
						<br />
						<small>Data as of
						<asp:Literal ID="literalDate" runat="server"></asp:Literal>
						ET <br />
						(minimum 20 minute delay)</small> </td>
						<td valign="top" width="262">
						<img alt="news and events" src="images/news_and_events_home.jpg" />
						<asp:Repeater ID="RepeaterNews" runat="server">
						        <ItemTemplate>
						            <div class="news_home"><a href="news_release.aspx?id=<%#XPath("@ReleaseID")%>"><%#XPath("Title")%></a></div>
						        </ItemTemplate>
						    </asp:Repeater>
						</td>
						<td valign="top" width="242">
						<a href="investor_relations.aspx">
						<img alt="ir" src="images/home_ir.jpg" /></a></td>
						<td valign="top"><a href="properties.aspx">
						<img alt="properties" src="images/home_properties.jpg" /></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div id="footer_container">
		<div id="footer">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" width="520">
					<div style="width: 400px;">
						<big><b>Acadia Realty Trust</b></big><br />
						Acadia Realty Trust (NYSE:AKR) is a 
						fully integrated, self-managed and self-administered equity 
						REIT focused primarily on the ownership, acquisition, redevelopment 
						and management of retail properties, including neighborhood 
						/ community shopping centers and mixed-use properties with 
						retail components. </div>
					</td>
					<td valign="top" width="120"><big><b>Menu</b></big><br />
					<a href="company.aspx">Company</a><br />
					<a href="properties.aspx">Properties</a><br />
					<a href="Investor_Relations.aspx">Investor Relations</a><br />
					<a href="acquisitions.aspx">Acquisitions</a><br />
					<a href="contact.aspx">Contact Us</a><br />
					</td>
					<td valign="top" width="120"><big><b>Follow Us</b></big><br />
					<!--<a href="#">RSS Feed</a><br />-->
					<!--<a href="#">Twitter</a><br />--><a href="#">RSS Feed</a><br />
					<a href="http://www.linkedin.com/companies/acadia-realty-trust" target="_blank">LinkedIn</a> </td>
					<td valign="top"><big><b>Corporate Headquarters</b></big><br />
					1311 Mamaroneck Avenue<br />
					Suite 260<br />
					White Plains, NY 10605<br />
					<a href="mailto:info@acadiarealty.com">info@acadiarealty.com</a><br />
					<a href="http://maps.google.com/maps?f=d&amp;daddr=1311 Mamaroneck Avenue, White Plains, NY 10605" target="_blank">
					Google map directions</a><br />
					</td>
				</tr>
				<tr>
					<td colspan="4"><br />
					<br />
					© Copyright 2010 Acadia Realty Trust&nbsp;&nbsp; 
					|&nbsp;&nbsp; <a href="terms.html" target="_blank">Terms and Conditions </a>&nbsp;&nbsp; |&nbsp;&nbsp; 
					<a href="privacy.html" target="_blank">Privacy Statement </a> </td>
				</tr>
			</table>
		</div>
	</div>
	<script type="text/javascript">
    $(document).ready(function() {
    
    
$('#games').coinslider({ width: 935, height: 466, spw: 7, sph: 5, delay: 6000, sDelay: 30, opacity: .9, titleSpeed: 3000, effect: 'rain', navigation: true, links: true, hoverPause: true });
        
      
        
    });
    
	var timeout    = 500;
	var closetimer = 0;
	var ddmenuitem = 0;
	
	function jsddm_open()
	{  jsddm_canceltimer();
	   jsddm_close();
	   ddmenuitem = $(this).find('ul').css('visibility', 'visible');}
	
	function jsddm_close()
	{  if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}
	
	function jsddm_timer()
	{  closetimer = window.setTimeout(jsddm_close, timeout);}
	
	function jsddm_canceltimer()
	{  if(closetimer)
	   {  window.clearTimeout(closetimer);
	      closetimer = null;}}
	
	$(document).ready(function()
	{  $('#jsddm > li').bind('mouseover', jsddm_open)
	   $('#jsddm > li').bind('mouseout',  jsddm_timer)});
	
	document.onclick = jsddm_close;
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17447452-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script></form>


</body>

</html>
