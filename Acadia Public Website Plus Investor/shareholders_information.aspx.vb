﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml

Partial Class shareholders_information
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim xmlDS As DataSet = New DataSet()
        
        
        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=quotes", ConfigurationSettings.AppSettings("QuoteXMLFileName"))
        
        xmlDS.ReadXml(ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("QuoteXMLFileName"))

        Dim dt As DataTable = New DataTable()
        dt = xmlDS.Tables("Stock_Quote")

        Dim dtDate = xmlDS.Tables("Date")


        For Each x As DataRow In dt.Rows
            If (x("ticker") = "AKR") Then



                Dim changeString As String = x("change").ToString.Trim & " (" & Math.Round((x("change") / x("open")) * 100, 3) & "%)"
                Try

                    Dim change As Double = Convert.ToDouble(x("change").ToString.Trim)


                    If (change < 0) Then
                        imageChange.ImageUrl = "images/down.gif"
                        literalChange.Text = "<span style=""color:red"">" & changeString & "</span>"
                    Else
                        imageChange.ImageUrl = "images/up.gif"
                        literalChange.Text = "<span style=""color:green"">" & changeString & "</span>"
                    End If

                Catch ex As Exception
                    literalChange.Text = changeString
                End Try


                literalClass.Text = x("class").ToString.Trim
                literalExchange.Text = x("exchange").ToString.Trim
                literalCurrency.Text = x("currency").ToString.Trim
                literalVolume.Text = FormatNumber(x("volume").ToString.Trim, 0)
                literalTicker.Text = x("ticker").ToString.Trim
                literalTrade.Text = x("trade").ToString.Trim
                literal52High.Text = x("FiftyTwoWeekHigh").ToString.Trim
                literal52Low.Text = x("FiftyTwoWeekLow").ToString.Trim
                literalIntraDayHigh.Text = x("High").ToString.Trim
                literalIntraDayLow.Text = x("Low").ToString.Trim
                literalOpen.Text = x("open").ToString.Trim
                literalPreviousOpen.Text = x("PreviousClose").ToString.Trim

                For Each y As DataRow In dtDate.Rows
                    If (y("stock_quote_id") = x("stock_quote_id")) Then
                        literalDate.Text = y("date_text").ToString.Trim
                    End If

                Next


                Exit For
            End If
        Next

    End Sub
End Class
