﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.Text.RegularExpressions
Imports System.IO


Partial Class corporate_governance
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim corpGovDS As DataSet = New DataSet
        corpGovDS.ReadXml("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=cgrelateddocs")


        'corpGovDS.ReadXml(ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("CorpGovXMLFileName"))

        Dim corpGovDT As DataTable = corpGovDS.Tables("Document")

        For Each x As DataRow In corpGovDT.Rows
            If (x("type") = "HighLights") Then
                Dim theText As String = x("text").ToString.Replace("<p>", vbCrLf)
                theText = theText.ToString.Replace("</p>", vbCrLf)

                'literalCGText.Text = Regex.Replace(theText, "<(.|\n)*?>", String.Empty).Replace("Untitled Document", "").Replace(vbCrLf, "<br />")

                literalCGText.Text = x("text")

                x.Delete()
                Exit For
            End If
        Next

        RepeaterCGDocs.DataSource = corpGovDT
        RepeaterCGDocs.DataBind()


        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=committees", ConfigurationSettings.AppSettings("CommitteeXMLFileName"))
        Dim CommiteeDS As XmlDataSource = New XmlDataSource
        CommiteeDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("CommitteeXMLFileName")
        CommiteeDS.XPath = "IRXML/Committees/Committee/Document"


        repeaterCommittees.DataSource = CommiteeDS
        repeaterCommittees.DataBind()


    End Sub
End Class
