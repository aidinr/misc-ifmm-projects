﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml.XPath
Imports System.Text.RegularExpressions

Partial Class committee_composition
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim CommitteeDS As XmlDataSource = New XmlDataSource
        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=committees", ConfigurationSettings.AppSettings("CommitteeXMLFileName"))
        CommitteeDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("CommitteeXMLFileName")
        CommitteeDS.XPath = "IRXML/Committees/Committee"

        RepeaterCommiteeNames.DataSource = CommitteeDS
        RepeaterCommiteeNames.DataBind()

        Dim PeopleDS As XmlDataSource = New XmlDataSource
        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=people2", ConfigurationSettings.AppSettings("PeopleXMLFileName2"))
        PeopleDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("PeopleXMLFileName2")
        PeopleDS.XPath = "IRXML/People/Person[count(Company/Committees/Committee) > 0]"
        'PeopleDS.XPath = "IRXML/People/Person"

        RepeaterCommitteeMembers.DataSource = PeopleDS
        RepeaterCommitteeMembers.DataBind()


    End Sub

    Public Sub RepeaterCommitteeMembersItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim RepeaterMemberParticipation As Repeater = e.Item.FindControl("RepeaterMemberParticipation")

            Dim CommitteeDS As XmlDataSource = New XmlDataSource
            CommitteeDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("CommitteeXMLFileName")
            CommitteeDS.XPath = "IRXML/Committees/Committee"

            RepeaterMemberParticipation.DataSource = CommitteeDS
            RepeaterMemberParticipation.DataBind()

        End If


    End Sub

    Public Sub RepeaterMemberParticipationItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim ImageMemberParticipation As Image = e.Item.FindControl("ImageMemberParticipation")

            Dim currentCommitteeID As String = ""

            Dim navigableDataItem As XPathNavigator = CType(e.Item.DataItem, IXPathNavigable).CreateNavigator

            Dim it As XPathNodeIterator = navigableDataItem.Select("@CommitteeID")
            it.MoveNext()
            currentCommitteeID = it.Current.Value

            Dim navigableDataItemParent As XPathNavigator = CType(CType(e.Item.Parent.Parent, RepeaterItem).DataItem, IXPathNavigable).CreateNavigator

            Dim financialNavigator As XPathNavigator = navigableDataItemParent.SelectSingleNode("IsFinancialExpert")
            Dim it2 As XPathNodeIterator = financialNavigator.Select("IsFinancialExpert")

            it2.MoveNext()


            Dim isExpert As String = it2.Current.Value

            Dim nodeNavigator As XPathNavigator = navigableDataItemParent.SelectSingleNode("Company/Committees/Committee[@ID = '" & currentCommitteeID & "' ]")



            Try
                Dim it3 As XPathNodeIterator = nodeNavigator.Select("@ID")
                it3.MoveNext()

                Dim it4 As XPathNodeIterator = nodeNavigator.Select("IsChair")

                Dim isChair As String = it4.Current.Value

                If (InStr(isChair, "True") > 0) Then
                    ImageMemberParticipation.ImageUrl = "images/committee_chair_icon.jpg"
                ElseIf (isExpert = "True") Then
                    ImageMemberParticipation.ImageUrl = "images/commitee_expert_icon.jpg"
                Else
                    ImageMemberParticipation.ImageUrl = "images/committee_member_icon.jpg"
                End If





            Catch ex As Exception
                ImageMemberParticipation.ImageUrl = "images/spacer.gif"
                'not participating in this committee, do nothing
            End Try
          

         

            'dim parrentCommitteeID As String = CType(e.Item.Parent.Parent,RepeaterItem).DataItem("CommitteeID")


        End If
    End Sub
End Class
