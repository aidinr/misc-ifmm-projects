﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.IO

Partial Class _Default_test
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)


        Dim xmlDS As DataSet = New DataSet()
        'xmlDS.ReadXml(ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("QuoteXMLFileName"))
        xmlDS.ReadXml("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=quotes")




        '     <a href="http://www.minininjas.com/" target="_blank">
        '	<img src="http://www.lisisoft.com/imglisi/5/Screensavers/44637landscapes-2007.jpg" alt="Mini Ninjas" />
        '	<span>
        '		<b>Mini Ninjas</b><br />
        '		Your quest to defeat the Evil Samurai Warlord has begun. Control the powers of nature, possess creatures, use your
        '		furious Ninja skills to free your Ninja friends.
        '	</span>
        '</a>


        Dim ws = New acadiaws.Service1
        Dim featuredDT As DataTable = ws.ListProjects("", "", "", "", "1", "", "", "")




        'cache images
        For Each item In featuredDT.Rows
            SaveUrlToPath(System.AppDomain.CurrentDomain.BaseDirectory & "images\ftimage" & item("projectid") & ".jpg", Session("WSRetreiveAsset") & "qfactor=25&size=1&width=940&height=466&crop=1&type=project&cropcolor=black&id=" & item("projectid"))
        Next


        RepeaterFeatured.DataSource = featuredDT
        RepeaterFeatured.DataBind()
        'if request.querystring("cache") = 1 then

        RepeaterFeaturedCache.DataSource = featuredDT
        RepeaterFeaturedCache.DataBind()

        'end if
        Dim dt As DataTable = New DataTable()
        dt = xmlDS.Tables("Stock_Quote")

        Dim dtDate = xmlDS.Tables("Date")


        For Each x As DataRow In dt.Rows
            If (x("ticker") = "AKR") Then



                Dim changeString As String = x("change").ToString.Trim & " (" & Math.Round((x("change") / x("open")) * 100, 3) & "%)"
                Try

                    Dim change As Double = Convert.ToDouble(x("change").ToString.Trim)


                    If (change < 0) Then
                        imageChange.ImageUrl = "images/down.gif"
                        literalChange.Text = "<span style=""color:red"">" & changeString & "</span>"
                    Else
                        imageChange.ImageUrl = "images/up.gif"
                        literalChange.Text = "<span style=""color:green"">" & changeString & "</span>"
                    End If

                Catch ex As Exception
                    literalChange.Text = changeString
                End Try


                literalClass.Text = x("class").ToString.Trim
                literalExchange.Text = x("exchange").ToString.Trim
                literalCurrency.Text = x("currency").ToString.Trim
                literalVolume.Text = FormatNumber(x("volume").ToString.Trim, 0)
                literalTicker.Text = x("ticker").ToString.Trim
                literalPrice.Text = x("trade").ToString.Trim


                For Each y As DataRow In dtDate.Rows
                    If (y("stock_quote_id") = x("stock_quote_id")) Then
                        literalDate.Text = y("date_text").ToString.Trim
                    End If

                Next


                Exit For
            End If
        Next

        Dim newsDS As XmlDataSource = New XmlDataSource
        newsDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("NewsXMLFileName")
        newsDS.XPath = "IRXML/NewsReleases/NewsCategory/NewsRelease[position()<=3]"



        RepeaterNews.DataSource = newsDS
        RepeaterNews.DataBind()


    End Sub


    Public Shared Function chachepath(ByVal str As String, ByVal chunksize As Integer)
        Dim chunks As New List(Of String)
        Dim offset As Integer = 0
        While (offset < str.Length)
            Dim sizei As Integer = Math.Min(chunksize, str.Length - offset)
            chunks.Add(str.Substring(offset, sizei))
            offset += sizei
        End While
        Dim newpath As String = ""
        Dim i As Integer = 1
        For Each item In chunks
            newpath += item
            If i < chunks.Count Then
                newpath += "/"
            End If
            i += 1
        Next
        Return newpath
    End Function





    Sub CopyStream(ByRef source As Stream, ByRef target As Stream)
        Dim buffer As Byte() = New Byte(65535) {}
        If source.CanSeek Then
            source.Seek(0, SeekOrigin.Begin)
        End If
        Dim bytes As Integer = source.Read(buffer, 0, buffer.Length)
        Try
            While (bytes > 0)

                target.Write(buffer, 0, bytes)
                bytes = source.Read(buffer, 0, buffer.Length)
            End While
        Finally
            ' Or target.Close(); if you're done here already.
            target.Flush()
        End Try
    End Sub



    Sub SaveUrlToPath(ByVal sPath As String, ByVal sURL As String)

        Dim wc As Net.WebClient
        Dim stream As System.IO.Stream
        Try
            Dim t As Date = Now
            wc = New Net.WebClient
            'ping first to prevent a major bug on downloadfile call. If there's a 404, sPath will be deleted
            stream = wc.OpenRead(sURL)
            Dim tempPath As String = sPath
            If Not File.Exists(sPath) Then
                SaveStreamToFilePath(tempPath, stream)
            End If
        Catch ex As Exception

        Finally
            If Not wc Is Nothing Then
                If Not stream Is Nothing Then
                    stream.Close()
                End If
                wc.Dispose()
            End If
        End Try
    End Sub


    Sub SaveStreamToFilePath(ByVal sFilePath As String, ByRef istream As Stream)
        Dim outputStream As New FileStream(sFilePath, FileMode.Create)

        Try
            CopyStream(istream, outputStream)
        Finally
            outputStream.Close()
            istream.Close()
        End Try
    End Sub

End Class
