﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data

Partial Class email_alerts
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)
        Dim _email As String = Request.Form("email")
        Dim _subnews As String = Request.Form("subnews")
        Dim _subsec As String = Request.Form("subsec")
        Dim _subevent As String = Request.Form("subevents")
        'Dim ws = New acadiaws.Service1
        'Dim dtAssets = ws.GetProjectAssets(ConfigurationSettings.AppSettings("AcquisitionsPID"))

        ''For Each x As DataColumn In dtAssets.Columns
        ''    Console.WriteLine(x.ColumnName)
        ''Next

        'Dim dtDownloadAssets As DataTable = New DataTable
        'dtDownloadAssets.Columns.Add("name")
        'dtDownloadAssets.Columns.Add("asset_id")


        'For Each x As DataRow In dtAssets.Rows
        '    Dim r As DataRow = dtDownloadAssets.NewRow
        '    r("name") = x("name")
        '    r("asset_id") = x("asset_id")
        '    dtDownloadAssets.Rows.Add(r)
        'Next

        '' ''Dim URL As String
        '' ''Dim response As String
        '' ''Dim xmlDoc As XDocument


        '' ''xmlDoc = New XDocument
        '' ''URL = "http://www.corporate-ir.net/ireye/xmlsub.asp"

        '' ''xmlDoc = XDocument.Load("C:\Documents and Settings\jdoe\Desktop\alert.xml")

        '' ''xmlDoc.s()
        '' ''xmlObj.open("POST", URL, False)
        '' ''xmlObj.send(xmlDoc.)
        '' ''response = xmlObj.responseText 'view the value of response to see if the post was successful
        '' ''MsgBox("Done")




        '        "				<ALERTS>" & _
        '"					<ALERT SUBSCRIBE=""YES"">ir-news</ALERT>" & _
        '"					<ALERT SUBSCRIBE=""YES"">ir-sec</ALERT>" & _
        '"					<ALERT SUBSCRIBE=""YES"">ir-event</ALERT>" & _
        '"				</ALERTS>" & _
        If isEmail(_email) Then



            Dim RawData As String = _
            "<ALERT_SUBSCRIPTION>" & _
            "	<COMPANY CORPORATE_MASTER_ID=""61503"">" & _
            "		<MEMBERS>" & _
            "			<MEMBER>" & _
            "				<EMAIL_ADDRESS>" & _email & "</EMAIL_ADDRESS>" & _
             "				<ALERTS>"


            If _subnews <> "" Then
                If _subnews = "YES" Then
                    subnewson.text = "checked=""checked"""
                Else
                    subnewsoff.text = "checked=""checked"""
                End If
                RawData += "<ALERT SUBSCRIBE=""" & _subnews & """>ir-news</ALERT>"
            Else
                subnewsoff.text = "checked=""checked"""
            End If
            If _subsec <> "" Then
                If _subsec = "YES" Then
                    subsecon.text = "checked=""checked"""
                Else
                    subsecoff.text = "checked=""checked"""
                End If
                RawData += "<ALERT SUBSCRIBE=""" & _subsec & """>ir-sec</ALERT>"
            Else
                subsecoff.text = "checked=""checked"""
            End If
            If _subevent <> "" Then
                If _subevent = "YES" Then
                    subeventson.text = "checked=""checked"""
                Else
                    subeventsoff.text = "checked=""checked"""
                End If
                RawData += "<ALERT SUBSCRIBE=""" & _subevent & """>ir-event</ALERT>"
            Else
                subeventsoff.text = "checked=""checked"""
            End If

            RawData +="				</ALERTS>" & _
            "			</MEMBER>"


            RawData += _
            "		</MEMBERS>" & _
            "	</COMPANY>" & _
            "</ALERT_SUBSCRIPTION>"


            Dim xdoc As New System.Xml.XmlDocument
            Dim resp As String

            xdoc.LoadXml(RawData)
            Dim bdata() As Byte = System.Text.Encoding.ASCII.GetBytes(xdoc.OuterXml)

            Dim bresp() As Byte

            Dim wc As New System.Net.WebClient
            wc.Headers.Add("Content-Type", "text/xml")
            bresp = wc.UploadData("http://www.corporate-ir.net/ireye/xmlsub.asp ", bdata)

            resp = System.Text.Encoding.ASCII.GetString(bresp)



            Dim respstring As String = ""
            If IsPostBack Then
                respstring = "Your request to subscribe to the following list(s) has been submitted:<br><br>"
                If _subnews = "YES" Then
                    respstring += "<b>Acadia Realty Trust News Alert</b><br>"
                End If
                If _subsec = "YES" Then
                    respstring += "<b>Acadia Realty Trust SEC Filing Alert</b><br>"
                End If
                If _subevent = "YES" Then
                    respstring += "<b>Acadia Realty Trust Event Alert</b><br>"
                End If

                respstring += "<br>An e-mail with user validation has been sent to (" & _email & "). To complete the process, please follow the instructions within the e-mail. "
				literalsubmitreport.text = respstring 

            End If
        End If


    End Sub





    Function isEmail(ByVal inputEmail As String) As Boolean

        inputEmail = inputEmail.ToString()
        Dim strRegex As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
        Dim re As Regex = New Regex(strRegex)
        If (re.IsMatch(inputEmail)) Then
            Return True
        Else
            Return False
        End If

    End Function
End Class
