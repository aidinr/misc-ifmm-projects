﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.Xml.XPath
Imports System.Net
Imports System.IO
Partial Class event_detail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        hiddenFieldID.value = Request.QueryString("id")

        Dim successString As String = Request.QueryString("error")
        Dim errorString As String = Request.QueryString("errors")
        If (successString = "success") Then
            labelSuccess.Text = "Thank you for your request. Your submission has been successfully submitted.<br /><br />"
        ElseIf (successString <> Nothing) Then

            errorString = errorString.Replace("-", "<br />")
            labelSuccess.Text = "A technical error has occured. Please try registering again:<br /><br />" & errorString & "<br /><br />"
        End If

        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=events2", ConfigurationSettings.AppSettings("EventsXMLFileName"))
        Dim doc As XPathDocument = New XPathDocument(ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("EventsXMLFileName"))
        Dim nav As XPathNavigator = doc.CreateNavigator()


        Dim expr As XPathExpression
        '@EventStatus=0 and 
        expr = nav.Compile("IRXML/Events/Event[@EventStatus=0 and @EventID=" & Request.QueryString("id") & "]")

        expr.AddSort("DisplayableDatesTimes/DisplayableDateTime/@EventStartDate", XmlSortOrder.Descending, XmlCaseOrder.None, "", XmlDataType.Text)



        Dim iterator As XPathNodeIterator = nav.Select(expr)


        Do While iterator.MoveNext()

            Dim itTitle As XPathNodeIterator = iterator.Current.Select("EventTitle")
            Dim itDateStart As XPathNodeIterator = iterator.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@EventStartDate")
            Dim itTimeStart As XPathNodeIterator = iterator.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@EventStartTime")
            Dim itDateEnd As XPathNodeIterator = iterator.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@EventEndDate")
            Dim itTimeEnd As XPathNodeIterator = iterator.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@EventEndTime")
            Dim itTimeZone As XPathNodeIterator = iterator.Current.Select("DisplayableDatesTimes/DisplayableDateTime/@TimeZone")
            Dim itDuration As XPathNodeIterator = iterator.Current.Select("EventDuration/@Hours")

            Dim itDrillDown As XPathNodeIterator = iterator.Current.Select("@AllowCalendarToDetailsDrilldown")
            Dim itEventID As XPathNodeIterator = iterator.Current.Select("@EventID")
            Dim itAddress As XPathNodeIterator
            Dim itCity As XPathNodeIterator
            Dim itState As XPathNodeIterator
            Dim locationID As XPathNodeIterator = Nothing

            Dim itWebcasts As XPathNodeIterator

            Try
                itWebcasts = iterator.Current.Select("Webcasts/Webcast/.")

                Do While itWebcasts.MoveNext
                    Dim itProductType As XPathNodeIterator = itWebcasts.Current.Select("@ProductType")
                    itProductType.MoveNext()

                    If (itProductType.Current.Value = "Non-CCBN Product") Then 'external webcast path
                        Dim itExternalURL As XPathNodeIterator = itWebcasts.Current.Select("Streams/Stream/URL/.")
                        itExternalURL.MoveNext()
                        hyperlinkWebCast.NavigateUrl = itExternalURL.Current.Value
                    Else ' thompson reuters path
                        Dim itstreamID As XPathNodeIterator = itWebcasts.Current.Select("Streams/Stream/@ID")
                        Dim itRegister As XPathNodeIterator = itWebcasts.Current.Select("Streams/Stream/@RegistrationRequired")



                        itstreamID.MoveNext()
                        itRegister.MoveNext()

                        If (itRegister.Current.Value = "No") Then
                            hyperlinkWebCast.NavigateUrl = ConfigurationSettings.AppSettings("BaseWebCastURL") & "EventId=" & Request.QueryString("id") & "&StreamId=" & itstreamID.Current.Value & "RGS=1"
                        Else
                            placeholderForm.Visible = "true"
                            hiddenFieldStreamID.Value = itstreamID.Current.Value
                            hyperlinkWebCast.Visible = False

                        End If



                    End If
                Loop

            Catch ex As Exception

            End Try


            Dim itsupportMaterials As XPathNodeIterator
            Dim itSpeakers As XPathNodeIterator




            Dim theAddress As String = ""
            Dim theCity As String = ""
            Dim theState As String = ""

            Dim thesupportMaterialString As String = ""
            Dim theSpeakersString As String = ""





            Try
                itAddress = iterator.Current.Select("Location/Address1/.")
                itAddress.MoveNext()
                If (itAddress.CurrentPosition > 0) Then
                    theAddress = itAddress.Current.Value
                End If
            Catch ex As Exception

            End Try

            Try
                itCity = iterator.Current.Select("Location/City/.")
                itCity.MoveNext()
                If (itCity.CurrentPosition > 0) Then
                    theCity = itCity.Current.Value
                End If

            Catch ex As Exception

            End Try

            Try
                itState = iterator.Current.Select("Location/State/.")
                itState.MoveNext()
                If (itState.CurrentPosition > 0) Then
                    theState = itState.Current.Value
                End If
            Catch ex As Exception

            End Try

            Try
                itsupportMaterials = iterator.Current.Select("SupportMaterials/SupportMaterial")
                Dim initFlag As Integer = 0

                Do While itsupportMaterials.MoveNext
                    If (initFlag = 0) Then
                        initFlag = 1
                        thesupportMaterialString = thesupportMaterialString & "<b>Supporting Materials:</b> <br /><br />"
                    End If
                    Dim itsupportMaterialsURL As XPathNodeIterator = itsupportMaterials.Current.Select("URL/.")
                    Dim itsupportMaterialsLabel As XPathNodeIterator = itsupportMaterials.Current.Select("LinkLabel/.")
                    Dim itsupportMaterialIcon As XPathNodeIterator = itsupportMaterials.Current.Select("Encodings/Encoding/EncodingIconUrl/.")

                    itsupportMaterialsURL.MoveNext()
                    itsupportMaterialsLabel.MoveNext()
                    itsupportMaterialIcon.MoveNext()


                    thesupportMaterialString = thesupportMaterialString & "<img src=""" & itsupportMaterialIcon.Current.Value.ToString.Replace("/", "/") & """ alt=""encoding"" /> &nbsp; <a href=""" & itsupportMaterialsURL.Current.Value & """>" & itsupportMaterialsLabel.Current.Value & "</a><br />"

                Loop

            Catch ex As Exception

            End Try

            Try
                itSpeakers = iterator.Current.Select("Speakers/Speaker")

                Dim initFlag As Integer = 0
                Do While itSpeakers.MoveNext
                    If (initFlag = 0) Then
                        initFlag = 1
                    End If
                    Dim itSpeakersFirstName As XPathNodeIterator = itSpeakers.Current.Select("FirstName/.")
                    Dim itSpeakersMiddleName As XPathNodeIterator = itSpeakers.Current.Select("MiddleInitial/.")
                    Dim itSpeakersLastName As XPathNodeIterator = itSpeakers.Current.Select("LastName/.")
                    Dim itSpeakersTitle As XPathNodeIterator = itSpeakers.Current.Select("Title/.")

                    itSpeakersFirstName.MoveNext()
                    itSpeakersLastName.MoveNext()
                    itSpeakersMiddleName.MoveNext()
                    itSpeakersTitle.MoveNext()

                    Dim mnameString As String = ""

                    If (itSpeakersMiddleName.CurrentPosition > 0) Then
                        mnameString = itSpeakersMiddleName.Current.Value
                    End If

                    theSpeakersString = theSpeakersString & itSpeakersFirstName.Current.Value & " " & " " & mnameString & " " & itSpeakersLastName.Current.Value & ", " & itSpeakersTitle.Current.Value & "<br />"



                Loop
            Catch ex As Exception

            End Try

            itTitle.MoveNext()
            itDateStart.MoveNext()
            itTimeStart.MoveNext()
            itDateEnd.MoveNext()
            itTimeEnd.MoveNext()
            itDrillDown.MoveNext()
            itEventID.MoveNext()
            itTimeZone.MoveNext()
            itDuration.MoveNext()


            Dim theTitle As String = itTitle.Current.Value
            Dim theStartDate As DateTime = Nothing
            Dim theStartTime As DateTime = Nothing
            Dim theEndDate As DateTime = Nothing
            Dim theEndTime As DateTime = Nothing
            Dim theTimeZone As String = itTimeZone.Current.Value
            Dim theDuration As String = itDuration.Current.Value

            Dim theDrillDown As String = itDrillDown.Current.Value
            Dim theEID As String = itEventID.Current.Value




            Dim provider As CultureInfo = CultureInfo.InvariantCulture

            Try
                theStartDate = DateTime.ParseExact(itDateStart.Current.Value, "yyyyMMdd", provider)
                If (theStartDate > FormatDateTime(Date.Now, DateFormat.ShortDate)) Then
                    'Continue Do

                End If
                 If (theStartDate < FormatDateTime(Date.Now, DateFormat.ShortDate)) Then

                    placeholderForm.Visible = "false"
                End If
            Catch ex As Exception
                'Continue Do
            End Try
            Try
                theEndDate = DateTime.ParseExact(itDateEnd.Current.Value, "yyyyMMdd", provider)
            Catch ex As Exception

            End Try
            Try
                theStartTime = DateTime.Parse(itTimeStart.Current.Value, provider)
            Catch ex As Exception

            End Try
            Try
                theEndTime = DateTime.Parse(itTimeEnd.Current.Value, provider)
            Catch ex As Exception

            End Try

            Dim theTime As String = ""

            theTime = theTime & theStartDate.ToString("MM/dd/yy")

            If (theStartTime <> Nothing) Then
                theTime = theTime & " " & theStartTime.ToString("hh:mm tt") & " " & theTimeZone & "<br /> "
            End If

            If (theEndDate <> Nothing) Then
                theTime = theTime & "through <br />" & theEndDate.ToString("MM/dd/yy")

            End If
            If (theEndDate <> Nothing) Then
                theTime = theTime & " " & theEndTime.ToString("hh:mm tt") & " " & theTimeZone & "<br /> "
            End If

            If (theSpeakersString <> "") Then
                literalspeakersnames.Text = theSpeakersString
                literalspeakerstitle.Text = "<b>Speakers</b>"
            End If

            literalDateAndTime.Text = theTime
            literalDuration.Text = theDuration & " Hour(s)"

            Dim theAddressString As String = ""


            If (theAddress <> "" Or theCity <> "" Or theState <> "") Then
                If (theAddress <> "") Then
                    theAddressString = theAddressString & theAddress & ", "
                End If
                If (theCity <> "") Then
                    theAddressString = theAddressString & theCity & ", "
                End If
                If (theState <> "") Then
                    theAddressString = theAddressString & theState & ""
                End If
                theAddressString = theAddressString & "<br />"
                literallocationname.Text = theAddressString
                literallocationtitle.Text = "<b>Location</b>"
            End If



            If (thesupportMaterialString <> "") Then
                literalSupportingMaterials.Text = thesupportMaterialString & "<br /><br />"
            End If

            literalTitle.Text = theTitle


        Loop



    End Sub

    Sub imageButtonRegister_click(ByVal sender As Object, ByVal e As EventArgs) Handles imageButtonRegister.Click

        Dim firstName As String = textboxFirstName.Text
        Dim lastname As String = textboxLastName.Text
        Dim email As String = textboxEmail.Text
        Dim organization As String = textboxOrganization.Text
        Dim theID As String = hiddenFieldID.Value

        Dim theXMLString As String = "<IRXML CorpMasterID=""" & ConfigurationSettings.AppSettings("compID") & """><Registration><Registrant  EventID=""" & theID & """>"

        If (firstName <> "") Then
            theXMLString = theXMLString & "<FName>" & firstName & "</FName>"
        End If
        If (lastname <> "") Then
            theXMLString = theXMLString & "<LName>" & lastname & "</LName>"
        End If
        If (email <> "") Then
            theXMLString = theXMLString & "<Email>" & email & "</Email>"
        End If
        If (organization <> "") Then
            theXMLString = theXMLString & "<Institution>" & organization & "</Institution>"
        End If

        theXMLString = theXMLString & "<Response/>"
        theXMLString = theXMLString & "</Registrant></Registration></IRXML>"


        Dim request As WebRequest = WebRequest.Create("http://www.corporate-ir.net/ireye/xmlreg.asp")

        request.Method = "post"
        request.ContentType = "text/xml"
        request.ContentLength = theXMLString.Length.ToString

        Dim newStream As Stream = request.GetRequestStream()

        Dim encoding As ASCIIEncoding = New ASCIIEncoding()

        Dim postdata As Byte() = encoding.GetBytes(theXMLString)


        newStream.Write(postdata, 0, postdata.Length)
        ''Console.WriteLine("The value of 'ContentLength' property after sending the data is {0}", request.ContentLength)
        newStream.Flush()
        newStream.Close()

        Try
            Dim webResponse As WebResponse = request.GetResponse
            'Dim i As Integer = 0

            'While i < webResponse.Headers.Count
            '    Console.WriteLine(ControlChars.Cr + "Header Name:{0}, Header value :{1}", webResponse.Headers.Keys(i), webResponse.Headers(i))
            '    i = i + 1
            'End While

            Dim theResponseStream As Stream = webResponse.GetResponseStream
            Dim readData(webResponse.ContentLength) As Byte


            theResponseStream.Read(readData, 0, webResponse.ContentLength)

            theResponseStream.Close()



            'Dim theresponsestring As String = encoding.GetChars(readData)

            Dim theResponseXml As XmlDocument = New XmlDocument()

            Dim memStream As MemoryStream = New MemoryStream(readData)

            theResponseXml.Load(memStream)

            Dim nav As XPathNavigator = theResponseXml.CreateNavigator
            Dim itCode As XPathNodeIterator = nav.Select("IRXML/Registration/Registrant/Response/return_code/@code")

            itCode.MoveNext()

            Dim theCode As String = itCode.Current.Value

            If (theCode = 0) Then ' no errors, proceed to registrations
                Session("Webcast_Authorize_" & theID.ToString) = True
                Response.Redirect("view_webcast.aspx?id=" & theID & "&streamid=" & hiddenFieldStreamID.Value, False)
                'Response.End()

            Else
                Dim theErrorsString As String = ""
                Dim itErrors As XPathNodeIterator = nav.Select("IRXML/Registration/Registrant/Response/errors/error/.")

                While itErrors.MoveNext
                    theErrorsString = itErrors.Current.Value & "-"



                End While


                Response.Redirect("event_detail.aspx?error=error&errors=" & theErrorsString & "id=" & theID)
            End If
        Catch ex As Exception
            Response.Redirect("event_detail.aspx?error=error&errors=A technical error has occured&id=" & theID)
        End Try
       


    End Sub



End Class
