﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.Xml.XPath

Imports HtmlAgilityPack


Partial Class reporting
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)


        Select Case Request.QueryString("p")
            Case "irol-fundSnapshot"


                Dim hw As HtmlAgilityPack.HtmlWeb = New HtmlWeb
                Dim doc As HtmlDocument = hw.Load("http://investors.acadiarealty.com/phoenix.zhtml?c=61503&p=irol-fundSnapshot")
                Dim str As String = doc.DocumentNode.ChildNodes.Item(1).ChildNodes.Item(2).ChildNodes.Item(12).ChildNodes.Item(1).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(3).OuterHtml.Replace("<a ", "<aa ")
                scappedhtml_fundSnapshot.Text = str





                ' '' ''Snapshot
                '' ''Dim trSnapshot As XmlDataSource = New XmlDataSource
                '' ''Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=trsnapshot", ConfigurationSettings.AppSettings("snapshot"))
                '' ''trSnapshot.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("snapshot")
                '' ''trSnapshot.XPath = "IRXML/SnapshotIRXML/Snapshot/*[@Type='Revenue']"

                '' ''RepeaterRevenueAndEarnings.DataSource = trSnapshot
                '' ''RepeaterRevenueAndEarnings.DataBind()

                '' ''trSnapshot.XPath = "IRXML/SnapshotIRXML/Snapshot/*[@Type='Ratios']"
                '' ''RepeaterRatios.DataSource = trSnapshot
                '' ''RepeaterRatios.DataBind()

                '' ''trSnapshot.XPath = "IRXML/SnapshotIRXML/Snapshot/*[@Type='Dividends']"
                '' ''RepeaterDividends.DataSource = trSnapshot
                '' ''RepeaterDividends.DataBind()

                '' ''trSnapshot.XPath = "IRXML/SnapshotIRXML/Snapshot/*[@Type='Growth']"
                '' ''RepeaterGrowth.DataSource = trSnapshot
                '' ''RepeaterGrowth.DataBind()


            Case "irol-fundTrading"


                Dim hw As HtmlAgilityPack.HtmlWeb = New HtmlWeb
                Dim doc As HtmlDocument = hw.Load("http://investors.acadiarealty.com/phoenix.zhtml?c=61503&p=irol-fundTrading")
                Dim str As String = doc.DocumentNode.ChildNodes.Item(1).ChildNodes.Item(2).ChildNodes.Item(12).ChildNodes.Item(1).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(3).OuterHtml.Replace("<a ", "<aa ")
                scappedhtml_fundTrading.Text = str



             

            Case "irol-fundRatios"
                Dim hw As HtmlAgilityPack.HtmlWeb = New HtmlWeb
                Dim doc As HtmlDocument = hw.Load("http://investors.acadiarealty.com/phoenix.zhtml?c=61503&p=irol-fundRatios")
                Dim str As String = doc.DocumentNode.ChildNodes.Item(1).ChildNodes.Item(2).ChildNodes.Item(12).ChildNodes.Item(1).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(3).OuterHtml.Replace("<a ", "<aa ")
                scappedhtml_fundRatios.Text = str

            Case "irol-fundBalanceA"
                Dim hw As HtmlAgilityPack.HtmlWeb = New HtmlWeb
                Dim doc As HtmlDocument = hw.Load("http://investors.acadiarealty.com/phoenix.zhtml?c=61503&p=irol-fundBalanceA")
                Dim str As String = doc.DocumentNode.ChildNodes.Item(1).ChildNodes.Item(2).ChildNodes.Item(12).ChildNodes.Item(1).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(5).OuterHtml.Replace("<a ", "<aa ")
                scappedhtml_fundBalanceA.Text = "<div class=""reportheading""><a href=""reporting.aspx?p=irol-fundTrading""><b>Balance Sheet - Annual</b></a></div><br />" & str
            Case "irol-fundBalanceQ"
                Dim hw2 As HtmlAgilityPack.HtmlWeb = New HtmlWeb
                Dim doc2 As HtmlDocument = hw2.Load("http://investors.acadiarealty.com/phoenix.zhtml?c=61503&p=irol-fundBalanceQ")
                Dim str2 As String = doc2.DocumentNode.ChildNodes.Item(1).ChildNodes.Item(2).ChildNodes.Item(12).ChildNodes.Item(1).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(5).OuterHtml.Replace("<a ", "<aa ")
                scappedhtml_fundBalanceQ.Text = "<div class=""reportheading""><a href=""reporting.aspx?p=irol-fundTrading""><b>Balance Sheet - Quarterly</b></a></div><br />" & str2
            Case "irol-fundIncomeA"
                Dim hw3 As HtmlAgilityPack.HtmlWeb = New HtmlWeb
                Dim doc3 As HtmlDocument = hw3.Load("http://investors.acadiarealty.com/phoenix.zhtml?c=61503&p=irol-fundIncomeA")
                Dim str3 As String = doc3.DocumentNode.ChildNodes.Item(1).ChildNodes.Item(2).ChildNodes.Item(12).ChildNodes.Item(1).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(5).OuterHtml.Replace("<a ", "<aa ")
                scappedhtml_fundIncomeA.Text = "<div class=""reportheading""><a href=""reporting.aspx?p=irol-fundTrading""><b>Income Statement - Annual</b></a></div><br />" & str3
            Case "irol-fundIncomeQ"
                Dim hw4 As HtmlAgilityPack.HtmlWeb = New HtmlWeb
                Dim doc4 As HtmlDocument = hw4.Load("http://investors.acadiarealty.com/phoenix.zhtml?c=61503&p=irol-fundIncomeQ")
                Dim str4 As String = doc4.DocumentNode.ChildNodes.Item(1).ChildNodes.Item(2).ChildNodes.Item(12).ChildNodes.Item(1).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(5).OuterHtml.Replace("<a ", "<aa ")
                scappedhtml_fundIncomeQ.Text = "<div class=""reportheading""><a href=""reporting.aspx?p=irol-fundTrading""><b>Income Statement - Quarterly</b></a></div><br />" & str4
            Case "irol-fundCashFlowA"
                Dim hw5 As HtmlAgilityPack.HtmlWeb = New HtmlWeb
                Dim doc5 As HtmlDocument = hw5.Load("http://investors.acadiarealty.com/phoenix.zhtml?c=61503&p=irol-fundCashFlowA")
                Dim str5 As String = doc5.DocumentNode.ChildNodes.Item(1).ChildNodes.Item(2).ChildNodes.Item(12).ChildNodes.Item(1).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(5).OuterHtml.Replace("<a ", "<aa ")
                scappedhtml_fundCashFlowA.Text = "<div class=""reportheading""><a href=""reporting.aspx?p=irol-fundTrading""><b>Cash Flow - Annual</b></a></div><br />" & str5
            Case "irol-fundCashFlowQ"
                Dim hw6 As HtmlAgilityPack.HtmlWeb = New HtmlWeb
                Dim doc6 As HtmlDocument = hw6.Load("http://investors.acadiarealty.com/phoenix.zhtml?c=61503&p=irol-fundCashFlowQ")
                Dim str6 As String = doc6.DocumentNode.ChildNodes.Item(1).ChildNodes.Item(2).ChildNodes.Item(12).ChildNodes.Item(1).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(5).OuterHtml.Replace("<a ", "<aa ")
                scappedhtml_fundCashFlowQ.Text = "<div class=""reportheading""><a href=""reporting.aspx?p=irol-fundTrading""><b>Cash Flow - Quarterly</b></a></div><br />" & str6
            Case Else
 'Snapshot

                Dim hw As HtmlAgilityPack.HtmlWeb = New HtmlWeb
                Dim doc As HtmlDocument = hw.Load("http://investors.acadiarealty.com/phoenix.zhtml?c=61503&p=irol-fundSnapshot")
                Dim str As String = doc.DocumentNode.ChildNodes.Item(1).ChildNodes.Item(2).ChildNodes.Item(12).ChildNodes.Item(1).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes.Item(3).OuterHtml.Replace("<a ", "<aa ")
                scappedhtml_fundSnapshot.Text = str


                ' '' ''Dim trSnapshot As XmlDataSource = New XmlDataSource
                ' '' ''Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=trsnapshot", ConfigurationSettings.AppSettings("snapshot"))
                ' '' ''trSnapshot.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("snapshot")
                ' '' ''trSnapshot.XPath = "IRXML/SnapshotIRXML/Snapshot/*[@Type='Revenue']"

                ' '' ''RepeaterRevenueAndEarnings.DataSource = trSnapshot
                ' '' ''RepeaterRevenueAndEarnings.DataBind()

                ' '' ''trSnapshot.XPath = "IRXML/SnapshotIRXML/Snapshot/*[@Type='Ratios']"
                ' '' ''RepeaterRatios.DataSource = trSnapshot
                ' '' ''RepeaterRatios.DataBind()

                ' '' ''trSnapshot.XPath = "IRXML/SnapshotIRXML/Snapshot/*[@Type='Dividends']"
                ' '' ''RepeaterDividends.DataSource = trSnapshot
                ' '' ''RepeaterDividends.DataBind()

                ' '' ''trSnapshot.XPath = "IRXML/SnapshotIRXML/Snapshot/*[@Type='Growth']"
                ' '' ''RepeaterGrowth.DataSource = trSnapshot
                ' '' ''RepeaterGrowth.DataBind()
        End Select

       
    End Sub

    Public Function cConverttoString(ByVal str As String) As String
        Try
            If IsNumeric(str) Then 'decimal'
                Return Format(CType(str, Decimal), "#.##").Replace("NaN", "")
            Else
                'return string
                Return str
            End If

        Catch ex As Exception
            Return str
        End Try
    End Function

    Public Sub RepeaterDividendsOnItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim literalDividend As Literal = e.Item.FindControl("literalDividend")

            Dim navigableDataItem As XPathNavigator = CType(e.Item.DataItem, IXPathNavigable).CreateNavigator

            Dim it As XPathNodeIterator = navigableDataItem.Select(".")
            it.MoveNext()
            Dim theString = it.Current.Value

            Try
                literalDividend.Text = Math.Round(CDbl(theString), 2)


            Catch ex As Exception

                literalDividend.Text = theString

            End Try
           


        End If
    End Sub

End Class
