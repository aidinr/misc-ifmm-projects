﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Partial Class faq
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim FAQDS As XmlDataSource = New XmlDataSource
        Functions.ConvertDataSetToXMLDataSource("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=faq", ConfigurationSettings.AppSettings("FAQXMLFileName"))
        FAQDS.DataFile = ConfigurationSettings.AppSettings("XMLLocation") & ConfigurationSettings.AppSettings("FAQXMLFileName")
        FAQDS.XPath = "IRXML/FAQS/FAQ"

        RepeaterFAQ.DataSource = FAQDS
        RepeaterFAQ.DataBind()
    End Sub
End Class
