<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Benefits.aspx.vb" Inherits="Benefits" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<style type="text/css">
 	@import url("css/style.css");
</style>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery.perciformes.js"></script>
<script type="text/javascript" src="js/navi.js"></script>


<title>Acadia Realty Trust - Company</title>
</head>
<body>
    <form id="form1" runat="server">
   <div id="container">
		<div id="header">
			<table width="100%" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				<td width="424" valign="top"><a href="default.aspx"><img class="navi" src="images/logo.jpg" /></a></td>
				<td valign="bottom" align="right">
				<ul id="nav">
					 <li class="main_nav"><a href="company.aspx"><img  src="images/navi_company_on.jpg" /></a>
					 	<ul>
						      <!--<li><a href="about.aspx"><img class="navi" src="images/subnavi_about_off.png" alt="about"/></a></li>-->
						      <li><a href="employment.aspx"><img class="navi" src="images/subnavi_employment_off.png" alt="employment"/></a></li>
						      <li class="last"><a href="benefits.aspx"><img src="images/subnavi_benefits_on.png" alt="benefit"/></a></li>
						    </ul>
					  </li>
					   <li class="main_nav"><a href="properties.aspx"><img class="navi" src="images/navi_properties_off.jpg" /></a>
					  </li>
					   <li class="main_nav"><a href="#"><img class="navi" src="images/navi_ir_off.jpg" /></a>
					  </li>
					   <li class="main_nav"><a href="acquisitions.aspx"><img class="navi" src="images/navi_acquisitions_off.jpg" /></a>
					 	<!--<ul>
						      <li><a href="dispositions.aspx"><img class="navi" src="images/subnavi_dispositions_off.png" alt="about"/></a></li>
						      <li class="last"><a href="submit.aspx"><img class="navi" src="images/subnavi_submit_off.png" alt="about"/></a></li>
						    </ul>-->
					  </li>
					   <li class="main_nav"><a href="contact.aspx"><img class="navi" src="images/navi_contact_off.jpg" /></a>
					  </li>
				</ul>
				
				</td>
				</tr>
			</table>
		</div>
		<div id="body">
			<div class="body_content">
				
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td valign="top" width="584">
						
						  <div class="cookie_crumbs"><a href="Default.aspx">Home</a> | <a href="company.aspx">Company</a> | Benefits</div>
						    
						    <div class="general_title"><img src="images/benefits_title.jpg" alt="Benefits" /></div>
						
						    <div class="general_content">
						    
						    
						 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam facilisis, tortor eget eleifend feugiat, diam libero consectetur erat, eget feugiat dui magna vel orci. Vestibulum eu elementum lectus. Ut dictum nisi a leo ultrices accumsan. Nullam nec condimentum orci. Vivamus a gravida orci. Fusce porta cursus massa, vel placerat diam fermentum ut. Vestibulum feugiat hendrerit consectetur. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean auctor lacus augue. Pellentesque commodo, turpis non facilisis fringilla, neque libero rutrum dolor, sit amet blandit erat sem quis lectus. Nulla ligula justo, placerat eu vestibulum at, molestie eu lorem. In euismod venenatis facilisis.
<br /><br />
•  Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br />
•  Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br />
•  Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br />
•  Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br />
•  Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br />
•  Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br />
•  Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br />

						    
						    </div>
						</td>
						<td valign="top" class="background_image"><img class="general_image" src="images/company_image.jpg" alt="properties" /></td>
					</tr>
				
				</table>
			</div>
		</div>
	</div>
	<div id="footer_container">
		<div id="footer">
			<table cellpadding="0" cellspacing="0">
				<tr>
				<td valign="top" width="520">
				<div style="width:400px;">
				<big><b>Acadia Realty Trust</b></big><br />
				Acadia Realty Trust
				Acadia Realty Trust (NYSE:AKR) is a fully integrated, self-managed and self-administered equity REIT focused primarily on the ownership, acquisition, redevelopment and management of retail properties, including neighborhood / community shopping centers and mixed-use properties with retail components. 
				</div>
				</td>
				<td valign="top" width="120">
				<big><b>Menu</b></big><br />
				<a href="company.aspx">Company</a><br />
				<a href="properties.aspx">Properties</a><br />
				<a href="Investor_Relations.aspx">Investor Relations</a><br />
				<a href="acquisitions.aspx">Acquisitions</a><br />
				<a href="contact.aspx">Contact Us</a><br />
				</td>
				<td valign="top" width="120">
				<big><b>Follow Us</b></big><br />
				<!--<a href="#">RSS Feed</a><br />-->
				<!--<a href="#">Twitter</a><br />-->
				<a href="#">RSS Feed</a><br />
				<a href="http://www.linkedin.com/companies/acadia-realty-trust" target="_blank">LinkedIn</a>
				</td>
				<td valign="top">
				<big><b>Corporate Headquarters</b></big><br />
				1311 Mamaroneck Avenue<br />
				Suite 260<br />
				White Plains, NY 10605<br />
				<a href="mailto:info@acadiarealty.com">info@acadiarealty.com</a><br />
				<a target="_blank" href="http://maps.google.com/maps?f=d&daddr=1311 Mamaroneck Avenue, White Plains, NY 10605">Google map directions</a><br />
				</td>
				</tr>
				<tr>
					<td colspan="4">
					<br /><br />
					© Copyright 2010 Acadia Realty Trust&nbsp;&nbsp; 
					|&nbsp;&nbsp; <a href="terms.html" target="_blank">Terms and Conditions </a>&nbsp;&nbsp; |&nbsp;&nbsp; 
					<a href="privacy.html" target="_blank">Privacy Statement </a>					</td>
				
				</tr>
			</table>
			</div>
	</div>	
	
	
    	<div id="property_search">
		<a href="javascript:toggleSearch();"><img src="images/property_search.jpg" alt="property search" /></a>
		<div id="property_search_form">

	        <asp:PlaceHolder ID="placeholderSearchForm" runat="server"></asp:PlaceHolder>

	        </div>
	</div>
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17447452-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script></form>

</body>
</html>
