<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EmploymentApplication.aspx.vb" Inherits="EmploymentApplication" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 <style type="text/css">
 	@import url("css/style.css");
</style>
<script src="http://cdn.jquerytools.org/1.2.2/full/jquery.tools.min.js" type="text/javascript"></script>
<script src="js/jquery.perciformes.js" type="text/javascript"></script>
<script src="js/navi.js" type="text/javascript"></script>
<script src="http://use.typekit.com/spa4odv.js" type="text/javascript"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>


<style>
#property_search {
	position: absolute;
	top:98px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
</style>

<!--[if lte IE 7]>
<style type="text/css">
html, body {
	overflow-x: hidden;
}
</style>
<![endif]-->
<!--[if IE 7]>
<style>
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 20px;
	left:1px;
}

#property_search {
	position: absolute;
	top:122px;
	left: 50%;
	margin-left: 277px;
	z-index: 1;	
	background: #f9a131;
}


</style>
<![endif]-->
<style type="text/css">
@-moz-document url-prefix() {
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1007;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 25px;
	left:1px;
}

#property_search {
	position: absolute;
	top:102px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
#header {
	position:relative;
	margin-top:19px;
	margin-bottom: 14px;	
}
}
</style>



<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    function validateForm() {

        $('form').valid();
    };

</script>
<title>Acadia Realty Trust - Company - Employment Application</title>
</head>
<body>
    <form id="form1" runat="server">
				
    <div id="container">
    <div id="header">
    <!--property search-->
		<div id="property_search">
		<a href="javascript:toggleSearch();"><img src="images/property_search.jpg" alt="property search" /></a>
		<div id="property_search_form">
	        <asp:PlaceHolder ID="placeholderSearchForm" runat="server"></asp:PlaceHolder>
	        </div>
		</div>
		<!-- end of property search-->
			<table width="100%" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				<td width="424" valign="top"><a href="default.aspx"><img class="navi" src="images/logo.jpg" /></a></td>
				<td valign="bottom" align="right">
				<!--menu-->
		<ul id="jsddm">
			<li class="main_nav"><a href="company.aspx">
			<img src="images/navi_company_on.jpg" /></a>
			<ul>
				<!--<li><a href="about.aspx"><img class="navi" src="images/subnavi_about_off.png" alt="about"/></a></li>-->
				<li><a href="employment.aspx">
				<img alt="employment" src="images/subnavi_employment_on.png" /></a></li>
				<!--<li class="last"><a href="benefits.aspx"><img class="navi" src="images/subnavi_benefits_off.png" alt="benefit"/></a></li>-->
			</ul>
			</li>
			<li class="main_nav"><a href="properties.aspx">
			<img class="navi" src="images/navi_properties_off.jpg" /></a> </li>
			<li class="main_nav"><a href="investor_relations.aspx">
			<img class="navi" src="images/navi_ir_off.jpg" /></a>
			<ul>
				<li><a href="news_and_events.aspx">
				<img alt="news and events" class="navi" src="images/subnavi_news_off.png" /></a></li>
				<li><a href="reporting.aspx">
				<img alt="reporting" class="navi" src="images/subnavi_reporting_off.png" /></a></li>
				<li><a href="quarterly_reports.aspx">
				<img alt="quarterly reports" class="navi" src="images/subnavi_quarterly_off.png" /></a></li>
				<li><a href="annual_reports.aspx">
				<img alt="annual reports" class="navi" src="images/subnavi_annual_off.png" /></a></li>
				<li><a href="presentations.aspx">
				<img alt="presentations" class="navi" src="images/subnavi_presentations_off.png" /></a></li>
				<li><a href="analyst_coverage.aspx">
				<img alt="analyst" class="navi" src="images/subnavi_analyst_off.png" /></a></li>
				<li><a href="sec_filings.aspx">
				<img alt="filings" class="navi" src="images/subnavi_filings_off.png" /></a></li>
				<li><a href="dividends.aspx">
				<img alt="dividends" class="navi" src="images/subnavi_dividends_off.png" /></a></li>
				<li><a href="shareholders_information.aspx">
				<img alt="shareholders" class="navi" src="images/subnavi_shareholders_off.png" /></a></li>
				<li><a href="information_request.aspx">
				<img alt="information request" class="navi" src="images/subnavi_information_off.png" /></a></li>
				<li><a href="corporate_governance.aspx">
				<img alt="corporate" class="navi" src="images/subnavi_corporate_off.png" /></a></li>
				<li><a href="board_of_directors.aspx">
				<img alt="board of directors" class="navi" src="images/subnavi_bod_off.png" /></a></li>
				<li><a href="management.aspx">
				<img alt="management" class="navi" src="images/subnavi_management_off.png" /></a></li>
				<li><a href="committee_composition.aspx">
				<img alt="committee compositions" class="navi" src="images/subnavi_committee_off.png" /></a></li>
				<li><a href="faq.aspx">
				<img alt="faq" class="navi" src="images/subnavi_faq_off.png" /></a></li>
			</ul>
			</li>
			<li class="main_nav"><a href="acquisitions.aspx">
			<img alt="acquisitions" class="navi" src="images/navi_acquisitions_off.jpg" /></a>
			<!--
					 	<ul>
						      <li><a href="dispositions.aspx"><img class="navi" src="images/subnavi_dispositions_off.png" alt="about"/></a></li>
						      <li class="last"><a href="submit.aspx"><img class="navi" src="images/subnavi_submit_off.png" alt="about"/></a></li>
						    </ul>
						    --></li>
			<li class="main_nav"><a href="contact.aspx">
			<img alt="contact" class="navi" src="images/navi_contact_off.jpg" /></a>
			</li>
		</ul>
		<!--menu-->
				</td>
				</tr>
			</table>
		</div>
		<div id="body">
			<div class="body_content">
				
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td valign="top" width="584">
						
						    <div class="cookie_crumbs"><a href="Default.aspx">Home</a> | <a href="company.aspx">Company</a> | <a href="Employment.aspx">Employment</a> | Employment Application</div>
						    
						    <div class="general_title"><img src="images/application_form_title.jpg" alt="About Us" /></div>
						
						    <div class="general_content">
                            Thanks for your interest in becoming a part of our team.
                            <br /><br />
                            <asp:literal ID="labelError" runat="server"></asp:literal>

						    <table cellpadding="0" cellspacing="0" class="application_form">
						        <tr>
						            <td valign="top" width="200"><big><b>Position</b></big></td>
						            <td valign="top"></td>
						        </tr>
						        <tr>
						            <td valign="top" width="200">Department</td>
						            <td valign="top"><asp:DropDownList ID="formDepartment" runat="server">
                                    <asp:ListItem Value="Not selected">Select a Department</asp:ListItem>  
						            <asp:ListItem Value="Accounting">Accounting</asp:ListItem>  
                                    <asp:ListItem Value="Acquisitions">Acquisitions</asp:ListItem> 
                                    <asp:ListItem Value="Administrative services">Administrative services</asp:ListItem>  
                                    <asp:ListItem Value="Construction">Construction</asp:ListItem>  
                                    <asp:ListItem Value="Finance">Finance</asp:ListItem>  
                                    <asp:ListItem Value="Human Resources">Human Resources</asp:ListItem>  
                                    <asp:ListItem Value="Information Technology">Information Technology</asp:ListItem>  
                                    <asp:ListItem Value="Lease Administration">Lease Administration</asp:ListItem>  
                                    <asp:ListItem Value="Leasing">Leasing</asp:ListItem>  
                                    <asp:ListItem Value="Legal">Legal</asp:ListItem>  
                                    <asp:ListItem Value="Management">Management</asp:ListItem>  
                                   <asp:ListItem Value="Marketing">Marketing</asp:ListItem>  
                                   <asp:ListItem Value="Property Management">Property Management</asp:ListItem>  
                                   <asp:ListItem Value="Other department or not sure">Other department or not sure</asp:ListItem> 
                                    <asp:ListItem Value="Internship">Internship</asp:ListItem> 
						            </asp:DropDownList></td>
						        </tr>
						          <tr>
						            <td valign="top" width="200"><big><b>Contact Info</b></big></td>
						            <td valign="top"></td>
						        </tr>
						        <tr>
						            <td valign="top" width="200">First Name*</td>
						            <td valign="top"><asp:TextBox ID="formFirstName" CssClass="required" runat="server"></asp:TextBox></td>
						        </tr>
						         <tr>
						            <td valign="top" width="200">Last Name*</td>
						            <td valign="top"><asp:TextBox ID="formLastName" CssClass="required" runat="server"></asp:TextBox></td>
						        </tr>
						          <tr>
						            <td valign="top" width="200">Address*</td>
						            <td valign="top"><asp:TextBox ID="formAddress" CssClass="required" runat="server"></asp:TextBox></td>
						        </tr>
						          <tr>
						            <td valign="top" width="200">City*</td>
						            <td valign="top"><asp:TextBox ID="formCity" CssClass="required" runat="server"></asp:TextBox></td>
						        </tr>
						          <tr>
						            <td valign="top" width="200">State*</td>
						            <td valign="top"><asp:DropDownList CssClass="required" ID="formState" runat="server">
                                    <asp:ListItem Value="AK">Alaska</asp:ListItem>
                                    <asp:ListItem Value="AL">Alabama</asp:ListItem>
                                    <asp:ListItem Value="AR">Arkansas</asp:ListItem>
                                    <asp:ListItem Value="AZ">Arizona</asp:ListItem>
                                    <asp:ListItem Value="CA">California</asp:ListItem>
                                    <asp:ListItem Value="CO">Colorado</asp:ListItem>
                                    <asp:ListItem Value="CT">Connecticut</asp:ListItem>
                                    <asp:ListItem Value="DC">District of Columbia</asp:ListItem>
                                    <asp:ListItem Value="DE">Delaware</asp:ListItem>
                                    <asp:ListItem Value="FL">Florida</asp:ListItem>
                                    <asp:ListItem Value="GA">Georgia</asp:ListItem>
                                    <asp:ListItem Value="HI">Hawaii</asp:ListItem>
                                    <asp:ListItem Value="IA">Iowa</asp:ListItem>
                                    <asp:ListItem Value="ID">Idaho</asp:ListItem>
                                    <asp:ListItem Value="IL">Illinois</asp:ListItem>
                                    <asp:ListItem Value="IN">Indiana</asp:ListItem>
                                    <asp:ListItem Value="KS">Kansas</asp:ListItem>
                                    <asp:ListItem Value="KY">Kentucky</asp:ListItem>
                                    <asp:ListItem Value="LA">Louisiana</asp:ListItem>
                                    <asp:ListItem Value="MA">Massachusetts</asp:ListItem>
                                    <asp:ListItem Value="MD">Maryland</asp:ListItem>
                                    <asp:ListItem Value="ME">Maine</asp:ListItem>
                                    <asp:ListItem Value="MI">Michigan</asp:ListItem>
                                    <asp:ListItem Value="MN">Minnesota</asp:ListItem>
                                    <asp:ListItem Value="MO">Missouri</asp:ListItem>
                                    <asp:ListItem Value="MS">Mississippi</asp:ListItem>
                                    <asp:ListItem Value="MT">Montana</asp:ListItem>
                                    <asp:ListItem Value="NC">North Carolina</asp:ListItem>
                                    <asp:ListItem Value="ND">North Dakota</asp:ListItem>
                                    <asp:ListItem Value="NE">Nebraska</asp:ListItem>
                                    <asp:ListItem Value="NV">Nevada</asp:ListItem>
                                    <asp:ListItem Value="NH">New Hampshire</asp:ListItem>
                                    <asp:ListItem Value="NJ">New Jersey</asp:ListItem>
                                    <asp:ListItem Value="NM">New Mexico</asp:ListItem>
                                    <asp:ListItem Value="NY">New York</asp:ListItem>
                                    <asp:ListItem Value="OH">Ohio</asp:ListItem>
                                    <asp:ListItem Value="OK">Oklahoma</asp:ListItem>
                                    <asp:ListItem Value="OR">Oregon</asp:ListItem>
                                    <asp:ListItem Value="PA">Pennsylvania</asp:ListItem>
                                    <asp:ListItem Value="RI">Rhode Island</asp:ListItem>
                                    <asp:ListItem Value="SC">South Carolina</asp:ListItem>
                                    <asp:ListItem Value="SD">South Dakota</asp:ListItem>
                                    <asp:ListItem Value="TN">Tennessee</asp:ListItem>
                                    <asp:ListItem Value="TX">Texas</asp:ListItem>
                                    <asp:ListItem Value="UT">Utah</asp:ListItem>
                                    <asp:ListItem Value="VA">Virginia</asp:ListItem>
                                    <asp:ListItem Value="VT">Vermont</asp:ListItem>
                                    <asp:ListItem Value="WA">Washington</asp:ListItem>
                                    <asp:ListItem Value="WI">Wisconsin</asp:ListItem>
                                    <asp:ListItem Value="WV">West Virginia</asp:ListItem>
                                    <asp:ListItem Value="WY">Wyoming</asp:ListItem>
                                </asp:DropDownList></td>
						        </tr>
						          <tr>
						            <td valign="top" width="200">Zip Code*</td>
						            <td valign="top"><asp:TextBox ID="formZip" CssClass="required" runat="server"></asp:TextBox></td>
						        </tr>
						          <tr>
						            <td valign="top" width="200">Daytime Phone*</td>
						            <td valign="top"><asp:TextBox ID="formPhone" CssClass="required" runat="server"></asp:TextBox></td>
						        </tr>
						          <tr>
						            <td valign="top" width="200">Email Address*</td>
						            <td valign="top"><asp:TextBox ID="formEmail" CssClass="required email" runat="server"></asp:TextBox></td>
						        </tr>
						          <tr>
						            <td valign="top" width="200">Best way to contact you*</td>
						            <td valign="top"><asp:DropDownList CssClass="required" ID="formBestWay" runat="server">
						            <asp:ListItem Value="Phone">Phone</asp:ListItem>
						            <asp:ListItem Value="Email">Email</asp:ListItem>
						            <asp:ListItem Value="No Preference">No Preference</asp:ListItem>
						            </asp:DropDownList></td>
						        </tr>
						        <tr>
						            <td valign="top" width="200"><big><b>Experience*</b></big></td>
						            <td valign="top"></td>
						        </tr>
						          <tr>
						            <td valign="top" width="200"># of Years Full Time Experience*</td>
						            <td valign="top"><asp:TextBox CssClass="required" ID="formExperience" runat="server"></asp:TextBox></td>
						        </tr>
						          <tr>
						            <td valign="top" width="200">Current Title*</td>
						            <td valign="top"><asp:TextBox CssClass="required" ID="formCurrentTitle" runat="server"></asp:TextBox></td>
						        </tr>
						          <tr>
						            <td valign="top" width="200">Education Level*</td>
						            <td valign="top"><asp:DropDownList CssClass="required" ID="formEducation" runat="server">
						                <asp:ListItem Value="High School Diploma">High School Diploma</asp:ListItem>
						                <asp:ListItem Value="Some College">Some College</asp:ListItem>
						                <asp:ListItem Value="Undergraduate Degree">Undergraduate Degree</asp:ListItem>
						                <asp:ListItem Value="Graduate Degree">Graduate Degree</asp:ListItem>
						            </asp:DropDownList></td>
						        </tr>
						          <tr>
						            <td valign="top" width="200">Are you eligible to work in the United States?*</td>
						            <td valign="top"><asp:DropDownList CssClass="required" ID="formEligible" runat="server">
						            <asp:ListItem Value="Yes">Yes</asp:ListItem>
						            <asp:ListItem Value="No">No</asp:ListItem>
						            </asp:DropDownList></td>
						        </tr>
						        <tr>
						            <td valign="top" width="200"><big><b>Attachments</b></big></td>
						            <td valign="top"></td>
						        </tr>
						        <tr>
						            <td valign="top" width="200">Attach Resume*</td>
						            <td valign="top"><asp:FileUpload ID="formResume" CssClass="required" runat="server" /></td>
						        </tr>
						        <tr>
						            <td valign="top" width="200">Attach Cover Letter</td>
						            <td valign="top"><asp:FileUpload ID="formCoverLetter" runat="server" /></td>
						        </tr>
						         
						         <tr>
						            <td valign="top" width="200">Type the two words*</td>
						            <td valign="top"><recaptcha:RecaptchaControl  ID="recaptcha"  runat="server"  PublicKey="6LcZf7sSAAAAACI3kWDqQ6n8D0aXX7sU1GrAWU1W" PrivateKey="6LcZf7sSAAAAAHWrxO1osl8YHHxye9Bj5cmq2BPd" /></td>
						        </tr>
						        <tr>
						            <td valign="top" width="200"><b>* Denotes mandatory fields</b></td>
						            <td valign="top"><asp:ImageButton runat="server" OnClientClick="validateForm();" ID="submitApp" ImageUrl="images/button_submit_application.jpg" CssClass="submitFormButton" /></td>
						        </tr>
						    </table>
						    </div>
						</td>
						<td valign="top" class="background_image"><img class="general_image" src="images/employment_image.jpg" alt="employment" /></td>
					</tr>
				
				</table>
			</div>
		</div>
	</div>
	<div id="footer_container">
		<div id="footer">
			<table cellpadding="0" cellspacing="0">
				<tr>
				<td valign="top" width="520">
				<div style="width:400px;">
				<big><b>Acadia Realty Trust</b></big><br />
				Acadia Realty Trust
				Acadia Realty Trust (NYSE:AKR) is a fully integrated, self-managed and self-administered equity REIT focused primarily on the ownership, acquisition, redevelopment and management of retail properties, including neighborhood / community shopping centers and mixed-use properties with retail components. 
				</div>
				</td>
				<td valign="top" width="120">
				<big><b>Menu</b></big><br />
				<a href="company.aspx">Company</a><br />
				<a href="properties.aspx">Properties</a><br />
				<a href="Investor_Relations.aspx">Investor Relations</a><br />
				<a href="acquisitions.aspx">Acquisitions</a><br />
				<a href="contact.aspx">Contact Us</a><br />
				</td>
				<td valign="top" width="120">
				<big><b>Follow Us</b></big><br />
				<!--<a href="#">RSS Feed</a><br />-->
				<!--<a href="#">Twitter</a><br />-->
				<a href="#">RSS Feed</a><br />
				<a href="http://www.linkedin.com/companies/acadia-realty-trust" target="_blank">LinkedIn</a>
				</td>
				<td valign="top">
				<big><b>Corporate Headquarters</b></big><br />
				1311 Mamaroneck Avenue<br />
				Suite 260<br />
				White Plains, NY 10605<br />
				<a href="mailto:info@acadiarealty.com">info@acadiarealty.com</a><br />
				<a target="_blank" href="http://maps.google.com/maps?f=d&daddr=1311 Mamaroneck Avenue, White Plains, NY 10605">Google map directions</a><br />
				</td>
				</tr>
				<tr>
					<td colspan="4">
					<br /><br />
					© Copyright 2010 Acadia Realty Trust&nbsp;&nbsp; 
					|&nbsp;&nbsp; <a href="terms.html" target="_blank">Terms and Conditions </a>&nbsp;&nbsp; |&nbsp;&nbsp; 
					<a href="privacy.html" target="_blank">Privacy Statement </a>					</td>
				
				</tr>
			</table>
			</div>
	</div>	
	

	<script type="text/javascript">
    
	var timeout    = 500;
	var closetimer = 0;
	var ddmenuitem = 0;
	
	function jsddm_open()
	{  jsddm_canceltimer();
	   jsddm_close();
	   ddmenuitem = $(this).find('ul').css('visibility', 'visible');}
	
	function jsddm_close()
	{  if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}
	
	function jsddm_timer()
	{  closetimer = window.setTimeout(jsddm_close, timeout);}
	
	function jsddm_canceltimer()
	{  if(closetimer)
	   {  window.clearTimeout(closetimer);
	      closetimer = null;}}
	
	$(document).ready(function()
	{  $('#jsddm > li').bind('mouseover', jsddm_open)
	   $('#jsddm > li').bind('mouseout',  jsddm_timer)});
	
	document.onclick = jsddm_close;
</script>
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17447452-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script></form>

</body>
</html>
