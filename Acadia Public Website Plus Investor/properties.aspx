<%@ Page Language="VB" AutoEventWireup="false" CodeFile="properties.aspx.vb" Inherits="properties" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 <style type="text/css">
 	@import url("css/style.css");
</style>
<script src="http://cdn.jquerytools.org/1.2.2/full/jquery.tools.min.js" type="text/javascript"></script>
<script src="js/jquery.perciformes.js" type="text/javascript"></script>
<script src="js/navi.js" type="text/javascript"></script>
<script src="http://use.typekit.com/spa4odv.js" type="text/javascript"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>


<style>
#property_search {
	position: absolute;
	top:98px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
</style>

<!--[if lte IE 7]>
<style type="text/css">
html, body {
	overflow-x: hidden;
}
</style>
<![endif]-->
<!--[if IE 7]>
<style>
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 20px;
	left:1px;
}

#property_search {
	position: absolute;
	top:122px;
	left: 50%;
	margin-left: 277px;
	z-index: 1;	
	background: #f9a131;
}


</style>
<![endif]-->
<style type="text/css">
@-moz-document url-prefix() {
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1007;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 25px;
	left:1px;
}

#property_search {
	position: absolute;
	top:102px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
#header {
	position:relative;
	margin-top:19px;
	margin-bottom: 14px;	
}
}
.auto-style1 {
	text-align: right;
}
</style>


<script type="text/javascript">
    $(document).ready(function() {

        $(".tenantTooltip[title]").tooltip();
    
    }
);

    function switchPage(i) {

        searchCallback.callback('paging',i);
    };
    function sortSearch(column) {

        searchCallback.callback('sort', column);
    };
    function filterSearch(filter) {

        searchCallback.callback('filter', filter);
    };

</script>
<title>Acadia Realty Trust - Search Result</title>
</head>
<body>
    <form id="form1" runat="server">
    
				
    <div id="container">
    <!--property search-->
		<div id="property_search">
		<a href="javascript:toggleSearch();"><img src="images/property_search.jpg" alt="property search" /></a>
		<div id="property_search_form">
	        <asp:PlaceHolder ID="placeholderSearchForm" runat="server"></asp:PlaceHolder>
	        </div>
		</div>
		<!-- end of property search-->
		<div id="header">
			<table width="100%" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				<td width="424" valign="top"><a href="default.aspx"><img class="navi" src="images/logo.jpg" alt="" /></a></td>
				<td valign="bottom" align="right">
				<!--menu-->
		<ul id="jsddm">
			<li class="main_nav"><a href="company.aspx">
			<img class="navi" src="images/navi_company_off.jpg" /></a>
			<ul>
				<!--<li><a href="about.aspx"><img class="navi" src="images/subnavi_about_off.png" alt="about"/></a></li>-->
				<li><a href="employment.aspx">
				<img alt="employment" class="navi" src="images/subnavi_employment_off.png" /></a></li>
				<!--<li class="last"><a href="benefits.aspx"><img class="navi" src="images/subnavi_benefits_off.png" alt="benefit"/></a></li>-->
			</ul>
			</li>
			<li class="main_nav"><a href="properties.aspx">
			<img src="images/navi_properties_on.jpg" /></a> </li>
			<li class="main_nav"><a href="investor_relations.aspx">
			<img class="navi" src="images/navi_ir_off.jpg" /></a>
			<ul>
				<li><a href="news_and_events.aspx">
				<img alt="news and events" class="navi" src="images/subnavi_news_off.png" /></a></li>
				<li><a href="reporting.aspx">
				<img alt="reporting" class="navi" src="images/subnavi_reporting_off.png" /></a></li>
				<li><a href="quarterly_reports.aspx">
				<img alt="quarterly reports" class="navi" src="images/subnavi_quarterly_off.png" /></a></li>
				<li><a href="annual_reports.aspx">
				<img alt="annual reports" class="navi" src="images/subnavi_annual_off.png" /></a></li>
				<li><a href="presentations.aspx">
				<img alt="presentations" class="navi" src="images/subnavi_presentations_off.png" /></a></li>
				<li><a href="analyst_coverage.aspx">
				<img alt="analyst" class="navi" src="images/subnavi_analyst_off.png" /></a></li>
				<li><a href="sec_filings.aspx">
				<img alt="filings" class="navi" src="images/subnavi_filings_off.png" /></a></li>
				<li><a href="dividends.aspx">
				<img alt="dividends" class="navi" src="images/subnavi_dividends_off.png" /></a></li>
				<li><a href="shareholders_information.aspx">
				<img alt="shareholders" class="navi" src="images/subnavi_shareholders_off.png" /></a></li>
				<li><a href="information_request.aspx">
				<img alt="information request" class="navi" src="images/subnavi_information_off.png" /></a></li>
				<li><a href="corporate_governance.aspx">
				<img alt="corporate" class="navi" src="images/subnavi_corporate_off.png" /></a></li>
				<li><a href="board_of_directors.aspx">
				<img alt="board of directors" class="navi" src="images/subnavi_bod_off.png" /></a></li>
				<li><a href="management.aspx">
				<img alt="management" class="navi" src="images/subnavi_management_off.png" /></a></li>
				<li><a href="committee_composition.aspx">
				<img alt="committee compositions" class="navi" src="images/subnavi_committee_off.png" /></a></li>
				<li><a href="faq.aspx">
				<img alt="faq" class="navi" src="images/subnavi_faq_off.png" /></a></li>
			</ul>
			</li>
			<li class="main_nav"><a href="acquisitions.aspx">
			<img alt="acquisitions" class="navi" src="images/navi_acquisitions_off.jpg" /></a>
			<!--
					 	<ul>
						      <li><a href="dispositions.aspx"><img class="navi" src="images/subnavi_dispositions_off.png" alt="about"/></a></li>
						      <li class="last"><a href="submit.aspx"><img class="navi" src="images/subnavi_submit_off.png" alt="about"/></a></li>
						    </ul>
						    --></li>
			<li class="main_nav"><a href="contact.aspx">
			<img alt="contact" class="navi" src="images/navi_contact_off.jpg" /></a>
			</li>
		</ul>
		<!--menu-->
				</td>
				</tr>
			</table>
		</div>
		<div id="body">
			                <div class="body_content" style="border:1px solid white;">

						
						    <div class="cookie_crumbs"><a href="Default.aspx">Home</a> | Properties</div>
						    
						    <div class="general_title"><img src="images/search_results_title.jpg" alt="Search Results" /></div>
						<div style="position: absolute; width: 258px; height: 2px; z-index: 1; left: 671px; top: 50px;" id="layer1" class="auto-style1"><a href="<asp:Literal ID=ltrdownload runat=server ></asp:Literal>" target="_blank">Download Listing<img src="images/property_list.jpg" alt="Property Listings" /></a></div>
						    <div class="general_content_search">
						    
						    <big><b>You searched for: <span class="search_terms"><asp:Literal ID="literalSearchTerms" runat="server"></asp:Literal></span></b></big><br />
                                Sort by selecting a category
						        <br /><br />
			                    Search Terms: <asp:TextBox ID="textboxFilter" runat="server"></asp:TextBox>       
			                    <br /><br />
                             <ComponentArt:CallBack runat="server" ID="searchCallback">
			                    <Content>
			                    <asp:PlaceHolder ID="slideshowPlaceholder" runat="server">
			                  
			                    
			                    <asp:Literal ID="literalPaging" runat="server"></asp:Literal>
			                    
			                    <asp:Repeater ID="RepeaterSearchResults" runat="server" OnItemDataBound="RepeaterSearchResultsItemDataBound">
						                <HeaderTemplate>
						                    <table cellpadding="0" cellspacing="0" width="100%" id="table_search">
						                        <thead>
						                                <tr>
						                                <th valign="top" width="111"></th>
						                                <th valign="top" width="197"><a href="javascript:sortSearch('name')">Property Name</a></th>
						                                <th valign="top" width="127"><a href="javascript:sortSearch('location')">Location</a></th>
						                                <th valign="top" width="95"><a href="javascript:sortSearch('gla')">GLA</a></th>
						                                <th valign="top" width="127"><a href="javascript:sortSearch('availability')">Availability</a></th>
						                                <th valign="top">Major Tenant</th>
						                                </tr>
						                        </thead>
						                        <tbody>
						                </HeaderTemplate>
						          <ItemTemplate>
						                <tr class="odd">
						                               <td valign="top" align="center" width="111"><a href="property_detail.aspx?id=<%#Container.DataItem("projectid")%>"><img src="<%=Session("WSRetreiveAsset") & "qfactor=25&width=111&height=75&crop=1&size=1&type=project&id=" %><%#Container.DataItem("projectid")%>" alt="<%#Container.DataItem("name").tostring.trim%>" /></a></td>
						                                <td valign="middle" width="197"><a href="property_detail.aspx?id=<%#Container.DataItem("projectid")%>"><%#Container.DataItem("name").trim%></a><asp:Literal ID="literlmanaged2" runat="server" ></asp:Literal></td>
						                                <td valign="middle" width="127"><%#Container.DataItem("city").ToString.trim%>, <%#Container.DataItem("state_id").ToString.trim%></td>
						                                <td valign="middle" width="95"><%#FormatNumber(Container.DataItem("GLA"), 0).ToString.Trim%> SF</td>
						                                <td valign="middle" width="127"><asp:Literal ID="literalAvailability" runat="server"></asp:Literal></td>
						                                <td valign="middle"><div style="width:130px;"><asp:Literal ID="literalTenants" runat="server"></asp:Literal></div></td>
						                </tr>
						          </ItemTemplate>
						          <AlternatingItemTemplate>
						                <tr class="even">
						                            <td valign="top" align="center" width="111"><a href="property_detail.aspx?id=<%#Container.DataItem("projectid")%>"><img src="<%=Session("WSRetreiveAsset") & "qfactor=25&width=111&height=75&crop=1&size=1&type=project&id=" %><%#Container.DataItem("projectid")%>" alt="<%#Container.DataItem("name").tostring.trim%>" /></a></td>
						                                <td valign="middle" width="197"><a href="property_detail.aspx?id=<%#Container.DataItem("projectid")%>"><%#Container.DataItem("name").trim%></a><asp:Literal ID="literlmanaged2"  runat="server" ></asp:Literal></td>
						                                <td valign="middle" width="127"><%#Container.DataItem("city").ToString.trim%>, <%#Container.DataItem("state_id").ToString.trim%></td>
						                                <td valign="middle" width="95"><%#FormatNumber(Container.DataItem("GLA"), 0).ToString.Trim%> SF</td>
						                                <td valign="middle" width="127"><asp:Literal ID="literalAvailability" runat="server"></asp:Literal></td>
						                                <td valign="middle"><div style="width:130px;"><asp:Literal ID="literalTenants" runat="server"></asp:Literal></div></td>
						                </tr>
						          </AlternatingItemTemplate>
						          
						          <FooterTemplate>
						          </tbody>
						          </table>
						          </FooterTemplate>
						          </asp:Repeater>
			                    
			                    <asp:Literal ID="literalPagingBottom" runat="server"></asp:Literal>
			                    </asp:PlaceHolder>				
				                </Content>
				                <LoadingPanelClientTemplate>
                                                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                        <tr>
                                                                            <td align="center">
                                                                                <table cellspacing="0" cellpadding="0" border="0">
                                                                                    <tr>
                                                                                        <td style="font-size: 10px">
                                                                                            Loading...
                                                                                        </td>
                                                                                        <td>
                                                                                            <img height="16" src="images/spinner.gif" width="16" border="0">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </LoadingPanelClientTemplate>
				               
                                                                				                </ComponentArt:CallBack>
                           
						  
						          






						    </div>
						
			</div>
		</div>
	</div>
	<div id="footer_container">
		<div id="footer">
			<table cellpadding="0" cellspacing="0">
				<tr>
				<td valign="top" width="520">
				<div style="width:400px;">
				<big><b>Acadia Realty Trust</b></big><br />
				Acadia Realty Trust (NYSE:AKR) is a fully integrated, self-managed and self-administered equity REIT focused primarily on the ownership, acquisition, redevelopment and management of retail properties, including neighborhood / community shopping centers and mixed-use properties with retail components. 
				</div>
				</td>
				<td valign="top" width="120">
				<big><b>Menu</b></big><br />
				<a href="company.aspx">Company</a><br />
				<a href="properties.aspx">Properties</a><br />
				<a href="Investor_Relations.aspx">Investor Relations</a><br />
				<a href="acquisitions.aspx">Acquisitions</a><br />
				<a href="contact.aspx">Contact Us</a><br />
				</td>
				<td valign="top" width="120">
				<big><b>Follow Us</b></big><br />
				<!--<a href="#">RSS Feed</a><br />-->
				<!--<a href="#">Twitter</a><br />-->
				<a href="#">RSS Feed</a><br />
				<a href="http://www.linkedin.com/companies/acadia-realty-trust" target="_blank">LinkedIn</a>
				</td>
				<td valign="top">
				<big><b>Corporate Headquarters</b></big><br />
				1311 Mamaroneck Avenue<br />
				Suite 260<br />
				White Plains, NY 10605<br />
				<a href="mailto:info@acadiarealty.com">info@acadiarealty.com</a><br />
				<a target="_blank" href="http://maps.google.com/maps?f=d&daddr=1311 Mamaroneck Avenue, White Plains, NY 10605">Google map directions</a><br />
				</td>
				</tr>
				<tr>
					<td colspan="4">
					<br /><br />
					&copy; Copyright 2010 Acadia Realty Trust&nbsp;&nbsp; 
					|&nbsp;&nbsp; <a href="terms.html" target="_blank">Terms and Conditions </a>&nbsp;&nbsp; |&nbsp;&nbsp; 
					<a href="privacy.html" target="_blank">Privacy Statement </a> </td>
					</td>
				
				</tr>
			</table>
			</div>
	</div>	
	

	<script type="text/javascript">
    
	var timeout    = 500;
	var closetimer = 0;
	var ddmenuitem = 0;
	
	function jsddm_open()
	{  jsddm_canceltimer();
	   jsddm_close();
	   ddmenuitem = $(this).find('ul').css('visibility', 'visible');}
	
	function jsddm_close()
	{  if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}
	
	function jsddm_timer()
	{  closetimer = window.setTimeout(jsddm_close, timeout);}
	
	function jsddm_canceltimer()
	{  if(closetimer)
	   {  window.clearTimeout(closetimer);
	      closetimer = null;}}
	
	$(document).ready(function()
	{  $('#jsddm > li').bind('mouseover', jsddm_open)
	   $('#jsddm > li').bind('mouseout',  jsddm_timer)});
	
	document.onclick = jsddm_close;
</script>
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17447452-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script></form>

</body>
</html>
