﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.Xml.XPath
Partial Class news_release
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)


        Dim ws = New acadiaws.Service1

        Dim ds As DataSet = ws.GetT1Feed("http://xml.corporate-ir.net/irxmlclient.asp?compid=" & ConfigurationSettings.AppSettings("compID") & "&reqtype=releasetxt&reqid=" & Request.QueryString("id"))

        Dim releaseXML As XmlDocument = New XmlDocument
        releaseXML.LoadXml(ds.GetXml)

        Dim nav As XPathNavigator = releaseXML.CreateNavigator
        Dim itTitle As XPathNodeIterator = nav.Select("IRXML/NewsReleaseText/Title")
        Dim itReleaseText As XPathNodeIterator = nav.Select("IRXML/NewsReleaseText/ReleaseText")


        itReleaseText.MoveNext()

        literalReleaseText.Text = itReleaseText.Current.Value






    End Sub
End Class
