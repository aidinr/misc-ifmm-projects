﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data

Partial Class property_leasing_plan

    Inherits System.Web.UI.Page
   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)

        Dim ws = New acadiaws.Service1
        Dim ws2 = New acadiaws.Service1

        Dim theProjectID As String = Request.QueryString("id")
        Dim theFloor As String = Request.QueryString("floor")

        If (theFloor = "") Then
            theFloor = "1"
        End If

        Dim dt As DataTable = ws.ListProjects("", "", "", "", "", "", theProjectID, "")

        If (dt.Rows.Count > 0) Then
            literalProjectCrumbs.Text = dt.Rows(0)("name").ToString.Trim

            Page.Header.Title = "Acadia Realty Trust - Leasing Plan - " & dt.Rows(0)("name").ToString.Trim & " - " & dt.Rows(0)("city").ToString.Trim & ", " & dt.Rows(0)("state_id").ToString.Trim

            literalProjectName.Text = dt.Rows(0)("name").ToString.Trim
            literalProjectLocation.Text = dt.Rows(0)("city").ToString.Trim & ", " & dt.Rows(0)("state_id").ToString.Trim & " " & dt.Rows(0)("zip").ToString.Trim
        End If

        Dim dtSpaces As DataTable = ws2.GetSpaces(theProjectID)

        Dim dtCurrentTenants As DataTable = New DataTable
        dtCurrentTenants.Columns.Add("shortname")
        dtCurrentTenants.Columns.Add("SF")
        dtCurrentTenants.Columns.Add("suite")
        dtCurrentTenants.Columns.Add("space_id")
        dtCurrentTenants.Columns.Add("floor")
        'dtCurrentTenants.Columns("suite").DataType = GetType(Integer)



        Dim dtAvailableSpaces As DataTable = New DataTable
        dtAvailableSpaces.Columns.Add("SF")
        dtAvailableSpaces.Columns.Add("suite")
        dtAvailableSpaces.Columns.Add("space_id")
        dtAvailableSpaces.Columns.Add("floor")
        'dtCurrentTenants.Columns("suite").DataType = GetType(Integer)


        Dim dtFloors As DataTable = New DataTable()
        Dim dtFloors2 As DataTable = New DataTable()

        dtFloors = dtSpaces.Copy

        Dim dv As DataView = New DataView(dtFloors)

        Dim dtuniquefloors As New DataTable

        'dtuniquefloors.Columns.Add("floor", System.Type.GetType("System.Int32"))
        dtuniquefloors = dv.ToTable("floors", True, "floor")


        'dtuniquefloors.Columns(0).DataType = 
        dtuniquefloors.DefaultView.Sort = "floor asc"
        repeaterFloorSelectors.DataSource = dtuniquefloors.DefaultView
        repeaterFloorSelectors.DataBind()


        For Each y As DataRow In dtSpaces.Rows
            Dim dtTenant As DataTable = ws2.GetSpaceTenant(y("space_id"))

            If (dtTenant.Rows.Count > 0) Then

                Dim dtTenantInfo As DataTable = ws.GetTenantDetail(dtTenant.Rows(0)("tenant_id"))

                Dim r As DataRow = dtCurrentTenants.NewRow
                'r("suite") = CInt(y("suite"))
                r("suite") = y("suite")
                r("SF") = y("SF")
                r("space_id") = y("space_id")
                r("shortname") = dtTenantInfo.Rows(0)("shortname")
                r("floor") = y("floor")
                dtCurrentTenants.Rows.Add(r)

            Else
                Dim r As DataRow = dtAvailableSpaces.NewRow
                r("suite") = y("suite")
                r("SF") = y("SF")
                r("space_id") = y("space_id")
                r("floor") = y("floor")
                dtAvailableSpaces.Rows.Add(r)
            End If

        Next

        Dim dvAvailableSpace As DataView = New DataView(dtAvailableSpaces)
        Dim dvCurrentTenants As DataView = New DataView(dtCurrentTenants)

        dvAvailableSpace.Sort = "suite asc"
        dvCurrentTenants.Sort = "suite asc"

        dvAvailableSpace.RowFilter = "floor = " & theFloor.ToString.Trim
        dvCurrentTenants.RowFilter = "floor = " & theFloor.ToString.Trim

        repeaterAvailableSpaces.DataSource = dvAvailableSpace
        repeaterCurrentTenants.DataSource = dvCurrentTenants

        If (dvAvailableSpace.Count > 0) Then
            repeaterAvailableSpaces.DataBind()
        Else
            repeaterAvailableSpaces.Visible = False

        End If

        If (dvCurrentTenants.Count > 0) Then
            repeaterCurrentTenants.DataBind()
        Else
            repeaterCurrentTenants.Visible = False
        End If



       
        Dim dtAssets = ws.GetProjectAssets(theProjectID)
        dtAssets.DefaultView.Sort = "name desc"

        Dim dtDownloadAssets As DataTable = New DataTable
        dtDownloadAssets.Columns.Add("name")
        dtDownloadAssets.Columns.Add("asset_id")
		dim xFloor as string = "asdfasdfasfd"
		if request.querystring("floor") <> "" then
			xFloor = request.querystring("floor")
		end if
        For Each x As DataRow In dtAssets.DefaultView.ToTable.Rows
            If (x("catname") = "Leasing Plan") Then


                If (InStr(x("name"), xFloor.Trim) > 0) Then
                    imageLeasingPlan.ImageUrl = Session("WSRetreiveAsset") & "qfactor=25&width=900&height=450&size=1&type=asset&id=" & x("asset_id")
                    printlink.NavigateUrl = "PrintImage.aspx?id=" & x("asset_id")
                    printlink.Target = "_Blank"
                    imageLeasingPlan.AlternateText = x("name")
                    imageLeasingPlan.ImageAlign = ImageAlign.Middle
                    hyperlinkImage1.NavigateUrl = (Session("WSRetreiveAsset") & "qfactor=25&width=1200&height=1000&size=1&type=asset&id=" & x("asset_id")).ToString.Replace("(", "%28").Replace(")", "%29")
                    hyperlinkImage1.Attributes.Add("rel", "lightbox")
                    hyperlinkImage1.Attributes.Add("title", dt.Rows(0)("name").ToString.Trim)
                    Exit For
                Else
                    imageLeasingPlan.ImageUrl = Session("WSRetreiveAsset") & "qfactor=25&width=900&height=450&size=1&type=asset&id=" & x("asset_id")
                    printlink.NavigateUrl = "PrintImage.aspx?id=" & x("asset_id")
                    printlink.Target = "_Blank"
                    imageLeasingPlan.AlternateText = x("name")
                    imageLeasingPlan.ImageAlign = ImageAlign.Middle
                    hyperlinkImage1.NavigateUrl = (Session("WSRetreiveAsset") & "qfactor=25&width=1200&height=1000&size=1&type=asset&id=" & x("asset_id")).ToString.Replace("(", "%28").Replace(")", "%29")
                    hyperlinkImage1.Attributes.Add("rel", "lightbox")
                    hyperlinkImage1.Attributes.Add("title", dt.Rows(0)("name").ToString.Trim)

                End If
            ElseIf (x("catname") = "Downloads") Then
                Dim r As DataRow = dtDownloadAssets.NewRow
                r("name") = x("name")
                r("asset_id") = x("asset_id")
                dtDownloadAssets.Rows.Add(r)
            End If

        Next




        RepeaterAssetDownloads.DataSource = dtDownloadAssets
        RepeaterAssetDownloads.DataBind()


        'competition map check

        Dim dtCompAssets = ws.GetProjectAssets(theProjectID)
        Dim compFlag As Integer = 0

        For Each x As DataRow In dtCompAssets.Rows
            If (x("catname") = "Competition Map") Then
                compFlag = 1
            End If
        Next


        If (compFlag = 0) Then
            placeholderCompetitionMap.Visible = False
        End If



    End Sub

    Public Sub repeaterCurrentTenantsItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        Dim ws2 = New acadiaws.Service1
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim literalSpace As Literal = e.Item.FindControl("literalSpace")
            Dim dtAsset As DataTable = ws2.GetSpaceAssets(e.Item.DataItem("space_id"))

            If (dtAsset.Rows.Count > 0) Then
                literalSpace.Text = "<b><a target=""_blank"" href=""" & Session("WSDownloadAsset") & "size=0&assetid=" & dtAsset.Rows(0)("asset_id") & """>" & e.Item.DataItem("suite") & ".</a></b>"
            Else
                literalSpace.Text = e.Item.DataItem("suite") & "."
            End If

        End If
    End Sub


    Public Sub repeaterAvailableSpacesItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        Dim ws2 = New acadiaws.Service1
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim literalSpace As Literal = e.Item.FindControl("literalSpace")
            Dim dtAsset As DataTable = ws2.GetSpaceAssets(e.Item.DataItem("space_id"))

            If (dtAsset.Rows.Count > 0) Then
                literalSpace.Text = "<b><a target=""_blank"" href=""" & Session("WSDownloadAsset") & "size=0&assetid=" & dtAsset.Rows(0)("asset_id") & """>" & "Space " & e.Item.DataItem("suite") & "</a></b>"
            Else
                literalSpace.Text = "Space " & e.Item.DataItem("suite")
            End If
        End If

    End Sub

    Public Sub repeaterFloorSelectorsOnItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)

        Dim theFloor As String = Request.QueryString("floor")

        Dim literalButtonSelector As Literal = e.Item.FindControl("literalButtonSelector")



        If (theFloor = "") Then
            theFloor = "1"
        End If

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then

            If (CInt(theFloor - 1) = e.Item.ItemIndex) Then
                literalButtonSelector.Text = "<div class=""leasing_plan_selector_active"">"
            Else
                literalButtonSelector.Text = "<div class=""leasing_plan_selector"">"
            End If

        End If

    End Sub

End Class

