<%@ Page Language="VB" AutoEventWireup="false" CodeFile="information_request.aspx.vb" Inherits="information_request" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

<style type="text/css">
 	@import url("css/style.css");
</style>
<script src="http://cdn.jquerytools.org/1.2.2/full/jquery.tools.min.js" type="text/javascript"></script>
<script src="js/jquery.perciformes.js" type="text/javascript"></script>
<script src="js/navi.js" type="text/javascript"></script>
<script src="http://use.typekit.com/spa4odv.js" type="text/javascript"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>


<style>
#property_search {
	position: absolute;
	top:98px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
</style>

<!--[if lte IE 7]>
<style type="text/css">
html, body {
	overflow-x: hidden;
}
</style>
<![endif]-->
<!--[if IE 7]>
<style>
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 20px;
	left:1px;
}

#property_search {
	position: absolute;
	top:122px;
	left: 50%;
	margin-left: 277px;
	z-index: 1;	
	background: #f9a131;
}


</style>
<![endif]-->
<style type="text/css">
@-moz-document url-prefix() {
#jsddm {
	margin: 0;
	padding: 0;
	top: 0px;
	left: 0px;
	position: relative;
	float: left;
	z-index: 1007;
}

.body_content {
	background: #ffffff;
	color: #000000;
	font-size: 12px;
	line-height: 20px;
	position:relative;
	top: 25px;
	left:1px;
}

#property_search {
	position: absolute;
	top:102px;
	left: 50%;
	margin-left: 277px;
	z-index: 1006;	
	background: #f9a131;
}
#header {
	position:relative;
	margin-top:19px;
	margin-bottom: 14px;	
}
}
</style>



<script type="text/javascript" src="js/jquery.validate.min.js"></script>

<title>Acadia Realty Trust - Investor Relations - Information Request</title>
<script type="text/javascript">
    $(document).ready(function() {

    $('.imageButtonSubmit').click(function(event) {

            $('form').valid();

        });

    });
</script>
</head>
<body>
    <form id="form1" runat="server">

				
   <div id="container">
   <!--property search-->
		<div id="property_search">
		<a href="javascript:toggleSearch();"><img src="images/property_search.jpg" alt="property search" /></a>
		<div id="property_search_form">
	        <asp:PlaceHolder ID="placeholderSearchForm" runat="server"></asp:PlaceHolder>
	        </div>
		</div>
		<!-- end of property search-->
		<div id="header">
			<table width="100%" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				<td width="424" valign="top"><a href="default.aspx"><img class="navi" src="images/logo.jpg" /></a></td>
				<td valign="bottom" align="right">
				<!--menu-->
		<ul id="jsddm">
			<li class="main_nav"><a href="company.aspx">
			<img class="navi" src="images/navi_company_off.jpg" /></a>
			<ul>
				<!--<li><a href="about.aspx"><img class="navi" src="images/subnavi_about_off.png" alt="about"/></a></li>-->
				<li><a href="employment.aspx">
				<img alt="employment" class="navi" src="images/subnavi_employment_off.png" /></a></li>
				<!--<li class="last"><a href="benefits.aspx"><img class="navi" src="images/subnavi_benefits_off.png" alt="benefit"/></a></li>-->
			</ul>
			</li>
			<li class="main_nav"><a href="properties.aspx">
			<img class="navi" src="images/navi_properties_off.jpg" /></a> </li>
			<li class="main_nav"><a href="investor_relations.aspx">
			<img src="images/navi_ir_on.jpg" /></a>
			<ul>
				<li><a href="news_and_events.aspx">
				<img alt="news and events" class="navi" src="images/subnavi_news_off.png" /></a></li>
				<li><a href="reporting.aspx">
				<img alt="reporting" class="navi" src="images/subnavi_reporting_off.png" /></a></li>
				<li><a href="quarterly_reports.aspx">
				<img alt="quarterly reports" class="navi" src="images/subnavi_quarterly_off.png" /></a></li>
				<li><a href="annual_reports.aspx">
				<img alt="annual reports" class="navi" src="images/subnavi_annual_off.png" /></a></li>
				<li><a href="presentations.aspx">
				<img alt="presentations" class="navi" src="images/subnavi_presentations_off.png" /></a></li>
				<li><a href="analyst_coverage.aspx">
				<img alt="analyst" class="navi" src="images/subnavi_analyst_off.png" /></a></li>
				<li><a href="sec_filings.aspx">
				<img alt="filings" class="navi" src="images/subnavi_filings_off.png" /></a></li>
				<li><a href="dividends.aspx">
				<img alt="dividends" class="navi" src="images/subnavi_dividends_off.png" /></a></li>
				<li><a href="shareholders_information.aspx">
				<img alt="shareholders" class="navi" src="images/subnavi_shareholders_off.png" /></a></li>
				<li><a href="information_request.aspx">
				<img alt="information request" class="navi" src="images/subnavi_information_on.png" /></a></li>
				<li><a href="corporate_governance.aspx">
				<img alt="corporate" class="navi" src="images/subnavi_corporate_off.png" /></a></li>
				<li><a href="board_of_directors.aspx">
				<img alt="board of directors" class="navi" src="images/subnavi_bod_off.png" /></a></li>
				<li><a href="management.aspx">
				<img alt="management" class="navi" src="images/subnavi_management_off.png" /></a></li>
				<li><a href="committee_composition.aspx">
				<img alt="committee compositions" class="navi" src="images/subnavi_committee_off.png" /></a></li>
				<li><a href="faq.aspx">
				<img alt="faq" class="navi" src="images/subnavi_faq_off.png" /></a></li>
			</ul>
			</li>
			<li class="main_nav"><a href="acquisitions.aspx">
			<img alt="acquisitions" class="navi" src="images/navi_acquisitions_off.jpg" /></a>
			<!--
					 	<ul>
						      <li><a href="dispositions.aspx"><img class="navi" src="images/subnavi_dispositions_off.png" alt="about"/></a></li>
						      <li class="last"><a href="submit.aspx"><img class="navi" src="images/subnavi_submit_off.png" alt="about"/></a></li>
						    </ul>
						    --></li>
			<li class="main_nav"><a href="contact.aspx">
			<img alt="contact" class="navi" src="images/navi_contact_off.jpg" /></a>
			</li>
		</ul>
		<!--menu-->
				</td>
				</tr>
			</table>
		</div>
		<div id="body">
			<div class="body_content">
				
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td valign="top" width="584">
						
						    <div class="cookie_crumbs"><a href="Default.aspx">Home</a> | <a href="investor_relations.aspx">Investor Relations</a> | Information Request</div>
						    
						    <div class="general_title"><img src="images/title_information.jpg" alt="Company" /></div>
						
						    <div class="general_content">
						    <asp:Label ID="labelSuccess" runat="server" ForeColor="Red" Font-Bold="true" ></asp:Label>
						    
						    <b>To request information via mail, please fill out and submit the form below:</b>
						    <br /><br />
						    <b>Information Requested:</b><br />
						    <asp:CheckBox runat="server" ID="checkBoxReport"   /> &nbsp; Annual Report<br />
						    <asp:CheckBox runat="server" ID="checkBoxQuestion" /> &nbsp; Question / Comments
						    
						    <br /><br />
						    
						    <table cellpadding="0" cellspacing="0" width="100%" class="form">
						        <tr>
						            <td width="200"><b>Personal Information</b></td>
						            <td></td>
						        </tr>
						        <tr>
						            <td width="200">First Name*</td>
						            <td><asp:TextBox runat="server" Width="160" ID="textboxFirstName" CssClass="required"></asp:TextBox></td>
						        </tr>
						        <tr>
						            <td width="200">Last Name*</td>
						            <td><asp:TextBox runat="server" Width="160" ID="textboxLastName" CssClass="required"></asp:TextBox></td>
						        </tr>
						        <tr>
						            <td width="200">Title</td>
						            <td><asp:TextBox runat="server" Width="160" ID="textboxTitle"></asp:TextBox></td>
						        </tr>
						        <tr>
						            <td width="200">Organization</td>
						            <td><asp:TextBox runat="server" Width="160" ID="textboxOrganization"></asp:TextBox></td>
						        </tr>
						         <tr>
						            <td width="200">Investor Type</td>
						            <td><asp:DropDownList runat="server" ID="formInvestorType">
						            <asp:ListItem value="1">Individual Investor</asp:ListItem>
				
				                    <asp:ListItem value="2">Buy-Side Analyst</asp:ListItem>
                    				
				                    <asp:ListItem value="3">Sell-Side Analyst</asp:ListItem>
                    				
				                    <asp:ListItem value="4">Portfolio Manager</asp:ListItem>
                    				
				                    <asp:ListItem value="5">Stock Broker</asp:ListItem>

                    				
				                    <asp:ListItem value="6">Employee</asp:ListItem>
                    				
				                    <asp:ListItem value="7">News Media</asp:ListItem>
                    				
				                    <asp:ListItem value="9">Library</asp:ListItem>
                    				
				                    <asp:ListItem value="10">Other</asp:ListItem>

						            </asp:DropDownList></td>
						        </tr>
						        <tr>
						            <td width="200">Address 1</td>
						            <td><asp:TextBox runat="server" Width="160" ID="textboxAddress1"></asp:TextBox></td>
						        </tr>
						          <tr>
						            <td width="200">Address 2</td>
						            <td><asp:TextBox runat="server" Width="160" ID="textboxAddress2"></asp:TextBox></td>
						        </tr>
						        <tr>
						            <td width="200">City</td>
						            <td><asp:TextBox runat="server" Width="160" ID="textboxCity"></asp:TextBox></td>
						        </tr>
						        <tr>
						            <td width="200">State</td>
						            <td>
						                <asp:DropDownList ID="formState" runat="server">
    <asp:ListItem Value="">Select a State or Province</asp:ListItem>
    <asp:ListItem Value="AK">AK</asp:ListItem>
    <asp:ListItem Value="AL">AL</asp:ListItem>
    <asp:ListItem Value="AR">AR</asp:ListItem>
    <asp:ListItem Value="AZ">AZ</asp:ListItem>
    <asp:ListItem Value="CA">CA</asp:ListItem>
    <asp:ListItem Value="CO">CO</asp:ListItem>
    <asp:ListItem Value="CT">CT</asp:ListItem>
    <asp:ListItem Value="DC">DC</asp:ListItem>
    <asp:ListItem Value="DE">DE</asp:ListItem>
    <asp:ListItem Value="FL">FL</asp:ListItem>
    <asp:ListItem Value="GA">GA</asp:ListItem>
    <asp:ListItem Value="HI">HI</asp:ListItem>
    <asp:ListItem Value="IA">IA</asp:ListItem>
    <asp:ListItem Value="ID">ID</asp:ListItem>
    <asp:ListItem Value="IL">IL</asp:ListItem>
    <asp:ListItem Value="IN">IN</asp:ListItem>
    <asp:ListItem Value="KS">KS</asp:ListItem>
    <asp:ListItem Value="KY">KY</asp:ListItem>
    <asp:ListItem Value="LA">LA</asp:ListItem>
    <asp:ListItem Value="MA">MA</asp:ListItem>
    <asp:ListItem Value="MD">MD</asp:ListItem>
    <asp:ListItem Value="ME">ME</asp:ListItem>
    <asp:ListItem Value="MI">MI</asp:ListItem>
    <asp:ListItem Value="MN">MN</asp:ListItem>
    <asp:ListItem Value="MO">MO</asp:ListItem>
    <asp:ListItem Value="MS">MS</asp:ListItem>
    <asp:ListItem Value="MT">MT</asp:ListItem>
    <asp:ListItem Value="NC">NC</asp:ListItem>
    <asp:ListItem Value="ND">ND</asp:ListItem>
    <asp:ListItem Value="NE">NE</asp:ListItem>
    <asp:ListItem Value="NV">NV</asp:ListItem>
    <asp:ListItem Value="NH">NH</asp:ListItem>
    <asp:ListItem Value="NJ">NJ</asp:ListItem>
    <asp:ListItem Value="NM">NM</asp:ListItem>
    <asp:ListItem Value="NY">NY</asp:ListItem>
    <asp:ListItem Value="OH">OH</asp:ListItem>
    <asp:ListItem Value="OK">OK</asp:ListItem>
    <asp:ListItem Value="OR">OR</asp:ListItem>
    <asp:ListItem Value="PA">PA</asp:ListItem>
    <asp:ListItem Value="RI">RI</asp:ListItem>
    <asp:ListItem Value="SC">SC</asp:ListItem>
    <asp:ListItem Value="SD">SD</asp:ListItem>
    <asp:ListItem Value="TN">TN</asp:ListItem>
    <asp:ListItem Value="TX">TX</asp:ListItem>
    <asp:ListItem Value="UT">UT</asp:ListItem>
    <asp:ListItem Value="VA">VA</asp:ListItem>
    <asp:ListItem Value="VT">VT</asp:ListItem>
    <asp:ListItem Value="WA">WA</asp:ListItem>
    <asp:ListItem Value="WI">WI</asp:ListItem>
    <asp:ListItem Value="WV">WV</asp:ListItem>
    <asp:ListItem Value="WY">WY</asp:ListItem>
    <asp:ListItem value="AB">AB</asp:ListItem>
    <asp:ListItem value="BC">BC</asp:ListItem>
    <asp:ListItem value="MB">MB</asp:ListItem>
    <asp:ListItem value="NB">NB</asp:ListItem>
    <asp:ListItem value="NF">NF</asp:ListItem>
    <asp:ListItem value="NT">NT</asp:ListItem>
    <asp:ListItem value="NS">NS</asp:ListItem>
    <asp:ListItem value="NU">NU</asp:ListItem>
    <asp:ListItem value="ON">ON</asp:ListItem>
    <asp:ListItem value="PE">PE</asp:ListItem>
    <asp:ListItem value="QC">QC</asp:ListItem>
    <asp:ListItem value="SK">SK</asp:ListItem>
    <asp:ListItem value="YT">YT</asp:ListItem>
    <asp:ListItem value="Other">Other</asp:ListItem>
</asp:DropDownList>
						            </td>
						        </tr>
						        <tr>
						            <td width="200">Zip</td>
						            <td><asp:TextBox runat="server" Width="160" ID="textboxZip"></asp:TextBox></td>
						        </tr>
						        <tr>
						            <td width="200">Country</td>
						            <td>
						            
						            
						             <asp:DropDownList ID="formCountry" runat="server">
                               	<asp:ListItem value="US">USA</asp:ListItem>
				
				<asp:ListItem value="AF">AFGHANISTAN</asp:ListItem>
				
				<asp:ListItem value="AL">ALBANIA</asp:ListItem>

				
				<asp:ListItem value="DZ">ALGERIA</asp:ListItem>
				
				<asp:ListItem value="AS">AMERICAN SAMOA</asp:ListItem>
				
				<asp:ListItem value="AD">ANDORRA</asp:ListItem>
				
				<asp:ListItem value="AI">ANGUILLA</asp:ListItem>
				
				<asp:ListItem value="AG">ANTIGUA</asp:ListItem>
				
				<asp:ListItem value="AN1">ANTILLES, NETHERLAND</asp:ListItem>

				
				<asp:ListItem value="AR">ARGENTINA</asp:ListItem>
				
				<asp:ListItem value="AM">ARMENIA</asp:ListItem>
				
				<asp:ListItem value="AW">ARUBA</asp:ListItem>
				
				<asp:ListItem value="SH1">ASCENSION</asp:ListItem>
				
				<asp:ListItem value="AU">AUSTRALIA</asp:ListItem>
				
				<asp:ListItem value="AT">AUSTRIA</asp:ListItem>

				
				<asp:ListItem value="AZ">AZERBAIJAN</asp:ListItem>
				
				<asp:ListItem value="PT1">AZORES </asp:ListItem>
				
				<asp:ListItem value="BS">BAHAMAS</asp:ListItem>
				
				<asp:ListItem value="BH">BAHRAIN</asp:ListItem>
				
				<asp:ListItem value="BD">BANGLADESH</asp:ListItem>
				
				<asp:ListItem value="BB">BARBADOS</asp:ListItem>

				
				<asp:ListItem value="AG1">BARBUDA</asp:ListItem>
				
				<asp:ListItem value="BY">BELARUS</asp:ListItem>
				
				<asp:ListItem value="BE">BELGIUM</asp:ListItem>
				
				<asp:ListItem value="BZ">BELIZE</asp:ListItem>
				
				<asp:ListItem value="BJ">BENIN</asp:ListItem>
				
				<asp:ListItem value="BM">BERMUDA</asp:ListItem>

				
				<asp:ListItem value="BT">BHUTAN</asp:ListItem>
				
				<asp:ListItem value="BO">BOLIVIA</asp:ListItem>
				
				<asp:ListItem value="XB">BONAIRE</asp:ListItem>
				
				<asp:ListItem value="BA">BOSNIA-HERZEGOVINA</asp:ListItem>
				
				<asp:ListItem value="BW">BOTSWANA</asp:ListItem>
				
				<asp:ListItem value="BR">BRAZIL</asp:ListItem>

				
				<asp:ListItem value="VG">BRITISH VIRGIN ISLANDS</asp:ListItem>
				
				<asp:ListItem value="BN">BRUNEI</asp:ListItem>
				
				<asp:ListItem value="BG">BULGARIA</asp:ListItem>
				
				<asp:ListItem value="BF">BURKINA FASO</asp:ListItem>
				
				<asp:ListItem value="BI">BURUNDI</asp:ListItem>
				
				<asp:ListItem value="KH">CAMBODIA (KAMPUCHEA)</asp:ListItem>

				
				<asp:ListItem value="CM">CAMEROON</asp:ListItem>
				
				<asp:ListItem value="CA">CANADA</asp:ListItem>
				
				<asp:ListItem value="IC">CANARY ISLANDS</asp:ListItem>
				
				<asp:ListItem value="CV">CAPE VERDE</asp:ListItem>
				
				<asp:ListItem value="KY">CAYMAN ISLANDS</asp:ListItem>
				
				<asp:ListItem value="CF">CENTRAL AFRICAN REPUBLIC</asp:ListItem>

				
				<asp:ListItem value="TD">CHAD</asp:ListItem>
				
				<asp:ListItem value="GB3">CHANNEL ISLANDS</asp:ListItem>
				
				<asp:ListItem value="CL">CHILE</asp:ListItem>
				
				<asp:ListItem value="CN">CHINA</asp:ListItem>
				
				<asp:ListItem value="CO">COLOMBIA</asp:ListItem>
				
				<asp:ListItem value="KM">COMOROS</asp:ListItem>

				
				<asp:ListItem value="CG">CONGO</asp:ListItem>
				
				<asp:ListItem value="CK">COOK ISLANDS</asp:ListItem>
				
				<asp:ListItem value="FR1">CORSICA</asp:ListItem>
				
				<asp:ListItem value="CR">COSTA RICA</asp:ListItem>
				
				<asp:ListItem value="HR">CROATIA</asp:ListItem>
				
				<asp:ListItem value="XC">CURACAO</asp:ListItem>

				
				<asp:ListItem value="CY">CYPRUS </asp:ListItem>
				
				<asp:ListItem value="CZ">CZECH REPUBLIC</asp:ListItem>
				
				<asp:ListItem value="DK">DENMARK</asp:ListItem>
				
				<asp:ListItem value="DJ">DJIBOUTI</asp:ListItem>
				
				<asp:ListItem value="DM">DOMINICA</asp:ListItem>
				
				<asp:ListItem value="DO">DOMINICAN REPUBLIC</asp:ListItem>

				
				<asp:ListItem value="TP">EAST TIMOR</asp:ListItem>
				
				<asp:ListItem value="EC">ECUADOR</asp:ListItem>
				
				<asp:ListItem value="EG">EGYPT</asp:ListItem>
				
				<asp:ListItem value="SV">EL SALVADOR</asp:ListItem>
				
				<asp:ListItem value="GB">ENGLAND</asp:ListItem>
				
				<asp:ListItem value="GQ">EQUATORIAL GUINEA</asp:ListItem>

				
				<asp:ListItem value="ER">ERITREA</asp:ListItem>
				
				<asp:ListItem value="EE">ESTONIA</asp:ListItem>
				
				<asp:ListItem value="ET">ETHIOPIA</asp:ListItem>
				
				<asp:ListItem value="FK">FALKLAND ISLANDS</asp:ListItem>
				
				<asp:ListItem value="FO">FAROE ISLANDS</asp:ListItem>
				
				<asp:ListItem value="FJ">FIJI</asp:ListItem>

				
				<asp:ListItem value="FI">FINLAND</asp:ListItem>
				
				<asp:ListItem value="FR">FRANCE</asp:ListItem>
				
				<asp:ListItem value="GF">FRENCH GUIANA</asp:ListItem>
				
				<asp:ListItem value="PF">FRENCH POLYNESIA</asp:ListItem>
				
				<asp:ListItem value="GA">GABON</asp:ListItem>
				
				<asp:ListItem value="GM">GAMBIA </asp:ListItem>

				
				<asp:ListItem value="GE">GEORGIA, REPUBLIC OF</asp:ListItem>
				
				<asp:ListItem value="DE">GERMANY</asp:ListItem>
				
				<asp:ListItem value="GH">GHANA</asp:ListItem>
				
				<asp:ListItem value="GI">GIBRALTAR</asp:ListItem>
				
				<asp:ListItem value="GB4">GREAT BRITAIN</asp:ListItem>
				
				<asp:ListItem value="GR">GREECE</asp:ListItem>

				
				<asp:ListItem value="GL">GREENLAND</asp:ListItem>
				
				<asp:ListItem value="GD">GRENADA</asp:ListItem>
				
				<asp:ListItem value="GP">GUADELOUPE</asp:ListItem>
				
				<asp:ListItem value="GU">GUAM</asp:ListItem>
				
				<asp:ListItem value="GT">GUATEMALA</asp:ListItem>
				
				<asp:ListItem value="GG">GUERNSEY</asp:ListItem>

				
				<asp:ListItem value="GN">GUINEA</asp:ListItem>
				
				<asp:ListItem value="GW">GUINEA-BISSAU</asp:ListItem>
				
				<asp:ListItem value="GY">GUYANA, BRITISH</asp:ListItem>
				
				<asp:ListItem value="HT">HAITI</asp:ListItem>
				
				<asp:ListItem value="NL1">HOLLAND</asp:ListItem>
				
				<asp:ListItem value="HN">HONDURAS</asp:ListItem>

				
				<asp:ListItem value="HK">HONG KONG</asp:ListItem>
				
				<asp:ListItem value="HU">HUNGARY</asp:ListItem>
				
				<asp:ListItem value="IN">INDIA</asp:ListItem>
				
				<asp:ListItem value="ID">INDONESIA</asp:ListItem>
				
				<asp:ListItem value="IE">IRELAND</asp:ListItem>
				
				<asp:ListItem value="IL">ISRAEL</asp:ListItem>

				
				<asp:ListItem value="IT">ITALY</asp:ListItem>
				
				<asp:ListItem value="CI">IVORY COAST</asp:ListItem>
				
				<asp:ListItem value="JM">JAMAICA</asp:ListItem>
				
				<asp:ListItem value="JP">JAPAN</asp:ListItem>
				
				<asp:ListItem value="JE">JERSEY</asp:ListItem>
				
				<asp:ListItem value="JO">JORDAN</asp:ListItem>

				
				<asp:ListItem value="KH1">KAMPUCHEA</asp:ListItem>
				
				<asp:ListItem value="KZ">KAZAKHSTAN</asp:ListItem>
				
				<asp:ListItem value="KE">KENYA</asp:ListItem>
				
				<asp:ListItem value="KI">KIRIBATI</asp:ListItem>
				
				<asp:ListItem value="KR">SOUTH KOREA</asp:ListItem>
				
				<asp:ListItem value="GU2">KOSRAE</asp:ListItem>

				
				<asp:ListItem value="KW">KUWAIT</asp:ListItem>
				
				<asp:ListItem value="KG">KYRGYZSTAN</asp:ListItem>
				
				<asp:ListItem value="LA">LAOS</asp:ListItem>
				
				<asp:ListItem value="LV">LATVIA</asp:ListItem>
				
				<asp:ListItem value="LB">LEBANON</asp:ListItem>
				
				<asp:ListItem value="LS">LESOTHO</asp:ListItem>

				
				<asp:ListItem value="LR">LIBERIA</asp:ListItem>
				
				<asp:ListItem value="LI">LIECHTENSTEIN</asp:ListItem>
				
				<asp:ListItem value="LT">LITHUANIA</asp:ListItem>
				
				<asp:ListItem value="LU">LUXEMBOURG</asp:ListItem>
				
				<asp:ListItem value="MO">MACAU</asp:ListItem>
				
				<asp:ListItem value="MK">MACEDONIA,  REPUBLIC OF</asp:ListItem>

				
				<asp:ListItem value="MG">MADAGASCAR</asp:ListItem>
				
				<asp:ListItem value="PT2">MADEIRA ISLANDS</asp:ListItem>
				
				<asp:ListItem value="MW">MALAWI</asp:ListItem>
				
				<asp:ListItem value="MY">MALAYSIA</asp:ListItem>
				
				<asp:ListItem value="MV">MALDIVES</asp:ListItem>
				
				<asp:ListItem value="ML">MALI</asp:ListItem>

				
				<asp:ListItem value="MT">MALTA</asp:ListItem>
				
				<asp:ListItem value="MH">MARSHALL ISLANDS</asp:ListItem>
				
				<asp:ListItem value="MQ">MARTINIQUE</asp:ListItem>
				
				<asp:ListItem value="MR">MAURITANIA</asp:ListItem>
				
				<asp:ListItem value="MU">MAURITIUS</asp:ListItem>
				
				<asp:ListItem value="MX">MEXICO</asp:ListItem>

				
				<asp:ListItem value="GU3">MICRONESIA</asp:ListItem>
				
				<asp:ListItem value="MD">MOLDOVA</asp:ListItem>
				
				<asp:ListItem value="MC">MONACO</asp:ListItem>
				
				<asp:ListItem value="MN">MONGOLIA</asp:ListItem>
				
				<asp:ListItem value="YU1">MONTENEGRO</asp:ListItem>
				
				<asp:ListItem value="MS">MONTSERRAT</asp:ListItem>

				
				<asp:ListItem value="MA">MOROCCO</asp:ListItem>
				
				<asp:ListItem value="MZ">MOZAMBIQUE</asp:ListItem>
				
				<asp:ListItem value="MM1">MYANMAR</asp:ListItem>
				
				<asp:ListItem value="NA">NAMIBIA</asp:ListItem>
				
				<asp:ListItem value="NR">NAURU</asp:ListItem>
				
				<asp:ListItem value="NP">NEPAL</asp:ListItem>

				
				<asp:ListItem value="AN">NETHERLAND ANTILLES</asp:ListItem>
				
				<asp:ListItem value="NL">NETHERLANDS</asp:ListItem>
				
				<asp:ListItem value="KN2">NEVIS</asp:ListItem>
				
				<asp:ListItem value="NC">NEW CALEDONIA</asp:ListItem>
				
				<asp:ListItem value="NZ">NEW ZEALAND</asp:ListItem>
				
				<asp:ListItem value="NI">NICARAGUA</asp:ListItem>

				
				<asp:ListItem value="NG">NIGERIA</asp:ListItem>
				
				<asp:ListItem value="NU">NIUE</asp:ListItem>
				
				<asp:ListItem value="AU1">NORFOLK ISLANDS</asp:ListItem>
				
				<asp:ListItem value="GB5">NORTHERN IRELAND</asp:ListItem>
				
				<asp:ListItem value="MP">NORTHERN MARIANA ISLANDS</asp:ListItem>
				
				<asp:ListItem value="NO">NORWAY</asp:ListItem>

				
				<asp:ListItem value="OM">OMAN</asp:ListItem>
				
				<asp:ListItem value="PK">PAKISTAN</asp:ListItem>
				
				<asp:ListItem value="GU4">PALAU</asp:ListItem>
				
				<asp:ListItem value="PA">PANAMA</asp:ListItem>
				
				<asp:ListItem value="PG">PAPUA NEW GUINEA</asp:ListItem>
				
				<asp:ListItem value="PY">PARAGUAY</asp:ListItem>

				
				<asp:ListItem value="PE">PERU</asp:ListItem>
				
				<asp:ListItem value="PH">PHILIPPINES</asp:ListItem>
				
				<asp:ListItem value="PN">PITCAIRN ISLANDS</asp:ListItem>
				
				<asp:ListItem value="GU5">POHNPEI</asp:ListItem>
				
				<asp:ListItem value="PL">POLAND</asp:ListItem>
				
				<asp:ListItem value="PT">PORTUGAL</asp:ListItem>

				
				<asp:ListItem value="PR">PUERTO RICO</asp:ListItem>
				
				<asp:ListItem value="QA">QATAR</asp:ListItem>
				
				<asp:ListItem value="RE">REUNION</asp:ListItem>
				
				<asp:ListItem value="RO">ROMANIA</asp:ListItem>
				
				<asp:ListItem value="GU6">ROTA</asp:ListItem>
				
				<asp:ListItem value="RU">RUSSIA</asp:ListItem>

				
				<asp:ListItem value="RW">RWANDA</asp:ListItem>
				
				<asp:ListItem value="XM1">SABA</asp:ListItem>
				
				<asp:ListItem value="MP1">SAIPAN</asp:ListItem>
				
				<asp:ListItem value="SM">SAN MARINO (ITALY)</asp:ListItem>
				
				<asp:ListItem value="ST">SAOTOME  PRINCIPE</asp:ListItem>
				
				<asp:ListItem value="SA">SAUDI ARABIA</asp:ListItem>

				
				<asp:ListItem value="GB8">SCOTLAND</asp:ListItem>
				
				<asp:ListItem value="SN">SENEGAL</asp:ListItem>
				
				<asp:ListItem value="SC">SEYCHELLES</asp:ListItem>
				
				<asp:ListItem value="SL">SIERRA LEONE</asp:ListItem>
				
				<asp:ListItem value="SG">SINGAPORE</asp:ListItem>
				
				<asp:ListItem value="SK">SLOVAKIA (SLOVAK REPUBLIC)</asp:ListItem>

				
				<asp:ListItem value="SI">SLOVENIA</asp:ListItem>
				
				<asp:ListItem value="SB">SOLOMON ISLANDS</asp:ListItem>
				
				<asp:ListItem value="SO">SOMALIA</asp:ListItem>
				
				<asp:ListItem value="ZA">SOUTH AFRICA</asp:ListItem>
				
				<asp:ListItem value="ES">SPAIN</asp:ListItem>
				
				<asp:ListItem value="LK">SRILANKA</asp:ListItem>

				
				<asp:ListItem value="XY">ST. BARTHELEMY</asp:ListItem>
				
				<asp:ListItem value="KN1">ST. CHRISTOPHER</asp:ListItem>
				
				<asp:ListItem value="VI1">ST. CROIX</asp:ListItem>
				
				<asp:ListItem value="XE">ST. EUSTATIUS</asp:ListItem>
				
				<asp:ListItem value="SH">ST. HELENA</asp:ListItem>
				
				<asp:ListItem value="VI5">ST. JOHN</asp:ListItem>

				
				<asp:ListItem value="KN">ST. KITTS</asp:ListItem>
				
				<asp:ListItem value="LC">ST. LUCIA</asp:ListItem>
				
				<asp:ListItem value="XM">ST. MAARTEN</asp:ListItem>
				
				<asp:ListItem value="VI4">ST. MARTIN</asp:ListItem>
				
				<asp:ListItem value="PM">ST. PIERRE / MIQUELON</asp:ListItem>
				
				<asp:ListItem value="VI2">ST. THOMAS</asp:ListItem>

				
				<asp:ListItem value="VC">ST. VINCENT</asp:ListItem>
				
				<asp:ListItem value="SD">SUDAN</asp:ListItem>
				
				<asp:ListItem value="SR">SURINAME</asp:ListItem>
				
				<asp:ListItem value="SZ">SWAZILAND</asp:ListItem>
				
				<asp:ListItem value="SE">SWEDEN</asp:ListItem>
				
				<asp:ListItem value="CH">SWITZERLAND</asp:ListItem>

				
				<asp:ListItem value="PF1">TAHITI</asp:ListItem>
				
				<asp:ListItem value="TW">TAIWAN</asp:ListItem>
				
				<asp:ListItem value="TJ">TAJIKISTAN</asp:ListItem>
				
				<asp:ListItem value="TZ">TANZANIA</asp:ListItem>
				
				<asp:ListItem value="TH">THAILAND</asp:ListItem>
				
				<asp:ListItem value="GU7">TINIAN</asp:ListItem>

				
				<asp:ListItem value="TG">TOGO</asp:ListItem>
				
				<asp:ListItem value="TO">TONGA</asp:ListItem>
				
				<asp:ListItem value="VG1">TORTOLA</asp:ListItem>
				
				<asp:ListItem value="TT">TRINIDAD AND TOBAGO</asp:ListItem>
				
				<asp:ListItem value="SH2">TRISTAN DA CUNHA</asp:ListItem>
				
				<asp:ListItem value="GU8">TRUK</asp:ListItem>

				
				<asp:ListItem value="TN">TUNISIA</asp:ListItem>
				
				<asp:ListItem value="TR">TURKEY</asp:ListItem>
				
				<asp:ListItem value="TM">TURKMENISTAN</asp:ListItem>
				
				<asp:ListItem value="TC">TURKS AND CAICOS ISLANDS</asp:ListItem>
				
				<asp:ListItem value="TV">TUVALU</asp:ListItem>
				
				<asp:ListItem value="US">UNITED STATES</asp:ListItem>

				
				<asp:ListItem value="VI">U.S. VIRGIN ISLANDS</asp:ListItem>
				
				<asp:ListItem value="UG">UGANDA</asp:ListItem>
				
				<asp:ListItem value="UA">UKRAINE</asp:ListItem>
				
				<asp:ListItem value="WS1">UNION ISLAND</asp:ListItem>
				
				<asp:ListItem value="AE">UNITED ARAB EMIRATES</asp:ListItem>
				
				<asp:ListItem value="GB1">UNITED KINGDOM</asp:ListItem>

				
				<asp:ListItem value="UY">URUGUAY</asp:ListItem>
				
				<asp:ListItem value="UZ">UZBEKISTAN</asp:ListItem>
				
				<asp:ListItem value="VU">VANUATU</asp:ListItem>
				
				<asp:ListItem value="VA">VATICAN CITY</asp:ListItem>
				
				<asp:ListItem value="VE">VENEZUELA</asp:ListItem>
				
				<asp:ListItem value="VN">VIETNAM</asp:ListItem>

				
				<asp:ListItem value="VG2">VIRGIN ISLANDS (BRITISH)</asp:ListItem>
				
				<asp:ListItem value="VI3">VIRGIN ISLANDS (U.S.)</asp:ListItem>
				
				<asp:ListItem value="WA">WAKE ISLAND</asp:ListItem>
				
				<asp:ListItem value="GBA">WALES</asp:ListItem>
				
				<asp:ListItem value="WF">WALLIS AND FUTINA ISLANDS</asp:ListItem>
				
				<asp:ListItem value="WS">WESTERN SAMOA</asp:ListItem>

				
				<asp:ListItem value="GU9">YAP</asp:ListItem>
				
				<asp:ListItem value="YE">YEMEN</asp:ListItem>
				
				<asp:ListItem value="YU">YUGOSLAVIA</asp:ListItem>
				
				<asp:ListItem value="ZR">CONGO, THE DEM. REP. OF</asp:ListItem>
				
				<asp:ListItem value="ZM">ZAMBIA</asp:ListItem>
				
				<asp:ListItem value="ZW">ZIMBABWE</asp:ListItem>
				</asp:DropDownList> 

                                </td>
						        </tr>
						        <tr>
						            <td width="200">Phone #</td>
						            <td><asp:TextBox runat="server" Width="160" ID="textboxPhone"></asp:TextBox></td>
						        </tr>
						        <tr>
						            <td width="200">Fax #</td>
						            <td><asp:TextBox runat="server" Width="160" ID="textboxFax"></asp:TextBox></td>
						        </tr>
						        <tr>
						            <td width="200">E-mail Address*</td>
						            <td><asp:TextBox runat="server" Width="160" ID="textboxEmail" CssClass="required email"></asp:TextBox></td>
						        </tr>
						        <tr>
						            <td width="200">Question/Comment Box</td>
						            <td><asp:TextBox runat="server" Width="220" ID="textboxComments"  TextMode="MultiLine" Rows="5" Height="75"></asp:TextBox></td>
						        </tr>
						        <tr>
						            <td width="200"><small>Required fields denoted<br /> by an asterisk (*)</small></td>
						            <td><asp:ImageButton ImageUrl="images/button_submit_request.jpg" ID="imageButtonSubmit" CssClass="imageButtonSubmit" runat="server" AlternateText="Submit Request" /></td>
						        </tr>
						    
						    
						    </table>
						    
						    </div>
						</td>
						<td valign="top" class="background_image">
						<div class="general_content_right">
						    <img src="images/title_contacts.jpg" alt="Contacts" /> 
						    <br /><br />
						    <b>Primary IR Contact</b><br />
						    Jon Grisham<br />
						    Phone: 914-288-8142<br />
						    E-mail: <a href="#">jgrisham@acadiarealty.com</a>
						    <br /><br />
						    <b>Transfer Agent</b><br />
						    American Stock Transfer & Trust Company<br />
                            40 Wall Street<br />
                            New York, NY 10005<br />
                            Phone: 212-936-5100<br />
                            E-mail: info@amstock.com<br />
                            Corporate Website: <a href="http://www.amstock.com">
							www.amstock.com</a><br />
                            <br />
                            <b>Accountants</b><br />
                            BDO Seidman<br />
                            330 Madison Avenue<br />
                            New York, NY 10017<br />    
                            <br />
                            <b>Legal</b><br />
                            Paul, Hastings, Janofsky & Walker LLP<br />
                            Park Avenue Tower<br />
                            75 East 55th Street<br />
                            New York, NY 10022<br />
						</div>
						</td>
					</tr>
				
				</table>
			</div>
		</div>
	</div>
	<div id="footer_container">
		<div id="footer">
			<table cellpadding="0" cellspacing="0">
				<tr>
				<td valign="top" width="520">
				<div style="width:400px;">
				<big><b>Acadia Realty Trust</b></big><br />
				Acadia Realty Trust (NYSE:AKR) is a fully integrated, self-managed and self-administered equity REIT focused primarily on the ownership, acquisition, redevelopment and management of retail properties, including neighborhood / community shopping centers and mixed-use properties with retail components. 
				</div>
				</td>
				<td valign="top" width="120">
				<big><b>Menu</b></big><br />
				<a href="company.aspx">Company</a><br />
				<a href="properties.aspx">Properties</a><br />
				<a href="Investor_Relations.aspx">Investor Relations</a><br />
				<a href="acquisitions.aspx">Acquisitions</a><br />
				<a href="contact.aspx">Contact Us</a><br />
				</td>
				<td valign="top" width="120">
				<big><b>Follow Us</b></big><br />
				<!--<a href="#">RSS Feed</a><br />-->
				<!--<a href="#">Twitter</a><br />-->
				<a href="#">RSS Feed</a><br />
				<a href="http://www.linkedin.com/companies/acadia-realty-trust" target="_blank">LinkedIn</a>
				</td>
				<td valign="top">
				<big><b>Corporate Headquarters</b></big><br />
				1311 Mamaroneck Avenue<br />
				Suite 260<br />
				White Plains, NY 10605<br />
				<a href="mailto:info@acadiarealty.com">info@acadiarealty.com</a><br />
				<a target="_blank" href="http://maps.google.com/maps?f=d&daddr=1311 Mamaroneck Avenue, White Plains, NY 10605">Google map directions</a><br />
				</td>
				</tr>
				<tr>
					<td colspan="4">
					<br /><br />
					&copy; Copyright 2010 Acadia Realty Trust&nbsp;&nbsp; 
					|&nbsp;&nbsp; <a href="terms.html" target="_blank">Terms and Conditions </a>&nbsp;&nbsp; |&nbsp;&nbsp; 
					<a href="privacy.html" target="_blank">Privacy Statement </a>					</td>
				
				</tr>
			</table>
			</div>
	</div>	
	
	
	<script type="text/javascript">
    
	var timeout    = 500;
	var closetimer = 0;
	var ddmenuitem = 0;
	
	function jsddm_open()
	{  jsddm_canceltimer();
	   jsddm_close();
	   ddmenuitem = $(this).find('ul').css('visibility', 'visible');}
	
	function jsddm_close()
	{  if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}
	
	function jsddm_timer()
	{  closetimer = window.setTimeout(jsddm_close, timeout);}
	
	function jsddm_canceltimer()
	{  if(closetimer)
	   {  window.clearTimeout(closetimer);
	      closetimer = null;}}
	
	$(document).ready(function()
	{  $('#jsddm > li').bind('mouseover', jsddm_open)
	   $('#jsddm > li').bind('mouseout',  jsddm_timer)});
	
	document.onclick = jsddm_close;
</script>
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17447452-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script></form>

</body>
</html>
