﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data

Partial Class EmploymentDetail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ws = New acadiaws.Service1

        Dim dt As DataTable = ws.ListJobItems

        Dim theID As String = Request.QueryString("id")

        Dim datarow As DataRow() = dt.Select("job_id = " & theID)

        repeaterCareers.DataSource = dt
        repeaterCareers.DataBind()


        Dim searchControl As Web.UI.Control

        searchControl = Me.LoadControl("search.ascx")

        placeholderSearchForm.Controls.Add(searchControl)



    End Sub
End Class
