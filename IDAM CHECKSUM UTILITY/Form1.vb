﻿Imports System.IO
Imports System.Security.Cryptography
Imports System
Imports System.Data.OleDb
Imports System.Configuration

Public Class Form1
    Public Shared ConnectionStringIDAM As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
    Public Shared ConnectionStringIDAMIDAMINSTANCE As String = ConfigurationSettings.AppSettings("IDAM.IDAMINSTANCE")
    Public Shared ConnectionStringIDAMENGINE As String = ConfigurationSettings.AppSettings("IDAM.IDAMENGINEID")
    Public Shared bState As Boolean = True
    Public Shared AppIDUnique As String = System.Guid.NewGuid.ToString
    Private Delegate Sub ExecuteChecksum()
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If CheckBox1.Checked Then
            Dim del As ExecuteChecksum
            del = New ExecuteChecksum(AddressOf ExecuteCS)
            del.Invoke()
        End If
    End Sub


    Private Sub ExecuteCS()
        'get all assets without a checksum
        'interate thru repo

        Dim sql As String = "select * from ipm_asset_field_desc where Item_Tag = 'CHECKSUM'"
        If GetDataTable(sql, ConnectionStringIDAM).Rows.Count <> 1 Then
            Exit Sub
        End If
        Dim checksumitemid As String = GetDataTable(sql, ConnectionStringIDAM).Rows(0)("item_id")
        'get all assets
        Dim allassets As DataTable = GetDataTable("select location_id,asset_id,extension from ipm_asset a, IPM_FILETYPE_LOOKUP b where a.Media_Type  = b.Media_Type and a.asset_id not in (select asset_id from ipm_asset_field_value where item_id = " & checksumitemid & ") and available = 'Y'", ConnectionStringIDAM)
        TextBox1.Text = "Total assets converted: 0 of " & allassets.Rows.Count.ToString
        ProgressBar1.Maximum = allassets.Rows.Count
        Dim sDestRoot As String = ConfigurationSettings.AppSettings("IDAM.IDAMREPOROOT")
        For Each row As DataRow In allassets.Rows
            'find original asset
            System.Threading.Thread.CurrentThread.Sleep(10)
            System.Windows.Forms.Application.DoEvents()
            Do While Not bState
                'paused
                System.Threading.Thread.CurrentThread.Sleep(1000)
                System.Windows.Forms.Application.DoEvents()
            Loop
            Dim sSourceFile, sSourceFileThumb As String
            sSourceFile = sDestRoot + row("location_id").ToString.Trim + "\resource\" & row("asset_id") & "." & row("extension")
            sSourceFileThumb = sDestRoot + row("location_id").ToString.Trim + "\resource\thumb_" & row("asset_id") & ".jpg"
            'get checksum
            Dim sSourceChecksumVal As String = GetFileChecksum(sSourceFile, row("asset_id"))
            'set checksum
            If Not sSourceChecksumVal Is Nothing Then
                addUDFtoasset(row("asset_id"), checksumitemid, sSourceChecksumVal)

                ProgressBar1.Refresh()
                TextBox1.Refresh()
                'add asset to the queue
                If CheckBox2.Checked Then
                    'check for preview
                    If Not File.Exists(sSourceFileThumb) Then
                        Me.ExecuteTransaction("insert into ipm_asset_queue(instance_id,asset_id,asset_handler,status,priority,engine_id)values('" & ConnectionStringIDAMIDAMINSTANCE & "'," & row("asset_id").ToString.Trim & ",2,0,1,'" & ConnectionStringIDAMENGINE & "')")
                        SendError("Forced Reconvert (Missing Preview Files)", "CheckSum Missing Original File", row("asset_id"))
                    End If

                End If
            End If
            ProgressBar1.Value += 1
            TextBox1.Text = "Total assets converted: " & ProgressBar1.Value & " of " & allassets.Rows.Count.ToString
        Next
        MsgBox("Completed")
    End Sub

    Public Shared Function GetFileChecksum(ByVal pFilePath As String, ByVal pAssetID As String) As String

        Try


            Dim fs As New FileStream(pFilePath, FileMode.Open, FileAccess.Read, FileShare.Read, 8192)

            Dim sha1 As SHA1CryptoServiceProvider = New SHA1CryptoServiceProvider
            Dim hash As Byte() = sha1.ComputeHash(fs)

            Dim buff As New Text.StringBuilder
            For Each hashbyte As Byte In hash
                buff.Append(String.Format("{0:X1}", hashbyte))
            Next
            Return buff.ToString
        Catch ex As Exception
            If InStr(ex.Message, "Could not find file") > 0 Then
                'log possible missing file
                SendError(ex.Message, "CheckSum Missing Original File", pAssetID)
            End If
        End Try

    End Function



    Public Shared Function SendError(ByVal sError, Optional ByVal object_type = "", Optional ByVal object_id = "")
        
        Dim sql As String
        sError = sError.ToString.Replace("'", "''")
        
        Try
            sql = "insert into ipm_error (date,error_level,machine_id,instance_id,app_id,output,object_type,object_id) values (getdate(),'High','Localhost','" & ConnectionStringIDAMIDAMINSTANCE & "','" & AppIDUnique & "','" & sError & "','" & object_type & "','" & object_id & "')"
            ExecuteTransaction(sql)

        Catch ex As Exception

        End Try



    End Function



    Public Shared Sub ExecuteTransaction(ByVal sqlstring As String, Optional ByVal connectionstring As String = "")
        If connectionstring = "" Then
            connectionstring = ConnectionStringIDAM
        End If

        Dim connection As New OleDbConnection(connectionstring)
        Dim command As New OleDbCommand
        Dim transaction As OleDbTransaction

        ' Set the Connection to the new OleDbConnection.
        command.Connection = connection

        ' Open the connection and execute the transaction.
        Try
            connection.Open()

            ' Start a local transaction with ReadCommitted isolation level.
            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            ' Assign transaction object for a pending local transaction.
            command.Connection = connection
            command.Transaction = transaction

            ' Execute the commands.
            command.CommandText = sqlstring
            command.ExecuteNonQuery()

            ' Commit the transaction.
            transaction.Commit()
            Console.WriteLine("Executed.")

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Try to rollback the transaction
            Try
                transaction.Rollback()

            Catch
                ' Do nothing here; transaction is not active.
            End Try
        End Try
        ' The connection is automatically closed when the
        ' code exits the Using block.

    End Sub

    Public Shared Function GetDataTable(ByVal query As String, ByVal ConnectionString As String) As DataTable
        'Dim connection1 As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & System.Web.HttpContext.Current.Server.MapPath(dbFile))
        Dim connection1 As OleDbConnection = New OleDbConnection(ConnectionString)
        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)
        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()

        End Try
        Return table1
    End Function
    Function isullcheck(ByVal v)
        If v Is System.DBNull.Value Then
            isullcheck = ""
        Else
            isullcheck = v
        End If
    End Function



    Private Function addUDFtoasset(ByVal asset_id As String, ByVal UDFid As String, ByVal UDFValue As String) As String
        'check to see if exists
        Try
            'add
            'assume we have a tagid now
            If GetDataTable("select * from ipm_asset_field_value where asset_id = " & asset_id & " and item_id = " & UDFid, ConnectionStringIDAM).Rows.Count = 0 Then
                ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & asset_id & "," & UDFid & ",'" & UDFValue.Replace("'", "''") & "')")
            End If

        Catch ex As Exception

        End Try
        Return UDFid
    End Function

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Button2.Text = "Pause" Then
            bState = False
            Button2.Text = "Resume"
        Else
            bState = True
            Button2.Text = "Pause"
        End If


    End Sub
End Class
