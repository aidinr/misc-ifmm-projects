﻿delete from ipm_asset_field_value where item_id = 21575366 
/*institutional photo 21575366 IDAM_PHOTO_TYPE*/
insert into ipm_asset_field_value 
select asset_id,21575366 item_id, 'Institutional' item_value from ipm_asset where substring(name,1,1) = 'N' and available = 'Y'
/*Historical photo IDAM_PHOTO_TYPE*/
insert into ipm_asset_field_value 
select asset_id,21575366 item_id, 'Historical' from ipm_asset where substring(name,1,1) <> 'N' and available = 'Y'
delete from ipm_asset_field_value where item_id = 21575367 
insert into ipm_asset_field_value 
select a.asset_id,21575367 item_id,1 item_value  from ipm_asset a, ipm_asset_tag b, ipm_asset_field_value c where a.asset_id = b.asset_id and b.tagid in (157682,157190)
and a.asset_id = c.asset_id and c.item_id = 21548341 and c.item_value = 1 group by a.asset_id, item_id, item_value
/*Institutional Artifact 21575367 IDAM_PHOTO_ARTIFACT*/