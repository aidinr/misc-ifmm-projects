﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="awards-recognition.aspx.vb" Inherits="awards_recognition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<div id="navigation-lower">
		<ul class="second-level">
				  <li class="navlink">      
				  <a href="investment-management">Investment Management </a></li>
				  <li class="navlink">      
				  <a href="development">Development</a></li>
				  <li class="navlink">      
				  <a href="asset-management">Asset Management</a></li>		  
				  <li class="navlink">      
				  <a href="organization">Organization</a></li>
				  <li class="navlink">      
				  <a href="affordable-housing">Affordable Housing</a></li>	  
				  <li class="navlink">      
				  <a href="awards-recognition">Awards</a></li>
				  <li class="navlink">      
				  <a href="public-art">Public Art</a></li>	  
			</ul>
   </div>
   <div id="content">
	<div class="inner-content">		
		<div class="container_12" id="main">
	
				<div class="jbg_ul_contain">
					<div class="grid_12 page-header sidebar">
						<h1>Awards &amp; Recognition: <%=SelectedYear %></h1>  
					</div>
					<div class="clear"></div>
					<asp:Literal ID="DistinctYears_Literal" runat="server"></asp:Literal>
					<hr/>
				</div>    


<div class="grid_4"></div>
<div class="grid_8 content">
<p class="cms cmsType_TextMulti cmsName_Awards_Intro">Intro</p>
</div>

<div class="grid_4"><h1><asp:Literal ID="SelectedYear_Literal" runat="server"></asp:Literal></h1></div>
<div class="grid_8 content">
<asp:Literal ID="Awards_Literal" runat="server"></asp:Literal>
</div>


		</div>
	</div>	
   </div>
</asp:Content>
