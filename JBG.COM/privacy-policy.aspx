﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="privacy-policy.aspx.vb" Inherits="privacy_policy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

   <div id="navigation-lower">
		<ul class="second-level">
		  <li><a href="privacy-policy">Privacy Policy</a></li>
		</ul>
   </div>

   <div id="content">
	 <div class="inner-content" >
		<div class="container_12" id="main">
			<div class="grid_12 page-header">
				<h1>Privacy Policy</h1>
			</div>
			<div class="clear"></div>
			<hr/>
			<div class="grid_4"></div>
			<div class="grid_8">
				<p class="cms cmsType_TextMulti cmsName_Privacy_Intro"></p>






<div class="cmsGroup cmsName_Privacy">

 <!--
                    <div class="cmsGroupItem">
			        <h2 class="cms cmsType_TextSingle cmsName_Content">Heading</h2>
			        <p class="cms cmsType_TextMulti cmsName_Content">Content</p>
                   <div class="clear"></div>
                   </div>
  -->

<%  For i As Integer = 1 To CMSFunctions.CountTextSingle("Privacy_Item_" & "*" & "_Content")%>
                 <div class="cmsGroupItem">
			        <h2 class="cms cmsType_TextSingle cmsName_Privacy_Item_<%=i%>_Heading"></h2>
			        <p class="cms cmsType_TextMulti cmsName_Privacy_Item_<%=i%>_Content"></p>

                   <div class="clear"></div>
                   </div>
<%Next %>

</div>



              </div>
	    </div>
	</div>	
   </div>
</asp:Content>

