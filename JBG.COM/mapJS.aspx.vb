Imports System.Web
Imports System.Data.SqlClient
Imports System.Data


Partial Class _mapJS
    Inherits System.Web.UI.Page

    Protected Function trimTruncate(theString As String) As String
        Dim temp As String = ""
        theString = theString.Replace(vbNewLine, " ").Replace(vbCR, " ").replace("""", "\""")
        Dim words() As String = split(theString)
        For i = 0 To words.length - 1
            If temp.length + words(i).length + 1 > 250 Then
                'temp &= "&#8230; <a href=\""gallery.aspx?p=" & projectID & "\"" >[MORE]</a>"
                temp &= "..."
                Return temp
            Else
                temp &= words(i) & " "
            End If
        Next
        Return temp

    End Function

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.ContentType = "application/javascript; charset=utf-8"
        Dim js As String
 
        js = "prop_data = ["
        Dim sql As String = ""
        sql &= "select p.ProjectID, p.address, p.City, p.State_id, p.zip, v2.item_value latlong, "
        sql &= "coalesce(v3.item_value, p.name) shortname, v4.item_value type,  Description, '0' inDev, F1.Item_Value as SeoURL "
        sql &= "from ipm_project p join ipm_project_field_value v1 on p.projectid = v1.projectid and v1.item_value = '1' "
        sql &= "join ipm_project_field_desc d1 on v1.Item_ID = d1.Item_ID and d1.Item_Tag = 'IDAM_PPUBLIC' "
        sql &= "join ipm_project_field_value v2 on p.projectid = v2.projectid and v2.item_value <> '' "
        sql &= "join ipm_project_field_desc d2 on v2.Item_ID = d2.Item_ID and d2.Item_Tag = 'IDAM_LATLONG' "
        sql &= "join ipm_project_field_value v3 on p.projectid = v3.projectid  join ipm_project_field_desc d3 on v3.Item_ID = d3.Item_ID and d3.Item_Tag = 'IDAM_SHORTNAME'  "
        sql &= "join ipm_project_field_value v4 on p.projectid = v4.projectid  join ipm_project_field_desc d4 on v4.Item_ID = d4.Item_ID and d4.Item_Tag = 'IDAM_PROJECTTYPE' "
        sql &= "left join ipm_project_field_value F1 on p.ProjectID = F1.ProjectID and F1.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SEO_URL') "
        sql &= "WHERE p.Available = 'Y' AND p.Show = 1 "
        If IDAMFunctions.IsLoggedIn = True Then
            sql &= "UNION Select p.ProjectID, p.address, p.City, p.State_id, p.zip, v2.item_value latlong, "
            sql &= "coalesce(v3.item_value, p.name) shortname, v4.item_value type,  Description, Isnull(v5.Item_Value, '0') inDev, F1.Item_Value as SeoURL "
            sql &= "from ipm_project p  join ipm_project_field_value v2 on p.projectid = v2.projectid and v2.item_value <> '' "
            sql &= "join ipm_project_field_desc d2 on v2.Item_ID = d2.Item_ID and d2.Item_Tag = 'IDAM_LATLONG' "
            sql &= "join ipm_project_field_value v3 on p.projectid = v3.projectid  join ipm_project_field_desc d3 on v3.Item_ID = d3.Item_ID and d3.Item_Tag = 'IDAM_SHORTNAME'  "
            sql &= "join ipm_project_field_value v4 on p.projectid = v4.projectid  join ipm_project_field_desc d4 on v4.Item_ID = d4.Item_ID and d4.Item_Tag = 'IDAM_PROJECTTYPE' "
            sql &= "left join ipm_project_field_value F1 on p.ProjectID = F1.ProjectID and F1.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SEO_URL') "
            sql &= "join ipm_project_field_value v5 on p.projectid = v5.projectid and v5.Item_ID = (Select Item_id From ipm_project_field_desc where Item_Tag = 'IDAM_INDEV') and v5.Item_Value = '1' "
            sql &= "WHERE p.Available = 'Y' AND p.Show = 1 "
        End If



        Dim DT1 As New DataTable("FProjects")
        DT1 = DBFunctions.GetDataTable(sql)

        If DT1.Rows.Count > 0 Then
            For i As Integer = 0 To DT1.Rows.Count - 1

                Dim ProjectURL As String = ""
                If DT1.Rows(i).Item("SeoURL").ToString.Trim <> "" Then
                    ProjectURL = DT1.Rows(i).Item("SeoURL").ToString.Trim
                Else
                    ProjectURL = DT1.Rows(i).Item("ProjectID").ToString.Trim
                End If
                ProjectURL = ProjectURL & "-property-gallery"

                If i <> 0 Then
                    js &= " , "
                End If


                ' If DT1(i)("latlong").Substring(0,DT1(i)("latlong").IndexOf(",") -1).Trim() > 36 && DT1(i)("latlong").Substring(0,DT1(i)("latlong").IndexOf(",") -1).Trim() > 40 && DT1(i)("latlong").Substring(1,DT1(i)("latlong").IndexOf(",")).Trim() < -76 && DT1(i)("latlong").Substring(1,DT1(i)("latlong").IndexOf(",")).Trim() > -81 
                ' End If

                js &= "{ ""id"": """ & DT1(i)("projectid") & """, "
                js &= """title"": """ & DT1(i)("shortname").Replace(vbNewLine, " ").Replace(vbCR, " ").replace("""", "\""") & """, "
                js &= """slug"": """ & DT1(i)("projectid") & """, "
                js &= """city"": """ & DT1(i)("city").Replace(vbNewLine, " ").Replace(vbCR, " ").replace("""", "\""") & """, "
                js &= """state"": """ & DT1(i)("state_id").Replace(vbNewLine, " ").Replace(vbCR, " ").replace("""", "\""") & """, "
                js &= """zip_code"": """ & DT1(i)("zip").Replace(vbNewLine, " ").Replace(vbCR, " ").replace("""", "\""") & """, "
                js &= """short_description"": """ & FormatFunctions.AutoFormatText(trimTruncate(DT1(i)("Description"))).Replace(vbNewLine, " ").Replace(vbCR, " ").replace("""", "\""") & """, "
                js &= """geocode"": """ & DT1(i)("latlong").replace(",-", ", -") & """, "
                js &= """link"": """ & ProjectURL & """, "
                js &= """image"": ""/dynamic/image/always/project/fit/230x120/92/ffffff/center/" & DT1(i)("Projectid").ToString().Trim() + ".jpg"
                js &= """, "
                If DT1(i)("type").ToString.Trim.IndexOf("Office") <> -1 Then
                    If DT1(i)("inDev").ToString = "1" Then
                        js &= """cat_id94"": ""1"", "
                    Else
                        js &= """cat_id4"": ""1"", "
                    End If
                End If

                If DT1(i)("type").ToString.Trim.IndexOf("Hotel") <> -1 Then
                    If DT1(i)("inDev").ToString = "1" Then
                        js &= """cat_id96"": ""1"", "
                    Else
                        js &= """cat_id6"": ""1"", "
                    End If
                End If

                If DT1(i)("type").ToString.Trim.IndexOf("Residential") <> -1 Then
                    If DT1(i)("inDev").ToString = "1" Then
                        js &= """cat_id95"": ""1"", "
                    Else
                        js &= """cat_id5"": ""1"", "
                    End If
                End If

                If DT1(i)("type").ToString.Trim.IndexOf("Retail") <> -1 Then
                    If DT1(i)("inDev").ToString = "1" Then
                        js &= """cat_id97"": ""1"", "
                    Else
                        js &= """cat_id7"": ""1"", "
                    End If
                End If

                If DT1(i)("inDev").ToString = "1" Then
                    js &= """dev"": ""1"", "
                End If

                js &= """active"": ""0"" } "


            Next

                End If


        js &= "];"

        Response.Clear()
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Write(js)

    End Sub


End Class
