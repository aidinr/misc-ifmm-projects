﻿Imports System.Net.Mail
Imports System.IO
Imports System.Threading
Imports System.Web.HttpResponse

Partial Class contact_submit
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim EmailBody As StringBuilder = New StringBuilder
        Dim EmailTo as String = CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Text\Contact_Recipient.txt")
        Dim message As MailMessage = New MailMessage(ConfigurationManager.AppSettings("ContactEmailFrom").ToString(), EmailTo)

        If (Not String.IsNullOrEmpty(Request.Params("firstname"))) Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.Write("-9")
            HttpContext.Current.Response.Flush()
            Return
            Exit Sub
        End If


        If (Not String.IsNullOrEmpty(Request.Params("last_name"))) Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.Write("-8")
            HttpContext.Current.Response.Flush()
            Return
            Exit Sub
        End If

        If (String.IsNullOrEmpty(Request.Params("name"))) Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.Write("-7")
            HttpContext.Current.Response.Flush()
            Return
            Exit Sub
        End If

        If (Not String.IsNullOrEmpty(Request.Params("Telephone"))) Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.Write("-6")
            HttpContext.Current.Response.Flush()
            Return
            Exit Sub
        End If

        If (Not String.IsNullOrEmpty(Request.Params("e-mail"))) Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.Write("-5")
            HttpContext.Current.Response.Flush()
            Return
            Exit Sub
        End If

        If (String.IsNullOrEmpty(Request.Params("email"))) Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.Write("-7")
            HttpContext.Current.Response.Flush()
            Return
            Exit Sub
        End If

        Try
            Dim FILE_NAME As String = Server.MapPath("submissions/submit-contact.txt")
            Dim Msg As String
            Msg = Request.Params("name") & "|"
            Msg &= Request.Params("company_name") & "|" 
            Msg &= Request.Params("email") & "|" 
            Msg &= Request.Params("address") & "|" 
            Msg &= Request.Params("City") & "|" 
            Msg &= Request.Params("State") & "|" 
            Msg &= Request.Params("zip") & "|" 
            Msg &= Request.Params("phone") & "|" 
            Msg &= Request.Params("message").Replace(vbCr, " ").Replace(vbLf, " ")  & "|" 
            Msg &= Request.ServerVariables("REMOTE_ADDR").ToString() & "|" 
            Msg &= Environment.NewLine & "-----------------------------------------" & Environment.NewLine

            If System.IO.File.Exists(FILE_NAME) = True Then
                Dim file_stream As FileStream = File.Open(FILE_NAME, FileMode.Append)
                Dim bytes As Byte() = New UTF8Encoding().GetBytes(Msg)
                file_stream.Write(bytes, 0, bytes.Length)
                file_stream.Flush()
                file_stream.Close()
            End If
        Catch ex As Exception

        End Try

            EmailBody.Append("Name: " + Request.Params("name") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Company Name: " + Request.Params("company_name") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Email: " + Request.Params("email") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Address: " + Request.Params("address") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("City: " + Request.Params("City") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("State: " + Request.Params("State") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("ZIP: " + Request.Params("zip") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Phone: " + Request.Params("phone") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Message: " + Request.Params("Message") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("IP Address: " + Request.ServerVariables("REMOTE_ADDR").ToString()  + Environment.NewLine + Environment.NewLine)
            message.IsBodyHtml = False
            message.Body = EmailBody.ToString()
            message.From = New System.Net.Mail.MailAddress(ConfigurationManager.AppSettings("ContactEmailFrom").ToString())
            message.Subject = "JBG Website Inquiry"
            message.ReplyTo = New MailAddress(Request.Params("email"))

            Dim SmtpMail As New SmtpClient
            Dim basicAuthenticationInfo As New System.Net.NetworkCredential(ConfigurationManager.AppSettings("SMTPUser").ToString(), ConfigurationManager.AppSettings("SMTPPassword").ToString())

            SmtpMail.Host = ConfigurationManager.AppSettings("SMTPServer").ToString()
            SmtpMail.UseDefaultCredentials = False
            smtpmail.Port = CInt(ConfigurationManager.AppSettings("SMTPPort").ToString())
            smtpmail.EnableSsl = CBool(ConfigurationManager.AppSettings("SMTPSSL").ToString())
            SmtpMail.Credentials = basicAuthenticationInfo
            SmtpMail.Send(message)

            Dim Response As HttpResponse = Context.Response

            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.BufferOutput = False
            HttpContext.Current.Response.ContentType = "text/html"
            HttpContext.Current.Response.Write("1")
            HttpContext.Current.Response.Flush()
            Try
                HttpContext.Current.Response.End()
            Catch ex As Exception

            End Try
            Thread.Sleep(900)

            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.Write("-9")
            HttpContext.Current.Response.Flush()
            Try
                HttpContext.Current.Response.End()
            Catch ex1 As Exception

            End Try
            Thread.Sleep(50)
      
    End Sub
End Class
