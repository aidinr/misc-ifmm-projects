﻿Imports System.Data

Partial Class search
    Inherits System.Web.UI.Page

    Public SearchPage As String
    Public SearchWords As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Request.QueryString("p") Is Nothing Then
            SearchPage = Request.QueryString("p")
        End If
        If Not Request.QueryString("q") Is Nothing Then
            SearchWords = Request.QueryString("q")
        End If

        Dim ProjectSearchOUT As String = ""

        Dim ContainerHTML As String = ""
        Dim ContentHTML As String = ""

        For i As Integer = 1 To CMSFunctions.CountTextSingle("Properties_Categories_Item_" & "*" & "_Heading_1")
            Dim CatName As String = ""
            Dim CatHeader As String = ""
            CatName = CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Properties_Categories_Item_" & i & "_Nav_Menu_Text")
            CatHeader = CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Properties_Categories_Item_" & i & "_Heading_1")

            Dim SearchSQL1 As String = ""
            SearchSQL1 &= "select p.ProjectID,  isnull(nullif(n.item_value, '' ),p.name) ProjectName, F1.Item_Value as SeoURL, p.State_Id, p.City, P.Description as ProjectDescription "
            SearchSQL1 &= "from ipm_project p "
            SearchSQL1 &= "left join ipm_project_field_value v2 on p.ProjectID = v2.ProjectID and v2.Item_ID = ( "
            SearchSQL1 &= "select Item_ID "
            SearchSQL1 &= "from ipm_project_field_desc where Item_Tag = 'IDAM_INDEV') "
            SearchSQL1 &= "join ipm_project_field_value v on p.ProjectID = v.ProjectID and v.Item_ID = ( select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_PPUBLIC') and v.Item_value = '1' "
            SearchSQL1 &= "left join ipm_project_field_value n on p.ProjectID = n.ProjectID and n.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SHORTNAME') "
            SearchSQL1 &= "join ipm_project_field_value t on p.ProjectID = t.ProjectID and t.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_PROJECTTYPE') "
            SearchSQL1 &= "join ipm_project_field_value l on p.ProjectID = l.ProjectID and l.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'iDAM_LDHEAD') "
            SearchSQL1 &= "left join ipm_project_field_value F1 on p.ProjectID = F1.ProjectID and F1.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SEO_URL') "
            SearchSQL1 &= "where p.Available = 'Y'  and  ISNULL(NULLIF(v2.Item_value,''),'0') <> '1' "
            SearchSQL1 &= "and t.item_value like '%" & CatName & "%' "
            SearchSQL1 &= "and ( "
            SearchSQL1 &= "p.name like  '%" & SearchWords & "%' "
            SearchSQL1 &= "or p.Description like '%" & SearchWords & "%' "
            SearchSQL1 &= "or p.DescriptionMedium like '%" & SearchWords & "%' "
            SearchSQL1 &= "or l.item_value like '%" & SearchWords & "%' "
            SearchSQL1 &= ") order by  isnull(nullif(n.item_value, '' ), p.name) "
            Dim ProjectsDT As New DataTable
            ProjectsDT = DBFunctions.GetDataTable(SearchSQL1)
            If ProjectsDT.Rows.Count > 0 Then
                ProjectSearchOUT &= "<div class=""grid_4"">" & vbNewLine
                ProjectSearchOUT &= "<h1>" & CatHeader & "</h1>" & vbNewLine
                ProjectSearchOUT &= "</div>" & vbNewLine

                ProjectSearchOUT &= "<div class=""grid_8"">"
                ProjectSearchOUT &= "<div class=""properties-cell"">"
                ProjectSearchOUT &= "<ul class=""pp"">"
                Dim HalfTable As Integer = 0
                HalfTable = ProjectsDT.Rows.Count \ 2

                For R As Integer = 0 To ProjectsDT.Rows.Count - 1
                    Dim ProjectURL As String = ""
                    If ProjectsDT.Rows(R).Item("SeoURL").ToString.Trim <> "" Then
                        ProjectURL = ProjectsDT.Rows(R).Item("SeoURL").ToString.Trim
                    Else
                        ProjectURL = ProjectsDT.Rows(R).Item("ProjectID").ToString.Trim
                    End If
                    If R < HalfTable Then
                        ProjectSearchOUT &= "<li  class=""tip-popper goLeft"">"
                    Else
                        ProjectSearchOUT &= "<li  class=""tip-popper goRight"">"
                    End If
                    ProjectSearchOUT &= "<a href=""" & ProjectURL & "-property-overview"">" & formatfunctionssimple.AutoFormatText(ProjectsDT.Rows(R).Item("ProjectName").ToString.Trim) & "</a>" & vbNewLine
                    ProjectSearchOUT &= "<div class=""box-info"">" & vbNewLine
                    ProjectSearchOUT &= "<h3 class=""title ppTitle"">" & vbNewLine
                    ProjectSearchOUT &= "<a href=""#"">" & ProjectsDT.Rows(R).Item("ProjectName").ToString.Trim & "</a>" & vbNewLine
                    ProjectSearchOUT &= "</h3>" & vbNewLine
                    ProjectSearchOUT &= "<div class=""inner-info"">" & vbNewLine
                    ProjectSearchOUT &= "<h2>" & ProjectsDT.Rows(R).Item("City").ToString.Trim & ", " & ProjectsDT.Rows(R).Item("State_ID").ToString.Trim & "</h2>" & vbNewLine
                    ProjectSearchOUT &= "<img src=""dynamic/image/always/project/best/230x120/92/ffffff/Center/" & ProjectsDT.Rows(R).Item("ProjectID").ToString.Trim & ".jpg" & """" & " alt=""""/>" & vbNewLine
                    ProjectSearchOUT &= "<p>" & formatfunctions.AutoFormatText(ProjectsDT.Rows(R).Item("ProjectDescription").ToString.Trim) & "</p>" & vbNewLine
                    ProjectSearchOUT &= "</div>" & vbNewLine
                    ProjectSearchOUT &= "</div>" & vbNewLine
                    ProjectSearchOUT &= "</li>" & vbNewLine
                Next
                ProjectSearchOUT &= "</ul>" & vbNewLine
                'ProjectSearchOUT &= "<hr>" & vbNewLine
                ProjectSearchOUT &= "</div>" & vbNewLine
                ProjectSearchOUT &= "</div>" & vbNewLine
            End If
        Next

        If IDAMFunctions.IsLoggedIn = True Then
            Dim SearchSQL2 As String = ""
            SearchSQL2 &= "select p.projectid,  isnull(nullif(n.item_value, '' ),p.name) ProjectName, F1.Item_Value as SeoURL, p.State_Id, p.City, P.Description as ProjectDescription "
            SearchSQL2 &= "from ipm_project p "
            SearchSQL2 &= "join ipm_project_field_value v2 on p.ProjectID = v2.ProjectID and v2.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_INDEV')  and  v2.Item_value = '1' "
            SearchSQL2 &= "left join ipm_project_field_value n on p.ProjectID = n.ProjectID and n.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SHORTNAME') "
            SearchSQL2 &= "join ipm_project_field_value l on p.ProjectID = l.ProjectID and l.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'iDAM_LDHEAD') "
            SearchSQL2 &= "left join ipm_project_field_value F1 on p.ProjectID = F1.ProjectID and F1.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SEO_URL') "
            SearchSQL2 &= "where p.Available = 'Y' "
            SearchSQL2 &= "and ( "
            SearchSQL2 &= "p.name like  '%" & SearchWords & "%' "
            SearchSQL2 &= "or p.Description like '%" & SearchWords & "%' "
            SearchSQL2 &= "or p.DescriptionMedium like '%" & SearchWords & "%' "
            SearchSQL2 &= "or l.item_value like '%" & SearchWords & "%' "
            SearchSQL2 &= ") order by  isnull(nullif(n.item_value, '' ), p.name) "
            Dim DevelopmentDT As New DataTable
            DevelopmentDT = DBFunctions.GetDataTable(SearchSQL2)
            If DevelopmentDT.Rows.Count > 0 Then

                ProjectSearchOUT &= "<div class=""grid_4"">" & vbNewLine
                ProjectSearchOUT &= "<h1>Development</h1>" & vbNewLine
                ProjectSearchOUT &= "</div>" & vbNewLine

                ProjectSearchOUT &= "<div class=""grid_8"">"
                ProjectSearchOUT &= "<div class=""properties-cell"">"
                ProjectSearchOUT &= "<ul class=""pp"">"
                Dim HalfTable As Integer = 0
                HalfTable = DevelopmentDT.Rows.Count \ 2

                For R As Integer = 0 To DevelopmentDT.Rows.Count - 1
                    Dim ProjectURL As String = ""
                    If DevelopmentDT.Rows(R).Item("SeoURL").ToString.Trim <> "" Then
                        ProjectURL = DevelopmentDT.Rows(R).Item("SeoURL").ToString.Trim
                    Else
                        ProjectURL = DevelopmentDT.Rows(R).Item("ProjectID").ToString.Trim
                    End If
                    If R < HalfTable Then
                        ProjectSearchOUT &= "<li  class=""tip-popper goLeft"">"
                    Else
                        ProjectSearchOUT &= "<li  class=""tip-popper goRight"">"
                    End If
                    ProjectSearchOUT &= "<a href=""" & ProjectURL & "-property-overview"">" & formatfunctionssimple.AutoFormatText(DevelopmentDT.Rows(R).Item("ProjectName").ToString.Trim) & "</a>" & vbNewLine
                    ProjectSearchOUT &= "<div class=""box-info"">" & vbNewLine
                    ProjectSearchOUT &= "<h3 class=""title ppTitle"">" & vbNewLine
                    ProjectSearchOUT &= "<a href=""#"">" & DevelopmentDT.Rows(R).Item("ProjectName").ToString.Trim & "</a>" & vbNewLine
                    ProjectSearchOUT &= "</h3>" & vbNewLine
                    ProjectSearchOUT &= "<div class=""inner-info"">" & vbNewLine
                    ProjectSearchOUT &= "<h2>" & DevelopmentDT.Rows(R).Item("City").ToString.Trim & ", " & DevelopmentDT.Rows(R).Item("State_ID").ToString.Trim & "</h2>" & vbNewLine
                    ProjectSearchOUT &= "<img src=""dynamic/image/always/project/best/230x120/92/ffffff/Center/" & DevelopmentDT.Rows(R).Item("ProjectID").ToString.Trim & ".jpg" & """" & " alt=""""/>" & vbNewLine
                    ProjectSearchOUT &= "<p>" & formatfunctions.AutoFormatText(DevelopmentDT.Rows(R).Item("ProjectDescription").ToString.Trim) & "</p>" & vbNewLine
                    ProjectSearchOUT &= "</div>" & vbNewLine
                    ProjectSearchOUT &= "</div>" & vbNewLine
                    ProjectSearchOUT &= "</li>" & vbNewLine
                Next
                ProjectSearchOUT &= "</ul>" & vbNewLine
                ProjectSearchOUT &= "<hr>" & vbNewLine
                ProjectSearchOUT &= "</div>" & vbNewLine
                ProjectSearchOUT &= "</div>" & vbNewLine
            End If
        End If


        Projects_Literal.Text = ProjectSearchOUT


    End Sub


End Class
