﻿Imports System.Data
Partial Class energystar
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim tiii As Integer = 0
        Dim ProjectsSQL As String = ""
        ProjectsSQL &= "select p.ProjectID, p.Name as ProjectName, p.State_Id, p.City, LTRIM(RTRIM(S.Name)) as ProjectState, F1.Item_Value as SeoURL, P.Description as ProjectDescription  from ipm_project p "
        ProjectsSQL &= "join ipm_project_field_value f on p.ProjectID = f.ProjectID and f.Item_ID = 21610620 and f.Item_Value = '1' "
        ProjectsSQL &= "join ipm_project_field_value f2 on p.ProjectID = f2.ProjectID and f2.Item_ID = 21610726 and f2.Item_Value = '1' "
        ProjectsSQL &= "left join ipm_project_field_value F1 on p.ProjectID = F1.ProjectID and F1.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SEO_URL') "
        ProjectsSQL &= "join IPM_STATE S ON S.State_id = p.State_id "
        ProjectsSQL &= "where p.Show = 1 and p.Available = 'Y' "
        ProjectsSQL &= "Order By p.Name "
        Dim ProjectsDT As New DataTable
        ProjectsDT = DBFunctions.GetDataTable(ProjectsSQL)
        Dim ProjectsOUT As String = ""
        If ProjectsDT.Rows.Count > 0 Then
            ProjectsDT.DefaultView.Sort = "ProjectState ASC"
            Dim UniqueStatesDT As New DataTable
            UniqueStatesDT = ProjectsDT.DefaultView.ToTable(True, "ProjectState")
            'UniqueStatesDT.DefaultView.Sort = "ProjectState ASC"
            Dim GroupedProjectsDT As DataTable
            For S As Integer = 0 To UniqueStatesDT.Rows.Count - 1
                ProjectsOUT &= "<div class=""grid_8"">"
                ProjectsOUT &= "<h3>" & UniqueStatesDT.Rows(S).Item("ProjectState").ToString.Trim & "  </h3>" & vbNewLine
                ProjectsOUT &= "<ul class=""pp"">"
                GroupedProjectsDT = New DataTable
                ProjectsDT.DefaultView.RowFilter = "ProjectState = '" & UniqueStatesDT.Rows(S).Item("ProjectState") & "'"
                ProjectsDT.DefaultView.Sort = "ProjectName ASC"
                GroupedProjectsDT = ProjectsDT.DefaultView.ToTable
                For R As Integer = 0 To GroupedProjectsDT.Rows.Count - 1
                    Dim ProjectURL As String = ""
                    If GroupedProjectsDT.Rows(R).Item("SeoURL").ToString.Trim <> "" Then
                        ProjectURL = GroupedProjectsDT.Rows(R).Item("SeoURL").ToString.Trim
                    Else
                        ProjectURL = GroupedProjectsDT.Rows(R).Item("ProjectID").ToString.Trim
                    End If
                    tiii = tiii +1
                    ProjectsOUT &= "<li class=""tip-popper"">"
                    ProjectsOUT &= "<a class=""pop"" rel=""#pop" & tiii & """ href=""" & ProjectURL & "-property-sustainability"">" & formatfunctionssimple.AutoFormatText(GroupedProjectsDT.Rows(R).Item("ProjectName").ToString.Trim) & "</a>" & vbNewLine
                    ProjectsOUT &= "<div class=""box-info propList"" id=""pop" & tiii & """>" & vbNewLine
                    ProjectsOUT &= "<h3 class=""title ppTitle"">" & vbNewLine
                    ProjectsOUT &= "<a href=""" & ProjectURL & "-property-sustainability" & """>" & GroupedProjectsDT.Rows(R).Item("ProjectName").ToString.Trim & "</a>" & vbNewLine
                    ProjectsOUT &= "</h3>" & vbNewLine
                    ProjectsOUT &= "<div class=""inner-info"">" & vbNewLine
                    ProjectsOUT &= "<h2>" & GroupedProjectsDT.Rows(R).Item("City").ToString.Trim & ", " & GroupedProjectsDT.Rows(R).Item("State_ID").ToString.Trim & "</h2>" & vbNewLine
                    ProjectsOUT &= "<img src=""dynamic/image/always/project/best/230x120/92/ffffff/south/" & GroupedProjectsDT.Rows(R).Item("ProjectID").ToString.Trim & ".jpg" & """" & " alt=""""/>" & vbNewLine
                    ProjectsOUT &= "<p>" & formatfunctions.AutoFormatText(CMSFunctions.GetWords(GroupedProjectsDT.Rows(R).Item("ProjectDescription").ToString.Trim, 40)) & "&#8230; <a href=""" & ProjectURL & "-property-gallery"">[More]</a></p>" & vbNewLine
                    ProjectsOUT &= "</div>" & vbNewLine
                    ProjectsOUT &= "</div>" & vbNewLine
                    ProjectsOUT &= "</li>" & vbNewLine
                Next
                ProjectsOUT &= "</ul>" & vbNewLine
                ProjectsOUT &= "</div>" & vbNewLine
                ProjectsOUT &= "<div class=""grid_2 alpha"">" & vbNewLine
                ProjectsOUT &= "&nbsp;" & vbNewLine
                ProjectsOUT &= "</div>" & vbNewLine
            Next


            Projects_Literal.Text = ProjectsOUT
        End If



    End Sub


    End Class
