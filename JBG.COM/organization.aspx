﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="organization.aspx.vb" Inherits="organization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="navigation-lower">
		<ul class="second-level">
				  <li class="navlink">      
				  <a href="investment-management">Investment Management </a></li>
				  <li class="navlink">      
				  <a href="development">Development</a></li>
				  <li class="navlink">      
				  <a href="asset-management">Asset Management</a></li>		  
				  <li class="navlink">      
				  <a href="organization">Organization</a></li>
				  <li class="navlink">      
				  <a href="affordable-housing">Affordable Housing</a></li>	  
				  <li class="navlink">      
				  <a href="awards-recognition">Awards</a></li>
				  <li class="navlink">      
				  <a href="public-art">Public Art</a></li>	  
			</ul>
 </div>
   <div id="content">
	<div class="inner-content" >		
    <%If HasFilters = True Then%>
            <div class="container_12" id="main">  
            <%If FilterType = "ByLastName" Then%>
            <div class="grid_12 page-header"> 
		     <h1>Organization:<asp:Literal ID="FilterValue_Literal" runat="server"></asp:Literal> BROWSE BY LAST NAME</h1>
             </div> 		 
			<div class="clear"></div> 
			<div class="grid_12"><asp:Literal ID="FirstLetterHeader_Literal" runat="server"></asp:Literal></div>
             <%End If%>
             <%If FilterType = "ByGroup" Then%>
             <div class="grid_12 page-header"> 
             <h1><%=FilterValue.Replace("-", " ")%>: <span style="padding: 0 0 0 60px; font-size: 11px; color: #474747">BROWSE BY GROUP</span></h1></div> 
             <div class="grid_12">
             <ul class="orgGroup orgGroupTop">
             <li><a href="organization-senior-advisors">Senior Advisors</a></li>
			 <li><a href="organization-executive-committee">Executive Committee</a></li>
			 <li><a href="organization-management-committee">Management Committee</a></li>
			 <li><a href="organization-owners">Owners</a></li>
			 <li><a href="organization-key-personnel">Key Personnel</a></li>
             </ul>
             </div>
             <%End If%>
			
			<div class="clear"></div>
			<hr/>
			<div class="quicklinks">
			<div class="qlink float-left">
			<a href="organization">&lt;&nbsp;&nbsp;&nbsp;BACK TO ORGANIZATION</a>
			</div>
			</div>
			<div class="clear"></div>
			<div class="grid_4"></div>
			<div class="grid_8"><asp:Literal ID="Users_Literal" runat="server"></asp:Literal></div>
			</div>

    <%Else%>
    <div class="container_12"  id="main"> 

		<div class="grid_12 page-header"> 
		<h1>Organization</h1> 
		</div> 
		<div class="clear"></div>
		<hr/>
		<div class="grid_4">
		     <img style="float: right; padding-bottom: 20px;" src="/assets/images/Employment_Image.png" alt="Not Yet Available">
		</div>
		<div class="grid_8">
		<p class="cms cmsType_TextMulti cmsName_Organization_Intro">
			
		</p>
		</div><div class="clear"></div>

<div class="longBar">
<h1>Browse by last name &emsp; &gt; &emsp;</h1>
<asp:Literal ID="FirstLetter_Literal" runat="server"></asp:Literal>
<span class="clear"></span>
</div>
<div class="longBar">
<h1>Browse by Group &emsp; &gt; &emsp;</h1>
			 <ul class="orgAlpha">
			  <li><a href="organization-senior-advisors">Senior Advisors</a></li>
			  <li><a href="organization-executive-committee">Executive Committee</a></li>
			  <li><a href="organization-management-committee">Management Committee</a></li>
			  <li><a href="organization-owners">Owners</a></li>
			  <li><a href="organization-key-personnel">Key Personnel</a></li>
			 </ul>
<span class="clear"></span>
</div>
<div class="clear"></div>

	</div>
    <%End If%>

	</div>	
   </div>
</asp:Content>

