﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="property.aspx.vb" Inherits="_property" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/JBG.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%
Dim pageURL As String = HttpContext.Current.Request.RawUrl
Dim PageName As String = Path.GetFileName(Request.PhysicalPath).ToLower
pageURL = Replace(pageURL, "/", "")
If InStr(pageURL, "?") > 0 Then
pageURL = pageURL.Substring(0, pageURL.IndexOf("?"))
pageURL = Replace(pageURL, "/", "")
End If
%>

<%If File.Exists(Server.MapPath("~") & "cms\data\Metadata\" & "metadataTitle_" & pageURL & ".txt") Then%>
<%If CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Metadata\" & "metadataTitle_" & pageURL & ".txt") <> "" Then%>
<%="<title>" & CMSFunctions.GetMetaTitle(Server.MapPath("~") & "cms\data\", pageURL) & "</title>"%>
<%Else%>
<%  If ProjectSEOTitle <> "" Then%>
<%= "<title>" & ProjectSEOTitle & "</title>"%>
<%Else%>
<%= "<title>" & ProjectTitle & " / JBG</title>"%>
<%End If%>
<%End If%>
<%Else%>
<%  If ProjectSEOTitle <> "" Then%>
<%= "<title>" & ProjectSEOTitle & "</title>"%>
<%Else%>
<%= "<title>" & ProjectTitle & " / JBG</title>"%>
<%End If%>
<%End If%>

<%  If File.Exists(Server.MapPath("~") & "cms\data\Metadata\" & "metadataDescription_" & pageURL & ".txt") Then%>
<%  If CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Metadata\" & "metadataDescription_" & pageURL & ".txt") <> "" Then%>
<meta name="description" content="<% =CMSFunctions.GetMetaDescription(Server.MapPath("~") & "cms\data\", pageURL)%>" />
<%Else%>
<%  If ProjectSEODescription <> "" Then%>
<meta name="description" content="<% =ProjectSEODescription%>" />
<%Else%>
<%="<meta name=""description"" content="" "" />"%>
<%End If%>
<%End If%>
<%Else%>
<%  If ProjectSEODescription <> "" Then%>
<meta name="description" content="<% =ProjectSEODescription%>" />
<%Else%>
<meta name="description" content=" " />
<%End If%>
<%End If%>

<%  If File.Exists(Server.MapPath("~") & "cms\data\Metadata\" & "metadataKeywords_" & pageURL & ".txt") Then%>
<%  If CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Metadata\" & "metadataKeywords_" & pageURL & ".txt") <> "" Then%>
<meta name="keywords" content="<% =CMSFunctions.GetMetaKeywords(Server.MapPath("~") & "cms\data\", pageURL)%>" />
<%Else%>
<%  If ProjectSEOKeywords <> "" Then%>
<meta name="keywords" content="<% =ProjectSEOKeywords%>" />
<%Else%>
<%="<meta name=""keywords"" content="" "" />"%>
<%End If%>
<%End If%>
<%Else%>
<%  If ProjectSEOKeywords <> "" Then%>
<meta name="keywords" content="<% =ProjectSEOKeywords%>" />
<%Else%>
<meta name="keywords" content=" " />
<%End If%>
<%End If%>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
		<ul class="second-level">
			  <li class="navlink">      
			  <a href="properties-map">Properties Map</a></li>
			  <li class="navlink">      
			  <a href="properties">Properties List</a></li> 	  
		</ul>
        <div id="propSearch">
            <form method="get" action="search" id="propsearch"><input type="text" class="q" name="q"  alt="PROPERTY SEARCH"/><input type="submit" value="Search" class="submit"/>
            <input type="hidden" value="propertiesmap" name="p"/></form>
        </div>
   </div>
   <div id="content">
	<div class="inner-content" >
		<div class="container_12"  id="main">
        <div class="property-page"> 

			<div class="grid_12 page-header">
				<h1><asp:Literal ID="ProjectTitle1_Literal" runat="server"></asp:Literal></h1>
                <ul>
					<li><a href="<%=UrlIdValue%>-property-gallery">Gallery</a></li>
					<li><a href="<%=UrlIdValue%>-property-overview">Overview</a></li>
                    <%If LatxLong <> "" Then%>
					<li><a href="<%=UrlIdValue%>-property-map">Map</a></li>
                    <%End if %>
                    <%If HasPublicArt = True Then%>
                    <li><a href="<%=UrlIdValue%>-property-public-art">Public Art</a></li>
                    <%End If %>
                    <%If HasFloorPlans = True Then%>
                    <li><a href="<%=UrlIdValue%>-property-floor-plans">Floor Plans</a></li>
                    <%End If%>
                    <%If HasTestFits = True Then%>
					<li><a href="<%=UrlIdValue%>-property-test-fits">Test Fits</a></li>
                    <%End if %>
                    <%If HasVideos = True Then%>
                    <li><a href="<%=UrlIdValue%>-property-video">Video </a></li>
                    <%End if %>
                    <%If HasSustainability = True Then%>
					<li><a href="<%=UrlIdValue%>-property-sustainability">Sustainability </a></li>
                    <%End if %>
                    <%If HasPublicResources = True Then%>
                    <li><a href="<%=UrlIdValue%>-property-resources">Resources</a></li>
                    <%ElseIf HasPublicResources = True And IDAMFunctions.IsLoggedIn = True Then%>
                    <li><a href="<%=UrlIdValue%>-property-resources">Resources</a></li>
                    <%End If %>
					 <%If HasPano = True Then%>
                    <li><a class="Panorama" rel="<%=ProjectID%>">360 Panorama</a></li>
                    <%End if %>
				</ul>
			</div>
			<div class="clear"></div>


            <%Select SelectedTab%>
            <%Case "gallery"%>
            <asp:Literal ID="Gallery_Literal" runat="server"></asp:Literal>
			<div class="prefix_4 grid_8 content">
				<h2><asp:Literal ID="GalleyHeadline_Literal" runat="server"></asp:Literal></h2>
				<p><asp:Literal ID="GalleryDescription_Literal" runat="server"></asp:Literal></p>
			</div>
			<div class="clear"></div>

            <%--NEWS--%>
                        <asp:Literal ID="GalleryRecentNews_Literal" runat="server"></asp:Literal>
             <%--END NEWS--%>

              <%Case "overview"%>
	    <hr/>

			<div class="grid_4">
				<h1 class="right"><asp:Literal ID="ProjectTitle2_Literal" runat="server"></asp:Literal></h1>
			</div>
            <div class="clear clear-none"></div>
			<div class="grid_4">
				<dl>

                <asp:Literal ID="ProjectLeft_Literal" runat="server"></asp:Literal>
              
                </dl>
			</div>
			<div class="grid_4">
            <asp:Literal ID="ProjectDescription_Literal" runat="server"></asp:Literal>
			
            <%If LeasingAgentsOUT <> "" Or AwardsOUT <> "" Or RelatedProjectsOUT <> "" Then%>
            <dl>
            <%End If%>
            <asp:Literal ID="LeasingAgents_Literal" runat="server"></asp:Literal>
            <asp:Literal ID="ProjectRight_Literal" runat="server"></asp:Literal>
            <asp:Literal ID="ProjectAwards_Literal" runat="server"></asp:Literal>
            <asp:Literal ID="RelatedProjects_Literal" runat="server"></asp:Literal>
             <%If LeasingAgentsOUT <> "" Or AwardsOUT <> "" Or RelatedProjectsOUT <> "" Then%>
            </dl>
             <%End If%>
			</div>	
            <%Case "map"%>
            <asp:Literal ID="ProjectMap_Literal" runat="server"></asp:Literal>
            <%Case "video" %>
	    <hr/>

<div class="vid-contain">
<div class="vimeo">
<%
    Dim VideoID As String() = VideoLink.Split("/")
    %>
<iframe width="980" height="550" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src="http://vimeo.com/moogaloop.swf?clip_id=<%=VideoID.Last%>&amp;autoplay=1"></iframe>
<%--<a href="<%=VideoLink%>"></a>--%>
</div>
</div>
<div class="hr slim"></div>
<div class="clear"></div>

<%Case "sustainability" %>
	    <hr/>

			<div class="grid_4">
			<h1 class="right"><asp:Literal ID="ProjectTitle3_Literal" runat="server"></asp:Literal></h1>
			</div>
			<div class="grid_4">
             <asp:Literal ID="SustainabilityLeft_Literal" runat="server"></asp:Literal>
			</div>
			<div class="grid_4">
            <asp:Literal ID="SustainabilityRight_Literal" runat="server"></asp:Literal>
			</div>
<%Case "resources"%>
	    <hr/>

<div class="grid_4">
<h1 class="right"><asp:Literal ID="ProjectTitle4_Literal" runat="server"></asp:Literal></h1>
</div>
			<div class="grid_8">
            <asp:Literal ID="PublicResources_Literal" runat="server"></asp:Literal>
            
            <asp:Literal ID="SecuredResources_Literal" runat="server"></asp:Literal>
			</div>
<%Case "testfits"%>
	    <hr/>
	<div class="grid_4">
			<h1 class="right"><asp:Literal ID="ProjectTitle5_Literal" runat="server"></asp:Literal></h1>
    </div>
<div class="grid_8">
			<asp:Literal ID="TestFits_Literal" runat="server"></asp:Literal>
</div>
<%Case "floorplans" %>
	    <hr/>
	<div class="grid_4">
			<h1 class="right"><asp:Literal ID="ProjectTitle6_Literal" runat="server"></asp:Literal></h1>
    </div>
<div class="grid_8">
<asp:Literal ID="FloorPlans_Literal" runat="server"></asp:Literal>
</div>
<%Case "publicart" %>
	    <hr/>
	<div class="grid_4">
	<h1 class="right"><asp:Literal ID="ProjectTitle7_Literal" runat="server"></asp:Literal></h1>
    </div>
<div class="grid_4">
	<asp:Literal ID="PublicArt_Literal" runat="server"></asp:Literal>
</div>
<div class="grid_4">
<asp:Literal ID="PublicArtImage_Literal" runat="server"></asp:Literal>
   </div>
<%End Select%>

            </div>  
		</div>
		<div class="clear"></div><br />
   	    </div>
	</div>	


</asp:Content>

