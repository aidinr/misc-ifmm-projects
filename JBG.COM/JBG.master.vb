﻿Imports System.Data
Imports System.IO
Partial Class JBG
    Inherits System.Web.UI.MasterPage

    Public UrlPage As String
    Public OriginalUrlPage As String

    Public OneCarouselAssetID As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
     

        'If pageURL <> "" Then
        '    Dim CheckOldLinkSQL As String = "select Name from IPM_CATEGORY where PARENT_CAT_ID = 21613935 and DESCRIPTION = '" & pageURL.ToLower & "' "
        '    Dim CheckOldLinkDT As New DataTable
        '    CheckOldLinkDT = DBFunctions.GetDataTable(CheckOldLinkSQL)
        '    If CheckOldLinkDT.Rows.Count > 0 Then
        '        Dim NewLinkURL As String = CheckOldLinkDT.Rows(0).Item("Name").Tostring.Trim
        '        Response.Status = "307 Temporary Redirect"
        '        Response.AddHeader("Location", NewLinkURL)
        '    End If
        'End If



        Dim OneCarouselSQL As String = ""
        OneCarouselSQL &= "SELECT TOP 1 a.Asset_id, WPixel "
        OneCarouselSQL &= "FROM IPM_CARROUSEL c "
        OneCarouselSQL &= "JOIN IPM_CARROUSEL_ITEM i on c.Carrousel_ID = i.Carrousel_ID and c.Available = 'Y' and c.Active = 1 and i.Available = 'Y' and i.Active = 1 and Available_Date < getdate() join IPM_ASSET a on i.Asset_ID = a.Asset_ID and a.Available = 'Y' and a.Active = 1 and WPixel > 0 and HPixel > 0 "
        OneCarouselSQL &= "WHERE c.name = 'Home Page Public Web Site Backgrounds' Order By newid() "
        Dim OneCarouselDT As New DataTable
        OneCarouselDT = DBFunctions.GetDataTable(OneCarouselSQL)
        If OneCarouselDT.Rows.Count > 0 Then
            OneCarouselAssetID = OneCarouselDT.Rows(0).Item("Asset_id")
        End If


        Dim CarouselSQL As String = ""
        CarouselSQL &= "SELECT TOP 6 a.Asset_id, WPixel "
        CarouselSQL &= "FROM IPM_CARROUSEL c "
        CarouselSQL &= "JOIN IPM_CARROUSEL_ITEM i on c.Carrousel_ID = i.Carrousel_ID and c.Available = 'Y' and c.Active = 1 and i.Available = 'Y' and i.Active = 1 and Available_Date < getdate() join IPM_ASSET a on i.Asset_ID = a.Asset_ID and a.Available = 'Y' and a.Active = 1 and WPixel > 0 and HPixel > 0 "
        CarouselSQL &= "WHERE c.name = 'Home Page Public Web Site Backgrounds' Order By newid() "
        Dim CarouselDT As New DataTable
        CarouselDT = DBFunctions.GetDataTable(CarouselSQL)

        Dim CarouselOUT As String = ""
        Dim BgOUT As String = ""

        If CarouselDT.Rows.Count > 0 Then
            CarouselOUT &= "jQuery(function ($) { " & vbNewLine
            CarouselOUT &= "$.supersized({ " & vbNewLine
            CarouselOUT &= "start_slide: 0, " & vbNewLine
            CarouselOUT &= "new_window: 1, " & vbNewLine
            CarouselOUT &= "image_protect: 1, " & vbNewLine
            CarouselOUT &= "min_width: 1024, " & vbNewLine
            CarouselOUT &= "min_height: 0, " & vbNewLine
            CarouselOUT &= "vertical_center: 1, " & vbNewLine
            CarouselOUT &= "horizontal_center: 1, " & vbNewLine
            CarouselOUT &= "fit_always: 0, " & vbNewLine
            CarouselOUT &= "fit_portrait: 1, " & vbNewLine
            CarouselOUT &= "fit_landscape: 0, " & vbNewLine
            CarouselOUT &= "slides: [ " & vbNewLine
            For R As Integer = 0 To CarouselDT.Rows.Count - 1

                BgOUT &= " <img src=""/assets/images/clear.gif"" longdesc=""" & CarouselDT.Rows(R)("Asset_ID").ToString.Trim & ".jpg"" alt="""" />" & vbNewLine


                CarouselOUT &= "{"
                CarouselOUT &= "image: 'dynamic/image/week/asset/liquid/x2000/95/ffffff/Center/" & CarouselDT.Rows(R)("Asset_ID").ToString.Trim & ".jpg'," & vbNewLine
                CarouselOUT &= "title: '" & formatfunctionsjs.AutoFormatText(CarouselDT.Rows(R)("WPixel").ToString.Trim) & "'" & vbNewLine
                If R < CarouselDT.Rows.Count Then
                    CarouselOUT &= "}, " & vbNewLine
                Else
                    CarouselOUT &= "} " & vbNewLine
                End If
            Next
            CarouselOUT &= "] " & vbNewLine
            CarouselOUT &= "}); " & vbNewLine
            CarouselOUT &= "}); " & vbNewLine


            bg_literal.Text = BgOUT
        End If

    End Sub

End Class

