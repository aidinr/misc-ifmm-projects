﻿Imports System
Imports System.Data
Imports System.IO
Imports System.Net
Imports System.Text
Imports SharpCompress.Archive
Imports SharpCompress.Common

Partial Class _property
    Inherits System.Web.UI.Page
    'Inherits CMSBasePage

    Public ProjectID As String = ""
    Public UrlIdValue As String = ""
    Public SelectedTab As String = ""
    Public IsDevProject As Boolean
    Public LatxLong As String = ""
    Public HasPublicArt As Boolean = False
    Public HasFloorPlans As Boolean = False
    Public HasTestFits As Boolean = False
    Public HasVideos As Boolean = False
    Public HasSustainability As Boolean = False
    Public HasPublicResources As Boolean = False
    Public HasSecuredResources As Boolean = False

    Public HasPano As Boolean = False

    Public ProjectTitle As String = ""
    Public ProjectSEOTitle As String
    Public ProjectSEODescription As String
    Public ProjectSEOKeywords As String

    Public LeasingAgentsOUT As String = ""
    Public AwardsOUT As String = ""
    Public RelatedProjectsOUT As String = ""
    Public VideoLink As String = ""

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        'Dim aaaa As String = HttpContext.Current.Request.RawUrl
        'Response.Write("Page:" & aaaa)


        If Not Request.QueryString("tab") Is Nothing Then
            SelectedTab = Request.QueryString("tab")
        End If
        If Not Request.QueryString("id") Is Nothing Then
            UrlIdValue = Request.QueryString("id")
            If IsNumeric(UrlIdValue) = True Then
                Dim CheckProjectSql As String = "SELECT P.ProjectID FROM IPM_PROJECT P where P.ProjectID = " & UrlIdValue & ""
                Dim CheckProjectDT As New DataTable("CheckProjectDT")
                CheckProjectDT = DBFunctions.GetDataTable(CheckProjectSql)
                If CheckProjectDT.Rows.Count > 0 Then
                    ProjectID = CheckProjectDT.Rows(0)("ProjectID").ToString.Trim
                Else
                    Dim CheckProjectByUrlSql As String = ""
                    CheckProjectByUrlSql &= "SELECT IV.ProjectID, P.Name "
                    CheckProjectByUrlSql &= "FROM IPM_PROJECT_FIELD_VALUE IV "
                    CheckProjectByUrlSql &= "JOIN IPM_PROJECT P ON P.ProjectID = IV.ProjectID "
                    CheckProjectByUrlSql &= "WHERE IV.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SEO_URL') AND IV.Item_Value = '" & UrlIdValue & "'"
                    Dim CheckProjectByUrlDT As New DataTable("CheckProjectByUrlDT")
                    CheckProjectByUrlDT = DBFunctions.GetDataTable(CheckProjectByUrlSql)
                    If CheckProjectByUrlDT.Rows.Count > 0 Then
                        ProjectID = CheckProjectByUrlDT.Rows(0)("ProjectID").ToString.Trim
                    Else
                    End If
                End If
                'if is not numeric
            Else
                Dim CheckProjectByUrlSql As String = ""
                CheckProjectByUrlSql &= "SELECT IV.ProjectID, P.Name "
                CheckProjectByUrlSql &= "FROM IPM_PROJECT_FIELD_VALUE IV "
                CheckProjectByUrlSql &= "JOIN IPM_PROJECT P ON P.ProjectID = IV.ProjectID "
                CheckProjectByUrlSql &= "WHERE IV.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SEO_URL') AND IV.Item_Value = '" & UrlIdValue & "'"
                Dim CheckProjectByUrlDT As New DataTable("CheckProjectByUrlDT")
                CheckProjectByUrlDT = DBFunctions.GetDataTable(CheckProjectByUrlSql)
                If CheckProjectByUrlDT.Rows.Count > 0 Then
                    ProjectID = CheckProjectByUrlDT.Rows(0)("ProjectID").ToString.Trim
                Else
                End If
            End If
            'if is nothing
        Else
        End If

        If ProjectID <> "" AndAlso IsNumeric(ProjectID) = True AndAlso SelectedTab <> "" Then
            Dim ProjectTypeSQL As String = ""
            ProjectTypeSQL &= "select p.projectid, isnull(nullif(v1.Item_Value,''), '0') PublicProject , isnull(nullif(v2.Item_Value,''),'0') DevProject "
            ProjectTypeSQL &= "from ipm_project p "
            ProjectTypeSQL &= "left join ipm_project_field_value v1 on p.ProjectID = v1.ProjectID and v1.item_id = (select item_id from ipm_project_field_desc where item_tag = 'IDAM_PPUBLIC') "
            ProjectTypeSQL &= "left join ipm_project_field_value v2 on p.ProjectID = v2.ProjectID and v2.item_id = (select item_id from ipm_project_field_desc where item_tag = 'IDAM_INDEV') "
            ProjectTypeSQL &= "Where p.ProjectID = " & ProjectID & ""
            Dim ProjectTypeDT As New DataTable
            ProjectTypeDT = DBFunctions.GetDataTable(ProjectTypeSQL)
            If ProjectTypeDT.Rows.Count > 0 Then
                If ProjectTypeDT.Rows(0).Item("DevProject").ToString.Trim = "1" Then
                    IsDevProject = True
                End If
            End If

            If IsDevProject = True And IDAMFunctions.IsLoggedIn = False Then
                Response.Redirect("properties")
            End If

            Dim OverviewDR() As DataRow
            Dim ProjectOverviewSQL As String = ""
            ProjectOverviewSQL &= "select d.Item_Tab, d.Item_group, d.Item_Name, v.Item_value from ipm_project_field_desc d "
            ProjectOverviewSQL &= "Join ipm_project_field_value v on d.Item_Id = v.Item_Id and v.ProjectId = " & ProjectID & " "
            ProjectOverviewSQL &= "where d.Active = 1 and d.Viewable = 1 Order By Item_Tab, Item_Group, Item_Name "
            Dim ProjectOverviewDT As New DataTable
            ProjectOverviewDT = DBFunctions.GetDataTable(ProjectOverviewSQL)
            If ProjectOverviewDT.Rows.Count > 0 Then
                OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Web Site Content' AND Item_Group = '' AND Item_Name = '*Short Name'")
                If OverviewDR.Length > 0 Then
                    ProjectTitle1_Literal.Text = OverviewDR(0)("Item_value").ToString.Trim
                    ProjectTitle2_Literal.Text = OverviewDR(0)("Item_value").ToString.Trim
                    ProjectTitle = OverviewDR(0)("Item_value").ToString.Trim
                End If
            End If

            OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Sustainability' AND Item_Group = 'Features' AND Item_Name = '*Green Features'")
            If OverviewDR.Length > 0 Then
                If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                    HasSustainability = True
                End If
            End If


            Dim ProjectLatXLongSQL As String = ""
            ProjectLatXLongSQL &= "select v.Item_Value from IPM_PROJECT_FIELD_DESC d "
            ProjectLatXLongSQL &= "join IPM_PROJECT_FIELD_VALUE v on d.Item_Tag = 'IDAM_LATLONG' and d.Item_ID = v.Item_Id and d.active = 1  and v.Item_value <> '' "
            ProjectLatXLongSQL &= "where projectid = " & ProjectID & ""
            Dim ProjectLatXLongDT As New DataTable
            ProjectLatXLongDT = DBFunctions.GetDataTable(ProjectLatXLongSQL)
            If ProjectLatXLongDT.Rows.Count > 0 Then
                LatxLong = ProjectLatXLongDT.Rows(0).Item("Item_Value").ToString.ToString
            End If

            Dim PublicArtAssetsSQL As String = ""
            PublicArtAssetsSQL &= "select a.Asset_id,  v2.Item_value ArtDesc from ipm_asset a "
            PublicArtAssetsSQL &= "join ipm_asset_field_value v on a.Asset_id = v.Asset_id and v.Item_id = (select item_id from ipm_asset_field_desc where Item_Tab = 'Info' and Item_Group = 'Website Controls' and Item_Name = '*Art Image' and Active = 1 And Viewable = 1 ) AND v.Item_value = '1' "
            PublicArtAssetsSQL &= "left join ipm_asset_field_value v2 on a.Asset_id = v2.Asset_id and v2.Item_id = (select item_id from ipm_asset_field_desc where Item_Tab = 'Info' and Item_Group = 'Website Controls' and Item_Name = '*Art Descritption' and Active = 1 And Viewable = 1 ) "
            PublicArtAssetsSQL &= "where ProjectID = " & ProjectID & " "
            Dim PublicArtResourcesDT As New DataTable
            PublicArtResourcesDT = DBFunctions.GetDataTable(PublicArtAssetsSQL)
            Dim PublicArtResourcesOUT As String = ""
            Dim PublicArtImageOUT As String = ""
            If PublicArtResourcesDT.Rows.Count > 0 Then
                HasPublicArt = True
            End If

            Dim FloorPlansAssetsSQL As String = ""
            FloorPlansAssetsSQL &= "select Asset_ID, a.Name, a.Description from ipm_asset a "
            FloorPlansAssetsSQL &= "join ipm_asset_category c on a.projectid = c.projectid and c.Category_ID = a.Category_ID and c.available = 'Y' and c.Active = 1 and c.Name = 'Web Site Floor Plans' "
            FloorPlansAssetsSQL &= "where a.available = 'Y' and a.Active = 1  and a.projectid  = " & ProjectID & " "
            Dim FloorPlansResourcesDT As New DataTable
            FloorPlansResourcesDT = DBFunctions.GetDataTable(FloorPlansAssetsSQL)
            Dim FloorPlansResourcesOUT As String = ""
            If FloorPlansResourcesDT.Rows.Count > 0 Then
                HasFloorPlans = True
            End If

            Dim TestFitsAssetsSQL As String = ""
            TestFitsAssetsSQL &= "select a.Asset_ID, a.Name, a.Description from ipm_asset a "
            TestFitsAssetsSQL &= "join ipm_asset_category c on a.projectid = c.projectid and c.Category_ID = a.Category_ID and c.available = 'Y' and c.Active = 1 and c.Name = 'Web Site Test Fits' "
            TestFitsAssetsSQL &= "left join ipm_asset_field_value v on a.asset_id = v.asset_id and v.item_id = (select Item_ID from ipm_asset_field_desc Where Item_Tag = 'IDAM_ASSETSORT') "
            TestFitsAssetsSQL &= "where a.available = 'Y' and a.Active = 1 and a.projectid  = " & ProjectID & " "
            TestFitsAssetsSQL &= "Order By "
            TestFitsAssetsSQL &= "Case isnumeric( isnull(nullif(v.Item_Value,''),'0')) "
            TestFitsAssetsSQL &= "When 1 Then cast(isnull(nullif(v.Item_Value,''),'0') as integer) Else 0 End desc "
            Dim TestFitsResourcesDT As New DataTable
            TestFitsResourcesDT = DBFunctions.GetDataTable(TestFitsAssetsSQL)
            Dim TestFitsResourcesOUT As String = ""
            If TestFitsResourcesDT.Rows.Count > 0 Then
                HasTestFits = True
            End If

            Dim PublicResourcesSQL As String = ""
            PublicResourcesSQL &= "select a.Asset_ID, a.Name, a.Description "
            PublicResourcesSQL &= "from ipm_asset a "
            PublicResourcesSQL &= "join ipm_asset_category c on a.projectid = c.projectid and c.Category_ID = a.Category_ID and c.available = 'Y' and c.Active = 1 and c.Name = 'Public Resources' "
            PublicResourcesSQL &= "left join ipm_asset_field_value v on a.asset_id = v.asset_id and v.item_id = (select Item_ID from ipm_asset_field_desc Where Item_Tag = 'IDAM_ASSETSORT') Where a.available = 'Y' and a.Active = 1 and a.projectid  = " & ProjectID & " "
            PublicResourcesSQL &= "Order By "
            PublicResourcesSQL &= "Case isnumeric( isnull(nullif(v.Item_Value,''),'0')) "
            PublicResourcesSQL &= "When 1 Then cast(isnull(nullif(v.Item_Value,''),'0') as integer) Else 0 End desc "
            Dim PublicResourcesDT As New DataTable
            PublicResourcesDT = DBFunctions.GetDataTable(PublicResourcesSQL)
            Dim PublicResourcesOUT As String = ""
            If PublicResourcesDT.Rows.Count > 0 Then
                HasPublicResources = True
            End If

            Dim CheckVideoLinkSQL As String = ""
            CheckVideoLinkSQL &= "select Item_value video from ipm_project_field_value where Item_ID = (select Item_ID from ipm_project_field_desc "
            CheckVideoLinkSQL &= "where Item_Tab = '*Web Site Content' and Item_Group = 'Display Information' and Item_name = '*Video Link' and active = 1) "
            CheckVideoLinkSQL &= "and  Item_value <> '' and projectid = " & ProjectID & " "
            Dim CheckVideoLinkDT As New DataTable
            CheckVideoLinkDT = DBFunctions.GetDataTable(CheckVideoLinkSQL)
            If CheckVideoLinkDT.Rows.Count > 0 Then
                HasVideos = True
            End If


            Dim RarAssetID As String = ""
            Dim RarUpdateDate As Date

            Dim RarUnarchivedAssetID As String = ""
            Dim RarUnarchivedDate As Date

            Dim CheckPanoSQL As String = ""
            CheckPanoSQL &= "select A.Asset_ID, A.Name, Upload_Date, Update_Date, Media_Type, Version_ID "
            CheckPanoSQL &= "from IPM_ASSET A "
            CheckPanoSQL &= "join IPM_ASSET_CATEGORY AC on AC.CATEGORY_ID = A.Category_ID AND AC.NAME = '360' "
            CheckPanoSQL &= "where A.ProjectID = " & ProjectID & " "
            Dim CheckPanoDT As New DataTable
            CheckPanoDT = DBFunctions.GetDataTable(CheckPanoSQL)
            If CheckPanoDT.Rows.Count > 0 Then
                HasPano = True

                Dim PanoIsNew As Boolean = False

                Dim SourceFolder As String = Server.MapPath("~") & "pan\panos\temp\"
                Dim DestinationFolder As String = Server.MapPath("~") & "pan\panos\" & ProjectID & "\"
                Dim RarArchiveName As String = SourceFolder & ProjectID & ".rar"


              

                RarAssetID = CheckPanoDT.Rows(0).Item("Asset_ID").ToString
                RarUpdateDate = CheckPanoDT.Rows(0).Item("Update_Date")


                Dim CheckIfWasUnarchivedSQL As String = "select * from z_360Archives where ProjectID = " & ProjectID & " "
                Dim CheckIfWasUnarchivedDT As New DataTable
                CheckIfWasUnarchivedDT = DBFunctions.GetDataTable(CheckIfWasUnarchivedSQL)
                If CheckIfWasUnarchivedDT.Rows.Count > 0 Then

                    RarUnarchivedAssetID = CheckIfWasUnarchivedDT.Rows(0).Item("Asset_ID").ToString
                    RarUnarchivedDate = CheckIfWasUnarchivedDT.Rows(0).Item("UnArchivedDate")


                    If RarAssetID <> RarUnarchivedAssetID Then
                        If Directory.Exists(DestinationFolder) Then
                            Directory.Delete(DestinationFolder, True)
                        End If
                        PanoIsNew = True
                    End If

                    If RarUpdateDate > RarUnarchivedDate Then
                        If Directory.Exists(DestinationFolder) Then
                            Directory.Delete(DestinationFolder, True)
                        End If
                        PanoIsNew = True
                    End If
                    File.Delete(RarArchiveName)

                Else
                    PanoIsNew = True
                End If

               


                If PanoIsNew = True Then

                    If Not Directory.Exists(SourceFolder) Then
                        Try
                            Directory.CreateDirectory(SourceFolder)
                        Catch ex As Exception
                        End Try
                    End If
                    If Not Directory.Exists(DestinationFolder) Then
                        Try
                            Directory.CreateDirectory(DestinationFolder)
                        Catch ex As Exception
                        End Try
                    End If


                    Dim TempDownloadRar As String = ""
                    TempDownloadRar = Session("WSDownloadAsset").ToString.Trim & "dtype=assetdownload&assetid=" & RarAssetID & "&size=0"



                    Try
                        Dim SourceStream As Stream
                        Dim request As WebRequest = WebRequest.Create(TempDownloadRar)
                        request.Credentials = CredentialCache.DefaultCredentials
                        Dim response As HttpWebResponse = CType(request.GetResponse(), HttpWebResponse)
                        SourceStream = response.GetResponseStream
                        Dim Buffer(4096) As Byte
                        Dim BlockSize As Integer
                        Dim writeStream As FileStream = New FileStream(RarArchiveName, FileMode.Create, FileAccess.Write)
                        Do
                            BlockSize = SourceStream.Read(Buffer, 0, 4096)
                            If BlockSize > 0 Then writeStream.Write(Buffer, 0, BlockSize)
                        Loop While BlockSize > 0
                        writeStream.Flush()
                        writeStream.Close()
                        SourceStream.Close()
                        response.Close()
                    Catch ex As Exception
                        Response.Redirect("/404", True)
                    End Try


                    Dim FI As New FileInfo(RarArchiveName)
                    Dim archive = ArchiveFactory.Open(FI)
                    For Each entry In archive.Entries
                        If Not entry.IsDirectory Then
                            entry.WriteToDirectory(DestinationFolder, ExtractOptions.ExtractFullPath Or ExtractOptions.Overwrite)
                        End If
                    Next

                    Dim DeleteOldRarSQL As String = "delete from z_360Archives where ProjectID = " & ProjectID & " "
                    DBFunctions.NonQuerySql(DeleteOldRarSQL)

                    Dim LogUnarchivedRarSQL As String = ""
                    LogUnarchivedRarSQL &= "insert into z_360Archives "
                    LogUnarchivedRarSQL &= "select " & ProjectID & ", " & RarAssetID & ", GetDate() "
                    DBFunctions.NonQuerySql(LogUnarchivedRarSQL)



                End If 'if Pano is new

            End If 'If has Pano


            Dim SecuredResourcesDT As New DataTable
            Dim SecuredResourcesSQL As String = ""
            Dim SecuredResourcesOUT As String = ""
            If IDAMFunctions.IsLoggedIn = True Then
                SecuredResourcesSQL &= "select a.Asset_ID, a.Name, a.Description "
                SecuredResourcesSQL &= "from ipm_asset a "
                SecuredResourcesSQL &= "join ipm_asset_category c on a.projectid = c.projectid and c.Category_ID = a.Category_ID and c.available = 'Y' and c.Active = 1 and c.Name = 'Private Resources' "
                SecuredResourcesSQL &= "left join ipm_asset_field_value v on a.asset_id = v.asset_id and v.item_id = (select Item_ID from ipm_asset_field_desc Where Item_Tag = 'IDAM_ASSETSORT') Where a.available = 'Y' and a.Active = 1 and a.projectid  = " & ProjectID & " "
                SecuredResourcesSQL &= "Order By "
                SecuredResourcesSQL &= "Case isnumeric( isnull(nullif(v.Item_Value,''),'0')) "
                SecuredResourcesSQL &= "When 1 Then cast(isnull(nullif(v.Item_Value,''),'0') as integer) Else 0 End desc "
                SecuredResourcesDT = DBFunctions.GetDataTable(SecuredResourcesSQL)
                If SecuredResourcesDT.Rows.Count > 0 Then
                    HasSecuredResources = True
                End If
            End If


            Select Case SelectedTab.ToLower
                Case "gallery"
                    Dim GallerySQL As String = ""
                    GallerySQL &= "select * from ( "
                    GallerySQL &= "select a.asset_id, Crop = Case WHEN WPixel / HPixel > 2.4 THEN '2' WHEN WPixel / HPixel < 1.2 THEN '2' ELSE '1' END,  "
                    GallerySQL &= "case isnumeric( isnull(nullif(v.Item_Value,''),'0')) when 1 Then cast(isnull(nullif(v.Item_Value,''),'0') as integer) Else 0 End  Sort  "
                    GallerySQL &= "from ipm_asset a  "
                    GallerySQL &= "join ipm_project_asset pa on a.projectid = pa.Project_id and a.asset_id = pa.asset_id and pa.available = 'Y' and pa.Type = 1   "
                    GallerySQL &= "left join ipm_asset_field_value v on a.asset_id = v.asset_id and v.item_id = (select Item_ID from ipm_asset_field_desc Where Item_Tag = 'IDAM_ASSETSORT')   "
                    GallerySQL &= "where a.available = 'Y' and a.Active = '1'  and a.projectid  =  " & ProjectID & " "
                    GallerySQL &= "union  "
                    GallerySQL &= "select a.asset_id, Crop = Case WHEN WPixel / HPixel > 2.4 THEN '2' WHEN WPixel / HPixel < 1.2 THEN '2' ELSE '1' END,  "
                    GallerySQL &= "case isnumeric( isnull(nullif(v.Item_Value,''),'0')) When 1 Then cast(isnull(nullif(v.Item_Value,''),'0') as integer) Else 0 End  Sort  "
                    GallerySQL &= "from ipm_asset a  "
                    GallerySQL &= "join ipm_asset_category c on a.projectid = c.projectid and c.Category_ID = a.Category_ID and c.available = 'Y' and c.Active = '1' and c.Name = 'Web Site Gallery'   "
                    GallerySQL &= "left join ipm_asset_field_value v on a.asset_id = v.asset_id and v.item_id = (select Item_ID from ipm_asset_field_desc Where Item_Tag = 'IDAM_ASSETSORT')   "
                    GallerySQL &= "where a.available = 'Y' and a.Active = 1 and (WPixel >= 1 OR HPixel >= 1) "
                    GallerySQL &= "and a.projectid = " & ProjectID & " "
                    GallerySQL &= "union  "
                    GallerySQL &= "select a.asset_id, Crop = Case WHEN WPixel / HPixel > 2.4 THEN '2' WHEN WPixel / HPixel < 1.2 THEN '2' ELSE '1' END,  "
                    GallerySQL &= "case isnumeric( isnull(nullif(v.Item_Value,''),'0')) When 1 Then cast(isnull(nullif(v.Item_Value,''),'0') as integer) Else 0 End  Sort  "
                    GallerySQL &= "from ipm_asset a  "
                    GallerySQL &= "join ipm_asset_category c on a.projectid = c.projectid and c.Category_ID = a.Category_ID and c.available = 'Y' and c.Active = '1' and c.Name = 'Private Gallery'   "
                    GallerySQL &= "left join ipm_asset_field_value v on a.asset_id = v.asset_id and v.item_id = (select Item_ID from ipm_asset_field_desc Where Item_Tag = 'IDAM_ASSETSORT')   "
                    GallerySQL &= "where a.available = 'Y' and a.Active = 1 and (WPixel >= 1 OR HPixel >= 1) "
                    GallerySQL &= "and a.projectid  =  " & ProjectID & " "
                    GallerySQL &= ") t Order By Sort desc  "
                    Dim GalleryDT As New DataTable
                    GalleryDT = DBFunctions.GetDataTable(GallerySQL)
                    Dim GalleryOUT As String = ""
                    If GalleryDT.Rows.Count > 0 Then
                        GalleryOUT &= "<ul class=""bxslider"">"
                        For R As Integer = 0 To GalleryDT.Rows.Count - 1
                            GalleryOUT &= "<li>"
                            GalleryOUT &= "<img src=""dynamic/image/always/asset/best/980x508/92/e2e2e2/Center/" & GalleryDT.Rows(R).Item("asset_id").ToString.Trim & ".jpg" & """" & " alt=""""/>"
                            GalleryOUT &= "</li>"
                        Next
                        GalleryOUT &= "</ul>"
                        Gallery_Literal.Text = GalleryOUT
                    End If

                    Dim GalleryTextSQL As String = ""
                    GalleryTextSQL &= "select p.Name, v1.Item_value Headline, DescriptionMedium LongDescription from ipm_project p "
                    GalleryTextSQL &= "left join ipm_project_field_value v1 on v1.projectid = p.projectid and v1.Item_id = 21610623 "
                    GalleryTextSQL &= "left join ipm_project_field_value v2 on v2.projectid = p.projectid and v2.Item_id = 21610595 "
                    GalleryTextSQL &= ""
                    GalleryTextSQL &= "where p.projectid = " & ProjectID & ""
                    Dim GalleryTextDT As New DataTable
                    GalleryTextDT = DBFunctions.GetDataTable(GalleryTextSQL)
                    If GalleryTextDT.Rows.Count > 0 Then
                        GalleyHeadline_Literal.Text = formatfunctionssimple.AutoFormatText(GalleryTextDT.Rows(0).Item("Headline").ToString.Trim)
                        GalleryDescription_Literal.Text = formatfunctions.AutoFormatText(GalleryTextDT.Rows(0).Item("LongDescription").ToString.Trim)
                    End If

                    Dim GalleryNewsSQL As String = ""
                    GalleryNewsSQL &= "select n.News_Id, Headline from ipm_news_related_projects r "
                    GalleryNewsSQL &= "join ipm_news n on r.News_Id = n.News_Id Where r.ProjectID = " & ProjectID & ""
                    Dim GalleryNewsDT As New DataTable
                    GalleryNewsDT = DBFunctions.GetDataTable(GalleryNewsSQL)
                    Dim GalleryNewsOUT As String = ""
                    If GalleryNewsDT.Rows.Count > 0 Then
                        For R As Integer = 0 To GalleryNewsDT.Rows.Count - 1
                            GalleryNewsOUT &= "<a href=""article-" & GalleryNewsDT.Rows(R).Item("News_Id").ToString.Trim & """>" & formatfunctionssimple.AutoFormatText(GalleryNewsDT.Rows(R).Item("Headline").ToString.Trim) & "</a>"
                        Next
                        GalleryRecentNews_Literal.Text = "<div id=""headlines"" class=""bar80 infobar""><div class=""grid_4""><h1>Recent News</h1></div><div class=""grid_8""><div id=""headline-rotate"">" & GalleryNewsOUT & "<span class=""clear""></span></div></div></div>"
                    End If
                Case "overview"
                    'TITLE
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Web Site Content' AND Item_Group = '' AND Item_Name = '*Short Name'")
                    If OverviewDR.Length > 0 Then
                        ProjectTitle2_Literal.Text = formatfunctionssimple.AutoFormatText(OverviewDR(0)("Item_value").ToString.Trim)
                    End If

                    Dim ProjectOtherDetailsSQL As String = ""
                    ProjectOtherDetailsSQL = "Select * from ipm_project where projectid = " & ProjectID & " "
                    Dim ProjectOtherDetailsDT As New DataTable
                    ProjectOtherDetailsDT = DBFunctions.GetDataTable(ProjectOtherDetailsSQL)
                    'Adress
                    Dim ProjectLeftOUT As String = ""
                    ProjectLeftOUT &= "<dt>Address</dt>" & vbNewLine
                    ProjectLeftOUT &= "<dd>" & formatfunctionssimple.AutoFormatText(ProjectOtherDetailsDT.Rows(0).Item("Address").ToString.Trim) & "<br/> " & ProjectOtherDetailsDT.Rows(0).Item("City").ToString.Trim & ", " & ProjectOtherDetailsDT.Rows(0).Item("State_id").ToString.Trim & " " & ProjectOtherDetailsDT.Rows(0).Item("Zip").ToString.Trim & "</dd>" & vbNewLine
                    'Architect
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Consultants' AND Item_Group = 'Design' AND Item_Name = '*Design Architect'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Project Architect</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & formatfunctionssimple.AutoFormatText(OverviewDR(0)("Item_value").ToString.Trim) & "</dd>" & vbNewLine
                        End If
                    End If
                    'General Contractor
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Consultants' AND Item_Group = 'Construction' AND Item_Name = '*General Contractor'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>General Contractor</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & formatfunctionssimple.AutoFormatText(OverviewDR(0)("Item_value").ToString.Trim) & "</dd>" & vbNewLine
                        End If
                    End If
                    'Building Size
                    ProjectLeftOUT &= "<dt>Building Size</dt>" & vbNewLine
                    ProjectLeftOUT &= "<dd>"
                    'Total
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Size-Type' AND Item_Group = 'Gross SF' AND Item_Name = '*Total SF'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<span class=""sf"">Total <span class=""sf2"">" & formatfunctionssimple.AutoFormatText(OverviewDR(0)("Item_value").ToString.Trim) & " SF</span></span><br/>" & vbNewLine
                        End If
                    End If
                    'Hotel
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Size-Type' AND Item_Group = 'Gross SF' AND Item_Name = '*Hotel Size'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<span class=""sf"">Hotel <span class=""sf2"">" & formatfunctionssimple.AutoFormatText(OverviewDR(0)("Item_value").ToString.Trim) & " SF</span></span><br/>" & vbNewLine
                        End If
                    End If
                    'Office
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Size-Type' AND Item_Group = 'Gross SF' AND Item_Name = '*Office Size'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<span class=""sf"">Office <span class=""sf2"">" & OverviewDR(0)("Item_value").ToString.Trim & " SF</span></span><br/>" & vbNewLine
                        End If
                    End If
                    'Residential
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Size-Type' AND Item_Group = 'Gross SF' AND Item_Name = '*Residential Size'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<span class=""sf"">Residential <span class=""sf2"">" & OverviewDR(0)("Item_value").ToString.Trim & " SF</span></span><br/>" & vbNewLine
                        End If
                    End If
                    'Retail
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Size-Type' AND Item_Group = 'Gross SF' AND Item_Name = '*Retail Size'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<span class=""sf"">Retail <span class=""sf2"">" & OverviewDR(0)("Item_value").ToString.Trim & " SF</span></span><br/>" & vbNewLine
                        End If
                    End If
                    ProjectLeftOUT &= "</dd>"
                    'Type
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Specs' AND Item_Group = 'Residential' AND Item_Name = 'Residential Type'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Type</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & OverviewDR(0)("Item_value").ToString.Trim & "</dd>" & vbNewLine
                        End If
                    End If
                    'Number of Residences
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Specs' AND Item_Group = 'Residential' AND Item_Name = 'Residential Units'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Number of Residences</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & OverviewDR(0)("Item_value").ToString.Trim & "</dd>" & vbNewLine
                        End If
                    End If
                    'Home Features
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Specs' AND Item_Group = 'Residential' AND Item_Name = 'Residential Home Features'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Home Features</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & formatfunctionssimple.AutoFormatText(OverviewDR(0)("Item_value").ToString.Trim) & "</dd>" & vbNewLine
                        End If
                    End If
                    'Community Features
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Specs' AND Item_Group = 'Residential' AND Item_Name = 'Residential Community Features'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Community Features</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & formatfunctionssimple.AutoFormatText(OverviewDR(0)("Item_value").ToString.Trim) & "</dd>" & vbNewLine
                        End If
                    End If
                    'Quality Tier
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Specs' AND Item_Group = 'Hotel' AND Item_Name = '*Quality Tier'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Quality Tier</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & formatfunctionssimple.AutoFormatText(OverviewDR(0)("Item_value").ToString.Trim) & "</dd>" & vbNewLine
                        End If
                    End If
                    'Guest Rooms
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Specs' AND Item_Group = 'Hotel' AND Item_Name = '*Hotel Keys'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Guest Rooms</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & OverviewDR(0)("Item_value").ToString.Trim & "</dd>" & vbNewLine
                        End If
                    End If
                    'Meeting Space
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Specs' AND Item_Group = 'Hotel' AND Item_Name = '*Meeting Rooms'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Meeting Space</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & OverviewDR(0)("Item_value").ToString.Trim & "</dd>" & vbNewLine
                        End If
                    End If
                    'Number of Stories
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Specs' AND Item_Group = 'Office' AND Item_Name = '*Number of Stories'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Number of Stories</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & OverviewDR(0)("Item_value").ToString.Trim & "</dd>" & vbNewLine
                        End If
                    End If
                    'Parking Ratio
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Specs' AND Item_Group = 'Office' AND Item_Name = '*Parking Ratio'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Parking Ratio</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & OverviewDR(0)("Item_value").ToString.Trim & "</dd>" & vbNewLine
                        End If
                    End If
                    'Floorplates
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Specs' AND Item_Group = 'Office' AND Item_Name = '*Floorplate Size'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Floorplates</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & OverviewDR(0)("Item_value").ToString.Trim & "</dd>" & vbNewLine
                        End If
                    End If
                    'Finished Ceiling Height
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Specs' AND Item_Group = 'Office' AND Item_Name = '*Finished Ceiling Height'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Finished Ceiling Height</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & OverviewDR(0)("Item_value").ToString.Trim & "</dd>" & vbNewLine
                        End If
                    End If
                    'Column Spacing
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Specs' AND Item_Group = 'Office' AND Item_Name = '*Column Spacing'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Column Spacing</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & OverviewDR(0)("Item_value").ToString.Trim & "</dd>" & vbNewLine
                        End If
                    End If
                    'HVAC System
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Specs' AND Item_Group = 'Office' AND Item_Name = '*HVAC System'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>HVAC System</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & formatfunctions.AutoFormatText(OverviewDR(0)("Item_value").ToString.Trim) & "</dd>" & vbNewLine
                        End If
                    End If
                    'BUILDING AMENITIES
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Specs' AND Item_Group = 'Office' AND Item_Name = '*Building Amenities'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>BUILDING AMENITIES</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & formatfunctions.AutoFormatText(OverviewDR(0)("Item_value").ToString.Trim) & "</dd>" & vbNewLine
                        End If
                    End If
                    'Land Area
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Size-Type' AND Item_Group = 'Land' AND Item_Name = '*Land Area'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Land Area</dt>" & vbNewLine
                            ProjectLeftOUT &= "<dd> " & OverviewDR(0)("Item_value").ToString.Trim & "</dd>" & vbNewLine
                        End If
                    End If
                    'Web Site
                    OverviewDR = ProjectOverviewDT.Select("Item_Tab = '*Web Site Content' AND Item_Group = '' AND Item_Name = '*Web Site URL'")
                    If OverviewDR.Length > 0 Then
                        If OverviewDR(0)("Item_value").ToString.Trim <> "" Then
                            ProjectLeftOUT &= "<dt>Web Site</dt>" & vbNewLine

                            Dim WebisteString As String = OverviewDR(0)("Item_value").ToString.Trim

                            If WebisteString.Substring(0, 8) <> "https://" Or WebisteString.Substring(0, 7) <> "http://" Then
                                WebisteString = "http://" & WebisteString
                            End If

                            ProjectLeftOUT &= "<dd><a target=""_blank"" href=""" & WebisteString & """>" & WebisteString.Replace("http://", "").Replace("https://", "") & "</a></dd>" & vbNewLine
                        End If
                    End If
                    ProjectLeft_Literal.Text = ProjectLeftOUT

                    'Description
                    ProjectDescription_Literal.Text &= "<h6>Overview</h6>" & vbNewLine
                    ProjectDescription_Literal.Text &= "<p>" & formatfunctions.AutoFormatText(ProjectOtherDetailsDT.Rows(0).Item("DescriptionMedium").ToString.Trim) & "</p>" & vbNewLine

                    'Leasing Agents
                    Dim LeasingAgentsSQL As String = ""
                    LeasingAgentsSQL &= "select d1.Item_Tag, u.FirstName, u.LastName, u.Email, u.Phone "
                    LeasingAgentsSQL &= "from ipm_project_contact c "
                    LeasingAgentsSQL &= "join ipm_user_field_value v1 on v1.User_ID = c.UserID "
                    LeasingAgentsSQL &= "join ipm_user_field_desc d1 on v1.Item_ID = d1.Item_ID and d1.Item_Tag in ('IDAM_OLEASE', 'IDAM_RLEASE', 'IDAM_ALEASE','IDAM_CSALES', 'IDAM_PMAN') and v1.Item_Value = '1' "
                    LeasingAgentsSQL &= "join ipm_user u on  v1.User_ID = u.UserID and u.Active = 'Y' where c.ProjectID = " & ProjectID & " "
                    Dim NoFilterLeasingAgentsDT As New DataTable
                    NoFilterLeasingAgentsDT = DBFunctions.GetDataTable(LeasingAgentsSQL)

                    Dim LeasingAgentsDT As DataTable

                    'Apartment Leasing
                    NoFilterLeasingAgentsDT.DefaultView.RowFilter = "Item_Tag = 'IDAM_ALEASE'"
                    LeasingAgentsDT = New DataTable
                    LeasingAgentsDT = NoFilterLeasingAgentsDT.DefaultView.ToTable
                    If LeasingAgentsDT.Rows.Count > 0 Then
                        LeasingAgentsOUT &= "<dt>Apartment Leasing</dt>" & vbNewLine
                        LeasingAgentsOUT &= "<dd>" & vbNewLine
                        For R As Integer = 0 To LeasingAgentsDT.Rows.Count - 1
                            LeasingAgentsOUT &= formatfunctionssimple.AutoFormatText(LeasingAgentsDT.Rows(R).Item("FirstName").ToString.Trim & " " & LeasingAgentsDT.Rows(R).Item("LastName").ToString.Trim & "<br/>" & LeasingAgentsDT.Rows(R).Item("Phone").ToString.Trim & "<br/>") & vbNewLine
                            LeasingAgentsOUT &= "<a href=""mailto:" & LeasingAgentsDT.Rows(R).Item("Email").ToString.Trim & """>" & LeasingAgentsDT.Rows(R).Item("Email").ToString.Trim & "</a><br/><br/>" & vbNewLine
                        Next
                        LeasingAgentsOUT &= "</dd>" & vbNewLine
                    End If
                    'Condo Sales
                    NoFilterLeasingAgentsDT.DefaultView.RowFilter = "Item_Tag = 'IDAM_CSALES'"
                    LeasingAgentsDT = New DataTable
                    LeasingAgentsDT = NoFilterLeasingAgentsDT.DefaultView.ToTable
                    If LeasingAgentsDT.Rows.Count > 0 Then
                        LeasingAgentsOUT &= "<dt>Condo Sales</dt>" & vbNewLine
                        LeasingAgentsOUT &= "<dd>" & vbNewLine
                        For R As Integer = 0 To LeasingAgentsDT.Rows.Count - 1
                            LeasingAgentsOUT &= formatfunctionssimple.AutoFormatText(LeasingAgentsDT.Rows(R).Item("FirstName").ToString.Trim & " " & LeasingAgentsDT.Rows(R).Item("LastName").ToString.Trim & "<br/>" & LeasingAgentsDT.Rows(R).Item("Phone").ToString.Trim & "<br/>") & vbNewLine
                            LeasingAgentsOUT &= "<a href=""mailto:" & LeasingAgentsDT.Rows(R).Item("Email").ToString.Trim & """>" & LeasingAgentsDT.Rows(R).Item("Email").ToString.Trim & "</a><br/><br/>" & vbNewLine
                        Next
                        LeasingAgentsOUT &= "</dd>" & vbNewLine
                    End If
                    'Office Leasing
                    NoFilterLeasingAgentsDT.DefaultView.RowFilter = "Item_Tag = 'IDAM_OLEASE'"
                    LeasingAgentsDT = New DataTable
                    LeasingAgentsDT = NoFilterLeasingAgentsDT.DefaultView.ToTable
                    If LeasingAgentsDT.Rows.Count > 0 Then
                        LeasingAgentsOUT &= "<dt>Office Leasing</dt>" & vbNewLine
                        LeasingAgentsOUT &= "<dd>" & vbNewLine
                        For R As Integer = 0 To LeasingAgentsDT.Rows.Count - 1
                            LeasingAgentsOUT &= formatfunctionssimple.AutoFormatText(LeasingAgentsDT.Rows(R).Item("FirstName").ToString.Trim & " " & LeasingAgentsDT.Rows(R).Item("LastName").ToString.Trim & "<br/>" & LeasingAgentsDT.Rows(R).Item("Phone").ToString.Trim & "<br/>") & vbNewLine
                            LeasingAgentsOUT &= "<a href=""mailto:" & LeasingAgentsDT.Rows(R).Item("Email").ToString.Trim & """>" & LeasingAgentsDT.Rows(R).Item("Email").ToString.Trim & "</a><br/><br/>" & vbNewLine
                        Next
                        LeasingAgentsOUT &= "</dd>" & vbNewLine
                    End If
                    'Retail Leasing
                    NoFilterLeasingAgentsDT.DefaultView.RowFilter = "Item_Tag = 'IDAM_RLEASE'"
                    LeasingAgentsDT = New DataTable
                    LeasingAgentsDT = NoFilterLeasingAgentsDT.DefaultView.ToTable
                    If LeasingAgentsDT.Rows.Count > 0 Then
                        LeasingAgentsOUT &= "<dt>Retail Leasing</dt>" & vbNewLine
                        LeasingAgentsOUT &= "<dd>" & vbNewLine
                        For R As Integer = 0 To LeasingAgentsDT.Rows.Count - 1
                            LeasingAgentsOUT &= formatfunctionssimple.AutoFormatText(LeasingAgentsDT.Rows(R).Item("FirstName").ToString.Trim & " " & LeasingAgentsDT.Rows(R).Item("LastName").ToString.Trim & "<br/>" & LeasingAgentsDT.Rows(R).Item("Phone").ToString.Trim & "<br/>") & vbNewLine
                            LeasingAgentsOUT &= "<a href=""mailto:" & LeasingAgentsDT.Rows(R).Item("Email").ToString.Trim & """>" & LeasingAgentsDT.Rows(R).Item("Email").ToString.Trim & "</a><br/><br/>" & vbNewLine
                        Next
                        LeasingAgentsOUT &= "</dd>" & vbNewLine
                    End If
                    'Property Manager
                    NoFilterLeasingAgentsDT.DefaultView.RowFilter = "Item_Tag = 'IDAM_PMAN'"
                    LeasingAgentsDT = New DataTable
                    LeasingAgentsDT = NoFilterLeasingAgentsDT.DefaultView.ToTable
                    If LeasingAgentsDT.Rows.Count > 0 Then
                        LeasingAgentsOUT &= "<dl>" & vbNewLine
                        LeasingAgentsOUT &= "<dt>Property Manager</dt>" & vbNewLine
                        LeasingAgentsOUT &= "<dd>" & vbNewLine
                        For R As Integer = 0 To LeasingAgentsDT.Rows.Count - 1
                            LeasingAgentsOUT &= formatfunctionssimple.AutoFormatText(LeasingAgentsDT.Rows(R).Item("FirstName").ToString.Trim & " " & LeasingAgentsDT.Rows(R).Item("LastName").ToString.Trim & "<br/>" & LeasingAgentsDT.Rows(R).Item("Phone").ToString.Trim & "<br/>") & vbNewLine
                            LeasingAgentsOUT &= "<a href=""mailto:" & LeasingAgentsDT.Rows(R).Item("Email").ToString.Trim & """>" & LeasingAgentsDT.Rows(R).Item("Email").ToString.Trim & "</a><br/><br/>" & vbNewLine
                        Next
                        LeasingAgentsOUT &= "</dd>" & vbNewLine
                        LeasingAgentsOUT &= "</dl>" & vbNewLine
                    End If
                    LeasingAgents_Literal.Text = LeasingAgentsOUT

                    'Awards
                    Dim AwardsSQL As String = ""
                    AwardsSQL &= "select t.type_value awardType, a.Headline Name, PublicationTitle Catagory,  year(Post_Date) year "
                    AwardsSQL &= "from IPM_AWARDS_RELATED_PROJECTS p "
                    AwardsSQL &= "join ipm_awards a on p.awards_id = a.awards_id and Post_Date <= getdate() and Pull_Date >= getdate() and Show = 1 "
                    AwardsSQL &= "join ipm_awards_type t on a.Type = t.Type_id "
                    AwardsSQL &= "where projectid = " & ProjectID & " "
                    AwardsSQL &= "Order By Post_Date desc "
                    Dim AwardsDT As New DataTable
                    AwardsDT = DBFunctions.GetDataTable(AwardsSQL)

                    If AwardsDT.Rows.Count > 0 Then
                        AwardsOUT &= "<dt>Awards</dt>"
                        AwardsOUT &= "<dd class=""propawards"">"
                        For R As Integer = 0 To AwardsDT.Rows.Count - 1
                            AwardsOUT &= "<p>" & AwardsDT.Rows(R).Item("year").ToString.ToString & " — " & formatfunctionssimple.AutoFormatText(AwardsDT.Rows(R).Item("Name").ToString.Trim) & "<br />" ' 
                            AwardsOUT &= formatfunctionssimple.AutoFormatText(AwardsDT.Rows(R).Item("Catagory").ToString.Trim) & "</p>" '& "<br><br>"
                        Next
                        AwardsOUT &= "</dd>"
                        ProjectAwards_Literal.Text = AwardsOUT
                    End If

                    'Related Properties
                    Dim RelatedProjectsSQL As String = ""
                    RelatedProjectsSQL &= "Select p.ProjectID, Name, F1.Item_Value as SeoURL from ipm_project_related r "
                    RelatedProjectsSQL &= "join ipm_project p on r.Ref_Id = p.ProjectId "
                    RelatedProjectsSQL &= "Left join ipm_project_field_value F1 on p.ProjectID = F1.ProjectID and F1.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SEO_URL') "
                    RelatedProjectsSQL &= "Where p.Available = 'Y' "
                    RelatedProjectsSQL &= "and r.Project_Id = " & ProjectID & " Order By Ordering "
                    Dim RelatedProjectsDT As New DataTable
                    RelatedProjectsDT = DBFunctions.GetDataTable(RelatedProjectsSQL)

                    If RelatedProjectsDT.Rows.Count > 0 Then
                        RelatedProjectsOUT &= "<dt>Related Properties</dt><dd>"
                        For R As Integer = 0 To RelatedProjectsDT.Rows.Count - 1
                            Dim ProjectURL As String = ""
                            If RelatedProjectsDT.Rows(R).Item("SeoURL").ToString.Trim <> "" Then
                                ProjectURL = RelatedProjectsDT.Rows(R).Item("SeoURL").ToString.Trim
                            Else
                                ProjectURL = RelatedProjectsDT.Rows(R).Item("ProjectID").ToString.Trim
                            End If
                            RelatedProjectsOUT &= "<a href=""" & ProjectURL & "-property-gallery"">" & formatfunctionssimple.AutoFormatText(RelatedProjectsDT.Rows(R).Item("Name").ToString.Trim) & "</a>" & vbNewLine
                            RelatedProjectsOUT &= "<br />"
                        Next
                        RelatedProjectsOUT &= "</dd>"
                        RelatedProjects_Literal.Text = RelatedProjectsOUT
                    End If

                Case "map"
                    Dim ProjectMapAddressOUT As String = ""
                    Dim ProjectMapAddressSQL As String = ""
                    ProjectMapAddressSQL &= "select ISNULL(Address, '') + '  ' + ISNULL(City, '') + ' , ' + ISNULL(State_id,'') + ' ' + ISNULL(Zip, '') StreetAddress, name "
                    ProjectMapAddressSQL &= "from ipm_project where ProjectId = " & ProjectID & ""
                    Dim ProjectMapAddressDT As New DataTable
                    ProjectMapAddressDT = DBFunctions.GetDataTable(ProjectMapAddressSQL)
                    If ProjectMapAddressDT.Rows.Count > 0 Then

                    End If


                    Dim ProjectMapCategoriesSQL As String = ""
                    ProjectMapCategoriesSQL &= "select Item_Value from ipm_project_field_desc d join ipm_project_field_value v on d.Item_ID = v.Item_ID where Item_Tag = 'IDAM_PROJECTTYPE' "
                    ProjectMapCategoriesSQL &= "and projectid = " & ProjectID & ""
                    Dim ProjectMapCategoriesDT As New DataTable
                    ProjectMapCategoriesDT = DBFunctions.GetDataTable(ProjectMapCategoriesSQL)

                    Dim ImageName As String

                    If ProjectLatXLongDT.Rows.Count > 0 Then
                        ProjectMap_Literal.Text &= "<div id=""propMapLatLong"">" & LatxLong & "</div>"
                        ProjectMap_Literal.Text &= "<div id=""propMap"">"
                        If ProjectMapCategoriesDT.Rows.Count > 0 Then
                            Dim ProjectMapCat As String = ProjectMapCategoriesDT.Rows(0).Item("Item_Value").ToString.Trim
                            If ProjectMapCat <> "" Then


                                If ProjectMapCat.IndexOf("Office") <> -1 Then
                                    ImageName = ""
                                    If IsDevProject = True Then
                                        ImageName = "m-dev.png"
                                    Else
                                        ImageName = "m-office.png"
                                    End If
                                End If
                                If ProjectMapCat.IndexOf("Hotel") <> -1 Then
                                    ImageName = ""
                                    If IsDevProject = True Then
                                        ImageName = "m-dev.png"
                                    Else
                                        ImageName = "m-hotel.png"
                                    End If
                                End If
                                If ProjectMapCat.IndexOf("Residential") <> -1 Then
                                    ImageName = ""
                                    If IsDevProject = True Then
                                        ImageName = "m-dev.png"
                                    Else
                                        ImageName = "m-residential.png"
                                    End If
                                End If
                                If ProjectMapCat.IndexOf("Retail") <> -1 Then
                                    ImageName = ""
                                    If IsDevProject = True Then
                                        ImageName = "m-dev.png"
                                    Else
                                        ImageName = "m-shops.png"
                                    End If
                                End If
                                If IsDevProject = True Then

                                End If

                            End If
                        End If
                        ProjectMap_Literal.Text &= "<img src=""/assets/images/" & ImageName & """ alt="""" />"

                        ProjectMap_Literal.Text &= "</div>"

                    End If



                Case "floorplans"
                    Dim FloorPlansSQL As String = ""
                    FloorPlansSQL &= "select d.Item_Tab, d.Item_group, d.Item_Name, v.Item_value "
                    FloorPlansSQL &= "from ipm_project_field_desc d Join ipm_project_field_value v on d.Item_Id = v.Item_Id and v.ProjectId = " & ProjectID & " "
                    FloorPlansSQL &= "Where d.Active =1 and d.Viewable =1 and d.Editable = 1 "
                    FloorPlansSQL &= "Order By Item_Tab, Item_Group, Item_Name "
                    Dim FloorPlansDT As New DataTable
                    FloorPlansDT = DBFunctions.GetDataTable(FloorPlansSQL)
                    If FloorPlansDT.Rows.Count > 0 Then
                        Dim FloorPlansDR() As DataRow
                        FloorPlansDR = FloorPlansDT.Select("Item_Tab = '*Web Site Content' AND Item_Group = '' AND Item_Name = '*Short Name'")
                        If FloorPlansDR.Length > 0 Then
                            If FloorPlansDR(0)("Item_value").ToString.Trim <> "" Then
                                ProjectTitle6_Literal.Text = FloorPlansDR(0)("Item_value").ToString.Trim
                            End If
                        End If
                    End If


                    If FloorPlansResourcesDT.Rows.Count > 0 Then
                        FloorPlansResourcesOUT &= "<dl>"
                        FloorPlansResourcesOUT &= "<dt>Floor Plans</dt>"
                        FloorPlansResourcesOUT &= "<dd>"
                        For R As Integer = 0 To FloorPlansResourcesDT.Rows.Count - 1
                            FloorPlansResourcesOUT &= "<a href=""/dynamic/document/week/asset/download/" & FloorPlansResourcesDT.Rows(R).Item("Asset_ID").ToString.Trim & "/" & Regex.Replace(FloorPlansResourcesDT.Rows(R).Item("Name").ToString.Trim.Replace(".pdf", ""), "[^a-zA-Z0-9]", "") & ".pdf"">"
                            FloorPlansResourcesOUT &= "<span class=""title"">" & formatfunctionssimple.AutoFormatText(FloorPlansResourcesDT.Rows(R).Item("Name").ToString.Trim) & "</span> "
                            FloorPlansResourcesOUT &= "<span class=""description"">" & formatfunctionssimple.AutoFormatText(FloorPlansResourcesDT.Rows(R).Item("Description").ToString.Trim) & "</span>"
                            FloorPlansResourcesOUT &= "</a>"
                            FloorPlansResourcesOUT &= "<br/>"
                        Next
                        FloorPlansResourcesOUT &= "</dd>"
                        FloorPlansResourcesOUT &= "</dl>"
                        FloorPlans_Literal.Text = FloorPlansResourcesOUT
                    End If
                Case "testfits"
                    Dim TestFitsSQL As String = ""
                    TestFitsSQL &= "select d.Item_Tab, d.Item_group, d.Item_Name, v.Item_value "
                    TestFitsSQL &= "from ipm_project_field_desc d Join ipm_project_field_value v on d.Item_Id = v.Item_Id and v.ProjectId = " & ProjectID & " "
                    TestFitsSQL &= "Where d.Active =1 and d.Viewable =1 and d.Editable = 1 "
                    TestFitsSQL &= "Order By Item_Tab, Item_Group, Item_Name "
                    Dim TestFitsDT As New DataTable
                    TestFitsDT = DBFunctions.GetDataTable(TestFitsSQL)
                    If TestFitsDT.Rows.Count > 0 Then
                        Dim TestFitsDR() As DataRow
                        TestFitsDR = TestFitsDT.Select("Item_Tab = '*Web Site Content' AND Item_Group = '' AND Item_Name = '*Short Name'")
                        If TestFitsDR.Length > 0 Then
                            If TestFitsDR(0)("Item_value").ToString.Trim <> "" Then
                                ProjectTitle5_Literal.Text = TestFitsDR(0)("Item_value").ToString.Trim
                            End If
                        End If
                    End If

                    If TestFitsResourcesDT.Rows.Count > 0 Then
                        TestFitsResourcesOUT &= "<dl>"
                        TestFitsResourcesOUT &= "<dt>TEST FITS</dt>"
                        TestFitsResourcesOUT &= "<dd>"
                        For R As Integer = 0 To TestFitsResourcesDT.Rows.Count - 1
                            TestFitsResourcesOUT &= "<a href=""/dynamic/document/week/asset/download/" & TestFitsResourcesDT.Rows(R).Item("Asset_ID").ToString.Trim & "/" & Regex.Replace(TestFitsResourcesDT.Rows(R).Item("Name").ToString.Trim.Replace(".pdf", ""), "[^a-zA-Z0-9]", "") & ".pdf"">"
                            TestFitsResourcesOUT &= "<span class=""title"">" & TestFitsResourcesDT.Rows(R).Item("Name").ToString.Trim & "</span> "
                            TestFitsResourcesOUT &= "<span class=""description"">" & TestFitsResourcesDT.Rows(R).Item("Description").ToString.Trim & "</span>"
                            TestFitsResourcesOUT &= "</a>"
                            TestFitsResourcesOUT &= "<br/>"
                        Next
                        TestFitsResourcesOUT &= "</dd>"
                        TestFitsResourcesOUT &= "</dl>"
                        TestFits_Literal.Text = TestFitsResourcesOUT
                    End If
                Case "sustainability"
                    'Left
                    Dim SustainabilitySQL As String = ""
                    SustainabilitySQL &= "select d.Item_Tab, d.Item_group, d.Item_Name, v.Item_value "
                    SustainabilitySQL &= "from ipm_project_field_desc d Join ipm_project_field_value v on d.Item_Id = v.Item_Id and v.ProjectId = " & ProjectID & " "
                    SustainabilitySQL &= "Where d.Active =1 and d.Viewable =1 and d.Editable = 1 "
                    SustainabilitySQL &= "Order By Item_Tab, Item_Group, Item_Name "
                    Dim SustainabilityDT As New DataTable
                    SustainabilityDT = DBFunctions.GetDataTable(SustainabilitySQL)
                    If SustainabilityDT.Rows.Count > 0 Then
                        Dim SustainabilityDR() As DataRow
                        SustainabilityDR = SustainabilityDT.Select("Item_Tab = '*Web Site Content' AND Item_Group = '' AND Item_Name = '*Short Name'")
                        If SustainabilityDR.Length > 0 Then
                            If SustainabilityDR(0)("Item_value").ToString.Trim <> "" Then
                                ProjectTitle3_Literal.Text = SustainabilityDR(0)("Item_value").ToString.Trim
                            End If
                        End If

                        'LEED Status
                        Dim SustainabilityLeftOUT As String = ""
                        SustainabilityLeftOUT &= "<dl>" & vbNewLine
                        SustainabilityLeftOUT &= "<dt>LEED Status</dt>" & vbNewLine
                        SustainabilityLeftOUT &= "<dd> "
                        SustainabilityDR = SustainabilityDT.Select("Item_Tab = '*Sustainability' AND Item_Group = '' AND Item_Name = '*LEED 1a Type'")
                        If SustainabilityDR.Length > 0 Then
                            If SustainabilityDR(0)("Item_value").ToString.Trim <> "" Then
                                SustainabilityLeftOUT &= SustainabilityDR(0)("Item_value").ToString.Trim & "</br>" & vbNewLine
                            End If
                        End If
                        SustainabilityDR = SustainabilityDT.Select("Item_Tab = '*Sustainability' AND Item_Group = '' AND Item_Name = '*LEED 1b Rating'")
                        If SustainabilityDR.Length > 0 Then
                            If SustainabilityDR(0)("Item_value").ToString.Trim <> "" Then
                                SustainabilityLeftOUT &= SustainabilityDR(0)("Item_value").ToString.Trim & "</br>" & vbNewLine
                            End If
                        End If
                        SustainabilityDR = SustainabilityDT.Select("Item_Tab = '*Sustainability' AND Item_Group = '' AND Item_Name = '*LEED 2a Type'")
                        If SustainabilityDR.Length > 0 Then
                            If SustainabilityDR(0)("Item_value").ToString.Trim <> "" Then
                                SustainabilityLeftOUT &= SustainabilityDR(0)("Item_value").ToString.Trim & "</br>" & vbNewLine
                            End If
                        End If
                        SustainabilityDR = SustainabilityDT.Select("Item_Tab = '*Sustainability' AND Item_Group = '' AND Item_Name = '*LEED 2b Rating'")
                        If SustainabilityDR.Length > 0 Then
                            If SustainabilityDR(0)("Item_value").ToString.Trim <> "" Then
                                SustainabilityLeftOUT &= SustainabilityDR(0)("Item_value").ToString.Trim & "</br>" & vbNewLine
                            End If
                        End If
                        SustainabilityLeftOUT &= "</dd> " & vbNewLine

                        SustainabilityDR = SustainabilityDT.Select("Item_Tab = '*Sustainability' AND Item_Group = 'Features' AND Item_Name = '*Energy Star Features'")
                        If SustainabilityDR.Length > 0 Then
                            If SustainabilityDR(0)("Item_value").ToString.Trim <> "" Then
                                SustainabilityLeftOUT &= "<dt>ENERGY STAR</dt>" & vbNewLine
                                SustainabilityLeftOUT &= "<dd> " & formatfunctions.AutoFormatText(SustainabilityDR(0)("Item_value").ToString.Trim) & "</dd>" & vbNewLine
                            End If
                        End If
                        SustainabilityLeftOUT &= "</dl>" & vbNewLine
                        SustainabilityLeft_Literal.Text = SustainabilityLeftOUT

                        'Right
                        Dim SustainabilityRightOUT As String = ""
                        SustainabilityRightOUT &= "<dl>" & vbNewLine
                        SustainabilityRightOUT &= "<dt>Green Features</dt>" & vbNewLine
                        SustainabilityRightOUT &= "<dd> " & vbNewLine
                        SustainabilityDR = SustainabilityDT.Select("Item_Tab = '*Sustainability' AND Item_Group = 'Features' AND Item_Name = '*Green Features'")
                        If SustainabilityDR.Length > 0 Then
                            If SustainabilityDR(0)("Item_value").ToString.Trim <> "" Then
                                SustainabilityRightOUT &= SustainabilityDR(0)("Item_value").ToString.Trim & "<br>" & vbNewLine
                            End If
                        End If
                        SustainabilityRightOUT &= "</dd> " & vbNewLine
                        SustainabilityRightOUT &= "</dl>" & vbNewLine
                        SustainabilityRight_Literal.Text = SustainabilityRightOUT
                    End If
                Case "resources"
                    Dim ProjectOtherDetailsSQL As String = ""
                    ProjectOtherDetailsSQL = "Select * from ipm_project where projectid = " & ProjectID & " "
                    Dim ProjectOtherDetailsDT As New DataTable
                    ProjectOtherDetailsDT = DBFunctions.GetDataTable(ProjectOtherDetailsSQL)

                    Dim ProjectResourcesSQL As String = ""
                    ProjectResourcesSQL &= "select d.Item_Tab, d.Item_group, d.Item_Name, v.Item_value "
                    ProjectResourcesSQL &= "from ipm_project_field_desc d Join ipm_project_field_value v on d.Item_Id = v.Item_Id and v.ProjectId = " & ProjectID & " "
                    ProjectResourcesSQL &= "where d.Active =1 and d.Viewable =1 and d.Editable = 1 "
                    ProjectResourcesSQL &= "order By Item_Tab, Item_Group, Item_Name "
                    Dim ProjectResourcesDT As New DataTable
                    ProjectResourcesDT = DBFunctions.GetDataTable(ProjectResourcesSQL)
                    Dim ProjectResourcesDR() As DataRow
                    If ProjectResourcesDT.Rows.Count > 0 Then
                        ProjectResourcesDR = ProjectResourcesDT.Select("Item_Tab = '*Web Site Content' AND Item_Group = ''AND Item_Name = '*Short Name'")
                        If ProjectResourcesDR.Length > 0 Then
                            If ProjectResourcesDR(0)("Item_value").ToString.Trim <> "" Then
                                ProjectTitle4_Literal.Text = ProjectResourcesDR(0)("Item_value").ToString.Trim
                            End If
                        End If
                    End If

                    'Public Resources

                    If PublicResourcesDT.Rows.Count > 0 Then
                        PublicResourcesOUT &= "<dl>"
                        PublicResourcesOUT &= "<dt>Resources</dt>"
                        PublicResourcesOUT &= "<dd>"
                        For R As Integer = 0 To PublicResourcesDT.Rows.Count - 1
                            PublicResourcesOUT &= "<a href=""/dynamic/document/week/asset/download/" & PublicResourcesDT.Rows(R).Item("Asset_ID").ToString.Trim & "/" & Regex.Replace(PublicResourcesDT.Rows(R).Item("Name").ToString.Trim.Replace(".pdf", ""), "[^a-zA-Z0-9]", "") & ".pdf"">"
                            PublicResourcesOUT &= "<span class=""title"">" & PublicResourcesDT.Rows(R).Item("Name").ToString.Trim & "</span> "
                            PublicResourcesOUT &= "<span class=""description"">" & PublicResourcesDT.Rows(R).Item("Description").ToString.Trim & "</span>"
                            PublicResourcesOUT &= "</a>"
                            PublicResourcesOUT &= "<br/>"
                        Next
                        PublicResourcesOUT &= "</dd>"
                        PublicResourcesOUT &= "</dl>"
                        PublicResources_Literal.Text = PublicResourcesOUT
                    End If

                    If IDAMFunctions.IsLoggedIn = True Then
                        'Secured Resources
                        If SecuredResourcesDT.Rows.Count > 0 Then
                            SecuredResourcesOUT &= "<dl>"
                            SecuredResourcesOUT &= "<dt>Secured Resources</dt>"
                            SecuredResourcesOUT &= "<dd>"
                            For R As Integer = 0 To SecuredResourcesDT.Rows.Count - 1
                                SecuredResourcesOUT &= "<a href=""/dynamic/document/week/asset/download/" & SecuredResourcesDT.Rows(R).Item("Asset_ID").ToString.Trim & "/" & Regex.Replace(SecuredResourcesDT.Rows(R).Item("Name").ToString.Trim.Replace(".pdf", ""), "[^a-zA-Z0-9]", "") & ".pdf"">"
                                SecuredResourcesOUT &= "<span class=""title"">" & SecuredResourcesDT.Rows(R).Item("Name").ToString.Trim & "</span> "
                                SecuredResourcesOUT &= "<span class=""description"">" & SecuredResourcesDT.Rows(R).Item("Description").ToString.Trim & "</span>"
                                SecuredResourcesOUT &= "</a>"
                                SecuredResourcesOUT &= "<br/>"
                            Next
                            SecuredResourcesOUT &= "</dd>"
                            SecuredResourcesOUT &= "</dl>"
                            SecuredResources_Literal.Text = SecuredResourcesOUT
                        End If
                    End If

                    'select p.ProjectID, Name 
                    'from ipm_project_related r
                    'join ipm_project p on r.Ref_Id = p.ProjectId 
                    'Where r.Project_Id = 21612097
                    'Order By Ordering 

                    'select t.type_value awardType, a.Headline Name, ProjectID 
                    'from IPM_AWARDS_RELATED_PROJECTS p 
                    'join ipm_awards a on p.awards_id = a.awards_id and Post_Date <= getdate() and Pull_Date >= getdate() and Show =1 
                    'join ipm_awards_type t on a.Type = t.Type_id where projectid = 21612097


                Case "video"
                    Dim VideoLinkSQL As String = ""
                    VideoLinkSQL &= "select Item_value video from ipm_project_field_value where Item_ID = (select Item_ID from ipm_project_field_desc "
                    VideoLinkSQL &= "where Item_Tab = '*Web Site Content' and Item_Group = 'Display Information' and Item_name = '*Video Link' and active = 1) "
                    VideoLinkSQL &= "and  Item_value <> '' and projectid = " & ProjectID & " "
                    Dim VideoLinkDT As New DataTable
                    VideoLinkDT = DBFunctions.GetDataTable(VideoLinkSQL)
                    If VideoLinkDT.Rows.Count > 0 Then
                        VideoLink = VideoLinkDT.Rows(0).Item("video").ToString.Trim
                    End If
                Case "publicart"
                    Dim PublicArtSQL As String = ""
                    PublicArtSQL &= "select d.Item_Tab, d.Item_group, d.Item_Name, v.Item_value "
                    PublicArtSQL &= "from ipm_project_field_desc d Join ipm_project_field_value v on d.Item_Id = v.Item_Id and v.ProjectId = " & ProjectID & " "
                    PublicArtSQL &= "Where d.Active =1 and d.Viewable =1 and d.Editable = 1 "
                    PublicArtSQL &= "Order By Item_Tab, Item_Group, Item_Name "
                    Dim PublicArtDT As New DataTable
                    PublicArtDT = DBFunctions.GetDataTable(PublicArtSQL)
                    If PublicArtDT.Rows.Count > 0 Then
                        Dim PublicArtDR() As DataRow
                        PublicArtDR = PublicArtDT.Select("Item_Tab = '*Web Site Content' AND Item_Group = '' AND Item_Name = '*Short Name'")
                        If PublicArtDR.Length > 0 Then
                            If PublicArtDR(0)("Item_value").ToString.Trim <> "" Then
                                ProjectTitle7_Literal.Text = PublicArtDR(0)("Item_value").ToString.Trim
                            End If
                        End If
                    End If


                    If PublicArtResourcesDT.Rows.Count > 0 Then
                        PublicArtResourcesOUT &= "<dl>"
                        PublicArtResourcesOUT &= "<dt>ART DESCRIPTION</dt>"
                        PublicArtResourcesOUT &= "<dd>"

                        PublicArtImageOUT &= "<dl>"
                        PublicArtImageOUT &= "<dt>IMAGE</dt>"
                        PublicArtImageOUT &= "<dd>"

                        For R As Integer = 0 To PublicArtResourcesDT.Rows.Count - 1
                            PublicArtResourcesOUT &= PublicArtResourcesDT.Rows(R).Item("ArtDesc").ToString.Trim
                            PublicArtImageOUT &= "<img src=""dynamic/image/always/asset/liquid/x200/92/ffffff/Center/" & PublicArtResourcesDT.Rows(R).Item("Asset_id").ToString.Trim & ".jpg" & """" & " alt=""""/>"
                        Next
                        PublicArtResourcesOUT &= "</dd>"
                        PublicArtResourcesOUT &= "</dl>"

                        PublicArtImageOUT &= "</dd>"
                        PublicArtImageOUT &= "</dl>"
                        PublicArt_Literal.Text = PublicArtResourcesOUT

                        PublicArtImage_Literal.Text = PublicArtImageOUT
                    End If
            End Select
        Else

            Dim PageURLL As String = HttpContext.Current.Request.RawUrl
            PageURLL = PageURLL.Remove(0, 1)

            'Dim PageName As String = Path.GetFileName(HttpContext.Current.Request.PhysicalPath)

            'pageURL = Replace(pageURL, "/", "")
            'If InStr(pageURL, "?") > 0 Then
            '    pageURL = pageURL.Substring(0, pageURL.IndexOf("?"))
            '    pageURL = Replace(pageURL, "/", "")
            'End If
            'UrlPage = pageURL

            Dim CheckOldLinkSQL As String = "select Name from IPM_CATEGORY where PARENT_CAT_ID = 21613935 and DESCRIPTION = '" & PageURLL & "' "
            Dim CheckOldLinkDT As New DataTable
            CheckOldLinkDT = DBFunctions.GetDataTable(CheckOldLinkSQL)
            If CheckOldLinkDT.Rows.Count > 0 Then
                Dim NewLinkURL As String = CheckOldLinkDT.Rows(0).Item("Name").ToString.Trim
                'Response.Redirect("/" & NewLinkURL)
                HttpContext.Current.Response.Status = "307 Temporary Redirect"
                HttpContext.Current.Response.AddHeader("Location", "/" & NewLinkURL)
            Else
                Response.Status = "307 Temporary Redirect"
                Response.AddHeader("Location", "/404")
            End If
        End If

    End Sub


    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)




        If Not Request.QueryString("tab") Is Nothing Then
            SelectedTab = Request.QueryString("tab")
        End If
        If Not Request.QueryString("id") Is Nothing Then
            UrlIdValue = Request.QueryString("id")
            If IsNumeric(UrlIdValue) = True Then
                Dim CheckProjectSql As String = "SELECT P.ProjectID FROM IPM_PROJECT P where P.ProjectID = " & UrlIdValue & ""
                Dim CheckProjectDT As New DataTable("CheckProjectDT")
                CheckProjectDT = DBFunctions.GetDataTable(CheckProjectSql)
                If CheckProjectDT.Rows.Count > 0 Then
                    ProjectID = CheckProjectDT.Rows(0)("ProjectID").ToString.Trim
                Else
                    Dim CheckProjectByUrlSql As String = ""
                    CheckProjectByUrlSql &= "SELECT IV.ProjectID, P.Name "
                    CheckProjectByUrlSql &= "FROM IPM_PROJECT_FIELD_VALUE IV "
                    CheckProjectByUrlSql &= "JOIN IPM_PROJECT P ON P.ProjectID = IV.ProjectID "
                    CheckProjectByUrlSql &= "WHERE IV.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SEO_URL') AND IV.Item_Value = '" & UrlIdValue & "'"
                    Dim CheckProjectByUrlDT As New DataTable("CheckProjectByUrlDT")
                    CheckProjectByUrlDT = DBFunctions.GetDataTable(CheckProjectByUrlSql)
                    If CheckProjectByUrlDT.Rows.Count > 0 Then
                        ProjectID = CheckProjectByUrlDT.Rows(0)("ProjectID").ToString.Trim
                    Else
                    End If
                End If
                'if is not numeric
            Else
                Dim CheckProjectByUrlSql As String = ""
                CheckProjectByUrlSql &= "SELECT IV.ProjectID, P.Name "
                CheckProjectByUrlSql &= "FROM IPM_PROJECT_FIELD_VALUE IV "
                CheckProjectByUrlSql &= "JOIN IPM_PROJECT P ON P.ProjectID = IV.ProjectID "
                CheckProjectByUrlSql &= "WHERE IV.Item_ID = (Select Item_ID From IPM_PROJECT_FIELD_DESC where Item_Tag = 'IDAM_SEO_URL') AND IV.Item_Value = '" & UrlIdValue & "'"
                Dim CheckProjectByUrlDT As New DataTable("CheckProjectByUrlDT")
                CheckProjectByUrlDT = DBFunctions.GetDataTable(CheckProjectByUrlSql)
                If CheckProjectByUrlDT.Rows.Count > 0 Then
                    ProjectID = CheckProjectByUrlDT.Rows(0)("ProjectID").ToString.Trim
                Else
                End If
            End If
            'if is nothing
        Else
        End If


        If ProjectID <> "" AndAlso IsNumeric(ProjectID) = True AndAlso SelectedTab <> "" Then
            Dim stringWriter As New System.IO.StringWriter()
            Dim htmlWriter As New HtmlTextWriter(stringWriter)
            MyBase.Render(htmlWriter)
            Dim html As String = stringWriter.ToString()
            html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
            writer.Flush()
            writer.Write(html)
            If Master.OriginalUrlPage.Contains("?") = False Then
                CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
            Else
                CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, Master.UrlPage)
            End If
        End If

    End Sub



End Class
