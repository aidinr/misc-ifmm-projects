﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="properties.aspx.vb" Inherits="properties" %>

<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
		<ul class="second-level">
			  <li class="navlink">      
			  <a href="properties-map">Properties Map</a></li>
			  <li class="navlink">      
			  <a class="active" href="properties">Properties List</a></li> 	  
		</ul>
        <div id="propSearch">
            <form method="get" action="search" id="propsearch"><input type="text" class="q" name="q" alt="PROPERTY SEARCH"/><input type="submit" value="Search" class="submit"/>
            <input type="hidden" value="propertiesmap" name="p"/></form>
        </div>
   </div>


  <%If HasCategory = False Then%>
<div id="content">
	<div class="inner-content" >
		<div class="container_12" id="main">
			<div class="grid_12 page-header">
				<h1>Properties</h1>
				<ul>
<%  For i As Integer = 1 To CMSFunctions.CountTextSingle("Properties_Categories_Item_" & "*" & "_Heading_1")%>
<li><a href="<%=CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", "Properties_Categories_Item_" & i & "_Link", "href")%>"><%=CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Properties_Categories_Item_" & i & "_Nav_Menu_Text")%></a></li>
<%Next %>

             <%If IDAMFunctions.IsLoggedIn = true Then%>
                <li id="Development"><a href="properties-development">Development</a></li>    
                <%End If%>

		    </ul>
			</div>
			<div class="clear"></div>
			<hr/>
			
<div class="content-properties cmsGroup cmsName_Properties_Categories">

 <!--

                    <div class="cmsGroupItem">
                    <div class="grid_4">
				    <h1 class="cms cmsType_TextSingle cmsName_Heading_1">Heading 1</h1>
			        </div>
			        <div class="grid_8">
				    <h2>Heading 1</h2>
				    <p>Description</p>
				    <h2><a  href="#">Link</a></h2>
                    <em>Upper menu text</em>
				    <hr/>
			       </div>
                   </div>
-->

<%  For i As Integer = 1 To CMSFunctions.CountTextSingle("Properties_Categories_Item_" & "*" & "_Heading_1")%>
<div class="cmsGroupItem">
            <div class="grid_4">
				<h1 class="cms cmsType_TextSingle cmsName_Properties_Categories_Item_<%=i%>_Heading_1">Office Properties</h1>
			</div>
			<div class="grid_8">
				<h2 class="cms cmsType_TextSingle cmsName_Properties_Categories_Item_<%=i%>_Heading_2"></h2>
				<p class="cms cmsType_TextMulti cmsName_Properties_Categories_Item_<%=i%>_Description"></p>
				<h2><a class="cms cmsType_Link cmsName_Properties_Categories_Item_<%=i%>_Link" href="#"></a></h2>
				<em class="cmsonly cms cmsType_TextSingle cmsName_Properties_Categories_Item_<%=i%>_Nav_Menu_Text"></em>
                <hr/>
			</div>
</div>
 <div class="clear"></div>
<%Next %>
            <div class="clear"></div>

<div id="headlines" class="bar80 infobar">
<div class="grid_4"><h1>Recent News</h1></div>
<div class="grid_8">
<div id="headline-rotate">
<asp:Literal ID="News_Literal" runat="server"></asp:Literal>
<span class="clear"></span>
</div>
</div>
</div>


		  </div>		  
	    </div>
	</div>
   </div>

   <%Else%>
<div id="content">
	<div class="inner-content">
		<div class="container_12" id="main">
			<div class="grid_12 page-header">
				<h1>Properties</h1>
                <ul>
					<%  For i As Integer = 1 To CMSFunctions.CountTextSingle("Properties_Categories_Item_" & "*" & "_Heading_1")%>
                    <%
                        Dim NavText As String = CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Properties_Categories_Item_" & i & "_Nav_Menu_Text")
                        If NavText.ToLower = SelectedCategory.ToLower Then
                            SelectedCategoryIndex = i
                        End If
                    %>
                   <li><a href="<%=CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", "Properties_Categories_Item_" & i & "_Link", "href")%>"><%= NavText%></a></li>
               <%Next %>
                <%If IDAMFunctions.IsLoggedIn = true Then%>
                <li><a href="properties-development">Development</a></li>    
                <%End If%>
                </ul>
			</div>
			<div class="clear"></div>
			<hr/>

             <%
                    SelectedCategoryHeader1 = CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Properties_Categories_Item_" & SelectedCategoryIndex & "_Heading_1")
                    SelectedCategoryHeader2 = CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Properties_Categories_Item_" & SelectedCategoryIndex & "_Heading_2")
                    SelectedCategoryIntro = CMSFunctions.getContentMulti(Server.MapPath("~") & "cms\data\", "Properties_Categories_Item_" & SelectedCategoryIndex & "_Description")
             %>

			<div class="grid_4">
            <%
                If SelectedCategory = "development" And IDAMFunctions.IsLoggedIn = True Then
                   %>
                   <h1 class="right cms cmsType_TextSingle cmsName_Properties_Category_Development_Heading_1"></h1>
                    <%Else%>
                    <h1 class="right"><%=SelectedCategoryHeader1%></h1>
                    <%
                End If
                %>
				
			</div>

			<div class="grid_8" >
             <%
                 If SelectedCategory = "development" And IDAMFunctions.IsLoggedIn = True Then
                   %>
                   <h2 class="cms cmsType_TextSingle cmsName_Properties_Category_Development_Heading_2"></h2>
                    <%Else%>
                    	<h2><%=SelectedCategoryHeader2%></h2>
                    <%
                End If
                %>
			
                 <%
                     If SelectedCategory = "development" And IDAMFunctions.IsLoggedIn = True Then
                   %>
                   <p class="cms cmsType_TextMulti cmsName_Properties_Category_Development_Description"></p>
                    <%Else%>
                    <p><%=SelectedCategoryIntro%></p>
                    <%
                End If
                %>
				
			</div><!--End-grid_8-->
			
			<div class="grid_4"></div>
			<div class="grid_8 content main-office">   
	       <div class="properties-cell">        
                 
            <asp:Literal ID="Projects_Literal" runat="server"></asp:Literal>
                 
        </div>
    </div>
    <div class="clear"></div>
      </div>
		<div class="clear"></div>
                
 	    </div>
	</div>
   <%End If %>
   
</asp:Content>

