﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="contact-us.aspx.vb" Inherits="contact_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<div id="navigation-lower">
		<ul class="second-level">
			<li><a href="contact-us">CONTACT US</a></li>
		</ul>
   </div>
   <div id="content">
	<div class="inner-content">		
		<div class="container_12" id="main">
			<div class="grid_12 page-header">
				<h1>contact</h1>
			</div>
			<div class="clear"></div>
			<hr/>
			<div class="grid_4">
				<p class="contactInfo">
				<strong class="cms cmsType_TextSingle cmsName_Contact_Company_Name">The JBG Companies</strong><br/>
<span class="cms cmsType_TextMulti cmsName_Contact_Company_Address">
				4445 Willard Avenue Suite 400<br/>
				Chevy Chase, Maryland 20815<br/>
				Phone: 240.333.3600
</span>
				</p>
			</div>

			<div class="grid_8 formcontain">

				<h2 class="cms cmsType_TextMulti cmsName_Contact_Intro">Thank you for your interest in The JBG Companies.</h2>
				<h2 id="thanks" class="cms cmsType_TextMulti cmsName_Contact_Thanks">We will respond promptly.</h2>

			  <form accept-charset="utf-8" action="submit/submit.aspx" method="post" class="form-stacked" id="subForm">

					<p>* fields are required.</p>

					<label class="left spx" for="firstname">*First Name</label>
					<input type="text" id="firstname" name="firstname" class="spx" tabindex="-1" />


					<label class="spx" for="last_name">*Last Name</label>
					<input type="text" id="last_name" name="last_name" class="required spx" tabindex="-1" />

					<span class="colRight">
						<label class="left" for="company_name">Company Name</label>
						<input type="text" id="company_name" name="company_name" tabindex="-1" />
					</span>

					<label class="left" for="name">*Name</label>
					<input type="text" id="name" name="name" class="required" tabindex="-1" />

					<span class="colRight">
						<label class="left" for="zip">Zip Code</label>
						<input type="text" id="zip" name="zip" tabindex="-1"/>
					</span>

					<label class="left" for="address">Street Address</label>
					<input type="text" id="address" name="address" tabindex="-1" />
					<span class="colRight">
						<label class="left" for="State">State</label>
						<input type="text" id="State" name="State" tabindex="-1" />
					</span>

					<label class="left" for="City">City</label>
					<input type="text" id="City" name="City" tabindex="-1" />

					<span class="colRight">
						<label class="left" for="email">*Email</label>
						<input type="text" id="email" name="email" class="required email" tabindex="-1" />
					</span>

					<label class="left" for="phone">*Phone</label>
					<input type="text" id="phone" name="phone" class="required" tabindex="-1" />

					<label class="spx" for="Telephone">*Telephone</label>
					<input type="text" name="Telephone" class="required spx" tabindex="-1" />

					<label class="spx" for="e-mail">*E-mail</label>
					<input type="text" id="e-mail" name="e-mail" class="required spx" tabindex="-1" />

					<br/>
					<label class="left" for="message">*Message</label>
					<textarea cols="30" rows="5" name="message" id="message" class="required" tabindex="-1"></textarea>                 
					<br/><br/><input type="submit" class="btn" value="Contact" id="submit" tabindex="-1" />

					<div class="cmsFill_Static cms cmsType_TextSingle cmsName_Contact_Recipient"></div>

				</form>

			</div>
			<div class="clear"></div>
			<br/><br/>
			<div class="infobar">
				<div class="grid_4 sidebar">
					<h1 class="cms cmsType_TextSingle cmsName_Contact_Link_Name">INTRALINKS</h1>
				</div>

				<div class="grid_8"> 
				  <a href="employment" class="cms cmsType_Link cmsName_Contact_Link">Click Here to View All Open Positions</a>
				</div>
			<span class="clear"></span>
			</div>
		</div>
		</div>
	</div>	


</asp:Content>

