﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="jbg-cares-partners.aspx.vb" Inherits="jbg_cares_partners" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
		<ul class="second-level">
			  <li class="navlink"><a href="jbg-cares-partners">Partners</a></li>
		</ul>
   </div>
   <div id="content">
	<div class="inner-content">

		<div id="main" class="container_12 partners">

			<div class="grid_12 page-header">
				<h1>Partners</h1>
                    </div>
                    <div class="clear"></div>
                    <hr/>




<div class="cmsGroup cmsName_Cares_Partners">
<!--
<div class="cmsGroupItem">
			<div class="grid_4">
				<h1><img src="assets/images/partners_logo5.png" alt="" /></h1>
			</div>
			<div class="grid_8">
				<p>
				        <span class="cms cmsType_TextMulti cmsName_Content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut nulla vitae enim sollicitudin condimentum at ac nibh. Duis porta sollicitudin nibh nec cursus. Suspendisse ultricies dignissim nisi, a gravida urna iaculis quis. Vivamus nec ultricies nibh. Nullam diam arcu, feugiat nec nisl non, faucibus facilisis diam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis viverra, metus vel mollis dui nec diam.</span>
					<span class="partnersPerson">&dash; Name of Person</span>
					<span class="partnersCompany"><a href="#">Partner Link</a></span>
					<br /><br />
				</p>
			</div>
			<div class="clear"></div>
</div>
-->

<!--
CMSFunctions.CountTextSingle("Cares_Partners_Item_" & "*" & "_Content")
-->

<%  For i As Integer = 1 To CMSFunctions.CountTextSingle("Cares_Partners_Item_" & "*" & "_Content") %>
<div class="cmsGroupItem">
			<div class="grid_4">
				<h1><a href="<%=CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", "Cares_Partners_Item_" & i & "_Link", "href")%>" target="_blank"><img src="cms/data/Sized/fit/140x100/85/transparent/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Cares_Partners_Item_" & i & "_Logo")%>"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Cares_Partners_Item_" & i & "_Logo")%>" class="cms cmsType_Image cmsName_Cares_Partners_Item_<%=i%>_Logo" /></a></h1>
			</div>
			<div class="grid_8">
				<p>
				        <span class="cms cmsType_TextMulti cmsName_Cares_Partners_Item_<%=i%>_Content"></span>
					<span class="partnersPerson cms cmsType_TextSingle cmsName_Cares_Partners_Item_<%=i%>_Person"></span>
					<span class="partnersCompany"><a href="http://arlingtonenvironment.org/" target="_blank" class="cms cmsType_Link cmsName_Cares_Partners_Item_<%=i%>_Link"></a></span>
				</p>
			</div>
			<div class="clear"></div>
</div>
<%Next %>



<div class="clear"></div>









		<div class="grid_4"></div><div class="grid_8"><span class="clear"></span></div>        
	  <div class="clear"></div>
	</div>
	</div>	
   </div>
</asp:Content>

