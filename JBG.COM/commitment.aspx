﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="commitment.aspx.vb" Inherits="commitment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
		<ul class="second-level">
		  <li><a href="leed-team">TEAM</a></li><li><a href="leed">LEED</a></li>
		  <li><a href="energystar">ENERGY STAR</a></li>
		  <li><a href="commitment">COMMITMENT</a></li>
		</ul>
   </div>
   <div id="content">
	<div class="inner-content">		
		<div class="container_12" id="main">
			<div class="grid_12 page-header">
				<h1>Commitment</h1>
			</div>
			<div class="clear"></div>
			<hr/>
			<div class="grid_4"><h1 class="cms cmsType_TextSingle cmsName_Commitment_Heading">Heading</h1></div>
<div class="grid_8">

<p class="cms cmsType_TextMulti cmsName_Commitment_Intro">Content</p>

</div>
<div class="clear"></div>
<br /><br />


<div class="grid_4"><h1 class="cms cmsType_TextSingle cmsName_Commitment_Links_Heading">Heading</h1></div>
<div class="grid_8">



<div class="com4  cmsGroup cmsName_Commitment_Links">

<!--
<div class="grid_4 cmsGroupItem">
 <img src="/dynamic/image/always/project/best/270x120/92/ffffff/Center/21610769.jpg" class="cms cmsType_Image cmsName_Image" />
 <a href="#">View Item</a>
</div>
-->

<%  For i As Integer = 1 To CMSFunctions.CountImages("Commitment_Links_Item_" & "*" & "_Image") %>
                 <div class="grid_4 cmsGroupItem">
<%
Dim l as String = "#"
l = CMSFunctions.GetFile(Server.MapPath("~") & "cms\data\File\", "Commitment_Links_Item_" & i &"_PDF", "link")
if l.Length < 3
l = CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", "Commitment_Links_Item_" & i &"_Link", "href")
end if
%>
			        <a href="<%=l%>"><img src="/cms/data/Sized/best/270x120/85/transparent/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Commitment_Links_Item_" & i & "_Image")%>"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Commitment_Links_Item_" & i & "_Image")%>" class="cms cmsType_Image cmsName_Commitment_Links_Item_<%=i%>_Image" /></a>
			        <a href="Link" class="cms cmsType_Link cmsName_Commitment_Links_Item_<%=i%>_Link">Link</a>
			        <a href="Link" class="cms cmsType_File cmsName_Commitment_Links_Item_<%=i%>_PDF">PDF</a>
                  </div>
       <%Next %>
   </div>  


</div>
<div class="clear"></div>
<br /><br />


<div class="grid_4"><h1 class="cms cmsType_TextSingle cmsName_Commitment_Case_Heading">Heading</h1></div>
<div class="grid_8">



<div class="com4  cmsGroup cmsName_Commitment_Case">

<!--
<div class="grid_4 cmsGroupItem">
 <img src="/dynamic/image/always/project/best/270x120/92/ffffff/Center/21610769.jpg" class="cms cmsType_Image cmsName_Image" />
 <a href="#">View Item</a>
 <em><a href="#">Name</a></em>
 <strong><a href="#">View PDF</a></strong>
</div>
-->

<%  For i As Integer = 1 To CMSFunctions.CountImages("Commitment_Case_Item_" & "*" & "_Image") %>

<%
Dim l as String = "#"
l = CMSFunctions.GetFile(Server.MapPath("~") & "cms\data\File\", "Commitment_Case_Item_" & i &"_PDF", "link")
%>

                 <div class="grid_4 cmsGroupItem">
			        <a href="<%=l%>"><img src="/cms/data/Sized/best/270x120/85/transparent/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Commitment_Case_Item_" & i & "_Image")%>"  alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Commitment_Case_Item_" & i & "_Image")%>" class="cms cmsType_Image cmsName_Commitment_Case_Item_<%=i%>_Image" /></a>
			        <span class="cms cmsType_TextSingle cmsName_Commitment_Case_Item_<%=i%>_Title">Title</span>
			        <em class="cms cmsType_TextSingle cmsName_Commitment_Case_Item_<%=i%>_Text">Text</em>
			        <strong><a href="Link" class="cms cmsType_File cmsName_Commitment_Case_Item_<%=i%>_PDF">PDF</a></strong>
                  </div>
       <%Next %>
   </div>  


<div class="clear"></div>







</div>
		</div>

	</div>	
	</div>

</asp:Content>

