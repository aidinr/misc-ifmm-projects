﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="development.aspx.vb" Inherits="development" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<div id="navigation-lower">
		<ul class="second-level">
				  <li class="navlink">      
				  <a href="investment-management">Investment Management </a></li>
				  <li class="navlink">      
				  <a href="development">Development</a></li>
				  <li class="navlink">      
				  <a href="asset-management">Asset Management</a></li>		  
				  <li class="navlink">      
				  <a href="organization">Organization</a></li>
				  <li class="navlink">      
				  <a href="affordable-housing">Affordable Housing</a></li>	  
				  <li class="navlink">      
				  <a href="awards-recognition">Awards</a></li>
				  <li class="navlink">      
				  <a href="public-art">Public Art</a></li>	  
			</ul>
   </div>
   <div id="content">
	<div class="inner-content" >
		<div class="container_12" id="main">
				<div class="grid_12 page-header">
					<h1>development</h1>
				</div>
				<div class="clear"></div>
				<hr/>



                <div class="cmsGroup cmsName_Development_Intro">

 <!--
                    <div class="cmsGroupItem">
                    <div class="grid_4">
			        <h1>Item Heading</h1>
			        </div>
			        <div class="grid_8">
                    <h2>Item Heading</h2>
			        <p class="cms cmsType_TextMulti cmsName_Content">
				    Content
			        </p>
                    </div>
                    </div>
-->

<%  For i As Integer = 1 To CMSFunctions.CountTextSingle("Development_Intro_Item_" & "*" & "_Content")%>
                 <div class="cmsGroupItem">
                    <div class="grid_4">
			        <h1><span class="cms cmsType_TextSingle cmsName_Development_Intro_Item_<%=i%>_Heading"></span><br /></h1></div>
			        <div class="grid_8">
                    <h2 class="cms cmsType_TextSingle cmsName_Development_Intro_Item_<%=i%>_Content_Heading">Item Heading</h2>
			        <p class="cms cmsType_TextMulti cmsName_Development_Intro_Item_<%=i%>_Content">Content</p>
                   </div>
                  </div>
                  <div class="clear"></div>
       <%Next %>
           
		  </div>


		<div class="grid_4"></div>
			<div class="grid_8">
				<h2>Featured Properties</h2>
				<asp:Literal ID="LeedList_Literal" runat="server"></asp:Literal>
			</div>        
		  <div class="clear"></div>
		</div>
	</div>	
   </div>

</asp:Content>

