﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="jbg-cares.aspx.vb" Inherits="jbg_cares" %>

<%@ Import Namespace="System.IO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
		<ul class="second-level">
			  <li class="navlink"><a href="jbg-cares-partners">Partners</a></li>
		</ul>
</div>


   <div id="content">
	<div class="inner-content">
		<div class="container_12" id="main">
			<div class="grid_12 page-header">
				<h1>JBG Cares</h1>
			</div>
			<div class="clear"></div>
			<hr/>

 <div class="cmsGroup cmsName_Cares_Intro">

 <!--
                    <div class="cmsGroupItem">
                    <div class="grid_4">
			        <h1>
                    <img alt="" src="assets/images/thinkgreen.png"/>
                    </h1>
			        </div>
			        <div class="grid_8">
			        <h2>Item Heading</h2>
			        <p class="cms cmsType_TextMulti cmsName_Content">
				    Content
			        </p>
                    </div>
                    </div>
  -->

<%  For i As Integer = 1 To CMSFunctions.CountTextSingle("Cares_Intro_Item_" & "*" & "_Content")%>
                 <div class="cmsGroupItem">
                    <div class="grid_4">
			        <h1>
                     <img class="cms cmsType_Image cmsName_Cares_Intro_Item_<%=i%>_Image" src="cms/data/Sized/liquid/140x/75/ffffff/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Cares_Intro_Item_" & i & "_Image")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Cares_Intro_Item_" & i & "_Image")%>" />
                    </h1>
			        </div>
			        <div class="grid_8">
                                <h2 class="cms cmsType_TextSingle cmsName_Cares_Intro_Item_<%=i%>_Heading"></h2>
			        <p class="cms cmsType_TextMulti cmsName_Cares_Intro_Item_<%=i%>_Content">
				    Content
			        </p>

                   </div>
                   <div class="clear"></div>
                  </div>
<%Next %>




           
		  </div>

			<div class="clear"></div>
            <div class="clear"></div>
		</div>
	</div>
   </div>

</asp:Content>

