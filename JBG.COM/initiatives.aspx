﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="initiatives.aspx.vb" Inherits="initiatives" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
			<ul class="second-level">
		  <li><a href="leed-team">TEAM</a></li><li><a href="leed">LEED</a></li>
		  <li><a href="energystar">ENERGY STAR</a></li>
		  <li><a href="commitment">COMMITMENT</a></li>
		</ul>
   </div>
   <div id="content">
	<div class="inner-content">		
		<div class="container_12" id="main">
			<div class="grid_12 page-header">
				<h1>Initiatives</h1>
			</div>
			<div class="clear"></div>
			<hr/>
			<div class="grid_4"></div>
<div class="grid_8">
<div class="cmsGroup cmsName_Initiatives_Group">
<!--
  <div class="cmsGroupItem">
      <h2>Item Heading</h2>
      <p class="cms cmsType_TextMulti cmsName_Content">Content</p>
  </div>
-->

<%  For i As Integer = 1 To CMSFunctions.CountTextSingle("Initiatives_Group_Item_" & "*" & "_Content")%>
                 <div class="cmsGroupItem">
			        <h2 class="cms cmsType_TextSingle cmsName_Initiatives_Group_Item_<%=i%>_Heading">Title</h2>
			        <p class="cms cmsType_TextMulti cmsName_Initiatives_Group_Item_<%=i%>_Content">Content</p>
			        
                  </div>
       <%Next %>
   </div>  


<div class="clear"></div>

</div>
		</div>

	</div>	
	</div>

</asp:Content>

