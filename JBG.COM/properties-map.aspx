﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="properties-map.aspx.vb" 
Inherits="properties_map" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="map_canvas"></div> <!--End-map_canvas-->

        <div id="navigation-lower">
                <ul class="second-level">
                          <li class="navlink">
                          <a class="active" href="properties-map">Properties Map</a></li>
                          <li class="navlink">
                          <a href="properties">Properties List</a></li>
                </ul>
        <div id="propSearch">
            <form method="get" action="search" id="propsearch"><input type="text" class="q" name="q" alt="PROPERTY SEARCH"/><input type="submit" 
value="Search" class="submit"/>
            <input type="hidden" value="propertiesmap" name="p"/></form>
        </div>
   </div>

      <div id="property-navigation">

              <div class="grid_12">

                <ul class="second-level l1">
                  <li><div class="nav-label">CATEGORY:</div></li>
                  <li><a  class="category-filter all active" href="">All</a>
                  </li><li><a class="category-filter" href="office">Office</a></li>
                  <li><a  class="category-filter" href="residential">Residential</a></li>
                  <li><a  class="category-filter" href="hotel">Hotel</a></li>
                  <li><a class="category-filter" href="shops">Retail</a></li>
                  <li><a  class="category-filter" href="mixed">Mixed-Use</a></li>
                </ul>
              <ul class="second-level l2">
                <li><div class="nav-label">LOCATION:</div></li>
                <li><a  class="state-filter all active" href="">All</a></li>
                <li><a class="state-filter" href="DC">District of Columbia</a></li>
                <li><a  class="state-filter" href="MD">Maryland</a></li>
                <li><a class="state-filter" href="VA">Virginia</a></li>
              </ul>
              </div>

              <div class="grid_5">
                <ul class="icons">
                  <li><a href="#" class="reset-button">Reset</a></li>
                  <li><a href="#" class="zoom-button zoom-in">Zoom In</a></li>
                  <li><a href="#" class="zoom-button zoom-out">Zoom Out</a></li>
                  <li><a href="#" class="show-extents-button">Show</a></li>
                  <li><a style="background-image: none;" href="#" class="full-map-btn full-nav">Show Map</a></li>
                </ul>
                <span class="clear"></span>
              </div>
  
    </div>
    <style>
    .full-map-btn {
    }
    </style>
    <script>
    // disable page horizontally on iphone
    $(window).resize(checkWindowSize);
    function checkWindowSize () {
        if (($(window).width() < 668) && ($(window).width() > 480)) {
            $('body').css('display','none');
            $('html').css({"color":"#fff","background":"#000","font-size":"15px","font-family":"arial","margin-top":"50px","text-align":"center"});
            $('html').text('Please rotate your device.');
        } else {
            location.reload();
        }
    }
     
    if ($(window).width() < 500) {
      $('#property-navigation ul.icons li a.show-extents-button').parent().remove();
      $('#property-navigation ul.icons li a.full-map-btn').css('display','block');
      $('#property-navigation ul.icons li a.full-map-btn').click(function () {
        if ($(this).hasClass('full-nav')) {
          $('.second-level').slideUp('fast');
          $(this).removeClass('full-nav');
          $(this).text('Show Nav');
        } else {
          $('.second-level').slideDown('fast');
          $(this).addClass('full-nav');
          $(this).text('Show Map');
        }
      });
    }

    </script>

   <div id="content" style="height:100%; background:none; box-shadow:none;">

   <div class="inner-content" >
        </div><!--End-inner-content-->
   </div>


</asp:Content>


