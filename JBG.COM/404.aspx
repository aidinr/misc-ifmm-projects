﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="404.aspx.vb" Inherits="NotFound" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

   <div id="navigation-lower">
		<ul class="second-level">
		  <li><a>Not Found</a></li>
		</ul>
   </div>

   <div id="content">
	 <div class="inner-content" >
		<div class="container_12" id="main">
			<div class="grid_12 page-header">
				<h1>Not Found</h1>
			</div>
			<div class="clear"></div>
			<hr/>
			<div class="grid_4"></div>
			<div class="grid_8">
				<p class="cms cmsType_TextMulti cmsName_Not_Found"></p>



              </div>
	    </div>
	</div>	
   </div>
</asp:Content>

