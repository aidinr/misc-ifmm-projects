﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="asset-management.aspx.vb" Inherits="asset_management" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	   <div id="navigation-lower">
			<ul class="second-level">
				  <li class="navlink">      
				  <a href="investment-management">Investment Management </a></li>
				  <li class="navlink">      
				  <a href="development">Development</a></li>
				  <li class="navlink">      
				  <a href="asset-management">Asset Management</a></li>		  
				  <li class="navlink">      
				  <a href="organization">Organization</a></li>
				  <li class="navlink">      
				  <a href="affordable-housing">Affordable Housing</a></li>	  
				  <li class="navlink">      
				  <a href="awards-recognition">Awards</a></li>
				  <li class="navlink">      
				  <a href="public-art">Public Art</a></li>	  
			</ul>
	   </div>
	   <div id="content">
		<div class="inner-content" >		
			<div class="container_12" id="main">
				<div class="grid_12 page-header">
					<h1>Asset Management</h1>
				</div>
				<div class="clear"></div>
				<hr/>
				<div class="grid_4">
					<h1>&nbsp;</h1>
				</div>                    
				<div class="grid_8">			  
				  <div id="assetManaContent">
					<div class="cms cmsType_TextMulti cmsName_Assets_Management_Content">Content</div>
				  </div>
				</div>
			</div>
		</div>	
	   </div>
</asp:Content>

