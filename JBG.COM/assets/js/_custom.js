var hash = window.location.hash.replace('#', '');
var oldHash = '';
var ajaxData;
var LastLoadTime=0;
var moving = 0;
var flickerfree = true;
var homeBGtimer;
var propSlide=0;

if( window.location.search || readCookie('SiteEdit')) flickerfree = false;

history.navigationMode = 'compatible';

$('*').unbind();
$(window).unload(function () {
    scroll(0, 0);
    $('*').unbind();
    $('input[type=text], select, textarea').val('');
});

if(flickerfree) {
  $(document).ready(function () {
    if(getPage() != '' && hash.length < 2) {
        if (location.href.indexOf('search')<1) document.location = './#' + getPage();
    } else if(1==2&&getPage() != '' && hash.length > 2) {
        document.location = './' + hash;
    } else if(getPage() != '' && hash.length < 2) {
        document.location = './#' + getPage();
    } else if(getPage() == '' && hash.length < 2) {
        document.location = './#home';
    }
    homeTitle = document.title;
    homeData = $('<textarea/>').html($('noscript#homePage').html()).val();
    ajax_content();
  });
}
else {
  $(document).ready(function () {
    ready();
  });
}

$(document).ready(function () {
    $('#resizable-background img').css('opacity',0);
    $('#resizable-background img').each(function(){
     if( Math.floor((Math.random()*3)+1)==1) $(this).parent().prepend($(this));
    });
    $('#resizable-background img:eq(0)').css('opacity',1);
    if( window.location.hash.indexOf('?') > 0 && location.href.indexOf('search')<1) document.location = String(document.location.href).replace('#', '').split('!')[0];
    else {
        oldHash = hash;
        hash = window.location.hash.replace('#', '');
        ajax_content();
    }
});

$.windowWidth = function () {
    $('body').css('overflow', 'hidden');
    var w = $('body').width();
    $('body').css('overflow', '');
    return w;
}
$(window).on('hashchange', function () {
    if(window.location.hash.indexOf('?') > 0 && location.href.indexOf('search')<1) document.location = String(document.location.href).replace('#', '').split('!')[0];
    else {
        oldHash = hash;
        hash = window.location.hash.replace('#', '');
        ajax_content();
    }
});

function ajax_content() {
    if(!flickerfree) return false;
    setTimeout("W3CV();", 100);
    if(new Date().getTime() - LastLoadTime < 500 || moving == 1) {
        return false;
    } else {
        if(LastLoadTime>0) changeBG();
        LastLoadTime = new Date().getTime();
        clearInterval(homeBGtimer);
        if(ajaxData) ajaxData.abort();
        if(hash == 'home') {
            populate(homeTitle, homeData);
            homeBGtimer=setInterval(changeBG,4500);
        } else {
            ajaxData = $.ajax({
                url: hash.split('!')[0],
                error: function (x) {
                    if(!x.status || x.status == 0) return;
                    var error = x.responseText;
                    error = error.substring(error.indexOf('<title>') + 7);
                    error = error.substring(0, error.indexOf('<'));
                    alert('Server Error ' + x.status + ':\n' + error)
                },
                success: function (data) {
                    ajaxData = null;
                    data = $('<div>' + data + '</div>');
                    var title = data.find('title').text();
                    if(data.has('#inner-wrapper').length > 0) {
                        populate(title, data.find('#inner-wrapper').html());
                    } else if(data.has('#inner-wrapper').length > 0) {
                        populate(title, data.find('.content-properties').html());
                    } else {
                        populate(title, data.find('#content').html(), true);
                    }
                    ready();

                }
            }).done(function () {});
        }
    }
}

function ready() {
   setTimeout("loadGoogleAnalytics(GoogleAnalyticsAccount)", 500);
   $('#cluetip').remove();
   var loggedin = readCookie('loggedin');
   if(loggedin) {
     if(!$('a.logout').hasClass('logout')) $('.footer-p').prepend('<a href="logout" class="logout">Logout</a>');
     if($('a.category-filter').is('a')) $('ul.second-level.l1').append('<li><a class="category-filter" href="#development">Development</a></li>');
     if($('#loginstatus').is('form')) {
       $('#loginstatus').before('<p class="loggedin">You are now logged in. <a href="logout" class="logout">Logout</a></p>');
       $('#loginstatus').remove();
     }
   }

   addfancybox();
   if($.windowWidth()<=520) { updatemenu(); }
   if($('.popup').hasClass('active')) { $('.popup').removeClass('active'); togglepopmenu(); }
   initgmap();
   $('.bx-wrapper img:eq(0)').css('opacity',1);
   if($('form input.q[alt]').hasClass('q')) {
     $('form input.q[alt]').attr('placeholder',$('form input.q[alt]').attr('alt'));
   }

   $('#propsearch').unbind();
   $('#propsearch').submit(function () {
     document.location='#search?q='+$('#propsearch .q').val();
     return false;
   });
   propSlide=0;

   if($('.bxslider li').length >1){
   	//propSlide.destroySlider();
   	propSlide = $('.bxslider').bxSlider({
   	  auto: true,
   	  autoControls: true
   	});
   }

   $('.person-organization a.popper').cluetip({local:true, hideLocal: true, sticky: true, arrows: true, cursor: 'pointer'});
   if($('.tip-popper a.pop').hasClass('pop')) {
      $('.tip-popper a.pop').cluetip({local:true, hideLocal: true, sticky: true, arrows: true, cursor: 'pointer'});
      $('.tip-popper .box-info').click(function () {
       document.location=String($(this).find('a:eq(0)').attr('href'));
      });
   }

   $('#subForm').unbind();
   $('#subForm').submit(function () {
      var error = 0;
      $('#subForm .required').removeClass('error');
      $('#subForm .required').not('.spx').each(function () {
        if(!$(this).val() || $(this).val() == $(this).attr('title') || ($(this).hasClass('email') &&
          /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test($(this).val()) == false)) {
          $(this).addClass('error');
          error = 1;
        }
      });
      $('#subForm .required').not('.spx').unbind();
      $('#subForm .required.error').keyup(function () {
        $(this).removeClass('error');
      })
      if(error==1) {
        alert('Please complete all required fields.');
        return false;
      }
      else {


        $.post('submit/submit-contact.aspx', $(this).serialize(), function (response) {
          if(response == '1') {
            $('#thanks').show();
            $('.contact-form').remove();
            _gaq.push(['_setAccount', GoogleAnalyticsAccount]);
            _gaq.push(['_trackPageview', '/contact_submitted_' + window.location.hash.substr(1)]);

          } else {
            alert('There was an error.  Please try again later.');
            $('#subForm').remove();
          }
          scroll(0, 0);
        });
        $('#subForm').fadeOut(500, function () {
          $('#thanks').fadeIn(400);
        });
        $('html, body, .content, #main').animate({
          scrollTop: (0)
        }, 120, function () {
          //
        });
        return false;
      }
   });

}

function addfancybox() {
        $(".videoLink").click(function () {
        var w = 960;
        var h = 540;
        var ww = $(window).width();
        var hh = $(window).height();
        if (ww < 970 && (hh > (ww * 0.5625 * .8) < hh)) {
            w = ww * 0.8;
            h = ww * 0.5625 * 0.8;
        } else if (ww < 970) {
            w = 380;
            h = 380 * 0.5625;
        }
        $.fancybox({
            href: 'http://player.vimeo.com/video/' + $(this).attr('href').replace(/[^0-9]/g, '')+'?&autoplay=1',
            width: w + 'px',
            height: h + 'px',
            type: 'iframe',
            fitToView: false
        });
        return false;
    });
}

function aLinks(subpage) {
  if(flickerfree){
    $('[href=#]').click(function (event) {
        event.preventDefault();
    });
    $('[href="/"],[href="./"]').each(function (ev) {
        $(this).attr('href', '#home');
    });
    $('a[href]').not('[href^="http"],[href*="dynamic/"],[href^="mailto:"],[href*="?"],[href*="/File/"],[href*=".pdf"],[href*="vcard"],[href="/"],[href="./"],[href^="#"]').each(function (ev) {
        $(this).attr('href', '#' + $(this).attr('href').replace('#!', '!').replace(/\//g, ''));
    });

    $('a.active').removeClass('active');
    $('a[href="#' + hash + '"]').addClass('active');

    $('ul.third-level a.active:eq(0)').each(function(){
     $('a[href="' + $(this).attr('href').slice(0,-5) + '"]').addClass('active');
    });

    $('a.active:eq(0)').each(function(){

     if($(this).attr('href').slice(0,-5	).indexOf('leed') == -1) $('a[href="' + $(this).attr('href').slice(0,-5) + '"]').addClass('active');
    });
    if(!$('.nav a.active').hasClass('active')) {
      if(hash.substr(0,7)=='article') {
        $('a[href="#news"]').addClass('active');
        $('.third-level a:eq(0)').removeClass('active');
        $('.third-level a:contains("'+ $('.copy h4:eq(0)').text().slice(-4) +'")').addClass('active');
      }
      else if(hash.substr(0,10)=='properties') {
        $('a[href="#properties"]').addClass('active');
        $('.third-level a[href*="'+ $('h1.right:eq(0)').text().toLowerCase().split(' ')[0] +'"]').addClass('active');
      }
      else if(hash.substr(0,12)=='organization') {
        $('a[href="#organization"]').addClass('active');
        if(hash.slice(-2).substr(0,1)=='-') $('.orgAlpha a[href="#'+ hash.slice(-1) +'"]').addClass('active');
      }
      else if($('.property-page').hasClass('property-page')) $('.nav a[href="#properties-map"]').addClass('active');


    }
    $('#navigation-lower-all a.active, #navigation-lower a.active:eq(0)').each(function(){
      if( $(this).closest('ul[id]').length==1 ) $('.nav ul li a:eq('+ $(this).closest('ul[id]').attr('id').substr(2) +')').addClass('active');
    });

  }
}

function getPage() {
    var nPage = location.href.replace('#','');
    nPage = nPage.substring(nPage.lastIndexOf('/') + 1);
    return nPage;
}

function populate(title, data, subpage) {

    if(!flickerfree) return false;
    document.title = title;
    $('.mCustomScrollbar').mCustomScrollbar('destroy');
    if(subpage) {
        $('#content').html(data);
    } else {
        $('#inner-wrapper').html(data);
    }
    sizer();
    aLinks(subpage);
}

function changeBG() {
         $('#resizable-background img').last().animate({
             opacity: 1
           }, 400, function() {
              $(this).parent().prepend($(this));
              $(this).siblings().css('opacity',0);
         });
}

function W3CV() {
  return false;
  $('#w3CV').remove();
  var script = document.createElement('script');
  script.type = 'text/javascript';
  if(window.location.hash) script.src = 'http://codebonus.com/projects/validate/?HTTP_REFERER=' + String(window.location.href).replace('#home', '').replace('#', '').split('!')[0];
  else script.src = 'http://codebonus.com/projects/validate/';
  script.id = 'w3CV';
  $('head').append(script);
}

function createCookie(name, value, days) {
  if(days) {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    var expires = "; expires=" + date.toGMTString();
  } else var expires = "";
  document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while(c.charAt(0) == ' ') c = c.substring(1, c.length);
    if(c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}


  if(readCookie('SiteEdit') || window.location.search.substr(0, 5) == '?edit') {
    setTimeout(function () {
      if(readCookie('SiteEdit')) $('body').addClass('cmsEditing');
      var css = document.createElement('link');
      css.setAttribute("rel", "stylesheet");
      css.setAttribute("type", "text/css");
      css.setAttribute("href", '/cms/assets/cms.css');
      $('head').append(css);
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = '/cms/assets/cms.js';
      $('head').append(script);
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = '/cms/assets/tiny_mce/jquery.tinymce.js';
      $('head').append(script);
    }, 1100);
  }

function sizer(){
  var w = $(window).width();
  var h = $(window).height();
  w = Math.ceil(w/100)*100;
  h = Math.ceil(h/100)*100;

  if(w > 2000) w=2000;
  if(w < 200) w=200;
  $('#resizable-background img').each(function(){
   $(this).attr('src','/dynamic/image/always/assets/fit/'+w+'x2000/92/333545/center/' + $(this).attr('longdesc')).load(function () {
    $(this).css('width', $(window).width()+'px').css('height', '');
    if( $(this).height() < $(window).height()) $(this).css('width', '').css('height', $(window).height()+'px');
   });
  });
	$('.select-option').click(function(){
		$(this).find('.select-option').slideToggle(300);
	});

	if($('.properties-cell ul.2-column').hasClass('2-column')) {
	 $('.properties-cell ul.2-column').each(function(){
	
	   if( $(this).find('li').length > 5) {
	    $(this).find('li').each(function(i){
	     if(i%2==1) $(this).addClass('goRight');
	     else $(this).addClass('goLeft');
	    });
	   }
	 });
	}

	scroll();
	cycle();
	if($('#propMapLatLong').is('div')) propMap();
}

	var pmap_newmapstyle = [
	{
		elementType: "labels",
		stylers: [
		{ visibility: "on" }
		]
	}, {
		elementType: "geometry",
		stylers: [
		{ hue: "#0044ff" },
		{ lightness: 45 },
		{ saturation: 39 }
		]
	}, {
		featureType: "road",
		elementType: "labels",
		stylers: [
		{ visibility: "simplified" }
		]
	}, {
		featureType: "road",
		elementType: "geometry",
		stylers: [
		{ saturation: -65 },
		{ visibility: "on" },
		{ lightness: 2 },
		{ hue: "#005eff" }
		]
	}, {
		featureType: "water",
		elementType: "geometry",
		stylers: [
		{ lightness: 19 }
		]
	}, {
		featureType: "poi",
		elementType: "geometry",
		stylers: [
		{ lightness: 30 }
		]
	}
	];

function initgmap() {
	if(typeof(hash)=='undefined') return;
	if(hash.indexOf('properties-map')==-1) return;

	pmap_vals=[];
	// if(!prop_data.length) { console.log('No properties data loaded...'); return; }
	for(i=0;i<prop_data.length;i++) {
		var latLng=prop_data[i].geocode.split(', ');
		if(latLng.length<2) {
			latLng=prop_data[i].geocode.split(' ');
			if(latLng.length<2) continue;
		}
		pmap_vals[i]={};
		pmap_vals[i].lat=latLng[0];
		pmap_vals[i].lng=latLng[1];
		pmap_vals[i].data={};
		//pmap_vals[i].latLng=latLng;
		var cs='',ic='',img='';
		for(var k in prop_data[i]) {
			pmap_vals[i].data[k]=prop_data[i][k];
			if(k.indexOf('cat_id')!=-1) {
				cs+=k.substr(6)+',';
				if(ic==='') {
					ic=k.substr(6);
					if(ic=='4') img='assets/images/m-office.png';
					else if(ic=='5') img='assets/images/m-residential.png';
					else if(ic=='6') img='assets/images/m-hotel.png';
					else if(ic=='7') img='assets/images/m-shops.png';
					else if(ic=='94') img='assets/images/d-office.png';
					else if(ic=='95') img='assets/images/d-residential.png';
					else if(ic=='96') img='assets/images/d-hotel.png';
					else if(ic=='97') img='assets/images/d.png';
					else img='assets/images/m-mixed.png';
				}
			}
		}
		if(ic==='') { ic=0;	img='assets/images/m-mixed.png'; }
		pmap_vals[i].tag=[prop_data[i].state,ic,cs];
		pmap_vals[i].options={ icon: img };
	}
	//console.log(pmap_vals);
	if($.windowWidth()<=520) { $("#map_canvas").css('height',$(window).height()+'px'); }
	$("#map_canvas").gmap3({
		verbose: true,
		map: {
			options: {
				zoom: 11,
				minZoom: 4, // the min is really the max you can zoom out. Think of it as zoom levels on a camera.
				maxZoom: 18,
				disableDefaultUI: true,
				center: [38.92197625, -77.24113549999998],
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				styles: pmap_newmapstyle
			}
		},
		marker: {
			values: pmap_vals,
			events: {
				click: function (marker, event, context) {
					//console.log(context);// console.log(event); console.log(marker);
					pmap=$("#map_canvas").gmap3('get');
					pmap.setCenter(marker.getPosition());
					pmap.setZoom(15);
					if($.windowWidth()<=520) { pmap.panBy(-($.windowWidth()/3), 0); }
					else { pmap.panBy(-100, 0); }
					//panBy(x:number, y:number)


					var d=context.data, bhtml='';
					//http://jbgcms.153.ifmmbeta.com/#21612055-property-gallery
					// http://jbgcms.153.ifmmbeta.com/dynamic/image/always/project/fit/230x120/80/ffffff/center/21599822.jpg
bhtml+='<div class="infobox"><img class="infobox-close" src="http://www.google.com/intl/en_us/mapfiles/close.gif" align="right" onclick="pmap_closebox();">';
bhtml+='<div class="infobox-inner"><div class="infobox-title"><a href="#'+d.link+'">'+d.title+'</a></div>';
bhtml+='<div class="infobox-content" onclick="ibClick(this)"><p class="infobox-city">'+d.city+', '+d.state+'</p><p class="infobox-pimg">';
bhtml+='<img src="'+d.image+'" alt="" title="'+d.title+'" class="infobox-img">';
bhtml+='</p><p class="infobox-desc">'+d.short_description+'</p></div></div></div>';
					$("#map_canvas").gmap3(
						{
							clear: 'overlay'
						},
						{
							overlay: {
								latLng: marker.getPosition(),
								options: {
									content: bhtml,
									offset: { x: -300, y: -150 }
								}
							}
						}
					);

				}
			},
			cluster: {
				radius: 20,
				maxZoom: 17,
            events: { // events trigged by clusters
                click: function (cluster, event, data) {
                    var bounds = new google.maps.LatLngBounds(
                        new google.maps.LatLng(data.data.markers[0].lat, data.data.markers[0].lng),
                        new google.maps.LatLng(data.data.markers[0].lat, data.data.markers[0].lng));
                    for(var i = 1; i < data.data.markers.length; i++) {
                        bounds.extend(new google.maps.LatLng(data.data.markers[i].lat, data.data.markers[i].lng));
                    }
                    var map = $(this).gmap3("get");
                    map.fitBounds(bounds);
                }

            },
				0: {
					content: '<span class="map-cluster1">CLUSTER_COUNT</span>',
					width: 30,
					height: 30
				},
				5: {
					content: '<span class="map-cluster2">CLUSTER_COUNT</span>',
					width: 40,
					height: 40
				}
			}

		}
	});

	pmap=$("#map_canvas").gmap3('get');

	$('#property-navigation .zoom-in').click(function (e) { e.preventDefault(); pmap_zoomin(); });
	$('#property-navigation .zoom-out').click(function (e) { e.preventDefault(); pmap_zoomout(); });
	$('#property-navigation .reset-button').click(function (e) { e.preventDefault(); pmap_reset(); });
	$('#property-navigation .show-extents-button').click(function (e) { e.preventDefault(); pmap_fitbounds(); });

	$('.state-filter').click(function (e) {
		e.preventDefault();
		$('.state-filter').removeClass('active');
		var state=$(this).addClass('active').attr('href');
		pmap_filter(state,$('.category-filter.active').attr('href'));
	});
	$('.category-filter').click(function (e) {
		e.preventDefault();
		$('.category-filter').removeClass('active');
		var cat=$(this).addClass('active').attr('href');
		pmap_filter($('.state-filter.active').attr('href'),cat);
	});

}

function pmap_closebox() {
	$("#map_canvas").gmap3({clear: 'overlay'});
}

function pmap_fitbounds() {
		pmap_closebox();
     var state=$('.state-filter.active').attr('href');
     var cat=$('.category-filter.active').attr('href');
     var bounds;
     for(var k in pmap_vals) {
			if(!pmap_checkfilter(pmap_vals[k],state,cat)) continue;
			if(typeof(bounds)=='undefined') bounds=new google.maps.LatLngBounds(new google.maps.LatLng(pmap_vals[k].lat, pmap_vals[k].lng), new google.maps.LatLng(pmap_vals[k].lat, pmap_vals[k].lng));
         else bounds.extend(new google.maps.LatLng(pmap_vals[k].lat, pmap_vals[k].lng));
     }
     pmap.fitBounds(bounds);

}

function pmap_filter(state,cat) {
	$('#map_canvas').gmap3({get:"clusterer"}).filter(function(data) {
		return pmap_checkfilter(data,state,cat);
	});
	pmap_fitbounds();
}

function pmap_checkfilter(obj,state,cat) {
	if(typeof(state)=='undefined') state=''; else if(typeof(state[0])!='undefined'&&state[0]=='#') state=state.substr(1);
	if(typeof(cat)=='undefined') cat=''; else if(typeof(cat[0])!='undefined'&&cat[0]=='#') cat=cat.substr(1);
	if(typeof(obj.tag[0])=='undefined') return true;
	var st=obj.tag[0],cats=obj.tag[2];
	if(state&&st!=state) return false;
	if(cat.length) {
		if(cat=='office'&&cats.indexOf('4')==-1) { return false; }
		else if(cat=='residential'&&cats.indexOf('5')==-1) return false;
		else if(cat=='hotel'&&cats.indexOf('6')==-1) return false;
		else if(cat=='shops'&&cats.indexOf('7')==-1) return false;
		else if(cat=='mixed'&&cats.split(',').length<3) return false;
		else if(cat=='development'&&cats.indexOf('9')==-1) return false;
	}
	return true;
}

function pmap_zoomin() {
	pmap=$("#map_canvas").gmap3('get');
	var zl=pmap.getZoom();
	pmap.setZoom(zl+1);
}
function pmap_zoomout() {
	pmap=$("#map_canvas").gmap3('get');
	var zl=pmap.getZoom();
	pmap.setZoom(zl-1);
}

function pmap_reset() {
	$("#map_canvas").gmap3('destroy');
	initgmap();
}


jQuery.fn.blindLeftToggle = function (duration, easing, complete) {
    return this.animate({
        marginLeft: parseFloat(this.css('marginLeft')) < 0 ? 0 : -(this.outerWidth()+20)
    }, jQuery.speed(duration, easing, complete));
};

$.windowWidth = function() {
    $('body').css('overflow','hidden');
    var w = $('body').width();
    $('body').css('overflow','');
    return w;
};

$(document).ready(function($) {

	$('.popup').click(function(event){
		event.stopPropagation();
		$('.popup').toggleClass('active');
		togglepopmenu();
	});

	$('.nav li a').click(function() {
		$('.popup').toggleClass('active');
		togglepopmenu();
	});

	$('#inner-wrapper').on('click','#navigation-lower li a', function() {
		if($('.popup').hasClass('active')) {
			$('.popup').removeClass('active');
			togglepopmenu();
		}
	});

});

function togglepopmenu() {
	if($.windowWidth()>520&&$.windowWidth()<1030) {
		$('.nav').blindLeftToggle(0);
		var mwidth='230px';
		if($('.popup').hasClass('active')) {
			$('.top-header, #inner-wrapper').animate({ marginLeft: '+='+mwidth }, 0, function() {
				$(this).addClass('content').css('margin-left','');
			});
		} else {
			$('.top-header, #inner-wrapper').removeClass('content').css('margin-left',mwidth).animate({ marginLeft: '-='+mwidth }, 0, function() {
				$(this).css('margin-left','');
			});
		}
	}
	if($.windowWidth()<=520) {
		if($('.popup').hasClass('active')) {
			$('.nav').fadeIn();
		} else {
			$('.nav').fadeOut();
		}
	}
}

function updatemenu() { //iPhone size menu update
	if(!$('.mplus').length) $('.nav li a.plus').each(function(index) {
		$('<span class="mplus">+</span>').data('index',index).appendTo(this);
	});
	if($('.nav>ul>li>.active').length && !$('.nav>ul>li>.active div').length) { //close submenu if other menu active
			$('.nav #navigation-lower').remove();
			$('.mplus').text('+');
	}

	$('.mplus').click(function(e) {
		var mobj=this;
		e.stopPropagation(); e.preventDefault();
		if($(this).text()!='+') {
				$('#navigation-lower').fadeOut(function() { $(mobj).text('+'); $('.nav #navigation-lower').remove(); });
				return;
		} else {
			openmenu(mobj);
		}
	});
}

function openmenu(mobj) {
	$('.nav #navigation-lower').remove();
	$('.mplus').text('+');

	var o=$('<div id="navigation-lower"></div>').append($('#sm'+$(mobj).data('index')).clone());
	$(mobj).parent().parent().append(o); o=null;
	$('#navigation-lower').fadeIn(function() { $(mobj).text('-'); });
}


$(window).on("resize", function() {
    clearTimeout($(this).data('timer'));
    $(this).data('timer', setTimeout(function() {
	scroll();
	sizer();
        },100)
    );
});

function scroll(){
	// document.title="Width: "+$( window ).width();
	if(typeof(prevWidth)!='undefined') {
		if(($.windowWidth()>520&&prevWidth<=520)||($.windowWidth()<=520&&prevWidth>520)) {
			prevWidth=$.windowWidth();
			window.location.reload();
		} else prevWidth=$.windowWidth();
	} else prevWidth=$.windowWidth();
	if($.windowWidth()>520) {
		$('#content').outerHeight(function(){
			return $(window).height() - $(this).offset().top - 20;
		});
		$('#content .inner-content , #content #main').css('height', ($('#content').outerHeight()-0)+'px');
		$(".inner-content").mCustomScrollbar('destroy');
		$(".inner-content").mCustomScrollbar({	scrollButtons:{ enable:true }	});
	} else {
		$('#content .inner-content , #content #main').css('height',''); //no height limitation for limited width
	}

}
function cycle(){
	if($("#headline-rotate a").length > 1){
		var sfx='scrollUp';
		if($.windowWidth()<=720) sfx='fade';
		$("#headline-rotate").cycle({
		speed: 600,
		timeout: 4500,
		fx: sfx,
		slideExpr: 'a',
		requeueOnImageNotLoaded: true,
		requeueTimeout: 250,
		cleartype: false
		}).cycle('resume').hover(function() {
		$(this).cycle('pause');
		},function(){
			$(this).cycle('resume');
		});
	}
}


function ibClick(t) {
 document.location = $(t).parent().find('a:eq(0)').attr('href');
}

function propMap() {
 $('#propMap').css('height',($('#main').height())+'px');
       directionsMapLatLong = $('#propMapLatLong').text();
       var lat  = Number(directionsMapLatLong.replace(/ /g,'').split(",")[0]);
       var long = Number(directionsMapLatLong.replace(/ /g,'').split(",")[1]);
       var image = $('#propMap>img').attr('src');
       var options = {
	mapTypeControlOptions: {
		mapTypeIds: [ 'Styled']
	},
        zoomControl: true,
        streetViewControl: false,
        overviewMapControl: false,
        mapTypeControl: false,
        panControl: true,
	center: new google.maps.LatLng(lat, long),
	zoom: 14,
	mapTypeId: 'Styled'
       };
       var div = document.getElementById('propMap');
       var map = new google.maps.Map(div, options);
       var styledMapType = new google.maps.StyledMapType(pmap_newmapstyle, { name: 'Styled' });
       map.mapTypes.set('Styled', styledMapType);
        var myMarker = new google.maps.Marker({
         position: new google.maps.LatLng(lat,long),
         map: map,
         icon: image
        });
        google.maps.event.addDomListener(myMarker, 'click', function(){
         window.open('https://maps.google.com/maps?q='+lat+','+long+'&hl=es;z=14','_blank');
        });
}

function loadGoogleAnalytics(acct) {
    if(readCookie('SiteEdit')) return false;
    if($('#gacode').attr('id')) {
        _gaq.push(['_setAccount', GoogleAnalyticsAccount]);
        _gaq.push(['_trackPageview', getPage()]);
    } else {
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."),
            pageTracker,
            s;
        s = document.createElement('script');
        s.src = gaJsHost + 'google-analytics.com/ga.js';
        s.id = 'gacode';
        s.type = 'text/javascript';
        s.onloadDone = false;
        function init() {
            pageTracker = _gat._getTracker(acct);
            pageTracker._trackPageview();
        }
        s.onload = function () {
            s.onloadDone = true;
            init();
        };
        s.onreadystatechange = function () {
            if(('loaded' === s.readyState || 'complete' === s.readyState) && !s.onloadDone) {
                s.onloadDone = true;
                init();
            }
        };
        document.getElementsByTagName('head')[0].appendChild(s);
    }
}