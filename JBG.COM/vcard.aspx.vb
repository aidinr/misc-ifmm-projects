Imports System.Web
Imports System.Data.SqlClient
Imports System.Data


Partial Class _vcard
    Inherits System.Web.UI.Page

    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim id As String = ""
        id = Request.QueryString("p")

        Dim sql As String = "Select u.FirstName, u.LastName,  u.Phone, u.Email, u.Position, u.Agency, u.Department  from ipm_user u join ipm_user_field_value v on u.UserID = v.User_ID and v.Item_ID = 21610584 and Item_value = '1' where u.Active = 'Y' and UserID = " & id
        Dim DT1 As New DataTable("FProjects")
        DT1 = DBFunctions.GetDataTable(sql)

        Dim sql2 As String = "select d.Item_Tag, v.Item_Value from ipm_user_field_value v join ipm_user_field_desc d on v.Item_ID = d.Item_Id where User_ID = " & id
        Dim DT2 As New DataTable("FProjects")
        DT2 = DBFunctions.GetDataTable(sql2)

        Dim tempRows() As DataRow

        Dim ecom As String = ""
        tempRows = DT2.Select("Item_Tag = 'IDAM_ECOM'")
        If tempRows.Length > 0 Then
            If tempRows(0)("Item_value").ToString().Trim() = "1" Then
                ecom = "Executive Committee"
            End If
        End If

        Dim mcom As String = ""
        tempRows = DT2.Select("Item_Tag = 'IDAM_MCOM'")
        If tempRows.Length > 0 Then
            If tempRows(0)("Item_value").ToString().Trim() = "1" Then
                mcom = "Management Committee"
            End If
        End If

        Dim dept1 As String = ""
        tempRows = DT2.Select("Item_Tag = 'IDAM_DEPT1'")
        If tempRows.Length > 0 Then
            dept1 = tempRows(0)("Item_value").ToString().Trim()
        End If

        Dim dept2 As String = ""
        tempRows = DT2.Select("Item_Tag = 'IDAM_DEPT2'")
        If tempRows.Length > 0 Then
            dept2 = tempRows(0)("Item_value").ToString().Trim()
        End If

        Dim dept3 As String = ""
        tempRows = DT2.Select("Item_Tag = 'IDAM_DEPT3'")
        If tempRows.Length > 0 Then
            dept3 = tempRows(0)("Item_value").ToString().Trim()
        End If

        Dim dept4 As String = ""
        tempRows = DT2.Select("Item_Tag = 'IDAM_DEPT4'")
        If tempRows.Length > 0 Then
            dept4 = tempRows(0)("Item_value").ToString().Trim()
        End If

        Dim depttemp As String = ""
        If ecom <> "" Then
            depttemp = ecom
        End If

        If mcom <> "" Then
            If depttemp <> "" Then
                depttemp &= ", "
            End If
            depttemp &= mcom
        End If

        If dept1 <> "" Then
            If depttemp <> "" Then
                depttemp &= ", "
            End If
            depttemp &= dept1
        End If
        If dept2 <> "" Then
            If depttemp <> "" Then
                depttemp &= ", "
            End If
            depttemp &= dept2
        End If
        If dept3 <> "" Then
            If depttemp <> "" Then
                depttemp &= ", "
            End If
            depttemp &= dept3
        End If
        If dept4 <> "" Then
            If depttemp <> "" Then
                depttemp &= ", "
            End If
            depttemp &= dept4
        End If


        If DT1.Rows.Count > 0 Then
            Response.Clear()
            Response.Charset = ""
            Response.AddHeader("Content-Disposition", "attachment; filename=" & DT1(0)("email").ToString.Trim.Substring(0, DT1(0)("email").IndexOf("@")) & ".vcf")
            Response.ContentType = "text/x-vCard"
            Response.Write("BEGIN:VCARD" & vbCrLf)
            Response.Write("VERSION:2.1" & vbCrLf)
            Response.Write("N:" & DT1(0)("LastName").ToString.Trim & ";" & DT1(0)("FirstName").ToString.Trim & vbCrLf)
            Response.Write("FN:" & DT1(0)("FirstName").ToString.Trim & " " & DT1(0)("LastName").ToString.Trim & vbCrLf)
            Response.Write("ORG:" & DT1(0)("Agency").ToString.Trim & ";" & depttemp & vbCrLf)
            Response.Write("TITLE:" & DT1(0)("Position").ToString.Trim & vbCrLf)
            'Response.Write("DEPARTMENT:" & DT1(0)("Department").ToString.Trim & vbCrLf)
            Response.Write("TEL;WORK;VOICE:" & formatfunctions.AutoFormatText(DT1(0)("phone").ToString.Trim) & vbCrLf)
            Response.Write("EMAIL;PREF;INTERNET:" & DT1(0)("email").ToString.Trim & vbCrLf)
            Response.Write("URL;WORK:www.jbg.com" & vbCrLf)
            Dim revTime As Date
            revTime = Now
            Dim rev As String
            rev = "REV:" & revTime.Year.ToString & revTime.Month.ToString & revTime.Day.ToString & "T" & revTime.Hour.ToString & revTime.Minute.ToString & revTime.Second.ToString & "Z"
            Response.Write(rev & vbCrLf)
            Response.Write("END:VCARD" & vbCrLf)
            Response.End()
        End If

 
    End Sub


End Class
