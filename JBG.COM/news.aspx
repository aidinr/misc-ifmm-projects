﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="news.aspx.vb" Inherits="news" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
		<ul class="second-level">
			<li><a href="news-press-releases">Press Releases</a></li>
			<li><a href="news-articles">Articles</a></li>
        </ul>
   </div>
   <div id="content">
	<div class="inner-content" >
		<div class="container_12" id="main">
        <div class="newsbop">
	        <div id="module" class="modulebox leading-module">
                <div class="grid_12 page-header sidebar">
                    <h1><asp:Literal ID="NewsTitle_Literal" runat="server"></asp:Literal></h1>
                </div>
                <div class="clear"></div>
                <asp:Literal ID="DistinctYears_Literal" runat="server"></asp:Literal>
                <hr/>
                <asp:Literal ID="News_Back" runat="server"></asp:Literal>
                <div class="clear"></div>

				<div class="grid_4"></div>
                <div class="grid_8 content">

                <asp:Literal ID="News_Literal" runat="server"></asp:Literal>
                     
                    <div class="clear"></div>
                    <div class="clear"></div>
                    <div id="ContentPlaceHolderDefault_aspcontainer_news_3_mailto">
                		    <hr/>
		                    <h4>For all media inquiries contact:</h4>
		                    <br/>
		                    Matt Blocher<br/>
		                    Senior Vice President, Marketing<br/>
		                    <a href="mailto:mblocher@jbg.com">mblocher@jbg.com</a>
                    </div>
	            </div>

                <div style="display: none;" class="grid_4" id="contentPlaceHolderDefault_aspcontainer_news_3_newsRight"></div>
                <div style="display: none;" class="grid_8 content" id="contentPlaceHolderDefault_aspcontainer_news_3_articlediv">

		            <h3 class="publication-info"></h3>
                    <h4></h4>
		            <div class="copy">
			            <h4></h4>

                    </div>
                </div>
            </div>
        </div>
		<br/>&nbsp;<br/>
	    </div>
	  </div>
	</div>

</asp:Content>

