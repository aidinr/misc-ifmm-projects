﻿
Partial Class logout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim myCookie As HttpCookie = New HttpCookie("loggedin")
        myCookie.Expires = DateTime.Now.AddDays(-1D)
        Response.Cookies.Add(myCookie)
        Response.Status = "307 Temporary Redirect"
        Response.AddHeader("Location", "/")
    End Sub


End Class
