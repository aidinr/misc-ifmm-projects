﻿Imports System
Imports System.Web
Imports System.Text
Imports System.Web.UI
Imports System.IO
Imports System.Text.RegularExpressions
Imports HtmlAgilityPack
Imports System.Data


Public Class CMSBasePage
Inherits Page
    Dim urlPage As String
    Dim originalUrlPage As String
    Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreInit
        Dim pageUrl As String = HttpContext.Current.Request.RawUrl
        OriginalUrlPage = HttpContext.Current.Request.RawUrl
        Dim pageName As String = Path.GetFileName(Request.PhysicalPath)
        pageURL = Replace(pageURL, "/", "")
        If InStr(pageURL, "?") > 0 Then
            pageURL = pageURL.Substring(0, pageURL.IndexOf("?"))
            pageURL = Replace(pageURL, "/", "")
        End If
        UrlPage = pageURL
    End Sub
    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter()
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        MyBase.Render(htmlWriter)
        Dim html As String = stringWriter.ToString()
        html = System.Text.RegularExpressions.Regex.Replace(html.Trim(), "(?=\s\s)\s*?(\n)\s*|(\s)\s+", "$1$2")
        Dim doc As New HtmlAgilityPack.HtmlDocument
        'If HtmlNode.ElementsFlags.ContainsKey("br") Then
        '    HtmlNode.ElementsFlags("br") = HtmlElementFlag.Closed
        'Else
        '    HtmlNode.ElementsFlags.Add("br", HtmlElementFlag.Closed)
        'End If
        If HtmlNode.ElementsFlags.ContainsKey("img") Then
            HtmlNode.ElementsFlags("img") = HtmlElementFlag.Closed
        Else
            HtmlNode.ElementsFlags.Add("img", HtmlElementFlag.Closed)
        End If
        HtmlNode.ElementsFlags.Remove("form")
        HtmlNode.ElementsFlags.Remove("option")
        'HtmlNode.ElementsFlags.Remove("ul")
        doc.OptionWriteEmptyNodes = True
        doc.OptionAutoCloseOnEnd = False
        doc.OptionCheckSyntax = False
        doc.OptionFixNestedTags = False
        doc.LoadHtml(html)
        html = ""
        For Each CmsNode As HtmlNode In doc.DocumentNode.SelectNodes("//@*")
            If CmsNode.Attributes.Contains("class") = True Then
                If CmsNode.Attributes("class").Value.Contains("cms") = True Then
                    Dim FullCmsClassName As String = ""
                    Dim CmsType As String = ""
                    Dim CmsName As String = ""
                    Dim CmsFill As String = ""
                    FullCmsClassName = CmsNode.Attributes("class").Value
                    CmsType = CMSFunctions.GetCmsType(FullCmsClassName)
                    CmsName = CMSFunctions.GetCmsName(FullCmsClassName)
                    CmsFill = CMSFunctions.GetCmsFillType(FullCmsClassName)
                    If CmsFill <> "Static" Then
                        Select Case CmsType
                            Case "TextSingle"
                                Dim elementText As String = CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", CmsName)
                                If elementText = "" AndAlso FullCmsClassName.Contains("CmsEmpty") = False Then
                                    doc.DocumentNode.SelectSingleNode(CmsNode.XPath).Attributes("class").Value = "CmsEmpty " & FullCmsClassName
                                Else
                                    If elementText.ToLower = "not yet available" AndAlso FullCmsClassName.Contains("CmsEmpty") = False Then
                                        doc.DocumentNode.SelectSingleNode(CmsNode.XPath).Attributes("class").Value = "CmsEmpty " & FullCmsClassName
                                    Else
                                        doc.DocumentNode.SelectSingleNode(CmsNode.XPath).InnerHtml = elementText
                                    End If
                                End If
                            Case "TextMulti"
                                Dim elementText As String = CMSFunctions.getContentMulti(Server.MapPath("~") & "cms\data\", CmsName)
                                If elementText = "" AndAlso FullCmsClassName.Contains("CmsEmpty") = False Then
                                    doc.DocumentNode.SelectSingleNode(CmsNode.XPath).Attributes("class").Value = "CmsEmpty " & FullCmsClassName
                                Else
                                    If elementText.ToLower = "not yet available" AndAlso FullCmsClassName.Contains("CmsEmpty") = False Then
                                        doc.DocumentNode.SelectSingleNode(CmsNode.XPath).Attributes("class").Value = "CmsEmpty " & FullCmsClassName
                                    Else
                                        doc.DocumentNode.SelectSingleNode(CmsNode.XPath).InnerHtml = elementText
                                    End If
                                End If
                            Case "Rich"
                                Dim elementText As String = CMSFunctions.GetContentRich(Server.MapPath("~") & "cms\data\", CmsName)
                                If elementText = "" AndAlso FullCmsClassName.Contains("CmsEmpty") = False Then
                                    doc.DocumentNode.SelectSingleNode(CmsNode.XPath).Attributes("class").Value = "CmsEmpty " & FullCmsClassName
                                Else
                                    If elementText.ToLower = "not yet available" AndAlso FullCmsClassName.Contains("CmsEmpty") = False Then
                                        doc.DocumentNode.SelectSingleNode(CmsNode.XPath).Attributes("class").Value = "CmsEmpty " & FullCmsClassName
                                    Else
                                        doc.DocumentNode.SelectSingleNode(CmsNode.XPath).InnerHtml = elementText
                                    End If
                                End If
                            Case "Image"
                                If CMSFunctions.CountImages(CmsName) = 0 AndAlso FullCmsClassName.Contains("CmsEmpty") = False Then
                                    doc.DocumentNode.SelectSingleNode(CmsNode.XPath).Attributes("class").Value = "CmsEmpty " + FullCmsClassName
                                End If
                            Case "Link"
                                Dim LinkUrl As String = CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", CmsName, "href")
                                Dim LinkText As String = CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", CmsName, "text")
                                If LinkText <> "" And LinkText <> "Link" Then
                                    doc.DocumentNode.SelectSingleNode(CmsNode.XPath).InnerHtml = LinkText
                                End If
                                If LinkUrl <> "" Then
                                    doc.DocumentNode.SelectSingleNode(CmsNode.XPath).Attributes("href").Value = LinkUrl
                                Else
                                    doc.DocumentNode.SelectSingleNode(CmsNode.XPath).Attributes("class").Value = "CmsEmpty " + FullCmsClassName
                                    doc.DocumentNode.SelectSingleNode(CmsNode.XPath).Attributes("href").Value = "#"
                                End If
                            Case "File"
                                Dim FileText As String = CMSFunctions.GetFile(Server.MapPath("~") & "cms\data\File\", CmsName, "text")
                                Dim FileLink As String = CMSFunctions.GetFile(Server.MapPath("~") & "cms\data\File\", CmsName, "link")
                                If FileText <> "" And FileText <> "Not Yet Available" Then
                                    doc.DocumentNode.SelectSingleNode(CmsNode.XPath).InnerHtml = FileText
                                End If
                                If FileLink <> "" And FileLink <> "#" Then
                                    doc.DocumentNode.SelectSingleNode(CmsNode.XPath).Attributes("href").Value = FileLink
                                Else
                                    doc.DocumentNode.SelectSingleNode(CmsNode.XPath).Attributes("class").Value = "CmsEmpty " + FullCmsClassName
                                End If
                            Case "Linkset"
                                Dim LinksetOUT As String = ""
                                For i As Integer = 0 To 10
                                    If Not CMSFunctions.CheckTextFile(Server.MapPath("~") & "cms\data\Linkset\" & CmsName & "\" & CmsName, i) Then
                                        Exit For
                                    End If
                                    Dim OpenType As String = ""
                                    If Not File.Exists(Server.MapPath("~") & "cms\data\Linkset\" & CmsName & "\" & CmsName & "_xc" & i & ".txt") Then
                                        OpenType = "blank"
                                    Else
                                        OpenType = "self"
                                    End If
                                    LinksetOUT &= "<a href=""" & CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Linkset\" & CmsName & "\" & CmsName & "_xa" & i.ToString.Trim & ".txt") & """ target=""_" & OpenType & """>" & CMSFunctions.FormatSimple(Server.MapPath("~") & "cms\data\Linkset\" & CmsName & "\" & CmsName & "_xb" & i & ".txt") & "</a> <br />"
                                Next
                                'Dim elementText As String = CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", CmsName)
                                doc.DocumentNode.SelectSingleNode(CmsNode.XPath).InnerHtml = LinksetOUT

                        End Select
                    End If
                End If
            End If
        Next
        html = doc.DocumentNode.OuterHtml
        html = Replace(html, "<br>", "<br/>")
        html = Replace(html, "></img>", "/>")
        'html = Replace(html, "></option>", "/>")
        writer.Flush()
        writer.Write(html)


        If OriginalUrlPage.Contains("?") = False Then
            CMSFunctions.WriteHtmlFile(Server.MapPath("~") & "cms\data\", html, UrlPage)
        Else
            CMSFunctions.DeleteHtmlFile(Server.MapPath("~") & "cms\data\", html, UrlPage)
        End If

    End Sub

   

End Class
