Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Web
Imports System.Data
Imports Newtonsoft.Json
Imports System.Globalization
Imports System.Collections.Generic

Public Class IDAMFunctions

    Public Shared Function IsLoggedIn() As Boolean
        Dim cookie As HttpCookie = HttpContext.Current.Request.Cookies("loggedin")
        If cookie Is Nothing Then
            IsLoggedIn = False
        Else
            Dim CookieValue As String = cookie.Value
            Dim SessionFile As String = HttpContext.Current.Server.MapPath("~") & "cms\data\Users\" & CookieValue.Split("/")(0) & "\" & CookieValue.Split("/")(1) & ".dat"
            If (Not File.Exists(SessionFile)) Then
                IsLoggedIn = False
            Else
                IsLoggedIn = True
            End If
        End If
    End Function

    

End Class




