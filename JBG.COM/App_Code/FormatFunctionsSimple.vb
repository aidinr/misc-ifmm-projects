Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.VisualBasic

Public Class formatfunctionssimple

    Public Shared Function AutoFormatText(ByVal input As String) As String
        input = HttpUtility.HtmlEncode(input)
        input = RegularExpressions.Regex.Replace(input, "  ", " &nbsp;")
        input = RegularExpressions.Regex.Replace(input, VbNewLine, VbNewLine + "<br />")
        input = RegularExpressions.Regex.Replace(input, "-- ", "&mdash; ")
        input = RegularExpressions.Regex.Replace(input, "(?<=&)amp;(?=#?\w+;)", String.Empty)
        input = RegularExpressions.Regex.Replace(input,"&lt;br?/&gt;", "<br />" & VbNewLine)



' Spaces
input = RegularExpressions.Regex.Replace(input, "  ", " &nbsp;")

' Line Breaks
input = RegularExpressions.Regex.Replace(input, VbNewLine, VbNewLine +"<br />")

' Dash Symbol
input = RegularExpressions.Regex.Replace(input,"-- ", "&mdash; ")

' Bullet Symbol
input = RegularExpressions.Regex.Replace(input, "\{\-\}", "&#8226;")

' Copyright Symbol
input = RegularExpressions.Regex.Replace(input, "\{[cC]\}", "&#169;")

' Trademark Symbol
input = RegularExpressions.Regex.Replace(input, "\{[tT][mM]\}", "&#8482;")

' Registered Trademark Symbol
input = RegularExpressions.Regex.Replace(input, "\{[rR]\}", "&#174;")

' Fix Double Entities
input = RegularExpressions.Regex.Replace(input, "(?<=&)amp;(?=#?\w+;)", String.Empty)

        Return input
    End Function

End Class