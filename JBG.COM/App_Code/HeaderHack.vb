Imports System.Web
Imports System.IO
Namespace org.HeaderHack
	Public Class CustomHeaderModule
		Implements IHttpModule
		Public Sub Dispose() Implements IHttpModule.Dispose
		End Sub
		Public Sub Init(context As HttpApplication) Implements IHttpModule.Init
			AddHandler context.PreSendRequestHeaders, AddressOf OnPreSendRequestHeaders
		End Sub
		Private Sub OnPreSendRequestHeaders(sender As Object, e As EventArgs)
			HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Public)
			HttpContext.Current.Response.Headers.Remove("Server")
			HttpContext.Current.Response.Headers.Remove("X-AspNet-Version")
			HttpContext.Current.Response.Headers.Remove("Last-Modified")
			HttpContext.Current.Response.Headers.Remove("Expires")
			HttpContext.Current.Response.AddHeader("Expires", Now.AddMinutes( New Random().Next(5760, 11520) ).ToString("ddd, dd MMM yyyy HH:mm:ss") & " GMT")
			HttpContext.Current.Response.AddHeader("Connection", "close")

		End Sub
	End Class
End Namespace