Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.VisualBasic

Public Class formatfunctionsjs

    Public Shared Function AutoFormatText(ByVal input As String) As String

        input = HttpUtility.HtmlEncode(input)

        ' Line Breaks
        input = RegularExpressions.Regex.Replace(input, vbNewLine, "<br />")

	' Dash Symbol
	input = RegularExpressions.Regex.Replace(input,"-- ", "&mdash; ")

	' Bullet Symbol
	input = RegularExpressions.Regex.Replace(input, "\{\-\}", "&#8226;")

	' Copyright Symbol
	input = RegularExpressions.Regex.Replace(input, "\{[cC]\}", "&#169;")

	' Trademark Symbol
	input = RegularExpressions.Regex.Replace(input, "\{[tT][mM]\}", "&#8482;")

	' Registered Trademark Symbol
	input = RegularExpressions.Regex.Replace(input, "\{[rR]\}", "&#174;")

	' Quote
	input = RegularExpressions.Regex.Replace(input,"""", "&quot;")

	' Single Quote
	input = RegularExpressions.Regex.Replace(input,"'", "&rsquo;")

	' Slash
	input = RegularExpressions.Regex.Replace(input,"\\", "&#92;")

        ' Fix Double Entities
        input = RegularExpressions.Regex.Replace(input, "(?<=&)amp;(?=#?\w+;)", String.Empty)

        Return input
    End Function

End Class