﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="investment-management.aspx.vb" Inherits="investment_management" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
		<ul class="second-level">
				  <li class="navlink">      
				  <a href="investment-management">Investment Management </a></li>
				  <li class="navlink">      
				  <a href="development">Development</a></li>
				  <li class="navlink">      
				  <a href="asset-management">Asset Management</a></li>		  
				  <li class="navlink">      
				  <a href="organization">Organization</a></li>
				  <li class="navlink">      
				  <a href="affordable-housing">Affordable Housing</a></li>	  
				  <li class="navlink">      
				  <a href="awards-recognition">Awards</a></li>
				  <li class="navlink">      
				  <a href="public-art">Public Art</a></li>	  
			</ul>
   </div>
   <div id="content">
	<div class="inner-content" >
		<div class="container_12"  id="main" >
			<div class="grid_12 page-header">
				<h1>Investment management</h1>
			</div>
			<div class="clear"></div>
			<hr/>
			
            <div class="cmsGroup cmsName_Investment_Intro">

 <!--
                    <div class="cmsGroupItem">
                    <div class="grid_4">
			        <h1>Item Heading</h1>
			        </div>
			        <div class="grid_8">
			        <p class="cms cmsType_TextMulti cmsName_Content">
				    Content
			        </p>
                    </div>
                    </div>
-->

<%  For i As Integer = 1 To CMSFunctions.CountTextSingle("Investment_Intro_Item_" & "*" & "_Content")%>
                 <div class="cmsGroupItem">
                    <div class="grid_4"><h1><span class="cms cmsType_TextSingle cmsName_Investment_Intro_Item_<%=i%>_Heading"></span></h1></div>
			        <div class="grid_8">
			        <p class="cms cmsType_TextMulti cmsName_Investment_Intro_Item_<%=i%>_Content">
				    Content
			        </p>
                   </div>
                  </div>
                  <div class="clear"></div>
       <%Next %>
		  </div>


			<div class="clear"></div>
			<div class="bar80 infobar">
				 <div class="grid_4 sidebar">
					<h1>INTRALINKS&nbsp;&nbsp;&nbsp;&nbsp;&gt;</h1>
				 </div>

				 <div class="grid_8">
					<a target="_blank" href="https://services.intralinks.com/logon.html?clientID=8818744706">Click Here to Login</a>
				 </div>
				 <div class="clear"></div>
			</div>
			<div class="clear"></div>

		</div>
	</div>	
   </div>

</asp:Content>

