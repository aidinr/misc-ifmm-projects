﻿Imports System.Data

Partial Class public_art
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ProjectsSQL As String = ""
        ProjectsSQL &= "select top 8 p.ProjectID, v5.Item_Value [Description], coalesce( nullif(p.Name, ''), "
        ProjectsSQL &= "v3.Item_Value) ShortName, isnull(a.asset_id, '') asset_id, F1.Item_Value as SeoURL "
        ProjectsSQL &= "from ipm_project p "
        ProjectsSQL &= "join ipm_project_field_value v1 on p.ProjectID = v1.ProjectID join ipm_project_field_desc d1 on v1.Item_ID = d1.Item_ID and d1.Item_Tag = 'IDAM_PAGELOCATION' and v1.Item_Value like '%Public Art Page%' "
        ProjectsSQL &= "join ipm_project_field_value v2 on p.projectID = v2.ProjectID join ipm_project_field_desc d2 on v2.Item_ID = d2.Item_ID and d2.Item_Tag = 'IDAM_FEATURE' and v2.Item_Value = '1' "
        ProjectsSQL &= "join ipm_project_field_value v3 on p.projectID = v3.ProjectID join ipm_project_field_desc d3 on v3.Item_ID = d3.Item_ID and d3.Item_Tag = 'IDAM_SHORTNAME' "
        ProjectsSQL &= "left join ipm_asset a on p.ProjectId = a.ProjectId join ipm_asset_field_value v4 on a.asset_id = v4.asset_id and  v4.Item_Value = '1' "
        ProjectsSQL &= "join ipm_asset_field_desc d4 on v4.Item_ID = d4.Item_ID and d4.Item_tag ='IDAM_ART' join ipm_asset_field_value v5 on a.asset_id = v5.asset_id "
        ProjectsSQL &= "join ipm_asset_field_desc d5 on d5.Item_ID = v5.Item_ID and d5.Item_Tag = 'IDAM_ART_DESC' "
        ProjectsSQL &= "left join ipm_project_field_value F1 on p.ProjectID = F1.ProjectID and F1.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SEO_URL') "
        ProjectsSQL &= "Order By newid() "
        Dim LeedProjectsDT As New DataTable
        LeedProjectsDT = DBFunctions.GetDataTable(ProjectsSQL)
        Dim LeedProjectsOUT As String = ""
        If LeedProjectsDT.Rows.Count > 0 Then
            For R As Integer = 0 To LeedProjectsDT.Rows.Count - 1
                LeedProjectsOUT &= ""
                If R Mod 2 Then
                    LeedProjectsOUT &= "<div class=""grid_4 omega"">" & vbNewLine
                Else
                    LeedProjectsOUT &= "<div class=""grid_4 alpha"">" & vbNewLine
                End If
                LeedProjectsOUT &= "<div class=""property-cell"">" & vbNewLine
                Dim ProjectURL As String = ""
                If LeedProjectsDT.Rows(R).Item("SeoURL").ToString.Trim <> "" Then
                    ProjectURL = LeedProjectsDT.Rows(R).Item("SeoURL").ToString.Trim
                Else
                    ProjectURL = LeedProjectsDT.Rows(R).Item("ProjectID").ToString.Trim
                End If
                LeedProjectsOUT &= "<div class=""image-container"">" & vbNewLine
                LeedProjectsOUT &= "<a href=""" & ProjectURL & "-property-public-art""><img src=""dynamic/image/always/asset/best/300x130/92/ffffff/center/" & LeedProjectsDT.Rows(R).Item("asset_id").ToString.Trim & ".jpg" & """" & " alt=""""/></a>"
                LeedProjectsOUT &= "</div>" & vbNewLine
                LeedProjectsOUT &= "<h2><a href=""" & ProjectURL & "-property-public-art"">" & formatfunctionssimple.AutoFormatText(LeedProjectsDT.Rows(R).Item("ShortName").ToString.Trim) & "</a></h2>" & vbNewLine
                LeedProjectsOUT &= "<p>" & formatfunctions.AutoFormatText(CMSFunctions.GetWords(LeedProjectsDT.Rows(R).Item("description").ToString.Trim, 50)) & "&#8230; <a href=""" & ProjectURL & "-property-public-art"">[More]</a></p>" & vbNewLine
                LeedProjectsOUT &= "</div>" & vbNewLine
                LeedProjectsOUT &= "</div>" & vbNewLine
                If R Mod 2 = 1 Then
                    LeedProjectsOUT &= "<div class=""clear""></div>" & vbNewLine
                End If
            Next
            LeedList_Literal.Text = LeedProjectsOUT
        End If

    End Sub

End Class
