﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="search.aspx.vb" Inherits="search" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
		<ul class="second-level">
			  <li class="navlink">      
			  <a href="properties-map">Properties Map</a></li>
			  <li class="navlink">      
			  <a class="active" href="properties">Properties List</a></li> 	  
		</ul>
        <div id="propSearch">
            <form method="get" action="search" id="propsearch"><input type="text" class="q" name="q" alt="PROPERTY SEARCH"/><input type="submit" value="Search" class="submit"/>
            <input type="hidden" value="propertiesmap" name="p"/></form>
        </div>
   </div>


<div id="content">
	<div class="inner-content" >
		<div class="container_12">
        <div class="property-page">
			<div class="grid_12 page-header">
				<h1>PROPERTY SEARCH RESULTS</h1>
				<ul>
<%  For i As Integer = 1 To CMSFunctions.CountTextSingle("Properties_Categories_Item_" & "*" & "_Heading_1")%>
<li>
<a href="<%=CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", "Properties_Categories_Item_" & i & "_Link", "href")%>"><%=CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Properties_Categories_Item_" & i & "_Nav_Menu_Text")%></a>
</li>
<%Next %>

             <%If IDAMFunctions.IsLoggedIn = true Then%>
                <li id="Development"><a href="properties-development">Development</a></li>    
                <%End If%>
		    </ul>
		</div>
		<div class="clear"></div>
	<hr/>
			
            <asp:Literal ID="Projects_Literal" runat="server"></asp:Literal>
		  
	    </div>
       </div>
	</div>
   </div>

</asp:Content>

