﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="leed.aspx.vb" Inherits="leed" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
	     <ul class="second-level">
		  <li><a href="leed-team">TEAM</a></li><li><a href="leed">LEED</a></li>
		  <li><a href="energystar">ENERGY STAR</a></li>
		  <li><a href="commitment">COMMITMENT</a></li>
		</ul>
   </div>
   <div id="content">
	<div class="inner-content">		
		<div class="container_12" id="main">
			<div class="grid_12 page-header">
				<h1>LEED</h1>
			</div>
			<div class="clear"></div>
			<hr/>
			 <div class="grid_4">
			   <h1 class="topLogo"><img class="cms cmsType_Image cmsName_Leed_Intro_Image_1" src="cms/data/Sized/liquid/110x/75/ffffff/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Leed_Intro_Image_1")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Leed_Intro_Image_1")%>" /></h1>
			 </div>
			 <div class="grid_8">
			   <p class="cms cmsType_TextMulti cmsName_Leed_Intro_Content"></p>
			 </div>
			 <div class="clear"></div>

			 <div class="grid_4"></div>
			 <div class="grid_8">
			   <h2 class="cms cmsType_TextMulti cmsName_Leed_Second_Intro_Heading"></h2>
			   <p class="cms cmsType_TextMulti cmsName_Leed_Second_Intro_Content"></p>

			   <div class="com4">
			   <div class="grid_4">
			    <img class="cms cmsType_Image cmsName_Leed_Featured_1_Image" src="cms/data/Sized/liquid/270x/75/ffffff/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Leed_Featured_1_Image")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Leed_Featured_1_Image")%>" />
			    <h2 class="cms cmsType_TextSingle cmsName_Leed_Featured_1_Title"></h2>
			    <p class="cms cmsType_TextMulti cmsName_Leed_Featured_1_Content"></p>
			   </div>

			   <div class="grid_4">
			    <img class="cms cmsType_Image cmsName_Leed_Featured_2_Image" src="cms/data/Sized/liquid/270x/75/ffffff/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Leed_Featured_2_Image")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Leed_Featured_2_Image")%>" />
			    <h2 class="cms cmsType_TextSingle cmsName_Leed_Featured_2_Title"></h2>
			    <p class="cms cmsType_TextMulti cmsName_Leed_Featured_2_Content"></p>
			   </div>
			   </div>

			 </div>

			 <div class="clear"></div>
			 <div class="grid_4"></div>
			 <div class="grid_8">
				<h2>Featured LEED Projects</h2>
                <asp:Literal ID="LeedList_Literal" runat="server"></asp:Literal>
			</div>
			<div class="clear"></div>
		</div>
	</div>	
   </div>
</asp:Content>

