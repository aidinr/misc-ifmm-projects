﻿Imports System.Data

Partial Class sustainability
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim SustainabilityNewsSQL As String = ""
        SustainabilityNewsSQL &= "select top 8  n.News_Id, Headline from ipm_news_field_desc d "
        SustainabilityNewsSQL &= "join ipm_news_field_value v on v.Item_ID = d.Item_ID and Item_Value = '1' join ipm_news n on v.News_ID = n.News_ID and n.Show = 1 and n.Post_Date <= getdate() and Pull_Date >= getdate()  where d.Item_Tag = 'IDAM_SNEWS' and d.Active = 1 "
        SustainabilityNewsSQL &= "Order By newid() "
        Dim SustainabilityNewsDT As New DataTable
        SustainabilityNewsDT = DBFunctions.GetDataTable(SustainabilityNewsSQL)
        Dim SustainabilityNewsOUT As String = ""
        If SustainabilityNewsDT.Rows.Count > 0 Then
            For R As Integer = 0 To SustainabilityNewsDT.Rows.Count - 1
                SustainabilityNewsOUT &= "<a href=""article-" & SustainabilityNewsDT.Rows(R).Item("News_Id").ToString.Trim & """>" & FormatFunctionsSimple.AutoFormatText(SustainabilityNewsDT.Rows(R).Item("Headline").ToString.Trim) & "</a>"
            Next
            News_Literal.Text = SustainabilityNewsOUT
        End If
    End Sub


End Class
