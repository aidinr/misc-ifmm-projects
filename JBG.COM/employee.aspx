﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="employee.aspx.vb" Inherits="employee" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="navigation-lower">
		<ul class="second-level">
				  <li class="navlink">      
				  <a href="investment-management">Investment Management </a></li>
				  <li class="navlink">      
				  <a href="development">Development</a></li>
				  <li class="navlink">      
				  <a href="asset-management">Asset Management</a></li>		  
				  <li class="navlink">      
				  <a href="organization">Organization</a></li>
				  <li class="navlink">      
				  <a href="affordable-housing">Affordable Housing</a></li>	  
				  <li class="navlink">      
				  <a href="awards-recognition">Awards</a></li>
				  <li class="navlink">      
				  <a href="public-art">Public Art</a></li>	  
			</ul>
 </div>

  <div id="content">
	<div class="inner-content" >
<div id="main" class="container_12">

  <div class="grid_4 page-header">
    <h1>Organization</h1>
  </div>
  <div class="clear"></div>
  <div class="quicklinks">
    <div class="qlink float-left">
      <a href="/organization">BACK TO ORGANIZATION</a>
    </div>
  </div>
  <div class="grid_4 sidebar">
    <h1 class="contact-name"><%=e_name%></h1>
  </div>

  <div class="grid_4">
    <div class="employee">
      <h3 class="contact-title"><%=e_position%></h3>
      <h4 class="contact-department"><asp:Literal ID="e_departments_label" runat="server"></asp:Literal></h4>
    </div>
  </div>

<!--
  <div class="grid_4 emp">
        <asp:Literal ID="e_isowner_label" runat="server"></asp:Literal>
  </div>
-->

  <div class="clear"></div>
  <div class="grid_12">

      <div class="grid_4 empImg"> 
      <asp:Literal ID="e_Image_Label" runat="server"></asp:Literal>
      </div> 

      <div class="grid_4 empBio"> 
          <p>
              <asp:Literal ID="e_description" runat="server"></asp:Literal>
          </p>
          <%If e_email <> "" %>
        <a href="mailto:<%=e_email%>" class="email-link"><%=e_email%></a>
          <%End if %>
        <!--
        <a class="phone-link"></a>
        -->
        <a href="/vcard.aspx?p=<%=e_id%>" class="vcard-link">vCard</a>
      </div>
  
      <div class="clear"></div>
    </div>
  </div>
  </div>

 </div>


</asp:Content>

