﻿Imports System.Data

Partial Class awards_recognition
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Public SelectedYear As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim DistinctYearsSQL As String = ""
        DistinctYearsSQL &= "select distinct year(Post_Date) year from ipm_awards "
        DistinctYearsSQL &= "Where Show = 1 and Post_Date <= getdate() and Pull_Date >= getdate() "
        DistinctYearsSQL &= "Order By year(Post_Date) Desc "
        Dim DistinctYearsDT As New DataTable
        DistinctYearsDT = DBFunctions.GetDataTable(DistinctYearsSQL)
        Dim DistinctYearsOUT As String = ""
        If DistinctYearsDT.Rows.Count > 0 Then
            DistinctYearsOUT &= "<ul class=""third-level padLeft"">"
            For R As Integer = 0 To DistinctYearsDT.Rows.Count - 1
                DistinctYearsOUT &= "<li><a class=""award-link"" href=""awards-recognition-" & DistinctYearsDT.Rows(R).Item("year").ToString.Trim & """>" & DistinctYearsDT.Rows(R).Item("year").ToString.Trim & "</a></li>"
            Next
            DistinctYearsOUT &= "</ul>"
            DistinctYears_Literal.Text = DistinctYearsOUT
        End If

        If Not Request.QueryString("year") Is Nothing Then
            SelectedYear = Request.QueryString("year")
            SelectedYear_Literal.Text = SelectedYear
        Else
            'Response.Status = "307 Temporary Redirect"
            'Response.AddHeader("Location", "awards-recognition-" & DistinctYearsDT.Rows(0).Item("year").ToString.Trim & "")
            Response.Redirect("awards-recognition-" & DistinctYearsDT.Rows(0).Item("year").ToString.Trim & "")
        End If
        

        Dim AwardsSQL As String = ""
        AwardsSQL &= "select "
        AwardsSQL &= "a.Awards_ID, a.Headline, p.Name, a.PublicationTitle, a.Post_Date, "
        AwardsSQL &= "p.ProjectID, v.Item_Value, p.Available, F1.Item_Value as SeoURL  "
        AwardsSQL &= "from ipm_awards a "
        AwardsSQL &= "join ipm_awards_related_projects r on a.Awards_Id = r.Awards_ID "
        AwardsSQL &= "join ipm_project p on r.ProjectID = p.ProjectID "
        AwardsSQL &= "left join ipm_project_field_value v on p.ProjectID = v.ProjectId and v.Item_Id = (select item_id from ipm_project_field_desc where Item_Tag = 'IDAM_PPUBLIC' ) "
        AwardsSQL &= "Left join ipm_project_field_value F1 on p.ProjectID = F1.ProjectID and F1.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SEO_URL') "
        AwardsSQL &= "Where a.Show = 1 and a.Post_Date <= getdate() and a.Pull_Date >= getdate() and year(a.Post_Date) =  '" & SelectedYear & "' "
        AwardsSQL &= "Order By a.Post_Date Desc, a.Headline, p.Name, a.PublicationTitle "
        Dim AwardsDT As New DataTable
        AwardsDT = DBFunctions.GetDataTable(AwardsSQL)

        Dim UniqueHeadlineDT As New DataTable
        UniqueHeadlineDT = AwardsDT.DefaultView.ToTable(True, "Headline")

        Dim UniqueProjectsDT As DataTable

        Dim AwardsOUT As String = ""

        If UniqueHeadlineDT.Rows.Count > 0 Then
            Dim GroupedAwardsDT As DataTable
            For H As Integer = 0 To UniqueHeadlineDT.Rows.Count - 1

                AwardsDT.DefaultView.RowFilter = "Headline = '" & UniqueHeadlineDT.Rows(H).Item("Headline").Replace("'", "''") & "'"
                Dim asdt As New DataTable
                asdt = AwardsDT.DefaultView.ToTable

                UniqueProjectsDT = New DataTable
                UniqueProjectsDT = asdt.DefaultView.ToTable(True, "ProjectID")

                For P As Integer = 0 To UniqueProjectsDT.Rows.Count - 1
                    AwardsDT.DefaultView.RowFilter = "Headline = '" & UniqueHeadlineDT.Rows(H).Item("Headline").Replace("'", "''") & "' and ProjectID = '" & UniqueProjectsDT.Rows(P).Item("ProjectID") & "'"
                    GroupedAwardsDT = New DataTable
                    GroupedAwardsDT = AwardsDT.DefaultView.ToTable

                    If GroupedAwardsDT.Rows.Count > 0 Then
                        AwardsOUT &= "<div class=""awardsItem"">"
                        AwardsOUT &= "<h3>" & formatfunctionssimple.AutoFormatText(GroupedAwardsDT.Rows(0).Item("Headline").ToString.Trim) & "</h3>"
                        Dim ProjectURL As String = ""
                        If GroupedAwardsDT.Rows(0).Item("SeoURL").ToString.Trim <> "" Then
                            ProjectURL = GroupedAwardsDT.Rows(0).Item("SeoURL").ToString.Trim
                        Else
                            ProjectURL = GroupedAwardsDT.Rows(0).Item("ProjectID").ToString.Trim
                        End If
                        If GroupedAwardsDT.Rows(0).Item("Item_Value").ToString.Trim <> "" Then
                            AwardsOUT &= "<h4>"
                            AwardsOUT &= "<a href=""" & ProjectURL & "-property-gallery"">" & formatfunctionssimple.AutoFormatText(GroupedAwardsDT.Rows(0).Item("Name").ToString.Trim) & "</a>"
                            AwardsOUT &= "</h4>"
                        Else
                            AwardsOUT &= "<h4>" & formatfunctionssimple.AutoFormatText(GroupedAwardsDT.Rows(0).Item("Name").ToString.Trim) & "</h4>"
                        End If

                        For R As Integer = 0 To GroupedAwardsDT.Rows.Count - 1
                            AwardsOUT &= formatfunctionssimple.AutoFormatText(GroupedAwardsDT.Rows(R).Item("PublicationTitle").ToString.Trim) & "<br/>"
                        Next
                        AwardsOUT &= "</div>"
                    End If

                Next

            Next

            Awards_Literal.Text = AwardsOUT

        End If





        'Dim AwardsOUT As String = ""
        'If AwardsDT.Rows.Count > 0 Then
        '    AwardsOUT &= ""
        '    For R As Integer = 0 To AwardsDT.Rows.Count - 1

        '        Dim ProjectURL As String = ""
        '        If AwardsDT.Rows(R).Item("SeoURL").ToString.Trim <> "" Then
        '            ProjectURL = AwardsDT.Rows(R).Item("SeoURL").ToString.Trim
        '        Else
        '            ProjectURL = AwardsDT.Rows(R).Item("ProjectID").ToString.Trim
        '        End If
        '        AwardsOUT &= "<h3>" & formatfunctionssimple.AutoFormatText(AwardsDT.Rows(R).Item("Headline").ToString.Trim) & "</h3>"
        '        If AwardsDT.Rows(R).Item("Item_Value").ToString.Trim <> "" Then
        '            AwardsOUT &= "<h4>"
        '            AwardsOUT &= "<a href=""" & ProjectURL & "-property-gallery"">" & formatfunctionssimple.AutoFormatText(AwardsDT.Rows(R).Item("Name").ToString.Trim) & "</a>"
        '            AwardsOUT &= "</h4>"
        '        Else
        '            AwardsOUT &= "<h4>" & formatfunctionssimple.AutoFormatText(AwardsDT.Rows(R).Item("Name").ToString.Trim) & "</h4>"
        '        End If
        '        AwardsOUT &= formatfunctionssimple.AutoFormatText(AwardsDT.Rows(R).Item("PublicationTitle").ToString.Trim) & "<br/>"
        '    Next
        '    Awards_Literal.Text = AwardsOUT
        'End If

    End Sub



End Class
