﻿Imports System.Data

Partial Class properties
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Public HasCategory As Boolean = False
    Public SelectedCategory As String = ""
    Public SelectedCategoryIndex As Integer
    Public SelectedCategoryHeader1 As String = ""
    Public SelectedCategoryHeader2 As String = ""
    Public SelectedCategoryIntro As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Request.QueryString("category") Is Nothing Then
            HasCategory = True
            SelectedCategory = Request.QueryString("category")
        End If

        If SelectedCategory = "development" And IDAMFunctions.IsLoggedIn = False Then
            Response.Redirect("properties")
        End If

        Dim tiii As Integer = 0

        Dim ProjectsSQL As String = ""
        ProjectsSQL &= "select p.ProjectID,  isnull(nullif(n.item_value, '' ),p.name) ProjectName,P.Description as ProjectDescription,P.DescriptionMedium , t.item_value As Categories, LTRIM(RTRIM(S.Name)) as ProjectState, P.State_ID, P.City, F1.Item_Value as SeoURL "
        ProjectsSQL &= "from ipm_project p "
        ProjectsSQL &= "left join ipm_project_field_value v2 on p.ProjectID = v2.ProjectID and v2.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_INDEV') "
        If SelectedCategory <> "development" Then
            ProjectsSQL &= "join ipm_project_field_value v on p.ProjectID = v.ProjectID and v.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_PPUBLIC') and v.Item_value = '1' "
        End If
        ProjectsSQL &= "left join ipm_project_field_value n on p.ProjectID = n.ProjectID and n.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SHORTNAME') "
        ProjectsSQL &= "join ipm_project_field_value t on p.ProjectID = t.ProjectID and t.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_PROJECTTYPE') "
        ProjectsSQL &= "join ipm_project_field_value l on p.ProjectID = l.ProjectID and l.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'iDAM_LDHEAD') "
        ProjectsSQL &= "Left join ipm_project_field_value F1 on p.ProjectID = F1.ProjectID and F1.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SEO_URL') "
        ProjectsSQL &= "join IPM_STATE S ON S.State_id = p.State_id "
        ProjectsSQL &= "where 1=1 "
        ProjectsSQL &= "and p.Available = 'Y' "
        If SelectedCategory = "development" Then
            ProjectsSQL &= "and  ISNULL(NULLIF(v2.Item_value,''),'0') = '1' "
        Else
            ProjectsSQL &= "and  ISNULL(NULLIF(v2.Item_value,''),'0') <> '1' "
        End If
        If SelectedCategory <> "development" Then
            If HasCategory = True Then
                If SelectedCategory.ToLower = "mixed-use" Then
                    ProjectsSQL &= "and charindex(',',LTRIM(RTRIM(t.Item_Value)))!= 0 "
                Else
                    ProjectsSQL &= "and charindex('" & SelectedCategory & "',LTRIM(RTRIM(t.Item_Value)))!= 0 "
                End If
            End If
        End If

        Dim ProjectsDT As New DataTable
        ProjectsDT = DBFunctions.GetDataTable(ProjectsSQL)

        Dim ProjectsOUT As String = ""
        Dim SelectProjectsOUT As String = ""
        Dim PartialProjectOUT As String = ""
        Dim FullProjectOUT As String = ""
        If ProjectsDT.Rows.Count > 0 Then
            ProjectsDT.DefaultView.Sort = "ProjectState ASC"
            Dim UniqueStatesDT As New DataTable
            UniqueStatesDT = ProjectsDT.DefaultView.ToTable(True, "ProjectState")
            'UniqueStatesDT.DefaultView.Sort = "ProjectState ASC"
            Dim GroupedProjectsDT As DataTable
            For S As Integer = 0 To UniqueStatesDT.Rows.Count - 1
                ProjectsOUT = ""
                SelectProjectsOUT = ""
                PartialProjectOUT = ""
                ProjectsOUT &= "<h2>" & UniqueStatesDT.Rows(S).Item("ProjectState").ToString.Trim & "  </h2>" & vbNewLine
                ProjectsOUT &= "<ul class=""pp 2-column menu_1 pp""> " & vbNewLine

                SelectProjectsOUT &= "<div class=""select-option"">" & vbNewLine
                SelectProjectsOUT &= "<span class=""click-select"">SELECT " & UniqueStatesDT.Rows(S).Item("ProjectState").ToString.Trim & "</span>" & vbNewLine
                SelectProjectsOUT &= "<ul class=""select-option"">" & vbNewLine

                GroupedProjectsDT = New DataTable
                ProjectsDT.DefaultView.RowFilter = "ProjectState = '" & UniqueStatesDT.Rows(S).Item("ProjectState") & "'"
                ProjectsDT.DefaultView.Sort = "ProjectName ASC"
                GroupedProjectsDT = ProjectsDT.DefaultView.ToTable
                For R As Integer = 0 To GroupedProjectsDT.Rows.Count - 1
                    Dim ProjectURL As String = ""
                    If GroupedProjectsDT.Rows(R).Item("SeoURL").ToString.Trim <> "" Then
                        ProjectURL = GroupedProjectsDT.Rows(R).Item("SeoURL").ToString.Trim
                    Else
                        ProjectURL = GroupedProjectsDT.Rows(R).Item("ProjectID").ToString.Trim
                    End If
                    tiii = tiii +1
                    ProjectsOUT &= "<li class=""tip-popper"">" & vbNewLine
                    ProjectsOUT &= "<a class=""pop"" rel=""#pop" & tiii & """ href=""" & ProjectURL & "-property-gallery"">" & formatfunctionssimple.AutoFormatText(GroupedProjectsDT.Rows(R).Item("ProjectName").ToString.Trim) & "</a>" & vbNewLine
                    ProjectsOUT &= "<div class=""box-info propList"" id=""pop" & tiii & """>" & vbNewLine
                    ProjectsOUT &= "<h3 class=""title ppTitle"">" & vbNewLine
                    ProjectsOUT &= "<span>" & formatfunctionssimple.AutoFormatText(GroupedProjectsDT.Rows(R).Item("ProjectName").ToString.Trim) & "</span>" & vbNewLine
                    ProjectsOUT &= "</h3>" & vbNewLine
                    ProjectsOUT &= "<div class=""inner-info"">" & vbNewLine
                    ProjectsOUT &= "<h2>" & GroupedProjectsDT.Rows(R).Item("City").ToString.Trim & ", " & GroupedProjectsDT.Rows(R).Item("State_ID").ToString.Trim & "</h2>" & vbNewLine
                    ProjectsOUT &= "<img src=""dynamic/image/always/project/best/230x120/92/ffffff/Center/" & GroupedProjectsDT.Rows(R).Item("ProjectID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
                    ProjectsOUT &= "<div class=""clear""></div>" & vbNewLine

                    If GroupedProjectsDT.Rows(R).Item("DescriptionMedium").ToString.Trim <> "" Then
                     ProjectsOUT &= "<p>" & formatfunctions.AutoFormatText(CMSFunctions.GetWords(GroupedProjectsDT.Rows(R).Item("DescriptionMedium").ToString.Trim, 40)) & "&#8230; <a href=""" & ProjectURL & "-property-gallery"">[More]</a></p>" & vbNewLine
                    End If

                    ProjectsOUT &= "</div>" & vbNewLine
                    ProjectsOUT &= "</div>" & vbNewLine
                    ProjectsOUT &= "</li>" & vbNewLine
                    SelectProjectsOUT &= "<li><a href=""" & ProjectURL & "-property-gallery"">" & GroupedProjectsDT.Rows(R).Item("ProjectName").ToString.Trim & "</a></li>" & vbNewLine
                Next
                ProjectsOUT &= "</ul>" & vbNewLine
                SelectProjectsOUT &= "</ul>" & vbNewLine
                SelectProjectsOUT &= "</div>" & vbNewLine
                SelectProjectsOUT &= "<div class=""clear"">&nbsp;</div>" & vbNewLine
                SelectProjectsOUT &= "<hr />" & vbNewLine
                PartialProjectOUT = ProjectsOUT + SelectProjectsOUT
                FullProjectOUT &= PartialProjectOUT
            Next
            Projects_Literal.Text = FullProjectOUT
        End If

        Dim NewsSQL As String = ""
        NewsSQL &= "select top 8 News_Id, Headline from ipm_news where Show = 1 and Post_Date <= getdate() and Pull_Date >= getdate() order by post_date desc"
        Dim NewsNewsDT As New DataTable
        NewsNewsDT = DBFunctions.GetDataTable(NewsSQL)
        Dim SustainabilityNewsOUT As String = ""
        If NewsNewsDT.Rows.Count > 0 Then
            For R As Integer = 0 To NewsNewsDT.Rows.Count - 1
                SustainabilityNewsOUT &= "<a href=""article-" & NewsNewsDT.Rows(R).Item("News_Id").ToString.Trim & """>" & formatfunctionssimple.AutoFormatText(NewsNewsDT.Rows(R).Item("Headline").ToString.Trim) & "</a>"
            Next
            News_Literal.Text = SustainabilityNewsOUT
        End If

    End Sub

End Class
