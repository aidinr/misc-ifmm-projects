﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="affordable-housing.aspx.vb" Inherits="affordable_housing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<div id="navigation-lower">
			<ul class="second-level">
				  <li class="navlink">
				  <a href="investment-management">Investment Management </a></li>
				  <li class="navlink">
				  <a href="development">Development</a></li>
				  <li class="navlink">
				  <a href="asset-management">Asset Management</a></li>
				  <li class="navlink">
				  <a href="organization">Organization</a></li>
				  <li class="navlink">
				  <a href="affordable-housing">Affordable Housing</a></li>
				  <li class="navlink">
				  <a href="awards-recognition">Awards</a></li>
				  <li class="navlink">
				  <a href="public-art">Public Art</a></li>
			</ul>
	   </div>
	   <div id="content">
		<div class="inner-content">
				<div class="container_12" id="main">
				<div class="grid_12 page-header">
					<h1>Affordable Housing</h1>
				</div>
				<div class="clear"></div>
				<hr/>
				
                <div class="cmsGroup cmsName_Affordable_Housing_Intro">

 <!--
                    <div class="cmsGroupItem">
                    <div class="grid_4"></div>
			        <div class="grid_8">
                    <span>Item Heading</span><br />
			        <p class="cms cmsType_TextMulti cmsName_Content">
				    Content
			        </p>
                    </div>
                    </div>
-->

<%  For i As Integer = 1 To CMSFunctions.CountTextSingle("Affordable_Housing_Intro_Item_" & "*" & "_Content")%>
                 <div class="cmsGroupItem">
                    <div class="grid_4">
			        <h1 class="cms cmsType_TextSingle cmsName_Affordable_Housing_Intro_Item_<%=i%>_Heading"></h1>
			        </div>
			        <div class="grid_8">
                    <h2 class="cms cmsType_TextSingle cmsName_Affordable_Housing_Intro_Item_<%=i%>_Content_Heading"></h2>
			        <p class="cms cmsType_TextMulti cmsName_Affordable_Housing_Intro_Item_<%=i%>_Content">
				    Content
			        </p>
                   </div>
                  </div>
                  <div class="clear"></div>
       <%Next %>
		  </div>
				
		</div>
		</div>
	   </div>
</asp:Content>

