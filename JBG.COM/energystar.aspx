﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="energystar.aspx.vb" Inherits="energystar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
		<ul class="second-level">
		  <li><a href="leed-team">TEAM</a></li><li><a href="leed">LEED</a></li>
		  <li><a href="energystar">ENERGY STAR</a></li>
		  <li><a href="commitment">COMMITMENT</a></li>
		</ul>
   </div>
   <div id="content">
	<div class="inner-content">
		<div class="container_12"  id="main">
				<div class="grid_12 page-header sidebar">
				<h1>Energy Star&reg;</h1>
				</div>
				<div class="clear"></div>
				<hr/>
				 <div class="grid_4">
				   <h1>
                   <img class="cms cmsType_Image cmsName_Energystar_Intro_Image_1" src="cms/data/Sized/liquid/130x/75/ffffff/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Energystar_Intro_Image_1")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Energystar_Intro_Image_1")%>" />
                   </h1>
				 </div>
				 <div class="grid_8">
					<p class="cms cmsType_TextMulti cmsName_Energystar_Intro_Content">
						Intro
					</p>
				 </div>
				<div class="clear"></div>
				<div class="grid_4"></div>
		      <div class="grid_8">
			<asp:Literal ID="Projects_Literal" runat="server"></asp:Literal>
		      </div>
		</div>
	</div>
   </div>

</asp:Content>

