﻿Imports System.Data
Partial Class organization
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Public HasFilters As Boolean = False
    Public FilterType As String = ""
    Public FilterValue As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Request.QueryString("filter") Is Nothing Then
            HasFilters = True
            FilterValue = Request.QueryString("filter")
            If FilterValue.Length > 1 Then
                FilterType = "ByGroup"
            Else
                FilterType = "ByLastName"
            End If
        End If


        If FilterValue = "management-committee" Then
            FilterValue = "management-committees"
        End If

        Dim tiii As Integer = 0
        Dim FirstLetterSQL As String = ""
        FirstLetterSQL &= "select LEFT(u.LastName, 1) AS Letter "
        FirstLetterSQL &= "from ipm_user u "
        FirstLetterSQL &= "join ipm_user_field_value v on u.UserID = v.User_ID and Item_value = '1' "
        FirstLetterSQL &= "join ipm_user_field_desc d on v.Item_ID = d.Item_ID and d.Item_Tag = 'IDAM_ACTIVE' "
        FirstLetterSQL &= "Left Join ipm_user_field_value v2 on u.UserID = v2.User_ID and v2.Item_ID = 21611765 "
        FirstLetterSQL &= "Join ipm_user_field_value dv1 on u.UserId = dv1.User_ID "
        FirstLetterSQL &= "join ipm_user_field_desc dd1 on dv1.item_id = dd1.item_id and dd1.Item_tag = 'IDAM_DEPT1' "
        FirstLetterSQL &= "Join ipm_user_field_value dv2 on u.UserId = dv2.User_ID "
        FirstLetterSQL &= "join ipm_user_field_desc dd2 on dv2.item_id = dd2.item_id and dd2.Item_tag = 'IDAM_DEPT2' "
        FirstLetterSQL &= "Join ipm_user_field_value dv3 on u.UserId = dv3.User_ID "
        FirstLetterSQL &= "join ipm_user_field_desc dd3 on dv3.item_id = dd3.item_id and dd3.Item_tag = 'IDAM_DEPT3' "
        FirstLetterSQL &= "Join ipm_user_field_value dv4 on u.UserId = dv4.User_ID "
        FirstLetterSQL &= "join ipm_user_field_desc dd4 on dv4.item_id = dd4.item_id and dd4.Item_tag = 'IDAM_DEPT4' "
        FirstLetterSQL &= "where u.Active = 'Y' "
        FirstLetterSQL &= "group by LEFT(u.LastName, 1) "
        FirstLetterSQL &= "order by LEFT(u.LastName, 1) "
        Dim FirstLetterDT As New DataTable
        FirstLetterDT = DBFunctions.GetDataTable(FirstLetterSQL)
        Dim FirstLetterOUT As String = ""
        If FirstLetterDT.Rows.Count > 0 Then
            FirstLetterOUT &= "<ul class=""orgAlpha top"">"
            For R As Integer = 0 To FirstLetterDT.Rows.Count - 1
                FirstLetterOUT &= "<li><a href=""organization-" & FirstLetterDT.Rows(R).Item("Letter").ToString.Trim & """>" & FirstLetterDT.Rows(R).Item("Letter").ToString.Trim & "</a></li>"
            Next
            FirstLetterOUT &= "</ul>"
            If HasFilters = False Then
                FirstLetter_Literal.Text = FirstLetterOUT
            Else
                FirstLetterHeader_Literal.Text = FirstLetterOUT
            End If
        End If

        If HasFilters = True Then
            FilterValue_Literal.Text = ""
            Dim UsersSQL As String = ""
            UsersSQL &= "select distinct "
            UsersSQL &= "u.UserID, u.FirstName, u.LastName, u.Position, u.Phone, u.Email, v2.Item_Value dept_head "
            UsersSQL &= "from ipm_user u "
            UsersSQL &= "join ipm_user_field_value v on u.UserID = v.User_ID and Item_value = '1' "
            UsersSQL &= "join ipm_user_field_desc d on v.Item_ID = d.Item_ID and d.Item_Tag = 'IDAM_ACTIVE' "
            UsersSQL &= "Left Join ipm_user_field_value v2 on u.UserID = v2.User_ID and v2.Item_ID = 21611765 "
            If FilterType = "ByGroup" Then
                UsersSQL &= "Join ipm_user_field_value g on u.UserId = g.User_ID  and g.Item_Id = (Select Item_id from ipm_user_field_desc where REPLACE(Item_tag, ' ', '') = 'IDAM_Group_" & FilterValue.Replace("-", "") & "') and g.Item_Value = '1' "
            End If
            UsersSQL &= "where 1=1"
            If FilterType = "ByLastName" Then
                UsersSQL &= "and LEFT(u.LastName, 1) = '" & FilterValue & "' "
            End If
            UsersSQL &= "and u.Active = 'Y' "
            UsersSQL &= "Order by u.LastName, u.FirstName "
            Dim UsersDT As New DataTable
            UsersDT = DBFunctions.GetDataTable(UsersSQL)
            Dim UsersOUT As String = ""
            If UsersDT.Rows.Count > 0 Then
                UsersOUT &= "<ul>"
                For R As Integer = 0 To UsersDT.Rows.Count - 1
                    tiii = tiii +1
                    UsersOUT &= "<li class=""person-organization"">"
                    UsersOUT &= "<br/><a class=""person popper"" rel=""#pop" & tiii & """ href=""employee-" & UsersDT.Rows(R).Item("UserID").ToString.Trim & """ >" & UsersDT.Rows(R).Item("FirstName").ToString.Trim & " " & UsersDT.Rows(R).Item("LastName").ToString.Trim & "</a>"
                    UsersOUT &= "<div class=""box-info"" id=""pop" & tiii & """>"
                    UsersOUT &= "<h3 class=""title ppTitle"">"
                    UsersOUT &= "<span>" & formatfunctionssimple.AutoFormatText(UsersDT.Rows(R).Item("Position").ToString.Trim) & "</span>"
                    UsersOUT &= "</h3>"
                    UsersOUT &= "<div class=""inner-info"">"
                    UsersOUT &= "<h2>" & UsersDT.Rows(R).Item("FirstName").ToString.Trim & " " & UsersDT.Rows(R).Item("LastName").ToString.Trim & "</h2>"
                    UsersOUT &= "<div class=""email-link"">"
                    UsersOUT &= "<a href=""mailto:" & UsersDT.Rows(R).Item("Email").ToString.Trim & """>" & UsersDT.Rows(R).Item("Email").ToString.Trim & "</a>"
                    UsersOUT &= "</div>"
                    UsersOUT &= "<a class=""vcard-link"" href=""vcard.aspx?p=" & UsersDT.Rows(R).Item("UserID").ToString.Trim & """>vCard</a>"
                    UsersOUT &= "</div>"
                    UsersOUT &= "</div>"
                    UsersOUT &= "</li>"
                Next
                UsersOUT &= "</ul>"
                Users_Literal.Text = UsersOUT
            End If

        End If


    End Sub


End Class
