Imports System.Web
Imports System.Data.SqlClient
Imports System.Data

Partial Class sitemap
    Inherits System.Web.UI.Page
    Protected Sub _Index_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Response.AddHeader("Content-type", "application/xml")
        Dim out As String = "<?xml version=""1.0"" encoding=""UTF-8"" ?>" & vbNewLine
        out &= "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">" & vbNewLine
        Dim url() As String = { _
"http://www.jbg.com/", _
"http://www.jbg.com/about-jbg", _
"http://www.jbg.com/investment-management", _
"http://www.jbg.com/development", _
"http://www.jbg.com/asset-management", _
"http://www.jbg.com/organization", _
"http://www.jbg.com/organization-senior-advisors", _
"http://www.jbg.com/organization-executive-committee", _
"http://www.jbg.com/organization-management-committee", _
"http://www.jbg.com/organization-owners", _
"http://www.jbg.com/organization-key-personnel", _
"http://www.jbg.com/affordable-housing", _
"http://www.jbg.com/public-art", _
"http://www.jbg.com/properties-map", _
"http://www.jbg.com/properties", _
"http://www.jbg.com/jbg-cares", _
"http://www.jbg.com/jbg-cares-partners", _
"http://www.jbg.com/sustainability", _
"http://www.jbg.com/leed", _
"http://www.jbg.com/energystar", _
"http://www.jbg.com/initiatives", _
"http://www.jbg.com/employment", _
"http://www.jbg.com/contact-us", _
"http://www.jbg.com/login", _
"http://www.jbg.com/privacy-policy" _
}
        'Static Pages
        Dim yesterday As String = Date.Now.AddDays(-1).ToString("yyyy-MM-dd")
        For i As Integer = 0 To url.Length - 1
            out &= "<url><loc>" & url(i).Trim & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
        Next
        'Properties Categories
        For i As Integer = 1 To CMSFunctions.CountTextSingle("Properties_Categories_Item_" & "*" & "_Heading_1")
            Dim CatLink As String = ""
            CatLink = CMSFunctions.GetLink(Server.MapPath("~") & "cms\data\Link\", "Properties_Categories_Item_" & i & "_Link", "href")
            out &= "<url><loc>http://www.jbg.com/" & CatLink & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
        Next
        'Organization By Last Name
        Dim FirstLetterSQL As String = ""
        FirstLetterSQL &= "select LEFT(u.LastName, 1) AS Letter "
        FirstLetterSQL &= "from ipm_user u "
        FirstLetterSQL &= "join ipm_user_field_value v on u.UserID = v.User_ID and Item_value = '1' "
        FirstLetterSQL &= "join ipm_user_field_desc d on v.Item_ID = d.Item_ID and d.Item_Tag = 'IDAM_ACTIVE' "
        FirstLetterSQL &= "Left Join ipm_user_field_value v2 on u.UserID = v2.User_ID and v2.Item_ID = 21611765 "
        FirstLetterSQL &= "Join ipm_user_field_value dv1 on u.UserId = dv1.User_ID "
        FirstLetterSQL &= "join ipm_user_field_desc dd1 on dv1.item_id = dd1.item_id and dd1.Item_tag = 'IDAM_DEPT1' "
        FirstLetterSQL &= "Join ipm_user_field_value dv2 on u.UserId = dv2.User_ID "
        FirstLetterSQL &= "join ipm_user_field_desc dd2 on dv2.item_id = dd2.item_id and dd2.Item_tag = 'IDAM_DEPT2' "
        FirstLetterSQL &= "Join ipm_user_field_value dv3 on u.UserId = dv3.User_ID "
        FirstLetterSQL &= "join ipm_user_field_desc dd3 on dv3.item_id = dd3.item_id and dd3.Item_tag = 'IDAM_DEPT3' "
        FirstLetterSQL &= "Join ipm_user_field_value dv4 on u.UserId = dv4.User_ID "
        FirstLetterSQL &= "join ipm_user_field_desc dd4 on dv4.item_id = dd4.item_id and dd4.Item_tag = 'IDAM_DEPT4' "
        FirstLetterSQL &= "where u.Active = 'Y' "
        FirstLetterSQL &= "group by LEFT(u.LastName, 1) "
        FirstLetterSQL &= "order by LEFT(u.LastName, 1) "
        Dim FirstLetterDT As New DataTable
        FirstLetterDT = DBFunctions.GetDataTable(FirstLetterSQL)
        Dim FirstLetterOUT As String = ""
        If FirstLetterDT.Rows.Count > 0 Then
            For R As Integer = 0 To FirstLetterDT.Rows.Count - 1
                out &= "<url><loc>http://www.jbg.com/organization-" & FirstLetterDT.Rows(R).Item("Letter").ToString.Trim & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
            Next
        End If
        'Awards Years
        Dim DistinctYearsSQL As String = ""
        DistinctYearsSQL &= "select distinct year(Post_Date) year from ipm_awards "
        DistinctYearsSQL &= "Where Show = 1 and Post_Date <= getdate() and Pull_Date >= getdate() "
        DistinctYearsSQL &= "Order By year(Post_Date) Desc "
        Dim DistinctYearsDT As New DataTable
        DistinctYearsDT = DBFunctions.GetDataTable(DistinctYearsSQL)
        Dim DistinctYearsOUT As String = ""
        If DistinctYearsDT.Rows.Count > 0 Then
            DistinctYearsOUT &= "<ul class=""third-level padLeft"">"
            For R As Integer = 0 To DistinctYearsDT.Rows.Count - 1
                out &= "<url><loc>http://www.jbg.com/awards-recognition-" & DistinctYearsDT.Rows(R).Item("year").ToString.Trim & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
            Next
        End If
        'General News Years
        Dim GeneralNewsDistinctYearsSQL As String = ""
        GeneralNewsDistinctYearsSQL &= "Select distinct year(Post_Date) as [year] from ipm_news "
        GeneralNewsDistinctYearsSQL &= "Where Post_Date <= getdate() and Pull_Date >= getdate() "
        GeneralNewsDistinctYearsSQL &= "and Show = 1 "
        GeneralNewsDistinctYearsSQL &= "and Type in (select Type_Id from ipm_news_type Where Show = 1) "
        GeneralNewsDistinctYearsSQL &= "Order by YEAR(Post_DATE) DESC"
        Dim GeneralNewsDistinctYearsDT As New DataTable
        GeneralNewsDistinctYearsDT = DBFunctions.GetDataTable(GeneralNewsDistinctYearsSQL)
        Dim GeneralNewsDistinctYearsOUT As String = ""
        If GeneralNewsDistinctYearsDT.Rows.Count > 0 Then
            For R As Integer = 0 To GeneralNewsDistinctYearsDT.Rows.Count - 1
                out &= "<url><loc>http://www.jbg.com/news-" & GeneralNewsDistinctYearsDT.Rows(R).Item("year").ToString.Trim & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
            Next
        End If
        'Press Releases Years
        Dim PressReleaseDistinctYearsSQL As String = ""
        PressReleaseDistinctYearsSQL &= "Select distinct year(Post_Date) as [year] from ipm_news "
        PressReleaseDistinctYearsSQL &= "Where Post_Date <= getdate() and Pull_Date >= getdate() "
        PressReleaseDistinctYearsSQL &= "and Show = 1 "
        PressReleaseDistinctYearsSQL &= "and Type in (8) "
        PressReleaseDistinctYearsSQL &= "Order by YEAR(Post_DATE) DESC"
        Dim PressReleaseDistinctYearsDT As New DataTable
        PressReleaseDistinctYearsDT = DBFunctions.GetDataTable(PressReleaseDistinctYearsSQL)
        Dim PressReleaseDistinctYearsOUT As String = ""
        If PressReleaseDistinctYearsDT.Rows.Count > 0 Then
            For R As Integer = 0 To PressReleaseDistinctYearsDT.Rows.Count - 1
                out &= "<url><loc>http://www.jbg.com/news-press-releases-" & PressReleaseDistinctYearsDT.Rows(R).Item("year").ToString.Trim & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
            Next
        End If
        'Articles Years
        Dim ArticlesDistinctYearsSQL As String = ""
        ArticlesDistinctYearsSQL &= "Select distinct year(Post_Date) as [year] from ipm_news "
        ArticlesDistinctYearsSQL &= "Where Post_Date <= getdate() and Pull_Date >= getdate() "
        ArticlesDistinctYearsSQL &= "and Show = 1 "
        ArticlesDistinctYearsSQL &= "and Type in (9) "
        ArticlesDistinctYearsSQL &= "Order by YEAR(Post_DATE) DESC"
        Dim ArticlesDistinctYearsDT As New DataTable
        ArticlesDistinctYearsDT = DBFunctions.GetDataTable(ArticlesDistinctYearsSQL)
        Dim ArticlesDistinctYearsOUT As String = ""
        If ArticlesDistinctYearsDT.Rows.Count > 0 Then
            For R As Integer = 0 To ArticlesDistinctYearsDT.Rows.Count - 1
                out &= "<url><loc>http://www.jbg.com/news-articles-" & ArticlesDistinctYearsDT.Rows(R).Item("year").ToString.Trim & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
            Next
        End If
        'News
        Dim NewsSQL As String = ""
        NewsSQL &= "select TOP 25 News_Id, convert(char(10),Post_Date,101) as newsdate , Headline "
        NewsSQL &= "from ipm_news "
        NewsSQL &= "Where Post_Date <= getdate() and Pull_Date >= getdate() "
        NewsSQL &= "and Show = 1 "
        NewsSQL &= "and Type in (select Type_Id from ipm_news_type Where Show = 1) "
        NewsSQL &= "Order by Post_DATE DESC "
        Dim NewsDT As New DataTable
        NewsDT = DBFunctions.GetDataTable(NewsSQL)
        If NewsDT.Rows.Count > 0 Then
            For R As Integer = 0 To NewsDT.Rows.Count - 1
                out &= "<url><loc>http://www.jbg.com/article-" & NewsDT.Rows(R).Item("News_Id").ToString.Trim & "</loc><lastmod>" & yesterday & "</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>" & vbNewLine
            Next
        End If
        'Projects
        Dim ProjectsSQL As String = ""
        ProjectsSQL &= "select p.ProjectID,  isnull(nullif(n.item_value, '' ),p.name) ProjectName,P.Description as ProjectDescription,P.DescriptionMedium , t.item_value As Categories, LTRIM(RTRIM(S.Name)) as ProjectState, P.State_ID, P.City, F1.Item_Value as SeoURL, p.update_date "
        ProjectsSQL &= "from ipm_project p "
        ProjectsSQL &= "left join ipm_project_field_value v2 on p.ProjectID = v2.ProjectID and v2.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_INDEV') "
        ProjectsSQL &= "join ipm_project_field_value v on p.ProjectID = v.ProjectID and v.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_PPUBLIC') and v.Item_value = '1' "
        ProjectsSQL &= "left join ipm_project_field_value n on p.ProjectID = n.ProjectID and n.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SHORTNAME') "
        ProjectsSQL &= "join ipm_project_field_value t on p.ProjectID = t.ProjectID and t.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_PROJECTTYPE') "
        ProjectsSQL &= "join ipm_project_field_value l on p.ProjectID = l.ProjectID and l.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'iDAM_LDHEAD') "
        ProjectsSQL &= "Left join ipm_project_field_value F1 on p.ProjectID = F1.ProjectID and F1.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SEO_URL') "
        ProjectsSQL &= "join IPM_STATE S ON S.State_id = p.State_id "
        ProjectsSQL &= "where 1=1 "
        ProjectsSQL &= "and p.Available = 'Y' "
        ProjectsSQL &= "and  ISNULL(NULLIF(v2.Item_value,''),'0') <> '1' "
        Dim ProjectsDT As New DataTable
        ProjectsDT = DBFunctions.GetDataTable(ProjectsSQL)
        If ProjectsDT.Rows.Count > 0 Then
            For R As Integer = 0 To ProjectsDT.Rows.Count - 1
                Dim ProjectURL As String = ""
                If ProjectsDT.Rows(R).Item("SeoURL").ToString.Trim <> "" Then
                    ProjectURL = ProjectsDT.Rows(R).Item("SeoURL").ToString.Trim
                Else
                    ProjectURL = ProjectsDT.Rows(R).Item("ProjectID").ToString.Trim
                End If
                out &= "<url><loc>http://www.jbg.com/" & ProjectURL & "-property-gallery</loc><lastmod>" & ProjectsDT.Rows(R)("update_date").ToString.Trim & "</lastmod><changefreq>weekly</changefreq><priority>1.0</priority></url>" & vbNewLine
            Next
        End If
        out &= "</urlset>"
        Response.Write(out)
    End Sub

End Class