﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="article.aspx.vb" Inherits="article" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
		<ul class="second-level">                
			<li><a href="news-press-releases">Press Releases</a></li>
			<li><a href="news-articles">Articles</a></li>
        </ul>
   </div>
   <div id="content">
	<div class="inner-content">		
		<div class="container_12"  id="main">
        <div class="newsbop">
	        <div id="module" class="modulebox leading-module">
                <div class="grid_12 page-header sidebar">
                    <h1>JBG in the News</h1>
                </div>
                <div class="clear"></div>
				<asp:Literal ID="DistinctYears_Literal" runat="server"></asp:Literal>
                <hr/>
                <div class="quicklinks" id="ContentPlaceHolderDefault_aspcontainer_news_3_newslink">
                    <div class="qlink float-left">
                        <a href="news">&lt;&nbsp;&nbsp;&nbsp;Recent News</a>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="grid_4" id="ContentPlaceHolderDefault_aspcontainer_news_3_newsRight">
                <%If hasimahe = "1" Then%>
                <p class="newsRight">
                <img src="/dynamic/image/week/news/best/140x91/92/ffffff/West/<%=NewsID%>.jpg" alt=""/>
                </p>
                <%End if %>

                </div>
                <div class="grid_8 content" id="ContentPlaceHolderDefault_aspcontainer_news_3_articlediv">								
		            <h3 class="publication-info"><asp:Literal ID="NewsHeadline_Literal" runat="server"></asp:Literal></h3>
                    <h4><asp:Literal ID="SubHeadline_Literal" runat="server"></asp:Literal></h4>
		            <div class="copy">
			            <h4><asp:Literal ID="NewsDate_Literal" runat="server"></asp:Literal></h4>
                        <asp:Literal ID="News_Content" runat="server"></asp:Literal>
						
						<br/>
                        <hr/>

                        <div id="newscontact">

                        <%If Author <> "" Or Publication <> "" Then%>
                        <dl>
                        <%If Author <> "" Then%>
                        <dt>Author</dt>
                        <dd>
                        <%= Author%>
                        </dd>
                        <%End If%>
                        <%If Publication <> "" Then%>
                        <dt><br/>Publication</dt>
                        <dd>
                       <%= Publication%>
                       </dd>
                        <%End If%>
                        </dl>
                        <%End if %>

                        </div>

                        <div id="newsrelatedprojects">
                        <asp:Literal ID="NewsRelatedProjects_Literal" runat="server"></asp:Literal>
                        </div>

					</div>
				</div>
			</div>
		</div>    
	</div>
</div>
</div>	

</asp:Content>

