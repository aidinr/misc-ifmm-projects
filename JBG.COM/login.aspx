﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="login.aspx.vb" Inherits="login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
		<ul class="second-level">
			<li><a href="login">LOGIN</a></li>
		</ul>
   </div>
   <div id="content" class="login">
	<div class="inner-content" >		
		<div class="container_12"  id="main">
			<div class="grid_12 page-header">
			  <h1>Login</h1>
			</div>
			<div class="clear"></div>
			<hr/>
			<div class="grid_4"></div>
			<div class=" grid_8 content">
			<h2 class="cms cmsType_TextSingle cmsName_Login_Heading">Investor Login</h2>
			<p class="investorLoing cms cmsType_TextMulti cmsName_Login_Content"></p>
			</div>
			<div class="bar80 infobar">
			 <div class="grid_4 sidebar">
			  <h1 class="cms cmsType_TextSingle cmsName_Login_Link_Name">INTRALINKS</h1>
			 </div>
			 <div class="grid_8">
			  <a target="_blank" href="https://services.intralinks.com/logon.html?clientID=8818744706" class="cms cmsType_Link cmsName_Login_Link">Click Here to Login</a>
			 </div>
			</div>
			 <div class="clear"></div>
			</div>
		</div>
	</div>	

</asp:Content>

