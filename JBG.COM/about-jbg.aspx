﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="about-jbg.aspx.vb" Inherits="about_jbg" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

		<div id="navigation-lower">
			<ul class="second-level">
				  <li class="navlink">
				  <a href="investment-management">Investment Management </a></li>
				  <li class="navlink">
				  <a href="development">Development</a></li>
				  <li class="navlink">
				  <a href="asset-management">Asset Management</a></li>
				  <li class="navlink">
				  <a href="organization">Organization</a></li>
				  <li class="navlink">
				  <a href="affordable-housing">Affordable Housing</a></li>
				  <li class="navlink">
				  <a href="awards-recognition-2013">Awards</a></li>
				  <li class="navlink">
				  <a href="public-art">Public Art</a></li>
			</ul>
	   </div>
	  
       <%--<div id="resizable-background">
		<img src="assets/images/slides/bg1.jpg" alt="" />
	  </div>--%>

	   <div id="content">
		<div class="inner-content" style="height:600px;">
			<div class="container_12" id="main" style="height: 600px;">
				<div class="grid_12 page-header">
					<h1>EXCELLENCE IS OUR STANDARD</h1>
				</div>
				<div class="clear"></div>

				<hr/>

				<div class="grid_4">
					<h1>&nbsp;</h1>
				</div>

				<div class="grid_8">
					<h2>Creating and Enhancing Value for more than 50 Years</h2>
					<p>It is the mission of The JBG Companies to be a world-class investor, owner, developer and manager of real estate properties in the Washington Metropolitan Area. We seek to generate superior, risk-adjusted returns for our investors while actively mitigating risk. JBG is committed to being an engaged and responsible member of the communities in which we operate. <br/>
					<br/>

					JBG invests almost exclusively in urban-infill, transit-oriented developments, and holds a diverse portfolio that encompasses over 23.6 million square feet of office, residential, hotel and retail space. Having deep experience across all product types enables us to diversify our investments, take advantage of market opportunities and mitigate risk through market cycles. We apply skill, experience and rigor to creating and preserving outstanding value for our investors.</p>
					<p>JBG believes that each development we undertake should enrich a community. We are dedicated to developing active, sustainable communities, advancing affordable housing and promoting public art.</p>
				</div>
				<div class="clear"></div>

				<div class="grid_4">
               <h1>Watch the JBG Video</h1>
            </div>
				<div class="grid_8">
            	<a href="https://vimeo.com/73310573" class="videoLink"><img src="assets/images/jbgVid.png" alt="" /><em></em></a>
               <br /><br />
            </div>
				<div class="clear"></div>

				<div class="grid_4">
					<h1>JBG at a Glance</h1>
				</div>
				<div class="grid_8">
					<div class="grid_3 omega">
						<h2>Founded</h2>
						<p>1960</p>
						<h2>Current Portfolio*</h2>
						<p>Office: 10.9 million square feet
						<br/>
						Residential: 7,600 multifamily units
						<br/>
						Hotel: 4,725 hotel rooms
						<br/>
						Retail: 2.9 million square feet
						<br/>
						<br/>
						<small>*As of February 2015</small></p>
					  <!--
						<h2></h2>
						<p><br/ /></p>
					  -->
					</div>
					<div class="grid_4 alpha">
						<h2>Investment Funds</h2>
						<p class="funds">
							<span class="i1">Fund I</span><span class="i2">1999</span><span class="i3">$28 million</span><br/>
							<span class="i1">Fund II</span><span class="i2">2002</span><span class="i3">$29 million</span><br/>
							<span class="i1">Fund III</span><span class="i2">2002</span><span class="i3">$210 million</span><br/>
							<span class="i1">Fund IV</span><span class="i2">2004</span><span class="i3">$250 million</span><br/>
							<span class="i1">Fund V</span><span class="i2">2005</span><span class="i3">$528 million</span><br/>
							<span class="i1">Fund VI</span><span class="i2">2007</span><span class="i3">$600 million</span><br/>
							<span class="i1">Fund VII</span><span class="i2">2010</span><span class="i3">$576 million</span><br/>
							<span class="i1">Fund VIII</span><span class="i2">2011</span><span class="i3">$752 million</span><br/>
							<span class="i1">Fund IX</span><span class="i2">2014</span><span class="i3">$680 million</span><br/>							
							<span class="i1"></span><span class="i2"></span><span class="i3"></span><br/>

							<span class="i1">JBG Urban</span><span class="i2">2007</span><span class="i3">$2.5 billion</span><br/>
							<span class="i1"></span><span class="i2"></span><span class="i3"></span><br/>
							<span class="i1"></span><span class="i2"></span><span class="i3"></span><br/>
							<span class="i1"></span><span class="i2"></span><span class="i3"></span><br/>
							<span class="i1"></span><span class="i2"></span><span class="i3"></span><br/>
							<span class="i1"></span><span class="i2"></span><span class="i3"></span><br/>
						</p>
					  <!--
						<h2>Equity Raised</h2>
						<p>$5.5 billion</p>
					  -->
					  <!--
						<h2></h2>
						<p></p>
					  -->
					</div>
				</div>

				<div class="clear"></div>
				<div id="headlines" class="about-news">
						<div class="grid_4 ">
							<h1>Recent News</h1>
						</div>
						<div class="grid_8 ">
							<div id="headline-rotate" style="position: relative; width: 630px; height: 34px; overflow: hidden;">				
								<a href="/news/article/21613709">JBG’s Days of Giving Concludes with more than 1,600 Volunteer Hours</a>
								<a href="/news/article/21613709">-- JBG’s Days of Giving Concludes with more than 1,600 Volunteer Hours --</a>				
<!--
								<div class="headline-article" style="position: absolute; top: -34px; left: 0px; z-index: 8; opacity: 1; display: none; width: 630px; height: 34px;"><a href="/news/article/21613786">A Search Has Begun for Washington’s “Top Dog”</a></div>
								<div class="headline-article" style="position: absolute; top: -34px; left: 0px; display: none; z-index: 8; opacity: 1; width: 630px; height: 34px;"><a href="/news/article/21613787">Volunteerism Has Changed Since 1989</a></div>
								<div class="headline-article" style="position: absolute; top: -34px; left: 0px; display: none; z-index: 8; opacity: 1; width: 630px; height: 34px;"><a href="/news/article/21613763">New Look For Reston Heights Redevelopment</a></div>
								<div class="headline-article" style="position: absolute; top: 0.545511px; left: 0px; display: block; z-index: 9; opacity: 1; width: 630px; height: 34px;"><a href="/news/article/21613764">JBG’s Days Of Giving Concludes With More Than 1,600 Volunteer Hours</a></div>		
								<div class="headline-article" style="position: absolute; top: 0.545511px; left: 0px; display: block; z-index: 9; opacity: 1; width: 630px; height: 34px;"><a href="/news/article/21613709">JBG’s Days of Giving Concludes with more than 1,600 Volunteer Hours</a></div>
								<div class="headline-article" style="position: absolute; top: 0.545511px; left: 0px; display: block; z-index: 9; opacity: 1; width: 630px; height: 34px;"><a href="/news/article/21613709">-- JBG’s Days of Giving Concludes with more than 1,600 Volunteer Hours --</a></div>
								<div class="headline-article" style="position: absolute; top: -34px; left: 0px; display: none; z-index: 8; opacity: 1; width: 630px; height: 34px;"><a href="/news/article/21613698">JBG, Arlington officials cut ribbon for Sedona|Slate</a></div>
								<div class="headline-article" style="position: absolute; top: -34px; left: 0px; display: none; z-index: 8; opacity: 1; width: 630px; height: 34px;"><a href="/news/article/21613323">Twinbr/ook “Pit Stop” Attracts Cyclists</a></div>
								<div class="headline-article" style="position: absolute; top: -34px; left: 0px; display: none; z-index: 8; opacity: 1; width: 630px; height: 34px;"><a href="/news/article/21613263">JBG adds Bennington Crossings to its apartment portfolio</a></div> 
 -->										
							</div>
						</div>



				</div>
			  <br/>&nbsp;<br/><br/><br/>
			</div>
		</div>
	   </div>


</asp:Content>

