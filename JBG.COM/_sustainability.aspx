﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="sustainability.aspx.vb" Inherits="sustainability" %>
<%@ Import Namespace="System.IO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<div id="navigation-lower">
		<ul class="second-level">
		  <li><a href="leed-team">TEAM</a></li><li><a href="leed">LEED</a></li>
		  <li><a href="energystar">ENERGY STAR</a></li>
		  <li><a href="commitment">COMMITMENT</a></li>
		</ul>
   </div>
   <div id="content">
	<div class="inner-content">
		<div class="container_12" id="main">
			<div class="grid_12 page-header">
				<h1>Sustainability</h1>
			</div>
			<div class="clear"></div>
			<hr/>

 <div class="cmsGroup cmsName_Sustainability_Intro">

 <!--
                    <div class="cmsGroupItem">
                    <div class="grid_4">
			        <h1>Item Heading<br />
                    <img alt="" src="assets/images/thinkgreen.png"/>
                    </h1>
			        </div>
			        <div class="grid_8">
			        <p class="cms cmsType_TextMulti cmsName_Content">
				    Content
			        </p>
                    </div>
                    </div>
  -->

<%  For i As Integer = 1 To CMSFunctions.CountTextSingle("Sustainability_Intro_Item_" & "*" & "_Content")%>
                 <div class="cmsGroupItem">
                    <div class="grid_4">
			<h1>
			 <span class="sideHead cms cmsType_TextSingle cmsName_Sustainability_Intro_Item_<%=i%>_Heading"></span>
			 <span class="sideHead cms cmsType_TextSingle cmsName_Sustainability_Intro_Item_<%=i%>_Heading_2"></span>
			 <img class="cms cmsType_Image cmsName_Sustainability_Intro_Item_<%=i%>_Image" src="cms/data/Sized/liquid/180x/75/ffffff/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Sustainability_Intro_Item_" & i & "_Image")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Sustainability_Intro_Item_" & i & "_Image")%>" />
			</h1>
			        </div>
			        <div class="grid_8">
			        <p class="cms cmsType_TextMulti cmsName_Sustainability_Intro_Item_<%=i%>_Content">
				    Content
			        </p>
                   </div>
                   <div class="clear"></div>
                  </div>
<%Next %>
           
		  </div>




<div class="clear"></div>
<div class="grid_4"></div>
<div class="grid_8">
<p class="cmsGroup cmsName_Sustainability_Specs">
<!--
 <span class="infoColumns cmsGroupItem">
  <span class="col1 cms cmsType_TextSingle cmsName_Column_1">ENERGY STAR Label&reg;</span>
  <span class="col2 ">18 Buildings</span>
  <span class="col3">5,907,871 SF</span>
 </span>
-->

<%  For i As Integer = 1 To CMSFunctions.CountTextSingle("Sustainability_Specs_Item_" & "*" & "_Column_1")%>
 <span class="infoColumns cmsGroupItem">
  <span class="col1 cms cmsType_TextSingle cmsName_Sustainability_Specs_Item_<%=i%>_Column_1"></span>
  <span class="col2 cms cmsType_TextSingle cmsName_Sustainability_Specs_Item_<%=i%>_Column_2"></span>
  <span class="col3 cms cmsType_TextSingle cmsName_Sustainability_Specs_Item_<%=i%>_Column_3"></span>
 </span>
<%Next %>

</p>
</div>


			<div id="headlines" class="bar80 infobar">
				<div class="grid_4">
					<h1>SUSTAINABILITY NEWS</h1>
				</div>
				<div class="grid_8">
					<div id="headline-rotate">
						<asp:Literal ID="News_Literal" runat="server"></asp:Literal>
					        <span class="clear"></span>
					</div>
				<span class="clear"></span>
				</div>
				<span class="clear"></span>
			</div>
				<span class="clear"></span><br />




		</div>
	</div>
   </div>

</asp:Content>

