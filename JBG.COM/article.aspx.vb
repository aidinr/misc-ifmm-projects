﻿Imports System.Data

Partial Class article
    Inherits System.Web.UI.Page

    Public NewsID As Integer
    Public Publication As String
    Public Author As String
    Public hasimahe As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hasimahe = ""
        If Not Request.QueryString("id") Is Nothing Then
            NewsID = Request.QueryString("id")
        End If

        Dim DistinctYearsSQL As String = ""
        DistinctYearsSQL &= "Select distinct year(Post_Date) as [year] from ipm_news "
        DistinctYearsSQL &= "Where Post_Date <= getdate() and Pull_Date >= getdate() "
        DistinctYearsSQL &= "and Show = 1 "
        DistinctYearsSQL &= "and Type in (select Type_Id from ipm_news_type Where Show = 1) "
        DistinctYearsSQL &= "Order by YEAR(Post_DATE) DESC"
        Dim DistinctYearsDT As New DataTable
        DistinctYearsDT = DBFunctions.GetDataTable(DistinctYearsSQL)
        Dim DistinctYearsOUT As String = ""
        If DistinctYearsDT.Rows.Count > 0 Then
            DistinctYearsOUT &= "<ul class=""third-level padLeft"">"
            For R As Integer = 0 To DistinctYearsDT.Rows.Count - 1
                DistinctYearsOUT &= "<li><a class=""award-link"" href=""news-" & DistinctYearsDT.Rows(R).Item("year").ToString.Trim & """>" & DistinctYearsDT.Rows(R).Item("year").ToString.Trim & "</a></li>"
            Next
            DistinctYearsOUT &= "</ul>"
            DistinctYears_Literal.Text = DistinctYearsOUT
        End If

        Dim NewsContentSQL As String = ""
        NewsContentSQL &= "select n.Headline, n.Content, Convert(char(10),n.Post_Date,101) newsdate, n.PublicationTitle, n.Contact, n.picture, n.pdf, n.type, F1.Item_Value AS IDAM_AUTHOR, F2.Item_Value AS IDAM_SNEWS, F3.Item_Value AS IDAM_SUBTITLE "
        NewsContentSQL &= "from ipm_news n  "
        NewsContentSQL &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F1 on n.News_Id = F1.NEWS_ID and F1.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_AUTHOR' AND Active = 1) "
        NewsContentSQL &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F2 on n.News_Id = F2.NEWS_ID and F2.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_SNEWS' AND Active = 1) "
        NewsContentSQL &= "LEFT JOIN IPM_NEWS_FIELD_VALUE F3 on n.News_Id = F3.NEWS_ID and F3.Item_ID = (Select Item_ID From IPM_NEWS_FIELD_DESC where Item_Tag = 'IDAM_SUBTITLE' AND Active = 1) "
        NewsContentSQL &= "where n.news_id = " & NewsID & ""
        Dim NewsContentDT As New DataTable
        NewsContentDT = DBFunctions.GetDataTable(NewsContentSQL)
        If NewsContentDT.Rows.Count > 0 Then
            hasimahe = NewsContentDT.Rows(0).Item("picture").ToString.Trim
            NewsHeadline_Literal.Text = formatfunctionssimple.AutoFormatText(NewsContentDT.Rows(0).Item("Headline").ToString.Trim)
            SubHeadline_Literal.Text = formatfunctionssimple.AutoFormatText(NewsContentDT.Rows(0).Item("IDAM_SUBTITLE").ToString.Trim)
            NewsDate_Literal.Text = NewsContentDT.Rows(0).Item("newsdate").ToString.Trim
            News_Content.Text = formatfunctions.AutoFormatText(NewsContentDT.Rows(0).Item("Content").ToString.Trim)
            Author = NewsContentDT.Rows(0).Item("IDAM_AUTHOR").ToString.Trim
            Publication = formatfunctionssimple.AutoFormatText(NewsContentDT.Rows(0).Item("PublicationTitle").ToString.Trim)
        End If

        Dim RelatedProjectsSQL As String = ""
        RelatedProjectsSQL &= "select p.ProjectID, Name, F1.Item_Value as SeoURL from ipm_news_related_projects r "
        RelatedProjectsSQL &= "join ipm_project p on r.ProjectID = p.ProjectID join ipm_project_field_value v on p.ProjectID = v.ProjectId and v.Item_Value = '1' and v.Item_id = (Select item_id from ipm_project_field_desc where Item_Tag = 'IDAM_PPUBLIC' and Active = 1) "
        RelatedProjectsSQL &= "left join ipm_project_field_value F1 on p.ProjectID = F1.ProjectID and F1.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SEO_URL') "
        RelatedProjectsSQL &= "where news_id = " & NewsID & ""
        Dim RelatedProjectsDT As New DataTable
        RelatedProjectsDT = DBFunctions.GetDataTable(RelatedProjectsSQL)
        Dim RelatedProjectsOUT As String = ""
        If RelatedProjectsDT.Rows.Count > 0 Then
            RelatedProjectsOUT &= " <dl>"
            If RelatedProjectsDT.Rows.Count > 1 Then
                RelatedProjectsOUT &= "<dt>Related Project(s)</dt>"
            Else
                RelatedProjectsOUT &= "<dt>Related Project</dt>"
            End If
            For R As Integer = 0 To RelatedProjectsDT.Rows.Count - 1
                Dim ProjectURL As String = ""
                If RelatedProjectsDT.Rows(R).Item("SeoURL").ToString.Trim <> "" Then
                    ProjectURL = RelatedProjectsDT.Rows(R).Item("SeoURL").ToString.Trim
                Else
                    ProjectURL = RelatedProjectsDT.Rows(R).Item("ProjectID").ToString.Trim
                End If
                RelatedProjectsOUT &= ""
                RelatedProjectsOUT &= "<dd>"
                RelatedProjectsOUT &= "<a href=""" & ProjectURL & "-property-gallery"">" & formatfunctionssimple.AutoFormatText(RelatedProjectsDT.Rows(R).Item("Name").ToString.Trim) & "</a>"
                RelatedProjectsOUT &= "</dd>"
            Next
            RelatedProjectsOUT &= "</dl>"
            NewsRelatedProjects_Literal.Text = RelatedProjectsOUT
        End If


    End Sub

End Class
