﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="employment.aspx.vb" Inherits="employment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
		<ul class="second-level">
			<li><a href="https://careers-jbg.icims.com/jobs/" target="_blank">Employment</a></li>
		</ul>
   </div>
   <div id="content" class="employment">
	<div class="inner-content" >		
		<div class="container_12"  id="main">
			<div class="grid_12 page-header">
			  <h1>Employment</h1>
			</div>
			<div class="clear"></div>
			<hr/>
			<div class="grid_4">
			 <img class="cms cmsType_Image cmsName_Employment_Image" src="cms/data/Sized/liquid/180x/75/ffffff/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Employment_Image")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Employment_Image")%>" />
			</div>
			<div class=" grid_8 content">
			<h2 class="cms cmsType_TextSingle cmsName_Employment_Heading"></h2>
			<p class="investorLoing cms cmsType_TextMulti cmsName_Employment_Content"></p>
			</div>
			<div class="bar80 infobar">
			 <div class="grid_4 sidebar">
			  <h1 class="cms cmsType_TextSingle cmsName_Employment_Link_Name">INTRALINKS</h1>
			 </div>
			 <div class="grid_8">
			  <a target="_blank" href="https://services.intralinks.com/logon.html?clientID=8818744706" class="cms cmsType_Link cmsName_Employment_Link">Click Here</a>
			 </div>
			</div>
			 <div class="clear"></div>
			</div>
		</div>
	</div>	

</asp:Content>

			