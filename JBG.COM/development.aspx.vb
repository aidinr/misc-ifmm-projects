﻿Imports System.Data

Partial Class development
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim ProjectsSQL As String = ""
        ProjectsSQL &= "select top 8 p.ProjectID, p.Description,  coalesce( nullif(p.Name, ''), v3.Item_Value) ShortName, F1.Item_Value as SeoURL "
        ProjectsSQL &= "from ipm_project p "
        ProjectsSQL &= "join ipm_project_field_value v1 on p.ProjectID = v1.ProjectID join ipm_project_field_desc d1 on v1.Item_ID = d1.Item_ID and d1.Item_Tag = 'IDAM_PAGELOCATION' and v1.Item_Value like '%Development Page%' "
        ProjectsSQL &= "join ipm_project_field_value v2 on p.projectID = v2.ProjectID join ipm_project_field_desc d2 on v2.Item_ID = d2.Item_ID and d2.Item_Tag = 'IDAM_FEATURE' and v2.Item_Value = '1' "
        ProjectsSQL &= "join ipm_project_field_value v3 on p.projectID = v3.ProjectID join ipm_project_field_desc d3 on v3.Item_ID = d3.Item_ID and d3.Item_Tag = 'IDAM_SHORTNAME' "
        ProjectsSQL &= "left join ipm_project_field_value F1 on p.ProjectID = F1.ProjectID and F1.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SEO_URL') "
        ProjectsSQL &= "Order By newid() "
        Dim LeedProjectsDT As New DataTable
        LeedProjectsDT = DBFunctions.GetDataTable(ProjectsSQL)
        Dim LeedProjectsOUT As String = ""
        If LeedProjectsDT.Rows.Count > 0 Then
            For R As Integer = 0 To LeedProjectsDT.Rows.Count - 1
                LeedProjectsOUT &= ""
                If R Mod 2 Then
                    LeedProjectsOUT &= "<div class=""grid_4 omega"">" & vbNewLine
                Else
                    LeedProjectsOUT &= "<div class=""grid_4 alpha"">" & vbNewLine
                End If
                LeedProjectsOUT &= "<div class=""property-cell"">" & vbNewLine
                LeedProjectsOUT &= "<div class=""image-container"">" & vbNewLine
                LeedProjectsOUT &= "<img src=""dynamic/image/always/project/best/300x130/92/transparent/center/" & LeedProjectsDT.Rows(R).Item("ProjectID").ToString.Trim & ".jpg" & """" & " alt=""""/>"
                LeedProjectsOUT &= "</div>" & vbNewLine
                Dim ProjectURL As String = ""
                If LeedProjectsDT.Rows(R).Item("SeoURL").ToString.Trim <> "" Then
                    ProjectURL = LeedProjectsDT.Rows(R).Item("SeoURL").ToString.Trim
                Else
                    ProjectURL = LeedProjectsDT.Rows(R).Item("ProjectID").ToString.Trim
                End If
                LeedProjectsOUT &= "<h2><a href=""" & ProjectURL & "-property-gallery"">" & formatfunctionssimple.AutoFormatText(LeedProjectsDT.Rows(R).Item("ShortName").ToString.Trim) & "</a></h2>" & vbNewLine
                LeedProjectsOUT &= "<p>" & formatfunctions.AutoFormatText(CMSFunctions.GetWords(LeedProjectsDT.Rows(R).Item("description").ToString.Trim, 50)) & "&#8230; <a href=""" & ProjectURL & "-property-gallery"">[More]</a></p>" & vbNewLine
                LeedProjectsOUT &= "</div>" & vbNewLine
                LeedProjectsOUT &= "</div>" & vbNewLine
                If R Mod 2 = 1 Then
                    LeedProjectsOUT &= "<div class=""clear""></div>" & vbNewLine
                End If
            Next
            LeedList_Literal.Text = LeedProjectsOUT
        End If


    End Sub

End Class
