﻿<%@ Page Title="" Language="VB" MasterPageFile="~/JBG.master" AutoEventWireup="false" CodeFile="leed-team.aspx.vb" Inherits="leedteam" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="navigation-lower">
	     <ul class="second-level">
		  <li><a href="leed-team">TEAM</a></li><li><a href="leed">LEED</a></li>
		  <li><a href="energystar">ENERGY STAR</a></li>
		  <li><a href="commitment">COMMITMENT</a></li>
		</ul>
   </div>
   <div id="content">
	<div class="inner-content">		
		<div class="container_12" id="main">
			<div class="grid_12 page-header">
				<h1>TEAM</h1>
			</div>
			<div class="clear"></div>
			<hr/>

			<div class="grid_4"><h1 class="cms cmsType_TextSingle cmsName_Leed_Team_Top_2_Heading"></h1></div>                    
			<div class="grid_8">
				<p class="cms cmsType_TextMulti cmsName_Leed_Team_Top_Content"></p>
			</div>

			<div class="clear"></div>
			<div class="grid_4"></div>                    
			<div class="grid_8">
				<p class="cms cmsType_TextMulti cmsName_Leed_Team_Top_2_Content"></p>
			</div>

			<div class="clear"></div>
			<div class="grid_4"><h1 class="cms cmsType_TextSingle cmsName_Leed_Bottom_Heading">LEED Accredited Professionals</h1>
			  <p class="goRightPadTop"><img class="cms cmsType_Image cmsName_Leed_Bottom_Image" src="cms/data/Sized/liquid/70x/75/ffffff/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Leed_Bottom_Image")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Leed_Bottom_Image")%>" /></p>
			</div>                    
			<div class="grid_8">
				
				<p class="cms cmsType_TextMulti cmsName_Leed_Bottom_Content"></p>
				<p class="cms cmsType_TextMulti cmsName_Leed_Bottom_List_Column_1"></p>
				<p class="cms cmsType_TextMulti cmsName_Leed_Bottom_List_Column_2"></p>
			</div>
			<div class="clear"></div>
		</div>
	</div>	
   </div>
</asp:Content>

