﻿Imports System.Data

Partial Class news
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Public SelectedYear As String = ""
    Public SelectedNewsType As String = "general"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Request.QueryString("type") Is Nothing Then
            SelectedNewsType = Request.QueryString("type")
        Else
            SelectedNewsType = "general"
        End If
        Dim newsTop as String = "999"
        Dim DistinctYearsSQL As String = ""
        DistinctYearsSQL &= "Select distinct year(Post_Date) as [year] from ipm_news "
        DistinctYearsSQL &= "Where Post_Date <= getdate() and Pull_Date >= getdate() "
        DistinctYearsSQL &= "and Show = 1 "
        Select Case SelectedNewsType
            Case "general"
                DistinctYearsSQL &= "and Type in (select Type_Id from ipm_news_type Where Show = 1) "
            Case "press-releases"
                DistinctYearsSQL &= "and Type in (8) "
            Case "articles"
                DistinctYearsSQL &= "and Type in (9) "
        End Select

        DistinctYearsSQL &= "Order by YEAR(Post_DATE) DESC"
        Dim DistinctYearsDT As New DataTable
        DistinctYearsDT = DBFunctions.GetDataTable(DistinctYearsSQL)
        Dim DistinctYearsOUT As String = ""
        If DistinctYearsDT.Rows.Count > 0 Then
            DistinctYearsOUT &= "<ul class=""third-level padLeft"">"
            For R As Integer = 0 To DistinctYearsDT.Rows.Count - 1
                Select Case SelectedNewsType
                    Case "general"
                        DistinctYearsOUT &= "<li><a class=""award-link"" href=""news-" & DistinctYearsDT.Rows(R).Item("year").ToString.Trim & """>" & DistinctYearsDT.Rows(R).Item("year").ToString.Trim & "</a></li>"
                    Case "press-releases"
                        DistinctYearsOUT &= "<li><a class=""award-link"" href=""news-press-releases-" & DistinctYearsDT.Rows(R).Item("year").ToString.Trim & """>" & DistinctYearsDT.Rows(R).Item("year").ToString.Trim & "</a></li>"
                    Case "articles"
                        DistinctYearsOUT &= "<li><a class=""award-link"" href=""news-articles-" & DistinctYearsDT.Rows(R).Item("year").ToString.Trim & """>" & DistinctYearsDT.Rows(R).Item("year").ToString.Trim & "</a></li>"
                End Select
            Next
            DistinctYearsOUT &= "</ul>"
            DistinctYears_Literal.Text = DistinctYearsOUT
        End If

        If Not Request.QueryString("year") Is Nothing Then
            SelectedYear = Request.QueryString("year")
            News_Back.Text = "<div class=""quicklinks"" id=""ContentPlaceHolderDefault_aspcontainer_news_3_newslink""><div class=""qlink float-left""><a href=""news"">&lt;&nbsp;&nbsp;&nbsp;Recent News</a></div></div>"
        Else
            SelectedYear = "Recent"
            newsTop = "25"
        End If

        Select Case SelectedNewsType
            Case "general"
                NewsTitle_Literal.Text = "JBG NEWS: " & SelectedYear
            Case "press-releases"
                NewsTitle_Literal.Text = "PRESS RELEASES: " & SelectedYear
                News_Back.Text = "<div class=""quicklinks"" id=""ContentPlaceHolderDefault_aspcontainer_news_3_newslink""><div class=""qlink float-left""><a href=""news"">&lt;&nbsp;&nbsp;&nbsp;Recent News</a></div></div>"
            Case "articles"
                NewsTitle_Literal.Text = "ARTICLES: " & SelectedYear
                News_Back.Text = "<div class=""quicklinks"" id=""ContentPlaceHolderDefault_aspcontainer_news_3_newslink""><div class=""qlink float-left""><a href=""news"">&lt;&nbsp;&nbsp;&nbsp;Recent News</a></div></div>"
        End Select

        Dim NewsSQL As String = ""
        NewsSQL &= "select TOP " & newsTop & " News_Id, convert(char(10),Post_Date,101) as newsdate , Headline "
        NewsSQL &= "from ipm_news "
        NewsSQL &= "Where Post_Date <= getdate() and Pull_Date >= getdate() "
        NewsSQL &= "and Show = 1 "
        Select Case SelectedNewsType
            Case "general"
                NewsSQL &= "and Type in (select Type_Id from ipm_news_type Where Show = 1) "
            Case "press-releases"
                NewsSQL &= "and Type in (8) "
            Case "articles"
                NewsSQL &= "and Type in (9) "
        End Select
        if IsNumeric(SelectedYear) 
           NewsSQL &= "and year(Post_Date) = '" & SelectedYear & "'"
        End If
        NewsSQL &= "Order by Post_DATE DESC "
        Dim NewsDT As New DataTable
        NewsDT = DBFunctions.GetDataTable(NewsSQL)
        Dim NewsOUT As String = ""
        If NewsDT.Rows.Count > 0 Then
            For R As Integer = 0 To NewsDT.Rows.Count - 1
                NewsOUT &= "<div class=""news-item"">"
                NewsOUT &= "<div class=""grid_1 alpha"">"
                NewsOUT &= "<a href=""article-" & NewsDT.Rows(R).Item("News_Id").ToString.Trim & """ class=""news-item-date"">" & NewsDT.Rows(R).Item("newsdate").ToString.Trim & "</a>"
                NewsOUT &= "</div>"
                NewsOUT &= "<div class=""grid_6 omega"">"
                NewsOUT &= "<a href=""article-" & NewsDT.Rows(R).Item("News_Id").ToString.Trim & """>" & FormatFunctionsSimple.AutoFormatText(NewsDT.Rows(R).Item("Headline").ToString.Trim) & "</a>"
                NewsOUT &= "</div>"
                NewsOUT &= "</div>"
            Next
            News_Literal.Text = NewsOUT
        End If
    End Sub

End Class
