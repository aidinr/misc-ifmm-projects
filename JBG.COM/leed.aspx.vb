﻿Imports System.Data

Partial Class leed
    'Inherits System.Web.UI.Page
    Inherits CMSBasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim LeedProjectsSQL As String = ""
        LeedProjectsSQL &= "select top 4 p.ProjectId, v3.Item_value ShortName, ad.Item_Value description, ai.asset_id, a.WPixel, a.HPixel, F1.Item_Value as SeoURL "
        LeedProjectsSQL &= "from ipm_project_field_desc d "
        LeedProjectsSQL &= "join ipm_project_field_value v on v.Item_Id = d.Item_Id and v.Item_value = '1' "
        LeedProjectsSQL &= "join ipm_project p on v.ProjectID = p.ProjectId and p.Available = 'Y' "
        LeedProjectsSQL &= "join ipm_project_field_value v1 on v1.ProjectId = p.ProjectId and v1.Item_Value <> '' "
        LeedProjectsSQL &= "join ipm_project_field_desc d1 on d1.Item_Id = v1.Item_id and d1.Item_Tag = 'p_leed' "
        LeedProjectsSQL &= "join ipm_project_field_value v2 on v2.ProjectId = p.ProjectId and v2.Item_Value <> '' "
        LeedProjectsSQL &= "join ipm_project_field_desc d2 on  d2.Item_Id = v2.Item_id and d2.Item_Tag = 'IDAM_LEED1' join ipm_project_field_value v3 on v3.ProjectId = p.ProjectID "
        LeedProjectsSQL &= "join ipm_project_field_desc d3 on d3.Item_Id = v3.Item_Id and d3.Item_Tag = 'IDAM_SHORTNAME' join ipm_asset a on p.ProjectID = a.ProjectID "
        LeedProjectsSQL &= "Left join ipm_project_field_value F1 on p.ProjectID = F1.ProjectID and F1.Item_ID = ( Select Item_ID from ipm_project_field_desc where Item_Tag = 'IDAM_SEO_URL') "
        LeedProjectsSQL &= "join ipm_asset_field_value ai on a.asset_id = ai.asset_id and ai.Item_ID = 21611766 and ai.Item_value = '1' "
        LeedProjectsSQL &= "join ipm_asset_field_value ad on a.asset_id = ad.asset_id and ad.item_id = 21611767 "
        LeedProjectsSQL &= "where d.Item_tag = 'IDAM_PPUBLIC' "
        LeedProjectsSQL &= "Order by newid() "
        Dim LeedProjectsDT As New DataTable
        LeedProjectsDT = DBFunctions.GetDataTable(LeedProjectsSQL)
        Dim LeedProjectsOUT As String = ""
        If LeedProjectsDT.Rows.Count > 0 Then
            For R As Integer = 0 To LeedProjectsDT.Rows.Count - 1
                LeedProjectsOUT &= ""
                If R Mod 2 Then
                    LeedProjectsOUT &= "<div class=""grid_4 omega"">" & vbNewLine
                Else
                    LeedProjectsOUT &= "<div class=""grid_4 alpha"">" & vbNewLine
                End If
                LeedProjectsOUT &= "<div class=""property-cell"">" & vbNewLine
                LeedProjectsOUT &= "<div class=""image-container"">" & vbNewLine
                LeedProjectsOUT &= "<img src=""dynamic/image/always/asset/best/300x130/92/ffffff/Center/" & LeedProjectsDT.Rows(R).Item("asset_id").ToString.Trim & ".jpg" & """" & " alt=""""/>"
                LeedProjectsOUT &= "</div>" & vbNewLine
                Dim ProjectURL As String = ""
                If LeedProjectsDT.Rows(R).Item("SeoURL").ToString.Trim <> "" Then
                    ProjectURL = LeedProjectsDT.Rows(R).Item("SeoURL").ToString.Trim
                Else
                    ProjectURL = LeedProjectsDT.Rows(R).Item("ProjectID").ToString.Trim
                End If
                LeedProjectsOUT &= "<h2><a href=""" & ProjectURL & "-property-sustainability"">" & LeedProjectsDT.Rows(R).Item("ShortName").ToString.Trim & "</a></h2>" & vbNewLine
                LeedProjectsOUT &= "<p>" & formatfunctions.AutoFormatText(CMSFunctions.GetWords(LeedProjectsDT.Rows(R).Item("description").ToString.Trim, 50)) & "&#8230; <a href=""" & ProjectURL & "-property-sustainability"">[More]</a></p>" & vbNewLine
                LeedProjectsOUT &= "</div>" & vbNewLine
                LeedProjectsOUT &= "</div>" & vbNewLine
                If R + 1 = 2 Then
                    LeedProjectsOUT &= "<div class=""clear""></div>" & vbNewLine
                End If
            Next
            LeedList_Literal.Text = LeedProjectsOUT
        End If




    End Sub

End Class
