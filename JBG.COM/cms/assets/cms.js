String.prototype.capitalize = function () {
    return this.replace(/(^|[_\-])([a-z])/g, function (m, p1, p2) {
        return p1 + p2.toUpperCase();
    });
};

// Start Modal //

$('body').append('<span class="cmsLogout">LOGOUT</span>').append('<span class="cmsGoogleAnalytics">Google Analytics</span>');

if($('body').hasClass('cms')) $('body').append('<span class="cmsEditMetadata">METADATA</span>');;

$('body .cmsGroup').prepend('<span class="cmsGroupAdd">ADD</span>');

$('body .cms.cmsType_TextSingle, body .cms.cmsType_TextMulti, body .cms.cmsType_Link, body .cms.cmsType_Rich, body .cms.cmsType_Textset, body .cms.cmsType_Linkset').each(function () {
    $(this).prepend('<span class="cmsEdit ' + getCMSname($(this).attr('class')) + '">EDIT</span>');
})
$('.cms.cmsType_Image, .cms.cmsType_File').each(function () {
    $(this).before('<span class="cmsEdit  ' + getCMSname($(this).attr('class')) + '">EDIT</span>');
})
$('body').append('<div id="cmsModalIframe"><iframe width="100" height="100" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src="/cms/assets/loading.html"></iframe></div><div id="cmsModalOverlay"></div><div id="cmsModalLoading"></div>');

$('.cmsGroupAdd').click(function () {
    var addI = $(this).parent().find('.cmsGroupItem').length + 1;
    var group = $(this).parent().contents().filter(function(){return this.nodeType==8;});

    $(this).parent().append(group[0].nodeValue);
    $(this).parent().find('.cmsGroupItem').last().prepend('<span class="cmsGroupItemRemove"></span>');

    $('.cmsGroupItemRemove').unbind();
    $('.cmsGroupItemRemove').click(function () {

     $(this).closest('.cmsGroup').find('.cmsGroupAdd:eq(0)').show();

     $(this).closest('.cmsGroupItem').parent().find('.cmsGroupItem').last().remove();
     $(this).remove();

   });

    var obj = $(this).parent().find('.cmsGroupItem').last();
    var groupName = getCMSname(obj.parent().attr('class'));

    $(obj).find('.cms.cmsType_TextSingle, .cms.cmsType_TextMulti, .cmsType_Link, .cms.cmsType_Rich, .cms.cmsType_Textset, .cms.cmsType_Linkset').each(function () {
        $(this).prepend('<span class="cmsEdit ' + getCMSname($(this).attr('class')) + '">EDIT</span>');
    });

    obj.find('.cms.cmsType_Image, .cms.cmsType_File').each(function () {
        $(this).before('<span class="cmsEdit ' + getCMSname($(this).attr('class')) + '">EDIT</span>');
    });
    obj.find("[class*='cmsName_']").each(function () {
        var cmsName = getCMSname($(this).attr('class'));
        $(this).removeClass(cmsName);
        $(this).addClass(cmsName.replace(/cmsName_/g, groupName + '_Item_' + addI + '_'));
    });
    obj.find('span.cmsEdit').click(function () {
        if ($(this).parent().hasClass('cms')) var obj = $(this).parent();
        else if ($(this).next().hasClass('cms')) var obj = $(this).next();

        var WidthHeight = '';
        if (getVal('cmsWidth_', obj.attr('class')) || getVal('cmsHeight_', obj.attr('class'))) {
            WidthHeight = '&width=' + getVal('cmsWidth_', obj.attr('class')) + '&height=' + getVal('cmsHeight_', obj.attr('class'));
        }

        var h = $(window).height() - 170;
        if (getVal('cmsType_', obj.attr('class')) == 'TextSingle') h = 220;
        else if (getVal('cmsType_', obj.attr('class')) == 'TextMulti') h = 440;
        else if (getVal('cmsType_', obj.attr('class')) == 'Link') h = 260;
        else if (getVal('cmsType_', obj.attr('class')) == 'Image') h = 260;
        else if (getVal('cmsType_', obj.attr('class')) == 'File') h = 350;
        else if (getVal('cmsType_', obj.attr('class')) == 'Rich') h = 430;

        var t = getVal('cmsType_', String(obj.attr('class')));
        var n = getVal('cmsName_', String(obj.attr('class')));

        $('#cmsModalLoading').css('opacity', '.4').show().animate({
            'opacity': 1
        }, 300);
        $('#cmsModalOverlay').css('opacity', '0').show().animate({
            'opacity': .8
        }, 300);

        $('#cmsModalIframe').css('display', 'block').css('visibility', 'visible').css('opacity', '0');
        $('#cmsModalIframe iframe:eq(0)').attr('width', '800').attr('height', h).attr('src', '/cms/assets/cms.html?name=' + encodeURIComponent(n) + WidthHeight + '&type=' + encodeURIComponent(t));
        return false;
    });
    $(this).hide();
    return false;
});

$('.cmsGroup').each(function () {
 $(this).find('.cmsGroupItem').each(function () {
  $(this).prepend('<span class="cmsGroupItemUp"></span><span class="cmsGroupItemDown"></span><span class="cmsGroupItemDelete"></span>');
 });
 $(this).find('.cmsGroupItemFirst').removeClass('cmsGroupItemFirst');
 $(this).find('.cmsGroupItemLast').removeClass('cmsGroupItemLast');
 $(this).find('.cmsGroupItemUp:eq(0)').addClass('cmsGroupItemFirst').addClass('cmsGroupItemFirst');
 $(this).find('.cmsGroupItemDown:eq(0)').addClass('cmsGroupItemFirst').addClass('cmsGroupItemFirst');
 $(this).find('.cmsGroupItemUp').last().addClass('cmsGroupItemLast').addClass('cmsGroupItemLast');
 $(this).find('.cmsGroupItemDown').last().addClass('cmsGroupItemLast').addClass('cmsGroupItemLast');
 $('.cmsGroupItemDelete').unbind();
 $('.cmsGroupItemDelete').each(function () {
  $(this).click(function () {
   if(confirm('This item will be deleted.')) {
    $(this).unbind();
    $('*').unbind();
    $.get('/cms/update.aspx?cmsGroupItemDelete='+(1+Math.round($(this).closest('.cmsGroupItem').index('.'+ getCMSname($(this).closest('.cmsGroup').attr('class')) +' .cmsGroupItem')))+'&cmsGroup=' + escape(getCMSname($(this).closest('.cmsGroup').attr('class')).substr(8)) + '&v=' + new Date().getTime(), function (data) {
     document.location=String(document.location).split('#')[0].split('?')[0]+'?' + new Date().getTime()
    });
   }
  });
 });
 $('.cmsGroupItemUp').unbind();
 $('.cmsGroupItemUp').each(function () {
  $(this).click(function () {
    $(this).unbind();
    $('*').unbind();
   $.get('/cms/update.aspx?cmsGroupItemUp='+(1+Math.round($(this).closest('.cmsGroupItem').index('.'+ getCMSname($(this).closest('.cmsGroup').attr('class')) +' .cmsGroupItem')))+'&cmsGroup=' + escape(getCMSname($(this).closest('.cmsGroup').attr('class')).substr(8)) + '&v=' + new Date().getTime(), function (data) {
    document.location=String(document.location).split('#')[0].split('?')[0]+'?' + new Date().getTime()
   });
  });
 });
 $('.cmsGroupItemDown').unbind();
 $('.cmsGroupItemDown').each(function () {
  $(this).click(function () {
    $(this).unbind();
    $('*').unbind();
   $.get('/cms/update.aspx?cmsGroupItemDown='+(1+Math.round($(this).closest('.cmsGroupItem').index('.'+ getCMSname($(this).closest('.cmsGroup').attr('class')) +' .cmsGroupItem')))+'&cmsGroup=' + escape(getCMSname($(this).closest('.cmsGroup').attr('class')).substr(8)) + '&v=' + new Date().getTime(), function (data) {
    document.location=String(document.location).split('#')[0].split('?')[0]+'?' + new Date().getTime()
   });
  });
 });
});

function cmsModalShow() {
    $('#cmsModalLoading').animate({
        opacity: 0
    }, 400, function () {
        $('#cmsModalLoading').hide()
    });

    var x = ($('body').width() / 2) - ($('#cmsModalIframe').outerWidth() / 2);
    $('#cmsModalIframe').css('left', x + 'px').css('visibility', 'visible').css('display', 'block').css('opacity', '0').animate({
        'opacity': 1
    }, 800);
}

function cmsModalUpdated() {
    $('#cmsModalIframe').fadeOut(200);
    $('#cmsModalOverlay').animate({
        opacity: 1
    }, 400, function () {
     $('#cmsModalIframe').fadeOut(300);
     $('#cmsModalOverlay').animate({opacity: 1}, 200, function() {document.location=String(document.location).split('#')[0].split('?')[0]+'?u=' + new Date().getTime()});
    });
}

function cmsModalClose(x) {
    if (x) {
        $('#cmsModalIframe').fadeOut(400);
        $('#cmsModalOverlay').animate({
            opacity: 0
        }, 400, function () {
            $('#cmsModalOverlay').hide()
        });
        return false;
    }
    var answer = confirm('Any changes you made will be lost unless you save them.')
    if (answer) {
        $('#cmsModalIframe').fadeOut(400);
        $('#cmsModalOverlay').animate({
            opacity: 0
        }, 400, function () {
            $('#cmsModalOverlay').hide()
        });
    }
    return false;
}

$(document).keyup(function (e) {
    if (e.keyCode == 27 && $('#cmsModalIframe').is(':visible')) {
        cmsModalClose();
        return false;
    }
});

$('span.cmsLogout').click(function () {
    $.get('/cms/Logout.aspx?v=' + new Date().getTime(), function (data) {
        document.location=String(document.location).split('#')[0].split('?')[0]+'?' + new Date().getTime()
    });
    return false;
});

$('span.cmsEdit, span.cmsEditMetadata, span.cmsPassword, span.cmsGoogleAnalytics').click(function () {
    if ($(this).parent().hasClass('cms')) var obj = $(this).parent();
    else if ($(this).next().hasClass('cms')) var obj = $(this).next();

    var WidthHeight = '';
    if (getVal('cmsWidth_', obj.attr('class')) || getVal('cmsHeight_', obj.attr('class'))) {
        WidthHeight = '&width=' + getVal('cmsWidth_', obj.attr('class')) + '&height=' + getVal('cmsHeight_', obj.attr('class'));
    }

    var h = $(window).height() - 170;
    if ($(this).hasClass('cmsEditMetadata')) h = 436;
    else if (getVal('cmsType_', obj.attr('class')) == 'TextSingle') h = 220;
    else if (getVal('cmsType_', obj.attr('class')) == 'TextMulti') h = 440;
    else if (getVal('cmsType_', obj.attr('class')) == 'Link') h = 260;
    else if ($(this).attr('class') == 'cmsPassword') h = 340;
    else if ($(this).attr('class') == 'cmsGoogleAnalytics') h = 200;
    else if (getVal('cmsType_', obj.attr('class')) == 'Image') h = 260;
    else if (getVal('cmsType_', obj.attr('class')) == 'File') h = 350;
    else if (getVal('cmsType_', obj.attr('class')) == 'Rich') h = 430;
    var t = '';
    var n = '';
    var p = '';

    if ($(this).hasClass('cmsEditMetadata')) {
        var t = 'Metadata';
        var p = getPage(document.location);
        if (p) {
            var n = String(p.replace(/\.[^/.]+$/, "")).capitalize().replace(/-/g, ' ');
            p = String(p.replace(/\.[^/.]+$/, ""));
        } else var n = String(window.location.hostname);
    } else if ($(this).hasClass('cmsPassword')) {
        var t = 'Password';
        var p = '';
        var n = '';
    } else if ($(this).hasClass('cmsGoogleAnalytics')) {
        var t = 'Google Analytics';
        var p = '';
        var n = '';
    } else {
        var t = getVal('cmsType_', String(obj.attr('class')));
        var n = getVal('cmsName_', String(obj.attr('class')));
    }

    $('#cmsModalLoading').css('opacity', '.4').show().animate({
        'opacity': 1
    }, 300);
    $('#cmsModalOverlay').css('opacity', '0').show().animate({
        'opacity': .8
    }, 300);
    $('#cmsModalIframe').css('display', 'block').css('visibility', 'visible').css('opacity', '0');
    $('#cmsModalIframe iframe:eq(0)').attr('width', '800').attr('height', h).attr('src', '/cms/assets/cms.html?name=' + encodeURIComponent(n) + WidthHeight + '&type=' + encodeURIComponent(t) + '&page=' + encodeURIComponent(p));
    return false;
});

// Start Login //

if (window.location.search.substr(0, 5) == '?edit' && readCookie('SiteEdit') != '1') {
    $('.cmsEdit, .cmsEditMetadata, .cmsGoogleAnalytics, .cmsPassword, .cmsLogout, .cmsGroupAdd, .cmsGroupItemUp, .cmsGroupItemDown, .cmsGroupItemDelete').remove();
    $('.cms').removeClass('cms');
    $('.cmsGroupItem').removeClass('cmsGroupItem');
    setTimeout("cmsLogin()", 800);
} else if (window.location.search.substr(0, 5) == '?edit') {
    document.location = String(document.location).split('?')[0].split('#')[0];
}

function cmsLogin() {
    $('#cmsModalLoading').css('opacity', '.3').show().animate({
        'opacity': 1
    }, 300);
    $('#cmsModalOverlay').css('opacity', '0').show().animate({
        'opacity': .8
    }, 300);
    $('#cmsModalIframe').css('display', 'block').css('visibility', 'visible').css('opacity', '0');
    $('#cmsModalIframe iframe:eq(0)').attr('width', '800').attr('height', '240').attr('src', '/cms/assets/login.html');
    return false;
}

// End Login //

function getPage() {
    var nPage = window.location.pathname;
    nPage = nPage.substring(nPage.lastIndexOf('/') + 1);
    return nPage;
}

function getCMSname(str) {
    return str.match(/cmsName_[0-9a-zA-Z\-_]+/)[0];
}

function getVal(prefix, str) {
 return String(new RegExp(prefix + "[\\w]*").exec(str)).substr(prefix.length)
}