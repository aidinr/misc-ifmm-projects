﻿Imports System.Data

Partial Class employee
    Inherits System.Web.UI.Page
    Public e_id As String = ""
    Public e_name As String = ""
    Public e_phone As String = ""
    Public e_email As String = ""
    Public e_position As String = ""
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not Request.QueryString("id") Is Nothing Then
            e_id = Request.QueryString("id")
        End If

        Dim EmployeeSQL As String = ""
        EmployeeSQL &= "Select u.UserID, u.FirstName, u.LastName,  u.Phone, u.Email, u.Position from ipm_user u "
        EmployeeSQL &= "join ipm_user_field_value v on u.UserID = v.User_ID and v.Item_ID = 21610584 and Item_value = '1' "
        EmployeeSQL &= "where u.Active = 'Y' and UserID = " & e_id & ""
        Dim EmployeeDT As New DataTable
        EmployeeDT = DBFunctions.GetDataTable(EmployeeSQL)
        If EmployeeDT.Rows.Count > 0 Then
            e_name = EmployeeDT.Rows(0).Item("FirstName").ToString.Trim & " " & EmployeeDT.Rows(0).Item("LastName").ToString.Trim
            e_phone = EmployeeDT.Rows(0).Item("Phone").ToString.Trim
            e_email = EmployeeDT.Rows(0).Item("Email").ToString.Trim
            e_position = EmployeeDT.Rows(0).Item("Position").ToString.Trim
        End If

        Dim EmployeeInfoSQL As String = ""
        EmployeeInfoSQL &= "select d.Item_Tag, v.Item_Value from ipm_user_field_value v "
        EmployeeInfoSQL &= "join ipm_user_field_desc d on v.Item_ID = d.Item_Id where User_ID = " & e_id & ""
        Dim EmployeeInfoDT As New DataTable
        Dim EmployeeInfoDR() As DataRow
        EmployeeInfoDT = DBFunctions.GetDataTable(EmployeeInfoSQL)
        If EmployeeInfoDT.Rows.Count > 0 Then

            'IMAGE
            EmployeeInfoDR = EmployeeInfoDT.Select("Item_Tag = 'IDAM_HIDEIMAGE'")
            If EmployeeInfoDR.Length > 0 Then
                If EmployeeInfoDR(0)("Item_value").ToString.Trim <> "1" Then
                    Dim e_ImageOut As String = ""
                    e_ImageOut &= "<div id=""personImg"">" & vbNewLine
                    e_ImageOut &= "<img src=""" & "/dynamic/image/always/contact/best/150x200/92/ffffff/Center/" & e_id & ".jpg" & """" & " alt=""""/>" & vbNewLine
                    e_ImageOut &= "</div>" & vbNewLine
                    e_Image_Label.Text = e_ImageOut
                End If
            End If

            'DEPARTMENTS
            Dim e_DepartmentsOUT As String = ""
            Dim e_dep1 As String = ""
            Dim e_dep2 As String = ""
            Dim e_dep3 As String = ""
            Dim e_dep4 As String = ""
            Dim e_IDAM_ECOM As String = ""
            Dim e_IDAM_MCOM As String = ""

            EmployeeInfoDR = EmployeeInfoDT.Select("Item_Tag = 'IDAM_ECOM'")
            If EmployeeInfoDR.Length > 0 Then
                If EmployeeInfoDR(0)("Item_value").ToString.Trim = "1" Then
                    e_IDAM_ECOM = "Executive Committee"
                End If
            End If
            EmployeeInfoDR = EmployeeInfoDT.Select("Item_Tag = 'IDAM_MCOM'")
            If EmployeeInfoDR.Length > 0 Then
                If EmployeeInfoDR(0)("Item_value").ToString.Trim = "1" Then
                    e_IDAM_MCOM = "Management Committee"
                End If
            End If
            EmployeeInfoDR = EmployeeInfoDT.Select("Item_Tag = 'IDAM_DEPT1'")
            If EmployeeInfoDR.Length > 0 Then
                If EmployeeInfoDR(0)("Item_value").ToString.Trim <> "" Then
                    e_dep1 = EmployeeInfoDR(0)("Item_value").ToString.Trim
                End If
            End If
            EmployeeInfoDR = EmployeeInfoDT.Select("Item_Tag = 'IDAM_DEPT2'")
            If EmployeeInfoDR.Length > 0 Then
                If EmployeeInfoDR(0)("Item_value").ToString.Trim <> "" Then
                    e_dep2 = EmployeeInfoDR(0)("Item_value").ToString.Trim
                    End If
                End If
                EmployeeInfoDR = EmployeeInfoDT.Select("Item_Tag = 'IDAM_DEPT3'")
                If EmployeeInfoDR.Length > 0 Then
                    If EmployeeInfoDR(0)("Item_value").ToString.Trim <> "" Then
                    e_dep3 = EmployeeInfoDR(0)("Item_value").ToString.Trim
                    End If
                End If
                EmployeeInfoDR = EmployeeInfoDT.Select("Item_Tag = 'IDAM_DEPT4'")
                If EmployeeInfoDR.Length > 0 Then
                    If EmployeeInfoDR(0)("Item_value").ToString.Trim <> "" Then
                    e_dep4 = EmployeeInfoDR(0)("Item_value").ToString.Trim
                    End If
            End If

            e_DepartmentsOUT &= e_IDAM_ECOM
            If e_IDAM_ECOM <> "" Then
                e_DepartmentsOUT &= ", " & e_IDAM_MCOM
            Else
                e_DepartmentsOUT &= e_IDAM_MCOM
            End If
            If e_IDAM_MCOM <> "" Then
                e_DepartmentsOUT &= ", " & e_dep1
            Else
                e_DepartmentsOUT &= e_dep1
            End If
            If e_dep1 <> "" Then
                e_DepartmentsOUT &= ", " & e_dep2
            Else
                e_DepartmentsOUT &= e_dep2
            End If
            If e_dep2 <> "" Then
                e_DepartmentsOUT &= ", " & e_dep3
            Else
                e_DepartmentsOUT &= e_dep3
            End If

                e_DepartmentsOUT &= e_dep3
           
                e_DepartmentsOUT = e_DepartmentsOUT.TrimEnd(" ")

                e_DepartmentsOUT = e_DepartmentsOUT.TrimEnd(",")

            e_departments_label.Text = FormatFunctions.AutoFormatText(e_DepartmentsOUT)
        End If
        'DESCRIPTION
        EmployeeInfoDR = EmployeeInfoDT.Select("Item_Tag = 'IDAM_WEBBIO'")
        If EmployeeInfoDR.Length > 0 Then
            If EmployeeInfoDR(0)("Item_value").ToString.Trim <> "" Then
                e_description.Text = FormatFunctions.AutoFormatText(EmployeeInfoDR(0)("Item_value").ToString.Trim)
            End If
        End If
        'OWNER

        Dim RightColumntOut As String = ""
        EmployeeInfoDR = EmployeeInfoDT.Select("Item_Tag = 'IDAM_Group_ExecutiveCommittee'")
        If EmployeeInfoDR.Length > 0 Then
            If EmployeeInfoDR(0)("Item_value").ToString.Trim = "1" Then
                RightColumntOut &= "Executive Committee" & "<br/>"
            End If
        End If
        EmployeeInfoDR = EmployeeInfoDT.Select("Item_Tag = 'IDAM_Group_ManagementCommittees'")
        If EmployeeInfoDR.Length > 0 Then
            If EmployeeInfoDR(0)("Item_value").ToString.Trim = "1" Then
                RightColumntOut &= "Management Committee" & "<br/>"
            End If
        End If

        EmployeeInfoDR = EmployeeInfoDT.Select("Item_Tag = 'IDAM_Group_Owners'")
        If EmployeeInfoDR.Length > 0 Then
            If EmployeeInfoDR(0)("Item_value").ToString.Trim = "1" Then
                RightColumntOut &= "Owner"
            End If
        End If
        e_isowner_label.Text = FormatFunctions.AutoFormatText(RightColumntOut)

    End Sub

End Class
