﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings("jsFolder")%>mootools-1.2.1-core-yc.js"></script>
<script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings("jsFolder")%>mootools-1.2-more.js"></script>
<script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings("jsFolder")%>milkbox.js"></script>
<script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings("jsFolder")%>swfobject.js"></script>
<script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings("jsFolder")%>main.js"></script>
<!--Millennium only-->

<script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings("jsFolder")%>jquery-1.2.3.pack.js"></script>

<script type="text/javascript">
    jQuery.noConflict();
</script>
<script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings("jsFolder")%>thickbox-compressed.js"></script>
<script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings("jsFolder")%>jquery.jcarousel.js"></script>


<link rel="stylesheet" type="text/css" href="<%=System.Configuration.ConfigurationManager.AppSettings("cssFolder")%>jquery.jcarousel.css" />
<link rel="stylesheet" type="text/css" href="<%=System.Configuration.ConfigurationManager.AppSettings("cssFolder")%>skins/tango/skin.css" />

<!--End Millennium Only-->
<link rel="stylesheet" href="<%=System.Configuration.ConfigurationManager.AppSettings("cssFolder")%>style.css" type="text/css" />
<link rel="stylesheet" href="<%=System.Configuration.ConfigurationManager.AppSettings("cssFolder")%>thickbox.css" type="text/css" />
<link rel="stylesheet" href="<%=System.Configuration.ConfigurationManager.AppSettings("cssFolder")%>milkbox/milkbox.css" type="text/css" />
<title><%=System.Configuration.ConfigurationManager.AppSettings("property") %></title>
</head>
<body>	
	<div id="container">
	
		<div id="screensaver">
		</div>
			<script type="text/javascript">
					    // <![CDATA[
			    var so = new SWFObject('<%=System.Configuration.ConfigurationManager.AppSettings("swfFolder")%>ScreenSaver.swf', "sotester", "1360", "768", "9", "#FFFFFF");
					    so.addParam("wmode", "opaque");
					    so.write("screensaver");
					    // ]]>
			</script>
		<div id="video"></div>
		<div id="video_swf"></div>	
							        			<script type="text/javascript">
							        			    // <![CDATA[				
							        			    var sp = new SWFObject('<%=System.Configuration.ConfigurationManager.AppSettings("swfFolder")%>video.swf', "sotester2", "1358", "768", "9", "#000000");
							        			    sp.addParam("wmode", "opaque");
							        			    sp.write("video_swf");
							        			    // ]]>
			                </script>
		<div id="page_container">
           
			<div id="main_home">
			     <div id="delray_home" style="position:absolute;top:0;left:0;">
			    <img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>home_image.jpg" alt="del ray home page" />
			    </div>
			    <!--
				<div id="overlay_home">
				<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>overlay_home.png" alt="overlay" />
				</div>		
				-->
				<!--<map name="home">
				<area onclick="fireVideo();" href="#" alt="River House Video" title="River House Video" shape="rect" coords="1078,596,1311,627" />
				</map>	
				<img usemap="#home" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>home.png" alt="Home Page" />
				-->
				<!--
				<table cellpadding="0" cellspacing="0" style="position:relative;top:25px;left:5px;">
				        <tr>
				            <td width="179" valign="top"><asp:Image ID="imgHomeLogo" runat="server" AlternateText="Logo" /></td>
				            <td width="25"><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" width="25" alt="spacer" /></td>
				            <td width="1106" valign="top"><asp:Image ID="imgHomeImage" runat="server" AlternateText="Home Image" /></td>
				        </tr>
				        <tr>
				            <td width="179" valign="bottom"></td>
				            <td width="25"><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" width="25" alt="spacer" /></td>
				            <td width="1106" valign="bottom">
				            <table width="100%" cellpadding="0" cellspacing="0">
				            <tr>
				                <td colspan="2">
				                <img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" alt="spacer" width="1" height="20" />
				                </td>
				            </tr>
				            <tr>
				                <td align="left"><!--<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>home_title.jpg" alt="Live Here. Be Everywhere." />--></td>
                                <!--				               
				                <td align="right"><!--<img onclick="fireVideo();" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>button_video.jpg" alt="View Video" />--></td>
				            <!--
				            </tr>
				            </table>
				            
				            </td>
				        </tr>
				</table>
				-->
				
			</div>

			<div id="main_live">
			    <!--
				<map name="live" id="live">
				<area href="#" class="navi_search" alt="Search for Your Home Now" title="Search for Your Home Now" shape="rect" coords="0,632,247,662" />
				<area onclick="Milkbox.showThisImage('<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>site_plan_large.png','River House Site Plan');" href="#" alt="Enlarge Site Plan" title="Enlarge Site Plan" shape="rect" coords="1140,456,1310,487" />
				</map>	
				-->
						
				<!--<img usemap="#live" id="navi_search" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>live_here.png" alt="Live Here" />-->
				<!--<map name="liveHereMap" id="liveHereMap">
                    <area shape="rect" coords="195,291,248,344" href="javascript:filterAndScroll(0);" alt="Below grade floor" />
                      <area shape="rect" coords="195,223,248,276" href="javascript:filterAndScroll(1);" alt="First floor" />
                      <area shape="rect" coords="195,160,248,213" href="javascript:filterAndScroll(2);" alt="Second floor" />
                      <area shape="rect" coords="195,97,248,150" href="javascript:filterAndScroll(3);" alt="Third floor" />
                      <area shape="rect" coords="195,34,248,87" href="javascript:filterAndScroll(4);" alt="Fourth floor" />
                </map>-->
				<table cellpadding="0" cellspacing="0" width="1313">
				    <!-- non millenium-->
				    
				    <tr>
				    
				   

				            <td class="liveLeft" colspan="2"><!--<asp:Image Width="447" Height="417" ID="imgLiveLeft" runat="server" />-->
				            
				            
 <div style="float: left; width: 438px; border-right: 7px solid white;">
   <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="438" height="421" id="Object1" align="middle"> 
	<param name="allowScriptAccess" value="sameDomain" /> 
	<param name="allowFullScreen" value="false" /> 
	<param name="wmode" value="transparent"> 
	<param name="movie" value="map1.swf" /> 
	<param name="quality" value="high" /> 
	<param name="FlashVars" value="xmlfile=community.xml" /> 
	<embed FlashVars="xmlfile=community.xml" src="map1.swf" quality="high" width="438" height="421" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" wmode="transparent" pluginspage="http://www.macromedia.com/go/getflashplayer"/> 
   </object> 
  </div> 
  <div style="float: left; width: 854px; height: 421px; border-right: 7px solid white;"><iframe src="fairway.html" name="complex" id="name="complex" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" width="854" height="421"></iframe></div>

				            
				            
				           <!-- <iframe src="swf/StCharles/stcharlesFlashXMLmap/select_community.html" name="complex" id="complex" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" width="1306" height="421"></iframe>-->
				            
				            
				            
				        
				            
				            
				            
				            </td>
				    </tr>
				    <tr>
				            <td colspan="2" height="25">&nbsp;</td>
				    </tr>
				    <tr>
				            <td style="line-height:20px;"><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>live_here_title.jpg" alt="Live Here!" /><br /><div style="color:Black;"><asp:Label ID="lblLiveHere" runat="server"></asp:Label></div><br /><br /><a href="#" class="navi_search"><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_button.jpg" alt="Search" /></a></td>
				            <td align="right" valign="top"><asp:Label ID="lblEnlarge" runat="server"></asp:Label>
				              <img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>title_featured.jpg" alt="Featured Residences" /><br /><br />
				                    <div id="featured_carousel">
				                    <ul id="mycarousel3" class="jcarousel jcarousel-skin-tango">
                                        <asp:Repeater ID="RepeaterFeaturedCarousel" runat="server">
                                        <ItemTemplate>
                                                <li onclick="loadApt('unit',this.id);$('main_details').fade('in');naviScroll.start(4299,0);" id="<%#Container.DataItem("Unit")%>"><img src="<%=Session("WSRetreiveAsset")%>size=1&width=131&height=131&crop=1&id=<%#Container.DataItem("Model_ASSETID")%>" width="131" height="131" /><p><%#Container.DataItem("Price_String")%><br /><small><%#Container.DataItem("Type").ToString.Replace("Bedroom", "BR")%> / <%#Container.DataItem("Bathroom").ToString%> BA</small></p></li>
                                        </ItemTemplate>
                                        </asp:Repeater>
                                     </ul>
                                     </div>
				            
				            </td>
				    </tr>  
				    
				    
				    <!--millenium floor selector-->
				    <!--
				    <tr>
				            <td colspan="2" id="floorSelector"></td>
				    </tr>
				    <tr>
				            <td colspan="2" height="25">
				            
				            <script type="text/javascript">
				                // <![CDATA[
				                var floorselector = new SWFObject('<%=System.Configuration.ConfigurationManager.AppSettings("swfFolder")%>FloorSelector.swf', "sotester3", "1313", "425", "9", "#FFFFFF");
				                floorselector.addParam("wmode", "opaque");
				                floorselector.write("floorSelector");
				                // ]]>
			                </script>
				            
				            &nbsp;</td>
				    </tr>
				    -->
				    <tr>
				            <td style="line-height:20px;"><!--<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>live_here_title.jpg" alt="Live Here!" /><br /><br /><asp:Label ID="lblLiveHereX" runat="server"></asp:Label><br /><br /><a href="#" class="navi_search"><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_button.jpg" alt="Search" /></a>--></td>
				            <td align="right" valign="top">
				                  
				            
				            <asp:Label ID="lblEnlargeX" runat="server"></asp:Label></td>
				    </tr> 
				    
				</table>
				
				<!--
				<div id="overlay_live">
				<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>overlay_live_here.png" alt="overlay" />
				</div>
				-->
			</div>
			
			<asp:PlaceHolder ID="PlaceholderSearch" runat="server"></asp:PlaceHolder>
			
			<div id="main_gallery">
				<div id="gallery_container">
				<div id="galleries">
					<div id="galleries_exterior">
					<asp:Label ID="gallery_exterior" runat="server"></asp:Label>
					</div>
					<div id="galleries_interior">
					<asp:Label ID="gallery_interior" runat="server"></asp:Label>
					</div>
					<div id="galleries_amenities">
					<asp:Label ID="gallery_amenities" runat="server"></asp:Label>
					</div>
					<div id="galleries_views">
					<asp:Label ID="gallery_views" runat="server"></asp:Label>
					</div>					
				</div>	
				</div>
				<div id="galleries_menu">
				<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>title_gallery.jpg" alt="Gallery Menu" />
				<img class="navi_gallery active_gallery" id="gallery_navi_exterior" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>gallery_exterior_on.jpg" alt="Gallery Menu" />
				<img class="navi_gallery" id="gallery_navi_interior" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>gallery_interior_off.jpg" alt="Gallery Menu" />
				<img class="navi_gallery" id="gallery_navi_amenities" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>gallery_amenities_off.jpg" alt="Gallery Menu" />
				<img class="navi_gallery" id="gallery_navi_views" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>gallery_views_off.jpg" alt="Gallery Menu" />
				</div>
			</div>
			<div id="main_amenities">
				<!--<map name="amenities">
				<area href="#" alt="See More Amenities" title="See More Amenities" shape="rect" coords="1139,630,1299,661" />
				</map>
				<img usemap="#amenities" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>amenities.png" alt="Amenities" />
				-->
				<table cellpadding="0" cellspacing="0">
				        <tr>
				            <td class="amenitiesLeft"><asp:Image ID="imgAmenitiesLeft" runat="server" /></td>
				            <td class="amenitiesCenter" ><asp:Image ID="imgAmenitiesCenter" runat="server" /></td>
				            <td class="amenitiesRight" ><asp:Image ID="imgAmenitiesRight" runat="server" /></td>
				        </tr>
				        <tr>
				            <td colspan="3">
				            <br />
				            <img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>amenities_title.jpg" alt="Amenities" />
				            <br /><div style="color:Black;line-height:20px;">
				            <asp:Repeater ID="repeaterAmenities" runat="server">
				            <ItemTemplate>
				            <div class="amenities_item"><%#Container.DataItem%></div>
				            </ItemTemplate>
				            </asp:Repeater>
				            </div>
				            </td>
				        </tr>
				</table>
			</div>
			<div id="main_neighborhood">
			<table cellpadding="0" cellspacing="0">
				        <tr >
				            <td style="width:200px;" valign=top><img class="" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>neighborhood_all.jpg" alt="" /><br /><br />
				            
				            <img class="navi_gallery" id="Img1" onclick="$('Iframeneighborhoods').src='transportation.html';" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>n_transportation_off.jpg" alt="Gallery Menu" /><br />
				            <img class="navi_gallery" id="Img2" onclick="$('Iframeneighborhoods').src='shopping.html';" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>n_shopping_off.jpg" alt="Gallery Menu" /><br />
				            <img class="navi_gallery" id="Img3" onclick="$('Iframeneighborhoods').src='services.html';" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>n_services_on.jpg" alt="Gallery Menu" /><br />
				            <img class="navi_gallery" id="Img4" onclick="$('Iframeneighborhoods').src='dining.html';" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>n_dining_off.jpg" alt="Gallery Menu" /><br />
				            			            
				            </td>
				            <td class="" >
				            
				            <br /><br />
				           <div style="float: left;  border: 7px solid white;"><iframe src="services.html" name="Iframeneighborhoods" id="Iframeneighborhoods" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" width="1080" height="600" ></iframe></div>
				            
				            
				            
				            </td>
				        </tr>
				        
				</table>
			</div>
			<div id="main_services">
				<!--<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>services.png" alt="Services" />-->
				<asp:PlaceHolder runat="server" ID="PlaceholderServices"></asp:PlaceHolder>
			</div>
			<div id="main_about">
				<!--<map name="about">
				<area onclick="Milkbox.showThisImage('<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>properties_map_large.png','Kettler Properties Map');" href="#" alt="Kettler Properties Map" title="Kettler Properties Map" shape="rect" coords="1130,456,1311,486" />
				</map>
				<img usemap="#about" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>about.png" alt="About Kettler" />-->
								<table cellpadding="0" cellspacing="0">
				        <tr>
				            <td valign="top" class="aboutLeft"><asp:Image ID="imgAboutLeft" runat="server" /></td>
				            <td valign="top" class="aboutRight"><asp:Image ID="imgAboutRight" runat="server" /></td>
				        </tr>
				        <tr>
				            <td valign="top"><br />
				            <img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>about_title.jpg" alt="" /><br />
				            <asp:Image ID="imgLogo" runat="server" />
				            <br /><br />
				            </td>
				            <td valign="top" style="color:Black;" align="right"><br /><!--<asp:Label ID="lblAboutMap" runat="server"></asp:Label>--></td>
				        </tr>
				        <tr>
				            <td valign="top" colspan=2 style="line-height:20px;color:Black; width:600px;"><asp:Label ID="lblAboutKettler" runat="server"></asp:Label><!--<asp:Image ID="imgAwards" runat="server" />--></td>
				           
				        </tr>
				</table>	
			</div>				
		</div>
		<div id="footer">
			<div id="navi">
				<div id="menu">
				<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_gap.jpg" alt="Gap" /><img class="navi active" id="navi_home" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_home_on.jpg" alt="Home" /><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_gap.jpg" alt="Gap" /><img class="navi" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_live_off.jpg" id="navi_live" alt="Live" /><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_gap.jpg" alt="Gap" /><img class="navi" id="navi_gallery" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_gallery_off.jpg" alt="Gallery" /><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_gap.jpg" alt="Gap" /><img class="navi" id="navi_amenities" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_amenities_off.jpg" alt="Amenities" /><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_gap.jpg" alt="Gap" /><img class="navi" id="navi_neighborhood" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_neighborhood_off.jpg" alt="Neighborhood" /><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_gap.jpg" alt="Gap" /><img class="navi" id="navi_services" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_services_off.jpg" alt="Resident Services" /><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_gap.jpg" alt="Gap" /><img class="navi" id="navi_about" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_about_off.jpg" alt="About Kettler" /><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>navi_gap.jpg" alt="Gap" />
				</div>
			</div>
		</div>
	</div>
	<ComponentArt:CallBack ID="CallbackReload" runat="server">
	<ClientEvents >
	<CallbackComplete EventHandler="reloadKiosk" />
	</ClientEvents>
	</ComponentArt:CallBack>
</body>
</html>

