package map2 {
	import flash.display.Sprite;
	import flash.text.*;
	import flash.filters.BitmapFilter;
    import flash.filters.BitmapFilterQuality;
    import flash.filters.DropShadowFilter;

		
	public class Box extends Sprite {
		private var outer:Sprite;
		private var inner:Sprite;
		private var borderWidth:int = 12;
		private var descTxt:TextField;
		private var textMargin:int = 20;
		
		public function Box() {
			XML.prettyIndent = 0;
			
			outer = new Sprite();
			outer.graphics.beginFill(uint(Map2.xml.settings.box.border_color));
			outer.graphics.drawRect(0, 0, int(Map2.xml.settings.box.width), int(Map2.xml.settings.box.height));
			outer.graphics.endFill();
			addChild(outer);
			
			inner = new Sprite();
			inner.graphics.beginFill(0xffffff);
			inner.graphics.drawRect(0, 0, int(Map2.xml.settings.box.width)-2*borderWidth, int(Map2.xml.settings.box.height)-2*borderWidth);
			
			inner.x = borderWidth;
			inner.y = borderWidth;
			
			inner.graphics.endFill();
			addChild(inner);
			
			mouseEnabled = false;
			mouseChildren = false;
			
			//create TF
			
			descTxt = new TextField();
			descTxt.selectable = false;
			descTxt.multiline = true;
			descTxt.wordWrap = true;
			descTxt.autoSize = TextFieldAutoSize.LEFT;
			descTxt.width = inner.width - 2*textMargin;
			descTxt.x = textMargin;
			descTxt.y = textMargin;
			descTxt.condenseWhite = true;
			
			
			var fmt:TextFormat = new TextFormat();
			fmt.font = Map2.xml.settings.box.font;
			//fmt.size = Map2.xml.settings.size;
			//fmt.color = uint(Map2.xml.settings.key_text_color);
						
			descTxt.defaultTextFormat = fmt;
			
			addChild(descTxt);
			
			filters = [getShadow()];
		}
		internal function showDesc(txt:String):void {
			descTxt.htmlText = txt;
			
			outer.height = int(Map2.xml.settings.box.height);
			inner.height = outer.height - 2 * borderWidth;
			
			if(descTxt.y + descTxt.height + 2*textMargin > outer.height) {
				//bg.height = tfDesc.y + tfDesc.height + margin;
				outer.height = descTxt.y + descTxt.height + 2 * textMargin;
				inner.height = outer.height - 2 * borderWidth;
			}
		}
		private function getShadow():BitmapFilter {
            var color:Number = 0x000000;
            var angle:Number = 45;
            var alpha:Number = 0.8;
            var blurX:Number = 8;
            var blurY:Number = 8;
            var distance:Number = 6;
            var strength:Number = 0.65;
            var inner:Boolean = false;
            var knockout:Boolean = false;
            var quality:Number = BitmapFilterQuality.HIGH;
            return new DropShadowFilter(distance,
                                        angle,
                                        color,
                                        alpha,
                                        blurX,
                                        blurY,
                                        strength,
                                        quality,
                                        inner,
                                        knockout);
        }

	}

}