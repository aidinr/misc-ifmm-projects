﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Search.ascx.vb" Inherits="ascx_Leesburg_Search" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<map name="liveHereMap" id="liveHereMap">
    <area shape="rect" coords="277,118,327,168" href="javascript:filterAndScroll('1604 Village Market Boulevard D');" alt="1" />
    <area shape="rect" coords="404,122,454,172" href="javascript:filterAndScroll('1606 Village Market Boulevard C');" alt="2" />
    <area shape="rect" coords="114,207,164,257" href="javascript:filterAndScroll('1601 Village Market Boulevard F/G');" alt="3" />
    <area shape="rect" coords="267,212,317,262" href="javascript:filterAndScroll('1603 Village Market Boulevard H');" alt="4" />
    <area shape="rect" coords="395,222,445,272" href="javascript:filterAndScroll('1605 Village Market Boulevard J');" alt="5" />  
    <area shape="rect" coords="503,269,553,319" href="javascript:filterAndScroll('1607 Village Market Boulevard K/L');" alt="6" />  
    <area shape="rect" coords="275,324,325,374" href="javascript:filterAndScroll('1501 Balch Drive U/V');" alt="7" />  
    <area shape="rect" coords="332,332,382,382" href="javascript:filterAndScroll('1500 Balch Drive S/T');" alt="8" />  
    <area shape="rect" coords="477,360,527,410" href="javascript:filterAndScroll('1609 Village Market Boulevard M');" alt="9" />  
</map>
				
								<div id="main_search">
				<table cellpadding="0" cellspacing="0" width="100%" border="0">
					<tr>
						<td align="left" valign="top">
							<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>title_live_here.jpg" alt="Live Here" /><br /><br />
						</td>
						<td align="left" valign="top">
							<div id="results_labels">
							    
							     
								<table width="100%">
									<tr>
										<td align="left">Results for: <span style="color: #ffffff;" id="SearchHeaderBuilding">All Buildings</span></td>
										<td align="right"><ComponentArt:CallBack ID="CallBackSearchHeader" runat="server">
							     <Content><asp:PlaceHolder ID="PlaceHolderSearchHeader" runat="server">Showing: <asp:Label ID="lblSearchResults" runat="server"></asp:Label> results</asp:PlaceHolder>
								</Content>
								</ComponentArt:CallBack></td>
									</tr>
								</table>
								
							</div>
						</td>
						<td>
						<!--empty-->
						</td>
					</tr>
					<tr>
						<td align="left" valign="top">
						<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>image_search.jpg" alt="Live Here" />
						<br /><br /><br />
						<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>title_refine_search.jpg" alt="Refine Search" /><br /><br />
						<div id="filter">
						<div class="search_filters"><div id="filter_building_value">Show All Buildings</div>
                            <table cellspacing="0" cellpadding="0" style="top:-240px;position:relative;">
 					            <tr>
						            <td width="4" height="4" class="filter_top_left"></td>
						            <td height="4" class="filter_top"></td>
						            <td width="4" height="4" class="filter_top_right"></td>
					            </tr>                   
					            <tr>
						            <td width="4" class="filter_left"></td>
						            <td align="left" class="navigation">
						            			    
							                <a href="javascript:doFilter('building','all');">Show All Buildings</a>
						                    <ComponentArt:CallBack ID="CallBackFilterBuilding" runat="server">
						                    <Content>
						                    <asp:PlaceHolder runat="server" ID="PlaceholderFilterBuilding">
						                    <asp:Repeater id="filter_building" runat="server">
						                    <ItemTemplate>
						                    <hr />
							                <a href="javascript:doFilter('building','<%#Container.DataItem("Building")%>');"><%#Container.DataItem("Building")%></a>
						                    </ItemTemplate>
						                    </asp:Repeater>
						                    </asp:PlaceHolder>
						                    </Content>
		                                    <LoadingPanelClientTemplate>
		                                    Loading...&nbsp;<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spinner.gif" width="16" height="16" border="0">
		                                    </LoadingPanelClientTemplate>			                    
						                    </ComponentArt:CallBack>  
						           </td>
						            <td class="filter_right"></td>

					            </tr>
					            <tr>
						            <td width="4" height="4" class="filter_bottom_left"></td>
						            <td height="4" class="filter_bottom"></td>
						            <td width="4" height="4" class="filter_bottom_right"></td>
					            </tr>
				            </table>
				        </div>    										
						<br />
						<div class="search_filters"><div id="filter_type_value">Show All Unit Types</div>
                                                      <table cellspacing="0" cellpadding="0">
 					            <tr>
						            <td width="4" height="4" class="filter_top_left"></td>
						            <td height="4" class="filter_top"></td>
						            <td width="4" height="4" class="filter_top_right"></td>
					            </tr>                   
					            <tr>
						            <td width="4" class="filter_left"></td>
						            <td align="left" class="navigation">			    
							             <a href="javascript:doFilter('type','all');">Show All Unit Types</a>
							                <ComponentArt:CallBack ID="CallBackFilterType" runat="server">
						                    <Content>
						                    <asp:PlaceHolder runat="server" ID="PlaceholderFilterType">
						                    <asp:Repeater id="filter_type" runat="server">
						                    <ItemTemplate>
						                    <hr />
							                <a href="javascript:doFilter('type','<%#Container.DataItem("Type")%>');"><%#Container.DataItem("Type")%></a>
						                    </ItemTemplate>
						                    </asp:Repeater>
						                    </asp:PlaceHolder>
						                    </Content>
		                                    <LoadingPanelClientTemplate>
		                                    Loading...&nbsp;<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spinner.gif" width="16" height="16" border="0">
		                                    </LoadingPanelClientTemplate>						                    
						                    </ComponentArt:CallBack> 						                    
            							 
						            </td>
						            <td class="filter_right"></td>

					            </tr>
					            <tr>
						            <td width="4" height="4" class="filter_bottom_left"></td>
						            <td height="4" class="filter_bottom"></td>
						            <td width="4" height="4" class="filter_bottom_right"></td>
					            </tr>
				            </table>
				            </div>				
						<br />
						<div class="search_filters"><div id="filter_price_value">Show All Price Range</div>
                                                      <table cellspacing="0" cellpadding="0">
 					            <tr>
						            <td width="4" height="4" class="filter_top_left"></td>
						            <td height="4" class="filter_top"></td>
						            <td width="4" height="4" class="filter_top_right"></td>
					            </tr>                   
					            <tr>
						            <td width="4" class="filter_left"></td>
						            <td align="left" class="navigation">
						                 <a href="javascript:doFilter('price','all');">Show All Price Range</a>		    
							                <ComponentArt:CallBack ID="CallBackFilterPrice" runat="server">
						                    <Content>
						                    <asp:PlaceHolder runat="server" ID="PlaceholderFilterPrice">
						                    <asp:Label ID="lblFilterPrice1" runat="server"></asp:Label>
						                    <asp:Label ID="lblFilterPrice2" runat="server"></asp:Label>
						                    <asp:Label ID="lblFilterPrice3" runat="server"></asp:Label>
						                    <asp:Label ID="lblFilterPrice4" runat="server"></asp:Label>
						                    </asp:PlaceHolder>
						                    </Content>
		                                    <LoadingPanelClientTemplate>
		                                    Loading...&nbsp;<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spinner.gif" width="16" height="16" border="0">
		                                    </LoadingPanelClientTemplate>						                    
						                    </ComponentArt:CallBack>
						            </td>
						            <td class="filter_right"></td>

					            </tr>
					            <tr>
						            <td width="4" height="4" class="filter_bottom_left"></td>
						            <td height="4" class="filter_bottom"></td>
						            <td width="4" height="4" class="filter_bottom_right"></td>
					            </tr>
				            </table>
				            </div>	
						<br />
						<div class="search_filters"><div id="filter_floor_value">Show All Floors</div>
                                                     <table cellspacing="0" cellpadding="0">
 					            <tr>
						            <td width="4" height="4" class="filter_top_left"></td>
						            <td height="4" class="filter_top"></td>
						            <td width="4" height="4" class="filter_top_right"></td>
					            </tr>                   
					            <tr>
						            <td width="4" class="filter_left"></td>
						            <td align="left" class="navigation">			    
							             <a href="javascript:doFilter('floor','all');">Show All Floors</a>
							             <hr />
							             <div id="filter_floor_buttons_container">
							                <ComponentArt:CallBack ID="CallBackFilterFloors" runat="server">
						                    <Content>
						                    <asp:PlaceHolder runat="server" ID="PlaceholderFilterFloors">							             
						                    <asp:Repeater id="filter_floor" runat="server">
						                    <ItemTemplate>
							                <div class="filter_floor_buttons" style="margin:7px;"><a href="javascript:doFilter('floor','<%#Container.DataItem("Floor")%>');"><%#Container.DataItem("Floor")%></a></div>
						                    </ItemTemplate>
						                    </asp:Repeater>
						                    </asp:PlaceHolder>
						                    </Content>
		                                    <LoadingPanelClientTemplate>
		                                    Loading...&nbsp;<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spinner.gif" width="16" height="16" border="0">
		                                    </LoadingPanelClientTemplate>						                    
						                    </ComponentArt:CallBack>
						                  </div> 							                    
            							 
						            </td>
						            <td class="filter_right"></td>

					            </tr>
					            <tr>
						            <td width="4" height="4" class="filter_bottom_left"></td>
						            <td height="4" class="filter_bottom"></td>
						            <td width="4" height="4" class="filter_bottom_right"></td>
					            </tr>
				            </table>
				            </div>					
						<br />
						</div>
						</td>
						<td align="left" valign="top">
							<div id="search_sort">
							<img id="sort_type" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_sort_unit_on_asc.png" alt="Sort Unit" /><img id="sort_building" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_sort_building_off.png" alt="Sort Building" /><img id="sort_floor" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_sort_floor_off.png" alt="Sort Floor" /><img id="sort_area" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_sort_area_off.png" alt="Sort Area" /><img id="sort_price" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_sort_price_off.png" alt="Sort Price" /><img id="sort_available" id="sort_unit" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_sort_available_off.png" alt="Sort Available" style="margin-right:0;" />
							</div>
						<div id="search_results">
 <ComponentArt:CallBack id="CallBack2" CacheContent="false" CssClass="CallBack" runat="server">
        <Content>
          <asp:PlaceHolder id="PlaceHolder2" runat="server">			
						<asp:Repeater id="search_results_rows" runat="server">
						<ItemTemplate>
							<div class="search_row" id="<%#Container.ItemIndex%>" onclick="loadApt(this.id);$('main_details').fade('in');naviScroll.start(4299,0);">
								<div class="unit">
								<table height="75" cellpadding="0" cellspacing="0">
								<tr>
								    <td valign="middle" align="center" style="background:#fff;">
								    <img align="center" src="<%=Session("WSRetreiveAsset")%>size=1&width=75&height=75&id=<%#Container.DataItem("Model_ASSETID")%>" alt="<%#Container.DataItem("Unit")%>" />
								    </td>
								    <td valign="middle" align="center" valign="middle">
								    <img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" alt="spacer" width="3" height="1"/><%#Container.DataItem("Type")%>
								    </td>
								</tr>
								</table>
								</div>
								<div class="building"><%#Container.DataItem("Building")%></div>
								<div class="floor"><%#Container.DataItem("Floor")%></div>
								<div class="sqf"><%#Container.DataItem("Area")%></div>
								<div class="price"><%#Container.DataItem("Price_String")%></div>
								<div class="available"><%#Container.DataItem("Available")%></div>
							</div>							
						</ItemTemplate>
						<FooterTemplate>
						<script type="text/javascript">
						    CallBackSearchHeader.callback($(document.body).getElements('.search_row').length);
						</script>
						</FooterTemplate>
						</asp:Repeater>
						<asp:Label ID="lblSearchResultsMore" runat="server"></asp:Label>
			</asp:PlaceHolder>
		</Content>
		<ClientEvents>
		<CallbackComplete EventHandler="reFade" />
		</ClientEvents>
		</ComponentArt:CallBack>
						</div>
						<div id="search_results_row_loading">
				      <table width="100%" height="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                      <tr>
                        <td align="center">
                        <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                          <td style="font-family:tahoma;font-size:10px;">Loading...&nbsp;</td>
                          <td><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spinner.gif" width="16" height="16" border="0"></td>
                        </tr>
                        </table>
                        </td>
                      </tr>
                      </table>
						</div>
						</td>
						<td align="left" valign="top" width="55">
							<div id="search_scroll">
								<img id="search_scroll_top" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_scroll_top_off.jpg" alt="Scroll Top" /><br />
								<img id="search_scroll_up" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_scroll_up_off.jpg" alt="Scroll Up" /><br />
								<img id="search_scroll_down" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_scroll_down_off.jpg" alt="Scroll Down" /><br />
								<img id="search_scroll_bottom" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>search_scroll_bottom_off.jpg" alt="Scroll Bottom" /><br />
							</div>
						</td>
					</tr>
					</table>
					</div>
					
					<div id="main_details">

				<ComponentArt:CallBack id="CallBack1"  CacheContent="False" CssClass="CallBack" runat="server" >
                   <Content>
                       <asp:PlaceHolder id="PlaceHolder1" runat="server">
				<div id="details_header">
				Results for: <span style="color:#ffffff;"><asp:Label ID="lblAptNumber" runat="server"></asp:Label></span> <img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>detail_header_gap.gif" alt="gap" /> &nbsp; <asp:Label ID="lblModelNumber" runat="server"> </asp:Label> &nbsp; <img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>detail_header_gap.gif" alt="gap" /> <span style="color:#ffffff;"><asp:Label ID="lblAptType" runat="server"></asp:Label></span>

				</div>
				<div id="details_body">
					<div id="details_gallery">
					<div id="details_gallery_image1"><asp:Label ID="lblDetailGalleryMainImage1" runat="server"></asp:Label></div>
					<div id="details_gallery_image2"><asp:Label ID="lblDetailGalleryMainImage2" runat="server"></asp:Label></div>
					</div>
					<div id="details_content">
					<big>Apartment Details</big>

					<br />
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td width="175">Apartment Number:</td>
							<td><b><asp:Label ID="lblAptNumber2" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">Building:</td>

							<td><b><asp:Label ID="lblAptBuilding" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">Type:</td>
							<td><b><asp:Label ID="lblAptType2" runat="server"></asp:Label></b></td>
						</tr>
						<tr>

							<td width="175">Bathroom(s):</td>
							<td><b><asp:Label ID="lblAptBath" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">Square Feet:</td>
							<td><b><asp:Label ID="lblAptArea" runat="server"></asp:Label></b></td>
						</tr>

						<tr>
							<td width="175">Floor Level:</td>
							<td><b><asp:Label ID="lblAptFloor" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">View(s):</td>
							<td><b><asp:Label ID="lblAptView" runat="server"></asp:Label></b></td>

						</tr>
						<tr>
							<td width="175">Price:</td>
							<td><b><asp:Label ID="lblAptPrice" runat="server"></asp:Label></b></td>
						</tr>
						<tr>
							<td width="175">Availability:</td>

							<td><b><asp:Label ID="lblAptAvailable" runat="server"></asp:Label></b></td>
						</tr>						
					</table>
					<br />
					<div id="gallery_detail_thumbs">
					<asp:Label ID="lblDetailGalleryThumbPrev" runat="server"></asp:Label><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" width="11" height="82" alt="thumbnails" />
					    <div id="gallery_detail_thumb_container"><div id="gallery_detail_thumb_images">
					    
					    <asp:Label ID="lblDetailGalleryFirstImage" runat="server"></asp:Label>
					    <asp:Repeater ID="gallery_detail_thumb_images_additional_image" runat="server">
					    <ItemTemplate>
					    <a href="javascript:loadDetailGallery('<%=Session("WSRetreiveAsset")%>size=1&crop=2&width=861&height=531&id=<%#Container.DataItem("Image_Asset_ID")%>');"><img src="<%=Session("WSRetreiveAsset")%>size=1&crop=1&width=80&height=80&id=<%#Container.DataItem("Image_Asset_ID")%>" alt="<%#Container.DataItem("Image_Asset_ID")%>" class="detail_thumb" /></a>
					    </ItemTemplate>
					    </asp:Repeater>
					    </div></div>
					<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" width="11" height="82" alt="thumbnails" /><asp:Label ID="lblDetailGalleryThumbNext" runat="server"></asp:Label>
					</div>
					<div id="gallery_detail_misc"><!--<img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>gallery_detail_button_email.jpg" alt="button" /><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" height="32" width="18" alt="spacer" />--><img id="print_button" onclick="window.print();" src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>gallery_detail_button_print.jpg" alt="button" /></div>
					</div>

					
				</div>
				<div id="details_navi"><a onclick="naviScroll.start(2866,0);$('main_details').fade('out');" href="#" class="navi_search"><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>gallery_detail_button_back.jpg" alt="navi" /></a><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" width="690" height="30" alt="navi" /><asp:Label id="lblAptPrev" runat="server"></asp:Label><img src="<%=System.Configuration.ConfigurationManager.AppSettings("imageFolder")%>spacer.gif" width="16" height="30" alt="navi" /><asp:Label id="lblAptNext" runat="server"></asp:Label></div>
				<script type="text/javascript">
				    fireDetailGallery();
				</script>
				</asp:PlaceHolder>
                </Content>

                  </ComponentArt:CallBack>


			</div>