﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Web.UI.WebControls
Partial Class CarouselCart
    Inherits System.Web.UI.Page

    Protected Sub CarouselCart_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim pageIndex As String = Request.QueryString("page")

        If (pageIndex = "") Then
            pageIndex = "0"
        End If

        If (Request.QueryString("action") = "delete") Then

            deleteFromCarousel(Request.QueryString("deleteid"))


        End If

        Dim dr As DataRow() = getCarouselDataRows()

        Try
            If (dr.Length > 0) Then


                RepeaterPrint.DataSource = dr
                RepeaterPrint.DataBind()

                Dim pds As PagedDataSource = New PagedDataSource()
                pds.DataSource = dr
                pds.AllowPaging = True
                pds.PageSize = 6


                If (pageIndex > pds.PageCount - 1 And pageIndex <> 0) Then
                    pds.CurrentPageIndex = pageIndex - 1
                ElseIf (pageIndex <> 0) Then
                    pds.CurrentPageIndex = pageIndex
                Else
                    pds.CurrentPageIndex = 0
                End If


                literalPaging.Text = "Page " & pds.CurrentPageIndex + 1 & " of " & pds.PageCount

                If (pds.CurrentPageIndex = 0) Then
                    imagePrev.ImageUrl = "images/DelRay/cart_prev_off.jpg"
                    linkPrev.NavigateUrl = "#"
                Else
                    imagePrev.ImageUrl = "images/DelRay/cart_prev_on.jpg"
                    linkPrev.NavigateUrl = "CarouselCart.aspx?page=" & pds.CurrentPageIndex - 1
                End If

                If (pds.CurrentPageIndex = pds.PageCount - 1) Then
                    imageNext.ImageUrl = "images/DelRay/cart_next_off.jpg"
                    linkNext.NavigateUrl = "#"
                Else
                    imageNext.ImageUrl = "images/DelRay/cart_next_on.jpg"
                    linkNext.NavigateUrl = "CarouselCart.aspx?page=" & pds.CurrentPageIndex + 1
                End If

                repeaterCarousel.DataSource = pds
                repeaterCarousel.DataBind()
            End If
        Catch ex As Exception
            imagePrev.Visible = False
            imageNext.Visible = False
            imagePrint.Visible = False
        End Try
        

    End Sub


    Public Function getCarouselDataRows() As DataRow()
        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)

        Dim assetID As String = ""
        Dim dr() As DataRow
        Dim sessionArrayList = CType(Session("Carousel"), ArrayList)
        If (sessionArrayList.Count > 0) Then
            For x = 0 To sessionArrayList.Count - 1
                assetID = assetID & sessionArrayList(x).ToString & ", "
            Next
            assetID = assetID.Substring(0, assetID.Length - 2)


            dr = ds.Tables("Units").Select("Asset_ID In (" & assetID & ")", "unit")

        End If
        Return dr

    End Function

    Public Sub deleteFromCarousel(ByVal assetID As String)
        Dim ds As DataSet
        ds = CType(Application("DS"), DataSet)

        Dim sessionArrayList = CType(Session("Carousel"), ArrayList)

        sessionArrayList.Remove(assetID)

        Session("Carousel") = sessionArrayList
    End Sub

    Public Sub repeaterCarousel_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim literalFloor As Literal = e.Item.FindControl("literalFloor")
            Dim lastFloor As String = e.Item.DataItem("Floor").ToString.Substring(e.Item.DataItem("Floor").ToString.Length - 1, 1)
            literalFloor.Text = e.Item.DataItem("Floor")
            If (lastFloor = 1) Then
                literalFloor.Text = literalFloor.Text & "st"
            ElseIf (lastFloor = 2) Then
                literalFloor.Text = literalFloor.Text & "nd"
            ElseIf (lastFloor = 3) Then
                literalFloor.Text = literalFloor.Text & "rd"
            Else
                literalFloor.Text = literalFloor.Text & "th"
            End If


        End If
    End Sub

    Public Sub repeaterPrint_ItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim ds As DataSet
            ds = CType(Application("DS"), DataSet)

            Dim iDAMPATHThumb As String = Replace(Session("WSRetreiveAsset") & "size=1&crop=1&width=80&height=80&id=", "&", "&amp;")
            Dim iDAMPATHFull As String = Replace(Session("WSRetreiveAsset") & "size=0&width=650&height=531&qfactor=25&crop=2&id=", "&", "&amp;")
            Dim iDAMPATHPrint As String = Replace(Session("WSRetreiveAsset") & "size=0&width=750&height=450&qfactor=25&crop=2&id=", "&", "&amp;")

            Dim AptPrice As String
            Dim AptArea As Integer

         

           

            AptArea = CInt(e.Item.DataItem("Area"))

            AptPrice = e.Item.DataItem("Price_String")

   

            Dim lblModelNumber As Label = e.Item.FindControl("printNumber")
            Dim lblAptArea As Label = e.Item.FindControl("printSQF")
            Dim lblAptAvailable As Label = e.Item.FindControl("printAvailability")
            Dim lblAptBath As Label = e.Item.FindControl("printBath")
            Dim lblAptFloor As Label = e.Item.FindControl("printFloor")
            Dim lblAptPrice As Label = e.Item.FindControl("printPrice")
            Dim lblAptType As Label = e.Item.FindControl("printType")
            Dim lblDetailMainImage As Label = e.Item.FindControl("printImage")
            Dim lblDetailThumb1 As Label = e.Item.FindControl("printImage1")
            Dim lblDetailThumb2 As Label = e.Item.FindControl("printImage2")
            Dim lblAptView As Label = e.Item.FindControl("printView")



            lblModelNumber.Text = e.Item.DataItem("Model_Name")
            lblAptArea.Text = String.Format("{0:n}", AptArea)
            lblAptArea.Text = AptArea
            lblAptAvailable.Text = e.Item.DataItem("Available")
            lblAptBath.Text = e.Item.DataItem("Bathroom")
            lblAptFloor.Text = e.Item.DataItem("Floor")
            lblAptPrice.Text = AptPrice
            lblAptType.Text = e.Item.DataItem("Type")
            lblAptView.Text = e.Item.DataItem("Amenities")


            'gallery
            lblDetailMainImage.Text = "<img src=""" & iDAMPATHPrint & e.Item.DataItem("Model_AssetID") & """ alt=""" & e.Item.DataItem("Unit") & """/>"
            lblDetailThumb1.Text = "<img src=""images/thumb_1.jpg"" />"
            lblDetailThumb2.Text = "<img src=""images/thumb_2.jpg"" />"
           



        End If

    End Sub




End Class
