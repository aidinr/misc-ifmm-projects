﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Partial Class _Default
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load




        If Request.Form.Keys.Count = 0 Then
            

            'Dim ds As DataSet
            'ds = CType(Application("DS"), DataSet)

            Dim WS As kettler_ws.Service = New kettler_ws.Service

            Dim ds As DataSet = New DataSet


            DS = WS.GetAllProjects()

            Application("DS") = ds
            PopulateHome(ds)
            PopulateLiveHere(ds)
            PopulateAmenities(ds)
            PopulateAbout(ds)
            PopulateGalleries(ds)
        End If

        Dim webcontrol1 As Web.UI.Control

        webcontrol1 = Me.LoadControl(System.Configuration.ConfigurationManager.AppSettings("ascxFolder") & "Services.ascx")

        Dim webcontrol As Web.UI.Control
        webcontrol = Me.LoadControl(System.Configuration.ConfigurationManager.AppSettings("ascxFolder") & "Search.ascx")


        PlaceholderServices.Controls.Add(webcontrol1)

        PlaceholderSearch.Controls.Add(webcontrol)


    End Sub
    Public Sub PopulateHome(ByVal theDataSet As DataSet)
        Dim iDAMPATHFull As String = Session("WSRetreiveAsset") & "size=0&id="
        Dim iDAMDownloadPathFull As String = ""
        
            For Each Row As DataRow In theDataSet.Tables("HomeAssets").Rows
                Select Case Row("name")
                    Case "home_logo"
                        imgHomeLogo.ImageUrl = iDAMPATHFull & Row("asset_id")
                        imgHomeLogo.AlternateText = Row("description")
                    Case "home_image"
                        imgHomeImage.ImageUrl = iDAMPATHFull & Row("asset_id")
                        imgHomeImage.AlternateText = Row("description")
                End Select
            Next

            'Non-Millennium
            'imgHomeLogo.Visible = True

            'Millennium
            imgHomeLogo.Visible = False

        

    End Sub
    Public Sub PopulateLiveHere(ByVal theDataSet As DataSet)
       
            Dim iDAMPATHFull As String = Session("WSRetreiveAsset") & "size=0&id="

            'Non(-Millennium)
            lblLiveHere.Text = theDataSet.Tables("LiveHereDescription").Rows(0)("descriptionmedium")
            For Each Row As DataRow In theDataSet.Tables("LiveHereAssets").Rows
                Select Case Row("name")
                    Case "live_left"
                        imgLiveLeft.ImageUrl = iDAMPATHFull & Row("asset_id")
                        imgLiveLeft.AlternateText = Row("description")
                    Case "live_right"
                        'imgLiveRight.ImageUrl = iDAMPATHFull & Row("asset_id")
                        'imgLiveRight.AlternateText = Row("description")
                    Case "site_plan"
                        If (System.Configuration.ConfigurationManager.AppSettings("property") = "Allegro") Then
                            lblEnlarge.Visible = False
                        Else
                            'lblEnlarge.Text = "<img onclick=""Milkbox.showThisImage('" & iDAMPATHFull & Row("asset_id") & "','" & Row("description") & "');"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "site_plan_button.jpg"" alt=""Enlarge Site Plan"" />"
                        End If

                End Select
            Next


            'Millennium below

            'imgLiveLeft.Visible = False
            'imgLiveRight.Visible = False


            'lblLiveHere.Text = theDataSet.Tables("LiveHereDescription").Rows(0)("descriptionmedium")

            Dim featuredUnits() As DataRow
            Dim ds As DataSet
            ds = CType(Application("DS"), DataSet)

            featuredUnits = ds.Tables("Units").Select("Featured = 1")

            RepeaterFeaturedCarousel.DataSource = featuredUnits
            RepeaterFeaturedCarousel.DataBind()



    End Sub
    Public Sub PopulateAmenities(ByVal theDataSet As DataSet)

        Dim iDAMPATHFull As String = Session("WSRetreiveAsset") & "size=0&id="

        For Each Row As DataRow In theDataSet.Tables("AmenitiesAssets").Rows
            Select Case Row("name")
                Case "amenities_left"
                    imgAmenitiesLeft.ImageUrl = iDAMPATHFull & Row("asset_id")
                    imgAmenitiesLeft.AlternateText = Row("description")
                Case "amenities_right"
                    imgAmenitiesRight.ImageUrl = iDAMPATHFull & Row("asset_id")
                    imgAmenitiesRight.AlternateText = Row("description")
                Case "amenities_center"
                    imgAmenitiesCenter.ImageUrl = iDAMPATHFull & Row("asset_id")
                    imgAmenitiesCenter.AlternateText = Row("description")
            End Select
        Next

        'lblLiveHere.Text = theDataSet.Tables("AmenitiesDescription").Rows(0)("descriptionmedium")
        Dim AmenitiesArray() As String = theDataSet.Tables("AmenitiesDescription").Rows(0)("descriptionmedium").ToString.Split(vbCrLf)



        repeaterAmenities.DataSource = AmenitiesArray
        repeaterAmenities.DataBind()

    End Sub

    Public Sub PopulateAbout(ByVal theDataSet As DataSet)

        Dim iDAMPATHFull As String = Session("WSRetreiveAsset") & "size=0&id="

        For Each Row As DataRow In theDataSet.Tables("AboutKettlerAssets").Rows
            Select Case Row("name")
                Case "about_left"
                    imgAboutLeft.ImageUrl = iDAMPATHFull & Row("asset_id")
                    imgAboutLeft.AlternateText = Row("description")
                Case "about_right"
                    imgAboutRight.ImageUrl = iDAMPATHFull & Row("asset_id")
                    imgAboutRight.AlternateText = Row("description")
                Case "about_map"
                    lblAboutMap.Text = "<img onclick=""Milkbox.showThisImage('" & iDAMPATHFull & Row("asset_id") & "','" & Row("description") & "');"" src=""" & System.Configuration.ConfigurationManager.AppSettings("imageFolder") & "button_map.jpg"" alt=""View Site Map"" />"
                Case "about_logo"
                    imgLogo.ImageUrl = iDAMPATHFull & Row("asset_id")
                    imgLogo.AlternateText = Row("description")
                Case "about_awards"
                    imgAwards.ImageUrl = iDAMPATHFull & Row("asset_id")
                    imgAwards.AlternateText = Row("description")
            End Select
        Next

        lblAboutKettler.Text = theDataSet.Tables("AboutKettlerDescription").Rows(0)("descriptionmedium")

    End Sub



    Public Sub PopulateGalleries(ByVal theDataSet As DataSet)
        'Begin display galleries

        Dim iDAMPATHThumb As String = Replace(Session("WSRetreiveAsset") & "size=1&crop=1&width=179&height=179&qfactor=" & System.Configuration.ConfigurationManager.AppSettings("qfactor") & "&id=", "&", "&amp;")
        Dim iDAMPATHFull As String = Replace(Session("WSRetreiveAsset") & "size=1&crop=1&width=1106&height=572&qfactor=" & System.Configuration.ConfigurationManager.AppSettings("qfactor") & "&id=", "&", "&amp;")

        For Each Row As DataRow In theDataSet.Tables("Galleries").Rows
            Select Case Row("Gallery_Category")
                Case "Exterior"
                    gallery_exterior.Text = gallery_exterior.Text & "<a href=""" & iDAMPATHFull & Row("Gallery_Image_ID") & """ rel=""milkbox[exterior]"" title=" & Row("Gallery_Image_Name") & "><img src=""" & iDAMPATHThumb & Row("Gallery_Image_ID") & """ alt=""Gallery""/></a>"
                Case "Interior"
                    gallery_interior.Text = gallery_interior.Text & "<a href=""" & iDAMPATHFull & Row("Gallery_Image_ID") & """ rel=""milkbox[interior]"" title=" & Row("Gallery_Image_Name") & "><img src=""" & iDAMPATHThumb & Row("Gallery_Image_ID") & """ alt=""Gallery""/></a>"
                Case "Amenities"
                    gallery_amenities.Text = gallery_amenities.Text & "<a href=""" & iDAMPATHFull & Row("Gallery_Image_ID") & """ rel=""milkbox[amenities]"" title=" & Row("Gallery_Image_Name") & "><img src=""" & iDAMPATHThumb & Row("Gallery_Image_ID") & """ alt=""Gallery""/></a>"
                Case "Views"
                    gallery_views.Text = gallery_views.Text & "<a href=""" & iDAMPATHFull & Row("Gallery_Image_ID") & """ rel=""milkbox[views]"" title=" & Row("Gallery_Image_Name") & "><img src=""" & iDAMPATHThumb & Row("Gallery_Image_ID") & """ alt=""Gallery""/></a>"
            End Select
        Next
        'End display galleries       

    End Sub

    Public Sub CallBackReload_Callback(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.CallBackEventArgs) Handles CallbackReload.Callback
        Session.Clear()
        Session.Abandon()
        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))

    End Sub

End Class