﻿
var SSTimeOut = 600000;
var SSTimer;

var currentPage = "main_home";
var currentPageOrder = 0;

var naviScroll;
var searchScroll;

function fireSS() {
    $('video').setStyle('display', 'none');
    $('video_swf').setStyle('display', 'none');
    $('screensaver').setStyle('display', 'block');
    clearTimeout(SSTimer);
    naviScroll.set(0, 0);
    $(document.body).getElement('.active').src = $(document.body).getElement('.active').src.replace("_on", "_off");
    $(document.body).getElement('.active').removeClass('active');
    $('navi_home').addClass('active');
    $('navi_home').src = $('navi_home').src.replace("_off", "_on");
}
function fireNaviState() {


    naviScroll = new Fx.Scroll('container', { duration: 1000, transition: Fx.Transitions.Quart.easeInOut });


    $(document.body).getElements('.navi').addEvent('mouseover', function() {
        currentSrc = $(this).src;
        if ($(this).hasClass("active") == false) {
            $(this).src = $(this).src.replace("_off", "_on");
        }
    });
    $(document.body).getElements('.navi').addEvent('mouseout', function() {
        if ($(this).hasClass("active") == false) {
            $(this).src = $(this).src.replace("_on", "_off");
        }

    });
    $(document.body).getElements('.navi').addEvent('click', function() {
        if ($(this).hasClass("active") == false) {
            $(document.body).getElement('.active').src = $(document.body).getElement('.active').src.replace("_on", "_off");
            $(document.body).getElement('.active').removeClass('active');
            $(this).addClass('active');
        }

    });

    $('navi_home').addEvent('click', function() {
        naviScroll.start(0, 0);
        $('main_search').fade('out');
        $('main_details').fade('out');
    });
    $('navi_live').addEvent('click', function() {
        naviScroll.start(1433, 0);
        $('main_search').fade('out');
        $('main_details').fade('out');
    });
    $(document.body).getElements('.navi_search').addEvent('click', function() {
        $('main_search').fade('in');
        naviScroll.start(2866, 0);
    });


    $('navi_gallery').addEvent('click', function() {
        naviScroll.start(5732, 0);
        $('main_search').fade('out');
        $('main_details').fade('out');
    });
    $('navi_amenities').addEvent('click', function() {
        naviScroll.start(7165, 0);
        $('main_search').fade('out');
        $('main_details').fade('out');
    });
    $('navi_neighborhood').addEvent('click', function() {
        naviScroll.start(8598, 0);
        $('main_search').fade('out');
        $('main_details').fade('out');
    });
    $('navi_services').addEvent('click', function() {
        naviScroll.start(10031, 0);
        $('main_search').fade('out');
        $('main_details').fade('out');
    });
    $('navi_about').addEvent('click', function() {
        naviScroll.start(11464, 0);
        $('main_search').fade('out');
        $('main_details').fade('out');
    });

}

var detailGalleryScrollIndex = 0;

function fireDetailGallery() {
    $('details_gallery_image2').fade('out');
    detailThumbGalleryScroll = new Fx.Scroll('gallery_detail_thumb_container', { duration: 1000, transition: Fx.Transitions.Quart.easeInOut });


    var detailGalleryThumbScrollPosition = 0;

    //$('gallery_detail_thumb_images').setStyle('width', 96 * ($(document.body).getElements('#gallery_detail_thumb_images img').length) + 'px');
    //$('gallery_detail_thumb_images').setStyle('border','1px solid orange');

    $("detail_gallery_thumb_prev").addEvent('click', function() {
        if (detailGalleryThumbScrollPosition != 0) {
            detailGalleryThumbScrollPosition -= 287;
            detailThumbGalleryScroll.start(detailGalleryThumbScrollPosition, 0);
        }
    });
    $("detail_gallery_thumb_next").addEvent('click', function() {
        detailGalleryThumbScrollPosition += 287;
        detailThumbGalleryScroll.start(detailGalleryThumbScrollPosition, 0);
    });


    //border click state for detail gallery
    $(document.body).getElements('.detail_thumb').addEvent('click', function() {
        $(document.body).getElements('.detail_thumb').removeClass('active_thumb');
        $(this).addClass('active_thumb');
    });
}

function fireGalleryNaviState() {


    galleryScroll = new Fx.Scroll('gallery_container', { duration: 1000, transition: Fx.Transitions.Quart.easeInOut });

    $('gallery_navi_views').addEvent('click', function() {
        galleryScroll.start(0, 1980);
    });
    $('gallery_navi_exterior').addEvent('click', function() {
        galleryScroll.start(0, 0);
    });
    $('gallery_navi_interior').addEvent('click', function() {
        galleryScroll.start(0, 660);
    });
    $('gallery_navi_amenities').addEvent('click', function() {
        galleryScroll.start(0, 1320);
    });

    $(document.body).getElements('.navi_gallery').addEvent('mouseover', function() {
        currentSrc = $(this).src;
        if ($(this).hasClass("active_gallery") == false) {
            $(this).src = $(this).src.replace("_off", "_on");
        }
    });
    $(document.body).getElements('.navi_gallery').addEvent('mouseout', function() {
        if ($(this).hasClass("active_gallery") == false) {
            $(this).src = $(this).src.replace("_on", "_off");
        }

    });
    $(document.body).getElements('.navi_gallery').addEvent('click', function() {
        if ($(this).hasClass("active_gallery") == false) {
            $(document.body).getElement('.active_gallery').src = $(document.body).getElement('.active_gallery').src.replace("_on", "_off");
            $(document.body).getElement('.active_gallery').removeClass('active_gallery');
            $(this).addClass('active_gallery');
        }

    });
}

function loadApt(param) //param = index number
{
    var maxLength = $(document.body).getElements(".search_row").length;
    if (maxLength == 51) {
        maxLength = 50;
    }
    CallBack1.callback(param, maxLength, searchFilter, searchSort);
}

function loadSearch(paramFilter, paramSort) {
    /*function to fade in and out here*/
    $('search_results').fade('out');
    $('search_results_row_loading').fade('in');
    searchScroll.toTop();
    CallBack2.callback(paramFilter, paramSort);
}
function reFade() {
    $('search_results').fade('in');
    $('search_results_row_loading').fade('out');

}
function rebuildFilters(paramFilter, penthouseFlag) {



    if (penthouseFlag == 0) { //not penthouse {
        CallBackFilterPenthouse.callback(paramFilter);
        CallBackFilterPrice.callback("price > 0");
        CallBackFilterType.callback("type not like 'penthouse*'");
        CallBackFilterFloors.callback("floor > -1 and floor <> 4");
    }
    else {//penthouse
        CallBackFilterPenthouse.callback("type like '%penthouse%'");
        CallBackFilterPrice.callback(paramFilter);
        CallBackFilterType.callback(paramFilter);
        CallBackFilterFloors.callback(paramFilter);
    }
    

}

/*Begin filter and sort for search result*/

var searchSort = "type asc";
var searchFilter = "";

var penthouseFilter = "building <> ''";
var floorFilter = "floor > 0";
var priceFilter = "price >= 0";
var typeFilter = "type <> ''";

function doSort(imgDOM) {
    currentImage = imgDOM.src;
    menuArray = new Array();
    menuArray = $(document.body).getElements('#search_sort img')

    for (i = 0; i < menuArray.length; i++) {
        if (menuArray[i].id != imgDOM.id) {
            menuArray[i].src = menuArray[i].src.replace("_on_asc", "_off");
            menuArray[i].src = menuArray[i].src.replace("_on_desc", "_off");
        }
    }


    if (currentImage.match("_on_asc") == "_on_asc") {
        imgDOM.src = imgDOM.src.replace("_on_asc", "_on_desc");
        searchSort = imgDOM.id.replace("sort_", "") + " desc";

    }
    else if (currentImage.match("_on_desc") == "_on_desc") {
        imgDOM.src = imgDOM.src.replace("_on_desc", "_on_asc");
        searchSort = imgDOM.id.replace("sort_", "") + " asc";
    }
    else {
        imgDOM.src = imgDOM.src.replace("_off", "_on_asc");
        searchSort = imgDOM.id.replace("sort_", "") + " asc";
    }
    loadSearch(searchFilter, searchSort);

}

function doFilter(filterType, filterConstraint) {

    penthouseFilter = '';

    switch (filterType) {
        case 'penthouse':

            //first reset all filters and clear all visual indicators

            floorFilter = "floor > -1";
            $('filter_floor_value').getParent().removeClass('search_filters_active');
            $('filter_floor_value').set('html', "Show All Floors");
            priceFilter = "price > 0";
            $('filter_price_value').getParent().removeClass('search_filters_active');
            $('filter_price_value').set('html', "Show All Price Range");
            typeFilter = "type not like 'penthouse*'";
            $('filter_type_value').getParent().removeClass('search_filters_active');
            $('filter_type_value').set('html', "Show All Unit Types");

            //end reset

            if (filterConstraint != "all") {
                penthouseFilter = "type = '" + filterConstraint + "'";
                $('filter_penthouse_value').getParent().removeClass('search_filters_active');
                $('filter_penthouse_value').getParent().addClass('search_filters_active');

                if (filterConstraint.length > 20) {
                    $('filter_penthouse_value').set('html', filterConstraint.replace("Bedroom", "BR"));
                }
                else {
                    $('filter_penthouse_value').set('html', filterConstraint);
                }
                $('SearchHeaderBuilding').set('html', filterConstraint);
            }
            else {
                penthouseFilter = "type like '%penthouse%'";
                //$('filter_penthouse_value').getParent().removeClass('search_filters_active');
                $('filter_penthouse_value').getParent().removeClass('search_filters_active');
                $('filter_penthouse_value').getParent().addClass('search_filters_active');                
                $('filter_penthouse_value').set('html', "Showing All Penthouse");
                $('SearchHeaderBuilding').set('html', "All Penthouse");
            }
            break;
        case 'type':

            //reset penthouse filter
            penthouseFilter = ''; 
            $('filter_penthouse_value').getParent().removeClass('search_filters_active');
            $('filter_penthouse_value').set('html', "Show Penthouse Lofts");

            if (filterConstraint != "all") {
                typeFilter = "type = '" + filterConstraint + "'";
                $('filter_type_value').getParent().removeClass('search_filters_active');
                $('filter_type_value').getParent().addClass('search_filters_active');
                $('filter_type_value').set('html', filterConstraint);
                $('SearchHeaderBuilding').set('html', filterConstraint);
            }
            else {
                typeFilter = "type not like 'penthouse*'";
                $('filter_type_value').getParent().removeClass('search_filters_active');
                $('filter_type_value').set('html', "Show All Apt. Types");
                $('SearchHeaderBuilding').set('html', "All Apartments");
            }
            break;
        case 'price':

            penthouseFilter = '';
            $('filter_penthouse_value').getParent().removeClass('search_filters_active');
            $('filter_penthouse_value').set('html', "Show Penthouse Lofts");

            $('filter_price_value').getParent().removeClass('search_filters_active');
            $('filter_price_value').getParent().addClass('search_filters_active');
            if (filterConstraint == 0) {
                priceFilter = "price >= 0 and price < 1000";
                $('filter_price_value').set('html', "$0 - $999+");
            }
            else if (filterConstraint == 1000) {
                priceFilter = "price >= 1000 and price < 2000";
                $('filter_price_value').set('html', "$1,000 - $1,999+");
            }
            else if (filterConstraint == 2000) {
                priceFilter = "price >= 2000 and price < 3000";
                $('filter_price_value').set('html', "$2,000 - $3,000");
            }
            else if (filterConstraint == 3000) {
                priceFilter = "price >= 3000";
                $('filter_price_value').set('html', "$3,000+");
            }
            else {
                priceFilter = "price > 0";
                $('filter_price_value').getParent().removeClass('search_filters_active');
                $('filter_price_value').set('html', "Show All Price Range");
            }
            break;
        case 'floor':

            penthouseFilter = '';
            $('filter_penthouse_value').getParent().removeClass('search_filters_active');
            $('filter_penthouse_value').set('html', "Show Penthouse Lofts");

            if (filterConstraint != "all") {
                floorFilter = "floor = " + filterConstraint;
                $('filter_floor_value').getParent().removeClass('search_filters_active');
                $('filter_floor_value').getParent().addClass('search_filters_active');
                $('filter_floor_value').set('html', filterConstraint);
            }
            else {
                floorFilter = "floor > -1";
                $('filter_floor_value').getParent().removeClass('search_filters_active');
                $('filter_floor_value').set('html', "Show All Floors");
            }
            break;
    }

    //hide hover menu

    $(document.body).getElements('#filter div.search_filters table').setStyle('left', '-999em');

    //building filter
    if(penthouseFilter != "") {
        searchFilter = penthouseFilter;
        rebuildFilters(searchFilter,0);
    }
    else {
        searchFilter = typeFilter + " and " + priceFilter + " and " + floorFilter;
        rebuildFilters(searchFilter, 1);
    }
    

    //rebuild filter with callbacks and load new search results
    //rebuildFilters(searchFilter);
    loadSearch(searchFilter, searchSort);
}

/*End filter and sort for search result*/
var activeGallery = 1;
var theImg;
var theDiv;
var theOtherDiv;

function loadDetailGallery(imgURL) {

    if (activeGallery == 1) {
        theImg = $(document.body).getElement('#details_gallery_image2 img');
        theDiv = $("details_gallery_image2");
        theOtherDiv = $("details_gallery_image1");
        activeGallery = 2;
    }
    else {
        theImg = $(document.body).getElement('#details_gallery_image1 img');
        theDiv = $("details_gallery_image1");
        theOtherDiv = $("details_gallery_image2");
        activeGallery = 1;
    }

    theImg.src = imgURL;

    theImg.onload = function() {
        theDiv.fade('in');
        theOtherDiv.fade('out');
    }

}

function fireVideo() {
    $('video').setStyle('display', 'block');
    $('video_swf').setStyle('display', 'block');

}

function filterAndScroll(theFloor) {
    if (theFloor != 4) {
        typeFilter = "type not like 'penthouse*'";
        priceFilter = "price > 0";
        floorFilter = "floor = " + theFloor;
        $('filter_price_value').getParent().removeClass('search_filters_active');
        $('filter_price_value').set('html', "Show All Price Range");
        $('filter_type_value').getParent().removeClass('search_filters_active');
        $('filter_type_value').set('html', "Show All Apt. Types");
        $('SearchHeaderBuilding').set('html', "All Apartments");
        $('filter_penthouse_value').getParent().removeClass('search_filters_active');
        $('filter_penthouse_value').set('html', "Show Penthouse Lofts");
        $('filter_floor_value').getParent().removeClass('search_filters_active');
        $('filter_floor_value').getParent().addClass('search_filters_active');
        $('filter_floor_value').set('html', theFloor);               
        searchFilter = typeFilter + " and " + priceFilter + " and " + floorFilter;
        rebuildFilters(searchFilter, 1); 
        loadSearch(searchFilter, searchSort);
    }
    else {
        doFilter('penthouse', 'all');
        $('filter_price_value').getParent().removeClass('search_filters_active');
        $('filter_price_value').set('html', "Show All Price Range");
        $('filter_type_value').getParent().removeClass('search_filters_active');
        $('filter_type_value').set('html', "Show All Apt. Types");
        $('SearchHeaderBuilding').set('html', "All Apartments");
        $('filter_floor_value').getParent().removeClass('search_filters_active');
        $('filter_floor_value').set('html', "Show All Floors");      
        $('filter_penthouse_value').getParent().removeClass('search_filters_active');
        $('filter_penthouse_value').getParent().addClass('search_filters_active');
    }
    $('main_search').fade('in');
    naviScroll.start(2866, 0);    

}

window.addEvent('domready', function() {



    //navi hover state
    fireNaviState();
    fireDetailGallery();
    fireGalleryNaviState();

    SSTimer = setTimeout(function() { fireSS(); }, SSTimeOut);

    var searchResults = $('search_results');
    searchScroll = new Fx.Scroll('search_results', { duration: 'long' });
    var rowHeight = 77;
    var rowMargin = 10;
    var scrollRow = 6;

    var scrollPerClick = (rowHeight + rowMargin) * scrollRow;

    //screen saver timer reset
    $('container').addEvent('click', function() {
        clearTimeout(SSTimer);
        SSTimer = setTimeout(function() { fireSS(); }, SSTimeOut);
    });
    $('screensaver').addEvent('click', function() {
        location.reload(true); //just reload to make sure everything resets
        //$('screensaver').setStyle('display','none');
    });


    $('search_scroll_top').addEvent('click', function() {
        searchScroll.toTop();
    });
    $('search_scroll_bottom').addEvent('click', function() {
        searchScroll.toBottom();
    });
    $('search_scroll_up').addEvent('click', function() {
        searchScroll.start(0, searchResults.scrollTop - scrollPerClick);
    });
    $('search_scroll_down').addEvent('click', function() {
        searchScroll.start(0, searchResults.scrollTop + scrollPerClick);
    });
    $(document.body).getElements('#search_sort img').addEvent('click', function() {
        doSort($(this));
    });

    $(document.body).getElements('#filter div.search_filters').addEvent('click', function() {
        //hide all and then show
        $(document.body).getElements('#filter div.search_filters table').setStyle('left', '-999em');
        $(this).getElement('table').setStyles({
            left: '200px',
            margin: '-27px 0 0 -10px'
        });

    });

    $('search_results_row_loading').fade('out');

    $('video_swf').addEvent('click', function() {
        $('video').setStyle('display', 'none');
        $('video_swf').setStyle('display', 'none');
    });


});   //End domstart