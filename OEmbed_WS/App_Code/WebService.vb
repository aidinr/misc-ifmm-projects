﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Newtonsoft.Json
Imports WillStrohl.API.oEmbed
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data
Imports WebArchives.iDAM.WebCommon.Security
Imports System.Net
Imports System.IO

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class WebService
     Inherits System.Web.Services.WebService
    Dim sBaseIP As String = ConfigurationSettings.AppSettings("IDAMLocation")
    Dim sBaseInstance As String = ConfigurationSettings.AppSettings("IDAMInstance")
    Dim sVSIDAMClient As String = ConfigurationSettings.AppSettings("IDAMLocationBrowse")
    Dim ClientSessionID As String
    Dim WSRetreiveAsset As String

    <WebMethod()> _
    Public Function GetIDAMAsset(asset_id As String, ByRef htmlOutput As String, ByRef xmlOutput As String) As String


        Dim ctlOEmbedRich As New WillStrohl.API.oEmbed.RichInfo
        Dim ctlOEmbedly As New WillStrohl.API.oEmbed.Providers.Embedly
        Dim ctlOohEmbed As New WillStrohl.API.oEmbed.Providers.oohEmbed
        Dim converter As New Newtonsoft.Json.Converters.XmlNodeConverter
        Dim ws As New WillStrohl.API.oEmbed.Wrapper
        Dim strJsonOutput As String
        'Starts the new session
        If WSRetreiveAsset = "" Then
            Session_Start()
        End If
        Dim url As String = WSRetreiveAsset & "id=" & asset_id & "&type=asset&size=0"

        ''ctlOEmbedRich = ctlOEmbedly.GetRichContentObject(url)

        strJsonOutput = GetWebRequest("http://api.embed.ly/1/oembed?key=1091eea890c111e193df4040aae4d8c9&url=" & Server.UrlEncode(url))
        JsonConvert.PopulateObject(strJsonOutput, ctlOEmbedRich)
        ctlOEmbedRich.ProviderUrl = url
        ' if no HTML content found
        If ctlOEmbedRich.Html = "" Then
            If ctlOEmbedRich.Type.ToString().ToLower() = "photo" Then
                ctlOEmbedRich.Html = BuildPhotoHTML(ctlOEmbedRich)
            Else
                ctlOEmbedRich.Html = BuildVideoHTML(ctlOEmbedRich)
            End If

        End If
        htmlOutput = ctlOEmbedRich.Html

        Dim doc As XmlDocument = CType(JsonConvert.DeserializeXmlNode(strJsonOutput, "ROOT"), XmlDocument)
        xmlOutput = doc.OuterXml.ToString()


        Return strJsonOutput
    End Function

    Private Function GetWebRequest(url As String) As String
        Dim webStream As Stream
        Dim webResponse = ""
        Dim req As HttpWebRequest
        Dim res As HttpWebResponse

        req = WebRequest.Create(url)
        req.Method = "GET"
        res = req.GetResponse()
        webStream = res.GetResponseStream()
        Dim webStreamReader As New StreamReader(webStream)

        While webStreamReader.Peek >= 0

            webResponse = webStreamReader.ReadToEnd()

        End While

        GetWebRequest = webResponse.ToString()

    End Function
    


    Private Function BuildPhotoHTML(ctlOEmbedRich As RichInfo) As String
        BuildPhotoHTML = "<img src=""" & ctlOEmbedRich.ProviderUrl & """ width=""" & ctlOEmbedRich.Width & """ height=""" & ctlOEmbedRich.Height & """ ></img>"
    End Function
    Private Function BuildVideoHTML(ctlOEmbedRich As RichInfo) As String
        
        BuildVideoHTML = "<iframe src=""" & ctlOEmbedRich.ProviderUrl & """ width=""" & ctlOEmbedRich.Width & """ height=""" & ctlOEmbedRich.Height & """ ></iframe>"
    End Function


    <WebMethod()> _
    Public Function GetImage(asset_id As String) As String


        Dim ctlOEmbedRich As New WillStrohl.API.oEmbed.RichInfo ' Wrapper
        Dim ctlOEmbedPhoto As New WillStrohl.API.oEmbed.PhotoInfo ' Wrapper
        Dim ctlOEmbed As New WillStrohl.API.oEmbed.Wrapper ' Wrapper
        Dim ctlOEmbedly As New WillStrohl.API.oEmbed.Providers.Embedly ' Wrapper
        Dim ctlOohEmbed As New WillStrohl.API.oEmbed.Providers.oohEmbed ' Wrapper
        Dim ctlOFlickr As New WillStrohl.API.oEmbed.Providers.Flickr ' Wrapper
        Dim requestinfo As New RequestInfo

        'Starts the new session
        'Starts the new session
        If WSRetreiveAsset = "" Then
            Session_Start()
        End If
        'Dim url As String = "http://idam.clarkrealty.com/IDAMClient/(F2KEGX55UBUMLVUDMJMABW45)/RetrieveAsset.aspx?instance=IDAM_CRC&id=200001951&type=asset&size=0&crop=1" 'Session("WSRetreiveAsset") & "id=" & asset_id & "&type=asset"
        Dim url As String = WSRetreiveAsset & "id=" & asset_id & "&type=asset&size=0"
        requestinfo.URL = url
        requestinfo.MaxHeight = 600
        requestinfo.MaxWidth = 800

        ctlOEmbedRich.ProviderUrl = url

        Dim converter As New Newtonsoft.Json.Converters.XmlNodeConverter

        'Dim paramArr() As String = New String() {"format=xml"}



        'Dim strOutput As String = ctlOEmbed.GetContent(New RequestInfo(url))
        'Dim strOutput As String = ctlOEmbed.GetContent(ctlOEmbedRich)
        'ctlOEmbedRich = ctlOohEmbed.GetRichContentObject(url)
        'ctlOEmbedPhoto = ctlOFlickr.GetPhotoObject(url, 150, 200)

        ctlOEmbedRich = ctlOEmbedly.GetRichContentObject(url)

        Dim strOutput As String = JsonConvert.SerializeObject(ctlOEmbedRich)
        Dim doc As XmlDocument = CType(JsonConvert.DeserializeXmlNode(strOutput, "root"), XmlDocument)
        Return doc.OuterXml.ToString()
        'Return strOutput
    End Function

    <WebMethod()> _
    Public Function GetVideo(asset_id As String) As String

        Dim ctlOEmbedRich As New WillStrohl.API.oEmbed.RichInfo ' Wrapper
        Dim ctlOEmbedPhoto As New WillStrohl.API.oEmbed.PhotoInfo ' Wrapper
        Dim ctlOEmbed As New WillStrohl.API.oEmbed.Wrapper ' Wrapper
        Dim ctlOEmbedly As New WillStrohl.API.oEmbed.Providers.Embedly ' Wrapper
        Dim ctlOohEmbed As New WillStrohl.API.oEmbed.Providers.oohEmbed ' Wrapper
        Dim ctlOFlickr As New WillStrohl.API.oEmbed.Providers.Flickr ' Wrapper
        Dim requestinfo As New RequestInfo

        'Starts the new session
        If WSRetreiveAsset = "" Then
            Session_Start()
        End If

        'Dim url As String = "http://idam.clarkrealty.com/IDAMClient/(F2KEGX55UBUMLVUDMJMABW45)/RetrieveAsset.aspx?instance=IDAM_CRC&id=200001951&type=asset&size=0&crop=1" 'Session("WSRetreiveAsset") & "id=" & asset_id & "&type=asset"
        'Dim url As String = "http://www.youtube.com/watch?v=hQTrUpmIgH0"
        Dim url As String = WSRetreiveAsset & "id=" & asset_id & "&type=asset&size=0"
        requestinfo.URL = url

        ctlOEmbedRich.ProviderUrl = url

        Dim converter As New Newtonsoft.Json.Converters.XmlNodeConverter

        'Dim paramArr() As String = New String() {"format=xml"}



        'Dim strOutput As String = ctlOEmbed.GetContent(New RequestInfo(url))
        'Dim strOutput As String = ctlOEmbed.GetContent(ctlOEmbedRich)
        'ctlOEmbedRich = ctlOohEmbed.GetRichContentObject(url)
        'ctlOEmbedPhoto = ctlOFlickr.GetPhotoObject(url, 150, 200)

        ctlOEmbedRich = ctlOEmbedly.GetRichContentObject(url)

        Dim strOutput As String = JsonConvert.SerializeObject(ctlOEmbedRich)
        Dim doc As XmlDocument = CType(JsonConvert.DeserializeXmlNode(strOutput, "root"), XmlDocument)
        Return doc.OuterXml.ToString()
        'Return strOutput
    End Function

    Sub Session_Start()
        ' Code that runs when a new session is started
        ' Code that runs when a new session is started
        Dim assetRetrieve = sBaseIP & "/" & sVSIDAMClient

        Dim theLogin As String = ConfigurationSettings.AppSettings("IDAMLogin")
        Dim thePassword As String = ConfigurationSettings.AppSettings("IDAMPassword")
        Dim loginStatus As Boolean = False
        Dim loginResult1 As String = ""
        Dim sessionID As String = "(S(" & GetSessionID("http://" & assetRetrieve & "/GetSessionID.aspx") & "))"
        ClientSessionID = sessionID
        Dim iDAMBrowse As New browseservice.BrowseService
        iDAMBrowse.Url = "http://" & assetRetrieve & "/" & sessionID & "/" & "BrowseService.asmx"
        Try

            loginResult1 = LoginToClient("http://" & assetRetrieve & "/" & sessionID & "/Login.aspx", theLogin, thePassword, sBaseInstance)

        Catch Ex As Exception
            Throw Ex
        End Try

        WSRetreiveAsset = "http://" & assetRetrieve & "/" + sessionID + "/RetrieveAsset.aspx?instance=" & sBaseInstance & "&"

        Dim loginResult2 As String = ""


    End Sub




    Private Function LoginToClient(ByVal pURL As String, ByVal pUsername As String, ByVal pPassword As String, ByVal pInstanceID As String) As Boolean
        Dim fullURL As String
        Try

            Dim wc As New Net.WebClient
            Dim result As String
            fullURL = GetLoginRequestURL(pURL, pUsername, pPassword, pInstanceID, False, False, False, 2)
            Dim data As IO.Stream = wc.OpenRead(fullURL)
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            Return (str.IndexOf("SUCCESS") >= 0)
        Catch ex As Exception
            'Log.Error("error logging in to :" + fullURL, ex)
            Return False
        End Try
    End Function


    Public Shared Function GetSessionID(ByVal URL As String) As String
        Try
            Dim clientreq As HttpWebRequest
            clientreq = HttpWebRequest.Create(URL)
            clientreq.Timeout = CType(10000, Integer)
            Dim clientres As HttpWebResponse = clientreq.GetResponse
            Dim data As IO.Stream = clientres.GetResponseStream
            Dim reader As IO.StreamReader = New IO.StreamReader(data)
            Dim str As String = ""
            str = reader.ReadToEnd
            If str.IndexOf("SESSIONID") >= 0 Then
                Dim strtidx As Integer = str.IndexOf("SESSIONID=") + 10
                Return str.Substring(strtidx, str.IndexOf("</span>") - strtidx)
            Else
                Throw New Exception("No session ID found. check URL if it has the proper format" + URL)
            End If
        Catch ex As Exception
            Throw New Exception("WebService not available." + URL)
        End Try
    End Function

    Public Const LOGIN As String = "txtLogin"
    Public Const PASSWORD As String = "txtPassword"
    Public Const INSTANCEID As String = "txtInstanceID"
    Public Const DIRECT As String = "direct"
    Public Const ENCRYPTED As String = "encrypted"
    Public Const AUTHENTICATIONLEVEL As String = "level"
    Public Const SECUREMODE As String = "secure"
    Public Const LDAPDOMAIN As String = "ld"
    Public Const LDAPPATH As String = "ldp"

    Public Shared Function GetLoginRequestURL(ByVal pLocation As String, ByVal pLogin As String, ByVal pPassword As String, ByVal pInstanceID As String, ByVal pSecureMode As Boolean, Optional ByVal pEncrypted As Boolean = False, Optional ByVal pDirectMode As Boolean = False, Optional ByVal pAuthenticationLevel As Integer = 0, Optional ByVal pLDAPDomain As String = "", Optional ByVal pLDAPPath As String = "") As String
        If pLocation Is Nothing Then
            Throw New ArgumentException("location cannot be null")
        End If
        If pLogin Is Nothing Then
            Throw New ArgumentException("login cannot be null")
        End If
        If pInstanceID Is Nothing Then
            Throw New ArgumentException("pInstanceID cannot be null")
        End If
        If pPassword Is Nothing Then
            Throw New ArgumentException("pPassword cannot be null")
        End If

        Dim url As New Text.StringBuilder
        url.Append(pLocation)
        url.Append("?")
        url.Append(LOGIN).Append("=").Append(pLogin).Append("&")
        url.Append(PASSWORD).Append("=").Append(pPassword).Append("&")
        url.Append(INSTANCEID).Append("=").Append(pInstanceID).Append("&")
        url.Append(DIRECT).Append("=").Append(pDirectMode.ToString).Append("&")
        url.Append(ENCRYPTED).Append("=").Append(pEncrypted.ToString).Append("&")
        url.Append(AUTHENTICATIONLEVEL).Append("=").Append(pAuthenticationLevel.ToString).Append("&")
        url.Append(SECUREMODE).Append("=").Append(pSecureMode.ToString).Append("&")
        If pLDAPDomain <> "" Then
            url.Append(LDAPDOMAIN).Append("=").Append(pLDAPDomain.ToString).Append("&")
        End If
        If pLDAPPath <> "" Then
            url.Append(LDAPPATH).Append("=").Append(pLDAPPath.ToString)
        End If


        Return url.ToString

    End Function
End Class