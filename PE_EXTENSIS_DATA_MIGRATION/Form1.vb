Imports System.Data.OleDb
Imports System.Configuration
Imports log4net
Imports log4net.Config
Imports System.IO
Imports System.Text.RegularExpressions
Imports Leadtools
Imports Leadtools.Codecs
Imports Leadtools.ImageProcessing


Public Class Form1
    Inherits System.Windows.Forms.Form
    Private Shared log As log4net.ILog

    Public IDAMROOT As String = ConfigurationSettings.AppSettings("IDAM.IDAMROOT")
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Public PEROOT As String = ConfigurationSettings.AppSettings("IDAM.PEROOT")


    Protected leadObject As Leadtools.IRasterImage
    Protected codecs As RasterCodecs

    Private sInstanceID As String
    Private defaultQFactor As Integer = 2
    Private disposed As Boolean = False
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Public Const CODEC_PATH As String = "C:\IDAM\bin\codecs\14.5.0.70(2.0)"


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub


    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    Public ConnectionStringIDAM As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
    Public ConnectionStringVISION As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringVISION")
    Public ConnectionStringEXTENSIS As String = ConfigurationSettings.AppSettings("IDAM.ConnectionStringEXTENSIS")

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents chkUpdateOnly As System.Windows.Forms.CheckBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.ListView1 = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.Button2 = New System.Windows.Forms.Button
        Me.chkUpdateOnly = New System.Windows.Forms.CheckBox
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.Button9 = New System.Windows.Forms.Button
        Me.Button10 = New System.Windows.Forms.Button
        Me.Button11 = New System.Windows.Forms.Button
        Me.Button12 = New System.Windows.Forms.Button
        Me.Button13 = New System.Windows.Forms.Button
        Me.Button14 = New System.Windows.Forms.Button
        Me.Button15 = New System.Windows.Forms.Button
        Me.Button16 = New System.Windows.Forms.Button
        Me.Button17 = New System.Windows.Forms.Button
        Me.Button18 = New System.Windows.Forms.Button
        Me.Button19 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(513, 46)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(208, 32)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Execute PE Data Extraction"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(16, 368)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(472, 23)
        Me.ProgressBar1.TabIndex = 2
        '
        'ListView1
        '
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.ListView1.FullRowSelect = True
        Me.ListView1.Location = New System.Drawing.Point(16, 56)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(472, 304)
        Me.ListView1.TabIndex = 3
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "ProjectName"
        Me.ColumnHeader1.Width = 500
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(136, 416)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(232, 32)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Add Project Image"
        '
        'chkUpdateOnly
        '
        Me.chkUpdateOnly.Checked = True
        Me.chkUpdateOnly.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkUpdateOnly.Location = New System.Drawing.Point(408, 24)
        Me.chkUpdateOnly.Name = "chkUpdateOnly"
        Me.chkUpdateOnly.Size = New System.Drawing.Size(112, 16)
        Me.chkUpdateOnly.TabIndex = 5
        Me.chkUpdateOnly.Text = "Update Only"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(8, 464)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(144, 32)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "Pre-Process FileListing"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(16, 416)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(88, 32)
        Me.Button4.TabIndex = 7
        Me.Button4.Text = "Add Asset Overrides"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(392, 416)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(96, 32)
        Me.Button5.TabIndex = 8
        Me.Button5.Text = "Project UDFS"
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(136, 480)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(232, 32)
        Me.Button6.TabIndex = 9
        Me.Button6.Text = "Project Plate Upload"
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(384, 488)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 10
        Me.Button7.Text = "Button7"
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(112, 568)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(304, 56)
        Me.Button8.TabIndex = 11
        Me.Button8.Text = "Remap Assets / projects /alliances"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(144, 664)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(288, 72)
        Me.Button9.TabIndex = 12
        Me.Button9.Text = "Remap Project Plates"
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(564, 12)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(112, 23)
        Me.Button10.TabIndex = 13
        Me.Button10.Text = "UDFs and Keywords"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(16, 15)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(208, 32)
        Me.Button11.TabIndex = 14
        Me.Button11.Text = "Monitor DFS"
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(513, 103)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(208, 32)
        Me.Button12.TabIndex = 15
        Me.Button12.Text = "Execute PE Network Location "
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(513, 212)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(208, 32)
        Me.Button13.TabIndex = 16
        Me.Button13.Text = "Execute Add Files to IDAM Repo"
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(513, 278)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(208, 32)
        Me.Button14.TabIndex = 17
        Me.Button14.Text = "Execute Add Files IDAM Queue"
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(757, 7)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(112, 43)
        Me.Button15.TabIndex = 18
        Me.Button15.Text = "Keywords Mapping from Extensis"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(256, 15)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(112, 32)
        Me.Button16.TabIndex = 19
        Me.Button16.Text = "Vision Import"
        Me.Button16.UseVisualStyleBackColor = True
        '
        'Button17
        '
        Me.Button17.Location = New System.Drawing.Point(766, 159)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(112, 43)
        Me.Button17.TabIndex = 20
        Me.Button17.Text = "Vision User Import"
        Me.Button17.UseVisualStyleBackColor = True
        '
        'Button18
        '
        Me.Button18.Location = New System.Drawing.Point(766, 273)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(112, 43)
        Me.Button18.TabIndex = 21
        Me.Button18.Text = "MR to HR script"
        Me.Button18.UseVisualStyleBackColor = True
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(699, 393)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(105, 32)
        Me.Button19.TabIndex = 22
        Me.Button19.Text = "Test File Integrity"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(968, 458)
        Me.Controls.Add(Me.Button19)
        Me.Controls.Add(Me.Button18)
        Me.Controls.Add(Me.Button17)
        Me.Controls.Add(Me.Button16)
        Me.Controls.Add(Me.Button15)
        Me.Controls.Add(Me.Button14)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.chkUpdateOnly)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.ListView1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form1"
        Me.Text = "PE DFS Monitor"
        Me.ResumeLayout(False)

    End Sub

#End Region







    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'preproccess
        ExecuteTransaction("update ipm_phase set KeyName = 'Select a phase...' where KeyID = 0")

        'add apply to website tag
        ExecuteTransaction("delete from IPM_ASSET_FIELD_VALUE where Item_ID = 2400401")
        ExecuteTransaction("insert into  IPM_ASSET_FIELD_VALUE select asset_id,2400401 item_id,1 item_value from IPM_ASSET where Available = 'Y' and Category_ID in (select Category_ID from IPM_ASSET_CATEGORY where Name = 'Public Website Images')")
        initproc()
        addudfsandindex()
    End Sub


    Public Function GetFileContents(ByVal FullPath As String, _
       Optional ByRef ErrInfo As String = "") As String

        Dim strContents As String
        Dim objReader As StreamReader
        Try

            objReader = New StreamReader(FullPath)
            strContents = objReader.ReadToEnd()
            objReader.Close()
            Return strContents
        Catch Ex As Exception
            ErrInfo = Ex.Message
        End Try
    End Function




    Private Sub initproc()

        'get normalized data
        'create project info (shortname longname descriptionlong publish country city state address, alliance, 
        'get category for projects
        'create alliance
        'create project
        'create marketing folder
        'add assets
        'add ordering to asset
        'add related projects to projectcategories with ordering


        'get category id to populate
        Dim catIDUploadLocation As String = GetDataTable("select category_id from ipm_category where name = 'Projects' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Dim AllAssets As DataTable = GetDataTable(GetFileContents(System.AppDomain.CurrentDomain.BaseDirectory() + "sqlassetselect.txt"), ConnectionStringEXTENSIS)  'and oid = '041018.00'


        Dim i As Integer
        ProgressBar1.Maximum = AllAssets.Rows.Count
        ProgressBar1.Value = 0

        For i = 0 To AllAssets.Rows.Count - 1
            Try
                ProgressBar1.Value = ProgressBar1.Value + 1
                If Me.chkUpdateOnly.Checked Then
                    Dim sAlliance As String = ""
                    Dim sAllianceFolder, sVisionProjectNumber As String
                    Dim sAllianceCatID, catID, sProjectNumber As String
                    Dim sProjectID As String = ""
                    catID = sProjectID
                    Dim sProjectName As String = AllAssets.Rows(i)("projectnamemerg").trim
                    ListView1.Items.Add(sProjectName + ": " + AllAssets.Rows(i)("projectnamemerg"))

                    ListView1.Refresh()
                    Me.Refresh()


                    Dim sDirectoryRoot As String = "V:\"


                    'get vision project number
                    'get Alliance
                    If AllAssets.Rows(i)("vision_project_number") Is System.DBNull.Value Then
                        sVisionProjectNumber = ""
                    Else
                        sVisionProjectNumber = AllAssets.Rows(i)("vision_project_number")
                    End If
                    'sVisionProjectNumber = AllAssets.Rows(i)("vision_project_number")

                    'get Alliance
                    If AllAssets.Rows(i)("practicearea") Is System.DBNull.Value Then
                        sAlliance = "Miscellaneous"
                    Else
                        sAlliance = AllAssets.Rows(i)("practicearea")
                    End If


                    'create alliance dir if needed
                    'does it exist already?
                    sAllianceCatID = ""
                    If GetDataTable("select category_id from ipm_category where parent_cat_id = " & catIDUploadLocation & " and name = '" & sAlliance.Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sAllianceCatID = GetDataTable("select category_id from ipm_category where parent_cat_id = " & catIDUploadLocation & " and name = '" & sAlliance.Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("Category_id")
                    Else

                        sAllianceCatID = CreateNewAllianceCategory(sAlliance, catIDUploadLocation, "1")
                    End If


                    'create project if needed
                    If GetDataTable("select projectid from ipm_project where category_id = " & sAllianceCatID & " and name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sProjectID = GetDataTable("select projectid from ipm_project where category_id = " & sAllianceCatID & " and name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("projectid")
                    Else
                        sProjectID = CreateNewProject(sProjectName.Trim, sAllianceCatID, sVisionProjectNumber)
                    End If


                    '                    Item_ID(Item_Tag)
                    '2401275:            IDAM_EXTENSIS_PROJECTNUM()
                    '2401276:            IDAM_COSENTIAL_PROJECTNUM()

                    '2401275:            IDAM_EXTENSIS_PROJECTNUM()
                    '2401276:            IDAM_COSENTIAL_PROJECTNUM()

                    'get vision number


                    'copy cossential value to udf
                    'get value
                    Dim dtCossentialid As New DataTable
                    dtCossentialid = GetDataTable("select projectnumber from ipm_project where projectid = " & sProjectID & " and available = 'Y'", ConnectionStringIDAM)
                    If dtCossentialid.Rows.Count > 0 And GetDataTable("select * from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 2401276", ConnectionStringIDAM).Rows.Count = 0 Then
                        'erase any possibel previous value
                        ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 2401276")
                        If Not IsDBNull(dtCossentialid.Rows(0)("projectnumber")) Then
                            ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",2401276,'" & dtCossentialid.Rows(0)("projectnumber") & "')")
                        End If

                    End If

                    'extensis id
                    If isullcheck(AllAssets.Rows(i)("extensis_projectnumber")) <> "" Then
                        'erase any possibel previous value
                        ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 2401275")
                        ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",2401275,'" & AllAssets.Rows(i)("extensis_projectnumber").Replace("'", "''") & "')")
                    End If

                    'set vision project number
                    ExecuteTransaction("update ipm_project set projectnumber = '" & sVisionProjectNumber.Replace("'", "''") & "' where projectid = " & sProjectID)





                    'add asset
                    Dim sAssetName As String = ""
                    sAssetName = AllAssets.Rows(i)("filename").ToString.Trim



                    Dim sProjectfolderID As String = ""
                    'create alliance dir if needed
                    'does it exist already?
                    sProjectfolderID = ""
                    If GetDataTable("select category_id from ipm_asset_category where name = 'Marketing' and projectid = " & sProjectID & " and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sProjectfolderID = GetDataTable("select category_id from ipm_asset_category where name = 'Marketing' and projectid = " & sProjectID & " and available = 'Y'", ConnectionStringIDAM).Rows(0)("Category_id")
                    Else
                        sProjectfolderID = checkorCreateProjectfolder("0", "Marketing", sProjectID, True)
                    End If


                    'get media_type
                    'get extension
                    Dim sExtensionid As String = ""
                    Dim sExtensiontmp() = AllAssets.Rows(i)("filename").split(".")
                    Dim sExtension As String
                    If sExtensiontmp.Length = 1 Then
                        sExtension = "UNK"
                    Else
                        sExtensiontmp.Reverse(sExtensiontmp)
                        sExtension = sExtensiontmp(0).ToString.ToUpper
                    End If
                    'check to see if in filetype_lookup
                    imTypeid = imTypeid + 1
                    Try
                        sExtensionid = GetDataTable("select * from ipm_filetype_lookup where extension = '" & sExtension & "'", Me.ConnectionStringIDAM).Rows(0)("media_type")
                    Catch ex As Exception

                        'add extension
                        Me.ExecuteTransaction("insert into ipm_filetype_lookup (media_type,name,extension,useicon) values (" & imTypeid & ",'" & sExtension & " File" & "','" & sExtension & "',1)")
                        sExtensionid = imTypeid
                    End Try

                    'check to see if asset already exists and use update
                    If GetDataTable("select asset_id from ipm_asset where projectid = " & sProjectID & " and filename = '" & AllAssets.Rows(i)("filename").ToString.Trim.Replace("'", "''") & "'", ConnectionStringIDAM).Rows.Count = 0 Then
                        Me.ExecuteTransaction("insert into ipm_asset(description,filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date,oid) values ('','" & AllAssets.Rows(i)("filename").replace("'", "''") & "'," & sProjectID & ",'1'," & AllAssets.Rows(i)("record_id") & ",1,1,'" & sAssetName.Replace("'", "''") & "'," & sExtensionid & "," & sProjectID & "," & sProjectfolderID & ",getdate(),getdate()," & AllAssets.Rows(i)("record_id").ToString.Trim & ")")
                    End If



                    'add project number
                    'If sVisionProjectNumber <> "" Then

                    'ExecuteTransaction("update ipm_project set projectnumber = '" & sVisionProjectNumber.Replace("'", "''") & "' where projectid = " & catID)

                    'add longname
                    'get info from source
                    'idam_project_shortname

                    '' ''Dim sProjectDescription As String = AllProjects.Rows(i)("description")

                    '' ''Dim saddress1 As String = isullcheck(AllProjects.Rows(i)("projaddress"))
                    '' ''Dim saddress2 As String = isullcheck(AllProjects.Rows(i)("projaddress2"))

                    '' ''Dim scity As String = isullcheck(AllProjects.Rows(i)("projcity"))
                    ' '' ''Dim sstate As String = isullcheck(AllProjects.Rows(i)("projstateid"))
                    '' ''Dim szip As String = isullcheck(AllProjects.Rows(i)("projzip"))

                    'ExecuteTransaction("update ipm_project set descriptionmedium = '" & sProjectDescription.Trim.Replace("'", "''") & "' where projectid = " & sProjectID)



                    ' '' ''If isullcheck(sProjectDescription) <> "" Then
                    ' '' ''    'erase any possibel previous value
                    ' '' ''    ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 2400108")
                    ' '' ''    ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",2400108,'" & sProjectDescription.Replace("'", "''") & "')")
                    ' '' ''End If

                    ' '' ''ExecuteTransaction("update ipm_project set address = '" & saddress1.Trim.Replace("'", "''") & vbCrLf & saddress2.Trim.Replace("'", "''") & vbCrLf & "' where projectid = " & sProjectID)
                    ' '' ''If isullcheck(AllProjects.Rows(i)("projaddress")) <> "" Then
                    ' '' ''    'erase any possibel previous value
                    ' '' ''    ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 2400111")
                    ' '' ''    ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",2400111,'" & AllProjects.Rows(i)("projaddress").Replace("'", "''") & "')")
                    ' '' ''End If

                    ' '' ''ExecuteTransaction("update ipm_project set city = '" & scity.Trim.Replace("'", "''") & "' where projectid = " & sProjectID)
                    ' '' ''If isullcheck(AllProjects.Rows(i)("projcity")) <> "" Then
                    ' '' ''    'erase any possibel previous value
                    ' '' ''    ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 2400112")
                    ' '' ''    ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",2400112,'" & AllProjects.Rows(i)("projcity").Replace("'", "''") & "')")
                    ' '' ''End If

                    ' '' ''If isullcheck(AllProjects.Rows(i)("statename")) <> "" Then
                    ' '' ''    'erase any possibel previous value
                    ' '' ''    ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 2400109")
                    ' '' ''    ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",2400109,'" & AllProjects.Rows(i)("statename").Replace("'", "''") & "')")
                    ' '' ''End If


                    ' '' ''If CType(isullcheck(AllProjects.Rows(i)("projstateid")), String) <> "" Then


                    ' '' ''    If CType(isullcheck(AllProjects.Rows(i)("projstateid")), Integer) < 52 Then
                    ' '' ''        ExecuteTransaction("update ipm_project set state_id = '" & CType(isullcheck(AllProjects.Rows(i)("projstateid")), Integer) & "' where projectid = " & sProjectID)
                    ' '' ''    End If
                    ' '' ''End If
                    '' ''ExecuteTransaction("update ipm_project set zip = '" & szip.Trim.Replace("'", "''") & "' where projectid = " & sProjectID)
                    '' ''If isullcheck(AllProjects.Rows(i)("projzip")) <> "" Then
                    '' ''    'erase any possibel previous value
                    '' ''    ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 2400110")
                    '' ''    ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",2400110,'" & AllProjects.Rows(i)("projzip").Replace("'", "''") & "')")
                    '' ''End If
                    '' ''If isullcheck(AllProjects.Rows(i)("header")) <> "" Then
                    '' ''    'erase any possibel previous value
                    '' ''    ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 2400053")
                    '' ''    ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",2400053,'" & RemoveHtml(AllProjects.Rows(i)("header")).Replace("'", "''") & "')")
                    '' ''End If

                    '' ''If isullcheck(AllProjects.Rows(i)("countryname")) <> "" Then
                    '' ''    'erase any possibel previous value
                    '' ''    ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 2400054")
                    '' ''    ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",2400054,'" & RemoveHtml(AllProjects.Rows(i)("countryname")).Replace("'", "''") & "')")
                    '' ''End If





                    ' End If ' end vision check
                End If

            Catch ex As Exception

                Dim ErrorID As String
                Try
                    ErrorID = GetDataTable("select max(id) + 1 maxid from ipm_error", Me.ConnectionStringIDAM).Rows(0)("maxid")
                    If ErrorID Is System.DBNull.Value Then
                        ErrorID = 1
                    End If
                Catch ex2 As Exception
                    ErrorID = 1
                End Try
                Try
                    Dim sql As String
                    sql = "insert into ipm_error (id,date,output,object_type,object_id) values (" & ErrorID & ",getdate(),'" & ex.Message.Replace("'", "''") & "','ProjectNightlyError','44')"
                    ExecuteTransaction(sql)
                Catch ex3 As Exception
                End Try

            End Try
        Next i
    End Sub



    Function checkorCreateProjectfolder(ByVal parentcatid As String, ByVal sName As String, ByVal sProjectID As String, Optional ByVal bypassSec As Boolean = False) As String

        'check is exists
        Dim sProjectFolderID As String
        Try
            sProjectFolderID = GetDataTable("select category_id from ipm_asset_category where projectid = " & sProjectID & " and parent_cat_id = " & parentcatid & " and name = '" & sName.Trim.Replace("'", "''") & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")
            Return sProjectFolderID
        Catch ex As Exception
            Try
                'create category
                Dim sql As String
                sql = "exec sp_createnewassetcategory_wparent 1," & parentcatid & ","
                sql += "'" & Replace(sName, "'", "''") & "',"
                sql += sProjectID & ","
                sql += "'Y',"
                sql += "'',"
                sql += "'1'"
                ExecuteTransaction(sql)
                'get ID of category just inserted
                Dim catID As String = GetDataTable("select max(category_id) maxid from ipm_asset_category", Me.ConnectionStringIDAM).Rows(0)("maxid")
                sProjectFolderID = catID
                'add security defaults
                If Not bypassSec Then
                    sql = "select * from ipm_project_security where projectid = " & sProjectID
                    Dim securityCatTable As DataTable = GetDataTable(sql, Me.ConnectionStringIDAM)
                    Dim srow As DataRow
                    For Each srow In securityCatTable.Rows
                        sql = "insert into ipm_category_security (category_id, security_id, type) values (" & catID & ",'" & srow("security_id") & "','" & srow("type") & "')"
                        ExecuteTransaction(sql)
                    Next
                End If

                Return sProjectFolderID
            Catch exx As Exception
                Return "0"
            End Try
        End Try


    End Function

    Function addProjectfolderSecurity(ByVal groupid As String, ByVal folderid As String)
        Dim sql As String
        Try
            sql = "insert into ipm_category_security (category_id, security_id, type) values (" & folderid & ",'" & groupid & "','G')"
            ExecuteTransaction(sql)
        Catch ex As Exception
            SendError(ex.Message, "addProjectfolderSecurity", folderid + "::" + groupid)
        End Try

    End Function



    Function SendError(ByVal sError, Optional ByVal object_type = "", Optional ByVal object_id = "")
        'id	float	Unchecked
        'date	datetime	Checked
        'error_level	varchar(50)	Checked
        'machine_id	varchar(50)	Checked
        'app_id	varchar(50)	Checked
        'instance_id	varchar(50)	Checked
        'class_name	varchar(255)	Checked
        'output	varchar(4000)	Checked
        'object_type	varchar(50)	Checkeds
        'object_id	varchar(50)	Checked
        Dim sql As String
        sError = sError.ToString.Replace("'", "''")
        Dim ErrorID As String = 1
        Try
            ErrorID = GetDataTable("select max(id) + 1 maxid from ipm_error", Me.ConnectionStringIDAM).Rows(0)("maxid")
            If ErrorID Is System.DBNull.Value Then
                ErrorID = 1
            End If
        Catch ex As Exception
            ErrorID = 1
        End Try
        Try
            sql = "insert into ipm_error (id,date,output,object_type,object_id) values (" & ErrorID & ",getdate(),'" & sError & "','" & object_type & "','" & object_id & "')"
            ExecuteTransaction(sql)
        Catch ex As Exception

        End Try

    End Function




    Function CreateNewAllianceCategory(ByVal categoryName As String, ByVal parent_id As String, ByVal securitylevel As String) As String
        Try
            Dim sql As String
            sql = "exec sp_createnewcategory 1,"
            sql += "'" & Replace(categoryName, "'", "''") & "',"
            sql += Replace(parent_id, "CAT", "") & ","
            sql += "'Y',"
            sql += "'',"
            sql += "'" & securitylevel & "'"
            ExecuteTransaction(sql)
            'get ID of category just inserted
            Dim catID As String = GetDataTable("select max(category_id) maxid from ipm_category", Me.ConnectionStringIDAM).Rows(0)("maxid")
            'add security defaults
            sql = "select * from ipm_category_security where category_id = " & Replace(parent_id, "CAT", "")
            Dim securityCatTable As DataTable = GetDataTable(sql, Me.ConnectionStringIDAM)
            Dim srow As DataRow
            For Each srow In securityCatTable.Rows
                sql = "insert into ipm_category_security (category_id, security_id, type) values (" & catID & ",'" & srow("security_id") & "','" & srow("type") & "')"
                ExecuteTransaction(sql)
            Next
            Return catID
        Catch ex As Exception
            Return "0"
        End Try
    End Function

    Private Function CreateNewProject(ByVal sProjectName As String, ByVal sCatID As String, ByVal sProjectNumber As String) As String
        'here we go...
        'check to see if external reference
        'MITHUN_PROJECT_MAPPING()
        Dim sProjectID As String = ""
        Try




            Dim sql As String
            'Dim sProjectNumber As String = ProjectItem("oid")
            'Dim sProjectName As String = ProjectItem("name")
            ExecuteTransaction("exec sp_createnewproject2 " & sCatID & ",1,'" & Replace(sProjectName, "'", "''") & "','Imported from automated procedure.','1'")
            ''get ID of category just inserted
            sProjectID = GetDataTable("select max(projectid) maxid from ipm_project", Me.ConnectionStringIDAM).Rows(0)("maxid")
            'add security defaults
            sql = "select * from ipm_category_security where category_id = " & sCatID
            Dim securityCatTable As DataTable = GetDataTable(sql, Me.ConnectionStringIDAM)
            Dim srow As DataRow
            For Each srow In securityCatTable.Rows
                sql = "insert into ipm_project_security (projectid, security_id, type) values (" & sProjectID & ",'" & srow("security_id") & "','" & srow("type") & "')"
                ExecuteTransaction(sql)
            Next
            Dim catID As String = sProjectID
            'import data
            'add additional project information (#,long name, project discipline = discipline, project alliance = services,  type = office
            'add project code to description


            Dim sImageFolderID As String = "0"
            Dim sMarketingMaterialsID As String = "0"
            Dim sCommunicationsID As String = "0"
            Dim sStudioAssetsID As String = "0"
            Dim sSupplementalImageryID As String = "0"
            Dim sConfidentialRightsRestricted As String = "0"
            Dim sConfidential As String = "0"
            Dim sConfidential2 As String = "0"
            Dim sMisc As String = "0"


            '21503534:       General(Staff)
            '21503535        Market Studio 28                                  
            '21503536:       Marketing(Manager)
            '21503539:       Photo(Administrator)
            '21503537:       Marketing(Coordinator)
            '21503538:       Communications(Team)

            'set project folders
            sImageFolderID = checkorCreateProjectfolder("0", "Marketing", sProjectID, True)



            Dim IDAM_VISION_PROJECT_DESCRIPTION, IDAM_PRIMARY_STUDIO, IDAM_PRIMARY_PROJECT_TYPE, IDAM_PRIMARY_SERVICE, IDAM_ACTUAL_COMPLETION_DATE, IDAM_CONSTRUCTION_START_DATE, IDAM_CONSTRUCTION_END_DATE, IDAM_COMPETITION, IDAM_COUNTRY, IDAM_GEOGRAPHIC_REGION, IDAM_AWARDS, IDAM_PUBLICATIONS, IDAM_BIM, IDAM_CONFIDENTIAL, IDAM_START_DATE, IDAM_OCCUPANCY_DATE As String


            'End If
            'get newly added project id
            Return sProjectID.ToString
        Catch ex As Exception
            If sProjectID <> "" Then
                Return sProjectID
            Else
                Return "0"
            End If

        End Try

    End Function






    Private Sub ExecuteNBBJSQLII(ByVal catID As String, ByVal sProjectNumber As String)
        Dim sql As String
        Dim sProjectID As String = catID
        Try


            'add keywords
            'If chkprojectinformation.Checked Then
            'get the keywords
            'add project discipline = discipline, project(alliance = keyword, Type = office)
            ''discipline
            Dim SourceKeywords As DataTable = GetDataTable("select custdesignservice from projectcustomtabfields where wbs1 = '" & sProjectNumber.Trim & "' and wbs2 = ''", ConnectionStringVISION)
            If SourceKeywords.Rows.Count > 0 Then
                Dim sDiscipline As String = Me.isullcheck(SourceKeywords.Rows(0)("custdesignservice"))
                If sDiscipline <> "" Then
                    'check for discpliine
                    Dim DTDiscipline As DataTable = GetDataTable("select keyid from ipm_discipline where keyuse = 1 and keyname = '" & sDiscipline.Trim().Replace("'", "''") & "'", ConnectionStringIDAM)
                    If DTDiscipline.Rows.Count > 0 Then
                        'discpline exists so simply reference

                        ExecuteTransaction("delete from ipm_project_discipline where projectid = " & catID & " and keyid = " & DTDiscipline.Rows(0)("Keyid"))
                        ExecuteTransaction("insert into ipm_project_discipline (keyid,projectid) values (" & DTDiscipline.Rows(0)("Keyid") & "," & catID & ")")
                    Else
                        'add to disipline table
                        sql = "exec sp_createnewkeywordDisciplineType "
                        sql += "1,"
                        sql += "'" + sDiscipline.ToString().Replace("'", "''") + "', "
                        sql += "1" + ", "
                        sql += "'Imported from external source'"
                        ExecuteTransaction(sql)
                        'get ID of repo just inserted
                        Dim NewKeywordID As String = GetDataTable("select max(keyid) maxid from ipm_discipline", ConnectionStringIDAM).Rows(0)("maxid")
                        'add external id
                        ExecuteTransaction("insert into ipm_project_discipline (keyid,projectid) values (" & NewKeywordID & "," & catID & ")")
                    End If
                End If
            End If


            'add project type
            Dim SourceType As DataTable = GetDataTable("select cfgprojecttype.* from pr,cfgprojecttype where pr.projecttype = cfgprojecttype.code and pr.wbs1 = '" & sProjectNumber & "' and pr.wbs2 = ''", ConnectionStringVISION)
            If SourceType.Rows.Count > 0 Then
                Dim sType As String = SourceType.Rows(0)("description")

                'check for discpliine
                Dim DTType As DataTable = GetDataTable("select keyid from ipm_office where keyuse = 1 and keyname = '" & sType.Trim().Replace("'", "''") & "'", ConnectionStringIDAM)
                If DTType.Rows.Count > 0 Then
                    'discpline exists so simply reference
                    ExecuteTransaction("delete from ipm_project_office where projectid = " & catID & " and officeid = " & DTType.Rows(0)("Keyid"))
                    ExecuteTransaction("insert into ipm_project_office (officeid,projectid) values (" & DTType.Rows(0)("Keyid") & "," & catID & ")")
                Else
                    'add to disipline table
                    sql = "exec sp_createnewkeywordOfficeType "
                    sql += "1,"
                    sql += "'" + sType.ToString().Replace("'", "''") + "', "
                    sql += "1" + ", "
                    sql += "'Imported from external source'"
                    ExecuteTransaction(sql)
                    'get ID of repo just inserted
                    Dim NewKeywordID As String = GetDataTable("select max(keyid) maxid from ipm_office", ConnectionStringIDAM).Rows(0)("maxid")
                    'add external id
                    ExecuteTransaction("insert into ipm_project_office (officeid,projectid) values (" & NewKeywordID & "," & catID & ")")
                End If
            End If

            'add additional project description data
            'add owners
            If 1 = 1 Then
                'add project type
                Dim SourceType2 As DataTable = GetDataTable("select principal,projmgr from pr where pr.wbs1 = '" & catID & "' and pr.wbs2 = ''", Me.ConnectionStringVISION)
                If SourceType2.Rows.Count > 0 Then
                    Dim sPrincipal As String = SourceType2.Rows(0)("principal")
                    Dim sProjManger As String = SourceType2.Rows(0)("projmgr")

                    'check 
                    If sPrincipal <> "" Then
                        'discpline exists so simply reference
                        ExecuteTransaction("update ipm_project set principalid = '" & sPrincipal & "' where projectid = " & catID)
                    End If
                    If sProjManger <> "" Then
                        'discpline exists so simply reference
                        ExecuteTransaction("update ipm_project set pmid = '" & sProjManger & "' where projectid = " & catID)
                    End If
                End If
            End If




        Catch ex As Exception

        End Try





        '21503540: IDAM_VISION_PROJECT_DESCRIPTION()
        '21503541: IDAM_PRIMARY_STUDIO()
        '21503542: IDAM_CLIENT_NAME()
        '21503543: IDAM_PRIMARY_PROJECT_TYPE() = market
        '21503544: IDAM_VISION_PROJECT_NAME()
        '21503545: IDAM_PRIMARY_SERVICE()
        '21503715: IDAM_ACTUAL_COMPLETION_DATE()
        '21503717: IDAM_CONSTRUCTION_START_DATE()
        '21503718: IDAM_CONSTRUCTION_END_DATE()
        '21503719: IDAM_COMPETITION()
        '21503720: IDAM_COUNTRY()
        '21503721: IDAM_GEOGRAPHIC_REGION()
        '21503722: IDAM_AWARDS()
        '21503723: IDAM_PUBLICATIONS()
        '21503724: IDAM_BIM()
        '21503546: IDAM_CONFIDENTIAL()
        '21503714: IDAM_START_DATE()
        '21503716: IDAM_OCCUPANCY_DATE()

        Dim DT_Temp As DataTable


        'IDAM_PRIMARY_STUDIO
        DT_Temp = GetDataTable("select org from pr where wbs1 = '" & sProjectNumber.Replace("'", "''") & "'", Me.ConnectionStringVISION)
        If DT_Temp.Rows.Count > 0 Then
            Try


                'erase any possibel previous value
                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503541")
                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503541,'" & (DT_Temp.Rows(0)("org")).Replace("'", "''") & "')")
            Catch ex As Exception

            End Try
        End If



        'IDAM_VISION_PROJECT_DESCRIPTION
        DT_Temp = GetDataTable("select description from prdescriptions where desccategory = 'PROJDESC' and wbs1 = '" & sProjectNumber.Replace("'", "''") & "'", Me.ConnectionStringVISION)
        If DT_Temp.Rows.Count > 0 Then
            Try


                'erase any possibel previous value
                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503540")
                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503540,'" & RemoveHtml(DT_Temp.Rows(0)("description")).Replace("'", "''") & "')")
            Catch ex As Exception

            End Try
        End If

        'IDAM_PRIMARY_PROJECT_TYPE
        DT_Temp = GetDataTable("select b.description from pr a, cfgprojecttype b where a.projecttype = b.code and a.wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringVISION)
        If DT_Temp.Rows.Count > 0 Then
            'erase any possibel previous value
            Try

                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503543")
                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503543,'" & RemoveHtml(DT_Temp.Rows(0)("description")).Replace("'", "''") & "')")
            Catch ex As Exception

            End Try
        End If


        'IDAM_CONSTRUCTION_START_DATE()
        DT_Temp = GetDataTable("select custConstructionStartDate from ProjectCustomTabFields where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringVISION)
        If DT_Temp.Rows.Count > 0 Then
            'erase any possibel previous value
            Try

                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503717")
                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503717,'" & DT_Temp.Rows(0)("custConstructionStartDate") & "')")
            Catch ex As Exception

            End Try
        End If


        'IDAM_ACTUAL_COMPLETION_DATE()
        DT_Temp = GetDataTable("select ActCompletionDate from pr where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringVISION)
        If DT_Temp.Rows.Count > 0 Then
            'erase any possibel previous value
            Try

                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503715")
                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503715,'" & DT_Temp.Rows(0)("ActCompletionDate") & "')")
            Catch ex As Exception

            End Try
        End If


        'IDAM_CONSTRUCTION_END_DATE()
        DT_Temp = GetDataTable("select ConstComplDate from pr where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringVISION)
        If DT_Temp.Rows.Count > 0 Then
            'erase any possibel previous value
            Try

                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503718")
                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503718,'" & DT_Temp.Rows(0)("ConstComplDate") & "')")
            Catch ex As Exception

            End Try
        End If

        'IDAM_COMPETITION()
        DT_Temp = GetDataTable("select custBuildingStatusCompetition from ProjectCustomTabFields where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringVISION)
        If DT_Temp.Rows.Count > 0 Then
            'erase any possibel previous value
            Try

                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503719")
                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503719,'" & Me.isullcheck(DT_Temp.Rows(0)("custBuildingStatusCompetition")).replace("'", "''") & "')")
            Catch ex As Exception

            End Try
        End If

        'IDAM_COUNTRY()
        DT_Temp = GetDataTable("select Country from pr where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringVISION)
        If DT_Temp.Rows.Count > 0 Then
            'erase any possibel previous value
            Try

                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503720")
                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503720,'" & Me.isullcheck(DT_Temp.Rows(0)("Country")).replace("'", "''") & "')")
            Catch ex As Exception

            End Try
        End If

        'IDAM_GEOGRAPHIC_REGION()
        DT_Temp = GetDataTable("select custlocationcode from ProjectCustomTabFields where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringVISION)
        If DT_Temp.Rows.Count > 0 Then
            'erase any possibel previous value
            Try

                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503721")
                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503721,'" & Me.isullcheck(DT_Temp.Rows(0)("custlocationcode")).replace("'", "''") & "')")
            Catch ex As Exception

            End Try
        End If

        '21503722: IDAM_AWARDS()
        Dim ii As Integer
        Dim sAwards As String = ""
        DT_Temp = GetDataTable("select description,institution,awarddate from prawards where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringVISION)
        If DT_Temp.Rows.Count > 0 Then
            Try

                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503722")
                For ii = 0 To DT_Temp.Rows.Count - 1
                    'erase any possibel previous value
                    sAwards += Me.isullcheck(DT_Temp.Rows(ii)("institution")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("awarddate")) & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("description")).replace("'", "''") & vbCrLf & vbCrLf
                Next
                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503722,'" & sAwards & "')")
            Catch ex As Exception

            End Try
        End If


        'select custpublication,custpublisher,custpubdate,custpubarticle, from projects_publications order by seq
        '21503723: IDAM_PUBLICATIONS()
        Dim sPublications As String = ""
        DT_Temp = GetDataTable("select custpublication,custpublisher,custpubdate,custpubarticle from projects_publications where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = '' order by seq", Me.ConnectionStringVISION)
        If DT_Temp.Rows.Count > 0 Then
            Try

                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503723")
                For ii = 0 To DT_Temp.Rows.Count - 1
                    'erase any possibel previous value
                    sPublications += Me.isullcheck(DT_Temp.Rows(ii)("custpubdate")) & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpublisher")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpublication")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpubarticle")).replace("'", "''") & vbCrLf & vbCrLf
                Next
                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503723,'" & sPublications & "')")
            Catch ex As Exception

            End Try
        End If

        '21503714: IDAM_START_DATE()
        DT_Temp = GetDataTable("select startdate from pr where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringVISION)
        If DT_Temp.Rows.Count > 0 Then
            Try

                'erase any possibel previous value
                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503714")
                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503714,'" & Me.isullcheck(DT_Temp.Rows(0)("startdate")) & "')")
            Catch ex As Exception

            End Try
        End If


        '21503716: IDAM_OCCUPANCY_DATE()
        DT_Temp = GetDataTable("select custOccupancyDate from ProjectCustomTabFields where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringVISION)
        If DT_Temp.Rows.Count > 0 Then
            Try

                'erase any possibel previous value
                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503716")
                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503716,'" & Me.isullcheck(DT_Temp.Rows(0)("custOccupancyDate")) & "')")
            Catch ex As Exception

            End Try
        End If











    End Sub



















    Private Sub ExecuteNBBJSQL(ByVal catID As String, ByVal sProjectNumber As String)
        Dim sql As String


        'add keywords
        'If chkprojectinformation.Checked Then
        'get the keywords
        'add project discipline = discipline, project(alliance = keyword, Type = office)
        ''discipline
        Dim SourceKeywords As DataTable = GetDataTable("select custdesignservice from projectcustomtabfields where wbs1 = '" & sProjectNumber.Trim & "' and wbs2 = ''", Me.ConnectionStringVISION)
        If SourceKeywords.Rows.Count > 0 Then
            Dim sDiscipline As String = Me.isullcheck(SourceKeywords.Rows(0)("custdesignservice"))
            If sDiscipline <> "" Then
                'check for discpliine
                Dim DTDiscipline As DataTable = GetDataTable("select keyid from ipm_discipline where keyuse = 1 and keyname = '" & sDiscipline.Trim().Replace("'", "''") & "'", Me.ConnectionStringIDAM)
                If DTDiscipline.Rows.Count > 0 Then
                    'discpline exists so simply reference
                    ExecuteTransaction("delete from ipm_project_discipline where projectid = " & catID & " and keyid = " & DTDiscipline.Rows(0)("Keyid"))
                    ExecuteTransaction("insert into ipm_project_discipline (keyid,projectid) values (" & DTDiscipline.Rows(0)("Keyid") & "," & catID & ")")
                Else
                    'add to disipline table
                    sql = "exec sp_createnewkeywordDisciplineType "
                    sql += "1,"
                    sql += "'" + sDiscipline.ToString().Replace("'", "''") + "', "
                    sql += "1" + ", "
                    sql += "'Imported from external source'"
                    ExecuteTransaction(sql)
                    'get ID of repo just inserted
                    Dim NewKeywordID As String = GetDataTable("select max(keyid) maxid from ipm_discipline", Me.ConnectionStringIDAM).Rows(0)("maxid")
                    'add external id
                    ExecuteTransaction("insert into ipm_project_discipline (keyid,projectid) values (" & NewKeywordID & "," & catID & ")")
                End If
            End If
        End If


        'add project type
        Dim SourceType As DataTable = GetDataTable("select cfgprojecttype.* from pr,cfgprojecttype where pr.projecttype = cfgprojecttype.code and pr.wbs1 = '" & sProjectNumber & "' and pr.wbs2 = ''", Me.ConnectionStringVISION)
        If SourceType.Rows.Count > 0 Then
            Dim sType As String = SourceType.Rows(0)("description")

            'check for discpliine
            Dim DTType As DataTable = GetDataTable("select keyid from ipm_office where keyuse = 1 and keyname = '" & sType.Trim().Replace("'", "''") & "'", Me.ConnectionStringIDAM)
            If DTType.Rows.Count > 0 Then
                'discpline exists so simply reference
                ExecuteTransaction("delete from ipm_project_office where projectid = " & catID & " and officeid = " & DTType.Rows(0)("Keyid"))
                ExecuteTransaction("insert into ipm_project_office (officeid,projectid) values (" & DTType.Rows(0)("Keyid") & "," & catID & ")")
            Else
                'add to disipline table
                sql = "exec sp_createnewkeywordOfficeType "
                sql += "1,"
                sql += "'" + sType.ToString().Replace("'", "''") + "', "
                sql += "1" + ", "
                sql += "'Imported from external source'"
                ExecuteTransaction(sql)
                'get ID of repo just inserted
                Dim NewKeywordID As String = GetDataTable("select max(keyid) maxid from ipm_office", Me.ConnectionStringIDAM).Rows(0)("maxid")
                'add external id
                ExecuteTransaction("insert into ipm_project_office (officeid,projectid) values (" & NewKeywordID & "," & catID & ")")
            End If
        End If

        'add additional project description data
        'add owners
        If 1 = 1 Then
            'add project type
            Dim SourceType2 As DataTable = GetDataTable("select principal,projmgr from pr where pr.wbs1 = '" & catID & "' and pr.wbs2 = ''", Me.ConnectionStringVISION)
            If SourceType2.Rows.Count > 0 Then
                Dim sPrincipal As String = SourceType2.Rows(0)("principal")
                Dim sProjManger As String = SourceType2.Rows(0)("projmgr")

                'check 
                If sPrincipal <> "" Then
                    'discpline exists so simply reference
                    ExecuteTransaction("update ipm_project set principalid = '" & sPrincipal & "' where projectid = " & catID)
                End If
                If sProjManger <> "" Then
                    'discpline exists so simply reference
                    ExecuteTransaction("update ipm_project set pmid = '" & sProjManger & "' where projectid = " & catID)
                End If
            End If
        End If




    End Sub





    Public Shared Sub ExecuteTransaction(ByVal sqlstring As String)
        Dim ConnectionString As String = System.Configuration.ConfigurationSettings.AppSettings("IDAM.ConnectionStringIDAM")
        Dim connection As New OleDbConnection(ConnectionString)
        Dim command As New OleDbCommand
        Dim transaction As OleDbTransaction

        ' Set the Connection to the new OleDbConnection.
        command.Connection = connection

        ' Open the connection and execute the transaction.
        Try
            connection.Open()

            ' Start a local transaction with ReadCommitted isolation level.
            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            ' Assign transaction object for a pending local transaction.
            command.Connection = connection
            command.Transaction = transaction

            ' Execute the commands.
            command.CommandText = sqlstring
            command.ExecuteNonQuery()

            ' Commit the transaction.
            transaction.Commit()
            Console.WriteLine("Executed.")

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Try to rollback the transaction
            Try
                transaction.Rollback()

            Catch
                ' Do nothing here; transaction is not active.
            End Try
        End Try
        ' The connection is automatically closed when the
        ' code exits the Using block.

    End Sub

    Public Shared Function GetDataTable(ByVal query As String, ByVal ConnectionString As String) As DataTable
        'Dim connection1 As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & System.Web.HttpContext.Current.Server.MapPath(dbFile))
        Dim connection1 As OleDbConnection = New OleDbConnection(ConnectionString)
        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = New OleDbCommand(query, connection1)
        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()

        End Try
        Return table1
    End Function
    Function isullcheck(ByVal v)
        If v Is System.DBNull.Value Then
            isullcheck = ""
        Else
            isullcheck = v
        End If
    End Function

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'get media_type
        'get extension

        'F:\PAHIRESTEMPLOCATION\HiRes
        'E:\IDAM\Repositories\IDAM_USHMM
        Dim sSourceRoot As String = Me.PEROOT
        Dim sDestRoot As String = Me.IDAMROOT


        Dim Allfiles As DataTable = GetDataTable("select projectid from ipm_project where available = 'Y' ", ConnectionStringIDAM)
        Dim i As Integer
        Dim sSourceFile As String
        Dim sDestinationFile As String
        Dim sTifSwitch As Boolean = False

        For i = 0 To Allfiles.Rows.Count - 1
            Try
                Dim sasset_id As String = GetDataTable("select asset_id from ipm_asset where media_type = 10 and projectid = " & Allfiles.Rows(i)("projectid").ToString.Trim, ConnectionStringIDAM).Rows(0)("asset_id").ToString.Trim
                'get SourceFile
                sSourceFile = ""
                sDestinationFile = ""
                sSourceFile = sDestRoot + Allfiles.Rows(i)("projectid").ToString.Trim + "\resource\" & sasset_id & ".jpg"
                sDestinationFile = sDestRoot + Allfiles.Rows(i)("projectid").ToString.Trim + "\" + Allfiles.Rows(i)("projectid").ToString.Trim
                'is directories created?
                If Not IO.Directory.Exists(sDestRoot + Allfiles.Rows(i)("projectid").ToString.Trim) Then
                    IO.Directory.CreateDirectory(sDestRoot + Allfiles.Rows(i)("projectid").ToString.Trim)
                    IO.Directory.CreateDirectory(sDestRoot + Allfiles.Rows(i)("projectid").ToString.Trim + "\resource")
                    IO.Directory.CreateDirectory(sDestRoot + Allfiles.Rows(i)("projectid").ToString.Trim + "\acrobat")
                End If
                'now copy files
                'check to see if tiff exists .tif
                If Not IO.File.Exists(sDestinationFile & ".jpg") Then
                    IO.File.Copy(sSourceFile, sDestinationFile & "_lf.jpg", False)
                    IO.File.Copy(sSourceFile, sDestinationFile & "_lt.jpg", False)
                    IO.File.Copy(sSourceFile, sDestinationFile & ".jpg", False)
                End If


            Catch ex As Exception
                'check if tif override



                '' ''ListView1.Items.Add("Error: " & sSourceFile)
                '' ''Me.Refresh()
            End Try

            Me.Refresh()
        Next




    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'copy field values from cumulus to filelisting
        Me.ExecuteTransaction("delete from filelisting")
        Dim AllFiles As DataTable = GetDataTable("SELECT a.id,a.[asset identifier],a.[asset name],a.[file data size],a.[creation date],a.[modification date],a.[folder name],[cumulus project name],[project number],[cumulus market name],[vision market name],[vision long name]    FROM         CUMULUS a LEFT OUTER JOIN CUMULUS_MAPPING b ON SUBSTRING(replace([folder name],'MarketingMasters\IMAGES\',''),PATINDEX('%\%',replace([folder name],'MarketingMasters\IMAGES\',''))+1,100) = b.[cumulus project name]", ConnectionStringIDAM)
        Dim i As Integer
        Dim x As Integer = 0
        Dim sql As String
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try

                sql = "insert into filelisting (id,directory,files,filedate,assetid,processed,alliance,projectname,projectfolder,alliancefolder,oid) values " & _
                "('" & Me.isullcheck(AllFiles.Rows(i)("id")) & "','" & Me.isullcheck(AllFiles.Rows(i)("folder name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("asset name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("creation date")) & "','" & Me.isullcheck(AllFiles.Rows(i)("asset identifier")) & "',0,'" & Me.isullcheck(AllFiles.Rows(i)("vision market name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("vision long name")).replace("'", "''") & "','','" & Me.isullcheck(AllFiles.Rows(i)("vision market name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("project number")).replace("'", "''") & "')"
                Me.ExecuteTransaction(sql)

            Catch ex As Exception

                x += 1
            End Try
        Next

        sql = "update filelisting set projectname = SUBSTRING(replace([directory],'MarketingMasters\IMAGES\',''),PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES\',''))+1,100) where projectname = ''"
        Me.ExecuteTransaction(sql)

        sql = "update filelisting set alliance = replace(SUBSTRING(replace([directory],'MarketingMasters\IMAGES',''),PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES',''))+1,PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES\',''))),'\',''),alliancefolder = replace(SUBSTRING(replace([directory],'MarketingMasters\IMAGES',''),PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES',''))+1,PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES\',''))),'\','') where alliance = ''"
        Me.ExecuteTransaction(sql)

        sql = "update filelisting set alliance = replace(SUBSTRING(replace([directory],'MarketingMasters\IMAGES',''),PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES',''))+1,PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES\',''))),'\',''),alliancefolder = replace(SUBSTRING(replace([directory],'MarketingMasters\IMAGES',''),PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES',''))+1,PATINDEX('%\%',replace([directory],'MarketingMasters\IMAGES\',''))),'\','') where alliance = '<Null>'"
        Me.ExecuteTransaction(sql)

        sql = "update filelisting set alliance = 'Miscellaneous', alliancefolder = 'Miscellaneous' where alliance = ''"
        Me.ExecuteTransaction(sql)

        sql = "update filelisting set alliance = 'Miscellaneous', alliancefolder = 'Miscellaneous' where alliance in ('_L2 SMALL SCANSJUST','_BURNED_ReadySt','_BURNEDKwangmyon','_DROP','_DROPSamsan','_L2 SMALL SCANSJUST','_L2 SMALL SCANSSCIE','Dong San Medical Center, Keimyung University','Jakarta Mixed-Use Tower','Korean Animation')"
        Me.ExecuteTransaction(sql)

        '''''''''get all others not linked to vision project
        ''''''''AllFiles = GetDataTable("select a.id,a.[asset identifier],a.[asset name],a.[file data size],a.[creation date],a.[modification date],a.[folder name],SUBSTRING(replace([folder name],'MarketingMasters\IMAGES\',''),PATINDEX('%\%',replace([folder name],'MarketingMasters\IMAGES\',''))+1,100) [cumulus project name],''[project number],'Miscellaneous' [cumulus market name],'Miscellaneous' [vision market name],SUBSTRING(replace([folder name],'MarketingMasters\IMAGES\',''),PATINDEX('%\%',replace([folder name],'MarketingMasters\IMAGES\',''))+1,100) [vision long name]from cumulus a,(select id from cumulus where id not in (select a.id from cumulus a, cumulus_mapping b where SUBSTRING(replace([folder name],'MarketingMasters\IMAGES\',''),PATINDEX('%\%',replace([folder name],'MarketingMasters\IMAGES\',''))+1,100) = b.[cumulus project name])) c where a.id = c.id ", ConnectionStringIDAM)
        ''''''''ProgressBar1.Maximum = AllFiles.Rows.Count
        ''''''''ProgressBar1.Value = 0
        ''''''''For i = 0 To AllFiles.Rows.Count - 1
        ''''''''    Try

        ''''''''        sql = "insert into filelisting (id,directory,files,filedate,assetid,processed,alliance,projectname,projectfolder,alliancefolder,oid) values " & _
        ''''''''        "('" & Me.isullcheck(AllFiles.Rows(i)("id")) & "','" & Me.isullcheck(AllFiles.Rows(i)("folder name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("asset name")) & "','" & Me.isullcheck(AllFiles.Rows(i)("creation date")) & "','" & Me.isullcheck(AllFiles.Rows(i)("asset identifier")) & "',0,'" & Me.isullcheck(AllFiles.Rows(i)("vision market name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("vision long name")).replace("'", "''") & "','','" & Me.isullcheck(AllFiles.Rows(i)("vision market name")).replace("'", "''") & "','" & Me.isullcheck(AllFiles.Rows(i)("project number")).replace("'", "''") & "')"
        ''''''''        Me.ExecuteTransaction(sql)

        ''''''''    Catch ex As Exception

        ''''''''        x += 1
        ''''''''    End Try
        ''''''''Next






    End Sub


    Private Sub Button1_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Disposed

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'get media_type
        'get extension




        Dim Allfiles As DataTable = GetDataTable(" select b.projectid,c.category_id,pc#ProjectId pn,clientfilename,serverfilename,f#FileId fileid  from Project_images a, IPM_PROJECT b,IPM_ASSET_CATEGORY c where a.pc#ProjectId= b.ProjectNumber and c.PROJECTID = b.ProjectID and c.NAME = 'Marketing' ", ConnectionStringIDAM)
        Dim sExtensionid, sProjectID, sProjectfolderID As String
        Dim sExtensiontmp
        Dim sExtension As String = "JPG"

        'check to see if in filetype_lookup
        For i As Integer = 0 To Allfiles.Rows.Count
            'add asset
            Dim sAssetID As String
            'sAssetID = CreateAsset("0","1",sAssetName,"1",sProjectID

            Dim sAssetName As String = ""

            sAssetName = Allfiles.Rows(i)("clientfilename").ToString.Trim
            sProjectID = Allfiles.Rows(i)("projectid").ToString.Trim
            sProjectfolderID = Allfiles.Rows(i)("category_id").ToString.Trim
            sExtensionid = "10"
            'check to see if asset already exists and use update
            If GetDataTable("select asset_id from ipm_asset where asset_id = " & Allfiles.Rows(i)("fileid").ToString.Trim, ConnectionStringIDAM).Rows.Count = 0 Then
                Me.ExecuteTransaction("insert into ipm_asset(description,filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date,oid) values ('','" & Allfiles.Rows(i)("serverfilename") & "'," & sProjectID & ",'1'," & Allfiles.Rows(i)("fileid") & ",1,1,'" & sAssetName & "'," & sExtensionid & "," & sProjectID & "," & sProjectfolderID & ",getdate(),getdate()," & Allfiles.Rows(i)("fileid").ToString.Trim & ")")
            End If


            Try
                'add file to repository
                GetAssetHiRezFile()
                'send to queue
                Me.ExecuteTransaction("insert into ipm_asset_queue(instance_id,asset_id,asset_handler,status,priority,engine_id)values('IDAM_PE'," & Allfiles.Rows(i)("fileid").ToString.Trim & ",2,0,1,'48')")
            Catch ex As Exception
                Me.SendError(ex.Message, "FILE CATCH", "UNKNOWN")
            End Try

        Next


    End Sub



    Function GetAssetHiRezFile() As Boolean
        'F:\PAHIRESTEMPLOCATION\HiRes
        'E:\IDAM\Repositories\IDAM_USHMM
        Dim sSourceRoot As String = Me.PEROOT
        Dim sDestRoot As String = Me.IDAMROOT


        Dim AllFiles As DataTable = GetDataTable("select * from tmpfilemapping", ConnectionStringIDAM)
        Dim i As Integer
        Dim sSourceFile As String
        Dim sDestinationFile As String
        Dim sTifSwitch As Boolean = False
        '\\peapcny-s1f\S-Images\Marketing Images Library-High Resolution\100 West Putnam Avenue_29021\100 West Putnam Ave_29021_Ext Front Dusk_HR.tif
        For i = 0 To AllFiles.Rows.Count - 1
            Try

                'get SourceFile
                sSourceFile = ""
                sDestinationFile = ""
                sSourceFile = sSourceRoot & AllFiles.Rows(i)("asset_path").ToString.Trim.Replace("\\peapcny-s1f\S-Images\Marketing Images Library-High Resolution\", "")
                sDestinationFile = sDestRoot + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource\" + AllFiles.Rows(i)("asset_id").ToString.Trim + ".tif"
                ' MsgBox(sSourceFile & " " & sDestinationFile)

                'is directories created?
                If Not IO.Directory.Exists(sDestRoot + AllFiles.Rows(i)("projectid").ToString.Trim) Then
                    IO.Directory.CreateDirectory(sDestRoot + AllFiles.Rows(i)("projectid").ToString.Trim)
                    IO.Directory.CreateDirectory(sDestRoot + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource")
                    IO.Directory.CreateDirectory(sDestRoot + AllFiles.Rows(i)("projectid").ToString.Trim + "\acrobat")
                End If
                'now copy files
                'check to see if tiff exists .tif
                If Not IO.File.Exists(sDestinationFile) Then

                    IO.File.Copy(sSourceFile, sDestinationFile, False)

                End If
                ' MsgBox("completed first")
            Catch ex As Exception
                'check if tif override



                '' ''ListView1.Items.Add("Error: " & sSourceFile)
                '' ''Me.Refresh()
            End Try

            ' Me.Refresh()
        Next
        MsgBox("done")
    End Function



    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'add project udfs
        'get all projects with projectnums
        Dim DT_Temp As DataTable
        Dim AllFiles As DataTable = GetDataTable("select projectid, projectnumber from ipm_project where projectnumber <> '' and available = 'Y'", ConnectionStringIDAM)  'and oid = '041018.00'
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        Dim i As Integer


        '21503540: IDAM_VISION_PROJECT_DESCRIPTION()
        '21503541: IDAM_PRIMARY_STUDIO()
        '21503542: IDAM_CLIENT_NAME()
        '21503543: IDAM_PRIMARY_PROJECT_TYPE() = market
        '21503544: IDAM_VISION_PROJECT_NAME()
        '21503545: IDAM_PRIMARY_SERVICE()
        '21503715: IDAM_ACTUAL_COMPLETION_DATE()
        '21503717: IDAM_CONSTRUCTION_START_DATE()
        '21503718: IDAM_CONSTRUCTION_END_DATE()
        '21503719: IDAM_COMPETITION()
        '21503720: IDAM_COUNTRY()
        '21503721: IDAM_GEOGRAPHIC_REGION()
        '21503722: IDAM_AWARDS()
        '21503723: IDAM_PUBLICATIONS()
        '21503724: IDAM_BIM()
        '21503546: IDAM_CONFIDENTIAL()
        '21503714: IDAM_START_DATE()
        '21503716: IDAM_OCCUPANCY_DATE()


        For i = 0 To AllFiles.Rows.Count - 1
            'IDAM_VISION_PROJECT_DESCRIPTION
            DT_Temp = GetDataTable("select description from prdescriptions where desccategory = 'PROJDESC' and wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "'", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503540")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503540,'" & RemoveHtml(DT_Temp.Rows(0)("description")).Replace("'", "''") & "')")
            End If

            'IDAM_PRIMARY_PROJECT_TYPE
            DT_Temp = GetDataTable("select b.description from pr a, cfgprojecttype b where a.projecttype = b.code and a.wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503543")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503543,'" & RemoveHtml(DT_Temp.Rows(0)("description")).Replace("'", "''") & "')")
            End If


            'IDAM_CONSTRUCTION_START_DATE()
            DT_Temp = GetDataTable("select custConstructionStartDate from ProjectCustomTabFields where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503717")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503717,'" & DT_Temp.Rows(0)("custConstructionStartDate") & "')")
            End If


            'IDAM_ACTUAL_COMPLETION_DATE()
            DT_Temp = GetDataTable("select ActCompletionDate from pr where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503715")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503715,'" & DT_Temp.Rows(0)("ActCompletionDate") & "')")
            End If


            'IDAM_CONSTRUCTION_END_DATE()
            DT_Temp = GetDataTable("select ConstComplDate from pr where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503718")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503718,'" & DT_Temp.Rows(0)("ConstComplDate") & "')")
            End If

            'IDAM_COMPETITION()
            DT_Temp = GetDataTable("select custBuildingStatusCompetition from ProjectCustomTabFields where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503719")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503719,'" & Me.isullcheck(DT_Temp.Rows(0)("custBuildingStatusCompetition")).replace("'", "''") & "')")
            End If

            'IDAM_COUNTRY()
            DT_Temp = GetDataTable("select Country from pr where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503720")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503720,'" & Me.isullcheck(DT_Temp.Rows(0)("Country")).replace("'", "''") & "')")
            End If

            'IDAM_GEOGRAPHIC_REGION()
            DT_Temp = GetDataTable("select custlocationcode from ProjectCustomTabFields where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503721")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503721,'" & Me.isullcheck(DT_Temp.Rows(0)("custlocationcode")).replace("'", "''") & "')")
            End If

            '21503722: IDAM_AWARDS()
            Dim ii As Integer
            Dim sAwards As String = ""
            DT_Temp = GetDataTable("select description,institution,awarddate from prawards where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503722")
                For ii = 0 To DT_Temp.Rows.Count - 1
                    'erase any possibel previous value
                    sAwards += Me.isullcheck(DT_Temp.Rows(ii)("institution")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("awarddate")) & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("description")).replace("'", "''") & vbCrLf & vbCrLf
                Next
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503722,'" & sAwards & "')")
            End If


            'select custpublication,custpublisher,custpubdate,custpubarticle, from projects_publications order by seq
            '21503723: IDAM_PUBLICATIONS()
            Dim sPublications As String = ""
            DT_Temp = GetDataTable("select custpublication,custpublisher,custpubdate,custpubarticle from projects_publications where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = '' order by seq", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503723")
                For ii = 0 To DT_Temp.Rows.Count - 1
                    'erase any possibel previous value
                    sPublications += Me.isullcheck(DT_Temp.Rows(ii)("custpubdate")) & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpublisher")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpublication")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpubarticle")).replace("'", "''") & vbCrLf & vbCrLf
                Next
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503723,'" & sPublications & "')")
            End If

            '21503714: IDAM_START_DATE()
            DT_Temp = GetDataTable("select startdate from pr where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503714")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503714,'" & Me.isullcheck(DT_Temp.Rows(0)("startdate")) & "')")
            End If


            '21503716: IDAM_OCCUPANCY_DATE()
            DT_Temp = GetDataTable("select custOccupancyDate from ProjectCustomTabFields where wbs1 = '" & AllFiles.Rows(i)("projectnumber") & "' and wbs2 = ''", ConnectionStringVISION)
            If DT_Temp.Rows.Count > 0 Then
                'erase any possibel previous value
                Me.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & AllFiles.Rows(i)("projectid") & " and item_id = 21503716")
                Me.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & AllFiles.Rows(i)("projectid") & ",21503716,'" & Me.isullcheck(DT_Temp.Rows(0)("custOccupancyDate")) & "')")
            End If

        Next

    End Sub






    Public Function RemoveHtml(ByVal sTEXT As String) As String
        Dim sTemp As String
        sTemp = sTEXT.Replace("<br>", vbCr).Replace("&nbsp;", " ").Replace("</p>", vbCrLf)
        sTemp = System.Text.RegularExpressions.Regex.Replace(sTEXT, "<[^>]*>", "")

        Try
            Return sTemp.Trim
        Catch ex As Exception
            Return sTemp
        End Try

    End Function

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'add project plates
        ' types of imports
        '1 projectid and projectnumber = no prob
        '2 projectid no project num = no prob
        '3 projectnum and no projectid = new project in misc folder - import from vision
        '4 neither = new project in misc folder

        'update FILELISTINGPROJECTPLATES set projectname = replace(replace(replace(directory,'x:\project plates\',''),'\fonts',''),'\links','') 

        'linked set
        'select a.id,b.projectnumber,c.name from FILELISTINGPROJECTPLATES a, project_plate_mapping b, ipm_project c where replace(replace(replace(a.directory,'x:\project plates\',''),'\fonts',''),'\links','') = b.name and b.projectnumber is not null and c.projectnumber = b.projectnumber

        'all others
        'select
        'replace(replace(replace(directory,'x:\project plates\',''),'\fonts',''),'\links','') 
        'from FILELISTINGPROJECTPLATES where id not in (
        'select a.id from FILELISTINGPROJECTPLATES a, project_plate_mapping b, ipm_project c
        'where replace(replace(replace(a.directory,'x:\project plates\',''),'\fonts',''),'\links','') = b.name
        'and b.projectnumber is not null
        'and c.projectnumber = b.projectnumber)
        'group by replace(replace(replace(directory,'x:\project plates\',''),'\fonts',''),'\links','')

        'do not upload fonts

        'get category id to populate
        ' Dim catIDUploadLocation As String = GetDataTable("select category_id from ipm_category where name = 'Projects' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Dim AllFiles As DataTable = GetDataTable("select a.filedate,a.files,c.projectid,a.id,b.projectnumber,c.name from FILELISTINGPROJECTPLATES a, project_plate_mapping b, ipm_project c where replace(replace(replace(a.directory,'x:\project plates\',''),'\fonts',''),'\links','') = b.name and b.projectnumber is not null and c.projectnumber = b.projectnumber", ConnectionStringIDAM)  'and oid = '041018.00'
        Dim i As Integer
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                ProgressBar1.Value = ProgressBar1.Value + 1
                If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("id"), ConnectionStringIDAM).Rows.Count = 0 Or Me.chkUpdateOnly.Checked Then
                    Dim sAlliance As String = ""
                    Dim sAllianceFolder, sVisionProjectNumber As String
                    Dim sAllianceCatID, sProjectfolderID As String
                    Dim sDirectoryRoot As String = "X:\"
                    Dim sProjectID As String = AllFiles.Rows(i)("projectid")
                    ListView1.Items.Add(sProjectID + ": " + AllFiles.Rows(i)("files"))

                    ListView1.Refresh()
                    Me.Refresh()


                    'create alliance dir if needed
                    'does it exist already?
                    sProjectfolderID = ""
                    If GetDataTable("select category_id from ipm_asset_category where name = 'Marketing Materials' and projectid = " & sProjectID & " and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sProjectfolderID = GetDataTable("select category_id from ipm_asset_category where name = 'Marketing Materials' and projectid = " & sProjectID & " and available = 'Y'", ConnectionStringIDAM).Rows(0)("Category_id")
                    Else
                        sProjectfolderID = "0"
                    End If


                    'get media_type
                    'get extension
                    Dim sExtensionid As String
                    Dim sExtensiontmp() = AllFiles.Rows(i)("Files").split(".")
                    Dim sExtension As String
                    If sExtensiontmp.Length = 1 Then
                        sExtension = "UNK"
                    Else
                        sExtensiontmp.Reverse(sExtensiontmp)
                        sExtension = sExtensiontmp(0)
                    End If
                    'check to see if in filetype_lookup
                    imTypeid = imTypeid + 1
                    Try
                        sExtensionid = GetDataTable("select * from ipm_filetype_lookup where extension = '" & sExtension & "'", Me.ConnectionStringIDAM).Rows(0)("media_type")
                    Catch ex As Exception

                        'add extension
                        Me.ExecuteTransaction("insert into ipm_filetype_lookup (media_type,name,extension,useicon) values (" & imTypeid & ",'" & sExtension & " File" & "','" & sExtension & "',1)")
                        sExtensionid = imTypeid
                    End Try


                    'add asset
                    Dim sAssetID As String
                    'sAssetID = CreateAsset("0","1",sAssetName,"1",sProjectID

                    If sExtension.ToLower <> "ttf" And sExtension.ToLower <> "pfm" And sExtension.ToLower <> "pfb" Then
                        'check to see if asset already exists and use update
                        If chkUpdateOnly.Checked Then
                            'update location_id,projectid,category_id,
                            If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("id") & " and projectid = " & sProjectID & " and category_id = " & sProjectfolderID, ConnectionStringIDAM).Rows.Count = 0 Then
                                'assume asset is already there just not in the right spot
                                'Me.ExecuteTransaction("update ipm_asset set projectid = " & sProjectID & ",category_id = " & sProjectfolderID & " where asset_id = " & AllFiles.Rows(i)("id"))
                            End If
                        Else
                            Me.ExecuteTransaction("insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date) values ('" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sProjectID & ",'1'," & AllFiles.Rows(i)("id") & ",1,1,'" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sExtensionid & "," & sProjectID & "," & sProjectfolderID & ",getdate(),getdate())")
                        End If
                        'set processed flag to processed
                        Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 1 where id = " & AllFiles.Rows(i)("id"))
                    Else
                        'set processed flag to no import
                        Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 5 where id = " & AllFiles.Rows(i)("id"))
                    End If


                End If

            Catch ex As Exception
                Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 3 where id = " & AllFiles.Rows(i)("id"))
                Me.SendError(ex.Message, "MAIN CATCH", "UNKNOWN")
            End Try
        Next i
        Dim x As String = "what the"

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Dim sRootRepository As String = "D:\IDAM\repository\"
        Dim AllFiles As DataTable = GetDataTable("select b.asset_id, replace(a.directory,'MarketingMasters\','') directory, a.files, b.projectid, c.extension from FILELISTINGPROJECTPLATES a, ipm_asset b, ipm_filetype_lookup c where a.id = b.asset_id and b.media_type = c.media_type and a.processed = 1", ConnectionStringIDAM)
        Dim i As Integer
        Dim sExtensionid As String
        Dim sSourceFile As String
        Dim sDestinationFile As String
        Dim sTifSwitch As Boolean = False
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try

                'get SourceFile
                sSourceFile = ""
                sDestinationFile = ""
                sSourceFile = AllFiles.Rows(i)("Directory") + "\" + AllFiles.Rows(i)("Files")
                'sSourceFile = Replace(AllFiles.Rows(i)("Directory"), "X:\", "q:\") + "\" + AllFiles.Rows(i)("Files")
                sDestinationFile = sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource\" + AllFiles.Rows(i)("asset_id").ToString.Trim + "." + AllFiles.Rows(i)("extension").ToString.Trim
                'is directories created?
                ''''''''''If Not IO.Directory.Exists(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim) Then
                ''''''''''    IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim)
                ''''''''''    IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\resource")
                ''''''''''    IO.Directory.CreateDirectory(sRootRepository + AllFiles.Rows(i)("projectid").ToString.Trim + "\acrobat")
                ''''''''''End If
                'now copy files
                'check to see if tiff exists .tif
                If IO.File.Exists(sSourceFile) Then
                    'delete jpg
                    'IO.File.Delete(sDestinationFile)
                    IO.File.Copy(sSourceFile, sDestinationFile, False)
                    'change extension of asset
                Else
                    'already there...IO.File.Copy(sSourceFile, sDestinationFile, False)
                End If

                'now send to ipm_asset_queue and mark processed = 4

                Me.ExecuteTransaction("insert into ipm_asset_queue(instance_id,asset_id,asset_handler,status,priority,engine_id)values('IDAM_NBBJ'," & AllFiles.Rows(i)("asset_id").ToString.Trim & ",2,0,1,'20')")
                Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 12 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)

            Catch ex As Exception
                'MsgBox(ex.Message & AllFiles.Rows(i)("asset_id"))
                Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 66 where id = " & AllFiles.Rows(i)("asset_id").ToString.Trim)
            End Try
            ProgressBar1.Value = ProgressBar1.Value + 1
        Next
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click


        'does asset exist?
        'no - then normal routine for missing assets
        'yes

        'does alliance folder exist?
        'no then create
        'get id

        'does project exist in alliance?
        'no - then create in alliance
        'get id
        'yes...is it in proper alliance?
        'no - then remap

        'update asset with proper projectid


        'delete all


        'get category id to populate
        Dim catIDUploadLocation As String = GetDataTable("select category_id from ipm_category where name = 'Projects' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")

        'delete all categories
        Me.ExecuteTransaction("delete from ipm_category where parent_cat_id = " & catIDUploadLocation)

        'delete all projects and project folders
        Me.ExecuteTransaction("update ipm_project set available = 'N'")
        Me.ExecuteTransaction("update ipm_asset_category set available = 'N'")


        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Dim AllFiles As DataTable = GetDataTable("select * from filelisting where processed = 0 and files <> 'thumbs.db'", ConnectionStringIDAM)  'and oid = '041018.00'
        Dim i As Integer
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                ProgressBar1.Value = ProgressBar1.Value + 1
                If Me.chkUpdateOnly.Checked Then
                    Dim sAlliance As String = ""
                    Dim sAllianceFolder, sVisionProjectNumber As String
                    Dim sAllianceCatID, sProjectID As String
                    Dim sDirectoryRoot As String = "V:\"
                    Dim sProjectName As String = AllFiles.Rows(i)("projectname")
                    ListView1.Items.Add(sProjectName + ": " + AllFiles.Rows(i)("files"))

                    ListView1.Refresh()
                    Me.Refresh()

                    'get vision project number
                    sVisionProjectNumber = AllFiles.Rows(i)("oid")

                    'get Alliance
                    sAlliance = AllFiles.Rows(i)("alliance")

                    'create alliance dir if needed
                    'does it exist already?
                    sAllianceCatID = ""
                    If GetDataTable("select category_id from ipm_category where parent_cat_id = " & catIDUploadLocation & " and name = '" & sAlliance.Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sAllianceCatID = GetDataTable("select category_id from ipm_category where parent_cat_id = " & catIDUploadLocation & " and name = '" & sAlliance.Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("Category_id")
                    Else
                        sAllianceCatID = CreateNewAllianceCategory(sAlliance, catIDUploadLocation, "1")
                    End If


                    'create project if needed
                    sProjectID = ""
                    If GetDataTable("select projectid from ipm_project where category_id = " & sAllianceCatID & " and name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sProjectID = GetDataTable("select projectid from ipm_project where category_id = " & sAllianceCatID & " and name = '" & sProjectName.Replace("'", "''").Trim & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("projectid")
                    Else
                        sProjectID = CreateNewProject(sProjectName.Trim, sAllianceCatID, sVisionProjectNumber)
                    End If

                    'create project folders if needed
                    Dim sProjectFolderArray() As String
                    Dim sProjectfolderID As String = "0"
                    Try
                        If AllFiles.Rows(i)("folders").trim <> "" Then
                            sProjectFolderArray = AllFiles.Rows(i)("folders").split("\")
                            'assume value
                            Dim ii As Integer
                            For ii = 0 To sProjectFolderArray.Length - 1
                                If ii = 0 Then 'item at root

                                    sProjectfolderID = checkorCreateProjectfolder("0", sProjectFolderArray(ii), sProjectID)
                                Else
                                    If sProjectFolderArray(ii).Trim <> "" Then
                                        sProjectfolderID = checkorCreateProjectfolder(GetDataTable("select category_id from ipm_asset_category where projectid = " & sProjectID & " and name = '" & sProjectFolderArray(ii - 1).Trim.Replace("'", "''") & "' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id"), sProjectFolderArray(ii), sProjectID)
                                    Else
                                        'sProjectfolderID = "0"
                                    End If

                                End If
                            Next ii
                        Else
                            sProjectfolderID = "0"
                        End If
                    Catch ex As Exception
                        'put in general
                        sProjectfolderID = "0"
                    End Try



                    'project folder override for level
                    'get level
                    'select a.[approval status] from cumulus a, filelisting b where a.id = b.id and a.id = " &  AllFiles.Rows(i)("id")
                    Dim sLevel As String = "Level2"
                    Try
                        If GetDataTable("select a.[approval status] level from cumulus a, filelisting b where a.id = b.id and a.id = " & AllFiles.Rows(i)("id"), ConnectionStringIDAM).Rows(0)("level") = "L1" Then
                            sLevel = "Level1"
                        End If
                    Catch ex As Exception

                    End Try

                    'set folderid 
                    Try
                        Select Case sLevel
                            Case "Level1"
                                sProjectfolderID = GetDataTable("select category_id from ipm_asset_category where name = 'Images' and projectid = " & sProjectID, ConnectionStringIDAM).Rows(0)("category_id")
                            Case "Level2"
                                sProjectfolderID = GetDataTable("select category_id from ipm_asset_category where name = 'Supplemental Imagery' and projectid = " & sProjectID, ConnectionStringIDAM).Rows(0)("category_id")
                        End Select
                    Catch ex As Exception
                        sProjectfolderID = "0"
                    End Try



                    'get media_type
                    'get extension
                    Dim sExtensionid As String
                    Dim sExtensiontmp() = AllFiles.Rows(i)("Files").split(".")
                    Dim sExtension As String
                    If sExtensiontmp.Length = 1 Then
                        sExtension = "UNK"
                    Else
                        sExtensiontmp.Reverse(sExtensiontmp)
                        sExtension = sExtensiontmp(0)
                    End If
                    'check to see if in filetype_lookup
                    imTypeid = imTypeid + 1
                    Try
                        sExtensionid = GetDataTable("select * from ipm_filetype_lookup where extension = '" & sExtension & "'", Me.ConnectionStringIDAM).Rows(0)("media_type")
                    Catch ex As Exception

                        'add extension
                        Me.ExecuteTransaction("insert into ipm_filetype_lookup (media_type,name,extension,useicon) values (" & imTypeid & ",'" & sExtension & " File" & "','" & sExtension & "',1)")
                        sExtensionid = imTypeid
                    End Try


                    'add asset
                    Dim sAssetID As String
                    'sAssetID = CreateAsset("0","1",sAssetName,"1",sProjectID



                    'check to see if asset already exists and use update
                    If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("id"), ConnectionStringIDAM).Rows.Count = 1 Then
                        'update location_id,projectid,category_id,
                        If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("id") & " and projectid = " & sProjectID & " and category_id = " & sProjectfolderID, ConnectionStringIDAM).Rows.Count = 0 Then
                            'assume asset is already there just not in the right spot
                            Me.ExecuteTransaction("update ipm_asset set projectid = " & sProjectID & ",category_id = " & sProjectfolderID & " where asset_id = " & AllFiles.Rows(i)("id"))
                        End If
                    Else
                        Try
                            Me.ExecuteTransaction("insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date) values ('" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sProjectID & ",'1'," & AllFiles.Rows(i)("id") & ",1,1,'" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sExtensionid & "," & sProjectID & "," & sProjectfolderID & ",'" & AllFiles.Rows(i)("filedate") & "','" & AllFiles.Rows(i)("filedate") & "')")
                        Catch ex As Exception
                            ' MsgBox(ex.Message & ": " & "insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date) values ('" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sProjectID & ",'1'," & AllFiles.Rows(i)("id") & ",1,1,'" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sExtensionid & "," & sProjectID & "," & sProjectfolderID & ",'" & AllFiles.Rows(i)("filedate") & "','" & AllFiles.Rows(i)("filedate") & "')")
                        End Try
                    End If

                    'add keyword (Level)

                    Dim sDiscipline As String = sLevel
                    'check for discpliine
                    Dim DTDiscipline As DataTable = GetDataTable("select keyid from ipm_services where keyuse = 1 and keyname = '" & sDiscipline.Trim().Replace("'", "''") & "'", Me.ConnectionStringIDAM)
                    If DTDiscipline.Rows.Count > 0 Then
                        'discpline exists so simply reference
                        ExecuteTransaction("delete from ipm_asset_services where asset_id = " & AllFiles.Rows(i)("id") & " and keyid = " & DTDiscipline.Rows(0)("Keyid"))
                        ExecuteTransaction("insert into ipm_asset_services (keyid,asset_id) values (" & DTDiscipline.Rows(0)("Keyid") & "," & AllFiles.Rows(i)("id") & ")")
                    Else
                        'add to disipline table
                        Dim sql As String
                        sql = "exec sp_createnewkeywordServices "
                        sql += "1,"
                        sql += "'" + sDiscipline.ToString().Replace("'", "''") + "', "
                        sql += "1" + ", "
                        sql += "'Imported from external source'"
                        ExecuteTransaction(sql)
                        'get ID of repo just inserted
                        Dim NewKeywordID As String = GetDataTable("select max(keyid) maxid from ipm_services", Me.ConnectionStringIDAM).Rows(0)("maxid")
                        'add external id
                        ExecuteTransaction("insert into ipm_asset_services (keyid,projectid) values (" & NewKeywordID & "," & AllFiles.Rows(i)("id") & ")")
                    End If

                    'add user defined fields

                    Dim UDFSource As DataTable = GetDataTable("select * from cumulus where id = " & AllFiles.Rows(i)("id"), Me.ConnectionStringIDAM)
                    If UDFSource.Rows.Count > 0 Then
                        Dim IDAM_PHOTOGRAPHER_CREDIT, IDAM_PHOTOGRAPHER_CONTACT_INFO, IDAM_ASSET_RIGHTS, IDAM_ARCHIVE_CD, IDAM_CAPTION, IDAM_CUMULUS_CAPTION, IDAM_WORKING_CD As String
                        '21503637:               IDAM_PHOTOGRAPHER_CREDIT()
                        '21503638:               IDAM_PHOTOGRAPHER_CONTACT_INFO()
                        '21503639:               IDAM_ASSET_RIGHTS()
                        '21503640:               IDAM_ARCHIVE_CD()
                        '21503643:               IDAM_CAPTION()
                        '21503642:               IDAM_CUMULUS_CAPTION()
                        '21503641:               IDAM_WORKING_CD()

                        'assign photo credit
                        IDAM_PHOTOGRAPHER_CREDIT = isullcheck(UDFSource.Rows(0)("photographer(s)"))
                        If IDAM_PHOTOGRAPHER_CREDIT <> "" Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id= 21503637 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503637,'" & IDAM_PHOTOGRAPHER_CREDIT & "')")
                        End If

                        IDAM_ARCHIVE_CD = isullcheck(UDFSource.Rows(0)("Archive CD#"))
                        If IDAM_ARCHIVE_CD <> "" Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503640 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503640,'" & IDAM_ARCHIVE_CD & "')")
                        End If

                        IDAM_WORKING_CD = isullcheck(UDFSource.Rows(0)("Working CD#"))
                        If IDAM_WORKING_CD <> "" Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503641 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503641,'" & IDAM_WORKING_CD & "')")
                        End If

                        IDAM_CUMULUS_CAPTION = isullcheck(UDFSource.Rows(0)("Caption"))
                        If IDAM_CUMULUS_CAPTION <> "" And Not IDAM_CUMULUS_CAPTION.IndexOf("Adobe Photoshop 7.0") > 0 Then
                            'delete from table
                            ExecuteTransaction("delete from ipm_asset_field_value where item_id = 21503642 and asset_id = " & AllFiles.Rows(i)("id"))
                            'insert into table
                            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & AllFiles.Rows(i)("id") & ",21503642,'" & IDAM_CUMULUS_CAPTION & "')")
                        End If

                    End If


                    'add security defaults
                    Dim securityCatTable As DataTable
                    If sProjectfolderID = "0" Then
                        securityCatTable = GetDataTable("select * from ipm_project_security where projecid = " & sProjectID, Me.ConnectionStringIDAM)
                    Else
                        securityCatTable = GetDataTable("select * from ipm_category_security where category_id = " & sProjectfolderID, Me.ConnectionStringIDAM)
                    End If

                    Dim srow As DataRow
                    ExecuteTransaction("DELETE FROM ipm_ASSET_security WHERE ASSET_ID = " & AllFiles.Rows(i)("id"))
                    For Each srow In securityCatTable.Rows
                        ExecuteTransaction("insert into ipm_ASSET_security (ASSET_ID, security_id, type) values (" & AllFiles.Rows(i)("id") & ",'" & srow("security_id") & "','" & srow("type") & "')")
                    Next



                    'set processed flag
                    Me.ExecuteTransaction("update filelisting set processed = 1 where id = " & AllFiles.Rows(i)("id"))
                End If

            Catch ex As Exception
                Me.ExecuteTransaction("update filelisting set processed = 3 where id = " & AllFiles.Rows(i)("id"))
                Me.SendError(ex.Message, "MAIN CATCH", "UNKNOWN")
            End Try
        Next i
        Dim x As String = "what the"



    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        'add project plates
        ' types of imports
        '1 projectid and projectnumber = no prob
        '2 projectid no project num = no prob
        '3 projectnum and no projectid = new project in misc folder - import from vision
        '4 neither = new project in misc folder

        'update FILELISTINGPROJECTPLATES set projectname = replace(replace(replace(directory,'x:\project plates\',''),'\fonts',''),'\links','') 

        'linked set
        'select a.id,b.projectnumber,c.name from FILELISTINGPROJECTPLATES a, project_plate_mapping b, ipm_project c where replace(replace(replace(a.directory,'x:\project plates\',''),'\fonts',''),'\links','') = b.name and b.projectnumber is not null and c.projectnumber = b.projectnumber

        'all others
        'select
        'replace(replace(replace(directory,'x:\project plates\',''),'\fonts',''),'\links','') 
        'from FILELISTINGPROJECTPLATES where id not in (
        'select a.id from FILELISTINGPROJECTPLATES a, project_plate_mapping b, ipm_project c
        'where replace(replace(replace(a.directory,'x:\project plates\',''),'\fonts',''),'\links','') = b.name
        'and b.projectnumber is not null
        'and c.projectnumber = b.projectnumber)
        'group by replace(replace(replace(directory,'x:\project plates\',''),'\fonts',''),'\links','')

        'do not upload fonts
        Dim sqltmp As String = ""
        sqltmp = "update FILELISTINGPROJECTPLATES set processed = 0"
        Me.ExecuteTransaction(sqltmp)

        'get category id to populate
        ' Dim catIDUploadLocation As String = GetDataTable("select category_id from ipm_category where name = 'Projects' and available = 'Y'", ConnectionStringIDAM).Rows(0)("category_id")
        Dim imTypeid As Integer = 5000
        'process filelist (filter out thumbs.db)
        Dim AllFiles As DataTable = GetDataTable("select a.filedate,a.files,c.projectid,a.id,b.projectnumber,c.name from FILELISTINGPROJECTPLATES a, project_plate_mapping b, ipm_project c where replace(replace(replace(a.directory,'x:\project plates\',''),'\fonts',''),'\links','') = b.name and b.projectnumber is not null and c.projectnumber = b.projectnumber and c.available = 'Y'", ConnectionStringIDAM)  'and oid = '041018.00'
        Dim i As Integer
        ProgressBar1.Maximum = AllFiles.Rows.Count
        ProgressBar1.Value = 0
        For i = 0 To AllFiles.Rows.Count - 1
            Try
                ProgressBar1.Value = ProgressBar1.Value + 1
                If Me.chkUpdateOnly.Checked Then
                    Dim sAlliance As String = ""
                    Dim sAllianceFolder, sVisionProjectNumber As String
                    Dim sAllianceCatID, sProjectfolderID As String
                    Dim sDirectoryRoot As String = "X:\"
                    Dim sProjectID As String = AllFiles.Rows(i)("projectid")
                    ListView1.Items.Add(sProjectID + ": " + AllFiles.Rows(i)("files"))

                    ListView1.Refresh()
                    Me.Refresh()


                    'create alliance dir if needed
                    'does it exist already?
                    sProjectfolderID = ""
                    If GetDataTable("select category_id from ipm_asset_category where name = 'Marketing Materials' and projectid = " & sProjectID & " and available = 'Y'", ConnectionStringIDAM).Rows.Count > 0 Then
                        'get cat id
                        sProjectfolderID = GetDataTable("select category_id from ipm_asset_category where name = 'Marketing Materials' and projectid = " & sProjectID & " and available = 'Y'", ConnectionStringIDAM).Rows(0)("Category_id")
                    Else
                        sProjectfolderID = "0"
                    End If


                    'get media_type
                    'get extension
                    Dim sExtensionid As String
                    Dim sExtensiontmp() = AllFiles.Rows(i)("Files").split(".")
                    Dim sExtension As String
                    If sExtensiontmp.Length = 1 Then
                        sExtension = "UNK"
                    Else
                        sExtensiontmp.Reverse(sExtensiontmp)
                        sExtension = sExtensiontmp(0)
                    End If
                    'check to see if in filetype_lookup
                    imTypeid = imTypeid + 1
                    Try
                        sExtensionid = GetDataTable("select * from ipm_filetype_lookup where extension = '" & sExtension & "'", Me.ConnectionStringIDAM).Rows(0)("media_type")
                    Catch ex As Exception

                        'add extension
                        Me.ExecuteTransaction("insert into ipm_filetype_lookup (media_type,name,extension,useicon) values (" & imTypeid & ",'" & sExtension & " File" & "','" & sExtension & "',1)")
                        sExtensionid = imTypeid
                    End Try


                    'add asset
                    Dim sAssetID As String
                    'sAssetID = CreateAsset("0","1",sAssetName,"1",sProjectID

                    If sExtension.ToLower <> "ttf" And sExtension.ToLower <> "pfm" And sExtension.ToLower <> "pfb" Then
                        'check to see if asset already exists and use update
                        If chkUpdateOnly.Checked Then
                            'update location_id,projectid,category_id,
                            If GetDataTable("select asset_id from ipm_asset where asset_id = " & AllFiles.Rows(i)("id") & " and projectid = " & sProjectID & " and category_id = " & sProjectfolderID, ConnectionStringIDAM).Rows.Count = 0 Then
                                'assume asset is already there just not in the right spot
                                Me.ExecuteTransaction("update ipm_asset set projectid = " & sProjectID & ",category_id = " & sProjectfolderID & " where asset_id = " & AllFiles.Rows(i)("id"))
                            End If
                        Else
                            'Me.ExecuteTransaction("insert into ipm_asset(filename,location_id,userid,asset_id, repository_id, securitylevel_id,name,media_type,projectid,category_id,upload_date,update_date) values ('" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sProjectID & ",'1'," & AllFiles.Rows(i)("id") & ",1,1,'" & Replace(AllFiles.Rows(i)("Files"), "'", "''") & "'," & sExtensionid & "," & sProjectID & "," & sProjectfolderID & ",getdate(),getdate())")
                        End If
                        'set processed flag to processed
                        Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 1 where id = " & AllFiles.Rows(i)("id"))
                    Else
                        'set processed flag to no import
                        Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 5 where id = " & AllFiles.Rows(i)("id"))
                    End If


                End If

            Catch ex As Exception
                Me.ExecuteTransaction("update FILELISTINGPROJECTPLATES set processed = 3 where id = " & AllFiles.Rows(i)("id"))
                Me.SendError(ex.Message, "MAIN CATCH", "UNKNOWN")
            End Try
        Next i
        Dim x As String = "what the"
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button10_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If ConfigurationSettings.AppSettings("IDAM.EXECUTENIGHTLY") = "1" Then
            log = log4net.LogManager.GetLogger("PE_NIGHTLY_LOG")
            log4net.Config.XmlConfigurator.Configure()
            VisionNightly()
            log.Debug("Completed")
            Me.Close()
        End If
    End Sub


    Private Function addudfsandindex()
        'handle differently - use select into!!!!!!!!!!!!!!!!!!!!!!!
        'SELECT record_id,a.Field_ID,StringValue,name FROM customdata a,fielddef b where a.field_id = b.field_id


        'add udf fields
        Me.ExecuteTransaction("insert into ipm_asset_field_desc select Field_ID item_id, 'IDAM_PE_' + name item_tag,'' item_description, '' item_default_value, 10 item_type, 'General' item_group, 'IDAM_EXTENSIS' item_master, 1 active, 1 viewable, 0 editable, Name item_name,0 system, 'Extensis Legacy Data' item_tab from [PE_EXTENSIS].[dbo].fielddef")

        'add udf values
        Me.ExecuteTransaction("insert into ipm_asset_field_value select record_id asset_id, Field_ID item_id, StringValue item_value from [PE_EXTENSIS].[dbo].customdata ")

        ''add tags
        'Me.ExecuteTransaction("insert into ipm_tag select Record_ID tagid,keyword tagname,''tagdescription,1 taguse,1 createdby,GETDATE() createddate from [PE_EXTENSIS].[dbo].keyword")

        ''add asset tag ref
        'Me.ExecuteTransaction("insert into ipm_asset_tag select Item_ID asset_id, Keyword_ID tagid, 1 assignedby, 0 category_id, '' source from [PE_EXTENSIS].[dbo].item_keyword"

        'update IPM_ASSET set Version_ID = 0 where Version_ID is null - loose ends
        Me.ExecuteTransaction("update IPM_ASSET set Version_ID = 0 where Version_ID is null")
        'populate index
        Me.ExecuteTransaction(GetFileContents(System.AppDomain.CurrentDomain.BaseDirectory() + "update_sql.txt"))

        ' ''Dim AllUDFs As DataTable = GetDataTable("SELECT record_id,a.Field_ID,StringValue,name FROM customdata a,fielddef b where a.field_id = b.field_id", ConnectionStringEXTENSIS)
        ' ''For Each row As DataRow In AllUDFs.Rows
        ' ''    Dim assetid As String = row("record_id").ToString
        ' ''    addUDFtoasset(assetid, row("name").ToString, row("StringValue").ToString)
        ' ''Next

        ' ''Dim AllTags As DataTable = GetDataTable("SELECT c.keyword, a.record_id FROM item_table a, item_keyword b, keyword c where a.record_id = b.item_id and b.keyword_id = c.record_id", ConnectionStringEXTENSIS)
        ' ''For Each row As DataRow In AllTags.Rows
        ' ''    Dim assetid As String = row("record_id").ToString
        ' ''    addtagtoasset(assetid, row("keyword").ToString)
        ' ''Next

    End Function

    Private Sub Button10_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        addudfsandindex()

    End Sub

    Private Function addkeyword(ByVal keyword As String, ByVal description As String, ByVal keywordtype As String) As String
        'check to see if exists
        If GetDataTable("select * from " & keywordtype & " where keyname = '" & keyword.Replace("'", "''") & "'", ConnectionStringIDAM).Rows.Count = 0 Then
            Dim maxid As String
            Try
                maxid = (GetDataTable("select max(keyid) maxid from " & keywordtype, ConnectionStringIDAM).Rows(0)("maxid") + 1).ToString
            Catch ex As Exception
                maxid = 1
            End Try

            'add
            ExecuteTransaction("insert into " & keywordtype & " (keyid,keyname,keydescription,keyuse) values (" & maxid & ",'" & keyword & "','" & description.Replace("'", "''") & "',1)")
            Return maxid
        End If
    End Function

    Private Function addkeywordtoasset(ByVal asset_id As String, ByVal keyword As String, ByVal keywordtype As String) As String
        'check to see if exists
        Dim keyid As String = ""
        Try

            Dim dttag As DataTable = GetDataTable("select keyid from " & keywordtype & " where keyname = '" & keyword.Replace("'", "''") & "'", ConnectionStringIDAM)
            If dttag.Rows.Count = 0 Then
                keyid = addkeyword(keyword, "imported from script", keywordtype)
            Else
                keyid = dttag.Rows(0)("keyid")
            End If
            'add
            'assume we have a tagid now
            If GetDataTable("select * from ipm_asset_" & keywordtype.Replace("ipm_", "") & " where asset_id = " & asset_id & " and keyid = " & keyid, ConnectionStringIDAM).Rows.Count = 0 Then
                ExecuteTransaction("insert into ipm_asset_" & keywordtype.Replace("ipm_", "") & " (asset_id,keyid) values (" & asset_id & "," & keyid & ")")
            End If

        Catch ex As Exception

        End Try
        Return keyid
    End Function




    Private Function addkeywordtoproject(ByVal projectid As String, ByVal keyword As String, ByVal keywordtype As String) As String
        'check to see if exists
        Dim keyid As String = ""
        Try

            Dim dttag As DataTable = GetDataTable("select keyid from " & keywordtype & " where keyname = '" & keyword.Replace("'", "''") & "'", ConnectionStringIDAM)
            If dttag.Rows.Count = 0 Then
                keyid = addkeyword(keyword, "imported from script", keywordtype)
            Else
                keyid = dttag.Rows(0)("keyid")
            End If
            'add
            'assume we have a tagid now
            'trap for office
            Dim skeyid As String = ""
            If keywordtype = "ipm_office" Then
                skeyid = "officeid"
            Else
                skeyid = "keyid"
            End If
            If GetDataTable("select * from ipm_project_" & keywordtype.Replace("ipm_", "") & " where projectid = " & projectid & " and " & skeyid & " = " & keyid, ConnectionStringIDAM).Rows.Count = 0 Then
                ExecuteTransaction("insert into ipm_project_" & keywordtype.Replace("ipm_", "") & " (projectid," & skeyid & ") values (" & projectid & "," & keyid & ")")
            End If

        Catch ex As Exception

        End Try
        Return keyid
    End Function




    Private Function addtag(ByVal tag As String, ByVal description As String) As String
        'check to see if exists
        If GetDataTable("select * from ipm_tag where tagname = '" & tag.Replace("'", "''") & "'", ConnectionStringIDAM).Rows.Count = 0 Then
            Dim maxid As String = (GetDataTable("select max(tagid) maxid from ipm_tag", ConnectionStringIDAM).Rows(0)("maxid") + 1).ToString
            'add
            ExecuteTransaction("insert into ipm_tag (tagid,tagname,tagdescription,taguse,createdby,createddate) values (" & maxid & ",'" & tag & "','" & description.Replace("'", "''") & "',1,1,getdate())")
            Return maxid
        End If
    End Function

    Private Function addtagtoasset(ByVal asset_id As String, ByVal tag As String) As String
        'check to see if exists
        Dim tagid As String = ""
        Try

            Dim dttag As DataTable = GetDataTable("select tagid from ipm_tag where tagname = '" & tag.Replace("'", "''") & "'", ConnectionStringIDAM)
            If dttag.Rows.Count = 0 Then
                tagid = addtag(tag, "")
            Else
                tagid = dttag.Rows(0)("tagid")
            End If
            'add
            'assume we have a tagid now
            If GetDataTable("select * from ipm_asset_tag where asset_id = " & asset_id & " and tagid = " & tagid, ConnectionStringIDAM).Rows.Count = 0 Then
                ExecuteTransaction("insert into ipm_asset_tag (asset_id,tagid,assignedby,category_id) values (" & asset_id & "," & tagid & ",'1',0)")
            End If

        Catch ex As Exception

        End Try
        Return tagid
    End Function

    Private Function addUDF(ByVal item_description As String, ByVal item_group As String, ByVal item_name As String, ByVal item_tab As String) As String
        'check to see if exists
        If GetDataTable("select * from ipm_asset_field_desc where item_name = '" & item_name.Replace("'", "''") & "'", ConnectionStringIDAM).Rows.Count = 0 Then
            Dim maxid As String = (GetDataTable("select max(item_id) maxid from ipm_asset_field_desc", ConnectionStringIDAM).Rows(0)("maxid") + 1).ToString
            'add
            ExecuteTransaction("insert into ipm_asset_field_desc (item_id,item_name,item_description,item_group,item_tab,item_tag,item_type,item_master,active,viewable,editable,system) values (" & maxid & ",'" & item_name & "','" & item_description.Replace("'", "''") & "','" & item_group.Replace("'", "''") & "','" & item_tab.Replace("'", "''") & "','IDAM_EX_" & item_name.Replace("'", "''") & "',10,'IDAM_EX_" & item_name.Replace("'", "''") & "',1,1,1,0)")
            Return maxid
        End If
    End Function

    Private Function addUDFtoasset(ByVal asset_id As String, ByVal UDFName As String, ByVal UDFValue As String) As String
        'check to see if exists
        Dim UDFid As String = ""
        Try

            Dim dtUDF As DataTable = GetDataTable("select item_id from ipm_asset_field_desc where item_name = '" & UDFName.Replace("'", "''") & "'", ConnectionStringIDAM)
            If dtUDF.Rows.Count = 0 Then
                UDFid = addUDF("", "General", UDFName, "Extensis Metadata")
            Else
                UDFid = dtUDF.Rows(0)("item_id")
            End If
            'add
            'assume we have a tagid now
            If GetDataTable("select * from ipm_asset_field_value where asset_id = " & asset_id & " and item_id = " & UDFid, ConnectionStringIDAM).Rows.Count = 0 Then
                ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) values (" & asset_id & "," & UDFid & ",'" & UDFValue.Replace("'", "''") & "')")
            End If

        Catch ex As Exception

        End Try
        Return UDFid
    End Function


    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        'get files to migrate
        'select a.asset_id,a.name,d.name projectname from IPM_ASSET a,ipm_asset_field_value b, ipm_asset_field_desc c, ipm_project d where a.asset_id = b.asset_id and c.item_id = b.Item_ID and c.Item_Tag = 'IDAM_FILE_SYSTEM_PUSH_REPLICATION' and b.item_value = 1 and a.proc_status = 0 and a.Available = 'Y' and a.ProjectID = d.projectid
        Application.DoEvents()
        Dim IDAM_FILE_SYSTEM_NETWORK_LOCATION_ID As String = GetDataTable("select item_id from ipm_asset_field_desc where item_tag = 'IDAM_FILE_SYSTEM_NETWORK_LOCATION'", ConnectionStringIDAM).Rows(0)("item_id")
        Dim IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID As String = GetDataTable("select item_id from ipm_asset_field_desc where item_tag = 'IDAM_FILE_SYSTEM_INFORMATION_STATUS'", ConnectionStringIDAM).Rows(0)("item_id")
        Dim IDAM_FILE_SYSTEM_PUSH_REPLICATION_ID As String = GetDataTable("select item_id from ipm_asset_field_desc where item_tag = 'IDAM_FILE_SYSTEM_PUSH_REPLICATION'", ConnectionStringIDAM).Rows(0)("item_id")

        Dim currentasset_id As String = ""
        Dim currentfilepath As String = ""
        Do While 1 = 1
            Try
                currentasset_id = ""
                currentfilepath = ""

                'preprocess
                'IDAM_FILE_SYSTEM_NETWORK_LOCATION()
                'IDAM_FILE_SYSTEM_INFORMATION_STATUS()
                Dim AllAssets As DataTable = GetDataTable("select a.asset_id,a.filename,a.name,d.name projectname from IPM_ASSET a,ipm_asset_field_value b, ipm_asset_field_desc c, ipm_project d where a.asset_id = b.asset_id and c.item_id = b.Item_ID and c.Item_Tag = 'IDAM_FILE_SYSTEM_PUSH_REPLICATION' and b.item_value = 1 and a.proc_status = 0 and a.Available = 'Y' and a.ProjectID = d.projectid", ConnectionStringIDAM)  'and oid = '041018.00'

                For Each row As DataRow In AllAssets.Rows
                    currentasset_id = row("asset_id")
                    Dim filename As String = SafeOSName(row("filename"))
                    Dim filenamedestination As String = Regex.Replace(filename, ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONFILENAMEREPLACESTRING"), ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONFILENAMEREPLACEWITHSTRING"))
                    Dim filepath As String = ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONLOCATION") & SafeOSName(row("projectname")) & "\" & filenamedestination
                    'use legacy path?
                    If GetDataTable("select item_value from IPM_ASSET_FIELD_VALUE a, IPM_ASSET_FIELD_DESC b where a.Item_ID = b.Item_ID and b.Item_Tag = 'IDAM_FILE_SYSTEM_OVERRIDE_PATH' and a.ASSET_ID = '" & row("asset_id") & "'", ConnectionStringIDAM).Rows.Count > 0 Then
                        Dim item_value = GetDataTable("select item_value from IPM_ASSET_FIELD_VALUE a, IPM_ASSET_FIELD_DESC b where a.Item_ID = b.Item_ID and b.Item_Tag = 'IDAM_FILE_SYSTEM_OVERRIDE_PATH' and a.ASSET_ID = '" & row("asset_id") & "'", ConnectionStringIDAM).Rows(0)("item_value").ToString.Trim
                        If item_value <> "" Then
                            filepath = ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONLOCATION") & item_value.Replace(ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONPATHSCRUB"), "")
                            filepath = Regex.Replace(filepath, ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONFILENAMEREPLACESTRING"), ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONFILENAMEREPLACEWITHSTRING"))

                        End If
                    End If
                    'change extension
                    filepath = Regex.Replace(filepath, Path.GetExtension(filepath), ".jpg")
                    currentfilepath = filepath
                    'check to see if dir exists
                    If Not Directory.Exists(Path.GetDirectoryName(filepath)) Then
                        Directory.CreateDirectory(Path.GetDirectoryName(filepath))
                    End If
                    'check for file
                    If Not File.Exists(filepath) Then
                        'execute copy
                        'get paging if needed
                        Dim sPage As String = ""
                        sPage = getPaging(row("asset_id"))
                        getPreviewImageFromIDAM(row("asset_id"), ConfigurationSettings.AppSettings("IDAM.IDAMINSTANCE"), ConfigurationSettings.AppSettings("IDAM.IDAMBROWSEURL"), filepath, "")
                    End If
                    'confirm file got there
                    'CODE0 (Not Replicated)
                    'CODE1 (Replicated)
                    'CODE2 (ERROR)

                    Dim savefilepath As String = filepath
                    'override filepath for unc
                    If ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONLOCATIONUNCOVERRIDE") <> "" Then
                        savefilepath = filepath.Replace(ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONLOCATION"), ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONLOCATIONUNCOVERRIDE"))
                    End If

                    If File.Exists(filepath) Then
                        'update status and network location - SUCCESS
                        ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_NETWORK_LOCATION_ID & " and asset_id = " & row("asset_id"))
                        ExecuteTransaction("insert into ipm_asset_field_value (item_id,asset_id,item_value) values (" & IDAM_FILE_SYSTEM_NETWORK_LOCATION_ID & "," & row("asset_id") & ",'" & savefilepath.Replace("'", "''") & "')")
                        ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID & " and asset_id = " & row("asset_id"))
                        ExecuteTransaction("insert into ipm_asset_field_value (item_id,asset_id,item_value) values (" & IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID & "," & row("asset_id") & ",'CODE1 (Replicated)')")
                    Else
                        'update status and network location - FAILURE
                        ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_NETWORK_LOCATION_ID & " and asset_id = " & row("asset_id"))
                        ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID & " and asset_id = " & row("asset_id"))
                        ExecuteTransaction("insert into ipm_asset_field_value (item_id,asset_id,item_value) values (" & IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID & "," & row("asset_id") & ",'CODE2 (ERROR)')")
                    End If
                    'change checkbox so doesnt process again
                    ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_PUSH_REPLICATION_ID & " and asset_id = " & row("asset_id"))
                Next
            Catch ex As Exception
                SendError(ex.Message, "ASSET_REPLICATION")
                'change checkbox so doesnt process again
                ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_PUSH_REPLICATION_ID & " and asset_id = " & currentasset_id)
                'update status and network location - FAILURE
                ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_NETWORK_LOCATION_ID & " and asset_id = " & currentasset_id)
                ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID & " and asset_id = " & currentasset_id)
                ExecuteTransaction("insert into ipm_asset_field_value (item_id,asset_id,item_value) values (" & IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID & "," & currentasset_id & ",'CODE2 (ERROR)')")

            End Try
            System.Threading.Thread.Sleep(10000)
        Loop
        ''        filename = Path.GetFileName(Path)
        ''        extension = Path.GetExtension(Path)
        ''        fullPath = Path.GetFullPath(Path)
        ''        directoryName = Path.GetDirectoryName(Path)
        ''        FilenameWithoutExt = Path.GetFileNameWithoutExtension(Path
    End Sub

    Public Function getPaging(ByVal id) As String
        Try

            If GetDataTable("select convertertype from ipm_filetype_lookup where media_type in (select top 1 media_type from ipm_asset where asset_id = " & id & ")", ConnectionStringIDAM).Rows(0)("convertertype") = "2" Then
                Return "0"
            Else
                Return ""
            End If
        Catch ex As Exception
            Return ""
        End Try

    End Function

    Public Function SafeOSName(ByVal nom As String) As String

        'First Trim the raw string
        Dim safe As String = nom.Trim

        'Replace spaces with hyphens
        'safe = safe.Replace(" ", "-").ToLower()

        '' ''Replace double spaces with singles
        ' ''If safe.IndexOf("--") > 1 Then
        ' ''    While (safe.IndexOf("--") > -1)
        ' ''        safe = safe.Replace("--", "-")
        ' ''    End While
        ' ''End If

        'Trim out illegal characters
        '"\"M\"\\a/ry/ h**ad:>> a\\/:*?\"| li*tt|le|| la\"mb.?"
        safe = Regex.Replace(safe, "[\/:*?""<>|]", "")

        'Trim Length
        If safe.Length > 250 Then
            safe = safe.Substring(0, 249)
        End If

        ''Clean the beginning and end of filename
        'Dim replace As Char() = {"-", "."}
        'safe = safe.TrimStart(replace)
        'safe = safe.TrimEnd(replace)

        Return safe

    End Function



    Public Shared Function ProcessImage(ByRef filename As String, ByVal SavePath As String) As Stream
        Dim codecs As New RasterCodecs
        codecs.CodecsPath = CODEC_PATH
        Dim rasterImage As IRasterImage = codecs.Load(filename)
        rasterImage.XResolution = 200
        rasterImage.YResolution = 200
        codecs.Options.Jpeg.Save.QualityFactor = 2
        'Dim resultStream As New System.IO.MemoryStream
        codecs.Save(rasterImage, SavePath, RasterImageFormat.Jpeg, 24)
    End Function





    Function getPreviewImageFromIDAM(ByVal AssetId, ByVal instanceid, ByVal browseURL, ByVal cachepath, ByVal page) As String
        If page = "" Then
            page = "0"
        End If
        'RetrieveAsset.aspx?id=30056115&instance=IDAM_WEBARCHIVES&type=asset&size=1&cache=1
        Dim downloadURL As String = browseURL & "?id=" & AssetId & "&instance=" & instanceid & "&type=asset&size=1&width=4000&height=4000&cache=1"
        Dim cachepathname As String = cachepath
        SaveUrlToPath(cachepathname, downloadURL)
    End Function





    Sub CopyStream(ByRef source As Stream, ByRef target As Stream)
        Dim buffer As Byte() = New Byte(65535) {}
        If source.CanSeek Then
            source.Seek(0, SeekOrigin.Begin)
        End If
        Dim bytes As Integer = source.Read(buffer, 0, buffer.Length)
        Try
            While (bytes > 0)

                target.Write(buffer, 0, bytes)
                bytes = source.Read(buffer, 0, buffer.Length)
            End While
        Finally
            ' Or target.Close(); if you're done here already.
            target.Flush()
        End Try
    End Sub



    Sub SaveUrlToPath(ByVal sPath As String, ByVal sURL As String)

        Dim wc As Net.WebClient
        Dim stream As System.IO.Stream
        Try
            Dim t As Date = Now
            wc = New Net.WebClient
            'ping first to prevent a major bug on downloadfile call. If there's a 404, sPath will be deleted
            stream = wc.OpenRead(sURL)

            'save file to cache then process
            '' ''If Not IO.Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory() & "cache") Then
            '' ''    IO.Directory.CreateDirectory(System.AppDomain.CurrentDomain.BaseDirectory())
            '' ''End If
            Dim cachePath As String = System.AppDomain.CurrentDomain.BaseDirectory() & Guid.NewGuid.ToString & ".idam"
            SaveStreamToFilePath(cachePath, stream)
            ProcessImage(cachePath, sPath)

            IO.File.Delete(cachePath)
            'Dim tempPath As String = sPath
            'SaveStreamToFilePath(tempPath, stream)

        Catch ex As Exception

        Finally
            If Not wc Is Nothing Then
                If Not stream Is Nothing Then
                    stream.Close()
                End If
                wc.Dispose()
            End If
        End Try
    End Sub


    Sub SaveStreamToFilePath(ByVal sFilePath As String, ByRef istream As Stream)
        Dim outputStream As New FileStream(sFilePath, FileMode.Create)

        Try
            CopyStream(istream, outputStream)
        Finally
            outputStream.Close()
            istream.Close()
        End Try
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        Dim IDAM_FILE_SYSTEM_NETWORK_LOCATION_ID As String = GetDataTable("select item_id from ipm_asset_field_desc where item_tag = 'IDAM_FILE_SYSTEM_NETWORK_LOCATION'", ConnectionStringIDAM).Rows(0)("item_id")
        Dim IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID As String = GetDataTable("select item_id from ipm_asset_field_desc where item_tag = 'IDAM_FILE_SYSTEM_INFORMATION_STATUS'", ConnectionStringIDAM).Rows(0)("item_id")
        Dim IDAM_FILE_SYSTEM_PUSH_REPLICATION_ID As String = GetDataTable("select item_id from ipm_asset_field_desc where item_tag = 'IDAM_FILE_SYSTEM_PUSH_REPLICATION'", ConnectionStringIDAM).Rows(0)("item_id")
        Dim IDAM_FILE_SYSTEM_OVERRIDE_PATH_ID As String = GetDataTable("select item_id from ipm_asset_field_desc where item_tag = 'IDAM_FILE_SYSTEM_OVERRIDE_PATH'", ConnectionStringIDAM).Rows(0)("item_id")
        Dim AllAssets As DataTable = GetDataTable(GetFileContents(System.AppDomain.CurrentDomain.BaseDirectory() + "sqlassetselect.txt"), ConnectionStringEXTENSIS)  'and oid = '041018.00'
        For Each row As DataRow In AllAssets.Rows
            'populate UDFS
            ''IDAM_FILE_SYSTEM_NETWORK_LOCATION()






















            ''IDAM_FILE_SYSTEM_OVERRIDE_PATH()
            ''IDAM_FILE_SYSTEM_PUSH_REPLICATION()h

            Dim filepath As String = row("asset_path")
            filepath = filepath.Replace("\\peapcny-s1f\S-Images\Marketing Images Library-High Resolution", "M:\Images Library-Medium Resolution")
            filepath = Regex.Replace(filepath, ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONFILENAMEREPLACESTRING"), ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONFILENAMEREPLACEWITHSTRING"))
            filepath = Regex.Replace(filepath, Path.GetExtension(filepath), ".jpg")

            ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_OVERRIDE_PATH_ID & " and asset_id = " & row("record_id"))
            ExecuteTransaction("insert into ipm_asset_field_value (item_id,asset_id,item_value) values (" & IDAM_FILE_SYSTEM_OVERRIDE_PATH_ID & "," & row("record_id") & ",'" & row("asset_path") & "')")
            ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_NETWORK_LOCATION_ID & " and asset_id = " & row("record_id"))
            ExecuteTransaction("insert into ipm_asset_field_value (item_id,asset_id,item_value) values (" & IDAM_FILE_SYSTEM_NETWORK_LOCATION_ID & "," & row("record_id") & ",'" & filepath & "')")
            ExecuteTransaction("delete from ipm_asset_field_value where item_id = " & IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID & " and asset_id = " & row("record_id"))
            ExecuteTransaction("insert into ipm_asset_field_value (item_id,asset_id,item_value) values (" & IDAM_FILE_SYSTEM_INFORMATION_STATUS_ID & "," & row("record_id") & ",'CODE1 (Replicated)')")


        Next
    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        GetAssetHiRezFile()

    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        Me.ExecuteTransaction("insert into ipm_asset_queue(instance_id,asset_id,asset_handler,status,priority,engine_id)values('IDAM_PE'," & "11304" & ",2,0,1,'31')")
    End Sub

    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click


        'services = Interiors
        Dim AllTags As DataTable = GetDataTable("select record_id item_id,stringvalue keyword from customdata where stringvalue in ('Atrium','Auditorium','Bar','Bathroom','Bedroom - Patient Room','Bedroom - Resident Room','Bedroom - Residential','Breakout','Cafe','Cafeteria','Chapel/Synagogue','Classroom','Computer Lab','Conference Room','Corridor','Courtroom','Custom Furniture','Detail','Dining','Elevator Lobby','Entry','Exam Room','Fireplace','Fitness Center','Gallery Space','Gym','Kitchen','Laboratory','Library','Living Room','Lobby','Locker Room','Lounge','Nurses Station','Office','Operating Room','Pantry','Pediatrics','People','Pharmacy','Play Area','Pool','Procedure Room','Reception','Recovery','Registration Desk','Retail','Salon/Spa','Servery','Signage/Displays','Staircase','Sunroom/Porch','Trading Floor','Treatment Room','Waiting Area','Workstation')", ConnectionStringEXTENSIS)
        For Each row As DataRow In AllTags.Rows
            Dim assetid As String = row("item_id").ToString
            addkeywordtoasset(assetid, row("keyword").ToString, "ipm_services")
        Next

        'mediatype = Exteriors
        AllTags = GetDataTable("select record_id item_id,stringvalue keyword from customdata where stringvalue in ('Aerial','Athletic Field/Playground','Balcony','Bridge','Canopy/Sunshade','Context','Cottage','Courtyard/Plaza','Detail','Detail - Graphic','Dusk','Entrance Drive','Entry','People','Pool','Porch/Patio','Porte Cochere','Wandering Garden')", ConnectionStringEXTENSIS)
        For Each row As DataRow In AllTags.Rows
            Dim assetid As String = row("item_id").ToString
            addkeywordtoasset(assetid, row("keyword").ToString, "ipm_mediatype")
        Next

        'illusttype = Image Type
        AllTags = GetDataTable("select record_id item_id,stringvalue keyword from customdata where stringvalue in ('Diagram','Drawing: Detail','Drawing: Elevation','Drawing: Plan','Drawing: Section','Drawing: Site Plan','Model','Photograph: Before','Photograph: Final','Rendering')", ConnectionStringEXTENSIS)
        For Each row As DataRow In AllTags.Rows
            Dim assetid As String = row("item_id").ToString
            addkeywordtoasset(assetid, row("keyword").ToString, "ipm_illusttype")
        Next
    End Sub


    Public Sub VisionNightly()


        'select all projects in IDAM affected by nightly
        Dim AllProjects As DataTable = GetDataTable("select * From ipm_project where available = 'Y' and projectnumber like '%.%'", ConnectionStringIDAM)
        'Dim AllProjects As DataTable = GetDataTable("select * From ipm_project where available = 'Y' and projectnumber like '13720.00.0'", ConnectionStringIDAM)

        Me.ProgressBar1.Maximum = AllProjects.Rows.Count
        For Each project As DataRow In AllProjects.Rows
            Dim DT_Temp As DataTable
            Dim sProjectID As String = project("projectid")
            Dim sProjectNumber As String = project("projectnumber")

            Dim dtIDAM_PR As New DataTable
            dtIDAM_PR = GetDataTable("select * from idam_pr where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", ConnectionStringVISION)

            If dtIDAM_PR.Rows.Count > 0 Then






                Try

                    Dim saddress1 As String = isullcheck(dtIDAM_PR.Rows(0)("address1"))
                    Dim saddress2 As String = isullcheck(dtIDAM_PR.Rows(0)("address2"))
                    Dim saddress3 As String = isullcheck(dtIDAM_PR.Rows(0)("address3"))
                    Dim scity As String = isullcheck(dtIDAM_PR.Rows(0)("city"))
                    Dim sstate As String = isullcheck(dtIDAM_PR.Rows(0)("state"))
                    Dim szip As String = isullcheck(dtIDAM_PR.Rows(0)("zip"))

                    'ExecuteTransaction("update ipm_project set description = '" & sProjectDescription.Trim.Replace("'", "''") & "' where projectid = " & catID)


                    ExecuteTransaction("update ipm_project set address = '" & saddress1.Trim.Replace("'", "''") & vbCrLf & saddress2.Trim.Replace("'", "''") & vbCrLf & saddress3.Trim.Replace("'", "''") & "' where projectid = " & sProjectID)
                    ExecuteTransaction("update ipm_project set city = '" & scity.Trim.Replace("'", "''") & "' where projectid = " & sProjectID)
                    ExecuteTransaction("update ipm_project set state_id = '" & sstate.Trim.Replace("'", "''") & "' where projectid = " & sProjectID)
                    ExecuteTransaction("update ipm_project set zip = '" & szip.Trim.Replace("'", "''") & "' where projectid = " & sProjectID)




                Catch ex As Exception

                End Try








                Dim sClientID As String = isullcheck(dtIDAM_PR.Rows(0)("clientid")).ToString.Trim

                'add client info
                If sClientID <> "" Then
                    Dim SourceClient As DataTable = GetDataTable("select b.*,c.* from iDAM_PR a, CL b, claddress c where a.ClientID = b.ClientID and b.ClientID = c.clientid and c.PrimaryInd = 'Y' and a.clientid = '" & sClientID.Replace("'", "''").Trim() & "' and wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", ConnectionStringVISION)
                    'Dim SourceClient As DataTable = GetDataTable("select * from iDAM_PR a, CL b, claddress c where a.ClientID = b.ClientID and b.ClientID = c.clientid and c.PrimaryInd = 'Y' and a.clientid = '" & sClientID.Replace("'", "''").Trim() & "'", ConnectionStringVISION)


                    'select b.*,c.* from iDAM_PR a, CL b, claddress c where a.ClientID = b.ClientID and b.ClientID = c.clientid and c.PrimaryInd = 'Y' and a.clientid = '" & sClientID.Replace("'", "''").Trim() & "' and wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''

                    If SourceClient.Rows.Count > 0 Then
                        Dim sClientName As String = SourceClient.Rows(0)("name")
                        Dim sClientIDExt As String = SourceClient.Rows(0)("clientid")
                        'check to see if client exists
                        Dim isClientExist As Integer = GetDataTable("select clientid from ipm_client where name = '" & sClientName.Replace("'", "''").Trim() & "'", ConnectionStringIDAM).Rows.Count
                        If isClientExist > 0 Then
                            'client exists so simply reference
                            ExecuteTransaction("update ipm_project set clientname = '" & sClientName.Replace("'", "''").Trim & "' where projectid = " & sProjectID)
                        Else
                            'add to client table
                            ExecuteTransaction("exec sp_createnewclient " & _
                            " 1, '" & Replace(sClientName, "'", "''") & "', " & _
                            "'Imported from external project source', " & _
                            "'" & Replace(isullcheck(SourceClient.Rows(0)("address1")), "'", "''") & "', " & _
                            "'" & Replace(isullcheck(SourceClient.Rows(0)("address2")), "'", "''") & "', " & _
                            "'" & Replace(isullcheck(SourceClient.Rows(0)("address3")), "'", "''") & "', " & _
                            "'" & Replace(isullcheck(SourceClient.Rows(0)("address4")), "'", "''") & "', " & _
                            "'" & Replace(isullcheck(SourceClient.Rows(0)("email")), "'", "''") & "', " & _
                            "'" & Replace(isullcheck(SourceClient.Rows(0)("phone")), "'", "''") & "', " & _
                            "'" & Replace(isullcheck(SourceClient.Rows(0)("specialty")), "'", "''") & "', " & _
                            "'" & Replace(isullcheck(SourceClient.Rows(0)("city")), "'", "''") & "', " & _
                            "'', " & _
                            "'" & Replace(isullcheck(SourceClient.Rows(0)("zip")), "'", "''") & "', " & _
                            "'" & Replace(isullcheck(SourceClient.Rows(0)("fax")), "'", "''") & "', " & _
                            "'" & Replace(isullcheck(SourceClient.Rows(0)("moduser")), "'", "''") & "' ")
                            'add reference
                            ExecuteTransaction("update ipm_project set clientname = '" & Replace(sClientName, "'", "''").Trim & "' where projectid = " & sProjectID)
                            'get ID of repo just inserted
                            Dim NewClientID As String = GetDataTable("select top 1 clientid maxid from ipm_client order by update_date desc", ConnectionStringIDAM).Rows(0)("maxid")
                            'add external id
                            ExecuteTransaction("update ipm_client set externalclientid = '" & Replace(sClientIDExt, "'", "''").Trim & "' where clientid = " & NewClientID)
                        End If
                    End If ' end client addition
                End If



                'practice area (keyword - discipline)
                Dim DTPA As DataTable = GetDataTable("select b.description from idam_pr a, CFGProjectTypeDescriptions b where  a.ProjectType = b.Code and wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", ConnectionStringVISION)
                Dim sPracticeAreaAll As String = ""
                For Each keyword As DataRow In DTPA.Rows
                    'add keyword
                    addkeywordtoproject(sProjectID, keyword("description").ToString, "ipm_discipline")
                    sPracticeAreaAll += keyword("description").ToString.Replace("'", "''") & vbCrLf
                Next
                'Practice Area
                Dim sIDAM_V_PRACTICEAREA As String = CheckAddProjectUDF("sp_createnewprojectfield", "Practice Area", "Vision Data", "IDAM_V_PRACTICEAREA", "", 1, "General", 1, 1)
                If sPracticeAreaAll <> "" Then
                    Try
                        'erase any possibel previous value
                        ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_PRACTICEAREA)
                        ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_PRACTICEAREA & ",'" & sPracticeAreaAll & "')")
                    Catch ex As Exception

                    End Try
                End If



                'Country (keyword - office)
                If isullcheck(dtIDAM_PR.Rows(0)("country")) <> "" Then
                    Try
                        Dim countrylookup As String
                        countrylookup = GetDataTable("select name from ipm_country where country_id = '" & dtIDAM_PR.Rows(0)("country").trim & "'", ConnectionStringIDAM).Rows(0)("name").ToString.Trim
                        addkeywordtoproject(sProjectID, countrylookup, "ipm_office")
                        'IDAM_V_PROJECT_COUNTRY
                        Dim sIDAM_V_COUNTRY As String = CheckAddProjectUDF("sp_createnewprojectfield", "Country", "Vision Data", "IDAM_V_COUNTRY", "", 1, "General", 1, 1)
                        Try
                            'erase any possibel previous value
                            ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_COUNTRY)
                            ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_COUNTRY & ",'" & countrylookup.Replace("'", "''") & "')")
                        Catch ex As Exception

                        End Try
                    Catch ex As Exception
                        addkeywordtoproject(sProjectID, dtIDAM_PR.Rows(0)("country").trim, "ipm_office")
                    End Try
                End If



                'Firm and PE Office
                Dim sIDAM_V_PEOFFICE As String = CheckAddProjectUDF("sp_createnewprojectfield", "Perkins Eatman Office", "Vision Data", "IDAM_V_OFFICE", "", 1, "General", 1, 1)
                Dim sIDAM_V_PESTUDIO As String = CheckAddProjectUDF("sp_createnewprojectfield", "Perkins Eatman Studio", "Vision Data", "IDAM_V_STUDIO", "", 1, "General", 1, 1)
                Dim sIDAM_V_FIRM As String = CheckAddProjectUDF("sp_createnewprojectfield", "Firm", "Vision Data", "IDAM_V_FIRM", "", 1, "General", 1, 1)
                If isullcheck(dtIDAM_PR.Rows(0)("org")) <> "" Then
                    Try

                        Dim AllIDs As String() = dtIDAM_PR.Rows(0)("org").split(":")
                        If AllIDs.Length > 0 Then
                            'add firm
                            'get firm value
                            Dim dtFirm As DataTable = GetDataTable("select label From  dbo.CFGOrgCodesDescriptions where Code = '" & AllIDs(0) & "'", ConnectionStringVISION)
                            If dtFirm.Rows.Count > 0 Then
                                'erase any possibel previous value
                                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_FIRM)
                                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_FIRM & ",'" & RemoveHtml(dtFirm.Rows(0)("label").ToString).Replace("'", "''") & "')")
                            End If
                        End If

                        If AllIDs.Length > 1 Then
                            'add firm
                            'get firm value
                            Dim dtOffice As DataTable = GetDataTable("select label From  dbo.CFGOrgCodesDescriptions where Code = '" & AllIDs(1) & "'", ConnectionStringVISION)
                            If dtOffice.Rows.Count > 0 Then
                                'erase any possibel previous value
                                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_PEOFFICE)
                                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_PEOFFICE & ",'" & RemoveHtml(dtOffice.Rows(0)("label").ToString).Replace("'", "''") & "')")
                            End If
                        End If

                        If AllIDs.Length > 2 Then
                            'add firm
                            'get firm value
                            Dim dtStudio As DataTable = GetDataTable("select label From  dbo.CFGOrgCodesDescriptions where Code = '" & AllIDs(2) & "'", ConnectionStringVISION)
                            If dtStudio.Rows.Count > 0 Then
                                'erase any possibel previous value
                                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_PESTUDIO)
                                ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_PESTUDIO & ",'" & RemoveHtml(dtStudio.Rows(0)("label").ToString).Replace("'", "''") & "')")
                            End If
                        End If


                    Catch ex As Exception

                    End Try
                End If




                'Common Name
                Dim sIDAM_V_COMMONNAME As String = CheckAddProjectUDF("sp_createnewprojectfield", "Common Name", "Vision Data", "IDAM_V_COMMONNAME", "", 1, "General", 1, 1)
                If isullcheck(dtIDAM_PR.Rows(0)("longname")) <> "" Then
                    Try
                        'erase any possibel previous value
                        ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_COMMONNAME)
                        ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_COMMONNAME & ",'" & RemoveHtml(dtIDAM_PR.Rows(0)("longname").ToString).Replace("'", "''") & "')")
                    Catch ex As Exception

                    End Try
                End If


                'Description Publish
                Dim sIDAM_V_DESCRIPTION_PUBLISH As String = CheckAddProjectUDF("sp_createnewprojectfield", "Project Name: Publish", "Vision Data", "IDAM_V_DESCRIPTION_PUBLISH", "", 1, "General", 1, 0)
                If isullcheck(dtIDAM_PR.Rows(0)("longname")) <> "" Then
                    Try
                        'erase any possibel previous value
                        ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_DESCRIPTION_PUBLISH)
                        ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_DESCRIPTION_PUBLISH & ",'" & RemoveHtml(dtIDAM_PR.Rows(0)("longname").ToString).Replace("'", "''") & "')")
                    Catch ex As Exception

                    End Try
                End If




                ''Common Name IDAM_V_DESCRIPTION_PUBLISH
                'Dim sIDAM_V_DESCRIPTION_PUBLISH As String = CheckAddProjectUDF("sp_createnewprojectfield", "Project Name: Publish", "Vision Data", "IDAM_V_DESCRIPTION_PUBLISH", "", 1, "General", 1, 1)
                'If isullcheck(dtIDAM_PR.Rows(0)("description")) <> "" Then
                '    Try
                '        'erase any possibel previous value
                '        ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_DESCRIPTION_PUBLISH)
                '        ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_DESCRIPTION_PUBLISH & ",'" & RemoveHtml(project("description").ToString).Replace("'", "''") & "')")
                '    Catch ex As Exception

                '    End Try
                'End If



                'State Province (Keyword - keyword)
                If isullcheck(dtIDAM_PR.Rows(0)("state")) <> "" Then
                    'get lookup
                    Try
                        Dim statelookup As String
                        statelookup = GetDataTable("select name from ipm_state where state_id = '" & dtIDAM_PR.Rows(0)("state").trim & "'", ConnectionStringIDAM).Rows(0)("name").ToString.Trim
                        addkeywordtoproject(sProjectID, statelookup, "ipm_keyword")
                    Catch ex As Exception
                        addkeywordtoproject(sProjectID, dtIDAM_PR.Rows(0)("state").trim, "ipm_keyword")
                    End Try
                End If


                'Project(Description) : Project(Page) - 06
                'IDAM_V_DESCRIPTION_PROJECT_PAGE
                'get item_id
                'check if udf exists
                Dim sIDAM_V_DESCRIPTION_PROJECT_PAGE As String = CheckAddProjectUDF("sp_createnewprojectfield", "Description: Project Page", "Vision Data", "IDAM_V_DESCRIPTION_PROJECT_PAGE", "", 1, "General", 1, 0)
                'erase any possibel previous value
                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_DESCRIPTION_PROJECT_PAGE)
                DT_Temp = GetDataTable("select description from prdescriptions where desccategory = '06' and wbs1 = '" & sProjectNumber.Replace("'", "''") & "'", Me.ConnectionStringVISION)
                If DT_Temp.Rows.Count > 0 Then
                    Try

                        ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_DESCRIPTION_PROJECT_PAGE & ",'" & (DT_Temp.Rows(0)("description")).Replace("'", "''") & "')")
                    Catch ex As Exception

                    End Try
                End If
                'Project(Description) : Web() 10
                Dim sIDAM_V_DESCRIPTION_WEB As String = CheckAddProjectUDF("sp_createnewprojectfield", "Description: Web", "Vision Data", "IDAM_V_DESCRIPTION_WEB", "", 1, "General", 1, 0)
                'erase any possibel previous value
                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_DESCRIPTION_WEB)
                DT_Temp = GetDataTable("select description from prdescriptions where desccategory = '10' and wbs1 = '" & sProjectNumber.Replace("'", "''") & "'", Me.ConnectionStringVISION)
                If DT_Temp.Rows.Count > 0 Then
                    Try

                        ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_DESCRIPTION_WEB & ",'" & (DT_Temp.Rows(0)("description")).Replace("'", "''") & "')")
                    Catch ex As Exception

                    End Try
                End If
                'Project(Description) : Resume 08
                Dim sIDAM_V_DESCRIPTION_RESUME As String = CheckAddProjectUDF("sp_createnewprojectfield", "Description: Resume", "Vision Data", "IDAM_V_DESCRIPTION_RESUME", "", 1, "General", 1, 0)
                'erase any possibel previous value
                ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_DESCRIPTION_RESUME)
                DT_Temp = GetDataTable("select description from prdescriptions where desccategory = '08' and wbs1 = '" & sProjectNumber.Replace("'", "''") & "'", Me.ConnectionStringVISION)
                If DT_Temp.Rows.Count > 0 Then
                    Try

                        ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_DESCRIPTION_RESUME & ",'" & (DT_Temp.Rows(0)("description")).Replace("'", "''") & "')")
                    Catch ex As Exception

                    End Try
                End If


                'Construction(Type)
                Dim sIDAM_V_PROJECT_CTYPE As String = CheckAddProjectUDF("sp_createnewprojectfield", "Construction Type", "Vision Data", "IDAM_V_PROJECT_CTYPE", "", 1, "General", 1, 0)
                DT_Temp = GetDataTable("select custConstructionType from Projects_ConstructionType  where WBS2 = '' and  wbs1 = '" & sProjectNumber.Replace("'", "''") & "'", Me.ConnectionStringVISION)
                If DT_Temp.Rows.Count > 0 Then
                    Try
                        'erase any possibel previous value
                        ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_PROJECT_CTYPE)
                        ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_PROJECT_CTYPE & ",'" & RemoveHtml(DT_Temp.Rows(0)("custConstructionType")).Replace("'", "''") & "')")
                    Catch ex As Exception

                    End Try
                End If

                'Sub Practice Area
                Dim sIDAM_V_SUBPRACTICEAREA As String = CheckAddProjectUDF("sp_createnewprojectfield", "Sub Practice Area", "Vision Data", "IDAM_V_SUBPRACTICEAREA", "", 1, "General", 1, 0)
                DT_Temp = GetDataTable("select custSubPracticeArea from Projects_SubPracticeAreas  where WBS2 = '' and  wbs1 = '" & sProjectNumber.Replace("'", "''") & "'", Me.ConnectionStringVISION)
                If DT_Temp.Rows.Count > 0 Then
                    Try
                        'erase any possibel previous value
                        ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_SUBPRACTICEAREA)
                        ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_SUBPRACTICEAREA & ",'" & RemoveHtml(DT_Temp.Rows(0)("custSubPracticeArea")).Replace("'", "''") & "')")
                    Catch ex As Exception

                    End Try
                End If

                'Service(Type)
                Dim sIDAM_V_SERVICETYPE As String = CheckAddProjectUDF("sp_createnewprojectfield", "Service Type", "Vision Data", "IDAM_V_SERVICETYPE", "", 1, "General", 1, 0)
                DT_Temp = GetDataTable("select custServiceType from Projects_ServiceType  where WBS2 = '' and  wbs1 = '" & sProjectNumber.Replace("'", "''") & "'", Me.ConnectionStringVISION)
                If DT_Temp.Rows.Count > 0 Then
                    Try
                        'erase any possibel previous value
                        ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_SERVICETYPE)
                        ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_SERVICETYPE & ",'" & RemoveHtml(DT_Temp.Rows(0)("custServiceType")).Replace("'", "''") & "')")
                    Catch ex As Exception

                    End Try
                End If


                'Client(Confidential)
                'IDAM_V_CONFIDENTIAL_PROJECT()
                Dim sIDAM_V_CONFIDENTIAL_PROJECT As String = CheckAddProjectUDF("sp_createnewprojectfield", "Confidential", "Vision Data", "IDAM_V_CONFIDENTIAL_PROJECT", "", 1, "General", 1, 0)
                DT_Temp = GetDataTable("select clientconfidential from idam_pr where clientconfidential = 'Y' and wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringVISION)
                If DT_Temp.Rows.Count > 0 Then
                    'erase any possibel previous value
                    Try
                        ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_CONFIDENTIAL_PROJECT)
                        ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_CONFIDENTIAL_PROJECT & ",'1')")
                    Catch ex As Exception

                    End Try
                End If

                'IDAM_V_PROJECT_NUMBER
                Dim sIDAM_V_PROJECT_NUMBER As String = CheckAddProjectUDF("sp_createnewprojectfield", "Project Number", "Vision Data", "IDAM_V_PROJECT_NUMBER", "", 1, "General", 1, 0)
                Try
                    ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = " & sIDAM_V_PROJECT_NUMBER)
                    ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & "," & sIDAM_V_PROJECT_NUMBER & ",'" & sProjectNumber.Replace("'", "''") & "')")
                Catch ex As Exception

                End Try

            End If

            Me.ProgressBar1.Value += 1
        Next







        ' ''add project type
        ''Dim SourceType As DataTable = GetDataTable("select cfgprojecttype.* from idam_pr,cfgprojecttype where idam_pr.projecttype = cfgprojecttype.code and idam_pr.wbs1 = '" & sProjectNumber & "' and idam_pr.wbs2 = ''", Me.ConnectionStringExternal)
        ''If SourceType.Rows.Count > 0 Then
        ''    Dim sType As String = SourceType.Rows(0)("description")

        ''    'check for discpliine
        ''    Dim DTType As DataTable = GetDataTable("select keyid from ipm_office where keyuse = 1 and keyname = '" & sType.Trim().Replace("'", "''") & "'", ConnectionStringIDAM)
        ''    If DTType.Rows.Count > 0 Then
        ''        'discpline exists so simply reference
        ''        ExecuteTransaction("delete from ipm_project_office where projectid = " & catID & " and officeid = " & DTType.Rows(0)("Keyid"))
        ''        ExecuteTransaction("insert into ipm_project_office (officeid,projectid) values (" & DTType.Rows(0)("Keyid") & "," & catID & ")")
        ''    Else
        ''        'add to disipline table
        ''        sql = "exec sp_createnewkeywordOfficeType "
        ''        sql += "1,"
        ''        sql += "'" + sType.ToString().Replace("'", "''") + "', "
        ''        sql += "1" + ", "
        ''        sql += "'Imported from external source'"
        ''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction(sql)
        ''        'get ID of repo just inserted
        ''        Dim NewKeywordID As String = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select max(keyid) maxid from ipm_office").Rows(0)("maxid")
        ''        'add external id
        ''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("insert into ipm_project_office (officeid,projectid) values (" & NewKeywordID & "," & catID & ")")
        ''    End If
        ''End If

        '''''''''add additional project description data
        '''''''''add owners
        ''''''''If 1 = 1 Then
        ''''''''    'add project type
        ''''''''    Dim SourceType2 As DataTable = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select principal,projmgr from idam_pr where idam_pr.wbs1 = '" & sProjectNumber & "' and idam_pr.wbs2 = ''", Me.ConnectionStringExternal)
        ''''''''    If SourceType2.Rows.Count > 0 Then
        ''''''''        Dim sPrincipal As String = SourceType2.Rows(0)("principal")
        ''''''''        Dim sProjManger As String = SourceType2.Rows(0)("projmgr")

        ''''''''        'check 
        ''''''''        If sPrincipal <> "" Then
        ''''''''            'discpline exists so simply reference
        ''''''''            WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("update ipm_project set principalid = '" & sPrincipal & "' where projectid = " & catID)
        ''''''''        End If
        ''''''''        If sProjManger <> "" Then
        ''''''''            'discpline exists so simply reference
        ''''''''            WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("update ipm_project set pmid = '" & sProjManger & "' where projectid = " & catID)
        ''''''''        End If
        ''''''''    End If
        ''''''''End If





        '2403945: IDAM_V_PRACTICE_AREA() - 
        '2403946: IDAM_V_PROJECT_NUMBER() - idam_pr - wbs1
        '2403947: IDAM_V_OFFICE() - not available
        '2403948: IDAM_V_FIRM() - not available
        '2403949: IDAM_V_CONTRUCTION_TYPE() - not availalbe
        '2403950: IDAM_V_SUSTAINABLE() - not availalbe
        '2403951: IDAM_V_BUILDING_HEIGHT() - not available
        '2403952: IDAM_V_SUBPRACTICEAREA() - not available
        '2403953: IDAM_V_DESCRIPTION_PROJECT_PAGE() - done
        '2403954: IDAM_V_COMMON_NAME() - not available
        '2403955: IDAM_V_CONFIDENTIAL_PROJECT() - idam_pr - clientconfidential - done



        ''''''''IDAM_PRIMARY_PROJECT_TYPE
        '''''''DT_Temp = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select b.description from idam_pr a, cfgprojecttype b where a.projecttype = b.code and a.wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringExternal)
        '''''''If DT_Temp.Rows.Count > 0 Then
        '''''''    'erase any possibel previous value
        '''''''    Try

        '''''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503543")
        '''''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503543,'" & WebArchives.iDAM.Web.Core.IDAMFunctions.RemoveHtml(DT_Temp.Rows(0)("description")).Replace("'", "''") & "')")
        '''''''    Catch ex As Exception

        '''''''    End Try
        '''''''End If



        '''''IDAM_ACTUAL_COMPLETION_DATE()
        ''''DT_Temp = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select ActCompletionDate from pr where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringExternal)
        ''''If DT_Temp.Rows.Count > 0 Then
        ''''    'erase any possibel previous value
        ''''    Try

        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503715")
        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503715,'" & DT_Temp.Rows(0)("ActCompletionDate") & "')")
        ''''    Catch ex As Exception

        ''''    End Try
        ''''End If


        '''''IDAM_CONSTRUCTION_END_DATE()
        ''''DT_Temp = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select ConstComplDate from pr where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringExternal)
        ''''If DT_Temp.Rows.Count > 0 Then
        ''''    'erase any possibel previous value
        ''''    Try

        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503718")
        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503718,'" & DT_Temp.Rows(0)("ConstComplDate") & "')")
        ''''    Catch ex As Exception

        ''''    End Try
        ''''End If

        '''''IDAM_COMPETITION()
        ''''DT_Temp = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select custBuildingStatusCompetition from ProjectCustomTabFields where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringExternal)
        ''''If DT_Temp.Rows.Count > 0 Then
        ''''    'erase any possibel previous value
        ''''    Try

        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503719")
        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503719,'" & Me.isullcheck(DT_Temp.Rows(0)("custBuildingStatusCompetition")).replace("'", "''") & "')")
        ''''    Catch ex As Exception

        ''''    End Try
        ''''End If

        '''''IDAM_COUNTRY()
        ''''DT_Temp = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select Country from pr where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringExternal)
        ''''If DT_Temp.Rows.Count > 0 Then
        ''''    'erase any possibel previous value
        ''''    Try

        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503720")
        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503720,'" & Me.isullcheck(DT_Temp.Rows(0)("Country")).replace("'", "''") & "')")
        ''''    Catch ex As Exception

        ''''    End Try
        ''''End If

        '''''IDAM_GEOGRAPHIC_REGION()
        ''''DT_Temp = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select custlocationcode from ProjectCustomTabFields where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringExternal)
        ''''If DT_Temp.Rows.Count > 0 Then
        ''''    'erase any possibel previous value
        ''''    Try

        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503721")
        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503721,'" & Me.isullcheck(DT_Temp.Rows(0)("custlocationcode")).replace("'", "''") & "')")
        ''''    Catch ex As Exception

        ''''    End Try
        ''''End If

        '''''21503722: IDAM_AWARDS()
        ''''Dim ii As Integer
        ''''Dim sAwards As String = ""
        ''''DT_Temp = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select description,institution,awarddate from prawards where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringExternal)
        ''''If DT_Temp.Rows.Count > 0 Then
        ''''    Try

        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503722")
        ''''        For ii = 0 To DT_Temp.Rows.Count - 1
        ''''            'erase any possibel previous value
        ''''            sAwards += Me.isullcheck(DT_Temp.Rows(ii)("institution")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("awarddate")) & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("description")).replace("'", "''") & vbCrLf & vbCrLf
        ''''        Next
        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503722,'" & sAwards & "')")
        ''''    Catch ex As Exception

        ''''    End Try
        ''''End If


        '''''select custpublication,custpublisher,custpubdate,custpubarticle, from projects_publications order by seq
        '''''21503723: IDAM_PUBLICATIONS()
        ''''Dim sPublications As String = ""
        ''''DT_Temp = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select custpublication,custpublisher,custpubdate,custpubarticle from projects_publications where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = '' order by seq", Me.ConnectionStringExternal)
        ''''If DT_Temp.Rows.Count > 0 Then
        ''''    Try

        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503723")
        ''''        For ii = 0 To DT_Temp.Rows.Count - 1
        ''''            'erase any possibel previous value
        ''''            sPublications += Me.isullcheck(DT_Temp.Rows(ii)("custpubdate")) & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpublisher")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpublication")).replace("'", "''") & vbCrLf & Me.isullcheck(DT_Temp.Rows(ii)("custpubarticle")).replace("'", "''") & vbCrLf & vbCrLf
        ''''        Next
        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503723,'" & sPublications & "')")
        ''''    Catch ex As Exception

        ''''    End Try
        ''''End If

        '''''21503714: IDAM_START_DATE()
        ''''DT_Temp = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select startdate from pr where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringExternal)
        ''''If DT_Temp.Rows.Count > 0 Then
        ''''    Try

        ''''        'erase any possibel previous value
        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503714")
        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503714,'" & Me.isullcheck(DT_Temp.Rows(0)("startdate")) & "')")
        ''''    Catch ex As Exception

        ''''    End Try
        ''''End If


        '''''21503716: IDAM_OCCUPANCY_DATE()
        ''''DT_Temp = WebArchives.iDAM.Web.Core.IDAMQueryService.GetDataTable("select custOccupancyDate from ProjectCustomTabFields where wbs1 = '" & sProjectNumber.Replace("'", "''") & "' and wbs2 = ''", Me.ConnectionStringExternal)
        ''''If DT_Temp.Rows.Count > 0 Then
        ''''    Try

        ''''        'erase any possibel previous value
        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("delete from ipm_project_field_value where projectid = " & sProjectID & " and item_id = 21503716")
        ''''        WebArchives.iDAM.Web.Core.IDAMQueryService.ExecuteTransaction("insert into ipm_project_field_value (projectid,item_id,item_value) values (" & sProjectID & ",21503716,'" & Me.isullcheck(DT_Temp.Rows(0)("custOccupancyDate")) & "')")
        ''''    Catch ex As Exception

        ''''    End Try
        ''''End If




    End Sub

    Private Sub Button16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button16.Click


        VisionNightly()


    End Sub



    'sp_createnewprojectfield
    Private Function CheckAddProjectUDF(ByVal sUseStoredPro, ByVal item_Name, ByVal item_tab, ByVal item_tag, ByVal Item_Description, ByVal Item_type, ByVal item_group, ByVal viewable, ByVal editable) As String
        Dim dt As New DataTable
        dt = GetDataTable("select item_id from ipm_project_field_desc where item_tag = '" & item_tag & "'", ConnectionStringIDAM)
        If dt.Rows.Count.Equals(0) Then
            Dim sql As String
            Dim sKeyUse As String
            sql = "exec {0} "
            sql += "1,"
            sql += "'" + item_tag.ToString().Replace("'", "''") + "', "
            sql += "'" + Item_Description.ToString().Replace("'", "''") + "', "
            sql += "'', "
            sql += "'" + Item_type.ToString().Replace("'", "''") + "', "
            sql += "'" + item_group.ToString().Replace("'", "''") + "', "
            sql += "'Project Field', "
            sql += viewable.ToString() + ", "
            sql += editable.ToString() + ", "
            sql += "'" + item_Name.ToString().Replace("'", "''") + "'"
            sql += ",'" + item_tab.ToString().Replace("'", "''") + "' "
            sql = String.Format(sql, sUseStoredPro)
            ExecuteTransaction(sql)
            'get maxid
            Return GetDataTable("select item_id from ipm_project_field_desc where item_tag = '" & item_tag & "'", ConnectionStringIDAM).Rows(0)("item_id")
        Else
            Return dt.Rows(0)("item_id")
        End If

    End Function



    Private Sub Button17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button17.Click
        log = log4net.LogManager.GetLogger("PE_USER_LOG")
        log4net.Config.XmlConfigurator.Configure()
        log.Debug("Startup")
        'select all actie users
        'check if in ipm_user
        'if not, then add - assign 
        '        Vision Import Default Settings 
        '        role:   staff()
        '        Security(level) : Internal(use)
        '        user(Type) : user()
        '        use ldap authentication 
        Dim UserCheck As String
        Dim ActiveUsers As DataTable = GetDataTable("select a.*, b.username from idam_em a, iDAM_SEUser b where a.Employee = b.Employee and isnull(eMail,'') <> '' and email like '%perkinseastman.com'", Me.ConnectionStringVISION)
        ProgressBar1.Maximum = ActiveUsers.Rows.Count
        ProgressBar1.Value = 0
        For Each row As DataRow In ActiveUsers.Rows

            'check if in user table
            If GetDataTable("select userid from ipm_user where externaluserid= '" & row("employee").trim & "' or (lastname = '" & row("lastname").replace("'", "''").trim & "' and firstname = '" & row("firstname").replace("'", "''").trim & "')", Me.ConnectionStringIDAM).Rows.Count = 0 Then
                'add user
                ' If GetDataTable("select userid from ipm_user where login = " & row("employee"), Me.ConnectionStringIDAM).Rows.Count = 0 Then
                log.Debug("Adding user:" & row("firstname") & " " & row("lastname") & " " & row("employee"))

                ExecuteTransaction("exec sp_createnewuser50 " & _
              " 1, '', " & _
             "1" & ",  " & _
            "'" & Replace(row("firstname"), "'", "''") & "', " & _
           "'" & Replace(row("lastname"), "'", "''") & "', " & _
          "'" & Replace(isullcheck(row("email")), "'", "''") & "', " & _
         "'" & Replace("Perkins Eastman", "'", "''") & "', " & _
        "'" & Replace(isullcheck(row("workphone")), "'", "''") & "', " & _
       "'" & LCase(isullcheck(row("username"))) & "', " & _
      "'" & Replace("1", "'", "''") & "', " & _
                    "'" & Replace("", "'", "''") & "', " & _
                          "'" & LCase(isullcheck(row("username"))) & "', " & _
                  "'" & Replace("", "'", "''") & "', " & _
                 "'" & Replace("", "'", "''") & "', " & _
                "'" & Replace("", "'", "''") & "', " & _
                "'" & Replace("", "'", "''") & "', " & _
                "'" & Replace("", "'", "''") & "', " & _
                "'" & Replace("", "'", "''") & "', " & _
                "'" & Replace("", "'", "''") & "', " & _
                        "'" & Replace("", "'", "''") & "', " & _
                         "'" & "0" & "' ")
                'get ID of repo just inserted
                Dim NewUserID As String = GetDataTable("select top 1 userid maxid from ipm_user order by update_date desc", Me.ConnectionStringIDAM).Rows(0)("maxid")
                'add oid
                Me.ExecuteTransaction("update ipm_user set externaluserid = '" & row("employee").ToString.Trim & "',createdby='1' where userid = " & NewUserID.Trim)
                'Me.ExecuteTransaction("insert into ipm_user_role (userid,role_id) values (" & NewUserID.Trim & ",21513607)")
                'log insert
                Me.ExecuteTransaction("insert into ipm_error (type,object_id,output) values ('A','1','" & row("employee") & "')")
            End If



            '@UserID as char (50),
            '@Password as char (50),
            '@SecurityLevel_Id as char (10),
            '@FirstName as nvarchar (100),
            '@LastName as nvarchar (100),
            '@Email as nvarchar (100),
            '@Agency as nvarchar (50),
            '@Phone as nvarchar (50),
            '@Login as char (50),
            '@ldap_authentication as int,
            '@Description as ntext,
            '@UserName as nvarchar (255),
            '@WorkAddress as nvarchar (150),
            '@WorkCity as nvarchar (100),
            '@WorkState as char (2),
            '@WorkZip as nvarchar (50),
            '@Department as nvarchar (50),
            '@Position as nvarchar (50),
            '@Cell as nvarchar (50),
            '@Bio as nvarchar (2048),	
            '@Contact as char (1)



        Next
        ''''''''''select all inactive users
        ''''''''''if in system, then change to contact
        '''''''''Dim InActiveUsers As DataTable = GetDataTable("select * from em where status = 'T'", Me.ConnectionStringVISION)
        '''''''''ProgressBar1.Maximum = ActiveUsers.Rows.Count
        '''''''''ProgressBar1.Value = 0
        '''''''''For Each row As DataRow In InActiveUsers.Rows
        '''''''''    'check if in user table
        '''''''''    If GetDataTable("select * from ipm_user where oid = " & row("employee"), Me.ConnectionStringIDAM).Rows.Count > 0 Then
        '''''''''        'move user to contact
        '''''''''        Me.ExecuteTransaction("update ipm_user set contact = 1 where oid = " & row("employee"))
        '''''''''        Me.ExecuteTransaction("insert into ipm_error (type,object_id,output) values ('T','1','" & row("employee") & "')")
        '''''''''    End If
        '''''''''Next
        log.Debug("Completed")

    End Sub

    Private Sub Button18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button18.Click

        'select all assets in IDAM affected by duplication
        Dim allassets As DataTable = GetDataTable("SELECT     a.newid, a.matchname, a.highrezname, b.ProjectID, b.oldid, b.lowrezname, b.UserID FROM (SELECT     Asset_ID AS newid, REPLACE(Name, '_HR.tif', '_MR.jpg') AS matchname, Name AS highrezname  FROM IPM_ASSET  WHERE  (Name NOT LIKE '%_MR.jpg')) AS a INNER JOIN    (SELECT     ProjectID, Asset_ID AS oldid, Name AS lowrezname, UserID FROM IPM_ASSET  WHERE      (Available = 'Y') AND  (Name LIKE '%_MR.jpg') AND (Category_ID IN                                                (SELECT     CATEGORY_ID  FROM          IPM_ASSET_CATEGORY  WHERE      (NAME = 'Public WebSite Images')))) AS b ON a.matchname = b.lowrezname", ConnectionStringIDAM)
        ProgressBar1.Maximum = allassets.Rows.Count + 1
        ProgressBar1.Value = 0
        For Each row As DataRow In allassets.Rows
            ProgressBar1.Value = ProgressBar1.Value + 1
            Dim mrid As String = row("oldid")
            Dim hrid As String = row("newid")
            'copy data to new asset
            'first delete values if exist - shouldnt
            ExecuteTransaction("delete from ipm_asset_field_value where asset_id = " & hrid & " and item_id in (2400078,2400079,2400398,2400401,2401051)")
            ExecuteTransaction("insert into ipm_asset_field_value (asset_id,item_id,item_value) select " & hrid & " asset_id,item_id,item_value from ipm_asset_field_value where asset_id = " & mrid & " and item_id in (2400078,2400079,2400398,2400401,2401051)")
            'assign permissions - aid 2400447	secid 2401028	type U
            ExecuteTransaction("delete from ipm_asset_security where asset_id = " & hrid & " and security_id = 2401028 and type = 'U'")
            ExecuteTransaction("insert into ipm_asset_security (asset_id,security_id,type) select " & hrid & " asset_id,'2401028' security_id,'U' type")
            'tag old asset description with 'duplicate asset deleted 52510' and 'delete old asset
            ExecuteTransaction("update ipm_asset set description = 'duplicate asset deleted 52510', available = 'N' where asset_id = " & mrid)
            Me.Refresh()
            ProgressBar1.Refresh()
        Next
    End Sub

    Private Sub Button19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button19.Click
        'select b.Asset_ID,a.item_value from IPM_ASSET b, ipm_asset_field_value a where b.Name like '%_HR.%' and b.Available = 'Y' and a.ASSET_ID = b.Asset_ID and a.item_id = 30057067
        'select all assets in IDAM affected by duplication
        Dim allassets As DataTable = GetDataTable("select b.Asset_ID,a.item_value from IPM_ASSET b, ipm_asset_field_value a where b.Name like '%_HR.%' and b.Available = 'Y' and a.ASSET_ID = b.Asset_ID and a.item_id = 30057067", ConnectionStringIDAM)
        ProgressBar1.Maximum = allassets.Rows.Count + 1
        ProgressBar1.Value = 0
        Dim allvals As String
        For Each row As DataRow In allassets.Rows
            ProgressBar1.Value = ProgressBar1.Value + 1
            Dim _id As String = row("asset_id")
            Dim _val As String = row("item_value")
            
            'check to see if asset exists in dfs



            If Not File.Exists(_val.ToString.Replace(ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONLOCATIONUNCOVERRIDE"), ConfigurationSettings.AppSettings("IDAM.IDAMREPLICATIONLOCATION"))) Then
                'MsgBox(_val)
                allvals += _id & vbCrLf
            End If
            Clipboard.SetText(allvals)

            Me.Refresh()
            ProgressBar1.Refresh()
        Next

        Clipboard.SetText(allvals)
        MsgBox("done")
    End Sub
End Class
