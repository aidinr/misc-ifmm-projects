﻿SELECT     a.projectid,a.ProjectName, d.ProjectDisplayName, a.ProjCity, a.ProjAddress, a.ProjAddress2, a.ProjCounty, c.CountryName, a.ProjCountryID, e.StateName, a.ProjStateID, 
                      a.ProjZip, b.Header, b.Description, Shared_PracticeAreas.PracticeAreaName
FROM         Shared_PracticeAreas INNER JOIN
                      Grid_ProjPracticeAreas ON Shared_PracticeAreas.PracticeAreaId = Grid_ProjPracticeAreas.PracticeAreaId RIGHT OUTER JOIN
                      Projects AS a ON Grid_ProjPracticeAreas.ProjectId = a.ProjectID LEFT OUTER JOIN
                      Countries AS c ON a.ProjCountryID = c.CountryID LEFT OUTER JOIN
                      Descriptions AS b ON a.ProjectID = b.ProjectID LEFT OUTER JOIN
                      grid_Projects_additional AS d ON a.ProjectID = d.projectID LEFT OUTER JOIN
                      States AS e ON a.ProjStateID = e.StateID
WHERE     (a.ProjectID IN
                          (SELECT     pc#ProjectId
                            FROM          IDAM_PE.dbo.Project_Images
                            GROUP BY pc#ProjectId)) AND (b.WebsiteIndicator = 1)