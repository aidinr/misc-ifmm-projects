﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="property-inner.aspx.vb" Inherits="property_inner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content transparent-black-box no-top-border property-inner-page">
        <h1 class="headline"><%=dt.Rows(0)("Shortname") %></h1>
        <div id="container" class="cf">

          <div id="main" role="main">
              <section class="slider">
                <div class="flexslider carousel">
                  <ul class="slides">
                    <asp:Repeater ID="asset_list" runat="server">
                        <ItemTemplate>
                            <li>
                              <img  src="<%=Session("WSRetreiveAsset")%>type=asset&id=<%# Eval("asset_id")%>&width=832&height=371&crop=2" alt="" class="property-main-view"  />
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                  </ul>
                </div>
              </section>
            </div>
          </div>
        <div class="property-info">
            <div class="text"> <strong><%= dt.Rows(0)("Address")%>,<%= dt.Rows(0)("City")%>, <%= dt.Rows(0)("State_Id")%> <%= dt.Rows(0)("Zip")%> <br/>Phone: <%= dt.Rows(0)("Phone_Number")%></strong>

                <p><%= dt.Rows(0)("Description")%></p>
            </div>
            <div class="aside">
                <div class="property-links">
                    <a href="services-request" class="gear-icon">service request</a>
                    <a href="http://<%= dt.Rows(0)("WebURL")%>" class="web-icon">property website</a>
                </div>
                <div class="accordion">
                    <h3><strong>Apartment Amenities</strong><ins>&nbsp;</ins></h3>

                    <div class="billow">
                        <div class="text">
                            <ul class="disc-list">
                                <li><%= dt.Rows(0)("apartment_amenities")%></li>
                            </ul>
                        </div>
                    </div>
                    <h3><strong>Community Amenities</strong><ins>&nbsp;</ins></h3>

                    <div class="billow">
                        <div class="text">
                            <ul class="circle-list">
                                <li><%= dt.Rows(0)("property_amenities")%></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar property-inner-page-sidebar backToSearch">    
    </div>
</asp:Content>

