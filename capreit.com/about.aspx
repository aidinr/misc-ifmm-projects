﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="about.aspx.vb" Inherits="about" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content">
        <div class="tabs-content transparent-black-box no-top-border">
            <div id="overview">
                <h1 class="headline cms cmsType_TextSingle cmsName_About_Page_Heading">About Us</h1>
        <ul class="tabs-holder">
            <li>
                <a href="about">overview</a><ins> | </ins>
            </li>
            <li>
                <a href="about-investment-strategy">investment strategy</a><ins> | </ins>
            </li>
            <li>
                <a href="about-leadership">leadership</a><ins> | </ins>
            </li>
            <li>
                <a href="about-company-history">company history</a>
            </li>
        </ul>
                <div class="text">
                    <p class="cms cmsType_TextMulti cmsName_About_Page_Content">From its inception in 1993, CAPREIT has continued to grow into one of the nation’s leading housing companies. Over its history, CAPREIT has been involved in over 300 multifamily communities, comprising over 30,000 rental and condominium apartment homes
                        with a value in excess of $3.5 billion. These properties are/were located in forty-four states, including Virginia. CAPREIT has completed acquisitions with some of the world’s leading investors including: AREA, Blackrock, Praedium Group, Principal Growth
                        Investors and GE.
                        <br /><br />
                        The success of CAPREIT is cultivated through management’s implementation of a savvy acquisition strategy, innovative financial structures, and a professional property management style focused on resident satisfaction and retention, as well as asset preservation
                        and enhancement. CAPREIT is characterized by the excellence with which it has performed for over a decade. Today, CAPREIT maintains its focus on strategic acquisitions and value-added growth, continuing to successfully capitalize upon today’s opportunities
                        in multifamily housing.
                     </p>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar about">
        <a class="next-arrow ajax-next-arrow" href="about-investment-strategy"> <ins><var>&nbsp;</var></ins><span><strong>investment strategy</strong></span></a>
        <div class="switchable-quotes">
            <div class="transparent-blue-box quote-box">
                <big class="cms cmsType_TextSingle cmsName_About_Page_Quote_Heading">OUR VALUES</big><em class="cms cmsType_TextMulti cmsName_About_Page_Quote_Content">Integrity, Relationships and Results,  the new IRR</em><var class="triangle-figure">&nbsp;</var>
            </div>
        </div>
    </div>
</asp:Content>

