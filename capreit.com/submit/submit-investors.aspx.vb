﻿Imports System.Net.Mail
Imports System.IO
Imports System.Threading

Partial Class contact_submit
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim EmailBody As StringBuilder = New StringBuilder
        Dim message As MailMessage = New MailMessage(ConfigurationManager.AppSettings("ContactEmailFrom").ToString(), ConfigurationManager.AppSettings("ContactEmailTo").ToString())

        If (String.IsNullOrEmpty(Request.Params("First_Name"))) Then
            Response.Clear()
            Response.ClearHeaders()
            Response.ClearContent()
            Response.Write("-7")
            Response.Flush()
            Return
            Exit Sub
        End If

        If (String.IsNullOrEmpty(Request.Params("Last_Name"))) Then
            Response.Clear()
            Response.ClearHeaders()
            Response.ClearContent()
            Response.Write("-7")
            Response.Flush()
            Return
            Exit Sub
        End If

        If (String.IsNullOrEmpty(Request.Params("Email"))) Then
            Response.Clear()
            Response.ClearHeaders()
            Response.ClearContent()
            Response.Write("-7")
            Response.Flush()
            Return
            Exit Sub
        End If

        Try
            EmailBody.Append("First Name: " + Request.Params("First_Name") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Last Name: " + Request.Params("Last_Name") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Email: " + Request.Params("Email") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("Message: " + Request.Params("Message") + Environment.NewLine + Environment.NewLine)
            EmailBody.Append("IP Address: " + Request.ServerVariables("REMOTE_ADDR").ToString())

            message.IsBodyHtml = False
            message.Body = EmailBody.ToString()
            message.From = New System.Net.Mail.MailAddress(ConfigurationManager.AppSettings("ContactEmailFrom").ToString())
            message.Subject = "CAPREIT Investment Inquiry"
            message.ReplyTo = New MailAddress(Request.Params("email"))

            Dim SmtpMail As New SmtpClient
            Dim basicAuthenticationInfo As New System.Net.NetworkCredential(ConfigurationManager.AppSettings("SMTPUser").ToString(), ConfigurationManager.AppSettings("SMTPPassword").ToString())

            SmtpMail.Host = ConfigurationManager.AppSettings("SMTPServer").ToString()
            SmtpMail.UseDefaultCredentials = False
            SmtpMail.Credentials = basicAuthenticationInfo
            SmtpMail.Send(message)
            WriteToFile()

            Dim Response As HttpResponse = Context.Response

            Response.Clear()
            Response.ClearContent()
            Response.ClearHeaders()
            Response.BufferOutput = False
            Response.ContentType = "text/html"
            Context.Response.Write("1")
            Response.Flush()
            Try
                Response.End()
            Catch ex As Exception

            End Try
            Thread.Sleep(5000)
        Catch ex As Exception
            Response.Clear()
            Response.ClearHeaders()
            Response.ClearContent()
            Response.Write("-9")
            Response.Write(ex.Message)
            Response.Flush()
            Try
                Response.End()
            Catch ex1 As Exception

            End Try
            Thread.Sleep(5000)
        End Try

    End Sub
    Public Sub WriteToFile()
        Try
            Dim FILE_NAME As String = Server.MapPath("submissions/submit-investors.txt")
            Dim Msg As String
            Msg = Request.Params("First_Name") & "|"
            Msg &= Request.Params("Last_Name") & "|"
            Msg &= Request.Params("Email") & "|" 
            Msg &= Request.Params("Message") & "|" 
            Msg &= Request.ServerVariables("REMOTE_ADDR").ToString()
            Msg &= Environment.NewLine

            If System.IO.File.Exists(FILE_NAME) = True Then
                Dim file_stream As FileStream = File.Open(FILE_NAME, FileMode.Append)
                Dim bytes As Byte() = New UTF8Encoding().GetBytes(Msg)
                file_stream.Write(bytes, 0, bytes.Length)
                file_stream.Flush()
                file_stream.Close()
            End If
        Catch ex As Exception

        End Try
      
    End Sub
End Class