function flexslider(){
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    itemWidth: 830,
    itemMargin: 1,
    pausePlay: false
  });	
}

$(window).bind('load', function() {
    setTimeout(function() {
      flexslider();
    }, 1000);
}); 

