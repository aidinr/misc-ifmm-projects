﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="properties.aspx.vb" Inherits="properties" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="content propertySearch transparent-black-box no-top-border properties-page">
            <h1 class="headline">Properties <!-- <small>Please select one of the highlighted states or filter using the drop-down menus.</small> --> </h1>

              <!-- Property search setion --> 
              <div class="propSelect">
                <div class="selectOption state">
                 <p class="selectOptions"><a>State</a></p>
                 <p class="optionList">
                  <asp:Repeater ID="States_List" runat="server">
                        <ItemTemplate>
                            <a class="link" href="properties-<%# Eval("State").Trim%>"><%# Eval("State").Trim%></a>
                        </ItemTemplate>
                  </asp:Repeater>
                 </p>
                </div>

              <!--  <div class="selectOption city">
                 <p class="selectOptions"><a>City</a></p>
                 <p class="optionList">
                  <a class="link" href="properties-California-Los_Angeles">Los Angeles<em>, CA</em></a>
                  <a class="link" href="properties-California-Redlands">Redlands<em>, CA</em></a>
                  <a class="link" href="properties-Florida-Miami">Miami<em>, FL</em></a>
                  <a class="link" href="properties-New_York-New_York">New York<em>, NY</em></a>
                 </p>
                </div> -->

           <!--     <div class="selectOption community">
                 <p class="selectOptions"><a>Filter by Community</a></p>
                 <p class="optionList">
                  <a class="link" href="properties-community-Oak_Hills">Oak Hills</a>
                  <a class="link" href="properties-community-Rose_Trellis">Rose Trellis</a>
                  <a class="link" href="properties-community-Sunny_Lane">Sunny Lane</a>
                 </p>
                </div> -->
                <div class="c"></div>
              </div>

              <p id="propertySearch"><input type="text" value="Keywords to Search"  name="keyword" id="keyword" /><span></span></p>
              <input type="button" value="search" onclick="javascript: location.href='properties-search-' + document.getElementById('keyword').value;" class="search submit" name="submit" />
              <div class="c"></div>    
          <div class="map">
          <img src="/assets/map/map.png" width="681" height="431" alt="" class="map-img" border="0" usemap="#Map" id="img-map-img" />
          <map name="Map" id="Map">
            <area class="active" shape="poly" state="VA" full="Virginia"  alt="Virginia" coords="576,202,578,196,572,190,579,189,578,187,575,184,572,186,568,185,562,194,557,201,553,202,548,217,543,220,537,224,531,225,527,225,523,222,513,234,525,233,534,232,543,231,552,229,562,227,573,225,580,224,586,223,591,222,596,221,604,220,603,218,599,217,596,218,596,216,596,214,596,211,595,207,596,204,595,201,591,200,588,201,585,200,581,200,579,202" href="properties-Virginia" />
            <area class="active" shape="poly" state="MD" full="Maryland" alt="Maryland" coords="595,172,591,173,585,174,580,175,552,181,552,187,563,179,569,178,573,179,576,182,579,184,583,184,586,188,589,189,590,185,592,178,594,179,594,184,597,189,595,193,599,195,603,197,606,198,608,191,601,187,597,181" href="properties-Maryland" />
            <area shape="poly" state="KS" full="Kansas" alt="Kansas" coords="270,188,352,191,357,194,354,197,360,206,361,240,266,235" href="properties-Kansas" />
            <area class="active" shape="poly" state="MN" full="Minnesota" alt="Minnesota" coords="333,45,356,45,357,38,362,48,369,49,373,51,381,50,395,58,399,58,404,57,413,59,407,63,389,79,386,80,386,90,380,96,381,100,382,103,381,112,386,117,390,118,394,122,400,127,400,132,341,132,341,103,337,98,339,96,340,86,337,83,337,70,337,64,333,58,333,48" href="properties-Minnesota" />
            <area class="active" shape="poly" state="AL" full="Alabama" alt="Alabama" coords="450,271,481,268,493,309,493,313,493,318,494,328,494,330,460,333,459,337,463,342,458,346,456,338,453,343,450,340" href="properties-Alabama" />
            <area class="active" shape="poly" state="MO" full="Missouri" alt="Missouri" coords="363,247,363,204,357,198,361,195,358,192,352,187,349,182,397,181,399,185,399,190,400,195,409,203,411,208,416,209,416,212,415,215,414,218,416,221,418,223,421,225,424,225,426,228,426,231,426,234,427,236,428,237,430,240,432,241,432,244,429,245,427,248,425,252,421,254,420,254,423,247" href="properties-Missouri" />
            <area class="active" shape="poly" state="FL" full="Florida" alt="Florida" coords="462,336,466,342,469,341,470,342,478,341,481,342,484,345,488,347,493,350,496,352,501,351,509,346,514,344,517,347,521,349,523,352,526,355,529,357,532,358,533,362,533,366,534,370,535,375,538,377,538,383,540,388,544,392,547,393,550,396,550,401,553,403,554,405,556,407,560,407,563,411,565,416,568,418,573,416,576,413,576,409,576,403,577,399,576,396,576,391,575,388,573,385,570,380,566,373,563,367,563,362,559,358,555,354,545,332,541,331,541,335,540,339,535,336,531,335,499,338,496,334,492,333,481,335,476,336" href="properties-Florida" />
            <area class="active" shape="poly" state="TN" full="Tennessee" alt="Tennessee" coords="423,269,423,265,425,261,428,258,431,248,448,247,449,244,468,241,529,235,529,238,525,242,520,245,516,247,513,251,508,255,500,259,498,263,495,264,424,271" href="properties-Tennessee" />
            <area class="active" shape="poly" state="GA" full="Georgia" alt="Georgia" coords="484,268,495,267,504,265,513,264,511,268,514,271,519,273,521,276,526,281,533,286,535,290,540,293,541,297,544,300,547,304,549,307,550,311,548,318,545,327,539,329,537,333,501,335,496,329,497,318,497,311,484,270,484,272,486,277" href="properties-Georgia" />
            <area class="active" shape="poly" state="SC" full="South Carolina" alt="South Carolina" coords="514,268,526,259,542,258,546,262,551,263,560,261,577,273,571,282,572,285,568,288,563,296,560,297,553,297,553,300,555,303,551,306,538,289" href="properties-South_Carolina" />
            <area class="active" shape="poly" state="WI" full="Wisconsin" alt="Wisconsin" coords="389,83,395,83,404,77,403,83,409,82,411,86,417,88,422,89,427,91,434,91,437,94,437,100,440,101,442,104,439,109,438,113,444,110,448,104,444,113,443,117,441,124,441,131,441,140,442,146,411,148,406,144,404,136,402,129,402,124,397,122,391,116,385,113,384,104,382,98,389,90,389,86" href="properties-Wisconsin" />
            <area class="active" shape="poly" state="MI" full="Michigan" alt="Michigan" coords="412,82,435,67,431,74,430,78,437,75,440,78,443,82,451,82,458,77,465,77,468,75,469,78,477,79,481,85,478,92,489,97,491,102,493,107,492,112,489,117,487,120,489,124,493,122,498,117,501,120,504,125,503,128,506,131,505,137,503,138,501,143,500,148,498,152,458,156,462,146,463,139,463,133,460,129,458,126,458,119,460,115,460,108,464,105,467,107,469,105,469,100,471,97,473,93,477,90,475,87,475,85,466,85,463,87,457,90,455,93,451,92,448,93,445,100,440,95,437,90,431,89,424,87,416,84" href="properties-Michigan" />
            <area class="active" shape="poly" state="IL" full="Illinois" alt="Illinois" coords="413,150,442,149,443,154,447,159,449,197,448,201,449,205,450,209,446,215,444,219,445,225,445,229,440,232,441,234,435,235,433,236,430,236,429,228,426,223,419,221,416,217,419,212,418,207,412,206,411,201,406,197,403,192,402,186,405,180,408,173,408,169,412,168,416,162,418,158,415,152" href="properties-Illinois" />
            <area class="active" shape="poly" state="NC" full="North Carolina" alt="North Carolina" coords="501,263,506,258,511,256,516,252,520,248,524,246,527,243,531,240,533,236,536,234,544,233,550,231,557,230,564,229,572,228,578,226,591,225,597,223,605,222,607,227,601,231,598,229,596,231,597,235,601,236,605,234,607,237,610,235,609,239,607,243,601,242,597,243,601,245,600,247,600,251,602,253,604,251,597,257,592,261,589,264,587,269,581,272,574,268,569,264,565,261,562,259,556,259,551,261,544,257,539,257,533,256,525,258,518,260,514,262,504,263" href="properties-North_Carolina" />
            <area class="active" shape="poly" state="IN" full="Indiana" alt="Indiana" coords="450,161,454,161,457,158,470,157,480,157,480,163,482,175,483,183,484,193,485,203,479,204,478,210,475,212,473,215,472,220,467,216,465,217,463,220,459,221,453,223,447,221,450,214,452,211,454,208,451,202" href="properties-Indiana" />
            <area shape="poly" state="CA" full="California" alt="California" coords="10,105,10,112,10,115,1,128,2,133,4,140,2,153,4,160,7,166,7,171,10,173,13,172,18,174,15,177,16,183,10,181,10,188,14,194,14,199,12,201,14,207,20,221,23,227,21,233,25,239,31,240,41,249,46,252,46,255,50,258,57,269,57,278,87,283,92,281,91,277,90,272,94,268,94,262,99,260,97,252,96,247,44,166,57,119" href="properties-California"/>
            <area shape="poly" state="OH" full="Ohio" alt="Ohio" coords="482,157,490,156,499,155,503,157,512,157,518,155,531,146,532,150,532,156,533,161,533,166,533,169,534,172,533,176,532,180,533,184,529,188,524,191,522,196,519,196,518,200,518,205,510,202,507,204,501,204,495,202,491,198,486,196,486,191" href="properties-Ohio" />
            <area class="active" shape="poly" state="PA" full="Pennsylvania" alt="Pennsylvania" coords="534,146,540,141,542,145,593,134,598,140,602,142,599,149,599,155,604,162,600,167,596,170,565,176,551,179,539,181" href="properties-Pennsylvania" />
            <area shape="poly" state="CT" full="Conneticut" alt="Conneticut" coords="616,129,635,124,637,134,627,138,619,144" href="properties-Conneticut" />
            <area shape="poly" state="NJ" full="New Jersey" alt="New Jersey" coords="604,143,613,147,613,152,613,155,617,158,616,163,615,170,612,178,601,173,609,162,602,155,602,145" href="properties-New Jersey" />
            <area shape="poly" state="NV" alt="Nevada" coords="98,244,99,231,101,228,103,228,107,229,127,137,59,120,46,166" href="properties-Nevada" />
            <area shape="poly" state="AZ" full="Arazona" alt="Arazona" coords="89,285,94,282,94,279,93,274,97,268,97,263,102,260,102,257,101,253,98,247,100,243,101,232,103,229,106,231,110,230,113,217,174,227,160,317,157,317,150,316,143,315,137,314" href="properties-Arazona" />
            <area class="active" shape="poly" state="TX" full="Texas" alt="Texas" coords="253,245,291,248,290,279,295,282,298,283,300,286,305,286,309,288,314,287,317,292,322,291,326,293,328,295,331,293,335,293,339,295,344,293,354,293,359,293,363,296,370,297,370,323,373,328,372,331,376,334,376,337,376,343,373,348,376,352,373,356,370,360,365,362,361,359,358,359,357,363,356,367,355,371,351,376,344,378,341,375,337,377,335,379,336,382,333,384,329,385,326,388,328,390,323,392,326,394,324,399,319,399,321,402,323,403,321,408,323,415,325,421,319,420,315,418,306,416,298,413,294,404,291,393,287,389,284,384,280,379,276,367,266,354,247,353,239,365,235,365,220,351,218,339,197,315,246,319" href="properties-Texas" />
            <area shape="poly" state="OK" full="Oklahoma" alt="Oklahoma" coords="254,237,361,241,363,292,357,291,351,290,346,290,340,292,335,291,329,290,323,289,319,289,314,286,309,285,305,284,302,281,299,280,296,279,292,277,293,263,294,247,293,245,289,245,276,245,264,244,254,243" href="properties-Oklahoma" />
            <area shape="poly" state="WA" full="Washington" alt="Washington" coords="45,43,35,37,37,35,38,32,36,30,38,28,38,25,36,25,37,12,35,9,38,5,42,8,50,11,54,14,48,24,51,26,53,23,56,20,59,21,61,18,61,16,61,11,60,10,65,7,60,2,60,1,118,17,108,55,108,62,95,59,82,57,70,58,68,57,62,57,56,53,51,54,46,52,45,45" href="properties-Washington" />
            <area shape="poly" state="OR" full="Origon" alt="Origon" coords="11,104,12,91,16,85,19,82,19,79,27,61,32,49,33,42,37,41,39,43,42,45,43,48,43,52,47,57,52,57,54,56,58,56,60,59,65,59,69,60,73,60,78,59,82,59,87,60,92,61,98,62,104,64,108,65,109,67,112,71,108,75,105,79,102,83,99,87,99,90,101,93,99,97,99,102,98,106,95,116,92,125,91,126" href="properties-Oregon" />
            <area shape="poly" state="DC" full="Washington, DC" alt="Washington, DC" coords="582,186,581,191,575,191,580,195,578,200,583,197,587,199,585,195,589,191,584,191" href="properties-Washington_DC" />
            <area shape="poly" state="LA" full="Louisiana" alt="Louisiana" coords="430,339,428,342,429,344,432,347,429,347,426,346,422,347,422,350,423,353,426,354,429,352,432,354,435,353,437,352,436,356,433,358,433,360,440,365,438,367,430,362,427,363,426,366,423,366,417,366,413,366,403,357,398,357,397,360,393,361,389,359,384,358,379,359,374,359,377,355,377,352,378,347,379,344,380,342,380,335,378,333,375,329,374,325,372,322,372,307,409,306,409,312,411,316,412,317,409,321,407,323,406,326,405,330,404,332,404,337,403,340,406,339" href="properties-Louisiana" />
            <area shape="poly" state="NY" full="New York" alt="New York" coords="543,141,542,138,544,136,549,130,551,128,549,124,548,122,552,119,557,117,561,118,563,119,566,118,571,117,576,114,579,111,580,107,579,102,577,101,580,97,582,95,583,90,586,86,589,85,594,84,600,83,604,82,606,83,607,87,607,90,608,95,608,97,610,100,611,104,613,106,613,109,614,115,614,119,614,123,614,127,614,130,615,133,616,135,615,138,615,141,616,143,616,146,619,147,621,146,625,144,630,143,628,146,626,148,620,150,616,149,612,144,607,142,602,139,598,136,594,133,592,132,588,133,581,135,570,137,546,141" href="properties-New_York" />
            <area shape="poly" state="DE" full="Delaware" alt="Delaware" coords="598,172,599,175,600,178,600,180,601,183,602,185,603,186,604,188,607,189,610,188" href="properties-Delaware" />
            <area shape="poly" state="WV" full="West Virginia" alt="West Virginia" coords="517,208,520,205,521,203,521,199,523,198,525,197,525,193,530,190,533,188,535,185,536,178,538,183,541,183,545,182,549,182,550,189,553,188,555,187,559,184,562,182,567,179,570,181,565,185,563,188,561,191,557,194,556,198,552,198,550,201,550,204,548,207,546,211,544,214,542,217,539,219,534,222,529,223,525,221,523,219,517,211" href="properties-West_Virginia" />
            <area shape="poly" state="KY" full="Kentucky" alt="Kentucky" coords="434,245,435,241,436,240,436,238,439,237,441,238,443,237,444,235,444,234,446,231,448,230,448,228,449,224,452,225,456,224,459,224,462,222,465,222,467,220,470,221,473,221,475,217,478,214,480,210,482,207,485,205,488,200,491,202,494,204,498,206,502,206,508,206,511,206,513,208,514,212,516,214,518,217,521,220,518,224,515,228,512,231,508,234,495,236,490,237,484,237,477,238,473,238,467,238" href="properties-Kentucky" />
            <area shape="poly" state="RI" full="Rhode Island" alt="Rhode Island" coords="637,124,640,123,641,125,643,128,643,130,641,133,638,131,637,127" href="properties-Rhode_Island" />
            <area shape="poly" state="MA" full="Massachusets" alt="Massachusets" coords="616,117,621,116,626,115,629,114,633,113,637,112,641,111,643,109,646,111,644,114,644,116,647,118,649,121,651,122,659,124,655,126,651,126,648,127,643,124,640,121,637,121,633,123,628,124,624,125,617,126" href="properties-Massachusets" />
            <area shape="poly" state="VT" full="Vermont" alt="Vermont" coords="608,81,625,77,626,83,623,87,623,91,621,97,622,103,623,113,616,115,615,105,613,102,610,97" href="properties-Vermont" />
            <area shape="poly" state="NH" full="New Hampshire" alt="New Hampshire" coords="625,112,624,106,625,99,625,90,629,84,629,81,628,73,630,72,633,76,634,83,636,90,637,96,639,100,642,104,640,108,636,110,628,113" href="properties-New_Hampshire" />
            <area shape="poly" state="ME" full="Maine" alt="Maine" coords="643,101,640,95,638,89,636,82,635,77,632,71,636,69,637,65,638,63,637,41,638,37,639,31,642,31,644,34,648,34,651,32,653,30,656,31,659,33,660,36,662,41,663,45,664,50,665,52,668,55,670,58,673,59,676,61,677,64,678,66,674,69,668,75,665,76,661,74,658,75,658,80,659,82,656,84,654,86,651,88,648,89,648,92,647,95,645,100" href="properties-Maine" />
            <area shape="poly" state="AR" full="Arkansas" alt="Arkansas" coords="372,304,409,303,409,300,407,295,407,293,410,291,410,288,412,286,413,284,415,280,416,276,417,275,419,272,421,270,421,265,421,262,423,259,425,256,417,256,417,255,419,252,420,249,364,249,365,289,366,294,370,295,372,297" href="properties-Arkansas" />
            <area shape="poly" state="MS" full="Mississippi" alt="Mississippi" coords="406,337,407,333,408,330,409,324,415,318,415,315,412,313,411,300,410,293,412,288,416,284,418,278,421,273,448,271,448,320,448,325,449,330,448,334,448,339,449,343,449,345,441,345,439,346,435,347,431,343,433,338,431,336" href="properties-Mississippi" />
            <area shape="poly" state="NM" full="New Mexico" alt="New Mexico" coords="174,312,172,319,164,317,177,228,252,237,244,315,175,311" href="properties-New_Mexico" />
            <area shape="poly" state="ID" full="Idaho" alt="Idaho" coords="110,57,120,18,128,20,125,34,127,39,127,42,127,45,131,50,135,57,139,61,136,67,135,72,133,76,137,80,141,78,142,83,143,87,143,91,146,93,147,98,150,100,153,99,155,100,159,100,163,101,166,101,168,99,170,101,163,140,94,126,103,93,101,88,105,82,110,77,114,72,113,68,110,63" href="properties-Idaho" />
            <area shape="poly" state="ND" full="North Dakota" alt="North Dakota" coords="256,87,260,41,331,45,332,58,332,61,335,65,335,76,335,82,336,85,338,87,338,92" href="properties-North_Dakota" />
            <area class="active" shape="poly" state="IA" full="Iowa" alt="Iowa" coords="349,179,348,175,347,173,347,169,346,166,345,163,345,160,344,157,341,151,339,145,342,140,340,135,401,134,402,138,403,142,404,145,406,147,407,149,410,151,412,153,414,157,413,160,412,164,404,165,403,169,405,172,404,175,403,178,400,181,398,179" href="properties-Iowa" />
            <area shape="poly" state="NE" full="Nebraska" alt="Nebraska" coords="351,189,349,186,348,184,346,179,345,175,344,170,343,164,342,159,340,155,339,152,336,148,333,146,328,144,322,146,315,142,306,142,294,141,281,140,270,139,259,138,251,138,248,168,271,170,271,186" href="properties-Nebraska" />
            <area shape="poly" state="SD" full="South Dakota" alt="South Dakota" coords="337,145,338,143,339,140,337,137,337,132,339,132,339,104,334,99,337,95,305,92,291,92,255,90,251,135,316,139,322,143" href="properties-South_Dakota" />
            <area shape="poly" state="CO" full="Colorado" alt="Colorado" coords="264,235,269,173,186,163,177,226" href="properties-Colorado" />
            <area shape="poly" state="UT" full="Utah" alt="Utah" coords="113,214,129,137,162,143,160,158,184,163,174,226" href="properties-Utah" />
            <area shape="poly" state="MT" full="Montana" alt="Montana" coords="131,20,127,35,130,39,130,45,133,47,134,52,136,55,138,57,141,60,140,65,138,73,136,76,139,76,141,75,143,77,144,83,146,86,145,91,149,93,150,97,153,97,156,98,158,97,162,98,166,96,170,97,172,92,252,102,257,41" href="properties-Montana" />
            <area shape="poly" state="WY" full="Wyoming" alt="Wyoming" coords="163,157,173,95,251,105,246,168" href="properties-Wyoming" />
          </map>
        </div>
      </div>
</asp:Content>
