﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="careers.aspx.vb" Inherits="careers" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/OneColumn.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content careers">
      <div class="tabs-content transparent-black-box no-top-border">
            <div id="overview">
            <h1 class="headline cms cmsType_TextSingle cmsName_Careers_Page_Heading">Careers</h1>
            <ul class="tabs-holder">
             <li>
                <a rel="overview" href="careers">overview</a><ins> | </ins>
             </li>
             <li>
                <a rel="application-form" href="careers-apply">application form</a><ins></ins>
             </li>
            </ul>
                <div class="text cms cmsType_Rich cmsName_Careers_Page_Content">
                    <p class="less-bottom-margin">Because our employees and their well-being are important to us,
                        <br/>we offer a full complement of benefits including:</p>
                    <ul>
                        <li>– Medical, Dental, Vision, Life and Short Term Disability Insurance</li>
                        <li>– 401(K)</li>
                        <li>– Education Reimbursement</li>
                        <li>– Apartment Allowance Select a state to begin search.</li>
                    </ul>
                </div>

                <!-- 
                <h3 class="white-heading">Search available Opportunities:</h3>
                <div class="selectOption">
                 <p class="selectOptions"><a>All States</a></p>
                 <p class="optionList"></p>
                </div>
                <div class="c"></div>

                <h2 class="careers-heading">
                    <strong class="date">Posted</strong><strong class="location">City, State</strong><strong class="position">Job Title</strong>
                </h2> 

                <div class="accordion careers-accordion">
                 <div class="Florida">
                    <h3><strong class="date">1/16/2013</strong><strong class="location">St. Petersburg, Florida</strong><strong class="position">Marketing Associate</strong><ins>&nbsp;</ins></h3>
                    <div class="billow">
                        <div class="text">
                            <p>Richard L. Kadish is the Chief Executive Officer and President of CAPREIT, Inc. Mr. Kadish oversees all business affairs of CAPREIT and directs the Company's growth through acquisitions, development and portfolio management. Over the last nine years,
                                CAPREIT and its affiliates have owned and/or managed in excess of $1.6 billion of multifamily properties. Prior to joining CAPREIT in 1993, Mr. Kadish was Group Executive President for CRI, Inc. for 16 years, where he was principally responsible for the
                                acquisition of all multifamily residential and golf properties for the company. Prior to 1978, he served as a Deputy Attorney General of the State of New Jersey, where he was counsel to Department of Community Affairs and later was appointed as the Deputy
                                Executive Director of the New Jersey Housing Finance Agency (now New Jersey Housing and Mortgage Finance Agency). Mr. Kadish is a member of the American Bar Association and the New Jersey Bar Association and is a director of the National Multi Housing
                                Council. He holds Juris Doctorate and Master of Arts degrees from Rutgers University and a Bachelor of Arts degree from the University of Pennsylvania.</p>
                        </div>
                    </div>
                 </div>
                 <div class="California">
                    <h3><strong class="date">1/16/2013</strong><strong class="location">St. Petersburg, California</strong><strong class="position">Marketing Associate</strong><ins>&nbsp;</ins></h3>
                    <div class="billow">
                        <div class="text">
                            <p>Richard L. Kadish is the Chief Executive Officer and President of CAPREIT, Inc. Mr. Kadish oversees all business affairs of CAPREIT and directs the Company's growth through acquisitions, development and portfolio management. Over the last nine years,
                                CAPREIT and its affiliates have owned and/or managed in excess of $1.6 billion of multifamily properties. Prior to joining CAPREIT in 1993, Mr. Kadish was Group Executive President for CRI, Inc. for 16 years, where he was principally responsible for the
                                acquisition of all multifamily residential and golf properties for the company. Prior to 1978, he served as a Deputy Attorney General of the State of New Jersey, where he was counsel to Department of Community Affairs and later was appointed as the Deputy
                                Executive Director of the New Jersey Housing Finance Agency (now New Jersey Housing and Mortgage Finance Agency). Mr. Kadish is a member of the American Bar Association and the New Jersey Bar Association and is a director of the National Multi Housing
                                Council. He holds Juris Doctorate and Master of Arts degrees from Rutgers University and a Bachelor of Arts degree from the University of Pennsylvania.</p>
                        </div>
                    </div>
                 </div>
                 <div class="Alabama">
                    <h3><strong class="date">1/16/2013</strong><strong class="location">St. Petersburg, Alabama</strong><strong class="position">Marketing Associate</strong><ins>&nbsp;</ins></h3>
                    <div class="billow">
                        <div class="text">
                            <p>Richard L. Kadish is the Chief Executive Officer and President of CAPREIT, Inc. Mr. Kadish oversees all business affairs of CAPREIT and directs the Company's growth through acquisitions, development and portfolio management. Over the last nine years,
                                CAPREIT and its affiliates have owned and/or managed in excess of $1.6 billion of multifamily properties. Prior to joining CAPREIT in 1993, Mr. Kadish was Group Executive President for CRI, Inc. for 16 years, where he was principally responsible for the
                                acquisition of all multifamily residential and golf properties for the company. Prior to 1978, he served as a Deputy Attorney General of the State of New Jersey, where he was counsel to Department of Community Affairs and later was appointed as the Deputy
                                Executive Director of the New Jersey Housing Finance Agency (now New Jersey Housing and Mortgage Finance Agency). Mr. Kadish is a member of the American Bar Association and the New Jersey Bar Association and is a director of the National Multi Housing
                                Council. He holds Juris Doctorate and Master of Arts degrees from Rutgers University and a Bachelor of Arts degree from the University of Pennsylvania.</p>
                        </div>
                    </div>
                 </div>
                 <div class="Florida">
                    <h3><strong class="date">1/16/2013</strong><strong class="location">St. Petersburg, Florida</strong><strong class="position">Marketing Associate</strong><ins>&nbsp;</ins></h3>
                    <div class="billow">
                        <div class="text">
                            <p>Richard L. Kadish is the Chief Executive Officer and President of CAPREIT, Inc. Mr. Kadish oversees all business affairs of CAPREIT and directs the Company's growth through acquisitions, development and portfolio management. Over the last nine years,
                                CAPREIT and its affiliates have owned and/or managed in excess of $1.6 billion of multifamily properties. Prior to joining CAPREIT in 1993, Mr. Kadish was Group Executive President for CRI, Inc. for 16 years, where he was principally responsible for the
                                acquisition of all multifamily residential and golf properties for the company. Prior to 1978, he served as a Deputy Attorney General of the State of New Jersey, where he was counsel to Department of Community Affairs and later was appointed as the Deputy
                                Executive Director of the New Jersey Housing Finance Agency (now New Jersey Housing and Mortgage Finance Agency). Mr. Kadish is a member of the American Bar Association and the New Jersey Bar Association and is a director of the National Multi Housing
                                Council. He holds Juris Doctorate and Master of Arts degrees from Rutgers University and a Bachelor of Arts degree from the University of Pennsylvania.</p>
                        </div>
                    </div>
                 </div>
                 <div class="New_York">
                    <h3><strong class="date">1/16/2013</strong><strong class="location">St. Petersburg, New York</strong><strong class="position">Marketing Associate</strong><ins>&nbsp;</ins></h3>
                    <div class="billow">
                        <div class="text">
                            <p>Richard L. Kadish is the Chief Executive Officer and President of CAPREIT, Inc. Mr. Kadish oversees all business affairs of CAPREIT and directs the Company's growth through acquisitions, development and portfolio management. Over the last nine years,
                                CAPREIT and its affiliates have owned and/or managed in excess of $1.6 billion of multifamily properties. Prior to joining CAPREIT in 1993, Mr. Kadish was Group Executive President for CRI, Inc. for 16 years, where he was principally responsible for the
                                acquisition of all multifamily residential and golf properties for the company. Prior to 1978, he served as a Deputy Attorney General of the State of New Jersey, where he was counsel to Department of Community Affairs and later was appointed as the Deputy
                                Executive Director of the New Jersey Housing Finance Agency (now New Jersey Housing and Mortgage Finance Agency). Mr. Kadish is a member of the American Bar Association and the New Jersey Bar Association and is a director of the National Multi Housing
                                Council. He holds Juris Doctorate and Master of Arts degrees from Rutgers University and a Bachelor of Arts degree from the University of Pennsylvania.</p>
                        </div>
                    </div>
                 </div>
                </div>
                --> 
            </div>
        </div>
    </div>
    <div class="sidebar careers">
       <a href="careers-apply" rel="application-form" class="note-link link">JOB<br/>APPLICATION<br />FORM</a>
    </div>
</asp:Content>

