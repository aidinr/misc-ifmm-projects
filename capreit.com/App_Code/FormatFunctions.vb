Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.VisualBasic

Public Class formatfunctions

    Public Shared Function AutoFormatText(ByVal input As String) As String

        input = HttpUtility.HtmlEncode(input)
        input = RegularExpressions.Regex.Replace(input, "(^)- ", "&#8226; ")
        input = RegularExpressions.Regex.Replace(input, "(\n)- ", VbNewLine + "&#8226; ")
        input = RegularExpressions.Regex.Replace(input, "(^|\n)(?=.{1,150}\n)([A-Z][A-Za-z]*(?:(?: [\&\;a-zA-Z0-9]+)* [A-Z][A-Za-z\?\!]*)?)(\r\n\r\n)", "$1<strong>$2</strong>$3")
        input = RegularExpressions.Regex.Replace(input, "(https?://\S+[^\s@,.""']+)", "<a href=""$1"">$1</a>")
        input = RegularExpressions.Regex.Replace(input, "\w[\w\.]*\w?@[\w\.]+\w", "<a href=""mailto:$0"">$0</a>")
        'input = System.Text.RegularExpressions.Regex.Replace(input, "^([\w\d ]*:\s)", "<strong>$1</strong>", System.Text.RegularExpressions.RegexOptions.Multiline)
        input = RegularExpressions.Regex.Replace(input, "(?<!\S)(\www\.\S+[^\s@,.""']+)", "<a href=""http://$1"">$1</a>")
        input = RegularExpressions.Regex.Replace(input, "  ", " &nbsp;")
        input = RegularExpressions.Regex.Replace(input, VbNewLine, VbNewLine + "<br />")
        input = RegularExpressions.Regex.Replace(input, "-- ", "&mdash; ")
        input = RegularExpressions.Regex.Replace(input, "(?<!\S)\((@)(\S+[^\s\)]+)\)", "(<a href=""http://twitter.com/$2"">$1$2</a>)")
        input = System.Text.RegularExpressions.Regex.Replace(input, "\(([^)]+)\)(?m:\r$|())", "<em>($1)</em>")
        input = RegularExpressions.Regex.Replace(input, "(?<=&)amp;(?=#?\w+;)", String.Empty)
        input = RegularExpressions.Regex.Replace(input, "\{\[([^\]]*)\]\}(.*?)\{\[/\]\}", "<ins class=""lang lng$1"">$2</ins>")
        Return input
    End Function

    Public Shared Function AutoFormatTextList(ByVal input As String) As String
        input = RegularExpressions.Regex.Replace(input, vbNewLine, vbNewLine + "")
        Return input
    End Function

End Class