Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.VisualBasic

Public Class formatfunctionsPRE

    Public Shared Function AutoFormatTextPRE(ByVal input As String) As String
        input = HttpUtility.HtmlEncode(input)
        input = RegularExpressions.Regex.Replace(input, "(https?://\S+[^\s@,.""']+)", "<a href=""$1"">$1</a>")
        input = RegularExpressions.Regex.Replace(input, "\w[\w\.]*\w?@[\w\.]+\w", "<a href=""mailto:$0"">$0</a>")
        input = RegularExpressions.Regex.Replace(input, "(?<!\S)(\www\.\S+[^\s@,.""']+)", "<a href=""http://$1"">$1</a>")
        input = RegularExpressions.Regex.Replace(input, "-- ", "&mdash; ")
        input = RegularExpressions.Regex.Replace(input, "(?<!\S)\((@)(\S+[^\s\)]+)\)", "(<a href=""http://twitter.com/$2"">$1$2</a>)")
        input = RegularExpressions.Regex.Replace(input, "(?<=&)amp;(?=#?\w+;)", String.Empty)
        Return input
    End Function

End Class