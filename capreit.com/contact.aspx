﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="contact.aspx.vb" Inherits="contact" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/OneColumn.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content contact">
        <div class="tabs-content transparent-black-box no-top-border">
           <h1 class="headline cms cmsType_TextSingle cmsName_Contact_Page_Heading">Contact Us</h1>
           <div class="text contacts-page">
               <h2 class="white-heading contact-heading">corporate headquarters</h2>
               <div class="cms cmsType_TextMulti cmsName_Contact_Page_Address">
                 <p>11200 Rockville Pike, Suite 100
                   <br/>Rockville, MD 20852
                   <br/>301.231.8700</p>
               </div>
                <h2 class="white-heading">How can we direct your inquiry?</h2>
                <div class="selectOption">
                 <p class="selectOptions"><a>Please Select</a></p>
                 <p class="optionList">
                 <a class="link" href="investors-contact">Investment</a>
                 <a class="link" href="contact-rental">Rental Inquiry</a>
                 <a class="link" href="services-request">Property Service Request</a>
                 <a class="link" href="careers-apply">Careers</a>
                 </p>
                </div>
                <div class="c"></div>

              </div>
          </div>
       <div class="sidebar">
           <div class="transparent-blue-box social-contacts">
               <a href="<%=CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Twitter")%>" class="twi cms cmsFill_Static cmsType_TextSingle cmsName_Twitter" target="_blank">Follow us on Twitter</a>
               <a href="<%=CMSFunctions.getContentText(Server.MapPath("~") & "cms\data\", "Facebook")%>" class="fb cms cmsFill_Static cmsType_TextSingle cmsName_Facebook" target="_blank">Like us on Facebook</a>
           </div>
        </div>
    </div>
</asp:Content>

