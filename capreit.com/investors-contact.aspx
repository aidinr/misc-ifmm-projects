﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="investors-contact.aspx.vb" Inherits="investors_contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content">
      <div class="tabs-content transparent-black-box no-top-border">
        <h1 class="headline">Investment Inquiry</h1>
        <ul class="tabs-holder">
          <li>
              <a href="investors">overview</a><ins> | </ins>
          </li>
          <li>
              <a href="investors-contact">Investment Inquiry</a> <!-- <ins> | </ins> -->
          </li>
          <li>
              <!-- <a target="_blank" href="#">Investor Login</a> -->
          </li>
        </ul>
        <div class="text">
            <div id="thanks" class="cms cmsType_Rich cmsName_Investment_Inquiry_Thanks">
             <h3><%= CMSFunctions.getContentRich(Server.MapPath("~") & "cms\data\", "Investment_Inquiry_Thanks")%></h3>
            </div>
            <form method="post" action="submit/" name="submit-investors" class="form contact-form">
                <ol>
                    <li class="two-inputs">
                        <p><label>Name *</label></p>
                        <p><input type="text" value="First Name" name="First_Name" id="name" title="First Name" class="placeholder mr10 required" /></p>
                        <p><input type="text" value="Last Name" name="Last_Name" id="family" title="Last Name" class="placeholder required" /></p>
                    </li>
                    <li>
                        <p><label for="email">e-mail address *</label></p>
                        <p><input type="text" value="" name="Email" class="email required" id="email" /></p>
                    </li>
                    <li>
                        <p><label for="message">message *</label></p>
                        <p><textarea name="Message" id="message" rows="5" cols="4" class="required"></textarea></p>
                    </li>
                    <li>
                        <span class="sendTo cms cmsType_TextSingle cmsName_Investment_Inquiry_Recipient"></span>
                        <input type="submit" value="Submit" name="submit" />
                    </li>
                </ol>
            </form>

        </div>
      </div>
    </div>
    <!--  
    <div class="sidebar cms cmsType_TextSingle cmsName_Investment_Investor_Login">
        <a href="#" class="investor-login-link" target="_blank">investor
            <br/>login</a>
    </div>
    -->
</asp:Content>

