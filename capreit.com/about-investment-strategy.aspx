﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="about-investment-strategy.aspx.vb" Inherits="about_investment_strategy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content">
        <div class="tabs-content transparent-black-box no-top-border">
            <div id="investment-strategy">
                <h1 class="headline cms cmsType_TextSingle cmsName_Investment_Strategy_Page_Heading">Investment Strategy</h1>

                <ul class="tabs-holder">
                    <li>
                        <a href="about">overview</a><ins> | </ins>
                    </li>
                    <li>
                        <a href="about-investment-strategy">investment strategy</a><ins> | </ins>
                    </li>
                    <li>
                        <a href="about-leadership">leadership</a><ins> | </ins>
                    </li>
                    <li>
                        <a href="about-company-history">company history</a>
                    </li>
                </ul>

                <div class="text">
                    <p class="cms cmsType_TextMulti cmsName_Investment_Strategy_Page_Content">CAPREIT strives to identify well-located apartment communities within metropolitan areas that can benefit from substantial improvements in management. Typically, properties targeted for acquisition are constructed after 1990. During the acquisition process,
                    CAPREIT develops a detailed plan for both exterior/common area improvements as well as in-unit improvements, and the company’s extensive experience in the field allows its staff to develop estimates of projected incremental increase in rent. Depending
                    on the market, value-add opportunities can be acquired for 5.5%-6.5% cap rates on in-place Net Operating Income and purchased for approximately 60%-70% of replacement cost. CAPREIT has a lengthy and proven track record of delivering exceptional results and returns to its investment partners.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar about">
        <a class="next-arrow ajax-next-arrow" href="about-leadership"> <ins><var>&nbsp;</var></ins><span><strong>leadership</strong></span></a>
        <div class="switchable-quotes">
            <div class="transparent-blue-box quote-box"> <em class="cms cmsType_TextMulti cmsName_Investment_Strategy_Quote">Success is never an accident. It is always the result of high intention, sincere effort, intelligent direction and skillful execution.</em>
                <var class="triangle-figure">&nbsp;</var>
            </div>
        </div>
    </div>
</asp:Content>

