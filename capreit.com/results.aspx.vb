﻿Imports System.Data

Partial Class results
    Inherits System.Web.UI.Page
    Public city As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            city = (Request("STATE") & "").ToString.Split("-")(1).Replace("_", " ")
        Catch ex As Exception

        End Try

        Dim dt As DataTable = mmfunctions.ResultsDataTable(Request("q") & "", "", "", "", (Request("STATE") & "").ToString.Split("-")(0).Trim, city.Trim)
        Properties_result.DataSource = dt
        Properties_result.DataBind()
        Dim dt_city As DataTable = mmfunctions.UniqueCityDataTable(Request("q") & "", "", "", "", (Request("STATE") & "").ToString.Split("-")(0).Trim)
        Cities_List.DataSource = dt_city
        Cities_List.DataBind()

    End Sub
End Class
