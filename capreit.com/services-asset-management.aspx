﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="services-asset-management.aspx.vb" Inherits="services_asset_management" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content">
        <div class="tabs-content transparent-black-box no-top-border">            
            <div id="asset-management">
                <h1 class="headline cms cmsType_TextSingle cmsName_Asset_Management_Page_Heading">Asset Management</h1>
                <ul class="tabs-holder">
                    <li>
                        <a href="services">overview</a><ins> | </ins>
                    </li>
                    <li>
                        <a href="services-resident">resident services</a><ins> | </ins>
                    </li>
                    <li>
                        <a href="services-property-management">property management</a><ins> | </ins>
                    </li>
                    <li>
                        <a href="services-asset-management">asset management</a>
                    </li>
                </ul>
                <div class="text">
                  <p class="cms cmsType_TextMulti cmsName_Asset_Management_Page_Content">
                    CAPREIT is a company of real estate experts, each of whom has years of experience in the multifamily sector. Members of CAPREIT’s senior management team have worked together for many years in professionaly managing and positioning multifamily propertiesto achieve optimal value.
                    <br /><br />
                    Management’s quality and depth are unsurpassed in the industry. Dick Kadish, a leader in the multifamily housing industry for over 30 years, leads CAPREIT. The company’s senior managers each average more than 20 years of experience in the multifamily industry. Our property management team is supported by more than 400 real estate professionals who direct day-to-day, on-site management of CAPREIT’s diversified communities.
                  </p>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar services">
        <a class="next-arrow ajax-next-arrow" href="services"> <ins><var>&nbsp;</var></ins><span><strong>overview</strong></span></a>
        <div class="switchable-quotes">
            <div class="tabBox">
                <a href="services-request" class="note-link link">SERVICE REQUEST
                    <br/>FORM</a>
            </div>
        </div>
    </div>
</asp:Content>

