﻿Imports System.Data

Partial Class property_inner
    Inherits System.Web.UI.Page
    Public dt As DataTable
    Public dt_asset As DataTable
    Public asset_id As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dt = mmfunctions.PropertyDataTable(Request("id") & "")
        dt_asset = mmfunctions.PropertyAssetListDataTable(Request("id") & "")
        asset_list.DataSource = dt_asset
        asset_list.DataBind()
    End Sub
End Class
