﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="contact-rental.aspx.vb" Inherits="contact_rental" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content">
      <div class="tabs-content transparent-black-box no-top-border">
        <h1 class="headline">Rental Inquiry</h1>
        <div class="text">
            <div id="thanks" class="cms cmsType_Rich cmsName_Rental_Inquiry_Thanks">
             <h3><%= CMSFunctions.getContentRich(Server.MapPath("~") & "cms\data\", "Rental_Inquiry_Thanks")%></h3>
            </div>
            <form method="post" action="submit/" name="submit-rental" class="form contact-form">
                <ol>
                    <li class="two-inputs">
                        <label for="name">Name *</label>
                        <input type="text" value="First Name" name="First_Name" id="First_Name" title="First Name" class="placeholder mr10 required" />
                        <input type="text" value="Last Name" name="Last_Name" id="Last_Name" title="Last Name" class="placeholder required" />
                    </li>
                    <li>
                        <label for="email">e-mail address *</label>
                        <input type="text" value="" name="Email" class="email required" id="email" />
                    </li>
                    <li>
                        <label for="message">message *</label>
                        <textarea name="Message" id="Message" rows="5" cols="4" class="required"></textarea>
                    </li>
                    <li><span class="sendTo cms cmsType_TextSingle cmsName_Rental_Inquiry_Email_Recipient"></span>
                        <input type="submit" value="Submit" name="submit" />
                    </li>
                </ol>
            </form>
        </div>
      </div>
    </div>
</asp:Content>

