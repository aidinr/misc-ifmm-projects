﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="investors.aspx.vb" Inherits="investors" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/OneColumn.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content">
      <div class="tabs-content transparent-black-box no-top-border">
        <h1 class="headline cms cmsType_TextSingle cmsName_Investors_Page_Heading">Investors</h1>
        <ul class="tabs-holder">
          <li>
              <a href="investors">overview</a><ins> | </ins>
          </li>
          <li>
              <a href="investors-contact">Investment Inquiry</a> <!-- <ins> | </ins> -->
          </li>
          <li>
              <!-- <a target="_blank" href="#">Investor Login</a> -->
          </li>
        </ul>
        <div class="text">
            <p class="cms cmsType_TextMulti cmsName_Investors_Page_Content">CAPREIT has a competitive advantage over most potential purchasers of multifamily apartment properties. The company’s acquisition team has a strong record of acquiring quality residential real estate and enjoys a deserved reputation for integrity and
                candor throughout the industry. With a thorough knowledge of local markets and a demonstrated entrepreneurial drive, CAPREIT’s acquisition team moves quickly to identify opportunities, conduct prudent due diligence and efficiently close transactions designed
                to enhance the company’s long-term value.
                <br /><br />
                CAPREIT’s portfolio management is focused on optimizing performance through pro-active, service-oriented property management. In striving to maximize net operating income and asset value, CAPREIT’s management team emphasizes four chief priorities: resident
                satisfaction and retention, asset preservation and maintenance, consistently high occupancies, and cost containment.
                <br /><br />
                CAPREIT is not resting on its laurels. The company is poised to continue growing and improving the performance of its core portfolio. The steady increase in household formations, the overall moderate levels of new multifamily apartment construction,
                and CAPREIT’s dedication to resident satisfaction have all contributed to the high occupancy rates in the CAPREIT portfolio. Economic expansion creates the conditions for increased demand for high quality rental housing, allowing for rental increases
                and enhanced portfolio value. CAPREIT has the housing expertise to effectively capitalize on this environment, the critical mass to operate efficiently, the dedication to perform, and the resources to continue to grow and remain strong and prosperous.
                More than ever before, CAPREIT is favorably positioned to continue building its leadership role in the multifamily housing industry well into the new millennium.
                <br /><br />
                We maintain long term successful investor and borrower relationships with the following groups:
            </p>
        </div>
        <div id="scroller" class="investors-scroller">
            <ul class="cmsGroup cmsName_Investor_Slides">
                <!--
                    <a><img src="assets/images/temp/investor.png" class="cms cmsType_Image cmsName_Image" alt="" /></a>
                -->

                        <%
    For i As Integer = 1 To 20 - 1
                                If IO.Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Investor_Slides_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
   %>
                   <li class="cmsGroupItem">
                   <a> <img class="cms cmsType_Image cmsName_Investor_Slides_Item_<%=i%>_Image" align="left" src="cms/data/Sized/liquid/112x/65/ffffff/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "Investor_Slides_Item_" & i &"_Image")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "Investor_Slides_Item_" & i &"_Image")%>" /></a>
                </li>

 <%End If %>
 <%Next%>  

              
            </ul>
        </div>
      </div>
    </div>
    <!-- <div class="sidebar cms cmsFill_Static cmsType_TextSingle cmsName_Investment_Investor_Login">
        <a href="#" class="investor-login-link" target="_blank"> 
        investor<br/>login
       </a>
    </div> -->
</asp:Content>

