﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="services-request.aspx.vb" Inherits="services_request" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/OneColumn.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content transparent-black-box no-top-border serviceRequest">  
        <h1 class="headline">Service Request Form</h1>
            <div id="thanks" class="cms cmsType_Rich cmsName_Service_Request_Thanks">
             <h3><%= CMSFunctions.getContentRich(Server.MapPath("~") & "cms\data\", "Service_Request_Thanks")%></h3>
            </div>
        <form method="post" action="submit/" name="submit-service" class="form contact-form">
            <ol>
                <li class="two-inputs">
                    <label for="name">Name *</label>
                    <input type="text" value="First Name" name="name" id="name" title="First Name" class="placeholder mr10 required"/>
                    <input type="text" value="Last Name" name="family" id="family" title="Last Name" class="placeholder required"/>
                </li>
                <li>
                    <label>property *</label>
                    <input type="hidden" id="inputProperty" name="Property" val="" class="required">
                <div class="selectOption">
                 <p class="selectOptions"><a>Please Select</a></p>
                 <p class="optionList">
                   <asp:Repeater ID="Properties_List" runat="server">
                     <ItemTemplate>
                      
                     <a class="link"><%# Eval("Name")%></a>
                 </ItemTemplate>
                 </asp:Repeater>
                 </p>
                </div>

                </li>                        
                <li>
                    <label for="appartment">Apartment/Suite number *</label>
                    <input type="text" value="" name="appartment" id="appartment" class="required"/>
                </li>                        
                <li>
                    <label for="email">e-mail address *</label>
                    <input type="text" value="" name="email" id="email" class="required email"/>
                </li>  
                <li class="two-inputs">
                    <label for="daytime_phone">phone numbers *</label>
                    <input type="text" value="Daytime Phone" name="daytime_phone" id="daytime_phone" title="Daytime Phone" class="placeholder mr10 required"/>
                    <input type="text" value="Evening Phone" name="evening_phone" id="evening_phone" title="Evening Phone" class="placeholder"/>
                </li>                        
                <li>
                    <label>Problem Categories *</label>
                    <div class="stylish-checkbox">
                            <span><input type="checkbox" value="Air Conditioning" name="">Air Conditioning</span>
                            <span><input type="checkbox" value="Extermination" name="">Extermination</span>
                            <span><input type="checkbox" value="Electrical" name="">Electrical</span>
                            <span><input type="checkbox" value="Plumbing" name="">Plumbing</span>
                            <span><input type="checkbox" value="Water Leak" name="">Water Leak</span>
                            <span><input type="checkbox" value="Appliances" name="">Appliances</span>
                            <span><input type="checkbox" value="Other" name="">Other</span>

                    </div>
                    <div class="c"></div>
                </li>                           
                <li>
                    <label for="message">Detailed Description</label>
                    <textarea name="message" id="message" rows="5" cols="4"></textarea>
                </li>                          
                <li>
                    <span class="sendTo cms cmsType_TextSingle cmsName_Service_Request_Recipient"></span>
                    <input type="submit" value="Submit" name="submit"/>
                </li>
            </ol>
        </form>                
    </div>

    <div class="sidebar request-quote">
        <div class="transparent-blue-box quote-box tal text">
          <p class="cms cmsType_TextMulti cmsName_Service_Request_Form_Quote">
            <%= CMSFunctions.getContentMulti(Server.MapPath("~") & "cms\data\", "Service_Request_Form_Quote")%>
          </p>
        </div>  
    </div> 
</asp:Content>

