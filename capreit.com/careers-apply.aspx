﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="careers-apply.aspx.vb" Inherits="careers_apply" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content">
      <div class="tabs-content transparent-black-box no-top-border">
        <h1 class="headline">Job Inquiry</h1>
            <ul class="tabs-holder">
             <li>
                <a rel="overview" href="careers">overview</a><ins> | </ins>
             </li>
             <li>
                <a rel="application-form" href="careers-apply">application form</a><ins></ins>
             </li>
            </ul>
        <div class="text">
            <div id="thanks" class="cms cmsType_Rich cmsName_Job_Inquiry_Thanks">
             <h3><%= CMSFunctions.getContentRich(Server.MapPath("~") & "cms\data\", "Job_Inquiry_Thanks")%></h3>
            </div>
            <form method="post" action="submit/" name="submit-careers" class="form contact-form">
                <ol>
                    <li class="two-inputs">
                        <label for="name">Name *</label>
                        <input type="text" value="First Name" name="name" id="name" title="First Name" class="placeholder mr10 required" />
                        <input type="text" value="Last Name" name="family" id="family" title="Last Name" class="placeholder required" />
                    </li>
                    <li>
                        <label for="email">e-mail address *</label>
                        <input type="text" value="" name="email" class="email required" id="email" />
                    </li>
                    <li>
                        <label for="message">message *</label>
                        <textarea name="message" id="message" rows="5" cols="4" class="required"></textarea>
                    </li>
                    <li>
                        <span class="sendTo cms cmsType_TextSingle cmsName_Job_Inquiry_Recipient"></span>
                        <input type="submit" value="Submit" name="submit" />
                    </li>
                </ol>
            </form>
        </div>
      </div>
    </div>
    <div class="sidebar careers">
       <a href="careers-apply" rel="application-form" class="note-link link">JOB<br/>APPLICATION<br />FORM</a>
    </div>
</asp:Content>

