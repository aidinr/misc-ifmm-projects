﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="services.aspx.vb" Inherits="services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content">
        <div class="tabs-content transparent-black-box no-top-border">
            <div id="overview">
                <h1 class="headline cms cmsType_TextSingle cmsName_Services_Page_Heading">Services</h1>
                <ul class="tabs-holder">
                    <li>
                        <a href="services" title="">overview</a><ins> | </ins>
                    </li>
                    <li>
                        <a href="services-resident" title="">resident services</a><ins> | </ins>
                    </li>
                    <li>
                        <a href="services-property-management" title="">property management</a><ins> | </ins>
                    </li>
                    <li>
                        <a href="services-asset-management" title="">asset management</a>
                    </li>
                </ul>
                <div class="text">
                    <p class="cms cmsType_TextMulti cmsName_Services_Page_Content"></p>
                </div>
            </div>            
        </div>
    </div>
    <div class="sidebar services">
        <a class="next-arrow ajax-next-arrow" href="services-resident"> <ins><var>&nbsp;</var></ins><span><strong>resident services</strong></span></a>
        <div class="switchable-quotes">
            <div class="tabBox">
                <a href="services-request" class="note-link link">SERVICE REQUEST <br/>FORM</a>
            </div>
        </div>
    </div>
</asp:Content>

