﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="news.aspx.vb" Inherits="news" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content news">
      <div class="tabs-content transparent-black-box no-top-border">
        <h1 class="headline"> News &amp; Press</h1>
        <ul class="tabs-holder news"><li></li></ul>
        <div>
            <div class="year" id="year-2015">
                <div class="accordion">
                    <h3><strong>January 21, 2015</strong><em>CAPREIT Appoints New Executive Leadership</em><ins>&nbsp;</ins></h3>
                    <div class="billow">
                        <div class="text">
                            <p>
                                Rockville, Md.&mdash;CAPREIT, a national real estate development and investment company, has announced the restructuring of its executive leadership team through the appointment of three associates.

                                Andrew S. Kadish will serve as president, Ernest L. Heymann has been appointed the company&rsquo;s chief investment officer and Jennifer K. Cassell will be taking on the role as chief administrative officer. Founder and CAPREIT&rsquo;s previous president, Dick Kadish, has transitioned to chairman of the board of directors for the company.
                                As president, Kadish will oversee day-to-day management and operations of the company&rsquo;s portfolio. Previously Kadish served as senior vice president taking the lead on acquisition, financing, and renovation of multifamily developments.

                                Mr. Heymann, senior vice president of the company since its inception in 1993, led the acquisition of dozens of multifamily residential communities. As chief investment officer, he will lead the company&rsquo;s growth in the marketplace.

                                A member of the company for the past eight years, Jennifer Cassell served previously as senior vice president. In her role, she was responsible for management, operations and legal affairs and will continue this in her new position.
                                <br><br>
                                <a target="_blank" href="http://www.multihousingnews.com/news/capreit-appoints-new-executive-leadership/1004113743.html">http://www.multihousingnews.com/news/capreit-appoints-new-executive-leadership/1004113743.html</a>
                            </p>
                        </div>
                        <p class="RightCol">
                            <img src="assets/images/temp/news.jpg" alt="" />
                        </p>
                    </div>
                    <h3><strong>January 20, 2015</strong><em>Brantley Pines complex sells for $27.2 million</em><ins>&nbsp;</ins></h3>
                    <div class="billow">
                        <div class="text">
                            <p>
                                A real estate trust recently bought the Madison Brantley Pines apartment complex in Fort Myers for $27.2 million &mdash; and as Southwest Florida&rsquo;s rents rose at the fastest pace in the state.

                                Philadelphia-based Equus Capital Partners Ltd., a major private-equity fund manager, sold the 296-unit, 95-percent occupied property to CAPREIT, based in Rockville, Md.

                                Equus bought the garden-style complex for $19.2 million in April 2004, a year before prices crested in the real estate boom that collapsed at the end of 2005.

                                The latest sale comes as rents took a huge jump for the Fort Myers/Naples metro area in the last quarter of 2014, according to Carrolton, Texas-based ALN Apartment News, which tracks data for large apartment complexes around the country.

                                Even though market occupancy dropped 1.9 percent to 94.3 percent, rents &ldquo;jumped a whopping average of 5 percent and are up 12.9 percent from this time last year,&rdquo; according to ALN&rsquo;s January report.
                                For the average unit in a large complex, rent rose from $892 to $1,004, more than anywhere in the state but Palm Beach&rsquo;s $1,314 (up 10.2 percent year over year).

                                That&rsquo;s more than any other market in Florida &ndash; also the highest in the 26 Sun Belt markets ALN tracks.

                                &ldquo;Will it last? What unlocks that is the insurance and mortgage financing,&rdquo; said David Cobb, regional director in the Naples-Fort Myers area for Houston-based housing data provider Metrostudy, which tracks pricing and new-home construction.

                                The rental market is tight in large part because apartments are being occupied by people who lost their homes to foreclosure and as a result don&rsquo;t have the credit to buy again, he said.

                                If mortgage financing requirements ease up, Cobb said, &ldquo;You&rsquo;re going to see some of those folks who are renting move out and buy a house.&rdquo;
                                But that won&rsquo;t happen overnight, he said, and the tight apartment market &ldquo;is probably going to continue through 2016.&rdquo;

                                Equus was represented in the Brantley Pines sale by Jamie May and Edward Yang of Naples-based JBM Realty Advisors.
                                <br><br>
                                <a target="_blank" href="http://www.news-press.com/story/money/2015/01/20/brantley-pines-complex-sells-million/22040353/">http://www.news-press.com/story/money/2015/01/20/brantley-pines-complex-sells-million/22040353/</a>
                            </p>
                        </div>
                        <p class="RightCol">
                            <img src="assets/images/temp/news.jpg" alt="" />
                        </p>
                    </div>
                </div>
            </div>

            <div class="year" id="year-2014">
                <div class="accordion">

                    <h3><strong>September 19, 2014</strong><em>CAPREIT acquires Chaska townhomes</em><ins>&nbsp;</ins></h3>
                    <div class="billow">
                        <div class="text">
                            <p>
                                CAPREIT, a Maryland-based real estate investment trust, has added a $7.65 million affordable housing complex in Chaska to its Minnesota portfolio.
                                <br><br>
                                <a target="_blank" href="http://finance-commerce.com/tag/carver-ridge-townhomes">http://finance-commerce.com/tag/carver-ridge-townhomes/</a>
                            </p>
                        </div>
                        <p class="RightCol">
                            <img src="assets/images/temp/news.jpg" alt="" />
                        </p>
                    </div>

                    <h3><strong>January 8, 2014</strong><em>CAPREIT Acquires Minneapolis Grand for $11.2M</em><ins>&nbsp;</ins></h3>
                    <div class="billow">
                        <div class="text">
                            <p>
                                CAPREIT, a Maryland-based property investment firm, purchased the Minneapolis Grand multifamily building at 2401 Chicago Ave. in Minneapolis, MN from Base Management for $11.15 million, or about $124,000 per unit. 

                                The 90-unit, 104,734-square-foot apartment community was built in 2004 on nine-tenths of an acre in the Phillips submarket of Hennepin County, one block south of Peavey Field. It is comprised of 46 one-bedroom and 44 two-bedroom units, with 18,000 square feet of ground-floor retail space. 

                                Ted Abramson and Keith Collins of CBRE represented the seller. The buyer handled the acquisition in-house. 
                                <br><br>
                                <a target="_blank" href="http://www.costar.com/News/Article/CAPREIT-Acquires-Minneapolis-Grand-for-$112M/155922">http://www.costar.com/News/Article/CAPREIT-Acquires-Minneapolis-Grand-for-$112M/155922</a>
                            </p>
                        </div>
                        <p class="RightCol">
                            <img src="assets/images/temp/GetThumbnail-1.jpg" alt="" />
                        </p>
                    </div>

                </div>
            </div>

            <div class="year" id="year-2013">
                <div class="accordion">

                    <h3><strong>October 8, 2013</strong><em>Woodbrook Apts Trade for $6.5M</em><ins>&nbsp;</ins></h3>
                    <div class="billow">
                        <div class="text">
                            <p>
                                CAPREIT acquired the 196-unit Woodbrook Apartments at 5302 Woodbrook Dr. in Indianapolis, IN for $6.45 million, or about $33,000 per unit, from Hamilton Point Investments. The buyer now owns three multifamily communities in the Indianapolis area, and six in the state of Indiana. 

                                The Marion County community had been purchased out of foreclosure less than two years before by Hamilton Point. In that time the seller has increased the building occupancy rate and tenancy. 

                                Woodbrook features 196 units located on Indianapolis' west side. Built in 1981, it consists of 88 one-bedroom, 96 two-bedroom, and 12 three-bedroom townhome-style units. The property is comprised of 12 two-story buildings with 23 common hallways and a free-standing laundry room. The clubhouse contains the leasing office and a second laundry facility. On-site amenities include a swimming pool, a tennis/sports court, a lake, and patios or balconies. 

                                George Tikijian and Hannah Ott with Tikijian Associates represented the seller. CAPREIT is owned and led by Dick Kadish, a nationally recognized leader in the multifamily housing sector with 35 years of experience. 
                                <br><br>
                                <a target="_blank" href="http://www.costar.com/News/Article/Woodbrook-Apts-Trade-for-$65M/153025">http://www.costar.com/News/Article/Woodbrook-Apts-Trade-for-$65M/153025</a>
                            </p>
                        </div>
                        <p class="RightCol">
                            <img src="assets/images/temp/GetThumbnail.jpg" alt="" />
                        </p>
                    </div>

                </div>
            </div>

            <div class="year" id="year-2012">
                <div class="accordion">
                    <h3><strong>December 24, 2012</strong><em>Investors grab two North Texas apartment projects in year-end sale</em><ins>&nbsp;</ins></h3>
                    <div class="billow">
                        <div class="text">
                            <p>
                                Two investors &ndash; one from Maryland and the other from Canada &ndash; have bought a pair of North Texas apartment communities.

                                Institutional Property Advisors brokered both apartment deals.
                                Capreit, a real estate investment trust based in Rockville, MD, bought the 204-unit Valley Trails apartments in Irving&rsquo;s Valley Ranch community. The property was developed in 1995 and is substantially leased.

                                Institutional Property Advisors executive director Will Balthrope brokered the sale along with Drew Kile and Spencer Hurst and Jeff Kunitz of Marcus &amp; Millichap Real Estate Investment Services.

                                Balthrope said that Capreit is a nationwide investor that &ldquo;is entering Texas for the first time with this D-FW apartment acquisition.&rdquo;

                                The second property that just sold is the Dry Creek Ranch apartments, located in the town of Northlake on Interstate 35W, north of Alliance Airport.

                                Dry Creek Ranch has 288 apartments and opened in 2007.
                                Calgary-based Western Securities Ltd bought the apartments from a partnership between Dallas-based SWBC Real Estate LLC and Birmingham-based Harbert Management Corp.

                                The apartments were 95 percent leased when sold.
                                &ldquo;The property went under contract just two weeks into marketing, and the buyer closed the transaction in just 45 days,&rdquo; Balthrope said. &ldquo;We are seeing strong demand for suburban assets in good locations near job centers, and both of these assets were highly sought after in a competitive bid process.&rdquo;
                                Balthrope and Kile brokered the Dry Creek Ranch sale with Western Securities&rsquo; Marc Footlik.
                                <br><br>
                                <a target="_blank" href="http://bizbeatblog.dallasnews.com/2012/12/investors-grab-two-north-texas-apartment-projects-in-year-end-sale.html/">http://bizbeatblog.dallasnews.com/2012/12/investors-grab-two-north-texas-apartment-projects-in-year-end-sale.html/</a>
                            </p>
                        </div>
                        <p class="RightCol">
                            <img src="assets/images/temp/news.jpg" alt="" />
                        </p>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <div class="sidebar news">
        <a class="next-arrow"> <ins><var>&nbsp;</var></ins><span><strong></strong></span></a>
    </div>
</asp:Content>

