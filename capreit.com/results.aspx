﻿<%@ Page Title="" Language="VB" Debug="true" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="results.aspx.vb" Inherits="results" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="content transparent-black-box no-top-border search-results-page">
                <h2 class="white-heading"><small>Apartment Communities for </small><%= IIf(Request("STATE") & "" = "", Request("q")&"", IIf((Request("City") &"").Trim = "", Request("STATE") & "", Request("State") & " - " & Request("City"))) %></h2>

                <div class="resultsFilter">
                  <div class="selectOption city">
                   <p class="selectOptions"><a>Filter by City</a></p>
                   <p class="optionList">
                    <asp:Repeater ID="Cities_List" runat="server">
                        <ItemTemplate>
                      
                        <a class="link" href='properties-<%# Eval("State").Trim%>-<%# Eval("City").Trim.Replace(" ", "_")%>'><%# Eval("City")%><em>, <%# Eval("State_ID")%></em></a>
                       
                        </ItemTemplate>
                    </asp:Repeater>
                   </p>
                  </div>

                <!--  <div class="selectOption community">
                   <p class="selectOptions"><a>Filter by Community</a></p>
                   <p class="optionList">
                    <a class="link" href="properties-community-Oak_Hills">Oak Hills</a>
                    <a class="link" href="properties-community-Rose_Trellis">Rose Trellis</a>
                    <a class="link" href="properties-community-Sunny_Lane">Sunny Lane</a>
                   </p>
                  </div> -->
                </div>
                <div class="c"></div>

            <ul class="result-list">
              <asp:Repeater ID="Properties_result" runat="server">
                <ItemTemplate>
                <li>
                    <a href="property-inner-<%# Eval("projectid")%>"><div class="img-wrapper"><img src="<%=Session("WSRetreiveAsset")%>type=project&id=<%# Eval("projectid")%>&width=199&height=199&crop=0" alt="" /></div> <strong><%# Eval("Shortname")%></strong> <span><%# Eval("Name")%></span> <span><%# Eval("City")%>, <%# Eval("State_ID")%></span></a>
                </li>
                </ItemTemplate>
              </asp:Repeater>
              
            </ul>
        </div>
        <div class="sidebar search-results-page-sidebar">
            <a class="next-arrow go-back-link link" href="properties"><ins><var>&nbsp;</var></ins></a>
        </div>
    </asp:Content>

