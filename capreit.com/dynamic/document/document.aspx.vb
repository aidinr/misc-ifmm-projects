Imports System
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Web.Configuration
Imports System.Web.UI

Partial Public Class document
    Inherits System.Web.UI.Page

    ' xCache=fresh
    ' xDisposition=view
    ' xID=234342324

    '/fresh/ (options: fresh=never cache, today=cache for 1 day, store=cache forever)
    '/view/ (view=Open in browser when possible, download=force download)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim url As String = Request.Url.ToString()
        Dim xCache As String = ""
        If Not Request.QueryString("xCache") Is Nothing Then
            xCache = Request.QueryString("xCache")
        End If
        Dim xDisposition As String = ""
        If Not Request.QueryString("xDisposition") Is Nothing Then
            xDisposition = Request.QueryString("xDisposition")
        End If
        Dim xType As String = ""
        If Not Request.QueryString("xType") Is Nothing Then
            xType = Request.QueryString("xType")
        End If
        Dim xID As String = ""
        If Not Request.QueryString("xID") Is Nothing Then
            xID = Request.QueryString("xID")
        End If
        Dim xExtension As String = ""
        If Not Request.QueryString("xExtension") Is Nothing Then
            xExtension = Request.QueryString("xExtension")
        End If
        Dim xName As String = ""
        If Not Request.QueryString("xName") Is Nothing Then
            xName = Request.QueryString("xName")
        End If
        'get the file from iDAM
        Dim temp As String = Session("WSRetreiveAsset").ToString.Trim & "type=newspdf&id=" & xID & "&size=0&cache=1"
        Dim filepath As String = Server.MapPath("~") & "dynamic\document\" & xType & "\"
        Dim fullfilename As String = filepath & xID & "." & xExtension
        'WebEventLog.WriteToEventLog("fullfilename - " & fullfilename)
        'WebEventLog.WriteToEventLog("webrequest - " & temp)
        'Response.write(temp)
        'Response.write(fullfilename)
        'Return
        Dim SourceStream As Stream
        If Not Directory.Exists(filepath) Then
            Try
                Directory.CreateDirectory(filepath)
            Catch ex As Exception
            End Try
        End If
        Try
            Dim request As WebRequest = WebRequest.Create(temp)
            request.Credentials = CredentialCache.DefaultCredentials
            Dim response As HttpWebResponse = CType(request.GetResponse(), HttpWebResponse)
            SourceStream = response.GetResponseStream
            Dim Buffer(4096) As Byte
            Dim BlockSize As Integer
            Dim writeStream As FileStream = New FileStream(fullfilename, FileMode.Create, FileAccess.Write)
            Do
                BlockSize = SourceStream.Read(Buffer, 0, 4096)
                If BlockSize > 0 Then writeStream.Write(Buffer, 0, BlockSize)
            Loop While BlockSize > 0
            writeStream.Flush()
            writeStream.Close()
            SourceStream.Close()
            response.Close()
        Catch ex As Exception
            Response.Redirect("/404", True)
        End Try

        Dim fileinfo As FileInfo = New FileInfo(fullfilename)
        Dim filename As String = Regex.Replace(xName, "[^\w]", "")
        'WebEventLog.WriteToEventLog("filename - " & filename)

        Response.Buffer = False 'transmitfile self buffers
        Response.Clear()
        Response.ClearContent()
        Response.ClearHeaders()

        Select Case xCache.ToUpper
            Case "FRESH"
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.AppendHeader("Pragma", "no-cache")
                Response.AppendHeader("Connection", "close")
                Response.AppendHeader("Expires", "Wed, 31 Jan 2001 00:00:00 GMT")
                Response.AppendHeader("Date", "")
            Case "DAY"
                Dim T1970 As New DateTime(1970, 1, 1)
                Dim Timestamp As Integer = (DateTime.Now.ToUniversalTime - T1970).TotalSeconds
                Dim TDay As New Date(Today.Year, Today.Month, Today.Day, 0, 0, 1)
                Dim TDayOffset As Integer = (DateTime.Now.ToUniversalTime - TDay).TotalSeconds
                Dim TimestampToday As Integer = Timestamp - TDayOffset

                If Not Request.Headers("If-Modified-Since") Is Nothing Then

                    Dim tSince As String = Request.Headers("If-Modified-Since")
                    Dim t As Integer = System.Text.RegularExpressions.Regex.Replace(tSince, "[^0-9+]", "")

                    If Timestamp < (t + 86400) Then

                        Response.StatusCode = 304
                        Response.StatusDescription = "Not Modified"
                        Return

                    End If

                End If
                Response.Cache.SetCacheability(HttpCacheability.Public)
                Response.AppendHeader("Pragma", "public")
                Response.AppendHeader("Connection", "close")
                Response.AppendHeader("Last-Modified", TimestampToday & " GMT")
                Response.AppendHeader("ETag", "Dyno")

            Case "WEEK"
                Dim T1970 As New DateTime(1970, 1, 1)
                Dim Timestamp As Integer = (DateTime.Now.ToUniversalTime - T1970).TotalSeconds
                Dim firstDayThisWeek As Date = Today.AddDays(Today.DayOfWeek)
                Dim TWeek As New Date(firstDayThisWeek.Year, firstDayThisWeek.Month, firstDayThisWeek.Day, 0, 0, 1)
                Dim TWeekOffset As Integer = (DateTime.Now.ToUniversalTime - TWeek).TotalSeconds
                Dim TimestampWeek As Integer = Timestamp - TWeekOffset - 518400

                If Not Request.Headers("If-Modified-Since") Is Nothing Then

                    Dim tSince As String = Request.Headers("If-Modified-Since")
                    Dim t As Integer = System.Text.RegularExpressions.Regex.Replace(tSince, "[^0-9+]", "")

                    If Timestamp < (t + 604800) Then

                        Response.StatusCode = 304
                        Response.StatusDescription = "Not Modified"
                        Return

                    End If
                End If

                Response.Cache.SetCacheability(HttpCacheability.Public)
                Response.AppendHeader("Pragma", "public")
                Response.AppendHeader("Connection", "close")
                Response.AppendHeader("Last-Modified", TimestampWeek & " GMT")
                Response.AppendHeader("ETag", "Dyno")
            Case "MONTH"
                Dim T1970 As New DateTime(1970, 1, 1)
                Dim Timestamp As Integer = (DateTime.Now.ToUniversalTime - T1970).TotalSeconds
                Dim TMonth As New Date(Today.Year, Today.Month, 1, 0, 0, 1)
                Dim TMonthOffset As Integer = (DateTime.Now.ToUniversalTime - TMonth).TotalSeconds
                Dim TimestampMonth As Integer = Timestamp - TMonthOffset

                If Not Request.Headers("If-Modified-Since") Is Nothing Then

                    Dim tSince As String = Request.Headers("If-Modified-Since")
                    Dim t As Integer = System.Text.RegularExpressions.Regex.Replace(tSince, "[^0-9+]", "")

                    If Timestamp < (t + 2592000) Then

                        Response.StatusCode = 304
                        Response.StatusDescription = "Not Modified"
                        Return

                    End If

                End If
                Response.Cache.SetCacheability(HttpCacheability.Public)
                Response.AppendHeader("Pragma", "public")
                Response.AppendHeader("Connection", "close")
                Response.AppendHeader("Last-Modified", TimestampMonth & " GMT")
                Response.AppendHeader("ETag", "Dyno")

            Case "ALWAYS"
                Response.Cache.SetCacheability(HttpCacheability.Public)
                Response.AppendHeader("Pragma", "public")
                Response.AppendHeader("Connection", "close")
                Response.AppendHeader("Expires", "Wed, 31 Dec 2025 00:00:00 GMT")
                Response.AppendHeader("Date", "")
            Case "FOREVER"
                If Not Request.Headers("If-Modified-Since") Is Nothing Then
                    Response.StatusCode = 304
                    Response.StatusDescription = "Not Modified"
                    Return
                Else
                    Response.Cache.SetCacheability(HttpCacheability.Public)
                    Response.AppendHeader("Pragma", "public")
                    Response.AppendHeader("Connection", "close")
                    Response.AppendHeader("Last-Modified", "1325376000 GMT")
                    Response.AppendHeader("Expires", "1767139201 GMT")
                    Response.AppendHeader("ETag", "Dyno")
                End If
            Case Else
                Response.Redirect("/404", True)
        End Select

        Dim mime_type As String = ""
        Select Case xExtension
            Case "bmp"
                mime_type = "image/bmp"
            Case "doc"
                mime_type = "application/msword"
            Case "gif"
                mime_type = "image/gif"
            Case "jpeg"
                mime_type = "image/jpeg"
            Case "jpg"
                mime_type = "image/jpeg"
            Case "pdf"
                mime_type = "application/pdf"
            Case "tif"
                mime_type = "image/tiff"
            Case "tiff"
                mime_type = "image/tiff"
            Case "txt"
                mime_type = "text/plain"
            Case "xls"
                mime_type = "application/ms-excel"
            Case "zip"
                mime_type = "application/zip"
            Case Else
                Response.Redirect("/404", True)
        End Select
        Response.ContentType = mime_type
        Response.AddHeader("Content-Length", fileinfo.Length)
        Dim header As String = ""
        Select Case xDisposition
            Case "view"
                header = "inline; filename=" & filename & "." & xExtension
            Case "download"
                header = "attachment; filename=" & filename & "." & xExtension
        End Select
        Response.AddHeader("Content-Disposition", header)
        Response.TransmitFile(fullfilename) 'transmitfile keeps entire file from loading into memory
        Response.End()



    End Sub

End Class