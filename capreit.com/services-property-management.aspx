﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="services-property-management.aspx.vb" Inherits="services_property_management" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/OneColumn.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content">
        <div class="tabs-content transparent-black-box no-top-border">
            <div id="property-management">
                <h1 class="headline  cms cmsType_TextSingle cmsName_Property_Management_Page_Heading">Property Management</h1>

        <ul class="tabs-holder">
            <li>
                <a href="services">overview</a><ins> | </ins>
            </li>
            <li>
                <a href="services-resident">resident services</a><ins> | </ins>
            </li>
            <li>
                <a href="services-property-management" class="active">property management</a><ins> | </ins>
            </li>
            <li>
                <a href="services-asset-management">asset management</a>
            </li>
        </ul>

                <div class="text">
                  <p class="cms cmsType_TextMulti cmsName_Property_Management_Page_Content">
                    CAPREIT is a company of real estate experts, each of whom has years of experience in the multifamily sector. Members of CAPREIT’s senior management team have worked together for many years in professionaly managing and positioning multifamily properties to achieve optimal value.
                  </p>
                </div>
                <div class="accordion uppercased-titles cmsGroup cmsName_Property_Management_Accordion">

<!--
                    <h3><strong class="cms cmsType_TextSingle cmsName_Title">Lorem ipsum dolor sit amet, consectetur</strong><ins>&nbsp;</ins></h3>
                    <div class="billow">
                        <div class="text cms cmsType_Rich cmsName_Content">
                            <p>Praesent in metus massa. Aenean gravida blandit leo, id vulputate tellus egestas ut. Pellentesque orci eros, cursus a mollis et, malesuada et est. Suspendisse consequat molestie lobortis. Aenean nulla libero, auctor at rhoncus aliquet, pulvinar a dui. Donec pharetra ullamcorper scelerisque. Vestibulum eget purus enim, eu congue nunc. Duis fringilla blandit tellus, at aliquet elit sagittis et. Sed quis facilisis erat.</p>
                        </div>
                    </div>
-->


                        <%
    For i As Integer = 1 To 20 - 1
                                If IO.Directory.GetFiles(Server.MapPath("~") & "cms\data\", "Property_Management_Accordion_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then
   %>

             
                        <ins class="cmsGroupItem">
                    <h3><strong class="cms cmsType_TextSingle cmsName_Property_Management_Accordion_Item_<%=i%>_Title"></strong><ins>&nbsp;</ins></h3>
                    <div class="billow">
                        <div class="text cms cmsType_Rich cmsName_Property_Management_Accordion_Item_<%=i%>_Content">
                            
                        </div>
                    </div>
                   </ins>

 <%End If %>
 <%Next%>  

                   
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar services">
        <a class="next-arrow ajax-next-arrow" href="services-asset-management"> <ins><var>&nbsp;</var></ins><span><strong>asset management</strong></span></a>
        <div class="switchable-quotes">
            <div class="transparent-blue-box quote-box"> <big class="cms cmsType_TextSingle cmsName_Property_Management_Page_Quote_Heading">OUR VALUES</big><em class="cms cmsType_TextMulti cmsName_Property_Management_Page_Quote_Content">Integrity, Relationships and Results,  the new IRR</em><var class="triangle-figure">&nbsp;</var></div>
        </div>
    </div>
</asp:Content>

