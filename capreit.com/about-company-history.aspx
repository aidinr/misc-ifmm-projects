﻿<%@ Page Title="" Language="VB" MasterPageFile="~/OneColumn.master" AutoEventWireup="false" CodeFile="about-company-history.aspx.vb" Inherits="about_company_history" %>
<%@ Import Namespace="System.IO" %>
<%@ MasterType  virtualPath="~/OneColumn.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content">
        <div class="tabs-content transparent-black-box no-top-border">
            <div id="company-history">
                <h1 class="headline cms cmsType_TextSingle cmsName_History_Page_Heading">Company History</h1>
                <ul class="tabs-holder">
                    <li>
                        <a href="about">overview</a><ins> | </ins>
                    </li>
                    <li>
                        <a href="about-investment-strategy">investment strategy</a><ins> | </ins>
                    </li>
                    <li>
                        <a href="about-leadership">leadership</a><ins> | </ins>
                    </li>
                    <li>
                        <a href="about-company-history">company history</a>
                    </li>
                </ul>

                <div id="scroller" class="comapny-history-scroller">
                    <ul class="cmsGroup cmsName_History_Slides">
                        <!--
                            <h2 class="cms cmsType_TextSingle cmsName_Heading">2022</h2>
                            <div class="text">
                                <img src="assets/images/temp/history1.jpg" class="cms cmsType_Image cmsName_Image" alt="" />
                                <p class="cms cmsType_TextMulti cmsName_Content">The company completes the sale of 45-property portfolio to EQR for $608 million, netting a gain in excess of $125 million to CAPREIT and Apollo</p>
                            </div>
                        -->


                        <% For i As Integer = 1 To 50 - 1
                                If IO.Directory.GetFiles(Server.MapPath("~") & "cms\data\", "History_Slides_Item_" & i & "_*", SearchOption.AllDirectories).Length > 0 Then 
                        %>

                        <li class="cmsGroupItem">
                            <h2 class="cms cmsType_TextSingle cmsName_History_Slides_Item_<%=i%>_Heading"></h2>
                            <div class="text">
                                <img class="cms cmsType_Image cmsName_History_Slides_Item_<%=i%>_Image" align="left" src="cms/data/Sized/liquid/187x/65/ffffff/Center/<%=CMSFunctions.Image(Server.MapPath("~") & "cms\data\", "History_Slides_Item_" & i &"_Image")%>" alt="<%=CMSFunctions.GetContentAlt(Server.MapPath("~") & "cms\data\Image\", "History_Slides_Item_" & i &"_Image")%>" />
                                <p class="cms cmsType_TextMulti cmsName_History_Slides_Item_<%=i%>_Content"></p>
                            </div>
                        </li>

                        <%End If %>
                        <%Next%>  



                    
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar about">
        <a class="next-arrow ajax-next-arrow" href="about"> <ins><var>&nbsp;</var></ins><span><strong>overview</strong></span></a>
        <div class="switchable-quotes">
            <div class="transparent-blue-box quote-box"> <em class="cms cmsType_TextMulti cmsName_History_Page_Quote">Success is never an accident. It is always the result of high intention, sincere effort, intelligent direction and skillful execution.</em>
                <var class="triangle-figure">&nbsp;</var>
            </div>
        </div>
    </div>
</asp:Content>

