﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Kettler_WS
    Inherits System.Web.Services.WebService

    Public Function GetDataSet() As DataSet
        'To be changed to read from iDAMAssetService

        Dim DS As New DataSet

        'Try
        Dim MyConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("CONNECTIONSTRING"))
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select a.Asset_ID Asset_ID, a.Name Unit, b.Name Building, cast(u1.Item_Value as integer) Floor, u2.Item_Value Type, u3.Item_Value Bathroom, cast(u4.Item_Value as integer) Area, cast(u5.Item_Value as integer) AS Price, u6.Item_Value Views, u7.item_value Available, u8.item_value Model_ID, u9.asset_id Model_AssetID, u9.name Model_Name, u5.item_value As Price_String, u100.item_value as Featured, isnull(u101.item_value, '') as Amenities  from ipm_project b,((((((((((( ipm_asset a  left join ipm_asset_field_value u1 on a.asset_id = u1.asset_id and u1.item_id = 21550631 ) left join ipm_asset_field_value u2 on a.asset_id = u2.asset_id and u2.item_id = 21550632) left join ipm_asset_field_value u3 on a.asset_id = u3.asset_id and u3.item_id = 21550634) left join ipm_asset_field_value u4 on a.asset_id = u4.asset_id and u4.item_id = 21550635) left join ipm_asset_field_value u5 on a.asset_id = u5.asset_id and u5.item_id = 21550636) left join ipm_asset_field_value u6 on a.asset_id = u6.asset_id and u6.item_id = 21550637) left join ipm_asset_field_value u7  on   a.asset_id = u7.asset_id and u7.item_id = 21550638) left join ipm_asset_field_value u8  on   a.asset_id = u8.asset_id and u8.item_id = 21550857 ) left join ipm_asset_field_value u100  on   a.asset_id = u100.asset_id and u100.item_id = 420951 ) left join ipm_asset_field_value u101  on   a.asset_id = u101.asset_id and u101.item_id = 423783 )  left join (select asset_id,description,name from ipm_asset where available = 'y' and projectid = 224832 and version_id = 0) u9  on   u8.item_value = u9.description)  where a.projectid in (select projectid from ipm_project where category_id = 224825 and (projectid = 224826 or projectid = 224827 or projectid = 224828) ) and a.type <> 48 and a.available = 'y' and a.projectid = b.projectid and b.available = 'y' order by upload_date desc", MyConnection)
        Dim MyCommand2 As SqlDataAdapter = New SqlDataAdapter("select a.asset_id Parent_ID,a.associated_id Image_Asset_ID,b.description Model_ID from ipm_asset_associated a, ipm_asset b where a.asset_id = b.asset_id and b.available = 'y' and b.projectid in (select projectid from ipm_project where category_id = 224825 and (projectid = 224832) )", MyConnection) 'only from models project
        Dim MyCommand3 As SqlDataAdapter = New SqlDataAdapter("select asset_id Gallery_Image_ID, a.name Gallery_Image_Name, a.description Gallery_Image_Description, b.name Gallery_Category  from ipm_asset a,ipm_asset_category b where a.available = 'y' and b.available = 'y' and b.projectid = 224833 and a.projectid = b.projectid and a.category_id = b.category_id", MyConnection)
        Dim MyCommand4 As SqlDataAdapter = New SqlDataAdapter("select description,descriptionmedium from ipm_project where projectid = 225028 and available = 'y'", MyConnection)
        Dim MyCommand5 As SqlDataAdapter = New SqlDataAdapter("select asset_id,name,description from ipm_asset where projectid = 225028 and available = 'y'", MyConnection)
        Dim MyCommand6 As SqlDataAdapter = New SqlDataAdapter("select description,descriptionmedium from ipm_project where projectid = 225029 and available = 'y'", MyConnection)
        Dim MyCommand7 As SqlDataAdapter = New SqlDataAdapter("select asset_id,name,description from ipm_asset where projectid = 225029 and available = 'y'", MyConnection)
        Dim MyCommand8 As SqlDataAdapter = New SqlDataAdapter("select description,descriptionmedium from ipm_project where projectid = 225030 and available = 'y'", MyConnection)
        Dim MyCommand9 As SqlDataAdapter = New SqlDataAdapter("select asset_id,name,description from ipm_asset where projectid = 225030 and available = 'y'", MyConnection)
        Dim MyCommand10 As SqlDataAdapter = New SqlDataAdapter("select description,descriptionmedium from ipm_project where projectid = 225031 and available = 'y'", MyConnection)
        Dim MyCommand11 As SqlDataAdapter = New SqlDataAdapter("select asset_id,name,description from ipm_asset where projectid = 225031 and available = 'y'", MyConnection)
        Dim MyCommand12 As SqlDataAdapter = New SqlDataAdapter("select asset_id,name,description from ipm_asset where projectid = " & System.Configuration.ConfigurationManager.AppSettings("homePID") & " and available = 'y'", MyConnection) 'Home page


        Dim DTU As New DataTable("Units")
        Dim DTI As New DataTable("Images")
        Dim DTG As New DataTable("Galleries")
        Dim DTLHD As New DataTable("LiveHereDescription")
        Dim DTLHA As New DataTable("LiveHereAssets")
        Dim DTAD As New DataTable("AmenitiesDescription")
        Dim DTAA As New DataTable("AmenitiesAssets")
        Dim DTSD As New DataTable("ServicesDescription")
        Dim DTSA As New DataTable("ServicesAssets")
        Dim DTAKD As New DataTable("AboutKettlerDescription")
        Dim DTAKA As New DataTable("AboutKettlerAssets")
        Dim DTHA As New DataTable("HomeAssets")

        MyCommand1.Fill(DTU)
        MyCommand2.Fill(DTI)
        MyCommand3.Fill(DTG)
        MyCommand4.Fill(DTLHD)
        MyCommand5.Fill(DTLHA)
        MyCommand6.Fill(DTAD)
        MyCommand7.Fill(DTAA)
        MyCommand8.Fill(DTSD)
        MyCommand9.Fill(DTSA)
        MyCommand10.Fill(DTAKD)
        MyCommand11.Fill(DTAKA)
        MyCommand12.Fill(DTHA)

        For Each Row As DataRow In DTU.Rows
            If Row("Available") = "1" Then
                Row("Available") = "Vacant"
                Row("Price_String") = FormatCurrency(Row("Price_String"), 0)
            Else
                Row("Available") = "Occupied"
                Row("Price") = 0
                Row("Price_String") = "N/A"
            End If

            Row("Building") = Trim(Row("Building"))

        Next

        DS.Tables.Add(DTU)
        DS.Tables.Add(DTI)
        DS.Tables.Add(DTG)
        DS.Tables.Add(DTLHD)
        DS.Tables.Add(DTLHA)
        DS.Tables.Add(DTAD)
        DS.Tables.Add(DTAA)
        DS.Tables.Add(DTSD)
        DS.Tables.Add(DTSA)
        DS.Tables.Add(DTAKD)
        DS.Tables.Add(DTAKA)
        DS.Tables.Add(DTHA)

        'DS.WriteXml("C:\Projects\Kettler_WS\Kettler_WS.xml")
        ' Catch SQLException As SqlException
        'DS.ReadXml("C:\Projects\Kettler_WS\Kettler_WS.xml")
        'Finally

        ' End Try



        Return DS

    End Function

    <WebMethod()> _
    Public Function GetAllProjects() As DataSet
        Dim d As DataSet

        d = GetDataSet()

        Return d
    End Function

End Class
