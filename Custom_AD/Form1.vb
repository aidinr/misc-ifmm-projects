﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Serialization
Imports log4net
Imports System.Net.Mail
Imports System.Data.OleDb
Imports System
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.DirectoryServices
Imports System.Security
Imports System.Security.Principal
Imports System.Threading
Imports System.Globalization
Imports System.Windows.Forms
Imports System.Text.StringBuilder
Imports ADLibrary


Public Class Form1
    Private Shared ReadOnly Log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Dim dtUsers As DataTable
    Public Shared ADAdminUser As String = My.Settings.LDAPAdminUser
    ' = "administrator"; //Administrator username of DC
    Public Shared ADAdminPassword As String = My.Settings.LDAPAdminPassword
    ' = "password"; //Password of admin user on DC
    'This needs to have the domain name or IP address on this line
    Public Shared ADFullPath As String = My.Settings.LDAPRootpath
    Public Shared ADServer As String = My.Settings.LDAPServer
    Private con As New SqlConnection(My.Settings.ConnString)
    ' = "LDAP://10.3.12.250"; 
    'This must be the domain name of the domain controller (not the computer name, just the domain name)
    'Public Shared ADServer As String = "IFMM"
    ' = "sakura.com";
    '		private static string ADPath
    Sub LoadGroups()
    End Sub
    Sub LoadUserList()

    End Sub
    Private Shared Function GetDirectoryObject() As DirectoryEntry
        Dim oDE As DirectoryEntry

        oDE = New DirectoryEntry(ADFullPath, ADAdminUser, ADAdminPassword, AuthenticationTypes.Secure)

        Return oDE
    End Function

    Private Shared Function GetDirectoryObject(ByVal DomainReference As String) As DirectoryEntry
        Dim oDE As DirectoryEntry

        oDE = New DirectoryEntry(ADServer + DomainReference, ADAdminUser, ADAdminPassword, AuthenticationTypes.Secure)

        Return oDE
    End Function


    Public Shared Function GetAllGroups(GroupName As String) As DataSet
        Dim dsGroup As New DataSet()
        'Dim dirEntry As DirectoryEntry = GetDirectoryObject("DC=" & ADServer)
        Dim dirEntry As DirectoryEntry = GetDirectoryObject()

        'create instance fo the direcory searcher
        Dim dirSearch As New DirectorySearcher()

        'set the search filter
        dirSearch.SearchRoot = dirEntry
        dirSearch.SearchScope = SearchScope.Subtree
        'deSearch.PropertiesToLoad.Add("cn");
        'dirSearch.Filter = "(&(objectClass=group)(cn=CS_*))"
        dirSearch.Filter = "(&(objectClass=group)(cn=" + GroupName + "*))"

        'find the first instance
        Dim searchResults As SearchResultCollection = dirSearch.FindAll()

        'Create a new table object within the dataset
        Dim dtGroup As DataTable = dsGroup.Tables.Add("Groups")
        dtGroup.Columns.Add("GroupName")

        'if there are results (there should be some!!), then convert the results
        'into a dataset to be returned.
        If searchResults.Count > 0 Then

            'iterate through collection and populate the table with
            'the Group Name
            For Each Result As SearchResult In searchResults
                'set a new empty row
                Dim drGroup As DataRow = dtGroup.NewRow()

                'populate the column
                drGroup("GroupName") = Result.Properties("cn")(0)

                'append the row to the table of the dataset
                dtGroup.Rows.Add(drGroup)
            Next
        End If

        '---------------- UNIVERSAL GROUPS --------------------------

        'set the search filter
        dirSearch.SearchRoot = dirEntry
        dirSearch.SearchScope = SearchScope.Subtree
        'deSearch.PropertiesToLoad.Add("cn");
        'dirSearch.Filter = "(&(objectClass=group)(cn=CS_*))"
        dirSearch.Filter = "(&(objectClass=group)(groupType:1.2.840.113556.1.4.803:=8)(cn=" + GroupName + "*))"

        'find the first instance
        searchResults = dirSearch.FindAll()

        'Create a new table object within the dataset
        'Dim dtGroup As DataTable = dsGroup.Tables.Add("Groups")
        'dtGroup.Columns.Add("GroupName")

        'if there are results (there should be some!!), then convert the results
        'into a dataset to be returned.
        If searchResults.Count > 0 Then

            'iterate through collection and populate the table with
            'the Group Name
            For Each Result As SearchResult In searchResults
                'set a new empty row
                Dim drGroup As DataRow = dtGroup.NewRow()

                'populate the column
                drGroup("GroupName") = Result.Properties("cn")(0)

                'append the row to the table of the dataset
                dtGroup.Rows.Add(drGroup)
            Next
        End If


        Return dsGroup
    End Function

    Public Shared Function GetDirectoryEntry() As DirectoryEntry
        Dim dirEntry As DirectoryEntry = New DirectoryEntry()
        dirEntry.Path = My.Settings.LDAPRootpath
        dirEntry.Username = My.Settings.LDAPAdminUser
        dirEntry.Password = My.Settings.LDAPAdminPassword
        Return dirEntry
    End Function
    Public Shared Function GetDirectoryEntry(path As String) As DirectoryEntry
        Dim dirEntry As DirectoryEntry = New DirectoryEntry()
        dirEntry.Path = path
        dirEntry.Username = My.Settings.LDAPAdminUser
        dirEntry.Password = My.Settings.LDAPAdminPassword
        Return dirEntry
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        LoadGroups()

    End Sub
    Public Shared Function GetProperty(ByVal oDE As DirectoryEntry, ByVal PropertyName As String) As String
        Try

            Return oDE.Properties(PropertyName)(0).ToString()
        Catch ex As Exception
            Return String.Empty
        End Try

    End Function

    ''' <summary>
    ''' This is an override that will allow a property to be extracted directly from
    ''' a searchresult object
    ''' </summary>
    ''' <param name="searchResult"></param>
    ''' <param name="PropertyName"></param>
    ''' <returns></returns>
    Public Shared Function GetProperty(ByVal searchResult As SearchResult, ByVal PropertyName As String) As String
        If searchResult.Properties.Contains(PropertyName) Then
            Return searchResult.Properties(PropertyName)(0).ToString()
        Else
            Return String.Empty
        End If
    End Function

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Me.Close()
        'Me.Dispose()
        txtADFullPath.Text = ADFullPath
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs)
        LoadUserList()
    End Sub

    Public Shared Function GetUser(ByVal UserName As String) As DirectoryEntry
        'create an instance of the DirectoryEntry
        Dim dirEntry As DirectoryEntry = GetDirectoryObject()
        'Dim dirEntry As DirectoryEntry = GetDirectoryObject()

        'create instance fo the direcory searcher
        Dim dirSearch As New DirectorySearcher(dirEntry)

        dirSearch.SearchRoot = dirEntry
        'set the search filter
        dirSearch.Filter = "(&(objectCategory=user)(cn=" + UserName + "))"
        'deSearch.SearchScope = SearchScope.Subtree;

        'find the first instance
        Dim searchResults As SearchResult = dirSearch.FindOne()

        'if found then return, otherwise return Null
        If Not searchResults Is Nothing Then
            'de= new DirectoryEntry(results.Path,ADAdminUser,ADAdminPassword,AuthenticationTypes.Secure);
            'if so then return the DirectoryEntry object
            Return searchResults.GetDirectoryEntry()
        Else
            Return Nothing
        End If
    End Function


    Private Sub btnLogin_Click(sender As System.Object, e As System.EventArgs) Handles btnLogin.Click
        'Dim objAd As New ADClass()

        If ADClass.LoginUser(txtUsername.Text, txtPassword.Text, txtADFullPath.Text) = True Then
            MsgBox("Logged in")
        Else
            MsgBox("Not Logged in")
        End If
    End Sub
    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
        Dim ds As New DataSet
        ds = ADClass.GetUsersinGroup(txtUsername.Text, txtPassword.Text, txtADFullPath.Text, "badPwdCount", "0", "", "")
    End Sub


    Private Sub btnImport_Click(sender As System.Object, e As System.EventArgs)
    End Sub
    Private Sub importUser(deUser As DirectoryEntry)
        Try

            Dim firstname As String = GetProperty(deUser, "givenname")
            Dim lastname As String = GetProperty(deUser, "sn")
            Dim userid As String = GetProperty(deUser, "sAMAccountName")
            Dim email As String = GetProperty(deUser, "mail")
            Dim username As String

            ' for hdr they configure username with sAMAccountName
            'username = "cn=" & GetProperty(deUser, "cn") & IIf(GetProperty(deUser, "ou") <> "", ",ou=" & GetProperty(deUser, "ou"), "") & IIf(GetProperty(deUser, "o") <> "", ",o=" & GetProperty(deUser, "o"), "")
            username = userid

            Dim maxCounter As String = GetMaxCounterID().ToString()
            con.Open()
            Dim cmd As New SqlCommand("insert into IPM_User (userid, securitylevel_id, firstname, lastname, email, regdate, regupdatedate, login, username, LDAP_Authentication, Active, update_date) " & _
                                      " values (@maxCounter , 1, @firstname , @lastname , @email , getDate(), getDate(), @Login, @username, 1, 'Y', getDate() )  ", con)
            cmd.Parameters.AddWithValue("@maxCounter", maxCounter)
            cmd.Parameters.AddWithValue("@firstname", firstname)
            cmd.Parameters.AddWithValue("@lastname", lastname)
            cmd.Parameters.AddWithValue("@email", email)
            cmd.Parameters.AddWithValue("@Login", userid)
            cmd.Parameters.AddWithValue("@username", username)
            cmd.ExecuteNonQuery()

            cmd = New SqlCommand("insert into IPM_User_role (userid, role_id) " & _
                                      " select @maxCounter, role_id from ipm_role where name = 'Default'  ", con)
            cmd.Parameters.AddWithValue("@maxCounter", maxCounter)
            cmd.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            MsgBox("There was an issue with the Active Direcotry Import Module: " & ex.Message)
            con.Close()
        End Try

    End Sub
    Private Function GetMaxCounterID() As Integer
        Dim MyConnection As SqlConnection = New SqlConnection(My.Settings.ConnString)
        Dim MyCommand1 As SqlDataAdapter = New SqlDataAdapter("select counter from ipm_cat_counter", MyConnection)
        Dim DT1 As New DataTable("counter")
        MyCommand1.Fill(DT1)

        Dim MyCommand2 As New SqlCommand("update ipm_cat_counter set counter = counter + 1", MyConnection)
        MyCommand2.Connection.Open()
        MyCommand2.ExecuteNonQuery()
        MyCommand2.Connection.Close()

        Return DT1.Rows(0)("counter")
    End Function

    Private Function userExistsInIdam(userid) As Boolean
        Try

            con.Open()
            Dim cmd As New SqlCommand("select Login from IPM_User where login = @userid  ", con)
            cmd.Parameters.AddWithValue("@userid", userid)
            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable()
            da.Fill(dt)

            If dt.Rows.Count > 0 Then
                con.Close()
                Return True
            Else
                con.Close()
                Return False
            End If
        Catch ex As Exception
            MsgBox("There was an issue with the Active Direcotry Import Module: " & ex.Message)
            con.Close()
        End Try

    End Function

    Private Sub Label3_Click(sender As System.Object, e As System.EventArgs)

    End Sub

   
    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs)
      
    End Sub

 
End Class
