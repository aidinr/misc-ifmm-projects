$(document).ready(function() {

if( $('a.modal').first().attr('href') ) {

var modalCSS ='\
<style type="text/css">\
#modalBox{\
position: absolute;\
margin: auto;\
top: 70px;\
left: 15%;\
border: 8px solid #000;\
padding: 0;\
display: none;\
z-index: 112;\
}\
#modalBox iframe {\
padding: 0;\
margin: 0 0 0 0;\
display: block;\
float: left;\
z-index: 113;\
width: 500px;\
}\
#modalBox a {\
position: absolute;\
display: block;\
right: 0;\
width: 30px;\
height: 30px;\
background: transparent url(assets/modal/closebox.png) no-repeat scroll 100% 2px;\
cursor: pointer;\
}\
#modalBox a:hover {\
background-position: 100% -28px;\
}\
#modalOverlay {\
position: fixed;\
margin: 0;\
top: 0;\
left: 0;\
width: 100%;\
height: 100%;\
padding: 0;\
display: none;\
background: #707070;\
z-index: 111;\
}\
#modalLoading {\
left: 48%;\
position: fixed;\
z-index: 114;\
top: 33%;\
width: 32px;\
height: 32px;\
padding: 20px;\
border: 5px solid #111;\
background: #fff;\
display: none;\
}\
</style>';

 $('head').append(modalCSS);
 $('body').append('<div id="modalBox"></div><div id="modalOverlay"></div><div id="modalLoading"><img src="assets/modal/loading.gif" alt="Loading" title="Loading" /></div>');
 $("a.modal").click(function () { 
	$("#modalBox").html('<iframe src="'+ $(this).attr('href') +'" width="500" height="315" frameborder="0" marginheight="0" marginwidth="0" onload="modalShow(this)" /></iframe><a href="'+ $(this).attr('href') +'"></a>');
	$("#modalBox").css("opacity", "0");
	$("#modalBox").css("display", "block");

	$("#modalLoading").css("opacity", "0");
	$("#modalLoading").css("display", "block");
	$("#modalLoading").animate({"opacity": 1},1100);

	$("#modalOverlay").css("opacity", "0");
	$("#modalOverlay").css("display", "block");
	$("#modalOverlay").animate({"opacity": .8});
	return false;
 });
 $("#modalBox").click(function () { 
	$("#modalBox").css("display", "none");
	$("#modalOverlay").css("display", "none");
	$("#modalBox").removeAttr('style');
	$("#modalBox").html('');
	return false;
 });

}

});

function modalShow(e) {
 var x = ($('body').width() / 2 ) - (e.width/2);
 $('#modalBox').css('left',x+'px');
 $('#modalBox').animate({"opacity": 1});
 $("#modalLoading").css("display", "none");
}
