﻿<%@ page language="VB" autoeventwireup="false" inherits="_Default, App_Web_da7rlb2n" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>iDAM Login .NET Examples</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    Sample usage (set the IDs dynamically using code):<br /><br />
Image using retrieve asset: <img src="<%=Session("WSRetrieveAsset") %>716698&instance=IDAM_INTERFACE&type=asset&size=0" alt="image" />
<br /><br /><br />

Download PDF using download asset service: <a href="<%=Session("WSDownloadAsset") %>&assetid=761072&instance=IDAM_INTERFACE&size=0">Download PDF</a>
    </div>
    </form>
</body>
</html>
